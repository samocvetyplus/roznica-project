unit err_sell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, servdata, Grids, DBGridEh, StdCtrls, data, comdata;

type
  TfmErr_sell = class(TForm)
    DBGridEh1: TDBGridEh;
    BCor: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BCorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErr_sell: TfmErr_sell;

implementation

{$R *.dfm}

procedure TfmErr_sell.FormCreate(Sender: TObject);
begin
 dmServ.quErr_Sell.Open;
end;

procedure TfmErr_sell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 dmServ.quErr_Sell.Close;
end;

procedure TfmErr_sell.BCorClick(Sender: TObject);
begin
 Screen.Cursor:=crHourGlass;
 with dm.quTmp do
 begin
  sql.Text:='execute procedure correct_error_sell ';
  ExecQuery;
 end;
 dmCom.tr.CommitRetaining;
 dmServ.quErr_Sell.Close;
 dmServ.quErr_Sell.Open;
 Screen.Cursor:=crDefault 
end;

end.
