unit DataFR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComDrv32, RxStrUtils, ExtCtrls, Db, pFIBDataSet, FIBDataSet;

const
   Password: String = '0000';

   ErrStr:array[1..34] of String=('������ � ���������� ������, ������� ����������',
                                  '�� ������� �����',
                                  '�������� ������ ������� ������� � ���������� ������',
                                  '��������� ����� ���� �������',
                                  '�������� ������ ���� �������',
                                  '������ ������ �������',
                                  '�������� ����',
                                  '�������� �����',
                                  '���� ������ ��������� ����, ������������������ � ���������� ������',
                                  '�������� �������� �������������. �������� �����������',
                                  '����������� ������� ��',
                                  '�� ������ �����',
                                  '�� ������������',
                                  '������������ ��������� ������',
                                  '������ ������ � ���������� ������',
                                  '������ ��������� �������',
                                  '�������� ������ �������� ����������',
                                  '�������� ������ �� �����',
                                  '�������� ������ ���������������',
                                  '������� �� ��������������',
                                  '�������� ���� ������� ��� ���������',
                                  '������ ������ ���������� ������',
                                  '������������ ��������',
                                  '������������ ���� ������� ����� ������� �����',
                                  '�������� ������ �������',
                                  '���� ��� ����� ���������� ��������� � ����� �����������',
                                  '�� ������������',
                                  '������ � ������������ ����������',
                                  '��� ����� �������',
                                  '�������� ����������� �����',
                                  '��� ���������� �������',
                                  '����� ������ ����',
                                  '������ �� �������',
                                  '�� �� �������� !!!');
const
  FR_DATETIME = WM_USER + $1001;

type
  TFRDateTime = record
    Msg: Cardinal;
    WParam: Longint;
    LParam: Longint;
    Result: Longint;
    dt : TDateTime;
  end;

  TFRPrinterStatus = record
    Err       : boolean;  // ������
    PrnCheck  : boolean; // ������� ������ ???
    PaperLost : boolean; // ����������� ������
    Ready     : boolean; // ������� �����
    PrnUse    : boolean; // ����� �����, ��������� � ��������� offline ��� ��������� ������
  end;


  TdmFR = class(TDataModule)
    com: TCommPortDriver;
    tm: TTimer;
    taCheck: TpFIBDataSet;
    taCheckID: TIntegerField;
    taCheckNAME: TFIBStringField;
    taCheckLINE: TSmallintField;
    taCheckPOS: TSmallintField;
    taCheckFLAG: TFIBStringField;
    taCheckNAMED: TStringField;
    dsCheck: TDataSource;
    taIns: TpFIBDataSet;
    taInsNAME: TFIBStringField;
    taInsQUANTITY: TSmallintField;
    taInsWEIGHT: TFloatField;
    taInsCOLOR: TFIBStringField;
    taInsCHROMATICITY: TFIBStringField;
    taInsCLEANNES: TFIBStringField;
    taInsEDGTYPE: TFIBStringField;
    taInsEDGSHAPE: TFIBStringField;
    taInsGR: TFIBStringField;
    taInsN: TStringField;
    taCheckATYPE: TSmallintField;
    procedure comReceiveData(Sender: TObject; DataPtr: Pointer; DataSize: Integer);
    procedure DataModuleCreate(Sender: TObject);
    procedure tmTimer(Sender: TObject);
    procedure taInsCalcFields(DataSet: TDataSet);
  private
    CurPos: Integer;
    FRPrinterStatus : TFRPrinterStatus;
    FFRDateTime : TDateTime;
  public
    { Public declarations }
    Status: Integer;
    SendNext, Answer, Err: Boolean;
    OutBuf: array[1..2000] of byte;
    Buf: array[0..200] of byte;
    procedure SendPacket(Code, Size:Integer; var A: array of byte);
    function FRReady : boolean;
    function GetFRPrinterStatus : TFRPrinterStatus;
    function GetFRDateTime(var dt :TDateTime) : boolean;
  end;

var
  dmFR: TdmFR;

implementation

uses comdata, SellItem, Data, MsgDialog;

{$R *.DFM}

procedure TdmFR.SendPacket(Code, Size: Integer; var A: array of byte);
var s, i: Integer;
begin
 Err:=False;
 while not Answer do
  Application.ProcessMessages;
 A[0]:=$02;
 A[1]:=Code;
 for i:=2 to 5 do
  A[i]:=Ord(Password[i-1]);
 s:=0;
 for i:=1 to Size-4 do
  if (s + A[i]) > 255 then s:=s + A[i] - 256
   else s:=s + A[i];
 A[Size-3]:=Ord(Dec2Hex(s ,2)[1]);
 A[Size-2]:=Ord(Dec2Hex(s ,2)[2]);
 A[Size-1]:=$03;
 Answer:=False;
 tm.Enabled:=True;
 com.SendData(@A, Size);
 while not Answer and not Err do
  Application.ProcessMessages;
end;

procedure TdmFR.comReceiveData(Sender: TObject; DataPtr: Pointer;
  DataSize: Integer);
var P: PChar;
    i: Integer;
    s : string;
begin
 tm.Enabled:=False;
 tm.Enabled:=True;
 P:=DataPtr;
 //me.Lines.Add('');
 for i:=1 to DataSize do
  begin
   //if Ord(P^) < 33 then me.Text:=me.Text+IntToStr(Ord(P^))+'!'
   // else me.Text:=me.Text+P^;
   SendNext:=P^ = #6;
   if Ord(P^) = 5 then
    begin
     if MessageDialog('������ ������! ���������� ?', mtError, [mbYes, mbNo], 0) = mrYes then com.SendString(#04)
      else
        begin
         com.SendString(#01);
         Err:=True;
        end;
    end
     else if Ord(P^) = 2 then
           begin
            CurPos:=0;
            Buf[0]:=2;
           end
             else if (CurPos >= 0) and (Buf[CurPos] <> 3) then
                   begin
                    inc(CurPos);
                    Buf[CurPos]:=Ord(P^);
                   end;
   inc(P);
  end;
 if (CurPos > 0) and (Buf[CurPos] = 3) then
  begin
   tm.Enabled:=False;
   Answer:=True;
   Status:=Hex2Dec(chr(Buf[9])+chr(Buf[10])+chr(Buf[11])+chr(Buf[12]));
   if Status = 0 then
     case Buf[1] of
       $48 : begin
         s := chr(Buf[16])+chr(Buf[17])+'.'+chr(Buf[18])+chr(Buf[19])+'.'+chr(Buf[20])+chr(Buf[21])+chr(Buf[22])+chr(Buf[23]);
         s := s+' '+chr(Buf[25])+chr(Buf[26])+':'+chr(Buf[27])+chr(Buf[28]);
         FFRDateTime := StrToDateTime(s);
       end;
       $44 : begin
         s := '$'+chr(Buf[13]) + chr(Buf[14]);
         i := Hex2Dec(s);
         FRPrinterStatus.Err := ((i shr 3) and 1) = 0;
         FRPrinterStatus.PrnCheck := ((i shr 4) and 1) = 1;
         FRPrinterStatus.PaperLost := ((i shr 5) and 1) = 1;
         FRPrinterStatus.Ready := ((i shr 6) and 1) = 1;
         FRPrinterStatus.PrnUse := ((i shr 7) and 1) = 1;
       end;
     end;
   CurPos:=-1;
  end;
end;

procedure TdmFR.DataModuleCreate(Sender: TObject);
begin
 CurPos:=-1;
 Answer:=True;
{ if dmCom.FregCom='COM1' then com.ComPort:=pnCom1
 else if dmCom.FregCom='COM2' then com.ComPort:=pnCom2
      else if dmCom.FregCom='COM3' then com.ComPort:=pnCom3
           else com.ComPort:=pnCom4;  }
end;

procedure TdmFR.tmTimer(Sender: TObject);
begin
 tm.Enabled:=False;
 Answer:=True;
 SendNext:=True;
 Status:=34;
end;

procedure TdmFR.taInsCalcFields(DataSet: TDataSet);
begin
 taInsN.AsString:=taInsNAME.AsString + ' ' + taInsQUANTITY.AsString + ' ' +
                   taInsWEIGHT.AsString + ' ' + taInsEDGSHAPE.AsString + ' ' +
                   taInsEDGTYPE.AsString + ' ' + taInsCLEANNES.AsString + ' ' +
                   taInsCHROMATICITY.AsString + ' ' + taInsGR.AsString + ' ' +
                   taInsCOLOR.AsString;
end;

function TdmFR.FRReady: boolean;
begin
  Result := GetFRPrinterStatus.Ready;
end;

function TdmFR.GetFRPrinterStatus: TFRPrinterStatus;
begin
  if not com.Connected then com.Connect;
  // ������ ��������
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8] := $30;
  SendPacket($44, 12, OutBuf);
  if (Status > 0) then begin
    FRPrinterStatus.Err := True;
    FRPrinterStatus.Ready := False;
  end;
  Result := FRPrinterStatus;
end;

function TdmFR.GetFRDateTime(var dt: TDateTime): boolean;
begin
  // Get FR time
  if not com.Connected then com.Connect;
  //������������� ����-�����
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  SendPacket($48, 10, OutBuf);
  if (Status > 0) then Result := False
  else begin
    Result := True;
    dt := FFRDateTime;
  end;
end;

end.
