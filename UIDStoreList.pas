unit UIDStoreList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGridEh, Mask, DBCtrlsEh, MsgDialog,
  ImgList, ActnList, Menus, ComCtrls, FIBQuery, TB2Item, jpeg,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmUIDStoreList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siEdit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    plMain: TPanel;
    gridUIDStoreList: TDBGridEh;
    edYear: TDBNumberEditEh;
    Label1: TLabel;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    il16: TImageList;
    acRefresh: TAction;
    acEdit: TAction;
    acExit: TAction;
    acCheckUIDStoreData: TAction;
    stbrMain: TStatusBar;
    pmEdit: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    SpeedItem1: TSpeedItem;
    acTotal: TAction;
    acViewByArt: TAction;
    acViewByUID: TAction;
    acFillEntryData: TAction;
    acFillOutData: TAction;
    SpeedItem2: TSpeedItem;
    ppDoc: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    acShowId: TAction;
    acPrn: TAction;
    SpeedItem3: TSpeedItem;
    pmTotal: TTBPopupMenu;
    acWithCom: TAction;
    acWithOutCom: TAction;
    biWithCom: TTBItem;
    biWithOutCom: TTBItem;
    siHelp: TSpeedItem;
    procedure edYearChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acEditUpdate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acCheckUIDStoreDataExecute(Sender: TObject);
    procedure acCheckUIDStoreDataUpdate(Sender: TObject);
    procedure acTotalExecute(Sender: TObject);
    procedure acViewByUIDExecute(Sender: TObject);
    procedure acViewByArtExecute(Sender: TObject);
    procedure acFillEntryDataExecute(Sender: TObject);
    procedure acFillEntryDataUpdate(Sender: TObject);
    procedure acFillOutDataExecute(Sender: TObject);
    procedure acFillOutDataUpdate(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure acRefreshUpdate(Sender: TObject);
    procedure acTotalUpdate(Sender: TObject);
    procedure acPrnExecute(Sender: TObject);
    procedure acWithOutComExecute(Sender: TObject);
    procedure acWithComExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  
  private
    LogOperIdForm : string;
    procedure WriteTimeExec(CallTime : Cardinal);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    procedure Create_UIDStore;
  end;

var
  fmUIDStoreList: TfmUIDStoreList;
  bd, ed : TDateTime;

  implementation

uses Data3, DateUtils, m207Proc, Period, comdata, DB, UIDStore,
  UIDStoreCheck, UIDStoreItem, data, UIDStore_T, dbUtil{, MsgDialog},
  JewConst, ReportData, UIDStore_T_C, CLSINV_CHECK;

{$R *.dfm}

procedure TfmUIDStoreList.edYearChange(Sender: TObject);
begin
  dm3.UIDStoreYear := VarAsType(edYear.Value, varInteger);
  ReOpenDataSets([dm3.taUIDStoreList]);
end;

procedure TfmUIDStoreList.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  tb2.WallPaper := wp;
  //ppDoc.Skin := dmCom.PopupSkin;
  dm3.UIDStoreYear := YearOf(Now);
  edYear.OnChange := nil;
  edYear.Value := dm3.UIDStoreYear;
  edYear.OnChange := edYearChange;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  dmCom.tr.StartTransaction;
  OpenDataSets([dm3.taUIDStoreList]);
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStoreList.acAddExecute(Sender: TObject);
const
  mDay : array[1..12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var
  Bookmark : Pointer;
  Month : word;
  LogOperationID: string;
begin
  Bookmark := dm3.taUIDStoreList.GetBookmark;
  Month := 1;
  dm3.taUIDStoreList.DisableControls;
  try
    dm3.taUIDStoreList.First;
    while not dm3.taUIDStoreList.Eof do
    begin
      if MonthOf(dm3.taUIDStoreListSDATE.AsDateTime) > Month then
        Month := MonthOf(dm3.taUIDStoreListSDATE.AsDateTime);
      dm3.taUIDStoreList.Next;
    end;
  finally
    dm3.taUIDStoreList.GotoBookmark(Bookmark);
    dm3.taUIDStoreList.EnableControls;
  end;
  if not dm3.taUIDStoreList.IsEmpty then Inc(Month);
  bd := EncodeDate(edYear.Value, Month, 1);
  if IsLeapYear(edYear.Value)and(Month=2)then ed := EncodeDate(edYear.Value, Month, 29)
  else ed := EncodeDate(edYear.Value, Month, mDay[Month]);
  if GetPeriod(bd, ed) then
  begin
  if fmPeriod.ModalResult=mrOk then
  begin
    if dm.countmaxpross then sysUtils.Abort;  
    LogOperationID := dm3.insert_operation(sLog_UIDStoreAdd, LogOperIdForm);
    ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);
  //end;
   with dm, quTmp do
    begin
     Close;
     SQL.Text:='execute procedure CLSINV_period('''+datetimetostr(StartOfTheDay(bd))+''','''+datetimetostr(EndOfTheDay(ed))+''')';
     ExecQuery;
     if not(fields[0].IsNull) then
     begin
     if ShowAndFreeForm(TfmClsinv_Check, Self, TForm(fmClsinv_Check), True, False)=mrOk then
     begin
       dm3.taUIDStoreList.Append;
       dm3.taUIDStoreListSDATE.AsDateTime := StartOfTheDay(bd);
       dm3.taUIDStoreListNDATE.AsDateTime := EndOfTheDay(ed);
    with dm, quTmp do
    begin
      Close;
      SQL.Text:='select max(sn) from sinv where itype = 15 and FYEAR(Sdate)= FYEAR('''+
                dm3.taUIDStoreListSDATE.Asstring+''')';
      ExecQuery;
      if fields[0].IsNull then  dm3.taUIDStoreListSN.AsInteger := 1
      else dm3.taUIDStoreListSN.AsInteger := fields[0].AsInteger+1;
      Close;
    end;
    dm3.taUIDStoreList.Post;
    Application.ProcessMessages;
    Create_UIDStore;
    ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);
    dm3.update_operation(LogOperationID);
     end;

     end
     else
     begin
         dm3.taUIDStoreList.Append;
    dm3.taUIDStoreListSDATE.AsDateTime := StartOfTheDay(bd);
    dm3.taUIDStoreListNDATE.AsDateTime := EndOfTheDay(ed);
    with dm, quTmp do
    begin
      Close;
      SQL.Text:='select max(sn) from sinv where itype = 15 and FYEAR(Sdate)= FYEAR('''+
                dm3.taUIDStoreListSDATE.Asstring+''')';
      ExecQuery;
      if fields[0].IsNull then  dm3.taUIDStoreListSN.AsInteger := 1
      else dm3.taUIDStoreListSN.AsInteger := fields[0].AsInteger+1;
      Close;
    end;
    dm3.taUIDStoreList.Post;
    Application.ProcessMessages;
    Create_UIDStore;
    ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);
    dm3.update_operation(LogOperationID);
     end;
     end;
  end;
  end;
   end;




procedure TfmUIDStoreList.Create_UIDStore;
begin
  Screen.Cursor := crSQLWait;
  try
    dm3.quTmp.Transaction.CommitRetaining;
    dm3.quTmp.Close;
    dm3.quTmp.SQL.Text := 'execute procedure UIDSTORE_3('+dm3.taUIDStoreListSINVID.AsString+')';
    dm3.quTmp.ExecQuery;
    dm3.quTmp.Transaction.CommitRetaining;
    dm3.quTmp.Close;
    WriteTimeExec(dm3.quTmp.CallTime);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmUIDStoreList.acRefreshExecute(Sender: TObject);
var
  c : integer;
  Bookmark : Pointer;
  CallTime : Cardinal;
  LogOperationID: string;
begin
  if dm.countmaxpross then sysUtils.Abort;
  LogOperationID := dm3.insert_operation(sLog_UIDStoreRefresh, LogOperIdForm);
  try
  ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);
  //SetProgress('���������� ��������� ��������� �'+dm3.taUIDStoreListSN.AsString+'...');
  try
    Create_UIDStore;
  finally
    //UnProgress;
  end;
  CallTime := dm3.quTmp.CallTime;
  c := ExecSelectSQL('select count(*) from SInv where IType=15 and NDate>"'+DateTimeToStr(dm3.taUIDStoreListNDATE.AsDateTime)+'"', dm3.quTmp);

  if c=0 then eXit;
  //????
  if MessageDialog('�������� ����������� ��������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then
  eXit;

  Bookmark := dm3.taUIDStoreList.GetBookmark;
  try
    dm3.taUIDStoreList.Next;
    while not dm3.taUIDStoreList.Eof do
    begin
      //SetProgress('���������� ��������� ��������� �'+dm3.taUIDStoreListSN.AsString+'...');
      try
        Create_UIDStore;
      finally
        //UnProgress;
      end;
      CallTime := CallTime + dm3.quTmp.CallTime;
      dm3.taUIDStoreList.Next;
    end;
  finally
    dm3.taUIDStoreList.GotoBookmark(Bookmark);
  end;
  WriteTimeExec(CallTime);
  finally
   ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dm3.taUIDStoreList] );
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  Action := caFree;
  gridUIDStoreList.FieldColumns['SINVID'].Visible := False;
end;

procedure TfmUIDStoreList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUIDStoreList.acDelExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreDel, LogOperIdForm);
  dm3.taUIDStoreList.Delete;
  dm3.update_operation(LogOperIdForm);
end;

procedure TfmUIDStoreList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dm3.taUIDStoreList.Active;
end;

procedure TfmUIDStoreList.acEditUpdate(Sender: TObject);
begin
  acEdit.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.acExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfmUIDStoreList.acCheckUIDStoreDataExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmUIDStoreCheck, Self, TForm(fmUIDStoreCheck), True, False);
end;

procedure TfmUIDStoreList.acCheckUIDStoreDataUpdate(Sender: TObject);
begin
  acCheckUIDStoreData.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty); 
end;

procedure TfmUIDStoreList.acTotalExecute(Sender: TObject);
begin
//
end;

procedure TfmUIDStoreList.acViewByUIDExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreViewByUID, LogOperIdForm);
  ShowAndFreeForm(TfmUidItem, Self, TForm(fmUidItem), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.acViewByArtExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreViewByArt, LogOperIdForm);
  ShowAndFreeForm(TfmUIDStore, Self, TForm(fmUIDStore), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.WriteTimeExec(CallTime : Cardinal);
begin
  stbrMain.SimpleText := '����� ����������: '+Format('%.2f', [CallTime/1000])+'�';
end;

procedure TfmUIDStoreList.acFillEntryDataExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreFillEntryData, LogOperIdForm);
  ExecSQL('execute procedure UIDSTORE_FILLENTRY_BY_WH('+dm3.taUIDStoreListSINVID.AsString+')', dm3.quTmp);
  WriteTimeExec(dm3.quTmp.CallTime);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.acFillEntryDataUpdate(Sender: TObject);
begin
  acFillEntryData.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.acFillOutDataExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreFillOutData, LogOperIdForm);
  //SetProgress('���������� ������������ ������� �� ������...');
  try
    ExecSQL('execute procedure UIDSTORE_FILLFACT_BY_WH('+dm3.taUIDStoreListSINVID.AsString+')', dm3.quTmp);
    WriteTimeExec(dm3.quTmp.CallTime);
  finally
    //UnProgress;
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.acFillOutDataUpdate(Sender: TObject);
begin
  acFillOutData.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmUIDStoreList.acShowIdExecute(Sender: TObject);
begin
  gridUIDStoreList.FieldColumns['SINVID'].Visible := not gridUIDStoreList.FieldColumns['SINVID'].Visible;
end;

procedure TfmUIDStoreList.acRefreshUpdate(Sender: TObject);
begin
  acRefresh.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.acTotalUpdate(Sender: TObject);
begin
  acTotal.Enabled := dm3.taUIDStoreList.Active and (not dm3.taUIDStoreList.IsEmpty);
end;

procedure TfmUIDStoreList.acPrnExecute(Sender: TObject);
begin
  dmReport.PrintDocumentB(uid_store_Doc);
end;

procedure TfmUIDStoreList.acWithOutComExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreTotal, LogOperIdForm);
  ShowAndFreeForm(TfmUIDStore_T, Self, TForm(fmUIDStore_T), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreList.acWithComExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreTotalC, LogOperIdForm);
  ShowAndFreeForm(TfmUIDStore_T_C, Self, TForm(fmUIDStore_T_C), True, False);
  dm3.update_operation(LogOperationID);
end;


procedure TfmUIDStoreList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100420)
end;

procedure TfmUIDStoreList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
