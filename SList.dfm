object fmSList: TfmSList
  Left = 97
  Top = 205
  HelpContext = 100201
  Caption = #1055#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 648
  ClientWidth = 1020
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1020
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    ExplicitTop = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 698
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      DropDownMenu = pmAdd
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FF00FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00000000000000FFFFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B
        FFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFF7B7B7B000000FF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B000000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object siFind: TSpeedItem
      Action = acFind
      BtnCaption = #1055#1086#1080#1089#1082
      Caption = #1055#1086#1080#1089#1082
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FF000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF00FF000000000000000000000000FFFFFFFFFFFF00
        0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FF000000
        FFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8484
        00000000000000FFFFFFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF848400848400848400FFFFFFFFFFFFFF00FF000000
        FFFFFFFFFFFF000000000000000000000000000000FFFFFF8484008484008484
        00848400FFFFFFFFFFFFFF00FF000000FFFFFF00000000000000000000000000
        0000000000000000848400848400848400FFFFFFFFFFFFFFFFFFFF00FFFF00FF
        000000FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00000000FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF00FF000000FFFF000000FF0000FFFFFF000000FFFF
        FF00FFFF00FFFF00000000000000FFFFFF0000FF0000FFFFFFFFFF00FF000000
        FFFF000000FF0000FF0000FF0000FFFFFF00FFFF000000FF000000000000FFFF
        FF0000FFFFFFFF0000FFFF00FF000000FFFF000000FF0000FFFFFF000000FFFF
        FF000000FF0000FF0000FF0000FFFFFFFF0000FF0000FFFFFFFFFF00FF000000
        FFFF00FFFF000000FF0000FFFFFF00FFFF00FFFF00FFFF00000000000000FFFF
        FF0000FF0000FFFFFFFFFF00FFFF00FF000000FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00000000000000000000000000000000000000FF00FFFF00FF
        000000000000FFFF00FFFF00FFFF00FFFF00FFFF00000000FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000
        0000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 48
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acFindExecute
      SectionName = 'Untitled (0)'
    end
    object siRep: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmprint1
      Hint = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 67
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acRest
      BtnCaption = #1054#1089#1090#1072#1090#1082#1080
      Caption = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1086#1074
      Hint = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 63
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      OnClick = acRestExecute
      SectionName = 'Untitled (0)'
    end
    object spitImport: TSpeedItem
      Action = acImport
      BtnCaption = #1048#1084#1087#1086#1088#1090
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
        0000FF00FF00000000000000000000000000000000000000000000000000FFFF
        FF00FFFFFF00000000000000000000000000FFFFFF0000000000FFFF00000000
        000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
        0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF00000000
        0000FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
        000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF00000000
        000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
        FF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF00000000
        0000FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
        000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000FFFF00000000
        000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
        FF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FFFF00000000
        0000FFFFFF0000FFFF0000000000000000000000000000000000000000000000
        000000000000FFFFFF00FFFFFF0000000000FFFFFF0000000000000000000000
        000000000000FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00
        FF00FF00FF00000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF0000000000000000000000
        0000FFFFFF00FFFFFF0000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF0000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FF000000FF000000FF000000FF00
        0000FF000000FF00000000000000FFFFFF0000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      OnClick = acImportExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 634
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      ParentFont = False
      Spacing = 1
      Left = 514
      Top = 2
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acExport
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090
      Caption = 'SpeedItem4'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF000000000000000000FFFFFF00000000000000000000000000000000000000
        0000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
        FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF0000000000FF00FF000000
        0000FFFFFF0000000000FF00FF00FF00FF00FF00FF008400000000000000FFFF
        FF0000000000FF00FF0000000000FFFFFF0000000000FF00FF0000000000FF00
        FF00000000000000000000000000FF00FF00840000008400000000000000FFFF
        FF00FFFFFF0000000000FF00FF0000000000FF00FF0000000000FF00FF000000
        0000FF00FF00FF00FF00FF00FF00000000008400000084000000000000000000
        0000000000000000000000000000FF00FF0000000000FF00FF0000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF008400000084000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000FF00FF0000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF008400000084000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000008400000084000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000
        0000000000000000000000000000FF00FF008400000084000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 450
      Top = 2
      Visible = True
      OnClick = acExportExecute
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 1020
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDep: TLabel
      Left = 52
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 412
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbDep: TLabel
      Left = 8
      Top = 8
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1055#1077#1088#1080#1086#1076'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 330
      Top = 2
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 69
    Width = 1020
    Height = 579
    Align = alClient
    AllowedOperations = []
    AllowedSelections = [gstRecordBookmarks, gstRectangle, gstAll]
    Color = clBtnFace
    ColumnDefValues.Title.Alignment = taCenter
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSList
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 3
    IndicatorTitle.TitleButton = True
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmmain
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = acViewExecute
    OnGetCellParams = dg1GetCellParams
    OnKeyDown = dg1KeyDown
    OnMouseMove = dg1MouseMove
    OnTitleBtnClick = dg1TitleBtnClick
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NDATE'
        Footers = <
          item
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1047#1072#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Color = clRed
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1054#1090#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1042#1089#1077#1075#1086
            ValueType = fvtStaticText
          end>
        Title.Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SN'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'SnOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'SnClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'SN'
            ValueType = fvtCount
          end>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUP'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Width = 170
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1057#1082#1083#1072#1076
        Width = 151
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SSF'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1057'.'#1060'.'
        Width = 86
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1044#1072#1090#1072
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TOTALCOST'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'TotalCostOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'TotalCostClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'TOTALCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072
        Width = 83
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COST'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'CostOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'CostClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'COST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1091#1084#1084#1072
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NDS'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'NDSOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'NDSClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'NDS'
            ValueType = fvtSum
          end>
        Title.Caption = #1053#1044#1057
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TR'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'TrOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'TrClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'TR'
            ValueType = fvtSum
          end>
        Title.Caption = #1058#1056
        Width = 70
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TRNDS'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'TrNDSOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'TrNDSClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'TRNDS'
            ValueType = fvtSum
          end>
        Title.Caption = #1053#1044#1057' '#1058#1056
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TQ'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'TqOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'TQClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'TQ'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1083'-'#1074#1086
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TW'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'TWOpen'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'TWClose'
            ValueType = fvtSum
          end
          item
            FieldName = 'TW'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1077#1089
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 125
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NAMEPAYTYPE'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1042#1080#1076' '#1086#1087#1083#1072#1090#1099
        Width = 159
      end
      item
        EditButtons = <>
        FieldName = 'CONTRACT$CLASS$NAME'
        Footers = <>
        Title.Caption = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 28
    Top = 96
  end
  object pmmain: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 392
    Top = 224
    object IbAdd: TTBItem
      Action = acAdd
    end
    object IbDel: TTBItem
      Action = acDel
    end
    object IbView: TTBItem
      Action = acView
    end
    object IbFind: TTBItem
      Action = acFind
    end
    object IbPrint: TTBSubmenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      object IbTagmain: TTBItem
        Caption = #1041#1080#1088#1082#1080' ('#1087#1088#1080#1093#1086#1076')'
      end
      object N14: TTBItem
        Action = acPrintOrder
        Caption = #1055#1088#1080#1082#1072#1079
      end
      object mnitPrList1: TTBItem
        Tag = 3
        Caption = #1056#1077#1077#1089#1090#1088' '#1094#1077#1085
        OnClick = mnitPrList1Click
      end
      object N15: TTBItem
        Action = acPrintF
        Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072
      end
      object N16: TTBSeparatorItem
      end
      object mnitTagact1: TTBItem
        Caption = #1041#1080#1088#1082#1080' ('#1072#1082#1090')'
      end
      object mnitactOV1: TTBItem
        Action = acPrintActOV
        Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      end
      object N17: TTBSeparatorItem
      end
      object mnitFacture1: TTBItem
        Tag = 10
        Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072
        OnClick = mnitFacture1Click
      end
      object mnitInvoice1: TTBItem
        Tag = 11
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
        OnClick = mnitInvoice1Click
      end
      object mnitInvoiceItem1: TTBItem
        Tag = 12
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' ('#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086')'
        OnClick = mnitInvoiceItem1Click
      end
    end
    object N4: TTBSeparatorItem
    end
    object N5: TTBItem
      Caption = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ShortCut = 117
    end
    object N7: TTBSeparatorItem
    end
    object tbsCreateDInv: TTBSubmenuItem
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1042#1055' '#1085#1072
    end
    object TBItem1: TTBItem
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1074#1089#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
    end
    object SInvId1: TTBItem
      Action = acSinvID
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 96
    Top = 128
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
    end
    object acFind: TAction
      Caption = #1055#1086#1080#1089#1082
      ImageIndex = 48
      ShortCut = 118
      OnExecute = acFindExecute
    end
    object acPrintOrder: TAction
      Caption = 'acPrintOrder'
      OnExecute = acPrintOrderExecute
      OnUpdate = acPrintOrderUpdate
    end
    object acPrintClick: TAction
      Caption = 'acPrintClick'
      OnExecute = acPrintClickExecute
    end
    object acPrintActOV: TAction
      Tag = 3
      Caption = 'acPrintActOV'
      OnExecute = acPrintActOVExecute
    end
    object acPrintF: TAction
      Caption = 'acPrintF'
      OnExecute = acPrintFExecute
    end
    object acSinvID: TAction
      Caption = 'acSinvID'
      OnExecute = acSinvIDExecute
    end
    object acRest: TAction
      Caption = #1054#1089#1090#1072#1090#1082#1080
      ShortCut = 115
      OnExecute = acRestExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acSurPlus: TAction
      Caption = #1048#1079#1083#1080#1096#1082#1080
      OnExecute = acSurPlusExecute
    end
    object acImport: TAction
      Caption = #1048#1084#1087#1086#1088#1090
      ImageIndex = 11
      OnExecute = acImportExecute
    end
    object acExport: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090
      ImageIndex = 62
      OnExecute = acExportExecute
    end
  end
  object pmAdd: TTBPopupMenu
    Left = 40
    Top = 24
    object IbSamosvety: TTBItem
      Action = acAdd
      Caption = #1057#1072#1084#1086#1094#1074#1077#1090#1099
    end
    object IBSamocvetyTolling: TTBItem
      Caption = #1057#1072#1084#1086#1094#1074#1077#1090#1099' ('#1076#1072#1074#1072#1083#1100#1095#1077#1089#1082#1072#1103')'
      ImageIndex = 1
      ShortCut = 16429
      OnClick = IBSamocvetyTollingClick
    end
    object biSurplus: TTBItem
      Action = acSurPlus
    end
  end
  object pmprint1: TTBPopupMenu
    Left = 296
    Top = 40
    object tbTagInv: TTBSubmenuItem
      Caption = #1041#1080#1088#1082#1080' ('#1087#1088#1080#1093#1086#1076')'
    end
    object mnitOrder: TTBItem
      Action = acPrintOrder
      Caption = #1055#1088#1080#1082#1072#1079
    end
    object mnitPrList: TTBItem
      Tag = 3
      Caption = #1056#1077#1077#1089#1090#1088' '#1094#1077#1085
      OnClick = mnitPrList1Click
    end
    object mnitF: TTBItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1075#1086#1088'.'
      OnClick = acPrintFExecute
    end
    object N8: TTBItem
      Tag = 1
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1074#1077#1088'.'
      OnClick = N8Click
    end
    object N10: TTBItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
      OnClick = N10Click
    end
    object N11: TTBSeparatorItem
    end
    object tbTagAct: TTBSubmenuItem
      Caption = #1041#1080#1088#1082#1080' ('#1072#1082#1090')'
    end
    object mnitactOV: TTBItem
      Tag = 3
      Action = acPrintActOV
      Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
    end
    object N13: TTBSeparatorItem
    end
    object mnitInvoice: TTBItem
      Tag = 11
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      OnClick = mnitInvoice1Click
    end
    object mnitInvoiceItem: TTBItem
      Tag = 12
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' ('#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086')'
      OnClick = mnitInvoiceItem1Click
    end
    object mnitFacture: TTBItem
      Tag = 10
      Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072
      OnClick = mnitFacture1Click
    end
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -13
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 280
    Top = 152
  end
  object taComp_SupImport: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select D_COMPID, NAME, SNAME, SUPPORT_IMPORT'
      'from D_COMP '
      'where SUPPORT_IMPORT is not null'
      'order by SUPPORT_IMPORT asc')
    Left = 48
    Top = 156
    qoAutoCommit = True
    qoStartTransaction = True
    qoTrimCharFields = True
  end
  object DataSetExport: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from "temp$procedure$sinv$import"(:sinvid)  ;')
    BeforeOpen = DataSetExportBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 800
    Top = 184
    object DataSetExportART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object DataSetExportD_GOOD: TFIBStringField
      FieldName = 'D_GOOD'
      EmptyStrToNull = True
    end
    object DataSetExportMAT: TFIBStringField
      FieldName = 'MAT'
      Size = 22
      EmptyStrToNull = True
    end
    object DataSetExportINS: TFIBStringField
      FieldName = 'INS'
      EmptyStrToNull = True
    end
    object DataSetExportPROVIDERS: TFIBStringField
      FieldName = 'PROVIDERS'
      Size = 60
      EmptyStrToNull = True
    end
    object DataSetExportPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object DataSetExportBARCODE: TFIBIntegerField
      FieldName = 'BARCODE'
    end
    object DataSetExportSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object DataSetExportW: TFIBFloatField
      FieldName = 'W'
    end
    object DataSetExportH1: TFIBStringField
      FieldName = 'H1'
      Size = 41
      EmptyStrToNull = True
    end
  end
  object ExportDialog: TQExport3Dialog
    DataSet = DataSetExport
    RTFOptions.CaptionStyle.Font.Charset = DEFAULT_CHARSET
    RTFOptions.CaptionStyle.Font.Color = clBlack
    RTFOptions.CaptionStyle.Font.Height = -13
    RTFOptions.CaptionStyle.Font.Name = 'Arial'
    RTFOptions.CaptionStyle.Font.Style = [fsBold]
    RTFOptions.CaptionStyle.Alignment = talCenter
    RTFOptions.DataStyle.Font.Charset = DEFAULT_CHARSET
    RTFOptions.DataStyle.Font.Color = clBlack
    RTFOptions.DataStyle.Font.Height = -13
    RTFOptions.DataStyle.Font.Name = 'Arial'
    RTFOptions.DataStyle.Font.Style = []
    RTFOptions.FooterStyle.Font.Charset = DEFAULT_CHARSET
    RTFOptions.FooterStyle.Font.Color = clBlack
    RTFOptions.FooterStyle.Font.Height = -13
    RTFOptions.FooterStyle.Font.Name = 'Arial'
    RTFOptions.FooterStyle.Font.Style = []
    RTFOptions.HeaderStyle.Font.Charset = DEFAULT_CHARSET
    RTFOptions.HeaderStyle.Font.Color = clBlack
    RTFOptions.HeaderStyle.Font.Height = -13
    RTFOptions.HeaderStyle.Font.Name = 'Arial'
    RTFOptions.HeaderStyle.Font.Style = []
    RTFOptions.StripStyles = <>
    HTMLPageOptions.TextFont.Charset = DEFAULT_CHARSET
    HTMLPageOptions.TextFont.Color = clWhite
    HTMLPageOptions.TextFont.Height = -11
    HTMLPageOptions.TextFont.Name = 'Arial'
    HTMLPageOptions.TextFont.Style = []
    CSVOptions.Comma = ';'
    PDFOptions.PageOptions.MarginLeft = 1.170000000000000000
    PDFOptions.PageOptions.MarginRight = 0.570000000000000000
    PDFOptions.PageOptions.MarginTop = 0.780000000000000000
    PDFOptions.PageOptions.MarginBottom = 0.780000000000000000
    PDFOptions.HeaderFont.UserFont.Charset = DEFAULT_CHARSET
    PDFOptions.HeaderFont.UserFont.Color = clWindowText
    PDFOptions.HeaderFont.UserFont.Height = -13
    PDFOptions.HeaderFont.UserFont.Name = 'Arial'
    PDFOptions.HeaderFont.UserFont.Style = []
    PDFOptions.CaptionFont.UserFont.Charset = DEFAULT_CHARSET
    PDFOptions.CaptionFont.UserFont.Color = clWindowText
    PDFOptions.CaptionFont.UserFont.Height = -13
    PDFOptions.CaptionFont.UserFont.Name = 'Arial'
    PDFOptions.CaptionFont.UserFont.Style = []
    PDFOptions.DataFont.UserFont.Charset = DEFAULT_CHARSET
    PDFOptions.DataFont.UserFont.Color = clWindowText
    PDFOptions.DataFont.UserFont.Height = -13
    PDFOptions.DataFont.UserFont.Name = 'Arial'
    PDFOptions.DataFont.UserFont.Style = []
    PDFOptions.FooterFont.UserFont.Charset = DEFAULT_CHARSET
    PDFOptions.FooterFont.UserFont.Color = clWindowText
    PDFOptions.FooterFont.UserFont.Height = -13
    PDFOptions.FooterFont.UserFont.Name = 'Arial'
    PDFOptions.FooterFont.UserFont.Style = []
    XLSOptions.PageFooter = 'Page &P of &N'
    XLSOptions.SheetTitle = 'Sheet 1'
    XLSOptions.CaptionFormat.Font.Style = [xfsBold]
    XLSOptions.HyperlinkFormat.Font.Color = clrBlue
    XLSOptions.HyperlinkFormat.Font.Underline = fulSingle
    XLSOptions.NoteFormat.Alignment.Horizontal = halLeft
    XLSOptions.NoteFormat.Alignment.Vertical = valTop
    XLSOptions.NoteFormat.Font.Size = 8
    XLSOptions.NoteFormat.Font.Style = [xfsBold]
    XLSOptions.NoteFormat.Font.Name = 'Tahoma'
    XLSOptions.FieldFormats = <>
    XLSOptions.StripStyles = <>
    XLSOptions.Hyperlinks = <>
    XLSOptions.Notes = <>
    XLSOptions.Charts = <>
    XLSOptions.Pictures = <>
    XLSOptions.Images = <>
    XLSOptions.Cells = <>
    XLSOptions.MergedCells = <>
    Left = 808
    Top = 256
  end
end
