object fmPayment: TfmPayment
  Left = 261
  Top = 214
  Caption = #1054#1087#1083#1072#1090#1099
  ClientHeight = 692
  ClientWidth = 1421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object stbrStatus: TStatusBar
    Left = 0
    Top = 673
    Width = 1421
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 1421
    Height = 673
    ActivePage = cxTabSheet2
    Align = alClient
    TabOrder = 1
    TabWidth = 100
    OnChange = cxPageControl1Change
    ClientRectBottom = 673
    ClientRectRight = 1421
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1054#1087#1083#1072#1090#1099
      ImageIndex = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TBDock1: TTBDock
        Left = 0
        Top = 0
        Width = 1421
        Height = 26
        object TBToolbar1: TTBToolbar
          Left = 0
          Top = 0
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 0
          Images = ilButtons
          TabOrder = 0
          object TBItem3: TTBItem
            Action = acAdd
            DisplayMode = nbdmImageAndText
          end
          object TBItem2: TTBItem
            Action = acDel
            DisplayMode = nbdmImageAndText
          end
          object TBItem1: TTBItem
            Action = acPeriod
            DisplayMode = nbdmImageAndText
          end
          object TBItem4: TTBItem
            Action = acBalanceB
          end
          object TBSeparatorItem1: TTBSeparatorItem
          end
          object TBControlItem1: TTBControlItem
            Control = Label1
          end
          object TBControlItem2: TTBControlItem
            Control = cbComp
          end
          object Label1: TLabel
            Left = 332
            Top = 4
            Width = 61
            Height = 13
            Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090' '
          end
          object cbComp: TDBComboBoxEh
            Left = 393
            Top = 1
            Width = 121
            Height = 19
            DropDownBox.Rows = 15
            DropDownBox.Width = -1
            EditButtons = <>
            Flat = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Text = 'cbComp'
            Visible = True
            OnChange = cbCompChange
          end
        end
      end
      object GridPayment: TcxGrid
        Left = 0
        Top = 26
        Width = 1421
        Height = 623
        Align = alClient
        TabOrder = 1
        object GridPaymentView: TcxGridDBTableView
          OnMouseWheel = GridPaymentViewMouseWheel
          NavigatorButtons.ConfirmDelete = False
          OnInitEdit = GridPaymentViewInitEdit
          DataController.DataSource = dsrPayment
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'C'
              Column = GridPaymentViewC
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'C'
              Column = GridPaymentViewC
            end>
          DataController.Summary.SummaryGroups = <>
          DataController.OnGroupingChanged = GridPaymentViewDataControllerGroupingChanged
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          OptionsView.NoDataToDisplayInfoText = '...'
          OptionsView.Footer = True
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.Indicator = True
          object GridPaymentViewCOMPANYID: TcxGridDBColumn
            Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
            DataBinding.FieldName = 'COMPANY$ID'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Fixed = True
                HeaderAlignment = taCenter
                SortOrder = soAscending
                FieldName = 'SNAME'
              end
              item
                HeaderAlignment = taCenter
                Sorting = False
                FieldName = 'NAME'
              end>
            Properties.ListOptions.CaseInsensitive = True
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = dsCompany
            HeaderAlignmentHorz = taCenter
            Width = 367
          end
          object GridPaymentViewCONTRACTCLASS: TcxGridDBColumn
            Caption = #1044#1086#1075#1086#1074#1086#1088
            DataBinding.FieldName = 'CONTRACT$CLASS$ID'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAME'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = dsContractClass
            HeaderAlignmentHorz = taCenter
            Width = 122
          end
          object GridPaymentViewC: TcxGridDBColumn
            DataBinding.FieldName = 'C'
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 113
          end
          object GridPaymentViewPD: TcxGridDBColumn
            DataBinding.FieldName = 'PD'
            HeaderAlignmentHorz = taCenter
            SortIndex = 0
            SortOrder = soAscending
          end
          object GridPaymentViewBANK: TcxGridDBColumn
            DataBinding.FieldName = 'BANK'
            HeaderAlignmentHorz = taCenter
          end
          object GridPaymentViewBILL: TcxGridDBColumn
            DataBinding.FieldName = 'BILL'
            HeaderAlignmentHorz = taCenter
            Width = 219
          end
        end
        object GridPaymentLevel: TcxGridLevel
          GridView = GridPaymentView
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = #1041#1072#1083#1072#1085#1089
      ImageIndex = 1
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object TBDock2: TTBDock
        Left = 0
        Top = 0
        Width = 1421
        Height = 26
        object TBToolbar2: TTBToolbar
          Left = 0
          Top = 0
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 0
          Images = ilButtons
          TabOrder = 0
          object TBItem6: TTBItem
            Action = acBalancePrint
            DisplayMode = nbdmImageAndText
            ImageIndex = 4
          end
          object TBItem5: TTBItem
            Action = acPeriod
            DisplayMode = nbdmImageAndText
          end
          object TBSeparatorItem2: TTBSeparatorItem
          end
          object TBControlItem3: TTBControlItem
            Control = Label4
          end
          object TBControlItem4: TTBControlItem
            Control = cbCompBalance
          end
          object TBSeparatorItem3: TTBSeparatorItem
          end
          object TBControlItem9: TTBControlItem
            Control = Label5
          end
          object TBControlItem10: TTBControlItem
            Control = cbPaytypeBalance
          end
          object Label4: TLabel
            Left = 139
            Top = 4
            Width = 61
            Height = 13
            Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090' '
          end
          object Label5: TLabel
            Left = 327
            Top = 4
            Width = 44
            Height = 13
            Caption = #1044#1086#1075#1086#1074#1086#1088
          end
          object cbCompBalance: TDBComboBoxEh
            Left = 200
            Top = 1
            Width = 121
            Height = 19
            DropDownBox.Rows = 15
            DropDownBox.Width = -1
            EditButtons = <>
            Flat = True
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Text = 'cbComp'
            Visible = True
            OnChange = cbCompBalanceChange
          end
          object cbPaytypeBalance: TDBComboBoxEh
            Tag = 1
            Left = 371
            Top = 1
            Width = 104
            Height = 19
            AutoSize = False
            DropDownBox.Rows = 15
            DropDownBox.Width = -1
            EditButtons = <>
            Flat = True
            Items.Strings = (
              '*'#1042#1089#1077' '
              #1055#1086#1089#1090#1072#1074#1082#1072
              #1050#1086#1084#1080#1089#1089#1080#1103
              #1055#1077#1088#1077#1088#1072#1073#1086#1090#1082#1072)
            KeyItems.Strings = (
              '0'
              '1'
              '2'
              '4')
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            Visible = True
            OnChange = cbPaytypeBalanceChange
          end
        end
      end
      object GridBalance: TcxGrid
        Left = 0
        Top = 26
        Width = 1421
        Height = 623
        Align = alClient
        TabOrder = 1
        object GridBalanceView: TcxGridDBBandedTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = dsrBalance
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'BALANCE_B'
              Column = GridBalanceViewBALANCE_B
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'BALANCE_E'
              Column = GridBalanceViewBALANCE_E
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'ICOST'
              Column = GridBalanceViewICOST
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'RCOST'
              Column = GridBalanceViewRCOST
            end
            item
              Format = '0.00'
              Kind = skSum
              FieldName = 'CASHCOST'
              Column = GridBalanceViewCASHCOST
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skSum
              Column = GridBalanceViewBALANCE_B
            end
            item
              Kind = skSum
              Column = GridBalanceViewBALANCE_E
            end
            item
              Kind = skSum
              Column = GridBalanceViewICOST
            end
            item
              Kind = skSum
              Column = GridBalanceViewRCOST
            end
            item
              Kind = skSum
              Column = GridBalanceViewCASHCOST
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsView.NoDataToDisplayInfoText = '...'
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupSummaryLayout = gslAlignWithColumns
          OptionsView.Indicator = True
          OnColumnHeaderClick = GridBalanceViewColumnHeaderClick
          Bands = <
            item
              Caption = #1041#1072#1083#1072#1085#1089
              Width = 433
            end
            item
              Caption = #1057#1091#1084#1084#1099
              Width = 405
            end>
          object GridBalanceViewCOMPSNAME: TcxGridDBBandedColumn
            DataBinding.FieldName = 'COMPSNAME'
            Visible = False
            HeaderAlignmentHorz = taCenter
            Options.ShowCaption = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 202
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
            IsCaptionAssigned = True
          end
          object GridBalanceViewCOMPNAME: TcxGridDBBandedColumn
            DataBinding.FieldName = 'COMPNAME'
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 276
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object GridBalanceViewBALANCE_B: TcxGridDBBandedColumn
            Tag = 1
            Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103
            DataBinding.FieldName = 'BALANCE_B'
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 87
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object GridBalanceViewBALANCE_E: TcxGridDBBandedColumn
            Tag = 1
            Caption = #1050#1086#1085#1077#1095#1085#1072#1103
            DataBinding.FieldName = 'BALANCE_E'
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 82
            Position.BandIndex = 1
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object GridBalanceViewICOST: TcxGridDBBandedColumn
            Tag = 1
            DataBinding.FieldName = 'ICOST'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = fmBalanceInv.pnlBalancInv
            Properties.OnCloseUp = cxGrid1DBBandedTableView1ICOSTPropertiesCloseUp
            Properties.OnPopup = cxGrid1DBBandedTableView1ICOSTPropertiesPopup
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 81
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object GridBalanceViewRCOST: TcxGridDBBandedColumn
            Tag = 1
            DataBinding.FieldName = 'RCOST'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = fmBalanceInv.pnlBalancInv
            Properties.OnCloseUp = cxGrid1DBBandedTableView1RCOSTPropertiesCloseUp
            Properties.OnPopup = cxGrid1DBBandedTableView1RCOSTPropertiesPopup
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 75
            Position.BandIndex = 1
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object GridBalanceViewCASHCOST: TcxGridDBBandedColumn
            Tag = 1
            DataBinding.FieldName = 'CASHCOST'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = fmBalanceInv.pnlBalancInv
            Properties.OnCloseUp = cxGrid1DBBandedTableView1CASHCOSTPropertiesCloseUp
            Properties.OnPopup = cxGrid1DBBandedTableView1CASHCOSTPropertiesPopup
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 73
            Position.BandIndex = 1
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object GridBalanceViewCONTRACTCLASSNAME: TcxGridDBBandedColumn
            Caption = #1044#1086#1075#1086#1074#1086#1088
            DataBinding.FieldName = 'CONTRACT$CLASS$NAME'
            HeaderAlignmentHorz = taCenter
            Options.Grouping = False
            Width = 140
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object GridBalanceViewTag: TcxGridDBBandedColumn
            DataBinding.FieldName = 'Tag'
            Visible = False
            GroupIndex = 0
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
            IsCaptionAssigned = True
          end
        end
        object GridBalanceLevel: TcxGridLevel
          Caption = 'Table'
          GridView = GridBalanceView
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = #1043#1088#1072#1092#1080#1082' '#1086#1087#1083#1072#1090
      ImageIndex = 2
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1421
        Height = 649
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel2'
        TabOrder = 0
        object cxPageControl2: TcxPageControl
          Left = 0
          Top = 0
          Width = 1421
          Height = 649
          ActivePage = tshFuturePay
          Align = alClient
          TabOrder = 0
          ClientRectBottom = 649
          ClientRectRight = 1421
          ClientRectTop = 24
          object tshFuturePay: TcxTabSheet
            Caption = #1055#1088#1077#1076#1089#1090#1086#1103#1097#1080#1077' '#1087#1083#1072#1090#1077#1078#1080
            ImageIndex = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object gridPaymentFuture: TcxGrid
              Left = 0
              Top = 23
              Width = 1421
              Height = 602
              Align = alClient
              TabOrder = 0
              object gridPaymentFutureDBTableView: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.DataSource = dsrPayGraphF
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ImmediateEditor = False
                object gridPaymentFutureDBTableViewColumn1: TcxGridDBColumn
                  Caption = #1044#1072#1090#1072' '#1087#1083#1072#1090#1077#1078#1072
                  DataBinding.FieldName = 'D'
                end
                object gridPaymentFutureDBTableViewColumn2: TcxGridDBColumn
                  Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
                  DataBinding.FieldName = 'COMPSNAME'
                end
                object gridPaymentFutureDBTableViewColumn4: TcxGridDBColumn
                  Caption = #1057#1091#1084#1072' '#1087#1083#1072#1090#1077#1078#1072
                  DataBinding.FieldName = 'IC'
                  Width = 85
                end
                object gridPaymentFutureDBTableViewColumn3: TcxGridDBColumn
                  Caption = #1058#1080#1087' '#1086#1087#1083#1072#1090#1099
                  DataBinding.FieldName = 'PAYTYPENAME'
                end
                object gridPaymentFutureDBTableViewColumn5: TcxGridDBColumn
                  Caption = #1053#1086#1084#1077#1088' '#1087#1086#1089#1090#1072#1074#1082#1080
                  DataBinding.FieldName = 'INVNO'
                  Width = 123
                end
                object gridPaymentFutureDBTableViewColumn6: TcxGridDBColumn
                  Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
                  DataBinding.FieldName = 'INVDATE'
                  Width = 141
                end
              end
              object gridPaymentFutureLevel1: TcxGridLevel
                GridView = gridPaymentFutureDBTableView
              end
            end
            object TBDock4: TTBDock
              Left = 0
              Top = 0
              Width = 1421
              Height = 23
              object TBToolbar4: TTBToolbar
                Left = 0
                Top = 0
                BorderStyle = bsNone
                DockMode = dmCannotFloat
                DockPos = 0
                Images = ilButtons
                TabOrder = 0
                object TBControlItem7: TTBControlItem
                  Control = Label3
                end
                object TBControlItem8: TTBControlItem
                  Control = cbCompFuturePay
                end
                object Label3: TLabel
                  Left = 0
                  Top = 3
                  Width = 61
                  Height = 13
                  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090' '
                end
                object cbCompFuturePay: TDBComboBoxEh
                  Left = 61
                  Top = 0
                  Width = 121
                  Height = 19
                  DropDownBox.Rows = 15
                  DropDownBox.Width = -1
                  EditButtons = <>
                  Flat = True
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Visible = True
                  OnChange = cbCompFuturePayChange
                end
              end
            end
          end
          object tshPastPay: TcxTabSheet
            Caption = #1053#1077#1086#1087#1083#1072#1095#1077#1085#1085#1099#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
            ImageIndex = 1
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object TBDock3: TTBDock
              Left = 0
              Top = 0
              Width = 1421
              Height = 23
              object TBToolbar3: TTBToolbar
                Left = 0
                Top = 0
                BorderStyle = bsNone
                DockMode = dmCannotFloat
                DockPos = 0
                Images = ilButtons
                TabOrder = 0
                object TBControlItem5: TTBControlItem
                  Control = Label2
                end
                object TBControlItem6: TTBControlItem
                  Control = cbCompPastPay
                end
                object Label2: TLabel
                  Left = 0
                  Top = 3
                  Width = 61
                  Height = 13
                  Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090' '
                end
                object cbCompPastPay: TDBComboBoxEh
                  Left = 61
                  Top = 0
                  Width = 121
                  Height = 19
                  DropDownBox.Rows = 15
                  DropDownBox.Width = -1
                  EditButtons = <>
                  Flat = True
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  Visible = True
                  OnChange = cbCompPastPayChange
                end
              end
            end
            object gridPastPay: TcxGrid
              Left = 0
              Top = 23
              Width = 1421
              Height = 602
              Align = alClient
              TabOrder = 1
              LookAndFeel.Kind = lfUltraFlat
              object cxGridDBTableView1: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.DataSource = dsrPayGraphPast
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsBehavior.ImmediateEditor = False
                object cxGridDBTableView1Column1: TcxGridDBColumn
                  Caption = #1044#1072#1090#1072' '#1087#1083#1072#1090#1077#1078#1072
                  DataBinding.FieldName = 'D'
                end
                object cxGridDBTableView1Column2: TcxGridDBColumn
                  Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
                  DataBinding.FieldName = 'COMPSNAME'
                end
                object cxGridDBTableView1Column3: TcxGridDBColumn
                  Caption = #1057#1091#1084#1084#1072' '#1087#1083#1072#1090#1077#1078#1072
                  DataBinding.FieldName = 'P'
                end
                object cxGridDBTableView1Column4: TcxGridDBColumn
                  Caption = #1058#1080#1087' '#1086#1087#1083#1072#1090#1099
                  DataBinding.FieldName = 'PAYTYPENAME'
                end
                object cxGridDBTableView1Column5: TcxGridDBColumn
                  Caption = #1053#1086#1084#1077#1088' '#1087#1086#1089#1090#1072#1074#1082#1080
                  DataBinding.FieldName = 'INVNO'
                end
                object cxGridDBTableView1Column6: TcxGridDBColumn
                  Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
                  DataBinding.FieldName = 'INVDATE'
                end
              end
              object cxGridLevel1: TcxGridLevel
                GridView = cxGridDBTableView1
              end
            end
          end
        end
      end
    end
  end
  object ilButtons: TImageList
    Left = 204
    Top = 292
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A6A8A900FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008B8C8A00AFAFAF00AFAFAF00AFAFAF00AFAFAF00FFFFFF005C5C
      5300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000434242006363
      63007475750087898700AFAFAF00AFAFAF00AFAFAF00AFAFAF00FFFFFF006060
      5900494949006363630043424200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004342420090959500979D
      9D007A7D7D0087888600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF006060
      5800494949007274740063636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2D2D2009CA3A3009AA0
      A000646464004A494800616060005F5E5E005F5E5E00605F5F005D5D5D004747
      440054535300818585007B7E7E00434242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2D2D2009FA5A5009CA3
      A3009BA1A100999F9F00969C9C009499990092989800909595008D9393008A90
      9000888D8D0000B700007B7E7E00454545000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2D2D200A2A9A900895A
      63009E6A7200A77E7C00C48F9800C18E9500C18E9500C6969D00BC8D9100A983
      8400A76A7A00888D8D007B7E7E00454545000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2D2D200A3AAAA00874D
      5300A4838300A47A7A00A57B7B00A57B7B00A57B7B00A8807F00A9828100A984
      8200A58383008A9090007B7E7E00454545000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D2D2D200BEBEBE004A1F
      29005846490048333300635858005E5252005E5252005D5152005F565600421A
      1A00443235008D9393007B7E7E00525252000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000968A8A00BEBEBE005F4C
      4A00625B5B00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009B9B
      9B007A707400BEBEBE0060666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000847D7D009383
      830095868600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EBEBEB009B9B
      9B00968887009180800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00EBEBEB00E2E2E2009B9B
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FBFBFB00FFFFFF00FFFFFF00FFFFFF00FFFFFF009B9B
      9B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF009B9B9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009B9B9B009B9B9B009B9B9B009B9B9B009B9B9B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000262FA3001D20
      B500000000004B4B4B005B5B5B0087817B0087817B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009A6666009A666600B9666600BB6868006933340000000000000000000000
      0000000000000000000000000000000000000000000082776F005F61BC002126
      B20066605900A3A3A400FAFAFA00E3E0DC00D1CEC9009A97950082807E007874
      71005D5A58000000000000000000000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A6666009A66
      6600C66A6B00D06A6B00D2686900C3686900693334009A6666009A6666009A66
      66009A6666009A6666009A6666000000000000000000A89381007B75C4002024
      B200BA977500A3A3A400FFFFFF00FFFFFF00FFFFFD00D6D0CA00C8C0B800F6EB
      DD00EDE0D100A59B91006B686400000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000DA31B0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600DE73
      7400D7707100D56F7000D56D6E00C76A6D0069333400FEA2A300FCAFB000FABC
      BD00F9C5C600F9C5C6009A6666000000000000000000A89381007873C3002024
      B200B99B7E00A3A3A400FEFFFF00FFFFFF009F9FA00073717100353537007E7B
      7600FFF5E700FFF2E10087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EAA1D0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600E077
      7800DB757600DA747500DA727300CC6E71006933340039C5650025CF630029CC
      630019CB5B00F9C5C6009A6666000000000000000000A89483007873C5002024
      B300BAA18A00A3A3A400FEFFFF00FFFFFF00CACACC00F5F3F200FFFDF9003535
      3700F3EAE000FDF0E20087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EA81C0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600E57D
      7E00E07A7B00DF797A00DF777800D07275006933340042C4680030CD670033CB
      670024CB6000F9C5C6009A6666000000000000000000A8978B007876CA002024
      B200BBA89600A3A3A400FEFFFF00FFFFFF00B3B3B40082828300A2A1A2003535
      3700F8F2EB00FDF2E90087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70010AA1F0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600EA82
      8300E57F8000E37D7E00E6808100D3747600693334003DC2640029CB63002FCA
      640020CA5E00F9C5C6009A6666000000000000000000918984007779CF002024
      B200BCADA000A3A3A400FEFFFF00FFFFFF009A9A9B00353537007C7C7E00CCCC
      CB00FFFEFB00FCF6EF008786850000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A70019B02C0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A20000000000000000000000000000000000000000009A666600F087
      8800E9818200EC969700FBDDDE00D8888A0069333400B8E1AC006BDC89005DD5
      800046D47300F9C5C6009A66660000000000000000008E888800787BD4002024
      B100BDB2A900A3A3A400FEFEFF00FFFFFF00BCBCBD0035353700353537003535
      3700FFFEFD00FCF9F50087868500000000000000000008780E0076F9A70055E3
      830049DA720042D3680037C856002AB9430022B337001CB2300016AF27000FA8
      1D000EA91B000DA21B0008780E000000000000000000000000000104A2005983
      FF000026FF000030FF000030FB00002FF200002FE900002EE1000030D8000031
      D0000034CB000104A200000000000000000000000000000000009A666600F58C
      8D00EE868700F0999A00FDDCDD00DA888A0069333400FFF5D800FFFFE000FFFF
      DE00ECFDD400F9C5C6009A66660000000000000000008E8C8E00787EDA002024
      B100B4B3B900A3A3A400FEFEFE00E9E9E900E2E2E200BCBCBC00A2A2A300DFDF
      E000FFFFFF00FCFBFB0087868500000000000000000008780E0076F9A70076F9
      A70076F9A70076F9A70076F9A70076F9A7002CBB480076F9A70076F9A70076F9
      A70076F9A70076F9A70008780E000000000000000000000000000104A200ABC2
      FF006480FF006688FF006688FF006687FA006787F5006787F0005779E9004D70
      E4004D74E2000104A200000000000000000000000000000000009A666600FA91
      9200F48E8F00F28B8C00F48C8D00DC7F800069333400FDF3D400FFFFDF00FFFF
      DD00FFFFE000F9C5C6009A66660000000000000000008E8F94007880DE002023
      B100B4B3B900A3A3A400FFFFFF00A5A5A500BBBBBB009F9FA000C6C6C700C5C5
      C600E2E2E300FEFEFE008786850000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A7003CCB5D0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A20000000000000000000000000000000000000000009A666600FE97
      9800F9939400F8929300F9909200E085850069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A66660000000000000000008E8F95007880DF002023
      B100B4B3B900A3A3A400FFFFFF00B9B9B900B3B3B400ACACAD00A8A8A900B9B9
      BA00E0E0E100FFFFFF0087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70049D9720008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600FF9B
      9C00FD979800FC969700FE979800E388890069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A6666000000000000000000909197007B83E2002023
      B000B6B5B600A3A3A400FFFFFF0079797900E1E2E200FBFBFC00EDEDED00B9B9
      B900DBDBDC00FFFFFF0087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70055E2820008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600FF9F
      A000FF9A9B00FF999A00FF9A9B00E78C8D0069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000083848A00646AD2001D23
      BA006F7191009E9DAF00BBB9BC007F7D73008D8C8800EDEAE000E7E7E3006465
      620090909000FFFFFF0087868500000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70063F0970008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A6666009A66
      6600E98E8F00FE999A00FF9D9E00EB8F900069333400FBF0D200FDFCDC00FDFC
      DA00FDFCDC00F9C5C6009A666600000000000000000000000000414163002E31
      7D00303483003F4393003A3D94004042960044446000565599006D689300736E
      88005E5B6100918D8A0061616100000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70076F9A70008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009A666600B0717200D7868700DA888800693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000004B4B4B00000000003B3D62004B4B4B0037376500000000003333
      8300383865001414770000000000000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009A6666009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004B4B4B004B4B4B0000000000000000004B4B4E004C4C
      4C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000F81F000000000000
      F80F000000000000C00100000000000080010000000000008000000000000000
      8000000000000000800000000000000080000000000000008000000000000000
      8001000000000000C003000000000000F80F000000000000F80F000000000000
      F81F000000000000F83F000000000000FE7FC87FFFFFFFFFF07F8007FE7FFFFF
      C0018001FC3FFFFFC0018001FC3FFFFFC0018001FC3FFFFFC0018001FC3FFFFF
      C0018001C003E007C00180018001C003C00180018001C003C0018001C003E007
      C0018001FC3FFFFFC0018001FC3FFFFFC0018001FC3FFFFFC001C001FC3FFFFF
      F001FA23FE7FFFFFFC7FFCCFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object ActionList2: TActionList
    Images = ilButtons
    Left = 196
    Top = 204
    object acPeriod: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 1
      OnExecute = acPeriodExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 2
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 3
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acBalanceB: TAction
      Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1073#1072#1083#1072#1085#1089
      OnExecute = acBalanceBExecute
      OnUpdate = acBalanceBUpdate
    end
    object acClose: TAction
      Caption = 'acClose'
      OnExecute = acCloseExecute
    end
    object acBalancePrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acBalancePrintExecute
    end
  end
  object taPayment: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Payment set'
      '  C=:C,'
      '  PD=:PD,'
      '  COMPID=:COMPANY$ID,'
      '  contract$class$id = :contract$class$id'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from Payment where ID=:ID')
    InsertSQL.Strings = (
      'insert into Payment(ID, COMPID, C, PD, T, contract$class$id)'
      'values(:ID, :COMPANY$ID, :C, :PD, 1, :contract$class$id)')
    RefreshSQL.Strings = (
      
        'select p.ID, p.COMPID company$id, p.C, p.PD , c.BANK, c.BILL, p.' +
        'contract$class$id'
      'from Payment p, D_Comp c'
      'where p.COMPID=c.D_COMPID and'
      '      p.ID=:ID')
    SelectSQL.Strings = (
      
        'select p.ID, p.COMPID company$id, p.C, p.PD , c.BANK, c.BILL, p.' +
        'contract$class$id'
      'from Payment p, D_Comp c '
      'where p.COMPID=c.D_COMPID and'
      '      p.PD between :BD and :ED and'
      '      p.COMPID between :COMPID1 and :COMPID2 and'
      '      p.T = 1'
      '')
    AfterDelete = CommitRetaining
    AfterOpen = taPaymentAfterOpen
    AfterPost = CommitRetaining
    BeforeClose = taPaymentBeforeClose
    BeforeOpen = taPaymentBeforeOpen
    OnNewRecord = taPaymentNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 76
    Top = 204
    poSQLINT64ToBCD = True
    object taPaymentID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taPaymentC: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'C'
      currency = True
    end
    object taPaymentPD: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1086#1087#1083#1072#1090#1099
      FieldName = 'PD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taPaymentBANK: TFIBStringField
      DisplayLabel = #1041#1072#1085#1082
      FieldName = 'BANK'
      Size = 60
      EmptyStrToNull = True
    end
    object taPaymentBILL: TFIBStringField
      DisplayLabel = #1057#1095#1077#1090
      FieldName = 'BILL'
      Size = 60
      EmptyStrToNull = True
    end
    object taPaymentCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object taPaymentCONTRACTCLASSID: TFIBIntegerField
      FieldName = 'CONTRACT$CLASS$ID'
    end
  end
  object dsrPayment: TDataSource
    DataSet = taPayment
    Left = 76
    Top = 248
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.D_CompId, c.Name, c.SNAME'
      'from D_Comp c '
      'where c.d_compid<>-1000 and c.seller>=1 and workorg=1'
      '      and c.d_compid<>-1'
      'order by c.SNAME')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 128
    Top = 204
    poSQLINT64ToBCD = True
    object taCompD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taCompNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taCompSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object taBalance: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, SESSIONID, COMPID, BALANCE_B, BALANCE_E, '
      
        '       ICOST, RCOST, CASHCOST, COMPNAME, COMPSNAME, contract$cla' +
        'ss$name'
      'from BALANCE$STAT$COM(?t, ?company$id)'
      '')
    AfterOpen = taBalanceAfterOpen
    BeforeOpen = taBalanceBeforeOpen
    OnCalcFields = taBalanceCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 280
    Top = 176
    poSQLINT64ToBCD = True
    object taBalanceID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taBalanceSESSIONID: TFIBIntegerField
      FieldName = 'SESSIONID'
    end
    object taBalanceCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taBalanceCOMPNAME: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taBalanceBALANCE_B: TFIBFloatField
      DisplayLabel = #1053#1072#1095'. '#1073#1072#1083#1072#1085#1089
      FieldName = 'BALANCE_B'
      currency = True
    end
    object taBalanceBALANCE_E: TFIBFloatField
      DisplayLabel = #1050#1086#1085'. '#1073#1072#1083#1072#1085#1089
      FieldName = 'BALANCE_E'
      currency = True
    end
    object taBalanceICOST: TFIBFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072
      FieldName = 'ICOST'
      currency = True
    end
    object taBalanceRCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090
      FieldName = 'RCOST'
      currency = True
    end
    object taBalanceCASHCOST: TFIBFloatField
      DisplayLabel = #1054#1087#1083#1072#1090#1072
      FieldName = 'CASHCOST'
      currency = True
    end
    object taBalanceCOMPSNAME: TFIBStringField
      DisplayLabel = #1050#1088'. '#1085#1072#1080#1084#1077#1085'. '#1082#1086#1085#1090#1088#1072#1075#1077#1085#1090#1072
      FieldName = 'COMPSNAME'
      EmptyStrToNull = True
    end
    object taBalanceBD: TDateField
      FieldKind = fkCalculated
      FieldName = 'BD'
      Calculated = True
    end
    object taBalanceED: TDateField
      FieldKind = fkCalculated
      FieldName = 'ED'
      Calculated = True
    end
    object taBalanceCONTRACTCLASSNAME: TFIBStringField
      FieldName = 'CONTRACT$CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object taBalanceTag: TStringField
      FieldKind = fkCalculated
      FieldName = 'Tag'
      OnGetText = taBalanceTagGetText
      Size = 32
      Calculated = True
    end
  end
  object dsrBalance: TDataSource
    DataSet = taBalance
    Left = 268
    Top = 252
  end
  object cxLookAndFeelController: TcxLookAndFeelController
    Kind = lfOffice11
    Left = 20
    Top = 200
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 288
    Top = 324
  end
  object prgr: TPrintDBGridEh
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Title.Strings = (
      #1054#1087#1083#1072#1090#1099)
    Units = MM
    Left = 416
    Top = 304
  end
  object frBalance: TfrDBDataSet
    CloseDataSource = True
    DataSet = taBalanceReport
    Left = 340
    Top = 252
  end
  object taPayGraphF: TpFIBDataSet
    SelectSQL.Strings = (
      'select g.D, g.IC, g.PC, g.IPC, g.OPC, p.NAME PAYTYPENAME,'
      '       c.NAME COMPNAME,'
      '       c.SNAME COMPSNAME, i.SN INVNO, i.SDATE INVDATE,'
      '       g.INVID, g.T'
      'from Tmp_PayG g, D_PayType p, D_Comp c, SInv i'
      'where g.D>='#39'TODAY'#39' and'
      '      g.PayTypeId=p.PayTypeId and '
      '      g.T=2 and'
      '      g.COMPID=c.D_COMPID and'
      '      g.INVID=i.SINVID and'
      '      g.SESSIONID=:SESSIONID and'
      '      c.D_COMPID between :COMPID1 and :COMPID2 --and'
      '   --   p.paytypeid between :PAYTYPEID1 and :PAYTYPEID2 '
      '      '
      '        ')
    BeforeOpen = taPayGraphFBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 440
    Top = 208
    object taPayGraphFD: TFIBDateTimeField
      FieldName = 'D'
    end
    object taPayGraphFIC: TFIBFloatField
      FieldName = 'IC'
      currency = True
    end
    object taPayGraphFPC: TFIBFloatField
      FieldName = 'PC'
      currency = True
    end
    object taPayGraphFIPC: TFIBFloatField
      FieldName = 'IPC'
      currency = True
    end
    object taPayGraphFOPC: TFIBFloatField
      FieldName = 'OPC'
      currency = True
    end
    object taPayGraphFPAYTYPENAME: TFIBStringField
      FieldName = 'PAYTYPENAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taPayGraphFCOMPNAME: TFIBStringField
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taPayGraphFCOMPSNAME: TFIBStringField
      FieldName = 'COMPSNAME'
      EmptyStrToNull = True
    end
    object taPayGraphFINVNO: TFIBIntegerField
      FieldName = 'INVNO'
    end
    object taPayGraphFINVDATE: TFIBDateTimeField
      FieldName = 'INVDATE'
    end
    object taPayGraphFINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taPayGraphFT: TFIBSmallIntField
      FieldName = 'T'
    end
  end
  object dsrPayGraphF: TDataSource
    DataSet = taPayGraphF
    Left = 440
    Top = 256
  end
  object taPayGraphPast: TpFIBDataSet
    SelectSQL.Strings = (
      'select g.D, g.IC, g.PC, g.IPC, g.OPC, p.NAME PAYTYPENAME,'
      '       c.NAME COMPNAME,'
      '       c.SNAME COMPSNAME, i.SN INVNO, i.SDATE INVDATE,'
      '       g.INVID, g.T, (g.IC-g.IPC) P'
      'from Tmp_PayG g, D_PayType p, D_Comp c, SInv i'
      'where g.D<'#39'TODAY'#39' and'
      '      g.PayTypeId=p.PayTypeId and '
      '      g.T=2 and'
      '      g.COMPID=c.D_COMPID and'
      '      g.INVID=i.SINVID and'
      '      equal(g.IC, g.IPC, 0.001)=0 and'
      '      g.SESSIONID=:SESSIONID and'
      '      c.D_COMPID between :COMPID1 and :COMPID2 --and '
      '     -- p.paytypeid between :PAYTYPEID1 and :PAYTYPEID2 ')
    BeforeOpen = taPayGraphPastBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 512
    Top = 208
    object taPayGraphPastD: TFIBDateTimeField
      FieldName = 'D'
    end
    object taPayGraphPastIC: TFIBFloatField
      FieldName = 'IC'
      currency = True
    end
    object taPayGraphPastPC: TFIBFloatField
      FieldName = 'PC'
      currency = True
    end
    object taPayGraphPastIPC: TFIBFloatField
      FieldName = 'IPC'
      currency = True
    end
    object taPayGraphPastOPC: TFIBFloatField
      FieldName = 'OPC'
      currency = True
    end
    object taPayGraphPastPAYTYPENAME: TFIBStringField
      FieldName = 'PAYTYPENAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taPayGraphPastCOMPNAME: TFIBStringField
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taPayGraphPastCOMPSNAME: TFIBStringField
      FieldName = 'COMPSNAME'
      EmptyStrToNull = True
    end
    object taPayGraphPastINVNO: TFIBIntegerField
      FieldName = 'INVNO'
    end
    object taPayGraphPastINVDATE: TFIBDateTimeField
      FieldName = 'INVDATE'
    end
    object taPayGraphPastINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taPayGraphPastT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taPayGraphPastP: TFIBFloatField
      FieldName = 'P'
      currency = True
    end
  end
  object dsrPayGraphPast: TDataSource
    DataSet = taPayGraphPast
    Left = 516
    Top = 256
  end
  object taContractClass: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, NAME '
      'from contract$types'
      'where good = 1'
      'order by id')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 408
    Top = 144
    poSQLINT64ToBCD = True
    oFetchAll = True
    object taContractClassID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taContractClassNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object dsContractClass: TDataSource
    DataSet = taContractClass
    Left = 376
    Top = 144
  end
  object Localizer: TcxLocalizer
    StorageType = lstResource
    Left = 24
    Top = 136
  end
  object taCompany: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.D_CompId id, c.Name, c.SNAME'
      'from D_Comp c '
      'where c.d_compid<>-1000 and c.seller>=1 and workorg=1'
      '      and c.d_compid<>-1'
      'order by c.SNAME')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 408
    Top = 176
    poSQLINT64ToBCD = True
    oFetchAll = True
    object taCompanyID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCompanyNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taCompanySNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object dsCompany: TDataSource
    DataSet = taCompany
    Left = 376
    Top = 176
  end
  object taBalanceReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, SESSIONID, COMPID, BALANCE_B, BALANCE_E, '
      
        '       ICOST, RCOST, CASHCOST, COMPNAME, COMPSNAME, contract$cla' +
        'ss$name'
      'from BALANCE$STAT$COM(?t, ?company$id)'
      'order by COMPSNAME, COMPNAME'
      '')
    BeforeOpen = taBalanceReportBeforeOpen
    OnCalcFields = taBalanceReportCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 280
    Top = 208
    poSQLINT64ToBCD = True
    object taBalanceReportID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taBalanceReportSESSIONID: TFIBIntegerField
      FieldName = 'SESSIONID'
    end
    object taBalanceReportCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taBalanceReportBALANCE_B: TFIBFloatField
      FieldName = 'BALANCE_B'
    end
    object taBalanceReportBALANCE_E: TFIBFloatField
      FieldName = 'BALANCE_E'
    end
    object taBalanceReportICOST: TFIBFloatField
      FieldName = 'ICOST'
    end
    object taBalanceReportRCOST: TFIBFloatField
      FieldName = 'RCOST'
    end
    object taBalanceReportCASHCOST: TFIBFloatField
      FieldName = 'CASHCOST'
    end
    object taBalanceReportCOMPNAME: TFIBStringField
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taBalanceReportCOMPSNAME: TFIBStringField
      FieldName = 'COMPSNAME'
      EmptyStrToNull = True
    end
    object taBalanceReportCONTRACTCLASSNAME: TFIBStringField
      FieldName = 'CONTRACT$CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object taBalanceReportBD: TDateField
      FieldKind = fkCalculated
      FieldName = 'BD'
      Calculated = True
    end
    object taBalanceReportED: TDateField
      FieldKind = fkCalculated
      FieldName = 'ED'
      Calculated = True
    end
  end
end
