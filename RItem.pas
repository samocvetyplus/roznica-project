unit RItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Grids, DBGrids, RXDBCtrl, Placemnt, SpeedBar, ExtCtrls,
  DBCtrls, StdCtrls, Buttons, Menus, DB, M207Grid, M207IBGrid;

type
  TfmRItem = class(TForm)
    fs1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siExit: TSpeedItem;
    sb1: TStatusBar;
    pmSItem: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    dg1: TRxDBGrid;
    siAdd: TSpeedItem;
    SpeedItem2: TSpeedItem;
    N3: TMenuItem;
    N4: TMenuItem;
    pc1: TPageControl;
    Panel1: TPanel;
    sbAdd: TSpeedButton;
    SpeedButton2: TSpeedButton;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    M207IBGrid1: TM207IBGrid;
    pm1: TPopupMenu;
    N5: TMenuItem;
    M207IBGrid2: TM207IBGrid;
    procedure ToolButton3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure sbAddSItemClick(Sender: TObject);
    procedure siDelSItemClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1KeyPress(Sender: TObject; var Key: Char);
    procedure sbAddClick(Sender: TObject);
  private
    { Private declarations }

    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure UpdateQW;
  end;

var
  fmRItem: TfmRItem;

implementation

uses Data, comdata, CopySIt, Data2;

{$R *.DFM}

procedure TfmRItem.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRItem.ToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmRItem.FormActivate(Sender: TObject);
begin
  with dm, taSItem do
    begin
      Active:=True;
      if WChar<>#0 then
        begin
          Insert;
          taSItemW.AsString:=WChar;
          dg1.SelectedIndex:=1;

          keybd_event(VK_RETURN, MapVirtualKey(VK_RETURN, 0), 0, 0);
          keybd_event(VK_RETURN, 0, KEYEVENTF_KEYUP, 0);

          keybd_event(VK_END, MapVirtualKey(VK_END, 0), 0, 0);
          keybd_event(VK_END, 0, KEYEVENTF_KEYUP, 0);

        end;
    end;    
  dg1.SelectedIndex:=2;
end;

procedure TfmRItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSet(taSItem);
      CloseDataSets([taSItem, quOpted, quSelled]);
      tr.CommitRetaining;
      taSEl.Refresh;
      WChar:=#0;
    end;
end;

procedure TfmRItem.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  UpdateQW;
  with dm, dm2 do
    begin
      quOpted.Active:=True;
      quSelled.Active:=True;
    end;
end;

procedure TfmRItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRItem.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) AND (Shift=[ssCtrl]) then Close;
end;

procedure TfmRItem.sbAddSItemClick(Sender: TObject);
var i: integer;
begin
  with dm, taSItem do
    begin
      Append;
      taSItemSZ.AsString:='-';
      with dg1 do
        begin
          i:=0;
          while Columns[i].Field.FieldName<>'W' do Inc(i);
          SelectedIndex:=i;
        end
    end;
end;

procedure TfmRItem.siDelSItemClick(Sender: TObject);
begin
  with dm do
    begin
      taSItem.Delete;
    end;
end;

procedure TfmRItem.UpdateQW;
begin
  with dm, quElQW do
    begin
      Params[0].AsInteger:=taSElSElId.AsInteger;
      ExecQuery;
      sb1.Panels[0].Text:='���-��: '+Fields[0].AsString;
      sb1.Panels[1].Text:='���: '+Fields[1].AsString;
      Close;
    end;
end;


procedure TfmRItem.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmRItem.N3Click(Sender: TObject);
var w: extended;
    sz: string[20];
begin
  with dm do
    begin
      PostDataSets([taSItem]);
      if NOT taSItem.Fields[0].IsNull then
        begin
          w:=taSItemW.AsFloat;
          sz:=taSItemSZ.AsString;
          taSItem.Append;
          taSItemW.AsFloat:=w;
          taSItemSZ.AsString:=sz;
          taSItem.Post;
        end;
    end;
end;

procedure TfmRItem.N4Click(Sender: TObject);
var SItemId: integer;
begin
  with dm do
    begin
      PostDataSets([taSItem]);
      if NOT taSItem.Fields[0].IsNull then
        begin
          fmCopySItem:=TfmCopySItem.Create(Application);
          if fmCopySItem.ShowModal=mrOK then
            begin
              SItemId:=taSItemSItemId.AsInteger;
              taSItem.Active:=False;
              with quTmp do
                begin
                  SQL.Text:='execute procedure CopySItem '+IntToStr(SItemId)+', '+IntToStr(Round(fmCopySItem.se1.Value));
                  ExecQuery;
                end;
              taSItem.Active:=True;
              UpdateQW;
            end;
          fmCopySItem.Free;
        end;
    end;
end;

procedure TfmRItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;  
end;

procedure TfmRItem.dg1KeyPress(Sender: TObject; var Key: Char);
begin
  if dg1.SelectedField.FieldName='SZ' then
    if NOT (Key in ['0'..'9', '.' ,'-', #8]) then SysUtils.Abort;
end;

procedure TfmRItem.sbAddClick(Sender: TObject);
begin
  with dm, dm2 do
    begin
      taSItem.Append;
      case pc1.ActivePage.TabIndex of
          0:  begin
                if quOptedUID.IsNull then taSItemUID.Clear
                else taSItemUID.AsInteger:=quOptedUID.AsInteger;

                taSItemW.AsFloat:=quOptedW.AsFloat;
                taSItemSZ.AsString:=quOptedSZ.AsString;
             end;
          1: begin
                if quSelledUID.IsNull then taSItemUID.Clear
                else taSItemUID.AsInteger:=quSelledUID.AsInteger;

                taSItemW.AsFloat:=quOptedW.AsFloat;
                taSItemSZ.AsString:=quOptedSZ.AsString;
             end;
        end;
    end;
end;

end.
