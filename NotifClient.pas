unit NotifClient;

interface

uses Windows, Classes, Messages;

const
  NC_Recieve = WM_User + 8;

type
  TNCRecieve = record
    Msg : Cardinal;
    Text: PChar;
  end;

type
  TNotifReadThread = class(TThread)
  private
    FIp: string;
    FPort: word;
  protected
    procedure Execute; override;
  public
    // Ip ������ � ���������� �������
    property  NotifIp   : string read FIp  write FIp;
    property  NotifPort : word read FPort write FPort;
    constructor Create(CreateSuspended : boolean);overload;
    constructor Create(CreateSuspended : boolean; Ip: string; Port: word);overload;
  end;

implementation

uses WinSock, SyncObjs, Dialogs, SysUtils, Variants, Forms;

procedure HandleWinSockError;
begin
  ShowMessage('Winsock error #'+IntToStr(WSAGetLastError));
end;

{ TNotifReadThread }

constructor TNotifReadThread.Create(CreateSuspended: boolean);
begin
  FIp := '';
  FPort := 0;
  FreeOnTerminate := True;
  inherited Create(CreateSuspended);
end;

constructor TNotifReadThread.Create(CreateSuspended: boolean; Ip: string; Port: word);
begin
  FIp := Ip;
  FPort := Port;
  FreeOnTerminate := True;
  inherited Create(CreateSuspended);
end;

procedure TNotifReadThread.Execute;
var
  s : TSocket;
  server_addr : TSockAddrIn;
  nRead, i : integer;
  Buf : array[0..254] of char;
  wsaData: TWSAData;
  WSVer : word;
  Data : string;
begin
  WSVer := MakeWord(2, 0);
  if WSAStartup(WSVer, wsaData) <> 0 then
  begin
    HandleWinSockError;
    eXit;
  end;
  s := socket(AF_INET, SOCK_STREAM, 0);
  if s = INVALID_SOCKET then
  begin
    HandleWinSockError;
    eXit;
  end;
  try
    FillChar(server_addr, SizeOf(server_addr), #0);
    server_addr.sin_family := AF_INET;
    server_addr.sin_port := htons(FPort);
    server_addr.sin_addr.s_addr := inet_addr(PChar(FIp));
    if connect(s, server_addr, SizeOf(server_addr))= SOCKET_ERROR then
    begin
      HandleWinSockError;
      eXit;
    end;

    while not Terminated  do
    begin
      Data := '';
      FillChar(buf, SizeOf(buf), #0);
      nRead := recv(s,buf,SizeOf(buf),0);
      if (nRead = SOCKET_ERROR) then
      begin
        HandleWinSockError;
        eXit;
      end;
      i:=0;
      while (i<=nRead)and(buf[i]<>#0) do
      begin
        Data:=Data+buf[i];
        inc(i)
      end;
      SendMessage(Application.MainForm.Handle, NC_Recieve, Longint(PChar(Data)), 0);
    end;
  finally
    closesocket(s);
    WSACleanup;
  end;
end;

end.
