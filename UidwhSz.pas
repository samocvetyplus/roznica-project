unit UidwhSz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TfmUidwhSz = class(TForm)
    Label1: TLabel;
    edSz: TEdit;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUidwhSz: TfmUidwhSz;

implementation
uses data;
{$R *.dfm}

procedure TfmUidwhSz.FormCreate(Sender: TObject);
begin
 edSz.Text:=dm.UidwhSZ;
end;

procedure TfmUidwhSz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if ModalResult=mrOk then dm.UidwhSZ:=edSz.Text;
end;

end.
