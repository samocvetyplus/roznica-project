object fmErrClearDb: TfmErrClearDb
  Left = 373
  Top = 353
  Width = 241
  Height = 204
  AutoSize = True
  Caption = #1042#1077#1076#1080#1090#1077' '#1087#1077#1088#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object edtext: TEdit
    Left = 0
    Top = 0
    Width = 233
    Height = 21
    TabOrder = 0
    OnKeyDown = edtextKeyDown
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 152
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 152
    Top = 152
    Width = 75
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
  object RadioGroup1: TRadioGroup
    Left = 0
    Top = 24
    Width = 233
    Height = 121
    Caption = #1063#1080#1089#1090#1082#1072
    ItemIndex = 0
    Items.Strings = (
      #1080#1079#1076#1077#1083#1080#1081' (UID)'
      #1072#1088#1090#1080#1082#1091#1083#1086#1074' (SEL)'
      #1087#1088#1080#1082#1072#1079#1086#1074' (PRORD)'
      #1085#1072#1082#1083#1072#1076#1085#1099#1093' (SINV)'
      #1087#1088#1086#1076#1072#1078' (SELL)'
      #1087#1088#1080#1084#1077#1095#1072#1085#1080#1081' '#1082' '#1080#1079#1076#1077#1083#1080#1103#1084' (UID_INFO)')
    TabOrder = 3
  end
end
