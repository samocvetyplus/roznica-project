object fmApplDepItem: TfmApplDepItem
  Left = 162
  Top = 228
  HelpContext = 100292
  Caption = #1047#1072#1103#1074#1082#1072' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
  ClientHeight = 506
  ClientWidth = 875
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object sp1: TSplitter
    Left = 0
    Top = 238
    Width = 875
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 243
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 875
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1092#1086#1088#1084#1091
      ImageIndex = 0
      Spacing = 1
      Left = 578
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 354
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object pSelect: TPanel
    Left = 0
    Top = 241
    Width = 875
    Height = 265
    Align = alBottom
    TabOrder = 1
    object Splitter3: TSplitter
      Tag = 1
      Left = 33
      Top = 1
      Height = 263
      AutoSnap = False
      MinSize = 20
    end
    object Splitter8: TSplitter
      Tag = 2
      Left = 85
      Top = 1
      Height = 263
      AutoSnap = False
      MinSize = 20
    end
    object Splitter4: TSplitter
      Tag = 2
      Left = 137
      Top = 1
      Height = 263
      AutoSnap = False
      MinSize = 20
    end
    object Splitter10: TSplitter
      Left = 195
      Top = 1
      Height = 263
    end
    object Splitter5: TSplitter
      Tag = 3
      Left = 247
      Top = 1
      Height = 263
      AutoSnap = False
      MinSize = 20
    end
    object Splitter11: TSplitter
      Left = 302
      Top = 1
      Width = 4
      Height = 263
    end
    object Splitter1: TSplitter
      Left = 345
      Top = 1
      Width = 4
      Height = 263
    end
    object lbComp: TListBox
      Left = 1
      Top = 1
      Width = 32
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbCountry: TListBox
      Left = 36
      Top = 1
      Width = 49
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 1
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbGood: TListBox
      Left = 88
      Top = 1
      Width = 49
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 2
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbIns: TListBox
      Left = 140
      Top = 1
      Width = 55
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 3
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbMat: TListBox
      Left = 198
      Top = 1
      Width = 49
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 4
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbAtt1: TListBox
      Left = 250
      Top = 1
      Width = 52
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 5
      Visible = False
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbAtt2: TListBox
      Left = 306
      Top = 1
      Width = 39
      Height = 263
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 6
      Visible = False
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object pSelectUid: TPanel
      Left = 349
      Top = 1
      Width = 525
      Height = 263
      Align = alClient
      TabOrder = 7
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 523
        Height = 41
        Align = alTop
        TabOrder = 0
        object LArt: TLabel
          Left = 6
          Top = 14
          Width = 41
          Height = 13
          Caption = #1040#1088#1090#1080#1082#1091#1083
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object LUID: TLabel
          Left = 219
          Top = 13
          Width = 52
          Height = 13
          Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edArt: TEdit
          Left = 56
          Top = 10
          Width = 121
          Height = 21
          TabOrder = 0
          OnKeyDown = edArtKeyDown
        end
        object edUid: TEdit
          Left = 279
          Top = 9
          Width = 90
          Height = 21
          TabOrder = 1
          OnKeyDown = edUidKeyDown
          OnKeyPress = edUidKeyPress
        end
      end
      object dgWH: TDBGridEh
        Left = 1
        Top = 42
        Width = 523
        Height = 220
        Align = alClient
        AllowedOperations = []
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm2.dsD_WH2
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 2
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
        ParentFont = False
        RowDetailPanel.Color = clBtnFace
        SortLocal = True
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = dgWHDblClick
        OnGetCellParams = dgWHGetCellParams
        Columns = <
          item
            EditButtons = <>
            FieldName = 'PRODCODE'
            Footers = <>
            Title.Caption = #1048#1079#1075'.'
            Title.EndEllipsis = True
            Width = 38
          end
          item
            EditButtons = <>
            FieldName = 'D_MATID'
            Footers = <>
            Title.Caption = #1052#1072#1090'.'
            Title.EndEllipsis = True
            Width = 38
          end
          item
            EditButtons = <>
            FieldName = 'D_GOODID'
            Footers = <>
            Title.Caption = #1053#1072#1080#1084'.'
            Title.EndEllipsis = True
            Width = 39
          end
          item
            EditButtons = <>
            FieldName = 'D_INSID'
            Footers = <>
            Title.Caption = #1054#1042
            Title.EndEllipsis = True
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'D_COUNTRYID'
            Footers = <>
            Title.EndEllipsis = True
            Width = 35
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <
              item
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Value = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
                ValueType = fvtStaticText
              end
              item
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Value = #1042#1089#1077#1075#1086
                ValueType = fvtStaticText
              end>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.EndEllipsis = True
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'QUANTITY'
            Footers = <
              item
                FieldName = 'QUANTITY'
                ValueType = fvtCount
              end
              item
                FieldName = 'QUANTITY'
                ValueType = fvtSum
              end>
            Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1089#1077#1075#1086'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'WEIGHT'
            Footers = <
              item
              end
              item
                FieldName = 'WEIGHT'
                ValueType = fvtSum
              end>
            Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1089#1077#1075#1086'|'#1042#1077#1089
            Title.EndEllipsis = True
            Width = 49
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 41
    Width = 875
    Height = 197
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsApplDepItem
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.FieldName = 'UID'
        Footer.ValueType = fvtCount
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1079#1075'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1053#1072#1080#1084'. '#1080#1079#1076'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1042
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1052#1072#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1056#1072#1089#1093'. '#1094#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'COSTP'
        Footer.FieldName = 'COSTP'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1080#1093'. '#1094#1077#1085#1072
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'REFUSAL'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1054#1090#1082#1072#1079
        Width = 39
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr: TM207FormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'pSelect.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 368
    Top = 128
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 536
    Top = 120
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
  end
end
