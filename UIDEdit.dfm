object fmUIDEdit: TfmUIDEdit
  Left = 279
  Top = 151
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103
  ClientHeight = 308
  ClientWidth = 340
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 20
    Width = 39
    Height = 13
    Caption = #1056#1072#1079#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 44
    Width = 19
    Height = 13
    Caption = #1042#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 16
    Top = 120
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 16
    Top = 168
    Width = 50
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 16
    Top = 216
    Width = 69
    Height = 13
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 16
    Top = 236
    Width = 69
    Height = 13
    Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 16
    Top = 68
    Width = 42
    Height = 13
    Caption = #1045#1076'. '#1080#1079#1084'.'
  end
  object BitBtn1: TBitBtn
    Left = 228
    Top = 20
    Width = 101
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 228
    Top = 60
    Width = 101
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object DBEdit2: TDBEdit
    Left = 92
    Top = 16
    Width = 109
    Height = 21
    Color = clInfoBk
    DataField = 'SZ'
    DataSource = dm.dsUIDWH
    TabOrder = 2
  end
  object DBEdit3: TDBEdit
    Left = 92
    Top = 40
    Width = 109
    Height = 21
    Color = clInfoBk
    DataField = 'W'
    DataSource = dm.dsUIDWH
    TabOrder = 3
  end
  object DBEdit7: TDBEdit
    Left = 92
    Top = 116
    Width = 25
    Height = 21
    Color = clBtnFace
    DataField = 'PRODCODE'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 4
  end
  object DBEdit8: TDBEdit
    Left = 120
    Top = 116
    Width = 25
    Height = 21
    Color = clBtnFace
    DataField = 'MATID'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 5
  end
  object DBEdit9: TDBEdit
    Left = 148
    Top = 116
    Width = 25
    Height = 21
    Color = clBtnFace
    DataField = 'GOODID'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 6
  end
  object DBEdit10: TDBEdit
    Left = 176
    Top = 116
    Width = 25
    Height = 21
    Color = clBtnFace
    DataField = 'INSID'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 7
  end
  object DBEdit11: TDBEdit
    Left = 92
    Top = 140
    Width = 109
    Height = 21
    Color = clBtnFace
    DataField = 'ART'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 8
  end
  object DBEdit12: TDBEdit
    Left = 92
    Top = 164
    Width = 109
    Height = 21
    Color = clBtnFace
    DataField = 'ART2'
    DataSource = dm.dsUIDWH
    ReadOnly = True
    TabOrder = 9
  end
  object BitBtn3: TBitBtn
    Left = 231
    Top = 116
    Width = 101
    Height = 69
    Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
    ParentShowHint = False
    ShowHint = False
    TabOrder = 11
    OnClick = BitBtn3Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
      000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
      00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
      F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
      0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
      FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
      FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
      0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
      00333377737FFFFF773333303300000003333337337777777333}
    Layout = blGlyphTop
    NumGlyphs = 2
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 92
    Top = 213
    Width = 237
    Height = 21
    Color = clInfoBk
    DataField = 'GOODSID1'
    DataSource = dm.dsUIDWH
    KeyField = 'D_GOODSID_SAM'
    ListField = 'NAME_SAM'
    ListSource = dmCom.dsGoodsSam
    TabOrder = 12
  end
  object DBLookupComboBox2: TDBLookupComboBox
    Left = 92
    Top = 236
    Width = 237
    Height = 21
    Color = clInfoBk
    DataField = 'GOODSID2'
    DataSource = dm.dsUIDWH
    KeyField = 'D_GOODSID_SAM'
    ListField = 'NAME_SAM'
    ListSource = dmCom.dsGoodsSam
    TabOrder = 13
  end
  object BitBtn4: TBitBtn
    Left = 93
    Top = 268
    Width = 236
    Height = 29
    Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1087#1088#1080#1084#1077#1095#1072#1085#1080#1081
    ParentShowHint = False
    ShowHint = False
    TabOrder = 10
    OnClick = BitBtn4Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
      00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
      FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
      00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
      FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
      00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
      FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
      FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
      00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
      00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
      FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
      00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
      FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
      000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
      FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
      0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
      FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
      00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
      0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
      00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
      0000000000000000000000000000000000FF00FFFF00FFFF00FF}
  end
  object DBLookupComboBox3: TDBLookupComboBox
    Left = 92
    Top = 64
    Width = 110
    Height = 21
    Color = clInfoBk
    DataField = 'UNITID'
    DataSource = dm.dsUIDWH
    KeyField = 'UNITID'
    ListField = 'UNIT'
    ListSource = dm.dsUnitId
    TabOrder = 14
  end
end
