unit DList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, db, Menus, DBCtrls,
  {Word2000,} OleServer, DBGridEh, TB2Item, ActnList, PrnDbgeh, PrntsEh,
  Printers, RxStrUtils, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar,
  FIBDataSet, pFIBDataSet;

type
  TfmDList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    laDepFrom: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    siDepFrom: TSpeedItem;
    laDepTo: TLabel;
    spitPrint: TSpeedItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    dg2: TDBGridEh;
    pmmain: TTBPopupMenu;
    aclisst: TActionList;
    acAddItem: TAction;
    acAddDinv: TAction;
    acViewItem: TAction;
    acViewDinv: TAction;
    acDel: TAction;
    pmAdd: TTBPopupMenu;
    addItem: TTBItem;
    addDinv: TTBItem;
    pmView: TTBPopupMenu;
    ViewItem: TTBItem;
    ViewDinv: TTBItem;
    acClose: TAction;
    acAllClose: TAction;
    pmClose: TTBPopupMenu;
    pClose: TTBItem;
    AllClose: TTBItem;
    acCloseForm: TAction;
    MAddItem: TTBItem;
    MAddDinv: TTBItem;
    MDel: TTBItem;
    MViewItem: TTBItem;
    MViewDinv: TTBItem;
    MSeachFirstD: TTBItem;
    acSeachFirstD: TAction;
    MSeachSecondD: TTBItem;
    acSeachSecondD: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    pmprint: TTBPopupMenu;
    pStandart: TTBItem;
    pItem: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem4: TTBItem;
    TBItem5: TTBItem;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    siOpenDinv: TSpeedItem;
    acOpen: TAction;
    siAddAll: TSpeedItem;
    pmother: TTBPopupMenu;
    acCreateInvFromPrord: TAction;
    bicreateInvFromSinv: TTBItem;
    biCreateInvAll: TTBItem;
    acCreateInvAll: TAction;
    acCretaeOpt: TAction;
    ciCreateOpt: TTBItem;
    TBSeparatorItem5: TTBSeparatorItem;
    acCreateInvFromInv: TAction;
    SpeedItem1: TSpeedItem;
    acCheck: TAction;
    acCheckDel: TAction;
    TBItem6: TTBItem;
    TBSeparatorItem6: TTBSeparatorItem;
    biFictionInv: TTBItem;
    acFictionInv: TAction;
    siHelp: TSpeedItem;
    SpeedItem2: TSpeedItem;
    taHist: TpFIBDataSet;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    dsrDlist_H: TDataSource;
    biCreateInvFrominv: TTBItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure dg2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acAddItemExecute(Sender: TObject);
    procedure acViewItemExecute(Sender: TObject);
    procedure dg2DblClick(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acAllCloseExecute(Sender: TObject);
    procedure acCloseFormExecute(Sender: TObject);
    procedure acSeachFirstDExecute(Sender: TObject);
    procedure acSeachSecondDExecute(Sender: TObject);
    procedure pStandartClick(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acCreateInvFromPrordExecute(Sender: TObject);
    procedure acCreateInvAllExecute(Sender: TObject);
    procedure acCreateInvAllUpdate(Sender: TObject);
    procedure acCretaeOptUpdate(Sender: TObject);
    procedure acCretaeOptExecute(Sender: TObject);
    procedure acCreateInvFromPrordUpdate(Sender: TObject);
    procedure acCreateInvFromInvExecute(Sender: TObject);
    procedure acCreateInvFromInvUpdate(Sender: TObject);
    procedure acCheckExecute(Sender: TObject);
    procedure acCheckDelExecute(Sender: TObject);
    procedure acFictionInvUpdate(Sender: TObject);
    procedure acFictionInvExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure siOpenDinvClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    LogOprIdForm:string;
    procedure dsDListDataChange(Sender: TObject; Field: TField);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;

  public

  end;

var
  fmDList: TfmDList;
  wm:integer;
  WMode: string[10];

implementation

uses comdata, Data, SInv, DInv, ReportData, Dst, Period, InsRepl, Dst2,
  DstSel, Variants, M207Proc, SList, pFIBQuery, Data2, Dst3, SupCase, DInvItem,
  FIBQuery, Data3, DbUtil, OptBuyerCase, SelectDep, ServData, DInvCheck, MsgDialog,
  Hist, PrOrd;

{$R *.DFM}



procedure TfmDList.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDList.FormCreate(Sender: TObject);
begin
  dm.dsDList.OnDataChange:= dsDListDataChange;
  laDepTo.Caption:='';
  laDepFrom.Caption:='';
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  with dmCom, dm do
    begin
      R_Item:='DINV';
      tr.Active:=True;
      SetBeginDate;
      ShowPeriod;
      taRec.Active:=True;
      WorkMode:='DINV';
      sWorkInvID:='';
      DistrT:=1;
      PActAfterDInv:=False; // ������� �� ��������� �������� �� ��
    end;
 //dg2.FieldColumns['COST1'].Visible:=CenterDep; // �� �������� �� ���������� ����.�. ������,
 dg2.FieldColumns['SCOST'].Visible:=CenterDep; //  ���������
 dg2.FieldColumns['SCOSTD'].Visible:=CenterDep; // � ������� ����
 TBItem4.Visible:=CenterDep;  // � �� �������� �����.��������� � ��������� �����
 LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmDList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      CloseDataSets([dm.taDList, taRec]);
      tr.CommitRetaining;
      R_Item:='';
      WorkMode:='';
    end;
     WMode:='';
end;

procedure TfmDList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taDList]);
      ShowPeriod;
    end;
end;

procedure TfmDList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmDList.dg2GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);

begin

 if dm.taDListCLTID.AsInteger=1 then Background:=$00465AE8
 else if dm.taDListIsClosed.AsInteger=0 then Background:=dm.CU_IM0
 else Background:=dm.CU_IM1;
  if (column.Field<>NIL) then
   begin
    if column.Field.FieldName='SN' then Background:=clAqua
    else if column.Field.FieldName='DEPTO' then Background:=dm.taDListColor.AsInteger;
   end;
end;

procedure TfmDList.dg2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
     VK_SPACE, VK_RETURN:
     begin
      acViewItemExecute(acViewItem);
     end
 end;
end;

procedure TfmDList.acAddItemExecute(Sender: TObject);
var s:string;
    i:integer;
    LogOperationID, LogOperationID1:string;
begin
if not centerdep then
begin
  dm.DDepFromid:=SelfDepId;
  dm.SDepId:=CenterDepId; //�������������� ��������� ����������� � ������� �� ����� ��� �������� ����� ���������
end;  

  with dm do
    if DDepFromID=-1 then MessageDialog('���������� ������� �������� �����', mtInformation, [mbOK], 0)
    else if not checkInv_RUseDep(DDepFromID) then MessageDialog('������ ������� ��������� � ������� '#39 +
                 DDepFrom + #39' �� ��������� ���� ������', mtInformation, [mbOK], 0)
    else
      begin
        if (SDepId=-1) and (not CenterDep) then raise Exception.Create('������� �������� �����');
        if SDepId=-1 then
          begin
            if ShowAndFreeForm(TfmDstSel, Self, TForm(fmDstSel), True, False)=mrCancel then exit;
            LogOperationID:=dm3.insert_operation('�������� ��������� (�� ������ �������� �����)',LogOprIdForm);
            DstDepId:=DDepFromId;
            with quTmp do
              begin
                Close;
                SQL.Text:='EXECUTE PROCEDURE CREATEDINV '+IntToStr(DstDepId)+','+IntToStr(dmCom.UserId);
                ExecQuery;
                Close;
              end;
            dmCom.tr.CommitRetaining;
            ReOpenDataSets([taDList]);
            DstOne2Many:= True;
            dm2.NewSinvID:=1;
            case DstType of
            0: begin
                 LogOperationID1:=dm3.insert_operation('�� ������ ��������',LogOperationID);
                 ShowAndFreeForm(TfmDst2, Self, TForm(fmDst2), True, False);
                 dm3.update_operation(LogOperationID1);
               end;
            1: begin
                 LogOperationID1:=dm3.insert_operation('�� ������ ��������',LogOperationID);
                 ShowAndFreeForm(TfmDst, Self, TForm(fmDst), True, False);
                 dm3.update_operation(LogOperationID1);
               end
            else begin
                  LogOperationID1:=dm3.insert_operation('�� �������� � �������',LogOperationID);
                  ShowAndFreeForm(TfmDst3, Self, TForm(fmDst3), True, False);
                  dm3.update_operation(LogOperationID1);
                 end
            end;
            quTmp.Close;
            quTmp.SQL.Text:='select sinvstr from DelDInv ('+IntToSTr(dmCom.UserId)+')';
            quTmp.ExecQuery;
            s:=trim(qutmp.Fields[0].AsString);
            dmCom.tr.CommitRetaining;
            quTmp.Close;
            sWorkInvID:='';

            i:=1;
            while ExtractWord(i,s,[';'])<>'' do
            begin
             taDList.Locate('SINVID',strtoint(ExtractWord(i,s,[';'])),[]);
             WorkMode:='DINVDEL';
             taDList.Delete;
             WorkMode:='DINV';
             inc(i);
            end;
            dm3.update_operation(LogOperationID);
          end
      else
        begin
          with taDList do
            try
              DisableControls;
              LogOperationID:=dm3.insert_operation('�������� ��������� (������ �������� �����)',LogOprIdForm);
          // � ����������� ������� �� ������ �������� ��������� ��� ���������� ����������
          //   if not Locate('ISCLOSED;CRUSERID;USERID', VarArrayOf([0, dmCom.UserId, dmCom.UserId]), []) then
          //      if not Locate('ISCLOSED;CRUSERID;USERID', VarArrayOf([0, dmCom.UserId, null] ), []) then
          //         begin
                     Append;
                     Post;
         //          end;
              dmCom.tr.CommitRetaining;
            finally
              EnableControls;
            end;
          dm2.NewSinvID:=0;
          if (TComponent(Sender).Tag=0) then
          begin
            LogOperationID:=dm3.insert_operation('������� ��������� �� ��������',LogOprIdForm);
            ShowAndFreeForm(TfmDInvItem, Self, TForm(fmDInvItem), True, False);
            dm3.update_operation(LogOperationID);
          end
          else
          begin
           LogOperationID:=dm3.insert_operation('������� ��������� �� ����������',LogOprIdForm);
           ShowAndFreeForm(TfmDInv, Self, TForm(fmDInv), True, False);
           dm3.update_operation(LogOperationID);
          end;

          dm3.update_operation(LogOperationID);
        end;
    end;
   i:=dm.taDListSINVID.AsInteger;
   dg2.SumList.RecalcAll;
   dm.taDList.Locate('SINVID', i, []);
end;

procedure TfmDList.acViewItemExecute(Sender: TObject);
var LogOperationID:string;
    sinvid :integer;
begin
 dm.taDList.Refresh;
 if dm.taDListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSet(dm.taDList);
 end else if (TComponent(Sender).Tag=0) then
 begin
  LogOperationID:=dm3.insert_operation('�������� ��������� �� ��������',LogOprIdForm);
  ShowAndFreeForm(TfmDInvItem, Self, TForm(fmDInvItem), True, False);
  dm3.update_operation(LogOperationID);
 end
 else
 begin
  LogOperationID:=dm3.insert_operation('�������� ��������� �� ����������',LogOprIdForm);
  ShowAndFreeForm(TfmDInv, Self, TForm(fmDInv), True, False);
  dm3.update_operation(LogOperationID);
 end;

 sinvid := dm.taDListSINVID.AsInteger;
 dg2.SumList.RecalcAll;
 dm.taDList.Locate('SINVID', sinvid, []);
end;

procedure TfmDList.dg2DblClick(Sender: TObject);
begin
 acViewItemExecute(acViewItem);
end;




procedure TfmDList.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not AppDebug then
//   if not dm.checkInv_RUseDep(dm.taDListDEPFROMID.AsInteger) then
  // begin
  //    MessageDialog('������ ������� ��������� � ������� '#39 +
   //    dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName  + #39' �� ��������� ���� ������', mtInformation, [mbOK], 0);
 //eXit;
// end;
 LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
 dm.taDList.Delete;
 dm3.update_operation(LogOperationID);
end;

procedure TfmDList.acCloseExecute(Sender: TObject);
var sn:string;
    LogOperationID, s_close, s_closed:string;
begin
  fmDList.taHist.Active:=true;
  taHist.Open;
  with taHist do
  begin
    fmDList.taHist.Append;
    fmDList.taHistDOCID.AsInteger := dm.taDListSINVID.AsInteger;
    If (Hist.log='') then fmDList.taHistFIO.Value := dmCom.User.Alias
       else fmDList.taHistFIO.Value :=Hist.log;
    fmDList.taHistSTATUS.Value := '�������';
    fmDList.taHist.Post;
    taHist.Transaction.CommitRetaining;
  end;
  taHist.Close;
  fmDList.taHist.Active:=false;
  with dm do
    if taDListIsClosed.AsInteger=0 then
    begin
      if taDListCLTID.AsInteger=1 then raise Exception.Create('������ ������� ���������. ��������� �� �� ������.');
      CheckDInvN;
      taDList.Refresh;
      if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� ���� ������� ������ �������������');
      Screen.Cursor:=crSQLWait;
     {�������� �������� ��������, ��������� �����}
      with quTmp do
      begin
        close;
        SQL.Text:='select str_pr from CHECK_CLOSE_SinvVP ('+taDListSINVID.AsString +')';
        ExecQuery;
        if not Fields[0].IsNull then sn:=trim(Fields[0].AsString)
           else sn:='';
        Transaction.CommitRetaining;
        close;
      end;

     if sn = '' then
     begin
       with quTmp do
       begin
         {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
         SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+taDListSINVID.AsString+')';
         ExecQuery;
         s_close:=trim(Fields[0].AsString);
         s_closed:=trim(Fields[1].AsString);
         Transaction.CommitRetaining;
         close;
       end;
       if (s_close='') and (s_closed='') then
       begin
         Screen.Cursor:=crSQLWait;
         LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
         with qutmp do
         begin
           close;
           sql.Text:='SELECT R_STR from CloseInv('+taDListSInvId.AsString+', 2, '
                      +IntToStr(dmCom.UserId)+')';
           ExecQuery;
           Screen.Cursor:=crDefault;
           if not Fields[0].IsNull then
               MessageDialog('��������� � '+taDListSN.AsString+' ������������� � ��� ��������� '+Fields[0].AsString, mtWarning, [mbOK], 0);
           close;
           Transaction.CommitRetaining;
         end;
       end
         else begin
            if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
            if s_closed<>'' then  MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
         end;
         dmCom.tr.CommitRetaining;
     end
       else Application.MessageBox(pchar(sn),'��������!!',0);
     taDList.Refresh;
     Screen.Cursor:=crDefault;
     SetCloseInvBtn(siOpenDinv, 1);
     sinvid:=dm.taDListSINVID.AsInteger;
     dg2.SumList.RecalcAll;
     dm.taDList.Locate('SINVID',sinvid, []);
   end;
end;


procedure TfmDList.acAllCloseExecute(Sender: TObject);
var sn, sn1: string;
    LogOperationID, s_close, s_closed:string;
    sinvid:integer;
  //  buttonSelected , prsi_dep: Integer;
begin
sn1:='';
//prsi_dep:=0;
  taHist.Active:=true;
  if MessageDialog('������� ��� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
     with dm do
     begin
       Screen.Cursor:=crSQLWait;
       taDList.First;
       while NOT taDList.EOF do
       begin
         if taDListIsClosed.AsInteger=0 then CheckDInvN;
         taDList.Next;
       end;
       LogOperationID:=dm3.insert_operation('������� ��� ��������� ��',LogOprIdForm);
       taDList.First;
       while NOT taDList.EOF do
       begin
         taDList.Refresh;
         if (taDListIsClosed.AsInteger=0) and (taDListCLTID.AsInteger=0) then
         begin
           fmDList.taHist.Append;
           fmDList.taHistDOCID.AsInteger := dm.taSListSINVID.AsInteger;
           fmDList.taHistFIO.Value := dmCom.User.Alias;
           fmDList.taHistSTATUS.Value := '�������';
           fmDList.taHist.Post;
           {�������� �������� ��������, ��������� �����}
           with quTmp do
           begin
             close;
             SQL.Text:='select str_pr from CHECK_CLOSE_SinvVP ('+dm.taDListSINVID.AsString +')';
             ExecQuery;
             if not Fields[0].IsNull then  sn:=trim(Fields[0].AsString)
                else sn:='';
             Transaction.CommitRetaining;
             close;
             SQL.Text:='select str_pr from CHECK_CLOSE_Sinv2Prod ('+dm.taDListSINVID.AsString +')'; {�������� ������� � �������� �����}
             ExecQuery;
             if not Fields[0].IsNull then  sn1:=trim(Fields[0].AsString)
                else sn1:='';
             Transaction.CommitRetaining;
             close;
           end;
           if (sn = '') and (sn1 = '') then
           begin
             with quTmp do
             begin
               {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
               SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+taDListSINVID.AsString+')';
               ExecQuery;
               s_close:=trim(Fields[0].AsString);
               s_closed:=trim(Fields[1].AsString);
               Transaction.CommitRetaining;
               close;
             end;
             if (s_close='') and (s_closed='') then
             begin
               Screen.Cursor:=crSQLWait;
               LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
               with qutmp do
               begin
                 close;
                 sql.Text:='SELECT R_STR from CloseInv('+taDListSInvId.AsString+', 2, '
                           +IntToStr(dmCom.UserId)+')';
                 ExecQuery;
                 Screen.Cursor:=crDefault;
                 if not Fields[0].IsNull then
                    MessageDialog('��������� � '+taDListSN.AsString+' ������������� � ��� ��������� '+Fields[0].AsString, mtWarning, [mbOK], 0);
                 close;
                 Transaction.CommitRetaining;
               end;
             end
               else
               begin
                 if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
                 if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
               end;
               dmCom.tr.CommitRetaining;
               if not pCreatePrord(taDListSINVID.AsString) then
               begin
                   with dm.quTmp do
                   begin
                     close;
                     SQL.Text:='update sinv set isclosed=0, userid = null where sinvid='+dm.taDListSINVID.AsString;
                     ExecQuery;
                     Transaction.CommitRetaining;
                     close;
                   end;
                   dm.taDList.Refresh;
                   dm.SetCloseInvBtn(siOpenDInv, 0);

                   exit;// �������� ������� � ���������, � ������� �������� ����.���� ������ � �������
                 end
               end
              else
              begin
                if sn<>'' then Application.MessageBox(pchar(sn),'��������!!',0);
                if sn1<>'' then Application.MessageBox(pchar(sn1),'��������!!',0);
              end;
              taDList.Refresh;
         end;
         taDList.Next;
       end;
       dmCom.tr.CommitRetaining;
       dm3.update_operation(LogOperationID);
       Screen.Cursor:=crDefault;
       sinvid:=dm.taDListSINVID.AsInteger;
       dg2.SumList.RecalcAll;
       dm.taDList.Locate('SINVID', sinvid, []);
    end;
    fmDList.taHist.Active:=false;
end;

procedure TfmDList.acCloseFormExecute(Sender: TObject);
begin
 Close;
end;

procedure TfmDList.acSeachFirstDExecute(Sender: TObject);
var p: TPoint;
begin
  p:=tb1.ClientToScreen(Point(0, tb2.Top+tb2.Height));
  siDep.DropDownMenu.PopUp(p.x, p.y);
end;

procedure TfmDList.acSeachSecondDExecute(Sender: TObject);
var p: TPoint;
begin
  p:=tb1.ClientToScreen(Point(siDepFrom.Left, tb2.Top+tb2.Height));
  siDepFrom.DropDownMenu.PopUp(p.x, p.y);
end;

procedure TfmDList.pStandartClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr := gen_arr(dg2, dm.taDListSINVID);
  if not dm.taDList.Active then dm.taDList.Active:=true;
  case TComponent(Sender).Tag of
   1: PrintDocument(arr, invoice_in);
   2: PrintDocument(arr, invoice_uin);
   7: PrintDocument(arr, invoice_in_sprice);
  end;
end;

procedure TfmDList.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdg1.Print;
end;

procedure TfmDList.acOpenExecute(Sender: TObject);
var FieldsRes:t_array_var;
    LogOperationID:string;
begin
 if dm.taDListISCLOSED.AsInteger<>0 then
 begin
   Screen.Cursor:=crSQLWait;
   LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
   try
    dmcom.ExecuteQutmp(dm.quTmp,'EXECUTE PROCEDURE OpenInv('+dm.taDListSInvId.AsString+', 2, '+IntToStr(dmCom.UserId) +')',0, FieldsRes);
   finally
    Finalize(FieldsRes);
   end;
   dm.taDList.Refresh;
   dm3.update_operation(LogOperationID);
   dm.SetCloseInvBtn(siOpenDinv, 0);
   Screen.Cursor:=crDefault;
 end;
end;

procedure TfmDList.FormActivate(Sender: TObject);
begin
 WMode:='DLIST';
 dm.pmDListFrom.Items[0].Click;
 dm.pmDListTo.Items[0].Click;
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDList.acCreateInvFromPrordExecute(Sender: TObject);
begin
 with dm do
 begin
  if DDepFromID=-1 then MessageDialog('���������� ������� �������� �����', mtInformation, [mbOK], 0)
  else
   if (SDepId=-1) then MessageDialog('���������� ������� �������� �����', mtInformation, [mbOK], 0)
   else
   begin
    ExecSQL('execute procedure CREATEDINVFROMSINV('+inttostr(Ddepfromid)+', '+inttostr(SDEPID)+', '+
            inttostr(dmcom.UserId)+')', qutmp);
    ReOpenDataSet(taDList);
    MessageDialog('��������� �� �������', mtInformation, [mbOk], 0);
   end;
  end
end;

procedure TfmDList.acCreateInvAllExecute(Sender: TObject);

  procedure DoInv;
  begin
    Screen.Cursor := crSQLWait;
    with dm.quTmp do
    try
      Close;
      SQL.Text := 'select SInvId from RetInv_betweenDep_ALL('+IntToStr(dm.SDepId)+', '+
                  IntToStr(dm.DDepFromId)+','+IntToStr(dmcom.User.UserId)+', null )';
      ExecQuery;
      Transaction.CommitRetaining;
      Close;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

var
  LogOperationID:string;
begin
  Screen.Cursor := crSQLWait;
  LogOperationID:=dm3.insert_operation('������� ����������� � ������� �������',LogOprIdForm);
  if dm.DDepFromId = -1 then MessageDialog('������� �������� �����!', mtWarning, [mbOk], 0)
  else if dm.SDepId=-1 then MessageDialog('������� �������� �����!', mtWarning, [mbOk], 0)
  else
  begin
    DoInv;
    ReOpenDataSets([dm.taDList]);
  end;
  dm3.update_operation(LogOperationID);
  Screen.Cursor := crDefault;
end;

procedure TfmDList.acCreateInvAllUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=dmcom.Adm;
end;

procedure TfmDList.acCretaeOptUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep and (not dm.taDListSINVID.IsNull) and
                          (dm.taDListISCLOSED.AsInteger=1);
end;

procedure TfmDList.acCretaeOptExecute(Sender: TObject);
var res_:integer;
begin
 if ShowAndFreeForm(TfmOptBuyerCase, Self, TForm(fmOptBuyerCase), True, False)=mrok then
 begin
  if dmcom.OptBuyerId=-1 then MessageDialog('��� ������� �����������', mtWarning, [mbOk], 0)
  else if dmcom.OptBuyerId=-2 then MessageDialog('������� ���������� ������', mtInformation, [mbOk], 0)
  else
  try
   with dm do
   begin
    quTmp.Close;
    quTmp.SQL.Text:='select fres from CretateOptInv ('+taDListSINVID.AsString+', '+
     inttostr(dmcom.OptBuyerId)+', '+inttostr(dmcom.UserId)+')';
    qutmp.ExecQuery;
    res_:=quTmp.Fields[0].AsInteger;
    quTmp.Transaction.CommitRetaining;
    quTmp.Close;    
    if res_=1 then MessageDialog('������� ������� ����� ������� ������ �� ������', mtWarning, [mbOk], 0)
    else if res_=2 then MessageDialog('��������� ����������� ����������� �������', mtWarning, [mbOk], 0)
    else if res_=3 then MessageDialog('�� ������ ������� ����������', mtWarning, [mbOk], 0)
    else if res_=4 then MessageDialog('������� �� ��������� �� �� ����� ���� �������, �.�. �� ��� �� ������', mtWarning, [mbOk], 0)
    else MessageDialog('��������� �� ������� ������� ������������!', mtInformation, [mbOk], 0);
   end
  except
    on E:Exception do
      MessageDialog('sinvid='+dm.taDListSINVID.AsString+';'+
                    'optbuyerid='+IntToStr(dmCom.OptBuyerId)+';'+
                    'UserId='+IntToStr(dmCom.UserId)+#13+
                    E.Message, mtError, [mbOk], 0);
  end
 end
end;

procedure TfmDList.acCreateInvFromPrordUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=dmcom.Adm and CenterDep;
end;

procedure TfmDList.acCreateInvFromInvExecute(Sender: TObject);
var CountDep:integer;
 procedure ExecQutmp (text:string);
 begin
  with dm do
  begin
   qutmp.Close;
   qutmp.SQL.Text:=text;
   qutmp.ExecQuery;
   CountDep:=qutmp.Fields[0].asInteger;
   qutmp.Close;
   qutmp.Transaction.CommitRetaining;
  end;
 end;
begin
 if CenterDep then
 begin
  ExecQutmp ('select count(*) from d_dep where d_depid<>-1000 and centerdep<>1');

  if CountDep>1 then
  begin
   dm3.DinvDepId:=-1;
   if ShowAndFreeForm(TfmSelectDep, Self, TForm(fmSelectDep), True, False)=mrOk then
   begin
    ExecQutmp ('select fres from CreateInvFromInv ('+inttostr(dm3.DinvDepId)+
               ', '+dm.taDListSINVID.AsString+', '+inttostr(dmcom.UserId)+')');
    if CountDep=1 then MessageDialog('��������� �� ����� ���� �������!', mtError, [mbOk], 0)
    else
    begin
     MessageDialog('��������� �������!', mtInformation, [mbOk], 0);
     ReOpenDataSet(dm.taDList);
    end
   end;
  end
  else
  begin
   ExecQutmp ('select fres from CreateInvFromInv ('+dm.taDListDEPFROMID.AsString+
              ', '+dm.taDListSINVID.AsString+', '+inttostr(dmcom.UserId)+')');
   if CountDep=1 then MessageDialog('��������� �� ����� ���� �������!', mtError, [mbOk], 0)
   else
   begin
    MessageDialog('��������� �������!', mtInformation, [mbOk], 0);
    ReOpenDataSet(dm.taDList);
   end
  end;
 end
 else
 begin
  ExecQutmp ('select fres from CreateInvFromInv ('+inttostr(CenterDepId)+
             ', '+dm.taDListSINVID.AsString+', '+inttostr(dmcom.UserId)+')');
  if CountDep=1 then MessageDialog('��������� �� ����� ���� �������!', mtError, [mbOk], 0)
  else
  begin
   MessageDialog('��������� �������!', mtInformation, [mbOk], 0);
   ReOpenDataSet(dm.taDList);
  end
 end
end;

procedure TfmDList.acCreateInvFromInvUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taDListSINVID.IsNull) and (
 (CenterDep and  (dm.taDListDEPID.AsInteger=CenterDepId) and (dm.taDListISCLOSED.AsInteger=1)) or
 ((not CenterDep) and (dm.taDListDEPFROMID.AsInteger=CenterDepId) and (dm.taDListISCLOSED.AsInteger=1)))

end;

procedure TfmDList.acCheckExecute(Sender: TObject);
begin
  if dm.taDListISCLOSED.AsInteger = 1 then
     MessageDialog('�������� �� �������� ��� �������� ���������', mtInformation, [mbOk], 0)
  else
  begin
    dm3.CHECKMode := 'DINV';
    ExecSQL('execute procedure DInvCHECK_FILL('+ dm.taDListSINVID.AsString+ ')', dmServ.quTmp );
    ShowAndFreeForm(TfrmDInvCheck, Self, TForm(frmDInvCheck), True, False);
  end;
end;

procedure TfmDList.acCheckDelExecute(Sender: TObject);
begin
  execSql('delete from Dinvcheck  where SInvID = '+ dm.taDListSINVID.AsString, dm.quTmp);
end;

procedure TfmDList.acFictionInvUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (dm.taDListISCLOSED.AsInteger=0) and
  (dm.taDListDEPID.AsInteger=SelfDepId) and (dm.taDListCLTID.AsInteger=0) and CenterDep
end;

procedure TfmDList.acFictionInvExecute(Sender: TObject);
begin
 ExecSQL('update sinv set cltid=1 where sinvid='+dm.taDListSINVID.AsString, dm.quTmp);
 dm.taDList.Refresh;
end;


procedure TfmDList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100221)
end;     

procedure TfmDList.SpeedItem2Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taDList);
  tr.CommitRetaining;
  ReOpenDataSet(taDList);
  taDList.Refresh;
  end;
end;

procedure TfmDList.siOpenDinvClick(Sender: TObject);
var sn:string;
begin
sn:='';
If dm.taDListISCLOSED.AsInteger=1 then
   ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False)
   else
   begin
     {�������� ������� ��������� � �������� ����� ������ ��� ��������}
      with dm.quTmp do
       begin
         close;
         SQL.Text:='select str_pr from CHECK_CLOSE_Sinv2Prod ('+dm.taDListSINVID.AsString +')';
         ExecQuery;
         if not Fields[0].IsNull then sn:=trim(Fields[0].AsString)
            else sn:='';
         Transaction.CommitRetaining;
         close;
       end;
       if (sn='') then
       begin
       //  acClose.Execute;
         if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
         begin
           acClose.Execute;
           if not dm.pCreatePrord(dm.taDListSINVID.AsString) then
           begin
             with dm.quTmp do
             begin
               close;
               SQL.Text:='update sinv set isclosed=0, userid = null where sinvid='+dm.taDListSINVID.AsString;
               ExecQuery;
               Transaction.CommitRetaining;
               close;
             end;
             dm.SetCloseInvBtn(siOpenDInv, 0);
             dm.taDList.Refresh;
             exit;// �������� ������� � ���������, � ������� �������� ����.���� ������ � �������
           end;
         end;
     end
      else
         Application.MessageBox(pchar(sn),'��������!!',0);
   end;
end;

procedure TfmDList.dsDListDataChange(Sender: TObject; Field: TField);
begin
 If (dm.taDListIsclosed.IsNull=true) then siOpenDinv.Enabled:=false
 else siOpenDinv.Enabled:=true;
  if dm.taDListISCLOSED.AsInteger = 0 then
  begin
   dm.SetCloseInvBtn(siOpenDInv, 0);
   siOpenDinv.DropDownMenu:=pmClose;
  end
 else
 begin
 dm.SetCloseInvBtn(siOpenDInv, 1);
 siOpenDinv.DropDownMenu:=nil;
 end;
end;

procedure TfmDList.FormDestroy(Sender: TObject);
begin
wmode:='';
dm.dsDList.OnDataChange:=nil;
end;




end.
