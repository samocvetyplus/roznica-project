object fmRItem: TfmRItem
  Left = 16
  Top = 82
  Width = 755
  Height = 406
  Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 747
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = ToolButton3Click
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = sbAddSItemClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = siDelSItemClick
      SectionName = 'Untitled (0)'
    end
  end
  object sb1: TStatusBar
    Left = 0
    Top = 360
    Width = 747
    Height = 19
    Panels = <
      item
        Text = #1050#1086#1083'-'#1074#1086
        Width = 100
      end
      item
        Text = #1042#1077#1089
        Width = 100
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object dg1: TRxDBGrid
    Left = 532
    Top = 42
    Width = 215
    Height = 318
    Align = alRight
    Color = clBtnFace
    DataSource = dm.dsSItem
    PopupMenu = pmSItem
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = dg1KeyDown
    OnKeyPress = dg1KeyPress
    FixedCols = 1
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        Expanded = False
        FieldName = 'RecNo'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '#'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 26
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UID'
        Title.Alignment = taCenter
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 69
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'W'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 39
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SZ'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 44
        Visible = True
      end>
  end
  object pc1: TPageControl
    Left = 0
    Top = 42
    Width = 489
    Height = 318
    ActivePage = TabSheet2
    Align = alClient
    TabIndex = 1
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #1054#1087#1090
      object M207IBGrid1: TM207IBGrid
        Left = 0
        Top = 0
        Width = 481
        Height = 290
        Align = alClient
        Color = clBtnFace
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm1
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = sbAddClick
        IniStorage = fs1
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = False
        SortOnTitleClick = False
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 88
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1085#1072#1082#1083'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 48
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 57
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 52
            Visible = True
          end>
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1086#1079#1085#1080#1094#1072
      ImageIndex = 1
      object M207IBGrid2: TM207IBGrid
        Left = 0
        Top = 0
        Width = 481
        Height = 290
        Align = alClient
        Color = clBtnFace
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm1
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = sbAddClick
        IniStorage = fs1
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = False
        SortOnTitleClick = False
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ADATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 88
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CHECKNO'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1095#1077#1082#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 48
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE0'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 57
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 52
            Visible = True
          end>
      end
    end
  end
  object Panel1: TPanel
    Left = 489
    Top = 42
    Width = 43
    Height = 318
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 4
    object sbAdd: TSpeedButton
      Left = 8
      Top = 40
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FF3333333333333447333333333333377FFF33333333333744473333333
        333337773FF3333333333444447333333333373F773FF3333333334444447333
        33333373F3773FF3333333744444447333333337F333773FF333333444444444
        733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
        999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
        33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
        333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
        33333777333333333333CC333333333333337733333333333333}
      NumGlyphs = 2
      OnClick = sbAddClick
    end
    object SpeedButton2: TSpeedButton
      Left = 8
      Top = 68
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF3333333333333744333333333333F773333333333337
        44473333333333F777F3333333333744444333333333F7733733333333374444
        4433333333F77333733333333744444447333333F7733337F333333744444444
        433333F77333333733333744444444443333377FFFFFFF7FFFFF999999999999
        9999733777777777777333CCCCCCCCCC33333773FF333373F3333333CCCCCCCC
        C333333773FF3337F333333333CCCCCCC33333333773FF373F3333333333CCCC
        CC333333333773FF73F33333333333CCCCC3333333333773F7F3333333333333
        CCC333333333333777FF33333333333333CC3333333333333773}
      NumGlyphs = 2
      OnClick = siDelSItemClick
    end
  end
  object fs1: TFormStorage
    UseRegistry = True
    StoredValues = <>
    Left = 428
    Top = 182
  end
  object pmSItem: TPopupMenu
    Images = dmCom.ilButtons
    Left = 426
    Top = 230
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 1
      ShortCut = 45
      OnClick = sbAddSItemClick
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelSItemClick
    end
    object N3: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1086#1076#1085#1086' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 52
      ShortCut = 116
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1085#1077#1089#1082#1086#1083#1100#1082#1086' '#1080#1079#1076#1077#1083#1080#1081
      ImageIndex = 51
      ShortCut = 117
      OnClick = N4Click
    end
  end
  object pm1: TPopupMenu
    Left = 280
    Top = 166
    object N5: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 32
      OnClick = sbAddClick
    end
  end
end
