object fmEditPswd: TfmEditPswd
  Left = 367
  Top = 195
  AutoSize = True
  Caption = #1057#1084#1077#1085#1072' '#1087#1072#1088#1086#1083#1103
  ClientHeight = 194
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object pl: TPanel
    Left = 0
    Top = 0
    Width = 329
    Height = 194
    Align = alClient
    BorderStyle = bsSingle
    TabOrder = 0
    object LOldPswd: TLabel
      Left = 8
      Top = 8
      Width = 157
      Height = 13
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1090#1072#1088#1099#1081' '#1087#1072#1088#1086#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LNewPswd: TLabel
      Left = 8
      Top = 61
      Width = 152
      Height = 13
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1085#1086#1074#1099#1081' '#1087#1072#1088#1086#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LRepairNewPwsd: TLabel
      Left = 8
      Top = 109
      Width = 267
      Height = 13
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1077' '#1085#1086#1074#1086#1075#1086' '#1087#1072#1088#1086#1083#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edOldPswd: TEdit
      Left = 64
      Top = 32
      Width = 169
      Height = 21
      PasswordChar = '*'
      TabOrder = 0
    end
    object edNewPswd: TEdit
      Left = 64
      Top = 80
      Width = 169
      Height = 21
      PasswordChar = '*'
      TabOrder = 1
    end
    object edRepairNewPswd: TEdit
      Left = 64
      Top = 128
      Width = 169
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object btOk: TBitBtn
      Left = 40
      Top = 160
      Width = 105
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      Kind = bkOK
    end
    object btCancel: TBitBtn
      Left = 184
      Top = 160
      Width = 105
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      Kind = bkCancel
    end
  end
  object acList: TActionList
    Left = 272
    Top = 24
    object acEnter: TAction
      Caption = 'acEnter'
      ShortCut = 13
      OnExecute = acEnterExecute
    end
    object acEsc: TAction
      Caption = 'acEsc'
      ShortCut = 27
      OnExecute = acEscExecute
    end
  end
end
