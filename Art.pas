unit Art;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ExtDlgs, StdCtrls, DBCtrls, JPEG, db, Buttons,
  ArtNav, ArtGrid, rxPlacemnt, rxSpeedbar;

type
  TfmArt = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Panel1: TPanel;
    Splitter1: TSplitter;
    opd1: TOpenPictureDialog;
    Splitter3: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    im1: TImage;
    Splitter2: TSplitter;
    DBMemo1: TDBMemo;
    fs1: TFormStorage;
    frArtNav1: TfrArtNav;
    frArtGrid1: TfrArtGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure im1DblClick(Sender: TObject);
    procedure frArtGrid1dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure SavePicture(Sender: TObject);
  public
    { Public declarations }
    procedure LoadPicture(Sender: TObject);    
  end;

var
  fmArt: TfmArt;

implementation

uses comdata, DBTree, strutils, M207Proc;

{$R *.DFM}

procedure TfmArt.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmArt.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom do
    begin
      tr.Active:=True;
      taComp.SelectSQL[taComp.SelectSQL.Count-2]:='WHERE PRODUCER=1';
      OpenDataSets([taComp, taMat, taGood, taArt, taIns]);
      D_CompId:=-1;
      D_MatId:='';
      D_GoodId:='';
    end;
  LoadPicture(nil);
  InitArtTree(frArtNav1.tv1);
  frArtGrid1.edArt.Text:='';
end;

procedure TfmArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSets([taArt]);
      CloseDataSets([taComp, taMat, taGood, taArt, taIns]);
      tr.CommitRetaining;
    end;
end;

procedure TfmArt.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmArt.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmArt.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taArt.Append;
end;

procedure TfmArt.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taArt.Delete;
end;

procedure TfmArt.im1DblClick(Sender: TObject);
begin
  if opd1.Execute then
    with dmCom, taArt do
      begin
        im1.Picture.LoadFromFile(opd1.FileName);
        SavePicture(nil);
      end;
end;

procedure TfmArt.LoadPicture(Sender: TObject);

procedure Load(c: TClass);
var s: TStream;
    g:TGraphic;
begin
  g:=TGraphic(c.Create);
  with dmCom do
    s:=taArt.CreateBlobStream(taArtPict, bmRead);
  try
    g.LoadFromStream(s);
    im1.Picture.Assign(g);
  finally
    s.Free;
    g.Free;
  end;
end;

begin
 if dmCom.taArtPict.IsNull then im1.Visible:=False
 else
   begin
     try
        Load(TBitmap);
      except
        try
          Load(TJPEGImage);
        except
          try
            Load(TMetafile);
          except
          end;
        end;
      end;
     im1.Visible:=True;
   end;
end;

procedure TfmArt.SavePicture(Sender: TObject);
var s: TStream;
    g: TGraphic;
begin
    with dmCom, taArt do
      begin
        if NOT(State IN [dsInsert, dsEdit]) then taArt.Edit;
        g:=TGraphic(im1.Picture.Graphic.ClassType.Create);
        s:=taArt.CreateBlobStream(taArtPict, bmWrite);
        try
          g.Assign(im1.Picture.Graphic);
          g.SaveToStream(s);
        finally
          s.Free;
          g.Free;
        end;
        Post;
        LoadPicture(NIL);
      end;
end;

procedure TfmArt.frArtGrid1dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='FULLART') then Background:=dmCom.clMoneyGreen;
end;

end.
