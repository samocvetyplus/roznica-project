unit ErrRq;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, pFIBDataSet, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, FIBDataSet;

type
  TfmErrRq = class(TForm)
    quErr: TpFIBDataSet;
    dsErr: TDataSource;
    dgErr: TM207IBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrRq: TfmErrRq;
  ErrFrom: string;

implementation

uses comdata, M207Proc;

{$R *.dfm}

procedure TfmErrRq.FormCreate(Sender: TObject);
begin
  Caption:=ErrFrom;
  if not dmCom.tr.Active then dmCom.tr.StartTransaction;
  quErr.SelectSQL.Text:='select * from '+ErrFrom;
  OpenDataSets([quErr]);
end;

procedure TfmErrRq.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([quErr]);
  dmCom.tr.CommitRetaining;
end;

end.
