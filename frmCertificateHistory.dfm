object DialogCertificateHistory: TDialogCertificateHistory
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072
  ClientHeight = 444
  ClientWidth = 416
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000001002000680400001600000028000000100000002000
    0000010020000000000000040000120B0000120B000000000000000000000000
    0000000000000000000000000000000000000000000028282802303030073030
    3007282828020000000000000000000000000000000000000000000000000000
    00000000000000000000000000004E4D4D198E8E8E7CB7B6B6C2C7C7C6DEC4C3
    C2DCAAAAA9BA7A7A7A6C47474712000000000000000000000000000000000000
    0000000000002626260180807F5CDEDDDDECECEBEBFFD0D8E8FFB1C3E4FFB6C5
    E2FFDADDE4FFE6E5E4FFC1C0BFDB646363430000000000000000000000000000
    00000000000089898863EEEEEDFCC8D6EDFF5389E6FF2669E0FF80A4E7FF4D7E
    DEFF2D67D8FF7396DCFFDBDDE4FFD5D4D3F06464634300000000000000000000
    00005F5F5F27ECECEBF5C1D3F1FF2B74EBFF397BE9FF256CE5FF2D6FE4FF2769
    E0FF2D6BDDFF376FDCFF4A7ADAFFDBDEE5FFC2C2C1DB48484812000000000000
    0000B5B5B59CEEF1F5FF4388F2FF2776F0FF2271EDFF236FEAFF246DE7FF256B
    E5FF266AE2FF2768DFFF3670DDFF7398DFFFE9E8E7FF7B7B7B6C000000003131
    3104E9E9E9EAAECBF6FF2B7BF4FF2577F3FF2174F2FF2272EFFF2270ECFF2670
    E9FF77A0E6FFADC0E1FF2C6EE2FF2B6ADEFFDDE1E8FFADADACBA2B2B2B024747
    4718F9F9F9FE89B5F6FF5191F3FF2B7BF4FF2678F4FF2275F3FF5A95F1FFCBD9
    F0FFB9CDECFF4582E8FF266DE6FF4A82E4FFB9CAE9FFC8C7C7DC343434074D4D
    4D1DFBFBFBFF90B9F7FF89B4F5FF3280F5FF2E7DF4FF297AF4FFB2CCF3FF8BB4
    F2FF2273F0FF2271EEFF2A74EBFF7FA9EEFFB4C9ECFFCCCCCBDE343434073B3A
    3A06F6F6F6F5A9C9F9FF3F87F5FF3A85F5FF3682F5FF317FF5FFA8C3EDFF6B9F
    EFFF2476F3FF2174F2FF2272F0FF2371EDFFD5DFEFFFBCBBBBC22D2D2D020000
    0000D3D3D3B5E9F0FCFF5796F6FF498DF5FF3E87F5FF3984F5FFAFC9F0FF72A4
    F2FF2B7BF4FF2778F4FF3883F3FF5291F2FFF3F3F2FF9291917C000000000000
    000089898943FDFDFDFEACCBFAFF4A8EF6FF4C8FF5FF4189F6FFB5CEF4FF79AA
    F4FF3581F4FF3480F4FF327FF4FFCEDDF5FFE4E4E4EC53535319000000000000
    000000000000C5C5C596FEFEFEFFADCCFAFF5E9AF7FF498EF6FFBBD3F8FF80AF
    F6FF3F87F5FF5695F5FFC9DBF7FFF5F5F5FC8383835C00000000000000000000
    0000000000003E3E3E04C6C6C696FEFEFEFEEBF2FDFFB0CDFAFFA0C3F9FF9CC1
    F9FFB9D3F9FFF4F6FAFFF2F2F2F58D8D8D632E2E2E0100000000000000000000
    00000000000000000000000000008C8C8C43D5D5D5B5F8F8F8F5FEFEFEFFFDFD
    FDFEEDEDEDEABABABA9C66666627000000000000000000000000000000000000
    00000000000000000000000000000000000000000000414141065555551D4F4F
    4F1839393904000000000000000000000000000000000000000000000000FC3F
    0000F00F0000C0070000C0030000800100008001000000000000000000000000
    0000000000008001000080010000C0030000C0030000F00F0000FC3F0000}
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 0
    Width = 416
    Height = 444
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object GridView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.ScrollBars = ssVertical
      OptionsView.GroupByBox = False
      OptionsView.Indicator = True
      Styles.Content = StyleOdd
      Styles.ContentEven = StyleEven
      object GridViewCOLOR: TcxGridDBColumn
        DataBinding.FieldName = 'COLOR'
        Visible = False
      end
      object GridViewOFFICE: TcxGridDBColumn
        Caption = #1052#1072#1075#1072#1079#1080#1085
        DataBinding.FieldName = 'OFFICE'
        OnCustomDrawCell = GridViewOFFICECustomDrawCell
        HeaderAlignmentHorz = taCenter
        Options.Focusing = False
      end
      object GridViewROUTE: TcxGridDBColumn
        Caption = #1044#1074#1080#1078#1077#1085#1080#1077
        DataBinding.FieldName = 'ROUTE'
        HeaderAlignmentHorz = taCenter
        Width = 139
      end
      object GridViewROUTEDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'ROUTE$DATE'
        HeaderAlignmentHorz = taCenter
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object Data: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1054#1090#1086#1074#1072#1088#1080#1074#1072#1085#1080#1077#39' as varchar(16)) route,  si.sdate rout' +
        'e$date, d.sname office, d.color'
      'from sert_info si, d_dep d'
      
        'where si.sert_id = :certificate$id and d.d_depid = si.d_depid   ' +
        'and si.ret = 1'
      'union all'
      
        'select cast('#39#1042#1086#1079#1074#1088#1072#1090#39' as varchar(16)) route,  si.sdate route$dat' +
        'e, d.sname office, d.color'
      'from sert_info si, d_dep d'
      
        'where si.sert_id = :certificate$id and d.d_depid = si.d_depid   ' +
        'and si.ret = -1'
      'union all'
      
        'select cast('#39#1055#1088#1086#1076#1072#1078#1072#39' as varchar(16)) route,  ss.selldate route$' +
        'date, d.sname office, d.color'
      'from sert_sell ss, d_dep d'
      'where ss.sert_id = :certificate$id and d.d_depid = ss.d_depid'
      'order by 2 asc'
      ''
      '')
    BeforeOpen = DataBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 24
    Top = 24
    object DataROUTE: TFIBStringField
      FieldName = 'ROUTE'
      Size = 16
      EmptyStrToNull = True
    end
    object DataROUTEDATE: TFIBDateTimeField
      FieldName = 'ROUTE$DATE'
    end
    object DataOFFICE: TFIBStringField
      FieldName = 'OFFICE'
      EmptyStrToNull = True
    end
    object DataCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
  end
  object DataSource: TDataSource
    AutoEdit = False
    DataSet = Data
    Left = 56
    Top = 24
  end
  object Styles: TcxStyleRepository
    Left = 24
    Top = 56
    PixelsPerInch = 96
    object StyleEven: TcxStyle
      AssignedValues = [svColor]
      Color = clInfoBk
    end
    object StyleOdd: TcxStyle
    end
  end
end
