unit HelpDins;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmHelpDins = class(TForm)
    Memo1: TMemo;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmHelpDins: TfmHelpDins;

implementation

{$R *.dfm}

procedure TfmHelpDins.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Close;
end;

procedure TfmHelpDins.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   Close;
end;

end.
