unit getdata;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, rxToolEdit;

type
  TfmData = class(TForm)
    deED: TDateEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
  end;

var
  fmData: TfmData;

function GetDate(var ED: TDateTime): boolean;

implementation
uses data;
{$R *.dfm}

function GetDate(var ED: TDateTime): boolean;
begin
  fmData:=TfmData.Create(nil);
  try
    with fmdata do
      begin
        deED.Date:=ED;
        Result:=ShowModal=mrOK;
        if Result then
          begin
            ED:=deED.Date+0.9999;
          end;
      end;
  finally
    fmdata.Free;
  end;
end;

end.
