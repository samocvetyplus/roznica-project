unit UIDStore_T_C;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, RXSplit, RxStrUtils, Buttons,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, Menus, DB,
  DBGridEh, FR_DSet, FR_DBSet, ActnList, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, FR_Class, jpeg, DBGridEhGrouping,
  rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmUIDStore_T_C = class(TForm)
    plSum: TPanel;
    stbrStatus: TStatusBar;
    grbxDep: TGroupBox;
    grbxMain: TGroupBox;
    RxSplitter1: TRxSplitter;
    plSellOut: TPanel;
    fmstrTSum: TFormStorage;
    lbInCap: TLabel;
    lbIn: TLabel;
    lbInvCap: TLabel;
    lbInv: TLabel;
    RxSplitter3: TRxSplitter;
    grbxCurr: TGroupBox;
    lbCurr: TLabel;
    lbCurrSum: TLabel;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    taIn: TpFIBDataSet;
    gridIn: TDBGridEh;
    dsrIn: TDataSource;
    taInW: TFloatField;
    taInCOST: TFloatField;
    taInGROUPNAME: TFIBStringField;
    taSInvDetail: TpFIBDataSet;
    taSInvDetailW: TFloatField;
    taSInvDetailCOST: TFloatField;
    taSInv: TpFIBDataSet;
    taSInvDetailNAME: TFIBStringField;
    gridSInv: TDBGridEh;
    dsrSInvDetail: TDataSource;
    dsrSInv: TDataSource;
    taSInvW: TFloatField;
    taSInvCOST: TFloatField;
    taSInvGROUPNAME: TFIBStringField;
    gridOut: TDBGridEh;
    taOut: TpFIBDataSet;
    dsrOut: TDataSource;
    taOutW: TFloatField;
    taOutCOST: TFloatField;
    taOutGROUPNAME: TFIBStringField;
    plSellIn: TPanel;
    taSell: TpFIBDataSet;
    taSell2: TpFIBDataSet;
    dsrSell: TDataSource;
    taSellW: TFloatField;
    taSellCOST: TFloatField;
    taSellGROUPNAME: TFIBStringField;
    gridSell: TDBGridEh;
    taSellDep: TpFIBDataSet;
    taDep: TpFIBDataSet;
    taSellDepQ: TIntegerField;
    taSellDepW: TFloatField;
    taSellDepCOST: TFloatField;
    taSellDepGROUPNAME: TFIBStringField;
    taSellDepDEPID: TIntegerField;
    taDepD_DEPID: TIntegerField;
    taDepSNAME: TFIBStringField;
    dsrSell2: TDataSource;
    taSell2Q: TIntegerField;
    taSell2W: TFloatField;
    taSell2COST: TFloatField;
    taSell2GROUPNAME: TFIBStringField;
    gridSell2: TDBGridEh;
    taSellDepCOST1: TFloatField;
    qutmp: TpFIBQuery;
    taOutDetail: TpFIBDataSet;
    gridMinus: TDBGridEh;
    dsrOutDetail: TDataSource;
    taOutDetailNAME: TFIBStringField;
    taOutDetailQ: TIntegerField;
    taOutDetailW: TFloatField;
    taOutDetailCOST: TFloatField;
    frIn: TfrDBDataSet;
    acEvent: TActionList;
    acPrint: TAction;
    frSell: TfrDBDataSet;
    frSell2: TfrDBDataSet;
    frOut: TfrDBDataSet;
    gridPlus: TDBGridEh;
    frSInv: TfrDBDataSet;
    frSInvDetail: TfrDBDataSet;
    frOutDetail: TfrDBDataSet;
    taSellQ: TFIBIntegerField;
    taF: TpFIBDataSet;
    dsrF: TDataSource;
    taFQ: TFIBIntegerField;
    taFW: TFIBFloatField;
    taFCOST: TFIBFloatField;
    taFGROUPNAME: TFIBStringField;
    DBGridEh1: TDBGridEh;
    frF: TfrDBDataSet;
    lbFSum: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    lbInCom: TLabel;
    taInCOSTCOM: TFIBFloatField;
    Label11: TLabel;
    lbInvCom: TLabel;
    taSInvCOSTCOM: TFIBFloatField;
    gridSellCom: TDBGridEh;
    taOutCOSTCOM: TFIBFloatField;
    taSellCom: TpFIBDataSet;
    dsrSellCom: TDataSource;
    taSellDepCom: TpFIBDataSet;
    taSellDepComQ: TFIBIntegerField;
    taSellDepComW: TFIBFloatField;
    taSellDepComCOST: TFIBFloatField;
    taSellDepComCOST1: TFIBFloatField;
    taSellComQ: TFIBIntegerField;
    taSellComW: TFIBFloatField;
    taSellComCOST: TFIBFloatField;
    taSellComCOM: TFIBStringField;
    taSellDepComDEPID: TFIBIntegerField;
    frSellCom: TfrDBDataSet;
    Label1: TLabel;
    lbCurrSumCom: TLabel;
    taSInvDetailQ: TFIBIntegerField;
    taSInvDetailCOSTCOM: TFIBFloatField;
    taInQ: TFIBIntegerField;
    taSInvQ: TFIBIntegerField;
    taOutQ: TFIBIntegerField;
    taOutDetailCOSTCOM: TFIBFloatField;
    siHelp: TSpeedItem;
    taSellAll: TpFIBDataSet;
    taSellAllDep: TpFIBDataSet;
    taSellAllQ: TFIBIntegerField;
    taSellAllW: TFIBFloatField;
    taSellAllCOST: TFIBFloatField;
    taSellAllCOM: TFIBStringField;
    taSellAllDepQ: TFIBIntegerField;
    taSellAllDepW: TFIBFloatField;
    taSellAllDepCOST: TFIBFloatField;
    taSellAllDepDEPID: TFIBIntegerField;
    taSellAllDepCOST1: TFIBFloatField;
    frSellAll: TfrDBDataSet;
    taInCostKeep: TFIBFloatField;
    taSInvCostKeep: TFIBFloatField;
    taOutCostKeep: TFIBFloatField;
    taSInvDetailCostKeep: TFIBFloatField;
    taOutDetailCOSTKEEP: TFIBFloatField;
    GridSellKeep: TDBGridEh;
    taSellKeep: TpFIBDataSet;
    taSellKeepQ: TFIBIntegerField;
    taSellKeepW: TFIBFloatField;
    taSellKeepCost: TFIBFloatField;
    taSellKeepCom: TFIBStringField;
    dsrSellKeep: TDataSource;
    RxSplitter2: TRxSplitter;
    taSellDepKeep: TpFIBDataSet;
    taSellDepKeepQ: TFIBIntegerField;
    taSellDepKeepW: TFIBFloatField;
    taSellDepKeepCost: TFIBFloatField;
    taSellDepKeepCost1: TFIBFloatField;
    taSellDepKeepDepID: TFIBIntegerField;
    frSellKeep: TfrDBDataSet;
    taInAll: TpFIBDataSet;
    taOutAll: TpFIBDataSet;
    frInAll: TfrDBDataSet;
    frOutAll: TfrDBDataSet;
    taInAllREALCOST: TFIBFloatField;
    taInAllFACTCOST: TFIBFloatField;
    taInAllOTV: TFIBFloatField;
    taInAllPRCOST: TFIBFloatField;
    taInAllCOST5: TFIBFloatField;
    taInAllCOST30: TFIBFloatField;
    taInAllCOST45: TFIBFloatField;
    taInAllCOST60: TFIBFloatField;
    taInAllCOST90: TFIBFloatField;
    taOutAllREALCOST: TFIBFloatField;
    taOutAllFACTCOST: TFIBFloatField;
    taOutAllOTV: TFIBFloatField;
    taOutAllPRCOST: TFIBFloatField;
    taOutAllCOST5: TFIBFloatField;
    taOutAllCOST30: TFIBFloatField;
    taOutAllCOST45: TFIBFloatField;
    taOutAllCOST60: TFIBFloatField;
    taOutAllCOST90: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure siExitClick(Sender: TObject);
    procedure BeforeOpen(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    LogOperIdForm : string;
  end;

var
  fmUIDStore_T_C: TfmUIDStore_T_C;

implementation

uses Data, comdata, Data2, M207Proc, ReportData, Variants, ServData, Period,
  Data3, JewConst;

{$R *.DFM}

const
  cCurrFormat = '### ### ##0.00';

procedure TfmUIDStore_T_C.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUIDStore_T_C.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    #27 : Close;
  end;
end;

procedure TfmUIDStore_T_C.FormCreate(Sender: TObject);
var
  f, fCom : double;
  ff : TFIBFloatField;
begin
  tb1.Wallpaper := wp;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  // �������� �����
  OpenDataSets([taIn]);
  f:=0;
  fcom:=0;
  while not taIn.Eof do
  begin
    f := f+taInCOST.AsFloat;
    fcom := fcom+taInCOSTCOM.AsFloat;
    taIn.Next;
  end;
  lbIn.Caption := FormatFloat(cCurrFormat, f);
  lbInCom.Caption := FormatFloat(cCurrFormat, fcom);
  // ������ �� ������
  OpenDataSets([taSInv]);
  f:=0;
  fcom:=0;  
  while not taSInv.Eof do
  begin
    f:=f+taSInvCOST.AsFloat;
    fcom := fcom+taSInvCOSTCOM.AsFloat;
    taSInv.Next;
  end;
  lbInv.Caption := FormatFloat(cCurrFormat, f);
  lbInvCom.Caption := FormatFloat(cCurrFormat, fcom);  
  // ����������� �� ����� ������� �� ������
  OpenDataSets([taSInvDetail]);
  // ��������� �������
  taDep.Open;
  while not taDep.Eof do
  begin
    ff := TFIBFloatField.Create(taSell);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSell.Name+ff.FieldName;
    ff.DataSet := taSell;
    taSell.SelectSQL[2]:=taSell.SelectSQL[2]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSell.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      Width:=85;
    end;

    ff := TFIBFloatField.Create(taSell2);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSell2.Name+ff.FieldName;
    ff.DataSet := taSell2;
    taSell2.SelectSQL[2]:=taSell2.SelectSQL[2]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSell2.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      width:=85;      
    end;

    ff := TFIBFloatField.Create(taSellCom);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSellCom.Name+ff.FieldName;
    ff.DataSet := taSellCom;
    taSellCom.SelectSQL[1]:=taSellCom.SelectSQL[1]+', cast(0.0 as double precision) '+ff.FieldName;
    taSellCom.SelectSQL[7]:=taSellCom.SelectSQL[7]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSellCom.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      width:=85;
    end;

    ff := TFIBFloatField.Create(taSellKeep);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSellKeep.Name+ff.FieldName;
    ff.DataSet := taSellKeep;
    taSellKeep.SelectSQL[1]:=taSellKeep.SelectSQL[1]+', cast(0.0 as double precision) '+ff.FieldName;
    taSellKeep.SelectSQL[7]:=taSellKeep.SelectSQL[7]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSellKeep.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      width:=85;
    end;

    ff := TFIBFloatField.Create(taSellAll);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSellAll.Name+ff.FieldName;
    ff.DataSet := taSellAll;
    taSellAll.SelectSQL[1]:=taSellAll.SelectSQL[1]+', cast(0.0 as double precision) '+ff.FieldName;
    taSellAll.SelectSQL[7]:=taSellAll.SelectSQL[7]+', cast(0.0 as double precision) '+ff.FieldName;


    taDep.Next;
  end;
  taDep.Close;

  OpenDataSets([taSell, taSell2, taSellCom, taSellKeep,taSellAll]);
  OpenDataSets([taSellDep, taSellDepCom, taSellDepKeep, taSellAllDep]);

  taSellDep.First;
  while not taSellDep.Eof do
  begin
   taSell.Locate('GROUPNAME',taSellDepGROUPNAME.AsString,[]);
   taSell.Edit;
   taSell.FieldByName('F_'+taSellDepDEPID.AsString).AsFloat:=taSellDepCOST.AsFloat;
   taSell.Post;

   taSell2.Locate('GROUPNAME',taSellDepGROUPNAME.AsString,[]);
   taSell2.Edit;
   taSell2.FieldByName('F_'+taSellDepDEPID.AsString).AsFloat:=taSellDepCOST1.AsFloat;
   taSell2.Post;

   taSellDep.Next;
  end;

  taSellDepCom.First;
  while not taSellDepCom.Eof do
  begin
   taSellCom.Locate('COM','� ��������� �����',[]);
   taSellCom.Edit;
   taSellCom.FieldByName('F_'+taSellDepComDEPID.AsString).AsFloat:=taSellDepComCOST.AsFloat;
   taSellCom.Post;

   taSellCom.Locate('COM','� ��������� �����',[]);
   taSellCom.Edit;
   taSellCom.FieldByName('F_'+taSellDepComDEPID.AsString).AsFloat:=taSellDepComCOST1.AsFloat;
   taSellCom.Post;

   taSellDepCom.Next;
  end;


  taSellDepKeep.First;
  while not taSellDepKeep.Eof do
  begin
   taSellKeep.Locate('COM','� ��������� �����',[]);
   taSellKeep.Edit;
   taSellKeep.FieldByName('F_'+taSellDepKeepDEPID.AsString).AsFloat:=taSellDepKeepCOST.AsFloat;
   taSellKeep.Post;

   taSellKeep.Locate('COM','� ��������� �����',[]);
   taSellKeep.Edit;
   taSellKeep.FieldByName('F_'+taSellDepKeepDEPID.AsString).AsFloat:=taSellDepKeepCOST1.AsFloat;
   taSellKeep.Post;

   taSellDepKeep.Next;
  end;


  taSellAllDep.First;
  while not taSellAllDep.Eof do
  begin
   taSellAll.Locate('COM','� ��������� �����',[]);
   taSellAll.Edit;
   taSellAll.FieldByName('F_'+taSellAllDepDEPID.AsString).AsFloat:=taSellAllDepCOST.AsFloat;
   taSellAll.Post;

   taSellAll.Locate('COM','� ��������� �����',[]);
   taSellAll.Edit;
   taSellAll.FieldByName('F_'+taSellAllDepDEPID.AsString).AsFloat:=taSellAllDepCOST1.AsFloat;
   taSellAll.Post;

   taSellAllDep.Next;
  end;

  // ��������� �����
  OpenDataSets([taOut]);
  f:=0;
  fcom:=0;
  while not taOut.Eof do
  begin
    f:=f+taOutCOST.AsFloat;
    fcom:=fcom+taOutCOSTCom.AsFloat;
    taOut.Next;
  end;
  lbCurrSum.Caption := FormatFloat(cCurrFormat, f);
  lbCurrSumCom.Caption := FormatFloat(cCurrFormat, fcom);

  qutmp.Close;
  // ����������� �������
  OpenDataSets([taOutDetail]);
  // ����������� �������
  OpenDataSets([taF]);
  f := 0;
  while not taF.Eof do
  begin
    f := f + taFCOST.AsFloat;
    taF.Next;
  end;
  lbFSum.Caption := FormatFloat(cCurrFormat, f);
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStore_T_C.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taIn, taSInv, taSInvDetail, taSell, taOut, taSell2, taOutDetail, taSellCom, taSellKeep, taSellAll]);
  dmCom.tr.CommitRetaining;
  Action := caFree;
end;

procedure TfmUIDStore_T_C.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDStore_T_C.BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := dm3.taUIDStoreListSINVID.AsInteger;
end;

procedure TfmUIDStore_T_C.acPrintExecute(Sender: TObject);
var
  LogOperationID: string;
  Dummy: Variant;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStorePrint, LogOperIdForm);
  Dummy := frVariables['TotalInCom'];
  dmReport.PrintDocumentB(uidstore_t_p_c);
  Dummy := frVariables['TotalInCom'];
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStore_T_C.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100422)
end;

procedure TfmUIDStore_T_C.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
