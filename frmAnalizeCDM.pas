unit frmAnalizeCDM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxLookAndFeelPainters, cxGroupBox, Menus, StdCtrls, cxButtons,
  DBClient, Provider, dxSkinsCore, dxSkinsDefaultPainters, cxGraphics,
  cxLookAndFeels;

type

  TDialogAnalizeCDM = class(TForm)
    DBSell: TpFIBDataSet;
    DBStorage: TpFIBDataSet;
    EditorDateBegin: TcxDateEdit;
    EditorDateEnd: TcxDateEdit;
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ProviderSell: TDataSetProvider;
    Sell: TClientDataSet;
    ProviderStorage: TDataSetProvider;
    Storage: TClientDataSet;
    SellMODEL: TStringField;
    SellMODELSIZE: TStringField;
    SellDEPARTMENTID: TIntegerField;
    SellSELLCOUNT: TIntegerField;
    SellSTORAGECOUNT: TIntegerField;
    StorageMODEL: TStringField;
    StorageMODELSIZE: TStringField;
    StorageDEPARTMENTID: TIntegerField;
    StorageSELLCOUNT: TIntegerField;
    StorageSTORAGECOUNT: TIntegerField;
    Departments: TpFIBDataSet;
    DepartmentsID: TFIBIntegerField;
    DepartmentsNAME: TFIBStringField;
    SaveDialog: TSaveDialog;
    procedure OnBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    procedure InternalExecute;
  public
    class procedure Execute;
  end;



implementation



uses ShlObj, uUtils;

{$R *.dfm}

var
  DialogAnalizeCDM: TDialogAnalizeCDM;

class procedure TDialogAnalizeCDM.Execute;
var
  DialogResult: Boolean;
begin
  DialogAnalizeCDM := TDialogAnalizeCDM.Create(nil);
  DialogResult := DialogAnalizeCDM.ShowModal = mrOK;
  if DialogResult then
  begin
    //DialogAnalizeCDM.Database.Connected := True;
    //DialogAnalizeCDM.Transaction.StartTransaction;
    DialogAnalizeCDM.InternalExecute;
    //DialogAnalizeCDM.Transaction.Commit;
    //DialogAnalizeCDM.Database.Connected := False;
  end;
  DialogAnalizeCDM.Free;
end;

procedure TDialogAnalizeCDM.InternalExecute;
var
  Model: string;
  ModelSize: string;
  DepartmentID: Integer;
  DepartmentName: string;
  FileName: string;
begin
  Screen.Cursor := crHourGlass;
  Sell.Active := True;
  Sell.LogChanges := False;
  Sell.ProviderName := '';
  Storage.Active := True;
  Storage.LogChanges := False;
  Storage.ProviderName := '';
  Storage.First;
  Sell.IndexFieldNames := 'Model;Model$Size;Department$ID';
  while not Storage.Eof do
  begin
    Model := StorageMODEL.AsString;
    ModelSize := StorageMODELSIZE.AsString;
    DepartmentID := StorageDEPARTMENTID.AsInteger;
    if Sell.FindKey([Model, ModelSize, DepartmentID]) then
    begin
      Sell.Edit;
      SellSTORAGECOUNT.AsInteger := StorageSTORAGECOUNT.AsInteger;
      Sell.Post;
    end
    else
    begin
      Sell.Append;
      SellMODEL.AsString := Model;
      SellMODELSIZE.AsString := ModelSize;
      SellDEPARTMENTID.AsInteger := DepartmentID;
      SellSELLCOUNT.AsInteger := 0;
      SellSTORAGECOUNT.AsInteger := StorageSTORAGECOUNT.AsInteger;
      Sell.Post;
    end;
    Storage.Next;
  end;
  Sell.First;
  Departments.Active := True;
  Departments.Last;
  Departments.First;
  Sell.SetOptionalParam('Department$Count', Departments.RecordCount);
  while not Departments.Eof do
  begin
    DepartmentID := DepartmentsID.AsInteger;
    DepartmentName := DepartmentsNAME.AsString;
    Sell.SetOptionalParam('Department$ID$' + IntToStr(Departments.RecNo), DepartmentID);
    Sell.SetOptionalParam('Department$Name$' + IntToStr(Departments.RecNo), DepartmentName);
    Departments.Next;
  end;
  Screen.Cursor := crDefault;
  Application.HandleMessage;
  //if SaveDialog.Execute then
  FileName := GetSpecialFolderLocation(CSIDL_APPDATA) + '\jew\analize.cds';

  Sell.SaveToFile(FileName, dfBinary);
  Application.MessageBox('���� ������� ��������.','����������',MB_OK);
end;

procedure TDialogAnalizeCDM.OnBeforeOpen(DataSet: TDataSet);
begin
  TClientDataSet(DataSet).Params.ParamByName('begin$date').AsDateTime := EditorDateBegin.EditValue;
  TClientDataSet(DataSet).Params.ParamByName('end$date').AsDateTime := EditorDateEnd.EditValue;
end;

procedure TDialogAnalizeCDM.FormCreate(Sender: TObject);
begin
  EditorDateBegin.EditValue := IncMonth(Date, -6);
  EditorDateEnd.EditValue := Date;
end;

end.

