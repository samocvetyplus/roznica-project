unit frmWarehouse;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid,
  FIBDataSet, pFIBDataSet, Provider, DBClient, StdCtrls, cxGridDBTableView,
  Grids, DBGrids, pFIBClientDataSet, cxLocalization, DateUtils,
  cxDBLookupComboBox, IniFiles, ShlObj, cxCheckBox, cxTextEdit,
  cxGridDBDataDefinitions, cxFilterConsts, DBCtrls, pFIBScripter, FIBDatabase,
  pFIBDatabase, SIBEABase, SIBFIBEA, FIBQuery, pFIBQuery, pFIBStoredProc,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter, dxBar, ImgList, Registry,
  dxSkinsdxBarPainter;

const

  FlagName: array[0..3] of Char = (' ', '�', '�', '�');

  UnitName: array[0..1] of string = ('��.','��.');

type

  TcxGridBandedTableController = class(cxGridBandedTableView.TcxGridBandedTableController)
  protected
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X: Integer; Y: Integer); override;
  end;

  TcxGridDBDataController = class(cxGridDBDataDefinitions.TcxGridDBDataController)
  private
    FNotifyFlag: Boolean;
  protected
    procedure FilterChanged; override;
    procedure SummaryChanged(ARedrawOnly: Boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    property NotifyFlag: Boolean read FNotifyFlag write FNotifyFlag;
  end;

  TcxGridDBBandedTableView = class(cxGridDBBandedTableView.TcxGridDBBandedTableView)
  protected
    function GetControllerClass: TcxCustomGridControllerClass; override;
    function GetDataControllerClass: TcxCustomDataControllerClass; override;
  end;

  TpFIBClientDataSet = class(pFIBClientDataSet.TpFIBClientDataSet)
  public
    function Locate(const KeyFields: string; const KeyValues: Variant;  Options: TLocateOptions): Boolean; override;
  end;

  TClientDataSet = class(DBClient.TClientDataSet)
  public
    function Locate(const KeyFields: string; const KeyValues: Variant;  Options: TLocateOptions): Boolean; override;
  end;

  TWarehouse = class(TFrame)
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    GridView: TcxGridDBBandedTableView;
    GridViewUID: TcxGridDBBandedColumn;
    GridViewMATID: TcxGridDBBandedColumn;
    GridViewINSID: TcxGridDBBandedColumn;
    GridViewART: TcxGridDBBandedColumn;
    GridViewART2: TcxGridDBBandedColumn;
    GridViewNDSNAME: TcxGridDBBandedColumn;
    GridViewW: TcxGridDBBandedColumn;
    GridViewSZ: TcxGridDBBandedColumn;
    GridViewDEP: TcxGridDBBandedColumn;
    GridViewPRODCODE: TcxGridDBBandedColumn;
    GridViewSUPCODE: TcxGridDBBandedColumn;
    GridViewSN: TcxGridDBBandedColumn;
    GridViewSN0: TcxGridDBBandedColumn;
    GridViewSSF0: TcxGridDBBandedColumn;
    GridViewSDATE0: TcxGridDBBandedColumn;
    GridViewSDATE: TcxGridDBBandedColumn;
    GridViewSELLDATE: TcxGridDBBandedColumn;
    GridViewNDATE: TcxGridDBBandedColumn;
    GridViewS_INS: TcxGridDBBandedColumn;
    GridViewD_COUNTRYID: TcxGridDBBandedColumn;
    GridViewSUPNAME: TcxGridDBBandedColumn;
    GridViewPRODNAME: TcxGridDBBandedColumn;
    GridViewNAMEGOOD: TcxGridDBBandedColumn;
    GridViewFLAGNAME: TcxGridDBBandedColumn;
    DataSource: TDataSource;
    DBData: TpFIBDataSet;
    GridViewGOODS1: TcxGridDBBandedColumn;
    GridViewGOODS2: TcxGridDBBandedColumn;
    GridViewPRICE: TcxGridDBBandedColumn;
    GridViewCOST: TcxGridDBBandedColumn;
    GridViewSPRICE0: TcxGridDBBandedColumn;
    GridViewMemo: TcxGridDBBandedColumn;
    GridViewUNITNAME: TcxGridDBBandedColumn;
    NDS: TClientDataSet;
    NDSProvider: TDataSetProvider;
    DBNDS: TpFIBDataSet;
    NDSID: TIntegerField;
    NDSNAME: TStringField;
    DBPayType: TpFIBDataSet;
    PayTypeProvider: TDataSetProvider;
    PayType: TClientDataSet;
    PayTypeID: TIntegerField;
    PayTypeNAME: TStringField;
    GridViewPAYTYPENAME: TcxGridDBBandedColumn;
    StyleRepository: TcxStyleRepository;
    StyleBold: TcxStyle;
    DBOffice: TpFIBDataSet;
    ProviderOffice: TDataSetProvider;
    Office: TClientDataSet;
    DBOfficeID: TFIBIntegerField;
    DBOfficeNAME: TFIBStringField;
    DBOfficeCOLOR: TFIBIntegerField;
    OfficeID: TIntegerField;
    OfficeNAME: TStringField;
    GridViewDEPID: TcxGridDBBandedColumn;
    StyleDefault: TcxStyle;
    StyleInfoBk: TcxStyle;
    StyleAqua: TcxStyle;
    GridViewT: TcxGridDBBandedColumn;
    GridViewISCLOSED: TcxGridDBBandedColumn;
    GridViewITYPE: TcxGridDBBandedColumn;
    GridViewCOST1: TcxGridDBBandedColumn;
    StyleGray: TcxStyle;
    GridViewNAMEDEPFROM: TcxGridDBBandedColumn;
    GridViewSDATETIME0: TcxGridDBBandedColumn;
    StyleMoneyGreen: TcxStyle;
    DBCompany: TpFIBDataSet;
    ProviderCompany: TDataSetProvider;
    Company: TClientDataSet;
    OfficeCOLOR: TIntegerField;
    CompanyNAME: TStringField;
    CompanySHORTNAME: TStringField;
    DataSourceCompany: TDataSource;
    CompanyCODE: TStringField;
    GridViewMARK: TcxGridDBBandedColumn;
    StyleSelectedHeader: TcxStyle;
    MemoryData: TpFIBClientDataSet;
    Provider: TpFIBDataSetProvider;
    MemoryDataUID: TIntegerField;
    MemoryDataMATID: TStringField;
    MemoryDataINSID: TStringField;
    MemoryDataART: TStringField;
    MemoryDataART2: TStringField;
    MemoryDataS_INS: TStringField;
    MemoryDataW: TFloatField;
    MemoryDataSZ: TStringField;
    MemoryDataCOST: TFloatField;
    MemoryDataDEPID: TIntegerField;
    MemoryDataSN0: TIntegerField;
    MemoryDataSSF0: TStringField;
    MemoryDataSPRICE0: TFloatField;
    MemoryDataSN: TIntegerField;
    MemoryDataT: TSmallintField;
    MemoryDataISCLOSED: TSmallintField;
    MemoryDataITYPE: TSmallintField;
    MemoryDataPRODCODE: TStringField;
    MemoryDataSUPCODE: TStringField;
    MemoryDataPRICE: TFloatField;
    MemoryDataDEP: TStringField;
    MemoryDataCOST1: TFloatField;
    MemoryDataD_COUNTRYID: TStringField;
    MemoryDataNAMEDEPFROM: TStringField;
    MemoryDataGOODS1: TStringField;
    MemoryDataGOODS2: TStringField;
    MemoryDataNAMEGOOD: TStringField;
    MemoryDataFLAG: TIntegerField;
    MemoryDataFLAGNAME: TStringField;
    MemoryDataUNITID: TIntegerField;
    MemoryDataUNITNAME: TStringField;
    MemoryDataNDSID: TIntegerField;
    MemoryDataNDSNAME: TStringField;
    MemoryDataPAYTYPEID: TIntegerField;
    MemoryDataPAYTYPENAME: TStringField;
    MemoryDataMEMO: TStringField;
    MemoryDataSDATE: TDateField;
    MemoryDataSELLDATE: TDateField;
    MemoryDataNDATE: TDateField;
    MemoryDataSDATETIME0: TDateTimeField;
    MemoryDataSDATE0: TDateField;
    MemoryDataPRODNAME: TStringField;
    MemoryDataSUPNAME: TStringField;
    MemoryDataMARK: TIntegerField;
    MemoryDataSITEMID: TIntegerField;
    GridViewSITEMID: TcxGridDBBandedColumn;
    MemoryDataFilter: TIntegerField;
    GridViewFilter: TcxGridDBBandedColumn;
    DBMaterial: TpFIBDataSet;
    ProviderMaterial: TDataSetProvider;
    Material: TClientDataSet;
    MaterialID: TStringField;
    MaterialNAME: TStringField;
    DBInsertion: TpFIBDataSet;
    ProviderInsertion: TDataSetProvider;
    Insertion: TClientDataSet;
    InsertionID: TStringField;
    InsertionNAME: TStringField;
    DBCountry: TpFIBDataSet;
    ProviderCountry: TDataSetProvider;
    Country: TClientDataSet;
    CountryID: TStringField;
    CountryNAME: TStringField;
    DBGood: TpFIBDataSet;
    ProviderGood: TDataSetProvider;
    Good: TClientDataSet;
    GoodID: TStringField;
    GoodNAME: TStringField;
    CompanyID: TIntegerField;
    MemoryDataART2ID: TIntegerField;
    MemoryDataUIDWHID: TIntegerField;
    MemoryDataREQUESTCOMPANY: TIntegerField;
    MemoryDataREQUESTOFFICE: TIntegerField;
    GridViewREQUESTCOMPANY: TcxGridDBBandedColumn;
    GridViewREQUESTOFFICE: TcxGridDBBandedColumn;
    Scripter: TpFIBScripter;
    Transaction: TpFIBTransaction;
    DBPrice: TpFIBDataSet;
    ProviderPrice: TDataSetProvider;
    DataPrice: TClientDataSet;
    DataPriceUIDWHID: TIntegerField;
    DataPricePRICEINV: TFloatField;
    MemoryDatapriceinv: TFloatField;
    MemoryDatacostinv: TFloatField;
    GridViewPriceInv: TcxGridDBBandedColumn;
    GridViewCostniv: TcxGridDBBandedColumn;
    EventAlerter: TSIBfibEventAlerter;
    DBPriceTolling: TpFIBDataSet;
    ProviderPriceTolling: TDataSetProvider;
    DataTolling: TClientDataSet;
    DataTollingUID: TIntegerField;
    ClientDataSet1PRICE: TBCDField;
    DataTollingWEIGHT: TBCDField;
    DataTollingWEIGHTINSERTION: TBCDField;
    DataTollingCOSTMATERIAL: TFloatField;
    DataTollingCOSTLOSSES: TFloatField;
    DataTollingCOSTSERVICE: TFloatField;
    DataTollingCOSTPRODUCT: TFloatField;
    DataTollingCOSTINSERTION: TFloatField;
    MemoryDatapricetolling: TFloatField;
    MemoryDatacosttolling: TFloatField;
    GridViewpricetolling: TcxGridDBBandedColumn;
    GridViewcosttolling: TcxGridDBBandedColumn;
    MemoryDatacprice: TFloatField;
    MemoryDataccost: TFloatField;
    MemoryDataEUID: TStringField;
    GridViewEUID: TcxGridDBBandedColumn;
    BarManager: TdxBarManager;
    MenuGrid: TdxBarPopupMenu;
    ButtonReset: TdxBarLargeButton;
    Image32: TcxImageList;
    procedure GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo;
      var ADone: Boolean);
    procedure MemoryDataCalcFields(DataSet: TDataSet);
    procedure GridViewDataControllerSummaryDefaultGroupSummaryItemsSummary(
      ASender: TcxDataSummaryItems; Arguments: TcxSummaryEventArguments;
      var OutArguments: TcxSummaryEventOutArguments);
    procedure GridViewDataControllerSummaryAfterSummary(
      ASender: TcxDataSummary);
    procedure GridViewDEPStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
    procedure GridViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
    procedure GridViewMARKPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure GridViewMARKPropertiesChange(Sender: TObject);
    procedure MemoryDataBeforePost(DataSet: TDataSet);
    procedure MemoryDataAfterPost(DataSet: TDataSet);
    procedure GridViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridViewStylesGetHeaderStyle(Sender: TcxGridTableView;
      AColumn: TcxGridColumn; out AStyle: TcxStyle);
    procedure GridViewDataControllerFilterChanged(Sender: TObject);
    procedure GridViewMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MemoryDataBeforeOpen(DataSet: TDataSet);
    procedure GridViewGetFilterValues(Sender: TcxCustomGridTableItem;
      AValueList: TcxDataFilterValueList);
    procedure GridViewFocusedItemChanged(Sender: TcxCustomGridTableView;
      APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
    procedure DataPriceBeforeOpen(DataSet: TDataSet);
    procedure GridViewCustomization(Sender: TObject);
    procedure EventAlerterEventAlert(Sender: TObject; EventName: string;
      EventCount: Integer);
    procedure GridViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ButtonResetClick(Sender: TObject);
    procedure GridViewBands0HeaderClick(Sender: TObject);
  private
    Flag: Boolean;
    FID: Integer;
    FUserID: Integer;
    FRangeBegin: TDateTime;
    FRangeEnd: TDateTime;
    Delta: TPacketDataSet;
    FileName: Variant;
    DeltaFileName: Variant;
    StyleArray: array[1..100] of TcxStyle;
    CU_SBD0, CU_SBD1, CU_R, CU_S0, CU_S1, CU_D0, CU_D1,
    CU_RT0, CU_RT1, CU_SL, CU_SO0, CU_SO1, CU_SR0, CU_SR1,
    CU_RO0, CU_RO1, CU_IM0, CU_IM1, CU_AO, CU_AC, CU_SI, CU_RO, CU_RC: TcxStyle;
    function GetAll: Variant;
    function GetSelected: Variant;
    function GetAllUID: Variant;
  protected
    Mutex: THandle;
    procedure Loaded; override;
  public
    procedure Execute;
    procedure FilterAll;
    procedure FilterSell;
    procedure FilterOpenSupply;
    procedure FilterReturn;
    procedure FilterWarehouse;
    procedure FilterOffice;
    procedure LayoutLoad(Name: string);
    procedure LayoutSave(Name: string);
    procedure LayoutDelete(Name: string);
    procedure SetFilter;
    procedure OfficeChanged;
    procedure UpdateRequestCompany(Reset: Boolean = False);
    procedure UpdateRequestOffice(Reset: Boolean = False);
    procedure BeforeDestruction; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property ID: Integer read FID;
    property UserID: Integer read FUserID;
    property All: Variant read GetAll;
    property AllUID: Variant read GetAllUID;
    property Selected: Variant read GetSelected;
    property RangeBegin: TDateTime read FRangeBegin write FRangeBegin;
    property RangeEnd: TDateTime read FRangeEnd write FRangeEnd;
  end;

var

  Warehouse: TWarehouse;

implementation

{$R *.dfm}

uses

  ComData, Data, uUtils, uDialog;

constructor TWarehouse.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Mutex := CreateMutex(nil, False, 'warehouse');

  if Mutex = 0 then
  begin
    RaiseLastOSError;
  end;

end;


destructor TWarehouse.Destroy;
begin
  if EventAlerter.Registered then
  begin
    EventAlerter.UnRegisterEvents;
  end;

  CloseHandle(Mutex);

  inherited Destroy;
end;
  

procedure TWarehouse.MemoryDataCalcFields(DataSet: TDataSet);
var
  s: string;
  uid, mu, t, it: Integer;
begin
  s:='';

  t := MemoryDataT.AsInteger;

  it := MemoryDataITYPE.AsInteger;

  uid := MemoryDataUID.AsInteger;

  mu := MemoryDataUNITID.AsInteger;

  MemoryDataFilter.AsInteger := 0;

  if (T = -1) or (T = 0) or (T = 1) or (T = 5) or (T = 7) or (T = 8) then
  begin
    MemoryDataFilter.AsInteger := 1;
  end else

  if (T = 2) then
  begin
    MemoryDataFilter.AsInteger := 2;
  end else

  if (T = 4) or (T = 5) then
  begin
    MemoryDataFilter.AsInteger := 3;
  end else

  if (T = -2) then
  begin
    MemoryDataFilter.AsInteger := 4;
  end;

  s := '';

  with DataSet do


  case T of
      0: case MemoryDataIType.AsInteger of
             2: if  MemoryDataSDATETIME0.AsDateTime<=dm.BeginDate then s:='������ �� ������ �������'
                else  s:='������ �� �����������';
             5: s:='�������';
             9: case MemoryDataIsClosed.AsInteger of
                 0: s:='������, �������� ���������';
                 1: s:='������, ��������� �������';
                end;
             10: s:='���������� �������';
             21: s:='�������';
             else  case MemoryDataIsClosed.AsInteger of
                       0: s:='������ �� ������ �������, �������� ���������';
                       1: s:='������ �� ������ �������';
                     end
           end;

      1,-1: begin
           case MemoryDataIType.AsInteger of
               1: s:='������ �� �����������';
                //���� ���� �����.����������� � ��� ���� ��������� � ����� �������, �� ��� ��� - ����� ��� ������
               2: if (MemoryDataSdate.IsNull or (MemoryDataSdate.AsDateTime<=dm.BeginDate))
                  then s:='������ �� ������ �������'
                  else s:='���������� �����������';

               6:
                  begin
                    s:='������� �� �����������';
                  end;
               8: s:='������� �� ������� �����������';
               9: case MemoryDataIsClosed.AsInteger of
                   0: s:='������, �������� ���������';
                   1: s:='������, ��������� �������';
                  end;
               10: s:='���������� �������';
               21: s:='�������';
             end;

           case MemoryDataIsClosed.AsInteger of
               0: s:=s+', �������� ���������';
             end;
         end;
      2: case MemoryDataIsCLosed.AsInteger of
      //��� ������ ��������� ����� � ������� �� �������� � ����������� ������
             0: s:='�������, �������� �����';

             1,2: s:='�������';
           end;
      3: case MemoryDataIsClosed.AsInteger of
            0: s:='������� �������, �������� ���������';

            1: s:='������� �������';
          end;
      4: case MemoryDataIsClosed.AsInteger of
            1: s:='������� �����������';
          end;
      5: case MemoryDataIsClosed.AsInteger of
            0: s:='������� �����������, �������� ���������';
          end;
      6: s:='��� ��������';

      7: s:='��� ��������, �������� ���������';

      8: s:='������� �������, �������� ���������';

      -2: s:='������ �� �����������, �������� ���������';
    end;

  MemoryDataMEMO.AsString := s;

  MemoryDataFLAGNAME.AsString := FlagName[MemoryDataFLAG.AsInteger];

  MemoryDataUNITNAME.AsString := UnitName[mu];

  if NDS.FindKey([MemoryDataNDSID.AsInteger]) then
  begin
    if NDSNAME.AsString <> '0%' then
    begin
      MemoryDataNDSNAME.AsString := NDSNAME.AsString;
    end;
  end else
  begin
    MemoryDataNDSNAME.AsString := '';
  end;

  if PayType.FindKey([MemoryDataPAYTYPEID.AsInteger]) then
  begin
    MemoryDataPAYTYPENAME.AsString := PayTypeNAME.AsString;
  end else
  begin
    MemoryDataPAYTYPENAME.AsString := '';
  end;

  if not MemoryDataSDATETIME0.IsNull then
  begin
    MemoryDataSDATE0.AsDateTime := DateOf(MemoryDataSDATETIME0.AsDateTime);
  end;

  if Company.FindKey([MemoryDataPRODCODE.AsString]) then
  begin
    MemoryDataPRODNAME.AsString := CompanySHORTNAME.AsString;
  end;

  if Company.FindKey([MemoryDataSUPCODE.AsString]) then
  begin
    MemoryDataSUPNAME.AsString := CompanySHORTNAME.AsString;
  end;

  if DataPrice.Active then
  begin
    if DataPrice.FindKey([MemoryDataUIDWHID.AsInteger]) then
    begin
      MemoryDatapriceinv.AsFloat := DataPricePRICEINV.AsFloat;

      if MemoryDataUNITID.AsInteger = 0 then
      begin
        MemoryDatacostinv.AsFloat := DataPricePRICEINV.AsFloat;
      end else
      begin
        MemoryDatacostinv.AsFloat := DataPricePRICEINV.AsFloat * MemoryDataW.AsFloat;
      end;
    end;
  end;
  //{
  if MemoryDataFLAG.AsInteger =2 then
  begin

  if DataTolling.FindKey([uid]) then
  begin
    MemoryDatapricetolling.AsFloat := MemoryDataSPRICE0.AsFloat;
            
    MemoryDatacosttolling.AsFloat := MemoryDataCOST1.AsFloat;

    MemoryDataccost.AsFloat := DataTollingCOSTMATERIAL.AsFloat + DataTollingCOSTLOSSES.AsFloat + DataTollingCOSTSERVICE.AsFloat;

    if mu = 0 then
    begin
     MemoryDatacprice.AsFloat := MemoryDataccost.AsFloat;
    end else
    begin
      MemoryDatacprice.AsFloat := MemoryDataccost.AsFloat / DataTollingWEIGHT.AsFloat;
    end;
  end;

  end else
  if MemoryDataFLAG.AsInteger = 1 then
  begin
    MemoryDatacprice.AsFloat := MemoryDataSPRICE0.AsFloat;

    MemoryDataccost.AsFloat := MemoryDataCOST1.AsFloat;
  end;

  if MemoryDataFLAG.AsInteger = 0 then
  begin
    MemoryDatacprice.AsFloat := MemoryDataSPRICE0.AsFloat;

    MemoryDataccost.AsFloat := MemoryDataCOST1.AsFloat;
  end;
  //}
  {
    MemoryDatacprice.AsFloat := MemoryDataSPRICE0.AsFloat;

    MemoryDataccost.AsFloat := MemoryDataCOST1.AsFloat;
  }
end;


procedure TWarehouse.BeforeDestruction;
begin
  inherited;

  LayoutDelete('warehouse');

  LayoutSave('warehouse');
end;

procedure TWarehouse.Execute;

type

  TRefreshMode = (rmNone, rmCache, rmDatabase);

var
  Color: Integer;
  Style: TcxStyle;
  Value: Variant;
  Catalog: string;
  IniFile: TIniFile;
  IniFileName: Variant;
  StoredDateTime: TDateTime;
  StoredDateTimeBegin: TDateTime;
  StoredDateTimeEnd: TDateTime;
  TimeStamp: string;
  TimeStampBegin: string;
  TimeStampEnd: string;
  RefreshMode: TRefreshMode;
  DateTime: TDateTime;
  DateTimeBegin: TDateTime;
  DateTimeEnd: TDateTime;
  sss: string;

function CreateStyle(Color: TColor): TcxStyle;
begin
  Result := TcxStyle.Create(Self);

  Result.Color := Color;
end;

procedure SetOffice;
var
  i: Integer;
begin
  if CenterDep then
  begin
    dm.SDepId := dm.pmWh.Items[0].Tag;

    dm.SDep :=  dm.pmWh.Items[0].Caption;
  end
  else
  begin
   for i:= 0 to dm.pmWh.Items.Count-1 do
   begin
     if dm.pmWh.items[i].Tag=SelfDepId then
     begin
        dm.SDepId := dm.pmWh.Items[i].Tag;

        dm.SDep :=  dm.pmWh.Items[i].Caption;

        Exit;
     end;
   end;
  end;
end;

procedure BeforeRefresh;
begin
  Screen.Cursor := crHourGlass;

  GridView.DataController.DataSource := nil;

  GridView.OptionsView.NoDataToDisplayInfoText := '���������� ������';

  Application.ProcessMessages;

  MemoryData.Active := False;

  Company.Active := False;

  Office.Active := False;

  Material.Active := False;

  Good.Active := False;

  Country.Active := False;

  Insertion.Active := False;

  NDS.Active := False;

  PayType.Active := False;

  Company.Active := True;

  Office.Active := True;

  Material.Active := True;

  Insertion.Active := True;

  Country.Active := True;

  Good.Active := True;  

  NDS.Active := True;

    PayType.Active := True;

  Office.First;

  while not Office.Eof do
  begin
    if StyleArray[OfficeID.AsInteger] = nil then
    begin
      Color := OfficeCOLOR.AsInteger;

      Style := TcxStyle.Create(Self);

      Style.Color := Color;

      StyleArray[OfficeID.AsInteger] := Style;
    end;

    Office.Next;
  end;
end;

procedure AfterRefresh;
begin
  if Gridviewpriceinv.Visible then
  begin
    DataPrice.Active := True;
  end;

  //if GridViewpricetolling.Visible then
  begin
    DataTolling.Active := True;
  end;

  MemoryData.OnCalcFields := MemoryDataCalcFields;

  GridView.DataController.BeginFullUpdate;

  GridView.DataController.DataSource := DataSource;

  GridView.DataController.EndFullUpdate;

  GridView.ViewData.Collapse(True);

  GridView.Controller.TopRowIndex := 0;

  GridView.Controller.FocusedRowIndex := 0;

  GridView.OptionsView.NoDataToDisplayInfoText := '...';

  MemoryData.IndexName := 'uid';

  Screen.Cursor := crDefault;
end;

procedure ApplyDelta;
var
  i: Integer;
  UID: Variant;
  Value: Variant;
  MarkField, UIDField, Source, Target: TField;
  Flag: Boolean;
begin
  Delta.First;

  UIDField := Delta.FieldByName('UID');

  MarkField := Delta.FieldByName('Mark');

  while not Delta.Eof do
  begin
    Delta.InitAltRecBuffers(False);

   if Delta.UpdateStatus = usUnmodified then
   begin
     Delta.InitAltRecBuffers(True);

     case Delta.UpdateStatus of

       usModified:
         begin

           UID := UIDField.OldValue;

           if MemoryData.FindKey([UID]) then
           begin
             Flag := False;

             MemoryData.Edit;

             for i := 0 to Delta.Fields.Count - 1 do
             begin
               Source := Delta.Fields[i];

               if (Source <> UIDField) and (Source <> MarkField) then
               begin
                 Value := Source.NewValue;

                 if not VarIsEmpty(Value) then
                 begin
                   Target := MemoryData.FieldByName(Source.FieldName);

                   if Target <> nil then
                   begin
                     Flag := True;

                     Target.Value := Value;
                   end;
                 end;
               end;
             end;

             if Flag then
             begin
               MemoryData.Post;
             end else
             begin
               MemoryData.Cancel;
             end;
           end;
         end;

       usInserted:
         begin
         end;

       usDeleted:
         begin
         end;
     end;

   end;

   Delta.Next;
  end;
end;

begin
  WaitForSingleObject(Mutex, INFINITE);

  try

  FUserID := dmCom.UserId;

  if dm.UIDWHType = 3 then
  begin
    FID := UserDefault;
  end else
  begin
    FID := dmCom.UserID;
  end;

  if not Flag then
  begin
    GridViewPAYTYPENAME.Visible := CenterDep;

    GridViewSPRICE0.Visible := CenterDep;

    GridViewCOST1.Visible := CenterDep;

    GridView.Bands[0].Visible := CenterDep;

    CU_SBD0 := CreateStyle(dm. CU_SBD0);
    CU_SBD1 := CreateStyle(dm.CU_SBD1);
    CU_R := CreateStyle(dm. CU_R);
    CU_S0 := CreateStyle(dm. CU_S0);
    CU_S1 := CreateStyle(dm. CU_S1);
    CU_D0 := CreateStyle(dm. CU_D0);
    CU_D1 := CreateStyle(dm. CU_D1);
    CU_RT0 := CreateStyle(dm. CU_RT0);
    CU_RT1 := CreateStyle(dm. CU_RT1);
    CU_SL := CreateStyle(dm. CU_SL);
    CU_SO0 := CreateStyle(dm. CU_SO0);
    CU_SO1 := CreateStyle(dm. CU_SO1);
    CU_SR0 := CreateStyle(dm. CU_SR0);
    CU_SR1 := CreateStyle(dm. CU_SR1);
    CU_RO0 := CreateStyle(dm. CU_RO0);
    CU_RO1 := CreateStyle(dm. CU_RO1);
    CU_IM0 := CreateStyle(dm. CU_IM0);
    CU_IM1 := CreateStyle(dm. CU_IM1);
    CU_AO := CreateStyle(dm. CU_AO);
    CU_AC := CreateStyle(dm. CU_AC);
    CU_SI := CreateStyle(dm. CU_SI);
    CU_RO := CreateStyle(dm. CU_RO);
    CU_RC := CreateStyle(dm. CU_RC);

    Delta := TPacketDataSet.Create(Self);

    Flag := True;
  end;

  Catalog := GetSpecialFolderLocation(CSIDL_APPDATA);

  Catalog := Catalog + '\Jew';

  if not DirectoryExists(Catalog) then
  begin
    CreateDir(Catalog);
  end;

  Catalog := Catalog + '\Warehouse\';

  if not DirectoryExists(Catalog) then
  begin
    CreateDir(Catalog);
  end;

  RefreshMode := rmNone;

  IniFile := nil;

  if ID = UserDefault then
  begin
    IniFileName := Catalog + 'Warehouse.ini';

    IniFile := TIniFile.Create(IniFileName);

    FileName := Catalog + 'Warehouse.cds';

    DeltaFileName := Catalog + 'Warehouse.Delta.cds';

    TimeStamp := IniFile.ReadString('TimeStamp', 'Current', '');

    DateTime := dmCom.db.QueryValue('SELECT UIDWHDATE FROM D_REC', 0);    

    if TimeStamp <> '' then
    begin
      if TimeStamp = DateTimeToStr(DateTime) then
      begin
        if FileExists(FileName) then
        begin
          RefreshMode := rmCache;
        end else
        begin
          RefreshMode := rmDatabase;
        end;
      end else
      begin
        RefreshMode := rmDatabase;
      end;
    end else
    begin
      RefreshMode := rmDatabase;
    end;

    TimeStamp := DateTimeToStr(DateTime);
  end else
  begin
    Catalog := Catalog + IntToHex(ID, 8) + '\';

    if not DirectoryExists(Catalog) then
    begin
      CreateDir(Catalog);
    end;

    IniFileName := Catalog + 'Warehouse.ini';

    IniFile := TIniFile.Create(IniFileName);

    FileName := Catalog + 'Warehouse.cds';

    DeltaFileName := Catalog + 'Warehouse.Delta.cds';

    TimeStampBegin := IniFile.ReadString('TimeStamp', 'Begin', '');

    TimeStampEnd := IniFile.ReadString('TimeStamp', 'End', '');

    DateTimeBegin := dm.UIDWHBD;

    DateTimeEnd := dm.UIDWHED;

    if (TimeStampBegin <> '') and (TimeStampEnd <> '') then
    begin
      if (TimeStampBegin = DateTimeToStr(DateTimeBegin)) and (TimeStampEnd =  DateTimeToStr(DateTimeEnd)) then
      begin
        if FileExists(FileName) then
        begin
          RefreshMode := rmCache;
        end else
        begin
          RefreshMode := rmDatabase;
        end;
      end else
      begin
        RefreshMode := rmDatabase;
      end;
    end else
    begin
      RefreshMode := rmDatabase;
    end;

    TimeStampBegin := DateTimeToStr(DateTimeBegin);

    TimeStampEnd := DateTimeToStr(DateTimeEnd);
  end;

  if RefreshMode = rmCache then
  begin
    BeforeRefresh;

    MemoryData.LoadFromFile(FileName);

    if FileExists(DeltaFileName) then
    begin
      Delta.LoadFromFile(DeltaFileName);

      ApplyDelta;
    end;

    AfterRefresh;
  end else

   if RefreshMode = rmDatabase then
  begin
    if FileExists(FileName) then
    begin
      DeleteFile(FileName);
    end;

    if FileExists(DeltaFileName) then
    begin
      //DeleteFile(DeltaFileName);
    end;

    BeforeRefresh;

    MemoryData.Active := True;

    MemoryData.LogChanges := False;

    MemoryData.SaveToFile(FileName);

    MemoryData.LogChanges := True;

    if ID = UserDefault then
    begin
      IniFile.WriteString('TimeStamp', 'Current', TimeStamp);
    end else
    begin
      IniFile.WriteString('TimeStamp', 'Begin', TimeStampBegin);

      IniFile.WriteString('TimeStamp', 'End', TimeStampEnd);
    end;

    AfterRefresh;

  end;

  IniFile.Free;

  SetFilter;

  SetOffice;

  if not EventAlerter.Registered then
  begin
    EventAlerter.RegisterEvents;
  end;

  finally
    ReleaseMutex(Mutex);
  end;
end;


procedure TWarehouse.GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

var
  Summary: TStringList;
  s: string;

procedure TWarehouse.GridViewDataControllerSummaryDefaultGroupSummaryItemsSummary(ASender: TcxDataSummaryItems; Arguments: TcxSummaryEventArguments; var OutArguments: TcxSummaryEventOutArguments);
begin

  if Arguments.SummaryItem.Tag = 0 then
  begin

  end;

  if Arguments.SummaryItem.Tag = 1 then
  begin
    if VarIsEmpty(OutArguments.SummaryValue) then
    begin
      if Summary = nil then
      begin
        Summary := TStringList.Create;

        Summary.Duplicates := dupIgnore;

        Summary.Sorted := True;

        Summary.Capacity
      end else
      begin
        Summary.Clear;
      end;
    end;

    if Summary <> nil then
    begin
      Summary.Add(OutArguments.Value);

      OutArguments.SummaryValue := Summary.Count;

      OutArguments.CountValue := Summary.Count;

      OutArguments.Done := True;
    end;
  end else
  begin
    OutArguments.Done := False;
  end;
end;

procedure TWarehouse.GridViewDataControllerFilterChanged(Sender: TObject);
begin
  GridView.Controller.TopRowIndex := 0;

  GridView.Controller.FocusedRowIndex := 0;

  SetFilter;
end;

procedure TWarehouse.GridViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
begin
  if Summary <> nil then
  begin
    FreeAndNil(Summary);
  end;
end;

procedure TWarehouse.GridViewDEPStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  Value: Variant;
  Style: TcxStyle;
  OfficeID: Integer;
begin
  Index := GridViewDEPID.Index;

  Value := ARecord.Values[Index];

  if not VarIsNull(Value) then
  begin
    OfficeID := Value;

    Style := StyleArray[OfficeID];

    if Style <> nil then
    begin
      AStyle := Style;
    end;
  end;
end;


procedure TWarehouse.GridViewFocusedItemChanged(Sender: TcxCustomGridTableView;
  APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
begin
  GridView.LayoutChanged;
end;

procedure TWarehouse.GridViewMARKPropertiesChange(Sender: TObject);
begin
  if MemoryData.State = dsEdit then
  begin
    MemoryData.Post;
  end;
end;

procedure TWarehouse.GridViewMARKPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  DisplayValue := DisplayValue;
end;


procedure TWarehouse.GridViewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AHitTest: TcxCustomGridHitTest;
begin
  AHitTest := TcxGridSite(Sender).ViewInfo.GetHitTest(X, Y);

  if AHitTest.HitTestCode = htFilterCloseButton then
  begin
    AHitTest := AHitTest;
  end;
end;

procedure TWarehouse.GridViewStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  T: Variant;
  IType: Variant;
  IsClosed: Variant;
  Sdate0: Variant;
  DepartmentID: Integer;
begin
 
  if AItem = nil then
  begin
    Exit;
  end;

  if ARecord.Index = -1 then
  begin
    Exit;
  end;

  if AItem.Tag <> 0 then
  begin
    if AItem.Styles.Content <> nil then
    begin
      AStyle := AItem.Styles.Content;
    end;

    Exit;
  end;

  T := ARecord.Values[GridViewT.Index];

  if VarIsNull(T) then
  begin
    Exit;
  end;

  IType := ARecord.Values[GridViewITYPE.Index];

  if VarIsNull(IType) then
  begin
    Exit;
  end;

  IsClosed := ARecord.Values[GridViewISCLOSED.Index];

  if VarIsNull(IsClosed) then
  begin
    Exit;
  end;


  Sdate0 := ARecord.Values[GridViewSDATE0.Index];

  if VarIsNull(Sdate0) then
  begin
    Exit;
  end;

  DepartmentID := ARecord.Values[GridViewDEPID.Index];

  if AItem.Tag = 0 then
  begin

  case T of

    0:
      case IType of

            2:
            begin
              if IsClosed=0 then
              begin
                AStyle := CU_IM0;
              end else
              begin
                if  Sdate0 < dm.BeginDate then
                begin
                  AStyle := CU_SBD1;
                end else
                begin
                  AStyle := CU_S1;
                end;
              end;
            end;

           5:
           begin
             if IsClosed = 0 then
              begin
                AStyle := CU_SBD0;
              end else
              begin
                AStyle := CU_SBD1;
              end;
           end;

           9:
           begin
             if IsClosed = 0 then
              begin
                AStyle := CU_RO;
              end else
              begin
                AStyle := CU_RC;
              end;
           end;

           10:
           begin
             AStyle := CU_SI;
           end;

           else
           begin
             AStyle := CU_R;
           end;
      end;

    -2, -1, 1:
       case IType of

          1:
          begin
            case IsClosed of
                0: AStyle := CU_S0;
                1: AStyle := CU_S1;
            end;
          end;

          2:
          begin
            if IsClosed=0 then
            begin
              AStyle := CU_IM0;
            end else
            if  Sdate0 < dm.BeginDate then
            begin
              AStyle := CU_SBD1;
            end
            else
            begin
              AStyle := CU_S1;
            end;
          end;

          6: case IsClosed of
              0: AStyle := CU_RT0;
              1: AStyle := CU_RT1;
             end;

          8: case IsClosed of
              0: AStyle := CU_RO0;
              1: AStyle := CU_RO1;
             end;

          9: case IsClosed of
               0: AStyle := CU_RO;
               1: AStyle := CU_RC;
             end;

          10: AStyle := CU_SI;
       end;


    2:
      case IsClosed of
         0,1: AStyle :=  StyleArray[DepartmentID];
         2: AStyle := CU_SL;
       end;

    3: AStyle := CU_SO1;
    4: AStyle := CU_SR1;
    5: AStyle := CU_SR0;
    6: AStyle := CU_AC;
    7: AStyle := CU_AO;
    8: AStyle := CU_SO0;
       end;
  end;
end;

procedure TWarehouse.MemoryDataBeforeOpen(DataSet: TDataSet);
begin
  MemoryData.Params.ParamByName('UserID').AsInteger := ID;
end;

procedure TWarehouse.MemoryDataBeforePost(DataSet: TDataSet);
begin
  if MemoryDataREQUESTCOMPANY.AsInteger = 0 then
  begin
    MemoryDataREQUESTCOMPANY.AsVariant := Null;
  end;

  if MemoryDataREQUESTOFFICE.AsInteger = 0 then
  begin
    MemoryDataREQUESTOFFICE.AsVariant := Null;
  end;

  TcxGridDBDataController(GridView.DataController).NotifyFlag := False;
end;

procedure TWarehouse.MemoryDataAfterPost(DataSet: TDataSet);
begin
  TcxGridDBDataController(GridView.DataController).NotifyFlag := True;

  if GridView.DataController.DataSource <> nil then
  begin
    if MemoryData.ChangeCount <> 0 then
    begin
      Delta.Data := MemoryData.Delta;

      Delta.SaveToFile(DeltaFileName);
    end else
    begin
      DeleteFile(DeltaFileName);
    end;
  end;
end;

procedure TWarehouse.GridViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  Item: TcxCustomGridTableItem;
begin
  if Key = 32 then
  begin
    Item := GridView.DataController.Controller.FocusedItem;

    if  GridViewMARK.Focused or ((Item <> nil) and (Item.Options.Editing = False)) then
    begin
      if MemoryData.State = dsBrowse then
      begin
        MemoryData.Edit;
      end;

      if MemoryDataMARK.AsInteger = 0 then
      begin
        MemoryDataMARK.AsInteger := 1;
      end else
      begin
        MemoryDataMARK.AsInteger := 0;
      end;

      MemoryData.Post;
    end;
  end;
end;

procedure TWarehouse.GridViewStylesGetHeaderStyle(Sender: TcxGridTableView; AColumn: TcxGridColumn; out AStyle: TcxStyle);
begin
if (AColumn <> nil) and (Sender.Controller.FocusedColumn = AColumn) then
    AStyle := StyleSelectedHeader;
end;

{ TpFIBClientDataSet }

function TpFIBClientDataSet.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  Result := FindKey([KeyValues]);
end;

{ TClientDataSet }

function TClientDataSet.Locate(const KeyFields: string; const KeyValues: Variant; Options: TLocateOptions): Boolean;
begin
  if AnsiLowerCase(IndexName) = AnsiLowerCase(KeyFields) then
  begin
    Result := FindKey([KeyValues]);
  end else
  begin
    Result := inherited Locate(KeyFields, KeyValues, []);
  end;
end;


function TWarehouse.GetAll: Variant;
var
  i: Integer;
  ID: Integer;
  Index: Integer;
  RecordIndex: Integer;
  ColumnIndex: Integer;
  MarkArray: Variant;
  MarkCount: Integer;
begin
  MarkCount := GridView.DataController.FilteredRecordCount;

  if MarkCount  <> 0 then
  begin
    i := 0;

    MarkArray := VarArrayCreate([0, MarkCount - 1], varInteger);

    ColumnIndex := GridViewSITEMID.Index;

    for Index := 0 to GridView.DataController.FilteredRecordCount - 1 do
    begin
      RecordIndex := GridView.DataController.FilteredRecordIndex[Index];

      ID := GridView.DataController.Values[RecordIndex, ColumnIndex];

      MarkArray[i] := ID;

      Inc(i);
    end;
  end;

  Result := MarkArray;
end;

function TWarehouse.GetAllUID: Variant;
var
  i: Integer;
  ID: Integer;
  Index: Integer;
  RecordIndex: Integer;
  ColumnIndex: Integer;
  MarkArray: Variant;
  MarkCount: Integer;
  T: Integer;
  TIndex: Integer;
begin
  MarkCount := GridView.DataController.FilteredRecordCount;

  if MarkCount  <> 0 then
  begin
    i := 0;

    MarkArray := VarArrayCreate([0, MarkCount - 1], varInteger);

    TIndex := GridViewT.Index;

    ColumnIndex := GridViewUID.Index;

    for Index := 0 to GridView.DataController.FilteredRecordCount - 1 do
    begin
      RecordIndex := GridView.DataController.FilteredRecordIndex[Index];

      ID := GridView.DataController.Values[RecordIndex, ColumnIndex];

      T := GridView.DataController.Values[RecordIndex, TIndex];

      if T in [2, 3, 4, 6] then
      begin
        ID := -ID;
      end;

      MarkArray[i] := ID;

      Inc(i);
    end;
  end;

  Result := MarkArray;
end;


function TWarehouse.GetSelected: Variant;
var
  ID, i: Integer;
  IDField: TField;
  MarkData: TClientDataSet;
  MarkArray: Variant;
begin
  MarkData := TClientDataSet.Create(Self);

  MarkData.CloneCursor(Warehouse.MemoryData, True, False);

  MarkData.IndexFieldNames := 'Mark;UID';

  MarkData.SetRange([1], [1]);

  MarkData.First;

  i := 0;

  if MarkData.RecordCount <> 0 then
  begin
    IDField := MarkData.FieldByName('sitemid');

    MarkArray := VarArrayCreate([0, MarkData.RecordCount - 1], varInteger);

    while not MarkData.Eof do
    begin
      ID := IDField.AsInteger;

      MarkArray[i] := ID;

      Inc(i);

      MarkData.Next;
    end;
  end else

  begin
    IDField := Warehouse.MemoryDataSITEMID;

    ID := IDField.AsInteger;

    MarkArray := VarArrayOf([ID]);
  end;

 MarkData.Free;

 Result := MarkArray;
end;

procedure TWarehouse.FilterOffice;
var
  OfficeID: Integer;
  OfficeName: string;
begin
  GridViewDepID.Filtered := False;

  OfficeID := dm.SDepId;

  OfficeName := dm.SDep;

  if OfficeID <> -1 then
  begin
    GridView.DataController.Filter.Root.InsertItem(GridViewDepID, foEqual, OfficeID, OfficeName, 0);
  end;
end;

procedure TWarehouse.FilterAll;
begin
  if GridViewFilter.Filtered then
  begin
    GridView.DataController.Filter.BeginUpdate;

    GridView.DataController.Filter.Active := False;

    GridViewFilter.Filtered := False;

    GridViewMemo.Filtered := False;

    FilterOffice;

    if GridView.DataController.Filter.Root.Count <> 0 then
    begin
       GridView.DataController.Filter.Active := True;
    end;

    GridView.DataController.Filter.EndUpdate;
  end;
end;

procedure TWarehouse.FilterWarehouse;
begin
  GridView.DataController.Filter.BeginUpdate;

  GridView.DataController.Filter.Active := False;

  GridViewFilter.Filtered := False;

  GridViewMemo.Filtered := False;

  FilterOffice;

  GridView.DataController.Filter.Root.InsertItem(GridViewFilter, foEqual, '1', '�� ������', 0);

  GridView.DataController.Filter.Active := True;

  GridView.DataController.Filter.EndUpdate;
end;

procedure TWarehouse.FilterSell;
begin
  GridView.DataController.Filter.BeginUpdate;

  GridView.DataController.Filter.Active := False;

  GridViewFilter.Filtered := False;

  GridViewMemo.Filtered := False;

  FilterOffice;

  GridView.DataController.Filter.Root.InsertItem(GridViewFilter, foEqual, '2', '�������', 0);

  GridView.DataController.Filter.Active := True;

  GridView.DataController.Filter.EndUpdate;
end;

procedure TWarehouse.FilterReturn;
begin
  GridView.DataController.Filter.BeginUpdate;

  GridView.DataController.Filter.Active := False;

  GridViewFilter.Filtered := False;

  GridViewMemo.Filtered := False;

  FilterOffice;

  //GridView.DataController.Filter.Root.InsertItem(GridViewFilter, foEqual, '3', '������� �����������', 0);

  GridView.DataController.Filter.Root.InsertItem(GridViewMemo, foLike, '������� �����������%', '������� �����������', 0);

  GridView.DataController.Filter.Active := True;

  GridView.DataController.Filter.EndUpdate;
end;

procedure TWarehouse.FilterOpenSupply;
begin
  GridView.DataController.Filter.Active := False;

  GridView.DataController.Filter.BeginUpdate;

  GridViewFilter.Filtered := False;

  GridViewMemo.Filtered := False;  

  FilterOffice;

  GridView.DataController.Filter.Root.InsertItem(GridViewFilter, foEqual, '4', '�������� ��������', 0);

  GridView.DataController.Filter.Active := True;

  GridView.DataController.Filter.EndUpdate;
end;

{ TcxGridBandedTableController }

procedure TcxGridBandedTableController.MouseDown(Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  GridView.Controller.ItemsCustomizationPopup.OnClosed := GridView.OnCustomization;

  inherited MouseDown(Button, Shift, X, Y);
end;

procedure TcxGridBandedTableController.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  i: Integer;
  f: Boolean;
  AHitTest: TcxCustomGridHitTest;
  Target: TcxDataFilterCriteria;
  TargetRoot: TcxFilterCriteriaItemList;
  TargetItem: TcxDataFilterCriteriaItem;
  Column: TcxGridDBBandedColumn;
begin
  AHitTest := GridView.Site.ViewInfo.GetHitTest(X, Y);

  if AHitTest.HitTestCode = htFilterCloseButton then
  begin
    Target := GridView.DataController.Filter;

    Target.BeginUpdate;

    TargetRoot := Target.Root;

    for i := TargetRoot.Count - 1 downto 0 do
    begin
       TargetItem := TcxDataFilterCriteriaItem(TargetRoot.Items[i]);

       f := True;

       if not TargetItem.IsItemList then
       begin
          Column := TcxGridDBBandedColumn(TargetItem.ItemLink);

          if (Column.AlternateCaption = '������') or (Column.AlternateCaption = '�����') then
          begin
            f := False;
          end;
       end;

       if f then
       begin
         TargetItem.Free;
       end;
    end;

    Target.EndUpdate;
  end else
  begin
    inherited MouseUp(Button, Shift, X, Y);
  end;
end;

{ TcxGridDBBandedTableView }

function TcxGridDBBandedTableView.GetControllerClass: TcxCustomGridControllerClass;
begin
  Result := TcxGridBandedTableController;
end;

function TcxGridDBBandedTableView.GetDataControllerClass: TcxCustomDataControllerClass;
begin
  Result := TcxGridDBDataController;
end;

{ TcxGridDBDataController }

constructor TcxGridDBDataController.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  NotifyFlag := True;
end;

procedure TcxGridDBDataController.FilterChanged;
begin
  if NotifyFlag then
  begin
    inherited FilterChanged;
  end;
end;

procedure TcxGridDBDataController.SummaryChanged(ARedrawOnly: Boolean);
begin{
  if NotifyFlag then
  begin
    inherited SummaryChanged(ARedrawOnly);
  end;}
  inherited SummaryChanged(ARedrawOnly);  
end;

procedure TWarehouse.SetFilter;
var
  i: Integer;
  v: Variant;
begin
  dmCom.D_CompId := -1;

  dmCom.D_SupId := -1;

  dmCom.D_MatId := '*';

  dmCom.D_GoodId := '*';

  dmCom.D_Note1 := -1;

  dmCom.D_Note2 := -1;

  dmCom.D_Att1ID := -2;

  dmCom.D_Att2ID := -2;

  dmCom.UIDWHSZ_ := '*';

  dmCom.D_InsId := '*';

  dmCom.FilterArt := '';

  for i := 0 to GridView.ViewData.FilterRow.ValueCount - 1 do
  begin
    v := GridView.ViewData.FilterRow.Values[i];

    if not VarIsNull(v) then
    begin

        if GridView.Columns[i] = GridViewPRODNAME then
          begin
            if Company.Locate('SHORT$NAME', v, []) then
            begin
              dmCom.D_CompId := CompanyID.AsInteger;
            end;
          end;

        if GridView.Columns[i] = GridViewSUPNAME then
          begin
            if Company.Locate('SHORT$NAME', v, []) then
            begin
              dmCom.D_SupId := CompanyID.AsInteger;
            end;
          end;

        if GridView.Columns[i] = GridViewDEP then
          begin
            if Office.Locate('NAME', v, []) then
            begin
              dm.SDepId := OfficeID.AsInteger;

              dm.SDep := OfficeNAME.AsString;
            end;
          end;

        if GridView.Columns[i] = GridViewNAMEGOOD then
          begin
            if Good.FindKey([v]) then
            begin
              dmCom.D_GoodId := GoodID.AsString;
            end;
          end;

        if GridView.Columns[i] = GridViewGOODS1 then
          begin

          end;

        if GridView.Columns[i] = GridViewGOODS2 then
          begin

          end;

        if GridView.Columns[i] = GridViewART then
          begin
            dmCom.FilterArt  := v;
          end;

        if GridView.Columns[i] = GridViewMATID then
          begin
            dmCom.D_MatId  := v;
          end;

        if GridView.Columns[i] = GridViewINSID then
          begin
            dmCom.D_InsId  := v;
          end;

        if GridView.Columns[i] = GridViewSZ then
          begin
            dmCom.UIDWHSZ_ := v;
          end;
    end;
  end;

end;

procedure TWarehouse.EventAlerterEventAlert(Sender: TObject; EventName: string; EventCount: Integer);
begin
  if EventName = 'warehouse.begin' then
  begin
    GridView.OptionsView.NoDataToDisplayInfoText := '�������� ������';

    MemoryData.Active := False;
  end else
  
  if EventName = 'warehouse.end' then
  begin
    GridView.OptionsView.NoDataToDisplayInfoText := '...';

    Execute;
  end;
end;

procedure TWarehouse.GridViewGetFilterValues(Sender: TcxCustomGridTableItem; AValueList: TcxDataFilterValueList);
var
  i: Integer;
  v: Variant;
  DisplayText: string;
  ValueList: TcxDataFilterValueList;
begin
  ValueList := TcxDataFilterValueList.Create(nil);

  ValueList.SortByDisplayText := True;

  for i := 0 to AValueList.Count - 1 do
  begin
    if AValueList.Items[i].Kind = fviValue then
    begin
      v := AValueList.Items[i].Value;

      DisplayText := v;

      if Sender = GridViewMATID then
      begin
        if Material.FindKey([v]) then
        begin
          DisplayText := MaterialNAME.AsString;
        end;
      end else

      if Sender = GridViewINSID then
      begin
        if Insertion.FindKey([v]) then
        begin
          DisplayText := InsertionNAME.AsString;
        end;
      end;

      if Sender = GridViewD_COUNTRYID then
      begin
        if Country.FindKey([v]) then
        begin
          DisplayText := CountryNAME.AsString;
        end;
      end;

      ValueList.Add(fviValue, v, DisplayText, True);
    end;
  end;

  AValueList.SortByDisplayText  := True;

  for i := AValueList.Count - 1 downto 0 do
  begin
    if AValueList.Items[i].Kind = fviValue then
    begin
      AValueList.Delete(i);
    end;
  end;

  for i := 0 to ValueList.Count - 1 do
  begin
    v := ValueList.Items[i].Value;

    DisplayText := ValueList.Items[i].DisplayText;

    AValueList.Add(fviValue, v, DisplayText, False);
  end;

  ValueList.Free;
end;

procedure TWarehouse.UpdateRequestCompany(Reset: Boolean = False);
var
  uid: Integer;
  Request: Integer;
  Clone: TClientDataSet;
  Flag: Boolean;
  SQL: string;
  Bookmark: TBookmark;
begin
  Scripter.Script.Clear;

  Clone := TClientDataSet.Create(nil);

  Clone.CloneCursor(MemoryData, True, False);

  Clone.AddIndex('request$company', 'request$company',[]);

  Clone.IndexName := 'request$company';

  Clone.SetRange([1], [MaxInt]);

  Clone.First;

  if Reset then
  begin
    MemoryData.AfterPost := nil;

    GridView.DataController.BeginLocate;

    Bookmark := MemoryData.GetBookmark;

    while Clone.RecordCount <> 0  do
    begin
      uid := Clone.FieldByName('uid').AsInteger;

      Request := 0;

      sql := 'update uidwh_t set APPL_Q = null, ' + ' appl_user = null where uid =  ' + IntToStr(uid) + ' and userid = ' + IntToStr(ID) + ';';

      Scripter.Script.Add(SQL);

      if MemoryData.FindKey([uid]) then
      begin
        MemoryData.Edit;

        MemoryData.FieldByName('request$company').AsVariant := null;

        MemoryData.Post;
      end;

      Clone.Next;
    end;

    MemoryData.GotoBookmark(Bookmark);

    MemoryData.FreeBookmark(Bookmark);

    GridView.DataController.EndLocate;

    MemoryData.AfterPost := MemoryDataAfterPost;

    MemoryDataAfterPost(nil);

  end else
  begin
    while not Clone.Eof do
    begin
      uid := Clone.FieldByName('uid').AsInteger;

      Request := Clone.FieldByName('request$company').AsInteger;

      sql := 'update uidwh_t set APPL_Q = ' + IntToStr(Request) + ', ' + ' appl_user = ' +  IntToStr(UserID) + ' where uid =  ' + IntToStr(uid) + ' and userid = ' + IntToStr(ID) + ';';

      Scripter.Script.Add(SQL);

      Clone.Next;
    end;
  end;

  Clone.Free;

  if Scripter.Script.Count <> 0 then
  begin
    Transaction.StartTransaction;

    Scripter.ExecuteScript;

    Transaction.Commit;
  end;
end;

procedure TWarehouse.UpdateRequestOffice(Reset: Boolean);
var
  uid: Integer;
  Request: Integer;
  Clone: TClientDataSet;
  Flag: Boolean;
  SQL: string;
  Bookmark: TBookmark;
begin
  Scripter.Script.Clear;

  Clone := TClientDataSet.Create(nil);

  Clone.CloneCursor(MemoryData, True, False);

  Clone.AddIndex('request$office', 'request$office',[]);

  Clone.IndexName := 'request$office';

  Clone.SetRange([1], [MaxInt]);

  Clone.First;

  if Reset then
  begin
    MemoryData.AfterPost := nil;

    GridView.DataController.BeginLocate;

    Bookmark := MemoryData.GetBookmark;

    while Clone.RecordCount <> 0  do
    begin
      uid := Clone.FieldByName('uid').AsInteger;

      Request := 0;

      sql := 'update uidwh_t set APPLDEP_Q = null, ' + ' appldep_user = null where uid =  ' + IntToStr(uid) + ' and userid = ' + IntToStr(ID) + ';';

      Scripter.Script.Add(SQL);

      if MemoryData.FindKey([uid]) then
      begin
        MemoryData.Edit;

        MemoryData.FieldByName('request$office').AsVariant := Null;

        MemoryData.Post;
      end;

      Clone.Next;
    end;

    MemoryData.GotoBookmark(Bookmark);

    MemoryData.FreeBookmark(Bookmark);

    GridView.DataController.EndLocate;

    MemoryData.AfterPost := MemoryDataAfterPost;

    MemoryDataAfterPost(nil);

  end else
  begin
    while not Clone.Eof do
    begin
      uid := Clone.FieldByName('uid').AsInteger;

      Request := Clone.FieldByName('request$office').AsInteger;

      sql := 'update uidwh_t set APPLDEP_Q = ' + IntToStr(Request) + ', ' + ' appldep_user = ' +  IntToStr(UserID) + ' where uid =  ' + IntToStr(uid) + ' and userid = ' + IntToStr(ID) + ';';

      Scripter.Script.Add(SQL);

      Clone.Next;
    end;
  end;

  Clone.Free;

  if Scripter.Script.Count <> 0 then
  begin
    Transaction.StartTransaction;

    Scripter.ExecuteScript;

    Transaction.Commit;
  end;
end;

procedure TWarehouse.LayoutDelete(Name: string);
var
  R: TRegistry;
begin
  R := TRegistry.Create;

  Name := 'jew\' + Name;

  if R.KeyExists(Name) then
  begin
    R.DeleteKey(Name);
  end;

  R.Free;
end;

procedure TWarehouse.LayoutLoad(Name: string);
begin
  GridView.RestoreFromRegistry('jew', True, False, [], Name);
end;

procedure TWarehouse.LayoutSave(Name: string);
begin
  GridView.StoreToRegistry('jew', True, [], Name);
end;

procedure TWarehouse.Loaded;
begin
  inherited Loaded;

  LayoutDelete('warehouse.default');

  LayoutSave('warehouse.default');

  LayoutLoad('warehouse');
end;

procedure TWarehouse.OfficeChanged;
begin
  GridView.DataController.Filter.BeginUpdate;

  GridView.DataController.Filter.Active := False;

  FilterOffice;

  GridView.DataController.Filter.Active := True;

  GridView.DataController.Filter.EndUpdate;
end;

procedure TWarehouse.DataPriceBeforeOpen(DataSet: TDataSet);
begin
  TClientDataSet(DataSet).Params.ParamByName('warehouse$id').AsInteger := ID;
end;

procedure TWarehouse.GridViewCustomization(Sender: TObject);
begin
  if Gridviewpriceinv.Visible or GridViewCostniv.Visible then
  begin
    if not DataPrice.Active then
    begin
      MemoryData.Active := False;

      Execute;
    end;
  end else
  begin
    DataPrice.Active := False;
  end;
end;

{
procedure CalculateMaxPrice;
var
  a: TAggregate;
begin
  a := MemoryData.Aggregates.Add;

  a.Expression := 'max(price)';

  a.IndexName := 'art';

  a.GroupingLevel := 1;

  a.Active := True;

  MemoryData.IndexName := 'art';

  MemoryData.AggregatesActive := True;

  MemoryData.First;

  MemoryData.DisableControls;

  while not MemoryData.Eof do
  begin
    MemoryData.Edit;

    //MemoryDataMAXPRICE.AsCurrency := a.Value;

    MemoryData.Post;

    MemoryData.Next;
  end;

  MemoryData.First;

  MemoryData.EnableControls;

  MemoryData.IndexName := 'uid';
end;}

procedure TWarehouse.GridViewBands0HeaderClick(Sender: TObject);
begin
  if TDialog.Confirmation('������� ��������� ��������� ��������?') = mrYes then
  begin
    LayoutLoad('warehouse.default');
  end;
end;

procedure TWarehouse.GridViewCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;  AShift: TShiftState; var AHandled: Boolean);
var
  Item: TcxCustomGridTableItem;
  Value: Variant;
  ColumnIndex: Integer;
begin
  Item := ACellViewInfo.Item;

  ColumnIndex := Item.Index;

  Value := GridView.Controller.FocusedRow.Values[ColumnIndex];

  GridView.DataController.Filter.BeginUpdate;

  GridView.DataController.Filter.Root.SetItem(Item, foEqual, Value, Value);

  GridView.DataController.Filter.Active := True;  

  GridView.DataController.Filter.EndUpdate;
end;

procedure TWarehouse.ButtonResetClick(Sender: TObject);
begin
  if TDialog.Confirmation('������� ��������� ��������� ��������?') = mrYes then
  begin
    LayoutLoad('warehouse.default');
  end;
end;

end.



