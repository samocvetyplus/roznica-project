unit SelDep_anlz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, dbUtil, Mask, DBCtrlsEh, rxSpeedbar, ExtCtrls, PEriod,
  Menus, PFIBDataSet, DB, DBTables, FIBQuery, pFIBQuery;

type
  TfmSelDep_anlz = class(TForm)
    btnOk: TButton;
  //  lcbDepName: TDBLookupComboBox;
    cbSelDep: TDBComboBox;
    qSelDep: TpFIBQuery;
    procedure cbSelDepDropDown(Sender: TObject);
  private
  procedure ClickUserDepMenu(pm: TPopupMenu);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelDep_anlz: TfmSelDep_anlz;

implementation

{$R *.dfm}

uses data, comdata;

procedure TfmSelDep_anlz.cbSelDepDropDown(Sender: TObject);
begin
  cbseldep.Clear;
  qSelDep.ExecQuery;
  while not qSelDep.eof do begin
   cbSelDep.items.add(qSelDep.fieldbyname('NAME').asstring);
   qSelDep.next;
  end;
  cbSelDep.ItemIndex:=1;
end;

procedure TfmSelDep_anlz.ClickUserDepMenu(pm: TPopupMenu);
begin

end;

end.
