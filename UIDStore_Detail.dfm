object fmUIDStore_Detail: TfmUIDStore_Detail
  Left = 184
  Top = 152
  Width = 624
  Height = 381
  Caption = #1048#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object gridUIDStore_Detail: TDBGridEh
    Left = 0
    Top = 39
    Width = 616
    Height = 313
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsrUIDStore_Detail
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.Value = #1048#1090#1086#1075#1086
        Footer.ValueType = fvtStaticText
        Footers = <>
        Title.EndEllipsis = True
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUM'
        Footer.FieldName = 'ENTRYNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'SINVNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1055#1086#1089#1090#1072#1074#1082#1072
        Title.EndEllipsis = True
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUM'
        Footer.FieldName = 'DINVNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1042#1055
        Title.EndEllipsis = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'RETNUM'
        Footer.FieldName = 'RETNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETNUM'
        Footer.FieldName = 'OPTRETNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090
        Title.EndEllipsis = True
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMFROM'
        Footer.FieldName = 'DINVNUMFROM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1042#1055
        Title.EndEllipsis = True
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'SELLNUM'
        Footer.FieldName = 'SELLNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1055#1088#1086#1076#1072#1078#1072'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLNUM'
        Footer.FieldName = 'OPTSELLNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1055#1088#1086#1076#1072#1078#1072'|'#1054#1087#1090
        Title.EndEllipsis = True
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETNUM'
        Footer.FieldName = 'SINVRETNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1042#1086#1079#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072'v'
        Title.EndEllipsis = True
        Width = 53
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'RESNUM'
        Footer.FieldName = 'RESNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Width = 43
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'F'
        Footer.FieldName = 'F'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Width = 40
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 616
    Height = 39
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Margin = 0
      Spacing = 1
      Left = 546
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
end
