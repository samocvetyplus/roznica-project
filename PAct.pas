unit PAct;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, StdCtrls, Grids,
  DBGrids, RXDBCtrl, M207Grid, M207IBGrid, Menus, M207DBCtrls, DBGridEh,
  PrnDbgeh, ActnList, DB, FIBDataSet, pFIBDataSet, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmPAct = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fr1: TFormStorage;
    Label1: TLabel;
    DBText1: TDBText;
    Label2: TLabel;
    DBText2: TDBText;
    Label3: TLabel;
    DBText3: TDBText;
    pa1: TPanel;
    siView: TSpeedItem;
    pm: TPopupMenu;
    N1: TMenuItem;
    siClosePAct: TSpeedItem;
    Label4: TLabel;
    DBText4: TDBText;
    N2: TMenuItem;
    PrOrdItemId1: TMenuItem;
    dg1: TDBGridEh;
    prdg1: TPrintDBGridEh;
    acList1: TActionList;
    acPrintGrid: TAction;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siViewClick(Sender: TObject);
    procedure siClosePActClick(Sender: TObject);
    procedure PrOrdItemId1Click(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure SetCloseBtn;
  end;

var
  fmPAct: TfmPAct;

implementation

uses comdata, Data, Data2, PrSi, M207Proc, FIBQuery, MsgDialog;

{$R *.DFM}

procedure TfmPAct.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmPAct.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPAct.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmPAct.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  SetCloseBtn;
  with dmCom, dm do
    begin
      taPrOrdItem.SelectSQL[4]:='WHERE RQ>0';
      OpenDataSets([taPrOrdItem]);
    end
end;

procedure TfmPAct.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      CloseDataSets([taPrOrdItem]);
      taPrOrdItem.SelectSQL[4]:='';
    end
end;

procedure TfmPAct.siViewClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmPrSi, Self, TForm(fmPrSi), True, False);
end;

procedure TfmPAct.siClosePActClick(Sender: TObject);
var s: string[10];
begin
  with dm do
    begin
      if taPrOrdPActClosed.AsInteger=1 then s:='0'
      else s:='1';
      with quTmp do
        begin
          SQL.Text:='UPDATE PRORD SET PACTCLOSED='+s+' WHERE PRORDID='+taPrOrdPrOrdId.AsString;
          ExecQuery;
          SQL.Text:='UPDATE PRORD SET SetEmpId='+ IntToStr(dmCom.UserId) +' WHERE PRORDID='+taPrOrdPrOrdId.AsString;
          ExecQuery;
          Transaction.CommitRetaining;
          close;
        end;
      taPrOrd.Refresh;
      SetCloseBtn;
    end;
end;

procedure TfmPAct.SetCloseBtn;
begin
  with dm, siClosePAct do
    case taPrOrdPActClosed.AsInteger of
        0: begin
             BtnCaption:='�������';
             ImageIndex:=5;
             Hint:='������� ��� ����������';
           end;
        1: begin
             BtnCaption:='�������';
             ImageIndex:=6;
             Hint:='������� ��� ����������';
           end
      end;
end;

procedure TfmPAct.PrOrdItemId1Click(Sender: TObject);
begin
  MessageDialog(dm.taPrOrdItemPrOrdItemId.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmPAct.acPrintGridExecute(Sender: TObject);
begin
 prdg1.Title.Clear;
 prdg1.Title.Add('                  ��� � '+dm.taPrOrdNACT.AsString);
 prdg1.Print;
end;

end.
