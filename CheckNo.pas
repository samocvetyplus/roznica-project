unit CheckNo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls,
  rxPlacemnt, rxToolEdit, rxCurrEdit;

type
  TfmCheckNo = class(TForm)
    bbOK: TBitBtn;
    BitBtn2: TBitBtn;
    laCheck: TLabel;
    edCheck: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ceP: TCurrencyEdit;
    ceS: TCurrencyEdit;
    ceR: TCurrencyEdit;
    FormStorage1: TFormStorage;
    tm1: TTimer;
    procedure FormActivate(Sender: TObject);
    procedure cePChange(Sender: TObject);
    procedure edCheckChange(Sender: TObject);
    procedure tm1Timer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCheckNo: TfmCheckNo;

implementation

uses Data;

{$R *.DFM}

procedure TfmCheckNo.FormActivate(Sender: TObject);
begin
  edCheck.Text:='';
  if ceP.Enabled then
  begin
   ActiveControl:=ceP;
   ceP.SelectAll;
   bbOK.Visible:=true;
   edCheck.Visible:=NOT dm.UseFReg;
   laCheck.Visible:=NOT dm.UseFReg;
   bbOK.Enabled:=dm.UseFReg;
   ceR.Value:=0;
   ceP.Value:=ceS.Value;
   ceP.Color:=clInfoBk;
   BitBtn2.Visible:=true;
  end
 else
 begin
  tm1.Enabled:=true;
  ceP.Color:=clBtnFace;
  bbOK.Visible:=false;
  BitBtn2.Visible:=false;
 end;

end;

procedure TfmCheckNo.cePChange(Sender: TObject);
begin
 ceR.Value:=ceP.Value-ceS.Value;
end;

procedure TfmCheckNo.edCheckChange(Sender: TObject);
begin
  try
    StrToInt(edCheck.Text);
    bbOK.Enabled:=True;    
  except
    bbOK.Enabled:=False;
  end;
end;

procedure TfmCheckNo.tm1Timer(Sender: TObject);
begin
 tm1.Enabled:=false;
 fmCheckNo.Close;
end;

end.
