object FrameBuyOrder: TFrameBuyOrder
  Left = 0
  Top = 0
  Width = 693
  Height = 389
  TabOrder = 0
  object GroupBox: TcxGroupBox
    Left = 237
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 0
    Height = 389
    Width = 456
    object Grid: TcxGrid
      Left = 2
      Top = 54
      Width = 452
      Height = 333
      Align = alClient
      TabOrder = 0
      object GridView: TcxGridDBBandedTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = BuyOrdersData.DataSourceOrder
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.ImmediateEditor = False
        OptionsCustomize.ColumnFiltering = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
        OptionsView.GroupByBox = False
        Bands = <
          item
            Width = 301
          end
          item
            Caption = #1062#1077#1085#1072
            Width = 139
          end>
        object GridViewProbe: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PROBEID'
          Width = 188
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridViewMemo: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MEMOID'
          Width = 82
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridViewColumn4: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PRICE'
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridViewColumn5: TcxGridDBBandedColumn
          DataBinding.FieldName = 'PRICE2'
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
      end
      object GridLevel: TcxGridLevel
        GridView = GridView
      end
    end
    object BarDockControl: TdxBarDockControl
      Left = 2
      Top = 2
      Width = 452
      Height = 52
      Align = dalTop
      BarManager = BarManager
    end
  end
  object VerticalGrid: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 237
    Height = 389
    Align = alLeft
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = 'Office2007Blue'
    OptionsView.ScrollBars = ssNone
    OptionsView.PaintStyle = psDelphi
    OptionsView.RowHeaderWidth = 113
    OptionsBehavior.ImmediateEditor = False
    OptionsBehavior.RowTracking = False
    OptionsBehavior.AllowChangeRecord = False
    OptionsData.Appending = False
    OptionsData.Deleting = False
    OptionsData.Inserting = False
    TabOrder = 5
    DataController.DataSource = BuyOrdersData.DataSourceOrders
    Version = 1
    object VerticalGridCategoryRow1: TcxCategoryRow
      Properties.Caption = #1055#1088#1080#1082#1072#1079
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object VerticalGridDBEditorRow1: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ORDER$NUMBER'
      Properties.Options.Editing = False
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object VerticalGridDateRow: TcxDBEditorRow
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.DataBinding.FieldName = 'ORDER$DATE'
      Properties.Options.Filtering = False
      Properties.Options.IncSearch = False
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object VerticalGridCategoryRow2: TcxCategoryRow
      Properties.Caption = #1057#1090#1072#1090#1091#1089
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object VerticalGridDBEditorRow3: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'ORDER$STATE'
      Properties.Options.Editing = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object VerticalGridDBEditorRow4: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'MODIFY$DATE'
      Properties.Options.Editing = False
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object VerticalGridDBEditorRow5: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'USER$NAME'
      Properties.Options.Editing = False
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object VerticalGridCategoryRow3: TcxCategoryRow
      Properties.Caption = #1040#1076#1088#1077#1089#1072#1090
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object VerticalGridDBEditorRow6: TcxDBEditorRow
      Properties.DataBinding.FieldName = 'DEPARTMENT'
      Properties.Options.Editing = False
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    LookAndFeel.SkinName = 'Office2007Blue'
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 1008
    Top = 80
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockControl = BarDockControl
      DockedDockControl = BarDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 799
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAdd'
        end
        item
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonExit'
        end>
      OneOnRow = True
      RotateWhenVertical = False
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object ButtonAdd: TdxBarLargeButton
      Action = acAdd
      Category = 0
      ShortCut = 45
      AutoGrayScale = False
    end
    object ButtonDelete: TdxBarLargeButton
      Action = acDelete
      Category = 0
      ShortCut = 46
      AutoGrayScale = False
    end
    object ButtonExit: TdxBarLargeButton
      Action = acExit
      Align = iaRight
      Category = 0
      ShortCut = 121
      AutoGrayScale = False
    end
  end
  object ActionList: TActionList
    Images = DataBuy.Image32
    Left = 1008
    Top = 112
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDeleteExecute
      OnUpdate = acDeleteUpdate
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 8
      OnExecute = acExitExecute
    end
  end
end
