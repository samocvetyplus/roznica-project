unit frmBuyBackwardPopup;

interface

uses

  Windows, SysUtils, Controls, Classes, DateUtils,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,
  cxClasses, cxCalendar, cxGroupBox,
  dxSkinsCore, dxLayoutControl, dxSkinsdxBarPainter, dxBar,
  frmPopup, uCalendar, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, ActnList, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, DB, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridBandedTableView, cxGridDBBandedTableView;

type

{ TDialogBuyRangePopup }

  TDialogBuyBackwardPopup = class(TPopup)
    BarManager: TdxBarManager;
    GroupBox: TcxGroupBox;
    Layout: TdxLayoutControl;
    LayoutGroup: TdxLayoutGroup;
    LayoutSeparatorItem: TdxLayoutSeparatorItem;
    BarDockControlBottom: TdxBarDockControl;
    LayoutItemBarBottom: TdxLayoutItem;
    BarBottom: TdxBar;
    ButtonOk: TdxBarButton;
    ButtonCancel: TdxBarButton;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    LayoutItemGrid: TdxLayoutItem;
    GridView: TcxGridDBBandedTableView;
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  protected
    procedure Ok; override;
  end;

implementation

{$R *.dfm}

uses

   dmBuy, uDialog;

{ TDialogBuyRangePopup }

procedure TDialogBuyBackwardPopup.ButtonOkClick(Sender: TObject);
begin
  Ok;
end;

procedure TDialogBuyBackwardPopup.ButtonCancelClick(Sender: TObject);
begin
  Cancel;
end;

procedure TDialogBuyBackwardPopup.Ok;
begin
  inherited Ok;
end;

end.


