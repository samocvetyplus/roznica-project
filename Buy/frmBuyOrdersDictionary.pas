unit frmBuyOrdersDictionary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB, cxDBData,
  dxSkinsdxBarPainter, ActnList, dxBar, cxClasses, cxGridLevel,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxPC,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGrid, cxGroupBox;

type
  TFrameBuyDictionary = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonExit: TdxBarLargeButton;
    ActionList: TActionList;
    acAdd: TAction;
    acDelete: TAction;
    acExit: TAction;
    Grid: TcxGrid;
    GridView: TcxGridDBBandedTableView;
    GridLevel: TcxGridLevel;
    procedure acAddExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Activate;
    procedure Deactivate;
  end;

implementation

{$R *.dfm}

uses uDialog, dmBuyOrders;

procedure TFrameBuyDictionary.Activate;
begin
  with  do
    begin
      try
        Active := true;
      except
        On E: Exception do
          TDialog.Error('������ ��� �������� �����������: ' + E.Message);
      end;

      GridView.DataController.C
    end;
end;

procedure TFrameBuyDictionary.Deactivate;
begin

end;

procedure TFrameBuyDictionary.acAddExecute(Sender: TObject);
begin
  with  do
    begin
      try
        Insert;
      except
        On  E:Exception do
          begin
            TDialog.Error('������ ���������� �������: ' + E.Message);
            Cancel;
          end;
      end;
    end;
end;

procedure TFrameBuyDictionary.acDeleteExecute(Sender: TObject);
begin
  with  do
    begin
      if TDialog.Confirmation('������� ������?') = mrYes then
        begin
          try
            Delete;
          except
            On  E:Exception do
              begin
                TDialog.Error('������ �������� �������: ' + E.Message);
                Cancel;
              end;
          end;
        end;
    end;
end;

procedure TFrameBuyDictionary.acExitExecute(Sender: TObject);
var
  Sheet: TcxTabSheet;
  DialogResult: TModalResult;
begin
  if BuyOrdersData.Order.State in [dsEdit, dsInsert] then
    begin
      DialogResult := TDialog.Confirmation('������� ���������������� ���������.' + #13#10 + '���������?');

      if DialogResult = mrYes then
        begin
          BuyOrdersData.Order.Post
        end
      else
        begin
          BuyOrdersData.Order.Cancel;
        end;
    end;

  if Owner is TcxTabSheet then
    begin
      Sheet := TcxTabSheet(Owner);
      FreeAndNil(Sheet);
    end;
end;

end.
