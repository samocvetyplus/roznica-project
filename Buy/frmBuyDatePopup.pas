unit frmBuyDatePopup;

interface

uses

  Windows, SysUtils, Controls, Classes, DateUtils,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,
  cxClasses, cxCalendar, cxGroupBox,
  dxSkinsCore, dxLayoutControl, dxSkinsdxBarPainter, dxBar,
  frmPopup, uCalendar, dxSkinsDefaultPainters, ActnList, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;

type

{ TDialogBuyDatePopup }

  TDialogBuyDatePopup = class(TPopup)
    BarManager: TdxBarManager;
    GroupBox: TcxGroupBox;
    Layout: TdxLayoutControl;
    LayoutGroup: TdxLayoutGroup;
    LayoutItemDate: TdxLayoutItem;
    DateEditor: TcxCalendar;
    LayoutSeparatorItem: TdxLayoutSeparatorItem;
    BarDockControlBottom: TdxBarDockControl;
    LayoutItemBarBottom: TdxLayoutItem;
    BarBottom: TdxBar;
    ButtonOk: TdxBarButton;
    ButtonCancel: TdxBarButton;
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    function GetDate: TDate;
    procedure SetDate(Value: TDate);
  public
    property Date: TDate read GetDate write SetDate;
  end;

implementation

{$R *.dfm}

uses

   dmBuy, uDialog;

{ TDialogBuyDatePopup }

procedure TDialogBuyDatePopup.ButtonOkClick(Sender: TObject);
begin
  Ok;
end;

procedure TDialogBuyDatePopup.ButtonCancelClick(Sender: TObject);
begin
  Cancel;
end;

function TDialogBuyDatePopup.GetDate: TDate;
begin
  Result := DateEditor.SelectDate;
end;

procedure TDialogBuyDatePopup.SetDate(Value: TDate);
begin
  DateEditor.SelectDate := Value;
end;

end.


