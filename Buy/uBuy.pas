unit uBuy;

interface

uses

  Windows, SysUtils, DateUtils, Controls, ShlObj, Forms, Graphics,
  cxGridBandedTableView, cxGridDBBandedTableView;

type

  TDateRange = (drCustom, drDay, drWeek, drMonth, drYear);


  { TGridViewHelper }

  TGridViewHelper = class helper for TcxGridDBBandedTableView
  private
    function GetBand(Name: string): TcxGridBand;

  public
     property Band[Name: string]: TcxGridBand read GetBand;
  end;

function IsAltDown : Boolean;

function IsCtrlDown : Boolean;

function IsShiftDown : Boolean;

function LocalFolder: string;

function GlobalFolder: string;

function SpecialFolder(Alias : Integer) : string;

function DateRangeAsRange(RangeBegin, RangeEnd: TDate): TDateRange;

function DateRangeAsString(RangeBegin, RangeEnd: TDate): string;

function Blend(Color1, Color2: TColor; A: Byte): TColor;

implementation

function IsAltDown : Boolean;
var
  State: TKeyboardState;
begin
  GetKeyboardState(State);

  Result := ((State[vk_Menu] and 128)<>0);
end;

function IsCtrlDown : Boolean;
var
  State: TKeyboardState;
begin
  GetKeyboardState(State);

  Result := ((State[VK_CONTROL] and 128)<>0);
end;

function IsShiftDown : Boolean;
var
  State: TKeyboardState;
begin
  GetKeyboardState(State);

  Result := ((State[vk_Shift] and 128)<>0);
end;

function SpecialFolder(Alias : Integer) : string;
var
  PIDL: PItemIDList;
  Folder: string;
begin
  Result := '';

  if SUCCEEDED(SHGetSpecialFolderLocation(0, Alias, PIDL)) then
  begin
    SetLength(Folder, MAX_PATH);

    SHGetPathFromIDList(PIDL, PChar(Folder));

    SetLength(Folder, StrLen(PChar(Folder)));

    Folder := IncludeTrailingPathDelimiter(Folder);

    Result := Folder;
  end;
end;

function GlobalFolder: string;
var
  Folder: string;
begin
  Folder := ExtractFilePath(Application.ExeName);

  Folder := IncludeTrailingPathDelimiter(Folder);

  Result := Folder;
end;

function LocalFolder: string;
var
  Name: string;
  Folder: string;
  Extension: string;
begin
  Name := ExtractFileName(Application.ExeName);

  Extension := ExtractFileExt(Name);

  SetLength(Name, Length(Name) - Length(Extension));

  Folder := SpecialFolder(CSIDL_APPDATA);

  Folder := Folder + Name;

  Folder := IncludeTrailingPathDelimiter(Folder);

  if not DirectoryExists(Folder) then
  begin
    CreateDirectory(PChar(Folder), nil);
  end;

  Result := Folder;
end;

function DateRangeAsRange(RangeBegin, RangeEnd: TDate): TDateRange;
begin
  Result := drCustom;

  if (RangeBegin = RangeEnd) then
  begin
    Result := drDay;
  end else

  if (RangeBegin = DateOf(StartOfTheWeek(RangeBegin))) and (RangeEnd = DateOf(EndOfTheWeek(RangeBegin))) then
  begin
    Result := drWeek;
  end else

  if (RangeBegin = DateOf(StartOfTheMonth(RangeBegin))) and (RangeEnd = DateOf(EndOfTheMonth(RangeBegin))) then
  begin
    Result := drMonth;
  end else

  if (RangeBegin = DateOf(StartOfTheYear(RangeBegin))) and (RangeEnd = DateOf(EndOfTheYear(RangeBegin))) then
  begin
    Result := drYear;
  end;
end;

function DateRangeAsString(RangeBegin, RangeEnd: TDate): string;
var
  DateRange: TDateRange;

function MonthName(Value: TDateTime): string;
begin
  Result := '';

  case MonthOf(Value) of
     1: Result := '������';
     2: Result := '�������';
     3: Result := '�����';
     4: Result := '������';
     5: Result := '���';
     6: Result := '����';
     7: Result := '����';
     8: Result := '�������';
     9: Result := '��������';
    10: Result := '�������';
    11: Result := '������';
    12: Result := '�������';
  end;
end;

begin
  Result := '';

  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  case DateRange of
    drCustom:
      begin
        Result := FormatDateTime('dd ' + MonthName(RangeBegin) + ' yyyy ����', RangeBegin) + ' ... ' + FormatDateTime('dd ' + MonthName(RangeEnd) + ' yyyy ����', RangeEnd);
      end;
    drDay:
      begin
        Result := FormatDateTime('dd ' + MonthName(RangeBegin) + ' yyyy ����', RangeBegin);
      end;
    drWeek:
      begin
        Result := FormatDateTime('dd ' + MonthName(RangeBegin) + ' yyyy ����', RangeBegin) + ' ... ' + FormatDateTime('dd ' + MonthName(RangeEnd) + ' yyyy ����', RangeEnd);
      end;
    drMonth:
      begin
        Result := FormatDateTime('mmmm yyyy ����', RangeBegin);
      end;
    drYear:
      begin
        Result := FormatDateTime('yyyy ���', RangeBegin);
      end;
  end;
end;

{ TGridViewHelper }

function TGridViewHelper.GetBand(Name: string): TcxGridBand;
var
  Index: Integer;
  Band: TcxGridBand;
begin
  Band := nil;

  Name := Trim(AnsiUpperCase(Name));

  for Index := 0 to Bands.Count - 1 do
  begin
    Band := Bands[Index];

    if Trim(AnsiUpperCase(Band.Caption)) = Name then
    begin
      break;
    end;

    Band := nil;
  end;

  Result := Band;
end;

function Blend(Color1, Color2: TColor; A: Byte): TColor;
var
  c1, c2: LongInt;
  r, g, b, v1, v2: byte;
begin
  A:= Round(2.55 * A);
  c1 := ColorToRGB(Color1);
  c2 := ColorToRGB(Color2);
  v1:= Byte(c1);
  v2:= Byte(c2);
  r:= A * (v1 - v2) shr 8 + v2;
  v1:= Byte(c1 shr 8);
  v2:= Byte(c2 shr 8);
  g:= A * (v1 - v2) shr 8 + v2;
  v1:= Byte(c1 shr 16);
  v2:= Byte(c2 shr 16);
  b:= A * (v1 - v2) shr 8 + v2;
  Result := (b shl 16) + (g shl 8) + r;
end;

end.
