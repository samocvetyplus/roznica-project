unit dmBuyOrders;

interface

uses
  SysUtils, Classes, Controls, Variants, Dialogs,
  FIBDataSet, pFIBDataSet,
  FIBDataBase, pFIBDataBase,
  FIBQuery, pFIBQUery,
  DB, DBCLient, Provider,
  DateUtils, frxClass, frxDBSet, frxDesgn, frxExportODF, frxExportImage,
  frxExportPDF;

const
  CenterDepID: integer = 1;

  sqlMemo: String = 'select ID, Memo from Buy$Item$Memo';

  sqlProbe: String = 'select dp.ID, (cast(dm.Name as char(10)) || ' + #39 + ' ' + #39 +
                     ' || stretrim(dp.Name)) Probe from D_Probe dp, D_Material dm where dm.ID = dp.MatID order by dp.MatID, dp.Probe';

  sqlMaterial: String = 'select ID, Name from D_Material';

  sqlProbeDictionary: String = 'select ID, MatID, Probe,Name from D_Probe';

  sqlMemoDictionary: String = 'select ID, Memo from Buy$Item$Memo';

  sqlReportHeaderCompany: String = '';

type
  TBuyOrdersData = class(TDataModule)
    OrdersList: TpFIBDataSet;
    Transaction: TpFIBTransaction;
    D_Probe: TClientDataSet;
    D_Memo: TClientDataSet;
    DataSourceOrders: TDataSource;
    Order: TpFIBDataSet;
    DataSourceOrder: TDataSource;
    quTemp: TpFIBQuery;
    OrderID: TFIBIntegerField;
    OrderORDERID: TFIBIntegerField;
    OrderPROBEID: TFIBIntegerField;
    OrderMEMOID: TFIBIntegerField;
    OrderPRICE: TFIBBCDField;
    OrderPRICE2: TFIBBCDField;
    DataSourceProbe: TDataSource;
    DataSourceMemo: TDataSource;
    OrdersListORDERID: TFIBIntegerField;
    OrdersListORDERNUMBER: TFIBIntegerField;
    OrdersListORDERDATE: TFIBDateTimeField;
    OrdersListCREATIONDATE: TFIBDateTimeField;
    OrdersListMODIFYDATE: TFIBDateTimeField;
    OrdersListUSERID: TFIBIntegerField;
    OrdersListORDERSTATE: TFIBStringField;
    OrdersListDEPARTMENT: TFIBStringField;
    OrdersListUSERNAME: TFIBStringField;
    OrdersListORDERSTATECODE: TFIBIntegerField;
    Report: TfrxReport;
    ReportHeader: TfrxDBDataset;
    ReportItems: TfrxDBDataset;
    cdsReportHeader: TClientDataSet;
    cdsReportItems: TClientDataSet;
    OrdersListDEPARTMENTID: TFIBIntegerField;
    Designer: TfrxDesigner;
    procedure OrdersListNewRecord(DataSet: TDataSet);
    procedure OrdersListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure RollbackRetaining(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DataModuleDestroy(Sender: TObject);
    procedure OrderNewRecord(DataSet: TDataSet);
    procedure OrderItemChange(Sender: TField);
    procedure OrderBeforeClose(DataSet: TDataSet);
    function DesignerSaveReport(Report: TfrxReport; SaveAs: Boolean): Boolean;
    function ReportUserFunction(const MethodName: string;
      var Params: Variant): Variant;
  private
    function PreparePrintOrder(DocumentID: Integer; TemplateID: integer): boolean;
    procedure FillDictionary(ClientDataSet: TClientDataSet; SQL: String);
  public
    RangeStart: TDate;
    RangeEnd: TDate;
    SelfDepID: Integer;
    UserID: Integer;
    SelectedDep: Integer;
    function IsCenter: boolean;
    function IsAdministrator: boolean;
    function PreparePrint(TemplateID: integer): boolean;
//    function CreateDictionaryDataSet(DictionaryType: Integer): boolean;
  end;

var
  BuyOrdersData: TBuyOrdersData;

implementation

uses ComData, uDialog, dmBuy;

{$R *.dfm}


procedure TBuyOrdersData.OrderBeforeClose(DataSet: TDataSet);
var DialogResult: TModalResult;
begin
  if dataSet.State in [dsEdit, dsInsert] then
  begin
    DialogResult := TDialog.Confirmation('������� ���������������� ���������. ���������?');

    if DialogResult = mrYes then
    begin

      try
        DataSet.Post;
      except
        On E:Exception do
        begin
          TDialog.Error('������ ��� ���������� ���������: ' + E.Message);
        end;
      end;

    end;

  end;

end;

procedure TBuyOrdersData.OrderNewRecord(DataSet: TDataSet);
begin
  OrderORDERID.AsInteger := OrdersListORDERID.AsInteger;
  OrderMEMOID.AsInteger := 0;
  OrderPRICE.AsCurrency := 0.00;
  OrderPRICE2.AsCurrency := 0.00;
end;

procedure TBuyOrdersData.OrderItemChange(Sender: TField);

  function NullExists(vArray: array of Variant): boolean;
  var
    i: integer;
    HighBound: Integer;
    LowBound: Integer;
  begin
  
    Result := false;

    LowBound := Low(vArray);
    HighBound := High(vArray);

    for i := LowBound to HighBound do
    begin

      if VarIsNull(vArray[i]) then
      begin
        Result := true;
        break;
      end;

    end;

  end;

var
  ChangedField: TFIBIntegerField;
  ItemProbeID: Variant;
  ItemMemoID: Variant;
  ItemOrderID: Variant;
  TwinsCount: Integer;
begin
  if Sender is TFIBIntegerField then
    begin
      ChangedField := TFIBIntegerField(Sender);
      if (ChangedField.Index in [2,3]) then
        begin
          with Order do
            begin

//              ItemID := FieldByName('ID').NewValue;
              ItemProbeID := FieldByName('ProbeID').NewValue;
              ItemMemoID := FieldByName('MemoID').NewValue;
              ItemOrderID := FieldByName('OrderID').NewValue;

              if not NullExists([ItemProbeID, ItemMemoID, ItemOrderID]) then
              begin
                with quTemp do
                  begin
                    Close;
                    SQL.Text := 'select count(ID) CID from Order_Item where ' +
                                'ProbeID = ' + VarToStr(ItemProbeID) + ' and ' +
                                'MemoID = ' + VarToStr(ItemMemoID) + ' and ' +
                                'OrderID = ' + VarToStr(ItemOrderID) {+ ' and ' +
                                'ID <> ' + VarToStr(ItemID)};
                    ExecQuery;
                    TwinsCount := Fields[0].AsInteger;
                  end;

                if TwinsCount > 0 then
                  begin
                    ChangedField.NewValue := ChangedField.OldValue;
                    TDialog.Warning('������� �������������� ������� ������� ��������!');
                  end;
              end;

            end;
        end;
    end;
end;

procedure TBuyOrdersData.OrdersListBeforeOpen(DataSet: TDataSet);
begin
  OrdersList.ParamByName('RangeStart').AsDateTime := RangeStart;
  OrdersList.ParamByName('RangeEnd').AsDateTime := RangeEnd;
end;

procedure TBuyOrdersData.OrdersListNewRecord(DataSet: TDataSet);
begin
  OrdersListORDERNUMBER.AsInteger := -1;
  OrdersListORDERDATE.AsDateTime := Date;
  OrdersListUSERID.AsInteger := UserID;
  OrdersListDEPARTMENTID.AsInteger := SelectedDep;
end;

procedure TBuyOrdersData.PostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
  DataSet.Cancel;
  TDialog.Error(E.Message);
end;

procedure TBuyOrdersData.CommitRetaining(DataSet: TDataSet);
begin
if DataSet is TpFIBDataSet then
  with tpFIBDataSet(DataSet).Transaction do
    begin
      if Active then
        CommitRetaining;
    end;
end;

function TBuyOrdersData.ReportUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  Result := Unassigned;

  if MethodName = '������������' then
  begin
    Result := OrdersListORDERNUMBER.AsString;
  end;

  if MethodName = '�����������' then
  begin
    Result := DateTimeToStr(DateOf(OrdersListORDERDATE.AsDateTime));
  end;

  
end;

procedure TBuyOrdersData.RollbackRetaining(DataSet: TDataSet);
begin
if DataSet is TpFIBDataSet then
  with tpFIBDataSet(DataSet).Transaction do
    begin                       
      if Active then
      begin
        RollbackRetaining;
      end;
    end;
end;

function TBuyOrdersData.IsCenter: boolean;
begin
  Result := SelfDepID = CenterDepID;
end;

function TBuyOrdersData.IsAdministrator: boolean;
begin
  Result := dmCom.User.Adm;
end;

procedure TBuyOrdersData.DataModuleCreate(Sender: TObject);
begin
  RangeStart := DateOf(StartOfTheMonth(Today));
  RangeEnd := DateOf(EndOFTheMonth(Today));

  if not Transaction.Active then Transaction.StartTransaction;

  with quTemp do
    begin
      Close;
      SQL.Text := 'select gen_id(selfdepid,0) from rdb$database';
      ExecQuery;
      SelfDepID := Fields[0].AsInteger;
    end;

  UserID := dmCom.User.UserID;

  FillDictionary(D_Probe, sqlProbe);
  FillDictionary(D_Memo, sqlMemo);

end;

procedure TBuyOrdersData.DataModuleDestroy(Sender: TObject);
begin
  Transaction.Commit;
  Order.Active := False;
  OrdersList.Active := False;
end;

function TBuyOrdersData.DesignerSaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var ReportStream: TMemoryStream;
begin

  ReportStream := TMemoryStream.Create;
  Report.SaveToStream(ReportStream);

  try
    with quTemp do
      begin
        Close;

        SQL.Text := 'update buy$report set template = :template where id = :id';

        Params[0].LoadFromStream(ReportStream);
        Params[1].AsInteger := Report.Tag;

        ExecQuery;

        Transaction.CommitRetaining;
      end;
  except
    On E: Exception do
    begin
      TDialog.Error(E.Message);
    end;
  end;

  FreeAndNil(ReportStream);
end;

procedure TBuyOrdersData.FillDictionary(ClientDataSet: TClientDataSet; SQL: string);
var
  SourceDataSet: tpFIBDataSet;
  Provider: TDataSetProvider;
begin

  SourceDataSet := TpFIBDataSet.Create(Self);
  SourceDataSet.Database := dmCom.db;
  SourceDataSet.Transaction := Transaction;
  SourceDataSet.SelectSQL.Text := SQL;

  Provider := TDataSetProvider.Create(Self);
  Provider.Name := 'SourceProvider';
  Provider.DataSet := SourceDataSet;

  ClientDataSet.ProviderName := Provider.Name;

  try
    try
      ClientDataSet.Active := true;
    except
      on E:Exception do
        ShowMessage(E.Message);
    end;
  finally
    SourceDataSet.Active := false;
    ClientDataSet.ProviderName := '';
    Provider.Free;
    SourceDataSet.Free;
  end;

end;

function TBuyOrdersData.PreparePrint(TemplateID: Integer): boolean;
var DocumentID: Integer;
begin

  case TemplateID of
    4,5:
      begin
        DocumentID := OrdersListORDERID.AsInteger;
        Result := PreparePrintOrder(DocumentID, TemplateID);
      end
  else
    begin
      TDialog.Warning('TBuyOrdersData.PreparePrint: ����������� ��� ������!');
    end;  
  end;

end;

function TBuyOrdersData.PreparePrintOrder(DocumentID: Integer; TemplateID: Integer): boolean;
var
  ReportStream: TMemoryStream;
begin
  try

    cdsReportHeader.Data := DataBuy.Table('select country, post$index, city, name, address, phone, bill, bank, ' + #13#10 +
                                                  'correspondent$bill, bik, inn, okpo, okonh ' +#13#10 +
                                          'from Buy$Order$Report$H');

    cdsReportItems.Data := DataBuy.Table('select annotation, probe$name, price from Buy$Order$Report$Item(:Order$ID, :Order$Type)',
                                          [DocumentID, TemplateID]);
    with quTemp do
      begin
        Close;
        SQL.Text := 'Select template from buy$report where id = :id';
        Params[0].AsInteger := TemplateID;
        ExecQuery;
      end;

    Report.AddFunction('function ������������: String');

    Report.AddFunction('function �����������: String');


    if not VarIsNull(quTemp.Fields[0].AsVariant) then
    begin
      ReportStream := TMemoryStream.Create;

      quTemp.Fields[0].SaveToStream(ReportStream);

      ReportStream.Position := 0;
      Report.LoadFromStream(ReportStream);

      FreeAndNil(ReportStream);
    end;

    Report.PrepareReport;

    Report.Tag := TemplateID;

    if IsAdministrator then
    begin
      Report.DesignReport;
    end else
    begin
      Report.PreviewOptions.AllowEdit := false;
      Report.ShowPreparedReport;
    end;

  except

    On E:Exception do
      TDialog.Error(E.Message);

  end;

end;

//function TBuyOrdersData.CreateDictionaryDataSet(DictionaryType: Integer):boolean;
//var
//  i: integer;
//  DataSet: TpFIBDataSet;
//
//begin
//
//Result := false;
//
//try
//  DataSet := TpFIBDataSet.Create(Self);
//
//  with DataSet do
//    begin
//
//      case DictionaryType of
//        1:
//          begin
//            SelectSQL.Text := sqlProbeDictionary;
//
//            FillDictionary(D_Material, sqlMaterial);
//
////            FieldByName('MatID').DisplayLabel := '��������';
////            FieldByName('Probe').DisplayLabel := '�����';
////            FieldByName('Transformation$585').DisplayLabel := '����. �������� � 585';
////            FieldByName('Transformation$Purity').DisplayLabel := '����. �������� � �������';
////            FieldByName('Correction$Koefficient').DisplayLabel := '��������, %';
////            FieldByName('Additional$Cost').DisplayLabel := '��������, ���.';
//
//          end;
//        2:
//          begin
//            SelectSQL.Text := sqlProbeDictionary;
//
////            FieldByName('Memo').DisplayLabel := '�������';
//          end;
//      end;
//
//      GenerateSQLs;
//
//      if not IsCenter then
//        AllowedUpdateKinds := [];
//
//    end;
//
//finally
//  DictionaryDataSet := DataSet;
//  FreeAndNil(DataSet);
//  Result := true;
//end;
//
//
//end;

end.
