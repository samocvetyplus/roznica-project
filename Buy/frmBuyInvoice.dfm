object FrameBuyInvoice: TFrameBuyInvoice
  Left = 0
  Top = 0
  Width = 1090
  Height = 633
  Align = alClient
  TabOrder = 0
  ExplicitWidth = 451
  ExplicitHeight = 304
  object GroupBoxHeader: TcxGroupBox
    Left = 0
    Top = 0
    Align = alLeft
    PanelStyle.Active = True
    TabOrder = 0
    ExplicitHeight = 304
    Height = 633
    Width = 369
    object Layout: TdxLayoutControl
      Left = 2
      Top = 2
      Width = 365
      Height = 629
      Align = alClient
      TabOrder = 0
      TabStop = False
      LayoutLookAndFeel = dxLayoutCxLookAndFeel1
      OptionsImage.Images = DataBuy.Image32
      ExplicitHeight = 300
      object LayoutEditorN: TcxDBTextEdit
        Left = 76
        Top = 28
        DataBinding.DataField = 'N'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = False
        Style.HotTrack = False
        TabOrder = 0
        OnExit = OnUpdate
        Width = 101
      end
      object LayoutEditorState: TcxDBTextEdit
        Left = 34
        Top = 73
        DataBinding.DataField = 'STATE$NAME'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 2
        Width = 68
      end
      object LayoutEditorStateDate: TcxDBTextEdit
        Left = 161
        Top = 73
        DataBinding.DataField = 'STATE$DATE'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 3
        Width = 121
      end
      object LayoutEditorStatePerson: TcxDBTextEdit
        Left = 95
        Top = 100
        DataBinding.DataField = 'STATE$BY$NAME'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 4
        Width = 229
      end
      object LayoutEditorDepartment: TcxDBTextEdit
        Left = 76
        Top = 196
        DataBinding.DataField = 'DEPARTMENT$NAME'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 6
        Width = 257
      end
      object LayoutEditorAgent: TcxDBTextEdit
        Left = 22
        Top = 262
        DataBinding.DataField = 'AGENT$NAME'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 7
        Width = 235
      end
      object LayoutEditorCompany: TcxDBTextEdit
        Left = 76
        Top = 169
        DataBinding.DataField = 'COMPANY$NAME'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.ReadOnly = True
        Style.HotTrack = False
        TabOrder = 5
        Width = 257
      end
      object LayoutEditorForeignN: TcxDBTextEdit
        Left = 95
        Top = 317
        DataBinding.DataField = 'FOREIGN$N'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Style.HotTrack = False
        TabOrder = 9
        OnExit = OnUpdate
        Width = 75
      end
      object LayoutEditorForeignDate: TcxDBDateEdit
        Left = 208
        Top = 317
        DataBinding.DataField = 'FOREIGN$DATE'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.DateButtons = []
        Style.HotTrack = False
        TabOrder = 10
        OnExit = OnUpdate
        Width = 121
      end
      object LayoutEditorForeignCost: TcxDBTextEdit
        Left = 95
        Top = 344
        DataBinding.DataField = 'FOREIGN$COST'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Style.HotTrack = False
        TabOrder = 11
        OnExit = OnUpdate
        Width = 76
      end
      object LayoutEditorDate: TcxDBDateEdit
        Left = 218
        Top = 28
        DataBinding.DataField = 'INVOICE$DATE'
        DataBinding.DataSource = SourceInvoice
        Enabled = False
        Properties.Alignment.Horz = taCenter
        Properties.Alignment.Vert = taVCenter
        Properties.DateButtons = []
        Properties.SaveTime = False
        Properties.ShowTime = False
        Style.HotTrack = False
        TabOrder = 1
        OnExit = OnUpdate
        Width = 121
      end
      object LayoutEditorAgentButton: TcxButton
        Left = 303
        Top = 253
        Width = 40
        Height = 40
        Action = ActionAgent
        TabOrder = 8
        Glyph.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000001010100010101000102020001
          0202000102020001020200010202000102020001020200010101000101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000052A6D4FF51A5D4FF51A5D3FF52A4D4FF51A4
          D3FF52A4D2FF53A3D2FF53A3D1FF54A2D0FF54A2D0FF56A2CFFF58A1CEFF59A1
          CCFF00000000000000000000000000000000000000005C9FC9FF5D9EC8FF5D9E
          C8FF5E9EC7FF0000000000000000000000000000000000000000000000000000
          000000000000000000000001010150A6D4FFCFEBFAFFBBE3F8FFAEDDF7FFAADA
          F6FFA8D9F7FFA7D8F7FFA7D8F7FFA6D7F7FFAAD8F8FFB5DDF9FFC9E7FBFF58A1
          CEFF00010101000000000000000000000000000000005C9FC9FFD6EDFCFFD5ED
          FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
          000000000000000000000001010150A7D6FFBCE3F8FF90D2F3FF74C6F0FF6BC1
          F0FF69BFF0FF68BDF0FF67BCF0FF67BBF1FF6EBDF2FF8AC9F5FFB5DDF9FF56A1
          CFFF00010101000000000000000000000000000000005B9FCAFFD6EDFCFFD6ED
          FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA8D6FFB0E0F6FF76C9EFFF50B9ECFF45B2
          EBFF42B0EBFF41AEECFF40ABECFF41AAEDFF4AADEEFF6FBCF3FFA9D7F8FF55A2
          D0FF00010202000000000000000000000000000000005B9FCAFFD6EEFCFFD6ED
          FCFF5D9EC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA8D7FFADDFF6FF6EC6EEFF46B6EBFF3AAF
          E9FF37ACEAFF36AAEAFF34A8EBFF35A6EBFF40A9EDFF67B9F1FFA6D6F7FF54A2
          D0FF00010202000000000000000000000000000000005BA0CBFFD7EEFCFFD6EE
          FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA9D7FFAEDFF5FF6EC7EEFF45B6EAFF38B0
          E9FF36ADE9FF35AAEAFF34A8EAFF34A7EBFF3FAAEDFF67BAF1FFA6D7F7FF54A2
          D1FF00010202000000000000000000000000000000005AA0CBFFD7EEFCFFD7EE
          FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA9D8FFAEDFF5FF6FC8EEFF46B8EAFF39B1
          E8FF37AEE9FF36ACEAFF35AAEAFF35A8EBFF40ABECFF68BBF1FFA6D7F7FF53A3
          D1FF00010202000000000000000000000000000000005AA0CBFFD8EEFCFFD7EE
          FCFF5B9FCAFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024DA9D8FFAFE0F5FF6FC9EEFF47B9EAFF3AB3
          E8FF38AFE9FF37ADE9FF35ABEAFF35AAEAFF40ACECFF68BCF0FFA6D7F7FF53A3
          D2FF000102020000000000000000000000000000000059A1CCFFD7EEFCFFD8EE
          FCFF5BA0CAFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024DAAD9FFAFE1F5FF70CAEEFF47BAE9FF3AB3
          E8FF38B1E8FF37AFE9FF36ACE9FF36ABEAFF41ADECFF65BBF0FFA2D5F6FF51A3
          D3FF010305050001020200010202000102020102030358A1CDFFD4ECFCFFD5EC
          FCFF5AA0CBFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024CAAD9FFB0E1F5FF70CBEEFF48BBE9FF3BB4
          E8FF39B2E8FF38B0E9FF37AEE9FF37ABEAFF3EADEBFF5BB8EFFF90CEF4FF7ABC
          E5FF51A3D3FF53A3D1FF54A2D1FF54A1D1FF54A1D1FF82BCE2FFC0E2FAFFCBE8
          FBFF59A0CCFF0001010100000000000000000000000000000000000000000000
          00000000000000000000000102024CABDAFFB0E1F5FF71CCEDFF49BDE9FF3CB6
          E7FF3AB3E8FF38B1E8FF37AFE9FF37ADE9FF3AACEAFF49B1ECFF6CBFF1FF90CD
          F5FFA2D5F6FFA7D7F7FFA6D6F7FFA5D5F8FFA0D2F7FF98CEF7FF9CCFF7FFB8DE
          FAFF589FCEFF0001010100000000000000000000000000000000000000000000
          00000000000000000000000202024BACDBFFB1E2F5FF72CDEDFF49BEE8FF3CB7
          E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF37ADEAFF3CADEBFF49B0EDFF5BB7
          EFFF65BAF1FF66B9F1FF66B8F2FF64B6F2FF62B4F2FF63B4F2FF78BEF5FFABD7
          F8FF56A0CEFF0001020200000000000000000000000000000000000000000000
          00000000000000000000010202024BACDBFFB1E2F5FF72CDEDFF4ABEE8FF3DB8
          E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37ADE9FF36ACEAFF39ABEAFF3CAA
          ECFF3EA9ECFF3FA8EDFF3EA6EDFF3DA4EEFF3CA3EEFF44A5F0FF67B5F3FFA4D3
          F8FF55A0D0FF0001020200000000000000000000000000000000000000000000
          00000000000000000000010304044AACDCFFACE1F4FF6FCDECFF4ABFE8FF3DB9
          E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AFE9FF36ACE9FF36AAEAFF35A8
          EBFF34A6EBFF33A4ECFF32A2ECFF31A0EDFF329EEDFF3CA2EFFF61B3F2FF9FD1
          F8FF54A0D0FF010204040000000000000000000000000000000048ADDEFF48AC
          DDFF49ACDDFF4AABDCFF4AACDCFF7FC8E8FF9EDDF2FF67CBEBFF47BFE7FF3EBA
          E6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0E9FF37AEE9FF36ABEAFF35A9
          EAFF33A7EBFF32A5EBFF31A2ECFF30A0ECFF309EEDFF38A1EEFF57AFF1FF8FCA
          F6FF7EBBE3FF56A1CEFF59A1CCFF59A1CCFF5AA0CBFF5BA0CBFF010203032F70
          90A58DCBEAFFE7F4FCFFDAF1FAFFBAE6F5FF86D6EFFF59C7E9FF43BFE6FF3EBB
          E6FF3CBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1E8FF37AFE9FF36ADE9FF35AA
          EAFF34A8EAFF33A6EBFF32A4ECFF31A1ECFF319FEDFF34A0EDFF49A8F0FF77BE
          F4FFACD7F8FFCDE8FBFFCDE8F8FF8EC1E0FF2F536A8400000000000000000102
          03032C69889C8ACAEAFFD5EFF9FFB3E4F4FF80D4EDFF57C8E8FF45C0E6FF3FBD
          E6FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF36AC
          EAFF35AAEAFF34A7EBFF33A5EBFF32A3ECFF31A2ECFF36A1EEFF48A9EFFF72BB
          F3FFA6D5F8FFBDDFF6FF8CC1E0FF2D5168810000000000000000000000000000
          0000000000002A64819486CAEAFFBBE6F5FF98DCF1FF6DCFEBFF4EC4E7FF41BF
          E5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37AD
          E9FF35ABEAFF34A9EAFF33A6EBFF33A5EBFF36A4EDFF44A9EEFF60B5F1FF8BC9
          F5FFAAD6F5FF87BFE2FF2A4F657D000000000000000000000000000000000000
          00000000000000010101265C758783C9E9FFAFE1F3FF8CD8EFFF5FCAE9FF46C0
          E6FF3FBDE5FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AF
          E9FF36ACE9FF35AAEAFF35A9EBFF38A7ECFF46ACEEFF61B7F0FF89C9F5FFA4D3
          F4FF85BFE3FF294F647B00010101000000000000000000000000000000000000
          000000000000000000000001010124566E7F81C9E9FFA6E1F2FF6DCFEBFF4AC2
          E6FF40BEE5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0
          E9FF37AEE9FF37ACEAFF3AABEBFF47B0EDFF63BAF0FF8BCBF4FFA4D4F3FF84C0
          E4FF274D64790001010100000000000000000000000000000000000000000000
          00000000000000000000000000000102030348AEDEFFB2E5F4FF74D2EBFF4DC4
          E7FF41C0E5FF3FBDE5FF3EBBE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1
          E8FF38B0E9FF3BAFEAFF49B2ECFF65BDEFFF8ECDF4FFA4D4F3FF84C1E4FF264C
          6276000101010000000000000000000000000000000000000000000000000000
          00000000000000000000000000000102020247AEDEFFB5E5F4FF77D2ECFF52C7
          E7FF49C3E6FF49C1E6FF47C0E7FF42BDE7FF3DB9E6FF3BB7E7FF3AB5E7FF3AB4
          E8FF3EB2E9FF4BB6EBFF67C0EFFF90D0F3FFA3D6F3FF85C1E5FF24485E700001
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000102020248AFDEFFBAE7F5FF82D6EDFF64CD
          E9FF65CCEAFF67CCEBFF61C9EAFF51C2E9FF44BDE7FF3EB9E7FF3CB6E7FF40B6
          E9FF4DBAEAFF6AC3EFFF92D2F3FFA4D6F2FF83C1E5FF22475B6D000101010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000001010147AEDFFFC7EAF7FF9EDFF1FF8FDA
          EFFF98DCF1FF9BDCF1FF91D8F0FF73CEECFF56C3E9FF45BCE7FF43BAE8FF4FBD
          EAFF6CC6EEFF94D4F3FFA4D7F1FF81C2E5FF2145596A00010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000001010147AEDFFFDAF1FAFFC9EBF7FFC4EA
          F7FF9CD6EFFF45AAD7F796D4EEFFA0DDF2FF79D0EEFF5DC5EAFF59C2EBFF6FC9
          EDFF96D6F3FFA4D7F1FF80C2E5FF204357670001010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046AEDFFF47AEDFFF47AEDEFF48AE
          DEFF48ADDDFF132F3B4446A6D3F499D5EEFFA6DFF3FF87D4F0FF82D2EFFF9BDA
          F3FFA6D9F2FF4BA8D6FC1E425564000101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000010101000101010001
          010100010101000101011129343C44A1CEEE98D3EDFFB9E5F6FFB6E3F6FFA9DB
          F1FF4BA8D7FC1E43556400010101000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000101010E232C33439FCBEB9BD3EDFFB5DEF3FF49A9
          D8FC1B3E505D0001010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000C1C23294299C5E449A9D8FC1A3B
          4C58000101010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000009161C211734434E0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        LookAndFeel.Kind = lfUltraFlat
        PaintStyle = bpsGlyph
        SpeedButtonOptions.CanBeFocused = False
        SpeedButtonOptions.Flat = True
        SpeedButtonOptions.Transparent = True
      end
      object LayoutGroupRoot: TdxLayoutGroup
        AlignHorz = ahClient
        AlignVert = avClient
        ButtonOptions.Buttons = <>
        Hidden = True
        ShowBorder = False
        object LayoutGroup5: TdxLayoutGroup
          ButtonOptions.Buttons = <>
          Hidden = True
          LayoutDirection = ldHorizontal
          ShowBorder = False
          object LayoutGroup6: TdxLayoutGroup
            AlignHorz = ahClient
            ButtonOptions.Buttons = <>
            Hidden = True
            ShowBorder = False
            object LayoutGroupInvoice: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avTop
              CaptionOptions.Text = #1044#1086#1082#1091#1084#1077#1085#1090
              ButtonOptions.Buttons = <>
              object LayoutGroup7: TdxLayoutGroup
                AlignHorz = ahClient
                ButtonOptions.Buttons = <>
                Hidden = True
                ShowBorder = False
                object LayoutGroup1: TdxLayoutGroup
                  ButtonOptions.Buttons = <>
                  Hidden = True
                  LayoutDirection = ldHorizontal
                  ShowBorder = False
                  object LayoutItemN: TdxLayoutItem
                    AlignHorz = ahClient
                    CaptionOptions.Text = #8470
                    Enabled = False
                    Control = LayoutEditorN
                    ControlOptions.ShowBorder = False
                  end
                  object LayoutItemDate: TdxLayoutItem
                    AlignHorz = ahClient
                    CaptionOptions.Text = #1044#1072#1090#1072
                    Enabled = False
                    Control = LayoutEditorDate
                    ControlOptions.ShowBorder = False
                  end
                end
                object LayoutGroupState: TdxLayoutGroup
                  AlignHorz = ahClient
                  CaptionOptions.Text = #1057#1086#1089#1090#1086#1103#1085#1080#1077
                  ButtonOptions.Buttons = <>
                  object LayoutGroup2: TdxLayoutGroup
                    ButtonOptions.Buttons = <>
                    Hidden = True
                    LayoutDirection = ldHorizontal
                    ShowBorder = False
                    object LayoutItemState: TdxLayoutItem
                      AlignHorz = ahClient
                      CaptionOptions.Text = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
                      CaptionOptions.Visible = False
                      Enabled = False
                      Control = LayoutEditorState
                      ControlOptions.ShowBorder = False
                    end
                    object LayoutItemStateDate: TdxLayoutItem
                      AlignHorz = ahClient
                      CaptionOptions.Text = #1044#1072#1090#1072
                      Enabled = False
                      Control = LayoutEditorStateDate
                      ControlOptions.ShowBorder = False
                    end
                  end
                  object LayoutItemStatePerson: TdxLayoutItem
                    AlignHorz = ahClient
                    CaptionOptions.Text = #1057#1086#1090#1088#1091#1076#1085#1080#1082
                    Enabled = False
                    Control = LayoutEditorStatePerson
                    ControlOptions.ShowBorder = False
                  end
                end
              end
            end
            object LayoutGroupCompany: TdxLayoutGroup
              CaptionOptions.Text = #1057#1082#1083#1072#1076
              ButtonOptions.Buttons = <>
              object LayoutItemCompany: TdxLayoutItem
                AlignHorz = ahClient
                CaptionOptions.Text = #1050#1086#1084#1087#1072#1085#1080#1103
                Enabled = False
                Control = LayoutEditorCompany
                ControlOptions.ShowBorder = False
              end
              object LayoutItemDepartment: TdxLayoutItem
                AlignHorz = ahClient
                CaptionOptions.Text = #1057#1082#1083#1072#1076
                Enabled = False
                Control = LayoutEditorDepartment
                ControlOptions.ShowBorder = False
              end
            end
            object LayoutGroupAgent: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avTop
              CaptionOptions.Text = #1057#1082#1083#1072#1076
              ButtonOptions.Buttons = <>
              object LayoutGroup4: TdxLayoutGroup
                AlignHorz = ahClient
                ButtonOptions.Buttons = <>
                Hidden = True
                LayoutDirection = ldHorizontal
                ShowBorder = False
                object LayoutItemAgent: TdxLayoutItem
                  AlignHorz = ahClient
                  AlignVert = avCenter
                  Enabled = False
                  Control = LayoutEditorAgent
                  ControlOptions.ShowBorder = False
                end
                object LayoutItemAgentButton: TdxLayoutItem
                  AlignHorz = ahRight
                  AlignVert = avCenter
                  CaptionOptions.Text = 'cxButton1'
                  CaptionOptions.Visible = False
                  Visible = False
                  Control = LayoutEditorAgentButton
                  ControlOptions.ShowBorder = False
                end
              end
              object LayoutGroupForeign: TdxLayoutGroup
                CaptionOptions.Text = #1044#1086#1082#1091#1084#1077#1085#1090
                Enabled = False
                Visible = False
                ButtonOptions.Buttons = <>
                object LayoutGroup3: TdxLayoutGroup
                  Enabled = False
                  ButtonOptions.Buttons = <>
                  Hidden = True
                  LayoutDirection = ldHorizontal
                  ShowBorder = False
                  object LayoutItem2: TdxLayoutItem
                    AlignHorz = ahClient
                    CaptionOptions.Text = #8470
                    Enabled = False
                    Control = LayoutEditorForeignN
                    ControlOptions.ShowBorder = False
                  end
                  object LayoutItem3: TdxLayoutItem
                    AlignHorz = ahClient
                    CaptionOptions.Text = #1044#1072#1090#1072
                    Enabled = False
                    Control = LayoutEditorForeignDate
                    ControlOptions.ShowBorder = False
                  end
                end
                object LayoutItemForeignCost: TdxLayoutItem
                  AlignHorz = ahLeft
                  CaptionOptions.Text = #1057#1090#1086#1080#1084#1086#1089#1090#1100
                  Enabled = False
                  Control = LayoutEditorForeignCost
                  ControlOptions.ShowBorder = False
                end
              end
            end
          end
        end
      end
    end
  end
  object SplitterLeft: TcxSplitter
    Left = 369
    Top = 0
    Width = 8
    Height = 633
    HotZoneClassName = 'TcxXPTaskBarStyle'
    AllowHotZoneDrag = False
    ResizeUpdate = True
    Control = GroupBoxHeader
    ExplicitHeight = 304
  end
  object GroupBoxContent: TcxGroupBox
    Left = 377
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 2
    ExplicitWidth = 74
    ExplicitHeight = 304
    Height = 633
    Width = 713
    object PageContent: TcxPageControl
      Left = 2
      Top = 2
      Width = 709
      Height = 629
      ActivePage = SheetContent
      Align = alClient
      Images = DataBuy.ImageChain
      TabOrder = 0
      TabPosition = tpBottom
      ExplicitWidth = 70
      ExplicitHeight = 300
      ClientRectBottom = 588
      ClientRectRight = 709
      ClientRectTop = 0
      object SheetContent: TcxTabSheet
        ImageIndex = 0
        ExplicitWidth = 70
        ExplicitHeight = 259
        object BarDock: TdxBarDockControl
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 701
          Height = 52
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 0
          Align = dalTop
          BarManager = BarManager
          ExplicitWidth = 62
        end
        object GridInvoiceItem: TcxGrid
          AlignWithMargins = True
          Left = 0
          Top = 60
          Width = 709
          Height = 249
          Margins.Left = 0
          Margins.Top = 4
          Margins.Right = 0
          Margins.Bottom = 0
          Align = alClient
          BevelEdges = [beTop]
          BevelKind = bkFlat
          BorderStyle = cxcbsNone
          TabOrder = 0
          TabStop = False
          ExplicitWidth = 70
          ExplicitHeight = 157
          object GridInvoiceItemView: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = SourceInvoiceItem
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'COST'
                Column = GridInvoiceItemViewCOST
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridInvoiceItemViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridInvoiceItemViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridInvoiceItemViewWEIGHT1000
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                FieldName = 'COST'
                Column = GridInvoiceItemViewCOST
                DisplayText = #1057#1090#1086#1080#1084#1086#1089#1090#1100
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT'
                Column = GridInvoiceItemViewWEIGHT
                DisplayText = #1063#1080#1089#1090#1099#1081
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT$REAL'
                Column = GridInvoiceItemViewWEIGHTREAL
                DisplayText = #1043#1088#1103#1079#1085#1099#1081
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT$1000'
                Column = GridInvoiceItemViewWEIGHT1000
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.BandMoving = False
            OptionsCustomize.ColumnVertSizing = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = '...'
            OptionsView.ScrollBars = ssVertical
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            OptionsView.BandHeaderLineCount = 2
            Bands = <
              item
                Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              end
              item
                Caption = #1042#1077#1089
              end
              item
                Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
              end>
            object GridInvoiceItemViewMATERIAL: TcxGridDBBandedColumn
              Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              DataBinding.FieldName = 'MATERIAL$NAME'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewPROBE: TcxGridDBBandedColumn
              Caption = #1055#1088#1086#1073#1072
              DataBinding.FieldName = 'PROBE$NAME'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              SortIndex = 1
              SortOrder = soAscending
              Width = 56
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewMEMO: TcxGridDBBandedColumn
              Caption = #1055#1088#1080#1079#1085#1072#1082
              DataBinding.FieldName = 'MEMO$NAME'
              Visible = False
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Sorting = False
              Width = 137
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewWEIGHT: TcxGridDBBandedColumn
              Caption = #1063#1080#1089#1090#1099#1081
              DataBinding.FieldName = 'WEIGHT'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewPRICE: TcxGridDBBandedColumn
              Caption = #1062#1077#1085#1072
              DataBinding.FieldName = 'PRICE'
              HeaderAlignmentHorz = taCenter
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 2
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewCOST: TcxGridDBBandedColumn
              Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
              DataBinding.FieldName = 'COST'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 2
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewWEIGHTREAL: TcxGridDBBandedColumn
              Caption = #1043#1088#1103#1079#1085#1099#1081
              DataBinding.FieldName = 'WEIGHT$REAL'
              HeaderAlignmentHorz = taCenter
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridInvoiceItemViewWEIGHT1000: TcxGridDBBandedColumn
              Caption = '1000'
              DataBinding.FieldName = 'WEIGHT$1000'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
          end
          object GridInvoiceItemLevel: TcxGridLevel
            GridView = GridInvoiceItemView
          end
        end
        object SplitterTop: TcxSplitter
          Left = 0
          Top = 309
          Width = 709
          Height = 8
          HotZoneClassName = 'TcxXPTaskBarStyle'
          AlignSplitter = salBottom
          ResizeUpdate = True
          Control = GridStore
          Visible = False
          ExplicitTop = -20
          ExplicitWidth = 70
        end
        object GridStore: TcxGrid
          Left = 0
          Top = 317
          Width = 709
          Height = 271
          Align = alBottom
          BevelEdges = [beTop]
          BevelKind = bkFlat
          BorderStyle = cxcbsNone
          TabOrder = 2
          TabStop = False
          Visible = False
          ExplicitTop = -12
          ExplicitWidth = 70
          object GridStoreView: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = SourceStore
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.BandMoving = False
            OptionsCustomize.ColumnVertSizing = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.NoDataToDisplayInfoText = '...'
            OptionsView.ScrollBars = ssVertical
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            OptionsView.Indicator = True
            OptionsView.BandHeaderLineCount = 2
            Bands = <
              item
                Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              end
              item
                Caption = #1042#1077#1089
              end>
            object GridStoreViewMATERIAL: TcxGridDBBandedColumn
              Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              DataBinding.FieldName = 'MATERIAL$NAME'
              HeaderAlignmentHorz = taCenter
              SortIndex = 0
              SortOrder = soAscending
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object GridStoreViewPROBE: TcxGridDBBandedColumn
              Caption = #1055#1088#1086#1073#1072
              DataBinding.FieldName = 'PROBE$NAME'
              HeaderAlignmentHorz = taCenter
              SortIndex = 1
              SortOrder = soAscending
              Width = 56
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridStoreViewWEIGHT: TcxGridDBBandedColumn
              Caption = #1063#1080#1089#1090#1099#1081
              DataBinding.FieldName = 'WEIGHT'
              HeaderAlignmentHorz = taCenter
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
            object GridStoreViewWEIGHTREAL: TcxGridDBBandedColumn
              Caption = #1043#1088#1103#1079#1085#1099#1081
              DataBinding.FieldName = 'WEIGHT$REAL'
              HeaderAlignmentHorz = taCenter
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridStoreViewWEIGHT1000: TcxGridDBBandedColumn
              Caption = '1000'
              DataBinding.FieldName = 'WEIGHT$1000'
              HeaderAlignmentHorz = taCenter
              Options.Grouping = False
              Width = 85
              Position.BandIndex = 1
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
          end
          object GridStoreLevel: TcxGridLevel
            GridView = GridStoreView
          end
        end
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'Gold'
      'Silver'
      'Delete'
      'Common')
    Categories.ItemsVisibles = (
      0
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 384
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 66
      FloatClientHeight = 104
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAddGold'
        end
        item
          Visible = True
          ItemName = 'ButtonAddSilver'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'LargeButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object ButtonAgent: TdxBarLargeButton
      Caption = #1040#1075#1077#1085#1090
      Category = 0
      Hint = #1040#1075#1077#1085#1090
      Visible = ivAlways
      LargeImageIndex = 9
      OnClick = ButtonAgentClick
    end
    object ButtonProbeGold: TdxBarLargeButton
      Tag = 1
      Caption = #1047#1086#1083#1086#1090#1086
      Category = 0
      Hint = #1047#1086#1083#1086#1090#1086
      Visible = ivAlways
      LargeImageIndex = 16
      OnClick = ButtonProbeClick
    end
    object ButtonProbeSilver: TdxBarLargeButton
      Tag = 2
      Caption = #1057#1077#1088#1077#1073#1088#1086
      Category = 0
      Hint = #1057#1077#1088#1077#1073#1088#1086
      Visible = ivAlways
      LargeImageIndex = 16
      OnClick = ButtonProbeClick
    end
    object ButtonMemo: TdxBarLargeButton
      Caption = #1055#1088#1080#1079#1085#1072#1082
      Category = 0
      Hint = #1055#1088#1080#1079#1085#1072#1082
      Visible = ivAlways
      LargeImageIndex = 17
      OnClick = ButtonMemoClick
    end
    object ButtonAddGold: TdxBarLargeButton
      Tag = 1
      Action = ActionAddGold
      Category = 1
      AutoGrayScale = False
      Width = 64
    end
    object ButtonAddSilver: TdxBarLargeButton
      Tag = 2
      Action = ActionAddSilver
      Category = 2
      AutoGrayScale = False
    end
    object ButtonDelete: TdxBarLargeButton
      Action = ActionDelete
      Category = 3
      AutoGrayScale = False
    end
    object LargeButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1042#1099#1081#1090#1080
      Category = 4
      Hint = #1042#1099#1081#1090#1080
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = LargeButtonCloseClick
      AutoGrayScale = False
      Width = 64
      SyncImageIndex = False
      ImageIndex = 9
    end
  end
  object InvoiceItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure buy$stub')
    DeleteSQL.Strings = (
      'execute procedure buy$stub')
    InsertSQL.Strings = (
      'execute procedure buy$stub')
    RefreshSQL.Strings = (
      'select'
      '  '
      '  id,'
      '  invoice$id,'
      '  material$id,'
      '  material$name,'
      '  probe$id,'
      '  probe$name,'
      '  probe$k,'
      '  memo$id,'
      '  memo$name,'
      '  weight,'
      '  weight$real,'
      '  weight$1000,'
      '  price,'
      '  cost'
      ''
      'from'
      ''
      'buy$invoice$item$s(:id) ')
    SelectSQL.Strings = (
      'select'
      ''
      '  id,'
      '  invoice$id,'
      '  material$id,'
      '  material$name,'
      '  probe$id,'
      '  probe$name,'
      '  probe$k,'
      '  memo$id,'
      '  memo$name,'
      '  weight,'
      '  weight$real,'
      '  weight$1000,'
      '  price,'
      '  cost'
      ''
      'from'
      ''
      'buy$invoice$item$a(:invoice$id)')
    AfterPost = InvoiceItemAfterPost
    BeforeEdit = InvoiceItemBeforeEdit
    BeforeOpen = InvoiceItemBeforeOpen
    BeforePost = InvoiceItemBeforePost
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    Left = 48
    Top = 448
    oStartTransaction = False
    object InvoiceItemID: TFIBIntegerField
      FieldName = 'ID'
    end
    object InvoiceItemINVOICEID: TFIBIntegerField
      FieldName = 'INVOICE$ID'
    end
    object InvoiceItemMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object InvoiceItemMATERIALNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'MATERIAL$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceItemPROBEID: TFIBIntegerField
      FieldName = 'PROBE$ID'
    end
    object InvoiceItemPROBENAME: TFIBStringField
      FieldName = 'PROBE$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object InvoiceItemMEMOID: TFIBIntegerField
      FieldName = 'MEMO$ID'
    end
    object InvoiceItemMEMONAME: TFIBStringField
      FieldName = 'MEMO$NAME'
      Size = 16
      EmptyStrToNull = True
    end
    object InvoiceItemWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object InvoiceItemWEIGHTREAL: TFIBBCDField
      FieldName = 'WEIGHT$REAL'
      Size = 3
      RoundByScale = True
    end
    object InvoiceItemWEIGHT1000: TFIBFloatField
      FieldName = 'WEIGHT$1000'
      DisplayFormat = '0.000'
    end
    object InvoiceItemPRICE: TFIBBCDField
      FieldName = 'PRICE'
      Size = 2
      RoundByScale = True
    end
    object InvoiceItemCOST: TFIBBCDField
      FieldName = 'COST'
      Size = 2
      RoundByScale = True
    end
    object InvoiceItemPROBEK: TFIBFloatField
      FieldName = 'PROBE$K'
    end
  end
  object SourceInvoiceItem: TDataSource
    DataSet = InvoiceItem
    Left = 16
    Top = 448
  end
  object Invoice: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure buy$stub')
    RefreshSQL.Strings = (
      'select'
      ''
      '  id,'
      '  reference$id,'
      '  forward$id,'
      '  backward$id,'
      '  company$id,'
      '  company$name,'
      '  department$id,'
      '  department$name,'
      '  class$code,'
      '  class$name,'
      '  route,'
      '  n,'
      '  invoice$date,'
      '  sender$id,'
      '  sender$name,'
      '  receiver$id,'
      '  receiver$name,'
      '  material$id,'
      '  material$name,'
      '  agent$id,'
      '  agent$class$code,'
      '  agent$name,'
      '  tax$id,'
      '  tax$name,'
      '  tax,'
      '  weight,'
      '  weight$real,'
      '  weight$1000,'
      '  cost,'
      '  cost$tax,'
      '  cost$total,'
      '  state$code,'
      '  state$name,'
      '  state$date,'
      '  state$by$id,'
      '  state$by$name,'
      '  foreign$n,'
      '  foreign$date,'
      '  foreign$cost'
      ''
      ''
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' )'
      '')
    SelectSQL.Strings = (
      'select'
      ''
      '  id,'
      '  reference$id,'
      '  forward$id,'
      '  backward$id,'
      '  company$id,'
      '  company$name,'
      '  department$id,'
      '  department$name,'
      '  class$code,'
      '  class$name,'
      '  route,'
      '  n,'
      '  invoice$date,'
      '  sender$id,'
      '  sender$name,'
      '  receiver$id,'
      '  receiver$name,'
      '  material$id,'
      '  material$name,'
      '  agent$id,'
      '  agent$class$code,'
      '  agent$name,'
      '  tax$id,'
      '  tax$name,'
      '  tax,'
      '  weight,'
      '  weight$real,'
      '  weight$1000,'
      '  cost,'
      '  cost$tax,'
      '  cost$total,'
      '  state$code,'
      '  state$name,'
      '  state$date,'
      '  state$by$id,'
      '  state$by$name,'
      '  foreign$n,'
      '  foreign$date,'
      '  foreign$cost'
      ''
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' )'
      '')
    AfterOpen = InvoiceAfterOpen
    BeforeOpen = InvoiceBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    Left = 48
    Top = 416
    oStartTransaction = False
    object InvoiceID: TFIBIntegerField
      FieldName = 'ID'
    end
    object InvoiceREFERENCEID: TFIBIntegerField
      FieldName = 'REFERENCE$ID'
    end
    object InvoiceFORWARDID: TFIBIntegerField
      FieldName = 'FORWARD$ID'
    end
    object InvoiceBACKWARDID: TFIBIntegerField
      FieldName = 'BACKWARD$ID'
    end
    object InvoiceCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object InvoiceCOMPANYNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'COMPANY$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object InvoiceDEPARTMENTNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'DEPARTMENT$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceCLASSCODE: TFIBIntegerField
      FieldName = 'CLASS$CODE'
    end
    object InvoiceCLASSNAME: TFIBStringField
      FieldName = 'CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceROUTE: TFIBIntegerField
      FieldName = 'ROUTE'
    end
    object InvoiceN: TFIBIntegerField
      Alignment = taCenter
      FieldName = 'N'
    end
    object InvoiceINVOICEDATE: TFIBDateField
      Alignment = taCenter
      FieldName = 'INVOICE$DATE'
    end
    object InvoiceSENDERID: TFIBIntegerField
      FieldName = 'SENDER$ID'
    end
    object InvoiceSENDERNAME: TFIBStringField
      FieldName = 'SENDER$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceRECEIVERID: TFIBIntegerField
      FieldName = 'RECEIVER$ID'
    end
    object InvoiceRECEIVERNAME: TFIBStringField
      FieldName = 'RECEIVER$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object InvoiceMATERIALNAME: TFIBStringField
      FieldName = 'MATERIAL$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceAGENTID: TFIBIntegerField
      FieldName = 'AGENT$ID'
    end
    object InvoiceAGENTCLASSCODE: TFIBIntegerField
      FieldName = 'AGENT$CLASS$CODE'
    end
    object InvoiceAGENTNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'AGENT$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceTAXID: TFIBIntegerField
      FieldName = 'TAX$ID'
    end
    object InvoiceTAXNAME: TFIBStringField
      FieldName = 'TAX$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object InvoiceTAX: TFIBIntegerField
      FieldName = 'TAX'
    end
    object InvoiceWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object InvoiceWEIGHTREAL: TFIBBCDField
      FieldName = 'WEIGHT$REAL'
      Size = 3
      RoundByScale = True
    end
    object InvoiceWEIGHT1000: TFIBBCDField
      FieldName = 'WEIGHT$1000'
      Size = 3
      RoundByScale = True
    end
    object InvoiceCOST: TFIBBCDField
      FieldName = 'COST'
      Size = 2
      RoundByScale = True
    end
    object InvoiceCOSTTAX: TFIBBCDField
      FieldName = 'COST$TAX'
      Size = 2
      RoundByScale = True
    end
    object InvoiceCOSTTOTAL: TFIBBCDField
      FieldName = 'COST$TOTAL'
      Size = 2
      RoundByScale = True
    end
    object InvoiceSTATECODE: TFIBIntegerField
      FieldName = 'STATE$CODE'
    end
    object InvoiceSTATENAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'STATE$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object InvoiceSTATEDATE: TFIBDateTimeField
      Alignment = taCenter
      FieldName = 'STATE$DATE'
    end
    object InvoiceSTATEBYID: TFIBIntegerField
      FieldName = 'STATE$BY$ID'
    end
    object InvoiceSTATEBYNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'STATE$BY$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceFOREIGNN: TFIBStringField
      FieldName = 'FOREIGN$N'
      Size = 16
      EmptyStrToNull = True
    end
    object InvoiceFOREIGNDATE: TFIBDateField
      Alignment = taCenter
      FieldName = 'FOREIGN$DATE'
    end
    object InvoiceFOREIGNCOST: TFIBBCDField
      Alignment = taCenter
      FieldName = 'FOREIGN$COST'
      Size = 2
      RoundByScale = True
    end
  end
  object SourceInvoice: TDataSource
    DataSet = Invoice
    Left = 16
    Top = 416
  end
  object LayoutLookAndFeel: TdxLayoutLookAndFeelList
    Left = 176
    Top = 448
    object dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel
    end
  end
  object Action: TActionList
    Images = DataBuy.Image32
    Left = 80
    Top = 416
    object ActionAddGold: TAction
      Caption = #1047#1086#1083#1086#1090#1086
      ImageIndex = 1
      OnExecute = ActionAddGoldExecute
      OnUpdate = ActionAddGoldUpdate
    end
    object ActionAddSilver: TAction
      Caption = #1057#1077#1088#1077#1073#1088#1086
      ImageIndex = 1
      OnExecute = ActionAddSilverExecute
      OnUpdate = ActionAddSilverUpdate
    end
    object ActionDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionDeleteExecute
      OnUpdate = ActionDeleteUpdate
    end
    object ActionPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 6
      OnExecute = ActionPrintExecute
    end
    object ActionAgent: TAction
      Caption = #1040#1075#1077#1085#1090
      ImageIndex = 9
      OnExecute = ActionAgentExecute
      OnUpdate = ActionAgentUpdate
    end
  end
  object MenuProbeGold: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 112
    Top = 384
  end
  object MenuOffice: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 48
    Top = 384
  end
  object MenuCompany: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 80
    Top = 384
  end
  object MenuMemoGold: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 144
    Top = 384
  end
  object CurrentStore: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 448
  end
  object Store: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 448
  end
  object SourceStore: TDataSource
    DataSet = Store
    Left = 144
    Top = 448
  end
  object MenuProbeSilver: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 176
    Top = 384
  end
end
