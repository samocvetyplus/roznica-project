unit frmBuyJournalAffinage;

interface

uses

  Controls, DB, Classes, ActnList,
  FIBDataSet, pFIBDataSet, StrUtils, Variants,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage,  cxDBData, dxSkinsdxBarPainter,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,
  cxGrid, cxGroupBox,
  dxSkinsCore,  dxSkinscxPCPainter, dxBar,
  frmBuyJournal, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxSplitter, cxTextEdit,
  cxMemo, cxRichEdit, GraphUtil, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

{ TFrameBuyJournalAffinage }
                                                                                         
  TFrameBuyJournalAffinage = class(TFrameBuyJournal)
    ButtonPrint103: TdxBarLargeButton;
    ButtonPrint104: TdxBarLargeButton;
    GridJournalViewLOSSESWEIGHT: TcxGridDBBandedColumn;
    GridJournalViewLOSSESPERCENT: TcxGridDBBandedColumn;
    InvoiceArrayLOSSESPERCENT: TFIBBCDField;
    InvoiceArrayLOSSESWEIGHT: TFIBBCDField;
    procedure OnLossesStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
  protected
    function GetClassCode: Integer; override;
    function GetInvoiceClass: TFrameBuyInvoiceClass; override;
    procedure CustomSetup; override;
  end;

implementation

{$R *.dfm}

uses

  uBuy, dmBuy, frmBuyInvoiceAffinage;

{ TFrameBuyJournalAffinage }

function TFrameBuyJournalAffinage.GetClassCode: Integer;
begin
  Result := 0;

  if ButtonSend.Down then
  begin
    Result := 5;
  end else

  if ButtonReceive.Down then
  begin
    Result := 6;
  end;
end;

function TFrameBuyJournalAffinage.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoiceAffinage;
end;

procedure TFrameBuyJournalAffinage.OnLossesStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;  out AStyle: TcxStyle);
var
  Index: Integer;
  Value: Variant;
begin
  AStyle := DataBuy.StyleDefault;

  Index := GridJournalViewLOSSESWEIGHT.Index;

  Value := ARecord.Values[Index];

  if not VarIsNull(Value) then
  begin
    if Value < 0 then
    begin
      AStyle := DataBuy.StyleGreen;
    end else

    if Value > 0 then
    begin
      AStyle := DataBuy.StyleRed;
    end;
  end;
end;

procedure TFrameBuyJournalAffinage.CustomSetup;
begin
  CategoryVisible['Send'] := cbTrue;

  CategoryVisible['Receive'] := cbTrue;

  if DepartmentID = DataBuy.SelfDepartmentID then
  begin

    CategoryVisible['Open + Close'] := cbTrue;

    if ButtonSend.Down then
    begin
      CategoryVisible['Add'] := cbTrue;
    end else

    if ButtonReceive.Down then
    begin
      CategoryVisible['Add'] := cbTrue;
    end;

    CategoryVisible['Delete'] := cbTrue;

    if ButtonSend.Down then
    begin
      CategoryVisible['Print'] := cbTrue;
    end;

  end else
  begin
    if ButtonSend.Down then
    begin
      CategoryVisible['Add'] := cbTrue;
    end;
  end;
                                                                                                               
  if ButtonReceive.Down then
  begin

  end;

  GridJournalView.Band['������� ��������'].Visible := ButtonReceive.Down;

  GridJournalView.Band['������'].Visible := ButtonGold.Down and ButtonReceive.Down;

  GridJournalViewCOSTTOTAL.Visible := ButtonReceive.Down;

  GridJournalViewWEIGHT.Visible := ButtonSend.Down;

  if GridJournalViewWEIGHT.Visible then
  begin
    GridJournalViewWEIGHTREAL.Caption := '�������';
  end else
  begin
    GridJournalViewWEIGHTREAL.Caption := '���';
  end;

  inherited CustomSetup;
end;


end.



