unit dmBuy;

interface

uses

  Forms, SysUtils, DB, DBClient, Provider, Messages, Windows,
  Classes, ImgList, Controls, Variants,
  FIB, FIBQuery, pFIBQuery, FIBDataSet, pFIBDataSet, IB_ErrorCodes,
  pFIBStoredProc, FIBDatabase, pFIBDatabase,
  cxLocalization, cxHint, cxContainer, cxEdit, cxLookAndFeels, cxGrid,
  cxGridCustomView, dxSkinsCore, AppEvnts, cxStyles, pFIBErrorHandler,
  dxSkinsForm, cxGraphics, dxPSEngn, cxGridTableView, cxGridExportLink,
  dxPSPrVwStd,
  dxPSPrVwAdv,
  dxPSPrVwRibbon, dxSkinsDefaultPainters, Dialogs, ActnList, frxExportODF,
  frxExportImage, frxClass, frxExportPDF, frxRes, Graphics, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;

const

  sqlOffice: string = 'select id, name from buy$office';

  sqlProbe: string = 'select id, name, probe, material$id from buy$probe(:material$id)';

  sqlMemo: string = 'select id, name from buy$memo';

  sqlAgent: string = 'select id, name from buy$agent(:flag)';

  sqlOwner: string = 'select id, name from buy$owner'; 

  sqlStore: string = 'select * from buy$store$self(:store$date, null)';

  sqlMol: string = 'select * from buy$mol';

type

{ TDataBuy }

  TDataBuy = class(TDataModule)
    Image32: TcxImageList;
    SkinController: TdxSkinController;
    EditStyleController: TcxEditStyleController;
    HintStyleController: TcxHintStyleController;
    Localizer: TcxLocalizer;
    TransactionRead: TpFIBTransaction;
    LookAndFeelController: TcxLookAndFeelController;
    TransactionWrite: TpFIBTransaction;
    ImageCalendar: TcxImageList;
    Image16: TcxImageList;
    Image24: TcxImageList;
    FIBErrorHandler: TpFibErrorHandler;
    StyleRepository: TcxStyleRepository;
    StyleBand: TcxStyle;
    ApplicationEvents: TApplicationEvents;
    PSEngineController: TdxPSEngineController;
    StyleDefault: TcxStyle;
    StyleSelected: TcxStyle;
    StyleGray: TcxStyle;
    StyleInfo: TcxStyle;
    SaveDialog: TSaveDialog;
    Actions: TActionList;
    ActionFit: TAction;
    ActionPrint: TAction;
    ActionSave: TAction;
    StyleRed: TcxStyle;
    StyleGreen: TcxStyle;
    ImageChain: TcxImageList;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure OnFIBError(Sender: TObject;  ErrorValue: EFIBError; KindIBError: TKindIBError; var DoRaise: Boolean);
    procedure ApplicationEventsShortCut(var Msg: TWMKey; var Handled: Boolean);
    procedure ActionGridUpdate(Sender: TObject);
    procedure ActionFitExecute(Sender: TObject);
    procedure ActionPrintExecute(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
  private
    FIsModern: Boolean;
    FSelfCompanyID: Integer;
    FSelfCompanyName: string;
    FSelfDepartmentName: string;
    function GetSelfPersonID: Integer;
    function GetSelfDepartmentID: Integer;
    function GetSelfDepartmentName: string;
    function GetIsCenterDepartment: Boolean;
    function GetDataBase: TFIBDatabase;
    function GetSelfCompanyName: string;
    procedure SetSelfCompanyID(const Value: Integer);
  public
    function Value(SQL: string): OleVariant; overload;
    function Value(SQL: string; Arguments: array of Variant): OleVariant; overload;
    function Row(SQL: string): OleVariant; overload;
    function Row(SQL: string; Arguments: array of Variant): OleVariant; overload;
    function Table(SQL: string): OleVariant; overload;
    function Table(SQL: string; Arguments: array of Variant): OleVariant; overload;
    procedure Execute(SQL: string); overload;
    procedure Execute(SQL: string; Arguments: array of Variant); overload;
    procedure StartTransaction;
    procedure Commit;
    procedure CommitRetaining;
    procedure Rollback;
    procedure RollbackRetaining;
    function ActiveGrid: TcxGrid;
    function MouseGrid: TcxGrid;
    property DataBase: TFIBDatabase read GetDataBase;
    property SelfPersonID: Integer read GetSelfPersonID;
    property SelfCompanyID: Integer read FSelfCompanyID write SetSelfCompanyID;
    property SelfDepartmentID: Integer read GetSelfDepartmentID;
    property SelfCompanyName: string read GetSelfCompanyName;
    property SelfDepartmentName: string read GetSelfDepartmentName;
    property IsCenterDepartment: Boolean read GetIsCenterDepartment;
    property IsModern: Boolean read FIsModern;
  end;

var
  DataBuy: TDataBuy;

implementation

uses

  ComData, Data,
  dmBuyInvoice,
  uGridPrinter, uBuy, dmBuyCollation;

{$R *.dfm}

{ TDataBuy }

type

  PDWORDArray = ^TDWORDArray;

  TDWORDArray = array[0..0] of DWORD;

  PWORDArray = ^TWORDArray;

  TWORDArray = array[0..0] of WORD;

procedure TDataBuy.DataModuleCreate(Sender: TObject);


begin
  Localizer.Active := True;

  Localizer.Locale := 1049;

  Localizer.Translate;

  StyleGreen.Color := Blend(ColorToRGB(clGreen), ColorToRGB(clSkyBlue), 60);

  StyleRed.Color := Blend(ColorToRGB(clRed), ColorToRGB(clSkyBlue), 60);

  TransactionRead.StartTransaction;

  if not IsCenterDepartment then
  begin
    SelfCompanyID := Value('select id from buy$owner$id');
  end;

  if SelfPersonID in [1] then
  begin
    FIsModern := True;
  end else

  if SelfPersonID in [3] then
  begin
    FIsModern := False;
  end else

  begin
    FIsModern := FindCmdLineSwitch('modern');

    FIsModern := True;
  end;

  FSelfDepartmentName := Trim(Value('select name from d_dep where d_depid = :department$id', [SelfDepartmentID]));

  TransactionWrite.StartTransaction;

  Execute('execute procedure buy$context$set(:name, :person$id)', ['person$id', SelfPersonID]);

  TransactionWrite.Commit;

  DataBuyInvoice := TDataBuyInvoice.Create(Self);

  DataBuyCollation := TDataBuyCollation.Create(Self);

  //PSEngineController.PreviewDialogStyle := 'Standard';

  //PSEngineController.PreviewDialogStyle := 'Advanced';

  PSEngineController.PreviewDialogStyle := 'Ribbon';
end;

procedure TDataBuy.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(DataBuyInvoice);

  if TransactionWrite.Active then
  begin
    TransactionWrite.Rollback;
  end;

  if TransactionRead.Active then
  begin
    TransactionRead.Commit;
  end;
end;

procedure TDataBuy.SetSelfCompanyID(const Value: Integer);
var
  DBValue: OleVariant;
begin
  FSelfCompanyID := Value;

  FSelfCompanyName := '';

  DBValue := Self.Value('select name from d_comp where d_compid = :company$id', [SelfCompanyID]);

  if not VarIsNull(DBValue) then
  begin
    FSelfCompanyName := Trim(DBValue);
  end;

  TransactionWrite.StartTransaction;

  Execute('execute procedure buy$context$set(:name, :company$id)', ['company$id', SelfCompanyID]);

  TransactionWrite.Commit;
end;

procedure TDataBuy.StartTransaction;
begin
  if not TransactionWrite.InTransaction then
  begin
    TransactionWrite.StartTransaction;
  end;
end;

procedure TDataBuy.Commit;
begin
  if TransactionWrite.Active then
  begin
    TransactionWrite.Commit;
  end;
end;

procedure TDataBuy.CommitRetaining;
begin
  if TransactionWrite.Active then
  begin
    TransactionWrite.CommitRetaining;
  end;
end;

procedure TDataBuy.Rollback;
begin
  if TransactionWrite.Active then
  begin
    TransactionWrite.Rollback;
  end;
end;

procedure TDataBuy.RollbackRetaining;
begin
  if TransactionWrite.Active then
  begin
    TransactionWrite.RollbackRetaining;
  end;
end;

procedure TDataBuy.OnFIBError(Sender: TObject; ErrorValue: EFIBError; KindIBError: TKindIBError; var DoRaise: Boolean);
var
  Message: TStringList;
begin
  if ErrorValue.IBErrorCode = isc_except then
  begin
    Message := TStringList.Create;

    Message.Text := ErrorValue.IBMessage;

    Message.Delete(0);

    ErrorValue.Message := Message.Text;

    Message.Free;
  end;

  DoRaise := True;
end;

function TDataBuy.GetSelfCompanyName: string;
begin
  Result := FSelfCompanyName;
end;

function TDataBuy.GetSelfDepartmentID: Integer;
begin
  Result := SelfDepId;
end;

function TDataBuy.GetSelfDepartmentName: string;
begin
  Result := FSelfDepartmentName;
end;

function TDataBuy.GetSelfPersonID: Integer;
begin
  Result := dmCom.User.UserId;
end;

function TDataBuy.GetDataBase: TFIBDatabase;
begin
  Result := TransactionRead.DefaultDatabase;
end;

function TDataBuy.GetIsCenterDepartment: Boolean;
begin
  Result := SelfDepID = CenterDepId;
end;

function TDataBuy.Value(SQL: string): OleVariant;
begin
  Result := Value(SQL, []);
end;

procedure TDataBuy.Execute(SQL: string);
begin
  Execute(SQL, []);
end;

procedure TDataBuy.Execute(SQL: string; Arguments: array of Variant);
var
  InTransaction: Boolean;
begin
  InTransaction := TransactionWrite.Active;

  if not InTransaction then
  begin
    StartTransaction;
  end;

  try

    Database.QueryValue(SQL, -1, Arguments, TransactionWrite);

    if not InTransaction then
    begin
       Commit;
    end else

    begin
      CommitRetaining;
    end;

  except

    on E: Exception do
    begin
      Rollback;

      raise;
    end;

  end;
end;

function TDataBuy.Value(SQL: string; Arguments: array of Variant): OleVariant;
begin
  Result := TransactionRead.DefaultDatabase.QueryValue(SQL, 0, Arguments, TransactionRead);
end;

function TDataBuy.Row(SQL: string): OleVariant;
begin
  Result := Row(SQL, []);
end;

function TDataBuy.Row(SQL: string; Arguments: array of Variant): OleVariant;
begin
  Result := TransactionRead.DefaultDatabase.QueryValues(SQL, Arguments, TransactionRead);
end;

function TDataBuy.Table(SQL: string): OleVariant;
begin
  Result := Table(SQL, []);
end;

function TDataBuy.Table(SQL: string; Arguments: array of Variant): OleVariant;
var
  i: Integer;
  DBDataSet: TpFIBDataSet;
  DataSet: TClientDataSet;
  ProviderDataSet: TDataSetProvider;
begin
  Result := Null;

  DBDataSet := TpFIBDataSet.Create(Self);

  DBDataSet.Database := TransactionRead.DefaultDatabase;

  DBDataSet .SelectSQL.Text := SQL;

  ProviderDataSet := TDataSetProvider.Create(Self);

  ProviderDataSet.Name := 'ProviderDataSet';

  ProviderDataSet.DataSet := DBDataSet;

  DataSet := TClientDataSet.Create(Self);

  DataSet.ProviderName := ProviderDataSet.Name;

  try

    if Length(Arguments) <> 0 then
    begin
      DataSet.FetchParams;
    end;

    for i := Low(Arguments) to High(Arguments) do
    begin
      DataSet.Params[i].Value := Arguments[i];
    end;

    DataSet.Active := True;

  except

  end;

  Result := DataSet.Data;

  DataSet.Active := False;

  DataSet.Free;

  ProviderDataSet.Free;

  DBDataSet.Free;
end;

function TDataBuy.ActiveGrid: TcxGrid;
var
  Grid: TcxGrid;
  GridView: TcxCustomGridView;
  GridSite: TcxGridSite;
  Component: TComponent;
  ActiveForm: TForm;
  Control: TWinControl;
  Handle: HWND;
begin
  Grid := nil;

  Handle :=  Application.ActiveFormHandle;

  if Handle <> 0 then
  begin
    ActiveForm := TForm(FindControl(Handle));

    if ActiveForm <> nil then
    begin
      Control := ActiveForm.ActiveControl;

      if Control is TcxGridSite then
      begin
        GridSite := TcxGridSite(Control);

        GridView := GridSite.GridView;

        Component := GridView.GetParentComponent;

        Grid := TcxGrid(Component);
      end;
    end;
  end;

  Result := Grid;
end;

function TDataBuy.MouseGrid: TcxGrid;
var
  Grid: TcxGrid;
  GridView: TcxCustomGridView;
  GridSite: TcxGridSite;
  Component: TComponent;
  Control: TWinControl;
  Point: TPoint;
  Handle: HWND;
begin
  Grid := nil;

  Point := Mouse.CursorPos;

  Handle := WindowFromPoint(Point);

  if Handle <> 0 then
  begin
    Control := FindControl(Handle);

    if Control <> nil then
    begin
      if Control is TcxGridSite then
      begin
        GridSite := TcxGridSite(Control);

        GridView := GridSite.GridView;

        Component := GridView.GetParentComponent;

        Grid := TcxGrid(Component);
      end;
    end;
  end;

  Result := Grid;
end;

procedure TDataBuy.ActionGridUpdate(Sender: TObject);
var
  Grid: TcxGrid;
  Action: TAction;
begin
  Grid := DataBuy.ActiveGrid;

  Action := TAction(Sender);

  Action.Enabled := Grid <> nil;
end;


procedure TDataBuy.ApplicationEventsShortCut(var Msg: TWMKey;  var Handled: Boolean);
var
Grid: TcxGrid;

function DiscoverGrid: TcxGrid;
var
  Grid: TcxGrid;
begin
  Grid := ActiveGrid;

  if Grid = nil then
  begin
    Grid := MouseGrid;
  end;

  Result := Grid;
end;

begin

  if Msg.CharCode = VK_F1  then
  begin
    ActionPrintExecute(nil);

    Handled := True;
  end else

  if Msg.CharCode = VK_F2  then
  begin
    ActionSaveExecute(nil);

    Handled := True;
  end else


  if Msg.CharCode = VK_F12  then
  begin
    ActionFitExecute(nil);

    Handled := True;
  end;

end;

procedure TDataBuy.ActionPrintExecute(Sender: TObject);

var
  Grid: TcxGrid;

function DiscoverGrid: TcxGrid;
var
  Grid: TcxGrid;
begin
  Grid := ActiveGrid;

  if Grid = nil then
  begin
    Grid := MouseGrid;
  end;

  Result := Grid;
end;

begin
  Grid := DiscoverGrid;

  if Grid <> nil then
  begin
    //TcxGridTableView(Grid.ActiveView).ApplyBestFit;
      
    TGridPrinter.Print(Grid);
  end;
end;

procedure TDataBuy.ActionSaveExecute(Sender: TObject);
var
  Grid: TcxGrid;
  FileName: string;

function DiscoverGrid: TcxGrid;
var
  Grid: TcxGrid;
begin
  Grid := ActiveGrid;

  if Grid = nil then
  begin
    Grid := MouseGrid;
  end;

  Result := Grid;
end;

begin
  Grid := DiscoverGrid;

  if Grid <> nil then
  begin
    if SaveDialog.Execute then
    begin
      //TcxGridTableView(Grid.ActiveView).ApplyBestFit;

      if SaveDialog.FilterIndex = 1 then
      begin
        FileName := ChangeFileExt(SaveDialog.FileName, '.xls');

        TGridPrinter.PrintToXLS(Grid, SaveDialog.FileName);
      end else

      if SaveDialog.FilterIndex = 2 then
      begin
        FileName := ChangeFileExt(SaveDialog.FileName, '.pdf');

        TGridPrinter.PrintToPDF(Grid, FileName);
      end;
    end;
  end;
end;

procedure TDataBuy.ActionFitExecute(Sender: TObject);
var
  Grid: TcxGrid;

function DiscoverGrid: TcxGrid;
var
  Grid: TcxGrid;
begin
  Grid := ActiveGrid;

  if Grid = nil then
  begin
    Grid := MouseGrid;
  end;

  Result := Grid;
end;

begin
  Grid := DiscoverGrid;

  if Grid <> nil then
  begin
    TcxGridTableView(Grid.ActiveView).ApplyBestFit;
  end;
end;




end.



