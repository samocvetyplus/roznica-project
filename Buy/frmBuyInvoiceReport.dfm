object DialogBuyInvoiceReport: TDialogBuyInvoiceReport
  Left = 0
  Top = 0
  Caption = #1054#1090#1095#1077#1090
  ClientHeight = 502
  ClientWidth = 188
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport: TfrxReport
    Version = '4.9.72'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.AllowEdit = False
    PreviewOptions.Buttons = [pbPrint, pbExport, pbZoom, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PreviewOptions.ZoomMode = zmWholePage
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38959.627749745400000000
    ReportOptions.LastChange = 42531.632071076390000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    OnUserFunction = OnfrxReportUserFunction
    Left = 32
    Top = 48
  end
  object frxHeader: TfrxDBDataset
    UserName = #1047#1072#1075#1086#1083#1086#1074#1086#1082
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Header
    BCDToCurrency = True
    Left = 32
    Top = 216
  end
  object frxBody: TfrxDBDataset
    UserName = #1058#1077#1083#1086
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Body
    BCDToCurrency = True
    Left = 32
    Top = 248
  end
  object Company: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 88
    object CompanyName: TStringField
      FieldName = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      Size = 64
    end
    object CompanyBoss: TStringField
      FieldName = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
      Size = 32
    end
    object CompanyBooker: TStringField
      FieldName = #1041#1091#1093#1075#1072#1083#1090#1077#1088
      Size = 32
    end
    object CompanyResponsiblePerson: TStringField
      FieldName = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
      Size = 32
    end
    object CompanyStringField: TStringField
      FieldName = #1040#1076#1088#1077#1089
      Size = 128
    end
    object CompanyField7: TStringField
      FieldName = #1055#1086#1095#1090#1086#1074#1099#1081' '#1072#1076#1088#1077#1089
      Size = 128
    end
    object CompanyField2: TStringField
      FieldName = #1048#1053#1053
      Size = 32
    end
    object CompanyField: TStringField
      FieldName = #1054#1050#1055#1054
      Size = 32
    end
    object CompanyField5: TStringField
      FieldName = #1041#1072#1085#1082
      Size = 64
    end
    object CompanyField3: TStringField
      FieldName = #1041#1048#1050
      Size = 32
    end
    object CompanyField4: TStringField
      FieldName = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1089#1095#1077#1090
      Size = 32
    end
    object CompanyField6: TStringField
      FieldName = #1050#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1090#1089#1082#1080#1081' '#1089#1095#1077#1090
      Size = 32
    end
    object CompanyField8: TStringField
      FieldName = #1043#1086#1088#1086#1076
    end
    object CompanyField9: TIntegerField
      FieldName = #1048#1085#1076#1077#1082#1089
    end
  end
  object Header: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    ProviderName = 'ProviderHeader'
    StoreDefs = True
    Left = 64
    Top = 216
    object HeaderN: TIntegerField
      FieldName = #8470
    end
    object HeaderDate: TDateField
      FieldName = #1044#1072#1090#1072
    end
    object HeaderMaterial: TStringField
      FieldName = #1052#1072#1090#1077#1088#1080#1072#1083
      Size = 32
    end
    object HeaderWeight: TCurrencyField
      FieldName = #1063#1080#1089#1090#1099#1081
    end
    object HeaderWeightReal: TCurrencyField
      FieldName = #1043#1088#1103#1079#1085#1099#1081
    end
    object HeaderWeight1000: TCurrencyField
      FieldName = '1000'
    end
    object HeaderCost: TCurrencyField
      FieldName = #1057#1090#1086#1080#1084#1086#1089#1090#1100
    end
    object HeaderStoreDate: TDateField
      FieldName = #1044#1072#1090#1072' '#1089#1082#1083#1072#1076#1072
    end
  end
  object Body: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    ProviderName = 'ProviderBody'
    StoreDefs = True
    OnCalcFields = BodyCalcFields
    Left = 64
    Top = 248
    object BodyDate: TDateField
      FieldName = #1044#1072#1090#1072
    end
    object BodyMaterial: TStringField
      FieldName = #1052#1072#1090#1077#1088#1080#1072#1083
      Size = 32
    end
    object BodyProbe: TStringField
      FieldName = #1055#1088#1086#1073#1072
      Size = 8
    end
    object BodyProbePercent: TCurrencyField
      FieldName = #1055#1088#1086#1073#1072'.%'
      DisplayFormat = '00.0#'
    end
    object BodyWeightReal: TCurrencyField
      FieldName = #1043#1088#1103#1079#1085#1099#1081
    end
    object BodyWeightRealActual: TCurrencyField
      FieldName = #1043#1088#1103#1079#1085#1099#1081'.'#1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1081
    end
    object BodyWeight: TCurrencyField
      FieldName = #1063#1080#1089#1090#1099#1081
    end
    object BodyWeightActual: TCurrencyField
      FieldName = #1063#1080#1089#1090#1099#1081'.'#1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1081
    end
    object BodyWeight1000: TCurrencyField
      FieldName = '1000'
    end
    object BodyWeight1000Actual: TCurrencyField
      FieldName = '1000.'#1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1081
    end
    object BodyPrice: TCurrencyField
      FieldName = #1062#1077#1085#1072
    end
    object BodyCost: TCurrencyField
      FieldName = #1057#1090#1086#1080#1084#1086#1089#1090#1100
    end
    object BodyGroup: TIntegerField
      FieldKind = fkCalculated
      FieldName = #1043#1088#1091#1087#1087#1072
      Calculated = True
    end
  end
  object frxFooter: TfrxDBDataset
    UserName = #1055#1086#1076#1074#1072#1083
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Footer
    BCDToCurrency = True
    Left = 32
    Top = 280
  end
  object Footer: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 280
    object Footerstub: TIntegerField
      FieldName = 'stub'
    end
  end
  object frxSenderCompany: TfrxDBDataset
    UserName = #1050#1086#1084#1087#1072#1085#1080#1103
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Company
    BCDToCurrency = True
    Left = 32
    Top = 88
  end
  object frxReceiverCompany: TfrxDBDataset
    UserName = #1040#1075#1077#1085#1090'.'#1050#1086#1084#1087#1072#1085#1080#1103
    CloseDataSource = False
    OpenDataSource = False
    DataSet = AgentCompany
    BCDToCurrency = True
    Left = 32
    Top = 120
  end
  object AgentCompany: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 120
    object AgentCompanyStringField: TStringField
      FieldName = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      Size = 64
    end
    object AgentCompanyStringField2: TStringField
      FieldName = #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100
      Size = 32
    end
    object AgentCompanyStringField3: TStringField
      FieldName = #1041#1091#1093#1075#1072#1083#1090#1077#1088
      Size = 32
    end
    object AgentCompanyStringField4: TStringField
      FieldName = #1040#1076#1088#1077#1089
      Size = 128
    end
    object AgentCompanyStringField5: TStringField
      FieldName = #1055#1086#1095#1090#1086#1074#1099#1081' '#1072#1076#1088#1077#1089
      Size = 128
    end
    object AgentCompanyStringField6: TStringField
      FieldName = #1048#1053#1053
      Size = 32
    end
    object AgentCompanyStringField7: TStringField
      FieldName = #1054#1050#1055#1054
      Size = 32
    end
    object AgentCompanyStringField8: TStringField
      FieldName = #1041#1072#1085#1082
      Size = 64
    end
    object AgentCompanyStringField9: TStringField
      FieldName = #1041#1048#1050
      Size = 32
    end
    object AgentCompanyStringField10: TStringField
      FieldName = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1089#1095#1077#1090
      Size = 32
    end
    object AgentCompanyStringField11: TStringField
      FieldName = #1050#1086#1088#1088#1077#1089#1087#1086#1085#1076#1077#1085#1090#1089#1082#1080#1081' '#1089#1095#1077#1090
      Size = 32
    end
    object AgentCompanyField: TStringField
      FieldName = #1044#1086#1075#1086#1074#1086#1088
      Size = 32
    end
    object AgentCompanyField2: TDateField
      FieldName = #1044#1072#1090#1072' '#1076#1086#1075#1086#1074#1086#1088#1072
    end
    object AgentCompanyField3: TStringField
      FieldName = #1043#1086#1088#1086#1076
    end
    object AgentCompanyField4: TIntegerField
      FieldName = #1048#1085#1076#1077#1082#1089
    end
  end
  object frxReceiverOffice: TfrxDBDataset
    UserName = #1040#1075#1077#1085#1090'.'#1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
    CloseDataSource = False
    OpenDataSource = False
    DataSet = AgentOffice
    BCDToCurrency = True
    Left = 32
    Top = 184
  end
  object AgentOffice: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 184
    object AgentOfficeName: TStringField
      FieldName = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      Size = 64
    end
  end
  object Office: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 152
    object OfficeName: TStringField
      FieldName = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      Size = 64
    end
    object OfficeFieldAbbreviation: TStringField
      FieldName = #1040#1073#1073#1088#1077#1074#1080#1072#1090#1091#1088#1072
      Size = 32
    end
  end
  object frxSenderOffice: TfrxDBDataset
    UserName = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Office
    BCDToCurrency = True
    Left = 32
    Top = 152
  end
  object frxImage: TfrxDBDataset
    UserName = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Image
    BCDToCurrency = True
    Left = 32
    Top = 312
  end
  object Image: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 312
    object ImageImage: TGraphicField
      FieldName = 'Image'
      BlobType = ftGraphic
    end
  end
  object Material: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 48
  end
  object Member: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 344
  end
  object frxMember: TfrxDBDataset
    UserName = #1063#1083#1077#1085#1099
    CloseDataSource = False
    OpenDataSource = False
    DataSet = Member
    BCDToCurrency = True
    Left = 32
    Top = 344
  end
  object ChairMan: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 376
  end
  object frxChairMan: TfrxDBDataset
    UserName = #1055#1088#1077#1076#1089#1077#1076#1072#1090#1077#1083#1100
    CloseDataSource = False
    OpenDataSource = False
    DataSet = ChairMan
    BCDToCurrency = True
    Left = 32
    Top = 376
  end
  object mol: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 408
  end
  object frxmol: TfrxDBDataset
    UserName = #1052#1054#1051
    CloseDataSource = False
    OpenDataSource = False
    DataSet = mol
    BCDToCurrency = True
    Left = 32
    Top = 408
  end
  object mol0: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 64
    Top = 440
  end
  object frxmol0: TfrxDBDataset
    UserName = #1052#1054#1051'0'
    CloseDataSource = False
    OpenDataSource = False
    DataSet = mol0
    BCDToCurrency = True
    Left = 32
    Top = 440
  end
end
