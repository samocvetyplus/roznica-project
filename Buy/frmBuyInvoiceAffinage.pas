unit frmBuyInvoiceAffinage;

interface

uses

  Classes, ActnList, Controls, DB, Windows,
  FIBDataSet, pFIBDataSet, Variants,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid,
  dxLayoutControl, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxGroupBox,
  dxSkinsCore, dxLayoutcxEditAdapters, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxBar, dxLayoutLookAndFeels,
  frmBuyInvoice, cxHeader, Menus, StdCtrls, cxButtons, cxSplitter, DBClient,
  dxSkinsDefaultPainters, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, cxCheckBox, cxGridDBTableView,
  cxCalendar, cxButtonEdit, cxPC, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

{ TFrameBuyInvoiceAffinage }

  TFrameBuyInvoiceAffinage = class(TFrameBuyInvoice)
    SourceChainOut: TDataSource;
    ChainOut: TClientDataSet;
    InvoiceGROUPID: TFIBIntegerField;
    SheetChain: TcxTabSheet;
    GridChainOut: TcxGrid;
    GridChainOutView: TcxGridDBBandedTableView;
    GridChainOutLevel: TcxGridLevel;
    GridChainOutViewMark: TcxGridDBBandedColumn;
    GridChainOutViewDate: TcxGridDBBandedColumn;
    GridChainOutViewN: TcxGridDBBandedColumn;
    GridChainOutViewOffice: TcxGridDBBandedColumn;
    GridChainOutViewWeight: TcxGridDBBandedColumn;
    GridChainIn: TcxGrid;
    GridChainInView: TcxGridDBBandedTableView;
    GridChainInViewMark: TcxGridDBBandedColumn;
    cxGridDBBandedColumn2: TcxGridDBBandedColumn;
    cxGridDBBandedColumn3: TcxGridDBBandedColumn;
    cxGridDBBandedColumn4: TcxGridDBBandedColumn;
    cxGridDBBandedColumn5: TcxGridDBBandedColumn;
    GridChainInLevel: TcxGridLevel;
    ChainIn: TClientDataSet;
    SourceChainIn: TDataSource;
    BarExit: TdxBar;
    BarExitDock: TdxBarDockControl;
    procedure ActionAgentUpdate(Sender: TObject);
    procedure GridChainOutViewCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridChainInViewCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
  private
    GroupID: Integer;
    AgentID: Integer;
    CompanyID: Integer;
    MarkCount: Integer;
    procedure ActivateChain;
    procedure SetChainInMark;
    procedure SetChainOutMark;
  protected
    class function AgentBit: TAgentBit; override;
    function InternalActivate: Boolean; override;
  end;

var
  FrameBuyInvoiceAffinage: TFrameBuyInvoiceAffinage;

implementation

{$R *.dfm}

uses

  dmBuy, uBuy, uDialog;

{ TFrameBuyInvoiceAffinage }

class function TFrameBuyInvoiceAffinage.AgentBit: TAgentBit;
begin
  Result := abAffinage;
end;

function TFrameBuyInvoiceAffinage.InternalActivate: Boolean;
begin
  Result := False;

  SheetChain.TabVisible := False;

  if ClassCode = 6 then
  begin
    LayoutGroupForeign.Tag := 1;
  end;

  if inherited InternalActivate then
  begin
    AgentID := InvoiceAGENTID.AsInteger;

    ActivateChain;

    if ClassCode = 5 then
    begin

    end else

    if ClassCode = 6 then
    begin
      if MaterialID = 1 then
      begin
        SheetChain.TabVisible := True;

        GridChainIn.Width := SheetChain.ClientWidth div 2;

        GridChainOut.Width := SheetChain.ClientWidth - GridChainIn.Width;
      end;

      GridInvoiceItemViewWEIGHT.Visible := False;

      GridInvoiceItemViewWEIGHTREAL.Caption := '���';
    end;

    Result := True;
  end;
end;

procedure TFrameBuyInvoiceAffinage.ActionAgentUpdate(Sender: TObject);
begin
  if not ReadOnly then
  begin
    if MarkCount <> 0 then ActionAgent.Enabled := False
    else inherited;
  end else
  begin
    ActionAgent.Enabled := False;
  end;
end;

procedure TFrameBuyInvoiceAffinage.SetChainOutMark;
var
  Mark: Variant;
  OutGroupID: Integer;
begin
  ChainOut.First;

  while not ChainOut.Eof do
  begin
    OutGroupID := ChainOut.FieldByName('group$id').AsInteger;

    ChainOut.Edit;

    Mark := Null;

    if OutGroupID = 0 then
    begin
      Mark := 0;
    end else

    if OutGroupID = GroupID then
    begin
      Mark := 1;

      Inc(MarkCount);
    end;

    ChainOut.FieldByName('mark').Value := Mark;

    ChainOut.Post;


    ChainOut.Next;
  end;

  ChainOut.First;
end;

procedure TFrameBuyInvoiceAffinage.SetChainInMark;
var
  Mark: Variant;
  InID: Integer;
  InGroupID: Integer;
begin
  ChainIn.First;

  while not ChainIn.Eof do
  begin
    InID := ChainIn.FieldByName('id').AsInteger;

    InGroupID := ChainIn.FieldByName('group$id').AsInteger;

    ChainIn.Edit;

    Mark := Null;

    if InID <> ID then
    begin
      if InGroupID = GroupID then
      begin
        Mark := 1;
      end else
      if InGroupID = InID then
      begin
        Mark := 0;
      end;
    end;

    ChainIn.FieldByName('mark').Value := Mark;

    ChainIn.Post;

    ChainIn.Next;
  end;

  ChainIn.First;
end;


procedure TFrameBuyInvoiceAffinage.ActivateChain;
var
  SQL: string;
begin
  MarkCount := 0;

  if ChainOut.Active then
  begin
    ChainOut.Active := False;
  end;

  if AgentID <> 0 then
  begin
    CompanyID := DataBuy.SelfCompanyID;

    GroupID := InvoiceGROUPID.AsInteger;

    SQL :=

    ' select ii.id, i.group$id, ii.n, cast(ii.invoice$date as date) invoice$date, ii.department$name, ii.weight$real, cast(0 as integer) as mark ' +
    ' from buy$invoice i left join buy$invoice$s(i.id) ii on 1 = 1 ' +
    ' where i.company$id = :company$id and i.class$code = 5 and i.state$code in (0, 1, 2) and i.agent$id = :agent$id and ii.material$id = :material$id order by i.invoice$date desc ';

    ChainOut.Data := DataBuy.Table(SQL, [CompanyID, AgentID, MaterialID]);

    SetChainOutMark;

    SQL :=

    ' select ii.id, i.group$id, ii.n, cast(ii.invoice$date as date) invoice$date, ii.department$name, ii.weight$real, cast(0 as integer) as mark ' +
    ' from buy$invoice i left join buy$invoice$s(i.id) ii on 1 = 1 ' +
    ' where i.company$id = :company$id and i.class$code = 6 and i.state$code in (0, 1, 2) and i.agent$id = :agent$id and ii.material$id = :material$id order by i.invoice$date desc ';

    ChainIn.Data := DataBuy.Table(SQL, [CompanyID, AgentID, MaterialID]);

    SetChainInMark;

    GridChainOutView.Controller.TopRowIndex := 0;

    GridChainOutView.Controller.FocusedRowIndex := 0;

    GridChainInView.Controller.TopRowIndex := 0;

    GridChainInView.Controller.FocusedRowIndex := 0;
  end;
end;

procedure TFrameBuyInvoiceAffinage.GridChainInViewCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  Mark: Variant;
  InID: Integer;
  SQL: string;
begin
  if ReadOnly then
  begin
    Exit;
  end;

  if ACellViewInfo.Item = GridChainInViewMark then
  begin
    Mark := ChainIn.FieldByName('mark').Value;

    if not VarIsNull(Mark) then
    begin
     if Mark = 1 then
     begin
       if TDialog.Confirmation('�� �������, ��� �� ������ ����������� ��������?') = mrYes then
       begin
         Mark := 0;

         InID := ChainIn.FieldByName('id').AsInteger;

         SQL := 'update buy$invoice set group$id = :group$id where id = :id';

         DataBuy.Execute(SQL, [InID, InID]);

         ChainIn.Edit;

         ChainIn.FieldByName('mark').Value := Mark;

         ChainIn.Post;
       end;
     end else
     begin
       if TDialog.Confirmation('�� �������, ��� �� ������ ������������ ��������?') = mrYes then
       begin
         Mark := 1;

         InID := ChainIn.FieldByName('id').AsInteger;

         SQL := 'update buy$invoice set group$id = :group$id where group$id = :id';

         DataBuy.Execute(SQL, [GroupID, InID]);

         SQL :=

         ' select ii.id, i.group$id, ii.n, cast(ii.invoice$date as date) invoice$date, ii.department$name, ii.weight$real, cast(0 as integer) as mark ' +
         ' from buy$invoice i left join buy$invoice$s(i.id) ii on 1 = 1 ' +
         ' where i.company$id = :company$id and i.class$code = 5 and i.state$code in (0, 1, 2) and i.agent$id = :agent$id and ii.material$id = :material$id order by i.invoice$date desc ';

         ChainOut.Data := DataBuy.Table(SQL, [CompanyID, AgentID, MaterialID]);

         SetChainOutMark;

         ChainIn.Edit;

         ChainIn.FieldByName('mark').Value := Mark;

         ChainIn.Post;
       end;
     end;

    end;
  end;
end;

procedure TFrameBuyInvoiceAffinage.GridChainOutViewCellClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
  Mark: Variant;
  OutID: Integer;
  SQL: string;
begin
  if ReadOnly then
  begin
    Exit;
  end;

  if ACellViewInfo.Item = GridChainOutViewMark then
  begin
    Mark := ChainOut.FieldByName('mark').Value;

    if not VarIsNull(Mark) then
    begin
     ChainOut.Edit;

     if Mark = 1 then
     begin
       Mark := 0;

       Dec(MarkCount);
     end else
     begin
       Mark := 1;

       Inc(MarkCount);
     end;

     OutID := ChainOut.FieldByName('id').AsInteger;

     SQL := 'update buy$invoice set group$id = :group$id where id = :id';

     if Mark = 1 then
     begin
       DataBuy.Execute(SQL, [GroupID, OutID]);
     end else
     begin
       DataBuy.Execute(SQL, [0, OutID]);
     end;

     ChainOut.FieldByName('mark').Value := Mark;

     ChainOut.Post;

    end;

  end;

end;

end.







