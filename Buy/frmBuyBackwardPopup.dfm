inherited DialogBuyBackwardPopup: TDialogBuyBackwardPopup
  Caption = ''
  ClientHeight = 298
  ClientWidth = 454
  ShowHint = True
  ExplicitWidth = 454
  ExplicitHeight = 298
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 4
    ExplicitWidth = 196
    ExplicitHeight = 396
    Height = 298
    Width = 454
    object Layout: TdxLayoutControl
      Left = 2
      Top = 2
      Width = 450
      Height = 294
      Align = alClient
      ParentBackground = True
      TabOrder = 0
      TabStop = False
      ExplicitWidth = 192
      ExplicitHeight = 392
      object BarDockControlBottom: TdxBarDockControl
        Left = 360
        Top = 243
        Width = 80
        Height = 38
        Align = dalTop
        BarManager = BarManager
      end
      object Grid: TcxGrid
        Left = 10
        Top = 10
        Width = 243
        Height = 215
        TabOrder = 0
        object GridView: TcxGridDBBandedTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.NoDataToDisplayInfoText = '...'
          OptionsView.GroupByBox = False
          Bands = <
            item
            end>
        end
        object GridLevel: TcxGridLevel
          GridView = GridView
        end
      end
      object LayoutGroup: TdxLayoutGroup
        AlignHorz = ahClient
        AlignVert = avClient
        CaptionOptions.Visible = False
        ButtonOptions.Buttons = <>
        Hidden = True
        ShowBorder = False
        object LayoutItemGrid: TdxLayoutItem
          Control = Grid
          ControlOptions.ShowBorder = False
        end
        object LayoutSeparatorItem: TdxLayoutSeparatorItem
          CaptionOptions.Text = 'Separator'
          SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
          SizeOptions.SizableHorz = False
          SizeOptions.SizableVert = False
        end
        object LayoutItemBarBottom: TdxLayoutItem
          AlignHorz = ahRight
          AlignVert = avTop
          Control = BarDockControlBottom
          ControlOptions.AutoColor = True
          ControlOptions.ShowBorder = False
        end
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.ImageCalendar
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 24
    Top = 304
    DockControlHeights = (
      0
      0
      0
      0)
    object BarBottom: TdxBar
      BorderStyle = bbsNone
      Caption = 'BarBottom'
      CaptionButtons = <>
      DockControl = BarDockControlBottom
      DockedDockControl = BarDockControlBottom
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 378
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 76
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOk'
        end
        item
          Visible = True
          ItemName = 'ButtonCancel'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ButtonOk: TdxBarButton
      Align = iaRight
      Caption = 'OK'
      Category = 0
      Visible = ivAlways
      ImageIndex = 4
      LargeImageIndex = 4
      OnClick = ButtonOkClick
    end
    object ButtonCancel: TdxBarButton
      Align = iaRight
      Caption = #1054#1090#1084#1077#1085#1072
      Category = 0
      Hint = #1054#1090#1084#1077#1085#1072
      Visible = ivAlways
      LargeImageIndex = 5
      OnClick = ButtonCancelClick
    end
  end
end
