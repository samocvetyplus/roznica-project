object BuyOrdersData: TBuyOrdersData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 347
  Width = 359
  object OrdersList: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure Buy$Orders$U(:Order$ID, :Order$Date, :Departme' +
        'ntID, :User$ID, :Order$State$Code)')
    DeleteSQL.Strings = (
      'execute procedure Buy$Orders$D(:Order$ID)')
    InsertSQL.Strings = (
      
        'execute procedure Buy$Orders$I(:Order$ID, :Order$Date, :Departme' +
        'ntID, :User$ID, :Order$Number)')
    RefreshSQL.Strings = (
      'select '
      '  Order$Number,'
      '  Order$Date,'
      '  Creation$Date,'
      '  Modify$Date,'
      '  User$ID,'
      '  DepartmentID,'
      '  Order$State,'
      '  Department,'
      '  User$Name,'
      '  Order$State$Code'
      'from'
      '  Buy$Orders$R(:Order$ID)')
    SelectSQL.Strings = (
      'select '
      '  Order$ID,'
      '  Order$Number,'
      '  Order$Date,'
      '  Creation$Date,'
      '  Modify$Date,'
      '  User$ID,'
      '  DepartmentID,'
      '  Order$State,'
      '  Department,'
      '  User$Name,'
      '  Order$State$Code'
      'from'
      '  Buy$Orders$S(:RangeStart, :RangeEnd)'
      'order by '
      '  Creation$Date desc')
    AfterCancel = RollbackRetaining
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = OrdersListBeforeOpen
    OnEditError = PostError
    OnNewRecord = OrdersListNewRecord
    OnPostError = PostError
    Transaction = Transaction
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 224
    object OrdersListORDERID: TFIBIntegerField
      FieldName = 'ORDER$ID'
    end
    object OrdersListORDERNUMBER: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'ORDER$NUMBER'
    end
    object OrdersListORDERDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'ORDER$DATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object OrdersListCREATIONDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
      FieldName = 'CREATION$DATE'
    end
    object OrdersListMODIFYDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
      FieldName = 'MODIFY$DATE'
    end
    object OrdersListUSERID: TFIBIntegerField
      FieldName = 'USER$ID'
    end
    object OrdersListORDERSTATE: TFIBStringField
      DisplayLabel = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      FieldName = 'ORDER$STATE'
      Size = 16
      EmptyStrToNull = True
    end
    object OrdersListDEPARTMENT: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldName = 'DEPARTMENT'
      EmptyStrToNull = True
    end
    object OrdersListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USER$NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object OrdersListORDERSTATECODE: TFIBIntegerField
      FieldName = 'ORDER$STATE$CODE'
    end
    object OrdersListDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENTID'
    end
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dmCom.db
    TimeoutAction = TARollback
    TRParams.Strings = (
      'write'
      'isc_tpb_nowait'
      'read_committed'
      'rec_version')
    TPBMode = tpbDefault
    Left = 16
    Top = 272
  end
  object D_Probe: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 16
    Top = 8
  end
  object D_Memo: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 16
    Top = 56
  end
  object DataSourceOrders: TDataSource
    DataSet = OrdersList
    Left = 96
    Top = 224
  end
  object Order: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ORDER_ITEM'
      'SET '
      '    ORDERID = :ORDERID,'
      '    PROBEID = :PROBEID,'
      '    MEMOID = :MEMOID,'
      '    PRICE = :PRICE,'
      '    PRICE2 = :PRICE2'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ORDER_ITEM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ORDER_ITEM('
      '    ID,'
      '    ORDERID,'
      '    PROBEID,'
      '    MEMOID,'
      '    PRICE,'
      '    PRICE2'
      ')'
      'VALUES('
      '    :ID,'
      '    :ORDERID,'
      '    :PROBEID,'
      '    :MEMOID,'
      '    :PRICE,'
      '    :PRICE2'
      ')')
    RefreshSQL.Strings = (
      'select'
      '  oi.ID,'
      '  oi.OrderID,'
      '  oi.ProbeID,'
      '  oi.MemoID,'
      '  oi.Price,'
      '  oi.Price2'
      'from '
      '  Order_Item oi'
      'where( '
      '  oi.OrderID = :OrderID'
      '     ) and (     OI.ID = :OLD_ID'
      '     )   ')
    SelectSQL.Strings = (
      'select'
      '  oi.ID,'
      '  oi.OrderID,'
      '  oi.ProbeID,'
      '  oi.MemoID,'
      '  oi.Price,'
      '  oi.Price2'
      'from '
      '  Order_Item oi'
      'where'
      '  oi.OrderID = ?Order$ID'
      'order by'
      '  oi.ProbeID, oi.MemoID')
    AfterCancel = RollbackRetaining
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = OrderBeforeClose
    OnNewRecord = OrderNewRecord
    Transaction = Transaction
    Database = dmCom.db
    DataSource = DataSourceOrders
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 176
    WaitEndMasterScroll = True
    dcForceOpen = True
    object OrderID: TFIBIntegerField
      FieldName = 'ID'
    end
    object OrderORDERID: TFIBIntegerField
      FieldName = 'ORDERID'
    end
    object OrderPROBEID: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1073#1072
      FieldName = 'PROBEID'
      OnChange = OrderItemChange
    end
    object OrderMEMOID: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1079#1085#1072#1082
      FieldName = 'MEMOID'
      OnChange = OrderItemChange
    end
    object OrderPRICE: TFIBBCDField
      DisplayLabel = #1057#1082#1091#1087#1082#1072
      FieldName = 'PRICE'
      Size = 2
      RoundByScale = True
    end
    object OrderPRICE2: TFIBBCDField
      DisplayLabel = #1054#1073#1084#1077#1085
      FieldName = 'PRICE2'
      Size = 2
      RoundByScale = True
    end
  end
  object DataSourceOrder: TDataSource
    DataSet = Order
    Left = 96
    Top = 176
  end
  object quTemp: TpFIBQuery
    Transaction = Transaction
    Database = dmCom.db
    Left = 96
    Top = 272
  end
  object DataSourceProbe: TDataSource
    DataSet = D_Probe
    Left = 96
    Top = 8
  end
  object DataSourceMemo: TDataSource
    DataSet = D_Memo
    Left = 96
    Top = 56
  end
  object Report: TfrxReport
    Version = '4.9.72'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42283.514146273150000000
    ReportOptions.LastChange = 42293.632003344910000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    OnUserFunction = ReportUserFunction
    Left = 216
    Top = 8
  end
  object ReportHeader: TfrxDBDataset
    UserName = 'ReportHeader'
    CloseDataSource = False
    DataSet = cdsReportHeader
    BCDToCurrency = False
    Left = 192
    Top = 224
  end
  object ReportItems: TfrxDBDataset
    UserName = 'ReportItems'
    CloseDataSource = False
    DataSet = cdsReportItems
    BCDToCurrency = False
    Left = 192
    Top = 272
  end
  object cdsReportHeader: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 224
  end
  object cdsReportItems: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 272
    Top = 272
  end
  object Designer: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnSaveReport = DesignerSaveReport
    Left = 216
    Top = 56
  end
end
