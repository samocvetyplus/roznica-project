unit frmBuyInvoiceSession;
                                        
interface
                                                                                          
uses
                                          
  Classes, ActnList, Controls, DB,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid,
  dxLayoutControl, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxGroupBox,
  dxSkinsCore, dxLayoutcxEditAdapters, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxBar, dxLayoutLookAndFeels,
  frmBuyInvoice, cxHeader, Menus, StdCtrls, cxButtons, DBClient, cxSplitter,
  dxSkinsDefaultPainters, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxPScxPageControlProducer,
  dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, cxCheckBox, cxGridDBTableView,
  cxCalendar, cxPC, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

{ TFrameBuyInvoiceSession }

  TFrameBuyInvoiceSession = class(TFrameBuyInvoice)
    InvoiceItemACTIONNAME: TFIBStringField;
    InvoiceItemACTIONCODE: TFIBIntegerField;
    GridInvoiceItemViewACTIONNAME: TcxGridDBBandedColumn;
  end;

var
  FrameBuyInvoiceSession: TFrameBuyInvoiceSession;

implementation

{$R *.dfm}

end.
