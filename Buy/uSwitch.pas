unit uSwitch;

interface

uses

  SysUtils;

const
  SwitchChars = ['/','-'];

type

  TSysCharSet = set of AnsiChar;

  TCmdLineSwitchType = (clstValueNextParam, clstValueAppended);

  TCmdLineSwitchTypes = set of TCmdLineSwitchType;

function CharInSet(C: AnsiChar; const CharSet: TSysCharSet): Boolean; overload; inline;

function CharInSet(C: WideChar; const CharSet: TSysCharSet): Boolean; overload; inline;

function CommandLineSwitch(const Switch: string; const Chars: TSysCharSet; IgnoreCase: Boolean): Boolean; overload;

function CommandLineSwitch(const Switch: string): Boolean; overload;

function CommandLineSwitch(const Switch: string; IgnoreCase: Boolean): Boolean; overload;

function CommandLineSwitch(const Switch: string; var Value: string; IgnoreCase: Boolean = True; const SwitchTypes: TCmdLineSwitchTypes = [clstValueNextParam, clstValueAppended]): Boolean; overload;

implementation

function CharInSet(C: AnsiChar; const CharSet: TSysCharSet): Boolean;
begin
  Result := C in CharSet;
end;

function CharInSet(C: WideChar; const CharSet: TSysCharSet): Boolean;
begin
  Result := C in CharSet;
end;

function CommandLineSwitch(const Switch: string; const Chars: TSysCharSet;
  IgnoreCase: Boolean): Boolean;
var
  I: Integer;
  S: string;
begin
  for I := 1 to ParamCount do
  begin
    S := ParamStr(I);
    if (Chars = []) or (S[1] in Chars) then
      if IgnoreCase then
      begin
        if (AnsiCompareText(Copy(S, 2, Maxint), Switch) = 0) then
        begin
          Result := True;
          Exit;
        end;
      end
      else begin
        if (AnsiCompareStr(Copy(S, 2, Maxint), Switch) = 0) then
        begin
          Result := True;
          Exit;
        end;
      end;
  end;
  Result := False;
end;

function CommandLineSwitch(const Switch: string): Boolean;
begin
  Result := CommandLineSwitch(Switch, SwitchChars, True);
end;

function CommandLineSwitch(const Switch: string; IgnoreCase: Boolean): Boolean;
begin
  Result := CommandLineSwitch(Switch, SwitchChars, IgnoreCase);
end;

function CommandLineSwitch(const Switch: string; var Value: string; IgnoreCase: Boolean = True;
  const SwitchTypes: TCmdLineSwitchTypes = [clstValueNextParam, clstValueAppended]): Boolean; overload;
type
  TCompareProc = function(const S1, S2: string): Boolean;
var
  Param: string;
  I, ValueOfs,
  SwitchLen, ParamLen: Integer;
  SameSwitch: TCompareProc;
begin
  Result := False;
  Value := '';
  if IgnoreCase then
    SameSwitch := SameText else
    SameSwitch := SameStr;
  SwitchLen := Length(Switch);

  for I := 1 to ParamCount do
  begin
    Param := ParamStr(I);

    if CharInSet(Param[1], SwitchChars) and SameSwitch(SubString( (1,SwitchLen), Switch) then
    begin
      ParamLen := Param.Length;
      // Look for an appended value if the param is longer than the switch
      if (ParamLen > SwitchLen + 1) then
      begin
        // If not looking for appended value switches then this is not a matching switch
        if not (clstValueAppended in SwitchTypes) then
          Continue;
        ValueOfs := SwitchLen + 1;
        if Param.Chars[ValueOfs] = ':' then
          Inc(ValueOfs);
        Value := Param.SubString(ValueOfs, MaxInt);
      end
      // If the next param is not a switch, then treat it as the value
      else if (clstValueNextParam in SwitchTypes) and (I < ParamCount) and
              not CharInSet(ParamStr(I+1).Chars[0], SwitchChars) then
        Value := ParamStr(I+1);
      Result := True;
      Break;
    end;
  end;
end;


end.
