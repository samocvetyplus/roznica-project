unit frmBuy;

interface

uses

  Windows, Forms, SysUtils, Controls, ExtCtrls, Classes, Messages, Graphics, StrUtils,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxGroupBox, cxPC, cxGrid, cxGridTableView,
  dxSkinsCore, dxSkinscxPCPainter, dxGDIPlusClasses, frmBuyStart, 
  dxSkinsDefaultPainters, dxBar, DBClient, StdCtrls, cxClasses, dxRibbon, dxRibbonForm,
  dxRibbonFormCaptionHelper, ActnList, dxStatusBar, dxRibbonStatusBar, AppEvnts,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter;

type

  TcxPageControl = class(cxPC.TcxPageControl)
  protected
    procedure AfterPaintTab(ACanvas: TcxCanvas; ATab: TcxTab; AImageAndTextData: TcxPCOutTabImageAndTextData); override;
  end;


{ TDialogBuyRoot }

  TDialogBuy = class(TdxRibbonForm)
    Sheets: TcxPageControl;
    SheetStart: TcxTabSheet;
    Sheet1: TcxTabSheet;
    Sheet2: TcxTabSheet;
    Sheet3: TcxTabSheet;
    Sheet4: TcxTabSheet;
    Sheet5: TcxTabSheet;
    Sheets1: TcxPageControl;
    Sheet11: TcxTabSheet;
    Sheets2: TcxPageControl;
    Sheet21: TcxTabSheet;
    Sheets3: TcxPageControl;
    Sheet31: TcxTabSheet;
    Sheets4: TcxPageControl;
    Sheet41: TcxTabSheet;
    Sheets5: TcxPageControl;
    Sheet51: TcxTabSheet;
    SheetStore: TcxTabSheet;
    Sheet0: TcxTabSheet;
    Sheets0: TcxPageControl;
    Sheet01: TcxTabSheet;
    FrameBuyStart: TFrameBuyStart;
    BarManager: TdxBarManager;
    MenuOwner: TdxBarPopupMenu;
    ButtonOwner: TdxBarLargeButton;
    Ribbon: TdxRibbon;
    BarAccess: TdxBar;
    ButtonPrint: TdxBarButton;
    ButtonSave: TdxBarButton;
    ButtonFit: TdxBarButton;
    Sheet6: TcxTabSheet;
    Sheets6: TcxPageControl;
    Sheet61: TcxTabSheet;
    StatusBar: TdxRibbonStatusBar;
    ApplicationEvents: TApplicationEvents;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SheetsPageChanging(Sender: TObject; Sheet: TcxTabSheet; var AllowChange: Boolean);
    procedure SheetsCanClose(Sender: TObject; var ACanClose: Boolean);
    procedure SheetsChange(Sender: TObject);
    procedure Sheets0PageChanging(Sender: TObject; NewPage: TcxTabSheet;
      var AllowChange: Boolean);

    procedure ButtonOwnerClick(Sender: TObject);
    procedure SheetsNChange(Sender: TObject);
    procedure ApplicationEventsShowHint(var HintStr: string;
      var CanShow: Boolean; var HintInfo: THintInfo);
  private
    FReadOnly: Boolean;
    SelfCompanyID: Integer;
    function IsReadOnly: Boolean;
  protected
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    X, Y: Integer;
    procedure Execute;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
  end;

implementation

uses

  dmBuy,
  dmBuyOrders,

  uGridPrinter,

  frmBuyStore,

  frmBuySetup,

  frmBuyJournal,
  frmBuyJournalSession,
  frmBuyJournalMoving,
  frmBuyJournalAffinage,
  frmBuyJournalRecycling,
  frmBuyJournalTrade,
  frmBuyJournalCommon,

  frmBuyOrders,
  frmBuyOrder;

{$R *.dfm}


var

  JournalClassArray: array[1..6] of TFrameBuyJournalClass = (

    TFrameBuyJournalSession,

    TFrameBuyJournalMoving,

    TFrameBuyJournalAffinage,

    TFrameBuyJournalRecycling,

    TFrameBuyJournalTrade,

    TFrameBuyJournalCommon


  );

{ TDialogBuyRoot }


procedure TDialogBuy.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);

  BoundsRect := Rect;

  DataBuy := TDataBuy.Create(Self);

  BuyOrdersData := TBuyOrdersData.Create(Self);
end;

procedure TDialogBuy.FormDestroy(Sender: TObject);
begin
  FreeAndNil(DataBuy);

  FreeAndNil(BuyOrdersData);
end;

function TDialogBuy.IsReadOnly: Boolean;
begin
  Result := ReadOnly;
end;

procedure TDialogBuy.ApplicationEventsShowHint(var HintStr: string; var CanShow: Boolean; var HintInfo: THintInfo);
begin
  StatusBar.Panels[0].Text := HintStr;
end;

procedure TDialogBuy.ButtonOwnerClick(Sender: TObject);
begin
  SelfCompanyID := TdxBarLargeButton(Sender).Tag;
end;

procedure TDialogBuy.Execute;
var
  IsCenterDepartment: Boolean;

  Dialog: TDialogBuySetup;

procedure MenuOwnerPopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlOwner);

  if DataSet.Active then
  begin
    DataSet.First;

    while not DataSet.Eof do
    begin
      Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

      Button.ButtonStyle := bsDefault;

      Button.LargeImageIndex := ButtonOwner.LargeImageIndex;

      Button.Tag := DataSet.FieldByName('ID').AsInteger;

      Button.Caption := DataSet.FieldByName('NAME').AsString;

      Button.OnClick := ButtonOwnerClick;

      ButtonLink := MenuOwner.ItemLinks.Add;

      ButtonLink.Item := Button;

      if MenuOwner.ItemLinks.Count = 2 then
      begin
        ButtonLink.BeginGroup := True;
      end;

      DataSet.Next;
    end;

    DataSet.Free;
  end;
end;

begin
  SelfCompanyID := 0;

  IsCenterDepartment := DataBuy.IsCenterDepartment;

  MenuOwnerPopulate;

  if MenuOwner.ItemLinks.Count <> 0 then
  begin
    if IsCenterDepartment then
    begin
      MenuOwner.Popup(X, Y);

      if SelfCompanyID <> 0 then
      begin
        DataBuy.SelfCompanyID  := SelfCompanyID;
      end;
    end else

    begin
      if DataBuy.SelfPersonID = 1 then
      begin
        if IsCtrlPressed then
        begin
          Application.MainForm.Visible := False;
                  
          Dialog := TDialogBuySetup.Create(Self);

          SelfCompanyID := TDialogBuySetup.Execute;

          if SelfCompanyID <> 0 then
          begin
            DataBuy.SelfCompanyID  := SelfCompanyID;
          end;

          Dialog.Free;
        end else
        begin
          SelfCompanyID := DataBuy.SelfCompanyID;
        end;
      end else
      begin
        SelfCompanyID := DataBuy.SelfCompanyID;
      end;
    end;
  end;

  if SelfCompanyID <> 0 then
  begin
    Caption := DataBuy.SelfCompanyName + ' - ' + '���������';

    Sheet4.TabVisible := IsCenterDepartment;

    Sheet5.TabVisible := IsCenterDepartment;

    FrameBuyStart.Activate;

    ShowModal;
  end;

  Application.MainForm.Visible := True;
end;

procedure TDialogBuy.Sheets0PageChanging(Sender: TObject; NewPage: TcxTabSheet;
  var AllowChange: Boolean);
begin
  with BuyOrdersData do
  begin
    if NewPage.Tag <> 0 then
    begin
      OrdersList.DisableControls;
      OrdersList.Locate('Order$ID', NewPage.Tag, []);
      OrdersList.EnableControls;
    end;
  end;
end;



procedure TDialogBuy.SheetsCanClose(Sender: TObject;  var ACanClose: Boolean);
var
  PageControl: TcxPageControl;
begin
  ACanClose := False;

  if Sender is TcxPageControl then
  begin
    PageControl := TcxPageControl(Sender);

    ACanClose := PageControl.ActivePageIndex <> 0;
  end;
end;

procedure TDialogBuy.SheetsChange(Sender: TObject);
begin
  if Sheets.ActivePage = SheetStore then
  begin
    if not BuyStore.Store.Active then
    begin
      BuyStore.Reload;
    end;
  end;
end;

procedure TDialogBuy.SheetsPageChanging(Sender: TObject; Sheet: TcxTabSheet; var AllowChange: Boolean);
var
  Control: TControl;
  PageControl: TcxPageControl;
  Order: TFrameBuyOrders;
  OrderSheet: TcxTabSheet;
  Journal: TFrameBuyJournal;
  JournalClass: TFrameBuyJournalClass;
  JournalSheet: TcxTabSheet;
begin
  if Sheet.PageIndex = 0 then
  begin
    AllowChange := True;
  end else

  begin
    if Sheet.Tag = -1 then    //�������
      begin
        if Sheet.ControlCount <> 0 then
        begin
          Control := Sheet.Controls[0];
          if Control is TcxPageControl then
          begin
            PageControl := TcxPageControl(Control);
            if PageControl.PageCount <> 0 then
            begin
              OrderSheet := PageControl.Pages[0];
              if OrderSheet.ControlCount = 0 then
              begin
                Order := TFrameBuyOrders.Create(OrderSheet);
                Order.Align := alClient;
                Order.Parent := OrderSheet;
                Order.Activate;
              end;
            end;
          end;
        end;
      end
    else
    if Sheet.Tag > 0 then
    begin

      if Sheet.Tag in [1..6] then
      begin
        if Sheet.ControlCount <> 0 then
        begin
          Control := Sheet.Controls[0];

          if Control is TcxPageControl then
          begin
            PageControl := TcxPageControl(Control);

            if PageControl.PageCount <> 0 then
            begin
              JournalClass := JournalClassArray[Sheet.Tag];

              if JournalClass <> nil then
              begin
                JournalSheet := PageControl.Pages[0];

                if JournalSheet.ControlCount = 0 then
                begin
                  Journal := JournalClass.Create(JournalSheet);

                  Journal.ReadOnly := IsReadOnly;

                  Journal.Align := alClient;

                  Journal.Parent := JournalSheet;

                  Journal.Activate;

                  Sheet.Tag := - Sheet.Tag;
                end;
             end;
           end;
          end;
        end;
      end else

      begin
        if Sheet = SheetStore then
        begin
          BuyStore := TFrameBuyStore.Create(Sheet);

          BuyStore.Align := alClient;

          BuyStore.Parent := Sheet;

          if BuyStore.Activate then
          begin
            Sheet.Tag := - Sheet.Tag;
          end;
        end;
      end;
    end else
    begin

    end;

    AllowChange := Sheet.Tag < 0;
  end;
end;

procedure TDialogBuy.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then
  begin
    if not IsIconic(Application.Handle) then
    begin
      Application.NormalizeTopMosts;

      SetActiveWindow(Application.Handle);

      ShowWindow(Application.Handle, SW_MINIMIZE);
    end;
  end
  else inherited;
end;

{ TcxPageControl }

type

  TcxPCCustomPainterAccess = class(TcxPCCustomPainter)

  end;

procedure TcxPageControl.AfterPaintTab(ACanvas: TcxCanvas; ATab: TcxTab; AImageAndTextData: TcxPCOutTabImageAndTextData);
var
  R: TRect;
  PainterAccess: TcxPCCustomPainterAccess;
  Sheet: TcxTabSheet;
  Text: string;
begin
  R := ATab.FullRect;

  PainterAccess := TcxPCCustomPainterAccess(Painter);

  ACanvas.Font := PainterAccess.ParentInfo.Canvas.Font;

  SetBkMode(ACanvas.Handle, TRANSPARENT);

  Sheet := Pages[ATab.Index];

  if Sheet.Caption = '' then
  begin
    Text := Sheet.Hint;

    Text := AnsiReplaceStr(Text, '?', #13#10);

    ACanvas.DrawText(Text, R, taCenter, vaCenter, True, False);
  end;
end;

procedure TDialogBuy.SheetsNChange(Sender: TObject);
var
  ClassName: string;
  Sheets: TcxPageControl;
  Sheet: TcxTabSheet;
  Control: TControl;
  Journal: TFrameBuyJournal;
begin
  if Sender <> nil then
  begin
    if Sender is TcxPageControl then
    begin
      Sheets := TcxPageControl(Sender);

      if Sheets.ActivePageIndex = 0 then
      begin
        Sheet := Sheets.ActivePage;

        if Sheet.ControlCount = 1 then
        begin
          Control := Sheet.Controls[0];

          if Control is TFrameBuyJournal then
          begin
            Journal := TFrameBuyJournal(Control);

            Journal.Reload;
          end;
        end;
      end;
    end;
  end;

  ClassName := Sender.ClassName;
end;


end.


