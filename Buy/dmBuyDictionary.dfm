object BuyDictionary: TBuyDictionary
  OldCreateOrder = False
  Height = 348
  Width = 269
  object DictionaryProbe: TpFIBDataSet
    Transaction = trRead
    UpdateTransaction = trWrite
    Left = 32
    Top = 16
    oStartTransaction = False
    oKeepSorting = True
  end
  object DictionaryMaterial: TpFIBDataSet
    Transaction = trRead
    UpdateTransaction = trWrite
    Left = 32
    Top = 72
    oStartTransaction = False
  end
  object DictionaryMemo: TpFIBDataSet
    Transaction = trRead
    Database = dmCom.db
    UpdateTransaction = trWrite
    Left = 32
    Top = 128
    oStartTransaction = False
  end
  object trRead: TpFIBTransaction
    TimeoutAction = TARollback
    TRParams.Strings = (
      'read'
      'nowait'
      'rec_version'
      'read_commited')
    TPBMode = tpbDefault
    Left = 24
    Top = 216
  end
  object trWrite: TpFIBTransaction
    TimeoutAction = TARollback
    TRParams.Strings = (
      'write'
      'nowait'
      'rec_version'
      'read_commited')
    TPBMode = tpbDefault
    Left = 24
    Top = 272
  end
  object SourceDictionaryProbe: TDataSource
    Left = 152
    Top = 16
  end
  object SourceDictionaryMaterial: TDataSource
    Left = 152
    Top = 72
  end
  object SourceDictionaryMemo: TDataSource
    Left = 152
    Top = 128
  end
end
