unit frmBuyJournalCommon;

interface                                         

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frmBuyJournal, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, DB, cxDBData, dxBar, FIBDataSet, pFIBDataSet, ActnList,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid,
  cxGroupBox, ExtCtrls, cxSplitter, cxTextEdit, cxMemo, frxClass, DBClient,
  frxDBSet, DateUtils, frmPopup, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, dxSkinsdxBarPainter;

type

  TFrameBuyJournalCommon = class(TFrameBuyJournal)
    frxReport: TfrxReport;
    BodyBegin: TClientDataSet;
    BodyEnd: TClientDataSet;
    BodyIn: TClientDataSet;
    BodyOut: TClientDataSet;
    BodyAll: TClientDataSet;
    frxBodyBegin: TfrxDBDataset;
    frxBodyEnd: TfrxDBDataset;
    frxBodyIn: TfrxDBDataset;
    frxBodyOut: TfrxDBDataset;
    Header: TClientDataSet;
    DataSet: TClientDataSet;
    HeaderCompanyName: TStringField;
    HeaderOfficeName: TStringField;
    HeaderPersonBookKeeper: TStringField;
    HeaderPersonResponsible: TStringField;
    HeaderRangeBegin: TDateField;
    HeaderRangeEnd: TDateField;
    HeaderCompanyCode: TStringField;
    frxHeader: TfrxDBDataset;
    ButtonPrint1: TdxBarLargeButton;
    function OnfrxReportUserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure ButtonPrintClick(Sender: TObject);
  private
     procedure OnReport(Popup: TPopup);
  protected
    function GetClassCode: Integer; override;
    procedure CustomSetup; override;
  end;

var
  FrameBuyJournalCommon: TFrameBuyJournalCommon;

implementation

{$R *.dfm}

uses

  dmBuy, frmBuyRangePopup, frmBuyInvoiceReport,  uDialog;

{ TFrameBuyJournalCommon }


procedure TFrameBuyJournalCommon.CustomSetup;
begin
  CategoryVisible['Send'] := cbTrue;

  CategoryVisible['Receive'] := cbTrue;

  CategoryVisible['Print'] := cbTrue;

  inherited CustomSetup;
end;
                                                                                              
function TFrameBuyJournalCommon.GetClassCode: Integer;
begin
  Result := 0;
                                                                                            
  if ButtonSend.Down then
  begin
    Result := Low(Integer);
  end else

  if ButtonReceive.Down then
  begin
    Result := High(Integer);
  end;
end;

procedure TFrameBuyJournalCommon.ButtonPrintClick(Sender: TObject);
var
  DialogModern: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  CurrentDate: TDate;
  RangeBegin: TDate;
  RangeEnd: TDate;
  Day, Month, Year: Word;
begin
  OnReport(nil);

  Exit;

  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonPrint.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  DialogModern := TDialogBuyRangePopup.Create(Self);

  CurrentDate := DateOf(Date);

  DecodeDate(Date, Year, Month, Day);

  Day := 1;

  IncAMonth(Year, Month, Day, -1);

  RangeBegin := EncodeDate(Year, Month, Day);

  RangeEnd := DateOf(EndOfTheMonth(RangeBegin));

  DialogModern.RangeBegin := RangeBegin;

  if RangeEnd > CurrentDate then
  begin
    DialogModern.RangeEnd := CurrentDate;
  end else
  begin
    DialogModern.RangeEnd := RangeEnd;
  end;

  DialogModern.OnOk := OnReport;

  DialogModern.Popup(X, Y);
end;

procedure TFrameBuyJournalCommon.OnReport(Popup: TPopup);
var
  //RangeBegin: TDate;
  //RangeEnd: TDate;
  CurrentDate: TDate;
  Dialog: TDialogBuyRangePopup;
  CompanyID: Integer;
  OfficeID: Integer;
begin
  //Dialog := TDialogBuyRangePopup(Popup);

  //RangeBegin := Dialog.RangeBegin;

  //RangeEnd := Dialog.RangeEnd;

  CurrentDate := DateOf(Date);

  if RangeEnd > CurrentDate then
  begin
    RangeEnd := CurrentDate;
  end;

  CompanyID := DataBuy.SelfCompanyID;

  OfficeID := DataBuy.SelfDepartmentID;

  Header.EmptyDataSet;

  Header.Append;

  HeaderRangeBegin.AsDateTime := DateOf(RangeBegin);

  HeaderRangeEnd.AsDateTime := DateOf(RangeEnd);

  HeaderCompanyName.AsString := DataBuy.SelfCompanyName;

  HeaderOfficeName.AsString := DataBuy.SelfDepartmentName;

  DataSet.Data := DataBuy.Table('select okpo, buh, ofio from d_comp where d_compid = :id', [CompanyID]);

  HeaderCompanyCode.AsString := DataSet.FieldByName('okpo').AsString;

  HeaderPersonBookKeeper.AsString := DataSet.FieldByName('buh').AsString;

  if DataBuy.IsCenterDepartment then
  begin
    HeaderPersonResponsible.AsString := DataSet.FieldByName('ofio').AsString;
  end else
  begin
    HeaderPersonResponsible.AsVariant := DataBuy.Value('select fio from d_mol where d_depid = :office$id and upper(post) like ' + '''' + '��������%' + '''', [OfficeID]);
  end;

  Header.Post;

  BodyAll.Data := DataBuy.Table('select * from buy$trade$report(:range$begin, :range$end)', [RangeBegin, RangeEnd]);

  BodyBegin.CloneCursor(BodyAll, True, False);

  BodyBegin.IndexFieldNames := 'class$code';

  BodyBegin.SetRange([Low(Integer)], [Low(Integer)]);

  BodyIn.CloneCursor(BodyAll, True, False);

  BodyIn.IndexFieldNames := 'route; invoice$date; class$code';

  BodyIn.SetRange([+1], [+1]);

  BodyOut.CloneCursor(BodyAll, True, False);

  BodyOut.IndexFieldNames := 'route; invoice$date; class$code';

  BodyOut.SetRange([-1], [-1]);

  BodyEnd.CloneCursor(BodyAll, True, False);

  BodyEnd.IndexFieldNames := 'class$code';

  BodyEnd.SetRange([High(Integer)], [High(Integer)]);

  frxReport.Script.ClearItems(nil);

  frxReport.AddFunction('function �����(v: Currency): string');

  frxReport.AddFunction('function �������(v: Currency): string');

  frxReport.AddFunction('function �����(v: Currency): string');

  frxReport.AddFunction('function ����������(v: Currency): string');

  frxReport.AddFunction('function ����(v: TDateTime): string');

  frxReport.AddFunction('function �����(v: TDateTime): string');

  frxReport.AddFunction('function ���(v: TDateTime): string');

  frxReport.AddFunction('function ����������(v: Currency): string');

  frxReport.PrepareReport;

  frxReport.ShowPreparedReport;
end;

function TFrameBuyJournalCommon.OnfrxReportUserFunction(const MethodName: string; var Params: Variant): Variant;
begin
  Result := frxReportUserFunction(MethodName, Params);
end;

end.



