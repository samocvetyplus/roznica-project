inherited FrameBuyJournalTrade: TFrameBuyJournalTrade
  inherited GroupBoxJournal: TcxGroupBox
    inherited GridJournal: TcxGrid
      inherited GridJournalView: TcxGridDBBandedTableView
        inherited GridJournalViewWEIGHT: TcxGridDBBandedColumn
          Visible = False
        end
        inherited GridJournalViewWEIGHTREAL: TcxGridDBBandedColumn
          Caption = #1042#1077#1089
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      0
      0
      0
      0
      0
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited ButtonOffice: TdxBarLargeButton
      ImageIndex = -1
    end
    inherited ButtonSend: TdxBarLargeButton
      ImageIndex = 17
    end
    inherited ButtonReceive: TdxBarLargeButton
      Down = True
    end
  end
  inherited Action: TActionList
    inherited ActionAdd: TAction
      Caption = #1055#1088#1086#1076#1072#1078#1072
    end
    inherited ActionSend: TAction
      Caption = #1055#1088#1086#1076#1072#1078#1072
    end
    inherited ActionReceive: TAction
      Caption = #1055#1086#1082#1091#1087#1082#1072
    end
  end
end
