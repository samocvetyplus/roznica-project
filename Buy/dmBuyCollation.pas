unit dmBuyCollation;

interface

uses

  Classes,
  FIBQuery, pFIBQuery, pFIBStoredProc;

type

{ TDataBuyInvoice }

  TDataBuyCollation = class(TDataModule)
    CollationOpen: TpFIBStoredProc;
    CollationClose: TpFIBStoredProc;
    CollationInsert: TpFIBStoredProc;
    CollationUpdate: TpFIBStoredProc;
    CollationDelete: TpFIBStoredProc;
    CollationItemDelete: TpFIBStoredProc;
    CollationItemUpdate: TpFIBStoredProc;
    CollationItemInsert: TpFIBStoredProc;
  end;

var
  DataBuyCollation: TDataBuyCollation;

implementation

{$R *.dfm}

uses

  dmBuy;

end.
