unit frmBuyMonthPopup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frmPopup, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxLayoutControl, dxBar, cxClasses, ImgList, DateUtils,
  cxContainer, cxEdit, cxGroupBox, dxLayoutcxEditAdapters, frxpngimage, cxImage,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinsdxBarPainter;

type

  TDialogBuyMonthPopup = class(TPopup)
    LayoutGroup_Root: TdxLayoutGroup;
    Layout: TdxLayoutControl;
    BarManager: TdxBarManager;
    BarMonth: TdxBar;
    dxBarDockControl1: TdxBarDockControl;
    LayoutItem1: TdxLayoutItem;
    ButtonMonth1: TdxBarButton;
    ButtonMonth2: TdxBarButton;
    ButtonMonth3: TdxBarButton;
    ButtonMonth7: TdxBarButton;
    ButtonMonth4: TdxBarButton;
    ButtonMonth6: TdxBarButton;
    ButtonMonth5: TdxBarButton;
    ButtonMonth8: TdxBarButton;
    ButtonMonth9: TdxBarButton;
    ButtonMonth10: TdxBarButton;
    ButtonMonth11: TdxBarButton;
    ButtonMonth12: TdxBarButton;
    BarManagerBar2: TdxBar;
    ButtonYearPrior: TdxBarButton;
    ButtonYear: TdxBarButton;
    ButtonYearNext: TdxBarButton;
    Images24: TcxImageList;
    GroupBox: TcxGroupBox;
    dxBarDockControl2: TdxBarDockControl;
    LayoutGroup1: TdxLayoutGroup;
    LayoutItem2: TdxLayoutItem;
    procedure ButtonMonthClick(Sender: TObject);
    procedure ButtonYearPriorClick(Sender: TObject);
    procedure ButtonYearNextClick(Sender: TObject);
  private
    FYear: Integer;
    FMonth: Integer;
    procedure UpdateButtons;
  public
    procedure Popup(X: Integer; Y: Integer); override;
    property Year: Integer read FYear;
    property Month: Integer read FMonth;
  end;

var
  DialogBuyMonthPopup: TDialogBuyMonthPopup;

implementation

{$R *.dfm}

procedure TDialogBuyMonthPopup.ButtonMonthClick(Sender: TObject);
var
  Button: TdxBarButton;
begin
  FYear := StrToInt(Trim(ButtonYear.Caption));

  Button := TdxBarButton(Sender);

  FMonth := Button.Tag;

  Ok;
end;

procedure TDialogBuyMonthPopup.ButtonYearNextClick(Sender: TObject);
begin
  Inc(FYear);

  ButtonYear.Caption := '    ' + IntToStr(Year) + '    ';

  ButtonYearPrior.Enabled := Year <> 2015;

  ButtonYearNext.Enabled := Year <> YearOf(Date);

  UpdateButtons;
end;

procedure TDialogBuyMonthPopup.ButtonYearPriorClick(Sender: TObject);
begin
  Dec(FYear);

  ButtonYear.Caption := '    ' + IntToStr(Year) + '    ';

  ButtonYearPrior.Enabled := Year <> 2015;

  ButtonYearNext.Enabled := Year <> YearOf(Date);

  UpdateButtons;
end;

procedure TDialogBuyMonthPopup.Popup(X, Y: Integer);
begin
  FYear := YearOf(Date);

  ButtonYear.Caption := '    ' + IntToStr(Year) + '    ';

  ButtonYearPrior.Enabled := Year <> 2015;

  ButtonYearNext.Enabled := Year <> YearOf(Date);

  UpdateButtons;

  inherited;
end;

procedure TDialogBuyMonthPopup.UpdateButtons;
var
  i: Integer;
  Button: TdxBarButton;
begin
  for i := 1 to 12 do
  begin
    Button := TdxBarButton(FindComponent('ButtonMonth' + IntToStr(i)));

    if Year < YearOf(Date) then
    begin
      Button.Enabled := True;
    end else
    begin
      if i > MonthOf(Date) then
      begin
        Button.Enabled := False;
      end else
      begin
        Button.Enabled := True;
      end;
    end;
  end;
end;

end.
