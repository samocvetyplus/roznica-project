inherited FrameBuyInvoiceTrade: TFrameBuyInvoiceTrade
  inherited GroupBoxHeader: TcxGroupBox
    ExplicitHeight = 304
    inherited Layout: TdxLayoutControl
      ExplicitHeight = 300
      inherited LayoutEditorN: TcxDBTextEdit
        ExplicitWidth = 101
        Width = 101
      end
      inherited LayoutEditorState: TcxDBTextEdit
        ExplicitWidth = 229
        Width = 229
      end
      inherited LayoutEditorStateDate: TcxDBTextEdit
        Left = 236
        ExplicitLeft = 236
      end
      inherited LayoutEditorStatePerson: TcxDBTextEdit
        ExplicitWidth = 229
        Width = 229
      end
      inherited LayoutEditorDepartment: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorAgent: TcxDBTextEdit
        ExplicitWidth = 235
        Width = 235
      end
      inherited LayoutEditorCompany: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorForeignN: TcxDBTextEdit
        ExplicitWidth = 75
        Width = 75
      end
      inherited LayoutEditorForeignDate: TcxDBDateEdit
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorDate: TcxDBDateEdit
        ExplicitWidth = 121
        Width = 121
      end
    end
  end
  inherited GroupBoxContent: TcxGroupBox
    ExplicitWidth = 74
    ExplicitHeight = 304
    inherited PageContent: TcxPageControl
      inherited SheetContent: TcxTabSheet
        inherited GridInvoiceItem: TcxGrid
          Height = 321
          ExplicitHeight = 321
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited LargeButtonClose: TdxBarLargeButton
      ImageIndex = 9
    end
  end
end
