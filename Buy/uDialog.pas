unit uDialog;

interface

uses

  Forms, Dialogs, Controls, StdCtrls, Classes,
  Graphics, ExtCtrls, Windows, Messages,
  dxGDIPlusClasses;

type

  { TDialog }

  TDialog = class(TObject)
  private
    class function Custom(DlgType: TMsgDlgType; const Msg: string; Buttons: TMsgDlgButtons): TModalResult; static;
  public
    class function Error(const Msg: string; Buttons: TMsgDlgButtons = [mbOK]): TModalResult; static;
    class function Information(const Msg: string; Buttons: TMsgDlgButtons = [mbOK]): TModalResult; static;
    class function Confirmation(const Msg: string;  Buttons: TMsgDlgButtons = [mbYes, mbNo]): TModalResult; static;
    class function Warning(const Msg: string; Buttons: TMsgDlgButtons = [mbOK]): TModalResult; static;
  end;

implementation

uses

   uSound;

{ TDialog }

class function TDialog.Custom(DlgType: TMsgDlgType; const Msg: string; Buttons: TMsgDlgButtons): TModalResult;
var
  Form: TForm;
  Image: TImage;
  Handle: HWND;
  PNGImage: TdxPNGImage;
  Stream: TResourceStream;
begin
  Handle := GetCapture;

  if Handle <> 0 then
  begin
    SendMessage(Handle, WM_CANCELMODE, 0, 0);
  end;

  Screen.Cursor := crDefault;

  Form := CreateMessageDialog(Msg, DlgType, Buttons);

  Form.Caption := Application.Title;

  Form.BorderStyle := bsToolWindow;

  Form.Position := poScreenCenter;

  if mbYes in Buttons then
  begin
    TButton(Form.FindComponent('Yes')).Caption := '��';
  end;

  if mbNo in Buttons then
  begin
    TButton(Form.FindComponent('No')).Caption := '���';
  end;

  if mbCancel in Buttons then
  begin
    TButton(Form.FindComponent('Cancel')).Caption := '������';
  end;

  PNGImage := TdxPngImage.Create;

  case DlgType of

    mtWarning:
     begin
      Stream := TResourceStream.Create(HInstance, 'Image.Warning', RT_RCDATA);

      PNGImage.LoadFromStream(Stream);

      Stream.Free;

      TSound.Warning;
     end;

    mtError:
     begin
      Stream := TResourceStream.Create(HInstance, 'Image.Error', RT_RCDATA);

      PNGImage.LoadFromStream(Stream);

      Stream.Free;


      TSound.Error;
     end;

    mtInformation:
     begin
      Stream := TResourceStream.Create(HInstance, 'Image.Information', RT_RCDATA);

      PNGImage.LoadFromStream(Stream);

      Stream.Free;


      TSound.Information;
     end;

    mtConfirmation:
     begin
      Stream := TResourceStream.Create(HInstance, 'Image.Confirmation', RT_RCDATA);

      PNGImage.LoadFromStream(Stream);

      Stream.Free;


      TSound.Confirmation;
     end;

  end;

  if not PNGImage.Empty then
  begin
    Image := TImage(Form.FindComponent('Image'));

    Image.Picture.Assign(PNGImage);
  end;

  PNGImage.Free;

  Result := Form.ShowModal;

  Form.Free;
end;

class function TDialog.Information(const Msg: string; Buttons: TMsgDlgButtons): TModalResult;
begin
  Result := Custom(mtInformation, Msg, Buttons);
end;

class function TDialog.Warning(const Msg: string; Buttons: TMsgDlgButtons): TModalResult;
begin
  Result := Custom(mtWarning, Msg, Buttons);
end;

class function TDialog.Error(const Msg: string; Buttons: TMsgDlgButtons): TModalResult;
begin
  Result := Custom(mtError, Msg, Buttons);
end;

class function TDialog.Confirmation(const Msg: string; Buttons: TMsgDlgButtons): TModalResult;
begin
  Result := Custom(mtConfirmation, Msg, Buttons);
end;

end.
