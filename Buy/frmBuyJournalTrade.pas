unit frmBuyJournalTrade;

interface

uses                             
                                                                                                    
  Controls, DB, Classes, ActnList,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage,  cxDBData, dxSkinsdxBarPainter,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,
  cxGrid, cxGroupBox,
  dxSkinsCore,  dxSkinscxPCPainter, dxBar,
  frmBuyJournal, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxSplitter, cxTextEdit,
  cxMemo, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;

type                               

{ TFrameBuyJournalTrade }

  TFrameBuyJournalTrade = class(TFrameBuyJournal)                               
  protected
    function GetClassCode: Integer; override;
    function GetInvoiceClass: TFrameBuyInvoiceClass; override;
    procedure CustomSetup; override;
  end;

var
  FrameBuyJournalTrade: TFrameBuyJournalTrade;

implementation

{$R *.dfm}

uses

  uBuy, dmBuy, frmBuyInvoiceTrade;


{ TFrameBuyJournalTrade }        

function TFrameBuyJournalTrade.GetClassCode: Integer;
begin
  Result := 0;

  if ButtonSend.Down then
  begin
    Result := 9;
  end else
                                                                                                     
  if ButtonReceive.Down then
  begin
    Result := 10;
  end;
end;

function TFrameBuyJournalTrade.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoiceTrade;
end;

procedure TFrameBuyJournalTrade.CustomSetup;
begin
  CategoryVisible['Send'] := cbTrue;

  CategoryVisible['Receive'] := cbTrue;

  if DepartmentID = DataBuy.SelfDepartmentID then
  begin

    CategoryVisible['Open + Close'] := cbTrue;

    CategoryVisible['Add'] := cbTrue;

    CategoryVisible['Delete'] := cbTrue;
  end;

  GridJournalView.Band['������� ��������'].Visible := ButtonReceive.Down;

  inherited CustomSetup;
end;


end.
