unit frmBuyInvoiceCollation;
                                                   
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxRibbonForm, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbon, dxBar, cxClasses, dxRibbonFormCaptionHelper,
  DB, FIBDataSet, pFIBDataSet, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridBandedTableView, cxGridDBBandedTableView, ActnList, DBClient, frxClass,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxRibbonPainter,
  dxSkinscxPCPainter, dxSkinsdxBarPainter, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, FIBQuery, pFIBQuery, StdCtrls;

type

  TDialogBuyInvoiceCollation = class(TdxRibbonForm)
    BarManager: TdxBarManager;
    BarAccess: TdxBar;
    ButtonFit: TdxBarButton;
    ButtonPrintGrid: TdxBarButton;
    ButtonSave: TdxBarButton;
    Ribbon: TdxRibbon;
    SourceInvoice: TDataSource;
    InvoiceItem: TpFIBDataSet;
    GridInvoiceLevel: TcxGridLevel;
    GridInvoice: TcxGrid;
    InvoiceItemID: TFIBIntegerField;
    InvoiceItemCOLLATIONID: TFIBIntegerField;
    InvoiceItemMATERIALID: TFIBIntegerField;
    InvoiceItemMATERIALNAME: TFIBStringField;
    InvoiceItemPROBEID: TFIBIntegerField;
    InvoiceItemPROBENAME: TFIBStringField;
    InvoiceItemWEIGHT: TFIBBCDField;
    InvoiceItemWEIGHTACTUAL: TFIBBCDField;
    InvoiceItemWEIGHTREAL: TFIBBCDField;
    InvoiceItemWEIGHTREALACTUAL: TFIBBCDField;
    InvoiceItemMODE: TFIBIntegerField;
    GridInvoiceView: TcxGridDBBandedTableView;
    GridInvoiceViewMATERIALNAME: TcxGridDBBandedColumn;
    GridInvoiceViewPROBENAME: TcxGridDBBandedColumn;
    GridInvoiceViewWEIGHT: TcxGridDBBandedColumn;
    GridInvoiceViewWEIGHTACTUAL: TcxGridDBBandedColumn;
    GridInvoiceViewWEIGHTREAL: TcxGridDBBandedColumn;
    GridInvoiceViewWEIGHTREALACTUAL: TcxGridDBBandedColumn;
    Bar: TdxBar;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    Actions: TActionList;
    ActionAdd: TAction;
    ActionDelete: TAction;
    ButtonMaterial: TdxBarLargeButton;
    ButtonGold: TdxBarLargeButton;
    ButtonSilver: TdxBarLargeButton;
    MenuMaterial: TdxBarPopupMenu;
    ActionGold: TAction;
    ActionSilver: TAction;
    BarDockControl: TdxBarDockControl;
    MenuProbeGold: TdxBarPopupMenu;
    MenuProbeSilver: TdxBarPopupMenu;
    ButtonProbe: TdxBarLargeButton;
    GridInvoiceViewMODE: TcxGridDBBandedColumn;
    InvoiceItemProficit: TCurrencyField;
    InvoiceItemDeficit: TCurrencyField;
    InvoiceItemProficitReal: TCurrencyField;
    InvoiceItemDeficitReal: TCurrencyField;
    GridInvoiceViewProficit: TcxGridDBBandedColumn;
    GridInvoiceViewDeficit: TcxGridDBBandedColumn;
    GridInvoiceViewProficitReal: TcxGridDBBandedColumn;
    GridInvoiceViewDeficitReal: TcxGridDBBandedColumn;
    ButtonMOL: TdxBarLargeButton;
    MenuMOL: TdxBarPopupMenu;
    Query: TpFIBQuery;
    procedure FormCreate(Sender: TObject);
    procedure InvoiceItemBeforeOpen(DataSet: TDataSet);
    procedure InvoiceItemBeforePost(DataSet: TDataSet);
    procedure OnWeightStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
    procedure ButtonMaterialClick(Sender: TObject);
    procedure ActionMaterialExecute(Sender: TObject);
    procedure ButtonAddClick(Sender: TObject);
    procedure ButtonProbeClick(Sender: TObject);
    procedure ActionAddExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure GridInvoiceViewFocusedItemChanged(Sender: TcxCustomGridTableView;
      APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
    procedure InvoiceItemAfterOpen(DataSet: TDataSet);
    procedure OnWeightRealStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
    procedure InvoiceItemCalcFields(DataSet: TDataSet);
    procedure ButtonMOLClick(Sender: TObject);
  private
    FCollationID: Integer;
    FReadOnly: Boolean;
    procedure Reload;
    function GetMaterialID: Integer;
  public
    procedure Execute;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property MaterialID: Integer read GetMaterialID;
    property CollationID: Integer read FCollationID write FCollationID;
  end;


var
  DialogBuyInvoiceCollation: TDialogBuyInvoiceCollation;

implementation

uses uDialog, dmBuy, dmBuyCollation;

{$R *.dfm}

{ TDialogBuyInvoiceCollation }

procedure TDialogBuyInvoiceCollation.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  //SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  //Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);

  //BoundsRect := Rect;

     InvoiceItem.First;

  while not (InvoiceItem.eof)  do
    begin
      //InvoiceItem.FieldByName('Weight$Real$actual').AsString := InvoiceItem.FieldByName('Weight$Real').AsString;

InvoiceItem.Edit;
InvoiceItem.FieldByName('Weight$Real$Actual').AsFloat := InvoiceItem.FieldByName('Weight$Real').AsFloat;
InvoiceItem.Post;
InvoiceItem.next;
    end;
end;

function TDialogBuyInvoiceCollation.GetMaterialID: Integer;
begin
  Result := ButtonMaterial.Tag;
end;

procedure TDialogBuyInvoiceCollation.GridInvoiceViewFocusedItemChanged(Sender: TcxCustomGridTableView; APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
var
  Mode: Integer;
begin
  if AFocusedItem = GridInvoiceViewWEIGHTACTUAL then
  begin
    Mode := InvoiceItemMODE.AsInteger;

    AFocusedItem.Options.Editing := Mode = 1;
  end;
end;

procedure TDialogBuyInvoiceCollation.InvoiceItemAfterOpen(DataSet: TDataSet);
begin
  GridInvoiceViewWEIGHTREALACTUAL.Focused := True;
end;

procedure TDialogBuyInvoiceCollation.InvoiceItemBeforeOpen(DataSet: TDataSet);
begin
  InvoiceItem.ParamByName('collation$id').AsInteger := CollationID;

  InvoiceItem.ParamByName('material$id').AsInteger := MaterialID;
end;

procedure TDialogBuyInvoiceCollation.InvoiceItemBeforePost(DataSet: TDataSet);
var
  ID: Integer;
  Mode: Integer;
  Weight: Currency;
  WeightActual: Currency;
  WeightReal: Currency;
  WeightRealActual: Currency;
  K: Double;
begin
  if InvoiceItem.State <> dsEdit then
  begin
    Exit;
  end;

  ID := InvoiceItemID.AsInteger;

  Mode := InvoiceItemMODE.AsInteger;

  Weight := InvoiceItemWEIGHT.AsCurrency;

  WeightActual := InvoiceItemWEIGHTACTUAL.AsCurrency;

  WeightReal := InvoiceItemWEIGHTREAL.AsCurrency;

  WeightRealActual := InvoiceItemWEIGHTREALACTUAL.AsCurrency;

  if Mode = 1 then
  begin

  end else

  if Mode = 2 then
  begin
    if WeightRealActual = WeightReal then
    begin
      WeightActual := Weight;
    end else
    begin
      K := WeightRealActual / WeightReal;

      WeightActual := Weight * K;
    end;
  end;

  DataBuy.Rollback;

  DataBuyCollation.CollationItemUpdate.ParamByName('ID').AsInteger := ID;

  DataBuyCollation.CollationItemUpdate.ParamByName('WEIGHT$ACTUAL').AsCurrency := WeightActual;

  DataBuyCollation.CollationItemUpdate.ParamByName('WEIGHT$REAL$ACTUAL').AsCurrency := WeightRealActual;

  DataBuy.StartTransaction;

  try

    DataBuyCollation.CollationItemUpdate.ExecProc;

    DataBuy.Commit;

  except

    on E: Exception do
    begin
      DataBuy.Rollback;

      TDialog.Error(E.Message);
    end;

  end;
end;

procedure TDialogBuyInvoiceCollation.InvoiceItemCalcFields(DataSet: TDataSet);
var
  Delta: Currency;
  DeltaReal: Currency;
  Weight: Currency;
  WeightReal: Currency;
  WeightActual: Currency;
  WeightRealActual: Currency;
begin
  InvoiceItemProficit.AsCurrency := 0;

  InvoiceItemProficitReal.AsCurrency := 0;

  InvoiceItemDeficit.AsCurrency := 0;

  InvoiceItemDeficitReal.AsCurrency := 0;

  Weight := InvoiceItemWEIGHT.AsCurrency;

  WeightReal := InvoiceItemWEIGHTREAL.AsCurrency;

  WeightActual := InvoiceItemWEIGHTACTUAL.AsCurrency;

  WeightRealActual := InvoiceItemWEIGHTREALACTUAL.AsCurrency;

  Delta := WeightActual - Weight;

  DeltaReal := WeightRealActual - WeightReal;

  if DeltaReal > 0 then
  begin
    InvoiceItemProficit.AsCurrency := Delta;

    InvoiceItemProficitReal.AsCurrency := DeltaReal;
  end else
  if Delta < 0 then
  begin
    InvoiceItemDeficit.AsCurrency := -Delta;

    InvoiceItemDeficitReal.AsCurrency := -DeltaReal;
  end;
end;

procedure TDialogBuyInvoiceCollation.OnWeightRealStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  Value: Variant;
  Mode: Integer;
begin
 AStyle := DataBuy.StyleGray;

 if not ReadOnly then
 begin
    Index := GridInvoiceViewMODE.Index;

    Value := ARecord.Values[Index];

    if not VarIsNull(Value) then
    begin
      Mode := Value;

      if Mode = 1 then
      begin
        AStyle := DataBuy.StyleInfo;
      end else

      if Mode = 2 then
      begin
        AStyle := DataBuy.StyleDefault;
      end;
    end;
 end;
end;

procedure TDialogBuyInvoiceCollation.OnWeightStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  Value: Variant;
  Mode: Integer;
  WeightReal: Currency;
  WeightRealActual: Currency;
  WeightDelta: Currency;
begin
  AStyle := DataBuy.StyleGray;

  if not ReadOnly then
  begin
    Index := GridInvoiceViewMODE.Index;

    Value := ARecord.Values[Index];

    if not VarIsNull(Value) then
    begin
      Mode := Value;

      if Mode = 1 then
      begin
        AStyle := DataBuy.StyleInfo;
      end else

      if Mode = 2 then
      begin
        AStyle := DataBuy.StyleGray;
      end;
    end;
  end;


  {
  AStyle := DataBuy.StyleDefault;

  WeightReal := 0;

  Index := GridInvoiceViewWEIGHTREAL.Index;

  Value := ARecord.Values[Index];

  if not VarIsNull(Value) then
  begin
    WeightReal := Value;
  end;

  WeightRealActual := 0;

  Index := GridInvoiceViewWEIGHTREALACTUAL.Index;

  Value := ARecord.Values[Index];

  if not VarIsNull(Value) then
  begin
    WeightRealActual := Value;

    WeightDelta := WeightRealActual - WeightReal;

    if WeightDelta > 0 then
    begin
      AStyle := DataBuy.StyleGreen;
    end else

    if WeightDelta < 0 then
    begin
     AStyle := DataBuy.StyleRed;
    end;
  end;
  }
end;

procedure TDialogBuyInvoiceCollation.Reload;
begin
  Screen.Cursor := crHourGlass;

  if InvoiceItem.State = dsEdit then
  begin
    InvoiceItem.Post;
  end;

  InvoiceItem.DisableControls;

  try

    if InvoiceItem.Active then
    begin
      InvoiceItem.Close;
    end;

    InvoiceItem.Open;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  InvoiceItem.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TDialogBuyInvoiceCollation.Execute;

procedure MenuProbePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
  MaterialID: Integer;
  Name: string;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlProbe, [0]);

  if DataSet.Active then
  begin
    DataSet.First;

    while not DataSet.Eof do
    begin
      MaterialID := DataSet.FieldByName('material$id').AsInteger;

      Name := DataSet.FieldByName('NAME').AsString;

      Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

      Button.ButtonStyle := bsDefault;

      Button.Tag := DataSet.FieldByName('ID').AsInteger;

      Button.Caption := Name;

      if MaterialID = ButtonGold.Tag then
      begin
        Button.LargeImageIndex := ButtonGold.LargeImageIndex;

        Button.OnClick := ButtonProbeClick;

        ButtonLink := MenuProbeGold.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Name = '1000' then
        begin
          ButtonLink.BeginGroup := True;
        end;

      end else

      if MaterialID = ButtonSilver.Tag then
      begin
        Button.LargeImageIndex := ButtonSilver.LargeImageIndex;

        Button.OnClick := ButtonProbeClick;

        ButtonLink := MenuProbeSilver.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Name = '1000' then
        begin
          ButtonLink.BeginGroup := True;
        end;

      end;

      DataSet.Next;
    end;

    DataSet.Free;
  end;
end;

procedure MenuMOLPopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
  Post: string;
  CurrentPost: string;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlMOL);

  DataSet.First;

  Post := '';

  while not DataSet.Eof do
  begin
    CurrentPost := DataSet.FieldByName('POST').AsString;

    Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

    Button.ButtonStyle := bsDefault;

    Button.LargeImageIndex := ButtonMOL.LargeImageIndex;

    Button.Tag := DataSet.FieldByName('ID').AsInteger;

    Button.Caption := DataSet.FieldByName('NAME').AsString;

    Button.OnClick := ButtonMOLClick;

    ButtonLink := MenuMOL.ItemLinks.Add;

    ButtonLink.Item := Button;

    if (Post <> '') and (CurrentPost <> Post) then
    begin
      Post := CurrentPost;
      
      ButtonLink.BeginGroup := True;
    end;

    DataSet.Next;
  end;

  DataSet.Free;
end;


begin
  try

    MenuProbePopulate;

    MenuMOLPopulate;

    GridInvoiceView.OptionsData.Editing := not ReadOnly;

    GridInvoiceView.OptionsSelection.CellSelect := not ReadOnly;

    if ReadOnly then
    begin
      InvoiceItem.SelectSQL.Text := InvoiceItem.SelectSQL.Text + ' and ((weight$real <> 0) or (weight$real$actual <> 0))' 
    end;

    InvoiceItem.Active := True;

    ShowModal;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;
end;

procedure TDialogBuyInvoiceCollation.ActionAddExecute(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonAdd.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if MaterialID = ButtonGold.Tag then
  begin
    MenuProbeGold.Popup(X, Y);
  end else

  if MaterialID = ButtonSilver.Tag then
  begin
    MenuProbeSilver.Popup(X, Y);
  end;
end;

procedure TDialogBuyInvoiceCollation.ActionDeleteExecute(Sender: TObject);
begin
  //
end;

procedure TDialogBuyInvoiceCollation.ActionMaterialExecute(Sender: TObject);
var
  Action: TAction;
begin
  Action := TAction(Sender);

  ButtonMaterial.Caption := Action.Caption;

  ButtonMaterial.Tag := Action.Tag;

  Reload;
end;

procedure TDialogBuyInvoiceCollation.ButtonMaterialClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonMaterial.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  MenuMaterial.Popup(X, Y);
end;


procedure TDialogBuyInvoiceCollation.ButtonMOLClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonMOL then
    begin
      GetWindowRect(Bar.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonMOL.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      MenuMOL.Popup(X, Y);
    end else

    begin
      ButtonMOL.Tag := Button.Tag;
    end;
  end;
end;



procedure TDialogBuyInvoiceCollation.ButtonAddClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonAdd.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if MenuProbeGold.ItemLinks.VisibleItemCount <> 0 then
  begin
    MenuProbeGold.Popup(X, Y);
  end else

  begin
    TDialog.Information('��� ���������.');
  end;
end;

procedure TDialogBuyInvoiceCollation.ButtonProbeClick(Sender: TObject);
begin
//
end;


end.
