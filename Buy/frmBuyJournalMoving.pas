unit frmBuyJournalMoving;

interface                                                    

uses
                                                                 
  Controls, DB, Classes, ActnList,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage,  cxDBData, dxSkinsdxBarPainter,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,
  cxGrid, cxGroupBox,
  dxSkinsCore,  dxSkinscxPCPainter, dxBar,
  frmBuyJournal, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxSplitter, cxTextEdit,
  cxMemo, cxRichEdit, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type
                                                                                                         
{ TFrameBuyJournalMoving }

  TFrameBuyJournalMoving = class(TFrameBuyJournal)
    ButtonPrint102: TdxBarLargeButton;
  protected
    function GetClassCode: Integer; override;
    function GetInvoiceClass: TFrameBuyInvoiceClass; override;
    procedure CustomSetup; override;
  end;

implementation

{$R *.dfm}

uses

  dmBuy, frmBuyInvoiceMoving;

{ TFrameBuyJournalMoving }

function TFrameBuyJournalMoving.GetClassCode: Integer;
begin
  Result := 0;

  if ButtonSend.Down then
  begin
    Result := 3;
  end else

  if ButtonReceive.Down then
  begin
    Result := 4;
  end;
end;

function TFrameBuyJournalMoving.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoiceMoving;
end;
                                                                                                                           
procedure TFrameBuyJournalMoving.CustomSetup;
begin
  CategoryVisible['Send'] := cbTrue;

  CategoryVisible['Receive'] := cbTrue;

  if DepartmentID = DataBuy.SelfDepartmentID then
  begin

    if ButtonSend.Down then
    begin
      CategoryVisible['Open + Close'] := cbTrue;

      CategoryVisible['Add'] := cbTrue;

      CategoryVisible['Delete'] := cbTrue;
    end;

    if ButtonReceive.Down then
    begin
      CategoryVisible['Open + Close'] := cbTrue;
    end;
  end;

  if DepartmentID <> 0 then
  begin
    CategoryVisible['Print'] := cbTrue;
  end;
  
  inherited CustomSetup;
end;


end.
