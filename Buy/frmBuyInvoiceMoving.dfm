inherited FrameBuyInvoiceMoving: TFrameBuyInvoiceMoving
  inherited SplitterLeft: TcxSplitter
    ExplicitHeight = 622
  end
  inherited GroupBoxContent: TcxGroupBox
    inherited PageContent: TcxPageControl
      inherited SheetContent: TcxTabSheet
        inherited GridInvoiceItem: TcxGrid
          Height = 238
          ExplicitWidth = 675
          ExplicitHeight = 313
          inherited GridInvoiceItemView: TcxGridDBBandedTableView
            Bands = <
              item
                Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              end
              item
                Caption = #1042#1077#1089
              end
              item
                Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
                Visible = False
              end>
          end
        end
        inherited SplitterTop: TcxSplitter
          ExplicitTop = 298
          ExplicitWidth = 675
        end
        inherited GridStore: TcxGrid
          ExplicitTop = 306
          ExplicitWidth = 675
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited LargeButtonClose: TdxBarLargeButton
      ImageIndex = 9
    end
  end
end
