unit frmBuyStore;

interface

uses

  Forms, Windows, Classes, Controls, DB, DBClient, SysUtils, Variants,
  FIB, FIBDataSet, pFIBDataSet, IBErrorCodes,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxClasses,
  cxGridCustomView, cxGrid,
  dxSkinsCore, dxSkinscxPCPainter, dxSkinsdxBarPainter, dxBar, ActnList,
  cxContainer, cxGroupBox, dxSkinsDefaultPainters, frmPopup, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;


type

{ TBuyStore }

  TFrameBuyStore = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonReload: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    Store: TpFIBDataSet;
    SourceStore: TDataSource;
    MenuOffice: TdxBarPopupMenu;
    Actions: TActionList;
    ActionReload: TAction;
    GroupBoxStore: TcxGroupBox;
    GridStore: TcxGrid;
    GridStoreView: TcxGridDBBandedTableView;
    GridStoreViewMATERIAL: TcxGridDBBandedColumn;
    GridStoreViewPROBE: TcxGridDBBandedColumn;
    GridStoreViewWEIGHT: TcxGridDBBandedColumn;
    GridStoreLevel: TcxGridLevel;
    BarDockControl: TdxBarDockControl;
    GridStoreViewWEIGHTREAL: TcxGridDBBandedColumn;
    ButtonGold: TdxBarLargeButton;
    ButtonSilver: TdxBarLargeButton;
    ActionGold: TAction;
    ActionSilver: TAction;
    GridStoreViewDEPARTMENTNAME: TcxGridDBBandedColumn;
    GridStoreViewWEIGHT1000: TcxGridDBBandedColumn;
    ButtonDate: TdxBarLargeButton;
    BarFilterDockControl: TdxBarDockControl;
    BarFilter: TdxBar;
    ButtonRangeDay: TdxBarLargeButton;
    StoreDEPARTMENTID: TFIBIntegerField;
    StoreDEPARTMENTNAME: TFIBStringField;
    StoreMATERIALID: TFIBIntegerField;
    StoreMATERIALNAME: TFIBStringField;
    StorePROBEID: TFIBIntegerField;
    StorePROBENAME: TFIBStringField;
    StoreWEIGHT: TFIBBCDField;
    StoreWEIGHTREAL: TFIBBCDField;
    StoreWEIGHT1000: TFIBFloatField;
    ButtonMaterial: TdxBarLargeButton;
    MenuMaterial: TdxBarPopupMenu;
    procedure StoreBeforeOpen(DataSet: TDataSet);
    procedure ButtonOfficeClick(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure GridStoreViewCustomDrawBandHeader(Sender: TcxGridBandedTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo;
      var ADone: Boolean);
    procedure ActionMaterialExecute(Sender: TObject);
    procedure GridStoreViewDataControllerGroupingChanged(Sender: TObject);
    procedure ButtonDateClick(Sender: TObject);
    procedure ButtonMaterialClick(Sender: TObject);
  private
    FActive: Boolean;
    OfficeName: string;
    StoreDate: TDate;
    procedure OnDateOk(Popup: TPopup);
    procedure InternalActivate;
    procedure InternalDeactivate;
    function GetDepartmentID: Integer;
    function GetMaterialID: Integer;
  public
    destructor Destroy; override;
    function Activate: Boolean;
    procedure Deactivate;
    procedure Reload;
    property Active: Boolean read FActive;
    property DepartmentID: Integer read GetDepartmentID;
    property MaterialID: Integer read GetMaterialID;
  end;

  TcxDataControllerGroupsAccess = class(TcxDataControllerGroups)
  end;

var

  BuyStore: TFrameBuyStore;

implementation

{$R *.dfm}

uses

  uBuy, dmBuy, uDialog, frmBuyDatePopup;

{ TBuyStore }

destructor TFrameBuyStore.Destroy;
begin
  Deactivate;

  inherited Destroy;
end;


function TFrameBuyStore.Activate: Boolean;
begin
  if not Active then
  begin
    InternalActivate;
  end;

  Result := Active;
end;

procedure TFrameBuyStore.Deactivate;
begin
  if Active then
  begin
    InternalDeactivate;
  end;
end;

procedure TFrameBuyStore.InternalActivate;
var
  CategoryIndex: Integer;

procedure MenuOfficePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  if MenuOffice.ItemLinks.Count = 0 then
  begin
    DataSet := TClientDataSet.Create(nil);

    try

      DataSet.Data := DataBuy.Table(sqlOffice);

      DataSet.First;

      while not DataSet.Eof do
      begin

        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := DataSet.FieldByName('ID').AsInteger;

        Button.Caption := DataSet.FieldByName('NAME').AsString;

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Button.Tag = DataBuy.SelfDepartmentID then
        begin
          Button.Down := True;

          ButtonOffice.Caption := Button.Caption;
        end;

        if MenuOffice.ItemLinks.Count = 2 then
        begin
          ButtonLink.BeginGroup := True;
        end;

        DataSet.Next;
      end;

      if MenuOffice.ItemLinks.Count > 1 then
      begin
        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := 0;

        Button.Caption := '���';

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        ButtonLink.BeginGroup := True;
      end;

    finally

      DataSet.Free;

    end;

  end;
end;

begin

  try

    ButtonOffice.Tag := DataBuy.SelfDepartmentID;

    if DataBuy.IsCenterDepartment then
    begin
      CategoryIndex := BarManager.Categories.IndexOf('Office');

      if CategoryIndex <> -1 then
      begin
        BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways;
      end;

      MenuOfficePopulate;
    end;

    StoreDate := Date;

    FActive := True;

  except

    on E: Exception do
    begin

    end;

  end;
end;

procedure TFrameBuyStore.InternalDeactivate;
begin

  try

    Store.Active := False;

  except

    on E: Exception do
    begin

    end;

  end;

  FActive := False;
end;

function TFrameBuyStore.GetDepartmentID: Integer;
begin
  Result := ButtonOffice.Tag;
end;

function TFrameBuyStore.GetMaterialID: Integer;
begin
  Result := ButtonMaterial.Tag;
end;


procedure TFrameBuyStore.GridStoreViewCustomDrawBandHeader(
  Sender: TcxGridBandedTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TFrameBuyStore.GridStoreViewDataControllerGroupingChanged(Sender: TObject);
begin
  GridStoreView.DataController.Groups.FullCollapse;

  GridStoreView.Controller.TopRowIndex := 0;

  GridStoreView.Controller.FocusedRowIndex := 0;
end;

procedure TFrameBuyStore.StoreBeforeOpen(DataSet: TDataSet);
begin
  Store.ParamByName('DEPARTMENT$ID').AsInteger := DepartmentID;

  Store.ParamByName('STORE$DATE').AsDateTime := StoreDate;

  Store.ParamByName('MATERIAL$ID').AsInteger := MaterialID;
end;

procedure TFrameBuyStore.Reload;
begin
  ActionReload.Execute;
end;

procedure TFrameBuyStore.ActionReloadExecute(Sender: TObject);
var
  BandCaption: string;
begin
  Screen.Cursor := crHourGlass;

  Store.DisableControls;

  try

    if Store.Active then
    begin
      Store.Close;
    end;

    //GridStoreView.Bands[0].Caption := '����';

    Store.Open;

    begin
      if OfficeName = '' then
      begin
        BandCaption := DataBuy.SelfDepartmentName;
      end else

      begin
        BandCaption := OfficeName;
      end;

      ButtonOffice.Caption := BandCaption;

      ButtonDate.Caption := DateRangeAsString(StoreDate, StoreDate);

      if DataBuy.IsModern then
      begin
        ButtonDate.LargeImageIndex := ButtonRangeDay.LargeImageIndex;
      end;

      BandCaption := BandCaption +  #13 + DateRangeAsString(StoreDate, StoreDate);

      //GridStoreView.Bands[0].Caption := BandCaption;

      GridStoreLevel.Caption := BandCaption;
    end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Store.EnableControls;

  GridStoreView.DataController.Groups.FullCollapse;

  GridStoreView.Controller.TopRowIndex := 0;

  GridStoreView.Controller.FocusedRowIndex := 0;

  Screen.Cursor := crDefault;
end;

procedure TFrameBuyStore.ButtonDateClick(Sender: TObject);
var
  Dialog: TDialogBuyDatePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonDate.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyDatePopup.Create(Self);

  Dialog.Date := StoreDate;

  Dialog.OnOk := OnDateOk;

  Dialog.Popup(X, Y);
end;

procedure TFrameBuyStore.OnDateOk(Popup: TPopup);
var
  Dialog: TDialogBuyDatePopup;
begin
  Dialog := TDialogBuyDatePopup(Popup);

  StoreDate := Dialog.Date;

  Reload;
end;


procedure TFrameBuyStore.ButtonOfficeClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonOffice then
    begin
      GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonOffice.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      MenuOffice.Popup(X, Y);
    end else

    begin
      ButtonOffice.Tag := Button.Tag;

      OfficeName := Button.Caption;

      Reload;
    end;
  end;
end;

procedure TFrameBuyStore.ActionMaterialExecute(Sender: TObject);
var
  Action: TAction;
begin
  Action := TAction(Sender);

  ButtonMaterial.Caption := Action.Caption;

  ButtonMaterial.Tag := Action.Tag;

  Reload;
end;

procedure TFrameBuyStore.ButtonMaterialClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonMaterial.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  MenuMaterial.Popup(X, Y);
end;


end.
