object DataBuyInvoice: TDataBuyInvoice
  OldCreateOrder = False
  Height = 277
  Width = 317
  object InvoiceOpen: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$O (?ID)')
    StoredProcName = 'BUY$INVOICE$O'
    Left = 40
    Top = 24
  end
  object InvoiceClose: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$C (?ID)')
    StoredProcName = 'BUY$INVOICE$C'
    Left = 40
    Top = 72
  end
  object InvoiceInsert: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$I (?CLASS$CODE, ?MATERIAL$ID)')
    StoredProcName = 'BUY$INVOICE$I'
    Left = 120
    Top = 24
    qoNoForceIsNull = True
  end
  object InvoiceUpdate: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      
        'EXECUTE PROCEDURE BUY$INVOICE$U (?ID, ?N, ?INVOICE$DATE, ?AGENT$' +
        'ID, ?FOREIGN$N, ?FOREIGN$DATE, ?FOREIGN$COST)')
    StoredProcName = 'BUY$INVOICE$U'
    Left = 120
    Top = 72
  end
  object InvoiceDelete: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$D (?ID)')
    StoredProcName = 'BUY$INVOICE$D'
    Left = 120
    Top = 120
  end
  object InvoiceItemDelete: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$ITEM$D (?INVOICE$ID, ?ID)')
    StoredProcName = 'BUY$INVOICE$ITEM$D'
    Left = 208
    Top = 120
  end
  object InvoiceItemUpdate: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      
        'EXECUTE PROCEDURE BUY$INVOICE$ITEM$U (?ID, ?WEIGHT, ?WEIGHT$REAL' +
        ', ?PRICE)')
    StoredProcName = 'BUY$INVOICE$ITEM$U'
    Left = 208
    Top = 72
  end
  object InvoiceItemInsert: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      
        'EXECUTE PROCEDURE BUY$INVOICE$ITEM$I (?INVOICE$ID, ?PROBE$ID, ?S' +
        'TORE$DATE)')
    StoredProcName = 'BUY$INVOICE$ITEM$I'
    Left = 208
    Top = 24
  end
  object InvoiceNoteGet: TpFIBStoredProc
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$NOTE$S (?ID)')
    StoredProcName = 'BUY$INVOICE$NOTE$S'
    Left = 40
    Top = 152
  end
  object InvoiceNoteSet: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$INVOICE$NOTE$U (:ID, :NOTE)')
    StoredProcName = 'BUY$INVOICE$NOTE$U'
    Left = 40
    Top = 200
  end
end
