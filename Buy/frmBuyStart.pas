unit frmBuyStart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsdxBarPainter, ActnList, dxBar,
  cxClasses, dxGDIPlusClasses, ExtCtrls, cxGroupBox, frxpngimage, cxImage,
  dxSkinsDefaultPainters, DBClient, frmPopup, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin;

type

  TFrameBuyStart = class(TFrame)
    GroupBoxImage: TcxGroupBox;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonSetup: TdxBarLargeButton;
    Actions: TActionList;
    ActionSetup: TAction;
    BarDockControl: TdxBarDockControl;
    Image: TcxImage;
    ActionCollation: TAction;
    ButtonCollation: TdxBarLargeButton;
    procedure ActionCollationUpdate(Sender: TObject);
    procedure ActionCollationExecute(Sender: TObject);
    procedure ActionSetupExecute(Sender: TObject);
  public
    procedure Activate;
  end;

implementation

{$R *.dfm}

uses

 dmBuy,
 frmBuySetup, frmBuyJournalCollation;

procedure TFrameBuyStart.Activate;
var
  CategoryIndex: Integer;
  Link: TdxBarItemLink;

function ItemLink(Bar: TdxBar; Item: TdxBarItem): TdxBarItemLink;
var
  i: Integer;
  Link: TdxBarItemLink;
  Flag: Boolean;
begin
  Link := nil;

  Flag := False;

  if Bar <> nil then
  begin
    if Item <> nil then
    begin
      for i := 0 to Bar.ItemLinks.Count - 1 do
      begin
        Link := Bar.ItemLinks[i];

        if Link.Item = Item then
        begin
          Flag := True;

          break;
        end;
      end;
    end;
  end;

  if not Flag then
  begin
    Link := nil;
  end;

  Result := Link;
end;

begin
  if DataBuy.IsCenterDepartment then
  begin
    CategoryIndex := BarManager.Categories.IndexOf('Office');

    if CategoryIndex <> -1 then
    begin
      BarManager.CategoryItemsVisible[CategoryIndex] := ivNever;
    end;
  end;

  if Bar.ItemLinks.VisibleItemCount = 0 then
  begin
    Bar.Visible := False;

    Image.Style.Edges := [];
  end else
  begin
    Link := ItemLink(Bar, ButtonSetup);

    if Link <> nil then
    begin
      Link.BeginGroup := True;
    end;
  end
end;

procedure TFrameBuyStart.ActionCollationUpdate(Sender: TObject);
begin
  ActionCollation.Enabled := True;
end;

procedure TFrameBuyStart.ActionSetupExecute(Sender: TObject);
var
  SelfCompanyID: Integer;
begin
  SelfCompanyID := DataBuy.SelfCompanyID;

  TDialogBuySetup.Execute(SelfCompanyID);
end;

procedure TFrameBuyStart.ActionCollationExecute(Sender: TObject);
var
  Dialog: TDialogJournalCollation;
begin
  Dialog := TDialogJournalCollation.Create(Self);

  Dialog.Execute;
  
  Dialog.Free;
end;



end.
