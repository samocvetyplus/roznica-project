unit uGridPrinter;

interface

uses

  SysUtils, Windows, Forms, Classes, Controls, Graphics, StrUtils, Math,
  cxGrid, cxGridCustomView, cxGridBandedTableView, cxGridExportLink,
  dxPSCore,dxPScxGrid6Lnk, dxPrnPg, dxPrnDev, dxPSPDFExport, dxPSPrVw, cxClasses,
  dxPSRes, dxPSPrVwRibbon, cxGridTableView, cxGridCustomTableView;

type

{ TGridPrinter }

  TGridPrinter = class
  private
    Printer:TdxComponentPrinter;
    GridReportLink:TdxGridReportLink;
    procedure OnCustomDrawBandCell(Sender: TdxGridReportLink;  ACanvas: TCanvas; AView: TcxGridBandedTableView; ABand: TcxGridBand;  AnItem: TdxReportCellString; var ADone: Boolean);
  private
    Grid: TcxGrid;
    FFileName: string;
    procedure InternalPrint;
    procedure InternalPrintToPDF;
    procedure InternalPrintToXLS;
    procedure OnBeforePreview(Sender: TObject; AReportLink: TBasedxReportLink);
    procedure OnAfterPreview(Sender: TObject; AReportLink: TBasedxReportLink);
  public
    constructor Create; overload;
    constructor Create(FileName: string); overload;
    destructor Destroy; override;
    class procedure Print(Grid: TcxGrid);
    class procedure PrintToPDF(Grid: TcxGrid; FileName: string);
    class procedure PrintToXLS(Grid: TcxGrid; FileName: string);
    property FileName: string read FFileName;
  end;

implementation

uses

  uBuy, dmBuy;

{ TGridPrinter }

class procedure TGridPrinter.Print(Grid:TcxGrid);
var
  GridPrinter:TGridPrinter;
begin
  if Grid <> nil then
  begin
    GridPrinter := TGridPrinter.Create;

    GridPrinter.Grid := Grid;

    GridPrinter.InternalPrint;

    GridPrinter.Free;
  end;
end;

class procedure TGridPrinter.PrintToPDF(Grid: TcxGrid; FileName: string);
var
  GridPrinter:TGridPrinter;
begin
  if Grid <> nil then
  begin
    GridPrinter := TGridPrinter.Create(FileName);

    GridPrinter.Grid := Grid;

    GridPrinter.InternalPrintToPDF;

    GridPrinter.Free;
  end;
end;

class procedure TGridPrinter.PrintToXLS(Grid: TcxGrid; FileName: string);
var
  GridPrinter:TGridPrinter;
begin
  if Grid <> nil then
  begin
    GridPrinter := TGridPrinter.Create(FileName);

    GridPrinter.Grid := Grid;

    GridPrinter.InternalPrintToXLS;

    GridPrinter.Free;
  end;
end;

constructor TGridPrinter.Create;
begin
  Printer:= TdxComponentPrinter.Create(nil);

  Printer.OnBeforePreview := OnBeforePreview;

  Printer.OnAfterPreview := OnAfterPreview;

  GridReportLink:= TdxGridReportLink.Create(nil);

  GridReportLink.ComponentPrinter:= Printer;
end;

constructor TGridPrinter.Create(FileName: string);
begin
  Create;

  FFileName := FileName;
end;

destructor TGridPrinter.Destroy;
begin
  GridReportLink.Free;

  Printer.Free;
end;

procedure TGridPrinter.InternalPrint;
var
  Caption: string;
  Left, Right, Top, Bottom: Integer;
  MinLeft, MinRight, MinTop, MinBottom: Integer;
begin
  Caption := Grid.ActiveLevel.Caption;

  Printer.PrintTitle := AnsiReplaceStr(Caption, #13#10, ' - ');

  Printer.PreviewOptions.Caption := 'Предварительный просмотр';

  GridReportLink.ReportTitle.Text := Caption;

  GridReportLink.ReportDocument.Caption := '';

  GridReportLink.ReportDocument.CreationDate := Date;

  GridReportLink.PrinterPage.DMPaper := DMPAPER_A4;

  //GridReportLink.PrinterPage.Orientation := poLandscape;

  GridReportLink.PrinterPage.ScaleMode := smFit;

  GridReportLink.ShowPageHeader := False;

  GridReportLink.ShowPageFooter := True;

  GridReportLink.ShowPageRowHeader := False;

  GridReportLink.PrinterPage.AutoSwapMargins := False;

  GridReportLink.PrinterPage.PageFooter.RightTitle.Text := '[Page #]';

  GridReportLink.PrinterPage.GetRealMinMargins(MinLeft, MinRight, MinTop, MinBottom);

  GridReportLink.PrinterPage.Margins.Top := Max(MinTop, 5 * 1000);

  GridReportLink.PrinterPage.Margins.Left := Max(MinLeft, 10 * 1000);

  GridReportLink.PrinterPage.Margins.Bottom := Max(MinBottom, 5 * 1000);

  GridReportLink.PrinterPage.Margins.Right := Max(MinRight, 5 * 1000);

  GridReportLink.OptionsView.FilterBar := False;

  GridReportLink.OptionsView.Caption := False;

  GridReportLink.OptionsView.ExpandButtons := False;

  GridReportLink.OptionsOnEveryPage.Caption := False;

  GridReportLink.OptionsOnEveryPage.Footers := False;

  GridReportLink.Component:= Grid;

  GridReportLink.OnCustomDrawBandCell := OnCustomDrawBandCell;

  GridReportLink.SupportedCustomDraw := True;

  GridReportLink.Preview(True);
end;

procedure TGridPrinter.InternalPrintToPDF;
var
  Caption: string;
begin
  Caption := Grid.ActiveLevel.Caption;

  Printer.PrintTitle := Caption;

  Printer.PreviewOptions.Caption := 'Предварительный просмотр';

  GridReportLink.ReportTitle.Text := Caption;

  GridReportLink.ReportDocument.Caption := Caption;

  GridReportLink.ReportDocument.CreationDate := Date;

  GridReportLink.PrinterPage.DMPaper := DMPAPER_A4;

  //GridReportLink.PrinterPage.Orientation := poLandscape;

  GridReportLink.PrinterPage.ScaleMode := smFit;

  GridReportLink.OptionsView.FilterBar := False;

  GridReportLink.OptionsView.Caption := False;

  GridReportLink.OptionsView.ExpandButtons := False;

  GridReportLink.Component:= Grid;

  GridReportLink.OnCustomDrawBandCell := OnCustomDrawBandCell;

  GridReportLink.SupportedCustomDraw := True;

  GridReportLink.PDFExportOptions.OpenDocumentAfterExport := True;

  if FileName <> '' then
  begin
    dxPSExportToPDFFile(FileName, GridReportLink, False, GridReportLink.PDFExportOptions);
  end;
end;

procedure TGridPrinter.InternalPrintToXLS;
begin
  if FileName <> '' then
  begin
    ExportGridToExcel(FileName, Grid, False, True, True, 'xls');
  end;
end;

procedure TGridPrinter.OnBeforePreview(Sender: TObject; AReportLink: TBasedxReportLink);
var
  Rect: TRect;
  PreviewWindow: TdxPSPreviewWindow;
  PreviewForm: TdxRibbonPrintPreviewForm;
  Name: string;
  Folder: string;
  FileName: string;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  PreviewWindow := AReportLink.PreviewWindow as TdxPSPreviewWindow;

  PreviewForm := TdxRibbonPrintPreviewForm(PreviewWindow.Form);

  PreviewForm.BoundsRect := Rect;

  Name := 'jew.print.ini';

  Folder := LocalFolder;

  FileName := LocalFolder + Name;

  if not FileExists(FileName) then
  begin
    Folder := GlobalFolder + Name;

    if not FileExists(FileName) then
    begin
      FileName := '';
    end;
  end;

  if FileName <> '' then
  begin
    PreviewForm.dxRibbon.BarManager.LoadFromIniFile(FileName);
  end;

  PreviewWindow.EnableOptions := [peoPrint, peoPageSetup];

  PreviewWindow.VisibleOptions := [pvoPrint, pvoPageSetup];

  PreviewWindow.ShowMarginBar := False;

  PreviewWindow.SetZoomFactorByText(cxGetResourceString(@sdxWholePage));
end;

procedure TGridPrinter.OnAfterPreview(Sender: TObject; AReportLink: TBasedxReportLink);
var
  Rect: TRect;
  PreviewWindow: TdxPSPreviewWindow;
  PreviewForm: TdxRibbonPrintPreviewForm;
  Name: string;
  Folder: string;
  FileName: string;
begin
  Name := 'jew.print.ini';

  Folder := LocalFolder;

  FileName := LocalFolder + Name;

  PreviewWindow := AReportLink.PreviewWindow as TdxPSPreviewWindow;

  PreviewForm := TdxRibbonPrintPreviewForm(PreviewWindow.Form);

  PreviewForm.dxRibbon.BarManager.SaveToIniFile(FileName);
end;

procedure TGridPrinter.OnCustomDrawBandCell(Sender: TdxGridReportLink; ACanvas: TCanvas; AView: TcxGridBandedTableView; ABand: TcxGridBand;  AnItem: TdxReportCellString; var ADone: Boolean);
begin
  AnItem.Multiline := True;
end;

end.

