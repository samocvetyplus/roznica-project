unit frmBuyJournal;

interface

uses

  Forms, Windows, SysUtils, DB, DBClient, Classes,
  ActnList, Controls, DateUtils, Variants, Graphics,
  FIB, FIBDataSet, pFIBDataSet, iBase,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles, cxCustomData, cxPC,
  cxFilter, cxData, cxDataStorage, cxDBData, cxClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridCustomView, cxGrid, cxGroupBox,
  dxSkinsCore, dxSkinscxPCPainter, dxSkinsdxBarPainter, dxBar,
  frmPopup, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxTextEdit, cxMemo,
  cxSplitter, cxRichEdit, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

  TCustomBoolean = (cbUnknown, cbFalse, cbTrue);

{ TFrameBuyJournal }

  TFrameBuyJournal = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    Action: TActionList;
    ActionAdd: TAction;
    ActionDelete: TAction;
    ActionOpen: TAction;
    ActionClose: TAction;
    InvoiceArray: TpFIBDataSet;
    SourceJournal: TDataSource;
    ActionRange: TAction;
    ActionSend: TAction;
    ActionReceive: TAction;
    ActionReload: TAction;
    ButtonRange: TdxBarLargeButton;
    ButtonInsert: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonOpen: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    ButtonReload: TdxBarLargeButton;
    ButtonPrint: TdxBarLargeButton;
    ButtonSend: TdxBarLargeButton;
    ButtonReceive: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    MenuOffice: TdxBarPopupMenu;
    GroupBoxJournal: TcxGroupBox;
    BarDock: TdxBarDockControl;
    ButtonInvoice: TdxBarLargeButton;
    ButtonRangeCustom: TdxBarButton;
    ButtonRangeDay: TdxBarButton;
    ButtonRangeWeek: TdxBarButton;
    ButtonRangeMonth: TdxBarButton;
    ButtonRangeYear: TdxBarButton;
    MenuPrint: TdxBarPopupMenu;
    ActionUnion: TAction;
    ButtonUnion: TdxBarLargeButton;
    BarFilter: TdxBar;
    BarFilterDock: TdxBarDockControl;
    InvoiceArrayID: TFIBIntegerField;
    InvoiceArrayREFERENCEID: TFIBIntegerField;
    InvoiceArrayFORWARDID: TFIBIntegerField;
    InvoiceArrayBACKWARDID: TFIBIntegerField;
    InvoiceArrayCOMPANYID: TFIBIntegerField;
    InvoiceArrayCOMPANYNAME: TFIBStringField;
    InvoiceArrayDEPARTMENTID: TFIBIntegerField;
    InvoiceArrayDEPARTMENTNAME: TFIBStringField;
    InvoiceArrayCLASSCODE: TFIBIntegerField;
    InvoiceArrayCLASSNAME: TFIBStringField;
    InvoiceArrayROUTE: TFIBIntegerField;
    InvoiceArrayN: TFIBIntegerField;
    InvoiceArrayINVOICEDATE: TFIBDateField;
    InvoiceArraySENDERID: TFIBIntegerField;
    InvoiceArraySENDERNAME: TFIBStringField;
    InvoiceArrayRECEIVERID: TFIBIntegerField;
    InvoiceArrayRECEIVERNAME: TFIBStringField;
    InvoiceArrayMATERIALID: TFIBIntegerField;
    InvoiceArrayMATERIALNAME: TFIBStringField;
    InvoiceArrayAGENTID: TFIBIntegerField;
    InvoiceArrayAGENTCLASSCODE: TFIBIntegerField;
    InvoiceArrayAGENTNAME: TFIBStringField;
    InvoiceArrayTAXID: TFIBIntegerField;
    InvoiceArrayTAXNAME: TFIBStringField;
    InvoiceArrayTAX: TFIBIntegerField;
    InvoiceArrayWEIGHT: TFIBBCDField;
    InvoiceArrayWEIGHTREAL: TFIBBCDField;
    InvoiceArrayWEIGHT1000: TFIBBCDField;
    InvoiceArrayCOST: TFIBBCDField;
    InvoiceArrayCOSTTAX: TFIBBCDField;
    InvoiceArrayCOSTTOTAL: TFIBBCDField;
    InvoiceArraySTATECODE: TFIBIntegerField;
    InvoiceArraySTATENAME: TFIBStringField;
    InvoiceArraySTATEDATE: TFIBDateTimeField;
    InvoiceArraySTATEBYID: TFIBIntegerField;
    InvoiceArraySTATEBYNAME: TFIBStringField;
    InvoiceArrayFOREIGNN: TFIBStringField;
    InvoiceArrayFOREIGNDATE: TFIBDateField;
    InvoiceArrayFOREIGNCOST: TFIBBCDField;
    ButtonGold: TdxBarLargeButton;
    ButtonSilver: TdxBarLargeButton;
    ActionGold: TAction;
    ActionSilver: TAction;
    ButtonMaterial: TdxBarLargeButton;
    MenuMaterial: TdxBarPopupMenu;
    Splitter: TcxSplitter;
    cxGroupBox1: TcxGroupBox;
    GridJournal: TcxGrid;
    GridJournalView: TcxGridDBBandedTableView;
    GridJournalViewN: TcxGridDBBandedColumn;
    GridJournalViewINVOICEDATE: TcxGridDBBandedColumn;
    GridJournalViewMATERIALNAME: TcxGridDBBandedColumn;
    GridJournalViewSENDERNAME: TcxGridDBBandedColumn;
    GridJournalViewRECEIVERNAME: TcxGridDBBandedColumn;
    GridJournalViewCOSTTOTAL: TcxGridDBBandedColumn;
    GridJournalViewWEIGHT: TcxGridDBBandedColumn;
    GridJournalViewWEIGHTREAL: TcxGridDBBandedColumn;
    GridJournalViewWEIGHT1000: TcxGridDBBandedColumn;
    GridJournalViewSTATECODE: TcxGridDBBandedColumn;
    GridJournalViewSTATENAME: TcxGridDBBandedColumn;
    GridJournalViewSTATEDATE: TcxGridDBBandedColumn;
    GridJournalViewSTATEBYNAME: TcxGridDBBandedColumn;
    GridJournalViewFOREIGNN: TcxGridDBBandedColumn;
    GridJournalViewFOREIGNDATE: TcxGridDBBandedColumn;
    GridJournalViewCLASSNAME: TcxGridDBBandedColumn;
    GridJournalLevel: TcxGridLevel;
    GroupBoxNote: TcxGroupBox;
    DockControlNote: TdxBarDockControl;
    EditorNote: TcxMemo;
    ButtonNoteOk: TdxBarLargeButton;
    ButtonNoteCancel: TdxBarLargeButton;
    BarNote: TdxBar;
    ButtonNote: TdxBarLargeButton;
    ActionNoteOK: TAction;
    ActionNoteCancel: TAction;
    ActionPrint: TAction;
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure ActionOpenUpdate(Sender: TObject);
    procedure ActionCloseUpdate(Sender: TObject);
    procedure ActionPrintUpdate(Sender: TObject);
    procedure ActionRangeUpdate(Sender: TObject);
    procedure ActionAddUpdate(Sender: TObject);
    procedure ActionRangeExecute(Sender: TObject);
    procedure ActionOpenExecute(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure ActionAddExecute(Sender: TObject);
    procedure JournalProviderBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TCustomClientDataSet;
      UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure InvoiceArrayBeforeOpen(DataSet: TDataSet);
    procedure OnActionRouteExecute(Sender: TObject);
    procedure ButtonOfficeClick(Sender: TObject);
    procedure GridJournalViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridJournalViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ButtonInvoiceClick(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure GridJournalViewCustomDrawBandHeader(
      Sender: TcxGridBandedTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
    procedure ActionUnionUpdate(Sender: TObject);
    procedure ActionUnionExecute(Sender: TObject);
    procedure GridJournalViewDataControllerGroupingChanged(Sender: TObject);
    procedure GridJournalViewStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure InvoiceArrayAfterOpen(DataSet: TDataSet);
    procedure ActionMaterialExecute(Sender: TObject);
    procedure ButtonMaterialClick(Sender: TObject);
    procedure SplitterBeforeClose(Sender: TObject; var AllowClose: Boolean);
    procedure SplitterBeforeOpen(Sender: TObject; var NewSize: Integer;
      var AllowOpen: Boolean);
    procedure SourceJournalDataChange(Sender: TObject; Field: TField);
    procedure ButtonNoteOkClick(Sender: TObject);
    procedure ButtonNoteCancelClick(Sender: TObject);
    procedure ActionNoteUpdate(Sender: TObject);
    procedure ButtonNoteClick(Sender: TObject);
    procedure ActionNoteOKExecute(Sender: TObject);
    procedure ActionNoteCancelExecute(Sender: TObject);
    procedure EditorNoteExit(Sender: TObject);
  private
    FActive: Boolean;
    FReadOnly: Boolean;
    NoteID: Integer;
    OfficeName: string;
    InvoiceID: Integer;
    InvoiceClassCode: Integer;
    CategoryArray: TStringList;
    procedure NoteGet;
    procedure NoteSet;
    procedure Setup;
    procedure UpdateRange;
    function GetDepartmentID: Integer;
    procedure OnRangeOk(Popup: TPopup);
    function GetCategoryVisible(Name: string): TCustomBoolean;
    procedure SetCategoryVisible(Name: string; Value: TCustomBoolean);
    function GetMaterialID: Integer;
  protected
    RangeBegin: TDate;
    RangeEnd: TDate;
    procedure CustomSetup; virtual;
    function GetClassCode: Integer; virtual;
    function GetInvoiceClass: TFrameBuyInvoiceClass; virtual;
    function InternalActivate: Boolean; virtual;
    procedure InternalDeactivate; virtual;
    property CategoryVisible[Name: string]: TCustomBoolean read GetCategoryVisible write SetCategoryVisible;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Activate: Boolean;
    procedure Deactivate;
    procedure Reload;
    property Active: Boolean read FActive;
    property ClassCode: Integer read GetClassCode;
    property DepartmentID: Integer read GetDepartmentID;
    property MaterialID: Integer read GetMaterialID;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
  end;

  TFrameBuyJournalClass = class of TFrameBuyJournal;

implementation

{$R *.dfm}

uses

  dmBuy, dmBuyInvoice,
  frmBuyRangePopup, frmBuyInvoiceReport, frmBuyInvoiceUnion,
  uBuy, uDialog, frmBuyRange, comdata;

constructor TFrameBuyJournal.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFrameBuyJournal.Destroy;
begin
  Deactivate;

  inherited Destroy;
end;


procedure TFrameBuyJournal.EditorNoteExit(Sender: TObject);
begin
  if EditorNote.ModifiedAfterEnter then
  begin
    if TDialog.Confirmation('������� ���� ��������.'#13#10'��������� ���������?') = mrYes then
    begin
      ActionNoteOK.Execute;
    end;
  end;
end;

function TFrameBuyJournal.GetClassCode: Integer;
begin
  Result := 0;
end;

function TFrameBuyJournal.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoice;
end;

function TFrameBuyJournal.GetMaterialID: Integer;
begin
  Result := ButtonMaterial.Tag;
end;

function TFrameBuyJournal.GetDepartmentID: Integer;
begin
  Result := ButtonOffice.Tag;
end;

procedure TFrameBuyJournal.InvoiceArrayAfterOpen(DataSet: TDataSet);
begin
  //GridJournalView.ApplyBestFit;
end;

procedure TFrameBuyJournal.InvoiceArrayBeforeOpen(DataSet: TDataSet);
begin
  InvoiceArray.Params.ParamByName('DEPARTMENT$ID').AsInteger := DepartmentID;

  InvoiceArray.Params.ParamByName('CLASS$CODE').AsInteger := ClassCode;

  InvoiceArray.Params.ParamByName('RANGE$BEGIN').AsDate := RangeBegin;

  InvoiceArray.Params.ParamByName('RANGE$END').AsDate := RangeEnd;

  InvoiceArray.Params.ParamByName('MATERIAL$ID').AsInteger := MaterialID;
end;

procedure TFrameBuyJournal.JournalProviderBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
begin
  Applied := True;
end;

procedure TFrameBuyJournal.OnActionRouteExecute(Sender: TObject);
var
  Action: TAction;
begin
  Action := TAction(Sender);

  ActionAdd.Caption := Action.Caption;

  Setup;
  
  ActionReload.Execute;
end;


procedure TFrameBuyJournal.GridJournalViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  iValue: Integer;
  Value: Variant;
begin
  AStyle := DataBuy.StyleDefault;

  if AItem <> nil then
  begin
    if AItem.Tag = 1 then
    begin
      Index := GridJournalViewSTATECODE.Index;

      Value := ARecord.Values[Index];

      if VarIsNull(Value) then
      begin
        iValue := 0;
      end else
      begin
        iValue := Value;
      end;

      if iValue = 2 then
      begin
        AStyle := DataBuy.StyleGray;
      end else

      begin
        AStyle := DataBuy.StyleInfo;
      end;

    end else

    begin
      if ARecord.Selected then
      begin
        AStyle := DataBuy.StyleSelected;
      end;
    end;

  end else
  begin
    if ARecord.Selected then
    begin
      AStyle := DataBuy.StyleSelected;
    end;
  end;
end;

procedure TFrameBuyJournal.ActionRangeExecute(Sender: TObject);
var
  DialogModern: TDialogBuyRangePopup;
  Dialog: TDialogByRange;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  if DataBuy.IsModern then
  begin
    GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

    Y := BarWindowRect.Bottom + 4;

    ButtonRect := ButtonRange.ClickItemLink.ItemRect;

    X := BarWindowRect.Left + ButtonRect.Left;

    DialogModern := TDialogBuyRangePopup.Create(Self);

    DialogModern.RangeBegin := RangeBegin;

    DialogModern.RangeEnd := RangeEnd;

    DialogModern.OnOk := OnRangeOk;

    DialogModern.Popup(X, Y);
  end else
  
  begin
    Dialog := TDialogByRange.Create(Self);

    Dialog.RangeBegin := RangeBegin;

    Dialog.RangeEnd := RangeEnd;

    if Dialog.Execute then
    begin
      RangeBegin := Dialog.RangeBegin;

      RangeEnd := Dialog.RangeEnd;

      UpdateRange;

      ActionReload.Execute;
    end;

    Dialog.Free;
  end;
end;

procedure TFrameBuyJournal.ActionOpenExecute(Sender: TObject);
var                      
  ID: Integer;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ���������?') <> mrYes then
  begin
    exit;
  end;

  Screen.Cursor := crHourGlass;

  InvoiceArray.DisableControls;

  try

    ID := InvoiceArrayID.AsInteger;

    //with DataBuy, DataBuyInvoice do
    begin
      DataBuyInvoice.InvoiceOpen.ParamByName('ID').AsInteger := ID;

      try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyInvoice.InvoiceOpen.ExecProc;

        DataBuy.Commit;

        InvoiceArray.Refresh;

      finally

        DataBuy.Rollback;

      end;
    end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  InvoiceArray.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TFrameBuyJournal.ActionCloseExecute(Sender: TObject);
var
  ID: Integer;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ���������?') <> mrYes then
  begin
    exit;
  end;

  Screen.Cursor := crHourGlass;

  InvoiceArray.DisableControls;

  try

    ID := InvoiceArrayID.AsInteger;

    //with DataBuy, DataBuyInvoice do
    begin
      DataBuyInvoice.InvoiceClose.ParamByName('ID').AsInteger := ID;

      try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyInvoice.InvoiceClose.ExecProc;

        DataBuy.Commit;

        InvoiceArray.Refresh;

      finally

        DataBuy.Rollback;

      end

    end

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  InvoiceArray.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TFrameBuyJournal.ActionDeleteExecute(Sender: TObject);
var
  ID: Integer;
  PageControl: TcxPageControl;
  Sheet: TcxTabSheet;
  PageIndex: Integer;
  PageCount: Integer;
  Flag: Boolean;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ���������?') <> mrYes then
  begin
    exit;
  end;

  ID := InvoiceArrayID.AsInteger;

  //with DataBuy, DataBuyInvoice do
  begin
    DataBuyInvoice.InvoiceDelete.ParamByName('ID').AsInteger := ID;


    DataBuy.Rollback;

    DataBuy.StartTransaction;

    try

      DataBuyInvoice.InvoiceDelete.ExecProc;

      DataBuy.Commit;

      InvoiceArray.Delete;

      begin
        if Owner is TcxTabSheet then
        begin
          Sheet := TcxTabSheet(Owner);

          PageControl := Sheet.PageControl;

          PageIndex := 1;

          PageCount := PageControl.PageCount;

          Flag := False;

          Sheet := nil;

          while PageIndex <= PageCount - 1 do
          begin
            Sheet := PageControl.Pages[PageIndex];

            if Sheet.Tag = ID then
            begin
              Flag := True;

              break;
            end;

            Inc(PageIndex);
          end;

          if Flag then
          begin
            Sheet.Free;
          end;
        end;
      end;

    except

      on E: Exception do
      begin

       TDialog.Error(E.Message);

       DataBuy.Rollback;

      end;

    end;

  end;
end;

procedure TFrameBuyJournal.ActionReloadExecute(Sender: TObject);
var stayfocus:integer;
begin
  stayfocus := GridJournalView.Controller.FocusedRowIndex;
  if ClassCode <> 0 then
  begin
    InvoiceArray.DisableControls;

    try

      Screen.Cursor := crHourGlass;

      InvoiceArray.Active := False;

      InvoiceArray.Active := True;

    finally

      Screen.Cursor := crDefault;

      InvoiceArray.EnableControls;

    end;

    if GridJournalView.GroupedColumnCount <> 0 then
    begin
      GridJournalView.DataController.Groups.FullCollapse;
    end;

    GridJournalView.Controller.TopRowIndex := 0;

    GridJournalView.Controller.FocusedRowIndex := stayfocus;

  end;

end;

procedure TFrameBuyJournal.ActionAddExecute(Sender: TObject);
var
  ID: Integer;
begin
  //with DataBuy, DataBuyInvoice do
  begin
    DataBuyInvoice.InvoiceInsert.ParamByName('CLASS$CODE').AsInteger := ClassCode;

    DataBuyInvoice.InvoiceInsert.ParamByName('MATERIAL$ID').AsInteger := MaterialID;    

    DataBuy.Rollback;

    DataBuy.StartTransaction;

    try

      DataBuyInvoice.InvoiceInsert.ExecProc;

      DataBuy.Commit;      

      ID := DataBuyInvoice.InvoiceInsert.ParamByName('ID').AsInteger;

      InvoiceArray.Append;

      InvoiceArrayID.AsInteger := ID;

      InvoiceArray.Post;

      InvoiceID := ID;

      InvoiceClassCode := ClassCode;

      ButtonInvoice.Click;

    except

      on E: Exception do
      begin
        if InvoiceArray.State <> dsBrowse then
        begin
          InvoiceArray.Cancel;
        end;

        TDialog.Error(E.Message);

        DataBuy.Rollback;
      end;

    end;

  end;
end;

procedure TFrameBuyJournal.ActionAddUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not ReadOnly;

  if Enabled then
  begin
    Enabled := InvoiceArray.Active;
  end;

  if Enabled then
  begin
    Enabled := ClassCode <> 0;
  end;

  ActionAdd.Enabled := Enabled;
end;

procedure TFrameBuyJournal.ActionDeleteUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not ReadOnly;

  if Enabled then
  begin
    Enabled := not InvoiceArray.IsEmpty;
  end;

  ActionDelete.Enabled := Enabled;
end;


procedure TFrameBuyJournal.ActionOpenUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not ReadOnly;

  if Enabled then
  begin
    Enabled := not InvoiceArray.IsEmpty;
  end;

  if Enabled then
  begin
    Enabled := InvoiceArraySTATECODE.AsInteger = 2;
  end;

  ActionOpen.Enabled := Enabled;
end;

procedure TFrameBuyJournal.ActionCloseUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not ReadOnly;

  if Enabled then
  begin
    Enabled := not InvoiceArray.IsEmpty;
  end;

  if Enabled then
  begin
    Enabled := InvoiceArraySTATECODE.AsInteger <> 2;
  end;

  ActionClose.Enabled := Enabled;
end;

procedure TFrameBuyJournal.ActionPrintUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := (ClassCode = Low(Integer)) or (ClassCode = High(Integer));

  if not Enabled then
  begin
    Enabled := not InvoiceArray.IsEmpty;
  end;

  ButtonPrint.Enabled := Enabled;
end;

procedure TFrameBuyJournal.ActionRangeUpdate(Sender: TObject);
begin
  ActionPrintUpdate(nil);

  ActionRange.Enabled := True;
end;

procedure TFrameBuyJournal.GridJournalViewCellDblClick(Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  InvoiceID := InvoiceArrayID.AsInteger;

  InvoiceClassCode := ClassCode;

  ButtonInvoice.Click;

  AHandled := True;
end;

procedure TFrameBuyJournal.GridJournalViewCustomDrawBandHeader(
  Sender: TcxGridBandedTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TFrameBuyJournal.GridJournalViewDataControllerGroupingChanged(Sender: TObject);
begin
  GridJournalView.DataController.Groups.FullCollapse;

  GridJournalView.Controller.TopRowIndex := 0;

  GridJournalView.Controller.FocusedRowIndex := 0;
end;

procedure TFrameBuyJournal.GridJournalViewKeyDown(Sender: TObject;  var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    InvoiceID := InvoiceArrayID.AsInteger;

    InvoiceClassCode := ClassCode;

    ButtonInvoice.Click;
  end;
end;


function TFrameBuyJournal.Activate: Boolean;
begin
  if not Active then
  begin
    FActive := InternalActivate;
  end;

  Result := Active;
end;

procedure TFrameBuyJournal.Deactivate;
begin
  if Active then
  begin
    InternalDeactivate;

    FActive := False;
  end;
end;

function TFrameBuyJournal.InternalActivate: Boolean;

procedure MenuOfficePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  if MenuOffice.ItemLinks.Count = 0 then
  begin
    DataSet := TClientDataSet.Create(nil);

    try

      DataSet.Data := DataBuy.Table(sqlOffice);

      DataSet.First;

      while not DataSet.Eof do
      begin

        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := DataSet.FieldByName('ID').AsInteger;

        Button.Caption := DataSet.FieldByName('NAME').AsString;

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        if DataBuy.IsCenterDepartment then
        begin
          if ClassCode <> 2 then
          begin
            if Button.Tag = DataBuy.SelfDepartmentID then
            begin
              Button.Down := True;
            end;
          end;
        end else
        begin
          if Button.Tag = DataBuy.SelfDepartmentID then
          begin
            Button.Down := True;
          end;
        end;

        if MenuOffice.ItemLinks.Count = 2 then
        begin
          ButtonLink.BeginGroup := True;
        end;

        DataSet.Next;
      end;

      if MenuOffice.ItemLinks.Count > 1 then
      begin
        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := 0;

        Button.Caption := '���';

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        if DataBuy.IsCenterDepartment then
        begin
          if ClassCode = 2 then
          begin
            Button.Down := True;

            OfficeName := Button.Caption;

            ButtonOffice.Tag := 0;
          end;
        end;

        ButtonLink.BeginGroup := True;
      end;

    finally

      DataSet.Free;

    end;

  end;
end;

begin
  Result := False;

  Screen.Cursor := crHourGlass;  

  try

    CategoryArray := TStringList.Create;

    CategoryArray.AddStrings(BarManager.Categories);

    ButtonOffice.Tag := DataBuy.SelfDepartmentID;

    RangeBegin := DateOf(StartOfTheMonth(Date));

    RangeEnd := DateOf(EndOfTheMonth(Date));

    if DataBuy.IsCenterDepartment then
    begin
      MenuOfficePopulate;
    end;

    UpdateRange;

    Setup;

    Splitter.MinSize := GroupBoxNote.Height;

    Splitter.CloseSplitter;

    InvoiceArray.Active := True;

    GridJournalView.Controller.TopRowIndex := 0;

    GridJournalView.Controller.FocusedRowIndex := 0;

    Result := True;

  except

    on E: Exception do
    begin

    end;

  end;

  Screen.Cursor := crDefault;
end;

procedure TFrameBuyJournal.InternalDeactivate;
begin
  try

   InvoiceArray.Active := False;

   CategoryArray.Free;

  except

    on E: Exception do
    begin

    end;

  end;

end;

procedure TFrameBuyJournal.ButtonOfficeClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonOffice then
    begin                                          
      GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonOffice.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      //MenuOffice.Popup(X, Y);

      MenuOffice.PopupEx(X, Y, 100, 100, True, nil);
    end else

    begin
      ButtonOffice.Tag := Button.Tag;

      OfficeName := Button.Caption;

      UpdateRange;

      Setup;

      ActionReload.Execute;
    end;
  end;
end;

procedure TFrameBuyJournal.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeBegin := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

  UpdateRange;

  ActionReload.Execute;
end;

procedure TFrameBuyJournal.Reload;
var
  ID: Integer;
begin
  if GridJournalView.GroupedColumnCount = 0 then
  begin
    ID := InvoiceArrayID.AsInteger;

    ActionReload.Execute;

    GridJournalView.DataController.LocateByKey(ID);
  end else
  begin
    ActionReload.Execute;
  end;
end;

function DateCompare(Item1, Item2: Pointer): Integer;
begin
  Result := Integer(Item1) - Integer(Item2);
end;

procedure TFrameBuyJournal.UpdateRange;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
  BandCaption: string;
begin
  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  ButtonArray[drCustom] := ButtonRangeCustom;

  ButtonArray[drDay] := ButtonRangeDay;

  ButtonArray[drWeek] := ButtonRangeWeek;

  ButtonArray[drMonth] := ButtonRangeMonth;

  ButtonArray[drYear] := ButtonRangeYear;

  if DataBuy.IsModern then
  begin
    ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;
  end;

  if OfficeName = '' then
  begin
    BandCaption := DataBuy.SelfDepartmentName;
  end else
  begin
    BandCaption := OfficeName;
  end;

  ButtonOffice.Caption := BandCaption;

  ButtonRange.Caption := DateRangeAsString(RangeBegin, RangeEnd);

  BandCaption := BandCaption + #13#10 + DateRangeAsString(RangeBegin, RangeEnd);

  {

  GridJournalView.Bands[0].Caption := BandCaption;

  }

  GridJournalLevel.Caption := BandCaption;

end;


procedure TFrameBuyJournal.ButtonInvoiceClick(Sender: TObject);
var
  InvoiceClass: TFrameBuyInvoiceClass;
  Invoice: TFrameBuyInvoice;
  Sheet: TcxTabSheet;
  PageControl: TcxPageControl;
  PageIndex: Integer;
  PageCount: Integer;
  CurrentSheet: TcxTabSheet;
  N: Integer;
  Flag: Boolean;
  Enabled: Boolean;
  DepartmentID: Integer;
  StateCode: Integer;
begin
  if InvoiceID <> 0 then
  begin
    InvoiceClass := GetInvoiceClass;

    if InvoiceClass <> nil then
    begin
      if Owner is TcxTabSheet then
      begin
        Sheet := TcxTabSheet(Owner);

        PageControl := Sheet.PageControl;

        PageIndex := 1;

        PageCount := PageControl.PageCount;

        Flag := False;

        CurrentSheet := nil;

        while PageIndex <= PageCount - 1 do
        begin
          CurrentSheet := PageControl.Pages[PageIndex];

          if CurrentSheet.Tag = InvoiceID then
          begin
            Flag := True;

            break;
          end;

          Inc(PageIndex);
        end;

        if Flag then
        begin
          PageControl.ActivePage := CurrentSheet;
        end else

        begin
          Sheet := TcxTabSheet.Create(PageControl);

          Sheet.TabVisible := False;

          Sheet.Tag := InvoiceID;

          Sheet.PageControl := PageControl;

          Invoice := InvoiceClass.Create(Sheet);

          Invoice.ClassCode := InvoiceClassCode;

          Invoice.Align := alClient;

          Invoice.Parent := Sheet;

          Invoice.ID := InvoiceID;

          Invoice.MaterialID := MaterialID;

          Enabled := not ReadOnly;

          if Enabled then
          begin
            DepartmentID := DataBuy.Value('select department$id from buy$invoice where id = :id', [InvoiceID]);

            Enabled := DepartmentID = DataBuy.SelfDepartmentID;
          end;

          if Enabled then
          begin
            StateCode := DataBuy.Value('select state$code from buy$invoice where id = :id', [InvoiceID]);

            Enabled := StateCode <> 2;
          end;

          if Enabled then
          begin

            if ButtonReceive.Down then
            begin
              Enabled := not (ClassCode in [2, 4]);
            end;

          end;

          Invoice.ReadOnly := not Enabled;

          if ClassCode in [2, 3, 4] then
          begin
            Invoice.LayoutItemAgent.CaptionOptions.Text := '�����';
          end else
          begin
            Invoice.LayoutItemAgent.CaptionOptions.Text := '��������';
          end;

          if (ClassCode mod 2) <> 0 then
          begin
            //Invoice.LayoutGroupInvoice.Caption := ButtonSend.Caption;
          end else

          begin
            //Invoice.LayoutGroupInvoice.Caption := ButtonReceive.Caption;
          end;

          if Invoice.Activate then
          begin
            N := Invoice.N;

            PageIndex := 1;

            PageCount := PageControl.PageCount;

            Flag := False;

            while PageIndex <= PageCount - 1 do
            begin
              CurrentSheet := PageControl.Pages[PageIndex];

              if Sheet <> CurrentSheet then
              begin
                if StrToInt(CurrentSheet.Caption) > N then
                begin
                  Flag := True;

                  break;
                end;
              end;

              Inc(PageIndex);
            end;

            if Flag then
            begin
              Sheet.PageIndex := PageIndex;
            end;

            Sheet.TabVisible := True;

            PageControl.ActivePage := Sheet;

            Sheet.Caption := IntToStr(Invoice.N);
          end else
          begin
            Sheet.Free;
          end;
        end;
      end;
    end;
  end;
end;

procedure TFrameBuyJournal.ButtonPrintClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
  ID : Integer;
  StateCode: Integer;
  Dialog: TDialogBuyInvoiceReport;
  Day, Month, Year: Integer;
  Flag: Boolean;
begin
  StateCode := InvoiceArraySTATECODE.AsInteger;

  if StateCode = 2 then
  begin
    if Sender is TdxBarLargeButton then
    begin
      Button := TdxBarLargeButton(Sender);

      if Button = ButtonPrint then
      begin

        if MenuPrint.ItemLinks.Count <> 0 then
        begin
          if MenuPrint.ItemLinks.Count <> 1 then
          begin
            GetWindowRect(Bar.Control.Handle, BarWindowRect);

            Y := BarWindowRect.Bottom + 4;

            ButtonRect := ButtonPrint.ClickItemLink.ItemRect;

            X := BarWindowRect.Left + ButtonRect.Left;

            MenuPrint.Popup(X, Y);
          end else

          begin
            MenuPrint.ItemLinks[0].Item.Click;
          end;
        end;

      end else

      begin
        if ClassCode = 2 then
        begin
          Flag := False;

          Day := DayOf(RangeBegin);

          if Day = 1 then
          begin
            Month := MonthOf(RangeBegin);

            Year := YearOf(RangeBegin);

            Day := DaysInAMonth(Year, Month);

            if RangeEnd = EncodeDate(Year, Month, Day) then
            begin
              Flag := True;
            end;
          end;

          if not Flag then
          begin
            TDialog.Error('������ "���������� ������" ������ ���� ����� ������.');

            Exit;
          end;

        end;

        ID := InvoiceArrayID.AsInteger;

        Dialog := TDialogBuyInvoiceReport.Create(Self);

        Dialog.ID := Button.Tag;

        Dialog.InvoiceID := ID;

        Dialog.DepartmentID := DepartmentID;

        Dialog.MaterialID := MaterialID;

        Dialog.ClassCode := ClassCode;

        Dialog.RangeBegin := RangeBegin;

        Dialog.RangeEnd := RangeEnd;

        Dialog.Execute;

        Dialog.Free;
      end;
    end;
  end else
  begin
    TDialog.Error('���������� ����������.'#13#10#13#10'��������� ������� ���������.');
  end;
end;

procedure TFrameBuyJournal.ActionUnionExecute(Sender: TObject);
begin
  BuyInvoiceUnion := TBuyInvoiceUnion.Create(Self);

  BuyInvoiceUnion.DepartmentID := DepartmentID;

  BuyInvoiceUnion.ClassCode := ClassCode;

  BuyInvoiceUnion.RangeBegin := RangeBegin;

  BuyInvoiceUnion.RangeEnd := RangeEnd;

  BuyInvoiceUnion.MaterialID := MaterialID;

  if OfficeName = '' then
  begin
    BuyInvoiceUnion.OfficeName :=  DataBuy.SelfDepartmentName;
  end else
  begin
    BuyInvoiceUnion.OfficeName :=  OfficeName;
  end;

  BuyInvoiceUnion.Execute;

  BuyInvoiceUnion.Free;
end;

procedure TFrameBuyJournal.ActionUnionUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not InvoiceArray.IsEmpty;

  ActionUnion.Enabled := Enabled;
end;

procedure TFrameBuyJournal.Setup;
var
  CategoryIndex: Integer;
  Visible: TCustomBoolean;
begin
  Visible := cbUnknown;

  for CategoryIndex := 0 to CategoryArray.Count - 1 do
  begin
    CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Visible));
  end;

  CustomSetup;

  for CategoryIndex := 0 to CategoryArray.Count - 1 do
  begin
    Visible := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);

    if Visible <> cbUnknown then
    begin
      if Visible = cbTrue then
      begin
        BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways;
      end else
      begin
           if (Visible = cbFalse) and ( dmCom.HereName = '�����') then
               BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways
           else
                BarManager.CategoryItemsVisible[CategoryIndex] := ivNever;
      end;


    end;
  end;
end;

procedure TFrameBuyJournal.NoteGet;
var
  ID: Integer;
  Note: string;
begin
  if InvoiceArray.IsEmpty then
  begin
    NoteID := 0;

    EditorNote.Lines.Text := '';

    EditorNote.Enabled := False;
  end else
  begin
    ID := InvoiceArrayID.AsInteger;

    if ID <> NoteID then
    begin
      NoteID := ID;

      DataBuyInvoice.InvoiceNoteGet.ParamByName('ID').AsInteger := NoteID;

      DataBuyInvoice.InvoiceNoteGet.ExecProc;

      EditorNote.Text := DataBuyInvoice.InvoiceNoteGet.ParamByName('NOTE').AsString;

      EditorNote.Enabled := True;

      if InvoiceArrayStateCode.AsInteger in [0, 1] then
      begin
        EditorNote.Style.Color := clWindow;

        EditorNote.Properties.ReadOnly := False;
      end else
      begin
        EditorNote.Style.Color := clBtnFace;

        EditorNote.Properties.ReadOnly := True;
      end;
    end;
  end;
end;


procedure TFrameBuyJournal.NoteSet;
var
  Note: string;
  Stream: TStringStream;
begin
  Note := EditorNote.Lines.Text;

  if NoteID <> 0 then
  begin
   
    try

      DataBuy.Rollback;
      
      DataBuy.StartTransaction;

      DataBuyInvoice.InvoiceNoteSet.ParamByName('ID').AsInteger := NoteID;

      DataBuyInvoice.InvoiceNoteSet.ParamByName('NOTE').AsString := Note;

      DataBuyInvoice.InvoiceNoteSet.ExecProc;

      DataBuy.Commit;

      EditorNote.ModifiedAfterEnter := False;

    except
    
      on E: Exception do
      begin
        DataBuy.Rollback;      
        
        TDialog.Error(E.Message);
      end;

    end;

  end;
end;

procedure TFrameBuyJournal.SourceJournalDataChange(Sender: TObject; Field: TField);
begin
  if Splitter.State = ssOpened then
  begin
    NoteGet;
  end;
end;

procedure TFrameBuyJournal.CustomSetup;
begin
  if DataBuy.IsCenterDepartment then
  begin
    CategoryVisible['Office'] := cbTrue;
  end else
  begin
    CategoryVisible['Office'] := cbFalse;
  end;

  CategoryVisible['Common'] := cbTrue;

  CategoryVisible['Send'] := cbFalse;

  CategoryVisible['Receive'] := cbFalse;

  CategoryVisible['Gold'] := cbTrue;

  CategoryVisible['Silver'] := cbTrue;

  CategoryVisible['Open + Close'] := cbFalse;

  CategoryVisible['Add'] := cbFalse;

  CategoryVisible['Delete'] := cbFalse;

  CategoryVisible['Print'] := cbFalse;

  if ClassCode in [2] then
  begin
    GridJournalViewSENDERNAME.Visible := False;

    GridJournalViewRECEIVERNAME.Visible := DepartmentID = 0;
  end else

  if ClassCode in [3, 5, 7, 9] then
  begin
    GridJournalViewSENDERNAME.Visible := DepartmentID = 0;

    GridJournalViewRECEIVERNAME.Visible := True;
  end else

  if ClassCode in [4, 6, 8, 10] then
  begin
    GridJournalViewSENDERNAME.Visible := True;

    GridJournalViewRECEIVERNAME.Visible := DepartmentID = 0;
  end else

  if ClassCode = Low(Integer) then
  begin
    GridJournalViewSENDERNAME.Visible := DepartmentID = 0;

    GridJournalViewRECEIVERNAME.Visible := True;
  end else

  if ClassCode = High(Integer) then
  begin
    GridJournalViewSENDERNAME.Visible := True;

    GridJournalViewRECEIVERNAME.Visible := DepartmentID = 0;
  end;

end;

function TFrameBuyJournal.GetCategoryVisible(Name: string): TCustomBoolean;
var
  CategoryIndex: Integer;
begin
  Result := cbUnknown;

  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin
    Result := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);
  end;
end;

procedure TFrameBuyJournal.SetCategoryVisible(Name: string; Value: TCustomBoolean);
var
  CategoryIndex: Integer;
  CurrentValue: TCustomBoolean;
begin
  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin

    if Value = cbUnknown then
    begin
      CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Value));
    end else

    begin
      CurrentValue := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);

      if CurrentValue = cbUnknown then
      begin
        CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Value));
      end;
    end;

  end;
end;

procedure TFrameBuyJournal.ButtonMaterialClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonMaterial.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  MenuMaterial.Popup(X, Y);
end;

procedure TFrameBuyJournal.ActionMaterialExecute(Sender: TObject);
var
  Action: TAction;
begin
  Action := TAction(Sender);

  ButtonMaterial.Caption := Action.Caption;

  ButtonMaterial.Tag := Action.Tag;

  Setup;

  ActionReload.Execute;

end;

procedure TFrameBuyJournal.ActionNoteCancelExecute(Sender: TObject);
begin
  NoteID := 0;

  NoteGet;
end;

procedure TFrameBuyJournal.ActionNoteOKExecute(Sender: TObject);
begin
  NoteSet;
end;

procedure TFrameBuyJournal.ActionNoteUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := EditorNote.ModifiedAfterEnter;
end;

procedure TFrameBuyJournal.SplitterBeforeClose(Sender: TObject; var AllowClose: Boolean);
begin
  GroupBoxNote.Margins.Top := 0;

  ButtonNote.Down := False;
end;

procedure TFrameBuyJournal.SplitterBeforeOpen(Sender: TObject; var NewSize: Integer; var AllowOpen: Boolean);
begin
  GroupBoxNote.Margins.Top := 4;

  ButtonNote.Down := True;

  NoteGet;    
end;

procedure TFrameBuyJournal.ButtonNoteCancelClick(Sender: TObject);
begin
  //
end;

procedure TFrameBuyJournal.ButtonNoteClick(Sender: TObject);
begin
  if ButtonNote.Down then
  begin
    Splitter.OpenSplitter;
  end else
  begin
    Splitter.CloseSplitter;
  end;
end;

procedure TFrameBuyJournal.ButtonNoteOkClick(Sender: TObject);
begin
  if EditorNote.ModifiedAfterEnter then
  begin
    NoteID := NoteID;
  end;
end;


end.


