inherited DialogBuyRangePopup: TDialogBuyRangePopup
  Caption = ''
  ClientHeight = 396
  ClientWidth = 196
  ShowHint = True
  ExplicitWidth = 196
  ExplicitHeight = 396
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 4
    Height = 396
    Width = 196
    object Layout: TdxLayoutControl
      Left = 2
      Top = 2
      Width = 192
      Height = 392
      Align = alClient
      ParentBackground = True
      TabOrder = 0
      TabStop = False
      object RangeEndEditor: TcxCalendar
        AlignWithMargins = True
        Left = 21
        Top = 198
        Width = 150
        Height = 126
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        CalendarButtons = []
      end
      object RangeBeginEditor: TcxCalendar
        AlignWithMargins = True
        Left = 21
        Top = 54
        Width = 150
        Height = 126
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        CalendarButtons = []
      end
      object BarDock: TdxBarDockControl
        Left = 10
        Top = 10
        Width = 100
        Height = 38
        Align = dalTop
        BarManager = BarManager
      end
      object BarDockControlBottom: TdxBarDockControl
        Left = 102
        Top = 342
        Width = 80
        Height = 38
        Align = dalTop
        BarManager = BarManager
      end
      object LayoutGroup: TdxLayoutGroup
        AlignHorz = ahClient
        AlignVert = avClient
        ButtonOptions.Buttons = <>
        Hidden = True
        ShowBorder = False
        object LayoutItemBar: TdxLayoutItem
          AlignVert = avTop
          Control = BarDock
          ControlOptions.AutoColor = True
          ControlOptions.ShowBorder = False
        end
        object LayoutItemRangeBegin: TdxLayoutItem
          AlignHorz = ahCenter
          Control = RangeBeginEditor
          ControlOptions.ShowBorder = False
        end
        object LayoutSeparatorItem1: TdxLayoutSeparatorItem
          CaptionOptions.Text = 'Separator'
          SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
          SizeOptions.SizableHorz = False
          SizeOptions.SizableVert = False
        end
        object LayoutItemRangeEnd: TdxLayoutItem
          AlignHorz = ahCenter
          Control = RangeEndEditor
          ControlOptions.ShowBorder = False
        end
        object LayoutSeparatorItem: TdxLayoutSeparatorItem
          CaptionOptions.Text = 'Separator'
          SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
          SizeOptions.SizableHorz = False
          SizeOptions.SizableVert = False
        end
        object LayoutItemBarBottom: TdxLayoutItem
          AlignHorz = ahRight
          AlignVert = avTop
          Control = BarDockControlBottom
          ControlOptions.AutoColor = True
          ControlOptions.ShowBorder = False
        end
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.ImageCalendar
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 344
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      BorderStyle = bbsNone
      Caption = 'BarTop'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 191
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonDay'
        end
        item
          Visible = True
          ItemName = 'ButtonWeek'
        end
        item
          Visible = True
          ItemName = 'ButtonMonth'
        end
        item
          Visible = True
          ItemName = 'ButtonYear'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarBottom: TdxBar
      BorderStyle = bbsNone
      Caption = 'BarBottom'
      CaptionButtons = <>
      DockControl = BarDockControlBottom
      DockedDockControl = BarDockControlBottom
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 378
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 76
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOk'
        end
        item
          Visible = True
          ItemName = 'ButtonCancel'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ButtonDay: TdxBarButton
      Caption = 'Day'
      Category = 0
      Hint = #1044#1077#1085#1100
      Visible = ivAlways
      ImageIndex = 0
      OnClick = ButtonDayClick
    end
    object ButtonWeek: TdxBarButton
      Caption = 'Week'
      Category = 0
      Hint = #1053#1077#1076#1077#1083#1103
      Visible = ivAlways
      ImageIndex = 1
      OnClick = ButtonWeekClick
    end
    object ButtonMonth: TdxBarButton
      Caption = 'Month'
      Category = 0
      Hint = #1052#1077#1089#1103#1094
      Visible = ivAlways
      ImageIndex = 2
      OnClick = ButtonMonthClick
    end
    object ButtonYear: TdxBarButton
      Caption = 'Year'
      Category = 0
      Hint = #1043#1086#1076
      Visible = ivAlways
      ImageIndex = 3
      OnClick = ButtonYearClick
    end
    object ButtonOk: TdxBarButton
      Align = iaRight
      Caption = 'OK'
      Category = 0
      Visible = ivAlways
      ImageIndex = 4
      LargeImageIndex = 4
      OnClick = ButtonOkClick
    end
    object ButtonCancel: TdxBarButton
      Align = iaRight
      Caption = #1054#1090#1084#1077#1085#1072
      Category = 0
      Hint = #1054#1090#1084#1077#1085#1072
      Visible = ivAlways
      LargeImageIndex = 5
      OnClick = ButtonCancelClick
    end
  end
end
