unit frmBuyRange;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  dxLayoutcxEditAdapters, dxLayoutLookAndFeels, dxLayoutControl, Menus,
  ActnList, StdCtrls, cxButtons, DateUtils, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters;

type
  TDialogByRange = class(TForm)
    EditorRangeEnd: TcxDateEdit;
    EditorRangeBegin: TcxDateEdit;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    dxLayoutControl1Group1: TdxLayoutGroup;
    dxLayoutControl1Item1: TdxLayoutItem;
    dxLayoutControl1Item2: TdxLayoutItem;
    dxLayoutControl1Group2: TdxLayoutGroup;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    dxLayoutControl1Group3: TdxLayoutGroup;
    dxLayoutControl1Item3: TdxLayoutItem;
    dxLayoutControl1Item4: TdxLayoutItem;
    ButtonOk: TcxButton;
    ButtonCancel: TcxButton;
    Actions: TActionList;
    ActionOK: TAction;
    ActionCancel: TAction;
    procedure ActionOKExecute(Sender: TObject);
    procedure ActionCancelExecute(Sender: TObject);
  private
    function GetRangeBegin: TDate;
    function GetRangeEnd: TDate;
    procedure SetRangeBegin(const Value: TDate);
    procedure SetRangeEnd(const Value: TDate);
  public
    function Execute: Boolean;
    property RangeBegin: TDate read GetRangeBegin write SetRangeBegin;
    property RangeEnd: TDate read GetRangeEnd write SetRangeEnd;
  end;

var
  DialogByRange: TDialogByRange;

implementation

uses dmBuy;

{$R *.dfm}


function TDialogByRange.GetRangeBegin: TDate;
begin
  Result := DateOf(EditorRangeBegin.Date);
end;

function TDialogByRange.GetRangeEnd: TDate;
begin
  Result := DateOf(EditorRangeEnd.Date);
end;

procedure TDialogByRange.SetRangeBegin(const Value: TDate);
begin
  EditorRangeBegin.Date := DateOf(Value);
end;

procedure TDialogByRange.SetRangeEnd(const Value: TDate);
begin
  EditorRangeEnd.Date := DateOf(Value);
end;

procedure TDialogByRange.ActionOKExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

function TDialogByRange.Execute: Boolean;
begin
  Result := ShowModal = mrOk;
end;

procedure TDialogByRange.ActionCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end;


end.
