unit frmBuyInvoiceRecycling;

interface                                                                        

uses                                       

  Classes, ActnList, Controls, DB,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles,                    
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid,
  dxLayoutControl, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxGroupBox,
  dxSkinsCore, dxLayoutcxEditAdapters, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxBar, dxLayoutLookAndFeels,
  frmBuyInvoice, cxHeader, Menus, StdCtrls, cxButtons, DBClient, cxSplitter,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxPScxGrid6Lnk, cxCheckBox, cxGridDBTableView, cxCalendar, cxPC, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters;

type

{ TFrameBuyInvoiceRecycling }

  TFrameBuyInvoiceRecycling = class(TFrameBuyInvoice)
  protected
    class function AgentBit: TAgentBit; override;
    function InternalActivate: Boolean; override;
  end;

var
  FrameBuyInvoiceRecycling: TFrameBuyInvoiceRecycling;

implementation

{$R *.dfm}

uses

  uBuy;

{ TFrameBuyInvoiceRecycling }

class function TFrameBuyInvoiceRecycling.AgentBit: TAgentBit;
begin
  Result :=  abRecycling;
end;

function TFrameBuyInvoiceRecycling.InternalActivate: Boolean;
begin
  Result := False;

  if inherited InternalActivate then
  begin
    GridInvoiceItemViewWEIGHT.Visible := False;

    GridInvoiceItemViewWEIGHTREAL.Caption := '���';

    Result := True;
  end;
end;

end.
