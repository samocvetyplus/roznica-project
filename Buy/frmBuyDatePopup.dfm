inherited DialogBuyDatePopup: TDialogBuyDatePopup
  Caption = ''
  ClientHeight = 249
  ClientWidth = 276
  ShowHint = True
  ExplicitWidth = 276
  ExplicitHeight = 249
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 4
    ExplicitWidth = 196
    ExplicitHeight = 212
    Height = 249
    Width = 276
    object Layout: TdxLayoutControl
      Left = 2
      Top = 2
      Width = 272
      Height = 245
      Align = alClient
      ParentBackground = True
      TabOrder = 0
      TabStop = False
      ExplicitWidth = 192
      ExplicitHeight = 208
      object DateEditor: TcxCalendar
        AlignWithMargins = True
        Left = 61
        Top = 10
        Width = 150
        Height = 126
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 0
        Margins.Bottom = 0
        CalendarButtons = []
      end
      object BarDockControlBottom: TdxBarDockControl
        Left = 182
        Top = 154
        Width = 80
        Height = 38
        Align = dalTop
        BarManager = BarManager
      end
      object LayoutGroup: TdxLayoutGroup
        AlignHorz = ahClient
        AlignVert = avClient
        ButtonOptions.Buttons = <>
        Hidden = True
        ShowBorder = False
        object LayoutItemDate: TdxLayoutItem
          AlignHorz = ahCenter
          Control = DateEditor
          ControlOptions.ShowBorder = False
        end
        object LayoutSeparatorItem: TdxLayoutSeparatorItem
          CaptionOptions.Text = 'Separator'
          SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
          SizeOptions.SizableHorz = False
          SizeOptions.SizableVert = False
        end
        object LayoutItemBarBottom: TdxLayoutItem
          AlignHorz = ahRight
          AlignVert = avTop
          Control = BarDockControlBottom
          ControlOptions.AutoColor = True
          ControlOptions.ShowBorder = False
        end
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.ImageCalendar
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 160
    DockControlHeights = (
      0
      0
      0
      0)
    object BarBottom: TdxBar
      BorderStyle = bbsNone
      Caption = 'BarBottom'
      CaptionButtons = <>
      DockControl = BarDockControlBottom
      DockedDockControl = BarDockControlBottom
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 378
      FloatTop = 8
      FloatClientWidth = 51
      FloatClientHeight = 76
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOk'
        end
        item
          Visible = True
          ItemName = 'ButtonCancel'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ButtonOk: TdxBarButton
      Align = iaRight
      Caption = 'OK'
      Category = 0
      Visible = ivAlways
      ImageIndex = 4
      LargeImageIndex = 4
      OnClick = ButtonOkClick
    end
    object ButtonCancel: TdxBarButton
      Align = iaRight
      Caption = #1054#1090#1084#1077#1085#1072
      Category = 0
      Hint = #1054#1090#1084#1077#1085#1072
      Visible = ivAlways
      LargeImageIndex = 5
      OnClick = ButtonCancelClick
    end
  end
end
