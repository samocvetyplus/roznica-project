unit frmBuyInvoiceReport;

interface

uses

  Forms, SysUtils, Classes, DB, DBClient, Variants, DateUtils, StrUtils,
  frxClass, frxDBSet, Math;


type

{ TDialogBuyInvoiceReport }

  TDialogBuyInvoiceReport = class(TForm)
    frxReport: TfrxReport;
    frxHeader: TfrxDBDataset;
    frxBody: TfrxDBDataset;
    Company: TClientDataSet;
    Header: TClientDataSet;
    Body: TClientDataSet;
    frxFooter: TfrxDBDataset;
    Footer: TClientDataSet;
    frxSenderCompany: TfrxDBDataset;
    frxReceiverCompany: TfrxDBDataset;
    AgentCompany: TClientDataSet;
    frxReceiverOffice: TfrxDBDataset;
    AgentOffice: TClientDataSet;
    Office: TClientDataSet;
    frxSenderOffice: TfrxDBDataset;
    CompanyName: TStringField;
    CompanyBoss: TStringField;
    OfficeName: TStringField;
    AgentOfficeName: TStringField;
    HeaderN: TIntegerField;
    HeaderDate: TDateField;
    BodyMaterial: TStringField;
    BodyWeightReal: TCurrencyField;
    BodyWeight: TCurrencyField;
    BodyPrice: TCurrencyField;
    BodyCost: TCurrencyField;
    CompanyBooker: TStringField;
    Footerstub: TIntegerField;
    HeaderWeightReal: TCurrencyField;
    HeaderCost: TCurrencyField;
    BodyWeight1000: TCurrencyField;
    BodyProbePercent: TCurrencyField;
    CompanyField2: TStringField;
    CompanyField3: TStringField;
    CompanyField4: TStringField;
    CompanyField5: TStringField;
    CompanyField6: TStringField;
    CompanyStringField: TStringField;
    CompanyField7: TStringField;
    CompanyField: TStringField;
    AgentCompanyStringField: TStringField;
    AgentCompanyStringField2: TStringField;
    AgentCompanyStringField3: TStringField;
    AgentCompanyStringField4: TStringField;
    AgentCompanyStringField5: TStringField;
    AgentCompanyStringField6: TStringField;
    AgentCompanyStringField7: TStringField;
    AgentCompanyStringField8: TStringField;
    AgentCompanyStringField9: TStringField;
    AgentCompanyStringField10: TStringField;
    AgentCompanyStringField11: TStringField;
    OfficeFieldAbbreviation: TStringField;
    AgentCompanyField: TStringField;
    AgentCompanyField2: TDateField;
    BodyProbe: TStringField;
    frxImage: TfrxDBDataset;
    Image: TClientDataSet;
    ImageImage: TGraphicField;
    CompanyResponsiblePerson: TStringField;
    BodyDate: TDateField;
    HeaderWeight: TCurrencyField;
    HeaderWeight1000: TCurrencyField;
    HeaderMaterial: TStringField;
    BodyWeightActual: TCurrencyField;
    BodyWeightRealActual: TCurrencyField;
    BodyWeight1000Actual: TCurrencyField;
    Material: TClientDataSet;
    BodyGroup: TIntegerField;
    Member: TClientDataSet;
    frxMember: TfrxDBDataset;
    ChairMan: TClientDataSet;
    frxChairMan: TfrxDBDataset;
    mol: TClientDataSet;
    frxmol: TfrxDBDataset;
    HeaderStoreDate: TDateField;
    mol0: TClientDataSet;
    frxmol0: TfrxDBDataset;
    CompanyField8: TStringField;
    CompanyField9: TIntegerField;
    AgentCompanyField3: TStringField;
    AgentCompanyField4: TIntegerField;
    function OnfrxReportUserFunction(const MethodName: string; var Params: Variant): Variant;
    procedure BodyCalcFields(DataSet: TDataSet);
  private
    FID: Integer;
    FInvoiceID: Integer;
    FDepartmentID: Integer;
    FMaterialID: Integer;
    FClassCode: Integer;
    FRangeBegin: TDateTime;
    FRangeEnd: TDateTime;
  public
    procedure Execute;
    property ID: Integer read FID write FID;
    property ClassCode: Integer read FClassCode write FClassCode;
    property InvoiceID: Integer read FInvoiceID write FInvoiceID;
    property MaterialID: Integer read FMaterialID write FMaterialID;
    property DepartmentID: Integer read FDepartmentID write FDepartmentID;
    property RangeBegin: TDateTime read FRangeBegin write FRangeBegin;
    property RangeEnd: TDateTime read FRangeEnd write FRangeEnd;
  end;

function frxReportUserFunction(const MethodName: string;  var Params: Variant): Variant;

implementation

{$R *.dfm}

uses

  dmBuy, uDialog, uPrinter,
  UtilLib;

{ TDialogBuyInvoiceReport }

procedure TDialogBuyInvoiceReport.BodyCalcFields(DataSet: TDataSet);
var
  Group: Integer;
  MaterialID: Integer;
  MaterialName: string;

procedure CalcField(BaseField: TField);
var
  Field: TCurrencyField;
  FieldName: string;
begin
  FieldName := BaseField.FieldName + '.' + MaterialName;

  Field := TCurrencyField(Body.FieldByName(FieldName));

  Field.Value := 0;

  if MaterialName = BodyMaterial.AsString then
  begin
    if not BaseField.IsNull then
    begin
      Field.Value := BaseField.Value;
    end;
  end;
end;

begin
  Material.First;

  while not Material.Eof do
  begin
    MaterialID := Material.FieldByName('ID').AsInteger;

    MaterialName := Material.FieldByName('NAME').AsString;

    CalcField(BodyWeight);

    CalcField(BodyWeightReal);

    CalcField(BodyWeight1000);

    CalcField(BodyWeightActual);

    CalcField(BodyWeightRealActual);

    CalcField(BodyWeight1000Actual);

    Material.Next;
  end;

  BodyGroup.AsInteger := ((Body.RecNo -1) div 22) + 1;

end;

procedure TDialogBuyInvoiceReport.Execute;
var
  Template: string;
  Stream: TMemoryStream;
  DataSet: TClientDataSet;
  N: Integer;
  StoreDate: TDateTime;
  InvoiceDate: TDateTime;
  Weight: Currency;
  WeightReal: Currency;
  Weight1000: Currency;
  MaterialName: string;
  Cost: Currency;
  CompanyID: Integer;
  OfficeID: Integer;
  AgentID: Integer;
  SQL: string;
  i: Integer;
  frxPage: TfrxPage;
  frxReportPage: TfrxReportPage;
  frxComponent: TfrxComponent;
  frxPictureView: TfrxPictureView;
  Margins: TMargins;

procedure ExploreCompany;
var
  sValue: string;
  iValue: Integer;
  Attributes: string;
begin
  Attributes := 'name, boss, buh, ofio, okpo, inn, bill, bank, bik, kbill, address, city, postindex, paddress';

  DataSet.Data := DataBuy.Table('select ' + Attributes + ' from d_comp where d_compid = :id', [CompanyID]);

  Company.Append;

  Company['������������'] := DataSet.FieldByName('NAME').AsString;
  Company['������������'] := DataSet.FieldByName('BOSS').AsString;
  Company['������������� ����'] := DataSet.FieldByName('OFIO').AsString;
  Company['���������'] := DataSet.FieldByName('BUH').AsString;
  Company['�����'] := DataSet.FieldByName('ADDRESS').AsString;
  Company['�����'] := DataSet.FieldByName('CITY').AsString;
  Company['������'] := DataSet.FieldByName('POSTINDEX').AsString;
  Company['�������� �����'] := DataSet.FieldByName('PADDRESS').AsString;
  Company['���'] := DataSet.FieldByName('INN').AsString;
  Company['����'] := DataSet.FieldByName('OKPO').AsString;
  Company['����'] := DataSet.FieldByName('BANK').AsString;
  Company['���'] := DataSet.FieldByName('BIK').AsString;
  Company['��������� ����'] := DataSet.FieldByName('BILL').AsString;
  Company['����������������� ����'] := DataSet.FieldByName('KBILL').AsString;

  sValue := DataSet.FieldByName('OKPO').AsString;

  if sValue <> '' then
  begin
    if TryStrToInt(sValue, iValue) then
    begin
      Company['����'] := iValue;
    end;
  end;

  Company.Post;
end;

procedure ExploreAgentCompany;
var
  sValue: string;
  iValue: Integer;
  Attributes: string;
  ContractClassID: Integer;
  SQL: string;
begin
  Attributes := 'name, boss, buh, okpo, inn, bill, bank, bik, kbill, address, city, postindex, paddress';

  DataSet.Data := DataBuy.Table('select ' + Attributes + ' from d_comp where d_compid = :id', [AgentID]);

  AgentCompany.Append;

  AgentCompany['������������'] := DataSet.FieldByName('NAME').AsString;
  AgentCompany['������������'] := DataSet.FieldByName('BOSS').AsString;
  AgentCompany['���������'] := DataSet.FieldByName('BUH').AsString;
  AgentCompany['�����'] := DataSet.FieldByName('ADDRESS').AsString;
  AgentCompany['�����'] := DataSet.FieldByName('CITY').AsString;
  AgentCompany['������'] := DataSet.FieldByName('POSTINDEX').AsString;
  AgentCompany['�������� �����'] := DataSet.FieldByName('PADDRESS').AsString;
  AgentCompany['���'] := DataSet.FieldByName('INN').AsString;
  AgentCompany['����'] := DataSet.FieldByName('OKPO').AsString;
  AgentCompany['����'] := DataSet.FieldByName('BANK').AsString;
  AgentCompany['���'] := DataSet.FieldByName('BIK').AsString;
  AgentCompany['��������� ����'] := DataSet.FieldByName('BILL').AsString;
  AgentCompany['����������������� ����'] := DataSet.FieldByName('KBILL').AsString;

  sValue := DataSet.FieldByName('OKPO').AsString;

  if sValue <> '' then
  begin
    if TryStrToInt(sValue, iValue) then
    begin
      AgentCompany['����'] := iValue;
    end;
  end;

  //Attributes := 'number, onlydate(contract$date) contract$date';

  SQL :=
  ' select first 1 number, onlydate(contract$date) contract$date' +
  ' from contract ' +
  ' where company$id = :company$id and agent$id = :agent$id and is$delete = 0 and contract$type = :contract$class$id and ' +
  ' ((is$unlimited = 1) or (onlydate(contract$end$date) >= :invoice$date))  ' +
  ' order by contract$date ';

  //DataSet.Data := DataBuy.Table('select first 1 ' + Attributes + ' from  contract where agent$id = :agent$id and company$id = :company$id and contract$type = :contract$class$id order by contract$date desc', [AgentID, CompanyID, ContractClassID]);

  ContractClassID := 0;

  if ClassCode = 5 then
  begin
    ContractClassID := 5;
  end else
  if ClassCode = 7 then
  begin
    ContractClassID := 4;
  end;

  DataSet.Data := DataBuy.Table(SQL, [CompanyID, AgentID, ContractClassID, DateOf(InvoiceDate)]);

  AgentCompany['�������'] := DataSet.FieldByName('NUMBER').AsString;

  AgentCompany['���� ��������'] := DataSet.FieldByName('CONTRACT$DATE').AsDateTime;

  AgentCompany.Post;
end;

procedure ExploreOffice;
var
  i: Integer;
  Name: string;
  Abbreviation: string;
begin
  Name := DataBuy.Value('select sname name from d_dep where d_depid = :office$id', [OfficeID]);

  DataSet.Data := DataBuy.Table('select upper(sname) name from d_dep where d_depid > 0 and d_depid <> :office$id', [OfficeID]);

  DataSet.IndexFieldNames := 'name';

  Abbreviation := '';

  for i := 1 to Length(Name) do
  begin
    Abbreviation := Abbreviation + AnsiUpperCase(Name[i]);

    DataSet.SetRange([Abbreviation], [Abbreviation + #255]);

    DataSet.First;

    if DataSet.RecordCount = 0 then
    begin
      break;
    end;
  end;

  DataSet.IndexFieldNames := '';

  Office.Append;

  OfficeName.AsString := Name;

  OfficeFieldAbbreviation.AsString := Abbreviation;

  Office.Post;
end;

procedure ExploreAgentOffice;
var
  Name: string;
begin
  Name:= DataBuy.Value('select name from d_dep where d_depid = :id', [AgentID]);

  AgentOffice.Append;

  AgentOfficeName.AsString := Name;

  AgentOffice.Post;
end;

procedure ExploreImage;
begin
  Image.Data := DataBuy.Table('select image from buy$image where invoice$id = :invoice$id', [0]);
end;

procedure CreateBodyDataSet;
var
  MaterialID: Integer;
  MaterialName: string;

procedure CreateField(BaseField: TField);
var
  Field: TCurrencyField;
  FieldName: string;
begin
  FieldName := BaseField.FieldName + '.' + MaterialName;

  Field := TCurrencyField.Create(Body);

  Field.FieldName := FieldName;

  Field.Calculated := True;

  Field.DataSet := Body;
end;

begin
  Material.Data := DataBuy.Table('select id, name from buy$material');

  Material.First;

  while not Material.Eof do
  begin
    MaterialID := Material.FieldByName('ID').AsInteger;

    MaterialName := Material.FieldByName('NAME').AsString;

    CreateField(BodyWeight);

    CreateField(BodyWeightReal);

    CreateField(BodyWeight1000);

    CreateField(BodyWeightActual);

    CreateField(BodyWeightRealActual);

    CreateField(BodyWeight1000Actual);

    Material.Next;
  end;

  Body.CreateDataSet;
end;

begin
  DataSet := TClientDataSet.Create(Self);

  Stream := TMemoryStream.Create;

  frxReport.AddFunction('function �����(v: Currency): string');

  frxReport.AddFunction('function �������(v: Currency): string');

  frxReport.AddFunction('function �����(v: Currency): string');

  frxReport.AddFunction('function ����������(v: Currency): string');

  frxReport.AddFunction('function ����(v: TDateTime): string');

  frxReport.AddFunction('function �����(v: TDateTime): string');

  frxReport.AddFunction('function ���(v: TDateTime): string');

  frxReport.AddFunction('function ����������(v: Currency): string');

  try

    if True then
    begin
      Company.CreateDataSet;

      AgentCompany.CreateDataSet;

      Office.CreateDataSet;

      AgentOffice.CreateDataSet;

      Header.CreateDataSet;

      CreateBodyDataSet;

      DataSet.Data := DataBuy.Table('select company$id, department$id, agent$id, n, invoice$date, material$name, weight, weight$real, weight$1000, cost from buy$invoice$s(:id)', [InvoiceID]);

      N := DataSet.FieldByName('N').AsInteger;

      InvoiceDate := DateOf(DataSet.FieldByName('INVOICE$DATE').AsDateTime);

      StoreDate := InvoiceDate;

      MaterialName := DataSet.FieldByName('MATERIAL$NAME').AsString;

      Weight := DataSet.FieldByName('WEIGHT').AsCurrency;

      WeightReal := DataSet.FieldByName('WEIGHT$REAL').AsCurrency;

      Weight1000 := DataSet.FieldByName('WEIGHT$1000').AsCurrency;

      Cost := DataSet.FieldByName('COST').AsCurrency;

      CompanyID := DataSet.FieldByName('COMPANY$ID').AsInteger;

      OfficeID := DataSet.FieldByName('DEPARTMENT$ID').AsInteger;

      AgentID := DataSet.FieldByName('AGENT$ID').AsInteger;

      Mol.Data := DataBuy.Table('select fio name, post from d_mol where d_compid = :company$id and d_depid = :department$id and (bit(member, 0) = 1) order by post, fio', [CompanyID, DepartmentID]);

      Member.Data:= DataBuy.Table('select fio name, post from d_mol where d_compid = :company$id and d_depid = :department$id and (bit(member, 2) = 1) order by post, fio', [CompanyID, 1]);

      if DataBuy.IsCenterDepartment then
      begin
        ChairMan.Data:= DataBuy.Table('select first 1 fio name, post from d_mol where d_compid = :company$id and d_depid = :department$id and (bit(member, 0) = 0) and (bit(member, 1) = 1) order by fio', [CompanyID, 1]);
      end else
      begin
        ChairMan.Data:= DataBuy.Table('select first 1 fio name, post from d_mol where d_compid = :company$id and d_depid = :department$id and (bit(member, 0) = 1) and (bit(member, 1) = 1) order by fio', [CompanyID, 1]);
      end;

      if ClassCode = 0 then
      begin
        InvoiceDate := DateOf(DataBuy.Value('select collation$date from buy$collation where id = :id', [InvoiceID]))
      end;

      if ClassCode in [2] then
      begin
        ExploreCompany;

        ExploreOffice;
      end else

      if ClassCode = 0 then
      begin
        ExploreCompany;

        ExploreOffice;
      end else

      if ClassCode in [3, 4] then
      begin
        ExploreCompany;

        ExploreOffice;

        ExploreAgentCompany;

        ExploreAgentOffice;
      end else

      if ClassCode in [5, 6, 7] then
      begin
        ExploreCompany;

        ExploreOffice;

        ExploreAgentCompany;
      end;

      Header.Append;

      HeaderN.AsInteger :=  N;

      HeaderDate.AsDateTime := InvoiceDate;

      HeaderStoreDate.AsDateTime := StoreDate;

      HeaderMaterial.AsString := MaterialName;

      HeaderWeight.AsCurrency := Weight;

      HeaderWeightReal.AsCurrency := WeightReal;

      HeaderWeight1000.AsCurrency := Weight1000;

      HeaderCost.AsCurrency := Cost;

      Header.Post;

      if ClassCode = 0 then
      begin
        SQL := 'select i.* from buy$collation$item$a(:invoice$id) i where ((i.weight$real <> 0) or (i.weight$real$actual <> 0))';

        DataSet.Data := DataBuy.Table(SQL, [InvoiceID]);
      end else

      if ClassCode = 2 then
      begin
        SQL := 'select i.* from buy$invoice$item$g2(:invoice$id, :department$id, :material$id, :class$code, :range$begin, :range$end) i ';

        DataSet.Data := DataBuy.Table(SQL, [InvoiceID, DepartmentID, MaterialID, ClassCode, RangeBegin, RangeEnd]);
      end else

      if ClassCode = 5 then
      begin
        SQL := 'select i.* from buy$invoice$item$g5(:invoice$id, :department$id, :material$id, :class$code, :range$begin, :range$end) i ';

        DataSet.Data := DataBuy.Table(SQL, [InvoiceID, DepartmentID, MaterialID, ClassCode, RangeBegin, RangeEnd]);
      end else

      if ClassCode = 7 then
      begin
        SQL := 'select i.* from buy$invoice$item$g7(:invoice$id, :department$id, :material$id, :class$code, :range$begin, :range$end) i ';

        DataSet.Data := DataBuy.Table(SQL, [InvoiceID, DepartmentID, MaterialID, ClassCode, RangeBegin, RangeEnd]);
      end else


      begin
        SQL := 'select i.* from buy$invoice$item$g(:invoice$id, :department$id, :material$id, :class$code, :range$begin, :range$end) i ';

        DataSet.Data := DataBuy.Table(SQL, [InvoiceID, DepartmentID, MaterialID, ClassCode, RangeBegin, RangeEnd]);
      end;

      DataSet.First;

      while not DataSet.Eof do
      begin
        Body.Append;

        if DataSet.FindField('INVOICE$DATE') <> nil then
        BodyDate.AsDateTime := DataSet.FieldByName('INVOICE$DATE').AsDateTime;

        if DataSet.FindField('MATERIAL$NAME') <> nil then
        BodyMaterial.AsString := DataSet.FieldByName('MATERIAL$NAME').AsString;

        if DataSet.FindField('PROBE$NAME') <> nil then
        BodyProbe.AsString := DataSet.FieldByName('PROBE$NAME').AsString;

        if DataSet.FindField('PROBE$PERCENT') <> nil then
        BodyProbePercent.AsCurrency := DataSet.FieldByName('PROBE$PERCENT').AsCurrency;

        if DataSet.FindField('WEIGHT') <> nil then
        BodyWeight.AsCurrency := DataSet.FieldByName('WEIGHT').AsCurrency;

        if DataSet.FindField('WEIGHT$REAL') <> nil then
        BodyWeightReal.AsCurrency := DataSet.FieldByName('WEIGHT$REAL').AsCurrency;

        if DataSet.FindField('WEIGHT$1000') <> nil then
        BodyWeight1000.AsCurrency := DataSet.FieldByName('WEIGHT$1000').AsCurrency;

        if DataSet.FindField('WEIGHT$ACTUAL') <> nil then
        BodyWeightActual.AsCurrency := DataSet.FieldByName('WEIGHT$ACTUAL').AsCurrency;

        if DataSet.FindField('WEIGHT$REAL$ACTUAL') <> nil then
        BodyWeightRealActual.AsCurrency := DataSet.FieldByName('WEIGHT$REAL$ACTUAL').AsCurrency;

        if DataSet.FindField('WEIGHT$1000$ACTUAL') <> nil then
        BodyWeight1000Actual.AsCurrency := DataSet.FieldByName('WEIGHT$1000$ACTUAL').AsCurrency;

        if DataSet.FindField('PRICE') <> nil then
        BodyPrice.AsCurrency := DataSet.FieldByName('PRICE').AsCurrency;

        if DataSet.FindField('COST') <> nil then
        BodyCost.AsCurrency := DataSet.FieldByName('COST').AsCurrency;

        Body.Post;

        DataSet.Next;
      end;
    end;

    Template := DataBuy.Value('select first 1 template from buy$report where id = :id and ((agent$id=:agent$id) or (agent$id=0)) and ((company$id=:company$id) or (company$id=0)) order by agent$id, company$id', [id, agentid, companyid]);

    if Template <> '' then
    begin
      Stream.Write(PChar(Template)^, Length(Template));

      Stream.Position := 0;

      frxReport.LoadFromStream(Stream);

      //frxReport.LoadFromFile('f:\10.fr3');

      for i := 0 to frxReport.PagesCount - 1 do
      begin


      frxPage := frxReport.Pages[i];

      if frxPage is TfrxReportPage then
      begin
        frxReportPage := TfrxReportPage(frxPage);

        frxComponent := frxReportPage.FindObject('Image');

        if frxComponent <> nil then
        begin
          if frxComponent is TfrxPictureView then
          begin
            if not Image.Active then
            begin
              ExploreImage;

              GetPrinterMargins(Margins);
            end;

            frxPictureView := TfrxPictureView(frxComponent);

            frxPictureView.Top := Margins.Top * fr01cm;

            frxPictureView.Left := Margins.Left * fr01cm;

            frxPictureView.Height := (297 - (Margins.Top + Margins.Bottom)) * fr01cm;

            frxPictureView.Width := (210 - (Margins.Left + Margins.Right)) * fr01cm;
          end;
        end;
      end;

      end;

      frxReport.PrepareReport;
      //frxReport.DesignReport();

      frxReport.ShowPreparedReport;
    end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message)
    end;

  end;

  DataSet.Free;

  Stream.Free;
end;


function TDialogBuyInvoiceReport.OnfrxReportUserFunction(const MethodName: string;  var Params: Variant): Variant;
begin
  Result := frxReportUserFunction(MethodName, Params);
end;

function frxReportUserFunction(const MethodName: string;  var Params: Variant): Variant;
var
  Value: Variant;
  sValue: string;
  cValue: Currency;

function MonthName(Value: TDateTime): string;
begin
  Result := '';

  case MonthOf(Value) of
     1: Result := '������';
     2: Result := '�������';
     3: Result := '�����';
     4: Result := '������';
     5: Result := '���';
     6: Result := '����';
     7: Result := '����';
     8: Result := '�������';
     9: Result := '��������';
    10: Result := '�������';
    11: Result := '������';
    12: Result := '�������';
  end;
end;

begin
  Result := Unassigned;

  if MethodName = '����' then
  begin
    Result := '';

    Value := Params[0];

    if Value <> 0 then
    begin
      Result := FormatDateTime('dd', Value);
    end;
  end else

  if MethodName = '�����' then
  begin
    Result := '';

    Value := Params[0];

    if Value <> 0 then
    begin
      Result := MonthName(Value);
    end;
  end else

  if MethodName = '���' then
  begin
    Result := '';

    Value := Params[0];

    if Value <> 0 then
    begin
      Result := FormatDateTime('yyyy', Value);
    end;
  end else

  if MethodName = '�����' then
  begin
    Value := Params[0];

    if Value = 0 then
    begin
      Result := '����';
    end else

    begin
      sValue := IntToCapitalsStr(Trunc(Value));

      sValue := StringReplace(sValue, '  ', ' ', [rfReplaceAll]);

      Result := sValue;
    end;
  end else

  if MethodName = '�������' then
  begin
    Value := Params[0];

    if Value = 0 then
    begin
      Result := '00'
    end else

    begin
      cValue := Value;

      Result := CurrToStr(Trunc(Frac(cValue) * 100));
    end;
  end else

  if MethodName = '�����' then
  begin
    Value := Params[0];

    if Value = 0 then
    begin
      Result := '����';
    end else

    begin
      sValue := IntToCapitalsStr(Trunc(Value));

      sValue := StringReplace(sValue, '  ', ' ', [rfReplaceAll]);

      Result := sValue;
    end;
  end else

  if MethodName = '����������' then
  begin
    Value := Params[0];

    if Value = 0 then
    begin
      Result := '0'
    end else

    begin
      cValue := Value;

      Result := CurrToStr(Trunc(Frac(cValue) * 1000));
    end;
  end else

  if MethodName = '����������' then
  begin
    Value := Params[0];

    if Value = 0 then
    begin
      Result := '����';
    end else

    begin
      sValue := IntToCapitalsStr(Trunc(Value));

      sValue := StringReplace(sValue, '  ', ' ', [rfReplaceAll]);

      Result := sValue;
    end;
  end
end;

end.
