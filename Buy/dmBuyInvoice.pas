unit dmBuyInvoice;

interface

uses

  Classes,
  FIBQuery, pFIBQuery, pFIBStoredProc;

type

{ TDataBuyInvoice }

  TDataBuyInvoice = class(TDataModule)
    InvoiceOpen: TpFIBStoredProc;
    InvoiceClose: TpFIBStoredProc;
    InvoiceInsert: TpFIBStoredProc;
    InvoiceUpdate: TpFIBStoredProc;
    InvoiceDelete: TpFIBStoredProc;
    InvoiceItemDelete: TpFIBStoredProc;
    InvoiceItemUpdate: TpFIBStoredProc;
    InvoiceItemInsert: TpFIBStoredProc;
    InvoiceNoteGet: TpFIBStoredProc;
    InvoiceNoteSet: TpFIBStoredProc;
  end;

var
  DataBuyInvoice: TDataBuyInvoice;

implementation

{$R *.dfm}

uses

  dmBuy;

end.
