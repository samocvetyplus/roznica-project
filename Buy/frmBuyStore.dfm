object FrameBuyStore: TFrameBuyStore
  Left = 0
  Top = 0
  Width = 1031
  Height = 745
  TabOrder = 0
  object GroupBoxStore: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 4
    Height = 745
    Width = 1031
    object GridStore: TcxGrid
      AlignWithMargins = True
      Left = 2
      Top = 104
      Width = 1027
      Height = 639
      Margins.Left = 0
      Margins.Top = 4
      Margins.Right = 0
      Margins.Bottom = 0
      Align = alClient
      BevelEdges = [beTop]
      BevelKind = bkFlat
      BorderStyle = cxcbsNone
      TabOrder = 0
      TabStop = False
      ExplicitLeft = 4
      ExplicitTop = 106
      object GridStoreView: TcxGridDBBandedTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = SourceStore
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.00'
            Kind = skSum
            FieldName = 'COST'
            DisplayText = #1057#1090#1086#1080#1084#1086#1089#1090#1100
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT'
            Column = GridStoreViewWEIGHT
            DisplayText = #1063#1080#1089#1090#1099#1081
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT$REAL'
            Column = GridStoreViewWEIGHTREAL
            DisplayText = #1043#1088#1103#1079#1085#1099#1081
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT$1000'
            Column = GridStoreViewWEIGHT1000
          end>
        DataController.Summary.SummaryGroups = <
          item
            Links = <
              item
              end
              item
              end
              item
                Column = GridStoreViewPROBE
              end>
            SummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'COST'
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
          end
          item
            Links = <
              item
                Column = GridStoreViewMATERIAL
              end>
            SummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'COST'
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
          end
          item
            Links = <
              item
              end>
            SummaryItems = <
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'COST'
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
          end
          item
            Links = <
              item
                Column = GridStoreViewDEPARTMENTNAME
              end>
            SummaryItems = <
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT'
                Column = GridStoreViewWEIGHT
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$REAL'
                Column = GridStoreViewWEIGHTREAL
              end
              item
                Format = '0.00'
                Kind = skSum
                Position = spFooter
                FieldName = 'COST'
              end
              item
                Format = '0.000'
                Kind = skSum
                Position = spFooter
                FieldName = 'WEIGHT$1000'
                Column = GridStoreViewWEIGHT1000
              end>
          end>
        DataController.OnGroupingChanged = GridStoreViewDataControllerGroupingChanged
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnSorting = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.ColumnVertSizing = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.CellSelect = False
        OptionsView.NoDataToDisplayInfoText = '...'
        OptionsView.ScrollBars = ssVertical
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.Indicator = True
        OptionsView.BandHeaderHeight = 32
        Styles.BandHeader = DataBuy.StyleBand
        Bands = <
          item
            Caption = #1052#1072#1090#1077#1088#1080#1072#1083
            Options.HoldOwnColumnsOnly = True
          end
          item
            Caption = #1042#1077#1089
            Options.HoldOwnColumnsOnly = True
            Width = 298
          end>
        OnCustomDrawBandHeader = GridStoreViewCustomDrawBandHeader
        object GridStoreViewMATERIAL: TcxGridDBBandedColumn
          Caption = #1052#1072#1090#1077#1088#1080#1072#1083
          DataBinding.FieldName = 'MATERIAL$NAME'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Sorting = False
          SortIndex = 1
          SortOrder = soAscending
          Width = 100
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object GridStoreViewPROBE: TcxGridDBBandedColumn
          Caption = #1055#1088#1086#1073#1072
          DataBinding.FieldName = 'PROBE$NAME'
          HeaderAlignmentHorz = taCenter
          Options.Sorting = False
          SortIndex = 2
          SortOrder = soAscending
          Width = 75
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridStoreViewWEIGHT: TcxGridDBBandedColumn
          Caption = #1063#1080#1089#1090#1099#1081
          DataBinding.FieldName = 'WEIGHT'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Options.Sorting = False
          Width = 85
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridStoreViewWEIGHTREAL: TcxGridDBBandedColumn
          Caption = #1043#1088#1103#1079#1085#1099#1081
          DataBinding.FieldName = 'WEIGHT$REAL'
          HeaderAlignmentHorz = taCenter
          Options.Sorting = False
          Width = 85
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridStoreViewDEPARTMENTNAME: TcxGridDBBandedColumn
          Caption = #1057#1082#1083#1072#1076
          DataBinding.FieldName = 'DEPARTMENT$NAME'
          HeaderAlignmentHorz = taCenter
          SortIndex = 0
          SortOrder = soAscending
          Width = 175
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridStoreViewWEIGHT1000: TcxGridDBBandedColumn
          Caption = '1000'
          DataBinding.FieldName = 'WEIGHT$1000'
          HeaderAlignmentHorz = taCenter
          Options.Sorting = False
          Position.BandIndex = 1
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object GridStoreLevel: TcxGridLevel
        GridView = GridStoreView
      end
    end
    object BarDockControl: TdxBarDockControl
      AlignWithMargins = True
      Left = 6
      Top = 6
      Width = 1019
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 0
      Align = dalTop
      BarManager = BarManager
    end
    object BarFilterDockControl: TdxBarDockControl
      Left = 2
      Top = 58
      Width = 1027
      Height = 42
      Align = dalTop
      BarManager = BarManager
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'Office'
      'Common'
      'Material')
    Categories.ItemsVisibles = (
      2
      0
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 8
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDockControl
      DockedDockControl = BarDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 64
      FloatClientHeight = 137
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonMaterial'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object BarFilter: TdxBar
      Caption = 'BarFilter'
      CaptionButtons = <>
      DockControl = BarFilterDockControl
      DockedDockControl = BarFilterDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1041
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOffice'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonDate'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object ButtonRangeDay: TdxBarLargeButton
      Caption = 'Day'
      Category = 0
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonOffice: TdxBarLargeButton
      Caption = #1057#1082#1083#1072#1076
      Category = 1
      Hint = #1057#1082#1083#1072#1076
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 10
      Down = True
      LargeImageIndex = 9
      OnClick = ButtonOfficeClick
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object ButtonDate: TdxBarLargeButton
      Align = iaClient
      Caption = #1044#1072#1090#1072
      Category = 2
      Hint = #1044#1072#1090#1072
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 20
      Down = True
      LargeImageIndex = 30
      OnClick = ButtonDateClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Align = iaRight
      Category = 2
      AutoGrayScale = False
      Width = 64
    end
    object ButtonGold: TdxBarLargeButton
      Tag = 1
      Action = ActionGold
      Category = 3
      ButtonStyle = bsChecked
      GroupIndex = 1
      Down = True
      AutoGrayScale = False
      Width = 64
    end
    object ButtonSilver: TdxBarLargeButton
      Tag = 2
      Action = ActionSilver
      Category = 3
      ButtonStyle = bsChecked
      GroupIndex = 1
      AutoGrayScale = False
      Width = 64
    end
    object ButtonMaterial: TdxBarLargeButton
      Tag = 1
      Caption = #1047#1086#1083#1086#1090#1086
      Category = 3
      Hint = #1047#1086#1083#1086#1090#1086
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 50
      Down = True
      LargeImageIndex = 0
      OnClick = ButtonMaterialClick
      AutoGrayScale = False
      Width = 64
    end
  end
  object Store: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  department$id,'
      '  department$name,'
      '  material$id,'
      '  material$name,'
      '  probe$id,'
      '  probe$name,'
      '  weight,'
      '  weight$real,'
      '  weight$1000'
      ''
      'from'
      ''
      '  buy$store$all(:department$id, :store$date)'
      ''
      'where material$id = :material$id')
    BeforeOpen = StoreBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    Left = 8
    Top = 240
    object StoreDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object StoreDEPARTMENTNAME: TFIBStringField
      FieldName = 'DEPARTMENT$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object StoreMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object StoreMATERIALNAME: TFIBStringField
      FieldName = 'MATERIAL$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object StorePROBEID: TFIBIntegerField
      FieldName = 'PROBE$ID'
    end
    object StorePROBENAME: TFIBStringField
      FieldName = 'PROBE$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object StoreWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object StoreWEIGHTREAL: TFIBBCDField
      FieldName = 'WEIGHT$REAL'
      Size = 3
      RoundByScale = True
    end
    object StoreWEIGHT1000: TFIBFloatField
      FieldName = 'WEIGHT$1000'
      DisplayFormat = '0.000'
    end
  end
  object SourceStore: TDataSource
    DataSet = Store
    Left = 40
    Top = 240
  end
  object MenuOffice: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 40
    Top = 208
  end
  object Actions: TActionList
    Images = DataBuy.Image32
    Left = 8
    Top = 176
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object ActionGold: TAction
      Tag = 1
      Caption = #1047#1086#1083#1086#1090#1086
      ImageIndex = 0
      OnExecute = ActionMaterialExecute
    end
    object ActionSilver: TAction
      Tag = 2
      Caption = #1057#1077#1088#1077#1073#1088#1086
      ImageIndex = 0
      OnExecute = ActionMaterialExecute
    end
  end
  object MenuMaterial: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonGold'
      end
      item
        Visible = True
        ItemName = 'ButtonSilver'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 72
    Top = 208
  end
end
