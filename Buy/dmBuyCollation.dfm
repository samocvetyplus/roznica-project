object DataBuyCollation: TDataBuyCollation
  OldCreateOrder = False
  Height = 352
  Width = 317
  object CollationOpen: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$COLLATION$O (:ID)')
    StoredProcName = 'BUY$COLLATION$O'
    Left = 40
    Top = 24
  end
  object CollationClose: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$COLLATION$C (:ID)')
    StoredProcName = 'BUY$COLLATION$C'
    Left = 40
    Top = 72
  end
  object CollationInsert: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$COLLATION$I (:STORE$DATE)')
    StoredProcName = 'BUY$COLLATION$I'
    Left = 120
    Top = 24
    qoNoForceIsNull = True
  end
  object CollationUpdate: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    Left = 120
    Top = 72
  end
  object CollationDelete: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUY$COLLATION$D (:ID)')
    StoredProcName = 'BUY$COLLATION$D'
    Left = 120
    Top = 120
  end
  object CollationItemDelete: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    Left = 216
    Top = 120
  end
  object CollationItemUpdate: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    SQL.Strings = (
      
        'EXECUTE PROCEDURE BUY$COLLATION$ITEM$U (?ID, ?WEIGHT$ACTUAL, ?WE' +
        'IGHT$REAL$ACTUAL)')
    StoredProcName = 'BUY$COLLATION$ITEM$U'
    Left = 216
    Top = 72
  end
  object CollationItemInsert: TpFIBStoredProc
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    Left = 216
    Top = 24
  end
end
