unit frmBuyJournalSession;

interface

uses

  Controls, DB, Classes, ActnList,                                                       
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage,  cxDBData, dxSkinsdxBarPainter,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,
  cxGrid, cxGroupBox,
  dxSkinsCore,  dxSkinscxPCPainter, dxBar,
  frmBuyJournal, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxSplitter, cxTextEdit,
  cxMemo, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;                                         

type

{ TFrameBuyJournalSession }                                                                              
                                                                                                 
  TFrameBuyJournalSession = class(TFrameBuyJournal)
    ButtonPrint101: TdxBarLargeButton;
  protected
    function GetClassCode: Integer; override;
    function GetInvoiceClass: TFrameBuyInvoiceClass; override;
    procedure CustomSetup; override;
  end;

implementation

{$R *.dfm}

uses

  dmBuy, frmBuyInvoiceSession;

{ TFrameBuyJournalSession }

function TFrameBuyJournalSession.GetClassCode: Integer;
begin
  Result := 0;

  if ButtonSend.Down then
  begin
    Result := 1;
  end else

  if ButtonReceive.Down then
  begin                          
    Result := 2;
  end;
end;

function TFrameBuyJournalSession.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoiceSession;
end;

procedure TFrameBuyJournalSession.CustomSetup;
begin
  CategoryVisible['Receive'] := cbTrue;

  if DepartmentID = DataBuy.SelfDepartmentID then                                            
  begin
    if not DataBuy.IsCenterDepartment then
    begin
      CategoryVisible['Open + Close'] := cbTrue;

      CategoryVisible['Delete'] := cbTrue;

    end;
  end;

  //if DepartmentID <> 0 then
  begin
    CategoryVisible['Print'] := cbTrue;
  end;

  inherited CustomSetup;
end;

end.

