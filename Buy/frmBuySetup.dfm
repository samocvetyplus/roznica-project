object DialogBuySetup: TDialogBuySetup
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  ClientHeight = 413
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Layout: TdxLayoutControl
    Left = 0
    Top = 24
    Width = 337
    Height = 389
    Align = alClient
    TabOrder = 0
    TabStop = False
    LayoutLookAndFeel = LayoutLookAndFeel
    object LayoutEditorModel: TcxDBTextEdit
      Left = 22
      Top = 197
      DataBinding.DataField = 'FR$MODEL'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 4
      Width = 121
    end
    object LayoutEditorProductionN: TcxDBTextEdit
      Left = 22
      Top = 242
      DataBinding.DataField = 'FR$PRODUCTION$NUMBER'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 5
      Width = 121
    end
    object LayoutEditorRegisrationN: TcxDBTextEdit
      Left = 22
      Top = 287
      DataBinding.DataField = 'FR$REGISTRATION$NUMBER'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 6
      Width = 121
    end
    object LayoutEditorCompany: TcxDBTextEdit
      Left = 22
      Top = 37
      DataBinding.DataField = 'COMPANY$NAME'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 1
      Width = 121
    end
    object LayoutEditorAddress: TcxDBTextEdit
      Left = 79
      Top = 122
      DataBinding.DataField = 'ADDRESS'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 3
      Width = 121
    end
    object LayoutEditorN: TcxDBSpinEdit
      Left = 22
      Top = 122
      DataBinding.DataField = 'CODE'
      DataBinding.DataSource = DataSource
      Enabled = False
      Style.HotTrack = False
      TabOrder = 2
      Width = 51
    end
    object LayoutEditorCompanyButton: TcxButton
      Left = 275
      Top = 28
      Width = 40
      Height = 40
      Caption = 'LayoutEditorCompanyButton'
      TabOrder = 0
      OnClick = LayoutEditorCompanyButtonClick
      Glyph.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000001010100010101000102020001
        0202000102020001020200010202000102020001020200010101000101010000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000000000000000000052A6D4FF51A5D4FF51A5D3FF52A4D4FF51A4
        D3FF52A4D2FF53A3D2FF53A3D1FF54A2D0FF54A2D0FF56A2CFFF58A1CEFF59A1
        CCFF00000000000000000000000000000000000000005C9FC9FF5D9EC8FF5D9E
        C8FF5E9EC7FF0000000000000000000000000000000000000000000000000000
        000000000000000000000001010150A6D4FFCFEBFAFFBBE3F8FFAEDDF7FFAADA
        F6FFA8D9F7FFA7D8F7FFA7D8F7FFA6D7F7FFAAD8F8FFB5DDF9FFC9E7FBFF58A1
        CEFF00010101000000000000000000000000000000005C9FC9FFD6EDFCFFD5ED
        FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
        000000000000000000000001010150A7D6FFBCE3F8FF90D2F3FF74C6F0FF6BC1
        F0FF69BFF0FF68BDF0FF67BCF0FF67BBF1FF6EBDF2FF8AC9F5FFB5DDF9FF56A1
        CFFF00010101000000000000000000000000000000005B9FCAFFD6EDFCFFD6ED
        FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024EA8D6FFB0E0F6FF76C9EFFF50B9ECFF45B2
        EBFF42B0EBFF41AEECFF40ABECFF41AAEDFF4AADEEFF6FBCF3FFA9D7F8FF55A2
        D0FF00010202000000000000000000000000000000005B9FCAFFD6EEFCFFD6ED
        FCFF5D9EC9FF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024EA8D7FFADDFF6FF6EC6EEFF46B6EBFF3AAF
        E9FF37ACEAFF36AAEAFF34A8EBFF35A6EBFF40A9EDFF67B9F1FFA6D6F7FF54A2
        D0FF00010202000000000000000000000000000000005BA0CBFFD7EEFCFFD6EE
        FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024EA9D7FFAEDFF5FF6EC7EEFF45B6EAFF38B0
        E9FF36ADE9FF35AAEAFF34A8EAFF34A7EBFF3FAAEDFF67BAF1FFA6D7F7FF54A2
        D1FF00010202000000000000000000000000000000005AA0CBFFD7EEFCFFD7EE
        FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024EA9D8FFAEDFF5FF6FC8EEFF46B8EAFF39B1
        E8FF37AEE9FF36ACEAFF35AAEAFF35A8EBFF40ABECFF68BBF1FFA6D7F7FF53A3
        D1FF00010202000000000000000000000000000000005AA0CBFFD8EEFCFFD7EE
        FCFF5B9FCAFF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024DA9D8FFAFE0F5FF6FC9EEFF47B9EAFF3AB3
        E8FF38AFE9FF37ADE9FF35ABEAFF35AAEAFF40ACECFF68BCF0FFA6D7F7FF53A3
        D2FF000102020000000000000000000000000000000059A1CCFFD7EEFCFFD8EE
        FCFF5BA0CAFF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024DAAD9FFAFE1F5FF70CAEEFF47BAE9FF3AB3
        E8FF38B1E8FF37AFE9FF36ACE9FF36ABEAFF41ADECFF65BBF0FFA2D5F6FF51A3
        D3FF010305050001020200010202000102020102030358A1CDFFD4ECFCFFD5EC
        FCFF5AA0CBFF0000000000000000000000000000000000000000000000000000
        00000000000000000000000102024CAAD9FFB0E1F5FF70CBEEFF48BBE9FF3BB4
        E8FF39B2E8FF38B0E9FF37AEE9FF37ABEAFF3EADEBFF5BB8EFFF90CEF4FF7ABC
        E5FF51A3D3FF53A3D1FF54A2D1FF54A1D1FF54A1D1FF82BCE2FFC0E2FAFFCBE8
        FBFF59A0CCFF0001010100000000000000000000000000000000000000000000
        00000000000000000000000102024CABDAFFB0E1F5FF71CCEDFF49BDE9FF3CB6
        E7FF3AB3E8FF38B1E8FF37AFE9FF37ADE9FF3AACEAFF49B1ECFF6CBFF1FF90CD
        F5FFA2D5F6FFA7D7F7FFA6D6F7FFA5D5F8FFA0D2F7FF98CEF7FF9CCFF7FFB8DE
        FAFF589FCEFF0001010100000000000000000000000000000000000000000000
        00000000000000000000000202024BACDBFFB1E2F5FF72CDEDFF49BEE8FF3CB7
        E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF37ADEAFF3CADEBFF49B0EDFF5BB7
        EFFF65BAF1FF66B9F1FF66B8F2FF64B6F2FF62B4F2FF63B4F2FF78BEF5FFABD7
        F8FF56A0CEFF0001020200000000000000000000000000000000000000000000
        00000000000000000000010202024BACDBFFB1E2F5FF72CDEDFF4ABEE8FF3DB8
        E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37ADE9FF36ACEAFF39ABEAFF3CAA
        ECFF3EA9ECFF3FA8EDFF3EA6EDFF3DA4EEFF3CA3EEFF44A5F0FF67B5F3FFA4D3
        F8FF55A0D0FF0001020200000000000000000000000000000000000000000000
        00000000000000000000010304044AACDCFFACE1F4FF6FCDECFF4ABFE8FF3DB9
        E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AFE9FF36ACE9FF36AAEAFF35A8
        EBFF34A6EBFF33A4ECFF32A2ECFF31A0EDFF329EEDFF3CA2EFFF61B3F2FF9FD1
        F8FF54A0D0FF010204040000000000000000000000000000000048ADDEFF48AC
        DDFF49ACDDFF4AABDCFF4AACDCFF7FC8E8FF9EDDF2FF67CBEBFF47BFE7FF3EBA
        E6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0E9FF37AEE9FF36ABEAFF35A9
        EAFF33A7EBFF32A5EBFF31A2ECFF30A0ECFF309EEDFF38A1EEFF57AFF1FF8FCA
        F6FF7EBBE3FF56A1CEFF59A1CCFF59A1CCFF5AA0CBFF5BA0CBFF010203032F70
        90A58DCBEAFFE7F4FCFFDAF1FAFFBAE6F5FF86D6EFFF59C7E9FF43BFE6FF3EBB
        E6FF3CBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1E8FF37AFE9FF36ADE9FF35AA
        EAFF34A8EAFF33A6EBFF32A4ECFF31A1ECFF319FEDFF34A0EDFF49A8F0FF77BE
        F4FFACD7F8FFCDE8FBFFCDE8F8FF8EC1E0FF2F536A8400000000000000000102
        03032C69889C8ACAEAFFD5EFF9FFB3E4F4FF80D4EDFF57C8E8FF45C0E6FF3FBD
        E6FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF36AC
        EAFF35AAEAFF34A7EBFF33A5EBFF32A3ECFF31A2ECFF36A1EEFF48A9EFFF72BB
        F3FFA6D5F8FFBDDFF6FF8CC1E0FF2D5168810000000000000000000000000000
        0000000000002A64819486CAEAFFBBE6F5FF98DCF1FF6DCFEBFF4EC4E7FF41BF
        E5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37AD
        E9FF35ABEAFF34A9EAFF33A6EBFF33A5EBFF36A4EDFF44A9EEFF60B5F1FF8BC9
        F5FFAAD6F5FF87BFE2FF2A4F657D000000000000000000000000000000000000
        00000000000000010101265C758783C9E9FFAFE1F3FF8CD8EFFF5FCAE9FF46C0
        E6FF3FBDE5FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AF
        E9FF36ACE9FF35AAEAFF35A9EBFF38A7ECFF46ACEEFF61B7F0FF89C9F5FFA4D3
        F4FF85BFE3FF294F647B00010101000000000000000000000000000000000000
        000000000000000000000001010124566E7F81C9E9FFA6E1F2FF6DCFEBFF4AC2
        E6FF40BEE5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0
        E9FF37AEE9FF37ACEAFF3AABEBFF47B0EDFF63BAF0FF8BCBF4FFA4D4F3FF84C0
        E4FF274D64790001010100000000000000000000000000000000000000000000
        00000000000000000000000000000102030348AEDEFFB2E5F4FF74D2EBFF4DC4
        E7FF41C0E5FF3FBDE5FF3EBBE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1
        E8FF38B0E9FF3BAFEAFF49B2ECFF65BDEFFF8ECDF4FFA4D4F3FF84C1E4FF264C
        6276000101010000000000000000000000000000000000000000000000000000
        00000000000000000000000000000102020247AEDEFFB5E5F4FF77D2ECFF52C7
        E7FF49C3E6FF49C1E6FF47C0E7FF42BDE7FF3DB9E6FF3BB7E7FF3AB5E7FF3AB4
        E8FF3EB2E9FF4BB6EBFF67C0EFFF90D0F3FFA3D6F3FF85C1E5FF24485E700001
        0101000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000102020248AFDEFFBAE7F5FF82D6EDFF64CD
        E9FF65CCEAFF67CCEBFF61C9EAFF51C2E9FF44BDE7FF3EB9E7FF3CB6E7FF40B6
        E9FF4DBAEAFF6AC3EFFF92D2F3FFA4D6F2FF83C1E5FF22475B6D000101010000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000001010147AEDFFFC7EAF7FF9EDFF1FF8FDA
        EFFF98DCF1FF9BDCF1FF91D8F0FF73CEECFF56C3E9FF45BCE7FF43BAE8FF4FBD
        EAFF6CC6EEFF94D4F3FFA4D7F1FF81C2E5FF2145596A00010101000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000001010147AEDFFFDAF1FAFFC9EBF7FFC4EA
        F7FF9CD6EFFF45AAD7F796D4EEFFA0DDF2FF79D0EEFF5DC5EAFF59C2EBFF6FC9
        EDFF96D6F3FFA4D7F1FF80C2E5FF204357670001010100000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000046AEDFFF47AEDFFF47AEDEFF48AE
        DEFF48ADDDFF132F3B4446A6D3F499D5EEFFA6DFF3FF87D4F0FF82D2EFFF9BDA
        F3FFA6D9F2FF4BA8D6FC1E425564000101010000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000010101000101010001
        010100010101000101011129343C44A1CEEE98D3EDFFB9E5F6FFB6E3F6FFA9DB
        F1FF4BA8D7FC1E43556400010101000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000101010E232C33439FCBEB9BD3EDFFB5DEF3FF49A9
        D8FC1B3E505D0001010100000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000C1C23294299C5E449A9D8FC1A3B
        4C58000101010000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000009161C211734434E0000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000}
      LookAndFeel.Kind = lfUltraFlat
      PaintStyle = bpsGlyph
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
    end
    object LayoutButtonOK: TcxButton
      Left = 241
      Top = 339
      Width = 40
      Height = 40
      Action = ActionOK
      TabOrder = 7
      LookAndFeel.Kind = lfUltraFlat
      PaintStyle = bpsGlyph
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
    end
    object LayoutButtonCancel: TcxButton
      Left = 287
      Top = 339
      Width = 40
      Height = 40
      Action = ActionCancel
      TabOrder = 8
      LookAndFeel.Kind = lfUltraFlat
      PaintStyle = bpsGlyph
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Flat = True
      SpeedButtonOptions.Transparent = True
    end
    object LayoutGroupRoot: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      object LayoutGroupCompany: TdxLayoutGroup
        CaptionOptions.Text = #1050#1086#1084#1087#1072#1085#1080#1103
        ButtonOptions.Buttons = <>
        LayoutDirection = ldHorizontal
        object LayoutItemCompanyButton: TdxLayoutItem
          AlignHorz = ahRight
          AlignVert = avCenter
          CaptionOptions.Visible = False
          CaptionOptions.Layout = clTop
          Visible = False
          Control = LayoutEditorCompanyButton
          ControlOptions.ShowBorder = False
        end
        object LayoutItemCompany: TdxLayoutItem
          AlignHorz = ahClient
          AlignVert = avCenter
          CaptionOptions.Layout = clTop
          Enabled = False
          Control = LayoutEditorCompany
          ControlOptions.ShowBorder = False
        end
      end
      object LayoutGroupBuy: TdxLayoutGroup
        CaptionOptions.Text = #1057#1082#1091#1087#1082#1072
        Enabled = False
        ButtonOptions.Buttons = <>
        object LayoutGroup2: TdxLayoutGroup
          AlignVert = avTop
          Enabled = False
          ButtonOptions.Buttons = <>
          Hidden = True
          LayoutDirection = ldHorizontal
          ShowBorder = False
          object LayoutItemN: TdxLayoutItem
            AlignHorz = ahLeft
            AlignVert = avTop
            CaptionOptions.Text = #8470
            CaptionOptions.Layout = clTop
            Enabled = False
            Control = LayoutEditorN
            ControlOptions.ShowBorder = False
          end
          object LayoutItemAddress: TdxLayoutItem
            AlignHorz = ahClient
            AlignVert = avTop
            CaptionOptions.Text = #1040#1076#1088#1077#1089
            CaptionOptions.Layout = clTop
            Enabled = False
            Control = LayoutEditorAddress
            ControlOptions.ShowBorder = False
          end
        end
      end
      object LayoutGroupRegister: TdxLayoutGroup
        AlignHorz = ahClient
        CaptionOptions.Text = #1060#1080#1089#1082#1072#1083#1100#1085#1099#1081' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1086#1088
        Enabled = False
        ButtonOptions.Buttons = <>
        object LayoutItemModel: TdxLayoutItem
          AlignHorz = ahClient
          AlignVert = avTop
          CaptionOptions.Text = #1052#1086#1076#1077#1083#1100
          CaptionOptions.Layout = clTop
          Enabled = False
          Control = LayoutEditorModel
          ControlOptions.ShowBorder = False
        end
        object LayoutItemProductionN: TdxLayoutItem
          AlignHorz = ahClient
          AlignVert = avTop
          CaptionOptions.Text = #1053#1086#1084#1077#1088' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
          CaptionOptions.Layout = clTop
          Enabled = False
          Control = LayoutEditorProductionN
          ControlOptions.ShowBorder = False
        end
        object LayoutItemRegistrationN: TdxLayoutItem
          AlignHorz = ahClient
          AlignVert = avTop
          CaptionOptions.Text = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1086#1085#1085#1099#1081' '#1085#1086#1084#1077#1088
          CaptionOptions.Layout = clTop
          Enabled = False
          Control = LayoutEditorRegisrationN
          ControlOptions.ShowBorder = False
        end
      end
      object LayoutSeparatorItem1: TdxLayoutSeparatorItem
        AlignVert = avBottom
        CaptionOptions.Text = 'Separator'
        SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
        SizeOptions.SizableHorz = False
        SizeOptions.SizableVert = False
      end
      object LayoutGroupControl: TdxLayoutGroup
        AlignVert = avBottom
        ButtonOptions.Buttons = <>
        Hidden = True
        LayoutDirection = ldHorizontal
        ShowBorder = False
        object LayoutItemOK: TdxLayoutItem
          AlignHorz = ahRight
          CaptionOptions.Visible = False
          Visible = False
          Control = LayoutButtonOK
          ControlOptions.ShowBorder = False
        end
        object LayoutItemCancel: TdxLayoutItem
          AlignHorz = ahRight
          CaptionOptions.Visible = False
          Control = LayoutButtonCancel
          ControlOptions.ShowBorder = False
        end
      end
    end
  end
  object Ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 337
    Height = 24
    ApplicationButton.Visible = False
    BarManager = BarManager
    ColorSchemeName = 'Blue'
    PopupMenuItems = []
    ShowTabGroups = False
    ShowTabHeaders = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 5
    TabStop = False
  end
  object Setup: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure buy$setup$iu(:company$id, :code, :address, :fr' +
        '$model, :fr$production$number, :fr$registration$number)')
    InsertSQL.Strings = (
      
        'execute procedure buy$setup$iu(:company$id, :code, :address, :fr' +
        '$model, :fr$production$number, :fr$registration$number)')
    RefreshSQL.Strings = (
      
        'select company$id, company$name, code, address, fr$model, fr$pro' +
        'duction$number, fr$registration$number'
      'from buy$setup$s(:company$id)')
    SelectSQL.Strings = (
      
        'select company$id, company$name, code, address, fr$model, fr$pro' +
        'duction$number, fr$registration$number'
      'from buy$setup$s(:company$id)')
    BeforeOpen = SetupBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    UpdateTransaction = DataBuy.TransactionWrite
    RefreshTransactionKind = tkUpdateTransaction
    Left = 160
    Top = 352
    object SetupCODE: TFIBIntegerField
      FieldName = 'CODE'
    end
    object SetupADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 256
      EmptyStrToNull = True
    end
    object SetupFRMODEL: TFIBStringField
      FieldName = 'FR$MODEL'
      Size = 32
      EmptyStrToNull = True
    end
    object SetupFRPRODUCTIONNUMBER: TFIBStringField
      FieldName = 'FR$PRODUCTION$NUMBER'
      Size = 32
      EmptyStrToNull = True
    end
    object SetupFRREGISTRATIONNUMBER: TFIBStringField
      FieldName = 'FR$REGISTRATION$NUMBER'
      Size = 32
      EmptyStrToNull = True
    end
    object SetupCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object SetupCOMPANYNAME: TFIBStringField
      Alignment = taCenter
      FieldName = 'COMPANY$NAME'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSource: TDataSource
    DataSet = Setup
    Left = 192
    Top = 352
  end
  object Actions: TActionList
    Images = DataBuy.Image32
    Left = 96
    Top = 352
    object ActionOK: TAction
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 21
      OnExecute = ActionOKExecute
      OnUpdate = ActionOKUpdate
    end
    object ActionCancel: TAction
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionCancelExecute
      OnUpdate = ActionCancelUpdate
    end
  end
  object LayoutLookAndFeelList: TdxLayoutLookAndFeelList
    Left = 128
    Top = 352
    object LayoutLookAndFeel: TdxLayoutCxLookAndFeel
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    PopupMenuLinks = <>
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 32
    Top = 352
    DockControlHeights = (
      0
      0
      0
      0)
    object ButtonOwner: TdxBarLargeButton
      Caption = #1042#1083#1072#1076#1077#1083#1077#1094
      Category = 0
      Hint = #1042#1083#1072#1076#1077#1083#1077#1094
      Visible = ivAlways
      LargeImageIndex = 9
      OnClick = ButtonOwnerClick
    end
  end
  object MenuOwner: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 64
    Top = 352
  end
end
