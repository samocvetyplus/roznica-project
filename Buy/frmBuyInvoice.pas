unit frmBuyInvoice;

interface

uses

  Forms, SysUtils, Windows, DB, DBClient, Classes, ActnList,
  Controls, Variants, StdCtrls, DateUtils,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxDBData, cxClasses, cxGridLevel, cxGridCustomTableView, cxButtons,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxGridCustomView, cxGrid,  cxDBEdit, cxTextEdit, cxMaskEdit, cxSplitter,
  cxDropDownEdit, cxGroupBox, cxPC, dxSkinsCore, Menus, dxLayoutcxEditAdapters,
  dxLayoutControl, dxSkinscxPCPainter, dxSkinsdxBarPainter, dxBar,
  dxLayoutLookAndFeels, dxSkinsDefaultPainters, cxGridDBTableView, cxCheckBox,
  cxCalendar, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;


type

  TAgentBit = (abNone, abOwner, abAffinage, abRecycling, abTrade);

{ TFrameBuyInvoice }

  TFrameBuyInvoice = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    GroupBoxHeader: TcxGroupBox;
    InvoiceItem: TpFIBDataSet;
    SourceInvoiceItem: TDataSource;
    Invoice: TpFIBDataSet;
    SourceInvoice: TDataSource;
    LayoutGroupRoot: TdxLayoutGroup;
    Layout: TdxLayoutControl;
    LayoutLookAndFeel: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel1: TdxLayoutCxLookAndFeel;
    LayoutGroupInvoice: TdxLayoutGroup;
    LayoutGroupAgent: TdxLayoutGroup;
    LayoutGroupState: TdxLayoutGroup;
    LayoutEditorN: TcxDBTextEdit;
    LayoutItemN: TdxLayoutItem;
    LayoutItemDate: TdxLayoutItem;
    LayoutEditorState: TcxDBTextEdit;
    LayoutItemState: TdxLayoutItem;
    LayoutEditorStateDate: TcxDBTextEdit;
    LayoutItemStateDate: TdxLayoutItem;
    LayoutEditorStatePerson: TcxDBTextEdit;
    LayoutItemStatePerson: TdxLayoutItem;
    Action: TActionList;
    BarDock: TdxBarDockControl;
    MenuProbeGold: TdxBarPopupMenu;
    ButtonAddGold: TdxBarLargeButton;
    ButtonProbeGold: TdxBarLargeButton;
    ActionAddGold: TAction;
    LargeButtonClose: TdxBarLargeButton;
    LayoutGroup7: TdxLayoutGroup;
    MenuOffice: TdxBarPopupMenu;
    LayoutEditorDepartment: TcxDBTextEdit;
    LayoutItemDepartment: TdxLayoutItem;
    LayoutEditorAgent: TcxDBTextEdit;
    LayoutItemAgent: TdxLayoutItem;
    LayoutGroup4: TdxLayoutGroup;
    ButtonAgent: TdxBarLargeButton;
    MenuCompany: TdxBarPopupMenu;
    GridInvoiceItem: TcxGrid;
    GridInvoiceItemView: TcxGridDBBandedTableView;
    GridInvoiceItemViewMATERIAL: TcxGridDBBandedColumn;
    GridInvoiceItemViewPROBE: TcxGridDBBandedColumn;
    GridInvoiceItemViewMEMO: TcxGridDBBandedColumn;
    GridInvoiceItemViewWEIGHT: TcxGridDBBandedColumn;
    GridInvoiceItemViewPRICE: TcxGridDBBandedColumn;
    GridInvoiceItemViewCOST: TcxGridDBBandedColumn;
    GridInvoiceItemLevel: TcxGridLevel;
    ActionDelete: TAction;
    ButtonDelete: TdxBarLargeButton;
    MenuMemoGold: TdxBarPopupMenu;
    ButtonMemo: TdxBarLargeButton;
    SplitterLeft: TcxSplitter;
    CurrentStore: TClientDataSet;
    SplitterTop: TcxSplitter;
    GridStore: TcxGrid;
    GridStoreView: TcxGridDBBandedTableView;
    GridStoreViewMATERIAL: TcxGridDBBandedColumn;
    GridStoreViewPROBE: TcxGridDBBandedColumn;
    GridStoreViewWEIGHT: TcxGridDBBandedColumn;
    GridStoreLevel: TcxGridLevel;
    Store: TClientDataSet;
    SourceStore: TDataSource;
    LayoutItemCompany: TdxLayoutItem;
    LayoutEditorCompany: TcxDBTextEdit;
    InvoiceItemID: TFIBIntegerField;
    InvoiceItemINVOICEID: TFIBIntegerField;
    InvoiceItemMATERIALID: TFIBIntegerField;
    InvoiceItemMATERIALNAME: TFIBStringField;
    InvoiceItemPROBEID: TFIBIntegerField;
    InvoiceItemMEMOID: TFIBIntegerField;
    InvoiceItemMEMONAME: TFIBStringField;
    InvoiceItemWEIGHT: TFIBBCDField;
    InvoiceItemPRICE: TFIBBCDField;
    InvoiceItemCOST: TFIBBCDField;
    InvoiceItemPROBENAME: TFIBStringField;
    ActionPrint: TAction;
    InvoiceItemWEIGHTREAL: TFIBBCDField;
    GridInvoiceItemViewWEIGHTREAL: TcxGridDBBandedColumn;
    GridStoreViewWEIGHTREAL: TcxGridDBBandedColumn;
    LayoutGroup6: TdxLayoutGroup;
    ActionAddSilver: TAction;
    ButtonAddSilver: TdxBarLargeButton;
    MenuProbeSilver: TdxBarPopupMenu;
    ButtonProbeSilver: TdxBarLargeButton;
    LayoutGroupForeign: TdxLayoutGroup;
    LayoutEditorForeignN: TcxDBTextEdit;
    LayoutItem2: TdxLayoutItem;
    LayoutEditorForeignDate: TcxDBDateEdit;
    LayoutItem3: TdxLayoutItem;
    LayoutEditorForeignCost: TcxDBTextEdit;
    LayoutItemForeignCost: TdxLayoutItem;
    LayoutGroup1: TdxLayoutGroup;
    LayoutGroup2: TdxLayoutGroup;
    LayoutGroup3: TdxLayoutGroup;
    LayoutGroup5: TdxLayoutGroup;
    LayoutEditorDate: TcxDBDateEdit;
    LayoutEditorAgentButton: TcxButton;
    LayoutItemAgentButton: TdxLayoutItem;
    InvoiceItemPROBEK: TFIBFloatField;
    InvoiceItemWEIGHT1000: TFIBFloatField;
    GridInvoiceItemViewWEIGHT1000: TcxGridDBBandedColumn;
    ActionAgent: TAction;
    InvoiceID: TFIBIntegerField;
    InvoiceREFERENCEID: TFIBIntegerField;
    InvoiceFORWARDID: TFIBIntegerField;
    InvoiceBACKWARDID: TFIBIntegerField;
    InvoiceCOMPANYID: TFIBIntegerField;
    InvoiceCOMPANYNAME: TFIBStringField;
    InvoiceDEPARTMENTID: TFIBIntegerField;
    InvoiceDEPARTMENTNAME: TFIBStringField;
    InvoiceCLASSCODE: TFIBIntegerField;
    InvoiceCLASSNAME: TFIBStringField;
    InvoiceROUTE: TFIBIntegerField;
    InvoiceN: TFIBIntegerField;
    InvoiceINVOICEDATE: TFIBDateField;
    InvoiceSENDERID: TFIBIntegerField;
    InvoiceSENDERNAME: TFIBStringField;
    InvoiceRECEIVERID: TFIBIntegerField;
    InvoiceRECEIVERNAME: TFIBStringField;
    InvoiceMATERIALID: TFIBIntegerField;
    InvoiceMATERIALNAME: TFIBStringField;
    InvoiceAGENTID: TFIBIntegerField;
    InvoiceAGENTCLASSCODE: TFIBIntegerField;
    InvoiceAGENTNAME: TFIBStringField;
    InvoiceTAXID: TFIBIntegerField;
    InvoiceTAXNAME: TFIBStringField;
    InvoiceTAX: TFIBIntegerField;
    InvoiceWEIGHT: TFIBBCDField;
    InvoiceWEIGHTREAL: TFIBBCDField;
    InvoiceWEIGHT1000: TFIBBCDField;
    InvoiceCOST: TFIBBCDField;
    InvoiceCOSTTAX: TFIBBCDField;
    InvoiceCOSTTOTAL: TFIBBCDField;
    InvoiceSTATECODE: TFIBIntegerField;
    InvoiceSTATENAME: TFIBStringField;
    InvoiceSTATEDATE: TFIBDateTimeField;
    InvoiceSTATEBYID: TFIBIntegerField;
    InvoiceSTATEBYNAME: TFIBStringField;
    InvoiceFOREIGNN: TFIBStringField;
    InvoiceFOREIGNDATE: TFIBDateField;
    InvoiceFOREIGNCOST: TFIBBCDField;
    LayoutGroupCompany: TdxLayoutGroup;
    GridStoreViewWEIGHT1000: TcxGridDBBandedColumn;
    PageContent: TcxPageControl;
    SheetContent: TcxTabSheet;
    GroupBoxContent: TcxGroupBox;
    procedure InvoiceBeforeOpen(DataSet: TDataSet);
    procedure InvoiceItemBeforeOpen(DataSet: TDataSet);
    procedure ActionAddGoldExecute(Sender: TObject);
    procedure ActionAddGoldUpdate(Sender: TObject);
    procedure LargeButtonCloseClick(Sender: TObject);
    procedure ButtonProbeClick(Sender: TObject);
    procedure ButtonAgentClick(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ButtonMemoClick(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure InvoiceItemBeforePost(DataSet: TDataSet);
    procedure ActionPrintExecute(Sender: TObject);
    procedure ActionAddSilverUpdate(Sender: TObject);
    procedure ActionAddSilverExecute(Sender: TObject);
    procedure OnUpdate(Sender: TObject);
    procedure ActionAgentExecute(Sender: TObject);
    procedure ActionAgentUpdate(Sender: TObject);
    procedure InvoiceItemBeforeEdit(DataSet: TDataSet);
    procedure InvoiceItemAfterPost(DataSet: TDataSet);
    procedure InvoiceAfterOpen(DataSet: TDataSet);
  private
    FID: Integer;
    FReadOnly: Boolean;
    FMaterialID: Integer;
    FClassCode: Integer;
    FActive: Boolean;
    StoredWeight: Currency;
    StoredWeightReal: Currency;
    StoredInvoiceDate: TDate;
    function GetN: Integer;
    function IsReadOnly: Boolean;

    procedure CalculateStore;

    function GetCategoryVisible(Name: string): Boolean;
    procedure SetCategoryVisible(Name: string; Value: Boolean);
  protected
    procedure Update;
    function InternalActivate: Boolean; virtual;
    procedure InternalDeactivate; virtual;
    property CategoryVisible[Name: string]: Boolean read GetCategoryVisible write SetCategoryVisible;
    class function AgentBit: TAgentBit; virtual;
  public
    function Activate: Boolean;
    procedure Deactivate;
    property Active: Boolean read FActive;
    property ReadOnly: Boolean read FReadOnly write FReadOnly;
    property ID: Integer read FID write FID;
    property N: Integer read GetN;
    property ClassCode: Integer read FClassCode write FClassCode;
    property MaterialID: Integer read FMaterialID write FMaterialID;
  end;

  TFrameBuyInvoiceClass = class of TFrameBuyInvoice;

implementation

{$R *.dfm}

uses

  uBuy, dmBuy, dmBuyInvoice,
  uDialog, uGridPrinter;

{ TFrameBuyInvoice }

class function TFrameBuyInvoice.AgentBit: TAgentBit;
begin
  Result := abNone;
end;

function TFrameBuyInvoice.Activate: Boolean;
begin
  if not Active then
  begin
    FActive := InternalActivate;
  end;

  Result := Active;
end;

procedure TFrameBuyInvoice.Deactivate;
begin
  if Active then
  begin
    InternalDeactivate;

    FActive := False;
  end;
end;

function TFrameBuyInvoice.InternalActivate: Boolean;

procedure MenuProbePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
  MaterialID: Integer;
  Name: string;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlProbe, [0]);

  if DataSet.Active then
  begin
    DataSet.First;

    while not DataSet.Eof do
    begin
      MaterialID := DataSet.FieldByName('material$id').AsInteger;

      Name := DataSet.FieldByName('NAME').AsString;

      Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

      Button.ButtonStyle := bsDefault;

      Button.Tag := DataSet.FieldByName('ID').AsInteger;

      Button.Caption := Name;

      if MaterialID = ButtonProbeGold.Tag then
      begin
        Button.LargeImageIndex := ButtonProbeGold.LargeImageIndex;

        Button.OnClick := ButtonProbeClick;

        ButtonLink := MenuProbeGold.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Name = '1000' then
        begin
          ButtonLink.BeginGroup := True;
        end;

      end else

      if MaterialID = ButtonProbeSilver.Tag then
      begin
        Button.LargeImageIndex := ButtonProbeSilver.LargeImageIndex;

        Button.OnClick := ButtonProbeClick;

        ButtonLink := MenuProbeSilver.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Name = '1000' then
        begin
          ButtonLink.BeginGroup := True;
        end;

      end;

      DataSet.Next;
    end;

    DataSet.Free;
  end;
end;

procedure MenuMemoPopulate;
var
  i: Integer;
  DataSet: TClientDataSet;
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DropDownMenu: TdxBarPopupMenu;
begin
  exit;
  
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlMemo);

  if DataSet.Active then
  begin
    for i := 0 to MenuProbeGold.ItemLinks.Count - 1 do
    begin
      ButtonLink := MenuProbeGold.ItemLinks[i];

      Button := TdxBarLargeButton(ButtonLink.Item);

      Button.ButtonStyle := bsDropDown;

      DropDownMenu := TdxBarPopupMenu.Create(Button);

      DropDownMenu.Tag := Button.Tag;

      Button.DropDownMenu := DropDownMenu;

      DataSet.First;

      while not DataSet.Eof do
      begin
        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := DropDownMenu.Tag;

        Button.ButtonStyle := bsDefault;

        Button.LargeImageIndex := ButtonMemo.LargeImageIndex;

        Button.Tag := DataSet.FieldByName('ID').AsInteger;

        Button.Caption := DataSet.FieldByName('NAME').AsString;

        Button.OnClick := ButtonMemoClick;

        ButtonLink := DropDownMenu.ItemLinks.Add;

        ButtonLink.Item := Button;

        if DropDownMenu.ItemLinks.Count = 2 then
        begin
          ButtonLink.BeginGroup := True;
        end;

        DataSet.Next;
      end;
    end;

    DataSet.Free;
  end;
end;


procedure MenuOfficePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
  DepartmentID: Integer;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlOffice);

  DataSet.First;

  while not DataSet.Eof do
  begin
    DepartmentID := DataSet.FieldByName('ID').AsInteger;

    if DepartmentID <> DataBuy.SelfDepartmentID then
    begin
      Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

      Button.ButtonStyle := bsDefault;

      Button.LargeImageIndex := ButtonAgent.LargeImageIndex;

      Button.Tag := DepartmentID;

      Button.Caption := DataSet.FieldByName('NAME').AsString;

      Button.OnClick := ButtonAgentClick;

      ButtonLink := MenuOffice.ItemLinks.Add;

      ButtonLink.Item := Button;
    end;

    DataSet.Next;
  end;

  DataSet.Free;
end;

procedure MenuCompanyPopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  if AgentBit = abNone then
  begin
    exit;
  end;

  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlAgent, [Ord(AgentBit) - 1]);

  DataSet.First;

  while not DataSet.Eof do
  begin

    Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

    Button.ButtonStyle := bsDefault;

    Button.LargeImageIndex := ButtonAgent.LargeImageIndex;

    Button.Tag := DataSet.FieldByName('ID').AsInteger;

    Button.Caption := DataSet.FieldByName('NAME').AsString;

    Button.OnClick := ButtonAgentClick;

    ButtonLink := MenuCompany.ItemLinks.Add;

    ButtonLink.Item := Button;

    DataSet.Next;
  end;

  DataSet.Free;
end;

procedure SetCaption;
var
  sCaption: string;
  Sender: string;
  Receiver: string;
begin
  sCaption := InvoiceDepartmentName.AsString + '. ';

  case ClassCode of
    1,2:  sCaption := ' ����� � ';

    3,4:  sCaption := ' ����������� � ';

    5,6:  sCaption := ' ����������� � ';

    7,8:  sCaption := ' ������������ � ';

    9,10: sCaption := ' �������� � ';
  end;

  sCaption := sCaption + InvoiceN.AsString;

  sCaption := sCaption + ' �� ';

  sCaption := sCaption + DateToStr(DateOf(InvoiceINVOICEDATE.AsDateTime));

  Caption := sCaption;                    

  Sender := InvoiceSENDERNAME.AsString;

  Receiver := InvoiceRECEIVERNAME.AsString;

  sCaption := sCaption + #13#10 + '�����������: ' + Sender + #13#10 + '����������: ' + Receiver;

  GridInvoiceItem.ActiveLevel.Caption := sCaption;

  if InvoiceROUTE.AsInteger = -1 then
  begin
     LayoutGroupCompany.CaptionOptions.Text := '�����������';

     LayoutGroupAgent.CaptionOptions.Text := '����������';
  end else
  if InvoiceROUTE.AsInteger = 1 then
  begin
    LayoutGroupCompany.CaptionOptions.Text := '����������';

    LayoutGroupAgent.CaptionOptions.Text := '�����������';
  end

end;

begin
  Result := False;

  if ID <> 0 then
  begin
    try
     
      Invoice.Active := True;

      InvoiceItem.Active := True;

      StoredInvoiceDate := DateOf(InvoiceINVOICEDATE.AsDateTime);

      //FReadOnly := True;

      //FReadOnly := False;

      if ReadOnly then
      begin
        GridInvoiceItemView.OptionsData.Editing := False;

        GridInvoiceItemView.OptionsSelection.CellSelect := False;
      end else

      if not ReadOnly  then
      begin
        if ClassCode in [3,5,6,7,8,9,10] then
        begin
          CategoryVisible['Delete'] := True;

          if MaterialID = 1 then
          begin
            CategoryVisible['Gold'] := True;
          end else

          if MaterialID = 2 then
          begin
            CategoryVisible['Silver'] := True;
          end;
        end;

        LayoutItemAgentButton.Visible := True;

        GridInvoiceItemView.OptionsData.Editing := ClassCode in [3, 5, 6, 7, 8, 9, 10];

        GridInvoiceItemView.OptionsSelection.CellSelect := GridInvoiceItemView.OptionsData.Editing;

        GridStore.Visible := ClassCode in [3, 5, 7, 9];

        SplitterTop.Visible := GridStore.Visible;

        CalculateStore;

        if ClassCode = 3 then
        begin
          LayoutEditorAgentButton.Tag := InvoiceAGENTCLASSCODE.AsInteger;

          MenuOfficePopulate;

          MenuProbePopulate;

          MenuMemoPopulate;
        end else

        if ClassCode in [5, 6, 7, 8, 9, 10] then
        begin
          LayoutEditorAgentButton.Tag := InvoiceAGENTCLASSCODE.AsInteger;

          MenuCompanyPopulate;

          MenuProbePopulate;

          MenuMemoPopulate;
        end;

        LayoutGroupForeign.Enabled := True;

        if ClassCode in [3, 5, 6, 7, 8, 9, 10] then
        begin
          LayoutEditorDate.Enabled := True;

          LayoutEditorN.Enabled := True;
        end;

      end;

      SetCaption;

      Result := True;

    except

      on E: Exception do
      begin
        TDialog.Error(E.Message);
      end;

    end;

  end;
end;

procedure TFrameBuyInvoice.InternalDeactivate;
begin

end;

function TFrameBuyInvoice.GetN: Integer;
begin
  Result := 0;

  if not Invoice.IsEmpty then
  begin
    Result := InvoiceN.AsInteger;
  end;
end;

procedure TFrameBuyInvoice.InvoiceAfterOpen(DataSet: TDataSet);
var
  CostMode: Integer;
begin
  if LayoutGroupForeign.Tag <> 0 then
  begin
    CostMode := 0;

    if InvoiceFOREIGNCOST.AsFloat <> 0 then
    begin
      CostMode := 1;
    end else

    if InvoiceCOST.AsFloat = 0 then
    begin
      CostMode := 2;
    end;

    LayoutItemForeignCost.Visible := CostMode <> 0;

    LayoutGroupForeign.Visible := True;

    GridInvoiceItemView.Band['���������'].Visible := CostMode <> 1;
  end;
end;

procedure TFrameBuyInvoice.InvoiceBeforeOpen(DataSet: TDataSet);
begin
  Invoice.ParamByName('ID').AsInteger := ID;
end;

procedure TFrameBuyInvoice.InvoiceItemAfterPost(DataSet: TDataSet);
begin
  CalculateStore;
end;

procedure TFrameBuyInvoice.InvoiceItemBeforeEdit(DataSet: TDataSet);
begin
  StoredWeightReal := InvoiceItemWEIGHTREAL.AsCurrency;

  StoredWeight := InvoiceItemWEIGHT.AsCurrency;  
end;

procedure TFrameBuyInvoice.InvoiceItemBeforeOpen(DataSet: TDataSet);
begin
  InvoiceItem.ParamByName('INVOICE$ID').AsInteger := ID;
end;

procedure TFrameBuyInvoice.InvoiceItemBeforePost(DataSet: TDataSet);
var
  ID: Integer;
  Weight: Currency;
  WeightReal: Currency;
  Price: Currency;
  K: Double;
begin
  if InvoiceItem.State <> dsEdit then
  begin
    Exit;
  end;

  ID := InvoiceItemID.AsInteger;

  WeightReal := InvoiceItemWEIGHTREAL.AsCurrency;

  Weight := InvoiceItemWEIGHT.AsCurrency;

  if not GridInvoiceItemViewWEIGHT.Visible then
  begin
    Weight := InvoiceItemWEIGHTREAL.AsCurrency;
  end else
  begin
    if StoredWeightReal = 0 then
    begin
      Weight := WeightReal;
    end else
    begin
      K := WeightReal / StoredWeightReal;

      Weight := StoredWeight * K;
    end;
  end;

  Price := InvoiceItemPrice.AsCurrency;

  //with DataBuy, DataBuyInvoice do
  begin
    DataBuyInvoice.InvoiceItemUpdate.ParamByName('ID').AsInteger := ID;

    DataBuyInvoice.InvoiceItemUpdate.ParamByName('WEIGHT').AsCurrency := Weight;

    DataBuyInvoice.InvoiceItemUpdate.ParamByName('WEIGHT$REAL').AsCurrency := WeightReal;

    DataBuyInvoice.InvoiceItemUpdate.ParamByName('PRICE').AsCurrency := Price;

    DataBuy.Rollback;

    DataBuy.StartTransaction;

    try

      DataBuyInvoice.InvoiceItemUpdate.ExecProc;

      DataBuy.Commit;

    except

      on E: Exception do
      begin
        DataBuy.Rollback;

        TDialog.Error(E.Message);
      end;

    end;
  end;
end;

function TFrameBuyInvoice.IsReadOnly: Boolean;
begin
  Result := ReadOnly;
end;


procedure TFrameBuyInvoice.ActionAddGoldUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not IsReadOnly;

  if Enabled then
  begin
    Enabled := InvoiceItem.Active;
  end;

  ActionAddGold.Enabled := Enabled;
end;

procedure TFrameBuyInvoice.ActionAddSilverUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not IsReadOnly;

  if Enabled then
  begin
    Enabled := InvoiceItem.Active;
  end;

  ActionAddSilver.Enabled := Enabled;
end;

procedure TFrameBuyInvoice.ActionAgentExecute(Sender: TObject);
var
  Rect: TRect;
  X, Y: Integer;
  Button: TcxButton;
begin
  Update;

  Button := LayoutEditorAgentButton;

  GetWindowRect(Button.Handle, Rect);

  X := Rect.Left;

  Y := Rect.Bottom + 4;

  if Button.Tag = 1 then
  begin
    MenuOffice.Popup(X, Y);
  end else

  if Button.Tag = 2 then
  begin
    MenuCompany.Popup(X, Y);
  end;
end;

procedure TFrameBuyInvoice.ActionAgentUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not ReadOnly;

  ActionAgent.Enabled := Enabled;
end;

procedure TFrameBuyInvoice.ActionDeleteExecute(Sender: TObject);
var
  ID: Integer;
  ItemID: Integer;
begin
  ID := InvoiceID.AsInteger;

  ItemID := InvoiceItemID.AsInteger;

  //with DataBuy, DataBuyInvoice do
  begin
    DataBuyInvoice.InvoiceItemDelete.ParamByName('INVOICE$ID').AsInteger := ID;

    DataBuyInvoice.InvoiceItemDelete.ParamByName('ID').AsInteger := ItemID;

    DataBuy.Rollback;

    DataBuy.StartTransaction;

    try

      DataBuyInvoice.InvoiceItemDelete.ExecProc;

      DataBuy.Commit;

      InvoiceItem.Delete;

      CalculateStore;

    except

      on E: Exception do
      begin
        DataBuy.Rollback;

        TDialog.Error(E.Message);
      end;

    end;
  end;
end;

procedure TFrameBuyInvoice.ActionDeleteUpdate(Sender: TObject);
begin
  ActionDelete.Enabled := InvoiceItem.Active and not (IsReadOnly or InvoiceItem.IsEmpty);
end;

procedure TFrameBuyInvoice.ActionPrintExecute(Sender: TObject);
begin
  TGridPrinter.Print(GridInvoiceItem);
end;

procedure TFrameBuyInvoice.ActionAddGoldExecute(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonAddGold.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if MenuProbeGold.ItemLinks.VisibleItemCount <> 0 then
  begin
    MenuProbeGold.Popup(X, Y);
  end else

  begin
    TDialog.Information('��� ���������.');
  end;
end;

procedure TFrameBuyInvoice.ActionAddSilverExecute(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonAddSilver.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if MenuProbeSilver.ItemLinks.VisibleItemCount <> 0 then
  begin
    MenuProbeSilver.Popup(X, Y);
  end else

  begin
    TDialog.Information('��� ���������.');
  end;
end;


procedure TFrameBuyInvoice.ButtonProbeClick(Sender: TObject);
var
  ID: Integer;
  ProbeID: Integer;
  Button: TdxBarLargeButton;
  StoreDate: TDateTime;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    ID := InvoiceID.AsInteger;

    ProbeID := Button.Tag;

    StoreDate := InvoiceINVOICEDATE.AsDateTime;

    //with DataBuy, DataBuyInvoice do
    begin
      DataBuyInvoice.InvoiceItemInsert.ParamByName('INVOICE$ID').AsInteger := ID;

      DataBuyInvoice.InvoiceItemInsert.ParamByName('PROBE$ID').AsInteger := ProbeID;

      DataBuyInvoice.InvoiceItemInsert.ParamByName('STORE$DATE').AsDateTime := StoreDate;

      DataBuy.Rollback;

      DataBuy.StartTransaction;

      try

        DataBuyInvoice.InvoiceItemInsert.ExecProc;

        DataBuy.Commit;

        ID := DataBuyInvoice.InvoiceItemInsert.ParamByName('ID').AsInteger;

        if ID <> 0 then
        begin
          InvoiceItem.Append;

          InvoiceItemID.AsInteger := ID;

          InvoiceItem.Post;

        end else

        begin

          InvoiceItem.Active := False;

          InvoiceItem.Active := True;

          CalculateStore;
        end;

      except

        on E: Exception do
        begin

          if InvoiceItem.State <> dsBrowse then
          begin
            InvoiceItem.Cancel;
          end;

          DataBuy.Rollback;

          TDialog.Error(E.Message);
        end;

      end;
    end;
  end;
end;

procedure TFrameBuyInvoice.ButtonMemoClick(Sender: TObject);
var
  ID: Integer;
  MemoID: Integer;
  ProbeID: Integer;
  Button: TdxBarLargeButton;
  StoreDate: TDateTime;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    MemoID := Button.Tag;

    begin

      ProbeID := Button.GroupIndex;

      ID := InvoiceID.AsInteger;

      StoreDate := InvoiceINVOICEDATE.AsDateTime;

      //with DataBuy, DataBuyInvoice do
      begin
        DataBuyInvoice.InvoiceItemInsert.ParamByName('INVOICE$ID').AsInteger := ID;

        DataBuyInvoice.InvoiceItemInsert.ParamByName('PROBE$ID').AsInteger := ProbeID;

        DataBuyInvoice.InvoiceItemInsert.ParamByName('MEMO$ID').AsInteger := MemoID;

        DataBuyInvoice.InvoiceItemInsert.ParamByName('STORE$DATE').AsDateTime := StoreDate;

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        try

          DataBuyInvoice.InvoiceItemInsert.ExecProc;

          ID := DataBuyInvoice.InvoiceItemInsert.ParamByName('ID').AsInteger;

          if ID <> 0 then
          begin
            InvoiceItem.Append;

            InvoiceItemID.AsInteger := ID;

            InvoiceItem.Post;

            DataBuy.Commit;

          end else

          begin
            DataBuy.Commit;

            InvoiceItem.Active := False;

            InvoiceItem.Active := True;

            CalculateStore;
          end;

        except

          on E: Exception do
          begin

            if InvoiceItem.State <> dsBrowse then
            begin
              InvoiceItem.Cancel;
            end;

            DataBuy.Rollback;

            TDialog.Error(E.Message);
          end;

        end;

      end;
    end;
  end;
end;

procedure TFrameBuyInvoice.Update;
var
  InvoiceDate: TDate;

procedure GenericPostEditorValue;
var
  AControl: TWinControl;
  ActiveEditor: TcxCustomEdit;
begin
  ActiveEditor := nil;

  AControl := Screen.ActiveControl;

  if AControl.Owner is TcxCustomEdit then
  begin
    ActiveEditor := TcxCustomEdit(AControl.Owner);
  end else

  if AControl is TcxCustomEdit then
  begin
    ActiveEditor := TcxCustomEdit(AControl);
  end;

  if ActiveEditor <> nil  then
  begin
    if ActiveEditor.EditModified then
    begin
      ActiveEditor.PostEditValue;
    end;
  end;

end;

begin
  GenericPostEditorValue;

  if Invoice.State = dsEdit then
  begin
    DataBuyInvoice.InvoiceUpdate.ParamByName('ID').AsInteger := ID;

    DataBuyInvoice.InvoiceUpdate.ParamByName('N').AsVariant := InvoiceN.AsVariant;

    DataBuyInvoice.InvoiceUpdate.ParamByName('INVOICE$DATE').AsVariant := InvoiceINVOICEDATE.AsVariant;    

    DataBuyInvoice.InvoiceUpdate.ParamByName('AGENT$ID').AsVariant := InvoiceAGENTID.AsVariant;

    DataBuyInvoice.InvoiceUpdate.ParamByName('FOREIGN$N').AsVariant := InvoiceFOREIGNN.AsVariant;

    DataBuyInvoice.InvoiceUpdate.ParamByName('FOREIGN$DATE').AsVariant := InvoiceFOREIGNDATE.AsVariant;

    DataBuyInvoice.InvoiceUpdate.ParamByName('FOREIGN$COST').AsVariant := InvoiceFOREIGNCOST.AsVariant;

    DataBuy.Rollback;

    DataBuy.StartTransaction;

    try

      DataBuyInvoice.InvoiceUpdate.ExecProc;

      DataBuy.Commit;

      Invoice.Post;      

      InvoiceDate := DateOf(InvoiceINVOICEDATE.AsDateTime);

      if InvoiceDate <> StoredInvoiceDate then
      begin
        StoredInvoiceDate := InvoiceDate;

        CurrentStore.Active := False;

        CalculateStore;
      end;

      if Parent is TcxTabSheet then
      begin
        TcxTabSheet(Parent).Caption := InvoiceN.AsString;
      end;

    except

      on E: Exception do
      begin

        if Invoice.State <> dsBrowse then
        begin
          Invoice.Cancel;
        end;

        DataBuy.Rollback;

        TDialog.Error(E.Message);
      end;

    end;
  end;

  if InvoiceItem.State = dsEdit then
  begin
    InvoiceItem.Post;
  end;
end;


procedure TFrameBuyInvoice.ButtonAgentClick(Sender: TObject);
var
  AgentID: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    AgentID := Button.Tag;

    Invoice.Edit;

    InvoiceAGENTID.AsInteger := AgentID;

    Update;
  end;
end;

procedure TFrameBuyInvoice.CalculateStore;
var
  Invoice: TClientDataSet;
  MaterialID: Integer;
  ProbeID: Integer;
  Weight: Currency;
  WeightReal: Currency;
  Weight1000: Currency;
  StoreDate: TDateTime;
  Flag: Boolean;
begin
  if not GridStore.Visible then
  begin
    Exit;
  end;

  Flag := False;

  if not CurrentStore.Active then
  begin
    Flag := True;

    StoreDate := DateOf(InvoiceINVOICEDATE.AsDateTime);

    CurrentStore.Data := DataBuy.Table(sqlStore, [StoreDate]);

    CurrentStore.IndexFieldNames := 'material$ID; probe$id;';
  end;

  Invoice := TClientDataSet.Create(Self);

  Invoice.Data := DataBuy.Table('select * from buy$invoice$item$a(:invoice$id)', [ID]);

  Invoice.First;

  Store.IndexFieldNames := 'material$ID; probe$id;';

  Store.DisableControls;

  Store.Filtered := False;

  if Flag then
  begin

  Invoice.First;

  while not Invoice.Eof do
  begin
    MaterialID := Invoice.FieldByName('Material$ID').AsInteger;

    ProbeID := Invoice.FieldByName('Probe$ID').AsInteger;

    Weight := Invoice.FieldByName('Weight').AsCurrency;

    WeightReal := Invoice.FieldByName('Weight$Real').AsCurrency;

    Weight1000 := Invoice.FieldByName('Weight$1000').AsCurrency;

    if CurrentStore.FindKey([MaterialID, ProbeID]) then
    begin
      CurrentStore.Edit;

      CurrentStore.FieldByName('Weight').AsCurrency := CurrentStore.FieldByName('Weight').AsCurrency + Weight;

      CurrentStore.FieldByName('Weight$Real').AsCurrency := CurrentStore.FieldByName('Weight$Real').AsCurrency + WeightReal;

      CurrentStore.FieldByName('Weight$1000').AsCurrency := CurrentStore.FieldByName('Weight$1000').AsCurrency + Weight1000;

    end else
    begin
      CurrentStore.Append;

      CurrentStore.FieldByName('Material$ID').AsInteger := MaterialID;

      CurrentStore.FieldByName('Probe$ID').AsInteger := ProbeID;

      CurrentStore.FieldByName('Weight').AsCurrency := Weight;

      CurrentStore.FieldByName('Weight$Real').AsCurrency := WeightReal;

      CurrentStore.FieldByName('Weight$1000').AsCurrency := Weight1000;
    end;

    CurrentStore.Post;

    Invoice.Next;
  end;


  end;

  Store.Data := CurrentStore.Data;

  TFloatField(Store.FieldByName('Weight')).DisplayFormat := '0.000';

  TFloatField(Store.FieldByName('Weight$Real')).DisplayFormat := '0.000';

  TFloatField(Store.FieldByName('Weight$1000')).DisplayFormat := '0.000';

  Invoice.First;

  while not Invoice.Eof do
  begin
    MaterialID := Invoice.FieldByName('Material$ID').AsInteger;

    ProbeID := Invoice.FieldByName('Probe$ID').AsInteger;

    Weight := Invoice.FieldByName('Weight').AsCurrency;

    WeightReal := Invoice.FieldByName('Weight$Real').AsCurrency;

    Weight1000 := Invoice.FieldByName('Weight$1000').AsCurrency;

    if Store.FindKey([MaterialID, ProbeID]) then
    begin
      Store.Edit;

      Store.FieldByName('Weight').AsCurrency := Store.FieldByName('Weight').AsCurrency - Weight;

      Store.FieldByName('Weight$Real').AsCurrency := Store.FieldByName('Weight$Real').AsCurrency - WeightReal;

      Store.FieldByName('Weight$1000').AsCurrency := Store.FieldByName('Weight$1000').AsCurrency - Weight1000;

    end else
    begin
      Store.Append;

      Store.FieldByName('Material$ID').AsInteger := MaterialID;

      Store.FieldByName('Probe$ID').AsInteger := ProbeID;

      Store.FieldByName('Weight').AsCurrency := -Weight;

      Store.FieldByName('Weight$Real').AsCurrency := -WeightReal;

      Store.FieldByName('Weight$1000').AsCurrency := -Weight1000;
    end;

    Store.Post;

    Invoice.Next;
  end;

  Store.SetRange([Self.MaterialID], [Self.MaterialID]);

  Store.Filter := 'Weight <> 0';

  Store.Filtered := True;

  Store.EnableControls;

  Invoice.Free;
end;

procedure TFrameBuyInvoice.LargeButtonCloseClick(Sender: TObject);
var
  Sheet: TcxTabSheet;
begin
  Update;

  if Owner is TcxTabSheet then
  begin
    Sheet := TcxTabSheet(Owner);

    Sheet.Free;
  end;
end;

procedure TFrameBuyInvoice.OnUpdate(Sender: TObject);
begin
  Update;
end;

procedure TFrameBuyInvoice.SetCategoryVisible(Name: string; Value: Boolean);
var
  CategoryIndex: Integer;
begin
  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin
    if Value then
    begin
      BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways;
    end else
    begin
      BarManager.CategoryItemsVisible[CategoryIndex] := ivNever;
    end;
  end;
end;

function TFrameBuyInvoice.GetCategoryVisible(Name: string): Boolean;
var
  CategoryIndex: Integer;
begin
  Result := False;

  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin
    Result := BarManager.CategoryItemsVisible[CategoryIndex] = ivAlways;
  end;
end;


end.



