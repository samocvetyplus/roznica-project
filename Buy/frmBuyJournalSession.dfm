inherited FrameBuyJournalSession: TFrameBuyJournalSession
  inherited GroupBoxJournal: TcxGroupBox
    inherited GridJournal: TcxGrid
      inherited GridJournalView: TcxGridDBBandedTableView
        Bands = <
          item
            Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          end
          item
            Caption = #1042#1085#1077#1096#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
            Visible = False
            Width = 207
          end
          item
            Caption = #1042#1077#1089
            Options.HoldOwnColumnsOnly = True
          end
          item
            Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Options.HoldOwnColumnsOnly = True
          end>
        inherited GridJournalViewN: TcxGridDBBandedColumn
          SortIndex = 2
          SortOrder = soAscending
        end
        inherited GridJournalViewRECEIVERNAME: TcxGridDBBandedColumn
          SortIndex = 1
          SortOrder = soAscending
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      0
      0
      0
      0
      0
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited ButtonOffice: TdxBarLargeButton
      ImageIndex = -1
    end
    object ButtonPrint101: TdxBarLargeButton [14]
      Tag = 101
      Caption = #1055#1088#1080#1093#1086#1076#1085#1099#1081' '#1086#1088#1076#1077#1088
      Category = 6
      Hint = #1055#1088#1080#1093#1086#1076#1085#1099#1081' '#1086#1088#1076#1077#1088
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = ButtonPrintClick
    end
    inherited ButtonSend: TdxBarLargeButton
      ImageIndex = 17
    end
    inherited ButtonReceive: TdxBarLargeButton
      Down = True
    end
  end
  inherited MenuPrint: TdxBarPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonPrint101'
      end>
  end
end
