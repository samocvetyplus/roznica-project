inherited FrameBuyInvoiceSession: TFrameBuyInvoiceSession
  inherited GroupBoxHeader: TcxGroupBox
    ExplicitHeight = 304
    inherited Layout: TdxLayoutControl
      ExplicitHeight = 300
      inherited LayoutEditorN: TcxDBTextEdit
        ExplicitWidth = 101
        Width = 101
      end
      inherited LayoutEditorState: TcxDBTextEdit
        ExplicitWidth = 68
        Width = 68
      end
      inherited LayoutEditorStateDate: TcxDBTextEdit
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorStatePerson: TcxDBTextEdit
        ExplicitWidth = 229
        Width = 229
      end
      inherited LayoutEditorDepartment: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorAgent: TcxDBTextEdit
        ExplicitWidth = 187
        Width = 187
      end
      inherited LayoutEditorCompany: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorForeignN: TcxDBTextEdit
        ExplicitWidth = 75
        Width = 75
      end
      inherited LayoutEditorForeignDate: TcxDBDateEdit
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorForeignCost: TcxDBTextEdit
        ExplicitWidth = 100
        Width = 100
      end
      inherited LayoutEditorDate: TcxDBDateEdit
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutGroupRoot: TdxLayoutGroup
        inherited LayoutGroup5: TdxLayoutGroup
          inherited LayoutGroup6: TdxLayoutGroup
            inherited LayoutGroupAgent: TdxLayoutGroup
              Visible = False
            end
          end
        end
      end
    end
  end
  inherited GroupBoxContent: TcxGroupBox
    ExplicitWidth = 74
    ExplicitHeight = 304
    inherited PageContent: TcxPageControl
      inherited SheetContent: TcxTabSheet
        inherited GridInvoiceItem: TcxGrid
          Height = 233
          ExplicitHeight = 233
          inherited GridInvoiceItemView: TcxGridDBBandedTableView
            Bands = <
              item
                Caption = #1052#1072#1090#1077#1088#1080#1072#1083
              end
              item
                Caption = #1042#1077#1089
              end
              item
                Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
              end
              item
                Caption = #1044#1077#1081#1089#1090#1074#1080#1077
              end>
            object GridInvoiceItemViewACTIONNAME: TcxGridDBBandedColumn
              Caption = '*'
              DataBinding.FieldName = 'ACTION$NAME'
              HeaderAlignmentHorz = taCenter
              Width = 100
              Position.BandIndex = 3
              Position.ColIndex = 0
              Position.RowIndex = 0
            end
          end
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited LargeButtonClose: TdxBarLargeButton
      ImageIndex = 9
    end
  end
  inherited InvoiceItem: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    DeleteSQL.Strings = (
      '')
    InsertSQL.Strings = (
      '')
    RefreshSQL.Strings = ()
    SelectSQL.Strings = (
      'select'
      '  '
      '  id,'
      '  invoice$id,'
      '  material$id,'
      '  material$name,'
      '  probe$id,'
      '  probe$name,'
      '  probe$k,'
      '  memo$id,'
      '  memo$name,'
      '  weight,'
      '  weight$real,'
      '  weight$1000,'
      '  price,'
      '  cost,'
      '  action$code,'
      '  action$name'
      ''
      'from'
      '  buy$invoice$item$a2(:invoice$id)')
    inherited InvoiceItemPROBEK: TFIBFloatField [6]
    end
    inherited InvoiceItemMEMOID: TFIBIntegerField [7]
    end
    inherited InvoiceItemMEMONAME: TFIBStringField [8]
    end
    inherited InvoiceItemWEIGHT: TFIBBCDField [9]
    end
    inherited InvoiceItemWEIGHTREAL: TFIBBCDField [10]
    end
    inherited InvoiceItemWEIGHT1000: TFIBFloatField [11]
    end
    inherited InvoiceItemPRICE: TFIBBCDField [12]
    end
    inherited InvoiceItemCOST: TFIBBCDField [13]
    end
    object InvoiceItemACTIONCODE: TFIBIntegerField
      FieldName = 'ACTION$CODE'
    end
    object InvoiceItemACTIONNAME: TFIBStringField
      FieldName = 'ACTION$NAME'
      Size = 8
      EmptyStrToNull = True
    end
  end
  inherited LayoutLookAndFeel: TdxLayoutLookAndFeelList
    Left = 48
    Top = 480
  end
end
