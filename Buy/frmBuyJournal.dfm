object FrameBuyJournal: TFrameBuyJournal
  Left = 0
  Top = 0
  Width = 918
  Height = 548
  Align = alClient
  TabOrder = 0
  ExplicitWidth = 451
  ExplicitHeight = 304
  object GroupBoxJournal: TcxGroupBox
    Left = 0
    Top = 0
    Align = alClient
    PanelStyle.Active = True
    TabOrder = 4
    ExplicitWidth = 451
    ExplicitHeight = 304
    Height = 548
    Width = 918
    object BarDock: TdxBarDockControl
      AlignWithMargins = True
      Left = 6
      Top = 6
      Width = 906
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 0
      Align = dalTop
      BarManager = BarManager
      ExplicitWidth = 439
    end
    object BarFilterDock: TdxBarDockControl
      AlignWithMargins = True
      Left = 6
      Top = 62
      Width = 906
      Height = 38
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 0
      Align = dalTop
      BarManager = BarManager
      ExplicitWidth = 439
    end
    object Splitter: TcxSplitter
      Left = 2
      Top = 193
      Width = 914
      Height = 8
      HotZoneClassName = 'TcxXPTaskBarStyle'
      AlignSplitter = salTop
      MinSize = 93
      ResizeUpdate = True
      Control = GroupBoxNote
      OnBeforeOpen = SplitterBeforeOpen
      OnBeforeClose = SplitterBeforeClose
      ExplicitWidth = 447
    end
    object cxGroupBox1: TcxGroupBox
      Left = 280
      Top = 336
      Caption = 'cxGroupBox1'
      TabOrder = 3
      Height = 105
      Width = 345
    end
    object GridJournal: TcxGrid
      AlignWithMargins = True
      Left = 6
      Top = 201
      Width = 906
      Height = 341
      Margins.Left = 4
      Margins.Top = 0
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelEdges = [beTop]
      BevelKind = bkFlat
      BorderStyle = cxcbsNone
      TabOrder = 4
      ExplicitWidth = 439
      ExplicitHeight = 97
      object GridJournalView: TcxGridDBBandedTableView
        OnKeyDown = GridJournalViewKeyDown
        NavigatorButtons.ConfirmDelete = False
        OnCellDblClick = GridJournalViewCellDblClick
        DataController.DataSource = SourceJournal
        DataController.KeyFieldNames = 'ID'
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = '0.000'
            Kind = skSum
            Position = spFooter
            FieldName = 'WEIGHT'
            Column = GridJournalViewWEIGHT
          end
          item
            Format = '0.000'
            Kind = skSum
            Position = spFooter
            FieldName = 'WEIGHT$REAL'
            Column = GridJournalViewWEIGHTREAL
          end
          item
            Format = '0.000'
            Kind = skSum
            Position = spFooter
            FieldName = 'WEIGHT$1000'
            Column = GridJournalViewWEIGHT1000
          end
          item
            Format = '0.00'
            Kind = skSum
            Position = spFooter
            FieldName = 'COST$TOTAL'
            Column = GridJournalViewCOSTTOTAL
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT'
            Column = GridJournalViewWEIGHT
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT$REAL'
            Column = GridJournalViewWEIGHTREAL
          end
          item
            Format = '0.00'
            Kind = skSum
            FieldName = 'COST$TOTAL'
            Column = GridJournalViewCOSTTOTAL
          end
          item
            Format = '0.000'
            Kind = skSum
            FieldName = 'WEIGHT$1000'
            Column = GridJournalViewWEIGHT1000
          end>
        DataController.Summary.SummaryGroups = <>
        DataController.OnGroupingChanged = GridJournalViewDataControllerGroupingChanged
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.BandMoving = False
        OptionsCustomize.ColumnVertSizing = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.FocusRect = False
        OptionsView.NoDataToDisplayInfoText = '...'
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfAlwaysVisible
        OptionsView.GroupSummaryLayout = gslAlignWithColumns
        OptionsView.Indicator = True
        OptionsView.BandHeaderHeight = 32
        Styles.OnGetContentStyle = GridJournalViewStylesGetContentStyle
        Styles.BandHeader = DataBuy.StyleBand
        Bands = <
          item
            Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          end
          item
            Caption = #1042#1085#1077#1096#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
            Width = 207
          end
          item
            Caption = #1042#1077#1089
            Options.HoldOwnColumnsOnly = True
          end
          item
            Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Options.HoldOwnColumnsOnly = True
          end>
        OnCustomDrawBandHeader = GridJournalViewCustomDrawBandHeader
        object GridJournalViewN: TcxGridDBBandedColumn
          Caption = #8470
          DataBinding.FieldName = 'N'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Width = 75
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridJournalViewINVOICEDATE: TcxGridDBBandedColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'INVOICE$DATE'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 85
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridJournalViewMATERIALNAME: TcxGridDBBandedColumn
          Caption = #1052#1072#1090#1077#1088#1080#1072#1083
          DataBinding.FieldName = 'MATERIAL$NAME'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 91
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object GridJournalViewSENDERNAME: TcxGridDBBandedColumn
          Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          DataBinding.FieldName = 'SENDER$NAME'
          HeaderAlignmentHorz = taCenter
          Width = 175
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object GridJournalViewRECEIVERNAME: TcxGridDBBandedColumn
          Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
          DataBinding.FieldName = 'RECEIVER$NAME'
          HeaderAlignmentHorz = taCenter
          Width = 175
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object GridJournalViewCOSTTOTAL: TcxGridDBBandedColumn
          Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
          DataBinding.FieldName = 'COST$TOTAL'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Width = 104
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object GridJournalViewWEIGHT: TcxGridDBBandedColumn
          Caption = #1063#1080#1089#1090#1099#1081
          DataBinding.FieldName = 'WEIGHT'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Width = 85
          Position.BandIndex = 2
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridJournalViewWEIGHTREAL: TcxGridDBBandedColumn
          Caption = #1043#1088#1103#1079#1085#1099#1081
          DataBinding.FieldName = 'WEIGHT$REAL'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Width = 85
          Position.BandIndex = 2
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridJournalViewWEIGHT1000: TcxGridDBBandedColumn
          Caption = '1000'
          DataBinding.FieldName = 'WEIGHT$1000'
          HeaderAlignmentHorz = taCenter
          Options.Grouping = False
          Width = 85
          Position.BandIndex = 2
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object GridJournalViewSTATECODE: TcxGridDBBandedColumn
          DataBinding.FieldName = 'STATE$CODE'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object GridJournalViewSTATENAME: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
          DataBinding.FieldName = 'STATE$NAME'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Width = 98
          Position.BandIndex = 3
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridJournalViewSTATEDATE: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'STATE$DATE'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Options.Grouping = False
          Width = 111
          Position.BandIndex = 3
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridJournalViewSTATEBYNAME: TcxGridDBBandedColumn
          Tag = 1
          Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
          DataBinding.FieldName = 'STATE$BY$NAME'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Width = 138
          Position.BandIndex = 3
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object GridJournalViewFOREIGNN: TcxGridDBBandedColumn
          Caption = #8470
          DataBinding.FieldName = 'FOREIGN$N'
          HeaderAlignmentHorz = taCenter
          Width = 83
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object GridJournalViewFOREIGNDATE: TcxGridDBBandedColumn
          Caption = #1044#1072#1090#1072
          DataBinding.FieldName = 'FOREIGN$DATE'
          HeaderAlignmentHorz = taCenter
          Width = 111
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridJournalViewCLASSNAME: TcxGridDBBandedColumn
          Caption = #1044#1074#1080#1078#1077#1085#1080#1077
          DataBinding.FieldName = 'CLASS$NAME'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object GridJournalLevel: TcxGridLevel
        GridView = GridJournalView
      end
    end
    object GroupBoxNote: TcxGroupBox
      AlignWithMargins = True
      Left = 6
      Top = 104
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 0
      Align = alTop
      PanelStyle.Active = True
      TabOrder = 5
      ExplicitWidth = 439
      Height = 89
      Width = 906
      object DockControlNote: TdxBarDockControl
        Left = 862
        Top = 2
        Width = 42
        Height = 85
        Align = dalRight
        BarManager = BarManager
        ExplicitLeft = 395
      end
      object EditorNote: TcxMemo
        AlignWithMargins = True
        Left = 2
        Top = 2
        Margins.Left = 0
        Margins.Top = 0
        Margins.Right = 4
        Margins.Bottom = 0
        Align = alClient
        Enabled = False
        Properties.MaxLength = 1024
        Properties.ReadOnly = True
        Properties.ScrollBars = ssVertical
        Properties.WordWrap = False
        Style.BorderStyle = ebsNone
        Style.Color = clBtnFace
        Style.Edges = []
        Style.Shadow = False
        TabOrder = 1
        OnExit = EditorNoteExit
        ExplicitWidth = 389
        Height = 85
        Width = 856
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Office'
      'Common'
      'Open + Close'
      'Add'
      'Delete'
      'Print'
      'Invoice'
      'Range'
      'Gold'
      'Silver'
      'Send'
      'Receive'
      'Note')
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      0
      0
      0
      0
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    ImageOptions.Images = DataBuy.Image16
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 8
    Top = 152
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonMaterial'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonSend'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReceive'
        end
        item
          Visible = True
          ItemName = 'ButtonOpen'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonInsert'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonNote'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonUnion'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarFilter: TdxBar
      BorderStyle = bbsNone
      Caption = 'Filter'
      CaptionButtons = <>
      DockControl = BarFilterDock
      DockedDockControl = BarFilterDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 974
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOffice'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonRange'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarNote: TdxBar
      BorderStyle = bbsNone
      Caption = 'Note'
      CaptionButtons = <>
      DockControl = DockControlNote
      DockedDockControl = DockControlNote
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 825
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonNoteOk'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonNoteCancel'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object ButtonOffice: TdxBarLargeButton
      Caption = #1057#1082#1083#1072#1076
      Category = 0
      Hint = #1057#1082#1083#1072#1076
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 20
      Down = True
      DropDownMenu = MenuOffice
      LargeImageIndex = 9
      OnClick = ButtonOfficeClick
      AutoGrayScale = False
      GlyphLayout = glLeft
      SyncImageIndex = False
      ImageIndex = -1
    end
    object ButtonRange: TdxBarLargeButton
      Action = ActionRange
      Align = iaClient
      Category = 1
      ButtonStyle = bsChecked
      GroupIndex = 10
      Down = True
      LargeImageIndex = 30
      ShortCut = 16464
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object ButtonUnion: TdxBarLargeButton
      Action = ActionUnion
      Category = 1
      AutoGrayScale = False
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Align = iaRight
      Category = 1
      ShortCut = 16466
      AutoGrayScale = False
      Width = 64
    end
    object ButtonMaterial: TdxBarLargeButton
      Tag = 1
      Caption = #1047#1086#1083#1086#1090#1086
      Category = 1
      Hint = #1047#1086#1083#1086#1090#1086
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 50
      Down = True
      LargeImageIndex = 0
      OnClick = ButtonMaterialClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonNote: TdxBarLargeButton
      Caption = #1047#1072#1084#1077#1090#1082#1080
      Category = 1
      Hint = #1047#1072#1084#1077#1090#1082#1080
      Visible = ivAlways
      AllowAllUp = True
      ButtonStyle = bsChecked
      GroupIndex = 70
      LargeImageIndex = 31
      OnClick = ButtonNoteClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonOpen: TdxBarLargeButton
      Action = ActionOpen
      Category = 2
      ShortCut = 16463
      AutoGrayScale = False
      Width = 64
    end
    object ButtonClose: TdxBarLargeButton
      Action = ActionClose
      Category = 2
      ShortCut = 16451
      AutoGrayScale = False
      Width = 64
    end
    object ButtonInsert: TdxBarLargeButton
      Action = ActionAdd
      Category = 3
      ShortCut = 16449
      AutoGrayScale = False
      Width = 64
    end
    object ButtonDelete: TdxBarLargeButton
      Action = ActionDelete
      Category = 4
      ShortCut = 16452
      AutoGrayScale = False
      Width = 64
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 5
      Visible = ivAlways
      LargeImageIndex = 6
      ShortCut = 16464
      OnClick = ButtonPrintClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonInvoice: TdxBarLargeButton
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Category = 6
      Hint = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Visible = ivAlways
      LargeImageIndex = 0
      OnClick = ButtonInvoiceClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonRangeCustom: TdxBarButton
      Caption = 'Custom'
      Category = 7
      Hint = 'Custom'
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object ButtonRangeDay: TdxBarButton
      Caption = 'Day'
      Category = 7
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonRangeWeek: TdxBarButton
      Caption = 'Week'
      Category = 7
      Hint = 'Week'
      Visible = ivAlways
      LargeImageIndex = 13
    end
    object ButtonRangeMonth: TdxBarButton
      Caption = 'Month'
      Category = 7
      Hint = 'Month'
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object ButtonRangeYear: TdxBarButton
      Caption = 'Year'
      Category = 7
      Hint = 'Year'
      Visible = ivAlways
      LargeImageIndex = 15
    end
    object ButtonGold: TdxBarLargeButton
      Action = ActionGold
      Category = 8
      ButtonStyle = bsChecked
      GroupIndex = 30
      Down = True
      AutoGrayScale = False
      Width = 64
    end
    object ButtonSilver: TdxBarLargeButton
      Action = ActionSilver
      Category = 9
      ButtonStyle = bsChecked
      GroupIndex = 30
      AutoGrayScale = False
      Width = 64
    end
    object ButtonSend: TdxBarLargeButton
      Action = ActionSend
      Category = 10
      ButtonStyle = bsChecked
      GroupIndex = 1
      AutoGrayScale = False
      Width = 64
      SyncImageIndex = False
      ImageIndex = 17
    end
    object ButtonReceive: TdxBarLargeButton
      Action = ActionReceive
      Category = 11
      ButtonStyle = bsChecked
      GroupIndex = 1
      AutoGrayScale = False
      Width = 64
    end
    object ButtonNoteOk: TdxBarLargeButton
      Action = ActionNoteOK
      Category = 12
      AutoGrayScale = False
      ShowCaption = False
    end
    object ButtonNoteCancel: TdxBarLargeButton
      Action = ActionNoteCancel
      Category = 12
      AutoGrayScale = False
      ShowCaption = False
    end
  end
  object Action: TActionList
    Images = DataBuy.Image32
    Left = 8
    Top = 120
    object ActionAdd: TAction
      Tag = 3
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = ActionAddExecute
      OnUpdate = ActionAddUpdate
    end
    object ActionDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionDeleteExecute
      OnUpdate = ActionDeleteUpdate
    end
    object ActionSend: TAction
      Tag = 1
      Caption = #1055#1077#1088#1077#1076#1072#1095#1072
      ImageIndex = 0
      OnExecute = OnActionRouteExecute
    end
    object ActionReceive: TAction
      Tag = 2
      Caption = #1055#1086#1083#1091#1095#1077#1085#1080#1077
      ImageIndex = 0
      OnExecute = OnActionRouteExecute
    end
    object ActionOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 3
      OnExecute = ActionOpenExecute
      OnUpdate = ActionOpenUpdate
    end
    object ActionClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 4
      OnExecute = ActionCloseExecute
      OnUpdate = ActionCloseUpdate
    end
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object ActionRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = ActionRangeExecute
      OnUpdate = ActionRangeUpdate
    end
    object ActionUnion: TAction
      Caption = #1040#1085#1072#1083#1080#1079
      ImageIndex = 23
      OnExecute = ActionUnionExecute
      OnUpdate = ActionUnionUpdate
    end
    object ActionGold: TAction
      Tag = 1
      Caption = #1047#1086#1083#1086#1090#1086
      ImageIndex = 0
      OnExecute = ActionMaterialExecute
    end
    object ActionSilver: TAction
      Tag = 2
      Caption = #1057#1077#1088#1077#1073#1088#1086
      ImageIndex = 0
      OnExecute = ActionMaterialExecute
    end
    object ActionNoteOK: TAction
      Caption = 'OK'
      ImageIndex = 31
      OnExecute = ActionNoteOKExecute
      OnUpdate = ActionNoteUpdate
    end
    object ActionNoteCancel: TAction
      Caption = #1054#1090#1084#1077#1085#1072
      ImageIndex = 33
      OnExecute = ActionNoteCancelExecute
      OnUpdate = ActionNoteUpdate
    end
    object ActionPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 6
      OnUpdate = ActionPrintUpdate
    end
  end
  object InvoiceArray: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure buy$stub')
    DeleteSQL.Strings = (
      'execute procedure buy$stub')
    InsertSQL.Strings = (
      'execute procedure buy$stub')
    RefreshSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost '
      ''
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' ) i'
      '')
    SelectSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost '
      ''
      'from'
      ''
      ' buy$invoice$a'
      ' ('
      '  :department$id,'
      '  :class$code,'
      '  :range$begin,'
      '  :range$end'
      ' ) i'
      ''
      ' where i.material$id = :material$id'
      '')
    AfterOpen = InvoiceArrayAfterOpen
    BeforeOpen = InvoiceArrayBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    Left = 8
    Top = 184
    oStartTransaction = False
    oFetchAll = True
    object InvoiceArrayID: TFIBIntegerField
      FieldName = 'ID'
    end
    object InvoiceArrayREFERENCEID: TFIBIntegerField
      FieldName = 'REFERENCE$ID'
    end
    object InvoiceArrayFORWARDID: TFIBIntegerField
      FieldName = 'FORWARD$ID'
    end
    object InvoiceArrayBACKWARDID: TFIBIntegerField
      FieldName = 'BACKWARD$ID'
    end
    object InvoiceArrayCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object InvoiceArrayCOMPANYNAME: TFIBStringField
      FieldName = 'COMPANY$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceArrayDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object InvoiceArrayDEPARTMENTNAME: TFIBStringField
      FieldName = 'DEPARTMENT$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceArrayCLASSCODE: TFIBIntegerField
      FieldName = 'CLASS$CODE'
    end
    object InvoiceArrayCLASSNAME: TFIBStringField
      FieldName = 'CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceArrayROUTE: TFIBIntegerField
      FieldName = 'ROUTE'
    end
    object InvoiceArrayN: TFIBIntegerField
      FieldName = 'N'
    end
    object InvoiceArrayINVOICEDATE: TFIBDateField
      FieldName = 'INVOICE$DATE'
    end
    object InvoiceArraySENDERID: TFIBIntegerField
      FieldName = 'SENDER$ID'
    end
    object InvoiceArraySENDERNAME: TFIBStringField
      FieldName = 'SENDER$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceArrayRECEIVERID: TFIBIntegerField
      FieldName = 'RECEIVER$ID'
    end
    object InvoiceArrayRECEIVERNAME: TFIBStringField
      FieldName = 'RECEIVER$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceArrayMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object InvoiceArrayMATERIALNAME: TFIBStringField
      FieldName = 'MATERIAL$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceArrayAGENTID: TFIBIntegerField
      FieldName = 'AGENT$ID'
    end
    object InvoiceArrayAGENTCLASSCODE: TFIBIntegerField
      FieldName = 'AGENT$CLASS$CODE'
    end
    object InvoiceArrayAGENTNAME: TFIBStringField
      FieldName = 'AGENT$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object InvoiceArrayTAXID: TFIBIntegerField
      FieldName = 'TAX$ID'
    end
    object InvoiceArrayTAXNAME: TFIBStringField
      FieldName = 'TAX$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object InvoiceArrayTAX: TFIBIntegerField
      FieldName = 'TAX'
    end
    object InvoiceArrayWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object InvoiceArrayWEIGHTREAL: TFIBBCDField
      FieldName = 'WEIGHT$REAL'
      Size = 3
      RoundByScale = True
    end
    object InvoiceArrayWEIGHT1000: TFIBBCDField
      FieldName = 'WEIGHT$1000'
      Size = 3
      RoundByScale = True
    end
    object InvoiceArrayCOST: TFIBBCDField
      FieldName = 'COST'
      Size = 2
      RoundByScale = True
    end
    object InvoiceArrayCOSTTAX: TFIBBCDField
      FieldName = 'COST$TAX'
      Size = 2
      RoundByScale = True
    end
    object InvoiceArrayCOSTTOTAL: TFIBBCDField
      FieldName = 'COST$TOTAL'
      Size = 2
      RoundByScale = True
    end
    object InvoiceArraySTATECODE: TFIBIntegerField
      FieldName = 'STATE$CODE'
    end
    object InvoiceArraySTATENAME: TFIBStringField
      FieldName = 'STATE$NAME'
      Size = 8
      EmptyStrToNull = True
    end
    object InvoiceArraySTATEDATE: TFIBDateTimeField
      FieldName = 'STATE$DATE'
    end
    object InvoiceArraySTATEBYID: TFIBIntegerField
      FieldName = 'STATE$BY$ID'
    end
    object InvoiceArraySTATEBYNAME: TFIBStringField
      FieldName = 'STATE$BY$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object InvoiceArrayFOREIGNN: TFIBStringField
      FieldName = 'FOREIGN$N'
      Size = 16
      EmptyStrToNull = True
    end
    object InvoiceArrayFOREIGNDATE: TFIBDateField
      FieldName = 'FOREIGN$DATE'
    end
    object InvoiceArrayFOREIGNCOST: TFIBBCDField
      FieldName = 'FOREIGN$COST'
      Size = 2
      RoundByScale = True
    end
  end
  object SourceJournal: TDataSource
    DataSet = InvoiceArray
    OnDataChange = SourceJournalDataChange
    Left = 40
    Top = 184
  end
  object MenuOffice: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 40
    Top = 152
  end
  object MenuPrint: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 72
    Top = 152
  end
  object MenuMaterial: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonGold'
      end
      item
        Visible = True
        ItemName = 'ButtonSilver'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 104
    Top = 152
  end
end
