unit uSound;

interface

uses

  SysUtils,
  MMSystem;

type

  TSound = class(TObject)
  private
    class procedure Custom(Name: string);
  public
    class procedure LogOn;
    class procedure LogOff;
    class procedure StartUp;
    class procedure Shutdown;
    class procedure Error;
    class procedure Warning;
    class procedure Confirmation;
    class procedure Information;
  end;

implementation

class procedure TSound.Custom(Name: string);
begin
  PlaySound(PChar(Name), 0, SND_ASYNC);
end;

class procedure TSound.LogOn;
begin
  Custom('WINDOWSLOGON');
end;

class procedure TSound.LogOff;
begin
  Custom('WINDOWSLOGOFF');
end;

class procedure TSound.StartUp;
begin
  Custom('OPEN');
end;

class procedure TSound.Shutdown;
begin
  Custom('CLOSE');
end;

class procedure TSound.Error;
begin
  Custom('APPGPFAULT');
end;

class procedure TSound.Warning;
begin
  Custom('SYSTEMEXCLAMATION');
end;

class procedure TSound.Confirmation;
begin
  Custom('SYSTEMQUESTION');
end;

class procedure TSound.Information;
begin
  Custom('SYSTEMNOTIFICATION');
end;

initialization

   TSound.LogOn;

end.
