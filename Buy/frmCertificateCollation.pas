unit frmCertificateCollation;
                                                   
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxRibbonForm, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbon, dxBar, cxClasses, dxRibbonFormCaptionHelper,
  DB, FIBDataSet, pFIBDataSet, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridBandedTableView, cxGridDBBandedTableView, ActnList, DBClient, frxClass,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinsdxRibbonPainter,
  dxSkinscxPCPainter, dxSkinsdxBarPainter, Provider, cxProgressBar,
  cxBarEditItem, dxBarExtItems, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

  TDialogCertificateCollation = class(TdxRibbonForm)
    BarManager: TdxBarManager;
    BarAccess: TdxBar;
    ButtonFit: TdxBarButton;
    ButtonPrintGrid: TdxBarButton;
    ButtonSave: TdxBarButton;
    Ribbon: TdxRibbon;
    SourceInvoice: TDataSource;
    DBInvoiceItem: TpFIBDataSet;
    GridInvoiceLevel: TcxGridLevel;
    GridInvoice: TcxGrid;
    GridInvoiceView: TcxGridDBBandedTableView;
    BarRed: TdxBar;
    Actions: TActionList;
    BarDockControl: TdxBarDockControl;
    GridInvoiceViewSERTIFICATEID: TcxGridDBBandedColumn;
    GridInvoiceViewSTATENAME: TcxGridDBBandedColumn;
    ActionPrint: TAction;
    ButtonPrint: TdxBarLargeButton;
    InvoiceItem: TClientDataSet;
    Provider: TDataSetProvider;
    InvoiceItemSTATENAME: TStringField;
    InvoiceItemACTUALSTATENAME: TStringField;
    InvoiceItemID: TIntegerField;
    InvoiceItemCERTIFICATEID: TIntegerField;
    InvoiceItemCOLLATIONID: TIntegerField;
    InvoiceItemOFFICEID: TIntegerField;
    InvoiceItemSTATE: TIntegerField;
    InvoiceItemSTATEDATE: TDateTimeField;
    InvoiceItemACTUALSTATE: TIntegerField;
    InvoiceItemOFFICENAME: TStringField;
    GridInvoiceViewOFFICENAME: TcxGridDBBandedColumn;
    GridInvoiceViewSTATEDATE: TcxGridDBBandedColumn;
    GridInvoiceViewACTUALSTATENAME: TcxGridDBBandedColumn;
    InvoiceItemFLAG: TIntegerField;
    Data: TClientDataSet;
    GridInvoiceViewOFFICEID: TcxGridDBBandedColumn;
    GridInvoiceViewSTATE: TcxGridDBBandedColumn;
    GridInvoiceViewACTUALSTATE: TcxGridDBBandedColumn;
    GridInvoiceViewFLAG: TcxGridDBBandedColumn;
    ProgressRed: TcxBarEditItem;
    BarGreen: TdxBar;
    ProgressGreen: TcxBarEditItem;
    BarManagerBar1: TdxBar;
    ButtonReload: TdxBarLargeButton;
    ButtonWizard: TdxBarLargeButton;
    Office: TClientDataSet;
    procedure ProviderBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet;
      DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure InvoiceItemCalcFields(DataSet: TDataSet);
    procedure InvoiceItemAfterPost(DataSet: TDataSet);
    procedure GridInvoiceViewSERTIFICATEIDStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonReloadClick(Sender: TObject);
    procedure InvoiceItemBeforeOpen(DataSet: TDataSet);
    procedure ButtonWizardClick(Sender: TObject);
    procedure GridInvoiceViewStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    FID: Integer;
    SelfOfficeID: Integer;
    SelfOfficeName: string;
    procedure Reload;
    procedure Progress;
  public
    procedure Execute;
    procedure OnCertificate(Certificate: string);
    property ID: Integer read FID write FID;
  end;

var
  DialogCertificateCollation: TDialogCertificateCollation;

implementation

uses

  ComData, Data, uDialog, dmBuy;

{$R *.dfm}

{ TDialogBuyInvoiceCollation }

procedure TDialogCertificateCollation.InvoiceItemAfterPost(DataSet: TDataSet);
begin
  InvoiceItem.ApplyUpdates(-1);
end;

procedure TDialogCertificateCollation.InvoiceItemBeforeOpen(DataSet: TDataSet);
begin
  InvoiceItem.Params.ParamByName('collation$id').AsInteger := ID;
end;

procedure TDialogCertificateCollation.InvoiceItemCalcFields(DataSet: TDataSet);
begin

  case InvoiceItemSTATE.AsInteger of

   -1:
      begin
        InvoiceItemSTATENAME.AsString := '������';
      end;


    0:

      begin
        InvoiceItemSTATENAME.AsString := '������';
      end;

    1:

      begin
        InvoiceItemSTATENAME.AsString := '�� ������';
      end;

    MaxInt:

      begin
        InvoiceItemSTATENAME.AsString := '';
      end;

  end;

  case InvoiceItemACTUALSTATE.AsInteger of

    0:

      InvoiceItemACTUALSTATENAME.AsString := '';

    1:

      InvoiceItemACTUALSTATENAME.AsString := '�� ������';

    2:

      InvoiceItemACTUALSTATENAME.AsString := '�� ������';

  end;

  if Office.FindKey([InvoiceItemOFFICEID.AsInteger]) then
  begin
    InvoiceItemOFFICENAME.AsString := Office.FieldByName('Name').AsString; 
  end;
end;


procedure TDialogCertificateCollation.ButtonReloadClick(Sender: TObject);
begin
  Reload;
end;

procedure TDialogCertificateCollation.ButtonWizardClick(Sender: TObject);
begin
  if TDialog.Confirmation('������� ����� ��������������?') = mrYes then
  begin
    DataBuy.Execute('execute procedure SERTIFICATE$COLLATION$I');

    ButtonReload.Click;
  end;
end;

procedure TDialogCertificateCollation.Execute;
var
  WorkMode: string;
begin
  WorkMode := dm.WorkMode;

  dm.WorkMode := 'Certificate Collation';

  DataBuy := TDataBuy.Create(Self);

  SelfOfficeID := DataBuy.SelfDepartmentID;

  SelfOfficeName := DataBuy.SelfDepartmentName;

  Office.Data := DataBuy.Table('select d_depid id, sname name from d_dep where d_depid > 0');

  Screen.Cursor := crHourGlass;

  InvoiceItem.DisableControls;

  try

    InvoiceItem.Active := True;

    InvoiceItem.EnableControls;

    Progress;

    ShowModal;

    InvoiceItem.Active := False;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Screen.Cursor := crDefault;

  FreeAndNil(DataBuy);

  dm.WorkMode := WorkMode;
end;

procedure TDialogCertificateCollation.GridInvoiceViewSERTIFICATEIDStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  OfficeID: Variant;
  State: Variant;
  ActualState: Variant;
  Flag: Variant;
begin
  Index := GridInvoiceViewSTATE.Index;

  State := ARecord.Values[Index];

  if not VarIsNull(State) then
  begin
    Index := GridInvoiceViewACTUALSTATE.Index;

    ActualState := ARecord.Values[Index];

    Index := GridInvoiceViewOFFICEID.Index;

    OfficeID := ARecord.Values[Index];

    Index := GridInvoiceViewFLAG.Index;

    Flag := ARecord.Values[Index];

    if Flag = 1 then
    begin
      AStyle := DataBuy.StyleGreen;
    end else
    begin
      AStyle := DataBuy.StyleRed;
    end;
  end;
end;

procedure TDialogCertificateCollation.GridInvoiceViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  AStyle := DataBuy.StyleDefault;

  if ARecord.Selected then
  begin
    AStyle := DataBuy.StyleSelected;
  end;
end;

procedure TDialogCertificateCollation.Reload;
begin
  Screen.Cursor := crHourGlass;

  InvoiceItem.DisableControls;

  try

    if InvoiceItem.Active then
    begin
      InvoiceItem.Active := False;
    end;

    InvoiceItem.Active := True;

    InvoiceItem.EnableControls;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Screen.Cursor := crDefault;

  Progress;
end;

procedure TDialogCertificateCollation.ProviderBeforeUpdateRecord(Sender: TObject; SourceDS: TDataSet; DeltaDS: TCustomClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
  ID: Integer;
  OfficeID: Integer;
  State: Variant;
  StateDate: Variant;
  ActualState: Integer;
  CertificateID: Integer;
  Flag: Integer;
  SQL: string;
begin
  if UpdateKind = ukModify then
  begin
    ID := InvoiceItemID.AsInteger;

    Flag := InvoiceItemFLAG.AsInteger;

    CertificateID := InvoiceItemCERTIFICATEID.AsInteger;

    ActualState := InvoiceItemACTUALSTATE.AsInteger;

    SQL := ' update sertificate$collation$item set actual$state = :actual$state, flag = :flag where id = :id ';

    DataBuy.Execute(SQL, [ActualState, Flag, ID]);
  end else
  begin
    ID := InvoiceItemID.AsInteger;

    if ID = 0 then
    begin
      ID := dmCom.db.Gen_Id('G$SERTIFICATE$COLLATION$ITEM', 1);
    end;

    Flag := InvoiceItemFLAG.AsInteger;

    OfficeID := InvoiceItemOFFICEID.AsInteger;

    CertificateID := InvoiceItemCERTIFICATEID.AsInteger;

    State := InvoiceItemSTATE.AsVariant;

    StateDate := InvoiceItemSTATEDATE.AsVariant;

    ActualState := InvoiceItemACTUALSTATE.AsInteger;

    SQL :=

    ' insert into sertificate$collation$item (id, certificate$id, collation$id, office$id, state, state$date, actual$state, flag) ' +
    ' values (:id, :certificate$id, 0, :office$id, :state, :state$date, 0, :flag) ';

    DataBuy.Execute(SQL, [ID, CertificateID, OfficeID, State, StateDate, Flag]);
  end;

  Progress;

  Applied := True;
end;

procedure TDialogCertificateCollation.OnCertificate(Certificate: string);
var
  ID: Integer;
  State: Integer;
  StateDate: Variant;
  OfficeID: Integer;
  ActualState: Integer;
  Flag: Integer;
begin
  ID := StrToInt(Certificate);

  ActualState := 1;

  if InvoiceItem.FindKey([ID]) then
  begin
    Flag := 1;

    InvoiceItem.Edit;

    InvoiceItemACTUALSTATE.AsInteger := ActualState;

    InvoiceItemFLAG.AsInteger := Flag;

    InvoiceItem.Post;

  end else
  begin
    Data.Data := DataBuy.Table('select id, d_depid office$id, state, statedate state$date from sertificate where id = :id', [ID]);

    if Data.RecordCount = 0 then
    begin
      State := MaxInt;

      StateDate := Null;

      OfficeID := -1000;

      Flag := 0;

    end else
    begin
      OfficeID := Data.FieldByName('office$id').AsInteger;

      State := Data.FieldByName('state').AsInteger;

      StateDate := Data.FieldByName('state$date').AsDateTime;

      if  (OfficeID = SelfOfficeID) and (State = ActualState) and (ActualState = 1) then
      begin
        Flag := 1;
      end else
      begin
        Flag := 0;
      end;
    end;

    InvoiceItem.Append;

    InvoiceItemID.AsInteger := 0;

    InvoiceItemCERTIFICATEID.AsInteger := ID;

    InvoiceItemCOLLATIONID.AsInteger := 0;

    InvoiceItemOFFICEID.AsInteger := OfficeID;

    InvoiceItemSTATE.AsInteger := State;

    InvoiceItemSTATEDATE.AsVariant := StateDate;

    InvoiceItemACTUALSTATE.AsInteger := ActualState;

    InvoiceItemFLAG.AsInteger := Flag;

    InvoiceItem.Post;
  end;

end;

procedure TDialogCertificateCollation.ButtonPrintClick(Sender: TObject);
begin
  GridInvoiceLevel.Caption := '��������������' + #13#10 + SelfOfficeName + #13#10 + DateToStr(Date);

  GridInvoiceView.DataController.Filter.BeginUpdate;

  GridInvoiceView.DataController.Filter.Root.InsertItem(GridInvoiceViewFLAG, foNotEqual, 1, '', 0);

  GridInvoiceView.DataController.Filter.Active := True;

  GridInvoiceView.DataController.Filter.EndUpdate;

  DataBuy.ActionPrint.Execute;

  GridInvoiceView.DataController.Filter.Active := False;

  GridInvoiceView.DataController.Filter.Clear;
end;

procedure TDialogCertificateCollation.Progress;
var
  All, Red, Green: Integer;
begin
  All := InvoiceItem.RecordCount;

  Red := DataBuy.Value('select count(*) from SERTIFICATE$COLLATION$ITEM where flag = 0');

  Green := All - Red;

  TcxProgressBarProperties(ProgressRed.Properties).Max := All;

  ProgressRed.EditValue := Red;

  TcxProgressBarProperties(ProgressGreen.Properties).Max := All;

  ProgressGreen.EditValue := Green;
end;


end.
