unit frmBuyJournalCollation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxBar,
  dxRibbon, cxClasses, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, FIBDataSet, pFIBDataSet, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView, DBClient,
  cxGridDBBandedTableView, cxGridCustomView, cxGrid, ActnList, dxRibbonForm,
  dxRibbonFormCaptionHelper, cxContainer, cxGroupBox, frmPopup, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinsdxRibbonPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

  TCustomBoolean = (cbUnknown, cbFalse, cbTrue);

  TDialogJournalCollation = class(TdxRibbonForm)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    BarAccess: TdxBar;
    ButtonReload: TdxBarLargeButton;
    ButtonFit: TdxBarButton;
    ButtonPrintGrid: TdxBarButton;                                                                                           
    ButtonSave: TdxBarButton;
    Ribbon: TdxRibbon;
    SourceJournal: TDataSource;
    Journal: TpFIBDataSet;
    Actions: TActionList;
    ActionAdd: TAction;
    ActionDelete: TAction;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    JournalID: TFIBIntegerField;
    JournalCOMPANYID: TFIBIntegerField;
    JournalCOMPANYNAME: TFIBStringField;
    JournalDEPARTMENTID: TFIBIntegerField;
    JournalDEPARTMENTNAME: TFIBStringField;
    JournalN: TFIBIntegerField;
    JournalCOLLATIONDATE: TFIBDateField;
    JournalSTATECODE: TFIBIntegerField;
    JournalSTATENAME: TFIBStringField;
    JournalSTATEDATE: TFIBDateTimeField;
    JournalSTATEBYID: TFIBIntegerField;
    JournalSTATEBYNAME: TFIBStringField;
    ActionReload: TAction;
    ButtonOffice: TdxBarLargeButton;
    ActionOpen: TAction;
    ActionClose: TAction;
    ButtonOpen: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    MenuOffice: TdxBarPopupMenu;
    ActionPrint: TAction;
    ButtonPrint: TdxBarLargeButton;
    BarFilter: TdxBar;
    ButtonCollation: TdxBarButton;
    GroupBoxJournal: TcxGroupBox;
    GridJournal: TcxGrid;
    GridJournalView: TcxGridDBBandedTableView;
    GridJournalViewCOMPANYNAME: TcxGridDBBandedColumn;
    GridJournalViewDEPARTMENTNAME: TcxGridDBBandedColumn;
    GridJournalViewN: TcxGridDBBandedColumn;
    GridJournalViewCOLLATIONDATE: TcxGridDBBandedColumn;
    GridJournalViewSTATENAME: TcxGridDBBandedColumn;
    GridJournalViewSTATEDATE: TcxGridDBBandedColumn;
    GridJournalViewSTATEBYNAME: TcxGridDBBandedColumn;
    GridJournalLevel: TcxGridLevel;
    BarDockControl: TdxBarDockControl;
    BarFilterDockControl: TdxBarDockControl;
    GridJournalViewSTOREDATE: TcxGridDBBandedColumn;
    JournalSTOREDATE: TFIBDateField;
    procedure FormCreate(Sender: TObject);
    procedure ActionAddUpdate(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure ButtonOfficeClick(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure JournalBeforeOpen(DataSet: TDataSet);
    procedure ActionOpenUpdate(Sender: TObject);
    procedure ActionCloseUpdate(Sender: TObject);
    procedure ActionPrintUpdate(Sender: TObject);
    procedure ActionReloadUpdate(Sender: TObject);
    procedure ActionAddExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionOpenExecute(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
    procedure ActionPrintExecute(Sender: TObject);
    procedure GridJournalViewKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure GridJournalViewCellDblClick(Sender: TcxCustomGridTableView;  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;     AShift: TShiftState; var AHandled: Boolean);
    procedure ButtonCollationClick(Sender: TObject);
  private
    OfficeName: string;
    CollationID: Integer;
    procedure Reload;
    procedure Setup;
    procedure CustomSetup;
    function GetOfficeID: Integer;
    property OfficeID: Integer read GetOfficeID;
  private
    CategoryArray: TStringList;
    procedure OnDateOk(Popup: TPopup);
    function GetCategoryVisible(Name: string): TCustomBoolean;
    procedure SetCategoryVisible(Name: string; Value: TCustomBoolean);
    property CategoryVisible[Name: string]: TCustomBoolean read GetCategoryVisible write SetCategoryVisible;
  public
    procedure Execute;
  end;

var
  DialogJournalCollation: TDialogJournalCollation;

implementation

{$R *.dfm}

uses

  ComData, uDialog, dmBuy, dmBuyCollation, frmBuyInvoiceCollation, frmBuyInvoiceReport, frmBuyDatePopup;

procedure TDialogJournalCollation.FormCreate(Sender: TObject);
var
  Rect: TRect;

procedure MenuOfficePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  if MenuOffice.ItemLinks.Count = 0 then
  begin
    DataSet := TClientDataSet.Create(nil);

    try

      DataSet.Data := DataBuy.Table(sqlOffice);

      DataSet.First;

      while not DataSet.Eof do
      begin

        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := DataSet.FieldByName('ID').AsInteger;

        Button.Caption := DataSet.FieldByName('NAME').AsString;

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Button.Tag = DataBuy.SelfDepartmentID then
        begin
          OfficeName := Button.Caption;

          ButtonOffice.Caption := OfficeName;

          Button.Down := True;
        end;

        if MenuOffice.ItemLinks.Count = 2 then
        begin
          ButtonLink.BeginGroup := True;
        end;

        DataSet.Next;
      end;

      if MenuOffice.ItemLinks.Count > 1 then
      begin
        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := 0;

        Button.Caption := '���';

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        ButtonLink.BeginGroup := True;
      end;

    finally

      DataSet.Free;

    end;

  end;
end;

begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);

  BoundsRect := Rect;

  ButtonOffice.Tag := DataBuy.SelfDepartmentID;

  MenuOfficePopulate;

  CategoryArray := TStringList.Create;

  CategoryArray.AddStrings(BarManager.Categories);

  Setup;
end;

procedure TDialogJournalCollation.FormDestroy(Sender: TObject);
begin
  CategoryArray.Free;
end;

function TDialogJournalCollation.GetOfficeID: Integer;
begin
  Result := ButtonOffice.Tag;
end;


procedure TDialogJournalCollation.Execute;
begin

  try

    Reload;

    ShowModal;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;
end;

procedure TDialogJournalCollation.Reload;
begin
  if Journal.Active then
  begin
    Journal.Active := False;
  end;

  Journal.Active := True;
end;

procedure TDialogJournalCollation.JournalBeforeOpen(DataSet: TDataSet);
begin
  Journal.ParamByName('department$id').AsInteger := OfficeID;
end;


function TDialogJournalCollation.GetCategoryVisible(Name: string): TCustomBoolean;
var
  CategoryIndex: Integer;
begin
  Result := cbUnknown;

  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin
    Result := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);
  end;
end;

procedure TDialogJournalCollation.SetCategoryVisible(Name: string; Value: TCustomBoolean);
var
  CategoryIndex: Integer;
  CurrentValue: TCustomBoolean;
begin
  CategoryIndex := BarManager.Categories.IndexOf(Name);

  if CategoryIndex <> -1 then
  begin

    if Value = cbUnknown then
    begin
      CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Value));
    end else

    begin
      CurrentValue := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);

      if CurrentValue = cbUnknown then
      begin
        CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Value));
      end;
    end;

  end;
end;

procedure TDialogJournalCollation.Setup;
var
  CategoryIndex: Integer;
  Visible: TCustomBoolean;
begin
  Visible := cbUnknown;

  for CategoryIndex := 0 to CategoryArray.Count - 1 do
  begin
    CategoryArray.Objects[CategoryIndex] := Pointer(Ord(Visible));
  end;

  CustomSetup;

  for CategoryIndex := 0 to CategoryArray.Count - 1 do
  begin
    Visible := TCustomBoolean(CategoryArray.Objects[CategoryIndex]);

    if Visible <> cbUnknown then
    begin
      if Visible = cbTrue then
      begin
        BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways;
      end else
      begin
        BarManager.CategoryItemsVisible[CategoryIndex] := ivNever;
      end;
    end;
  end;

  //GridJournalView.Bands[0].Caption := OfficeName;
end;

procedure TDialogJournalCollation.CustomSetup;
begin
  if DataBuy.IsCenterDepartment then
  begin
    CategoryVisible['Office'] := cbTrue;
  end else
  begin
    CategoryVisible['Office'] := cbFalse;
  end;

  CategoryVisible['Print'] := cbTrue;

  CategoryVisible['Common'] := cbTrue;

  if OfficeID  = DataBuy.SelfDepartmentID then
  begin
    CategoryVisible['Action'] := cbTrue;
  end else
  begin
    CategoryVisible['Action'] := cbFalse;
  end;
end;

procedure TDialogJournalCollation.ButtonOfficeClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonOffice then
    begin
      GetWindowRect(Bar.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonOffice.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      MenuOffice.Popup(X, Y);
    end else

    begin
      ButtonOffice.Tag := Button.Tag;

      OfficeName := Button.Caption;

      ButtonOffice.Caption :=  OfficeName;

      Setup;

      Reload;
    end;
  end;
end;

procedure TDialogJournalCollation.ActionAddUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  if Enabled then
  begin
    Enabled := Journal.Active;
  end;

  ActionAdd.Enabled := Enabled;
end;

procedure TDialogJournalCollation.ActionDeleteUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  if Enabled then
  begin
    Enabled := Journal.Active;
  end;

  if Enabled then
  begin
    Enabled := not Journal.IsEmpty;
  end;

  if Enabled then
  begin
    Enabled := JournalSTATECODE.AsInteger in [0, 1];
  end;

  ActionDelete.Enabled := Enabled;
end;


procedure TDialogJournalCollation.ActionOpenUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  if Enabled then
  begin
    Enabled := Journal.Active;
  end;

  if Enabled then
  begin
    Enabled := not Journal.IsEmpty;
  end;

  if Enabled then
  begin
    Enabled := JournalSTATECODE.AsInteger = 2;
  end;

  ActionOpen.Enabled := Enabled;
end;


procedure TDialogJournalCollation.ActionCloseUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  if Enabled then
  begin
    Enabled := Journal.Active;
  end;

  if Enabled then
  begin
    Enabled := not Journal.IsEmpty;
  end;

  if Enabled then
  begin
    Enabled := JournalSTATECODE.AsInteger in [0, 1];
  end;

  ActionClose.Enabled := Enabled;
end;

procedure TDialogJournalCollation.ActionPrintUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  if Enabled then
  begin
    Enabled := Journal.Active;
  end;

  if Enabled then
  begin
    Enabled := not Journal.IsEmpty;
  end;

  ActionPrint.Enabled := Enabled;
end;

procedure TDialogJournalCollation.ActionReloadUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := True;

  ActionReload.Enabled := Enabled;
end;

procedure TDialogJournalCollation.ActionAddExecute(Sender: TObject);
var
  Dialog: TDialogBuyDatePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonAdd.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyDatePopup.Create(Self);

  Dialog.Date := Date;

  Dialog.OnOk := OnDateOk;

  Dialog.Popup(X, Y);
end;

procedure TDialogJournalCollation.OnDateOk(Popup: TPopup);
var
  ID: Integer;
  StoreDate: TDateTime;
  Dialog: TDialogBuyDatePopup;
begin
  Dialog := TDialogBuyDatePopup(Popup);

  StoreDate := Dialog.Date;

  if StoreDate < Date then
  begin

  Screen.Cursor := crHourGlass;

  Journal.DisableControls;

  try

     try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyCollation.CollationInsert.ParamByName('STORE$DATE').AsDateTime := StoreDate;

        DataBuyCollation.CollationInsert.ExecProc;

        DataBuy.Commit;

        ID := DataBuyCollation.CollationInsert.ParamByName('ID').AsInteger;

        Journal.Append;

        JournalID.AsInteger := ID;

        Journal.Post;

     finally

        if Journal.State <> dsBrowse then
        begin
          Journal.Cancel;
        end;

        DataBuy.Rollback;

        Reload;
     end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Journal.EnableControls;

  Screen.Cursor := crDefault;

 end else

 begin
   TDialog.Error('���� ������ ������ ���� ������ �������.');
 end;

end;

procedure TDialogJournalCollation.ActionDeleteExecute(Sender: TObject);
var
  ID: Integer;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ��������������?') <> mrYes then
  begin
    exit;
  end;

  Screen.Cursor := crHourGlass;

  Journal.DisableControls;

  try

    ID := JournalID.AsInteger;         

    DataBuyCollation.CollationDelete.ParamByName('ID').AsInteger := ID;

     try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyCollation.CollationDelete.ExecProc;

        DataBuy.Commit;

        Journal.Delete;        

     finally

        DataBuy.Rollback;

     end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Journal.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TDialogJournalCollation.ActionOpenExecute(Sender: TObject);
var
  ID: Integer;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ��������������?') <> mrYes then
  begin
    Exit;
  end;

  Screen.Cursor := crHourGlass;

  Journal.DisableControls;

  try

    ID := JournalID.AsInteger;

    DataBuyCollation.CollationOpen.ParamByName('ID').AsInteger := ID;

     try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyCollation.CollationOpen.ExecProc;

        DataBuy.Commit;

        Journal.Refresh;        

     finally

       DataBuy.Rollback;

     end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Journal.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TDialogJournalCollation.ActionCloseExecute(Sender: TObject);
var
  ID: Integer;
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� ��������������?') <> mrYes then
  begin
    Exit;
  end;

  Screen.Cursor := crHourGlass;

  Journal.DisableControls;

  try

    ID := JournalID.AsInteger;

    DataBuyCollation.CollationClose.ParamByName('ID').AsInteger := ID;

     try

        DataBuy.Rollback;

        DataBuy.StartTransaction;

        DataBuyCollation.CollationClose.ExecProc;

        DataBuy.Commit;

        Journal.Refresh;        

     finally

       DataBuy.Rollback;

     end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  Journal.EnableControls;

  Screen.Cursor := crDefault;
end;

procedure TDialogJournalCollation.ActionPrintExecute(Sender: TObject);
var
  ID: Integer;
  DepartmentID: Integer;
  Dialog: TDialogBuyInvoiceReport;
begin
  ID := JournalID.AsInteger;

  DepartmentID := JournalDEPARTMENTID.AsInteger;

  Dialog := TDialogBuyInvoiceReport.Create(Self);

  Dialog.ID := 100;

  Dialog.InvoiceID := ID;

  Dialog.DepartmentID := DepartmentID;

  Dialog.MaterialID := 0;

  Dialog.ClassCode := 0;

  Dialog.Execute;

  Dialog.Free;
end;

procedure TDialogJournalCollation.ActionReloadExecute(Sender: TObject);
begin
  Reload;
end;

procedure TDialogJournalCollation.GridJournalViewCellDblClick( Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  CollationID := JournalID.AsInteger;

  ButtonCollation.Click;

  AHandled := True;
end;

procedure TDialogJournalCollation.GridJournalViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = 13 then
  begin
    CollationID := JournalID.AsInteger;

    ButtonCollation.Click;
  end;
end;

procedure TDialogJournalCollation.ButtonCollationClick(Sender: TObject);
var
  ReadOnly: Boolean;
  Dialog: TDialogBuyInvoiceCollation;
begin
  if CollationID <> 0 then
  begin
    ReadOnly := not (JournalSTATECODE.AsInteger in [0, 1]);

    Dialog := TDialogBuyInvoiceCollation.Create(Self);

    Dialog.ReadOnly := ReadOnly;

    Dialog.CollationID := CollationID;

    Dialog.Execute;

    Dialog.Free;
  end;
end;




end.
