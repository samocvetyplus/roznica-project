unit frmBuyInvoiceTrade;

interface                                                                             

uses

  Classes, ActnList, Controls, DB,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxGrid,
  dxLayoutControl, cxDBEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxGroupBox,
  dxSkinsCore, dxLayoutcxEditAdapters, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxBar, dxLayoutLookAndFeels,
  frmBuyInvoice, cxHeader, Menus, StdCtrls, cxButtons, DBClient, cxSplitter,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxPScxGrid6Lnk, cxCalendar, uCalendar, cxCheckBox, cxGridDBTableView, cxPC,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters;

type

{ TFrameBuyInvoiceTrade }

  TFrameBuyInvoiceTrade = class(TFrameBuyInvoice)
  protected
    class function AgentBit: TAgentBit; override;
    function InternalActivate: Boolean; override;
  end;

var
  FrameBuyInvoiceTrade: TFrameBuyInvoiceTrade;

implementation

{$R *.dfm}


uses

   uBuy;

{ TFrameBuyInvoiceTrade }

class function TFrameBuyInvoiceTrade.AgentBit: TAgentBit;
begin
  Result := abTrade;
end;

function TFrameBuyInvoiceTrade.InternalActivate: Boolean;
begin
  Result := False;

  if ClassCode = 10 then
  begin
    LayoutGroupForeign.Tag := 1;
  end;

  if inherited InternalActivate then
  begin

    GridInvoiceItemViewWEIGHT.Visible := False;


    GridInvoiceItemViewWEIGHTREAL.Caption := '���';

    Result := True;
  end;
end;

end.
