inherited FrameBuyJournalRecycling: TFrameBuyJournalRecycling
  Width = 1025
  Height = 550
  inherited GroupBoxJournal: TcxGroupBox
    Height = 550
    Width = 1025
    inherited BarDock: TdxBarDockControl
      Width = 1013
    end
    inherited BarFilterDock: TdxBarDockControl
      Width = 1013
    end
    inherited Splitter: TcxSplitter
      Width = 1021
    end
    inherited GridJournal: TcxGrid
      Width = 1013
      Height = 343
      inherited GridJournalView: TcxGridDBBandedTableView
        inherited GridJournalViewWEIGHT: TcxGridDBBandedColumn
          Visible = False
        end
        inherited GridJournalViewWEIGHTREAL: TcxGridDBBandedColumn
          Caption = #1042#1077#1089
        end
        inherited GridJournalViewWEIGHT1000: TcxGridDBBandedColumn
          Position.ColIndex = 3
        end
        object GridJournalViewWEIGHT585: TcxGridDBBandedColumn
          Caption = '585'
          DataBinding.FieldName = 'WEIGHT$585'
          HeaderAlignmentHorz = taCenter
          Width = 85
          Position.BandIndex = 2
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
    end
    inherited GroupBoxNote: TcxGroupBox
      Width = 1013
      inherited DockControlNote: TdxBarDockControl
        Left = 969
      end
      inherited EditorNote: TcxMemo
        Width = 963
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      0
      0
      0
      0
      0
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited ButtonOffice: TdxBarLargeButton
      ImageIndex = -1
    end
    inherited ButtonOpen: TdxBarLargeButton
      Width = 0
    end
    object ButtonPrint103: TdxBarLargeButton [14]
      Tag = 103
      Caption = #1054#1090#1087#1091#1089#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      Category = 6
      Hint = #1054#1090#1087#1091#1089#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = ButtonPrintClick
    end
    inherited ButtonSend: TdxBarLargeButton
      Down = True
      ImageIndex = 17
    end
  end
  inherited Action: TActionList
    inherited ActionAdd: TAction
      Caption = #1055#1077#1088#1077#1076#1072#1095#1072
    end
  end
  inherited InvoiceArray: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  cast(i.weight$1000 / 0.585 as numeric(18, 3)) weight$585,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost '
      ''
      'from'
      ''
      ' buy$invoice$a'
      ' ('
      '  :department$id,'
      '  :class$code,'
      '  :range$begin,'
      '  :range$end'
      ' ) i'
      ''
      ' where i.material$id = :material$id'
      '')
    object InvoiceArrayWEIGHT585: TFIBBCDField [27]
      FieldName = 'WEIGHT$585'
      Size = 3
      RoundByScale = True
    end
  end
  inherited MenuPrint: TdxBarPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonPrint103'
      end>
  end
end
