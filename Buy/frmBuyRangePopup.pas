unit frmBuyRangePopup;

interface

uses

  Windows, SysUtils, Controls, Classes, DateUtils,
  cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit,
  cxClasses, cxCalendar, cxGroupBox,
  dxSkinsCore, dxLayoutControl, dxSkinsdxBarPainter, dxBar,
  frmPopup, uCalendar, dxSkinsDefaultPainters, ActnList, dxSkinBlack,
  dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin;

type

{ TDialogBuyRangePopup }

  TDialogBuyRangePopup = class(TPopup)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonDay: TdxBarButton;
    ButtonWeek: TdxBarButton;
    ButtonMonth: TdxBarButton;
    ButtonYear: TdxBarButton;
    GroupBox: TcxGroupBox;
    Layout: TdxLayoutControl;
    LayoutGroup: TdxLayoutGroup;
    LayoutItemRangeEnd: TdxLayoutItem;
    RangeEndEditor: TcxCalendar;
    LayoutItemRangeBegin: TdxLayoutItem;
    RangeBeginEditor: TcxCalendar;
    BarDock: TdxBarDockControl;
    LayoutItemBar: TdxLayoutItem;
    LayoutSeparatorItem: TdxLayoutSeparatorItem;
    BarDockControlBottom: TdxBarDockControl;
    LayoutItemBarBottom: TdxLayoutItem;
    BarBottom: TdxBar;
    ButtonOk: TdxBarButton;
    ButtonCancel: TdxBarButton;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    procedure ButtonDayClick(Sender: TObject);
    procedure ButtonWeekClick(Sender: TObject);
    procedure ButtonMonthClick(Sender: TObject);
    procedure ButtonYearClick(Sender: TObject);
    procedure ButtonOkClick(Sender: TObject);
    procedure ButtonCancelClick(Sender: TObject);
  private
    function GetRangeBegin: TDate;
    function GetRangeEnd: TDate;
    procedure SetRangeBegin(const Value: TDate);
    procedure SetRangeEnd(const Value: TDate);
  protected
    procedure Ok; override;
  public
    property RangeBegin: TDate read GetRangeBegin write SetRangeBegin;
    property RangeEnd: TDate read GetRangeEnd write SetRangeEnd;
  end;

implementation

{$R *.dfm}

uses

   dmBuy, uDialog;

{ TDialogBuyRangePopup }

procedure TDialogBuyRangePopup.ButtonDayClick(Sender: TObject);
begin
  RangeEndEditor.SelectDate := RangeBegin;
end;

procedure TDialogBuyRangePopup.ButtonWeekClick(Sender: TObject);
begin
  RangeBeginEditor.SelectDate := DateOf(StartOfTheWeek(RangeBegin));

  RangeEndEditor.SelectDate := DateOf(EndOfTheWeek(RangeBegin));
end;

procedure TDialogBuyRangePopup.ButtonMonthClick(Sender: TObject);
begin
  RangeBeginEditor.SelectDate := DateOf(StartOfTheMonth(RangeBegin));

  RangeEndEditor.SelectDate := DateOf(EndOfTheMonth(RangeBegin));
end;

procedure TDialogBuyRangePopup.ButtonYearClick(Sender: TObject);
begin
  RangeBeginEditor.SelectDate := DateOf(StartOfTheYear(RangeBegin));

  RangeEndEditor.SelectDate := DateOf(EndOfTheYear(RangeBegin));
end;

procedure TDialogBuyRangePopup.ButtonOkClick(Sender: TObject);
begin
  Ok;
end;

procedure TDialogBuyRangePopup.ButtonCancelClick(Sender: TObject);
begin
  Cancel;
end;

function TDialogBuyRangePopup.GetRangeBegin: TDate;
begin
  Result := RangeBeginEditor.SelectDate;
end;

function TDialogBuyRangePopup.GetRangeEnd: TDate;
begin
  Result := RangeEndEditor.SelectDate;
end;

procedure TDialogBuyRangePopup.Ok;
begin
  if RangeBegin <= RangeEnd then
  begin
    inherited Ok;
  end else

  begin
    TDialog.Error('�������� ���� �� ����� ���� ������ ���������!');
  end;
end;

procedure TDialogBuyRangePopup.SetRangeBegin(const Value: TDate);
begin
  RangeBeginEditor.SelectDate := Value;
end;

procedure TDialogBuyRangePopup.SetRangeEnd(const Value: TDate);
begin
  RangeEndEditor.SelectDate := Value;
end;


end.


