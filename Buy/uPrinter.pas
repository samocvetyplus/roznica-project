unit uPrinter;

interface

uses

  Windows, Printers;

type

  TMargins = record
    Left,
    Top,
    Right,
    Bottom: Double
  end;

procedure GetPrinterMargins(var Margins: TMargins);


implementation

function InchToMm(Pixel: Double): Double;
begin
  Result := Pixel * 2.54 * 10;
end;

procedure GetPrinterMargins(var Margins: TMargins);
var
  PixelsPerInch: TPoint;
  PhysPageSize: TPoint;
  OffsetStart: TPoint;
  PageRes: TPoint;
begin
  PixelsPerInch.y := GetDeviceCaps(Printer.Handle, LOGPIXELSY);

  PixelsPerInch.x := GetDeviceCaps(Printer.Handle, LOGPIXELSX);

  Escape(Printer.Handle, GETPHYSPAGESIZE, 0, nil, @PhysPageSize);

  Escape(Printer.Handle, GETPRINTINGOFFSET, 0, nil, @OffsetStart);

  PageRes.y := GetDeviceCaps(Printer.Handle, VERTRES);

  PageRes.x := GetDeviceCaps(Printer.Handle, HORZRES);

  Margins.Top := OffsetStart.y / PixelsPerInch.y;

  Margins.Left := OffsetStart.x / PixelsPerInch.x;

  Margins.Bottom := ((PhysPageSize.y - PageRes.y) / PixelsPerInch.y) - (OffsetStart.y / PixelsPerInch.y);

  Margins.Right := ((PhysPageSize.x - PageRes.x) / PixelsPerInch.x) - (OffsetStart.x / PixelsPerInch.x);

  Margins.Top := InchToMm(Margins.Top);

  Margins.Left := InchToMm(Margins.Left);

  Margins.Bottom := InchToMm(Margins.Bottom);

  Margins.Right := InchToMm(Margins.Right);
end;

end.
