unit frmBuyInvoiceUnion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView, cxClasses,
  cxGridCustomView, cxGrid, FIBDataSet, pFIBDataSet, dxSkinsdxBarPainter, dxBar,
  ActnList, dxRibbon, dxRibbonForm, dxRibbonFormCaptionHelper, StrUtils,
  cxCustomPivotGrid, cxDBPivotGrid, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerDateNavigator, cxContainer,
  cxDateNavigator, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinsdxRibbonPainter;

type

{ TBuyInvoiceUnion }

  TBuyInvoiceUnion = class(TdxRibbonForm)
    GridInvoiceItem: TcxGrid;
    GridInvoiceItemView: TcxGridDBBandedTableView;
    GridInvoiceItemLevel: TcxGridLevel;
    InvoiceUnion: TpFIBDataSet;
    SourceInvoiceUnion: TDataSource;
    BarManager: TdxBarManager;
    LargeButtonClose: TdxBarLargeButton;
    Actions: TActionList;
    ActionGold: TAction;
    ActionSilver: TAction;
    ButtonGold: TdxBarLargeButton;
    ButtonSilver: TdxBarLargeButton;
    ButtonMaterial: TdxBarLargeButton;
    BarAccess: TdxBar;
    ButtonFit: TdxBarButton;
    ButtonPrint: TdxBarButton;
    ButtonSave: TdxBarButton;
    Ribbon: TdxRibbon;
    InvoiceUnionCLASSCODE: TFIBIntegerField;
    InvoiceUnionCLASSNAME: TFIBStringField;
    InvoiceUnionSENDERID: TFIBIntegerField;
    InvoiceUnionSENDERNAME: TFIBStringField;
    InvoiceUnionRECEIVERID: TFIBIntegerField;
    InvoiceUnionRECEIVERNAME: TFIBStringField;
    InvoiceUnionACTIONDATE: TFIBDateField;
    InvoiceUnionACTIONCODE: TFIBIntegerField;
    InvoiceUnionACTIONNAME: TFIBStringField;
    InvoiceUnionMATERIALID: TFIBIntegerField;
    InvoiceUnionMATERIALNAME: TFIBStringField;
    InvoiceUnionPROBEID: TFIBIntegerField;
    InvoiceUnionPROBENAME: TFIBStringField;
    InvoiceUnionWEIGHT: TFIBBCDField;
    InvoiceUnionWEIGHTREAL: TFIBBCDField;
    InvoiceUnionWEIGHT1000: TFIBFloatField;
    GridInvoiceItemViewCLASSNAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewSENDERNAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewRECEIVERNAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewACTIONDATE: TcxGridDBBandedColumn;
    GridInvoiceItemViewACTIONNAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewMATERIALNAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewPROBENAME: TcxGridDBBandedColumn;
    GridInvoiceItemViewWEIGHT: TcxGridDBBandedColumn;
    GridInvoiceItemViewWEIGHTREAL: TcxGridDBBandedColumn;
    GridInvoiceItemViewWEIGHT1000: TcxGridDBBandedColumn;
    InvoiceUnionsendertag: TStringField;
    InvoiceUnionreceivertag: TStringField;
    InvoiceUnionSENDERCLASSCODE: TFIBIntegerField;
    InvoiceUnionRECEIVERCLASSCODE: TFIBIntegerField;
    BarFilter: TdxBar;
    ButtonOffice: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    BarFilterDockControl: TdxBarDockControl;
    ButtonRangeCustom: TdxBarLargeButton;
    ButtonRangeDay: TdxBarLargeButton;
    ButtonRangeWeek: TdxBarLargeButton;
    ButtonRangeMonth: TdxBarLargeButton;
    ButtonRangeYear: TdxBarLargeButton;
    InvoiceUnionCOST: TFIBBCDField;
    GridInvoiceItemViewCOST: TcxGridDBBandedColumn;
    Bar: TdxBar;
    BarDockControl: TdxBarDockControl;
    procedure FormCreate(Sender: TObject);
    procedure InvoiceUnionBeforeOpen(DataSet: TDataSet);
    procedure LargeButtonCloseClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GridInvoiceItemViewCustomDrawBandHeader(
      Sender: TcxGridBandedTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
    procedure GridInvoiceItemViewDataControllerGroupingChanged(Sender: TObject);
    procedure ActionMaterialExecute(Sender: TObject);
    procedure InvoiceUnionsendertagGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure InvoiceUnionreceivertagGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure InvoiceUnionCalcFields(DataSet: TDataSet);
  private
    FRangeBegin: TDate;
    FRangeEnd: TDate;
    FOfficeName: string;
    FClassCode: Integer;
    FDepartmentID: Integer;
    function GetMaterialID: Integer;
    procedure SetMaterialID(const Value: Integer);
  public
    procedure Execute;
    procedure Reload;
    property RangeBegin: TDate read FRangeBegin write FRangeBegin;
    property RangeEnd: TDate read FRangeEnd write FRangeEnd;
    property ClassCode: Integer read FClassCode write FClassCode;
    property DepartmentID: Integer read FDepartmentID write FDepartmentID;
    property OfficeName: string read FOfficeName write FOfficeName;
    property MaterialID: Integer read GetMaterialID write SetMaterialID;
  end;

var
  BuyInvoiceUnion: TBuyInvoiceUnion;

implementation

{$R *.dfm}

uses

  uDialog, uBuy, dmBuy;

procedure TBuyInvoiceUnion.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);  

  BoundsRect := Rect;
end;

procedure TBuyInvoiceUnion.Execute;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
begin

  try
    //GridInvoiceItemView.Bands[0].Caption := OfficeName + #13 + DateRangeAsString(RangeBegin, RangeEnd);

    DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

    ButtonArray[drCustom] := ButtonRangeCustom;

    ButtonArray[drDay] := ButtonRangeDay;

    ButtonArray[drWeek] := ButtonRangeWeek;

    ButtonArray[drMonth] := ButtonRangeMonth;

    ButtonArray[drYear] := ButtonRangeYear;

    if DataBuy.IsModern then
    begin
      ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;
    end;

    ButtonOffice.Caption :=  OfficeName;

    ButtonRange.Caption := DateRangeAsString(RangeBegin, RangeEnd);

    GridInvoiceItemLevel.Caption := OfficeName + #13 + DateRangeAsString(RangeBegin, RangeEnd);

    if ClassCode in [2] then
    begin
      GridInvoiceItemViewSENDERNAME.Visible := False;

      GridInvoiceItemViewRECEIVERNAME.Visible := DepartmentID = 0;

      GridInvoiceItemViewCLASSNAME.Visible := False;
    end else

    if ClassCode in [3, 5, 7, 9] then
    begin
      GridInvoiceItemViewSENDERNAME.Visible := DepartmentID = 0;

      GridInvoiceItemViewRECEIVERNAME.Visible := True;

      GridInvoiceItemViewCLASSNAME.Visible := False;
    end else

    if ClassCode in [4, 6, 8, 10] then
    begin
      GridInvoiceItemViewSENDERNAME.Visible := True;

      GridInvoiceItemViewRECEIVERNAME.Visible := DepartmentID = 0;

      GridInvoiceItemViewCLASSNAME.Visible := False;
    end else

    if ClassCode = Low(Integer) then
    begin
      GridInvoiceItemViewSENDERNAME.Visible := DepartmentID = 0;

      GridInvoiceItemViewRECEIVERNAME.Visible := True;

      GridInvoiceItemViewCLASSNAME.Visible := True;
    end else

    if ClassCode = High(Integer) then
    begin
      GridInvoiceItemViewSENDERNAME.Visible := True;

      GridInvoiceItemViewRECEIVERNAME.Visible := DepartmentID = 0;

      GridInvoiceItemViewCLASSNAME.Visible := True;
    end;

    GridInvoiceItemViewCOST.Visible := ClassCode = 2;

    GridInvoiceItemViewACTIONNAME.Visible := ClassCode = 2;

    Reload;

    ShowModal;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

end;

procedure TBuyInvoiceUnion.InvoiceUnionBeforeOpen(DataSet: TDataSet);
begin
  InvoiceUnion.ParamByName('Department$ID').AsInteger := DepartmentID;

  InvoiceUnion.ParamByName('Class$Code').AsInteger := ClassCode;

  InvoiceUnion.ParamByName('Range$Begin').AsDate := RangeBegin;

  InvoiceUnion.ParamByName('Range$End').AsDate := RangeEnd;

  InvoiceUnion.ParamByName('Material$ID').AsInteger := MaterialID;
end;

procedure TBuyInvoiceUnion.ActionMaterialExecute(Sender: TObject);
begin
  ButtonMaterial.Tag := TAction(Sender).Tag;

  Reload;
end;

procedure TBuyInvoiceUnion.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
  begin
    ModalResult := mrCancel;
  end;
end;

function TBuyInvoiceUnion.GetMaterialID: Integer;
begin
  Result := ButtonMaterial.Tag;
end;

procedure TBuyInvoiceUnion.GridInvoiceItemViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TBuyInvoiceUnion.GridInvoiceItemViewDataControllerGroupingChanged(
  Sender: TObject);
begin
  GridInvoiceItemView.DataController.Groups.FullCollapse;

  GridInvoiceItemView.Controller.TopRowIndex := 0;

  GridInvoiceItemView.Controller.FocusedRowIndex := 0;
end;

procedure TBuyInvoiceUnion.LargeButtonCloseClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TBuyInvoiceUnion.Reload;
begin
  Screen.Cursor := crHourGlass;

  InvoiceUnion.DisableControls;

  try

    if InvoiceUnion.Active then
    begin
      InvoiceUnion.Active := False;
    end;

    InvoiceUnion.Active := True;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

  InvoiceUnion.EnableControls;

  GridInvoiceItemView.DataController.Groups.FullCollapse;

  GridInvoiceItemView.Controller.TopRowIndex := 0;

  GridInvoiceItemView.Controller.FocusedRowIndex := 0;

  Screen.Cursor := crDefault;
end;

procedure TBuyInvoiceUnion.SetMaterialID(const Value: Integer);
begin
  ButtonMaterial.Tag := Value;

  if MaterialID = 1 then
  begin
    ButtonMaterial.Caption := ButtonGold.Caption;
  end else

  if MaterialID = 2 then
  begin
    ButtonMaterial.Caption := ButtonSilver.Caption;
  end
end;

procedure TBuyInvoiceUnion.InvoiceUnionreceivertagGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  Text := InvoiceUnionRECEIVERNAME.AsString;
end;

procedure TBuyInvoiceUnion.InvoiceUnionsendertagGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  Text := InvoiceUnionSENDERNAME.AsString;
end;

procedure TBuyInvoiceUnion.InvoiceUnionCalcFields(DataSet: TDataSet);

function PadRight(Value: string; l: Integer): string;
begin
  Result := Value;

  Result := Result + DupeString(' ', l - Length(Value));
end;

begin

  InvoiceUnionSENDERTAG.AsString := InvoiceUnionSENDERCLASSCODE.AsString + '|' + PadRight(InvoiceUnionSENDERNAME.AsString, 64) + '|' + InvoiceUnionSENDERID.AsString;

  InvoiceUnionRECEIVERTAG.AsString := InvoiceUnionRECEIVERCLASSCODE.AsString + '|' + PadRight(InvoiceUnionRECEIVERNAME.AsString, 64) + '|' + InvoiceUnionRECEIVERID.AsString;

end;

end.
