unit frmStart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinsdxBarPainter, ActnList, dxBar,
  cxClasses, dxGDIPlusClasses, ExtCtrls, cxGroupBox;

type
  TFrameBuyStart = class(TFrame)
    GroupBoxImage: TcxGroupBox;
    Image: TImage;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    Actions: TActionList;
    ActionSetup: TAction;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

uses

  dmBuy;

end.
