object FrameBuyOrders: TFrameBuyOrders
  Left = 0
  Top = 0
  Width = 451
  Height = 304
  Align = alClient
  TabOrder = 0
  object Grid: TcxGrid
    Left = 0
    Top = 94
    Width = 451
    Height = 210
    Align = alClient
    TabOrder = 4
    object GridView: TcxGridDBBandedTableView
      OnKeyDown = GridViewKeyDown
      NavigatorButtons.ConfirmDelete = False
      OnCellDblClick = GridViewCellDblClick
      DataController.DataSource = BuyOrdersData.DataSourceOrders
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DateTimeHandling.DateFormat = 'dd.mm.yyyy hh:mm'
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.UnselectFocusedRecordOnExit = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.GroupByBox = False
      OptionsView.BandHeaderHeight = 32
      OptionsView.BandHeaderLineCount = 2
      Styles.BandHeader = DataBuy.StyleBand
      Bands = <
        item
          Caption = #1055#1088#1080#1082#1072#1079#1099
        end
        item
          Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        end>
      object GridViewColumn1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ORDER$NUMBER'
        HeaderAlignmentHorz = taCenter
        SortIndex = 0
        SortOrder = soDescending
        Width = 75
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewColumn2: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ORDER$DATE'
        HeaderAlignmentHorz = taCenter
        Width = 98
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewColumn3: TcxGridDBBandedColumn
        Caption = #1057#1086#1079#1076#1072#1085
        DataBinding.FieldName = 'CREATION$DATE'
        HeaderAlignmentHorz = taCenter
        Width = 100
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewColumn5: TcxGridDBBandedColumn
        Tag = 1
        Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
        DataBinding.FieldName = 'USER$NAME'
        HeaderAlignmentHorz = taCenter
        Options.Focusing = False
        Width = 138
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewColumn6: TcxGridDBBandedColumn
        Tag = 1
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'MODIFY$DATE'
        HeaderAlignmentHorz = taCenter
        Options.Focusing = False
        Width = 111
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewColumn7: TcxGridDBBandedColumn
        Tag = 1
        DataBinding.FieldName = 'ORDER$STATE'
        HeaderAlignmentHorz = taCenter
        Options.Focusing = False
        Width = 98
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewColumn4: TcxGridDBBandedColumn
        DataBinding.FieldName = 'DEPARTMENT'
        HeaderAlignmentHorz = taCenter
        Width = 175
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewORDERSTATECODE: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ORDER$STATE$CODE'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'Editing'
      'View'
      'Control'
      'Print'
      'Settings'
      'Department')
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True)
    ImageOptions.Images = DataBuy.Image16
    ImageOptions.LargeImages = DataBuy.Image32
    PopupMenuLinks = <
      item
      end
      item
      end>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 8
    Top = 112
    DockControlHeights = (
      0
      0
      94
      0)
    object Bar: TdxBar
      AllowClose = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 487
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAdd'
        end
        item
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonView'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonOpen'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonRefresh'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarFilter: TdxBar
      Caption = 'BarFilter'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 52
      DockingStyle = dsTop
      FloatLeft = 733
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonDepartment'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonRange'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OneOnRow = True
      Row = 1
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object ButtonRange: TdxBarLargeButton
      Action = acChangeRange
      Align = iaClient
      Category = 0
      ButtonStyle = bsChecked
      GroupIndex = 20
      Down = True
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object ButtonRefresh: TdxBarLargeButton
      Action = acRefresh
      Align = iaRight
      Category = 0
      LargeImageIndex = 7
      ShortCut = 16466
      AutoGrayScale = False
    end
    object ButtonSettings: TdxBarLargeButton
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      Category = 0
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080' '#1080' '#1087#1072#1088#1072#1084#1077#1090#1088#1099
      Visible = ivNever
      ButtonStyle = bsDropDown
      DropDownMenu = DepartmentsPopupMenu
      LargeImageIndex = 19
      AutoGrayScale = False
    end
    object ButtonAdd: TdxBarLargeButton
      Action = acAdd
      Category = 1
      ShortCut = 45
      AutoGrayScale = False
    end
    object ButtonDelete: TdxBarLargeButton
      Action = acDelete
      Category = 1
      ShortCut = 46
      AutoGrayScale = False
    end
    object ButtonView: TdxBarLargeButton
      Action = acView
      Category = 2
      LargeGlyph.Data = {
        36100000424D3610000000000000360000002800000020000000200000000100
        2000000000000010000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EAEAEE009E9E
        D7009595D300C4C4E600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDCE0002E2FAB002733
        C100303FD1001319B1005757BA00E1E1EA00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDCE0004747A700313FC300607D
        F7005A75FF00475CF300191FB4005757BA00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00DCDCE0002F30AB003A4BC3006C90F800234B
        D2000130B9001D44D1004154F3001116B100C4C4E600FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00DCDCE0004747A7003E51C30080A6F600284CDC00022B
        C400002FB9000130B8004D64FF002733D1009595D300FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00DCDCE0002F30AB003E51C3007FA8FB002D4CE500001FD0000026
        C6000230BC001D47CE004E65F7001E27C1009E9ED700FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00DCDCE0004747A7003E51C30082AAF7002D46EC00031ADE00001ED3000024
        C900224AD2005979F6002632C3002D2EAB00EAEAEE00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DCDC
        E0002F30AB003E51C3007FA6FD002D42F3000010E7000015E000031FD600294D
        DD006C8DF6002F3DC3004747A700DCDCE000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EBE7
        E300CFC0AA00CFC0AA00CFC0AA00D7CAB800F0EDEA00FF00FF00DCDCE0004747
        A7003E51C30082A9F7002D3EF800030DF100000EE9000013E2002D4CE5007BA3
        F9003647C3002E2FAB00DCDCE000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00E0DDD800C3A88300B87D2600B471
        0B00BA780F00BB780F00BA770C00B7740900B36F0B00BD873900614B79004550
        B20083ABFE002F3EFC000004F8000007F3000310EC002D46ED0082AAF6003E50
        C3004747A700DCDCE000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FCFCFC00D5C8B500B9802E00BC7D1900D2992F00DBA3
        3600DEA63500DEA53200DCA02B00D89B2400D0901A00C7851200AB690B009D76
        43007D92DB001921FD000004F9000007F5002D42F3007FA7FC003E51C3002F30
        AB00DCDCE000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FCFCFC00D4B18000B6771700D7A33F00E8B85200E7B64E00E6B4
        4900E4B04300E3B14800E1AB3D00DFA53200DCA12C00DB9E2700D6971E00C380
        1000A87224007F84B7001923FB002F41F90082AAF7003E51C3004747A700DCDC
        E000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00CDBDA700B6781800DBAA4A00EDC26000EBBF5C00EBBF6000F0D0
        8B00F6E4BD00F7E9C900F6E5C300F2DBAC00E7BC6700DFA63600DCA02A00DA9C
        2400C27F1000A87224007D92DA0083ABFE003E51C3002F30AB00DCDCE000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00DCD3C800B77C2600E2B65900F1CB6E00F0C96D00F3D59100FCF6E300FFFC
        EA00FFFEE700FFFFE600FFFFE700FFFFEA00FEFDF000F7EACC00E5B65800DEA5
        3300DBA02A00C58416009D7643004550B2004747A700DCDCE000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00CFA87000C68E3000F5D17900F3CF7600F6DC9D00FDF5DC00FFF6CF00FFF8
        D200FFFBDE00FFFEE500FFFFE700FFFFE700FFFFE700FFFFEC00F8EDD300E6B9
        5C00DFA73500DBA02E00AC6B0F00614B7900DCDCE000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F6F5
        F300B6761700E8C06900F7D68100F7DA8F00FEF7E000FFF1C000FFF8DE00FFF6
        D200FFF8D200FFFBDB00FFFEE500FFFFE600FFFFE700FFFFE700FFFFEC00F8EC
        CF00E4B04600E2AC3C00CC902400BD883900FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00ECDD
        C900C0852600F5D58200F9D98700FBEBC100FFF0C100FFF4D100FFFBEC00FFF3
        C300FFF5C900FFF8D200FFFBDE00FFFEE500FFFFE700FFFFE700FFFFE700FEFD
        F100ECC67600E4B24600D8A13500B4731000F0EDEA00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E0C6
        A300D5A54C00FBDF8F00FBE09700FDF3D500FFECB100FFFBEF00FFF9E500FFEE
        B500FFF2BF00FFF4C700FFF8D200FFFBDB00FFFEE500FFFFE600FFFFE700FFFF
        EA00F6E2B700E8B95100E3B24800BB7B1500D7CAB800FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00D9BB
        9000DFB55F00FDE19300FDE5A300FEF2CD00FFEDB700FFFDF700FFF9E800FFED
        B300FFEFB700FFF2BF00FFF5C900FFF8D200FFFBDE00FFFEE500FFFFE700FFFF
        E700F9EBCC00EBBF5E00E9BB5400BE801B00CFC0AA00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00D9B9
        8D00E2B96500FEE49700FEE7A700FEF2CC00FFEDB900FFFEFC00FFFDF700FFF0
        C200FFECAF00FFEEB500FFF2BF00FFF4C700FFF8D200FFFBDB00FFFEE500FFFF
        E600FAEFD200EEC76C00ECC15F00BF821F00CFC0AA00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00DDC2
        9C00D9AC5500FEE59900FEE7A500FFF4D300FFEBB000FFFCF300FFFFFF00FFF9
        E700FFEBAC00FFECAF00FFEFB700FFF2BF00FFF5C900FFF8D200FFFBDE00FFFE
        E700FAECCA00F0C96C00EDC36400BE811E00CFC0AA00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E8D5
        BB00C6903400FDE39600FFE69B00FFF4D600FFEBB000FFF6DB00FFFFFF00FFFF
        FF00FFFAEB00FFF1C500FFEFB800FFF0BC00FFF4C800FFF6D400FFF8D200FFFC
        EA00F8E0A500F3CE7400E8BD6100B7771500EDE7DF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F6EF
        E500B97C1C00F2D38300FFE69A00FFEAAD00FFF5D600FFEBB000FFFCF600FFFF
        FF00FFFFFF00FFFDF900FFFAEC00FFFAEC00FFFBEF00FFF7D900FFF6CF00FEF8
        E700F7D78600F5D27B00DFB25600B97E2900FCFCFC00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00C28F4800D6A85100FFE69A00FFE69A00FFF2CB00FFF2CA00FFEBAD00FFF4
        D600FFFBEE00FFFCF600FFFCF200FFF9E800FFF1C300FFF0BD00FEF7DF00FBE5
        AA00F9D98500F6D68100C0862700C3A88300FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00CBBFAE00B77A1F00F5D78900FFE69A00FFE7A100FFF2CB00FFF5D600FFEA
        AE00FFEAAB00FFECB300FFECB100FFEAAA00FFF0C100FFF8E200FDE9B200FBDF
        9000FADC8B00E2B86000BA802E00E0DDD800FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FCFCFC00CCAD8300BF852700F5D68700FFE69A00FFE69A00FFEAAD00FFF5
        D600FFF4D300FFF3CD00FFF3CF00FFF5D900FEF1CB00FEE7A400FDE29400FCE1
        9200E5BD6800B87A1D00D5C8B500FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00F3F2F100C2904900BF852700F5D78900FFE69A00FFE69A00FFE6
        9B00FFE9A600FFEAAB00FFEAAA00FFE8A300FEE59900FEE59800FEE49700EBC7
        7400B87A1E00D4B18000FCFCFC00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00F3F2F100CCAD8300B77A1F00D6A85100F2D38300FDE3
        9600FFE69A00FFE69A00FFE69A00FFE69A00FCE19400EDCB7B00CA943A00B77D
        2700CDBDA700FCFCFC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FCFCFC00CBBFAE00C28F4800B97C1C00C690
        3400D9AC5500E3BA6600E0B76200D6A85100C38A2E00B7791B00CDA36900DCD3
        C800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F6EFE500E8D5
        BB00DDC29C00D9B98D00D9BB9000E0C6A300ECDDC900F6F5F300FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      AutoGrayScale = False
    end
    object ButtonClose: TdxBarLargeButton
      Action = acClose
      Category = 3
      ShortCut = 16460
      AutoGrayScale = False
    end
    object ButtonOpen: TdxBarLargeButton
      Action = acOpen
      Category = 3
      ShortCut = 16463
      AutoGrayScale = False
      SyncImageIndex = False
      ImageIndex = -1
    end
    object ButtonPrintOrder: TdxBarButton
      Tag = 4
      Caption = #1055#1088#1080#1082#1072#1079' '#1085#1072' '#1089#1082#1091#1087#1082#1091
      Category = 4
      Hint = #1055#1077#1095#1072#1090#1100' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1089#1082#1091#1087#1082#1091
      Visible = ivAlways
      ImageIndex = 6
      OnClick = PrintButtonClick
    end
    object ButtonPrintOrderExchange: TdxBarButton
      Tag = 5
      Caption = #1055#1088#1080#1082#1072#1079' '#1085#1072' '#1086#1073#1084#1077#1085
      Category = 4
      Hint = #1055#1077#1095#1072#1090#1100' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1086#1073#1084#1077#1085
      Visible = ivAlways
      ImageIndex = 6
      OnClick = PrintButtonClick
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 4
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = PrintPopupMenu
      LargeImageIndex = 6
      ShortCut = 16464
      AutoGrayScale = False
    end
    object ButtonDepartment: TdxBarLargeButton
      Caption = #1054#1090#1076#1077#1083#1077#1085#1080#1077
      Category = 6
      Hint = #1060#1080#1083#1080#1072#1083', '#1076#1083#1103' '#1082#1086#1090#1086#1088#1086#1075#1086' '#1087#1088#1077#1076#1085#1072#1079#1085#1072#1095#1072#1077#1090#1089#1103' '#1087#1088#1080#1082#1072#1079
      Visible = ivAlways
      ButtonStyle = bsChecked
      GroupIndex = 10
      Down = True
      DropDownMenu = DepartmentsPopupMenu
      LargeImageIndex = 9
      OnClick = ButtonDepartmentClick
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
  end
  object ActionList: TActionList
    Images = DataBuy.Image32
    Left = 8
    Top = 144
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDeleteExecute
      OnUpdate = acDeleteUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 6
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 3
      OnExecute = acOpenExecute
      OnUpdate = acOpenUpdate
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 4
      OnExecute = acCloseExecute
      OnUpdate = acCloseUpdate
    end
    object acPrintOrder: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 6
      OnExecute = acPrintOrderExecute
      OnUpdate = acPrintOrderUpdate
    end
    object acChangeRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = acChangeRangeExecute
    end
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 6
      OnExecute = acRefreshExecute
    end
    object acShowDictionary: TAction
      Caption = 'acShowDictionary'
      ImageIndex = 20
    end
  end
  object DepartmentsPopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 8
    Top = 176
  end
  object PrintPopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonPrintOrder'
      end
      item
        Visible = True
        ItemName = 'ButtonPrintOrderExchange'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 8
    Top = 216
  end
end
