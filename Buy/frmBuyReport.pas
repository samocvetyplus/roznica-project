unit frmBuyReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, frxExportHTML, frxExportODF, frxExportImage, frxClass, frxExportPDF,
  DB, DBClient, frxDBSet;

type

  TReportClass = (rcNone, rcSheet);

  TDialogBuyReport = class(TForm)
    ReportSheet: TfrxReport;
    frxPDFExport: TfrxPDFExport;
    frxTIFFExport: TfrxTIFFExport;
    frxODSExport: TfrxODSExport;
    frxODTExport: TfrxODTExport;
    frxHTMLExport: TfrxHTMLExport;
    frxHeader: TfrxDBDataset;
    Header: TClientDataSet;
    Body: TClientDataSet;
    frxBody: TfrxDBDataset;
    frxFooter: TfrxDBDataset;
    Footer: TClientDataSet;
  private
     FID: Integer;
     FReportClass: TReportClass;
  public
    procedure Execute;
    property ID: Integer read FID write FID;
    property ReportClass: TReportClass read FReportClass write FReportClass;
  end;

var
  DialogBuyReport: TDialogBuyReport;

implementation

{$R *.dfm}

uses

  dmBuy;

{ TDialogBuyReport }

procedure TDialogBuyReport.Execute;
var
  Report: TfrxReport;

procedure ExploreSheet;
var
  SQL: string;
begin
  SQL := 'select * from buy$sheet where id = :id';

  Header.Data := DataBuy.Table(SQL, [ID]);

  SQL :=

  ' select m.name material, p.name probe, ii.weight$in, ii.weight$current$in, ii.weight$current$out, ii.weight$out ' +
  ' from buy$sheet$item ii, d_material m, d_probe p ' +
  ' where ii.sheet$id = :id and p.id = ii.probe$id and m.id = p.matid ' +
  ' order by m.name, p.probe ';


  Body.Data := DataBuy.Table(SQL, [ID]);

  SQL :=

  ' select sum(ii.weight$in * p.k) weight$in, sum(ii.weight$current$in * p.k) weight$current$in, sum(ii.weight$current$out * p.k) weight$current$out, sum(ii.weight$out * p.k) weight$out ' +
  ' from buy$sheet$item ii, d_material m, d_probe p ' +
  ' where ii.sheet$id = :id and p.id = ii.probe$id and m.id = p.matid ';

  Footer.Data := DataBuy.Table(SQL, [ID]);
end;

begin
  ID := 266;

  ReportClass := rcSheet;

  if ReportClass <> rcNone then
  begin
    if ID <> 0 then
    begin
      Report := nil;

      if ReportClass = rcSheet then
      begin
        Report := ReportSheet;

        ExploreSheet;
      end;

      if Report <> nil then
      begin
        Report.PrepareReport;

        Report.ShowPreparedReport;
      end;
    end;
  end;
end;

end.
