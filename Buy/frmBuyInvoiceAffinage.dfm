inherited FrameBuyInvoiceAffinage: TFrameBuyInvoiceAffinage
  Width = 907
  Height = 649
  inherited GroupBoxHeader: TcxGroupBox
    ExplicitWidth = 365
    ExplicitHeight = 304
    Height = 649
    Width = 365
    inherited Layout: TdxLayoutControl
      Height = 645
      Align = alLeft
      AutoSize = True
      ExplicitHeight = 300
      inherited LayoutEditorN: TcxDBTextEdit
        ExplicitWidth = 101
        Width = 101
      end
      inherited LayoutEditorState: TcxDBTextEdit
        ExplicitWidth = 68
        Width = 68
      end
      inherited LayoutEditorStateDate: TcxDBTextEdit
        Left = 161
        ExplicitLeft = 161
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorStatePerson: TcxDBTextEdit
        ExplicitWidth = 229
        Width = 229
      end
      inherited LayoutEditorDepartment: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorAgent: TcxDBTextEdit
        ExplicitWidth = 316
        Width = 316
      end
      inherited LayoutEditorCompany: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorForeignN: TcxDBTextEdit
        ExplicitWidth = 75
        Width = 75
      end
      inherited LayoutEditorForeignDate: TcxDBDateEdit
        Left = 210
        ExplicitLeft = 210
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorForeignCost: TcxDBTextEdit
        ExplicitWidth = 78
        Width = 78
      end
      inherited LayoutEditorDate: TcxDBDateEdit
        Left = 218
        ExplicitLeft = 218
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorAgentButton: TcxButton
        Left = 303
        ExplicitLeft = 303
      end
      inherited LayoutGroupRoot: TdxLayoutGroup
        inherited LayoutGroup5: TdxLayoutGroup
          inherited LayoutGroup6: TdxLayoutGroup
            inherited LayoutGroupAgent: TdxLayoutGroup
              inherited LayoutGroupForeign: TdxLayoutGroup
                CaptionOptions.Text = #1042#1085#1077#1096#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
                inherited LayoutGroup3: TdxLayoutGroup
                  inherited LayoutItem3: TdxLayoutItem
                    AlignHorz = ahLeft
                  end
                end
                inherited LayoutItemForeignCost: TdxLayoutItem
                  Visible = False
                end
              end
            end
          end
        end
      end
    end
  end
  inherited SplitterLeft: TcxSplitter
    Left = 365
    Height = 649
    ExplicitLeft = 365
  end
  inherited GroupBoxContent: TcxGroupBox
    Left = 373
    ExplicitLeft = 373
    ExplicitWidth = 78
    ExplicitHeight = 304
    Height = 649
    Width = 534
    inherited PageContent: TcxPageControl
      Width = 530
      Height = 645
      ImageBorder = 4
      Options = [pcoAlwaysShowGoDialogButton, pcoGradient, pcoGradientClientArea, pcoNoArrows, pcoRedrawOnResize]
      ExplicitWidth = 74
      ClientRectBottom = 596
      ClientRectRight = 530
      inherited SheetContent: TcxTabSheet
        ExplicitWidth = 74
        ExplicitHeight = 251
        inherited BarDock: TdxBarDockControl
          Width = 522
          ExplicitWidth = 66
        end
        inherited GridInvoiceItem: TcxGrid
          Width = 530
          Height = 257
          ExplicitWidth = 530
          ExplicitHeight = 257
        end
        inherited SplitterTop: TcxSplitter
          Top = 317
          Width = 530
          ExplicitTop = 317
          ExplicitWidth = 530
        end
        inherited GridStore: TcxGrid
          Top = 325
          Width = 530
          ExplicitTop = 325
          ExplicitWidth = 530
        end
      end
      object SheetChain: TcxTabSheet
        ImageIndex = 1
        ExplicitWidth = 74
        ExplicitHeight = 251
        object GridChainOut: TcxGrid
          Left = 0
          Top = 60
          Width = 497
          Height = 536
          Align = alLeft
          BevelEdges = [beTop]
          BevelKind = bkFlat
          BorderStyle = cxcbsNone
          TabOrder = 0
          TabStop = False
          LookAndFeel.NativeStyle = True
          ExplicitHeight = 191
          object GridChainOutView: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            OnCellClick = GridChainOutViewCellClick
            DataController.DataSource = SourceChainOut
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.BandMoving = False
            OptionsCustomize.ColumnVertSizing = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.NoDataToDisplayInfoText = '...'
            OptionsView.ScrollBars = ssVertical
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            OptionsView.BandHeaderLineCount = 2
            Bands = <
              item
                Caption = #1055#1077#1088#1077#1076#1072#1095#1072
              end>
            object GridChainOutViewMark: TcxGridDBBandedColumn
              DataBinding.FieldName = 'mark'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssInactive
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Width = 36
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
              IsCaptionAssigned = True
            end
            object GridChainOutViewDate: TcxGridDBBandedColumn
              Caption = #1044#1072#1090#1072
              DataBinding.FieldName = 'invoice$date'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              SortIndex = 0
              SortOrder = soDescending
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object GridChainOutViewN: TcxGridDBBandedColumn
              Caption = #8470
              DataBinding.FieldName = 'n'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 75
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object GridChainOutViewOffice: TcxGridDBBandedColumn
              Caption = #1057#1082#1083#1072#1076
              DataBinding.FieldName = 'department$name'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 175
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object GridChainOutViewWeight: TcxGridDBBandedColumn
              Caption = #1043#1088#1103#1079#1085#1099#1081
              DataBinding.FieldName = 'weight$real'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
          end
          object GridChainOutLevel: TcxGridLevel
            GridView = GridChainOutView
          end
        end
        object GridChainIn: TcxGrid
          Left = 33
          Top = 60
          Width = 497
          Height = 536
          Align = alRight
          BevelEdges = [beLeft, beTop]
          BevelKind = bkFlat
          BorderStyle = cxcbsNone
          TabOrder = 1
          TabStop = False
          LookAndFeel.NativeStyle = True
          ExplicitLeft = -423
          ExplicitHeight = 191
          object GridChainInView: TcxGridDBBandedTableView
            NavigatorButtons.ConfirmDelete = False
            OnCellClick = GridChainInViewCellClick
            DataController.DataSource = SourceChainIn
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnSorting = False
            OptionsCustomize.BandMoving = False
            OptionsCustomize.ColumnVertSizing = False
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.CellSelect = False
            OptionsView.NoDataToDisplayInfoText = '...'
            OptionsView.ScrollBars = ssVertical
            OptionsView.GroupByBox = False
            OptionsView.Indicator = True
            OptionsView.BandHeaderLineCount = 2
            Bands = <
              item
                Caption = #1055#1088#1080#1077#1084
              end>
            object GridChainInViewMark: TcxGridDBBandedColumn
              DataBinding.FieldName = 'mark'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.AllowGrayed = True
              Properties.NullStyle = nssInactive
              Properties.ValueChecked = '1'
              Properties.ValueUnchecked = '0'
              Width = 36
              Position.BandIndex = 0
              Position.ColIndex = 0
              Position.RowIndex = 0
              IsCaptionAssigned = True
            end
            object cxGridDBBandedColumn2: TcxGridDBBandedColumn
              Caption = #1044#1072#1090#1072
              DataBinding.FieldName = 'invoice$date'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              SortIndex = 0
              SortOrder = soDescending
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 1
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn3: TcxGridDBBandedColumn
              Caption = #8470
              DataBinding.FieldName = 'n'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 75
              Position.BandIndex = 0
              Position.ColIndex = 2
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn4: TcxGridDBBandedColumn
              Caption = #1057#1082#1083#1072#1076
              DataBinding.FieldName = 'department$name'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 175
              Position.BandIndex = 0
              Position.ColIndex = 3
              Position.RowIndex = 0
            end
            object cxGridDBBandedColumn5: TcxGridDBBandedColumn
              Caption = '1000'
              DataBinding.FieldName = 'weight$real'
              HeaderAlignmentHorz = taCenter
              Options.Editing = False
              Width = 85
              Position.BandIndex = 0
              Position.ColIndex = 4
              Position.RowIndex = 0
            end
          end
          object GridChainInLevel: TcxGridLevel
            GridView = GridChainInView
          end
        end
        object BarExitDock: TdxBarDockControl
          AlignWithMargins = True
          Left = 4
          Top = 4
          Width = 522
          Height = 52
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = dalTop
          BarManager = BarManager
          ExplicitWidth = 66
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    Top = 544
    DockControlHeights = (
      0
      0
      0
      0)
    inherited Bar: TdxBar
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAddGold'
        end
        item
          Visible = True
          ItemName = 'ButtonAddSilver'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          Visible = True
          ItemName = 'LargeButtonClose'
        end>
    end
    object BarExit: TdxBar [1]
      BorderStyle = bbsNone
      Caption = 'Exit'
      CaptionButtons = <>
      DockControl = BarExitDock
      DockedDockControl = BarExitDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 997
      FloatTop = 0
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'LargeButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    inherited LargeButtonClose: TdxBarLargeButton
      ImageIndex = 9
    end
  end
  inherited InvoiceItem: TpFIBDataSet
    Left = 248
    Top = 584
  end
  inherited SourceInvoiceItem: TDataSource
    Left = 184
    Top = 584
  end
  inherited Invoice: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost,'
      '  g.group$id'
      ''
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' ) i'
      ''
      'left join buy$invoice g on g.id = i.id'
      '')
    SelectSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost,'
      '  g.group$id'
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' ) i'
      ''
      'left join buy$invoice g on g.id = i.id'
      ''
      '')
    Top = 512
    inherited InvoiceFOREIGNCOST: TFIBBCDField
      Alignment = taRightJustify
    end
    object InvoiceGROUPID: TFIBIntegerField
      FieldName = 'GROUP$ID'
    end
  end
  inherited SourceInvoice: TDataSource
    Top = 512
  end
  inherited LayoutLookAndFeel: TdxLayoutLookAndFeelList
    Left = 216
    Top = 584
  end
  inherited Action: TActionList
    Top = 512
  end
  inherited MenuProbeGold: TdxBarPopupMenu
    Top = 544
  end
  inherited MenuOffice: TdxBarPopupMenu
    Top = 544
  end
  inherited MenuCompany: TdxBarPopupMenu
    Top = 544
  end
  inherited MenuMemoGold: TdxBarPopupMenu
    Top = 544
  end
  inherited CurrentStore: TClientDataSet
    Left = 144
    Top = 584
  end
  inherited Store: TClientDataSet
    Left = 136
    Top = 512
  end
  inherited SourceStore: TDataSource
    Left = 104
    Top = 512
  end
  inherited MenuProbeSilver: TdxBarPopupMenu
    Top = 544
  end
  object SourceChainOut: TDataSource
    DataSet = ChainOut
    Left = 80
    Top = 584
  end
  object ChainOut: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 584
  end
  object ChainIn: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 616
  end
  object SourceChainIn: TDataSource
    DataSet = ChainIn
    Left = 80
    Top = 616
  end
end
