inherited FrameBuyInvoiceRecycling: TFrameBuyInvoiceRecycling
  Width = 659
  Height = 484
  inherited GroupBoxHeader: TcxGroupBox
    ExplicitHeight = 304
    Height = 484
    inherited Layout: TdxLayoutControl
      Height = 480
      ExplicitHeight = 300
      inherited LayoutEditorN: TcxDBTextEdit
        ExplicitWidth = 101
        Width = 101
      end
      inherited LayoutEditorState: TcxDBTextEdit
        ExplicitWidth = 68
        Width = 68
      end
      inherited LayoutEditorStateDate: TcxDBTextEdit
        Left = 161
        ExplicitLeft = 161
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorStatePerson: TcxDBTextEdit
        ExplicitWidth = 229
        Width = 229
      end
      inherited LayoutEditorDepartment: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorAgent: TcxDBTextEdit
        ExplicitWidth = 187
        Width = 187
      end
      inherited LayoutEditorCompany: TcxDBTextEdit
        ExplicitWidth = 257
        Width = 257
      end
      inherited LayoutEditorForeignN: TcxDBTextEdit
        ExplicitWidth = 75
        Width = 75
      end
      inherited LayoutEditorForeignDate: TcxDBDateEdit
        Left = 208
        ExplicitLeft = 208
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorDate: TcxDBDateEdit
        Left = 218
        ExplicitLeft = 218
        ExplicitWidth = 121
        Width = 121
      end
      inherited LayoutEditorAgentButton: TcxButton
        Left = 303
        ExplicitLeft = 303
      end
      inherited LayoutGroupRoot: TdxLayoutGroup
        inherited LayoutGroup5: TdxLayoutGroup
          inherited LayoutGroup6: TdxLayoutGroup
            inherited LayoutGroupAgent: TdxLayoutGroup
              inherited LayoutGroupForeign: TdxLayoutGroup
                Visible = True
              end
            end
          end
        end
      end
    end
  end
  inherited SplitterLeft: TcxSplitter
    Height = 484
  end
  inherited GroupBoxContent: TcxGroupBox
    ExplicitWidth = 74
    ExplicitHeight = 304
    Height = 484
    Width = 282
    inherited PageContent: TcxPageControl
      Width = 278
      Height = 480
      ClientRectBottom = 439
      ClientRectRight = 278
      inherited SheetContent: TcxTabSheet
        inherited BarDock: TdxBarDockControl
          Width = 270
        end
        inherited GridInvoiceItem: TcxGrid
          Width = 278
          Height = 100
          ExplicitHeight = 7
        end
        inherited SplitterTop: TcxSplitter
          Top = 160
          Width = 278
        end
        inherited GridStore: TcxGrid
          Top = 168
          Width = 278
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited LargeButtonClose: TdxBarLargeButton
      ImageIndex = 9
    end
  end
end
