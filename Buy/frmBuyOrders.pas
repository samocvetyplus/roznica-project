unit frmBuyOrders;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinOffice2007Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxSkinsdxBarPainter,
  ActnList, dxBar, frmPopup,
  pFIBDataSet, FIBDataSet, FIBQuery, FIBDatabase, pFIBDatabase, cxPC,
  pFIBQuery, cxCheckComboBox, cxLabel, cxBarEditItem, cxDropDownEdit,
  cxGridBandedTableView, cxGridDBBandedTableView, dxSkinsDefaultPainters,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin;

type
//  TSheetViewMode = (vmOrder, vmDictionary);

  TFrameBuyOrders = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonOpen: TdxBarLargeButton;
    ButtonView: TdxBarLargeButton;
    ActionList: TActionList;
    acAdd: TAction;
    acDelete: TAction;
    acView: TAction;
    acClose: TAction;
    acOpen: TAction;
    acPrintOrder: TAction;
    acChangeRange: TAction;
    ButtonClose: TdxBarLargeButton;
    ButtonPrint: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    ButtonDepartment: TdxBarLargeButton;
    DepartmentsPopupMenu: TdxBarPopupMenu;
    acRefresh: TAction;
    ButtonRefresh: TdxBarLargeButton;
    GridView: TcxGridDBBandedTableView;
    GridViewColumn1: TcxGridDBBandedColumn;
    GridViewColumn2: TcxGridDBBandedColumn;
    GridViewColumn5: TcxGridDBBandedColumn;
    GridViewColumn6: TcxGridDBBandedColumn;
    GridViewColumn4: TcxGridDBBandedColumn;
    GridViewColumn3: TcxGridDBBandedColumn;
    GridViewColumn7: TcxGridDBBandedColumn;
    ButtonSettings: TdxBarLargeButton;
    acShowDictionary: TAction;
    ButtonPrintOrder: TdxBarButton;
    ButtonPrintOrderExchange: TdxBarButton;
    PrintPopupMenu: TdxBarPopupMenu;
    BarFilter: TdxBar;
    GridViewORDERSTATECODE: TcxGridDBBandedColumn;
    procedure acAddExecute(Sender: TObject);
    procedure acChangeRangeExecute(Sender: TObject);
    procedure DepartmentMenuButtonClick(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDeleteUpdate(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acOpenUpdate(Sender: TObject);
    procedure acCloseUpdate(Sender: TObject);
    procedure acPrintOrderExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure ButtonDepartmentClick(Sender: TObject);
    procedure acPrintOrderUpdate(Sender: TObject);
    procedure PrintButtonClick(Sender: TObject);
    procedure GridViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GridViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
  private
    function GetSelectedDep: Integer;
    procedure FillDepartments;
    procedure OnRangeOk(Popup: TPopup);
    procedure RepaintBandCaption(Band: TcxGridBand);
  public
    procedure Activate;
    procedure Deactivate;
  end;

implementation

{$R *.dfm}

uses
  dmBuyOrders, frmBuyRangePopup, DateUtils,
  frmBuyOrder, uDialog, uBuy, dmBuy;

procedure TFrameBuyOrders.FillDepartments;
var
  Button: TdxBarButton;
  ButtonLink: TdxBarItemLink;
  TextSQL: String;
begin
  with BuyOrdersData.quTemp do
    begin
      Close;

      TextSQL := 'select d_depid, coalesce(sname,' + #39 + '  ���  ' + #39 + ') from d_dep where d_depid <> 1';

      if not BuyOrdersData.IsCenter then
      begin
        TextSQL := TextSQL + ' and d_depid = ' + IntToStr(BuyOrdersData.SelfDepID);
      end;

      SQL.Text := TextSQL;

      ExecQuery;

      while not EOF do
        begin
          Button := TdxBarButton(BarManager.AddItem(TdxBarButton));

          Button.ButtonStyle := bsChecked;

          Button.GroupIndex := 6;

          Button.LargeImageIndex := ButtonDepartment.LargeImageIndex;

          Button.Tag := Fields[0].AsInteger;

          Button.Caption := trim(Fields[1].AsString);

          Button.OnClick := DepartmentMenuButtonClick;

          ButtonLink := DepartmentsPopupMenu.ItemLinks.Add;

          ButtonLink.Item := Button;

          if DepartmentsPopupMenu.Itemlinks.Count = 2 then
          begin
            ButtonLink.BeginGroup := True;
          end;

          if BuyOrdersData.IsCenter then
            begin
              if Button.Tag = -1000 then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end
          else
            begin
              if Button.Tag = BuyOrdersData.SelfDepID then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end;

          Next;
        end;
    end;

end;


procedure TFrameBuyOrders.ButtonDepartmentClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonDepartment then
    begin
      GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonDepartment.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      DepartmentsPopupMenu.Popup(X, Y);
    end;
  end;
end;

procedure TFrameBuyOrders.DepartmentMenuButtonClick(Sender: TObject);
var Button: TdxBarButton;
begin
  if Sender is TdxBarButton then
    begin
      Button := TdxBarButton(Sender);
      ButtonDepartment.Caption := Button.Caption;
      ButtonDepartment.Tag := Button.Tag;
      BuyOrdersData.SelectedDep := GetSelectedDep;

      if not BuyOrdersData.IsCenter then
      begin
        Exit;
      end;


      with BuyOrdersData.OrdersList do
        begin
          DisableControls;
          DisableScrollEvents;

          if Filtered then
            begin
              Filtered := false;
            end;

          if ButtonDepartment.Tag <> -1000 then
            begin
              Filter := 'DepartmentID = ' + IntToStr(ButtonDepartment.Tag);
              Filtered := true;
            end;

          EnableControls;
          EnableScrollEvents;
        end;
    end;
end;

procedure TFrameBuyOrders.PrintButtonClick(Sender: TObject);
var Button: TdxBarButton;
begin
  if Sender is TdxBarButton then
  begin
    Button := TdxBarButton(Sender);

    if (Button = ButtonPrintOrder) or (Button = ButtonPrintOrderExchange) then
    begin
      acPrintOrder.Tag := Button.Tag;

      acPrintOrder.Execute;
    end;

  end;

end;

function TFrameBuyOrders.GetSelectedDep: Integer;
begin
  Result := ButtonDepartment.Tag;
end;

procedure TFrameBuyOrders.RepaintBandCaption(Band: TcxGridBand);
var
  RangeBegin: TDate;
  RangeEnd: TDate;
  Caption: String;
begin
  RangeBegin := BuyOrdersData.RangeStart;

  RangeEnd   := BuyOrdersData.RangeEnd;

  {

  Caption := '';

  if (Rangebegin = RangeEnd) then
  begin
    Caption := Caption + dateToStr(RangeBegin);
  end else
  begin
    Caption := Caption + dateToStr(RangeBegin) + ' � ' + dateToStr(RangeEnd);
  end;

  Band.Caption := Caption;

  }

  ButtonRange.Caption := DateRangeAsString(RangeBegin, RangeEnd);
end;

procedure TFrameBuyOrders.GridViewCellDblClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  acView.Execute;
end;

procedure TFrameBuyOrders.GridViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    begin
      acView.Execute;
    end;
end;

procedure TFrameBuyOrders.GridViewStylesGetContentStyle( Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  iValue: Integer;
  Value: Variant;
begin
  AStyle := DataBuy.StyleDefault;

  if AItem <> nil then
  begin
    if AItem.Tag = 1 then
    begin
      Index := GridViewORDERSTATECODE.Index;

      Value := ARecord.Values[Index];

      if VarIsNull(Value) then
      begin
        iValue := 0;
      end else
      begin
        iValue := Value;
      end;

      if iValue = 2 then
      begin
        AStyle := DataBuy.StyleGray;
      end else

      begin
        AStyle := DataBuy.StyleInfo;
      end;

    end else

    begin
      if ARecord.Selected then
      begin
        AStyle := DataBuy.StyleSelected;
      end;
    end;

  end else
  begin
    if ARecord.Selected then
    begin
      AStyle := DataBuy.StyleSelected;
    end;
  end;
end;

procedure TFrameBuyOrders.acAddUpdate(Sender: TObject);
var ActionEnabled: Boolean;
begin
with BuyOrdersData do
  begin
    ActionEnabled := true;
    ActionEnabled := ActionEnabled and IsCenter;
    if ActionEnabled then
      ActionEnabled := ActionEnabled and (not VarIsNull(UserID));
      //TODO: ��������� ����� ������ �������� ��� �������������?
  end;
acAdd.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acChangeRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := BuyOrdersData.RangeStart;

  Dialog.RangeEnd := BuyOrdersData.RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);
end;

procedure TFrameBuyOrders.acCloseExecute(Sender: TObject);
var
  DialogResult: TModalResult;
  CloseCode: Integer;
begin
  with BuyOrdersData, OrdersList do
    begin

      with quTemp do
      begin
        Close;

        SQL.Text := 'select can$close, prior$date, prior$number from buy$order$can$close(:Order$ID)';

        ParamByName('Order$ID').AsInteger := OrdersListORDERID.AsInteger;

        ExecQuery;

        if not Fields[0].AsBoolean then
        begin
          TDialog.Warning('������� ������ �� ����� ���� ������ ������' + #13#10 +
                          '������� � ' + Fields[2].AsString + ' �� ' + Fields[1].AsString);
          Exit;
        end;

      end;

      if IsCenter then
        begin
          CloseCode := 2
        end
      else
        begin
          CloseCode := 4;
        end;


      if State in [dsEdit, dsInsert] then
         begin
           DialogResult := TDialog.Confirmation('������� ���������������� ���������.'+ #13#10 + '���������?');

           if DialogResult = mrYes then
             begin
               Post
             end
           else
             if DialogResult = mrNo then
               begin
                 Cancel;
               end;
         end;

      DialogResult := TDialog.Confirmation('������� ������?');

      if DialogResult = mrYes then
        begin
          try
            Edit;
            FieldByName('Order$State$Code').AsInteger := CloseCode;
            Post;
          except

          end;
        end;
    end;
end;

procedure TFrameBuyOrders.acCloseUpdate(Sender: TObject);
var
  ActionEnabled: Boolean;
  OrderStateCode: Integer;
begin
with BuyOrdersData do
  begin
    OrderStateCode := OrdersListORDERSTATECODE.AsInteger;

    ActionEnabled := true;
    ActionEnabled := ActionEnabled and (Not OrdersList.IsEmpty);

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (((OrderStateCode = 1) and IsCenter) or ((OrderStateCode = 3) and not IsCenter));

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (Not VarIsNull(UserID));
  end;
    //TODO: ��������� ����� ������ �������� ��� �������������?
  acClose.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acDeleteExecute(Sender: TObject);
var
  DialogResult: TModalResult;
begin
  with BuyOrdersData.OrdersList do
    begin
      if Active then
        begin
          if State in [dsEdit, dsInsert] then
            begin
              DialogResult := TDialog.Confirmation('������� ���������������� ���������.'+ #13#10 + '���������?');
              if DialogResult = mrYes then Post
              else
                if DialogResult = mrNo then Cancel;
            end;
        end;

      DialogResult := TDialog.Confirmation('������� ������?');

      if DialogResult = mrYes  then
        begin
          try
            Delete;
          except
            on E:Exception do
              begin
                TDialog.Error('������ ��� �������� �������: ' + E.Message);
                Cancel;
              end;
          end;

        acRefresh.Execute;

        end;
    end;
end;

procedure TFrameBuyOrders.acDeleteUpdate(Sender: TObject);
var
  ActionEnabled: Boolean;
begin
with BuyOrdersData do
  begin
    ActionEnabled := true;
    ActionEnabled := ActionEnabled and (not OrdersList.IsEmpty);

    if ActionEnabled then
      ActionEnabled := ActionEnabled and IsCenter;

    if ActionEnabled then
        ActionEnabled := ActionEnabled and (not VarIsNull(UserID));

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (OrdersListORDERSTATECODE.AsInteger = 1);
    //TODO: ������� ����� ������ �������� ��� �������������?
  end;
acDelete.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acOpenExecute(Sender: TObject);
var
  DialogResult: TModalResult;
begin
  with BuyOrdersData.OrdersList do
    begin

      if State in [dsEdit, dsInsert] then
        begin
          DialogResult := TDialog.Confirmation('������� ���������������� ���������.'+ #13#10 + '���������?');
          if DialogResult = mrYes then
            begin
              Post
            end
          else
            if DialogResult = mrNo then
              begin
                Cancel;
              end;
        end;

      DialogResult := TDialog.Confirmation('������� ������?');

      if DialogResult = mrYes then
        begin
          try
            Edit;
            FieldByName('Order$State$Code').AsInteger := 1;
            Post;
            Refresh;
          except

          end;
        end;
    end;
end;

procedure TFrameBuyOrders.acOpenUpdate(Sender: TObject);
var
  ActionEnabled: Boolean;
begin
with BuyOrdersData do
  begin
    ActionEnabled := true;
    ActionEnabled := ActionEnabled and IsCenter;

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (Not OrdersList.IsEmpty);

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (OrdersListORDERSTATECODE.AsInteger in [2, 3]);

    if ActionEnabled then
      ActionEnabled := ActionEnabled and (Not VarIsNull(UserID));
  end;

acOpen.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acPrintOrderExecute(Sender: TObject);
var TemplateID: Integer;
begin
  if Sender is TAction then
  begin
    TemplateID := TAction(Sender).Tag;
    BuyOrdersData.PreparePrint(TemplateID);
  end;
end;

procedure TFrameBuyOrders.acPrintOrderUpdate(Sender: TObject);
var ActionEnabled: boolean;
begin
with BuyOrdersData do
  begin
    ActionEnabled := true;
    ActionEnabled := ActionEnabled and (Not OrdersList.IsEmpty);
  end;
  acOpen.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acRefreshExecute(Sender: TObject);
var
  SavedPosition: Integer;
begin
  SavedPosition := 0;
  with BuyOrdersData.OrdersList do
    begin

      if not IsEmpty then
        SavedPosition := FieldByName('ORDER$ID').AsInteger;

      DisableControls;

      if State in [dsEdit, dsInsert] then Post;

      Screen.Cursor := crHourGlass;

      Active := false;

      try
        Active := true;
      except
        On E:Exception do
          TDialog.Error('������ ��� ���������� ������ ��������: ' + E.Message);
      end;

      Screen.Cursor := crDefault;

      if ((not IsEmpty) and (SavedPosition > 0)) then
        Locate('ORDER$ID', SavedPosition, [])
      else
        First;

      EnableControls;
    end;
end;

procedure TFrameBuyOrders.Activate;
begin
  FillDepartments;

  BuyOrdersData.OrdersList.Active := true;

  RepaintBandCaption(GridView.Bands[0]);
end;


procedure TFrameBuyOrders.acViewExecute(Sender: TObject);
var
  Sheet: TcxTabSheet;
  CurrentSheet : TcxTabSheet;
  PageControl: TcxPageControl;
  PageIndex: Integer;
  PageCount: Integer;
  OrderFrame: TFrameBuyOrder;
  BuyOrderID: Integer;
  OrderNumber: Integer;
  Flag: Boolean;
begin
  Flag := false;

  with BuyOrdersData do
    begin
      BuyOrderID := OrdersListORDERID.AsInteger;
      OrderNumber := OrdersListORDERNUMBER.AsInteger;
    end;

  if Owner is TcxTabSheet then
    begin
      Sheet := TcxTabSheet(Owner);
      PageControl := Sheet.PageControl;
      PageCount := PageControl.PageCount;
      PageIndex := 1;
      CurrentSheet := nil;

      while PageIndex <= PageCount - 1 do
        begin
          CurrentSheet := PageControl.Pages[PageIndex];

          if CurrentSheet.Tag = BuyOrderID then
          begin
            Flag := true;
            break;
          end;

          Inc(PageIndex);
        end;

      if Flag then
      begin
        PageControl.ActivePage := CurrentSheet
      end
      else
        begin
          Sheet := TcxTabSheet.Create(PageControl);
          Sheet.TabVisible := false;
          Sheet.PageControl := PageControl;
          Sheet.Caption := IntToStr(OrderNumber);
          Sheet.Tag := BuyOrderID;

          OrderFrame := TFrameBuyOrder.Create(Sheet);
          OrderFrame.Align := alClient;
          OrderFrame.Parent := Sheet;
          OrderFrame.Tag := BuyOrderID;
          OrderFrame.Activate;

          PageIndex := 1;
          PageCount := PageControl.PageCount;

          while PageIndex <= PageCount - 1 do
          begin
            CurrentSheet := PageControl.Pages[PageIndex];

            if Sheet <> CurrentSheet then
            begin
              if StrToInt(CurrentSheet.Caption) > OrderNumber then
              begin
                Flag := true;
                break;
              end;
            end;

            Inc(PageIndex);
          end;

          Sheet.Caption := IntToStr(OrderNumber);

          if Flag then
            begin
              Sheet.PageIndex := PageIndex;
            end;

          Sheet.TabVisible := true;
          PageControl.ActivePage := Sheet;

        end;
    end;
end;

procedure TFrameBuyOrders.acViewUpdate(Sender: TObject);
var
  ActionEnabled: Boolean;
begin
with BuyOrdersData do
  begin
    ActionEnabled := true;
    ActionEnabled := ActionEnabled and (not OrdersList.IsEmpty);
  end;
acView.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrders.acAddExecute(Sender: TObject);
var
  DialogResult: TModalResult;
begin
with BuyOrdersData.OrdersList do
  begin

    if State in [dsEdit, dsInsert] then
      begin
        DialogResult := TDialog.Confirmation('������� ���������������� ���������.'+ #13#10 + '���������?');

        if DialogResult = mrYes then
          begin
            Post
          end
        else
          if DialogResult = mrNo then
            begin
              Cancel;
            end
      end;

    try
      Insert;
      Post;
    except
      on E:Exception do
        begin
          TDialog.Error('������ ��� �������� �������:' + E.Message);
          Cancel;
        end;
    end;

    acRefresh.Execute;

  end;
end;



procedure TFrameBuyOrders.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  BuyOrdersData.RangeStart := Dialog.RangeBegin;

  BuyOrdersData.RangeEnd := Dialog.RangeEnd;

  acRefresh.Execute;

  RepaintBandCaption(GridView.Bands[0]);

  GridView.DataController.SelectAll
end;

procedure TFrameBuyOrders.Deactivate;
begin
  //
end;

end.
