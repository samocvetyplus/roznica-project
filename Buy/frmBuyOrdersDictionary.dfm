object FrameBuyDictionary: TFrameBuyDictionary
  Left = 0
  Top = 0
  Width = 598
  Height = 448
  TabOrder = 0
  object Grid: TcxGrid
    Left = 0
    Top = 52
    Width = 598
    Height = 396
    Align = alClient
    TabOrder = 4
    object GridView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = BuyOrdersData.DictionaryDataSource
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      Bands = <
        item
          Caption = #1069#1083#1077#1084#1077#1085#1090' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
          Width = 452
        end>
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    LookAndFeel.SkinName = 'Office2007Blue'
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 1008
    Top = 80
    DockControlHeights = (
      0
      0
      52
      0)
    object Bar: TdxBar
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 799
      FloatTop = 0
      FloatClientWidth = 51
      FloatClientHeight = 125
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAdd'
        end
        item
          Visible = True
          ItemName = 'ButtonDelete'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonExit'
        end>
      OneOnRow = True
      RotateWhenVertical = False
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object ButtonAdd: TdxBarLargeButton
      Action = acAdd
      Category = 0
      AutoGrayScale = False
    end
    object ButtonDelete: TdxBarLargeButton
      Action = acDelete
      Category = 0
      AutoGrayScale = False
    end
    object ButtonExit: TdxBarLargeButton
      Action = acExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
  end
  object ActionList: TActionList
    Images = DataBuy.Image32
    Left = 1008
    Top = 112
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddExecute
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDeleteExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 8
      OnExecute = acExitExecute
    end
  end
end
