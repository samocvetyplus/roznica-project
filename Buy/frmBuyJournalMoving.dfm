inherited FrameBuyJournalMoving: TFrameBuyJournalMoving
  inherited GroupBoxJournal: TcxGroupBox
    inherited GridJournal: TcxGrid
      inherited GridJournalView: TcxGridDBBandedTableView
        Bands = <
          item
            Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          end
          item
            Caption = #1042#1085#1077#1096#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
            Visible = False
            Width = 207
          end
          item
            Caption = #1042#1077#1089
            Options.HoldOwnColumnsOnly = True
          end
          item
            Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Options.HoldOwnColumnsOnly = True
          end>
        inherited GridJournalViewCOSTTOTAL: TcxGridDBBandedColumn
          Visible = False
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      0
      0
      0
      0
      0
      0
      0
      0
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited ButtonOffice: TdxBarLargeButton
      ImageIndex = -1
    end
    object ButtonPrint102: TdxBarLargeButton [14]
      Tag = 102
      Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Category = 6
      Hint = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Visible = ivAlways
      OnClick = ButtonPrintClick
    end
    inherited ButtonSend: TdxBarLargeButton
      Down = True
      ImageIndex = 17
    end
  end
  inherited Action: TActionList
    inherited ActionAdd: TAction
      Caption = #1055#1077#1088#1077#1076#1072#1095#1072
    end
  end
  inherited MenuPrint: TdxBarPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonPrint102'
      end>
  end
end
