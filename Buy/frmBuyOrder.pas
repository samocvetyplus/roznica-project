unit frmBuyOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinOffice2007Blue, cxStyles, cxEdit, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxBar, cxInplaceContainer, cxVGrid,
  ActnList, cxContainer, cxGroupBox, FIBDataSet, pFIBDataSet, cxPC,
  cxGridBandedTableView, cxGridDBBandedTableView, DBClient, Provider, cxDBVGrid,
  cxDBLookupComboBox, 
  dxSkinsDefaultPainters, cxCalendar;

type
  TFrameBuyOrder = class(TFrame)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ActionList: TActionList;
    acAdd: TAction;
    acDelete: TAction;
    GroupBox: TcxGroupBox;
    Grid: TcxGrid;
    GridLevel: TcxGridLevel;
    BarDockControl: TdxBarDockControl;
    GridView: TcxGridDBBandedTableView;
    GridViewProbe: TcxGridDBBandedColumn;
    GridViewMemo: TcxGridDBBandedColumn;
    GridViewColumn4: TcxGridDBBandedColumn;
    GridViewColumn5: TcxGridDBBandedColumn;
    VerticalGrid: TcxDBVerticalGrid;
    VerticalGridCategoryRow1: TcxCategoryRow;
    VerticalGridDBEditorRow1: TcxDBEditorRow;
    VerticalGridDateRow: TcxDBEditorRow;
    VerticalGridCategoryRow2: TcxCategoryRow;
    VerticalGridDBEditorRow3: TcxDBEditorRow;
    VerticalGridDBEditorRow4: TcxDBEditorRow;
    VerticalGridDBEditorRow5: TcxDBEditorRow;
    VerticalGridCategoryRow3: TcxCategoryRow;
    VerticalGridDBEditorRow6: TcxDBEditorRow;
    ButtonExit: TdxBarLargeButton;
    acExit: TAction;
    procedure acAddExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDeleteUpdate(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
  private
    OrderID: integer;
    AReadOnly: boolean;
  public
    function Activate: boolean;
    procedure Deactivate;
    property ID:integer read OrderID write OrderID;
  end;

implementation

{$R *.dfm}

uses ComData, dmBuyOrders, uDialog;

procedure TFrameBuyOrder.acAddExecute(Sender: TObject);
begin
  with BuyOrdersData, Order do
    begin
      try
        Insert;
      except
        On  E:Exception do
          begin
            TDialog.Error('������ ���������� �������: ' + E.Message);
            Cancel;
          end;
      end;
    end;
end;

procedure TFrameBuyOrder.acAddUpdate(Sender: TObject);
var ActionEnabled: boolean;
begin
with BuyOrdersData do
begin
  ActionEnabled := true;
  ActionEnabled := not AReadOnly;
//  ActionEnabled := ActionEnabled and IsCenter;
//  if ActionEnabled then
//    ActionEnabled := ActionEnabled and (OrdersListORDERSTATECODE.AsInteger = 1);
end;
  acAdd.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrder.acDeleteExecute(Sender: TObject);
begin
  with BuyOrdersData, Order do
    begin
      if TDialog.Confirmation('������� ������?') = mrYes then
        begin
          try
            Delete;
          except
            On  E:Exception do
              begin
                TDialog.Error('������ �������� �������: ' + E.Message);
                Cancel;
              end;
          end;
        end;
    end;
end;

procedure TFrameBuyOrder.acDeleteUpdate(Sender: TObject);
var
  ActionEnabled: boolean;
begin
with BuyOrdersData do
begin
  ActionEnabled := true;
  ActionEnabled := not AReadOnly;
//  ActionEnabled := ActionEnabled and IsCenter;
//  if ActionEnabled then
//    ActionEnabled := ActionEnabled and (OrdersListORDERSTATECODE.AsInteger = 1);
  if ActionEnabled then
    ActionEnabled := ActionEnabled and (not Order.IsEmpty);
end;
  acDelete.Enabled := ActionEnabled;
end;

procedure TFrameBuyOrder.acExitExecute(Sender: TObject);
var
  Sheet: TcxTabSheet;
  DialogResult: TModalResult;
  BookMark: Integer;
  ZeroPriceExists: boolean;
begin

  with BuyOrdersData.Order do
  begin

    if State in [dsEdit, dsInsert] then
    begin
      DialogResult := TDialog.Confirmation('������� ���������������� ���������.' + #13#10 + '���������?');

      if DialogResult = mrYes then
        begin
          Post;
        end
      else
        begin
          Cancel;
        end;
    end;
    

    ZeroPriceExists := false;

//    Bookmark := FieldByName('ID').AsInteger;

    DisableControls;

    ZeroPriceExists := Locate('Price', 0, []);

    if not ZeroPriceExists then
    begin
      ZeroPriceExists := Locate('Price2', 0, []);
    end;

//    if not ZeroPriceExists then
//    begin
//      Locate('ID', Bookmark, []);
//    end;

    EnableControls;
  end;

  if ZeroPriceExists then
  begin
    DialogResult := TDialog.Confirmation('���� �� ������ (�����) ����� ����!' + #13#10 + '����������?');

    if DialogResult = mrNo then
    begin
      Exit;
    end;
  end;

  if Owner is TcxTabSheet then
    begin
      Sheet := TcxTabSheet(Owner);
      FreeAndNil(Sheet);
    end;

end;

function TFrameBuyOrder.Activate: boolean;
begin
  Result := false;
  AReadOnly := false;

  AReadOnly := BuyOrdersData.OrdersListORDERSTATECODE.AsInteger <> 1;

  if not AReadOnly then
    begin
      AReadOnly := not BuyOrdersData.IsCenter;
    end;
  

//  GridViewMaterial.PropertiesClass := TcxLookupComboboxProperties;
//
//  with TcxLookupComboboxProperties(GridViewMaterial.Properties), BuyOrdersData do
//    begin
//      ListSource := DataSourceMaterial;
//      ListFieldNames := 'NAME';
//      KeyFieldNames := 'ID';
//      ListOptions.ShowHeader := false;
//    end;

  GridViewProbe.PropertiesClass := TcxLookupComboboxProperties;

  with TcxLookupComboboxProperties(GridViewProbe.Properties), BuyOrdersData do
    begin
      ListSource := DataSourceProbe;
      ListFieldNames := 'PROBE';
      KeyFieldNames := 'ID';
      ListOptions.ShowHeader := false;
    end;

  GridViewMemo.PropertiesClass := TcxLookupComboboxProperties;

  with TcxLookupComboboxProperties(GridViewMemo.Properties), BuyOrdersData do
    begin
      ListSource := DataSourceMemo;
      ListFieldNames := 'MEMO';
      KeyFieldNames := 'ID';
      ListOptions.ShowHeader := false;
    end;

  with GridView.OptionsData do
    begin
      Editing := not AReadOnly;
      Deleting := not AReadOnly;
      Inserting := not AReadOnly;
    end;

  VerticalGridDateRow.Properties.Options.Editing := not AReadOnly;

  try
    BuyOrdersData.Order.Active := true;
  finally
    Result := true;
  end;

end;



procedure TFrameBuyOrder.Deactivate;
begin
  //
end;


end.
