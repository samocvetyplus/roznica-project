unit frmBuySetup;

interface

uses

  Windows, Forms, SysUtils, Menus, Classes, ActnList, DB,
  StdCtrls, Controls, DBClient,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxButtons, cxMaskEdit, cxSpinEdit, cxDBEdit, cxTextEdit,
  dxLayoutControl, dxLayoutcxEditAdapters, dxLayoutLookAndFeels, dxSkinsCore,
  dxBar,
  FIBDataSet, pFIBDataSet, dxSkinsdxBarPainter, dxRibbonForm, dxRibbon,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters, dxSkinsdxRibbonPainter;

type

{ TDialogSetup }

  TDialogBuySetup = class(TdxRibbonForm)
    LayoutGroupRoot: TdxLayoutGroup;
    Layout: TdxLayoutControl;
    LayoutGroupCompany: TdxLayoutGroup;
    LayoutGroupRegister: TdxLayoutGroup;
    LayoutEditorModel: TcxDBTextEdit;
    LayoutItemModel: TdxLayoutItem;
    LayoutEditorProductionN: TcxDBTextEdit;
    LayoutItemProductionN: TdxLayoutItem;
    LayoutEditorRegisrationN: TcxDBTextEdit;
    LayoutItemRegistrationN: TdxLayoutItem;
    LayoutEditorCompany: TcxDBTextEdit;
    LayoutItemCompany: TdxLayoutItem;
    LayoutEditorAddress: TcxDBTextEdit;
    LayoutItemAddress: TdxLayoutItem;
    Setup: TpFIBDataSet;
    SetupCODE: TFIBIntegerField;
    SetupADDRESS: TFIBStringField;
    SetupFRMODEL: TFIBStringField;
    SetupFRPRODUCTIONNUMBER: TFIBStringField;
    SetupFRREGISTRATIONNUMBER: TFIBStringField;
    SetupCOMPANYID: TFIBIntegerField;
    SetupCOMPANYNAME: TFIBStringField;
    DataSource: TDataSource;
    LayoutEditorN: TcxDBSpinEdit;
    LayoutItemN: TdxLayoutItem;
    LayoutEditorCompanyButton: TcxButton;
    LayoutItemCompanyButton: TdxLayoutItem;
    LayoutGroupBuy: TdxLayoutGroup;
    Actions: TActionList;
    ActionOK: TAction;
    ActionCancel: TAction;
    LayoutLookAndFeelList: TdxLayoutLookAndFeelList;
    LayoutLookAndFeel: TdxLayoutCxLookAndFeel;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    LayoutButtonOK: TcxButton;
    LayoutItemOK: TdxLayoutItem;
    LayoutButtonCancel: TcxButton;
    LayoutItemCancel: TdxLayoutItem;
    LayoutGroupControl: TdxLayoutGroup;
    BarManager: TdxBarManager;
    ButtonOwner: TdxBarLargeButton;
    LayoutGroup2: TdxLayoutGroup;
    Ribbon: TdxRibbon;
    MenuOwner: TdxBarPopupMenu;
    procedure ActionOKUpdate(Sender: TObject);
    procedure ActionCancelUpdate(Sender: TObject);
    procedure ActionOKExecute(Sender: TObject);
    procedure ActionCancelExecute(Sender: TObject);
    procedure SetupBeforeOpen(DataSet: TDataSet);
    procedure ButtonOwnerClick(Sender: TObject);
    procedure LayoutEditorCompanyButtonClick(Sender: TObject);
  private
    CompanyID: Integer;
    procedure Reload;
    function Validate: Boolean;
    function InternalExecute: Boolean;
  public
    class function Execute(CompanyID: Integer = 0): Integer;
  end;


implementation

{$R *.dfm}

uses

  dmBuy,
  uDialog;

{ TDialogSetup }

class function TDialogBuySetup.Execute(CompanyID: Integer): Integer;
var
  Dialog: TDialogBuySetup;
begin
  Result := 0;

  Dialog := TDialogBuySetup.Create(nil);

  Dialog.CompanyID := CompanyID;

  if Dialog.InternalExecute then
  begin
    Result := Dialog.CompanyID;
  end;

  Dialog.Free;
end;

function TDialogBuySetup.InternalExecute: Boolean;

procedure MenuOwnerPopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  DataSet := TClientDataSet.Create(nil);

  DataSet.Data := DataBuy.Table(sqlOwner);

  DataSet.First;

  while not DataSet.Eof do
  begin

    Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

    Button.ButtonStyle := bsChecked;

    Button.GroupIndex := 1;

    Button.LargeImageIndex := ButtonOwner.LargeImageIndex;

    Button.Tag := DataSet.FieldByName('ID').AsInteger;

    Button.Caption := DataSet.FieldByName('NAME').AsString;

    Button.OnClick := ButtonOwnerClick;

    ButtonLink := MenuOwner.ItemLinks.Add;

    ButtonLink.Item := Button;

    DataSet.Next;
  end;

  DataSet.Free;
end;

begin
  Result := False;

  try

    MenuOwnerPopulate;

    if CompanyID = 0 then
    begin
      LayoutItemCompanyButton.Visible := MenuOwner.ItemLinks.Count > 1;

      CompanyID := DataBuy.SelfCompanyID;
    end;

    LayoutItemOK.Visible := DataBuy.SelfPersonID = 1;

    Setup.Active := True;

    if Setup.IsEmpty then
    begin
      Setup.Append;
    end else

    begin
      Setup.Edit;
    end;

    if ShowModal = mrOk then
    begin
      Result := True;
    end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message)
    end;

  end;

end;

procedure TDialogBuySetup.SetupBeforeOpen(DataSet: TDataSet);
begin
  Setup.ParamByName('COMPANY$ID').AsInteger := CompanyID;
end;

procedure TDialogBuySetup.ActionOKUpdate(Sender: TObject);
begin
  ActionOK.Enabled := Setup.Active and (Setup.State <> dsBrowse);

  if DataBuy.SelfPersonID = 1 then
  begin
    LayoutGroupBuy.Enabled := CompanyID <> 0;

    LayoutGroupRegister.Enabled := CompanyID <> 0;
  end;
end;

procedure TDialogBuySetup.ActionCancelUpdate(Sender: TObject);
begin
  ActionCancel.Enabled := True;
end;

procedure TDialogBuySetup.ActionOKExecute(Sender: TObject);
begin
  if Validate then
  begin

    try

      DataBuy.Rollback;

      DataBuy.StartTransaction;

      SetupCOMPANYID.AsInteger := CompanyID;

      Setup.Post;

      DataBuy.Commit;

      ModalResult := mrOk;

    except

      on E: Exception do
      begin
        TDialog.Error(E.Message);

        DataBuy.Rollback;
      end;

    end;

  end;

end;

procedure TDialogBuySetup.ActionCancelExecute(Sender: TObject);
begin
  if Setup.Active then
  begin

    if Setup.State <> dsBrowse then
    begin
      Setup.Cancel;
    end;

    Setup.Active := False;
  end;

  ModalResult := mrCancel;
end;

procedure TDialogBuySetup.Reload;
begin

  try

    if Setup.Active then
    begin
      Setup.Active := False;
    end;

    Setup.Active := True;

    CompanyID := SetupCOMPANYID.AsInteger;

    if Setup.IsEmpty then
    begin
      Setup.Append;
    end else

    begin
      Setup.Edit;
    end;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;

  end;

end;

function TDialogBuySetup.Validate: Boolean;
var
  Message: string;

procedure GenericPostEditorValue;
var
  AControl: TWinControl;
  ActiveEditor: TcxCustomEdit;
begin
  ActiveEditor := nil;

  AControl := Screen.ActiveControl;

  if AControl.Owner is TcxCustomEdit then
  begin
    ActiveEditor := TcxCustomEdit(AControl.Owner);
  end else

  if AControl is TcxCustomEdit then
  begin
    ActiveEditor := TcxCustomEdit(AControl);
  end;

  if ActiveEditor <> nil  then
  begin
    if ActiveEditor.EditModified then
    begin
      ActiveEditor.PostEditValue;
    end;
  end;

end;

begin
  Message := '';

  GenericPostEditorValue;

  if SetupCOMPANYID.AsInteger = 0 then
  begin
    Message := '�� ��������� "��������".';
  end;

  if SetupCODE.AsInteger = 0 then
  begin
    Message := '�� ��������� "�".';
  end else

  if Trim(SetupADDRESS.AsString) = '' then
  begin
    Message := '�� ��������� "�����".';
  end else

  if Trim(SetupFRMODEL.AsString) = '' then
  begin
    Message := '�� ��������� "������".';
  end else

  if Trim(SetupFRPRODUCTIONNUMBER.AsString) = '' then
  begin
    Message := '�� ��������� "����� �������������".';
  end else

  if Trim(SetupFRREGISTRATIONNUMBER.AsString) = '' then
  begin
    Message := '�� ��������� "��������������� �����".';
  end;

  if Message <> '' then
  begin
    TDialog.Error(Message);
  end;

  Result := Message = '';
end;

procedure TDialogBuySetup.ButtonOwnerClick(Sender: TObject);
var
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button.Tag <> CompanyID then
    begin
      CompanyID := Button.Tag;

      Reload;
    end;
  end;
end;

procedure TDialogBuySetup.LayoutEditorCompanyButtonClick(Sender: TObject);
var
  Rect: TRect;
  X, Y: Integer;
  Button: TcxButton;
begin
  if Sender is TcxButton then
  begin
    Button := TcxButton(Sender);

    GetWindowRect(Button.Handle, Rect);

    X := Rect.Left;

    Y := Rect.Bottom + 4;

    MenuOwner.Popup(X, Y);
  end;
end;

end.
