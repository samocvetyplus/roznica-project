unit frmBuyJournalRecycling;

interface
                                                                           
uses                                                                                  

  Controls, DB, Classes, ActnList,
  FIBDataSet, pFIBDataSet,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage,  cxDBData, dxSkinsdxBarPainter,
  cxClasses, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView,
  cxGrid, cxGroupBox,
  dxSkinsCore,  dxSkinscxPCPainter, dxBar,
  frmBuyJournal, frmBuyInvoice, dxSkinsDefaultPainters, ExtCtrls, cxSplitter, cxTextEdit,
  cxMemo, cxRichEdit, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type                        

{ TFrameBuyJournalRecycling }                                                         

  TFrameBuyJournalRecycling = class(TFrameBuyJournal)                          
    ButtonPrint103: TdxBarLargeButton;
    InvoiceArrayWEIGHT585: TFIBBCDField;
    GridJournalViewWEIGHT585: TcxGridDBBandedColumn;
  protected
    function GetClassCode: Integer; override;
    function GetInvoiceClass: TFrameBuyInvoiceClass; override;
    procedure CustomSetup; override;
  end;

implementation

{$R *.dfm}

uses

  uBuy, dmBuy, frmBuyInvoiceRecycling;

{ TFrameBuyJournalRecycling }

function TFrameBuyJournalRecycling.GetClassCode: Integer;
begin
  Result := 0;

  if ButtonSend.Down then
  begin
    Result := 7;
  end else

  if ButtonReceive.Down then
  begin
    Result := 8;
  end;
end;

function TFrameBuyJournalRecycling.GetInvoiceClass: TFrameBuyInvoiceClass;
begin
  Result := TFrameBuyInvoiceRecycling;
end;

procedure TFrameBuyJournalRecycling.CustomSetup;
begin
  CategoryVisible['Send'] := cbTrue;

  if DepartmentID = DataBuy.SelfDepartmentID then
  begin
                                                                                                      
    CategoryVisible['Open + Close'] := cbTrue;

    CategoryVisible['Add'] := cbTrue;

    CategoryVisible['Delete'] := cbTrue;

    if ButtonSend.Down then
    begin
      CategoryVisible['Print'] := cbTrue;
    end;
  end;

  GridJournalView.Band['������� ��������'].Visible := ButtonReceive.Down;

  inherited CustomSetup;
end;

end.
