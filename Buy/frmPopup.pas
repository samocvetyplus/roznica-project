unit frmPopup;

interface

uses

  Forms, Windows, Messages, Controls;

type

  TWMSizing = packed record
    Msg: Cardinal;
    Edge: Integer;
    DragRect: PRect;
    Result: Longint;
  end;

{ TDialogPopup }

  TPopup = class;

  TPopupState = (psNone, psOK, psCancel, psIgnore);

  TPopupEvent = procedure (Popup: TPopup) of object;

  TPopup = class(TForm)
  private
    FOnOk: TPopupEvent;
    FOnCancel: TPopupEvent;
    State: TPopupState;
    ActiveWindow: HWND;
  protected
    procedure WMActivate(var Message: TWMActivate); message WM_ACTIVATE;
    procedure WMMoving(var Message: TWMMoving); message WM_MOVING;
    procedure WMSizing(var Message: TWMSizing); message WM_SIZING;
    procedure CMDialogKey(var Message: TCMDialogKey); message CM_DIALOGKEY;
  protected
    procedure Ok; virtual;
    procedure Cancel; virtual;
    procedure DoOnOk; virtual;
    procedure DoOnCancel; virtual;
  public
    procedure Popup(X, Y: Integer); virtual;
    property OnOk: TPopupEvent read FOnOk write FOnOk;
    property OnCancel: TPopupEvent read FOnCancel write FOnCancel;
  end;

implementation

{$R *.dfm}

{ TDialogPopup }

procedure TPopup.Popup(X, Y: Integer);
begin
  State := psNone;

  Left := X;

  Top := Y;

  ActiveWindow := Screen.ActiveForm.Handle;

  Show;
end;

procedure TPopup.Ok;
begin
  State := psOK;

  Hide;
end;

procedure TPopup.Cancel;
begin
  State := psCancel;

  Hide;
end;

procedure TPopup.WMMoving(var Message: TWMMoving);
begin
  inherited;

  Message.DragRect^ := BoundsRect;
end;

procedure TPopup.WMSizing(var Message: TWMSizing);
begin
  inherited;

  Message.DragRect^ := BoundsRect;
end;

procedure TPopup.WMActivate(var Message: TWMActivate);
begin
  if State <> psIgnore then
  begin
    SendMessage(ActiveWindow, WM_NCACTIVATE, Ord(Message.Active <> WA_INACTIVE), 0);

    inherited;

    if Message.Active = WA_INACTIVE then
    begin
      if State = psNone then
      begin
        State := psCancel;
      end;

      if State = psOK then
      begin
        State := psIgnore;

        DoOnOk;
      end else

      if State = psCancel then
      begin
        State := psIgnore;

        DoOnCancel;
      end;

      Release;
    end;
  end else
  begin
    inherited;
  end;
end;

procedure TPopup.CMDialogKey(var Message: TCMDialogKey);
begin
  inherited;

  if Message.CharCode = 13 then
  begin
    Ok;
  end else

  if Message.CharCode = 27 then
  begin
    Cancel;
  end;
end;

procedure TPopup.DoOnOk;
begin
  if Assigned(FOnOk) then
  begin
    try

      OnOk(Self);

    except

    end;
  end;
end;

procedure TPopup.DoOnCancel;
begin
  if Assigned(FOnCancel) then
  begin
    OnCancel(Self);
  end;
end;

end.


