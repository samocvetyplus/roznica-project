inherited FrameBuyJournalAffinage: TFrameBuyJournalAffinage
  inherited GroupBoxJournal: TcxGroupBox
    inherited GridJournal: TcxGrid
      inherited GridJournalView: TcxGridDBBandedTableView
        Bands = <
          item
            Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          end
          item
            Caption = #1042#1085#1077#1096#1085#1080#1081' '#1076#1086#1082#1091#1084#1077#1085#1090
          end
          item
            Caption = #1042#1077#1089
            Options.HoldOwnColumnsOnly = True
          end
          item
            Caption = #1055#1086#1090#1077#1088#1080
          end
          item
            Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Options.HoldOwnColumnsOnly = True
          end>
        inherited GridJournalViewSTATENAME: TcxGridDBBandedColumn
          Position.BandIndex = 4
        end
        inherited GridJournalViewSTATEDATE: TcxGridDBBandedColumn
          Position.BandIndex = 4
        end
        inherited GridJournalViewSTATEBYNAME: TcxGridDBBandedColumn
          Position.BandIndex = 4
        end
        object GridJournalViewLOSSESWEIGHT: TcxGridDBBandedColumn
          Caption = '1000'
          DataBinding.FieldName = 'LOSSES$WEIGHT'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Styles.OnGetContentStyle = OnLossesStyle
          Position.BandIndex = 3
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object GridJournalViewLOSSESPERCENT: TcxGridDBBandedColumn
          Caption = '%'
          DataBinding.FieldName = 'LOSSES$PERCENT'
          HeaderAlignmentHorz = taCenter
          Options.Focusing = False
          Styles.OnGetContentStyle = OnLossesStyle
          Position.BandIndex = 3
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
      end
    end
  end
  inherited BarManager: TdxBarManager
    Categories.ItemsVisibles = (
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True
      True)
    DockControlHeights = (
      0
      0
      0
      0)
    inherited ButtonOffice: TdxBarLargeButton
      ImageIndex = -1
    end
    inherited ButtonNote: TdxBarLargeButton [7]
    end
    inherited ButtonOpen: TdxBarLargeButton [8]
    end
    inherited ButtonClose: TdxBarLargeButton [9]
    end
    inherited ButtonInsert: TdxBarLargeButton [10]
    end
    inherited ButtonDelete: TdxBarLargeButton [11]
    end
    inherited ButtonPrint: TdxBarLargeButton [12]
    end
    object ButtonPrint104: TdxBarLargeButton [13]
      Tag = 104
      Caption = #1054#1087#1080#1089#1100
      Category = 6
      Hint = #1054#1087#1080#1089#1100
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = ButtonPrintClick
    end
    object ButtonPrint103: TdxBarLargeButton [14]
      Tag = 103
      Caption = #1054#1090#1087#1091#1089#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      Category = 6
      Hint = #1054#1090#1087#1091#1089#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = ButtonPrintClick
    end
    inherited ButtonInvoice: TdxBarLargeButton [15]
    end
    inherited ButtonRangeCustom: TdxBarButton [16]
    end
    inherited ButtonRangeDay: TdxBarButton [17]
    end
    inherited ButtonRangeWeek: TdxBarButton [18]
    end
    inherited ButtonRangeMonth: TdxBarButton [19]
    end
    inherited ButtonRangeYear: TdxBarButton [20]
    end
    inherited ButtonGold: TdxBarLargeButton [21]
    end
    inherited ButtonMaterial: TdxBarLargeButton [22]
      Category = 9
    end
    inherited ButtonSend: TdxBarLargeButton
      Down = True
      ImageIndex = 17
    end
  end
  inherited Action: TActionList
    inherited ActionAdd: TAction
      Caption = #1055#1077#1088#1077#1076#1072#1095#1072
    end
  end
  inherited InvoiceArray: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost,'
      '  l.percent losses$percent,'
      '  l.weight losses$weight'
      'from'
      ''
      ' buy$invoice$s'
      ' ('
      '  :id'
      ' ) i '
      ''
      ' left join buy$losses(i.id) l on 1 = 1'
      ''
      ' where i.material$id = :material$id')
    SelectSQL.Strings = (
      'select'
      ''
      '  i.id,'
      '  i.reference$id,'
      '  i.forward$id,'
      '  i.backward$id,'
      '  i.company$id,'
      '  i.company$name,'
      '  i.department$id,'
      '  i.department$name,'
      '  i.class$code,'
      '  i.class$name,'
      '  i.route,'
      '  i.n,'
      '  i.invoice$date,'
      '  i.sender$id,'
      '  i.sender$name,'
      '  i.receiver$id,'
      '  i.receiver$name,'
      '  i.material$id,'
      '  i.material$name,'
      '  i.agent$id,'
      '  i.agent$class$code,'
      '  i.agent$name,'
      '  i.tax$id,'
      '  i.tax$name,'
      '  i.tax,'
      '  i.weight,'
      '  i.weight$real,'
      '  i.weight$1000,'
      '  i.cost,'
      '  i.cost$tax,'
      '  i.cost$total,'
      '  i.state$code,'
      '  i.state$name,'
      '  i.state$date,'
      '  i.state$by$id,'
      '  i.state$by$name,'
      '  i.foreign$n,'
      '  i.foreign$date,'
      '  i.foreign$cost,'
      '  l.percent losses$percent,'
      '  l.weight losses$weight'
      'from'
      ''
      ' buy$invoice$a'
      ' ('
      '  :department$id,'
      '  :class$code,'
      '  :range$begin,'
      '  :range$end'
      ' ) i '
      ''
      ' left join buy$losses(i.id) l on 1 = 1'
      ''
      ' where i.material$id = :material$id')
    object InvoiceArrayLOSSESPERCENT: TFIBBCDField
      FieldName = 'LOSSES$PERCENT'
      Size = 2
      RoundByScale = True
    end
    object InvoiceArrayLOSSESWEIGHT: TFIBBCDField
      FieldName = 'LOSSES$WEIGHT'
      Size = 3
      RoundByScale = True
    end
  end
  inherited MenuPrint: TdxBarPopupMenu
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonPrint104'
      end>
  end
end
