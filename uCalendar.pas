unit uCalendar;

interface

uses
  SysUtils, Classes, Controls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type

  TcxCalendar = class(TcxCustomCalendar)
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Align;
    property ArrowsForYear;
    property CalendarButtons;
    property Flat;
    property Font;
    property Kind;
    property LookAndFeel;
    property TimeFormat;
    property Use24HourFormat;
    property WeekNumbers;
    property YearsInMonthList;
    property OnDateTimeChanged;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Express Editors 6', [TcxCalendar]);
end;

{ TcxCalendar }

constructor TcxCalendar.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  CalendarButtons := [];

  Keys := Keys - [kAll];
end;

end.
