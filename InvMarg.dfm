object fmInvMargin: TfmInvMargin
  Left = 375
  Top = 219
  Width = 431
  Height = 375
  Caption = #1053#1072#1094#1077#1085#1082#1080' '#1076#1083#1103' '#1076#1072#1085#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 327
    Width = 423
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 423
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 72
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siSet: TSpeedItem
      BtnCaption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 56
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siSetClick
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TM207IBGrid
    Left = 0
    Top = 71
    Width = 423
    Height = 256
    Align = alClient
    Color = clBtnFace
    DataSource = dm2.dsInvMarg
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = pm1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = False
    Columns = <
      item
        Color = clMoneyGreen
        Expanded = False
        FieldName = 'SNAME'
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 207
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'MARGIN'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1094#1077#1085#1082#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 68
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ROUNDNORM'
        PickList.Strings = (
          #1050#1086#1087#1077#1081#1082#1080
          #1044#1077#1089#1103#1090#1082#1080' '#1082#1086#1087#1077#1077#1082
          #1056#1091#1073#1083#1080
          #1044#1077#1089#1103#1090#1082#1080' '#1088#1091#1073#1083#1077#1081
          #1057#1086#1090#1085#1080' '#1088#1091#1073#1083#1077#1081
          #1058#1099#1089#1103#1095#1080' '#1088#1091#1073#1083#1077#1081)
        Title.Alignment = taCenter
        Title.Caption = #1053#1086#1088#1084#1072' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 113
        Visible = True
      end>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 42
    Width = 423
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 3
    InternalVer = 1
    object Label1: TLabel
      Left = 10
      Top = 8
      Width = 26
      Height = 13
      Caption = #1054#1055#1058':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Bevel1: TBevel
      Left = 42
      Top = 2
      Width = 15
      Height = 23
      Shape = bsLeftLine
    end
    object Label2: TLabel
      Left = 48
      Top = 8
      Width = 47
      Height = 13
      Caption = #1053#1072#1094#1077#1085#1082#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 166
      Top = 8
      Width = 98
      Height = 13
      Caption = #1053#1086#1088#1084#1072' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object DBEdit1: TDBEdit
      Left = 98
      Top = 4
      Width = 61
      Height = 21
      Color = clInfoBk
      DataField = 'OPTMARGIN'
      DataSource = dm.dsSList
      TabOrder = 0
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 266
      Top = 4
      Width = 145
      Height = 21
      Color = clInfoBk
      DataField = 'OPTRNORM'
      DataSource = dm.dsSList
      ItemHeight = 13
      Items.Strings = (
        #1050#1086#1087#1077#1081#1082#1080
        #1044#1077#1089#1103#1090#1082#1080' '#1082#1086#1087#1077#1077#1082
        #1056#1091#1073#1083#1080
        #1044#1077#1089#1103#1090#1082#1080' '#1088#1091#1073#1083#1077#1081
        #1057#1086#1090#1085#1080' '#1088#1091#1073#1083#1077#1081
        #1058#1099#1089#1103#1095#1080' '#1088#1091#1073#1083#1077#1081
        ' ')
      TabOrder = 1
      Values.Strings = (
        '2'
        '1'
        '0'
        '-1'
        '-2'
        '-3'
        ' ')
    end
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 148
    Top = 174
    object N1: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 56
      ShortCut = 114
      OnClick = siSetClick
    end
    object N2: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100'  '#1074' '#1086#1073#1097#1080#1077' '#1085#1072#1094#1077#1085#1082#1080
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1080#1079' '#1086#1073#1097#1080#1093' '#1085#1072#1094#1077#1085#1086#1082
      OnClick = N3Click
    end
  end
end
