unit Dst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, db, DTotal, Menus, DBCtrls, Buttons,
  RXSpin, Mask, DBGridEh, Variants, ComDrv32,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  t_sinvid = record
              depid:integer;
              sinvid:Longint;
             end;
  t_massinvid = array of t_sinvid;

  TfmDst = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    pm2: TPopupMenu;
    miSupFilter: TMenuItem;
    miPriceFilter: TMenuItem;
    miSZFilter: TMenuItem;
    N1: TMenuItem;
    laDepFrom: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    laSup: TLabel;
    laPrice: TLabel;
    laSZ: TLabel;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    pa1: TPanel;
    Panel1: TPanel;
    laQC: TLabel;
    dtQT: TDBText;
    laWC: TLabel;
    dtWT: TDBText;
    Label2: TLabel;
    dtQDT: TDBText;
    Label3: TLabel;
    dtWDT: TDBText;
    Splitter2: TSplitter;
    paB: TPanel;
    dg1: TM207IBGrid;
    plFilter: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter7: TSplitter;
    Panel2: TPanel;
    dg3: TM207IBGrid;
    Splitter1: TSplitter;
    Label6: TLabel;
    Label7: TLabel;
    dbtFullArt: TDBText;
    dbtArt2: TDBText;
    Label8: TLabel;
    laInv: TLabel;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    siDTo: TSpeedItem;
    pmDTo: TPopupMenu;
    Label10: TLabel;
    edUID: TEdit;
    SpeedItem3: TSpeedItem;
    pm: TPopupMenu;
    Art2Id1: TMenuItem;
    N11: TMenuItem;
    ceArt: TComboEdit;
    cbSearch: TCheckBox;
    plWh: TPanel;
    lbWHInfo: TLabel;
    txtArt: TDBText;
    txtQ: TDBText;
    Label9: TLabel;
    lbW: TLabel;
    txtW: TDBText;
    lbCountry: TListBox;
    Splitter6: TSplitter;
    laFullArt: TLabel;
    laArt2: TLabel;
    dge2: TDBGridEh;
    lbAtt1: TListBox;
    Splitter8: TSplitter;
    lbAtt2: TListBox;
    Splitter10: TSplitter;
    dgWH: TDBGridEh;
    siHelp: TSpeedItem;
    Label11: TLabel;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure SetFilter(Sender: TObject);
    procedure ShowSZ(Sender: TObject);
    procedure paBResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure ViewClick(Sender: TObject);
    procedure AddClick(Sender: TObject);
    procedure DelClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure dg3GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dg3Exit(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem3Click(Sender: TObject);
    procedure Art2Id1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbSearchClick(Sender: TObject);
    procedure dge2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgWHDblClick(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgWHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
  private
    { Private declarations }
    FirstBtn: TButton;
    StopFlag: boolean;
    NewSinv:integer;
    newsinvid:t_massinvid;
    function Stop: boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure BtnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ClearFilter;
    procedure PMDToClick(Sender: TObject);
    procedure CMDialogKey(var Message: TCMDialogKey); message CM_DIALOGKEY;
    function  Add(Sender: TObject; Refrsh: boolean): integer;
    function GetIdxByCode(sl: TStringList; Code: Variant): integer;
    Procedure visible_btn;
    procedure edtFindArt(Sender: TObject);
  public
    { Public declarations }
    DToId: integer;
    Function  Select_Sinvid (d:integer):integer;
  end;

var
  fmDst: TfmDst;

implementation

uses comdata, Data, Data2, DBTree, DSZ, Distred, M207Proc, ResQ, SQLMon,
  FIBQuery, ServData, MsgDialog, uUtils;

{$R *.DFM}
Function TfmDst.Select_Sinvid (d:integer):integer;
var i:integer;
begin
 i:=0;
 while (i < length(newsinvid))and(newsinvid[i].depid<>d)do inc(i);
 if newsinvid[i].depid=d then result:=newsinvid[i].sinvid
 else result:=0;
end;

procedure TfmDst.siExitClick(Sender: TObject);
begin
  if dm.DstOne2Many then
    begin
      with pc1 do
        if ActivePage.TabIndex=0 then Close
        else
          begin
            ActivePage:=Pages[0];
            pc1Change(NIL);
          end;
    end
  else Close;
end;

procedure TfmDst.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


function TfmDst.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      eXit;
    end;
end;

procedure TfmDst.FormCreate(Sender: TObject);
  procedure CreateLbl(ACaption: string; var ATop: integer);
  begin
    with TPanel.Create(paB) do
      begin
        Caption:=ACaption;
        Top:=ATop;
        Height:=20;
        Left:=2;
        Width:=paB.Width-4;
        Alignment:=taCenter;
        Font.Color:=clNavy;
        with Font do
          Style:=Style+[fsBold];
        Parent:=paB;
        Inc(ATop,Height);
      end
  end;

  function CreateBtn(ACaption, AHint: string; ATag: integer;
                      var ATop: integer; oc: TNotifyEvent;
                      Bname:string): TButton;
  var b: TButton;
  begin
    b:= TButton.Create(paB);
    with b do
      begin
        Name:=BName;
        Caption:=ACaption;
        Hint:=AHint;
        ShowHint:=True;
        Top:=ATop;
        Left:=2;
        Height:=25;
        Width:=paB.Width-4;
        Parent:=paB;
        OnClick:=oc;
        Tag:=ATag;
        Inc(ATop,Height);
        b.OnKeyDown:=BtnKeyDown;
        Result:=b;
      end;
  end;

var t, i: integer;
    s: string;
    mi: TMenuItem;
    c: TColumnEh;
    btn: TButton;
    b: boolean;
begin
  NewSinv:=0;

  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  DToId:=-1;

  with dm,dmCom do
    begin
      Distr:=True;
      Old_D_MatId:='.';
      D_WHArt:='';
      FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, lbAtt1, lbAtt2);
      if DstOne2Many then
        begin
          lbComp.ItemIndex:=0;
          lbMat.ItemIndex:=0;
          lbGood.ItemIndex:=0;
          lbIns.ItemIndex:=0;
          lbCountry.ItemIndex:=0;
          lbAtt1.ItemIndex := 0;
          lbAtt2.ItemIndex := 0;
          D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
          D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
          D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
          D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
          D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
          D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
          D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
         // lbCompClick(NIL);
        end
       else
        begin
          lbComp.ItemIndex:=GetIdxByCode(dm.slProd, D_CompId);
          lbMat.ItemIndex:=GetIdxByCode(dm.slMat, D_MatId);
          lbGood.ItemIndex:=GetIdxByCode(dm.slGood, D_GoodId);
          lbIns.ItemIndex:=GetIdxByCode(dm.slIns, D_InsId);
          lbCountry.ItemIndex:=GetIdxByCode(dm.slCountry, D_CountryId);
          lbAtt1.ItemIndex:=GetIdxByCode(dm.slAtt1, D_Att1Id);
          lbAtt2.ItemIndex:=GetIdxByCode(dm.slAtt2, D_Att2Id);
        end;
      PriceFilter:=False;
      SupFilter:=False;


      if WorkMode='DINV' then
        begin
          laDepFrom.Caption:=dm.DDepFrom;
          with pc1 do
            ActivePage:=Pages[0];
//          fmDst.HelpContext:=100225;
        end
      else
        begin
          laDepFrom.Caption:=dm.SDep;
          DstArt2Id:=taSElArt2Id.AsInteger;
          with pc1 do
            ActivePage:=Pages[1];
          SetFilter(NIL);
          ReOpenDataSets([quDst, quD_Q]);
//          fmDst.HelpContext:=100224;
        end;
    end;


  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              begin
                mi:=TMenuItem.Create(pmDTo);
                mi.Caption:=Fields[2].AsString;
                mi.Tag:=Fields[0].AsInteger;
                mi.OnClick:=PMDToClick;
                pmDTo.Items.Add(mi);
              end;
          Next;
        end;
      Active:=False;
    end;


  t:=2;
  b:=True;
  CreateLbl('��������', t);
  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              begin
                btn:=CreateBtn(Fields[2].AsString, '����������� �� '+Fields[1].AsString, Fields[0].AsInteger, t, AddClick,
                               'AB_'+Fields[0].AsString);
                if b then
                  begin
                    FirstBtn:=btn;
                    b:=False;
                  end;
              end;
          Next;
        end;
      Active:=False;
    end;
  CreateLbl('��������', t);
  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              CreateBtn(Fields[2].AsString, '�������� ������������ ������� �� '+Fields[1].AsString, Fields[0].AsInteger, t, ViewClick,
                        'VB_'+Fields[0].AsString);
          Next;
        end;
      Active:=False;
    end;
  Inc(t, 10);
  CreateBtn('�������', '������� ������� �� �������� �����', -1, t, DelClick,'DelB');

  s:='';
  if dm.DstOne2Many then
    with dm do
      begin
       sWorkInvID:='';
       SetLength(newsinvid,0);

       qutmp.Close;
       quTmp.SQL.Text:='select d_depid, Sname from d_dep where d_depid<>-1000 and '+
                       ' isdelete <> 1 and  d_depid<>'+inttostr(DstDepId);
       qutmp.ExecQuery;
       while not qutmp.Eof do
        begin
         qutmp1.Close;
         qutmp1.SQL.Text:='select sinvid, sn from Seach_Sinvid( '+
                           qutmp.Fields[0].AsString+', '+inttostr(DstDepId)+', '+
                           inttostr(dmCom.UserId)+')';
         qutmp1.ExecQuery;
         if (qutmp1.Fields[0].AsInteger=0) then raise Exception.Create('��� �������� ��������� �� '+qutmp.Fields[1].asstring);

         s:=s+qutmp.Fields[1].AsString[1]+'-'+qutmp1.Fields[1].AsString+', ';
         sWorkInvID:=sWorkInvID+'*'+qutmp1.Fields[0].AsString+';';
         SetLength(newsinvid,length(newsinvid)+1);
         newsinvid[length(newsinvid)-1].depid:=qutmp.Fields[0].AsInteger;
         newsinvid[length(newsinvid)-1].sinvid:=qutmp1.Fields[0].AsInteger;

         qutmp.Next;
         qutmp1.Transaction.CommitRetaining;
         qutmp1.Close;
        end;
        qutmp.Transaction.CommitRetaining;
        qutmp.Close;
        SetLength(s, Length(s)-2);
      end
   else
   begin
    SetLength(newsinvid,1);
    newsinvid[0].sinvid:=dm.tadlistsinvid.asinteger;
    newsinvid[0].depid:=dm.tadlistdepid.asinteger;
    s:=dm.taDListSN.AsString;
   end;
  laInv.Caption:=s;

  with dm, dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RW_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;

          c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Items[0].FieldName:='A_'+slDepDepId[i];
          c.Footers.Items[0].ValueType:=fvtSum;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RQ_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;

        end;
    end;

  with dm do
    if DstEl then
      begin
        dbtFullArt.DataSource:=dsSEl;
        dbtArt2.DataSource:=dsSEl;
      end
    else
      begin
        dbtFullArt.DataSource:=dsD_WH;
        dbtArt2.DataSource:=dsD_WH;
      end;
  StopFlag:=False;

{ if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
  else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
   else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
    else  dmserv.ComScan.ComPort:=pnCOM4;

 dmServ.ComScan.Connect;
 dmcom.SScanZ:='';  }

  dgWh.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dgwh', [crpColIndexEh, crpColWidthsEh]);
  ActiveControl:=ceArt; 
  dge2.FieldColumns['PRICE'].Visible:=CenterDep;
//  ReOpenDataSets([dm.quD_WH]);
end;

procedure TfmDst.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDst.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
//  ReOpenDataSets([dm.quD_WH]);
end;

procedure TfmDst.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmDst.SetFilter(Sender: TObject);
begin
  if Sender<>NIL then
    with TMenuItem(Sender) do
      Checked:=NOT Checked;
  with dm do
    begin
      PriceFilter:=miPriceFilter.Checked;
      SupFilter:=miSupFilter.Checked;
      SZFilter:=miSZFilter.Checked;

      PriceFilterValue:=quD_UIDPrice.AsFloat;
      SupFilterId:=quD_UIDSupId.AsInteger;
      SZFilterValue:=quD_UIDSZ.AsString;
      ReOpenDataSets([quD_UID]);

      if PriceFilter then laPrice.Caption:=quD_UIDPrice.DisplayText
      else laPrice.Caption:='���';
      if SupFilter then laSup.Caption:=quD_UIDSup.DisplayText
      else laSup.Caption:='���';
      if SZFilter then laSZ.Caption:=quD_UIDSZ.DisplayText
      else laSZ.Caption:='���';
    end;
end;


procedure TfmDst.ClearFilter;
begin
  with dm do
    begin
      PriceFilter:=False;
      SupFilter:=False;
      SZFilter:=False;

      miPriceFilter.Checked:=False;
      miSupFilter.Checked:=False;
      miSZFilter.Checked:=False;

      PriceFilterValue:=quD_UIDPrice.AsFloat;
      SupFilterId:=quD_UIDSupId.AsInteger;
      SZFilterValue:=quD_UIDSZ.AsString;

      laPrice.Caption:='���';
      laSup.Caption:='���';
      laSZ.Caption:='���';
    end;
end;



procedure TfmDst.ShowSZ(Sender: TObject);
begin
  ShowAndFreeForm(TfmDSZ, Self, TForm(fmDSZ), True, False);
end;

procedure TfmDst.paBResize(Sender: TObject);
var i: integer;
begin
  with paB do
    for i:=0 to ComponentCount-1 do
      begin
        if Components[i] is TSpeedButton then
           TSpeedButton(Components[i]).Width:=paB.Width-4;
        if Components[i] is TPanel then
           TPanel(Components[i]).Width:=paB.Width-4;

      end
end;

procedure TfmDst.FormClose(Sender: TObject; var Action: TCloseAction);
var i:integer;
begin
  with dm do
    begin
      CloseDataSets([quDst, quD_UID, quD_T, quD_Q, dm2.quD_WH_T]);
      if DstOne2Many then quD_WH.Active:=False;
      Distr:=False;
      DstEl:=False;
      ClosedInvId:=taDListSInvId.AsInteger;
      for i:=0 to length(newsinvid)-1 do
      begin
       taDList.Locate('SINVID', newsinvid[i].sinvid, []);
       taDList.Refresh;
      end;
      taDList.Locate('SINVID', ClosedInvId, []);
{      ClosedInvId:=taDListSInvId.AsInteger;
       ReopenDatasets([taDList]);
       taDList.Locate('SINVID', ClosedInvId, []);}
    end;

 Finalize(newsinvid);
 
// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 dgwh.SaveColumnsLayoutIni(GetIniFileName, Name+'_dgwh', true);
end;

procedure TfmDst.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field<>NIL then
    if Field.FieldName='DEP' then Background:=dm.quDstColor.AsInteger
    else if NOT Highlight or dg1.ClearHighlight then
           if dm.quDstIsClosed.AsInteger=1 then Background:=clBtnFace
           else Background:=clAqua;
end;

procedure TfmDst.ViewClick(Sender: TObject);
var s: string; i,sn:integer;
begin
  with dm do
    begin
      with quTmp do
        begin
         if DstOne2Many then
          begin
           sn:=Select_Sinvid(tcomponent(Sender).Tag);
           if (sn<>0) then
            sql.Text:='select sn, sinvid from sinv where sinvid='+inttostr(sn)
           else raise Exception.Create('����������� ��������� ������!');
          end
         else
           SQL.Text:='SELECT SN, SINVID FROM SINV WHERE ITYPE=2 AND DEPFROMID='+IntToStr(DDepFromId)+' AND DepId='+IntToSTr(TComponent(Sender).Tag)+' AND SN in ('+laInv.Caption+') AND FYEAR(SDATE)=FYEAR(''TODAY'') and CRUSERID='+inttostr(taDListCrUserId.AsInteger);
          ExecQuery;
          if (Fields[1].isnull) then raise Exception.Create('���������� �������!');
          s:=Fields[0].AsString;
          i:=Fields[1].AsInteger;
          Transaction.CommitRetaining;
          Close;
        end;
      try
        quDistred.Tag:=i;
        fmDistred:=TfmDistred.Create(NIL);
        fmDistred.Caption:=TButton(Sender).Caption+': ��������� �'+s;
        fmDistred.ShowModal;
      finally
        fmDistred.Free;
      end
    end;
end;

function TfmDst.Add(Sender: TObject; Refrsh: boolean): integer;
var i,sn:integer;
begin
  Result := 0;
  
  with dm, dm2 do
    begin
      if quD_UIDSItemId.IsNull then Exit;
      with qutmp do
      begin
       Sql.Text := '  select uidwhcalc  from d_rec ';
       ExecQuery;
       i:= Fields[0].AsInteger;
       Transaction.CommitRetaining;
       close;
      end;
      If i = 1 then Raise Exception.Create('��������� ������ �����������');

      if quD_UIDFREPAIR.AsInteger=0 then
      begin
       with quInsDSItem, Params do
        begin
          ByName['SITEMID'].AsInteger:=quD_UIDSItemId.AsInteger;
          ByName['ART2ID'].AsInteger:=DstArt2Id;
          ByName['DEPFROMID'].AsInteger:=DstDepId;
          ByName['DEPID'].AsInteger:=TComponent(Sender).Tag;

          sn:=Select_Sinvid(TComponent(Sender).Tag);
          if sn<>0 then ByName['SINVID'].asinteger:=sn
          else raise Exception.Create('��������� �������');

          ExecQuery;
          Result:=Fields[0].AsInteger;
          dmcom.tr.CommitRetaining;

          Close;
          if Refrsh then
            begin
              quDst.Append;
              quDstSItemId.AsInteger:=Result;
              quDst.Post;
            end;
        end;
       quD_UID.Delete;
       quD_UID.tAG:=quD_UID.FieldByName('UID').AsInteger;
      end else ShowMessage('������� �� �������!'); 
    end;
end;


procedure TfmDst.AddClick(Sender: TObject);
var i, j: integer;
    Refrsh: boolean;
begin
  i := 0;
  with dmCom, dm, dm2 do
    begin
      Refrsh:=dg1.GetSortField('SZ')=-1;

      if not Refrsh then
        quDst.Active:=False;

      if dge2.SelectedRows.Count=0 then i:=Add(Sender, Refrsh)
      else
        begin
          for j:=0 to dge2.SelectedRows.Count-1 do
            begin
              try
                dge2.DataSource.DataSet.Bookmark:=dge2.SelectedRows[j];
                i:=Add(Sender, Refrsh);
              except
              end;
            end;
          dge2.SelectedRows.Clear;
        end;

//      dmCom.tr.CommitRetaining;
      if DstOne2Many then
       begin
//        dmCom.tr.Commit;
//        if not dmcom.tr.Active then  dmCom.tr.StartTransaction;
        ReOpenDataSets([quD_UID]);
        quD_UID.Locate('UID',quD_UID.TAG,[]);
       end
      else dmCom.tr.CommitRetaining;
      ReOpenDataSets([quD_T, quD_Q]);
      with quDst do
        try
          DisableControls;
          if not Active then Open;
          Locate('SITEMID', i, []);
          i:=RecNo;
          Last;
          MoveBy(i-RecordCount);
        finally
          EnableControls;
        end;
    end;
end;

procedure TfmDst.DelClick(Sender: TObject);
begin
  with dm, quDst do
    begin
      if quDstSItemId.IsNull or (quDstIsClosed.AsInteger=1) then Exit;
      with quD_UID do
        begin
          Append;
          quD_UIDSItemId.AsInteger:=quDstRef.AsInteger;
          Post;
        end;
      Delete;
      dmCom.tr.CommitRetaining;
      ReOpenDataSets([quD_T, quD_Q]);
    end
end;

Procedure TfmDst.visible_btn;
var i,sn:integer;
begin
 for i:=0 to dm2.slDepDepId.Count-1 do
 begin
  sn:=Select_Sinvid(strtoint(dm2.slDepDepId[i]));
  if sn<>0 then
   begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select isclosed, userid from sinv where sinvid='+inttostr(sn);
    dm.quTmp.ExecQuery;
    if (dm.quTmp.Fields[0].AsInteger=1)or(dm.quTmp.Fields[0].isnull) or
       ((not dm.quTmp.Fields[1].IsNull) and (dm.quTmp.Fields[1].AsInteger<>dmcom.UserId)) then
     with paB do
     tbutton(FindComponent('AB_'+dm2.slDepDepId[i])).Enabled:=false
    else
     with paB do
     tbutton(FindComponent('AB_'+dm2.slDepDepId[i])).Enabled:=true;
    dm.quTmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
   end;
 end;
end;

procedure TfmDst.pc1Change(Sender: TObject);
begin
  with pc1, dm do
   begin
    if ActivePage=Pages[1] then
      begin
        if (not quD_WH.Active) then SysUtils.Abort;
        visible_btn;
        DstArt2Id:=quD_WHArt2Id.AsInteger;
        SetFilter(NIL);
        ReOpenDataSets([quDst, quD_Q]);
      end
    else
      begin
        CloseDataSets([quD_UID, quD_Q, quD_T, quDst]);
        ceArt.SelectAll;
        ActiveControl:=ceArt;
        ClearFilter;
        if quD_WH.Active then
        begin
         quD_WH.Refresh;
         if quD_WHFLAG.AsInteger=0 then
         begin
          qud_wh.Locate('D_ARTID;FLAG',VarArrayOf([quD_WHD_artid.AsInteger,1]),[]);
          quD_WH.Refresh;
         end;
         dgwh.SumList.RecalcAll;
        end;
      end;

     if quD_WH.Active then
     begin
      laFullArt.Caption:=dbtFullArt.Field.AsString;
      laArt2.Caption:=dbtArt2.Field.AsString;
     end;
     SetVisEnabled(TWinControl(laFullArt),ActivePage=Pages[1]);
     SetVisEnabled(TWinControl(laArt2),ActivePage=Pages[1]);
     SetVisEnabled(TWinControl(dbtFullArt),ActivePage<>Pages[1]);
     SetVisEnabled(TWinControl(dbtArt2),ActivePage<>Pages[1]);
   end;
end;

procedure TfmDst.dg3GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (not Highlight or dg1.ClearHighlight) and (Field.FieldName='PRICE') then
    if dm.quD_QPEQ.AsInteger=1 then Background:=clInfoBk
    else Background:=clRed;
end;

procedure  TfmDst.BtnKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
begin
  case Key of
      VK_LEFT: ActiveControl:=dge2;
      VK_RIGHT: ActiveControl:=dg1;
    end;
end;

procedure TfmDst.dg3Exit(Sender: TObject);
begin
  PostDataSets([dm.quD_Q]);
end;

procedure TfmDst.SpeedItem1Click(Sender: TObject);
begin
  dm.Old_D_CompId:=dmCom.D_CompId;
  dm.Old_D_MatId:=dmCom.D_MatId;
  dm.Old_D_GoodId:=dmCom.D_GoodId;
  dm.Old_D_InsId:=dmCom.D_InsId;
  dm.Old_D_CountryId:=dmCom.D_CountryId;
  dm.Old_D_Att1Id := dmCom.D_Att1Id;
  dm.Old_D_Att2Id := dmCom.D_Att2Id;
  dm.D_WHArt:=ceArt.Text;
  ReopenDataSets([dm.quD_WH]);
end;

procedure TfmDst.PMDToClick(Sender: TObject);
begin
  DToId:=TMenuItem(Sender).Tag;
  siDTo.BtnCaption:=TMenuItem(Sender).Caption;
end;

procedure TfmDst.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var T,i, sn: integer;
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        if DToId=-1 then raise Exception.Create('����� �� ������!!!');

        with qutmp do
        begin
         close;
         Sql.Text := '  select uidwhcalc  from d_rec ';
         ExecQuery;
         i:= Fields[0].AsInteger;
         Transaction.CommitRetaining;
         close;
        end;
        If i = 1 then Raise Exception.Create('��������� ������ �����������');

        sn:=Select_Sinvid(DToId);
       {����������� ������������� ���� ��������� � �� ����������}
        with qutmp do
        begin
         sql.Text:='select isclosed, sinvid, userid from sinv where sinvid='+inttostr(sn);
         ExecQuery;
         if Fields[1].IsNull then
         begin
          Transaction.CommitRetaining;
          close;
          raise Exception.Create('��������� �������')
         end
         else if Fields[0].AsInteger=1 then
              begin
               Transaction.CommitRetaining;
               close;
               raise Exception.Create('��������� �������')
              end
         else if (not Fields[2].IsNull) and (Fields[2].asinteger<>dmcom.UserId) then
          begin
           Transaction.CommitRetaining;
           close;
           raise Exception.Create('��������� ������� ��. �������������');
          end;
         Transaction.CommitRetaining;
         close;
        end;
        {********************************************************}

        with quInsDUID2, Params do
          begin
            ByName['UID'].AsInteger:=StrToInt(edUID.Text);

            if sn<>0 then ByName['SINVID'].asinteger:=sn
            else raise Exception.Create('��������� �������');


            ByName['DEPID'].AsInteger:=DToId;
            ByName['DEPFROMID'].AsInteger:=DstDepId;

            ByName['OPT'].AsInteger:=0;
            ByName['CRUSERID'].AsInteger:=dmCom.UserId;
            ExecQuery;

            T:=FieldByName('T').AsInteger;
            Close;
          end;
        case T of
            2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
            3: MessageDialog('������� �������', mtInformation, [mbOK], 0);
            4: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
          end;
        qud_wh.Refresh;
        edUID.Text:='';
      end;
end;

procedure TfmDst.FormActivate(Sender: TObject);
begin
  if not dm.DstOne2Many then
    with pc1, dm do
      begin
        ActivePage:=Pages[1];
        SetFilter(NIL);
        ReOpenDataSets([quDst, quD_Q]);
      end;

  if not dm.DstOne2Many then fmDst.HelpContext:=100224
  else fmDst.HelpContext:=100225;

  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDst.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then
    siExitClick(NIL);
end;

procedure TfmDst.CMDialogKey(var Message: TCMDialogKey);
begin
  if Message.CharCode=VK_TAB then
    begin
      if ActiveControl=dge2 then ActiveControl:=FirstBtn
      else if ActiveControl.Parent=paB then ActiveControl:=dge2
           else inherited;
    end
  else inherited;
end;


procedure TfmDst.dg2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_TAB then Key:=0;
end;

procedure TfmDst.SpeedItem3Click(Sender: TObject);
begin
  ShowAndFreeForm(TfmRestQ, Self, TForm(fmRestQ), True, False);
end;

function TfmDst.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmDst.Art2Id1Click(Sender: TObject);
begin
  MessageDialog(dm.quD_WHArt2Id.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmDst.N11Click(Sender: TObject);
begin
  fmSQLMon:=TfmSQLMon.Create(Application);
  fmSQLMon.Show;
end;

procedure TfmDst.ceArtChange(Sender: TObject);
begin
//
end;

procedure TfmDst.edtFindArt(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    cbSearch.Tag:=0;
    if cbSearch.Checked then
    begin
      StopFlag:=False;
      with dm.quD_WH do
      begin
        if not Active then Open;
        if not LocateF(dm.quD_WH, 'ART', ceArt.Text, [loBeginingPart], False, Stop) then
        begin
          dmCom.D_CompId:=-1;
          dmCom.D_MatId:= '*';
          dmCom.D_GoodId:= '*';
          dmCom.D_InsId:= '*';
          dmCom.D_CountryId:= '*';
          dmCom.D_Att1Id := -2;
          dmCom.D_Att2Id := -2;
          lbComp.ItemIndex:=0;
          lbMat.ItemIndex:=0;
          lbGood.ItemIndex:=0;
          lbIns.ItemIndex:=0;
          lbCountry.ItemIndex:=0;
          lbAtt1.ItemIndex := 0;
          lbAtt2.ItemIndex := 0;

          ReOpenDataSets([dm.quD_WH]);
          LocateF(dm.quD_WH, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
        end;
      end;
    end;
  finally
    Screen.Cursor:=crDefault;
  end
end;

procedure TfmDst.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmDst.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then
                     begin
                          edtFindArt(ceArt);
//                       dgWhDblClick(NIL)
                     end
                     else {if cbSearch.Tag=0 then}
                            begin
                              dm.D_WHArt:=ceArt.Text;
                              ReopenDataSets([dm.quD_WH]);
                              cbSearch.Tag:=1;
                            end
{                          else
                            begin
                              dgWhDblClick(NIL);
                              cbSearch.Tag:=0;
                            end;                }
                   end;
      VK_ESCAPE: StopFlag:=True;
    end;
end;

procedure TfmDst.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then
  begin
   ActiveControl:=dgWH;
  end
end;

procedure TfmDst.cbSearchClick(Sender: TObject);
begin
  if cbSearch.Checked then
    with dm do
      begin
        D_WHArt:='';
        ReopenDataSets([quD_WH]);
      end;
end;

procedure TfmDst.dge2GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 with Column do
  if (Field.FieldName<>'PRICE') then
    case dm.quD_UIDFREPAIR.AsInteger of
     1: Background:=clBtnFace;
     else case dm.quD_UIDIsClosed.AsInteger of
           0: Background:=dmCom.clMoneyGreen;
           1: Background:=clInfoBk;
          end;
    end
end;

procedure TfmDst.dgWHDblClick(Sender: TObject);
begin
   with pc1 do
    ActivePage:=Pages[1];
  pc1Change(NIL);
end;

procedure TfmDst.dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 with dm do
  if Column.Field.Tag>0 then Background:=dm2.GetDepColor(Column.Field.Tag)
   else
    if (Column.Field.FieldName='DQ') or (Column.Field.FieldName='DW') then Background:=dmCom.clCream
    else
     if quD_WHDQ.AsInteger=0 then Background:=clBtnFace
      else if quD_WHQuantity.AsInteger=quD_WHDQ.AsInteger then Background:=clInfoBk
           else Background:=clAqua;
end;

procedure TfmDst.dgWHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if Key=VK_RETURN then dgWHDblClick(NIL)
 end;

procedure TfmDst.siHelpClick(Sender: TObject);
begin
 if not dm.DstOne2Many then Application.HelpContext(100224)
 else Application.HelpContext(100225);
end;

end.




