unit Client;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Menus, StdCtrls, DB, ActnList, DBGridEh, PrnDbgeh, Variants,
  M207Ctrls, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  tarray = array of integer;
  TfmClient = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    SpeedItem2: TSpeedItem;
    lbaddress: TListBox;
    ActionList1: TActionList;
    acNewClient: TAction;
    dg1: TDBGridEh;
    SpeedBar1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    pmfmain: TPopupMenu;
    nallfmain: TMenuItem;
    Nfmain: TMenuItem;
    Nnotfmain: TMenuItem;
    sisellitem: TSpeedItem;
    pmdep: TPopupMenu;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    pmdate: TPopupMenu;
    Nall: TMenuItem;
    Ndate: TMenuItem;
    acDel: TAction;
    siCalSum: TSpeedItem;
    acCalcSum: TAction;
    fr: TM207FormStorage;
    acPrintLetter: TAction;
    SpeedItem1: TSpeedItem;
    acUncheckLetter: TAction;
    ppLetter: TPopupMenu;
    acPrintLetter1: TMenuItem;
    N1: TMenuItem;
    siLose: TSpeedItem;
    siVibNodcard: TSpeedItem;
    acLose: TAction;
    acVibcard: TAction;
    SpeedBar2: TSpeedBar;
    Ldate: TLabel;
    SpeedbarSection3: TSpeedbarSection;
    sifmain: TSpeedItem;
    sidep: TSpeedItem;
    sidate: TSpeedItem;
    Label1: TLabel;
    edNodcard: TEdit;
    Label2: TLabel;
    edfio: TEdit;
    acSellItem: TAction;
    chblosecard: TCheckBox;
    siOvereating: TSpeedItem;
    acOvereating: TAction;
    NDateUpdate: TMenuItem;
    siHelp: TSpeedItem;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure lbaddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure lbaddressDblClick(Sender: TObject);
    procedure edNodcardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edfioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNodcardKeyPress(Sender: TObject; var Key: Char);
    procedure edNodcardKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edfioKeyPress(Sender: TObject; var Key: Char);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg1KeyPress(Sender: TObject; var Key: Char);
    procedure dg1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dg1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dg1DblClick(Sender: TObject);
    procedure nallfmainClick(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure NallClick(Sender: TObject);
    procedure NdateClick(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acCalcSumExecute(Sender: TObject);
    procedure acPrintLetterExecute(Sender: TObject);
    procedure acUncheckLetterExecute(Sender: TObject);
    procedure acLoseExecute(Sender: TObject);
    procedure acVibcardExecute(Sender: TObject);
    procedure acVibcardUpdate(Sender: TObject);
    procedure acSellItemExecute(Sender: TObject);
    procedure acLoseUpdate(Sender: TObject);
    procedure chblosecardClick(Sender: TObject);
    procedure acOvereatingUpdate(Sender: TObject);
    procedure acOvereatingExecute(Sender: TObject);
    procedure NDateUpdateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
  private
    { Private declarations }
    b:boolean; {����� �������������}
    flFieldED:boolean;  {true - ��������� ����� �� ���� ������}
    flHome_falt : boolean; {true - ��������� ����� ����� �������� "����" �� ���� Home_flat}
    flClientName : boolean; {true - ��������� ����� ����� �������� "����" �� ���� Name}
    CanselFl: boolean; {� ���� ����� ����. ����� ������� "�" ��� "�"}
    value :integer;
    addressid : tarray;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    Function CreatePmAddress(str:string):boolean;
    procedure pmDepClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  fmClient: TfmClient;

implementation

uses comdata, Data, M207Proc, Types, NsellItem, Period, FIBQuery, dbUtil,
   reportdata, MsgDialog, VibCard;

{$R *.DFM}

{����� ������ �������}
Function TfmClient.CreatePmAddress(str:string):boolean;
begin
 lbaddress.Clear;
 SetLength(addressid,0);
 with dmcom, dm, qutmp do
  begin
   close;
   if CenterDep then
    sql.Text:='select d_address_id, address from d_address ' +#13#10+
              'where d_address_id<>-1000 and fdel=0 and address like '''+str+'%'+''''+#13#10+
              'order by address'
   else
    sql.Text:='select d_address_id, address from d_address ' +#13#10+
              'where d_address_id<>-1000 and fdel=0 and address like '''+str+'%'+''''+#13#10+
              'and depid = '+taClientDEPID.AsString+#13#10+
              'order by address';
   ExecQuery;

   if Fields[0].AsInteger=0 then result:=false else result:=true;

   while not eof do
   begin
    lbaddress.Items.Add(TrimRight(Fields[1].AsString));
    setlength(addressid,length(addressid)+1);
    addressid[length(addressid)-1]:=Fields[0].AsInteger;
    next
   end;
   Transaction.CommitRetaining;
   close;
  end;

end;

procedure TfmClient.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmClient.FormCreate(Sender: TObject);
var  pm:TMenuItem;
begin
  b:=GetBit(dmCom.EditRefBook, 11);
  tb1.WallPaper:=wp;
  SpeedBar1.WallPaper:=wp;
  SpeedBar2.WallPaper:=wp;
  with dmCom, dm do
    begin
      {��������� ���������� ��������}
      chblosecard.Checked:=dmcom.IsLoseCard;
      FilterFIO:='';
      if CenterDep then
       begin
        FilterFmain1:=1;
        FilterFmain2:=1;
        sifmain.BtnCaption:='����. ����� �������';
       end
      else
       begin
        FilterFmain1:=0;
        FilterFmain2:=1;
        sifmain.BtnCaption:='��� ����. �����';
       end;
      ClientUpDate:=false;
      fdata:=false;
      dg1.ReadOnly:= not b; // Centerdep;
     { SpeedItem2.Visible:=not Centerdep;}
//      sidel.Visible:=(not Centerdep) ;
      acDel.Enabled:= b;
      dg1.FieldColumns['NODCARD'].ReadOnly:= dmcom.Seller; 
      dmcom.FilterDepID:=0;
      OpenDataSets([taClient]);
      flFieldED:=false;
      flClientName:=false;
      flHome_falt:=false;
      CanselFl:=false;
      lbaddress.Height:=dg1.Height;
{      if CenterDep then sisellitem.Left:=0;
      if CenterDep and (not b)then dg1.Options:= [dgTitles,dgIndicator,dgColumnResize,dgColLines,
                        dgRowLines,dgCancelOnExit]
      else dg1.Options:=[dgEditing,dgAlwaysShowEditor,dgTitles,
           dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgCancelOnExit]}
    end;

 if CenterDep then
 begin

  pm:=TMenuItem.Create(pmdep);
  pm.Caption:='��� ������';
  pm.Tag:=0;
  pm.OnClick:=pmDepClick;

  pmdep.Items.Add(pm);

  {��������� �������}
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid<>-1000 and ISDELETE <>1';
  dm.quTmp.ExecQuery;

  while not (dm.qutmp.Eof) do
  begin
   pm:=TMenuItem.Create(pmdep);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;

   pmdep.Items.Add(pm);

   dm.quTmp.Next;
  end;

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
  dmcom.FilterDepID:=0;
 end
 else
 begin
  sidep.Visible:=false;
  dmcom.FilterDepID:=SelfDepId;
 end;

 dmcom.bdata:=dmCom.GetServerTime;
 dmcom.edata:=dmCom.GetServerTime;
 if  dmcom.Seller then ActiveControl:=edfio;
end;

procedure TfmClient.FormClose(Sender: TObject; var Action: TCloseAction);
var i:integer;
    st:string;
begin
  with dmCom do
    begin
      if taClientClientId.IsNull then taClient.Tag:=-1
      else taClient.Tag:=taClientClientId.AsInteger;
      PostDataSets([taClient]);
      FilterFIO:='';
    end;
    
  if CenterDep then
  with dm do
  begin
   qutmp.Close;
   quTmp.SQL.Text:='select count(*), nodcard '+
                   'from client '+
                   'where fmain=1 and clientid<>-1000 and fdel=0 '+
                   'group by nodcard '+
                   'having count(*)>1 ';
   qutmp.ExecQuery;
   i:=0;
   st:='���������� ����� � �������� '+#10#13;
   if qutmp.Fields[0].AsInteger<>0 then i:=1;
   while not qutmp.Eof do
    begin
     st:=st+trim(qutmp.Fields[1].AsString)+#10#13;
     qutmp.Next;
    end;

   if i=1 then MessageDialog(st+'������ �� ���������� ��������', mtWarning, [mbOk], 0);
   qutmp.Transaction.CommitRetaining;
   qutmp.Close;
  end
end;

procedure TfmClient.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmClient.FormResize(Sender: TObject);
begin
  lbaddress.Height:=dg1.Height;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmClient.SpeedItem2Click(Sender: TObject);
begin
 ActiveControl := edNodcard;
 edNodcard.Text := '';
 raise Exception.Create('���������� �������� ������ ����� "����� ����. �����"!');
{
  dmCom.taClient.Append;
  dg1.SelectedField:=dmCom.taClientNODCARD;}
end;

procedure TfmClient.lbaddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_RETURN:
  with dmCom do
  begin
   taClient.Edit;
   if lbaddress.ItemIndex=-1 then lbaddress.ItemIndex:=0;
   taClientADDRESS.AsString:=lbaddress.Items[lbaddress.ItemIndex];
   taClientADDRESSID.AsInteger:=addressid[lbaddress.ItemIndex];
   taClient.Post;
   taClient.Refresh;
   flFieldED:=false;
   lbaddress.Visible:=false;
   flHome_falt:=FALSE;
   ActiveControl:=dg1;
   dg1.SelectedField:=taClient.FieldByName('HOME_FLAT');
  end;
 end;
end;

procedure TfmClient.FormShow(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmClient.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmClient.lbaddressDblClick(Sender: TObject);
begin
  with dmCom do
  begin
   taClient.Edit;
   if lbaddress.ItemIndex=-1 then lbaddress.ItemIndex:=0;
   taClientADDRESS.AsString:=lbaddress.Items[lbaddress.ItemIndex];
   taClientADDRESSID.AsInteger:=addressid[lbaddress.ItemIndex];
   taClient.Post;
   taClient.Refresh;
   flFieldED:=false;
   lbaddress.Visible:=false;
   ActiveControl:=dg1;
   dg1.SelectedField:=taClient.FieldByName('HOME_FLAT');
  end;
end;

procedure TfmClient.edNodcardKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var FieldsRes:t_array_var;
begin
 case key of
  VK_RETURN:
   with dm, dmcom do
   begin
    if (edNodcard.Text='') then Raise Exception.Create('������� ����� ����. �����');
    try
     if not CenterDep then
       ExecuteQutmp(quTmp, 'select nodcard, fmain from client where '+
                ' fdel=0  and depid = '+inttostr( SelfDepId)+
                ' and nodcard = '''+edNodcard.Text+'''', 2, FieldsRes)
     else
       ExecuteQutmp(quTmp,'select nodcard, fmain from client where '+
                ' fdel = 0 and nodcard = '''+edNodcard.Text+''' and '+
                ' fmain between '+inttostr(dmcom.FilterFmain1)+' and '+inttostr(dmcom.FilterFmain2), 2, FieldsRes);
     if ((FieldsRes[0]<>null)and
        (dmcom.FilterFmain1<=FieldsRes[1])and
        (FieldsRes[1]<=dmcom.FilterFmain2)) then
     begin
      if not (dmCom.taClient.Locate('nodcard',ednodcard.Text,[])) then
      begin dmcom.FilterFIO:=''; edfio.Text:='';
            ReOpenDataSets([dmcom.taClient]);
            dmCom.taClient.Locate('nodcard',ednodcard.Text,[]) end;
      ActiveControl:=dg1;
      dg1.SelectedField:=dmCom.taClientNAME;
      edNodcard.Text:='';
     end
     else
      begin
       if CenterDep then
       begin
        if not b then Application.MessageBox(pchar('����� '+edNodcard.Text+' �� �������!'),'��������!',0)
        else
        if Application.MessageBox('����� �� �������.'+#13#10+'������� ���������� ����� �� �����?','��������!',1)= IDOK then
        begin
          dmCom.taClient.Insert;
          dmcom.taClientNODCARD.AsString:=edNodcard.Text;
          dmcom.taClient.Post;
          ActiveControl:=dg1;
          dg1.SelectedField:=dmCom.taClientNAME;
          edNodcard.Text:='';
        end
        else edNodcard.Text:=''
       end
       else
       begin
         if (FieldsRes[0]=null) then
         begin
          if b then
          begin
           dmCom.taClient.Insert;
           dmcom.taClientNODCARD.AsString:=edNodcard.Text;
           dmcom.taClient.Post;
           ActiveControl:=dg1;
           dg1.SelectedField:=dmCom.taClientNAME;
          end
          else Application.MessageBox(pchar('����� '+edNodcard.Text+' �� �������'),'��������!',0);
          edNodcard.Text:='';
         end
         else Application.MessageBox('����� �� �������','��������!',0)
       end
      end;
     flClientName:=false;
   finally
    Finalize(FieldsRes);
   end;
  end;
 end;
end;

procedure TfmClient.edfioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_RETURN: begin dmcom.FilterFIO:= edfio.Text; ReOpenDataSets([dmcom.taClient]) end;
 vk_DOWN : begin ActiveControl := dg1; dg1.SelectedField := dmcom.taClientNAME end
 end
end;

procedure TfmClient.edNodcardKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '0'..'9','/',#8 : ;
 '�', '�': CanselFl:=true;
 else sysUtils.Abort;
 end;
end;

procedure TfmClient.edNodcardKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if CanselFl
  then begin edNodCard.Text:='��� ����. �����'; CanselFl:=false; end;
end;

procedure TfmClient.edfioKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '�'..'�',#8,'�'..'�','a'..'z','A'..'Z',' ', '.' : ;
 else sysUtils.Abort;
 end;
end;

procedure TfmClient.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if (Column.Field.FieldName='SNAME')  then Background:=dmcom.taClientCOLOR.asinteger
 else
 if (Column.FieldName='QRET') or (Column.FieldName='QSELL') or
    (Column.FieldName='CRET') or (Column.FieldName='CSELL') or
    (Column.FieldName='COST') then Background:=dmcom.taClientCOLORCOST.asinteger
 else if dmcom.taClientF_LOSE.AsInteger=1 then Background:=$00A09D9C;
end;

procedure TfmClient.dg1KeyPress(Sender: TObject; var Key: Char);
begin
 If dg1.SelectedField = dmcom.taClientNODCARD then
  case key of
   '0'..'9','/',#8 : ;
   else sysUtils.Abort;
  end;
end;

procedure TfmClient.dg1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
(* with dmcom do
 if dg1.SelectedField = taClientADDRESS then
  {����� ������ �������}
  if CreatePmAddress(dg1.InplaceEditor.EditText) then
  begin
   lbaddress.Visible:=true;
   lbaddress.Left:=dg1.SelectedRect.Right+1;
  end
  else lbaddress.Visible:=false
 else
  if flFieldED then
  begin
   ShowMessage('� ���� � ���� �����!!!!');
 {  i:= taClientCLIENTID.AsInteger;
   j:= dg1.SelectedIndex;
   taClient.Locate('CLIENTID',value,[]);
   dg1.SelectedField:=taClientADDRESS;
   taClient.Refresh;
   taClient.Locate('CLIENTID',i,[]);
   dg1.SelectedIndex:=j;
   flFieldED:=false;
   lbaddress.Visible:=false;}
  end
 else
 begin
  ShowMessage('� ����� � ������ ����!!!!!');
 { lbaddress.Visible:=false;}
 end
  *)
 if dg1.SelectedField = dmcom.taClientHOME_FLAT then flHome_falt:=true
 else flHome_falt:=false;

 if dg1.SelectedField = dmcom.taClientNAME then flClientName:=true
 else flClientName:=false;
end;

procedure TfmClient.dg1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:integer;
    dg1edittext:string;
    FieldsRes:t_array_var;
begin
// if (not CenterDep) or (CenterDep and (dmcom.FilterDepID=SelfDepId)) then
 if (not CenterDep) or (CenterDep and (dmcom.taClientDEPID.AsInteger=SelfDepId)) then
 with dmcom do
 if dg1.SelectedField = taClientADDRESS then
  case key of
  VK_RETURN:
   begin
    dg1edittext:=dg1.InplaceEditor.EditText;
    lbaddress.Visible:=false;
    dg1.InplaceEditor.EditText:=dg1edittext;
    try
     if dg1.InplaceEditor.EditText ='' then dg1.InplaceEditor.EditText:=' ';
     ExecuteQutmp(quTmp,'select d_address_id from d_address where fdel=0 and address='''+
                dg1.InplaceEditor.EditText+'''', 1, FieldsRes);
     if (FieldsRes[0]=null) then
      begin
       OpenDataSets([taAddress]);
       taAddress.Insert;
       taAddressADDRESS.AsString:=dg1.InplaceEditor.EditText;
       taAddress.Post;
       i:=taAddressD_ADDRESS_ID.AsInteger;
       CloseDataSets([taAddress]);
       taClient.Edit;
       taClientADDRESS.AsString:=dg1.InplaceEditor.EditText;
       taClientADDRESSID.AsInteger:=i;{&&&&&}
       taClient.Post;
       taClient.Refresh;
      end
     else
      begin
       taClient.Edit;
       taClientADDRESS.AsString:=dg1.InplaceEditor.Text;
       taClientADDRESSID.AsInteger:=FieldsRes[0];
       taClient.Post;
       taClient.Refresh;
      end;
    finally
     Finalize(FieldsRes);
    end;
    flFieldED:=false;
    ActiveControl:=dg1;
    dg1.SelectedField:=taClient.FieldByName('HOME_FLAT');
   end;
  VK_TAB: begin if lbaddress.Visible then ActiveControl:=lbaddress  end;
  VK_UP, VK_DOWN:
   begin
    if flFieldED then
    begin
     i:= taClientCLIENTID.AsInteger;
     taClient.Locate('CLIENTID',value,[]);
     dg1.SelectedField:=taClientADDRESS;
     taClient.Refresh;
     taClient.Locate('CLIENTID',i,[]);
     dg1.SelectedField:=taClientADDRESS;
     flFieldED:=false;
    end;
  {����� ������ �������}
    dg1edittext:=dg1.InplaceEditor.EditText;
    if CreatePmAddress(dg1.InplaceEditor.EditText) then
    begin
     lbaddress.Visible:=true;
//    lbaddress.Left:=dg1.SelectedRect.Right+1;
    end
    else lbaddress.Visible:=false;
    dg1.InplaceEditor.EditText:=dg1edittext;
   end;
  else
  begin
   dg1edittext:=dg1.InplaceEditor.EditText;
   if CreatePmAddress(dg1.InplaceEditor.EditText) then
   begin
    lbaddress.Visible:=true;
//    lbaddress.Left:=dg1.SelectedRect.Right+1;
   end
   else lbaddress.Visible:=false;
   dg1.InplaceEditor.EditText:=dg1edittext;
   dg1.InplaceEditor.SelStart:=length(dg1edittext);
   flFieldED:=true;
   value:=taClientCLIENTID.Asinteger;
  end
 end
 else
  begin
   {������� �� ������ ����}
   if key = vk_return then
   begin
    if dg1.SelectedField = taclientnodcard then begin dg1.SelectedField := taClientNAME; flClientName:=true; end
    else
    if dg1.SelectedField = taClientNAME then
     if flClientName then dg1.SelectedField := taClientADDRESS else flClientName:=true
    else
    if dg1.SelectedField = taClientADDRESS then begin dg1.SelectedField := taClientHOME_FLAT; flHome_falt:=TRUE end
    else
    if (dg1.SelectedField = taClientHOME_FLAT) then
     begin
     if flHome_falt then
     begin  if ((taClient.State = dsEdit) or (taClient.State = dsInsert)) then taClient.Post;
     ActiveControl := edNodcard; end else flHome_falt:=true; end
   end;

   if flFieldED then
    begin
     taClient.Refresh;
     flFieldED:=false;
     dg1edittext:=dg1.InplaceEditor.EditText;
     lbaddress.Visible:=false;
     dg1.InplaceEditor.EditText:=dg1edittext;
    end
   else begin
    dg1edittext:=dg1.InplaceEditor.EditText;
    lbaddress.Visible:=false;
    dg1.InplaceEditor.EditText:=dg1edittext;
   end
  end
end;

procedure TfmClient.dg1DblClick(Sender: TObject);
var  curval:integer;
begin
 with dmcom do
 begin
  if (dg1.SelectedField = taClientF1)and
     ((not CenterDep)or(CenterDep and (FilterDepID=SelfDepId))) then
   begin
    curval:=taClientFMAIN.AsInteger;
    taClient.Edit;
    taClientFMAIN.AsInteger:=integer(not boolean(curval));
    taClient.Post;
    tr.CommitRetaining;
    taClient.Refresh;
   end;
 end
end;

procedure TfmClient.nallfmainClick(Sender: TObject);
begin
 with dmcom do
 begin
  case TPopupMenu(Sender).Tag of
  0: begin FilterFmain1:=0; FilterFmain2:=1; end;
  1: begin FilterFmain1:=1; FilterFmain2:=1; end;
  2: begin FilterFmain1:=0; FilterFmain2:=0; end;
  end;
  ReOpenDataSets([taClient]);
  sifmain.BtnCaption:=TMenuItem(Sender).Caption
  end;
 end;

procedure tfmclient.pmDepClick (Sender: TObject);
begin
 dmcom.FilterDepID:=TMenuItem(Sender).Tag;
 sidep.BtnCaption:=TMenuItem(Sender).Caption;
 ReOpenDataSets([dmcom.taClient]);
{ if (CenterDep) and (dmcom.FilterDepID=SelfDepId) and  then
  begin dg1.ReadOnly:= false;  sidel.Visible:=true; sisellitem.Left:=66;
        dg1.Options:=[dgEditing,dgAlwaysShowEditor,dgTitles, dgIndicator,dgColumnResize,
                      dgColLines,dgRowLines,dgCancelOnExit]   end
 else begin dg1.ReadOnly:= Centerdep;  siDel.Visible:=not Centerdep; sisellitem.Left:=0;
            if CenterDep then dg1.Options:= [dgTitles,dgIndicator,dgColumnResize,dgColLines,
                              dgRowLines,dgCancelOnExit]
            else dg1.Options:=[dgEditing,dgAlwaysShowEditor,dgTitles,
                 dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgCancelOnExit]  end}
end;

procedure TfmClient.acPrintExecute(Sender: TObject);
begin
 pdg1.Title.Clear;
 pdg1.Title.Add('���������� �����������');
 pdg1.Title.Add(sifmain.BtnCaption);
 if dmcom.FilterDepID=0 then pdg1.Title.Add(sidep.BtnCaption)
 else pdg1.Title.Add('�����   '+sidep.BtnCaption);
 pdg1.Print;
end;

procedure TfmClient.NallClick(Sender: TObject);
begin
 dmcom.fdata:=false;
 dmcom.ClientUpDate:=false; 
 ReOpenDataSets([dmcom.taClient]);
 ldate.Caption:='';
 sidate.BtnCaption:=Nall.Caption;
end;

procedure TfmClient.NdateClick(Sender: TObject);
begin
 if GetPeriod(dmcom.bdata, dmcom.edata) then
 begin
  dmcom.fdata:=true;
  dmcom.ClientUpDate:=false;
  ReOpenDataSets([dmcom.taClient]);
  ldate.Caption:=' � '+datetimetostr(dmcom.bdata)+' �� '+ datetimetostr(dmcom.edata);
  sidate.BtnCaption:=Ndate.Caption;  
 end
end;

procedure TfmClient.acDelExecute(Sender: TObject);
begin
 dmCom.taClient.Delete
end;

procedure TfmClient.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 with dmcom do
 if dg1.SelectedField = taClientADDRESS then
 case key of
  VK_TAB: begin if lbaddress.Visible then ActiveControl:=lbaddress  end;
 end;
end;

procedure TfmClient.acCalcSumExecute(Sender: TObject);
begin
 Screen.Cursor:=crSQLWait;
 with dm, qutmp do
 begin
  ExecSQL('EXECUTE PROCEDURE FILL_CLIENT ', dmCom.quTmp);
 end;
 ReOpenDataSets([dmcom.taClient]);
 Screen.Cursor:=crDefault;
end;

procedure TfmClient.acPrintLetterExecute(Sender: TObject);
begin
  PostDataSet(dmCom.taClient);
  dmReport.PrintDocumentB(letter);
end;

procedure TfmClient.acUncheckLetterExecute(Sender: TObject);
var
  b : Pointer;
begin
  if MessageDialog('�� ������������� ������ ����� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  PostDataSet(dmCom.taClient);
  dmCom.taClient.DisableScrollEvents;
  dmCom.taClient.DisableControls;
  b := dmCom.taClient.GetBookmark;
  Screen.Cursor := crSQLWait;
  try
    dmCom.taClient.First;
    while not dmCom.taClient.Eof do
    begin
      if(dmCom.taClientISLETTER.AsInteger = 1) then
      begin
        dmCom.taClient.Edit;
        dmCom.taClientISLETTER.AsInteger := 0;
        dmCom.taClient.Post;
      end;
      dmCom.taClient.Next;
    end
  finally
    dmCom.taClient.GotoBookmark(b);
    dmCom.taClient.EnableScrollEvents;
    dmCom.taClient.EnableControls;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmClient.acLoseExecute(Sender: TObject);
var oldnodcard, newnodcard, name:string;
begin
 if (siLose.BtnCaption='�����.'+#13#10+'����� [F5]') then
 begin
  PostDataSet(dmcom.taClient);
  ExecSQL('execute procedure restore_nodcard ('#39+dmcom.taClientNODCARD.AsString+#39')', dm.quTmp);
  dmcom.taClient.Refresh;
 end
 else
 begin
  if MessageDialog('�������� ����� � ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then begin
   if not (dmcom.taClient.State in [dsEdit, dsInsert]) then dmcom.taClient.Edit;
   dmcom.taClientF_LOSE.AsInteger:=1;
   dmcom.taClient.Post;
  end else begin
   oldnodcard:=dmcom.taClientNODCARD.AsString;
   dm.IsClient:=0;
   if ShowAndFreeForm(TfmVibcard, Self, TForm(fmVibcard), True, False)=mrOk then
   begin
    newnodcard:=dmCom.taClientNODCARD.AsString;
    if newnodcard<>oldnodcard then begin
     try
      if dmCom.taClientNAME.IsNull then name:='' else name:=dmCom.taClientNAME.AsString;
      dmcom.taClient.Insert;
      dmcom.taClientNODCARD.AsString:=oldnodcard;
      dmcom.taClientNAME.AsString:=name;
      dmCom.taClientADDRESSID.AsInteger:=1;
      dmcom.taClientF_LOSE.AsInteger:=1;
      dmcom.taClientF2.AsInteger:=0;
      dmcom.taClientF3.AsInteger:=0;            
      dmcom.taClient.Post;
      ReOpenDataSet(dmcom.taClient);
      dmcom.taClient.Locate('NODCARD', newnodcard, [])
     except
      MessageDialog('��������� ����� ��� ���������!', mtWarning, [mbOk], 0);
     end;
    end;
   end
  end
 end
end;

procedure TfmClient.acVibcardExecute(Sender: TObject);
begin
 dm.IsClient:=0;
 ShowAndFreeForm(TfmVibcard, Self, TForm(fmVibcard), True, False);
end;

procedure TfmClient.acVibcardUpdate(Sender: TObject);
begin
 if (dmcom.taClientDEPID.AsInteger=SelfDepId) and
    (dmcom.taClientCLIENTID.AsInteger<>-1) and
    (dmcom.taClientF_LOSE.AsInteger=0) then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false
end;

procedure TfmClient.acSellItemExecute(Sender: TObject);
var idclient:integer;
    FieldsRes:t_array_var;
begin
  if (dmcom.taClientCLIENTID.AsInteger<>-1) then
  with dm, dmcom do
  begin
   try
    if not CenterDep then
     ExecuteQutmp(quTmp,'select count(*) from sellitem where clientid='+dmcom.taClientCLIENTID.AsString,
                 1, FieldsRes)
    else
     ExecuteQutmp(quTmp,'select count(*) from sellitem s, client cl where '+
      's.clientid=cl.clientid and '+'cl.nodcard='#39+dmcom.taClientNODCARD.AsString+#39,
       1, FieldsRes);
    idclient:=FieldsRes[0];
   finally
    Finalize(FieldsRes);
   end;
   if idclient=0 then MessageDialog('����. ����� �� ������������ � ��������', mtInformation, [mbOk], 0)
   else ShowAndFreeForm(TfmNsellItem, Self, TForm(fmNsellItem), True, False);
  end
end;

procedure TfmClient.acLoseUpdate(Sender: TObject);
var i:integer;
begin
 if (dmcom.taClientCLIENTID.AsInteger=-1) then TAction(Sender).Enabled:=false
 else begin
  if CenterDep then
  begin
   i:=0;
   if dmcom.taClientF_LOSE.AsInteger=1 then siLose.BtnCaption:='�����.'+#13#10+'����� [F5]'
   else begin siLose.BtnCaption:='�����'+#13#10+'������� [F5]'; i:=1 end;
   if (i=1) and (dmcom.taClientDEPID.AsInteger=SelfDepId) then TAction(Sender).Enabled:=true
   else if (i=0) then TAction(Sender).Enabled:=true
   else TAction(Sender).Enabled:=false;
  end
  else
  begin
   if (dmcom.taClientF_LOSE.AsInteger=0) then TAction(Sender).Enabled:=true
   else TAction(Sender).Enabled:=false;
  end
 end;
end;

procedure TfmClient.chblosecardClick(Sender: TObject);
begin
 dmcom.IsLoseCard:=chblosecard.Checked;
 ReOpenDataSet(dmcom.taClient);
end;

procedure TfmClient.acOvereatingUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dmcom.taClientCLIENTID.IsNull) and
  (not chblosecard.Checked) and
  ((dmcom.FilterDepID=SelfDepId) or (dmcom.FilterDepID=0));
end;

procedure TfmClient.acOvereatingExecute(Sender: TObject);
var newclientid, oldclientid:integer;
    nodcard:string;
begin
 newclientid:=-2; oldclientid:=-2;

 PostDataSet(dmCom.taClient);
 if not dmcom.taClient.Locate('F2',1,[]) then MessageDialog('�������� ����. ����� �� ����������� � ���� *', mtWarning, [mbOk], 0)
 else if dmcom.taClientCLIENTID.AsInteger=-1 then MessageDialog('����� �� ����� ���� ���������� � "��� ����. �����"', mtWarning, [mbOk], 0)
  else if dmcom.taClientF_LOSE.AsInteger=1 then MessageDialog('����. ����� �� ������ ���� ���������', mtWarning, [mbOk], 0)
   else if dmcom.taClientDEPID.AsInteger<>SelfDepId then MessageDialog('����. ����� ������ �������� ��������', mtWarning, [mbOk], 0)
    else begin newclientid:=dmCom.taClientCLIENTID.AsInteger; nodcard:=dmCom.taClientNODCARD.AsString end;

 if not dmcom.taClient.Locate('F3',1,[]) then MessageDialog('�������� ����. ����� �� ����������� � ���� **', mtWarning, [mbOk], 0)
 else if dmcom.taClientCLIENTID.AsInteger=-1 then MessageDialog('������ ����� "��� ����. �����" ����������', mtWarning, [mbOk], 0)
  else if dmcom.taClientF_LOSE.AsInteger=1 then MessageDialog('����. ����� �� ������ ���� ���������', mtWarning, [mbOk], 0)
   else if dmcom.taClientDEPID.AsInteger<>SelfDepId then MessageDialog('����. ����� ������ �������� ��������', mtWarning, [mbOk], 0)
    else oldclientid:=dmCom.taClientCLIENTID.AsInteger;

 if (newclientid<>-2) and (oldclientid<>-2) then
 begin
  Screen.Cursor:=crSQLWait;
  ExecSQL('execute procedure OVEREATINGCARD ('+inttostr(newclientid)+', '+
  inttostr(oldclientid)+')', dm.quTmp);
  ExecSQL('execute procedure calc_sum_sell ('+inttostr(newclientid)+', 1)', dm.quTmp);
  ReOpenDataSet(dmcom.taClient);
  dmcom.taClient.Locate('NODCARD',nodcard,[]);
  Screen.Cursor:=crDefault;
 end;
end;

procedure TfmClient.NDateUpdateClick(Sender: TObject);
begin
 if GetPeriod(dmcom.bdata, dmcom.edata) then
 begin
  dmcom.ClientUpDate:=true;
  dmcom.fdata:=false;  
  ReOpenDataSets([dmcom.taClient]);
  ldate.Caption:=' � '+datetimetostr(dmcom.bdata)+' �� '+ datetimetostr(dmcom.edata);
  sidate.BtnCaption:=NDateUpdate.Caption;  
 end
end;

procedure TfmClient.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100110)
end;

end.
