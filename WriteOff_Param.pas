unit WriteOff_Param;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, 
  ExtCtrls, DBGridEh, ActnList, jpeg, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfrmWriteoff_Param = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siOk: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    DBGridEh1: TDBGridEh;
    siCancel: TSpeedItem;
    ActionList1: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acPrint: TAction;
    acOk: TAction;
    acCancel: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acOkExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  frmWriteoff_Param: TfrmWriteoff_Param;

implementation

{$R *.dfm}

uses data3, AllowancesPrn, dbUtil, db, ComData, MsgDialog;

procedure TfrmWriteoff_Param.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  case fmActAllowancesPrn.Itype of
    1:  Self.Caption := '���������� ����� ���� ��������';
    2:  Self.Caption := '���������� ������ ��������';
    3:  Self.Caption := '���������� ��������';
    4:  Self.Caption := '���������� ������ �����';
    5:  Self.Caption := '���������� ������� ��������';
  end;
  dm3.quD_writeoff.Params.ByName['IType'].AsInteger := fmActAllowancesPrn.Itype;
  OpenDataSet(dm3.quD_writeoff);
end;

procedure TfrmWriteoff_Param.acAddExecute(Sender: TObject);
begin
   with dm3.quD_writeoff, Params do
     begin
       ByName['ITYPE'].AsInteger:=fmActAllowancesPrn.Itype;
       Append;
     end;
end;

procedure TfrmWriteoff_Param.acDelExecute(Sender: TObject);
begin
  if (dm3.quD_writeoffID.AsInteger = 8) or (dm3.quD_writeoffID.asInteger = 4) then
    MessageDialog('��� �������� ������ �������', mtWarning, [mbOk], 0)
  else
    dm3.quD_writeoff.delete;
end;

procedure TfrmWriteoff_Param.acOkExecute(Sender: TObject);
var sqlstr: string;
begin
  if (dm3.quD_writeoff.State = dsInsert) or (dm3.quD_writeoff.State = dsEdit) then
     dm3.quD_writeoff.Post;

  case fmActAllowancesPrn.Itype of
    1:  sqlstr := 'ActName_ID';
    2:  sqlstr := 'WriteOffReason_ID';
    3:  sqlstr := 'DefectDiscript_ID';
    4:  sqlstr := 'spoilingreason_ID';
    5:  sqlstr := 'Solution_ID';
  end;
  execSql('update Writeoff_sinv  set ' + sqlstr + ' = ' + trim(dm3.quD_writeoffID.AsString) +
          ' where SInvID = ' + dm3.quActAllowParamPrnSINVID.AsString , dm3.quTmp);
  CloseDataSet(dm3.quD_writeoff);
  Close;
end;

procedure TfrmWriteoff_Param.acCancelExecute(Sender: TObject);
begin
  if (dm3.quD_writeoff.State = dsInsert) or (dm3.quD_writeoff.State = dsEdit) then
    dm3.quD_writeoff.Cancel;
  CloseDataSet(dm3.quD_writeoff);
  Close;
end;

end.
