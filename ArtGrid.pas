unit ArtGrid;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, db, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  Menus, rxPlacemnt;

type
  TfrArtGrid = class(TFrame)
    Panel2: TPanel;
    Label1: TLabel;
    edArt: TEdit;
    dg1: TM207IBGrid;
    pmVisCol: TPopupMenu;
    N1: TMenuItem;
    fs1: TFormStorage;
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    function dg1GetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;
    procedure dg1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    ArtSearchChanged:boolean;
  public
    { Public declarations }
  end;

implementation

uses comdata, VisCol;

{$R *.DFM}

procedure TfrArtGrid.edArtChange(Sender: TObject);
begin
  ArtSearchChanged:=True;
end;

procedure TfrArtGrid.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with dmCom do
  if Key=VK_RETURN then
    begin
      if ArtSearchChanged then taArt.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey])
      else
        begin
          taArt.Next;
          taArt.LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
        end;
      ArtSearchChanged:=False;
    end;

end;

function TfrArtGrid.dg1GetCellCheckBox(Sender: TObject; Field: TField;
  var StateCheckBox: Integer): Boolean;
begin
  Result:=Field.FieldName='UNITID';
end;

procedure TfrArtGrid.dg1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var p: TPoint;
begin
  if (X<12) and (Y<16) and (Button=mbRight) then
    begin
      p:=dg1.ClientToScreen(Point(X,Y));
      pmVisCol.PopUp(P.X,P.Y);
    end;
end;

end.
