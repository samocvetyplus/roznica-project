object fmSellWH: TfmSellWH
  Left = 242
  Top = 101
  Width = 270
  Height = 311
  Caption = #1040#1085#1072#1083#1080#1079' '#1074#1099#1088#1091#1095#1082#1080' '#1080' '#1089#1082#1083#1072#1076#1072' '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbComp: TLabel
    Left = 4
    Top = 76
    Width = 71
    Height = 13
    Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object chlbProd: TCheckListBox
    Left = 87
    Top = 77
    Width = 158
    Height = 200
    Flat = False
    ItemHeight = 13
    TabOrder = 0
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 42
    Width = 262
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laPeriod: TLabel
      Left = 84
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laDep: TLabel
      Left = 168
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dmServ.pmList3
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 200
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1055#1077#1088#1080#1086#1076'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 262
    Height = 42
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 55
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 201
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siCalc: TSpeedItem
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      Hint = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1086#1089#1090#1086#1103#1085#1080#1103' '#1089#1082#1083#1072#1076#1072'|'
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siCalcClick
      SectionName = 'Untitled (0)'
    end
  end
  object svdFile: TOpenDialog
    DefaultExt = 'nlz'
    Left = 20
    Top = 206
  end
end
