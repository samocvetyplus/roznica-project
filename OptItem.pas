unit OptItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Buttons, db, Menus, StdCtrls, Mask, DBCtrls, jpeg,
  rxPlacemnt, rxSpeedbar;

type
  TfmOptItem = class(TForm)
    sb1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    pm1: TPopupMenu;
    N1: TMenuItem;
    pm2: TPopupMenu;
    N2: TMenuItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Panel2: TPanel;
    dg1: TRxDBGrid;
    FormStorage1: TFormStorage;
    N3: TMenuItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure sbAddClick(Sender: TObject);
    procedure sbDelClick(Sender: TObject);
    procedure M207IBGrid2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;

  public

    { Public declarations }
    procedure UpdateQW;    
  end;

var
  fmOptItem: TfmOptItem;

implementation

uses comdata, Data, M207Proc;

{$R *.DFM}

procedure TfmOptItem.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmOptItem.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmOptItem.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dm do
    begin
      OpenDataSets([taDItem]);
      UpdateQW;
    end;
end;

procedure TfmOptItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
    begin
      PostDataSets([taDItem]);
      CloseDataSets([quWHA2UID, taDItem,  quWHSZ, quWHA2SZ]);
      tr.CommitRetaining;
      dm.taSEl.Refresh;
      if WorkMode='DINV' then taDList.Refresh
      else if WorkMode='OPT' then taOptList.Refresh;
      SupFilter:=False;
      PriceFilter:=False;
    end;
end;

procedure TfmOptItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmOptItem.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='UID') then
    Background:=dmCom.clMoneyGreen;
end;

procedure TfmOptItem.sbAddClick(Sender: TObject);
var i: integer;
begin
  with dm, taDItem do
    begin
      Append;
      taDItemUID.AsInteger:=-1;
      with dg1 do
        begin
          i:=0;
          while Columns[i].Field.FieldName<>'W' do Inc(i);
          SelectedIndex:=i;
        end
    end;
end;

procedure TfmOptItem.sbDelClick(Sender: TObject);
begin
  with dm do
    begin
      taDItem.Delete;
    end;
end;

procedure TfmOptItem.M207IBGrid2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='SZ') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmOptItem.UpdateQW;
begin
  with dm, quElQW do
    begin
      Params[0].AsInteger:=taSElSElId.AsInteger;
      ExecQuery;
      sb1.Panels[0].Text:='���-��: '+Fields[0].AsString;
      sb1.Panels[1].Text:='���: '+Fields[1].AsString;
      Close;
    end;
end;

procedure TfmOptItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then Close;
end;

end.

