unit FindUID;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid;

type
  TfmFindUID = class(TForm)
    bbOK: TBitBtn;
    BitBtn2: TBitBtn;
    M207IBGrid1: TM207IBGrid;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFindUID: TfmFindUID;

implementation

uses Data;

{$R *.DFM}

procedure TfmFindUID.FormActivate(Sender: TObject);
begin
  bbOK.Enabled:=NOT dm.quFindUIDSItemId.IsNull
end;

end.
