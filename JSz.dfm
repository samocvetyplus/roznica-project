object fmJSz: TfmJSz
  Left = 190
  Top = 87
  Width = 881
  Height = 628
  HelpContext = 100401
  Align = alClient
  Caption = #1040#1085#1072#1083#1080#1079' ('#1087#1086' '#1088#1072#1079#1084#1077#1088#1072#1084')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 873
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 415
      Top = 7
      Width = 141
      Height = 13
      Caption = #1055#1086' '#1088#1072#1079#1084#1077#1088#1072#1084' '#1080#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1103
      Transparent = True
    end
    object Label2: TLabel
      Left = 415
      Top = 22
      Width = 71
      Height = 13
      Caption = #1057#1086' '#1074#1089#1090#1072#1074#1082#1072#1084#1080
      Transparent = True
    end
    object Label3: TLabel
      Left = 635
      Top = 7
      Width = 66
      Height = 13
      Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
      Transparent = True
    end
    object chbSup: TCheckBox
      Left = 400
      Top = 7
      Width = 12
      Height = 12
      TabOrder = 0
      OnClick = chbSupClick
    end
    object chbxInfo: TCheckBox
      Left = 620
      Top = 7
      Width = 12
      Height = 12
      Hint = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1087#1086#1083#1085#1091#1102' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1089#1086#1089#1090#1086#1103#1085#1080#1080' '#1072#1088#1090#1080#1082#1091#1083#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = chbxInfoClick
    end
    object cbIns: TCheckBox
      Left = 400
      Top = 22
      Width = 12
      Height = 12
      TabOrder = 2
      OnClick = cbInsClick
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 747
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitCalc: TSpeedItem
      BtnCaption = #1056#1072#1089#1095#1077#1090
      Caption = #1056#1072#1089#1095#1077#1090
      Hint = #1056#1072#1089#1095#1077#1090
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitCalcClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object spExport: TSpeedItem
      BtnCaption = #1058#1077#1082#1089#1090
      Caption = #1058#1077#1082#1089#1090
      DropDownMenu = pmText
      Hint = #1058#1077#1082#1089#1090'|'
      ImageIndex = 68
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1074' '#1079#1072#1103#1074#1082#1091'|'
      ImageIndex = 1
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1089#1086#1079#1076#1072#1085#1085#1099#1081' '#1076#1083#1103' '#1079#1072#1103#1074#1082#1080' '#1072#1088#1090#1080#1082#1091#1083'|'
      ImageIndex = 2
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1057#1086#1079#1076#1072#1090#1100
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1079#1072#1103#1074#1082#1091
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1079#1072#1103#1074#1082#1091'|'
      ImageIndex = 11
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 811
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object stbrJSz: TStatusBar
    Left = 0
    Top = 582
    Width = 873
    Height = 17
    Panels = <
      item
        Width = 503
      end
      item
        Width = 250
      end
      item
        Width = 120
      end>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 873
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object laPeriod: TLabel
      Left = 378
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laDep: TLabel
      Left = 714
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object lAll: TLabel
      Left = 16
      Top = 8
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dmServ.pmList3
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1055#1077#1088#1080#1086#1076'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 290
      Top = 2
      Visible = True
      OnClick = spitPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object spbr4: TSpeedBar
    Left = 0
    Top = 68
    Width = 873
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 3
    InternalVer = 1
    object lbMat: TLabel
      Left = 10
      Top = 8
      Width = 23
      Height = 13
      Caption = #1052#1072#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbGood: TLabel
      Left = 322
      Top = 8
      Width = 31
      Height = 13
      Caption = #1058#1086#1074#1072#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbComp: TLabel
      Left = 492
      Top = 8
      Width = 22
      Height = 13
      Caption = #1048#1079#1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbSup: TLabel
      Left = 156
      Top = 8
      Width = 37
      Height = 13
      Caption = #1043#1088'.'#1084#1072#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      BtnCaption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Hint = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = SpeedItem8Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem7: TSpeedItem
      BtnCaption = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      Caption = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      Hint = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = SpeedItem7Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      BtnCaption = #1058#1086#1074#1072#1088
      Caption = #1058#1086#1074#1072#1088
      Hint = #1058#1086#1074#1072#1088'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 355
      Top = 3
      Visible = True
      OnClick = SpeedItem9Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      BtnCaption = #1052#1072#1090#1077#1088#1080#1072#1083
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 35
      Top = 3
      Visible = True
      OnClick = SpeedItem10Click
      SectionName = 'Untitled (0)'
    end
  end
  object plTotal: TPanel
    Left = 0
    Top = 404
    Width = 873
    Height = 161
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
    object grTotal: TDBGridEh
      Left = 1
      Top = 1
      Width = 871
      Height = 159
      Align = alClient
      AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
      AllowedSelections = []
      Color = clBtnFace
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmServ.dsrTotJSz
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -8
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -8
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghMultiSortMarking, dghColumnResize, dghColumnMove]
      ParentFont = False
      RowDetailPanel.Color = clBtnFace
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clNavy
      TitleFont.Height = -8
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnGetCellParams = grTotalGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DEPNAME'
          Footers = <>
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SELLRANGE'
          Footers = <>
          Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
          Width = 78
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SELLQ'
          Footers = <>
          Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SELLW'
          Footers = <>
          Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
        end
        item
          EditButtons = <>
          FieldName = 'OUTRANGE'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082'|'#1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
          Width = 71
        end
        item
          EditButtons = <>
          FieldName = 'OUTQ'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082'|'#1050#1086#1083'-'#1074#1086
        end
        item
          EditButtons = <>
          FieldName = 'OUTW'
          Footers = <>
          Title.Caption = #1054#1089#1090#1072#1090#1086#1082'|'#1042#1077#1089
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 565
    Width = 873
    Height = 17
    Align = alBottom
    TabOrder = 5
    object g1: TProgressBar
      Left = 1
      Top = 1
      Width = 871
      Height = 15
      Align = alClient
      TabOrder = 0
    end
  end
  object gr: TDBGridEh
    Left = 0
    Top = 97
    Width = 873
    Height = 307
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsrJSz
    Flat = True
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmJSz
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = nAllzClick
    OnGetCellParams = grGetCellParams
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COMP'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1055#1088#1086#1080#1079#1074'.'
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1058#1086#1074#1072#1088
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1052#1072#1090'.'
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1042#1089#1090'.'
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART'
        Footer.FieldName = 'ART'
        Footers = <
          item
            FieldName = 'ART'
            ValueType = fvtCount
          end>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1040#1088#1090#1080#1082#1091#1083
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
      end
      item
        Color = clAqua
        EditButtons = <>
        FieldName = 'INS'
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1057#1091#1084#1084#1072
      end
      item
        Color = clAqua
        EditButtons = <>
        FieldName = 'INW'
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1042#1077#1089
      end
      item
        Color = clAqua
        EditButtons = <>
        FieldName = 'INQ'
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
      end
      item
        Color = clCaptionText
        EditButtons = <>
        FieldName = 'OUTS'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1057#1091#1084#1084#1072
      end
      item
        Color = clCaptionText
        EditButtons = <>
        FieldName = 'OUTW'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1042#1077#1089
      end
      item
        Color = clCaptionText
        EditButtons = <>
        FieldName = 'OUTQ'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'DEBTORS'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1080'|'#1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'DEBTORW'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1080'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'DEBTORQ'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1080'|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'SALES'
        Footers = <
          item
            FieldName = 'SALES'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080'|'#1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'SALEW'
        Footers = <
          item
            FieldName = 'SALEW'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SALEQ'
        Footers = <
          item
            FieldName = 'SALEQ'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080'|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RETS'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1090' '#1087#1086#1082#1091#1087'|'#1057#1091#1084#1084#1072' '
      end
      item
        EditButtons = <>
        FieldName = 'RETW'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1090' '#1087#1086#1082#1091#1087'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'RETQ'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1090' '#1087#1086#1082#1091#1087'|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RETOPTS'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1087#1090'.|'#1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'RETOPTW'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1087#1090'.|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'RETOPTQ'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1086#1087#1090'.|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RETVENDORS'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'|'#1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'RETVENDORW'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'RETVENDORQ'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'|'#1050'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'OPTS'
        Footers = <>
        Title.Caption = #1054#1087#1090'. '#1087#1088#1086#1076#1072#1078#1080'|C'#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'OPTW'
        Footers = <>
        Title.Caption = #1054#1087#1090'. '#1087#1088#1086#1076#1072#1078#1080'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'OPTQ'
        Footers = <>
        Title.Caption = #1054#1087#1090'. '#1087#1088#1086#1076#1072#1078#1080'|'#1050'-'#1074#1086
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'CURRS'
        Footers = <>
        Title.Caption = #1058#1077#1082#1091#1097#1080#1077'|'#1057#1091#1084#1084#1072
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'CURRW'
        Footers = <>
        Title.Caption = #1058#1077#1082#1091#1097#1080#1077'|'#1042#1077#1089
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'CURRQ'
        Footers = <>
        Title.Caption = #1058#1077#1082#1091#1097#1080#1077'|'#1050#1086#1083'-'#1074#1086
      end
      item
        Color = clMoneyGreen
        EditButtons = <>
        FieldName = 'ZQ'
        Footers = <>
      end
      item
        Color = clYellow
        EditButtons = <>
        FieldName = 'PERCENTSELL'
        Footers = <>
        Title.Caption = '% '#1087#1088#1086#1076'.'
      end
      item
        Color = clYellow
        EditButtons = <>
        FieldName = 'PERCENTRET'
        Footers = <>
        Title.Caption = '% '#1088#1086#1079'. '#1074#1086#1079'.'
      end
      item
        Color = clYellow
        EditButtons = <>
        FieldName = 'PERCENTSRET'
        Footers = <>
        Title.Caption = '% '#1087#1086#1089#1090'. '#1074#1086#1079#1074'.'
      end
      item
        EditButtons = <>
        FieldName = 'WRITE_OFFS'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'WRITE_OFFW'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'WRITE_OFFQ'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSS'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSQ'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSW'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object chlbMat: TCheckListBox
    Left = 21
    Top = 97
    Width = 140
    Height = 130
    OnClickCheck = chlbMatClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 7
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbGrMat: TCheckListBox
    Left = 182
    Top = 97
    Width = 140
    Height = 129
    OnClickCheck = chlbGrMatClickCheck
    Flat = False
    ItemHeight = 13
    TabOrder = 8
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbGood: TCheckListBox
    Left = 344
    Top = 97
    Width = 140
    Height = 129
    OnClickCheck = chlbGoodClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 9
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbProd: TCheckListBox
    Left = 507
    Top = 97
    Width = 140
    Height = 129
    OnClickCheck = chlbProdClickCheck
    Flat = False
    ItemHeight = 13
    TabOrder = 10
    Visible = False
    OnExit = chlbMatExit
  end
  object fmstrJsz: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 708
    Top = 152
  end
  object pmPrint: TPopupMenu
    Left = 752
    Top = 96
    object mnitPrintArt: TMenuItem
      Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100
      OnClick = spitPrintClick
    end
    object mnitPrintZ: TMenuItem
      Tag = 1
      Caption = #1047#1072#1103#1074#1082#1072
      OnClick = spitPrintClick
    end
  end
  object pmJSz: TPopupMenu
    Left = 812
    Top = 96
    object nDouble: TMenuItem
      Caption = #1076#1091#1073#1083#1080#1088#1086#1074#1072#1090#1100
      Visible = False
    end
    object nAllz: TMenuItem
      Caption = #1074#1089#1077#1075#1086' '#1079#1072#1103#1074#1083#1077#1085#1086
      OnClick = nAllzClick
    end
  end
  object pmText: TPopupMenu
    Left = 692
    Top = 96
    object miVed: TMenuItem
      Caption = #1074#1077#1076#1086#1084#1086#1089#1090#1100
      OnClick = spExportClick
    end
    object miZ: TMenuItem
      Caption = #1079#1072#1103#1074#1082#1072
      OnClick = miZClick
    end
  end
  object svdFile: TOpenDialog
    DefaultExt = 'nlz'
    Left = 676
    Top = 270
  end
end
