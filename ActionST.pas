unit ActionST;

interface

uses
    Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, StrUtils,
  dxSkinscxPCPainter, cxGridBandedTableView, cxGridDBBandedTableView, DateUtils,
  cxContainer, dxLayoutcxEditAdapters, FIBDatabase, pFIBDatabase,
  dxLayoutControl, cxTextEdit, cxDBEdit, dxLayoutLookAndFeels,
  dxSkinsdxBarPainter, dxBar, ActnList, frmPopup,  cxSplitter,
  cxCalendar, cxGroupBox, dxBarExtItems, cxGridExportLink, ShlObj, StdActns,
  dxSkinsDefaultPainters, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, ComObj, ActiveX, FIBQuery, pFIBQuery, cxCheckBox, cxBarEditItem,
  MidasLib, Provider, pFIBClientDataSet,  MemDS, DBAccess, MyAccess, 
 ImgList, DBClient;

type
  TFmActionST = class(TForm)
    BarManager: TdxBarManager;
    BarFilter: TdxBar;
    ButtonRange: TdxBarLargeButton;
    ButtonRefresh: TdxBarLargeButton;
    ButtonSettings: TdxBarLargeButton;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonView: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    ButtonOpen: TdxBarLargeButton;
    ButtonPrint: TdxBarLargeButton;
    ButtonDepartment: TdxBarLargeButton;
    ActionList: TActionList;
    acChangeRange: TAction;
    DepartmentsPopupMenu: TdxBarPopupMenu;
    quTemp: TpFIBQuery;
    Image32: TcxImageList;
    OrderSource: TDataSource;
    Connection: TMyConnection;
    dtAction: TMyQuery;
    Provider: TpFIBDataSetProvider;
    Action: TpFIBClientDataSet;
    dtActionid: TIntegerField;
    dtActiontype: TIntegerField;
    dtActioncode: TStringField;
    dtActionphone_number: TStringField;
    dtActiondatetime_generate: TDateTimeField;
    dtActiondatetime_activate: TDateTimeField;
    dtActionindex_activate: TLargeintField;
    dtActiontype_present: TIntegerField;
    dtActiontype_action: TIntegerField;
    dtActionDEP: TIntegerField;
    Actionid: TIntegerField;
    Actiontype: TIntegerField;
    Actioncode: TStringField;
    Actionphone_number: TStringField;
    Actiondatetime_generate: TDateTimeField;
    Actiondatetime_activate: TDateTimeField;
    Actionindex_activate: TLargeintField;
    Actiontype_present: TIntegerField;
    Actiontype_action: TIntegerField;
    ActionDEP: TIntegerField;
    ActionPresent: TStringField;
    dsAction: TMyDataSource;
    ActionSource: TDataSource;
    client: TpFIBDataSet;
    ClientProvider: TDataSetProvider;
    ActionfIOCL: TStringField;
    ClientCL: TClientDataSet;
    clientID: TFIBIntegerField;
    clientCARD: TFIBStringField;
    clientTITLE: TFIBStringField;
    clientPHONEMOBILE: TFIBStringField;
    ActionID_FILIAL: TIntegerField;
    ActionCard: TStringField;
    ClientCLID: TIntegerField;
    ClientCLCARD: TStringField;
    ClientCLTITLE: TStringField;
    clientPhone: TStringField;
    ActionPhone: TStringField;
    ClientCLPhone: TStringField;
    DepFilial: TpFIBDataSet;
    DepFilialProvider: TDataSetProvider;
    DepFilialCL: TClientDataSet;
    DepFilialID: TFIBIntegerField;
    DepFilialID_FILIAL: TFIBIntegerField;
    DepFilialDEP: TFIBIntegerField;
    DepFilialCLID: TIntegerField;
    DepFilialCLDEP: TIntegerField;
    ActionID_CENTER: TIntegerField;
    DepFilialCLID_FILIAL: TIntegerField;
    Sel: TpFIBDataSet;
    SelADATE: TFIBDateTimeField;
    SelCLIENTID: TFIBIntegerField;
    SelPRICE0: TFIBFloatField;
    SelW: TFIBFloatField;
    SelQ0: TFIBFloatField;
    SelSZ: TFIBStringField;
    SelUID: TFIBIntegerField;
    SelDEPID: TFIBIntegerField;
    SelPrice: TFIBFloatField;
    DataSource1: TDataSource;
    Grid: TcxGrid;
    ViewOrders: TcxGridDBBandedTableView;
    ViewHistory: TcxGridDBTableView;
    LevelOrders: TcxGridLevel;
    LevelHistory: TcxGridLevel;
    ViewOrdersid: TcxGridDBBandedColumn;
    ViewOrderstype: TcxGridDBBandedColumn;
    ViewOrderscode: TcxGridDBBandedColumn;
    ViewOrdersphone_number: TcxGridDBBandedColumn;
    ViewOrdersdatetime_generate: TcxGridDBBandedColumn;
    ViewOrdersdatetime_activate: TcxGridDBBandedColumn;
    ViewOrdersDEP: TcxGridDBBandedColumn;
    ViewOrdersPresent: TcxGridDBBandedColumn;
    ViewOrdersfIOCL: TcxGridDBBandedColumn;
    ViewOrdersCard: TcxGridDBBandedColumn;
    ViewHistoryADATE: TcxGridDBColumn;
    ViewHistoryCLIENTID: TcxGridDBColumn;
    ViewHistoryPRICE0: TcxGridDBColumn;
    ViewHistoryW: TcxGridDBColumn;
    ViewHistoryQ0: TcxGridDBColumn;
    ViewHistorySZ: TcxGridDBColumn;
    ViewHistoryUID: TcxGridDBColumn;
    ViewHistoryDEPID: TcxGridDBColumn;
    ViewHistoryPrice: TcxGridDBColumn;
    Dep: TpFIBDataSet;
    DepProvider: TDataSetProvider;
    DEPCL: TClientDataSet;
    ActionACTION: TStringField;
    ViewOrdersACTION: TcxGridDBBandedColumn;
    DepD_DEPID: TFIBIntegerField;
    DepNAME: TFIBStringField;
    DEPCLD_DEPID: TIntegerField;
    DEPCLNAME: TStringField;
    ActionDEP_NAME: TStringField;
    procedure acChangeRangeExecute(Sender: TObject);
    procedure OnRangeOk(Popup: TPopup);
    procedure ButtonDepartmentClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DepartmentMenuButtonClick(Sender: TObject);
    function  GetSelectedDep: Integer;
    procedure OrdersListBeforeOpen(DataSet: TDataSet);
    procedure ActionCalcFields(DataSet: TDataSet);
    procedure clientCalcFields(DataSet: TDataSet);
    procedure SelCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    RangeStart: TDate;
    RangeEnd: TDate;
  end;

var
  FmActionST: TFmActionST;
  SelfDepID: Integer;
  SelectedDep: Integer;

implementation
   uses frmBuyRangePopup, ComData,data,

   uBuy, uDialog, uGridPrinter;
{$R *.dfm}

procedure TFmActionST.ActionCalcFields(DataSet: TDataSet);
var
s:string;
begin
if Actiontype_present.asInteger = 1  then ActionPresent.value := '��� �������';
if Actiontype_present.asInteger = 2  then ActionPresent.value := '�� �������' ;
if Actiontype_present.asInteger = 0  then ActionPresent.value := '' ;
 ActionPhone.value:=Actionphone_number.AsString;
if Actiontype_action.asInteger = 1  then ActionACTION.value := '�����';
if Actiontype_action.asInteger = 0  then ActionACTION.value := '�������';

 if Action.fieldbyname('phone').AsString<> '' then
 begin
if ClientCL.FindKey([Action.fieldbyname('phone').AsString]) then
  begin
    ActionCard.value := ClientCLCard.AsString;
    ActionFIOCL.value := ClientCLTITLE.AsString;
    ActionID_CENTER.value := ClientCLID.Asinteger;
  end;
 end;

 if Action.fieldbyname('dep').asInteger<>0 then
 begin

 if DepCL.FindKey([ActionDep.Asinteger]) then
  begin
   ActionDEP_NAME.Value:= DepCLName.AsString;
  end;

   DepFilialCL.first;
   if DepFilialCL.FindKey([ActionID_center.Asinteger, ActionDEP.Asinteger ]) then
  begin
    ActionID_FILIAL.value := DepFilialCLID_FILIAL.Asinteger;
  end;


 end;

//  s:=Action.fieldbyname('phone_number').asString;
//
//  s:=StringReplace(s,'8','+7',[]);
//
//  Insert('(', s, 3);
//  Insert(')', s, 7);
//  Insert('-', s, 11);
//  Insert('-', s, 14);
//  ActionPhone.value:=s;
//  if Action.fieldbyname('dep').asInteger<>0 then
//  begin
//  client.close;
//  client.ParamByName('phone').AsString:=s;
//  client.ParamByName('dep').asInteger:=Action.fieldbyname('dep').asInteger;
//  client.open;
//  ActionfIOCL.value:=client.fieldbyname('title').AsString;
//  ActionCARD.value:=client.fieldbyname('CARD').asString;
//  ActionID_FILIAL.value:=client.fieldbyname('ID_FILIAL').asInteger;
//  end;
end;

procedure TFmActionST.ButtonDepartmentClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonDepartment then
    begin
      GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonDepartment.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      DepartmentsPopupMenu.Popup(X, Y);
    end;
  end;

end;

procedure TFmActionST.clientCalcFields(DataSet: TDataSet);
var s:string;
begin
 s:= Client.FieldByName('phone$mobile').AsString;
     s:=StringReplace(s,'+7','8',[]);

     while pos('(',s)<>0 do delete(s,pos('(',s),1);
     while pos(')',s)<>0 do delete(s,pos(')',s),1);
     while pos('-',s)<>0 do delete(s,pos('-',s),1);
     ClientPhone.value:=s;

end;

function  TFmActionST.GetSelectedDep: Integer;
begin
  Result := ButtonDepartment.Tag;
end;
procedure TFmActionST.DepartmentMenuButtonClick(Sender: TObject);
var Button: TdxBarButton;
begin
  if Sender is TdxBarButton then
    begin
      Button := TdxBarButton(Sender);
      ButtonDepartment.Caption := Button.Caption;
      ButtonDepartment.Tag := Button.Tag;
      SelectedDep := GetSelectedDep;

      if not SelfDepID=1 then
      begin
        Exit;
      end;


      with Action do
        begin
          DisableControls;
         // DisableScrollEvents;

          if Filtered then
            begin
              Filtered := false;
            end;

          if ButtonDepartment.Tag <> -1000 then
            begin
              Filter := 'Dep = ' + IntToStr(ButtonDepartment.Tag);
              Filtered := true;
            end;

          EnableControls;
         // EnableScrollEvents;
        end;
    end;
end;


procedure TFmActionST.FormCreate(Sender: TObject);
var
  Button: TdxBarButton;
  Button2: TdxBarButton;
  ButtonLink: TdxBarItemLink;
  TextSQL: String;
begin

  RangeEnd:=now;

  RangeStart:= ( strtodate ( (FormatDateTime('dd/mm',RangeEnd)) + (FormatDateTime('/yyyy', Date)) )        - 30);

with quTemp do
    begin
      Close;
      SQL.Text := 'select gen_id(selfdepid,0) from rdb$database';
      ExecQuery;
      SelfDepID := Fields[0].AsInteger;
    end;

    
  with quTemp do
    begin
      Close;

      TextSQL := 'select d_depid, coalesce(sname,' + #39 + '  ���  ' + #39 + ') from d_dep where d_depid <> 1 and d_depid<>26';

      if not SelfDepID=1 then
      begin
        TextSQL := TextSQL + ' and d_depid = ' + IntToStr(SelfDepID);
      end;

      SQL.Text := TextSQL;

      ExecQuery;

      while not EOF do
        begin
          Button := TdxBarButton(BarManager.AddItem(TdxBarButton));

          Button.ButtonStyle := bsChecked;

          Button.GroupIndex := 6;

          Button.LargeImageIndex := ButtonDepartment.LargeImageIndex;

          Button.Tag := Fields[0].AsInteger;

          Button.Caption := trim(Fields[1].AsString);

          Button.OnClick := DepartmentMenuButtonClick;

          ButtonLink := DepartmentsPopupMenu.ItemLinks.Add;

          ButtonLink.Item := Button;

          if DepartmentsPopupMenu.Itemlinks.Count = 2 then
          begin
            ButtonLink.BeginGroup := True;
          end;

          if SelfDepID=1 then
            begin
              if Button.Tag = -1000 then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end
          else
            begin
              if (Button.Tag = SelfDepID) then //or(SelfDepID=1 and Button.Tag =-1000) then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end;

          Next;
        end;
    end;
 Dep.Active:=true;
 DepCL.Active:=true;
 Sel.parambyname('BD').value:='01.12.2017';
 Sel.parambyname('ED').value:='15.01.2018';
 Sel.Active:=true;
 DepFilial.Active:=true;
 DepFilialCL.IndexFieldNames:= 'id;dep';
 DepFilialCL.Active:=true;
 Client.Active:=true;
 ClientCL.Active:=true;
 dtAction.Active:=true;
 Action.Active:=true;


end;

procedure TFmActionST.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeStart := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

 // acRefresh.Execute;

  //RepaintBandCaption(GridView.Bands[0]);

  //GridView.DataController.SelectAll
end;

procedure TFmActionST.OrdersListBeforeOpen(DataSet: TDataSet);
begin
//  OrdersList.ParamByName('BD').AsDateTime := RangeStart;
//  OrdersList.ParamByName('ED').AsDateTime := RangeEnd;
end;



procedure TFmActionST.SelCalcFields(DataSet: TDataSet);
begin
Selprice.value:= selprice0.AsFloat * selQ0.AsFloat;
end;

procedure TFmActionST.acChangeRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeStart;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);

end;
end.
