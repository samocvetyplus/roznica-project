object fmSellList: TfmSellList
  Left = 265
  Top = 163
  HelpContext = 100233
  Caption = #1055#1088#1086#1076#1072#1078#1080
  ClientHeight = 612
  ClientWidth = 1091
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1091
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (1)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 835
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siView: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7CE07FEF3D00001F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00000000E07F1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DFF7F007C007C007C007C007CFF7FEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 4
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object siClose: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000F75EF75EF75EF75EF75E00000000
        1F7C1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DF75EEF3D0000EF3DF75EEF3DEF3D
        00001F7C1F7C1F7C1F7C0000F75EF75EF75EF75EEF3D0000EF3DF75EF75EF75E
        F75E00001F7C1F7C1F7C0000EF3DEF3DEF3DF75EF75E0000F75EF75EEF3DEF3D
        EF3D00001F7C1F7C1F7C0000F75EF75EF75EF75E000000000000F75EF75EF75E
        F75E00001F7C1F7C1F7C0000EF3DEF3DEF3DEF3D000000000000EF3DEF3DEF3D
        EF3D00001F7C1F7C1F7C0000F75EF75EF75EF75EF75EF75EF75EF75EF75EF75E
        F75E00001F7C1F7C1F7C1F7C0000000000000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C0000F75E00001F7C1F7C1F7C0000F75E0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000F75E00001F7C1F7C1F7C0000F75E0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000F75E00001F7C1F7C1F7C0000F75E0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7CEF3DEF3DF75E000000000000F75EEF3DEF3D
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000F75EF75EF75EF75EF75E00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      ImageIndex = 5
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = siCloseClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = 'spitPrint'
      DropDownMenu = pmPrint
      Hint = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 771
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object spitTermCard: TSpeedItem
      Action = acTermCard
      BtnCaption = #1050#1072#1088#1090#1099
      Caption = #1050#1072#1088#1090#1099', '#1087#1088#1086#1096#1077#1076#1096#1080#1077' '#1087#1086' '#1090#1077#1088#1084#1080#1085#1072#1083#1091
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00B584
        8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
        8400B5848400B5848400B5848400B5848400B5848400FF00FF00EFCEC600FFFF
        FF00BD081800B5081000B5081000AD081000A50810009C0810009C0810009408
        1000940810008C0810008C08100084081000D6D6D600B5848400EFCEC60018A5
        FF0031ADFF0029ADFF0029A5FF0021A5FF0018A5FF0018A5FF00109CFF00109C
        FF00089CFF00089CFF00009CFF000094F7000094F700B5848400EFCEC60031AD
        FF0039ADFF0031ADFF0031ADFF0029A5FF0021A5FF0018A5FF0018A5FF00109C
        FF00109CFF00089CFF00089CFF00009CFF000094F700B5848400EFCEC600FFFF
        FF00FFFFFF00FFFFFF00945A0800FFFFFF0084081000D6CECE0084081000D6CE
        CE0084081000FFFFFF0084081000EFEFEF00EFEFEF00B5848400EFCEC600FFFF
        FF00FFFFFF00FFFFFF0084081000D6CECE00EFE7E700FFFFFF00FFFFFF008408
        1000D6CECE00A57B5A0084081000F7F7F700F7F7EF00B5848400EFCEC600FFFF
        FF00FFFFFF00FFFFFF008408100084081000FFFFFF00D6CECE0084081000FFFF
        FF00FFFFFF00A57B5A0084081000F7F7F700F7F7F700B5848400EFCEC600FFFF
        FF00FFFFFF00A57B5A00A57B5A00FFFFFF00A57B5A00A57B5A00FFFFFF00945A
        0800D6CECE00A57B5A00A59CA500FFFFF700F7F7F700B5848400EFCEC600B508
        1000AD081000A5081000A50810009C0810009C0810009C081000940810009408
        1000940810008C0810008C0810008408100084081000B5848400EFCEC600BD08
        1800B5081000AD081000AD081000A5081000A50810009C0810009C0810009408
        100094081000940810008C0810008C0810008C081000B5848400EFCEC600FFC6
        C600BD081800B5081000AD081000A5081000A50810009C0810009C0810009C08
        10009408100094081000940810008C081000FFC6C600B5848400FF00FF00EFCE
        C600EFCEC600EFCEC600EFCEC600EFCEC600EFCEC600EFCEC600EFCEC600EFCE
        C600EFCEC600EFCEC600EFCEC600EFCEC600EFCEC600FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ImageIndex = 86
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      OnClick = acTermCardExecute
      SectionName = 'Untitled (0)'
    end
    object SpeeItem2: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      Spacing = 1
      Left = 387
      Top = 3
      Visible = True
      WordWrap = True
      OnClick = SpeeItem2Click
      SectionName = 'Untitled (0)'
    end
    object siSert: TSpeedItem
      BtnCaption = #1057#1077#1088#1090#1080#1092'.'
      Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
      Hint = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'
      ImageIndex = 22
      Spacing = 1
      Left = 451
      Top = 3
      Visible = True
      OnClick = acSertExecute
      SectionName = 'Untitled (0)'
    end
    object siItogo: TSpeedItem
      BtnCaption = #1048#1090#1086#1075#1086
      Caption = #1048#1090#1086#1075#1086
      Hint = #1048#1090#1086#1075#1086'|'
      ImageIndex = 63
      Spacing = 1
      Left = 515
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siProfit: TSpeedItem
      Action = acProfit
      BtnCaption = #1055#1088#1080#1073#1099#1083#1100
      Caption = #1042#1072#1083#1086#1074#1072#1103' '#1087#1088#1080#1073#1099#1083#1100
      ImageIndex = 59
      Spacing = 1
      Left = 579
      Top = 3
      Visible = True
      OnClick = acProfitExecute
      SectionName = 'Untitled (0)'
    end
    object siSum: TSpeedItem
      Action = acKassaSum
      BtnCaption = #1050#1072#1089#1089#1072
      Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1082#1072#1089#1089#1077
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00000000008484840000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000848484000000000084848400848484008484
        84000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00000000008484840000000000FF00FF00FF00FF0084848400848484008484
        840084848400848484008484840000000000FF00FF00FF00FF0084848400FF00
        FF0000000000FF00FF00FF00FF00FF00FF00FFFFFF00FFFFFF00848484008484
        84008484840084848400848484008484840084848400FF00FF0084848400FF00
        FF00FF00FF00FF00FF00FFFFFF00FF00FF00FFFFFF00FF00FF00848484008484
        84008484840084848400848484008484840084848400FF00FF0084848400FF00
        FF00FFFFFF00FF00FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF008484
        84008484840084848400848484008484840084848400FF00FF0084848400FF00
        FF00FFFFFF0000000000FF00FF00FF00FF0000000000FF00FF00FF00FF00FF00
        FF008484840084848400848484008484840084848400FF00FF0084848400FF00
        FF00FF00FF00FF00FF0000000000FF00FF00FF00FF0000000000FF00FF00FFFF
        FF00FF00FF0084848400848484008484840084848400FF00FF00FF00FF008484
        8400FF00FF00FF00FF00FF00FF0000000000FF00FF00FFFFFF00848484008484
        840084848400FFFFFF00848484008484840084848400FF00FF00FF00FF00FF00
        FF0084848400FF00FF00FF00FF00FFFFFF008484840084848400FF00FF008484
        840000000000FFFFFF00848484008484840084848400FF00FF00FF00FF00FF00
        FF00FF00FF00848484000000000084848400FF00FF008484840000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF008484840000000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0084848400FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0084848400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFF
        FF0084848400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF008484840084848400FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ImageIndex = 41
      Spacing = 1
      Left = 643
      Top = 3
      Visible = True
      OnClick = acKassaSumExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = ActionRet
      BtnCaption = #1042#1086#1079#1074#1088#1072#1090#1099' '#1080' '#1050#1086#1088#1088#1077#1082#1094#1080#1103
      Caption = 'SpeedItem2'
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00F3F8FA0080B9CE00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0054A5C100A3EEFF00A3EEFF00A3EEFF00489BBA00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00509EBE00A3EEFF00A3EEFF00A3EEFF00A3EEFF00A3EEFF00A3EEFF00A3EE
        FF004FA0BD00F3F8FA00FF00FF00FF00FF00FF00FF0075B2CB00E8F1F500A3EE
        FF00A3EEFF00A3EEFF0098E3F6004C9FBD004C9FBE004D9FBE00A3EEFF00A3EE
        FF00A3EEFF00A3EEFF00489BBA006BB9D400A3EEFF0095DFF100F3F8FA0081CF
        E500489BBA007BD4E8005BB0CB0061B7D10081CFE500A3EEFF00A3EEFF0080CE
        E500A3EEFF00A3EEFF00A3EEFF00A3EEFF00A3EEFF0095DFF100FF00FF00FF00
        FF00A3CDDD004FA2BF004FA3C000D1E5ED00FF00FF0073B4CA009DE9FB00A3EE
        FF00A3EEFF00A3EEFF00A3EEFF00A3EEFF00A3EEFF0095DFF100FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00DCE5D600FF00FF00FF00FF0051A0
        BE00A3EEFF00A3EEFF00A3EEFF00A3EEFF005EAFCA00BAD8E500FF00FF00FF00
        FF00FF00FF00BDE0BA009ABE8C00779E6000BDE0BA009ABE8C00CBDAC200FF00
        FF00FF00FF00A3CDDD00A3CDDD00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00DCE5D600BDE0BA00BDE0BA00AACFA200BDE0BA00BDE0BA00BDE0BA00A2C7
        9700BACCAE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF008CAB7700BDE0BA00769C5E00BDE0BA00BDE0BA00FF00FF0089AE7600BDE0
        BA00AACFA200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF007BA16500BDE0BA00A2C79700BDE0BA00BDE0BA00BDE0BA00BDE0BA00BDE0
        BA009DB88C00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00A2C797008DB17B00AFD3A800EEF3EB00FF00FF0085AA7100BDE0BA00B4D8
        AE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00BDE0BA00769C5E00DCE6D600FF00FF00FF00FF00FF00FF00BDE0BA00CBDA
        C200FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EEF2
        EB00BDE0BA00BDE0BA007DA16600FF00FF00FF00FF00D4E0CC0099BE8C00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009FB9
        8F00769C5E00BDE0BA00BDE0BA0080A56B00769C5E00BDE0BA00BACCAE00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0097B4
        8500AFD2A800ACD0A500ACD0A500ACD0A500ACD0A500AFCFA600FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 707
      Top = 3
      Visible = True
      OnClick = ActionRetExecute
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 42
    Width = 1091
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object laDep: TLabel
      Left = 86
      Top = 6
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 408
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmSell
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 593
    Width = 1091
    Height = 19
    Panels = <>
  end
  object gridSell: TDBGridEh
    Left = 0
    Top = 71
    Width = 1091
    Height = 522
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSellList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorTitle.TitleButton = True
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    PopupMenu = pm1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = gridSellDblClick
    OnGetCellParams = gridSellGetCellParams
    OnKeyDown = gridSellKeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RN'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #8470
        Title.EndEllipsis = True
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
        Title.Caption = #1057#1084#1077#1085#1072
        Title.EndEllipsis = True
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Title.Caption = #1057#1082#1083#1072#1076
        Title.EndEllipsis = True
        Width = 93
      end
      item
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
        Title.Caption = #1053#1072#1095#1072#1083#1086
        Title.EndEllipsis = True
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
        Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'TOTAL'
        Footer.FieldName = 'TOTAL'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = 'Z - '#1086#1090#1095#1077#1090
      end
      item
        EditButtons = <>
        FieldName = 'SERTSELL'
        Footer.FieldName = 'SERTSELL'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
      end
      item
        EditButtons = <>
        FieldName = 'CSERT'
        Footer.FieldName = 'CSERT'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
      end
      item
        EditButtons = <>
        FieldName = 'real_vych'
        Footer.FieldName = 'real_vych'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'real_razn'
        Footer.FieldName = 'real_razn'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'COST0'
        Footer.FieldName = 'COST0'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.FieldName = 'SCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
      end
      item
        EditButtons = <>
        FieldName = 'CNT'
        Footer.FieldName = 'CNT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNT'
        Footers = <
          item
            FieldName = 'ACTDISCOUNT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'RCNT'
        Footer.FieldName = 'RCNT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'RW'
        Footer.FieldName = 'RW'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRET'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRET'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'RCSERT'
        Footer.FieldName = 'RCSERT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'RCOST'
        Footer.FieldName = 'RCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
        Title.EndEllipsis = True
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'RCOST0'
        Footer.FieldName = 'RCOST0'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'RSCOST'
        Footer.FieldName = 'RSCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'OPENEMP'
        Footers = <>
        Title.Caption = #1057#1086#1079#1076#1072#1083
        Width = 94
      end
      item
        EditButtons = <>
        FieldName = 'RETURN_COST'
        Footer.DisplayFormat = '0.00'
        Footer.EndEllipsis = True
        Footer.FieldName = 'RETURN_COST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1073#1099#1083#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
      object DBGridEh1: TDBGridEh
        Left = -933
        Top = -410
        Width = 933
        Height = 410
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsSellList
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorTitle.TitleButton = True
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        PopupMenu = pm1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = gridSellDblClick
        OnGetCellParams = gridSellGetCellParams
        OnKeyDown = gridSellKeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'RN'
            Footer.ValueType = fvtCount
            Footers = <>
            Title.Caption = #8470
            Title.EndEllipsis = True
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'N'
            Footers = <>
            Title.Caption = #1057#1084#1077#1085#1072
            Title.EndEllipsis = True
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'DEP'
            Footers = <>
            Title.Caption = #1057#1082#1083#1072#1076
            Title.EndEllipsis = True
            Width = 93
          end
          item
            EditButtons = <>
            FieldName = 'BD'
            Footers = <>
            Title.Caption = #1053#1072#1095#1072#1083#1086
            Title.EndEllipsis = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'ED'
            Footers = <>
            Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'TOTAL'
            Footer.FieldName = 'TOTAL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = 'Z - '#1086#1090#1095#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'SERTSELL'
            Footer.FieldName = 'SERTSELL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'CSERT'
            Footer.FieldName = 'CSERT'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'real_vych'
            Footer.FieldName = 'real_vych'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'real_razn'
            Footer.FieldName = 'real_razn'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'COST0'
            Footer.FieldName = 'COST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'SCOST'
            Footer.FieldName = 'SCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'CNT'
            Footer.FieldName = 'CNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNT'
            Footers = <
              item
                FieldName = 'ACTDISCOUNT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCNT'
            Footer.FieldName = 'RCNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RW'
            Footer.FieldName = 'RW'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNTRET'
            Footers = <
              item
                FieldName = 'ACTDISCOUNTRET'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCSERT'
            Footer.FieldName = 'RCSERT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'RCOST'
            Footer.FieldName = 'RCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
            Title.EndEllipsis = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'RCOST0'
            Footer.FieldName = 'RCOST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RSCOST'
            Footer.FieldName = 'RSCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'RSTATE'
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'OPENEMP'
            Footers = <>
            Title.Caption = #1057#1086#1079#1076#1072#1083
            Width = 94
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBGridEh2: TDBGridEh
    Left = 0
    Top = 71
    Width = 1091
    Height = 522
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSellList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorTitle.TitleButton = True
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    PopupMenu = pm1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = gridSellDblClick
    OnGetCellParams = gridSellGetCellParams
    OnKeyDown = gridSellKeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RN'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #8470
        Title.EndEllipsis = True
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
        Title.Caption = #1057#1084#1077#1085#1072
        Title.EndEllipsis = True
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Title.Caption = #1057#1082#1083#1072#1076
        Title.EndEllipsis = True
        Width = 93
      end
      item
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
        Title.Caption = #1053#1072#1095#1072#1083#1086
        Title.EndEllipsis = True
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
        Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'TOTAL'
        Footer.FieldName = 'TOTAL'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = 'Z - '#1086#1090#1095#1077#1090
      end
      item
        EditButtons = <>
        FieldName = 'SERTSELL'
        Footer.FieldName = 'SERTSELL'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
      end
      item
        EditButtons = <>
        FieldName = 'CSERT'
        Footer.FieldName = 'CSERT'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
      end
      item
        EditButtons = <>
        FieldName = 'real_vych'
        Footer.FieldName = 'real_vych'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'real_razn'
        Footer.FieldName = 'real_razn'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'COST0'
        Footer.FieldName = 'COST0'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.FieldName = 'SCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
      end
      item
        EditButtons = <>
        FieldName = 'CNT'
        Footer.FieldName = 'CNT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clNavy
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNT'
        Footers = <
          item
            FieldName = 'ACTDISCOUNT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'RCNT'
        Footer.FieldName = 'RCNT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'RW'
        Footer.FieldName = 'RW'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRET'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRET'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'RCSERT'
        Footer.FieldName = 'RCSERT'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'RCOST'
        Footer.FieldName = 'RCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
        Title.EndEllipsis = True
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'RCOST0'
        Footer.FieldName = 'RCOST0'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'RSCOST'
        Footer.FieldName = 'RSCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'OPENEMP'
        Footers = <>
        Title.Caption = #1057#1086#1079#1076#1072#1083
        Width = 94
      end
      item
        EditButtons = <>
        FieldName = 'RETURN_COST'
        Footer.DisplayFormat = '0.00'
        Footer.EndEllipsis = True
        Footer.FieldName = 'RETURN_COST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clGreen
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1080#1073#1099#1083#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
      object DBGridEh3: TDBGridEh
        Left = -933
        Top = -410
        Width = 933
        Height = 410
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsSellList
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorTitle.TitleButton = True
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        PopupMenu = pm1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = gridSellDblClick
        OnGetCellParams = gridSellGetCellParams
        OnKeyDown = gridSellKeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'RN'
            Footer.ValueType = fvtCount
            Footers = <>
            Title.Caption = #8470
            Title.EndEllipsis = True
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'N'
            Footers = <>
            Title.Caption = #1057#1084#1077#1085#1072
            Title.EndEllipsis = True
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'DEP'
            Footers = <>
            Title.Caption = #1057#1082#1083#1072#1076
            Title.EndEllipsis = True
            Width = 93
          end
          item
            EditButtons = <>
            FieldName = 'BD'
            Footers = <>
            Title.Caption = #1053#1072#1095#1072#1083#1086
            Title.EndEllipsis = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'ED'
            Footers = <>
            Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'TOTAL'
            Footer.FieldName = 'TOTAL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = 'Z - '#1086#1090#1095#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'SERTSELL'
            Footer.FieldName = 'SERTSELL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'CSERT'
            Footer.FieldName = 'CSERT'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'real_vych'
            Footer.FieldName = 'real_vych'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'real_razn'
            Footer.FieldName = 'real_razn'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'COST0'
            Footer.FieldName = 'COST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'SCOST'
            Footer.FieldName = 'SCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'CNT'
            Footer.FieldName = 'CNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNT'
            Footers = <
              item
                FieldName = 'ACTDISCOUNT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCNT'
            Footer.FieldName = 'RCNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RW'
            Footer.FieldName = 'RW'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNTRET'
            Footers = <
              item
                FieldName = 'ACTDISCOUNTRET'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCSERT'
            Footer.FieldName = 'RCSERT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'RCOST'
            Footer.FieldName = 'RCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
            Title.EndEllipsis = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'RCOST0'
            Footer.FieldName = 'RCOST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RSCOST'
            Footer.FieldName = 'RSCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'RSTATE'
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'OPENEMP'
            Footers = <>
            Title.Caption = #1057#1086#1079#1076#1072#1083
            Width = 94
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 0
        Height = 0
        Align = alClient
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsSellList
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorTitle.TitleButton = True
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        PopupMenu = pm1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = gridSellDblClick
        OnGetCellParams = gridSellGetCellParams
        OnKeyDown = gridSellKeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'RN'
            Footer.ValueType = fvtCount
            Footers = <>
            Title.Caption = #8470
            Title.EndEllipsis = True
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'N'
            Footers = <>
            Title.Caption = #1057#1084#1077#1085#1072
            Title.EndEllipsis = True
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'DEP'
            Footers = <>
            Title.Caption = #1057#1082#1083#1072#1076
            Title.EndEllipsis = True
            Width = 93
          end
          item
            EditButtons = <>
            FieldName = 'BD'
            Footers = <>
            Title.Caption = #1053#1072#1095#1072#1083#1086
            Title.EndEllipsis = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'ED'
            Footers = <>
            Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'TOTAL'
            Footer.FieldName = 'TOTAL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = 'Z - '#1086#1090#1095#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'SERTSELL'
            Footer.FieldName = 'SERTSELL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'CSERT'
            Footer.FieldName = 'CSERT'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'real_vych'
            Footer.FieldName = 'real_vych'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'real_razn'
            Footer.FieldName = 'real_razn'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'COST0'
            Footer.FieldName = 'COST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'SCOST'
            Footer.FieldName = 'SCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'CNT'
            Footer.FieldName = 'CNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNT'
            Footers = <
              item
                FieldName = 'ACTDISCOUNT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCNT'
            Footer.FieldName = 'RCNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RW'
            Footer.FieldName = 'RW'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNTRET'
            Footers = <
              item
                FieldName = 'ACTDISCOUNTRET'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCSERT'
            Footer.FieldName = 'RCSERT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'RCOST'
            Footer.FieldName = 'RCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
            Title.EndEllipsis = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'RCOST0'
            Footer.FieldName = 'RCOST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RSCOST'
            Footer.FieldName = 'RSCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'RSTATE'
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'OPENEMP'
            Footers = <>
            Title.Caption = #1057#1086#1079#1076#1072#1083
            Width = 94
          end
          item
            EditButtons = <>
            FieldName = 'RETURN_COST'
            Footer.DisplayFormat = '0.00'
            Footer.EndEllipsis = True
            Footer.FieldName = 'RETURN_COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1080#1073#1099#1083#1100
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object DBGridEh5: TDBGridEh
            Left = -933
            Top = -410
            Width = 933
            Height = 410
            AllowedOperations = []
            Color = clBtnFace
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dm.dsSellList
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterRowCount = 1
            IndicatorTitle.TitleButton = True
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
            PopupMenu = pm1
            ReadOnly = True
            RowDetailPanel.Color = clBtnFace
            SumList.Active = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = gridSellDblClick
            OnGetCellParams = gridSellGetCellParams
            OnKeyDown = gridSellKeyDown
            Columns = <
              item
                EditButtons = <>
                FieldName = 'RN'
                Footer.ValueType = fvtCount
                Footers = <>
                Title.Caption = #8470
                Title.EndEllipsis = True
                Width = 44
              end
              item
                EditButtons = <>
                FieldName = 'N'
                Footers = <>
                Title.Caption = #1057#1084#1077#1085#1072
                Title.EndEllipsis = True
                Width = 61
              end
              item
                EditButtons = <>
                FieldName = 'DEP'
                Footers = <>
                Title.Caption = #1057#1082#1083#1072#1076
                Title.EndEllipsis = True
                Width = 93
              end
              item
                EditButtons = <>
                FieldName = 'BD'
                Footers = <>
                Title.Caption = #1053#1072#1095#1072#1083#1086
                Title.EndEllipsis = True
                Width = 75
              end
              item
                EditButtons = <>
                FieldName = 'ED'
                Footers = <>
                Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'TOTAL'
                Footer.FieldName = 'TOTAL'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = 'Z - '#1086#1090#1095#1077#1090
              end
              item
                EditButtons = <>
                FieldName = 'SERTSELL'
                Footer.FieldName = 'SERTSELL'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
              end
              item
                EditButtons = <>
                FieldName = 'CSERT'
                Footer.FieldName = 'CSERT'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
              end
              item
                EditButtons = <>
                FieldName = 'real_vych'
                Footer.FieldName = 'real_vych'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
                Width = 80
              end
              item
                EditButtons = <>
                FieldName = 'real_razn'
                Footer.FieldName = 'real_razn'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
                Width = 89
              end
              item
                EditButtons = <>
                FieldName = 'COST'
                Footer.FieldName = 'COST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'COST0'
                Footer.FieldName = 'COST0'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'SCOST'
                Footer.FieldName = 'SCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
              end
              item
                EditButtons = <>
                FieldName = 'CNT'
                Footer.FieldName = 'CNT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'W'
                Footer.FieldName = 'W'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'ACTDISCOUNT'
                Footers = <
                  item
                    FieldName = 'ACTDISCOUNT'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ValueType = fvtSum
                  end>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
              end
              item
                EditButtons = <>
                FieldName = 'RCNT'
                Footer.FieldName = 'RCNT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'RW'
                Footer.FieldName = 'RW'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'ACTDISCOUNTRET'
                Footers = <
                  item
                    FieldName = 'ACTDISCOUNTRET'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clGreen
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ValueType = fvtSum
                  end>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
              end
              item
                EditButtons = <>
                FieldName = 'RCSERT'
                Footer.FieldName = 'RCSERT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
                Width = 74
              end
              item
                EditButtons = <>
                FieldName = 'RCOST'
                Footer.FieldName = 'RCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
                Title.EndEllipsis = True
                Width = 68
              end
              item
                EditButtons = <>
                FieldName = 'RCOST0'
                Footer.FieldName = 'RCOST0'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'RSCOST'
                Footer.FieldName = 'RSCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
              end
              item
                EditButtons = <>
                FieldName = 'RSTATE'
                Footers = <>
                Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'OPENEMP'
                Footers = <>
                Title.Caption = #1057#1086#1079#1076#1072#1083
                Width = 94
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
      object DBGridEh6: TDBGridEh
        Left = 0
        Top = 0
        Width = 0
        Height = 0
        Align = alClient
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsSellList
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorTitle.TitleButton = True
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        PopupMenu = pm1
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = gridSellDblClick
        OnGetCellParams = gridSellGetCellParams
        OnKeyDown = gridSellKeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'RN'
            Footer.ValueType = fvtCount
            Footers = <>
            Title.Caption = #8470
            Title.EndEllipsis = True
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'N'
            Footers = <>
            Title.Caption = #1057#1084#1077#1085#1072
            Title.EndEllipsis = True
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'DEP'
            Footers = <>
            Title.Caption = #1057#1082#1083#1072#1076
            Title.EndEllipsis = True
            Width = 93
          end
          item
            EditButtons = <>
            FieldName = 'BD'
            Footers = <>
            Title.Caption = #1053#1072#1095#1072#1083#1086
            Title.EndEllipsis = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'ED'
            Footers = <>
            Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'TOTAL'
            Footer.FieldName = 'TOTAL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = 'Z - '#1086#1090#1095#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'SERTSELL'
            Footer.FieldName = 'SERTSELL'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'CSERT'
            Footer.FieldName = 'CSERT'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
          end
          item
            EditButtons = <>
            FieldName = 'real_vych'
            Footer.FieldName = 'real_vych'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'real_razn'
            Footer.FieldName = 'real_razn'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'COST0'
            Footer.FieldName = 'COST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'SCOST'
            Footer.FieldName = 'SCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'CNT'
            Footer.FieldName = 'CNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNT'
            Footers = <
              item
                FieldName = 'ACTDISCOUNT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCNT'
            Footer.FieldName = 'RCNT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RW'
            Footer.FieldName = 'RW'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'ACTDISCOUNTRET'
            Footers = <
              item
                FieldName = 'ACTDISCOUNTRET'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          end
          item
            EditButtons = <>
            FieldName = 'RCSERT'
            Footer.FieldName = 'RCSERT'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'RCOST'
            Footer.FieldName = 'RCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
            Title.EndEllipsis = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'RCOST0'
            Footer.FieldName = 'RCOST0'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'RSCOST'
            Footer.FieldName = 'RSCOST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
          end
          item
            EditButtons = <>
            FieldName = 'RSTATE'
            Footers = <>
            Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
            Title.EndEllipsis = True
          end
          item
            EditButtons = <>
            FieldName = 'OPENEMP'
            Footers = <>
            Title.Caption = #1057#1086#1079#1076#1072#1083
            Width = 94
          end
          item
            EditButtons = <>
            FieldName = 'RETURN_COST'
            Footer.DisplayFormat = '0.00'
            Footer.EndEllipsis = True
            Footer.FieldName = 'RETURN_COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1055#1088#1080#1073#1099#1083#1100
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object DBGridEh7: TDBGridEh
            Left = -933
            Top = -410
            Width = 933
            Height = 410
            AllowedOperations = []
            Color = clBtnFace
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dm.dsSellList
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterRowCount = 1
            IndicatorTitle.TitleButton = True
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
            PopupMenu = pm1
            ReadOnly = True
            RowDetailPanel.Color = clBtnFace
            SumList.Active = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = gridSellDblClick
            OnGetCellParams = gridSellGetCellParams
            OnKeyDown = gridSellKeyDown
            Columns = <
              item
                EditButtons = <>
                FieldName = 'RN'
                Footer.ValueType = fvtCount
                Footers = <>
                Title.Caption = #8470
                Title.EndEllipsis = True
                Width = 44
              end
              item
                EditButtons = <>
                FieldName = 'N'
                Footers = <>
                Title.Caption = #1057#1084#1077#1085#1072
                Title.EndEllipsis = True
                Width = 61
              end
              item
                EditButtons = <>
                FieldName = 'DEP'
                Footers = <>
                Title.Caption = #1057#1082#1083#1072#1076
                Title.EndEllipsis = True
                Width = 93
              end
              item
                EditButtons = <>
                FieldName = 'BD'
                Footers = <>
                Title.Caption = #1053#1072#1095#1072#1083#1086
                Title.EndEllipsis = True
                Width = 75
              end
              item
                EditButtons = <>
                FieldName = 'ED'
                Footers = <>
                Title.Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'TOTAL'
                Footer.FieldName = 'TOTAL'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = 'Z - '#1086#1090#1095#1077#1090
              end
              item
                EditButtons = <>
                FieldName = 'SERTSELL'
                Footer.FieldName = 'SERTSELL'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1087#1088#1086#1076#1072#1085#1086
              end
              item
                EditButtons = <>
                FieldName = 'CSERT'
                Footer.FieldName = 'CSERT'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099'|'#1086#1090#1086#1074#1072#1088#1077#1085#1086
              end
              item
                EditButtons = <>
                FieldName = 'real_vych'
                Footer.FieldName = 'real_vych'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1074#1099#1095#1080#1089#1083#1077#1085#1085#1072#1103
                Width = 80
              end
              item
                EditButtons = <>
                FieldName = 'real_razn'
                Footer.FieldName = 'real_razn'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103' ('#1087#1088#1086#1074#1077#1088#1082#1072')|'#1088#1072#1079#1085#1080#1094#1072
                Width = 89
              end
              item
                EditButtons = <>
                FieldName = 'COST'
                Footer.FieldName = 'COST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'COST0'
                Footer.FieldName = 'COST0'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'SCOST'
                Footer.FieldName = 'SCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
              end
              item
                EditButtons = <>
                FieldName = 'CNT'
                Footer.FieldName = 'CNT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1050#1086#1083'-'#1074#1086
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'W'
                Footer.FieldName = 'W'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clNavy
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1042#1077#1089
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'ACTDISCOUNT'
                Footers = <
                  item
                    FieldName = 'ACTDISCOUNT'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clNavy
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ValueType = fvtSum
                  end>
                Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
              end
              item
                EditButtons = <>
                FieldName = 'RCNT'
                Footer.FieldName = 'RCNT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1050#1086#1083'-'#1074#1086
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'RW'
                Footer.FieldName = 'RW'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1042#1077#1089
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'ACTDISCOUNTRET'
                Footers = <
                  item
                    FieldName = 'ACTDISCOUNTRET'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clGreen
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = []
                    ValueType = fvtSum
                  end>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080
              end
              item
                EditButtons = <>
                FieldName = 'RCSERT'
                Footer.FieldName = 'RCSERT'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
                Width = 74
              end
              item
                EditButtons = <>
                FieldName = 'RCOST'
                Footer.FieldName = 'RCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
                Title.EndEllipsis = True
                Width = 68
              end
              item
                EditButtons = <>
                FieldName = 'RCOST0'
                Footer.FieldName = 'RCOST0'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1088#1072#1089#1093#1086#1076#1085#1072#1103
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'RSCOST'
                Footer.FieldName = 'RSCOST'
                Footer.Font.Charset = DEFAULT_CHARSET
                Footer.Font.Color = clGreen
                Footer.Font.Height = -11
                Footer.Font.Name = 'MS Sans Serif'
                Footer.Font.Style = []
                Footer.ValueType = fvtSum
                Footers = <>
                Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1085#1072#1103
              end
              item
                EditButtons = <>
                FieldName = 'RSTATE'
                Footers = <>
                Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
                Title.EndEllipsis = True
              end
              item
                EditButtons = <>
                FieldName = 'OPENEMP'
                Footers = <>
                Title.Caption = #1057#1086#1079#1076#1072#1083
                Width = 94
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
  end
  object fmsSell: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 14
    Top = 172
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 48
    Top = 144
    object N1: TMenuItem
      Action = acView
    end
    object N2: TMenuItem
      Action = acClose
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
    end
    object N3: TMenuItem
      Action = acAdd
    end
    object N4: TMenuItem
      Action = acDel
    end
  end
  object pmPrint: TPopupMenu
    Left = 16
    Top = 260
    object mnitBill: TMenuItem
      Action = acPrintBill
    end
    object mnitActDisc: TMenuItem
      Action = acPrintActDisc
    end
    object N6: TMenuItem
      Action = acPrintActRet
    end
    object nSell: TMenuItem
      Action = acPrintSellEntry
    end
    object N5: TMenuItem
      Tag = 1
      Action = acPrintSellOutlay
    end
    object NSellArt: TMenuItem
      Tag = 2
      Action = acPrintSellArt
    end
    object NSellNump: TMenuItem
      Tag = 3
      Action = acPrintSellSinv
    end
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 16
    Top = 144
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      ImageIndex = 5
      ShortCut = 119
      OnExecute = acCloseExecute
      OnUpdate = acCloseUpdate
    end
    object acPrintBill: TAction
      Caption = #1057#1095#1077#1090' '#1085#1072' '#1087#1088#1077#1076#1086#1087#1083#1072#1090#1091
      OnExecute = acPrintBillExecute
      OnUpdate = acPrintBillUpdate
    end
    object acPrintActDisc: TAction
      Caption = #1040#1082#1090' '#1089#1082#1080#1076#1082#1080' '#1085#1072' '#1087#1088#1086#1076#1072#1078#1091
      OnExecute = acPrintActDiscExecute
      OnUpdate = acPrintActDiscUpdate
    end
    object acPrintSellEntry: TAction
      Caption = #1057#1084#1077#1085#1072'('#1087#1088#1080#1093'.'#1094#1077#1085#1099')'
      OnExecute = acPrintSellExecute
      OnUpdate = acPrintSellEntryUpdate
    end
    object acPrintSellOutlay: TAction
      Tag = 1
      Caption = #1057#1084#1077#1085#1072'('#1088#1072#1089#1093'.'#1094#1077#1085#1099')'
      OnExecute = acPrintSellExecute
      OnUpdate = acPrintSellOutlayUpdate
    end
    object acPrintSellArt: TAction
      Tag = 2
      Caption = #1057#1084#1077#1085#1072'('#1089#1086#1088#1090'. '#1087#1086' '#1072#1088#1090'.)'
      OnExecute = acPrintSellExecute
      OnUpdate = acPrintSellArtUpdate
    end
    object acPrintSellSinv: TAction
      Tag = 3
      Caption = #1057#1084#1077#1085#1072'('#1085#1086#1084#1077#1088' '#1087#1088#1080#1093'.) '
      OnExecute = acPrintSellExecute
      OnUpdate = acPrintSellSinvUpdate
    end
    object acPrintGrid: TAction
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
    object acPrintActRet: TAction
      Caption = #1040#1082#1090' '#1089#1082#1080#1076#1082#1080' '#1085#1072' '#1074#1086#1079#1074#1088#1072#1090
      OnExecute = acPrintActRetExecute
      OnUpdate = acPrintActRetUpdate
    end
    object acTermCard: TAction
      Caption = #1050#1072#1088#1090#1099
      ImageIndex = 86
      OnExecute = acTermCardExecute
    end
    object acOpen: TAction
      Caption = 'acOpen'
      OnExecute = acOpenExecute
    end
    object acSert: TAction
      Caption = 'acSert'
      OnExecute = acSertExecute
    end
    object acProfit: TAction
      Caption = 'acProfit'
      ImageIndex = 59
      OnExecute = acProfitExecute
    end
    object acKassaSum: TAction
      Caption = #1050#1072#1089#1089#1072
      ImageIndex = 41
      OnExecute = acKassaSumExecute
    end
    object ActionRet: TAction
      Caption = 'ActionRet'
      ImageIndex = 89
      OnExecute = ActionRetExecute
    end
  end
  object prgSell: TPrintDBGridEh
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 16
    Top = 288
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 744
    Top = 40
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   8'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 8'
      'order by HDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 712
    Top = 40
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object XLSExportFile1: TXLSExportFile
    Left = 16
    Top = 200
  end
  object dsrProfit: TDataSource
    DataSet = taProfit
    Left = 736
    Top = 200
  end
  object taProfit: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select dep, city, sum(cost) cost, sum(scost) scost, sum(rcost-rs' +
        'cost) rcost, (sum(cost)-sum(scost)-sum(rcost)+sum(rscost)) itog'
      
        'from sell_s(?DEPID1,?DEPID2,?BD,?ED) group by dep, city order by' +
        ' city, dep')
    BeforeOpen = taProfitBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 736
    Top = 160
    object taProfitDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taProfitCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taProfitSCOST: TFIBFloatField
      FieldName = 'SCOST'
    end
    object taProfitRCOST: TFIBFloatField
      FieldName = 'RCOST'
    end
    object taProfitITOG: TFIBFloatField
      FieldName = 'ITOG'
    end
    object taProfitCITY: TFIBStringField
      FieldName = 'CITY'
      EmptyStrToNull = True
    end
  end
  object SD1: TSaveDialog
    DefaultExt = '.xls'
    Filter = 'Excel|*.xls'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 16
    Top = 232
  end
  object dsrProfit_Itog: TDataSource
    DataSet = taProfit_Itog
    Left = 776
    Top = 200
  end
  object taProfit_Itog: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select sum(cost) cost, sum(scost) scost, sum(rcost-rscost) rcost' +
        ', (sum(cost)-sum(scost)-sum(rcost)+sum(rscost)) itog'
      'from sell_s(?DEPID1,?DEPID2,?BD,?ED) ')
    BeforeOpen = taProfitBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 776
    Top = 160
    object taProfit_ItogCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taProfit_ItogSCOST: TFIBFloatField
      FieldName = 'SCOST'
    end
    object taProfit_ItogRCOST: TFIBFloatField
      FieldName = 'RCOST'
    end
    object taProfit_ItogITOG: TFIBFloatField
      FieldName = 'ITOG'
    end
  end
  object taProfit_city: TpFIBDataSet
    SelectSQL.Strings = (
      'select city, sum(cost) cost'
      'from sell_s(?DEPID1,?DEPID2,?BD,?ED) group by city order by city')
    BeforeOpen = taProfitBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 816
    Top = 160
    object taProfit_cityCITY: TFIBStringField
      FieldName = 'CITY'
      EmptyStrToNull = True
    end
    object taProfit_cityCOST: TFIBFloatField
      FieldName = 'COST'
    end
  end
  object dsProfit_city: TDataSource
    DataSet = taProfit_city
    Left = 816
    Top = 200
  end
end
