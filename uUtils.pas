unit uUtils;

interface

uses Forms, SysUtils, ShlObj, Windows;

type

  TScanCodeClass = (scUnknown, scDiscountCard, scArticle, scSertificate, scCoupon); // (�����������, ����.�����, �������)

  TCharSet = set of Char;

  TNotifyDialog = class(TForm)
  end;


function GetVersion(FileName:string): string; 
function GetIniFileName: string;
function ParseScanCode(ScanCode: string; var CodeClass: TScanCodeClass; var Code: string): Boolean;
function RemoveNotChars(Value:string;Chars:TCharSet):string;
function GetSpecialFolderLocation(nFolder : integer) : string;

implementation

function GetSpecialFolderLocation(nFolder : integer) : string;
var
  aPidl: PItemIDList;
  fLinkDir : string;
begin
  if SUCCEEDED(SHGetSpecialFolderLocation(0, nFolder, aPidl)) then
  begin
    SetLength(fLinkDir, MAX_PATH);
    SHGetPathFromIDList(aPidl, PChar(fLinkDir));
    SetLength(fLinkDir, StrLen(PChar(fLinkDir)));
    result := fLinkDir;
  end;
end;

function GetIniFileName: string;
var
  i: Integer;
  FilePath: string;
  ApplicationName: string;
begin
  ApplicationName := ExtractFileName(Application.ExeName);
  SetLength(ApplicationName, Length(ApplicationName) - 4);
  FilePath := GetSpecialFolderLocation(CSIDL_APPDATA);
  FilePath := FilePath + '\' + ApplicationName;
  if not DirectoryExists(FilePath) then CreateDir(FilePath);
  Result := FilePath + '\' + ApplicationName + '.ini';
end;

//----------- ������ ����. ���� ----------------//
function ParseScanCode(ScanCode: string; var CodeClass: TScanCodeClass; var Code: string): Boolean;
var
  i: Integer;
  Symbol: Char;
  ByteSymbol: Byte;
  AScanCode: string;
begin

  Code := '';   // ��������� ���, ������� ����������� � ���������
 // CodeClass := scUnknown; // ����� ���� (�.�. �������, ����.����� ��� �����������). �� ��������� - �� ���������
  // ������ ���������������� ���� (��� ������ �������� ������ �� ��������� �� ������ ��������� ��������)
  AScanCode := RemoveNotChars(ScanCode, ['a'..'z','A'..'Z','0'..'9','-']);

  if AScanCode = ScanCode then // ���� ��� ������������ �����
  begin
    i := 1;
    Code := '';
    // ���� � ������ ���� ������� �������, �� �� ���������� �� � ���
    while (i <= Length(ScanCode)) and (ScanCode[i] = '0') do Inc(i);

    while (i <= Length(ScanCode)) do // ����� ���������� � ���
    begin
      Code := Code + ScanCode[i];
      Inc(i);
    end;

    ScanCode := Code; // ����������� ��������� ���

    Code := '';

    if ScanCode <> '' then  // ���� ��������� ��� ����� ���� ���� ������
    begin
      Symbol := ScanCode[1]; // ���������� ������ ������ ����
      if Symbol in ['1'..'9'] then // ���� ������ �� "-", ��
      begin
        ByteSymbol := Ord(Symbol) - Ord('0');
        if ByteSymbol <= Byte(Ord(High(TScanCodeClass))) then
        begin
          CodeClass := TScanCodeClass(ByteSymbol);
          i := 2;
          while (i <= Length(ScanCode)) and (ScanCode[i] = '0') do Inc(i);

          while (i <= Length(ScanCode)) do
          begin
            Code := Code + ScanCode[i];
            Inc(i);
          end;

          if Code = '' then CodeClass := scUnknown;
        end;
      end;
    end;
  end;
  Result := CodeClass <> scUnknown; // ���� ����� ���� - ��� ��.����� �������, ��� ����.�����, �� true
end;

// ������ ���� ����������� �������� ��������������� ������
function RemoveNotChars(Value:string;Chars:TCharSet):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
  if Value[i] in Chars then Result := Result + Value[i];
end;

function GetVersion(FileName:string): string; 
var 
  VerInfoSize: DWORD; 
  VerInfo: Pointer; 
  VerValueSize: DWORD; 
  VerValue: PVSFixedFileInfo; 
  Dummy: DWORD; 
begin 
  VerInfoSize := GetFileVersionInfoSize(PChar(FileName), Dummy); 
  GetMem(VerInfo, VerInfoSize); 
  GetFileVersionInfo(PChar(FileName), 0, VerInfoSize, VerInfo); 
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize); 
  with VerValue^ do 
  begin 
    Result := IntToStr(dwFileVersionMS shr 16); 
    Result := Result + '.' + IntToStr(dwFileVersionMS and $FFFF); 
    Result := Result + '.' + IntToStr(dwFileVersionLS shr 16); 
    Result := Result + '.' + IntToStr(dwFileVersionLS and $FFFF); 
  end; 
  FreeMem(VerInfo, VerInfoSize); 
end;
end.
