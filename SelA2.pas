unit SelA2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid;

type
  TfmSelA2 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dg1: TM207IBGrid;
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure dg1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelA2: TfmSelA2;

implementation

uses comdata, Data, Data2;

{$R *.DFM}

procedure TfmSelA2.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_ESCAPE: ModalResult:=mrCancel;
      VK_RETURN: ModalResult:=mrOK;
    end;  
end;

procedure TfmSelA2.FormActivate(Sender: TObject);
begin
  ActiveControl:=dg1; 
end;

procedure TfmSelA2.dg1DblClick(Sender: TObject);
begin
  ModalResult:=mrOK;
end;

end.
