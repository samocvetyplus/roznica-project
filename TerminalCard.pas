unit TerminalCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, PrnDbgeh,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, ActnList,
  cxDropDownEdit, M207Ctrls, TB2Item, Menus, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, rxPlacemnt, cxLookAndFeels,
  cxLookAndFeelPainters;

type
  TfmTermCards = class(TForm)
    cxGridCardDBTableView1: TcxGridDBTableView;
    cxGridCardLevel1: TcxGridLevel;
    cxGridCard: TcxGrid;
    acList: TActionList;
    cxGridCardDBTableView1CHECKNO: TcxGridDBColumn;
    cxGridCardDBTableView1RN: TcxGridDBColumn;
    cxGridCardDBTableView1SNAME: TcxGridDBColumn;
    cxGridCardDBTableView1COST: TcxGridDBColumn;
    cxGridCardDBTableView1COST0: TcxGridDBColumn;
    cxGridCardDBTableView1COSTCARD: TcxGridDBColumn;
    fr1: TM207FormStorage;
    acPrintGrid: TAction;
    pmgrid: TTBPopupMenu;
    biPrintGrid: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintExecute(Sender: TObject);
    procedure cxGridCardDBTableView1CHECKNOPropertiesCloseUp(
      Sender: TObject);
    procedure cxGridCardDBTableView1CHECKNOPropertiesPopup(
      Sender: TObject);
    procedure cxGridCardDBTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure acPrintGridUpdate(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTermCards: TfmTermCards;

implementation
Uses Data, dbUtil, TermCardUid, reportdata;
{$R *.dfm}
procedure TfmTermCards.FormCreate(Sender: TObject);
begin
  OpenDataSet(dm.taCardList);
  fmTermCardUid := TfmTermCardUid.Create(nil);  
end;

procedure TfmTermCards.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CloseDataSet(dm.taCardList);
  fmTermCardUid.Free;
end;

procedure TfmTermCards.acPrintExecute(Sender: TObject);
begin
// dxpr.pri
end;

procedure TfmTermCards.cxGridCardDBTableView1CHECKNOPropertiesCloseUp(
  Sender: TObject);
begin
  CloseDataSet(fmTermCardUid.quTermUid);
end;

procedure TfmTermCards.cxGridCardDBTableView1CHECKNOPropertiesPopup(
  Sender: TObject);
begin
 OpenDataSet(fmTermCardUid.quTermUid);
end;

procedure TfmTermCards.cxGridCardDBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
 if AViewInfo.Item.Name='cxGridCardDBTableView1CHECKNO' then ACanvas.SetBrushColor(clMoneyGreen);
 if AViewInfo.Item.Name='cxGridCardDBTableView1COSTCARD' then ACanvas.SetBrushColor(clRed);
end;

procedure TfmTermCards.acPrintGridUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taCardListSELLID.IsNull)
end;

procedure TfmTermCards.acPrintGridExecute(Sender: TObject);
begin
 dmReport.PrintDocumentB(termcard_check);
end;

end.
