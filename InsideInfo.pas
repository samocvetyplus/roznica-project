unit InsideInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PrnDbgeh, StdCtrls, RXSpin, ExtCtrls, Grids, DBGridEh,
  ActnList, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping, rxSpeedbar,
  GridsEh;

type
  TfInsideInfo = class(TForm)
    DBGridEh1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siOpen: TSpeedItem;
    PrintDBGridEh1: TPrintDBGridEh;
    ActionList1: TActionList;
    acPrn: TAction;
    dsrTaSQL: TDataSource;
    taSQL: TpFIBDataSet;
    acFlOpen: TAction;
    opnFile: TOpenDialog;
    acClose: TAction;
    siSaveTofile: TSpeedItem;
    acSavetoFile: TAction;
    svfile: TSaveDialog;
    procedure acPrnExecute(Sender: TObject);
    procedure acFlOpenExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSavetoFileUpdate(Sender: TObject);
    procedure acSavetoFileExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fInsideInfo: TfInsideInfo;

implementation

{$R *.dfm}
Uses dbUtil, ComData;
procedure TfInsideInfo.acPrnExecute(Sender: TObject);
begin
  PrintDBGridEh1.Print;
end;

procedure TfInsideInfo.acFlOpenExecute(Sender: TObject);
var  ftext:textfile;
     tmpstr, Path :string;
begin
  if not opnFile.Execute then exit;

  CloseDataSets([taSQL]);
  taSQL.SelectSQL.Clear;
  Path:=opnFile.FileName;
  AssignFile(ftext, Path);
  Reset(ftext);
  try
   while not EOF(ftext) do
   begin
     readln(ftext, tmpstr);
     taSQL.SelectSQL.Add(tmpstr);
   end;
  finally
   CloseFile(ftext);
  end;
  taSQL.Prepare;
  OpenDataSets([taSQL]);
end;

procedure TfInsideInfo.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfInsideInfo.acSavetoFileUpdate(Sender: TObject);
begin
 if taSQL.Active then acSavetoFile.Enabled:=true
 else acSavetoFile.Enabled:=false
end;

procedure TfInsideInfo.acSavetoFileExecute(Sender: TObject);
var  ftext:textfile;
     tmpstr, Path, tmptype :string;
     i:integer;
begin
  if not svFile.Execute then exit;

  Path:=svFile.FileName;

  AssignFile(ftext, Path);
  Rewrite(ftext);
  try
   tmpstr:='';
   for i:=0  to taSQL.FieldCount-1 do
    tmpstr:=tmpstr+' {} '+trim(taSQL.Fields[i].FieldName);
   writeln(ftext,tmpstr);

   while not taSQL.Eof do
   begin
    tmpstr:='';
    for i:=0  to taSQL.FieldCount-1 do
    begin
     if taSQL.Fields[i].IsNull then tmptype:='NULL'
     else tmptype:=Trim(taSQL.Fields[i].AsString);
     tmpstr:=tmpstr+' {} '+tmptype;
    end;
    writeln(ftext,tmpstr);
    taSQL.Next;
   end;
  finally
   CloseFile(ftext);
  end;
end;

procedure TfInsideInfo.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
end;

end.
