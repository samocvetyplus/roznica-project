object fmNodCard: TfmNodCard
  Left = 326
  Top = 283
  BorderStyle = bsDialog
  Caption = #1053#1086#1084#1077#1088' '#1076#1080#1089#1082#1086#1085#1090#1085#1086#1081' '#1082#1072#1088#1090#1099
  ClientHeight = 90
  ClientWidth = 532
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Lbirthday: TLabel
    Left = 128
    Top = 32
    Width = 5
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edNodCard: TEdit
    Left = 0
    Top = 0
    Width = 177
    Height = 32
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnKeyDown = edNodCardKeyDown
    OnKeyPress = edNodCardKeyPress
    OnKeyUp = edNodCardKeyUp
  end
  object edClient: TEdit
    Left = 176
    Top = 0
    Width = 353
    Height = 32
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnKeyDown = edClientKeyDown
    OnKeyPress = edClientKeyPress
    OnKeyUp = edClientKeyUp
  end
  object btnok: TButton
    Left = 120
    Top = 64
    Width = 75
    Height = 25
    BiDiMode = bdLeftToRight
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    ModalResult = 1
    ParentBiDiMode = False
    TabOrder = 2
    OnKeyDown = btnokKeyDown
  end
  object btnno: TButton
    Left = 232
    Top = 64
    Width = 75
    Height = 25
    BiDiMode = bdLeftToRight
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    ParentBiDiMode = False
    TabOrder = 3
    OnKeyDown = btnnoKeyDown
  end
  object btVib: TButton
    Left = 336
    Top = 64
    Width = 105
    Height = 25
    Action = acVibcard
    TabOrder = 4
  end
  object acList: TActionList
    Left = 464
    Top = 40
    object acVibcard: TAction
      Caption = #1047#1072#1084#1077#1085#1080#1090#1100' '#1082#1072#1088#1090#1091' [F4]'
      ShortCut = 115
      OnExecute = acVibcardExecute
      OnUpdate = acVibcardUpdate
    end
  end
end
