object fmA2prHist: TfmA2prHist
  Left = 204
  Top = 130
  Width = 696
  Height = 480
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1072#1088#1090#1080#1082#1091#1083#1072' 2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 432
    Width = 688
    Height = 19
    Panels = <>
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 432
    Align = alClient
    Color = clBtnFace
    DataSource = dsA2PrHst
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = FormStorage1
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Expanded = False
        FieldName = 'PROD'
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_MATID'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 43
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_GOODID'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_INSID'
        Title.Alignment = taCenter
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UNITID'
        Title.Alignment = taCenter
        Title.Caption = #1045#1076'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 46
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 88
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FULLART'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 155
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUP'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SN'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TSPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1042#1088#1077#1084#1077#1085#1085#1072#1103' '#1087#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPTPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1054#1087#1090'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOPTPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1042#1088#1077#1084#1077#1085#1085#1072#1103' '#1086#1087#1090'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RECDATE'
        Title.Alignment = taCenter
        Title.Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2HSTID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2ID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_ARTID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUPID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SPEQ'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPEQ'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INVID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPR'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_COMPID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRORDITEMID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRORDITEMID2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ITYPE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 56
    Top = 76
  end
  object quA2PrHst: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT   ART2HSTID,  ART2ID, D_ARTID, ART2, SUPID,'
      '                 PRICE, OPTPRICE, TSPRICE, TOPTPRICE,'
      '                 SPEQ, OPEQ, INVID,  OPR, D_COMPID,'
      '                 D_MATID, D_GOODID, D_INSID, UNITID,'
      '                 ART, FULLART, PRORDITEMID, PRORDITEMID2,'
      '                 RECDATE, SUP, PROD, SN, ITYPE'
      'FROM  ART2HST_S(?ART2ID)'
      'ORDER BY RECDATE DESC'
      '')
    CacheModelOptions.BufferChunks = 1000
    AutoCalcFields = False
    BeforeOpen = quA2PrHstBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 76
    Top = 152
    object quA2PrHstPROD: TFIBStringField
      FieldName = 'PROD'
      EmptyStrToNull = True
    end
    object quA2PrHstD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quA2PrHstD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quA2PrHstD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quA2PrHstUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = quA2PrHstUNITIDGetText
    end
    object quA2PrHstART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quA2PrHstART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quA2PrHstFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quA2PrHstSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quA2PrHstSN: TIntegerField
      FieldName = 'SN'
    end
    object quA2PrHstPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quA2PrHstTSPRICE: TFloatField
      FieldName = 'TSPRICE'
      currency = True
    end
    object quA2PrHstOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object quA2PrHstTOPTPRICE: TFloatField
      FieldName = 'TOPTPRICE'
      currency = True
    end
    object quA2PrHstART2HSTID: TIntegerField
      FieldName = 'ART2HSTID'
    end
    object quA2PrHstART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quA2PrHstD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object quA2PrHstSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quA2PrHstSPEQ: TIntegerField
      FieldName = 'SPEQ'
    end
    object quA2PrHstOPEQ: TIntegerField
      FieldName = 'OPEQ'
    end
    object quA2PrHstINVID: TIntegerField
      FieldName = 'INVID'
    end
    object quA2PrHstOPR: TSmallintField
      FieldName = 'OPR'
    end
    object quA2PrHstD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quA2PrHstPRORDITEMID: TIntegerField
      FieldName = 'PRORDITEMID'
    end
    object quA2PrHstPRORDITEMID2: TIntegerField
      FieldName = 'PRORDITEMID2'
    end
    object quA2PrHstRECDATE: TDateTimeField
      FieldName = 'RECDATE'
    end
    object quA2PrHstITYPE: TIntegerField
      FieldName = 'ITYPE'
    end
  end
  object dsA2PrHst: TDataSource
    DataSet = quA2PrHst
    Left = 76
    Top = 204
  end
end
