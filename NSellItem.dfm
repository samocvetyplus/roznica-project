object fmNSellItem: TfmNSellItem
  Left = 374
  Top = 386
  Width = 453
  Height = 418
  Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 445
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 7
      Top = 11
      Width = 48
      Height = 16
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siclose: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'siclose'
      Hint = 'siclose|'
      ImageIndex = 0
      Spacing = 1
      Left = 378
      Top = 2
      Visible = True
      OnClick = sicloseClick
      SectionName = 'Untitled (0)'
    end
  end
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 41
    Width = 445
    Height = 348
    Align = alClient
    Color = clBtnFace
    DataGrouping.GroupLevels = <>
    DataSource = DataSource1
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 2
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = DBGridEh1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = #1060#1080#1083#1080#1072#1083
        Footers = <>
        Width = 119
      end
      item
        EditButtons = <>
        FieldName = #1048#1076'.'#8470
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = #1053#1086#1084#1077#1088' '#1089#1084#1077#1085#1099
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080'/'#1074#1086#1079#1074#1088#1072#1090#1072
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = #1057#1090#1086#1080#1084#1086#1089#1090#1100
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pdg1: TPrintDBGridEh
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    PrintFontName = 'Arial'
    Units = MM
    Left = 144
    Top = 168
  end
  object aclist: TActionList
    Left = 304
    Top = 128
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
  end
  object pFIBDataSet1: TpFIBDataSet
    SelectSQL.Strings = (
      'select  c.clientid, c.nodcard, d.sname Dep_Name, si.uid, s.rn,  '
      
        '        fifd(si.ret, si.retdate, si.adate) ADate, (si.Q0*si.Pric' +
        'e) Cost      '
      'from    client c, sellitem si, sell s, d_dep d'
      'where   c.clientid = si.clientid and'
      '        c.nodcard = :Card and'
      '        s.sellid = si.sellid and'
      '        d.d_depid=s.depid')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 120
    Top = 256
    object pFIBDataSet1DEP_NAME: TFIBStringField
      FieldName = #1060#1080#1083#1080#1072#1083
      EmptyStrToNull = True
    end
    object pFIBDataSet1UID: TFIBIntegerField
      FieldName = #1048#1076'.'#8470
    end
    object pFIBDataSet1RN: TFIBIntegerField
      FieldName = #1053#1086#1084#1077#1088' '#1089#1084#1077#1085#1099
    end
    object pFIBDataSet1DATE_SELL_RET: TFIBDateTimeField
      FieldName = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080'/'#1074#1086#1079#1074#1088#1072#1090#1072
    end
    object pFIBDataSet1COST: TFIBFloatField
      FieldName = #1057#1090#1086#1080#1084#1086#1089#1090#1100
    end
  end
  object DataSource1: TDataSource
    DataSet = pFIBDataSet1
    Left = 160
    Top = 256
  end
end
