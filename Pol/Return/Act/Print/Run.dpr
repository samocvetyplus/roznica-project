program Run;

uses
  ExceptionLog,
  pFIBDatabase,
  Forms,
  Return_Act_Print in 'Return_Act_Print.pas' {dm_Return_Act_Print: TDataModule};

{$R *.res}
var
  dm_Return_Act_Print: Tdm_Return_Act_Print;
  db: TpFIBDatabase;
  ta: TpFIBTransaction;
begin
  dm_Return_Act_Print := Tdm_Return_Act_Print.Create(Application);
  db := TpFIBDatabase.Create(Application);
  ta := TpFIBTransaction.Create(Application);

  with ta do
  begin
    TRParams.Add('read_committed');
    TRParams.Add('rec_version');
    TRParams.Add('nowait');
    DefaultDataBase := db;
  end;

  with db do
  begin
    DefaultTransAction := ta;
    DBName := '192.168.211.111:db/work/jew9.gdb';
    DBParams.Add('user_name=JEWELLERY');
    DBParams.Add('password=m207');
    DBParams.Add('lc_ctype=WIN1251');
    Open;
  end;

  Application.Initialize;

  with dm_Return_Act_Print do
  begin
    DataBase := db;
    SInvID := 15870;
    Report;
  end;

  Application.Run;
end.
