object dm_Return_Act_Print: Tdm_Return_Act_Print
  OldCreateOrder = False
  Height = 306
  Width = 335
  object DataSet_Customer: TpFIBDataSet
    SelectSQL.Strings = (
      'select Name,  Address, Phone from PRINT$SINV(:SInvID);')
    BeforeOpen = DataSet_CustomerBeforeOpen
    Left = 32
    Top = 104
  end
  object DataSet_Self: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      
        '  stretrim(c.Name) || '#39' '#39' || stretrim(c.PostIndex) || '#39' '#39' || str' +
        'etrim(c.City) || '#39' '#39' || stretrim(c.Address) Address,'
      '  c.INN,'
      '  c.KPP'
      'from d_rec r'
      '  left outer join d_comp c on (c.D_CompID = r.D_CompID)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 56
    object DataSet_SelfADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 1027
      EmptyStrToNull = True
    end
    object DataSet_SelfINN: TFIBStringField
      FieldName = 'INN'
      Size = 30
      EmptyStrToNull = True
    end
    object DataSet_SelfKPP: TFIBStringField
      FieldName = 'KPP'
      EmptyStrToNull = True
    end
  end
  object DataSet_Act: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  Number SN,'
      '  A$Date SDATE,'
      '  Company COMP,'
      '  Sender GRUZOOTPRAV,'
      '  Producer PROIZVODITEL,'
      '  Contract$string CONTRACT'
      'from'
      '  Report$Return$Act(:SInvID)')
    BeforeOpen = DataSet_CustomerBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 152
    object DataSet_ActSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object DataSet_ActSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object DataSet_ActCOMP: TFIBStringField
      FieldName = 'COMP'
      Size = 256
      EmptyStrToNull = True
    end
    object DataSet_ActGRUZOOTPRAV: TFIBStringField
      FieldName = 'GRUZOOTPRAV'
      Size = 256
      EmptyStrToNull = True
    end
    object DataSet_ActPROIZVODITEL: TFIBStringField
      FieldName = 'PROIZVODITEL'
      Size = 256
      EmptyStrToNull = True
    end
    object DataSet_ActCONTRACT: TFIBStringField
      FieldName = 'CONTRACT'
      Size = 128
      EmptyStrToNull = True
    end
  end
  object DataSet_ActItems: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  a2.Art Articul,'
      '  a2.Art2 Articul2,'
      '  g.Name Goods_Name,'
      '  m.Name Proba_Name,'
      '  r.W Weight,'
      '  r.SPrice Price,'
      '  r.Cost,'
      '  r.Sz StoneSize,'
      '  i.SSF,'
      '  i.NDate'
      'from SRet r'
      '  left outer join Art2 a2 on (a2.Art2ID = r.Art2ID)'
      '  left outer join D_Good g on (g.D_GoodID = a2.D_GoodID)'
      '  left outer join D_Mat m on (m.D_MatID = a2.D_MatID)'
      '  left outer join SItem it on (it.UID = r.UID)'
      '  left outer join Sel s on (s.SelID = it.SelID)'
      '  left outer join SInv i on (i.SInvID = s.SInvID)'
      'where'
      '  r.SInvID = :SInvID and'
      '  i.IType = 1'
      'order by '
      '  i.SSF, '
      '  a2.Art,'
      '  a2.Art2')
    BeforeOpen = DataSet_CustomerBeforeOpen
    Left = 32
    Top = 208
  end
  object DataSet_Doc: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Doc set'
      '  ID=:ID,'
      '  DOC=:DOC'
      'where ID=:OLD_ID')
    SelectSQL.Strings = (
      'select '
      '  ID,'
      '  Doc'
      'from Doc'
      'where'
      '  ID = :DocID')
    BeforeOpen = DataSet_DocBeforeOpen
    Left = 32
    Top = 8
  end
  object fr_Report: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    OnGetValue = fr_ReportGetValue
    Left = 264
    Top = 56
    ReportForm = {19000000}
  end
  object frdb_Self: TfrDBDataSet
    DataSource = dsrSelf
    Left = 152
    Top = 56
  end
  object frdb_Customer: TfrDBDataSet
    DataSet = DataSet_Customer
    Left = 120
    Top = 104
  end
  object frdb_Act: TfrDBDataSet
    DataSet = DataSet_Act
    Left = 120
    Top = 152
  end
  object frdb_ActItems: TfrDBDataSet
    DataSet = DataSet_ActItems
    Left = 120
    Top = 208
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 264
  end
  object dsrSelf: TDataSource
    DataSet = DataSet_Self
    Left = 96
    Top = 56
  end
end
