unit Return_Act_Print;

interface

uses
  SysUtils, Classes, FIBDatabase, pFIBDatabase, DB, FIBDataSet, pFIBDataSet,
  DBTables, FR_DSet, FR_DBSet, FR_Desgn, FR_Class, Forms, Controls,
  frXMLExl;

type
  Tdm_Return_Act_Print = class(TDataModule)
    DataSet_Customer: TpFIBDataSet;
    DataSet_Self: TpFIBDataSet;
    DataSet_Act: TpFIBDataSet;
    DataSet_ActItems: TpFIBDataSet;
    DataSet_Doc: TpFIBDataSet;
    fr_Report: TfrReport;
    frdb_Self: TfrDBDataSet;
    frdb_Customer: TfrDBDataSet;
    frdb_Act: TfrDBDataSet;
    frdb_ActItems: TfrDBDataSet;
    frXMLExcelExport1: TfrXMLExcelExport;
    DataSet_SelfADDRESS: TFIBStringField;
    DataSet_SelfINN: TFIBStringField;
    DataSet_SelfKPP: TFIBStringField;
    DataSet_ActSN: TFIBIntegerField;
    DataSet_ActSDATE: TFIBDateTimeField;
    DataSet_ActCOMP: TFIBStringField;
    DataSet_ActGRUZOOTPRAV: TFIBStringField;
    DataSet_ActPROIZVODITEL: TFIBStringField;
    dsrSelf: TDataSource;
    DataSet_ActCONTRACT: TFIBStringField;
    procedure DataSet_CustomerBeforeOpen(DataSet: TDataSet);
    procedure DataSet_DocBeforeOpen(DataSet: TDataSet);
    procedure fr_ReportGetValue(const ParName: String;
      var ParValue: Variant);
  private
    db: TpFIBDatabase;
    Executed: Boolean;
    PSInvID: Integer;
    PPreview: Boolean;

    procedure AllRefresh;
    procedure Execute;
  public
    property DataBase: TpFIBDatabase write db;
    property SInvID: Integer write PSInvID;
    property Preview: Boolean write PPreview;

    procedure Report;
  end;

const
  DocID = 76; // идентификатор документа


implementation

uses ReportData;

{$R *.dfm}

{ Tdm_Return_Act_Print }

procedure Tdm_Return_Act_Print.DataSet_CustomerBeforeOpen(
  DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('SInvID').AsInteger := PSInvID;
end;

procedure Tdm_Return_Act_Print.Report;
begin
  try
    Screen.Cursor := crHourGlass;

    if not Executed then
      Execute
    else
      AllRefresh;

    if PPreview then
      fr_Report.DesignReport
    else
      fr_Report.ShowReport;
      
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure Tdm_Return_Act_Print.Execute;
begin
  DataSet_Doc.DataBase := db;
  DataSet_Self.DataBase := db;
  DataSet_Customer.DataBase := db;
  DataSet_Act.DataBase := db;
  DataSet_ActItems.DataBase := db;

  AllRefresh;
  Executed := True;
end;

procedure Tdm_Return_Act_Print.AllRefresh;
begin
  DataSet_Doc.Close;
  DataSet_Self.Close;
  DataSet_Customer.Close;
  DataSet_Act.Close;
  DataSet_ActItems.Close;

  DataSet_Doc.Open;
  DataSet_Self.Open;
  DataSet_Customer.Open;
  DataSet_Act.Open;
  DataSet_ActItems.Open;

  fr_Report.LoadFromBlobField(DataSet_Doc.FieldByName('Doc'));
  dmReport.FDocId := DocID;
end;




procedure Tdm_Return_Act_Print.DataSet_DocBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('DocID').AsInteger := DocID;
end;


procedure Tdm_Return_Act_Print.fr_ReportGetValue(const ParName: String;
  var ParValue: Variant);
var
  s: string;
begin
  if (ParName = 'проба') then
  begin
    s := DataSet_ActItems.FieldByName('Proba_Name').AsString;
    Delete(s, 1, Pos(' ', s));
    ParValue := s;
  end;
end;

end.
