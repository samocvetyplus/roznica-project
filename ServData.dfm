object dmServ: TdmServ
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 709
  Width = 834
  object quWHA2UID: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub 0')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'SELECT si.SITEMID, si.UID, si.W, si.SZ,'
      '       i.DepID,  p.Price2,'
      
        '       sf.SInfoId, sf.Price, sf.SupId, sf.NDSId, sf.SN, sf.SSF, ' +
        'sf.SDate,'
      '       c.SName sup, n.Name NDSName'
      
        'FROM SITEM si, SINV i, SEL e, Price p, Art2 a2, SInfo sf, D_Comp' +
        ' c, D_NDS n'
      'where si.SItemId=?SItemId and'
      '      e.SInvId=i.SInvId AND'
      '      a2.Art2Id=e.Art2Id and'
      '      si.SElId= e.SElId AND'
      '      p.DepId=i.DepId and'
      '      p.Art2id=a2.Art2Id and'
      '      sf.SInfoId=si.SInfoId and'
      '      c.D_CompId=sf.SupId and'
      '      n.NDSID=sf.NDSID')
    SelectSQL.Strings = (
      'SELECT si.SITEMID, si.UID, si.W, si.SZ,'
      '       i.DepID,  p.Price2,'
      
        '       sf.SInfoId, sf.Price, sf.SupId, sf.NDSId, sf.SN, sf.SSF, ' +
        'sf.SDate,'
      '       c.SName sup, n.Name NDSName'
      
        'FROM SITEM si, SINV i, SEL e, Price p, Art2 a2, SInfo sf, D_Comp' +
        ' c, D_NDS n'
      'where '
      '      i.IsClosed=1 AND'
      '      e.SInvId=i.SInvId AND'
      '      a2.Art2Id=e.Art2Id and'
      '      a2.D_ArtId=:D_ArtId and'
      '      si.SElId= e.SElId AND'
      '      si.BusyType=0 and'
      '      p.DepId=i.DepId and'
      '      p.Art2id=a2.Art2Id and'
      '      sf.SInfoId=si.SInfoId and'
      '      c.D_CompId=sf.SupId and'
      '      n.NDSID=sf.NDSID'
      'group by si.UID'
      '      '
      '')
    BeforeOpen = quWHA2UIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Filtered = True
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 427
    Top = 161
    object quWHA2UIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quWHA2UIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quWHA2UIDW: TFloatField
      FieldName = 'W'
    end
    object quWHA2UIDPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quWHA2UIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHA2UIDSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object quWHA2UIDSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quWHA2UIDNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quWHA2UIDNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object quWHA2UIDDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quWHA2UIDSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object quWHA2UIDPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quWHA2UIDSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quWHA2UIDSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quWHA2UIDSN: TIntegerField
      FieldName = 'SN'
    end
  end
  object quWHSZ: TpFIBDataSet
    SelectSQL.Strings = (
      'select u.sz, count(*) Quantity, sum(u.W) Weight,u.DepId,'
      '         d.SName'
      'from FUIDREST u, D_Dep d'
      'where '
      ''
      '           u.DepId between :DEPID1 and :DEPID2 and'
      '/*        u.ndsid=:NDSID and*/'
      ''
      '           u.D_ArtId=:ARTID and'
      
        '           u.DepId  = d.D_DepId and d.ISDELETE <>1              ' +
        '        '
      'group by u.sz, u.DepId, d.SName '
      'order by  u.sz')
    BeforeOpen = quWHUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrWH
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 463
    Top = 262
    object quWHSZSZ: TFIBStringField
      FieldName = 'SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quWHSZQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Required = True
    end
    object quWHSZWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHSZDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quWHSZSNAME: TFIBStringField
      FieldName = 'SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrWHA2UID: TDataSource
    DataSet = quWHA2UID
    Left = 428
    Top = 210
  end
  object dsrWHSZ: TDataSource
    DataSet = quWHSZ
    Left = 467
    Top = 312
  end
  object taWH: TpFIBDataSet
    UpdateSQL.Strings = (
      'update d_art'
      'set zq=?zq'
      'where d_Artid=?old_d_artid')
    SelectSQL.Strings = (
      'select D_ARTID,  NDSID,ART, UNITID, D_COMPID, COMPCODE,'
      '  MAT, GOOD, INS, COUNTRYID, Q, W, TW, TQ, ZQ, Att1, Att2'
      'from ArtWh'
      'where Art between :ART1 and :ART2'
      ''
      ''
      ''
      ''
      ''
      ''
      'order by Art')
    AfterOpen = taWHAfterOpen
    AfterScroll = taWHAfterScroll
    BeforeOpen = taWHBeforeOpen
    OnCalcFields = taWHCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 314
    Top = 267
    object taWHD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'WH_S4.D_ARTID'
    end
    object taWHART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'WH_S4.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWHUNITID: TIntegerField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'UNITID'
      Origin = 'WH_S4.UNITID'
      OnGetText = UNITIDGetText
    end
    object taWHD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'WH_S4.D_COMPID'
    end
    object taWHCOMPCODE: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'COMPCODE'
      Origin = 'WH_S4.COMPCODE'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWHMAT: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'MAT'
      Origin = 'WH_S4.MAT'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWHGOOD: TFIBStringField
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'GOOD'
      Origin = 'WH_S4.GOOD'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWHINS: TFIBStringField
      DisplayLabel = #1042#1089#1090'.'
      FieldName = 'INS'
      Origin = 'WH_S4.INS'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWHQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WH_S4.Q'
    end
    object taWHW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WH_S4.W'
      DisplayFormat = '0.##'
    end
    object taWHTW: TFIBStringField
      FieldName = 'TW'
      Origin = 'WH_S4.TW'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taWHTQ: TFIBStringField
      FieldName = 'TQ'
      Origin = 'WH_S4.TQ'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taWHNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'WH_S4.NDSID'
    end
    object taWHNDSNAME: TStringField
      DisplayLabel = #1053#1044#1057
      FieldKind = fkLookup
      FieldName = 'NDSNAME'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Size = 10
      Lookup = True
    end
    object taWHCOUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      DisplayWidth = 3
      FieldName = 'COUNTRYID'
      Origin = 'WH_S4.COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWHZQ: TIntegerField
      FieldName = 'ZQ'
      Origin = 'WH_S4.ZQ'
    end
    object taWHATT1: TFIBStringField
      FieldName = 'ATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object taWHATT2: TFIBStringField
      FieldName = 'ATT2'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object dsrWH: TDataSource
    DataSet = taWH
    Left = 309
    Top = 312
  end
  object quWHArt2: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select u.Art2Id, a2.Art2, u.Price Price2, u.DepId D_DepId, d.SNa' +
        'me ,'
      '       count(*) Quantity, sum(u.w) Weight'
      'from FUIDREST u, Art2 a2,  D_Dep d '
      'where '
      ''
      '      u.DepId between :DEPID1 and :DEPID2 and'
      '/*       u.ndsid=:NDSID and*/'
      ''
      '      a2.Art2Id=u.Art2Id and'
      '      a2.D_ArtId=:ARTID and'
      
        '      u.DepId  =  d.D_DepId and d.IsDELETE <> 1                 ' +
        '     '
      'group by  u.Art2Id, a2.Art2, u.Price,u.DepId, d.SName '
      'order by a2.Art2')
    BeforeOpen = quWHUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrWH
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 513
    Top = 262
    object quWHArt2ART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quWHArt2ART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quWHArt2WEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHArt2QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object quWHArt2PRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quWHArt2D_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Required = True
    end
    object quWHArt2SNAME: TFIBStringField
      FieldName = 'SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrWHArt2: TDataSource
    DataSet = quWHArt2
    Left = 517
    Top = 312
  end
  object quWHUID: TpFIBDataSet
    UpdateSQL.Strings = (
      'update fuidrest'
      'set appldepq=:appldepq,'
      '    appldep_user=:appldep_user'
      'where FUIDRESTID=:FUIDRESTID')
    RefreshSQL.Strings = (
      'select  u.UID, u.W, u.Sz, u.DepId, a2.Art2, '
      
        '           u.Art2Id,  u.Price Price2, u.SPrice0 SPrice, u.SDate0' +
        ' SDate, '
      '           u.SN0 Sn, u.SSF0 SSF,'
      
        '           (select SName from D_Comp where D_CompId=u.SupId0)  s' +
        'up,'
      
        '           d.SName, u.D_Goods_sam1, u.d_goods_sam2, u.appldepq, ' +
        'u.appldep_user, u.FUIDRESTID'
      'from FUIDREST u, Art2 a2, D_Dep d'
      'where u.art2Id=a2.art2Id and'
      '      u.FUIDRESTID = :FUIDRESTID and'
      '      u.DepId =d.D_DepId')
    SelectSQL.Strings = (
      'select  a2.Art2, u.UID, u.W, u.Sz, u.DepId,'
      
        '           u.Art2Id,  u.Price Price2, u.SPrice0 SPrice, u.SDate0' +
        ' SDate, '
      '           u.SN0 Sn, u.SSF0 SSF,'
      '           c.sname sup,'
      
        '           d.SName, u.D_Goods_sam1, u.d_goods_sam2, u.appldepq, ' +
        'u.appldep_user, u.FUIDRESTID'
      'from  Art2 a2'
      '  left join FUIDREST u on u.art2Id=a2.art2Id'
      '  left join d_comp c on u.SupId0=c.d_compid'
      '  left join D_Dep d on u.DepId = d.D_DepId'
      'where   a2.D_ArtId=:ARTID and'
      '        u.DepId between :DEPID1 and :DEPID2 and'
      '        d.ISDELETE <>1')
    AfterPost = CommitRetaining
    BeforeClose = quWHUIDBeforeClose
    BeforeOpen = quWHUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrWH
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 413
    Top = 263
    object quWHUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quWHUIDUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quWHUIDW: TFIBFloatField
      FieldName = 'W'
    end
    object quWHUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHUIDDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quWHUIDART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quWHUIDPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object quWHUIDSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
    object quWHUIDSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quWHUIDSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quWHUIDSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quWHUIDSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quWHUIDSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quWHUIDD_GOODS_SAM1: TFIBStringField
      FieldName = 'D_GOODS_SAM1'
      Size = 60
      EmptyStrToNull = True
    end
    object quWHUIDD_GOODS_SAM2: TFIBStringField
      FieldName = 'D_GOODS_SAM2'
      Size = 60
      EmptyStrToNull = True
    end
    object quWHUIDAPPLDEPQ: TFIBSmallIntField
      FieldName = 'APPLDEPQ'
    end
    object quWHUIDAPPLDEP_USER: TFIBIntegerField
      FieldName = 'APPLDEP_USER'
    end
    object quWHUIDFUIDRESTID: TFIBIntegerField
      FieldName = 'FUIDRESTID'
    end
  end
  object dsWHUID: TDataSource
    DataSet = quWHUID
    Left = 416
    Top = 312
  end
  object qrSupInf: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_Sup, R_Sum, R_Supcode'
      'from SupInf'
      'order by R_SUPCODE')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 485
    Top = 56
    object qrSupInfR_SUP: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      FieldName = 'R_SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object qrSupInfR_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'R_SUM'
      currency = True
    end
    object qrSupInfR_SUPCODE: TFIBStringField
      DisplayLabel = #1050#1088'. '#1085#1072#1080#1084'.'
      FieldName = 'R_SUPCODE'
      EmptyStrToNull = True
    end
  end
  object dsrSupInf: TDataSource
    DataSet = qrSupInf
    Left = 488
    Top = 104
  end
  object qrWHInv: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT sum(u.W) SW,  count(u.UID) SC, a2.ART2, u.PRICE Price2, '
      '               u.Sprice0 SPrice, u.SDate0, u.SSF0, u.SN0'
      'FROM FUIDREST u , ART2 a2'
      'where   a2.Art2Id=u.Art2Id and'
      '            a2.D_ArtId=:ARTID '
      'group by u.SDate0, u.SSF0, u.Price, u.SPrice0, a2.Art2, u.SN0'
      'order by u.Sdate0 desc')
    BeforeOpen = qrWHInvBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrWH
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 240
    Top = 54
    object qrWHInvSW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'SW'
    end
    object qrWHInvSC: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'SC'
      Required = True
    end
    object qrWHInvART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      FieldName = 'ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object qrWHInvPRICE2: TFloatField
      DisplayLabel = #1056#1072#1089#1093'.'#1094#1077#1085#1072
      FieldName = 'PRICE2'
    end
    object qrWHInvSPRICE: TFloatField
      DisplayLabel = #1055#1088#1080#1093'.'#1094#1077#1085#1072
      FieldName = 'SPRICE'
    end
    object qrWHInvSDATE0: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1088#1080#1093'.'
      FieldName = 'SDATE0'
    end
    object qrWHInvSSF0: TFIBStringField
      DisplayLabel = #1042#1085#1077#1096'.'#1085#1072#1082#1083'.'
      FieldName = 'SSF0'
      FixedChar = True
      EmptyStrToNull = True
    end
    object qrWHInvSN0: TIntegerField
      DisplayLabel = #8470' '#1087#1088#1080#1093'.'
      FieldName = 'SN0'
    end
  end
  object dsrWHInv: TDataSource
    DataSet = qrWHInv
    Left = 241
    Top = 100
  end
  object frReport: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 14
    Top = 8
    ReportForm = {19000000}
  end
  object qrAnalitica: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from D_Art')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 368
    Top = 160
    object qrAnaliticaD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Required = True
    end
    object qrAnaliticaD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Required = True
    end
    object qrAnaliticaD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object qrAnaliticaD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object qrAnaliticaART: TFIBStringField
      FieldName = 'ART'
      Required = True
      EmptyStrToNull = True
    end
    object qrAnaliticaUNITID: TIntegerField
      FieldName = 'UNITID'
      Required = True
    end
    object qrAnaliticaMEMO: TMemoField
      FieldName = 'MEMO'
      BlobType = ftMemo
      Size = 8
    end
    object qrAnaliticaPICT: TBlobField
      FieldName = 'PICT'
      Size = 8
    end
    object qrAnaliticaD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object qrAnaliticaFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrAnalitica: TDataSource
    DataSet = qrAnalitica
    Left = 368
    Top = 208
  end
  object quProducer: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, CODE, SNAME'
      'FROM D_COMP'
      'WHERE PRODUCER=1'
      'ORDER BY SNAME'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 299
    Top = 57
    object quProducerD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Required = True
    end
    object quProducerCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quProducerSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object dsrProducer: TDataSource
    DataSet = quProducer
    Left = 298
    Top = 100
  end
  object quHistItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_UID, R_W, R_SZ, R_ART2, R_Art, R_UnitId, R_CompId,'
      '    R_PRICE, R_PRICE2, R_CurrPrice, R_CompName, R_Price2OLD'
      'from HistItem_S(:Link, :Art2Id, :R_I, :R_DepId,  :BD, :ED)'
      'order by R_UID')
    BeforeOpen = quHistItemBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrHistArt1
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 134
    Top = 152
    dcForceMasterRefresh = True
    dcForceOpen = True
    object quHistItemR_UID: TIntegerField
      DisplayLabel = #1048#1076'. '#1085#1086#1084#1077#1088
      FieldName = 'R_UID'
    end
    object quHistItemR_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'R_W'
    end
    object quHistItemR_SZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'R_SZ'
      EmptyStrToNull = True
    end
    object quHistItemR_ART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object quHistItemR_ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object quHistItemR_UNITID: TIntegerField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'R_UNITID'
      OnGetText = taWHUNITIDGetText
    end
    object quHistItemR_COMPID: TIntegerField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'R_COMPID'
    end
    object quHistItemR_PRICE: TFloatField
      DisplayLabel = #1055#1088#1080#1093'. '#1094#1077#1085#1072
      FieldName = 'R_PRICE'
      currency = True
    end
    object quHistItemR_PRICE2: TFloatField
      DisplayLabel = #1056#1072#1089#1093'. '#1094#1077#1085#1072
      FieldName = 'R_PRICE2'
      currency = True
    end
    object quHistItemR_CURRPRICE: TFloatField
      DisplayLabel = #1058#1077#1082'. '#1094#1077#1085#1072
      FieldName = 'R_CURRPRICE'
      currency = True
    end
    object quHistItemR_COMPNAME: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'R_COMPNAME'
      EmptyStrToNull = True
    end
    object quHistItemR_PRICE2OLD: TFloatField
      DisplayLabel = #1057#1090'. '#1094#1077#1085#1072
      FieldName = 'R_PRICE2OLD'
      currency = True
    end
  end
  object dsrHistItem: TDataSource
    DataSet = quHistItem
    Left = 134
    Top = 200
  end
  object quHistArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_SN, R_SDate, R_W, R_Art2, R_C,'
      '          R_I, R_SupId, R_SupName, R_Price,'
      '  R_Price2, R_DepId, R_DepName, R_Id Link, R_Art2Id Art2Id'
      'from HistArt_S(:ArtId, :BD, :ED, :DepId1, :DepId2)'
      'order by R_SDate')
    BeforeOpen = quHistArtBeforeOpen
    OnCalcFields = quHistArtCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 12
    Top = 152
    object quHistArtDescr: TStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldKind = fkCalculated
      FieldName = 'Descr'
      Size = 80
      Calculated = True
    end
    object quHistArtR_SN: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'R_SN'
    end
    object quHistArtR_SDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'R_SDATE'
    end
    object quHistArtR_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'R_W'
    end
    object quHistArtR_ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object quHistArtR_C: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'R_C'
    end
    object quHistArtR_I: TIntegerField
      FieldName = 'R_I'
    end
    object quHistArtR_SUPID: TIntegerField
      FieldName = 'R_SUPID'
    end
    object quHistArtR_SUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090'.'
      FieldName = 'R_SUPNAME'
      EmptyStrToNull = True
    end
    object quHistArtR_PRICE: TFloatField
      DisplayLabel = #1055#1088#1080#1093'. '#1094#1077#1085#1072
      FieldName = 'R_PRICE'
      currency = True
    end
    object quHistArtR_PRICE2: TFloatField
      DisplayLabel = #1056#1072#1089#1093'. '#1094#1077#1085#1072
      FieldName = 'R_PRICE2'
      currency = True
    end
    object quHistArtR_DEPID: TIntegerField
      FieldName = 'R_DEPID'
    end
    object quHistArtR_DEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'R_DEPNAME'
      EmptyStrToNull = True
    end
    object quHistArtLINK: TIntegerField
      FieldName = 'LINK'
    end
    object quHistArtART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
  end
  object dsrHistArt: TDataSource
    DataSet = quHistArt
    Left = 10
    Top = 201
  end
  object taRetCltList: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT SINVID,  SItemid, Art2Id, DEPID, DEP, SDATE,  SN, PNDS,'
      '    NDSName,  ISCLOSED, Clientid, CLIENTNAME,  USERID,'
      '    FIO, UID, W, SZ,  ART,  ART2, FULLART, D_RETID, RET,'
      '    PRICE2, Price2Old, SPRICE, CurrPRICE, COST2, SellN, RState, '
      '    RetPrice, SCOST, RETCOST, actdiscount, Checkno'
      'FROM RETCLTLIST_R(?SINVID)')
    SelectSQL.Strings = (
      'SELECT SINVID,  SItemid, Art2Id, DEPID, DEP, SDATE,  SN, PNDS,'
      '    NDSName,  ISCLOSED, Clientid, CLIENTNAME,  USERID,'
      '    FIO, UID, W, SZ,  ART,  ART2, FULLART, D_RETID, RET,'
      
        '    PRICE2, Price2Old, SPRICE, CurrPRICE, COST2, SellN, RState, ' +
        'RetPrice,'
      '    SCOST, RETCOST, actdiscount, Checkno, supname'
      'FROM RETCLTLIST_S(?DEPID1, ?DEPID2, ?BD, ?ED)'
      'ORDER BY SDATE')
    AfterDelete = taRetCltListAfterDelete
    BeforeOpen = taRetCltListBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 360
    Top = 56
    object taRetCltListSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taRetCltListSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taRetCltListART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taRetCltListDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taRetCltListDEP: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object taRetCltListSDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088'.'
      FieldName = 'SDATE'
    end
    object taRetCltListSN: TIntegerField
      DisplayLabel = #8470' '#1074#1086#1079#1074#1088'.'
      FieldName = 'SN'
    end
    object taRetCltListPNDS: TFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'PNDS'
    end
    object taRetCltListNDSNAME: TFIBStringField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object taRetCltListISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taRetCltListCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
    end
    object taRetCltListCLIENTNAME: TFIBStringField
      DisplayLabel = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taRetCltListUSERID: TIntegerField
      FieldName = 'USERID'
    end
    object taRetCltListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRetCltListUID: TIntegerField
      DisplayLabel = #1048#1076'.'#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taRetCltListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
    end
    object taRetCltListSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taRetCltListART: TFIBStringField
      DisplayLabel = #1040#1088#1090
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taRetCltListART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRetCltListFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085'.'#1072#1088#1090
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taRetCltListD_RETID: TFIBStringField
      FieldName = 'D_RETID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRetCltListRET: TFIBStringField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'RET'
      Size = 40
      EmptyStrToNull = True
    end
    object taRetCltListPRICE2: TFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1088#1077#1072#1083#1080#1079'.'
      FieldName = 'PRICE2'
      currency = True
    end
    object taRetCltListPRICE2OLD: TFloatField
      DisplayLabel = #1057#1090'. '#1094#1077#1085#1072
      FieldName = 'PRICE2OLD'
      currency = True
    end
    object taRetCltListSPRICE: TFloatField
      DisplayLabel = #1055#1088#1080#1093'. '#1094#1077#1085#1072
      FieldName = 'SPRICE'
      currency = True
    end
    object taRetCltListCURRPRICE: TFloatField
      DisplayLabel = #1058#1077#1082'. '#1094#1077#1085#1072
      FieldName = 'CURRPRICE'
      currency = True
    end
    object taRetCltListCOST2: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST2'
      currency = True
    end
    object taRetCltListSELLN: TIntegerField
      DisplayLabel = #8470' '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'SELLN'
    end
    object taRetCltListRSTATE: TSmallintField
      FieldName = 'RSTATE'
      Origin = 'RETCLTLIST_S.RSTATE'
      OnGetText = taRetCltListRSTATEGetText
    end
    object taRetCltListRETPRICE: TFloatField
      DisplayLabel = #1056#1072#1089#1093'. '#1094#1077#1085#1072
      FieldName = 'RETPRICE'
      Origin = 'RETCLTLIST_S.RETPRICE'
      currency = True
    end
    object taRetCltListSCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1087#1088#1080#1093'.'
      FieldName = 'SCOST'
      currency = True
    end
    object taRetCltListRETCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'RETCOST'
      currency = True
    end
    object taRetCltListACTDISCOUNT: TFIBFloatField
      FieldName = 'ACTDISCOUNT'
      currency = True
    end
    object taRetCltListCHECKNO: TFIBIntegerField
      FieldName = 'CHECKNO'
    end
    object taRetCltListSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsRetCltList: TDataSource
    DataSet = taRetCltList
    Left = 360
    Top = 100
  end
  object pmRetClt: TPopupMenu
    Left = 228
    Top = 437
    object N1: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      OnClick = RetCltClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object taTSumD: TpFIBDataSet
    SelectSQL.Strings = (
      'select  ID, IDATE, INC, INW, INQ, INVINC, INVW, INVQ, INVOSTC,'
      '    INVOSTW, INVOSTQ, SLINC, SLOUTC, SLW, SLQ, OPTINC,'
      '    OPTOUTC,  OPTW, OPTQ, RETOPTINC, RETOPTRW,'
      '    RETOPTRQ, RETINC, RETW, RETQ, RETSINC, RETSW, RETSQ,'
      '    OUTC, OUTW, OUTQ '
      'from Total'
      'where Idate between :BD and  :ED '
      'order by IDATE')
    BeforeOpen = taTSumDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 496
    Top = 164
    object taTSumDID: TIntegerField
      FieldName = 'ID'
      Required = True
    end
    object taTSumDIDATE: TDateTimeField
      DisplayLabel = #1044#1077#1085#1100' '#1088#1072#1089#1095#1077#1090#1072
      FieldName = 'IDATE'
      Required = True
    end
    object taTSumDINC: TFloatField
      DisplayLabel = #1042#1093'. '#1089#1091#1084#1084#1072
      FieldName = 'INC'
      Required = True
      currency = True
    end
    object taTSumDINW: TFloatField
      DisplayLabel = #1042#1093'. '#1074#1077#1089
      FieldName = 'INW'
    end
    object taTSumDINQ: TIntegerField
      DisplayLabel = #1042#1093'. '#1082'-'#1074#1086
      FieldName = 'INQ'
    end
    object taTSumDINVINC: TFloatField
      DisplayLabel = #1055#1086#1089#1090'.'
      FieldName = 'INVINC'
      Required = True
      currency = True
    end
    object taTSumDINVW: TFloatField
      DisplayLabel = #1055#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'INVW'
    end
    object taTSumDINVQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'INVQ'
    end
    object taTSumDINVOSTC: TFloatField
      DisplayLabel = #1054#1089#1090'.'
      FieldName = 'INVOSTC'
      Required = True
      currency = True
    end
    object taTSumDINVOSTW: TFloatField
      DisplayLabel = #1054#1089#1090'. '#1074#1077#1089
      FieldName = 'INVOSTW'
    end
    object taTSumDINVOSTQ: TIntegerField
      DisplayLabel = #1054#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'INVOSTQ'
    end
    object taTSumDSLINC: TFloatField
      DisplayLabel = #1055#1088#1086#1076'('#1087#1088#1080#1093'.) '
      FieldName = 'SLINC'
      Required = True
    end
    object taTSumDSLOUTC: TFloatField
      DisplayLabel = #1055#1088#1086#1076'('#1088#1072#1089#1093'.)'
      FieldName = 'SLOUTC'
      Required = True
      currency = True
    end
    object taTSumDSLW: TFloatField
      DisplayLabel = #1055#1088#1086#1076'. '#1074#1077#1089
      FieldName = 'SLW'
    end
    object taTSumDSLQ: TIntegerField
      DisplayLabel = #1055#1088#1086#1076'. '#1082'-'#1074#1086
      FieldName = 'SLQ'
    end
    object taTSumDOPTINC: TFloatField
      DisplayLabel = #1054#1087#1090'.'#1087#1088#1086#1076'.('#1087#1088#1080#1093'.)'
      FieldName = 'OPTINC'
      Required = True
      currency = True
    end
    object taTSumDOPTOUTC: TFloatField
      DisplayLabel = #1054#1087#1090'.'#1087#1088#1086#1076'.('#1088#1072#1089#1093'.)'
      FieldName = 'OPTOUTC'
      Required = True
      currency = True
    end
    object taTSumDOPTW: TFloatField
      DisplayLabel = #1054#1087#1090'.'#1087#1088#1086#1076'. '#1074#1077#1089
      FieldName = 'OPTW'
    end
    object taTSumDOPTQ: TIntegerField
      DisplayLabel = #1054#1087#1090'.'#1087#1088#1086#1076'. '#1082'-'#1074#1086
      FieldName = 'OPTQ'
    end
    object taTSumDRETOPTINC: TFloatField
      DisplayLabel = #1042#1086#1079#1074'.'#1086#1087#1090
      FieldName = 'RETOPTINC'
      Required = True
      currency = True
    end
    object taTSumDRETOPTRW: TFloatField
      DisplayLabel = #1042#1086#1079#1074'.'#1086#1087#1090'. '#1074#1077#1089
      FieldName = 'RETOPTRW'
    end
    object taTSumDRETOPTRQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074'. '#1086#1087#1090'. '#1082'-'#1074#1086
      FieldName = 'RETOPTRQ'
    end
    object taTSumDRETINC: TFloatField
      DisplayLabel = #1042#1086#1079#1074'.'#1088#1086#1079#1085'.'
      FieldName = 'RETINC'
      Required = True
      currency = True
    end
    object taTSumDRETW: TFloatField
      DisplayLabel = #1042#1086#1079#1074'. '#1074#1077#1089
      FieldName = 'RETW'
    end
    object taTSumDRETQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074'. '#1082'-'#1074#1086
      FieldName = 'RETQ'
    end
    object taTSumDRETSINC: TFloatField
      DisplayLabel = #1042#1086#1079#1074'.'#1087#1086#1089#1090'.'
      FieldName = 'RETSINC'
      Required = True
      currency = True
    end
    object taTSumDRETSW: TFloatField
      DisplayLabel = #1042#1086#1079#1074'.'#1087#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'RETSW'
    end
    object taTSumDRETSQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074'. '#1087#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'RETSQ'
    end
    object taTSumDOUTC: TFloatField
      DisplayLabel = #1048#1089#1093'. '
      FieldName = 'OUTC'
      Required = True
      currency = True
    end
    object taTSumDOUTW: TFloatField
      DisplayLabel = #1048#1089#1093'. '#1074#1077#1089
      FieldName = 'OUTW'
    end
    object taTSumDOUTQ: TIntegerField
      DisplayLabel = #1048#1089#1093'. '#1082'-'#1074#1086
      FieldName = 'OUTQ'
    end
  end
  object dsrTSumD: TDataSource
    DataSet = taTSumD
    Left = 497
    Top = 212
  end
  object quAct: TpFIBDataSet
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 185
    Top = 9
  end
  object quTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 404
    Top = 12
  end
  object taAppl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update aitem'
      ' set q=?new_q,'
      ' sz=?new_sz'
      'where aitemid=?aitemid')
    DeleteSQL.Strings = (
      'delete from aitem'
      'where aitemid=?old_aitemid')
    InsertSQL.Strings = (
      'insert into aItem(AITEMID,APPLID,ARTID,SZ,Q) '
      'values (?aitemid, ?applid, ?artid, ?sz, ?q)')
    RefreshSQL.Strings = (
      
        'select ap.AItemid, ap.ApplID, ap.ArtId, a.Art, a.UnitId, a.D_Ins' +
        'Id, a.D_matid, a.D_GoodId, a.D_CompId, ap.sz,'
      'ap.Q, ap.q*a.avg_w*a.avg_spr as pr, a.rq'
      'from AItem ap, D_Art a'
      'where ap.AItemid=?AItemid and '
      '           ap.ArtId=a.D_ArtId ')
    SelectSQL.Strings = (
      
        'select ap.AItemid, ap.ApplID, ap.ArtId, a.Art, a.UnitId, a.D_Ins' +
        'Id, a.D_matid, a.D_GoodId,'
      
        '       a.D_CompId, ap.sz,ap.Q, fif(a.UnitId,ap.q*a.avg_w*a.avg_s' +
        'pr,ap.q*a.avg_spr) as pr, '
      '       ap.q*a.avg_w as avg_w,'
      '       a.rq, ap.art2'
      'from   AItem ap, D_Art a, D_mat m'
      'where  ap.ApplId=:NAp and'
      '       ap.ArtId=a.D_ArtId and'
      '       m.d_matid=a.d_matid and'
      '       m.gr between :gr1 and :gr2'
      'ORDER by a.Art, ap.art2, ap.sz'
      ''
      ''
      '      ')
    AfterDelete = taApplAfterDelete
    AfterEdit = taApplAfterEdit
    AfterPost = taApplAfterPost
    BeforeDelete = taApplBeforeDelete
    BeforeOpen = taApplBeforeOpen
    BeforePost = taApplBeforePost
    OnNewRecord = taApplNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 576
    Top = 264
    object taApplART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplUNITID: TIntegerField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'UNITID'
      Origin = 'D_ART.UNITID'
      Required = True
      OnGetText = UNITIDGetText
    end
    object taApplD_INSID: TFIBStringField
      DisplayLabel = #1054#1089#1085'.'#1074#1089#1090'.'
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplD_MATID: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplD_GOODID: TFIBStringField
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_ART.D_COMPID'
      Required = True
    end
    object taApplQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AITEM.Q'
    end
    object taApplPR: TFloatField
      DisplayLabel = #1054#1088#1080#1077#1085#1090#1080#1088'. '#1089#1091#1084#1084#1072
      FieldName = 'PR'
      currency = True
    end
    object taApplAITEMID: TIntegerField
      FieldName = 'AITEMID'
      Origin = 'AITEM.AITEMID'
      Required = True
    end
    object taApplAPPLID: TIntegerField
      FieldName = 'APPLID'
      Origin = 'AITEM.APPLID'
    end
    object taApplARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AITEM.ARTID'
    end
    object taApplSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AITEM.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplRQ: TFIBIntegerField
      FieldName = 'RQ'
    end
    object taApplART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taApplAVG_W: TFIBFloatField
      FieldName = 'AVG_W'
    end
  end
  object dsrAppl: TDataSource
    DataSet = taAppl
    Left = 572
    Top = 312
  end
  object quArtByUId: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select distinct a2.D_ArtId, a2.Art'
      'from Art2 a2, SItem it, Sel s '
      'where it.UID=:UID and'
      '          it.SelId=s.SelId and'
      '          s.Art2Id=a2.Art2Id ')
    Left = 136
    Top = 8
  end
  object quHistArt1: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_SN, R_SDate, R_W, R_Art2, R_C,'
      '          R_I, R_SupId, R_SupName, R_Price,'
      '          R_Price2, R_DepId, R_DepName, R_Id Link, '
      '          R_Art2Id Art2Id, R_Art, R_MatId, R_GoodId'
      'from HistArt1_S(:ART, :BD, :ED, :DEPID1, :DEPID2, :KIND)'
      'order by R_SDate')
    BeforeOpen = quHistArt1BeforeOpen
    OnCalcFields = quHistArt1CalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 190
    Top = 152
    dcForceMasterRefresh = True
    dcForceOpen = True
    object quHistArt1R_SN: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'R_SN'
    end
    object quHistArt1R_SDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'R_SDATE'
    end
    object quHistArt1R_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'R_W'
    end
    object quHistArt1R_ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object quHistArt1R_C: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'R_C'
    end
    object quHistArt1R_I: TIntegerField
      FieldName = 'R_I'
    end
    object quHistArt1R_SUPID: TIntegerField
      FieldName = 'R_SUPID'
    end
    object quHistArt1R_SUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090'.'
      FieldName = 'R_SUPNAME'
      EmptyStrToNull = True
    end
    object quHistArt1R_PRICE: TFloatField
      DisplayLabel = #1055#1088#1080#1093'.'#1094#1077#1085#1072
      FieldName = 'R_PRICE'
      currency = True
    end
    object quHistArt1R_PRICE2: TFloatField
      DisplayLabel = #1056#1072#1089#1093'.'#1094#1077#1085#1072
      FieldName = 'R_PRICE2'
      currency = True
    end
    object quHistArt1R_DEPID: TIntegerField
      FieldName = 'R_DEPID'
    end
    object quHistArt1R_DEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'R_DEPNAME'
      EmptyStrToNull = True
    end
    object quHistArt1LINK: TIntegerField
      FieldName = 'LINK'
    end
    object quHistArt1ART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quHistArt1Descr: TStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldKind = fkCalculated
      FieldName = 'Descr'
      Size = 80
      Calculated = True
    end
    object quHistArt1R_ART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'R_ART'
      Origin = 'HISTART1_S.R_ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quHistArt1R_MATID: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'R_MATID'
      Origin = 'HISTART1_S.R_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quHistArt1R_GOODID: TFIBStringField
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'R_GOODID'
      Origin = 'HISTART1_S.R_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrHistArt1: TDataSource
    DataSet = quHistArt1
    Left = 196
    Top = 200
  end
  object dsrList3: TDataSource
    DataSet = taList3
    Left = 8
    Top = 312
  end
  object taList3: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select j.Id, j.ARTID, j.ART, j.FULLART, j.D_COMPID, j.D_MATID,  ' +
        '        j.D_GOODID, j.UNITID, j.D_INSID, j.INS, j.INW, j.INQ, j.' +
        'OUTS,                   j.OUTW, j.OUTQ, j.DEBTORS, j.DEBTORW, j.' +
        'DEBTORQ, j.SALES,   j.SALEW, j.SALEQ, j.RETS, j.RETW, j.RETQ,  j' +
        '.RETOPTS, j.RETOPTW, j.RETOPTQ, j.RETVENDORS, j.RETVENDORW, j.RE' +
        'TVENDORQ,            j.OPTS, j.OPTW, j.OPTQ, j.DepId1, j.DepId2,' +
        '  j.SHINS, j.SHINW, j.SHINQ, j.SHMOVETOS, j.SHMOVETOW, j.SHMOVET' +
        'OQ, j.SHMOVEFROMS, j.SHMOVEFROMW, j.SHMOVEFROMQ, j.SHSALES, j.SH' +
        'SALEW,  j.SHSALEQ, j.SHRETS, j.SHRETW, j.SHRETQ, j.SHOUTS, j.SHO' +
        'UTW, j.SHOUTQ, c.SName, j.GR, j.GRNAME'
      'from JTmp j, D_Comp c'
      'where j.UserId=:USERID and'
      '          j.D_CompId between :COMPID1 and :COMPID2 and'
      '          j.D_MatId between :MATID1 and :MATID2 and'
      '          j.D_GoodId between :GOODID1 and :GOODID2 and'
      '          j.D_CompId=c.D_CompId and         '
      '          j.Gr between :GROUPID1 and :GROUPID2 '
      'order by Art')
    BeforeOpen = taList3BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 8
    Top = 264
    object taList3ARTID: TIntegerField
      Tag = 1
      FieldName = 'ARTID'
    end
    object taList3ART: TFIBStringField
      Tag = 1
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taList3FULLART: TFIBStringField
      Tag = 1
      DisplayLabel = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taList3D_COMPID: TIntegerField
      Tag = 1
      DisplayLabel = #1055#1088#1086#1080#1079#1074'.'
      FieldName = 'D_COMPID'
    end
    object taList3D_MATID: TFIBStringField
      Tag = 1
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taList3D_GOODID: TFIBStringField
      Tag = 1
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taList3UNITID: TIntegerField
      Tag = 1
      DisplayLabel = #1045#1048
      FieldName = 'UNITID'
      OnGetText = taList3UNITIDGetText
    end
    object taList3D_INSID: TFIBStringField
      Tag = 1
      DisplayLabel = #1042#1089#1090'.'
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taList3INS: TFloatField
      DisplayLabel = #1042#1093'. '#1089#1091#1084#1084#1072
      FieldName = 'INS'
      currency = True
    end
    object taList3INW: TFloatField
      DisplayLabel = #1042#1093'. '#1074#1077#1089
      FieldName = 'INW'
      DisplayFormat = '0.##'
    end
    object taList3INQ: TIntegerField
      DisplayLabel = #1042#1093'. '#1082'-'#1074#1086
      FieldName = 'INQ'
    end
    object taList3OUTS: TFloatField
      DisplayLabel = #1042#1099#1093'. '#1089#1091#1084#1084#1072
      FieldName = 'OUTS'
      currency = True
    end
    object taList3OUTW: TFloatField
      DisplayLabel = #1042#1099#1093'. '#1074#1077#1089
      FieldName = 'OUTW'
      DisplayFormat = '0.##'
    end
    object taList3OUTQ: TIntegerField
      DisplayLabel = #1042#1099#1093'. '#1082'-'#1074#1086
      FieldName = 'OUTQ'
    end
    object taList3DEBTORS: TFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072
      FieldName = 'DEBTORS'
      currency = True
    end
    object taList3DEBTORW: TFloatField
      DisplayLabel = #1055#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'DEBTORW'
      DisplayFormat = '0.##'
    end
    object taList3DEBTORQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'DEBTORQ'
    end
    object taList3SALES: TFloatField
      DisplayLabel = #1056#1086#1079#1085'. '#1087#1088#1086#1076'.'
      FieldName = 'SALES'
      currency = True
    end
    object taList3SALEW: TFloatField
      DisplayLabel = #1056#1086#1079#1085'. '#1074#1077#1089
      FieldName = 'SALEW'
      DisplayFormat = '0.##'
    end
    object taList3SALEQ: TIntegerField
      DisplayLabel = #1056#1086#1079#1085'. '#1074#1077#1089
      FieldName = 'SALEQ'
    end
    object taList3RETS: TFloatField
      DisplayLabel = #1042#1086#1079#1074'. '#1086#1090' '#1087#1086#1082#1091#1087'.'
      FieldName = 'RETS'
      currency = True
    end
    object taList3RETW: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1082'. '#1074#1077#1089
      FieldName = 'RETW'
      DisplayFormat = '0.##'
    end
    object taList3RETQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1082'. '#1082'-'#1074#1086
      FieldName = 'RETQ'
    end
    object taList3RETOPTS: TFloatField
      DisplayLabel = #1042#1086#1079#1074'. '#1086#1087#1090'.'
      DisplayWidth = 10
      FieldName = 'RETOPTS'
      currency = True
    end
    object taList3RETOPTW: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1087#1090' '#1074#1077#1089
      FieldName = 'RETOPTW'
      DisplayFormat = '0.##'
    end
    object taList3RETOPTQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1087#1090' '#1082'-'#1074#1086
      FieldName = 'RETOPTQ'
    end
    object taList3RETVENDORS: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'. '
      FieldName = 'RETVENDORS'
      currency = True
    end
    object taList3RETVENDORW: TFloatField
      DisplayLabel = #1042#1086#1079#1074'. '#1087#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'RETVENDORW'
      DisplayFormat = '0.##'
    end
    object taList3RETVENDORQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'RETVENDORQ'
    end
    object taList3OPTS: TFloatField
      DisplayLabel = #1054#1087#1090'. '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'OPTS'
      currency = True
    end
    object taList3OPTW: TFloatField
      DisplayLabel = #1054#1087#1090'. '#1087#1088#1086#1076'. '#1074#1077#1089
      FieldName = 'OPTW'
      DisplayFormat = '0.##'
    end
    object taList3OPTQ: TIntegerField
      DisplayLabel = #1054#1087#1090'.'#1087#1088#1086#1076'. '#1082'-'#1074#1086
      FieldName = 'OPTQ'
    end
    object taList3SHINS: TFIBStringField
      FieldName = 'SHINS'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHINW: TFIBStringField
      FieldName = 'SHINW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHINQ: TFIBStringField
      FieldName = 'SHINQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHOUTW: TFIBStringField
      FieldName = 'SHOUTW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHOUTS: TFIBStringField
      FieldName = 'SHOUTS'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHOUTQ: TFIBStringField
      FieldName = 'SHOUTQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHRETS: TFIBStringField
      FieldName = 'SHRETS'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHRETW: TFIBStringField
      FieldName = 'SHRETW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHRETQ: TFIBStringField
      FieldName = 'SHRETQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHSALES: TFIBStringField
      FieldName = 'SHSALES'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHSALEQ: TFIBStringField
      FieldName = 'SHSALEQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHSALEW: TFIBStringField
      FieldName = 'SHSALEW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVETOS: TFIBStringField
      FieldName = 'SHMOVETOS'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVETOW: TFIBStringField
      FieldName = 'SHMOVETOW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVETOQ: TFIBStringField
      FieldName = 'SHMOVETOQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVEFROMS: TFIBStringField
      FieldName = 'SHMOVEFROMS'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVEFROMW: TFIBStringField
      FieldName = 'SHMOVEFROMW'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3SHMOVEFROMQ: TFIBStringField
      FieldName = 'SHMOVEFROMQ'
      Size = 250
      EmptyStrToNull = True
    end
    object taList3ID: TIntegerField
      FieldName = 'ID'
      Origin = 'JTMP.ID'
      Required = True
    end
    object taList3DEPID1: TIntegerField
      FieldName = 'DEPID1'
      Origin = 'JTMP.DEPID1'
    end
    object taList3DEPID2: TIntegerField
      FieldName = 'DEPID2'
      Origin = 'JTMP.DEPID2'
    end
    object taList3SNAME: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074'.'
      FieldName = 'SNAME'
      Origin = 'D_COMP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object quJTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure MainDoc(:USERID, :BD, :ED,  :DEPID1, :DEPID2)')
    Left = 8
    Top = 408
  end
  object taJSz: TpFIBDataSet
    UpdateSQL.Strings = (
      'update j1tmp set'
      '  Sz=?Sz,'
      '  ZQ=?ZQ'
      'where ARTID = ?ARTID and'
      '      Sz=?Sz and'
      '      D_INSID=?D_INSID'
      '')
    DeleteSQL.Strings = (
      'delete from j1tmp'
      'where  ArtID = ?OLD_ARTID and'
      '       Sz = ?OLD_SZ and'
      '       UserID = ?UserID')
    InsertSQL.Strings = (
      'execute procedure Ins_Art_Anlz(?ARTID,?SZ,?UserId,?ISCALC)')
    SelectSQL.Strings = (
      '/*'#1088#1077#1072#1083#1100#1085#1086' '#1074#1099#1087#1086#1083#1085#1103#1077#1084#1099#1081' '#1079#1072#1087#1088#1086#1089' '#1089#1084'. '#1087#1088#1086#1094'. DataModuleCreate */'
      'select  ARTID, ART, D_COMPID, D_MATID, D_GOODID, UNITID,'
      
        '           D_INSID, INS, INW, INQ, OUTS, OUTW, OUTQ, DEBTORS, DE' +
        'BTORW,'
      
        '           DEBTORQ, SALES, SALEW, SALEQ, RETS, RETW, RETQ, RETOP' +
        'TS,'
      
        '           RETOPTW, RETOPTQ, RETVENDORS, RETVENDORW, RETVENDORQ,' +
        ' OPTS,'
      '           OPTW, OPTQ, SZ, COMP, CURRS, CURRW,BD, ED, FULLART, '
      '           CURRQ, ZQ, IsCalc, isszopt, percentsell,'
      '           percentret, percentsret,ID, '
      
        '           Write_offS, Write_offW, Write_offQ, SURPLUSS, SURPLUS' +
        'Q, SURPLUSW'
      'from GROUPANLZ(:IsIns, :SzMode,:USERID)'
      'order by Art')
    AfterPost = taJSzAfterPost
    BeforeDelete = taJSzBeforeDelete
    BeforeOpen = taJSzBeforeOpen
    OnCalcFields = taJSzCalcFields
    OnNewRecord = taJSzNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 56
    Top = 260
    object taJSzARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taJSzART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taJSzD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taJSzD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object taJSzD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzINS: TFIBFloatField
      FieldName = 'INS'
    end
    object taJSzINW: TFIBFloatField
      FieldName = 'INW'
    end
    object taJSzINQ: TFIBIntegerField
      FieldName = 'INQ'
    end
    object taJSzOUTS: TFIBFloatField
      FieldName = 'OUTS'
    end
    object taJSzOUTW: TFIBFloatField
      FieldName = 'OUTW'
    end
    object taJSzOUTQ: TFIBIntegerField
      FieldName = 'OUTQ'
    end
    object taJSzDEBTORS: TFIBFloatField
      FieldName = 'DEBTORS'
    end
    object taJSzDEBTORW: TFIBFloatField
      FieldName = 'DEBTORW'
    end
    object taJSzDEBTORQ: TFIBIntegerField
      FieldName = 'DEBTORQ'
    end
    object taJSzSALES: TFIBFloatField
      FieldName = 'SALES'
    end
    object taJSzSALEW: TFIBFloatField
      FieldName = 'SALEW'
    end
    object taJSzSALEQ: TFIBIntegerField
      FieldName = 'SALEQ'
    end
    object taJSzRETS: TFIBFloatField
      FieldName = 'RETS'
    end
    object taJSzRETW: TFIBFloatField
      FieldName = 'RETW'
    end
    object taJSzRETQ: TFIBIntegerField
      FieldName = 'RETQ'
    end
    object taJSzRETOPTS: TFIBFloatField
      FieldName = 'RETOPTS'
    end
    object taJSzRETOPTW: TFIBFloatField
      FieldName = 'RETOPTW'
    end
    object taJSzRETOPTQ: TFIBIntegerField
      FieldName = 'RETOPTQ'
    end
    object taJSzRETVENDORS: TFIBFloatField
      FieldName = 'RETVENDORS'
    end
    object taJSzRETVENDORW: TFIBFloatField
      FieldName = 'RETVENDORW'
    end
    object taJSzRETVENDORQ: TFIBIntegerField
      FieldName = 'RETVENDORQ'
    end
    object taJSzOPTS: TFIBFloatField
      FieldName = 'OPTS'
    end
    object taJSzOPTW: TFIBFloatField
      FieldName = 'OPTW'
    end
    object taJSzOPTQ: TFIBIntegerField
      FieldName = 'OPTQ'
    end
    object taJSzSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzCOMP: TFIBStringField
      FieldName = 'COMP'
      EmptyStrToNull = True
    end
    object taJSzCURRS: TFIBFloatField
      FieldName = 'CURRS'
    end
    object taJSzCURRW: TFIBFloatField
      FieldName = 'CURRW'
    end
    object taJSzBD: TFIBDateTimeField
      FieldName = 'BD'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taJSzED: TFIBDateTimeField
      FieldName = 'ED'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taJSzFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taJSzCURRQ: TFIBIntegerField
      FieldName = 'CURRQ'
    end
    object taJSzZQ: TFIBIntegerField
      FieldName = 'ZQ'
    end
    object taJSzISCALC: TFIBSmallIntField
      FieldName = 'ISCALC'
    end
    object taJSzISSZOPT: TFIBSmallIntField
      FieldName = 'ISSZOPT'
    end
    object taJSzPERCENTSELL: TFIBFloatField
      FieldName = 'PERCENTSELL'
    end
    object taJSzPERCENTRET: TFIBFloatField
      FieldName = 'PERCENTRET'
    end
    object taJSzPERCENTSRET: TFIBFloatField
      FieldName = 'PERCENTSRET'
    end
    object taJSzID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taJSzWRITE_OFFS: TFIBFloatField
      FieldName = 'WRITE_OFFS'
    end
    object taJSzWRITE_OFFW: TFIBFloatField
      FieldName = 'WRITE_OFFW'
    end
    object taJSzWRITE_OFFQ: TFIBIntegerField
      FieldName = 'WRITE_OFFQ'
    end
    object taJSzSURPLUSS: TFIBFloatField
      FieldName = 'SURPLUSS'
    end
    object taJSzSURPLUSQ: TFIBIntegerField
      FieldName = 'SURPLUSQ'
    end
    object taJSzSURPLUSW: TFIBFloatField
      FieldName = 'SURPLUSW'
    end
  end
  object dsrJSz: TDataSource
    DataSet = taJSz
    Left = 56
    Top = 312
  end
  object quWH_S1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure WH_S1(:USERID, :ADDALL)')
    Left = 452
    Top = 8
  end
  object quWH_S2: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure WH_S1(:USERID, :ADDALL, :BD)')
    Left = 504
    Top = 8
  end
  object pmList3: TPopupMenu
    Left = 80
    Top = 432
    object mnitAllDep: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      OnClick = mnitList3Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
  end
  object quWH_T: TpFIBDataSet
    SelectSQL.Strings = (
      'select  TQ, TA, TW'
      'from  WH_T(:MATID1, :MATID2, :GOODID1, :GOODID2, :COMPID1,'
      
        '   :COMPID2, :COUNTRYID1, :COUNTRYID2,:ART1, :ART2, :INSID1,:INS' +
        'ID2)')
    BeforeOpen = taWHBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 364
    Top = 264
    object quWH_TTQ: TFIBStringField
      FieldName = 'TQ'
      Origin = 'WH_T.TQ'
      OnGetText = TGetText
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object quWH_TTA: TFIBStringField
      FieldName = 'TA'
      Origin = 'WH_T.TA'
      OnGetText = TGetText
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quWH_TTW: TFIBStringField
      FieldName = 'TW'
      Origin = 'WH_T.TW'
      OnGetText = TFGetText
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
  end
  object dsrWH_T: TDataSource
    DataSet = quWH_T
    Left = 360
    Top = 312
  end
  object frdsrList3: TfrDBDataSet
    DataSet = taList3
    Left = 8
    Top = 360
  end
  object frdsrJSz: TfrDBDataSet
    DataSet = taJSz
    Left = 56
    Top = 360
  end
  object quUIDWH_T: TpFIBDataSet
    SelectSQL.Strings = (
      'select SNAME,COLOR,C,SAC,STW,   STP, STP2, STP3 from '
      'UIDWH_ST3'
      '(:USERID, :COMPID1, :COMPID2, :MATID1,'
      '    :MATID2, :GOODID1, :GOODID2, :SUPID1, :SUPID2,'
      
        '    :ART1, :ART2, :DEPID1, :DEPID2, :T1, :T2, :T3, :T4, :T5, :go' +
        'odsid1_1, :goodsid1_2, :goodsid2_1, :goodsid2_2, :ATT1_1, :ATT1_' +
        '2, :ATT2_1, :ATT2_2, :SZ1, :SZ2, :ART21, :ART22, :INSID1, :INSID' +
        '2, :COMISSION, :PAYTYPE)'
      'order by sname')
    BeforeOpen = quUIDWH_TBeforeOpen
    Transaction = dmCom.tr1
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 348
    Top = 376
    object quUIDWH_TSNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'SNAME'
      Origin = 'UIDWH_ST.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDWH_TCOLOR: TIntegerField
      FieldName = 'COLOR'
      Origin = 'UIDWH_ST.COLOR'
    end
    object quUIDWH_TC: TIntegerField
      DisplayLabel = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      FieldName = 'C'
      Origin = 'UIDWH_ST.C'
    end
    object quUIDWH_TSAC: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'SAC'
      Origin = 'UIDWH_ST.SAC'
    end
    object quUIDWH_TSTW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'STW'
      Origin = 'UIDWH_ST.W'
      DisplayFormat = '0.##'
      Precision = 10
    end
    object quUIDWH_TSTP: TFloatField
      DisplayLabel = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1094#1077#1085#1099
      FieldName = 'STP'
      Origin = 'UIDWH_ST.STP'
      currency = True
    end
    object quUIDWH_TSTP2: TFloatField
      DisplayLabel = #1056#1072#1089#1093#1086#1076#1085#1099#1077' '#1094#1077#1085#1099
      FieldName = 'STP2'
      Origin = 'UIDWH_ST.STP2'
      currency = True
    end
    object quUIDWH_TSTP3: TFIBFloatField
      FieldName = 'STP3'
      Origin = 'UIDWH_ST.STP3'
      currency = True
    end
  end
  object dsrUIDWH_T: TDataSource
    DataSet = quUIDWH_T
    Left = 348
    Top = 432
  end
  object quHistArtSum: TpFIBDataSet
    SelectSQL.Strings = (
      'select TD,TS,TR,TRet, TC'
      'from HistArt_Sum(:ArtId, :BD, :ED, :DepId1, :DepId2)')
    BeforeOpen = quHistArtBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 70
    Top = 152
    object quHistArtSumTD: TFIBStringField
      FieldName = 'TD'
      Origin = 'HISTART_SUM.TD'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quHistArtSumTS: TFIBStringField
      FieldName = 'TS'
      Origin = 'HISTART_SUM.TS'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quHistArtSumTR: TFIBStringField
      FieldName = 'TR'
      Origin = 'HISTART_SUM.TR'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quHistArtSumTRET: TFIBStringField
      FieldName = 'TRET'
      Origin = 'HISTART_SUM.TRET'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quHistArtSumTC: TStringField
      FieldName = 'TC'
      OnGetText = TGetTextWithDel
      Size = 400
    end
  end
  object dsrHistArtSum: TDataSource
    DataSet = quHistArtSum
    Left = 68
    Top = 200
  end
  object quHistArt1Sum: TpFIBDataSet
    SelectSQL.Strings = (
      'select TD,TS,TR,TRet, TC, TACT'
      'from HistArt1_Sum(:ART, :BD, :ED, :DEPID1, :DEPID2, :KIND)')
    BeforeOpen = quHistArt1BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 254
    Top = 152
    object IBStringField1: TFIBStringField
      FieldName = 'TD'
      Origin = 'HISTART_SUM.TD'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object IBStringField2: TFIBStringField
      FieldName = 'TS'
      Origin = 'HISTART_SUM.TS'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object IBStringField3: TFIBStringField
      FieldName = 'TR'
      Origin = 'HISTART_SUM.TR'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object IBStringField4: TFIBStringField
      FieldName = 'TRET'
      Origin = 'HISTART_SUM.TRET'
      OnGetText = TGetTextWithDel
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object StringField1: TStringField
      FieldName = 'TC'
      OnGetText = TGetTextWithDel
      Size = 400
    end
    object quHistArt1SumTACT: TFIBStringField
      FieldName = 'TACT'
      OnGetText = TGetTextWithDel
      Size = 400
      EmptyStrToNull = True
    end
  end
  object dsrHistArt1Sum: TDataSource
    DataSet = quHistArt1Sum
    Left = 252
    Top = 200
  end
  object taJUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select ARTID, ART, FULLART, D_COMPID, D_MATID, D_GOODID, UNITID,'
      
        '           D_INSID, INS, INW, INQ, OUTS, OUTW, OUTQ, DEBTORS, DE' +
        'BTORW,'
      
        '           DEBTORQ, SALES, SALEW, SALEQ, RETS, RETW, RETQ, RETOP' +
        'TS,'
      
        '           RETOPTW, RETOPTQ, RETVENDORS, RETVENDORW, RETVENDORQ,' +
        ' OPTS,'
      
        '           OPTW, OPTQ, SZ, COMP, BD, ED, SALESTR,UID,UID_SUP,CUR' +
        'RQ'
      'from JUID1Tmp'
      'where UserId=:USERID'
      'order by Art')
    BeforeOpen = taJSzBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 260
    Top = 264
    object taJUIDARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'JUID1TMP.ARTID'
    end
    object taJUIDART: TFIBStringField
      FieldName = 'ART'
      Origin = 'JUID1TMP.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJUIDFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'JUID1TMP.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taJUIDD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'JUID1TMP.D_COMPID'
    end
    object taJUIDD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'JUID1TMP.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJUIDD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'JUID1TMP.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJUIDUNITID: TSmallintField
      FieldName = 'UNITID'
      Origin = 'JUID1TMP.UNITID'
    end
    object taJUIDD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'JUID1TMP.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJUIDINS: TFloatField
      FieldName = 'INS'
      Origin = 'JUID1TMP.INS'
    end
    object taJUIDINW: TFloatField
      FieldName = 'INW'
      Origin = 'JUID1TMP.INW'
    end
    object taJUIDINQ: TIntegerField
      FieldName = 'INQ'
      Origin = 'JUID1TMP.INQ'
    end
    object taJUIDOUTS: TFloatField
      FieldName = 'OUTS'
      Origin = 'JUID1TMP.OUTS'
    end
    object taJUIDOUTW: TFloatField
      FieldName = 'OUTW'
      Origin = 'JUID1TMP.OUTW'
    end
    object taJUIDOUTQ: TIntegerField
      FieldName = 'OUTQ'
      Origin = 'JUID1TMP.OUTQ'
    end
    object taJUIDDEBTORS: TFloatField
      FieldName = 'DEBTORS'
      Origin = 'JUID1TMP.DEBTORS'
    end
    object taJUIDDEBTORW: TFloatField
      FieldName = 'DEBTORW'
      Origin = 'JUID1TMP.DEBTORW'
    end
    object taJUIDDEBTORQ: TIntegerField
      FieldName = 'DEBTORQ'
      Origin = 'JUID1TMP.DEBTORQ'
    end
    object taJUIDSALES: TFloatField
      FieldName = 'SALES'
      Origin = 'JUID1TMP.SALES'
    end
    object taJUIDSALEW: TFloatField
      FieldName = 'SALEW'
      Origin = 'JUID1TMP.SALEW'
    end
    object taJUIDSALEQ: TIntegerField
      FieldName = 'SALEQ'
      Origin = 'JUID1TMP.SALEQ'
    end
    object taJUIDRETS: TFloatField
      FieldName = 'RETS'
      Origin = 'JUID1TMP.RETS'
    end
    object taJUIDRETW: TFloatField
      FieldName = 'RETW'
      Origin = 'JUID1TMP.RETW'
    end
    object taJUIDRETQ: TIntegerField
      FieldName = 'RETQ'
      Origin = 'JUID1TMP.RETQ'
    end
    object taJUIDRETOPTS: TFloatField
      FieldName = 'RETOPTS'
      Origin = 'JUID1TMP.RETOPTS'
    end
    object taJUIDRETOPTW: TFloatField
      FieldName = 'RETOPTW'
      Origin = 'JUID1TMP.RETOPTW'
    end
    object taJUIDRETOPTQ: TIntegerField
      FieldName = 'RETOPTQ'
      Origin = 'JUID1TMP.RETOPTQ'
    end
    object taJUIDRETVENDORS: TFloatField
      FieldName = 'RETVENDORS'
      Origin = 'JUID1TMP.RETVENDORS'
    end
    object taJUIDRETVENDORW: TFloatField
      FieldName = 'RETVENDORW'
      Origin = 'JUID1TMP.RETVENDORW'
    end
    object taJUIDRETVENDORQ: TIntegerField
      FieldName = 'RETVENDORQ'
      Origin = 'JUID1TMP.RETVENDORQ'
    end
    object taJUIDOPTS: TFloatField
      FieldName = 'OPTS'
      Origin = 'JUID1TMP.OPTS'
    end
    object taJUIDOPTW: TFloatField
      FieldName = 'OPTW'
      Origin = 'JUID1TMP.OPTW'
    end
    object taJUIDOPTQ: TIntegerField
      FieldName = 'OPTQ'
      Origin = 'JUID1TMP.OPTQ'
    end
    object taJUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'JUID1TMP.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJUIDCOMP: TFIBStringField
      FieldName = 'COMP'
      Origin = 'JUID1TMP.COMP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJUIDBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'JUID1TMP.BD'
    end
    object taJUIDED: TDateTimeField
      FieldName = 'ED'
      Origin = 'JUID1TMP.ED'
    end
    object taJUIDSALESTR: TFIBStringField
      FieldName = 'SALESTR'
      Origin = 'JUID1TMP.SALESTR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taJUIDUID: TIntegerField
      FieldName = 'UID'
      Origin = 'JUID1TMP.UID'
    end
    object taJUIDUID_SUP: TIntegerField
      FieldName = 'UID_SUP'
      Origin = 'JUID1TMP.UID_SUP'
    end
    object taJUIDCURRQ: TIntegerField
      FieldName = 'CURRQ'
      Origin = 'JUID1TMP.CURRQ'
    end
  end
  object dsrJUID: TDataSource
    DataSet = taJUID
    Left = 264
    Top = 312
  end
  object frdsrJUID: TfrDBDataSet
    DataSet = taJUID
    Left = 228
    Top = 380
  end
  object taJSzSum: TpFIBDataSet
    SelectSQL.Strings = (
      'select ARTID, ART, FULLART, D_COMPID, D_MATID, D_GOODID, UNITID,'
      
        '           D_INSID, INS, INW, INQ, OUTS, OUTW, OUTQ, DEBTORS, DE' +
        'BTORW,'
      
        '           DEBTORQ, SALES, SALEW, SALEQ, RETS, RETW, RETQ, RETOP' +
        'TS,'
      
        '           RETOPTW, RETOPTQ, RETVENDORS, RETVENDORW, RETVENDORQ,' +
        ' OPTS,'
      
        '           OPTW, OPTQ, SZ, COMP, BD, ED, SALESTR,CURRS, CURRW, C' +
        'URRQ, ID,ZQ, percentsell'
      'from J1TmpSum(:USERID,:CompSz)'
      ''
      'order by Art'
      '')
    BeforeOpen = taJSzSumBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 100
    Top = 264
    object taJSzSumARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'J1TMPSUM.ARTID'
    end
    object taJSzSumD_COMPID: TIntegerField
      DisplayLabel = #1050#1086#1076' '#1087#1088#1086#1080#1079#1074'.'
      FieldName = 'D_COMPID'
      Origin = 'J1TMPSUM.D_COMPID'
    end
    object taJSzSumCOMP: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074'.'
      FieldName = 'COMP'
      Origin = 'J1TMPSUM.COMP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJSzSumD_GOODID: TFIBStringField
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'D_GOODID'
      Origin = 'J1TMPSUM.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzSumD_MATID: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'D_MATID'
      Origin = 'J1TMPSUM.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzSumD_INSID: TFIBStringField
      DisplayLabel = #1042#1089#1090'.'
      FieldName = 'D_INSID'
      Origin = 'J1TMPSUM.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzSumUNITID: TSmallintField
      DisplayLabel = #1045#1048
      FieldName = 'UNITID'
      Origin = 'J1TMPSUM.UNITID'
    end
    object taJSzSumART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'J1TMPSUM.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJSzSumSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'J1TMPSUM.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJSzSumFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090'.'
      FieldName = 'FULLART'
      Origin = 'J1TMPSUM.FULLART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJSzSumINS: TFloatField
      DisplayLabel = #1042#1093'. '#1089#1091#1084#1084#1072
      FieldName = 'INS'
      Origin = 'J1TMPSUM.INS'
    end
    object taJSzSumINW: TFloatField
      DisplayLabel = #1042#1093'. '#1074#1077#1089
      FieldName = 'INW'
      Origin = 'J1TMPSUM.INW'
    end
    object taJSzSumINQ: TIntegerField
      DisplayLabel = #1042#1093'. '#1082'-'#1074#1086
      FieldName = 'INQ'
      Origin = 'J1TMPSUM.INQ'
    end
    object taJSzSumOUTS: TFloatField
      DisplayLabel = #1042#1099#1093'. '#1089#1091#1084#1084#1072
      FieldName = 'OUTS'
      Origin = 'J1TMPSUM.OUTS'
    end
    object taJSzSumOUTW: TFloatField
      DisplayLabel = #1042#1099#1093'. '#1074#1077#1089
      FieldName = 'OUTW'
      Origin = 'J1TMPSUM.OUTW'
    end
    object taJSzSumOUTQ: TIntegerField
      DisplayLabel = #1042#1099#1093'. '#1082'-'#1074#1086
      FieldName = 'OUTQ'
      Origin = 'J1TMPSUM.OUTQ'
    end
    object taJSzSumDEBTORS: TFloatField
      DisplayLabel = #1055#1086#1089#1090'.'
      FieldName = 'DEBTORS'
      Origin = 'J1TMPSUM.DEBTORS'
    end
    object taJSzSumDEBTORW: TFloatField
      DisplayLabel = #1055#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'DEBTORW'
      Origin = 'J1TMPSUM.DEBTORW'
    end
    object taJSzSumDEBTORQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'DEBTORQ'
      Origin = 'J1TMPSUM.DEBTORQ'
    end
    object taJSzSumSALES: TFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1080
      FieldName = 'SALES'
      Origin = 'J1TMPSUM.SALES'
    end
    object taJSzSumSALEW: TFloatField
      DisplayLabel = #1055#1088#1086#1076'. '#1074#1077#1089
      FieldName = 'SALEW'
      Origin = 'J1TMPSUM.SALEW'
    end
    object taJSzSumSALEQ: TIntegerField
      DisplayLabel = #1055#1088#1086#1076'. '#1082'-'#1074#1086
      FieldName = 'SALEQ'
      Origin = 'J1TMPSUM.SALEQ'
    end
    object taJSzSumRETS: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1090' '#1087#1086#1082#1091#1087'.'
      FieldName = 'RETS'
      Origin = 'J1TMPSUM.RETS'
    end
    object taJSzSumRETW: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1074#1077#1089
      FieldName = 'RETW'
      Origin = 'J1TMPSUM.RETW'
    end
    object taJSzSumRETQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1082'-'#1074#1086
      FieldName = 'RETQ'
      Origin = 'J1TMPSUM.RETQ'
    end
    object taJSzSumRETOPTS: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1087#1090'.'
      FieldName = 'RETOPTS'
      Origin = 'J1TMPSUM.RETOPTS'
    end
    object taJSzSumRETOPTW: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1087#1090'. '#1074#1077#1089
      FieldName = 'RETOPTW'
      Origin = 'J1TMPSUM.RETOPTW'
    end
    object taJSzSumRETOPTQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1086#1087#1090'. '#1082'-'#1074#1086
      FieldName = 'RETOPTQ'
      Origin = 'J1TMPSUM.RETOPTQ'
    end
    object taJSzSumRETVENDORS: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'.'
      FieldName = 'RETVENDORS'
      Origin = 'J1TMPSUM.RETVENDORS'
    end
    object taJSzSumRETVENDORW: TFloatField
      DisplayLabel = #1042#1086#1079#1074'. '#1087#1086#1089#1090'. '#1074#1077#1089
      FieldName = 'RETVENDORW'
      Origin = 'J1TMPSUM.RETVENDORW'
    end
    object taJSzSumRETVENDORQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088'. '#1087#1086#1089#1090'. '#1082'-'#1074#1086
      FieldName = 'RETVENDORQ'
      Origin = 'J1TMPSUM.RETVENDORQ'
    end
    object taJSzSumOPTS: TFloatField
      DisplayLabel = #1054#1087#1090'. '#1089#1091#1084#1084#1072
      FieldName = 'OPTS'
      Origin = 'J1TMPSUM.OPTS'
    end
    object taJSzSumOPTW: TFloatField
      DisplayLabel = #1054#1087#1090'. '#1074#1077#1089
      FieldName = 'OPTW'
      Origin = 'J1TMPSUM.OPTW'
    end
    object taJSzSumOPTQ: TIntegerField
      DisplayLabel = #1054#1087#1090' '#1082'-'#1074#1086
      FieldName = 'OPTQ'
      Origin = 'J1TMPSUM.OPTQ'
    end
    object taJSzSumBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'J1TMPSUM.BD'
    end
    object taJSzSumED: TDateTimeField
      FieldName = 'ED'
      Origin = 'J1TMPSUM.ED'
    end
    object taJSzSumSALESTR: TFIBStringField
      FieldName = 'SALESTR'
      Origin = 'J1TMPSUM.SALESTR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taJSzSumCURRS: TFloatField
      FieldName = 'CURRS'
      Origin = 'J1TMPSUM.CURRS'
    end
    object taJSzSumCURRW: TFloatField
      FieldName = 'CURRW'
      Origin = 'J1TMPSUM.CURRW'
    end
    object taJSzSumCURRQ: TIntegerField
      FieldName = 'CURRQ'
      Origin = 'J1TMPSUM.CURRQ'
    end
    object taJSzSumZQ: TIntegerField
      FieldName = 'ZQ'
      Origin = 'J1TMPSUM.ZQ'
    end
    object taJSzSumID: TIntegerField
      FieldName = 'ID'
      Origin = 'J1TMPSUM.ID'
    end
  end
  object taApplList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update appl'
      'set isuidwh = ?isuidwh,'
      '    issell = ?issell,'
      '    issend = ?issend,'
      '    isclosed = ?isclosed,'
      '    sdate = ?sdate '
      'where applid = ?applid')
    DeleteSQL.Strings = (
      'delete from Appl where ApplId=?OLD_ApplId')
    InsertSQL.Strings = (
      
        'insert into appl(applid,userid,zdate,supid, isuidwh, issell, iss' +
        'end, isclosed)'
      'values(?applid,?userid,?zdate,?supid, 0, 0, 0, 0)')
    RefreshSQL.Strings = (
      
        'select applid, noappl, zdate, userid, isuidwh, issell, sname, su' +
        'pid, fio, allq,'
      '       pr, f1, f2, issend, isclosed, op, dep, color, sdate'
      'from ApplList_R (:applid, :f1, :f2)')
    SelectSQL.Strings = (
      'select applid, noappl, zdate, userid, isuidwh, issell,'
      '       sname, supid, fio, allq, pr, f1, f2, issend, '
      '       isclosed, op, dep, color, sdate, w       '
      'from ApplList_S (:Sup1, :Sup2, :bd, :ed)')
    AfterDelete = taApplListAfterDelete
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeInsert = taApplListBeforeInsert
    BeforeOpen = taApplListBeforeOpen
    OnNewRecord = taApplListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1079#1072#1103#1074#1086#1082
    Left = 572
    Top = 144
    object taApplListAPPLID: TFIBIntegerField
      FieldName = 'APPLID'
    end
    object taApplListNOAPPL: TFIBIntegerField
      FieldName = 'NOAPPL'
    end
    object taApplListZDATE: TFIBDateTimeField
      FieldName = 'ZDATE'
    end
    object taApplListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taApplListISUIDWH: TFIBIntegerField
      FieldName = 'ISUIDWH'
    end
    object taApplListISSELL: TFIBIntegerField
      FieldName = 'ISSELL'
    end
    object taApplListSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taApplListSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taApplListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taApplListALLQ: TFIBIntegerField
      FieldName = 'ALLQ'
    end
    object taApplListPR: TFIBFloatField
      FieldName = 'PR'
      currency = True
    end
    object taApplListF1: TFIBSmallIntField
      FieldName = 'F1'
    end
    object taApplListF2: TFIBSmallIntField
      FieldName = 'F2'
    end
    object taApplListISSEND: TFIBSmallIntField
      FieldName = 'ISSEND'
    end
    object taApplListISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object taApplListOP: TFIBStringField
      FieldName = 'OP'
      Size = 30
      EmptyStrToNull = True
    end
    object taApplListDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 30
      EmptyStrToNull = True
    end
    object taApplListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taApplListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taApplListW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.00'
    end
  end
  object dsrApplList: TDataSource
    DataSet = taApplList
    Left = 572
    Top = 200
  end
  object quSupTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 300
    Top = 488
  end
  object dsrJSzSum: TDataSource
    DataSet = taJSzSum
    Left = 100
    Top = 312
  end
  object quErr_Sell: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select fes.uid, fes.rn, fes.bd, fes.ed, fes.busytype, fes.sellit' +
        'emid, d.name'
      'from find_error_sell fes, d_dep d'
      'where fes.depid=d.d_depid')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 632
    Top = 32
    object quErr_SellUID: TIntegerField
      DisplayLabel = #1048#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'FIND_ERROR_SELL.UID'
    end
    object quErr_SellRN: TIntegerField
      DisplayLabel = #8470' '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'RN'
      Origin = 'FIND_ERROR_SELL.RN'
    end
    object quErr_SellBD: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1086#1090#1082#1088#1099#1090#1080#1103' '#1089#1084#1077#1085#1099
      FieldName = 'BD'
      Origin = 'FIND_ERROR_SELL.BD'
    end
    object quErr_SellED: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1079#1072#1082#1088#1099#1090#1080#1103' '#1089#1084#1077#1085#1099
      FieldName = 'ED'
      Origin = 'FIND_ERROR_SELL.ED'
    end
    object quErr_SellBUSYTYPE: TIntegerField
      FieldName = 'BUSYTYPE'
      Origin = 'FIND_ERROR_SELL.BUSYTYPE'
    end
    object quErr_SellSELLITEMID: TIntegerField
      FieldName = 'SELLITEMID'
      Origin = 'FIND_ERROR_SELL.SELLITEMID'
    end
    object quErr_SellNAME: TFIBStringField
      DisplayLabel = #1044#1077#1087#1072#1088#1090#1072#1084#1077#1085#1090
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dserr_sell: TDataSource
    DataSet = quErr_Sell
    Left = 632
    Top = 96
  end
  object dsrTotJSz: TDataSource
    DataSet = taTotJSz
    Left = 156
    Top = 312
  end
  object taTotJSz: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select DepID, DepName, SellRange, SellQ, SellW, OUTQ, OUTW, OutR' +
        'ange'
      'from MakeTotAnlz(?userid)')
    BeforeOpen = taTotJSzBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 156
    Top = 264
    object taTotJSzDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076' '
      FieldName = 'DEPNAME'
      Origin = 'MAKETOTANLZ.DEPNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taTotJSzSELLRANGE: TIntegerField
      DisplayLabel = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      FieldName = 'SELLRANGE'
      Origin = 'MAKETOTANLZ.SELLRANGE'
    end
    object taTotJSzSELLQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'SELLQ'
      Origin = 'MAKETOTANLZ.SELLQ'
    end
    object taTotJSzSELLW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'SELLW'
      Origin = 'MAKETOTANLZ.SELLW'
      DisplayFormat = '0.##'
    end
    object taTotJSzDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'MAKETOTANLZ.DEPID'
    end
    object taTotJSzColor: TIntegerField
      FieldKind = fkLookup
      FieldName = 'Color'
      LookupDataSet = dmCom.quDep
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'COLOR'
      KeyFields = 'DEPID'
      Lookup = True
    end
    object taTotJSzOUTQ: TFIBIntegerField
      FieldName = 'OUTQ'
    end
    object taTotJSzOUTW: TFIBFloatField
      FieldName = 'OUTW'
      DisplayFormat = '0.##'
    end
    object taTotJSzOUTRANGE: TFIBIntegerField
      FieldName = 'OUTRANGE'
    end
  end
  object quPriceCheck: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 356
    Top = 488
  end
  object quNewAppl: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 232
    Top = 484
  end
  object XMLSell: TXMLDocument
    XML.Strings = (
      '<?xml version="1.0" encoding="UTF-8"?>'
      '<EXPORTDATA>'
      '</EXPORTDATA>'
      '')
    Left = 704
    Top = 536
    DOMVendorDesc = 'MSXML'
  end
  object quSellArt: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select   FULLART,  SZ, PRICE, DiscPRICE, DISCOUNT, D_NAME, SellF' +
        'IO, SellDate'
      'from   AnlzForSell(:I_BD, :I_ED, :I_Dep)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 656
    Top = 516
  end
  object quDiscount: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct  discount'
      'from SellItem'
      'where ADate between :I_BD and :I_ED')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 660
    Top = 564
  end
  object quWHAnlz1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure  ANLZFORPROCEEDS( :I_BD, :I_SINVID)')
    Left = 608
    Top = 426
  end
  object taWH_SellDate: TpFIBDataSet
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 548
    Top = 520
  end
  object taGrIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct T_Name'
      'from Proceeds'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 548
    Top = 564
  end
  object quComp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select distinct  CompName'
      'from InsGr'
      'order by  CompName')
    Left = 492
    Top = 564
  end
  object quUidWH5: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 384
    Top = 556
  end
  object dsrJDep: TDataSource
    DataSet = taJDep
    Left = 212
    Top = 312
  end
  object taJDep: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select d.Name, d.D_DepID, Sum(s.INSUMM),Sum(s.outsumm), Sum(s.re' +
        'tsumm), Sum(s.inpricesell),Sum(s.relpricesell), Sum(RESTSUMM)'
      'from depanlz s , d_dep d'
      'where s.depid = d.d_depid and d.ISDELETE <>1'
      ''
      ''
      ''
      ''
      ''
      ''
      'group by  d.Name, d.d_DepID')
    OnCalcFields = taJDepCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 212
    Top = 268
    object taJDepNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJDepD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Required = True
    end
    object taJDepSell_In: TFloatField
      DisplayLabel = #1055#1088#1086#1076'/'#1042#1093'. (%)'
      FieldKind = fkCalculated
      FieldName = 'Sell_In'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taJDepSell_InRet: TFloatField
      DisplayLabel = #1055#1088#1086#1076'/('#1042#1093'.-'#1042#1055#1089')  %'
      FieldKind = fkCalculated
      FieldName = 'Sell_InRet'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taJDepSUM: TFloatField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
      FieldName = 'SUM'
      DisplayFormat = '0.##'
    end
    object taJDepSUM1: TFloatField
      DisplayLabel = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
      FieldName = 'SUM1'
      DisplayFormat = '0.##'
    end
    object taJDepSUM2: TFloatField
      DisplayLabel = #1056#1086#1079#1085#1080#1095#1085'. '#1074#1086#1079#1074#1088#1072#1090#1099
      FieldName = 'SUM2'
      DisplayFormat = '0.##'
    end
    object taJDepSUM3: TFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072'. '#1055#1088#1080#1093'. '#1094#1077#1085#1099
      FieldName = 'SUM3'
      DisplayFormat = '0.##'
    end
    object taJDepSUM4: TFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072'. '#1056#1077#1072#1083'. '#1094#1077#1085#1099
      FieldName = 'SUM4'
      DisplayFormat = '0.##'
    end
    object taJDepSUM5: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1082#1080
      FieldName = 'SUM5'
      DisplayFormat = '0.##'
    end
    object taJDepColour: TIntegerField
      FieldKind = fkLookup
      FieldName = 'Colour'
      LookupDataSet = dmCom.quDep
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'COLOR'
      KeyFields = 'D_DEPID'
      Lookup = True
    end
    object taJDepSellDep: TFloatField
      DisplayLabel = #1054#1090' '#1086#1073#1097#1077#1075#1086' %'
      FieldKind = fkCalculated
      FieldName = 'SellDep'
      DisplayFormat = '0.##'
      Calculated = True
    end
  end
  object quInventory: TpFIBDataSet
    UpdateSQL.Strings = (
      'update INVENTORY'
      'set isin=:isin,'
      '    price0=:price0,'
      '    order_ = :order_'
      'where INVENTORYID = :INVENTORYID')
    DeleteSQL.Strings = (
      'execute procedure Inventory_D (:inventoryid, :fl)')
    InsertSQL.Strings = (
      
        'execute procedure Inventory_I (:inventoryid, :uid, :order_, :sin' +
        'vid, :fl)')
    RefreshSQL.Strings = (
      'select INVENTORYID, UID, ART2ID, SZ,'
      '       W, ISIN, depid, Prodcode, art,'
      '       art2, d_matid, d_goodid, d_insid,'
      '       sname, color, name, price,'
      '       order_, price0, D_SUPID, D_COMPID,'
      
        '       d_note1, d_note2, d_att1id, d_att2id, d_countryid, namesu' +
        'p,'
      
        '       nameatt1, nameatt2, goods1, goods2, cost, costp, sinvid, ' +
        'unitid, FL'
      'from INVENTORY_R (?Inventoryid, :FL)')
    SelectSQL.Strings = (
      'select INVENTORYID, UID, ART2ID, SZ, W, ISIN, depid, '
      '       Prodcode, art, art2, d_matid, d_goodid, '
      '       d_insid, sname, color, name, price, order_,'
      '       price0, D_SUPID, D_COMPID, d_note1, d_note2,'
      '       d_att1id, d_att2id, d_countryid, namesup,'
      
        '       nameatt1, nameatt2, goods1, goods2, cost, costp, sinvid, ' +
        'unitid, FL'
      'from INVENTORY_s (:ISINVID, :DEPID1, :DEPID2,'
      '     :MATID1, :MATID2, :GOODID1, :GOODID2,'
      '     :SUPID1, :SUPID2, :COMPID1, :COMPID2,'
      '     :GOODSID1_1, :GOODSID1_2, :GOODSID2_1, :GOODSID2_2,'
      '     :ATT1_1, :ATT1_2, :ATT2_1, :ATT2_2)'
      'where 1=1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    AfterDelete = quInventoryAfterDelete
    AfterPost = quInventoryAfterPost
    BeforeOpen = quInventoryBeforeOpen
    OnCalcFields = quInventoryCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 40
    Top = 512
    poSQLINT64ToBCD = True
    object quInventoryUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quInventoryART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quInventorySZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quInventoryW: TFIBFloatField
      FieldName = 'W'
      EditFormat = '0.##'
    end
    object quInventoryISIN: TFIBSmallIntField
      FieldName = 'ISIN'
    end
    object quInventoryDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quInventoryPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 60
      EmptyStrToNull = True
    end
    object quInventoryART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quInventoryART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quInventoryD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInventoryD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInventoryD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInventorySNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quInventoryCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object quInventoryNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object quInventoryPRICE: TFIBFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quInventoryDescription: TStringField
      FieldKind = fkCalculated
      FieldName = 'Description'
      Size = 40
      Calculated = True
    end
    object quInventoryINVENTORYID: TFIBIntegerField
      FieldName = 'INVENTORYID'
    end
    object quInventoryORDER_: TFIBIntegerField
      FieldName = 'ORDER_'
    end
    object quInventoryPRICE0: TFIBFloatField
      DisplayLabel = #1055#1088#1080#1093'. '#1094#1077#1085#1072
      FieldName = 'PRICE0'
      currency = True
    end
    object quInventoryD_SUPID: TFIBIntegerField
      FieldName = 'D_SUPID'
    end
    object quInventoryD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quInventoryD_NOTE1: TFIBIntegerField
      FieldName = 'D_NOTE1'
    end
    object quInventoryD_NOTE2: TFIBIntegerField
      FieldName = 'D_NOTE2'
    end
    object quInventoryD_ATT1ID: TFIBIntegerField
      FieldName = 'D_ATT1ID'
    end
    object quInventoryD_ATT2ID: TFIBIntegerField
      FieldName = 'D_ATT2ID'
    end
    object quInventoryD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInventoryNAMESUP: TFIBStringField
      FieldName = 'NAMESUP'
      Size = 60
      EmptyStrToNull = True
    end
    object quInventoryNAMEATT1: TFIBStringField
      FieldName = 'NAMEATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object quInventoryNAMEATT2: TFIBStringField
      FieldName = 'NAMEATT2'
      Size = 80
      EmptyStrToNull = True
    end
    object quInventoryGOODS1: TFIBStringField
      FieldName = 'GOODS1'
      Size = 80
      EmptyStrToNull = True
    end
    object quInventoryGOODS2: TFIBStringField
      FieldName = 'GOODS2'
      Size = 80
      EmptyStrToNull = True
    end
    object quInventoryCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quInventoryCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
    object quInventorySINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quInventoryUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object quInventoryFL: TFIBSmallIntField
      FieldName = 'FL'
    end
  end
  object dsInventory: TDataSource
    DataSet = quInventory
    Left = 40
    Top = 556
  end
  object ComScan: TCommPortDriver
    ComPortInBufSize = 16
    Left = 208
    Top = 616
  end
  object ComScan2: TCommPortDriver
    ComPortInBufSize = 16
    Left = 200
    Top = 560
  end
  object taMol: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_MOL'
      'SET FIO=?FIO,'
      '    POST=?POST,'
      '    d_DEPID=?d_DEPID,'
      '    PRESEDENT=?PRESEDENT,'
      '    MEMBER=?MEMBER'
      'WHERE D_MOLID=?D_MOLID')
    DeleteSQL.Strings = (
      'DELETE FROM D_MOL'
      'WHERE D_MOLID=?OLD_D_MOLID')
    InsertSQL.Strings = (
      
        'execute procedure D_MOL_I (:FIO, :D_DEPID, :post, :presedent, :m' +
        'ember)')
    RefreshSQL.Strings = (
      'select D_MOLID, D_COMPID, FIO, D_DEPID, COLORSHOP,'
      '       post, presedent, member'
      'from D_MOL_R (?D_MOLID)')
    SelectSQL.Strings = (
      'select D_MOLID, D_COMPID, FIO, D_DEPID, COLORSHOP,'
      
        '       post, presedent, member, bit(member, 0) is$mol, bit(membe' +
        'r, 1) is$chairman, bit(member, 2) is$member'
      'from D_MOL_S')
    AfterDelete = taMolAfterDelete
    AfterPost = taMolAfterPost
    BeforeDelete = taMolBeforeDelete
    BeforePost = taMolBeforePost
    OnNewRecord = taMolNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 112
    Top = 512
    poSQLINT64ToBCD = True
    object taMolD_MOLID: TFIBIntegerField
      FieldName = 'D_MOLID'
    end
    object taMolD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taMolFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 30
      EmptyStrToNull = True
    end
    object taMolD_DEPID: TFIBIntegerField
      FieldName = 'D_DEPID'
    end
    object taMolCOLORSHOP: TFIBIntegerField
      FieldName = 'COLORSHOP'
    end
    object taMolPOST: TFIBStringField
      FieldName = 'POST'
      Size = 30
      EmptyStrToNull = True
    end
    object taMolPRESEDENT: TFIBIntegerField
      FieldName = 'PRESEDENT'
    end
    object taMolMEMBER: TFIBIntegerField
      FieldName = 'MEMBER'
    end
    object taMolISMOL: TFIBIntegerField
      FieldName = 'IS$MOL'
    end
    object taMolISCHAIRMAN: TFIBIntegerField
      FieldName = 'IS$CHAIRMAN'
    end
    object taMolISMEMBER: TFIBIntegerField
      FieldName = 'IS$MEMBER'
    end
  end
  object dsMol: TDataSource
    DataSet = taMol
    Left = 112
    Top = 556
  end
  object ap1: TApplicationEvents
    Left = 24
    Top = 80
  end
  object quListInventory: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'Select  sinvid, depid, sdate, ndate, crdate, sn, isclosed, itype' +
        ', userid,'
      '            q,  FIO, CrUserid, CrFIO, InventoryDate, DEPNAME'
      'From ListInventory_R (?SINVID)'
      '')
    SelectSQL.Strings = (
      
        'Select  sinvid, depid, sdate, ndate, crdate, sn, isclosed, itype' +
        ', userid,'
      '        q,  FIO, CrUserid, CrFIO, InventoryDate, DEPNAME'
      'From ListInventory_S'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 288
    Top = 552
    poSQLINT64ToBCD = True
    object quListInventorySINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quListInventoryDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quListInventorySDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quListInventoryNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quListInventoryCRDATE: TFIBDateTimeField
      FieldName = 'CRDATE'
    end
    object quListInventorySN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quListInventoryISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quListInventoryITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quListInventoryUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quListInventoryFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object quListInventoryQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object quListInventoryCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object quListInventoryCRFIO: TFIBStringField
      FieldName = 'CRFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object quListInventoryINVENTORYDATE: TFIBDateTimeField
      FieldName = 'INVENTORYDATE'
    end
    object quListInventoryDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Size = 400
      EmptyStrToNull = True
    end
  end
  object dsListInventory: TDataSource
    DataSet = quListInventory
    Left = 288
    Top = 608
  end
  object quInvSinv: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure PARAM_INVENTORY_U '
      ' (:SINVID, :SDATE, :NDATE, :CRDATE, :Q, :CRUSERID)')
    RefreshSQL.Strings = (
      'SELECT SINVID, DEPID, SDATE, NDATE, CRDATE, SN, ISCLOSED,'
      '       ITYPE, USERID,  Q, CRUSERID, COMPID'
      'FROM PARAM_INVENTORY (:SINVID)')
    SelectSQL.Strings = (
      'SELECT SINVID, DEPID, SDATE, NDATE, CRDATE, SN, ISCLOSED,'
      '              ITYPE, USERID,  Q, CRUSERID, COMPID'
      'FROM PARAM_INVENTORY (:SINVID)')
    AfterPost = quInvSinvAfterPost
    BeforeOpen = quInvSinvBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 72
    Top = 608
    poSQLINT64ToBCD = True
    object quInvSinvSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quInvSinvDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quInvSinvSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quInvSinvNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quInvSinvCRDATE: TFIBDateTimeField
      FieldName = 'CRDATE'
    end
    object quInvSinvSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quInvSinvISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quInvSinvITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quInvSinvUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quInvSinvQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object quInvSinvCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object quInvSinvCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
  end
  object dsInvSinv: TDataSource
    DataSet = quInvSinv
    Left = 136
    Top = 608
  end
  object quTextInventory: TpFIBDataSet
    SelectSQL.Strings = (
      'select W, ART, ART2, PRICE, UNITID, Q'
      'from INVENTORYART (:SINVID, :DEPID1, :DEPID2, :FPRICE)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 384
    Top = 608
    poSQLINT64ToBCD = True
    object quTextInventoryW: TFIBFloatField
      FieldName = 'W'
    end
    object quTextInventoryART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quTextInventoryART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quTextInventoryPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quTextInventoryUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object quTextInventoryQ: TFIBIntegerField
      FieldName = 'Q'
    end
  end
  object DepSort: TpFIBDataSet
    SelectSQL.Strings = (
      'select cast(ApplDepNum as integer) as R_No, D_DepID, SName'
      'from D_Dep'
      'where D_DepID >0 and'
      '      ISDELETE <> 1 '
      'Order by ApplDepNum'
      ' ')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 104
    Top = 360
    object DepSortR_NO: TFIBIntegerField
      FieldName = 'R_NO'
    end
    object DepSortD_DEPID: TFIBIntegerField
      FieldName = 'D_DEPID'
    end
    object DepSortSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
end
