object fmSInvImport: TfmSInvImport
  Left = 297
  Top = 174
  BorderStyle = bsDialog
  Caption = #1048#1084#1087#1086#1088#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1093'.'
  ClientHeight = 298
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 4
    Width = 97
    Height = 13
    Caption = #1044#1086#1089#1090#1091#1087#1085#1099#1077' '#1084#1086#1076#1091#1083#1080
  end
  object lsvPlugins: TListView
    Left = 4
    Top = 20
    Width = 449
    Height = 125
    Columns = <
      item
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Width = 394
      end
      item
        Caption = #1042#1077#1088#1089#1080#1103
      end>
    ReadOnly = True
    TabOrder = 0
    ViewStyle = vsReport
    OnChange = lsvPluginsChange
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 152
    Width = 449
    Height = 105
    Caption = #1054' '#1084#1086#1076#1091#1083#1077
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 50
      Height = 13
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    end
    object Label3: TLabel
      Left = 4
      Top = 76
      Width = 61
      Height = 13
      Caption = #1048#1084#1103' '#1082#1083#1072#1089#1089#1072
    end
    object lbClassName: TLabel
      Left = 68
      Top = 76
      Width = 49
      Height = 13
      Caption = 'IUnknown'
    end
    object mmDescr: TMemo
      Left = 68
      Top = 20
      Width = 365
      Height = 49
      Color = clBtnFace
      TabOrder = 0
    end
  end
  object Button1: TButton
    Left = 288
    Top = 268
    Width = 75
    Height = 25
    Caption = #1042#1099#1073#1088#1072#1090#1100
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 372
    Top = 268
    Width = 75
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    TabOrder = 3
    OnClick = Button2Click
  end
end
