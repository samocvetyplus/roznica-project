object fmRet: TfmRet
  Left = 60
  Top = 4
  Width = 800
  Height = 600
  Caption = '�������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 93
    Width = 792
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter6: TSplitter
    Left = 0
    Top = 311
    Width = 792
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 554
    Width = 792
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 792
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = '�����'
      Caption = '�����'
      Hint = '������� ����'
      ImageIndex = 0
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = '��������'
      Caption = '��������'
      Hint = '�������� ������'
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siAddClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = '�������'
      Caption = '�������'
      Hint = '������� ������'
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      BtnCaption = '��.������'
      Caption = '��.������'
      Hint = '����������������� ������'
      ImageIndex = 35
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siUIDClick
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = '�������'
      Caption = '�������'
      Hint = '������� ���������'
      ImageIndex = 5
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 792
    Height = 52
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 76
      Height = 13
      Caption = '���� ��������'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 68
      Height = 13
      Caption = '� ���������'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 216
      Top = 8
      Width = 34
      Height = 13
      Caption = 'C����:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 216
      Top = 28
      Width = 63
      Height = 13
      Caption = '����������:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 488
      Top = 8
      Width = 87
      Height = 13
      Caption = '����� �����:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 580
      Top = 8
      Width = 97
      Height = 17
      Alignment = taRightJustify
      DataField = 'COST'
      DataSource = dm.dsRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 310
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm.dsRetList
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 93
      Height = 21
      Cursor = crDrag
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsRetList
      TabOrder = 0
    end
    object deSDate: TDBDateEdit
      Left = 116
      Top = 24
      Width = 93
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsRetList
      Color = clInfoBk
      NumGlyphs = 2
      TabOrder = 1
    end
    object lcSup: TDBLookupComboBox
      Left = 284
      Top = 24
      Width = 191
      Height = 21
      Color = clInfoBk
      DataField = 'SUPID'
      DataSource = dm.dsRetList
      KeyField = 'D_COMPID'
      ListField = 'NAME'
      ListSource = dm2.dsAllBuyer
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 314
    Width = 792
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 249
      Top = 0
      Width = 3
      Height = 240
      Cursor = crHSplit
    end
    object Panel3: TPanel
      Left = 252
      Top = 0
      Width = 540
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 540
        Height = 27
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label13: TLabel
          Left = 12
          Top = 6
          Width = 32
          Height = 13
          Caption = '�����'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edArt: TEdit
          Left = 50
          Top = 2
          Width = 79
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          Text = 'edArt'
          OnChange = edArtChange
          OnKeyDown = edArtKeyDown
        end
      end
      object M207IBGrid1: TM207IBGrid
        Left = 0
        Top = 27
        Width = 540
        Height = 213
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsDPrice
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pmWH
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = siAddClick
        FixedCols = 3
        OnGetCellParams = M207IBGrid1GetCellParams
        SortOnTitleClick = False
        Columns = <
          item
            Expanded = False
            FieldName = 'FULLART'
            Title.Alignment = taCenter
            Title.Caption = '������ �������'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 188
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = '������� 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 91
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UNITID'
            Title.Alignment = taCenter
            Title.Caption = '��'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 40
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'SPRICE'
            Title.Alignment = taCenter
            Title.Caption = '��.����'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = '���.����'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 249
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object Splitter4: TSplitter
        Left = 121
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object Splitter5: TSplitter
        Left = 184
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 124
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 187
        Top = 1
        Width = 61
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dgEl: TM207IBGrid
    Left = 0
    Top = 96
    Width = 792
    Height = 215
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsSEl
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = pmEl
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = siUIDClick
    OnEditButtonClick = dgElEditButtonClick
    FixedCols = 4
    OnGetCellParams = dgElGetCellParams
    SortOnTitleClick = False
    Columns = <
      item
        Expanded = False
        FieldName = 'RecNo'
        Title.Alignment = taCenter
        Title.Caption = '�'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 26
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'FULLART'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '������ �������'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '������� 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 163
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UNITID'
        Title.Alignment = taCenter
        Title.Caption = '��'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 25
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'QUANTITY'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '���-��'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 61
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'TOTALWEIGHT'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '����� ���'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cost'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = '�����'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 78
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = '��.����'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 74
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = '���.����'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 332
    Top = 160
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 460
    Top = 212
    object N1: TMenuItem
      Caption = '������� ������'
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelClick
    end
    object N3: TMenuItem
      Caption = '����������������� ������'
      ImageIndex = 35
      OnClick = siUIDClick
    end
  end
  object pmWH: TPopupMenu
    Images = dmCom.ilButtons
    Left = 380
    Top = 410
    object N2: TMenuItem
      Caption = '��������'
      ImageIndex = 1
      ShortCut = 13
      OnClick = siAddClick
    end
  end
end
