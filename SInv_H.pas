unit SInv_H;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmSInv_H = class(TForm)
    Label1: TLabel;
    dg1: TDBGridEh;
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    OpSBtn: TSpeedButton;
    ClSBtn: TSpeedButton;
    CnlSBtn: TSpeedButton;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    DataSource1: TDataSource;
    LogEd: TEdit;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    Label9: TLabel;
    procedure OpSBtnClick(Sender: TObject);
    procedure ClSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmSInv_H: TfmSInv_H;
  var log,s:string;
  procedure Hist(var user, st: string);
implementation

   uses comdata, data, SInv, MsgDialog, DInvItem, M207IBLogin;

   {$R *.dfm}
   procedure Hist(var user, st: string); //���������� ������ � ������� INVHIST
begin
     fmSInv_H.taHist.Append;
     fmSInv_H.taHistDOCID.AsInteger := dm.taSListSINVID.AsInteger;
     fmSInv_H.taHistFIO.Value := user;
     fmSInv_H.taHistSTATUS.Value := st;
     fmSInv_H.taHist.Post;
end;


procedure TfmSInv_H.OpSBtnClick(Sender: TObject);
  //�������� ���������
begin
 taEmp.Active:=true;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin
   log:=LogEd.Text;
   s:='�������';
   Hist(log, s);
   fmSInv.acOpen.Execute;
   close;
   end;
     taEmp.Active:=false;
end;

procedure TfmSInv_H.ClSBtnClick(Sender: TObject);  //�������� ���������
begin
 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
 if LogEd.Text='' then log:=dmCom.User.Alias;
     s:='�������';
     Hist(log, s);
     fmSInv.acClose.Execute;
     close;
    end;
end;

procedure TfmSInv_H.CnlSBtnClick(Sender: TObject);  //������
begin
 taHist.Active:=false;
 close;
end;

procedure TfmSInv_H.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  taHist.ParamByName('INVID').AsInteger := dm.taSListSINVID.AsInteger;
end;

procedure TfmSInv_H.FormCreate(Sender: TObject);
begin
   taHist.Active := True;
   dg1.DataSource.DataSet.Last;    //��������� ���������
end;

procedure TfmSInv_H.FormActivate(Sender: TObject);
begin
  Pswed.SetFocus;
  Caption:='�'+(dm.taSListSN.AsString);
  if dm.taSListISCLOSED.AsInteger<>0 then
   begin
   fmSInv_H.ClSBtn.Enabled:=false;
     //���������� ���� �����������
    Label9.Visible:=true;
    LogEd.Visible:=true;
    LogEd.Text:= dmCom.User.Alias;
    PswEd.Visible:=true;
    LaLogin.Visible:=true;
    LaPsw.Visible:=true;
   end
  else
   begin
   fmSInv_H.OpSBtn.Enabled:=false;
   //�������� ���� �����������
   Label9.Visible:=false;
   LogEd.Visible:=false;
   PswEd.Visible:=false;
   LaLogin.Visible:=false;
   LaPsw.Visible:=false;
   end;
end;

procedure TfmSInv_H.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 taHist.Active := false;
end;

procedure TfmSInv_H.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (key=vk_return) and (ClSBtn.Enabled=true) then ClSBtn.Click;
if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSInv_H.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
 if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSInv_H.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

end.
