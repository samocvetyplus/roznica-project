unit SellDeviceSum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,Data, Selllist, DBGridEhGrouping, DB, GridsEh, DBGridEh,EhLibIBX,
  rxPlacemnt, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid;

type
  TfmDeviceSell = class(TForm)
    dsrCassa: TDataSource;
    FormStorage1: TFormStorage;
    View: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    ViewColumn2: TcxGridDBColumn;
    ViewColumn3: TcxGridDBColumn;
    ViewColumn4: TcxGridDBColumn;
    ViewColumn1: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDeviceSell: TfmDeviceSell;

implementation

{$R *.dfm}

procedure TfmDeviceSell.FormCreate(Sender: TObject);
begin
  dm.dsCassa.Close;
  dm.dsCassa.Open;
  View.DataController.Groups.FullExpand;
end;



procedure TfmDeviceSell.FormShow(Sender: TObject);
begin
  fmDeviceSell.Caption:='����� �� �������� �������� �� �������'+' '+'� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);

end;

end.
