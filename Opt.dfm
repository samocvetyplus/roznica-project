object fmOpt: TfmOpt
  Left = 178
  Top = 198
  HelpContext = 100252
  Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1086#1087#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1072#1078#1080
  ClientHeight = 573
  ClientWidth = 960
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 93
    Width = 960
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter6: TSplitter
    Left = 0
    Top = 311
    Width = 960
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 316
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 554
    Width = 960
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 960
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 851
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00000000FF0000FF00000000FFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        000000000000FF0000FF000000000000000000007B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFF00000000FF0000FF0000FF0000FF0000FF0000FF
        0000FF007B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        FF0000FF0000FF0000FF0000FF0000FF0000FF007B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00000000000000000000FF0000FF000000000000
        000000007B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF00000000FF0000FF00000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000000000FF
        FFFFFFFF7B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF000000000000000000000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF7B7B7BFF00FFFF00FFFF00FF
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFF7B
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDelete
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acDeleteExecute
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      Action = acUid
      BtnCaption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FF000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF00FFFF00FFFF00FF000000
        FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFF000000FFFFFFFFFFFFFF
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF00FFFF00FFFF00FF000000
        FFFFFF000000000000000000000000000000000000000000000000FFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFF000000FFFFFFFF
        FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FF00FFFF00FFFF00FF000000
        FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFF00000000000000000000
        0000000000000000000000000000FFFFFF000000FF00FFFF00FFFF00FF000000
        FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FF
        FFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000FF00FFFF00FFFF00FF000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FFFF00FF000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFF
        FF000000FF00FFFF00FFFF00FF0000000000FF0000FF0000FF0000FF0000FF00
        00FF0000FF0000FF000000FFFFFF000000FF00FFFF00FFFF00FFFF00FF000000
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FF00
        FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000000000
        0000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 35
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acUidExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 259
      Top = 3
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 787
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Caption = 'Dummy'
      Hint = 'Dummy|'
      ImageIndex = 9
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 960
    Height = 52
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 73
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 216
      Top = 8
      Width = 87
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 216
      Top = 28
      Width = 63
      Height = 13
      Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 488
      Top = 8
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 578
      Top = 8
      Width = 97
      Height = 17
      Alignment = taRightJustify
      DataField = 'COST'
      DataSource = dm.dsOptList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 310
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPFROM'
      DataSource = dm.dsOptList
    end
    object Label3: TLabel
      Left = 488
      Top = 28
      Width = 96
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1087#1088'. '#1094#1077#1085#1072#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 584
      Top = 28
      Width = 89
      Height = 17
      Alignment = taRightJustify
      DataField = 'SCOST'
      DataSource = dm.dsOptList
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 93
      Height = 21
      Cursor = crDrag
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsOptList
      TabOrder = 0
    end
    object lcComp: TDBLookupComboBox
      Left = 284
      Top = 24
      Width = 191
      Height = 21
      Color = clInfoBk
      DataField = 'COMPID'
      DataSource = dm.dsOptList
      KeyField = 'D_COMPID'
      ListField = 'SNAME'
      ListSource = dm2.dsBuyer
      TabOrder = 1
    end
    object dbEdSdate: TDBEditEh
      Left = 116
      Top = 25
      Width = 77
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsOptList
      EditButtons = <>
      ReadOnly = True
      TabOrder = 2
      Visible = True
    end
    object btDate: TBitBtn
      Left = 190
      Top = 25
      Width = 20
      Height = 22
      TabOrder = 3
      OnClick = btDateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 314
    Width = 960
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter7: TSplitter
      Left = 313
      Top = 0
      Height = 240
    end
    object Panel3: TPanel
      Left = 316
      Top = 0
      Width = 644
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pc1: TPageControl
        Left = 0
        Top = 0
        Width = 644
        Height = 240
        ActivePage = tsWH
        Align = alClient
        TabOrder = 0
        OnChange = pc1Change
        object tsWH: TTabSheet
          Caption = #1057#1082#1083#1072#1076
          object dgWH: TM207IBGrid
            Left = 0
            Top = 29
            Width = 636
            Height = 183
            Align = alClient
            Color = clBtnFace
            DataSource = dm.dsD_WH
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
            PopupMenu = pmWH
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = acAddExecute
            OnMouseDown = dgWHMouseDown
            IniStorage = fr1
            TitleButtons = True
            OnGetCellParams = dgWHGetCellParams
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = False
            SortOnTitleClick = True
            Columns = <
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRODCODE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1048#1079#1075'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 35
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'D_MATID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1052#1072#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 40
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'D_GOODID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1053#1072#1080#1084'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 34
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'D_INSID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1054#1042
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 33
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'D_COUNTRYID'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'ART'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 110
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'ART2'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2 '
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 97
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'DQ'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'DW'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clYellow
                Expanded = False
                FieldName = 'OPTPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clAqua
                Expanded = False
                FieldName = 'PRICE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1055#1088'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRICE2'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'QUANTITY'
                Title.Alignment = taCenter
                Title.Caption = #1042#1089#1077#1075#1086' - '#1050#1086#1083'-'#1074#1086
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'WEIGHT'
                Title.Alignment = taCenter
                Title.Caption = #1042#1089#1077#1075#1086' - '#1042#1077#1089
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end>
          end
          object tb2: TSpeedBar
            Left = 0
            Top = 0
            Width = 636
            Height = 29
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
            BtnOffsetHorz = 3
            BtnOffsetVert = 3
            BtnWidth = 70
            BtnHeight = 23
            Images = dmCom.ilButtons
            TabOrder = 1
            InternalVer = 1
            object Label2: TLabel
              Left = 376
              Top = 8
              Width = 52
              Height = 13
              Caption = #1048#1076'. '#1085#1086#1084#1077#1088
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object Label7: TLabel
              Left = 104
              Top = 10
              Width = 30
              Height = 13
              Caption = #1055#1086#1080#1089#1082
              Transparent = True
            end
            object edUID: TEdit
              Left = 432
              Top = 4
              Width = 73
              Height = 21
              Color = clInfoBk
              TabOrder = 0
              Text = 'edUID'
              OnKeyDown = edUIDKeyDown
            end
            object cbSearch: TCheckBox
              Left = 88
              Top = 10
              Width = 12
              Height = 12
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object ceArt: TComboEdit
              Left = 140
              Top = 4
              Width = 93
              Height = 21
              ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
              Color = clInfoBk
              Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                04000000000080000000120B0000120B00001000000010000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
                DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
                9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
                9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
                90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
              NumGlyphs = 1
              TabOrder = 2
              OnButtonClick = ceArtButtonClick
              OnChange = ceArtChange
              OnKeyDown = ceArtKeyDown
              OnKeyUp = ceArtKeyUp
            end
            object SpeedbarSection2: TSpeedbarSection
              Caption = 'Untitled (0)'
            end
            object SpeedItem1: TSpeedItem
              BtnCaption = #1054#1090#1082#1088#1099#1090#1100
              Caption = 'SpeedItem1'
              Hint = #1054#1090#1082#1088#1099#1090#1100
              ImageIndex = 74
              Layout = blGlyphLeft
              Spacing = 1
              Left = 3
              Top = 3
              Visible = True
              OnClick = SpeedItem1Click
              SectionName = 'Untitled (0)'
            end
          end
        end
        object tsPrice: TTabSheet
          Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
          ImageIndex = 1
          object M207IBGrid1: TM207IBGrid
            Left = 0
            Top = 0
            Width = 636
            Height = 212
            Align = alClient
            Color = clBtnFace
            DataSource = dm.dsDPrice
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
            PopupMenu = pmWH
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnGetCellParams = M207IBGrid1GetCellParams
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = False
            SortOnTitleClick = False
            Columns = <
              item
                Expanded = False
                FieldName = 'PRODCODE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1048#1079#1075'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_MATID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1052#1072#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 31
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_GOODID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1053#1072#1080#1084'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 33
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_INSID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1054#1042
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_COUNTRYID'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ART'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 69
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ART2'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UNITID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1045#1048
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 27
                Visible = True
              end
              item
                Color = clYellow
                Expanded = False
                FieldName = 'OPTPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 71
                Visible = True
              end
              item
                Color = clAqua
                Expanded = False
                FieldName = 'SPRICE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1055#1088'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRICE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end>
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 313
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 121
        Top = 1
        Height = 238
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 238
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 238
      end
      object Splitter1: TSplitter
        Left = 53
        Top = 1
        Height = 238
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 57
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dgEl: TDBGridEh
    Left = 0
    Top = 96
    Width = 960
    Height = 215
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSEl
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmEl
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = dgElDblClick
    OnEditButtonClick = dgElEditButtonClick
    OnGetCellParams = dgElGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Title.Caption = #8470
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1048#1079#1075'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1052#1072#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1053#1072#1080#1084'. '#1080#1079#1076'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1054#1042
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1040#1088#1090#1080#1082#1091#1083
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 104
      end
      item
        EditButtons = <>
        FieldName = 'D_COUNTRYID'
        Footers = <>
        Width = 30
      end
      item
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.'
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'QUANTITY'
        Footers = <>
        Title.Caption = #1050#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'TOTALWEIGHT'
        Footers = <>
        Title.Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089
      end
      item
        Color = clYellow
        EditButtons = <>
        FieldName = 'EP2'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1074' '#1086#1087#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'SSUM'
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072'||'#1074' '#1087#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'Cost2'
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072'||'#1074' '#1086#1087#1090'.'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 12
    Top = 168
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 76
    Top = 172
    object N1: TMenuItem
      Action = acDelete
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
    end
    object N3: TMenuItem
      Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
    end
  end
  object pmWH: TPopupMenu
    Images = dmCom.ilButtons
    Left = 296
    Top = 442
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 13
    end
  end
  object pdgel: TPrintDBGridEh
    DBGridEh = dgEl
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 208
    Top = 176
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 152
    Top = 176
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
      OnUpdate = acPrintUpdate
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDeleteExecute
      OnUpdate = acDeleteUpdate
    end
    object acUid: TAction
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      OnExecute = acUidExecute
      OnUpdate = acUidUpdate
    end
  end
  object taUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select uid from a_uid_difference')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 248
    Top = 176
  end
end
