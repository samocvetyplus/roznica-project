unit WHA2UID;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmWHA2UID = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    ibgrA2UID: TM207IBGrid;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ibgrA2UIDGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHA2UID: TfmWHA2UID;

implementation

uses comdata, Data, ServData, WH, M207Proc;

{$R *.DFM}

procedure TfmWHA2UID.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWHA2UID.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHA2UID.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if dm.WorkMode = 'WH' then
  begin
    ibgrA2UID.DataSource := dmServ.dsrWHA2UID;
    KeyPreview := True;
    OnKeyPress := dm.CloseFormByEsc;
  end;
  ibgrA2UID.DataSource.DataSet.Open;

  dm.quWHA2UID.Active:=True;
end;

procedure TfmWHA2UID.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHA2UID.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.quWHA2UID.Active:=False;
end;

procedure TfmWHA2UID.ibgrA2UIDGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

end.
