unit AnalizSkl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxCheckBox,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid, StdCtrls,
  rxSpeedbar, ExtCtrls, Menus, Data, Comdata, MsgDialog, FIBDataSet, pFIBDataSet,
  FIBQuery, pFIBQuery,dbutil, fmUtils, ComCtrls, DBClient, pFIBClientDataSet,
  Provider, DateUtils;
 const

  FlagName: array[0..3] of Char = (' ', '�', '�', '�');

  UnitName: array[0..1] of string = ('��.','��.');
type
  TfrmAnalizSklad = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    spitPrint: TSpeedItem;
    SpeedItem3: TSpeedItem;
    spitHistory: TSpeedItem;
    siEdit: TSpeedItem;
    SpeedItem6: TSpeedItem;
    spItog: TSpeedItem;
    siCalc: TSpeedItem;
    siExit: TSpeedItem;
    siHelp: TSpeedItem;
    Color: TSpeedItem;
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem4: TSpeedItem;
    siinv: TSpeedItem;
    siPayType: TSpeedItem;
    siType: TSpeedItem;
    Grid: TcxGrid;
    GridView: TcxGridDBBandedTableView;
    GridViewMARK: TcxGridDBBandedColumn;
    GridViewDEP: TcxGridDBBandedColumn;
    GridViewUID: TcxGridDBBandedColumn;
    GridViewFLAGNAME: TcxGridDBBandedColumn;
    GridViewPRODCODE: TcxGridDBBandedColumn;
    GridViewPRODNAME: TcxGridDBBandedColumn;
    GridViewSUPCODE: TcxGridDBBandedColumn;
    GridViewSUPNAME: TcxGridDBBandedColumn;
    GridViewART: TcxGridDBBandedColumn;
    GridViewART2: TcxGridDBBandedColumn;
    GridViewD_COUNTRYID: TcxGridDBBandedColumn;
    GridViewMATID: TcxGridDBBandedColumn;
    GridViewUNITNAME: TcxGridDBBandedColumn;
    GridViewINSID: TcxGridDBBandedColumn;
    GridViewS_INS: TcxGridDBBandedColumn;
    GridViewSZ: TcxGridDBBandedColumn;
    GridViewW: TcxGridDBBandedColumn;
    GridViewNAMEGOOD: TcxGridDBBandedColumn;
    GridViewGOODS1: TcxGridDBBandedColumn;
    GridViewGOODS2: TcxGridDBBandedColumn;
    GridViewSPRICE0: TcxGridDBBandedColumn;
    GridViewPriceInv: TcxGridDBBandedColumn;
    GridViewPRICE: TcxGridDBBandedColumn;
    GridViewCOST1: TcxGridDBBandedColumn;
    GridViewCostniv: TcxGridDBBandedColumn;
    GridViewCOST: TcxGridDBBandedColumn;
    GridViewSN0: TcxGridDBBandedColumn;
    GridViewSDATE0: TcxGridDBBandedColumn;
    GridViewPAYTYPENAME: TcxGridDBBandedColumn;
    GridViewSSF0: TcxGridDBBandedColumn;
    GridViewNDATE: TcxGridDBBandedColumn;
    GridViewNDSNAME: TcxGridDBBandedColumn;
    GridViewSN: TcxGridDBBandedColumn;
    GridViewSDATE: TcxGridDBBandedColumn;
    GridViewNAMEDEPFROM: TcxGridDBBandedColumn;
    GridViewSELLDATE: TcxGridDBBandedColumn;
    GridViewMemo: TcxGridDBBandedColumn;
    GridViewREQUESTOFFICE: TcxGridDBBandedColumn;
    GridViewREQUESTCOMPANY: TcxGridDBBandedColumn;
    GridViewDEPID: TcxGridDBBandedColumn;
    GridViewT: TcxGridDBBandedColumn;
    GridViewISCLOSED: TcxGridDBBandedColumn;
    GridViewITYPE: TcxGridDBBandedColumn;
    GridViewSDATETIME0: TcxGridDBBandedColumn;
    GridViewSITEMID: TcxGridDBBandedColumn;
    GridViewFilter: TcxGridDBBandedColumn;
    GridViewpricetolling: TcxGridDBBandedColumn;
    GridViewcosttolling: TcxGridDBBandedColumn;
    GridViewEUID: TcxGridDBBandedColumn;
    GridViewStateWH: TcxGridDBBandedColumn;
    GridViewCOU: TcxGridDBBandedColumn;
    GridLevel: TcxGridLevel;
    SpeedItem2: TSpeedItem;
    pm1: TPopupMenu;
    N7: TMenuItem;
    miAnyPeriod: TMenuItem;
    N26: TMenuItem;
    NRealCost: TMenuItem;
    pmtype: TPopupMenu;
    NAll: TMenuItem;
    NAllWh: TMenuItem;
    NSell: TMenuItem;
    NSret: TMenuItem;
    NOpenSinv: TMenuItem;
    NClRet: TMenuItem;
    DBData: TpFIBDataSet;
    DBDataUIDWHID: TFIBIntegerField;
    DBDataSITEMID: TFIBIntegerField;
    DBDataUID: TFIBIntegerField;
    DBDataEUID: TFIBStringField;
    DBDataNDSID: TFIBIntegerField;
    DBDataW: TFIBFloatField;
    DBDataSZ: TFIBStringField;
    DBDataDEPID: TFIBIntegerField;
    DBDataSN0: TFIBIntegerField;
    DBDataSSF0: TFIBStringField;
    DBDataSPRICE0: TFIBFloatField;
    DBDataSN: TFIBIntegerField;
    DBDataSDATETIME0: TFIBDateTimeField;
    DBDataSDATE: TFIBDateField;
    DBDataSELLDATE: TFIBDateField;
    DBDataNDATE: TFIBDateField;
    DBDataT: TFIBSmallIntField;
    DBDataISCLOSED: TFIBSmallIntField;
    DBDataITYPE: TFIBSmallIntField;
    DBDataPRICE: TFIBFloatField;
    DBDataNAMEDEPFROM: TFIBStringField;
    DBDataFLAG: TFIBIntegerField;
    DBDataGOODS1: TFIBStringField;
    DBDataART2ID: TFIBIntegerField;
    DBDataGOODS2: TFIBStringField;
    DBDataMATID: TFIBStringField;
    DBDataUNITID: TFIBIntegerField;
    DBDataINSID: TFIBStringField;
    DBDataART: TFIBStringField;
    DBDataART2: TFIBStringField;
    DBDataS_INS: TFIBStringField;
    DBDataPRODCODE: TFIBStringField;
    DBDataD_COUNTRYID: TFIBStringField;
    DBDataCOST: TFIBFloatField;
    DBDataCOST1: TFIBFloatField;
    DBDataSUPCODE: TFIBStringField;
    DBDataDEP: TFIBStringField;
    DBDataNAMEGOOD: TFIBStringField;
    DBDataPAYTYPEID: TFIBIntegerField;
    DBDataREQUESTCOMPANY: TFIBIntegerField;
    DBDataREQUESTOFFICE: TFIBIntegerField;
    DBDataMARK: TFIBIntegerField;
    DBDataIDCLIENT: TFIBIntegerField;
    DBDataFIO: TFIBStringField;
    DBDataBIRTHDAY: TFIBDateField;
    DBDataCARD: TFIBIntegerField;
    DBDataPHONEMOBILE: TFIBStringField;
    DBDataSELLSUMM: TFIBFloatField;
    DBDataSELLQUANTITY: TFIBIntegerField;
    DBDataCOU: TFIBIntegerField;
    dqWHselect: TDataSource;
    GridViewIDCLIENT: TcxGridDBBandedColumn;
    GridViewCard: TcxGridDBBandedColumn;
    GridViewSELLSUMM: TcxGridDBBandedColumn;
    Provider: TpFIBDataSetProvider;
    DBNDS: TpFIBDataSet;
    NDSProvider: TDataSetProvider;
    NDS: TClientDataSet;
    NDSID: TIntegerField;
    NDSNAME: TStringField;
    DBPayType: TpFIBDataSet;
    PayTypeProvider: TDataSetProvider;
    PayType: TClientDataSet;
    PayTypeID: TIntegerField;
    PayTypeNAME: TStringField;
    DBOffice: TpFIBDataSet;
    DBOfficeID: TFIBIntegerField;
    DBOfficeNAME: TFIBStringField;
    DBOfficeCOLOR: TFIBIntegerField;
    ProviderOffice: TDataSetProvider;
    Office: TClientDataSet;
    OfficeID: TIntegerField;
    OfficeNAME: TStringField;
    OfficeCOLOR: TIntegerField;
    DBCompany: TpFIBDataSet;
    ProviderCompany: TDataSetProvider;
    Company: TClientDataSet;
    CompanyID: TIntegerField;
    CompanyCODE: TStringField;
    CompanyNAME: TStringField;
    CompanySHORTNAME: TStringField;
    DataSourceCompany: TDataSource;
    DBMaterial: TpFIBDataSet;
    ProviderMaterial: TDataSetProvider;
    Material: TClientDataSet;
    MaterialID: TStringField;
    MaterialNAME: TStringField;
    DBInsertion: TpFIBDataSet;
    ProviderInsertion: TDataSetProvider;
    Insertion: TClientDataSet;
    InsertionID: TStringField;
    InsertionNAME: TStringField;
    DBCountry: TpFIBDataSet;
    ProviderCountry: TDataSetProvider;
    Country: TClientDataSet;
    CountryID: TStringField;
    CountryNAME: TStringField;
    DBGood: TpFIBDataSet;
    ProviderGood: TDataSetProvider;
    Good: TClientDataSet;
    GoodID: TStringField;
    GoodNAME: TStringField;
    DBPrice: TpFIBDataSet;
    ProviderPrice: TDataSetProvider;
    DataPrice: TClientDataSet;
    DataPriceUIDWHID: TIntegerField;
    DataPricePRICEINV: TFloatField;
    DBPriceTolling: TpFIBDataSet;
    ProviderPriceTolling: TDataSetProvider;
    DataTolling: TClientDataSet;
    DBDataFilter: TIntegerField;
    DBDatamemo: TStringField;
    DBDataFlagname: TStringField;
    DBDataunitname: TStringField;
    DBDataSDate0: TDateTimeField;
    DBDatapriceInv: TFloatField;
    DBDataCostInv: TFloatField;
    DBDatapriceTolling: TFloatField;
    DBDataCostTolling: TFloatField;
    DBDataccost: TFloatField;
    DBDatacprice: TFloatField;
    DBPriceTollingUID: TFIBIntegerField;
    DBPriceTollingPRICE: TFIBBCDField;
    DBPriceTollingWEIGHT: TFIBBCDField;
    DBPriceTollingWEIGHTINSERTION: TFIBFloatField;
    DBPriceTollingCOSTMATERIAL: TFIBFloatField;
    DBPriceTollingCOSTLOSSES: TFIBFloatField;
    DBPriceTollingCOSTSERVICE: TFIBFloatField;
    DBPriceTollingCOSTPRODUCT: TFIBFloatField;
    DBPriceTollingCOSTINSERTION: TFIBFloatField;
    DataTollingUID: TIntegerField;
    DataTollingPRICE: TBCDField;
    DataTollingWEIGHT: TBCDField;
    DataTollingWEIGHTINSERTION: TFloatField;
    DataTollingCOSTMATERIAL: TFloatField;
    DataTollingCOSTLOSSES: TFloatField;
    DataTollingCOSTSERVICE: TFloatField;
    DataTollingCOSTPRODUCT: TFloatField;
    DataTollingCOSTINSERTION: TFloatField;
    DBDataSUPNAME: TFIBStringField;
    DBDataPRODNAME: TFIBStringField;
    DBDataPAYTYPENAME: TFIBStringField;
    DBDataNDSNAME: TFIBStringField;
    procedure Reopen(UserID: Integer);
    procedure acRealCostExecute(Sender: TObject);
    procedure ShowPeriod ;
    procedure GetSell;
    procedure FormActivate(Sender: TObject);
    procedure miAnyPeriodClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckCalcing;
    procedure acRecalcUiDWHExecute(Sender: TObject);
    procedure PrepareData;
    procedure DBDataCalcFields(DataSet: TDataSet);
  private
    FNotRecalc {������ ��������� ������},
    FThisCalcWH {����� �� ������ ��� ��� ������ �� �����}:boolean;

    LogOprIdForm:string;
  public
    { Public declarations }
  end;

var
  frmAnalizSklad: TfrmAnalizSklad;

implementation

uses SelectUidWh, Data3, Period, getdata;

{$R *.dfm}
 {************* ������� ������� ��� ������� ������ *****************************}

 procedure TfrmAnalizSklad.Reopen(UserID: Integer);
begin
 DBPriceTolling.Close;
 DataTolling.Close;
 DataPrice.Close;
 DBPrice.Close;
  DBPrice.ParamByName('warehouse$id').AsInteger := UserID;
 DBPriceTolling.ParamByName('warehouse$id').AsInteger := UserID;
 DBPriceTolling.Open;
 DataTolling.Open;
 DataPrice.Open;
 DBPrice.Open;


 DBData.Close;
Screen.Cursor:=crSQLWait;
DBData.ParamByName('UserID').AsInteger := UserID;
DBData.Open;
end;

procedure TfrmAnalizSklad.FormCreate(Sender: TObject);
begin


if dmCom.tr.Active then dmCom.tr.CommitRetaining;
      if not dmCom.tr.Active then dmCom.tr.StartTransaction;
      if NOT dmCom.tr1.Active then dmCom.tr1.StartTransaction;
      dm.UIDWHBD:=dmCom.FirstMonthDate;   // ������ �������� ��� ������ - ������ ���� ������
      dm.UIDWHED:= strtodatetime(datetostr(dmCom.GetServerTime))+0.9999; // ����� - ������� ����
      ShowPeriod;  // ������� ������� ��� ������� ������
      OpenDataSets([dmCom.taNDS, dmCom.quDep, dmCom.taAtt1, dmCom.taAtt2]);
      with dm.quTmp do
    begin
      Close;
      //SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=1 AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') and depid = '+inttostr(SDepId)+')';
      SQL.Text:='SELECT d_depid from d_dep where isprog=1';
      ExecQuery;
      //selfdep:=Fields[0].AsInteger;
      Close;
    end;

  dm.IsChangeApplDep:=false;
  dm.IsChangeAppl:=false;
  frmAnalizSklad.Caption:= '����� ��-��������';
  FThisCalcWH:=false;
  FNotRecalc:=false;


 //������� ��� ��������

 DBNDS.Open;
 NDS.Open;
 DBPayType.Open;
 PayType.Open;
 DBOffice.Open;
 Office.Open;
 DBCompany.Open;
 Company.Open;
 DBMaterial.Open;
 Material.Open;
 DBInsertion.Open;
 Insertion.Open;
 DBCountry.Open;
 Country.Open;
 DBGood.Open;
 Good.Open;


// DBPrice.ParamByName('warehouse$id').AsInteger := strtoint('-100');
// DBPriceTolling.ParamByName('warehouse$id').AsInteger := strtoint('-100');
// DBPriceTolling.Open;
// DataTolling.Open;
// DataPrice.Open;
// DBPrice.Open;
//
//
// DBData.Close;
//Screen.Cursor:=crSQLWait;
//DBData.ParamByName('UserID').AsInteger := strtoint('-100');
//DBData.Open;
    Reopen(strtoint('256'));
Screen.Cursor:=crDefault;
end;



procedure TfrmAnalizSklad.GetSell();
var  BD, ED: TDateTime;
     d,m,y : word;
begin
  with dmCom, dm do
  begin

    BD := UIDWHBD; // ��������� ���� ������� �� ���������
    DecodeDate(BD, y, m, d); // ���������� ������ ������������� ����

    if m = 12 then
    begin
     y := y + 1;
     m := 1;
    end;
    ED := EncodeDate(y, m+1, 1)-0.00001; // �������� ���� ��������������� �� ������ ���������� ������ �� ���������


    while (ED < UIDWHED) do // ���� ED ������ ������������� �������� ����
    begin
      with quTmp do
        begin
          Close;
          if CenterDep then
             SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month ?BD, ?ED,?Fin_ED, ?USERID'
          else
             SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month_dep ?BD, ?ED,?Fin_ED, ?USERID';

          Params[0].AsTimeStamp:=DateTimeToTimeStamp(BD);
          Params[1].AsTimeStamp:=DateTimeToTimeStamp(ED);
          Params[2].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
          if UIDWHType=3 then Params[3].AsInteger:=UserDefault
          else Params[3].AsInteger:=UserId;
          ExecQuery;
          tr.CommitRetaining;
          tr1.CommitRetaining;
        end;

      BD := ED+0.00001;
      DecodeDate(BD, y, m, d);
      if m = 12 then
       ED := EncodeDate(y+1, 1, 1)-0.00001
      else
        ED := EncodeDate(y, m+1, 1)-0.00001;
    end;

    ED:= UIDWHED;

    with quTmp do
    begin
      Close;
      if CenterDep then
       SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month ?BD, ?ED,?Fin_ED, ?USERID'
      else
       SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month_dep ?BD, ?ED,?Fin_ED, ?USERID';
      Params[0].AsTimeStamp:=DateTimeToTimeStamp(BD);
      Params[1].AsTimeStamp:=DateTimeToTimeStamp(ED);
      Params[2].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
      if UIDWHType=3 then Params[3].AsInteger:=UserDefault
      else Params[3].AsInteger:=UserId;
      ExecQuery;
      tr.CommitRetaining;
      tr1.CommitRetaining;
    end
  end;

end;





procedure TfrmAnalizSklad.CheckCalcing;
var i: integer;
begin
  with dm, quTmp do
  if (UIDWHType=3) then
    begin
      SQL.Text:='SELECT UIDWHCALC FROM D_REC';
      ExecQuery;
      i:=Fields[0].AsInteger;
      Close;
      if i>0 then
       begin

        raise Exception.Create('UIDWHCALC');
       end;
    end;
end;

procedure TfrmAnalizSklad.DBDataCalcFields(DataSet: TDataSet);
var
  s: string;
  uid, mu, t, it: Integer;
begin
  s:='';

  t := DBDataT.AsInteger;

  it := DBDataITYPE.AsInteger;

  uid := DBDataUID.AsInteger;

  mu := DBDataUNITID.AsInteger;

  DBDataFilter.AsInteger := 0;

  if (T = -1) or (T = 0) or (T = 1) or (T = 5) or (T = 7) or (T = 8) then
  begin
    DBDataFilter.AsInteger := 1;
  end else

  if (T = 2) then
  begin
    DBDataFilter.AsInteger := 2;
  end else

  if (T = 4) or (T = 5) then
  begin
    DBDataFilter.AsInteger := 3;
  end else

  if (T = -2) then
  begin
    DBDataFilter.AsInteger := 4;
  end;

  s := '';

  with DataSet do


  case T of
      0: case DBDataIType.AsInteger of
             2: if  DBDataSDATETIME0.AsDateTime<=dm.BeginDate then s:='������ �� ������ �������'
                else  s:='������ �� �����������';
             5: s:='�������';
             9: case DBDataIsClosed.AsInteger of
                 0: s:='������, �������� ���������';
                 1: s:='������, ��������� �������';
                end;
             10: s:='���������� �������';
             21: s:='�������';
             else  case DBDataIsClosed.AsInteger of
                       0: s:='������ �� ������ �������, �������� ���������';
                       1: s:='������ �� ������ �������';
                     end
           end;

      1,-1: begin
           case DBDataIType.AsInteger of
               1: s:='������ �� �����������';
                //���� ���� �����.����������� � ��� ���� ��������� � ����� �������, �� ��� ��� - ����� ��� ������
               2: if (DBDataSdate.IsNull or (DBDataSdate.AsDateTime<=dm.BeginDate))
                  then s:='������ �� ������ �������'
                  else s:='���������� �����������';

               6:
                  begin
                    s:='������� �� �����������';
                  end;
               8: s:='������� �� ������� �����������';
               9: case DBDataIsClosed.AsInteger of
                   0: s:='������, �������� ���������';
                   1: s:='������, ��������� �������';
                  end;
               10: s:='���������� �������';
               21: s:='�������';
             end;

           case DBDataIsClosed.AsInteger of
               0: s:=s+', �������� ���������';
             end;
         end;
      2: case DBDataIsCLosed.AsInteger of
      //��� ������ ��������� ����� � ������� �� �������� � ����������� ������
             0: s:='�������, �������� �����';

             1,2: s:='�������';
           end;
      3: case DBDataIsClosed.AsInteger of
            0: s:='������� �������, �������� ���������';

            1: s:='������� �������';
          end;
      4: case DBDataIsClosed.AsInteger of
            1: s:='������� �����������';
          end;
      5: case DBDataIsClosed.AsInteger of
            0: s:='������� �����������, �������� ���������';
          end;
      6: s:='��� ��������';

      7: s:='��� ��������, �������� ���������';

      8: s:='������� �������, �������� ���������';

      -2: s:='������ �� �����������, �������� ���������';
    end;

  DBDataMEMO.AsString := s;

  DBDataFLAGNAME.AsString := FlagName[DBDataFLAG.AsInteger];

  DBDataUNITNAME.AsString := UnitName[mu];

//  if NDS.FindKey([DBDataNDSID.AsInteger]) then
//  begin
//    if NDSNAME.AsString <> '0%' then
//    begin
//      DBDataNDSNAME.AsString := NDSNAME.AsString;
//    end;
//  end else
//  begin
//    DBDataNDSNAME.AsString := '';
//  end;

//  if PayType.FindKey([DBDataPAYTYPEID.AsInteger]) then
//  begin
//    DBDataPAYTYPENAME.AsString := PayTypeNAME.AsString;
//  end else
//  begin
//    DBDataPAYTYPENAME.AsString := '';
//  end;

  if not DBDataSDATETIME0.IsNull then
  begin
    DBDataSDATE0.AsDateTime := DateOf(DBDataSDATETIME0.AsDateTime);
  end;

//  if Company.FindKey([DBDataPRODCODE.AsString]) then
//  begin
//    DBDataPRODNAME.AsString := CompanySHORTNAME.AsString;
//  end;

//  if Company.FindKey([DBDataSUPCODE.AsString]) then
//  begin
//    DBDataSUPNAME.AsString := CompanySHORTNAME.AsString;
//  end;

  if DataPrice.Active then
  begin
    if DataPrice.FindKey([DBDataUIDWHID.AsInteger]) then
    begin
      DBDatapriceinv.AsFloat := DataPricePRICEINV.AsFloat;

      if DBDataUNITID.AsInteger = 0 then
      begin
        DBDatacostinv.AsFloat := DataPricePRICEINV.AsFloat;
      end else
      begin
        DBDatacostinv.AsFloat := DataPricePRICEINV.AsFloat * DBDataW.AsFloat;
      end;
    end;
  end;
  //{
  if DBDataFLAG.AsInteger =2 then
  begin

  if DataTolling.FindKey([uid]) then
  begin
    DBDatapricetolling.AsFloat := DBDataSPRICE0.AsFloat;

    DBDatacosttolling.AsFloat := DBDataCOST1.AsFloat;

    DBDataccost.AsFloat := DataTollingCOSTMATERIAL.AsFloat + DataTollingCOSTLOSSES.AsFloat + DataTollingCOSTSERVICE.AsFloat;

    if mu = 0 then
    begin
     DBDatacprice.AsFloat := DBDataccost.AsFloat;
    end else
    begin
      DBDatacprice.AsFloat := DBDataccost.AsFloat / DataTollingWEIGHT.AsFloat;
    end;
  end;

  end else
  if DBDataFLAG.AsInteger = 1 then
  begin
    DBDatacprice.AsFloat := DBDataSPRICE0.AsFloat;

    DBDataccost.AsFloat := DBDataCOST1.AsFloat;
  end;

  if DBDataFLAG.AsInteger = 0 then
  begin
    DBDatacprice.AsFloat := DBDataSPRICE0.AsFloat;

    DBDataccost.AsFloat := DBDataCOST1.AsFloat;
  end;


 
end;

procedure TfrmAnalizSklad.miAnyPeriodClick(Sender: TObject);
var st:string;
    FCalcUser {������ �� ������� ������������ ����������},
    FUseCalc {����� � ���� ������������� ��� ������ ��� ���},
    FCalcMaxWh {��������� ������������ ���������� �������},
    FManyUses {������ ����������� �����, ��� ��� � ����� ��� ���-�� �����},
    FShowLastWh {�������� ��� ������������ ����� ��� ���}:boolean;
    CountCalcWh:integer;

begin
  FUseCalc := False;
  FCalcMaxWh := False;

{ TODO : �������� ������ �� ��������� ������ }
 if dm.dsUIDWH.DataSet<>dm.quUIDWH then raise Exception.Create('��� ��������� ������ ����� ����� ����� "�� ����. �����"');

 if dm.countmaxpross then sysUtils.Abort; // ���� ��������� ������������ ���������� �������

 dm.UIDWHType:=TMenuItem(Sender).Tag;
  case TComponent(Sender).Tag of
      2: begin // ����� �� ������������ ����
           FManyUses:=false; // ������ ������ ��������
           with dm, qutmp do
           begin
            //�������� ������� ��� ������� �������������
            Screen.Cursor:=crSQLWait;
            close;
            sql.Text:='select first 1 uid from uidwh_t where userid='+inttostr(dmcom.UserId);
            ExecQuery;
            Screen.Cursor:=crDefault;
            if not fields[0].IsNull then FCalcUser:=true else FCalcUser:=false;
            close;
            Transaction.CommitRetaining;
            //���� ������� �� ������������ ���, �� ����������� ���������� �������� ������� �� ������ � ��������� � ���� ���-���
            if not FCalcUser then
            begin
             //������������ ���������� �������� �� ������ ������
             Screen.Cursor:=crSQLWait;
             dm.quTmp.Close;
             dm.quTmp.SQL.Text:='select distinct u.userid from uidwh_t u, d_emp de '+
                       'where u.userid=de.d_empid';
             dm.quTmp.ExecQuery;
             while not dm.quTmp.Eof do
             begin
               dm.quTmp.Next;
             end;
             CountCalcWh:=dm.quTmp.RecordCount;
             dm.quTmp.Close;
             dm.quTmp.Transaction.CommitRetaining;
             Screen.Cursor:=crDefault;

         //    close;
         //    Transaction.CommitRetaining;
             if CountCalcWh<dm.MaxCountUIDWH then  begin
              FCalcMaxWh:=false;
              FThisCalcWH:=true;
              dm.quTmp.SQL.Clear;
              dm.quTmp.SQL.Add( 'update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId));
              dm.quTmp.ExecQuery;

             end else begin
              FCalcMaxWh:=true;
              MessageDialog('���������� ������������ ���������� ������� �� ������.'+
               #13#10+'��� ������� ���������� � ��������������.', mtInformation, [mbOk], 0);
             end
            end
            else if not FThisCalcWH then begin
             Screen.Cursor:=crSQLWait;

             sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
             ExecQuery;

             Screen.Cursor:=crDefault;
             if fields[0].IsNull then FUseCalc:=false
             else if fields[0].AsInteger=0 then FUseCalc:=false else FUseCalc:=true;
             close;
             Transaction.CommitRetaining;
             if FUseCalc then
             begin
              if not FNotRecalc then
              if MessageDialog('����� �� ������ ������������. �������� ��������. ������� ������������ ������?', mtInformation, [mbYes, mbNo], 0)=mrYes then
              begin
               FNotRecalc:=true;
               Screen.Cursor:=crSQLWait;

              // sql.Text:='select uidwhbd, uidwhed from d_emp where d_empid='+inttostr(dmcom.UserId);
              // ExecQuery;

               Screen.Cursor:=crDefault;
               if not fields[0].IsNull then dm.UIDWHBD:=fields[0].AsDateTime;
               if not fields[1].IsNull then dm.UIDWHED:=fields[1].AsDateTime;
               close;
               Transaction.CommitRetaining;
               //ReOpenDataSet(dm.quUIDWH);
               Reopen(dmcom.UserId);

               frmAnalizSklad.Caption:='����� - '+trim(dmcom.UserName);

               ShowPeriod;

               ExecSQL('update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

              end
             end
             else ExecSQL('update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

            end else begin
              Screen.Cursor:=crSQLWait;

              sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
              ExecQuery;

              Screen.Cursor:=crDefault;
              if fields[0].IsNull then FManyUses:=false
              else if fields[0].AsInteger<=1 then FManyUses:=false else FManyUses:=true;
              close;
              Transaction.CommitRetaining;
              if FManyUses then
               MessageDialog('����� ��� ������ ������������� ��� ���-�� ������.'+
                #13#10+'�������� ��������.', mtInformation, [mbOk], 0);
            end;

            FShowLastWh:=false;
            if (FCalcUser) and (not FThisCalcWH) and (not FUseCalc) and (not FManyUses) then
            begin
             Screen.Cursor:=crSQLWait;

             sql.Text:='select uidwhbd, uidwhed from d_emp where d_empid='+inttostr(dmcom.UserId);
             ExecQuery;

             Screen.Cursor:=crDefault;
             if not fields[0].IsNull then begin
               st:=st+'� '+fields[0].AsString;
               dm.UIDWHBD:=fields[0].AsDateTime;
             end;
             if not fields[1].IsNull then begin
              st:=st+' �� '+fields[1].AsString;
              dm.UIDWHED:=fields[1].AsDateTime;
             end;
             close;
             Transaction.CommitRetaining;
             if MessageDialog('����� ��������� ��� �������� ������������ '+st+'. ����������� ����� ��� �������?', mtWarning, [mbYes, mbNo], 0)=mrNo then
             begin
              FThisCalcWH:=true;
              FShowLastWh:=true;
              Reopen(dmcom.UserId);



              //ReOpenDataSet(dm.quUIDWH);
              frmAnalizSklad.Caption:='����� - '+trim(dmcom.UserName);

               ShowPeriod;

             end
             else FShowLastWh:=false;
            end;


            if (((not FCalcUser) and (not FCalcMaxWh)) or
                (FCalcUser and (not FThisCalcWH) and (not FUseCalc) and (not FShowLastWh)) or
                (FCalcUser and FThisCalcWH and (not FShowLastWh) and (not FManyUses))) then
             if ShowAndFreeForm(TfmSelectUidWh, Self, TForm(fmSelectUidWh), True, False)=mrOk then
             begin
             case dm.UIDWHType of
             1: begin  {**** ����� �� ������������ ������ ***}
                  FThisCalcWH:=true;
                  dm.UIDWHBD:=dmCOm.FirstMonthDate-1+0.9999;
                  dm.UIDWHED:=dm.UIDWHBD;

                  acRecalcUiDWHExecute(nil); //�������� ������


                end;
             2: begin {*** ����� �� ������ ������ ***}
                  if GetPeriod(dm.UIDWHBD, dm.UIDWHED) then
                  begin
                    FThisCalcWH:=true;

                    acRecalcUiDWHExecute(nil);

                    frmAnalizSklad.Caption:='����� - '+trim(dmcom.UserName);
                  end
                  else
                    if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
                end;
             4: begin {*** ����� �� ���� ***}
                  if Getdate(dm.UIDWHED) then
                  begin
                    FThisCalcWH:=true;
                    dm.UIDWHBD:=dm.UIDWHED;

                    acRecalcUiDWHExecute(nil);

                    frmAnalizSklad.Caption:='����� - '+trim(dmcom.UserName);
                  end
                  else
                    if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
               end;
            end
            end
            else if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
         end;
        end;
      3: with dm.dsUIDWH.DataSet{quUIDWH} do // ������� �����
           begin
             try
               if FThisCalcWH or FNotRecalc then
               begin

                ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

                FThisCalcWH:=false;
                FNotRecalc:=false;
               end;
               FNotRecalc:=false;
               dm.UIDWHBD:=dmCom.FirstMonthDate;
               dm.UIDWHED:= strtodatetime(datetostr(dmCom.GetServerTime))+0.9999;
               Screen.Cursor:=crSQLWait;
               Active:=False;
               if dmCom.tr1.Active then dmCom.tr1.Commit;
               if not dmCom.tr1.Active then dmCom.tr1.StartTransaction;
               Application.ProcessMessages;

               CheckCalcing;

               Open;

               ShowPeriod;

               frmAnalizSklad.Caption:='������ ������ ��-��������';
             finally
               Reopen(dmcom.UserId);


               Screen.Cursor:=crDefault;
             end;
           end;
    end;

    //����� ������ �� ������������� �������

end;

procedure TfrmAnalizSklad.ShowPeriod;
begin
  case dm.UIDWHType of
      1: laPeriod.Caption:='������ ������';
      2: laPeriod.Caption:='� '+DateToStr(dm.UIDWHBD) + ' �� ' +DateToStr(dm.UIDWHED);
      3: with dm, quTmp do
           begin
             SQL.Text:='SELECT UIDWHDATE FROM D_REC';
             ExecQuery;
             laPeriod.Caption:='����� �� '+DateTimeToStr(quTmp.Fields[0].AsDateTime);
             Close;
           end;
      4: laPeriod.Caption:='����� �� ���� '+DateToStr(dm.UIDWHED);
    end;
end;

procedure TfrmAnalizSklad.PrepareData;  // ���������� ������
var i: integer;
 s:integer;
begin
  Application.Minimize;
  with dmCom, dm do
    begin
      Application.ProcessMessages;
      with quTmp do
      begin
        if UIDWHType=3 then
          begin
            SQL.Text:='EXECUTE PROCEDURE StartUIDWHCalc';
            ExecQuery;
            tr.CommitRetaining;
            ExecSQL('EXECUTE PROCEDURE CreateApplDep_Recalc ('+IntToStr(UserDefault)+')', qutmp); //�������� ������ �� ������� � �����������
            SQL.Text:='DELETE FROM UIDWH_T WHERE USERID='+IntToStr(UserDefault);
          end
        else
          begin
            ExecSQL('update d_rec set calcwhdate=calcwhdate+1',qutmp);
            ExecSQL('EXECUTE PROCEDURE CreateApplDep_Recalc ('+IntToStr(UserId)+')', qutmp);
            SQL.Text:='DELETE FROM UIDWH_T WHERE USERID='+IntToStr(UserId);
          end;
      end;
      UIDWHTime:=dmCom.GetServerTime;

      quTmp.ExecQuery;
      tr1.CommitRetaining;
      tr.CommitRetaining;
     

      Application.ProcessMessages;

     for i:=1 to 9 do
        begin
          if (i = 5) and (UIDWHType=2)  then
             GetSell  // ����� ��������� �������
          else
            with quTmp do
            begin
                  if CenterDep then
                  SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+' ?BD, ?ED, ?USERID'
                  else
                  SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+'_dep ?BD, ?ED, ?USERID';
                  Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
                  Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
     //             showmessage(Params[0].AsString + ' , ' + Params[1].AsString);
                  if UIDWHType=3 then
                     Params[2].AsInteger:=UserDefault
                     else Params[2].AsInteger:=UserId;
                     ExecQuery;
                  tr.CommitRetaining;
             end;
             Application.ProcessMessages;
        end;

        if CenterDep then
        begin
          with quTmp do
          begin
            SQL.Text:='EXECUTE PROCEDURE UIDWH_S_END ?BD, ?ED, ?USERID';
            Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
            Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);

            if UIDWHType=3 then Params[2].AsInteger:=UserDefault
            else Params[2].AsInteger:=UserId;
            ExecQuery;
            tr.CommitRetaining;
            tr1.CommitRetaining;
          end;
        end;
        

      with quTmp do
        begin
          if UIDWHType=3 then
            begin
              SQL.Text:='EXECUTE PROCEDURE EndUIDWHCalc '+IntToStr(UserId);
            end
          else
            begin
              SQL.Text:='UPDATE D_EMP SET UIDWHBD=?BD, UIDWHED=?ED, UIDWHDATE=''TODAY''  WHERE D_EMPID=?USERID';
              Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
              Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
              Params[2].AsInteger:=UserId;
            end;

          ExecQuery;
          tr.CommitRetaining;
          tr1.CommitRetaining;

         
        end;
        if UIDWHType<>3 then ExecSQL('update d_rec set calcwhdate=calcwhdate-1',qutmp);
       with dm.NoCloseRetAct do
       begin
         begin
           ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(UIDWHBD);
           ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(UIDWHED);
           SetDepFilter(NoCloseRetAct.Params, SDepId);
         end;
       end;
    end;
 Application.Restore;
end;

procedure TfrmAnalizSklad.acRealCostExecute(Sender: TObject);
begin
 if NRealCost.Checked then
 begin
   if dm.countmaxpross then begin
    NRealCost.Checked:=false;
    sysUtils.Abort;
   end;

  if dm.UIDWHType=3 then
   ExecSQL('execute procedure CALC_REAL_COST ('+inttostr(UserDefault)+');', dm.quTmp)
  else
   ExecSQL('execute procedure CALC_REAL_COST ('+inttostr(dmcom.UserId)+');', dm.quTmp);
  if dm.dsUIDWH.DataSet=dm.quUIDWH then ReOpenDataSet(dm.quUIDWH)
  else ReOpenDataSet(dm.quMaxUIDWH);

 end
end;




procedure TfrmAnalizSklad.acRecalcUiDWHExecute(Sender: TObject);
var LogOperationID:string;
    FManyUses:boolean;
begin
  { TODO : �������� ��������� ������ }
  try
   if not dm.countmaxpross then // ���� �� �������� ����������� ���������� ���-�� ��������
   begin
    {�������� ��� ��������� �� ������, ��� �� ����� ������������ ����� ������ ����� �� ������}
    if FThisCalcWH then // ���� ����� �� ������ ��� ��� ������ �� �����
    begin
     dm.quTmp.Close;
     dm.quTmp.sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
     dm.quTmp.ExecQuery;
     if dm.quTmp.fields[0].IsNull then FManyUses:=false
     else if dm.quTmp.fields[0].AsInteger<=1 then FManyUses:=false else FManyUses:=true;
     dm.quTmp.close;
     dm.quTmp.Transaction.CommitRetaining;
     if FManyUses then
      MessageDialog('����� ��� ������ ������������� ��� ���-�� ������.'+
                 #13#10+'�������� ��������.', mtInformation, [mbOk], 0);
    end else FManyUses:=false;
    {****************************************************************************************}
    if (not FManyUses) then
    begin
     Application.Minimize;
     Screen.Cursor:=crSQLWait;
     Application.ProcessMessages;
     //dm.SetVisEnabled(plTotal,false);
     with dm do
     begin
        dm.quUIDWH.Active:=False;
        LogOperationID:=dm3.insert_operation('�������� ������',LogOprIdForm);
        PrepareData; // ���������� ������
        acRealCostExecute(nil);
        {*�������������� ������� �������*
         UIDWHSZ:='';
         dmcom.UIDWHSZ_:='*';
         chlbSz.Items.Assign(dm.slSz);
         chlbSZ.ItemIndex := GetIdxByCode(dm.slSZ, dmcom.UidwhSZ_);
         chlbSZ.Checked[chlbSZ.ItemIndex]:=true;
         chlbSZClickCheck(chlbSZ);
         ReOpenDataSet(dm.dsUIDWH.DataSet);
        ********************************}
        dm3.update_operation(LogOperationID);
      if Not CenterDep then
        with NoCloseRetAct do
          begin
              ExecQuery;
              if (Fields[0].asInteger<>0) then
                MessageDialog('������� ' + IntToStr(Fields[0].asInteger) + ' �� �������� ���� �������� �� �����������', mtWarning, [mbOk], 0);
              Close;
          end;

     end;
     with dmCom do
      begin
        if tr1.Active then tr1.COmmit;
        if not tr1.Active then tr1.StartTransaction;
      end;
     dm.quUIDWH.Active:=True;
     dm.quUIDWH.Transaction.CommitRetaining;
     dmCom.tr.CommitRetaining;
     ShowPeriod; // ������� ������� ��� ������� ������
    end;
   end
  finally
               Screen.Cursor:=crSQLWait;
               DBData.Close;
               DBData.ParamByName('UserID').AsInteger := dmcom.UserId;
               DBData.Open;


               Screen.Cursor:=crDefault;
               Application.Restore;
  end;

end;

{******************************************************************************}
procedure TfrmAnalizSklad.FormActivate(Sender: TObject);
begin
with dm do
    begin
      dm.ReopenUidWH:=false;
      UIDWHTime:=dmCom.GetServerTime;
      ShowPeriod;  // ������� ������� ��� ������� ������
      if NOT dsUIDWH.DataSet{quUIDWH}.Active then
        if UIDWHType=3 then
          begin
            //ClickUserDepMenu(dm.pmWH);
          end;

    if Not CenterDep then
    begin

      with NoCloseRetAct do
         begin
           Params.ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(UIDWHBD);
           Params.ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(UIDWHED);
           SetDepFilter(NoCloseRetAct.Params, SDepId);

         end;



    end;
    end ;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
  

//  acBadPrice.Visible:= AppDebug or ((not AppDebug) and dmcom.Adm);
//  acTest.Visible:= acRepeat.Visible or acBadPrice.Visible;
//  acEditUidwh.Enabled:=CenterDep;
end;

end.
