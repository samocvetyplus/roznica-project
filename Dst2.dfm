object fmDst2: TfmDst2
  Left = 186
  Top = 263
  HelpContext = 100225
  Caption = 
    #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1085#1072' '#1085#1077#1089#1082#1086#1083#1100#1082#1086' '#1089#1082#1083#1072#1076#1086#1074' ('#1087#1086' '#1087#1077#1088#1074#1099#1084' '#1072#1088#1090#1080#1082#1091#1083#1072#1084 +
    ')'
  ClientHeight = 684
  ClientWidth = 940
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 665
    Width = 940
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 940
    Height = 46
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 40
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 8
      Top = 4
      Width = 61
      Height = 13
      Caption = 'laDepFrom'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 216
      Top = 4
      Width = 31
      Height = 13
      Caption = #1055#1086#1089#1090'.:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 216
      Top = 16
      Width = 44
      Height = 13
      Caption = #1055#1088' '#1094#1077#1085#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 216
      Top = 28
      Width = 42
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object laSup: TLabel
      Left = 264
      Top = 4
      Width = 19
      Height = 13
      Caption = #1042#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object laPrice: TLabel
      Left = 264
      Top = 16
      Width = 19
      Height = 13
      Caption = #1042#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object laSZ: TLabel
      Left = 264
      Top = 28
      Width = 19
      Height = 13
      Caption = #1042#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 388
      Top = 4
      Width = 57
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083': '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object DBText1: TDBText
      Left = 388
      Top = 20
      Width = 50
      Height = 13
      AutoSize = True
      DataField = 'FULLART'
      DataSource = dm2.dsD_WH2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 8
      Top = 16
      Width = 68
      Height = 13
      Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laInv: TLabel
      Left = 12
      Top = 28
      Width = 29
      Height = 13
      Caption = 'laInv'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laFullArt: TLabel
      Left = 396
      Top = 19
      Width = 5
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 619
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1042#1089#1077#1075#1086
      Caption = #1042#1089#1077#1075#1086
      Hint = #1042#1089#1077#1075#1086
      ImageIndex = 63
      Spacing = 1
      Left = 555
      Top = 3
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 699
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object pc1: TPageControl
    Left = 0
    Top = 46
    Width = 940
    Height = 619
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 2
    OnChange = pc1Change
    object TabSheet1: TTabSheet
      Caption = #1042#1099#1073#1086#1088' '#1072#1088#1090#1080#1082#1091#1083#1072
      object Splitter7: TSplitter
        Left = 413
        Top = 0
        Height = 591
        ExplicitHeight = 596
      end
      object plFilter: TPanel
        Left = 0
        Top = 0
        Width = 413
        Height = 591
        Align = alLeft
        TabOrder = 0
        object Splitter3: TSplitter
          Left = 65
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object Splitter4: TSplitter
          Left = 197
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object Splitter5: TSplitter
          Left = 345
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object Splitter6: TSplitter
          Left = 133
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object Splitter11: TSplitter
          Left = 253
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object Splitter10: TSplitter
          Left = 301
          Top = 1
          Height = 589
          ExplicitHeight = 594
        end
        object lbComp: TListBox
          Left = 1
          Top = 1
          Width = 64
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 0
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbMat: TListBox
          Left = 136
          Top = 1
          Width = 61
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 1
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbGood: TListBox
          Left = 200
          Top = 1
          Width = 53
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 2
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbIns: TListBox
          Left = 256
          Top = 1
          Width = 45
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 3
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbCountry: TListBox
          Left = 68
          Top = 1
          Width = 65
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 4
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbAtt1: TListBox
          Left = 304
          Top = 1
          Width = 41
          Height = 589
          Align = alLeft
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 5
          Visible = False
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
        object lbAtt2: TListBox
          Left = 348
          Top = 1
          Width = 64
          Height = 589
          Align = alClient
          Color = clBtnFace
          Constraints.MinWidth = 24
          ItemHeight = 13
          TabOrder = 6
          Visible = False
          OnClick = lbCompClick
          OnKeyPress = lbCompKeyPress
        end
      end
      object plData: TPanel
        Left = 416
        Top = 0
        Width = 516
        Height = 591
        Align = alClient
        TabOrder = 1
        object tb2: TSpeedBar
          Left = 1
          Top = 1
          Width = 514
          Height = 33
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          BoundLines = [blTop, blBottom, blLeft, blRight]
          Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
          BtnOffsetHorz = 3
          BtnOffsetVert = 3
          BtnWidth = 70
          BtnHeight = 27
          Images = dmCom.ilButtons
          TabOrder = 0
          InternalVer = 1
          object Label10: TLabel
            Left = 344
            Top = 10
            Width = 52
            Height = 13
            Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object Label9: TLabel
            Left = 112
            Top = 11
            Width = 32
            Height = 13
            Caption = #1055#1086#1080#1089#1082
            Transparent = True
          end
          object edUID: TEdit
            Left = 400
            Top = 6
            Width = 57
            Height = 21
            Color = clInfoBk
            TabOrder = 0
            OnKeyDown = edUIDKeyDown
          end
          object ceArt: TComboEdit
            Left = 152
            Top = 6
            Width = 93
            Height = 21
            ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
            Color = clInfoBk
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              04000000000080000000120B0000120B00001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
              DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
              9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
              9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
              90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
            NumGlyphs = 1
            TabOrder = 1
            OnButtonClick = ceArtButtonClick
            OnChange = ceArtChange
            OnKeyDown = ceArtKeyDown
            OnKeyUp = ceArtKeyUp
          end
          object cbSearch: TCheckBox
            Left = 96
            Top = 11
            Width = 12
            Height = 12
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = cbSearchClick
          end
          object SpeedbarSection2: TSpeedbarSection
            Caption = 'Untitled (0)'
          end
          object SpeedItem1: TSpeedItem
            BtnCaption = #1054#1090#1082#1088#1099#1090#1100
            Caption = #1054#1090#1082#1088#1099#1090#1100
            Hint = #1054#1090#1082#1088#1099#1090#1100
            ImageIndex = 74
            Layout = blGlyphLeft
            Spacing = 1
            Left = 3
            Top = 3
            Visible = True
            OnClick = SpeedItem1Click
            SectionName = 'Untitled (0)'
          end
          object siDTo: TSpeedItem
            BtnCaption = #1057#1082#1083#1072#1076
            Caption = #1057#1082#1083#1072#1076
            DropDownMenu = pmDTo
            Hint = #1057#1082#1083#1072#1076
            ImageIndex = 3
            Layout = blGlyphLeft
            Spacing = 1
            Left = 263
            Top = 3
            Visible = True
            SectionName = 'Untitled (0)'
          end
        end
        object plWh: TPanel
          Left = 1
          Top = 542
          Width = 514
          Height = 48
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 1
          Visible = False
          object lbWHInfo: TLabel
            Left = 5
            Top = 1
            Width = 67
            Height = 13
            Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object txtArt: TDBText
            Left = 80
            Top = 2
            Width = 24
            Height = 13
            AutoSize = True
            DataField = 'TA'
            DataSource = dm2.dsD_WH_T
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object txtQ: TDBText
            Left = 80
            Top = 16
            Width = 19
            Height = 13
            AutoSize = True
            DataField = 'TQ'
            DataSource = dm2.dsD_WH_T
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label7: TLabel
            Left = 5
            Top = 15
            Width = 34
            Height = 13
            Caption = #1050#1086#1083'-'#1074#1086
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object lbW: TLabel
            Left = 5
            Top = 29
            Width = 19
            Height = 13
            Caption = #1042#1077#1089
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object txtW: TDBText
            Left = 80
            Top = 30
            Width = 22
            Height = 13
            AutoSize = True
            DataField = 'TW'
            DataSource = dm2.dsD_WH_T
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
        object dgWH: TDBGridEh
          Left = 1
          Top = 34
          Width = 514
          Height = 508
          Align = alClient
          AllowedOperations = []
          Color = clBtnFace
          ColumnDefValues.Title.TitleButton = True
          DataGrouping.GroupLevels = <>
          DataSource = dm2.dsD_WH2
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 2
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
          ParentFont = False
          PopupMenu = pm2
          RowDetailPanel.Color = clBtnFace
          SortLocal = True
          SumList.Active = True
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clNavy
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          UseMultiTitle = True
          OnDblClick = dgWHDblClick
          OnGetCellParams = dgWHGetCellParams
          OnKeyDown = dgWHKeyDown
          Columns = <
            item
              EditButtons = <>
              FieldName = 'PRODCODE'
              Footers = <>
              Title.Caption = #1048#1079#1075'.'
              Title.EndEllipsis = True
              Width = 44
            end
            item
              EditButtons = <>
              FieldName = 'D_MATID'
              Footers = <>
              Title.Caption = #1052#1072#1090'.'
              Title.EndEllipsis = True
              Width = 50
            end
            item
              EditButtons = <>
              FieldName = 'D_GOODID'
              Footers = <>
              Title.Caption = #1053#1072#1080#1084'.'
              Title.EndEllipsis = True
              Width = 41
            end
            item
              EditButtons = <>
              FieldName = 'D_INSID'
              Footers = <>
              Title.Caption = #1054#1042
              Title.EndEllipsis = True
              Width = 55
            end
            item
              EditButtons = <>
              FieldName = 'D_COUNTRYID'
              Footers = <>
              Title.EndEllipsis = True
              Width = 38
            end
            item
              EditButtons = <>
              FieldName = 'ART'
              Footers = <
                item
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Value = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
                  ValueType = fvtStaticText
                end
                item
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Value = #1042#1089#1077#1075#1086
                  ValueType = fvtStaticText
                end>
              Title.Caption = #1040#1088#1090#1080#1082#1091#1083
              Title.EndEllipsis = True
              Width = 111
            end
            item
              EditButtons = <>
              FieldName = 'QUANTITY'
              Footers = <
                item
                  FieldName = 'QUANTITY'
                  ValueType = fvtCount
                end
                item
                  FieldName = 'QUANTITY'
                  ValueType = fvtSum
                end>
              Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1089#1077#1075#1086'|'#1050#1086#1083'-'#1074#1086
              Title.EndEllipsis = True
              Width = 77
            end
            item
              EditButtons = <>
              FieldName = 'WEIGHT'
              Footers = <
                item
                end
                item
                  FieldName = 'WEIGHT'
                  ValueType = fvtSum
                end>
              Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1089#1077#1075#1086'|'#1042#1077#1089
              Title.EndEllipsis = True
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077
      ImageIndex = 1
      object Splitter2: TSplitter
        Left = 325
        Top = 0
        Height = 591
        ExplicitHeight = 598
      end
      object pa1: TPanel
        Left = 0
        Top = 0
        Width = 325
        Height = 591
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'pa1'
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 0
          Top = 495
          Width = 325
          Height = 3
          Cursor = crVSplit
          Align = alBottom
          AutoSnap = False
          MinSize = 20
          ExplicitTop = 502
        end
        object Panel1: TPanel
          Left = 0
          Top = 476
          Width = 325
          Height = 19
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 0
          object laQC: TLabel
            Left = 4
            Top = 2
            Width = 25
            Height = 13
            Caption = #1050'-'#1074#1086':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dtQT: TDBText
            Left = 32
            Top = 2
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Color = clWhite
            DataField = 'QUANTITY'
            DataSource = dm2.dsD_T2
            ParentColor = False
          end
          object laWC: TLabel
            Left = 72
            Top = 2
            Width = 22
            Height = 13
            Caption = #1042#1077#1089':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dtWT: TDBText
            Left = 96
            Top = 2
            Width = 37
            Height = 13
            Alignment = taRightJustify
            Color = clWhite
            DataField = 'WEIGHT'
            DataSource = dm2.dsD_T2
            ParentColor = False
          end
          object Label2: TLabel
            Left = 136
            Top = 2
            Width = 25
            Height = 13
            Caption = #1050'-'#1074#1086':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dtQDT: TDBText
            Left = 161
            Top = 2
            Width = 32
            Height = 13
            Alignment = taRightJustify
            AutoSize = True
            DataField = 'DQ'
            DataSource = dm2.dsD_T2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label3: TLabel
            Left = 196
            Top = 2
            Width = 22
            Height = 13
            Caption = #1042#1077#1089':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object dtWDT: TDBText
            Left = 218
            Top = 2
            Width = 35
            Height = 13
            Alignment = taRightJustify
            AutoSize = True
            DataField = 'DW'
            DataSource = dm2.dsD_T2
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
        end
        object dg3: TM207IBGrid
          Left = 0
          Top = 498
          Width = 325
          Height = 93
          TabStop = False
          Align = alBottom
          Color = clBtnFace
          DataSource = dm2.dsD_Q2
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnExit = dg3Exit
          IniStorage = FormStorage1
          MultiShortCut = 0
          ColorShortCut = 0
          InfoShortCut = 0
          ClearHighlight = True
          SortOnTitleClick = False
          Columns = <
            item
              Color = clInfoBk
              Expanded = False
              FieldName = 'DEP'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1057#1082#1083#1072#1076
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Q1'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1050#1086#1083'-'#1074#1086
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'W1'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1042#1077#1089
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 34
              Visible = True
            end
            item
              Color = clAqua
              Expanded = False
              FieldName = 'Q2'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1050#1086#1083'-'#1074#1086
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 46
              Visible = True
            end
            item
              Color = clAqua
              Expanded = False
              FieldName = 'W2'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1042#1077#1089
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 30
              Visible = True
            end>
        end
        object dge2: TDBGridEh
          Left = 0
          Top = 0
          Width = 325
          Height = 476
          Align = alClient
          AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
          AllowedSelections = [gstRecordBookmarks]
          Color = clBtnFace
          ColumnDefValues.Title.TitleButton = True
          DataGrouping.GroupLevels = <>
          DataSource = dm2.dsD_UID2
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          OptionsEh = [dghFixed3D, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
          PopupMenu = pm2
          RowDetailPanel.Color = clBtnFace
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnGetCellParams = dge2GetCellParams
          OnKeyDown = dge2KeyDown
          Columns = <
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'UID'
              Footers = <>
              Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'W'
              Footers = <>
              Title.Caption = #1042#1077#1089
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'SUP'
              Footers = <>
              Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end
            item
              Color = clAqua
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'PRICE'
              Footers = <>
              Title.Caption = #1055#1088'.'#1094#1077#1085#1072
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'ART2'
              Footers = <>
              Title.Caption = #1040#1088#1090'2'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'NDS'
              Footers = <>
              Title.Caption = #1053#1044#1057
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 84
            end
            item
              Color = clInfoBk
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'SZ'
              Footers = <>
              Title.Caption = #1056#1072#1079#1084'.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object paB: TPanel
        Left = 328
        Top = 0
        Width = 77
        Height = 591
        Align = alLeft
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnResize = paBResize
      end
      object dg1: TM207IBGrid
        Left = 405
        Top = 0
        Width = 527
        Height = 591
        TabStop = False
        Align = alClient
        Color = clBtnFace
        DataSource = dm2.dsDst2
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dg1GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SaveOrder = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'DEP'
            Title.Alignment = taCenter
            Title.Caption = #1057#1082#1083#1072#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 66
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'.'#8470
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 45
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 33
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090'. 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 61
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NDS'
            Title.Alignment = taCenter
            Title.Caption = #1053#1044#1057
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 42
            Visible = True
          end>
      end
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'dg1.Width'
      'pa1.Width'
      'paB.Width'
      'dg3.Height'
      'cbSearch.Checked'
      'plFilter.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 364
    Top = 148
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 562
    Top = 150
    object N1: TMenuItem
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ShortCut = 114
      OnClick = ShowSZ
    end
    object miSupFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1087#1086#1089#1090#1072#1097#1080#1082#1091
      ShortCut = 16500
      OnClick = SetFilter
    end
    object miPriceFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1094#1077#1085#1077
      ShortCut = 16501
      OnClick = SetFilter
    end
    object miSZFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1088#1072#1079#1084#1077#1088#1091
      ShortCut = 16502
      OnClick = SetFilter
    end
  end
  object pmDTo: TPopupMenu
    Left = 468
    Top = 150
  end
end
