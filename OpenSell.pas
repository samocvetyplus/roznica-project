unit OpenSell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, RXSpin;

type
  TfmOpenSell = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    la1: TLabel;
    se1: TRxSpinEdit;
    Label2: TLabel;
    LCurUser: TLabel;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOpenSell: TfmOpenSell;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmOpenSell.FormActivate(Sender: TObject);
begin
  with dm, dmCom do
    begin
      if UseFReg then
        begin
          se1.Color:=clBtnFace;
          se1.Enabled:=False;
        end
      else
        begin
          se1.Color:=clInfoBk;
          se1.Enabled:=True;
        end;
//      quEmp.First;
//      lc1.KeyValue:=quEmpD_EMPID.Value;
      LCurUser.Caption:=dmcom.User.FIO;
//      SetCBDropDown(lc1);
    end;
end;

end.
