unit TmpDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Menus, ComCtrls, IniFiles, RXStrUtils;

type
  TfmTmpDir = class(TForm)
    PButton: TPanel;
    btok: TBitBtn;
    btcancel: TBitBtn;
    MSettings: TMainMenu;
    PC: TPageControl;
    tsell: TTabSheet;
    tscan: TTabSheet;
    tTmp: TTabSheet;
    cbShowNumEmp: TCheckBox;
    cbShowInfClient: TCheckBox;
    Label5: TLabel;
    Label3: TLabel;
    cbComScan: TComboBox;
    cbComScan2: TComboBox;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    NHelp: TMenuItem;
    cbWithOutActiveForm: TCheckBox;
    cbPrintCheck: TCheckBox;
    rgPrint: TRadioGroup;
    Shape1: TShape;
    cbFreg: TCheckBox;
    cbcomfreg: TComboBox;
    Label2: TLabel;
    tbTag: TTabSheet;
    chbTagOld: TCheckBox;
    chbTagNew: TCheckBox;
    chbTagBlack: TCheckBox;
    chbTagCode: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTmpDir: TfmTmpDir;

implementation

uses comdata, Data, M207Proc, uUtils;

{$R *.DFM}

procedure TfmTmpDir.FormCreate(Sender: TObject);
var i:integer;
begin
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      OpenDataSets([taRec]);
      rgPrint.ItemIndex:=printtype;
      cbShowNumEmp.Checked:=ShowNumEmp;
      cbShowInfClient.Checked:=ShowInfClient;
//      cbComScan.ItemIndex:=cbComScan.Items.IndexOf(ScanComZ);
//      cbComScan2.ItemIndex:=cbComScan.Items.IndexOf(ScanComM);
      tscan.TabVisible:=False; //************
      cbWithOutActiveForm.Checked:=IsActive;
      cbPrintCheck.Checked:=IsPrintCheck;
      cbFreg.Checked:= FReg_;
      cbcomfreg.ItemIndex:=cbcomfreg.Items.IndexOf(FregCom);

      chbTagNew.Checked:=false;
      chbTagOld.Checked:=false;
      chbTagBlack.Checked:=false;
      chbTagCode.Checked:=false;

      i:=1;
      while ExtractWord(i, dmcom.TypeTag, [';'])<>'' do
      begin
       case strtoint(ExtractWord(i, dmcom.TypeTag, [';'])) of
        0: chbTagNew.Checked:=true;
        1: chbTagOld.Checked:=true;
        2: chbTagBlack.Checked:=true;
        3: chbTagCode.Checked:=true;
       end;
       inc(i);
      end;

    end;
end;

procedure TfmTmpDir.FormClose(Sender: TObject; var Action: TCloseAction);
var IniF: TIniFile;
begin
  with dmCom, dm do
    begin
      if ModalResult=mrOK then
        begin
          PostDataSets([taRec]);
          TmpDir:=taRecTmpDir.AsString;
          TmpDB:=taRecTmpDB.AsString;
          ShowNumEmp:=cbShowNumEmp.Checked;
          ShowInfClient:=cbShowInfClient.Checked;
//          ScanComZ:=cbComScan.Items[cbComScan.ItemIndex];
//          ScanComM:=cbComScan.Items[cbComScan2.ItemIndex];
          IsActive:=cbWithOutActiveForm.Checked;
          IsPrintCheck:=cbPrintCheck.Checked;
          printtype:=rgPrint.ItemIndex;
          FReg_:=cbFreg.Checked;
          FregCom:=cbcomfreg.Items[cbcomfreg.ItemIndex];

          dmcom.TypeTag:='';
          if chbTagOld.Checked then dmcom.TypeTag:=dmcom.TypeTag+inttostr(chbTagOld.Tag)+';';
          if chbTagNew.Checked then dmcom.TypeTag:=dmcom.TypeTag+inttostr(chbTagNew.Tag)+';';
          if chbTagBlack.Checked then dmcom.TypeTag:=dmcom.TypeTag+inttostr(chbTagBlack.Tag)+';';
          if chbTagCode.Checked then dmcom.TypeTag:=dmcom.TypeTag+inttostr(chbTagCode.Tag)+';';
          if dmcom.TypeTag='' then dmcom.TypeTag:='3;';

          {������ � ini ����}
          IniF := TIniFile.Create(GetIniFileName);

          if SHOWNUMEMP then IniF.WriteString('SELL','SHOWNUMEMP','1')
          else IniF.WriteString('SELL','SHOWNUMEMP','0');

          if ShowInfClient then IniF.WriteString('SELL','SHOWINFCLIENT','1')
          else IniF.WriteString('SELL','SHOWINFCLIENT','0');

          if IsPrintCheck then IniF.WriteString('SELL','ISPRINTCHECK','1')
          else IniF.WriteString('SELL','ISPRINTCHECK','0');

          IniF.WriteString('SELL','PRINTTYPE',inttostr(printtype));

          if FReg_ then IniF.WriteString('SELL','FREG','1')
          else IniF.WriteString('SELL','FREG','0');

          IniF.WriteString('SELL','FREGCOM',FregCom);

     //     IniF.WriteString('SCAN','COMPORT_1',ScanComZ);
     //     IniF.WriteString('SCAN','COMPORT_2',ScanComM);

          if IsActive then IniF.WriteString('SCAN','ISACTIVE','1')
          else IniF.WriteString('SCAN','ISACTIVE','0');

          IniF.WriteString('TAGTYPE','TAGTYPE',dmcom.TypeTag);
                    
          IniF.Free;
        end
      else  OpenDataSets([taRec]);
      tr.CommitRetaining;
    end;
end;

end.
