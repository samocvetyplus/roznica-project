unit AMerge;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmAMerge = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter1: TSplitter;
    siOpen: TSpeedItem;
    Panel1: TPanel;
    Panel2: TPanel;
    dgArt: TM207IBGrid;
    Splitter2: TSplitter;
    dgA2: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure siOpenClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmAMerge: TfmAMerge;

implementation

uses comdata, Data, Data2, DBTree, M207Proc;

{$R *.DFM}

procedure TfmAMerge.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmAMerge.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm , dm2 do
    begin
      if not tr.Active then tr.StartTransaction;
      Old_D_MatId:='.';
    end;
end;

procedure TfmAMerge.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm , dm2 do
    begin
      tr.CommitRetaining;
    end;
end;

procedure TfmAMerge.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmAMerge.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmAMerge.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
end;

procedure TfmAMerge.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmAMerge.siOpenClick(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quArt]);
         Screen.Cursor:=crDefault;
       end;
end;

end.
