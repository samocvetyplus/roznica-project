unit ORetList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, db, Menus, RxMenus, DBCtrls,
  DBGridEh, MsgDialog, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmORetList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    laDepFrom: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    miAll: TMenuItem;
    SpeedItem1: TSpeedItem;
    spitPrint: TSpeedItem;
    ppPrint: TRxPopupMenu;
    mnitFacture: TMenuItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    dg1: TDBGridEh;
    siHelp: TSpeedItem;
    Label1: TLabel;
    SpeedItem2: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure siEditClick(Sender: TObject);
    procedure mnitFactureClick(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
    LogOprIdForm:string;    
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  fmORetList: TfmORetList;

implementation

uses comdata, Data, SInv, DInv, Opt, //OptDistr,
     ReportData, Period, SRet, ORet, M207Proc, Data3, JewConst, dbUtil;

{$R *.DFM}

procedure TfmORetList.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmORetList.FormCreate(Sender: TObject);
begin
  laDepFrom.Caption:='';
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  with dmCom do
    begin
      tr.Active:=True;
      taRec.Active:=True;
    end;
  dm.WorkMode:='ORET';
  dm.SetBeginDate;
  ShowPeriod;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);  
end;

procedure TfmORetList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      CloseDataSets([dm.taORetList, taRec]);
      tr.CommitRetaining;
    end;
  dm.WorkMode:='';
end;

procedure TfmORetList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmORetList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmORetList.siAddClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_AddORetLst,LogOprIdForm);
  dm.SDepID := 108;
  dm.taORetList.Append;
  dm.taORetList.Post;
  ShowAndFreeForm(TfmORet, Self, TForm(fmORet), True, False);
  dm3.update_operation(LogOperationID)
end;

procedure TfmORetList.siDelClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_DelORetLst,LogOprIdForm);
  dm.taORetList.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmORetList.FormActivate(Sender: TObject);
begin
  dm.ClickUserDepMenu(dm.pmORetList);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmORetList.siEditClick(Sender: TObject);
var LogOperationID:string;
begin
  if dm.taORetListIType.AsInteger=8 then
  begin
   dm.taORetList.Refresh;
   if dm.taORetListSINVID.IsNull then begin
    MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
    ReOpenDataSets([dm.taORetList]);
   end else begin
     LogOperationID:=dm3.insert_operation(sLog_ViewORetLst,LogOprIdForm);
     ShowAndFreeForm(TfmORet, Self, TForm(fmORet), True, False);
     dm3.update_operation(LogOperationID);
   end
  end
end;

procedure TfmORetList.mnitFactureClick(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_PrintORetLst,LogOprIdForm);
  arr:=gen_arr( dg1, dm.taORetListSINVID);
  try
    PrintDocument(arr, facture_r_opt);
  //  PrintDocument(arr, invoice_r_opt);
  finally
    Finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmORetList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taORetList]);
      ShowPeriod;
    end;
end;

procedure TfmORetList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;


procedure TfmORetList.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if dm.taORetListIType.AsInteger<>8 then Background:=clTeal
  else if dm.taORetListIsClosed.AsInteger=0 then Background:=clInfoBk
     else Background:=clBtnFace;
end;

procedure TfmORetList.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_SPACE, VK_RETURN: siEditClick(Sender);
    end;
end;

procedure TfmORetList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100261)
end;

procedure TfmORetList.SpeedItem2Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taORetList);
  tr.CommitRetaining;
  ReOpenDataSet(taORetList);
  taORetList.Refresh;
  end;
end;

end.
