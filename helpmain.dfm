object fmHelpMain: TfmHelpMain
  Left = 259
  Top = 82
  BorderStyle = bsNone
  Caption = 'fmHelpMain'
  ClientHeight = 570
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 368
    Top = 8
    Width = 30
    Height = 32
    Caption = 'F2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 16
    Top = 8
    Width = 148
    Height = 32
    Caption = #1055#1054#1057#1058#1040#1042#1050#1048
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 48
    Width = 78
    Height = 32
    Caption = #1062#1045#1053#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 368
    Top = 48
    Width = 30
    Height = 32
    Caption = 'F3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 88
    Width = 306
    Height = 32
    Caption = #1057#1050#1051#1040#1044' '#1055#1054' '#1040#1056#1058#1048#1050#1059#1051#1040#1052
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 16
    Top = 128
    Width = 298
    Height = 32
    Caption = #1057#1050#1051#1040#1044' '#1055#1054' '#1048#1047#1044#1045#1051#1048#1071#1052
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 16
    Top = 168
    Width = 334
    Height = 32
    Caption = #1042#1053#1059#1058#1056'. '#1055#1045#1056#1045#1052#1045#1065#1045#1053#1048#1071
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 16
    Top = 208
    Width = 138
    Height = 32
    Caption = #1055#1056#1054#1044#1040#1046#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 16
    Top = 248
    Width = 100
    Height = 32
    Caption = #1057#1052#1045#1053#1040
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 368
    Top = 88
    Width = 30
    Height = 32
    Caption = 'F4'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 368
    Top = 128
    Width = 30
    Height = 32
    Caption = 'F5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 368
    Top = 168
    Width = 30
    Height = 32
    Caption = 'F6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 369
    Top = 202
    Width = 30
    Height = 32
    Caption = 'F7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 368
    Top = 240
    Width = 30
    Height = 32
    Caption = 'F8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 16
    Top = 528
    Width = 94
    Height = 32
    Caption = #1042#1067#1061#1054#1044
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel
    Left = 368
    Top = 528
    Width = 54
    Height = 32
    Caption = 'ESC'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 16
    Top = 288
    Width = 286
    Height = 32
    Caption = #1054#1055#1058#1054#1042#1067#1045' '#1055#1056#1054#1044#1040#1046#1048
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label18: TLabel
    Left = 16
    Top = 328
    Width = 288
    Height = 32
    Caption = #1054#1057#1058#1040#1058#1050#1048' '#1053#1040' '#1057#1050#1051#1040#1044#1045
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel
    Left = 16
    Top = 376
    Width = 336
    Height = 64
    Caption = #1042#1054#1047#1042#1056#1040#1058#1067' '#1056#1054#1047#1053#1045#1063#1053#1067#1045#13#10#1055#1054#1050#1059#1055#1040#1058#1045#1051#1045#1049
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label20: TLabel
    Left = 16
    Top = 448
    Width = 290
    Height = 64
    Caption = #1042#1054#1047#1042#1056#1040#1058#1067' '#1054#1055#1058#1054#1042#1067#1061#13#10#1055#1054#1050#1059#1055#1040#1058#1045#1051#1045#1049
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label21: TLabel
    Left = 368
    Top = 280
    Width = 30
    Height = 32
    Caption = 'F9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label22: TLabel
    Left = 368
    Top = 328
    Width = 44
    Height = 32
    Caption = 'F10'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label23: TLabel
    Left = 368
    Top = 392
    Width = 44
    Height = 32
    Caption = 'F11'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label24: TLabel
    Left = 368
    Top = 464
    Width = 44
    Height = 32
    Caption = 'F12'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
end
