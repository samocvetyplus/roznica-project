object fmPAct: TfmPAct
  Left = 226
  Top = 163
  Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
  ClientHeight = 481
  ClientWidth = 894
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 894
    Height = 42
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 827
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siView: TSpeedItem
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080#1079#1076#1077#1083#1080#1081
      ImageIndex = 4
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siViewClick
      SectionName = 'Untitled (0)'
    end
    object siClosePAct: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1072#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      ImageIndex = 5
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = siClosePActClick
      SectionName = 'Untitled (0)'
    end
  end
  object pa1: TPanel
    Left = 0
    Top = 42
    Width = 894
    Height = 23
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 14
      Height = 13
      Caption = #8470':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 24
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'PRORD'
      DataSource = dm.dsPrOrd
    end
    object Label2: TLabel
      Left = 188
      Top = 4
      Width = 73
      Height = 13
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 264
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SNstr'
      DataSource = dm.dsPrOrd
    end
    object Label3: TLabel
      Left = 316
      Top = 4
      Width = 34
      Height = 13
      Caption = #1057#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 352
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm.dsPrOrd
    end
    object Label4: TLabel
      Left = 84
      Top = 4
      Width = 40
      Height = 13
      Caption = #8470' '#1072#1082#1090#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TDBText
      Left = 124
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'NACT'
      DataSource = dm.dsPrOrd
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 65
    Width = 894
    Height = 416
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.Alignment = taCenter
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsPrOrdItem
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pm
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = siViewClick
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'FULLART'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Width = 165
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 105
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'PRICE2'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
        Title.Caption = #1054#1089#1090#1072#1090#1086#1082
      end
      item
        EditButtons = <>
        FieldName = 'RW'
        Footers = <>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'OLDP2'
        Footers = <>
        Title.Caption = #1057#1090'. '#1094#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.'
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'Cost'
        Footers = <
          item
            FieldName = 'Cost'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090'-'#1090#1100
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'OldCost'
        Footers = <
          item
            FieldName = 'OldCost'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090'. '#1089#1090'-'#1090#1100
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'dPrice'
        Footers = <>
        Title.Caption = #1056#1072#1079#1085#1080#1094#1072' '#1094#1077#1085#1099
        Width = 75
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'dCost'
        Footers = <
          item
            FieldName = 'dCost'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1079#1085#1080#1094#1072' '#1089#1090'-'#1090#1080
        Width = 77
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 164
    Top = 168
  end
  object pm: TPopupMenu
    Images = dmCom.ilButtons
    Left = 388
    Top = 212
    object N1: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnClick = siViewClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object PrOrdItemId1: TMenuItem
      Caption = 'PrOrdItemId'
      OnClick = PrOrdItemId1Click
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 472
    Top = 240
  end
  object acList1: TActionList
    Left = 584
    Top = 208
    object acPrintGrid: TAction
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
  end
end
