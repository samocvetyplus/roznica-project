object fmJDep: TfmJDep
  Left = 136
  Top = 126
  Width = 1008
  Height = 277
  HelpContext = 100402
  Caption = #1040#1085#1072#1083#1080#1079' ('#1087#1086' '#1089#1082#1083#1072#1076#1072#1084')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 97
    Width = 1000
    Height = 151
    Align = alClient
    Color = clBtnFace
    DataSource = dmServ.dsrJDep
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = M207IBGrid1GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Expanded = False
        FieldName = 'NAME'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM1'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM3'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM4'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SUM5'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sell_In'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Sell_InRet'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SellDep'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1000
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 747
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitCalc: TSpeedItem
      BtnCaption = #1056#1072#1089#1095#1077#1090
      Caption = #1056#1072#1089#1095#1077#1090
      Hint = #1056#1072#1089#1095#1077#1090
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitCalcClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 67
      Top = 3
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 403
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 1000
    Height = 27
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laPeriod: TLabel
      Left = 380
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laDep: TLabel
      Left = 88
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object lAll: TLabel
      Left = 520
      Top = 8
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dmServ.pmList3
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1055#1077#1088#1080#1086#1076'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 290
      Top = 2
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object spbr4: TSpeedBar
    Left = 0
    Top = 68
    Width = 1000
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 2
    InternalVer = 1
    object lbMat: TLabel
      Left = 8
      Top = 8
      Width = 23
      Height = 13
      Caption = #1052#1072#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbGood: TLabel
      Left = 160
      Top = 8
      Width = 35
      Height = 13
      Caption = #1043#1088'.'#1052#1072#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbComp: TLabel
      Left = 320
      Top = 8
      Width = 31
      Height = 13
      Caption = #1058#1086#1074#1072#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbSup: TLabel
      Left = 486
      Top = 8
      Width = 22
      Height = 13
      Caption = #1048#1079#1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object btnDo: TSpeedButton
      Left = 660
      Top = 3
      Width = 25
      Height = 22
      Hint = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnDoClick
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      BtnCaption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Hint = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = SpeedItem8Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem7: TSpeedItem
      BtnCaption = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      Caption = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      Hint = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = SpeedItem7Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      BtnCaption = #1058#1086#1074#1072#1088
      Caption = #1058#1086#1074#1072#1088
      Hint = #1058#1086#1074#1072#1088'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 355
      Top = 3
      Visible = True
      OnClick = SpeedItem9Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      BtnCaption = #1052#1072#1090#1077#1088#1080#1072#1083
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 35
      Top = 3
      Visible = True
      OnClick = SpeedItem10Click
      SectionName = 'Untitled (0)'
    end
  end
  object chlbProd: TCheckListBox
    Left = 514
    Top = 93
    Width = 140
    Height = 129
    OnClickCheck = chlbProdClickCheck
    Flat = False
    ItemHeight = 13
    TabOrder = 3
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbGood: TCheckListBox
    Left = 349
    Top = 93
    Width = 140
    Height = 129
    OnClickCheck = chlbGoodClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 4
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbGrMat: TCheckListBox
    Left = 183
    Top = 93
    Width = 140
    Height = 129
    OnClickCheck = chlbGrMatClickCheck
    Flat = False
    ItemHeight = 13
    TabOrder = 5
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbMat: TCheckListBox
    Left = 28
    Top = 93
    Width = 140
    Height = 130
    OnClickCheck = chlbMatClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 6
    Visible = False
    OnExit = chlbMatExit
  end
end
