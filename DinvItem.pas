unit DinvItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, RXDBCtrl, Mask, ExtCtrls, 
  ComCtrls, Grids, DBGrids, M207Grid, M207IBGrid, DB,
  Menus, ComDrv32, DateUtils, M207Ctrls, Buttons, DBCtrlsEh, DBGridEh,
  PropFilerEh, PropStorageEh, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar, ActnList;

type
  TfmDInvItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    siUID: TSpeedItem;
    siCloseInv: TSpeedItem;
    siUID2: TSpeedItem;
    SpeedItem2: TSpeedItem;
    siPrint: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    laCost: TLabel;
    DBTextTo: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    Label3: TLabel;
    DBText4: TDBText;
    laCostD: TLabel;
    dbtCostD: TDBText;
    laCost0: TLabel;
    DBTextFr: TDBText;
    edSN: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    Label2: TLabel;
    edUid: TEdit;
    Label7: TLabel;
    edart: TEdit;
    pmprint: TPopupMenu;
    dbSdate: TDBEditEh;
    btSdate: TBitBtn;
    siHelp: TSpeedItem;
    dg1: TDBGridEh;
    FormStorage1: TFormStorage;
    AcList: TActionList;
    acOpen: TAction;
    acClose: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siCloseInvClick(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure siPrintClick(Sender: TObject);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siDelClick(Sender: TObject);
    procedure edartKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btSdateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
     procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acCloseExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
  
  private
   dgSelFil : TField;
   LogOprIdForm:string;
   procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;

  end;

var
  fmDInvItem: TfmDInvItem;
  f: boolean;
  WMode:string[10];
implementation

uses Data, M207Proc, comdata, reportdata, Data2, ServData, Data3, SetSDate,
     dbUtil, MsgDialog, uUtils, Hist, DList;

{$R *.dfm}


procedure TfmDInvItem.acCloseExecute(Sender: TObject);
var sn:string;
    FieldsRes:t_array_var;
    LogOperationID, s_close, s_closed:string;
begin
  fmDList.taHist.Active:=true;
  fmDList.taHist.Open;
    with fmDList.taHist do
  begin
  fmDList.taHist.Append;
  fmDList.taHistDOCID.AsInteger := dm.taDListSINVID.AsInteger;
  If (Hist.log='') then fmDList.taHistFIO.Value := dmCom.User.Alias
     else fmDList.taHistFIO.Value :=Hist.log;
  fmDList.taHistSTATUS.Value := '�������';
  fmDList.taHist.Post;
  fmDList.taHist.Transaction.CommitRetaining;
  end;
  fmDList.taHist.Close;
  fmDList.taHist.Active:=false;
  with dm do
    begin
      if taDListIsClosed.AsInteger=0 then
        begin
          if taDListCLTID.AsInteger=1 then raise Exception.Create('������ ������� ���������. ��������� �� �� ������.');

          PostDataSets([taSEl, taDList]);
          CheckDInvN;  // �������� �� ������������ ������ ���������

              taDList.Refresh;
              if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� ���� ������� ������ �������������');
              Screen.Cursor:=crSQLWait;
              {�������� �������� ��������, ��������� �����}
              try
                // ������ ��� ��������: ������������� ����������� �������� �� � ��������
                dmcom.ExecuteQutmp(quTmp,'select str_pr from CHECK_CLOSE_SinvVP ('+taDListSINVID.AsString +')',1, FieldsRes);
                if FieldsRes[0]=null then sn:=''
                   else sn:=trim(FieldsRes[0]);
              finally
                Finalize(FieldsRes);

             if sn = '' then
             begin
              try
               if dm.checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
                 with quTmp do
                 begin
                   {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
                   SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+taDListSINVID.AsString+')';
                   ExecQuery;
                   s_close:=trim(Fields[0].AsString);
                   s_closed:=trim(Fields[1].AsString);
                   Transaction.CommitRetaining;
                   close;
                 end
               else
               begin
                 s_close:= '';
                 s_closed:= '';
               end;
               if (s_close='') and (s_closed='') then
               begin
                Screen.Cursor:=crSQLWait;
                LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
                dm.SetSocketClose:=true;
                dmcom.ExecuteQutmp(quTmp,'SELECT R_STR from CloseInv('+taDListSInvId.AsString+', 2, '
                                   +IntToStr(dmCom.UserId)+')',1, FieldsRes);
                if not(FieldsRes[0]=null) then
                  MessageDialog('��������� � '+taDListSN.AsString+' ������������� � ��� ��������� '+FieldsRes[0], mtWarning, [mbOK], 0);
                Screen.Cursor:=crDefault;
               end
               else begin
                if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
                if s_closed<>'' then  MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
              end;
              finally
                Finalize(FieldsRes);
                dm3.update_operation(LogOperationID);
              end;
             end
               else Application.MessageBox(pchar(sn),'��������!!',0);

              taDList.Refresh;

              Screen.Cursor:=crDefault;
              if (sn = '') and (s_close='') and (s_closed='') then  SetCloseInvBtn(siCloseInv, 1);
            end;
        end;
        end;
end;

procedure TfmDInvItem.acOpenExecute(Sender: TObject);
var FieldsRes:t_array_var;
    LogOperationID:string;
begin
 with dm do
 begin
       Screen.Cursor:=crSQLWait;
       LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
         try
         dmcom.ExecuteQutmp(quTmp,'EXECUTE PROCEDURE OpenInv('+taDListSInvId.AsString+', 2, '+IntToStr(dmCom.UserId) +')',0, FieldsRes);
         finally
       Finalize(FieldsRes);
       taDList.Refresh;
       SetCloseInvBtn(siCloseInv, 0);
       dm3.update_operation(LogOperationID);
       Screen.Cursor:=crDefault;
          end;
 end;
end;

procedure TfmDInvItem.FormCreate(Sender: TObject);
var i: integer;
    c: TColumnEh;
begin
  tb1.WallPaper:=wp;
  with dm do
    begin
      DDepFromId:= taDList.FieldByName('DEPFROMID').asInteger;
      sWorkInvID:='*'+taDListSINVID.AsString+';';
      ReOpenDataSet(quDInvItem);
      dg1['DEPFROMPRICE'].Title.Caption:='���������� �����������|���� - '+taDListDepFrom.AsSTring; // � ������ ������
      dg1['DEPPRICE'].Title.Caption:='���������� �����������|���� - '+taDListDepTo.AsSTring;  // �� �����
      laCost.Caption:='����� - '+taDListDepTo.AsSTring+':';
      laCost0.Caption:='����� - '+taDListDepFrom.AsSTring+':';
      if not CenterDep then
      begin
        dg1['DEPFROMPRICE'].Visible:=DDepFromId=Selfdepid;
        dg1['DEPPRICE'].Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
        dg1['DEPFROMPRICE'].Color:=clInfoBk;
        laCost0.Visible:=DDepFromId=Selfdepid;
        DBTextFr.Visible:=DDepFromId=Selfdepid;
        laCost.Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
        DBTextTo.Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
      end
      else
      begin
        dg1['DEPFROMPRICE'].Visible:=True;
        dg1['DEPPRICE'].Visible:=True;
      end;
      DBText4.Visible:=CenterDep;
      Label3.Visible:=CenterDep;
      dg1['OPTPRICE'].Visible:=CenterDep;
      dg1['PPRICE'].Visible:=CenterDep;
    end;
  with dm, dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dg1.Columns.Add;
          c.Field:=quDInvItem.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Width:=80;
          c.Tag:=i+1;
        end;
    end;

  dm.ClosedInvId:=-1;

 if(dm.taDListURF_FROM.AsInteger = 1)or(dm.taDListURF_TO.AsInteger = 1) then
  begin
    laCostD.Visible:=True;
    dbtCostD.Visible:=True;
  end
  else begin
    laCostD.Visible:=False;
    dbtCostD.Visible:=False;
  end;
  ActiveControl := eduid;

  if dm.taDListISCLOSED.AsInteger = 0 then dm.SetCloseInvBtn(siCloseInv, 0)
   else dm.SetCloseInvBtn(siCloseInv, 1);

 dg1.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dg1', [crpColIndexEh, crpColWidthsEh]);

 dmcom.FillTagMenu(pmprint, siPrintClick, 0);
 LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;



procedure TfmDInvItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDInvItem.siCloseInvClick(Sender: TObject);
var sn: string;
  v: Variant;
begin
 f:=true;
 If dm.taDListISCLOSED.AsInteger=1 then
 begin
   v := dmCom.db.QueryValue('select sinvid from dinv where sinvid = :sinvid', 0, [dm.taDListSINVID.AsInteger]);

   if not VarIsNull(v) then
   begin
     Application.MessageBox('��������� �� ����� �������. ��� ��������.', '���������', 0);
     exit;
   end else
   begin
     ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False);
   end;
 end
 else
 {�������� ������� ��������� � �������� �����}
  begin
   with dm.quTmp do
   begin
     close;
     SQL.Text:='select str_pr from CHECK_CLOSE_Sinv2Prod ('+dm.taDListSINVID.AsString +')';
     ExecQuery;
     if not Fields[0].IsNull then sn:=trim(Fields[0].AsString)
        else sn:='';
     Transaction.CommitRetaining;
     close;
   end;
     if (sn='') then
     begin
    //   acClose.Execute;
       if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
       begin
         acClose.Execute;
         if not dm.pCreatePrord(dm.taDListSINVID.AsString) then
         begin
           with dm.quTmp do
           begin
              close;
              SQL.Text:='update sinv set isclosed=0, userid = null where sinvid='+dm.taDListSINVID.AsString;
              ExecQuery;
              Transaction.CommitRetaining;
              close;
           end;
           dm.SetCloseInvBtn(siCloseInv, 0);
           dm.quDInvItem.Refresh;
           dm.taDList.refresh;
           exit;// �������� ������� � ���������, � ������� �������� ����.���� ������ � �������
         end;
       end;
     end
     else
        Application.MessageBox(pchar(sn),'��������!!',0);
     dm.taDList.Refresh;
     Screen.Cursor:=crDefault;
  end;
end;

procedure TfmDInvItem.siExitClick(Sender: TObject);
begin
dg1.SaveColumnsLayoutIni(GetIniFileName, Name+'_dg1', true);
close;
end;

procedure TfmDInvItem.siPrintClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  try

  arr:=gen_arr(dg1, dm.quDInvItemSITEMID);

  PrintTag(arr, it_tag, TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
end;

procedure TfmDInvItem.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var T,i: integer;
    FieldsRes:t_array_var;
    SitemID: Integer;
begin
  case Key of
  VK_RETURN:
    with dm, dm2, dmCom do
      begin
       PostDataSets([taDList]);
       dmCom.tr.CommitRetaining;
       if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
       raise Exception.Create('������ ������������� ��������� � ������� '#39 +
         dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������');
       try
         dmcom.ExecuteQutmp(quTmp,'  select uidwhcalc  from d_rec ',1, FieldsRes);
         if FieldsRes[0]=null then i:=0 else i:=FieldsRes[0];
       finally
         Finalize(FieldsRes);
       end;
       If i = 1 then Raise Exception.Create('��������� ������ �����������');

       if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');

       with quInsDUID, Params do
        begin
         uid := StrToInt(edUID.Text);
         ByName['UID'].AsInteger:=StrToInt(edUID.Text);
         ByName['SINVID'].AsInteger:=taDListSInvId.AsInteger;
         ByName['DEPID'].AsInteger:=taDListDepId.AsInteger;
         ByName['DEPFROMID'].AsInteger:=taDListDepFromId.AsInteger;
         ByName['OPT'].AsInteger:=0;
         ExecQuery;

         T:=FieldByName('T').AsInteger;

         SItemID := FieldByName('SItemID').AsInteger;
         
         Close;
        end;

        case T of
         0,1: begin
                quDInvItem.Append;
                quDInvItemSItemID.AsInteger := SItemID;
                quDInvItem.Post;
                //quDInvItem.Refresh;
                //quDInvItem.Locate('uid',uid,[]);
              end;
         2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
         3: MessageDialog('������� �������', mtInformation, [mbOK], 0);
         4: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
        end;

        edUID.Text:='';
        edArt.Text:='';
        dmCom.tr.CommitRetaining;
        taDList.Refresh;
      end;
    VK_DOWN: begin ActiveControl:=dg1;
                   edUid.Text:='';
                   if CenterDep then dg1.SelectedField := dm.quDInvItemDEPPRICE
                   else if dmcom.DefaultDep.DepId = dm.taDListDEPFROMID.AsInteger then
                           dg1.SelectedField := dm.quDInvItemDEPFROMPRICE
                        else dg1.SelectedField := dm.quDInvItemDEPPRICE;
                   end;
   end;
end;

procedure TfmDInvItem.siDelClick(Sender: TObject);
var LogOperationID:string;
begin
 if not AppDebug then
  if not dm.checkInv_RUseDep(dm.taDListDEPFROMID.AsInteger) then
  begin
    MessageDialog('������ ������������� ��������� � ������� '#39 +
    dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName  + #39' �� ��������� ���� ������', mtInformation, [mbOK], 0);
    eXit;
  end;
  LogOperationID:=dm3.insert_operation('������� ������� �� ��������� ��',LogOprIdForm);
  dm.quDInvItem.Delete;
  dm3.update_operation(LogOperationID); 
end;

procedure TfmDInvItem.edartKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN:
      begin
       if dm.quDInvItem.Locate('ART', edart.Text,[]) then  ActiveControl:=dg1
       else MessageDialog('������� �� ������!', mtWarning, [mbOk], 0);
       edart.Text:='';
      end;
      VK_DOWN: begin ActiveControl:=dg1; edart.Text:='' end;
    end
end;

procedure TfmDInvItem.dg1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_UP, VK_DOWN: dg1.SelectedField := dgSelFil;
 end;
end;

procedure TfmDInvItem.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  dgSelFil := dg1.SelectedField;
end;

procedure TfmDInvItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
f:=false;
  with dm, dmCom do
    begin
      PostDataSets([taDList, quDInvItem ]);
      tr.CommitRetaining;
      ReOpenDataSets([quDInvItem]);
      taDList.Refresh;
      if taDListSINVID.IsNull then begin
       MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
       ReOpenDataSet(taDList);
      end else if not dm.closeinv(dm.taDListSINVID.AsInteger,0) then
      begin
       if dm.EmptyInv(dm.taDListSINVID.AsInteger,0) then
       begin
        ExecSQL('delete from sinv where sinvid='+dm.taDListSINVID.AsString, dm.quTmp);
        ReOpenDataSet(dm.taDList);
       end
      end;
      CloseDataSets([quDInvItem]);

      tr.CommitRetaining;

      taDList.Refresh;
 WMode:='';     
    end;
// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
end;

procedure TfmDInvItem.btSdateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taDListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm.taDListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm.taDListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm.taDList.Edit;
    dm.taDListSDATE.AsDateTime:=d;
    dm.taDList.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmDInvItem.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100222);
end;

procedure TfmDInvItem.FormActivate(Sender: TObject);
begin
  WMode:='DINVITEM';
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;

end;

procedure TfmDInvItem.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmDInvItem.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
if (Column.Tag>0) then
    Background := dmCom[Column.Field.Tag].Color
 else
 begin
   if (Column<>NIL) and
      ((Column.FieldName='ART') or
       (Column.FieldName='ART2') or
       (Column.FieldName='UID')) or
       (Column.FieldName='recNo') then
      Background:=dmCom.clMoneyGreen
      else
      if (Column<>NIL) and
         ((Column.FieldName='DEPFROMPRICE') or
          (Column.FieldName='DEPPRICE')) then
         Background:=dmCom.clCream;
 end
end;


end.
