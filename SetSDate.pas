unit SetSDate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls;

type
  TfmSetSDate = class(TForm)
    mc1: TMonthCalendar;
    Label1: TLabel;
    Label2: TLabel;
    tp1: TDateTimePicker;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure mc1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSetSDate: TfmSetSDate;

implementation
uses comdata;
{$R *.DFM}

procedure TfmSetSDate.mc1Click(Sender: TObject);
begin
 tp1.Time:=dmcom.GetServerTime;
end;

end.
