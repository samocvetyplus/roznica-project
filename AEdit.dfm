object fmAEdit: TfmAEdit
  Left = 381
  Top = 170
  HelpContext = 100205
  BorderStyle = bsDialog
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1072
  ClientHeight = 221
  ClientWidth = 315
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 8
    Width = 71
    Height = 13
    Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 32
    Width = 50
    Height = 13
    Caption = #1052#1072#1090#1077#1088#1080#1072#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 4
    Top = 56
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 4
    Top = 80
    Width = 94
    Height = 13
    Caption = #1054#1089#1085#1086#1074#1085#1072#1103' '#1074#1089#1090#1072#1074#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 4
    Top = 104
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 4
    Top = 128
    Width = 102
    Height = 13
    Caption = #1045#1076#1080#1085#1080#1094#1072' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 4
    Top = 152
    Width = 36
    Height = 13
    Caption = #1057#1090#1088#1072#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 4
    Top = 176
    Width = 54
    Height = 13
    Caption = #1040#1090#1090#1088#1080#1073#1091#1090' 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 4
    Top = 200
    Width = 54
    Height = 13
    Caption = #1040#1090#1090#1088#1080#1073#1091#1090' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lcProd: TDBLookupComboBox
    Left = 112
    Top = 4
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'D_COMPID'
    DataSource = dm.dsArt
    KeyField = 'D_COMPID'
    ListField = 'SNAME'
    ListSource = dm2.dsProd
    TabOrder = 0
    OnKeyDown = lcProdKeyDown
  end
  object lcMat: TDBLookupComboBox
    Left = 112
    Top = 28
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'D_MATID'
    DataSource = dm.dsArt
    KeyField = 'D_MATID'
    ListField = 'NAME'
    ListSource = dm2.dsMat
    TabOrder = 1
  end
  object lcGood: TDBLookupComboBox
    Left = 112
    Top = 52
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'D_GOODID'
    DataSource = dm.dsArt
    KeyField = 'D_GOODID'
    ListField = 'NAME'
    ListSource = dm2.dsGood
    TabOrder = 2
  end
  object lcIns: TDBLookupComboBox
    Left = 112
    Top = 76
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'D_INSID'
    DataSource = dm.dsArt
    KeyField = 'D_INSID'
    ListField = 'SNAME'
    ListSource = dm2.dsIns
    TabOrder = 3
  end
  object DBEdit1: TDBEdit
    Left = 112
    Top = 100
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'ART'
    DataSource = dm.dsArt
    TabOrder = 4
  end
  object lcUnit: TRxDBComboBox
    Left = 112
    Top = 124
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'UNITID'
    DataSource = dm.dsArt
    EnableValues = False
    ItemHeight = 13
    Items.Strings = (
      #1064#1090
      #1043#1088)
    TabOrder = 5
    Values.Strings = (
      '0'
      '1')
  end
  object lcCountry: TDBLookupComboBox
    Left = 112
    Top = 148
    Width = 105
    Height = 21
    Color = clInfoBk
    DataField = 'D_COUNTRYID'
    DataSource = dm.dsArt
    KeyField = 'D_COUNTRYID'
    ListField = 'NAME'
    ListSource = dm2.dsCountry
    TabOrder = 6
  end
  object lcAtt1: TDBLookupComboboxEh
    Left = 112
    Top = 172
    Width = 105
    Height = 19
    Color = clInfoBk
    DataField = 'ATT1'
    DataSource = dm.dsArt
    EditButtons = <>
    Flat = True
    KeyField = 'ID'
    ListField = 'NAME'
    ListSource = dm2.dsAtt1
    TabOrder = 7
    Visible = True
  end
  object lcAtt2: TDBLookupComboboxEh
    Left = 112
    Top = 195
    Width = 105
    Height = 19
    Color = clInfoBk
    DataField = 'ATT2'
    DataSource = dm.dsArt
    EditButtons = <>
    Flat = True
    KeyField = 'ID'
    ListField = 'NAME'
    ListSource = dm2.dsAtt2
    TabOrder = 8
    Visible = True
  end
  object btnOk: TButton
    Left = 236
    Top = 12
    Width = 75
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 9
  end
  object btnCancel: TButton
    Left = 236
    Top = 48
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 10
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 260
    Top = 96
  end
end
