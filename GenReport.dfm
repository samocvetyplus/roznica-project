object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 121
  ClientWidth = 457
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 61
    Height = 14
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 288
    Top = 10
    Width = 43
    Height = 14
    Caption = #1055#1077#1088#1080#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 24
    Top = 48
    Width = 83
    Height = 14
    Caption = #1060#1086#1088#1084#1072' '#1086#1087#1083#1072#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 24
    Top = 87
    Width = 61
    Height = 14
    Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object CheckBox1: TCheckBox
    Left = 288
    Top = 48
    Width = 97
    Height = 17
    Caption = #1061#1088#1072#1085#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Button1: TButton
    Left = 280
    Top = 88
    Width = 75
    Height = 25
    Caption = #1054#1090#1095#1077#1090'!'
    TabOrder = 1
  end
  object Button2: TButton
    Left = 374
    Top = 88
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    OnClick = Button2Click
  end
  object lcSup: TDBLookupComboBox
    Left = 113
    Top = 8
    Width = 160
    Height = 21
    Color = clInfoBk
    DataField = 'SUPID'
    DataSource = dm.dsSList
    KeyField = 'D_COMPID'
    ListField = 'FNAME'
    ListSource = dm.dsSup
    TabOrder = 3
  end
  object lcPayType: TDBLookupComboBox
    Left = 113
    Top = 44
    Width = 160
    Height = 21
    Color = clInfoBk
    DataField = 'PAYTYPEID'
    DataSource = dm.dsSList
    KeyField = 'PAYTYPEID'
    ListField = 'NAME'
    ListSource = dmCom.dsPayType
    TabOrder = 4
  end
  object ComboBox1: TComboBox
    Left = 113
    Top = 85
    Width = 161
    Height = 21
    AutoCloseUp = True
    DropDownCount = 3
    ItemHeight = 13
    TabOrder = 5
    Items.Strings = (
      #1074#1089#1077
      #1087#1088#1086#1076#1072#1085#1099
      #1085#1072' '#1089#1082#1083#1072#1076#1077)
  end
end
