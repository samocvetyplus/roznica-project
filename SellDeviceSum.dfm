object fmDeviceSell: TfmDeviceSell
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  ClientHeight = 769
  ClientWidth = 532
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PrintScale = poNone
  Scaled = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 0
    Width = 532
    Height = 769
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    LookAndFeel.SkinName = ''
    ExplicitWidth = 513
    ExplicitHeight = 289
    object View: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsrCassa
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skSum
          Position = spFooter
          Column = ViewColumn4
        end
        item
          Format = '0'
          Kind = skSum
          Position = spFooter
          Column = ViewColumn1
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = ViewColumn4
        end
        item
          Format = '0'
          Kind = skSum
          Column = ViewColumn1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.DragOpening = False
      OptionsBehavior.DragScrolling = False
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfAlwaysVisible
      object ViewColumn3: TcxGridDBColumn
        Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        GroupIndex = 0
        Width = 149
      end
      object ViewColumn2: TcxGridDBColumn
        Caption = #1059#1089#1090#1088#1086#1081#1089#1090#1074#1086
        DataBinding.FieldName = 'DEVICE'
        Width = 112
      end
      object ViewColumn1: TcxGridDBColumn
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1095#1077#1082#1086#1074
        DataBinding.FieldName = 'CHECKS$COUNT'
        Width = 115
      end
      object ViewColumn4: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'SUMMA'
        Width = 130
      end
    end
    object Level: TcxGridLevel
      GridView = View
    end
  end
  object dsrCassa: TDataSource
    DataSet = dm.dsCassa
    Left = 8
    Top = 232
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 8
    Top = 192
  end
end
