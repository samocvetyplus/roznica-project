object fmSRet: TfmSRet
  Left = 69
  Top = 164
  HelpContext = 100212
  Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
  ClientHeight = 606
  ClientWidth = 1119
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 116
    Width = 1119
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitWidth = 1045
  end
  object Splitter6: TSplitter
    Left = 0
    Top = 344
    Width = 1119
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 337
    ExplicitWidth = 1045
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 587
    Width = 1119
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1119
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 819
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00000000FF0000FF00000000FFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        000000000000FF0000FF000000000000000000007B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFF00000000FF0000FF0000FF0000FF0000FF0000FF
        0000FF007B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        FF0000FF0000FF0000FF0000FF0000FF0000FF007B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00000000000000000000FF0000FF000000000000
        000000007B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF00000000FF0000FF00000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000000000FF
        FFFFFFFF7B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF000000000000000000000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF7B7B7BFF00FFFF00FFFF00FF
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFF7B
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acReCalc
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
        000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBD
        BD00BDBDBD000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00000000007B7B7B007B7B7B00BDBDBD007B7B7B00000000007B7B
        7B00BDBDBD007B7B7B007B7B7B0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD007B7B7B00000000007B7B
        7B00BDBDBD00BDBDBD00BDBDBD00BDBDBD0000000000FF00FF00FF00FF00FF00
        FF00000000007B7B7B007B7B7B007B7B7B00BDBDBD00BDBDBD0000000000BDBD
        BD00BDBDBD007B7B7B007B7B7B007B7B7B0000000000FF00FF00FF00FF00FF00
        FF0000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD0000000000000000000000
        0000BDBDBD00BDBDBD00BDBDBD00BDBDBD0000000000FF00FF00FF00FF00FF00
        FF00000000007B7B7B007B7B7B007B7B7B007B7B7B0000000000000000000000
        00007B7B7B007B7B7B007B7B7B007B7B7B0000000000FF00FF00FF00FF00FF00
        FF0000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
        BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD0000000000FF00FF00FF00FF00FF00
        FF00FF00FF000000000000000000000000000000000000000000000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00
        FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00
        FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00
        FF0000000000BDBDBD0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF007B7B7B007B7B7B00BDBDBD0000000000000000000000
        0000BDBDBD007B7B7B007B7B7B00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000000000BDBDBD00BDBDBD00BDBDBD00BDBD
        BD00BDBDBD0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000000000000000000000000000000
        000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ImageIndex = 8
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = acReCalcExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 755
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 1119
    Height = 75
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    DesignSize = (
      1119
      75)
    object Label1: TLabel
      Left = 6
      Top = 28
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 87
      Height = 13
      Caption = #8470' '#1072#1082#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 216
      Top = 8
      Width = 87
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 430
      Top = 48
      Width = 61
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 961
      Top = 8
      Width = 74
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitLeft = 887
    end
    object DBText1: TDBText
      Left = 1049
      Top = 8
      Width = 50
      Height = 13
      Anchors = [akTop, akRight]
      AutoSize = True
      DataField = 'COST'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 310
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPFROM'
      DataSource = dm.dsSRetList
    end
    object dbSup: TDBText
      Left = 525
      Top = 57
      Width = 31
      Height = 13
      AutoSize = True
      DataField = 'COMP'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label3: TLabel
      Left = 961
      Top = 27
      Width = 14
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #1058#1056
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 964
      Top = 50
      Width = 41
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #1058#1056' '#1053#1044#1057
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 430
      Top = 7
      Width = 89
      Height = 13
      Caption = #1043#1088#1091#1079#1086#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 430
      Top = 29
      Width = 82
      Height = 13
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbt_Proizvoditel: TDBText
      Left = 525
      Top = 32
      Width = 75
      Height = 13
      AutoSize = True
      DataField = 'Proizvoditel'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object dbt_Gruzootprav: TDBText
      Left = 525
      Top = 8
      Width = 79
      Height = 13
      AutoSize = True
      DataField = 'Gruzootprav'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label11: TLabel
      Left = 6
      Top = 50
      Width = 47
      Height = 13
      Caption = #1044#1086#1075#1086#1074#1086#1088':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 93
      Height = 21
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsSRetList
      TabOrder = 0
    end
    object dbedSdate: TDBEditEh
      Left = 116
      Top = 25
      Width = 77
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsSRetList
      EditButtons = <>
      Enabled = False
      ReadOnly = True
      TabOrder = 1
      Visible = True
    end
    object bttime: TBitBtn
      Left = 190
      Top = 24
      Width = 20
      Height = 22
      TabOrder = 2
      OnClick = bttimeClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object edPTr: TM207DBEdit
      Left = 988
      Top = 23
      Width = 25
      Height = 21
      Anchors = [akTop, akRight]
      Color = clInfoBk
      DataField = 'PTR'
      DataSource = dm.dsSRetList
      TabOrder = 3
      OnKeyDown = edPTrKeyDown
    end
    object edTrans: TM207DBEdit
      Left = 1019
      Top = 23
      Width = 77
      Height = 21
      Anchors = [akTop, akRight]
      AutoSelect = False
      Color = clInfoBk
      DataField = 'TR'
      DataSource = dm.dsSRetList
      TabOrder = 4
      OnKeyDown = edTransKeyDown
    end
    object edPTrNDS: TM207DBEdit
      Left = 1011
      Top = 45
      Width = 25
      Height = 21
      Anchors = [akTop, akRight]
      Color = clInfoBk
      DataField = 'PTRNDS'
      DataSource = dm.dsSRetList
      TabOrder = 5
      OnKeyDown = edPTrNDSKeyDown
    end
    object DBEdit1: TM207DBEdit
      Left = 1035
      Top = 45
      Width = 61
      Height = 21
      Anchors = [akTop, akRight]
      Color = clInfoBk
      DataField = 'TRNDS'
      DataSource = dm.dsSRetList
      TabOrder = 6
      OnKeyDown = DBEdit1KeyDown
    end
    object chbxRet_NoNDS: TDBCheckBoxEh
      Left = 216
      Top = 24
      Width = 177
      Height = 17
      Alignment = taLeftJustify
      Caption = #1053#1077' '#1074#1099#1076#1077#1083#1103#1090#1100' '#1053#1044#1057' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
      DataField = 'RET_NONDS'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object lc_Gruzootprav: TcxDBLookupComboBox
      Left = 524
      Top = 3
      DataBinding.DataField = 'Gruzootprav_ID'
      DataBinding.DataSource = dm.dsSRetList
      Properties.KeyFieldNames = 'D_COMPID'
      Properties.ListColumns = <
        item
          FieldName = 'FNAME'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = dm.dsSupR
      Properties.OnEditValueChanged = lc_GruzootpravPropertiesChange
      Style.Color = clInfoBk
      TabOrder = 8
      Width = 205
    end
    object lc_Proizvoditel: TcxDBLookupComboBox
      Left = 524
      Top = 26
      DataBinding.DataField = 'Proizvoditel_ID'
      DataBinding.DataSource = dm.dsSRetList
      Properties.KeyFieldNames = 'D_COMPID'
      Properties.ListColumns = <
        item
          FieldName = 'FNAME'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = dm.dsSupR
      Style.Color = clInfoBk
      TabOrder = 9
      Width = 205
    end
    object lcComp: TcxDBLookupComboBox
      Left = 524
      Top = 49
      DataBinding.DataField = 'COMPID'
      DataBinding.DataSource = dm.dsSRetList
      Properties.KeyFieldNames = 'D_COMPID'
      Properties.ListColumns = <
        item
          FieldName = 'FNAME'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = dm.dsSupR
      Properties.OnEditValueChanged = lcCompPropertiesEditValueChanged
      Style.Color = clInfoBk
      TabOrder = 10
      Width = 205
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 347
    Width = 1119
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 305
      Top = 0
      Height = 240
    end
    object Panel3: TPanel
      Left = 308
      Top = 0
      Width = 811
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pc1: TPageControl
        Left = 0
        Top = 0
        Width = 811
        Height = 240
        ActivePage = tsWH
        Align = alClient
        TabOrder = 0
        object tsWH: TTabSheet
          Caption = #1057#1082#1083#1072#1076
          object tb2: TSpeedBar
            Left = 0
            Top = 0
            Width = 803
            Height = 29
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
            BtnOffsetHorz = 3
            BtnOffsetVert = 3
            BtnWidth = 70
            BtnHeight = 23
            Images = dmCom.ilButtons
            TabOrder = 0
            InternalVer = 1
            object Label2: TLabel
              Left = 302
              Top = 8
              Width = 52
              Height = 13
              Caption = #1048#1076'. '#1085#1086#1084#1077#1088
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              Transparent = True
            end
            object Label10: TLabel
              Left = 104
              Top = 8
              Width = 30
              Height = 13
              Caption = #1055#1086#1080#1089#1082
              Transparent = True
            end
            object edUID: TEdit
              Left = 360
              Top = 4
              Width = 73
              Height = 21
              Color = clInfoBk
              TabOrder = 0
              Text = 'edUID'
              OnKeyDown = edUIDKeyDown
            end
            object cbSearch: TCheckBox
              Left = 89
              Top = 9
              Width = 12
              Height = 12
              Font.Charset = RUSSIAN_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object ceArt: TComboEdit
              Left = 140
              Top = 4
              Width = 93
              Height = 21
              ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
              Color = clInfoBk
              Glyph.Data = {
                F6000000424DF600000000000000760000002800000010000000100000000100
                04000000000080000000120B0000120B00001000000010000000000000000000
                8000008000000080800080000000800080008080000080808000C0C0C0000000
                FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
                DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
                9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
                9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
                90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
              NumGlyphs = 1
              TabOrder = 2
              OnButtonClick = ceArtButtonClick
              OnChange = ceArtChange
              OnKeyDown = ceArtKeyDown
              OnKeyUp = ceArtKeyUp
            end
            object SpeedbarSection2: TSpeedbarSection
              Caption = 'Untitled (0)'
            end
            object SpeedItem1: TSpeedItem
              BtnCaption = #1054#1090#1082#1088#1099#1090#1100
              Caption = 'SpeedItem1'
              Hint = #1054#1090#1082#1088#1099#1090#1100
              ImageIndex = 74
              Layout = blGlyphLeft
              Spacing = 1
              Left = 3
              Top = 3
              Visible = True
              OnClick = SpeedItem1Click
              SectionName = 'Untitled (0)'
            end
          end
          object dgWH: TDBGridEh
            Left = 0
            Top = 29
            Width = 803
            Height = 183
            Align = alClient
            Color = clInfoBk
            ColumnDefValues.Title.Alignment = taCenter
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dm.dsD_WH
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
            RowDetailPanel.Color = clBtnFace
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnGetCellParams = dgWHGetCellParams
            Columns = <
              item
                EditButtons = <>
                FieldName = 'ART'
                Footers = <>
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083
              end
              item
                EditButtons = <>
                FieldName = 'ART2'
                Footers = <>
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
                Width = 61
              end
              item
                EditButtons = <>
                FieldName = 'D_MATID'
                Footers = <>
                Title.Caption = #1053#1072#1080#1084'.'
                Width = 33
              end
              item
                EditButtons = <>
                FieldName = 'D_MATID'
                Footers = <>
                Title.Caption = #1052#1072#1090'.'
                Width = 31
              end
              item
                EditButtons = <>
                FieldName = 'D_INSID'
                Footers = <>
                Title.Caption = #1054#1042
                Width = 27
              end
              item
                EditButtons = <>
                FieldName = 'PRODCODE'
                Footers = <>
                Title.Caption = #1048#1079#1075'.'
                Width = 35
              end
              item
                EditButtons = <>
                FieldName = 'D_COUNTRYID'
                Footers = <>
                Width = 40
              end
              item
                Color = 10930928
                EditButtons = <>
                FieldName = 'DQ'
                Footers = <>
                Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
                Width = 60
              end
              item
                Color = 10930928
                EditButtons = <>
                FieldName = 'DW'
                Footers = <>
                Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
                Width = 58
              end
              item
                Color = clYellow
                EditButtons = <>
                FieldName = 'OPTPRICE'
                Footers = <>
                Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
                Width = 54
              end
              item
                Color = clMoneyGreen
                EditButtons = <>
                FieldName = 'PRICE'
                Footers = <>
                Title.Caption = #1055#1088'.'#1094#1077#1085#1072
                Width = 52
              end
              item
                EditButtons = <>
                FieldName = 'PRICE2'
                Footers = <>
                Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
                Width = 53
              end
              item
                EditButtons = <>
                FieldName = 'QUANTITY'
                Footers = <>
                Title.Caption = #1042#1089#1077#1075#1086' - '#1050#1086#1083'-'#1074#1086
              end
              item
                EditButtons = <>
                FieldName = 'WEIGHT'
                Footers = <>
                Title.Caption = #1042#1089#1077#1075#1086' - '#1042#1077#1089
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 305
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 238
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 238
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 238
      end
      object Splitter7: TSplitter
        Left = 121
        Top = 1
        Height = 238
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 49
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dgEl: TDBGridEh
    Left = 0
    Top = 119
    Width = 1119
    Height = 225
    Align = alClient
    Color = clInfoBk
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSRet
    Flat = True
    FooterColor = clMoneyGreen
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghColumnResize, dghColumnMove, dghExtendVertLines]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    VertScrollBar.SmoothStep = True
    OnGetCellParams = dgElGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RecNo'
        Footer.FieldName = 'RecNo'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #8470
        Width = 28
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1048#1076'.'#1085#1086#1084#1077#1088
        Title.ToolTips = True
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'GOODID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1053#1072#1080#1084'.'
        Title.EndEllipsis = True
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1056#1072#1079#1084'.'
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'INSID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1054#1042
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'MATID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1052#1072#1090'.'
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1045#1048
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'PROD'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1048#1079#1075'.'
      end
      item
        EditButtons = <>
        FieldName = 'SUP'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1055#1086#1089#1090#1072#1074#1097#1080#1082
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
        Title.EndEllipsis = True
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'SPRICE'
        Footer.FieldName = 'SPRICE'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1055#1088#1080#1093'.'#1094#1077#1085#1072
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'SSF'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1042#1085#1077#1096'.'#8470
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1042#1085#1091#1090#1088'.'#8470
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'NDATE'
        Footer.FieldName = 'NDATE'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1042#1085#1077#1096'.'#1076#1072#1090#1072
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GridContract: TcxGrid
    Left = 8
    Top = 178
    Width = 301
    Height = 135
    TabOrder = 0
    Visible = False
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object GridContractView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsContract
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.ScrollBars = ssVertical
      OptionsView.GroupByBox = False
      object GridContractViewNUMBER: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'NUMBER'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewCONTRACTDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'CONTRACT$DATE'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewNAME: TcxGridDBColumn
        Caption = #1042#1080#1076
        DataBinding.FieldName = 'NAME'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewFORMATEDNAME: TcxGridDBColumn
        DataBinding.FieldName = 'NAME$FORMATED'
        Visible = False
      end
    end
    object GridContractLevel: TcxGridLevel
      GridView = GridContractView
    end
  end
  object lcContract: TcxDBExtLookupComboBox
    Left = 92
    Top = 91
    AutoSize = False
    DataBinding.DataField = 'CONTRACT$ID'
    DataBinding.DataSource = dm.dsSRetList
    Properties.Alignment.Horz = taCenter
    Properties.DropDownListStyle = lsFixedList
    Properties.View = GridContractView
    Properties.KeyFieldNames = 'ID'
    Properties.ListFieldItem = GridContractViewFORMATEDNAME
    Properties.PopupAlignment = taRightJustify
    Properties.OnCloseUp = lcContractPropertiesCloseUp
    Properties.OnEditValueChanged = lcContractPropertiesEditValueChanged
    Style.Color = clAqua
    TabOrder = 6
    Height = 21
    Width = 301
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 404
    Top = 436
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 468
    Top = 436
    object N1: TMenuItem
      Action = acDel
    end
  end
  object pmWH: TPopupMenu
    Images = dmCom.ilButtons
    Left = 544
    Top = 458
    object N2: TMenuItem
      Action = acAddArt
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 360
    Top = 424
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acCloseInv: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      OnExecute = acCloseInvExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
    object acAddArt: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ShortCut = 13
    end
    object acReCalc: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      OnExecute = acReCalcExecute
      OnUpdate = acReCalcUpdate
    end
    object acOpenInv: TAction
      Caption = 'acOpenInv'
      OnExecute = acOpenInvExecute
    end
  end
  object taContract: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.id, c.number, c.contract$date, c.contract$end$date, c.i' +
        's$unlimited, ct.id classid, ct.name'
      'from contract c, contract$types ct'
      'where c.company$id = 1 and c.agent$id = :agent$id and'
      
        '((onlydate(c.contract$end$date) >= :invoice$date) or (c.is$unlim' +
        'ited = 1)) and'
      
        'ct.id = c.contract$type and ct.good = 1 and ((:contract$class$id' +
        ' = 0) or ((ct.id = :contract$class$id))) and'
      'c.is$delete = 0'
      'order by ct.id, c.contract$date'
      ''
      ''
      ''
      ''
      '')
    AfterOpen = taContractAfterOpen
    BeforeOpen = taContractBeforeOpen
    OnCalcFields = taContractCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 368
    Top = 272
    object taContractID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taContractNUMBER: TFIBStringField
      FieldName = 'NUMBER'
      Size = 32
      EmptyStrToNull = True
    end
    object taContractCONTRACTDATE: TFIBDateTimeField
      FieldName = 'CONTRACT$DATE'
    end
    object taContractCONTRACTENDDATE: TFIBDateTimeField
      FieldName = 'CONTRACT$END$DATE'
    end
    object taContractISUNLIMITED: TFIBSmallIntField
      FieldName = 'IS$UNLIMITED'
    end
    object taContractCLASSID: TFIBIntegerField
      FieldName = 'CLASSID'
    end
    object taContractNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object taContractNAMEFORMATED: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'NAME$FORMATED'
      Size = 64
      EmptyStrToNull = True
      Calculated = True
    end
  end
  object dsContract: TDataSource
    DataSet = taContract
    Left = 336
    Top = 272
  end
end
