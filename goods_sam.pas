unit goods_sam;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, dbcgrids, StdCtrls, Mask, DBCtrls, Grids,
  DBGridEh, DB, Menus, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  Tfmgoods_sam = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    siSort: TSpeedItem;
    dg1: TDBGridEh;
    pmGoods: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siSortClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spitPrintClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmgoods_sam: Tfmgoods_sam;

implementation

uses comdata, DBTree, Sort, Data, M207Proc, MsgDialog;

{$R *.dfm}

procedure Tfmgoods_sam.siExitClick(Sender: TObject);
begin
 close;
end;

procedure Tfmgoods_sam.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taGoodsSam.Delete;
end;

procedure Tfmgoods_sam.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taGoodsSam.Append;
end;

procedure Tfmgoods_sam.FormCreate(Sender: TObject);
begin
tb1.Wallpaper:=wp;
  with dmCom do
    begin
      taGoodsSam.Active:=True;
    end;
end;

procedure Tfmgoods_sam.FormClose(Sender: TObject;
  var Action: TCloseAction);
var i:string;
    j:integer;
begin
with dmCom, dm do
 begin
  PostDataSet(taGoodsSam);
  
  quTmp.Close;
  quTmp.sql.Text:='select count(*), SName_Sam from d_goods_sam group by SName_Sam having count(*)>=2';
  quTmp.ExecQuery;
  i:='';j:=0;
  while not quTmp.eof do
   begin
    if j=0 then i:='��� ������� ���� ��������'+#13#10+ quTmp.Fields[1].AsString
    else i:=i+#13#10+ quTmp.Fields[1].AsString;
    inc(j);
    quTmp.Next;
   end;
  quTmp.Close;
  if i<>'' then raise Exception.Create(i);

  taGoodsSam.Active:=False;
  tr.CommitRetaining;
 end;
end;

procedure Tfmgoods_sam.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT d_goodsid_sam, NAME_sam '+
                       'FROM D_GOODs_sam '+
                       'where d_goodsid_sam<>-1000 ' +
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_GOODs_sam set SortInd=?SortInd where D_GOODsId_sam=?D_GOODsId_sam';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taGoodsSam]);
            end
        end;
    finally
      fmSort.Free;
    end;
end;

procedure Tfmgoods_sam.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure Tfmgoods_sam.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i, j: integer;
begin
  with dmCom, taGoodsSam do
    if (Key in [9, VK_RETURN])  then
      begin
        j:=dg1.SelectedIndex;
        i:=taGoodsSamD_GOODSID_SAM.AsInteger;
        if taGoodsSamNAME_SAM.AsString = '' then
        begin
          ActiveControl := dg1;
          dg1.SelectedField := taGoodsSamSNAME_SAM;
          Key := 0;
          SysUtils.Abort;
        end;

        if (State=dsInsert)or(State=dsEdit) then Post;
 {       if dg1.SelectedField.Name = 'NAME_SAM' then
         dg1.SelectedField.Name = 'SNAME_SAM';
  }
        if dg1.SelectedField.Name = 'taGoodsSamSNAME_SAM' then
        begin
         dm.quTmp.Close;
         dm.quTmp.SQL.Text:='select count(*) from d_goods_sam where sname_sam ='''
                              + dmcom.taGoodsSamSNAME_SAM.AsString+'''';
         dm.quTmp.ExecQuery;
         If (dm.quTmp.Fields[0].AsInteger>1) then
          begin
           dmCom.taGoodsSam.Close;
           dmCom.taGoodsSam.SelectSQL[3]:='Order by SName_Sam ';
           dmCom.taGoodsSam.Open;
           dm.quTmp.Close;
           MessageDialog('��� ������ ����� ���������� ��������', mtWarning, [mbOk], 0);
           j:=j-1;
          end else dm.quTmp.Close;
         end;

        Active:=False;
        Open;
        Locate('D_GOODSID_SAM', i, []);
        ActiveControl:=dg1;
        dg1.SelectedIndex:=j+1;
      end
end;

procedure Tfmgoods_sam.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsGoodsSam, 62); //'gd'
end;

procedure Tfmgoods_sam.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100109)
end;

procedure Tfmgoods_sam.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
