unit WH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,  M207Proc, ComCtrls,
  StdCtrls, ExtCtrls, db, Menus, DBCtrls, Buttons, RXCtrls, Mask,
  Variants, CheckLst, ActnList, DBGridEh, RxStrUtils, TB2Item,
  jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  TfmWH = class(TForm)
    FormStorage1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    stbtWh: TStatusBar;
    ppWH: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    spitPrint: TSpeedItem;
    pmPrint: TPopupMenu;
    mnitPrintArt: TMenuItem;
    mnitInv: TMenuItem;
    mnitPrintUID: TMenuItem;
    spitHist: TSpeedItem;
    spitCurrStArt: TSpeedItem;
    spitAppl: TSpeedItem;
    pmAppl: TPopupMenu;
    mnitCrAppl: TMenuItem;
    mnitClearAppl: TMenuItem;
    mnitShowAppl: TMenuItem;
    mnitAddAppl: TMenuItem;
    N4: TMenuItem;
    mnitLayoutArt: TMenuItem;
    mnitLayoutUID: TMenuItem;
    plWh: TPanel;
    lbWHInfo: TLabel;
    txtArt: TDBText;
    txtQ: TDBText;
    Label2: TLabel;
    pgctrl: TPageControl;
    tbshUID: TTabSheet;
    ibgrWHUID: TM207IBGrid;
    tbshSz: TTabSheet;
    grSz: TM207IBGrid;
    tbshArt2: TTabSheet;
    tbshInv: TTabSheet;
    dbgrWHInv: TM207IBGrid;
    spltWH: TSplitter;
    lbW: TLabel;
    txtW: TDBText;
    spbr4: TSpeedBar;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    chlbMat: TCheckListBox;
    chlbGood: TCheckListBox;
    chlbProd: TCheckListBox;
    chbxInfo: TCheckBox;
    lbFind: TLabel;
    edArt: TComboEdit;
    ibgrA2: TM207IBGrid;
    spcalc: TSpeedItem;
    ActionList1: TActionList;
    acCalc: TAction;
    lblArt: TLabel;
    lblQ: TLabel;
    lblW: TLabel;
    ibgrWH: TDBGridEh;
    pmappldep: TTBPopupMenu;
    acApplDepCreate: TAction;
    acApplDepClear: TAction;
    biApplDepCreate: TTBItem;
    biApplDepClear: TTBItem;
    siHelp: TSpeedItem;
    Label5: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siArtClick(Sender: TObject);
    procedure siUIDClick(Sender: TObject);
    procedure siSzClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtChange(Sender: TObject);
    procedure spitInvClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnDoClick(Sender: TObject);
    procedure mnitPrintArtClick(Sender: TObject);
    procedure spitHistClick(Sender: TObject);
    procedure ibgrWH1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spitCurrStArtClick(Sender: TObject);
    procedure mnitClearApplClick(Sender: TObject);
    procedure mnitCrApplClick(Sender: TObject);
    procedure mnitShowApplClick(Sender: TObject);
    procedure ppWHPopup(Sender: TObject);
    procedure mnitInvClick(Sender: TObject);
    procedure mnitAddApplClick(Sender: TObject);
    procedure mnitPrintUIDClick(Sender: TObject);
    procedure dtedCalcDateAcceptDate(Sender: TObject; var ADate: TDateTime;
      var Action: Boolean);
    procedure ibgrWH1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure chbxInfoClick(Sender: TObject);
    procedure ibgrWHUIDGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure grSzGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure ibgrA2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dbgrWHInvGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure pgctrlChange(Sender: TObject);
    procedure edArtButtonClick(Sender: TObject);
    procedure edArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure chlbProdClickCheck(Sender: TObject);
    procedure chlbMatClickCheck(Sender: TObject);
    procedure chlbGoodClickCheck(Sender: TObject);
    procedure SpeedItem8Click(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure chlbMatExit(Sender: TObject);
    procedure spcalcClick(Sender: TObject);
    procedure siExitClick0(Sender: TObject);
    procedure ibgrWHGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure ibgrWHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acApplDepCreateExecute(Sender: TObject);
    procedure acApplDepClearExecute(Sender: TObject);
    procedure acApplDepCreateUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    FSearchEnable : boolean;
    FStopArtSearch : boolean;
    FSearchingArt : boolean;
    LogOprIdForm:string;

    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    function GetWHDate: TDateTime;
    procedure SetWHDate(const Value: TDateTime);
    function  ChlbClickCheck(chlb:TCheckListBox;var st:string):variant;
  public
    property WHDate : TDateTime read GetWHDate write SetWHDate;
    function GetIdxByCode(sl : TStringList; Code : Variant) : integer;
    function CheckMulti(chlb:TCheckListBox;var st:string; lCanModif:boolean=false):boolean;
  end;

var
  fmWH: TfmWH;

implementation

uses comdata, Data, WHArt2, WHUID, WHSZ, Data2, ServData,
     WHInv, ReportData, pFIBDataSet, Period, CurrStArt, Appl, HistArt,
     dbTree,UIDWH, Data3, FIBQuery, dbUtil, MsgDialog, uUtils;

{$R *.DFM}

{$R img.res}

function Dep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;

procedure TfmWH.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmCom.FilterArt := '';
  PostDataSet(dmServ.taWH);
  if dmCom.tr.Active then  dmCom.tr.CommitRetaining;
  ibgrWH.SaveColumnsLayoutIni(GetIniFileName, Name+'_ibgrWH', true);  
  CloseDataSets([dmServ.taWH]);
end;

procedure TfmWH.FormCreate(Sender: TObject);
var
  i : integer;
begin

  tb1.WallPaper:=wp;
  spbr4.WallPaper:=wp;
  Screen.Cursor := crSQLWait;
  with dmCom, dmServ do
  try
    if tr.Active then dmCom.tr.CommitRetaining;
    if not tr.Active then tr.StartTransaction;

    (* ���������� ������ ������ :)) �������� *)
    edArt.Text := dmCom.FilterArt;
    dmServ.WHDepId := -1;
    dm.WorkMode := 'WH';
    dm.WHSZMode := 1;

    dm.SetBeginDate;

    dm.LoadArtSL(ALL_DICT);

    NoAppl:=-1;

    D_CountryId:='*';
    with dmCom, dm, dm2 do
    begin
      dm.LoadArtSL(ALL_DICT);
      D_MatId:='*';
      D_GoodId:='*';
      D_CompId:=-1;
      D_SupId := -1;
      SD_MatID:='';
      SD_GoodID:='';
      SD_SupID:='';
      SD_CompID:='';
      lMultiSelect:=false;
      lSellSelect:=false;
      AllUIDWH:=true;

      chlbProd.Items.Assign(dm.slProd);
      chlbProd.ItemIndex := GetIdxByCode(dm.slProd, D_CompId);
      chlbProd.Checked[chlbProd.ItemIndex]:=true;
      chlbProdClickCheck(chlbProd);

      chlbMat.Items.Assign(dm.slMat);
      chlbMat.ItemIndex := GetIdxByCode(dm.slMat, D_MatId);
      chlbMat.Checked[chlbMat.ItemIndex]:=true;
      chlbMatClickCheck(chlbMat);

      chlbGood.Items.Assign(dm.slGood);
      chlbGood.ItemIndex := GetIdxByCode(dm.slGood, D_GoodId);
      chlbGood.Checked[chlbGood.ItemIndex]:=true;
      chlbGoodClickCheck(chlbGood);
    end;

    for i:=0 to Pred(taWH.Fields.Count) do
      if taWH.Fields[i].FieldKind = fkCalculated then
       with ibgrWH.Columns.Add do
       begin
         FieldName := taWH.Fields[i].FieldName;
         if (ExtractWord(1,taWH.Fields[i].FieldName,['_'])='RW') then
          Title.Caption := '������� �� ���������|���|'+taWH.Fields[i].DisplayName
         else Title.Caption := '������� �� ���������|���-��|'+taWH.Fields[i].DisplayName;
         Title.Font.Color := clNavy;
         ReadOnly := True;
      end;
    taWH.Open;
    lblArt.Caption := dmServ.TA;
    lblQ.Caption := dmServ.TQ;
    lblW.Caption := dmServ.TW;

  finally
    Screen.Cursor := crDefault;
  end;

  spitHist.Visible := centerDep;
  spitAppl.Visible := not centerDep;
  ibgrWH.FieldColumns['ZQ'].Visible := false; //centerDep;
  ibgrWHUID.ColumnByName['SPRICE'].Visible := centerDep;
  dbgrWHInv.ColumnByName['SPRICE'].Visible := centerDep;

  chbxInfoClick(Sender);
  ibgrWH.RestoreColumnsLayoutIni(GetIniFileName, Name+'_ibgrWH', [crpColIndexEh, crpColWidthsEh]);
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

function TfmWH.CheckMulti(chlb:TCheckListBox; var st:string; lCanModif:boolean=false):boolean;
var i:integer;
    s:string;
begin
  Result:=False;
  st:='';s:='';
  if chlb.Checked[0] then
   begin
    for i:=1 to chlb.Items.Count-1 do
      chlb.Checked[i]:=False;
    st:='';
   end
  else
   begin
    for i:=0 to chlb.Items.Count-1 do
     if chlb.Checked[i] then
     begin
       s:=TNodeData(chlb.Items.Objects[I]).Code;
       if lCanModif then s:=#39+s+#39;
       st:=st+s+',';
     end;
   delete(st,length(st),1);
   if st<>'' then Result:=true;
//   if pos(',',st)<=0 then st:='';
   end;
end;

function TfmWH.ChlbClickCheck(chlb:TCheckListBox;var st:string):variant;
var i:integer;
begin
  i:=chlb.ItemIndex;
  WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
  if chlb.Checked[i] then
    begin
     st:=chlb.Items[i];
     if (i<>0) then     chlb.Checked[0]:=false;
    end
  else
    begin
     i:=chlb.Items.Count-1;
     WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
     st:=chlb.Items[i];
     chlb.Checked[i]:=true;
    end;
  Result:=TNodeData(chlb.Items.Objects[i]).Code;
end;

procedure TfmWH.chlbProdClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_CompId:=ChlbClickCheck(chlbProd,s);
 SpeedItem8.BtnCaption:=s;
end;

procedure TfmWH.chlbMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_MatId:=ChlbClickCheck(chlbMat,s);
 SpeedItem10.BtnCaption:=s;
end;

procedure TfmWH.chlbGoodClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GoodId:=ChlbClickCheck(chlbGood,s);
 SpeedItem9.BtnCaption:=s;
end;

procedure TfmWH.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWH.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmWH.siArtClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmWHArt2, Self, TForm(fmWHArt2), True, False);
end;

procedure TfmWH.siUIDClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmWHUID, Self, TForm(fmWHUID), True, False)
end;

procedure TfmWH.siSzClick(Sender: TObject);
begin
  dm.WHSZMode:=1;
  ShowAndFreeForm(TfmWHSZ, Self, TForm(fmWHSZ), True, False)
end;

procedure TfmWH.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var art: string;
begin
  case key of
    VK_RETURN :
      try
        Screen.Cursor := crSQLWait;
        art := trim(edArt.Text);
{        if art <> '' then
        begin}
          edArt.Glyph.Handle := LoadBitmap(hInstance, 'STOP');
          FStopArtSearch := False;
          FSearchingArt := True;
{          if not LocateF(dmServ.taWH, 'Art', Art, [], False, FStopArtSearchFunc)
          then if not FStopArtSearch then ShowMessage('������� �� ������');}
         dmCom.FilterArt := edArt.Text;
         ReOpenDataSets([dmServ.taWH]);
         lblArt.Caption := dmServ.TA;
         lblQ.Caption := dmServ.TQ;
         lblW.Caption := dmServ.TW;

//        end;
      finally
        edArt.Glyph.Handle := LoadBitmap(hInstance, 'FIND');
        FSearchingArt := False;
        Screen.Cursor := crDefault;
//        ActiveControl := ibgrWH;
//        Activecontrol.SetFocus;
//        ibgrWH.SelectedRows.CurrentRowSelected;

      end;
{       try
         Screen.Cursor := crSQLWait;
         dmCom.FilterArt := edArt.Text;
         ReOpenDataSets([dmServ.taWH]);
       finally
         Screen.Cursor := crDefault;
       end;}
  end;
end;

procedure TfmWH.edArtChange(Sender: TObject);
begin
  if FSearchEnable then
    dmServ.taWH.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
 end;

procedure TfmWH.spitInvClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmWHInv, Self, TForm(fmWHInv), True, False);
end;

procedure TfmWH.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F12 : if Shift = [] then Close;
  end;
end;

procedure TfmWH.btnDoClick(Sender: TObject);
var l1,l2,l3:boolean;
    s1,s2,s3: string;
begin
  Application.Minimize;
  l1:=CheckMulti(chlbProd,s1);
  if l1 then dmCom.D_CompId:=-1;
  l2:=CheckMulti(chlbMat,s2,true);
  if l2 then dmCom.D_MatId:='*';
  l3:=CheckMulti(chlbGood,s3,true);
  if l3 then dmCom.D_GoodId:='*';
  dm.lMultiSelect:=l1 or l2 or l3;
  dm.SD_CompId := s1;
  dm.SD_MatId := s2;
  dm.SD_GoodId := s3;

{  CheckCalcing;}
  ReOpenDataSets([dmServ.taWH]);
  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbProd,False);
  Application.Restore;
  lblArt.Caption := dmServ.TA;
  lblQ.Caption := dmServ.TQ;
  lblW.Caption := dmServ.TW;

end;

procedure TfmWH.mnitPrintArtClick(Sender: TObject);
begin
  dmReport.PrintLayout(art_all);
end;

procedure TfmWH.spitHistClick(Sender: TObject);
begin
  dmServ.Mode := '������� ��������. �����';
  dmServ.HistDepId :=  dmServ.WHDepId ;// dm.SDepId;
  dmServ.HistArtId := dmServ.taWHD_ARTID.AsInteger;
  ShowAndFreeForm(TfmHistArt, Self, TForm(fmHistArt), True, False);
end;

procedure TfmWH.ibgrWH1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_UP : if ssAlt in Shift then
            begin
              ActiveControl := edArt;
              edArt.SelectAll;
              ibgrWH.DataSource.DataSet.Next;
            end;
  end;
end;

procedure TfmWH.spitCurrStArtClick(Sender: TObject);
begin
 { if not Assigned(ibgrWH.SelectedField) then
    if dm.IsInArr(dm.arrWHDepID, ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    then dmServ.WHDepId := StrToInt(ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    else dmServ.WHDepId := -1
  else  dmServ.WHDepId := -1;}
  ShowAndFreeForm(TfmCurrStArt, Self, TForm(fmCurrStArt), True, False);
end;

procedure TfmWH.mnitClearApplClick(Sender: TObject);
begin
  Screen.Cursor := crSQLWait;
  Application.Minimize;
  dmServ.NoAppl:=-1;
  with dm.quTmp do
   try
    Close;
    SQL.Text := 'update d_Art set zq=0 where zq>0 ' ;//where UserId='+IntToStr(dmcom.UserId);
    ExecQuery;
    Close;
    dmcom.tr.CommitRetaining;
    if not dmcom.tr.Active then dmcom.tr.StartTransaction;
    ReOpenDataSets([dmServ.taWH]);
   finally
    Screen.Cursor := crDefault;
    Application.Restore;
   end;
end;

procedure TfmWH.mnitCrApplClick(Sender: TObject);
var LogOperationID:string;
begin
 with dmServ, quTmp do
  begin
    LogOperationID:=dm3.insert_operation('������������ ������',LogOprIdForm);
    Close;
    Transaction.CommitRetaining;
    if not Transaction.Active then Transaction.StartTransaction;
    if NoAppl<0 then
      begin
       Noappl:=dmCom.GetId(31);
       SQL.Text := 'insert into appl(applId, supid, isactive, IsClosed, UserId) '+
         ' values ('+inttostr(Noappl)+','+inttostr(dmCom.D_CompId)+', 0, 0, '+inttostr(dmCom.UserId)+')';
      end
    else
       SQL.Text := 'delete from aitem where applid='+inttostr(Noappl);
    ExecQuery;
    Close;
    Transaction.CommitRetaining;
    SQL.Text := 'insert into aItem(AITEMID,APPLID,ARTID,Q) select GEN_ID(gen_aitem_id,1),'+
    inttostr(Noappl)+', d_artid, zq from d_art where zq>0 ';
    ExecQuery;
    Transaction.CommitRetaining;
    dmcom.tr.CommitRetaining;
    dm3.update_operation(LogOperationID);    
    MessageDialog('������ ������������', mtInformation, [mbOk], 0);
 //   ShowAndFreeForm(TfmAppl, Self, TForm(fmAppl), True, False);
  end;
end;

procedure TfmWH.mnitShowApplClick(Sender: TObject);
var LogOperationID:string;
begin
  if dmServ.NoAppl = -1 then
    with dm.quTmp do
    begin
      Close;
      SQL.Text := 'select max(ApplId) from Appl where Supid is null ';
      ExecQuery;
      if Fields[0].IsNull then
      begin
        Close;
        raise Exception.Create('������ ���!');
      end
      else begin
        dmServ.NoAppl := Fields[0].AsInteger;
        Close;
        LogOperationID:=dm3.insert_operation('�������� ������',LogOprIdForm);
        ShowAndFreeForm(TfmAppl, Self, TForm(fmAppl), True, False);
        dm3.update_operation(LogOperationID);
      end
    end
  else begin
   LogOperationID:=dm3.insert_operation('�������� ������',LogOprIdForm);
   ShowAndFreeForm(TfmAppl, Self, TForm(fmAppl), True, False);
   dm3.update_operation(LogOperationID);   
  end
end;

procedure TfmWH.ppWHPopup(Sender: TObject);
begin
  {if not Assigned(ibgrWH.SelectedField) then
    if dm.IsInArr(dm.arrWHDepID, ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    then dmServ.WHDepId := StrToInt(ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    else dmServ.WHDepId := -1
  else  dmServ.WHDepId := -1;}
end;

procedure TfmWH.mnitInvClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmWHInv, Self, TForm(fmWHInv), True, False)
end;

procedure TfmWH.mnitAddApplClick(Sender: TObject);
begin
  with dm.quTmp do
  begin
    if dmServ.NoAppl = -1 then
    begin
      Close;
      SQL.Text := 'select max(NoAppl) from Appl';
      ExecQuery;
      if RecordCount = 0 then
      begin
        Close;
        raise Exception.Create('������ ���!');
      end
      else begin
        dmServ.NoAppl := Fields[0].AsInteger;
        Close;
      end;
    end;
    Close;
    SQL.Text := 'execute procedure Appl_I('+IntToStr(dmcom.UserId)+', 0, 0, '+
        IntToStr(dmServ.NoAppl)+')';
    ExecQuery;
    Close;
    dmcom.tr.CommitRetaining;
  end;
end;

procedure TfmWH.mnitPrintUIDClick(Sender: TObject);
begin
  {if not Assigned(ibgrWH.SelectedField) then
    if dm.IsInArr(dm.arrWHDepID, ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    then dmServ.WHDepId := StrToInt(ibgrWH.Columns[ibgrWH.SelectedIndex].FieldName)
    else dmServ.WHDepId := -1
  else  dmServ.WHDepId := -1;}
  dmReport.PrintLayout(uid_all);
end;

function TfmWH.GetWHDate: TDateTime;
begin
  with dmServ.quTmp do
  begin
    Close;
    SQL.Text := 'select WHDate from D_Emp where D_EmpId='+IntToStr(dmCom.UserId);
    ExecQuery;
    Result := Fields[0].AsDateTime;
    Close;
  end;
end;

procedure TfmWH.SetWHDate(const Value: TDateTime);
begin
  with dmServ.quTmp do
  begin
    Close;
    SQL.Text := 'update D_Emp set WHDate ='+DateToStr(Value)+' where D_EmpId='+IntToStr(dmCom.UserId);
    ExecQuery;
    Close;
  end;
end;

procedure TfmWH.dtedCalcDateAcceptDate(Sender: TObject;
  var ADate: TDateTime; var Action: Boolean);
begin
  Action := True;
  with dmServ, quWH_S2 do
  begin
    Transaction.CommitRetaining;
    Close;
    Params[0].AsInteger := dmCom.UserId;
    Params[1].AsInteger := 1;
    Params[2].AsTimeStamp := DateTimeToTimeStamp(ADate);
    ExecQuery;
    ReOpenDataSets([taWH]);
  end;
end;

procedure TfmWH.ibgrWH1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field.FieldKind = fkCalculated then Background := dmCom[Field.Tag].Color;
  if (Field<>NIL) and (Field.FieldName='NDSNAME') then
//     Background:=clBtnFace or ($FF shl dmSErv.taWHNDSID.AsInteger);
  if  dmSErv.taWHNDSID.AsInteger=0 then Background:=clBtnFace else Background:=clBlue;
end;

procedure TfmWH.chbxInfoClick(Sender: TObject);
begin
  if chbxInfo.Checked then
  begin
    pgctrl.Visible := True;
    spltWH.Visible := True;
    pgctrlChange(Sender);
  end
  else begin
    pgctrl.Visible := False;
    spltWH.Visible := False;
  end;
end;

procedure TfmWH.ibgrWHUIDGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
var i:integer;   
begin
  if (Highlight or ibgrWHUID.ClearHighlight) and (dmServ.quWHUIDDEPID.AsInteger >1)
  then Background := dmCom[dmServ.quWHUIDDEPID.AsInteger].Color
  else Background := clHighlight;
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
  if (Field.FieldName='APPLDEPQ') then
  begin
   if not dmserv.quWHUIDUID.IsNull then i:=dmserv.quWHUIDUID.AsInteger else i:=-1;
   if i<>-1 then
   with dm, qutmp do
   begin
     close;
     sql.Text:='select uid from appldep_item where uid='+inttostr(i);
     ExecQuery;
     if not Fields[0].IsNull then Background:=clYellow else Background:=clInfoBk;
     Transaction.CommitRetaining;
     close;
   end;
  end;
end;

procedure TfmWH.grSzGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight or grSz.ClearHighlight
  then Background := dmCom[dmServ.quWHSZDEPID.AsInteger].Color
  else Background:=clHighlight;
  if Assigned(Field)and(Field.FieldName='SZ') then Background := dmCom.clMoneyGreen;
  if Assigned(Field)and(Field.FieldName='APPL')then Background := clInfoBk;
end;

procedure TfmWH.ibgrA2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight or ibgrA2.ClearHighlight
  then Background := dmCom[dmServ.quWHArt2D_DEPID.AsInteger].Color
  else Background := clHighlight;
  if (Field<>NIL) and (Field.FieldName='ART2') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmWH.dbgrWHInvGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Assigned(Field) and (Field.FieldName='ART2')
  then Background:=dmCom.clMoneyGreen
end;

procedure TfmWH.pgctrlChange(Sender: TObject);

  procedure Open(ds : TpFIBDataSet);
  begin
    if ds.Active then eXit;
    Screen.Cursor := crSQLWait;
    try
      ds.Open;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

begin
  case pgctrl.ActivePage.Tag of
    1 : Open(dmServ.quWHSZ);
    2 : Open(dmServ.quWHArt2);
    3 : Open(dmServ.qrWHInv);
    4 : Open(dmServ.quWHUID);
  end;
end;

function TfmWH.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      eXit;
    end;
end;

procedure TfmWH.edArtButtonClick(Sender: TObject);
var
  Key : word;
begin
  Key := VK_RETURN;
  edArtKeyDown(Sender, Key, []);
end;

procedure TfmWH.edArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_DOWN : begin
      ActiveControl := ibgrWH;
      ActiveControl.SetFocus;
    end;
  end;
end;

procedure TfmWH.SpeedItem8Click(Sender: TObject);
begin
   dm.SetVisEnabled(chlbProd,True);
   chlbProd.SetFocus;
end;

procedure TfmWH.SpeedItem9Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGood,True);
  chlbGood.SetFocus;
end;

procedure TfmWH.SpeedItem10Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbMat,True);
  chlbMat.SetFocus;
end;

procedure TfmWH.chlbMatExit(Sender: TObject);
begin
   dm.SetVisEnabled(TWinControl(sender),False);
end;

procedure TfmWH.spcalcClick(Sender: TObject);
var LogOperationID:string;
    i:integer;
begin
 Screen.Cursor:=crSQLWait;
 LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
 with dmcom,dm, qutmp do
 begin
  if CenterDep then
   begin
    close;   
    SQL.Text:='select max(d_depid) from d_dep ';
    ExecQuery;
    i:=Fields[0].AsInteger+1;
    tr.CommitRetaining;
    close;
    ExecSQL('execute procedure FILLFUIDREST(0, '+inttostr(i)+') ', dmCom.quTmp);
   end
  else
    ExecSQL('execute procedure FILLFUIDREST('+IntToStr(SelfDepId)+', '+IntToStr(SelfDepId)+') ', dmCom.quTmp);
  ExecSQL('EXECUTE PROCEDURE WH_fill', dmCom.quTmp);
 end;
 ReOpenDataSets([dmServ.taWH]);
 dm3.update_operation(LogOperationID); 
 Screen.Cursor:=crDefault;
end;

procedure TfmWH.siExitClick0(Sender: TObject);
begin
 close;
end;

procedure TfmWH.ibgrWHGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if Column.Field.FieldKind = fkCalculated then Background := dmCom[Column.Field.Tag].Color;
  if (Column.Field<>NIL) and (Column.Field.FieldName='NDSNAME') then
//     Background:=clBtnFace or ($FF shl dmSErv.taWHNDSID.AsInteger);
  if  dmSErv.taWHNDSID.AsInteger=0 then Background:=clBtnFace else Background:=clBlue;
end;

procedure TfmWH.ibgrWHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_UP : if ssAlt in Shift then
            begin
              ActiveControl := edArt;
              edArt.SelectAll;
              ibgrWH.DataSource.DataSet.Next;
            end;
  end;
end;

procedure TfmWH.acApplDepCreateExecute(Sender: TObject);
begin
 PostDataSet(dmServ.quWHUID);
 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select nouid from Create_ApplDep_Dep ('+inttostr(dmcom.User.UserId)+')';
 dm.quTmp.ExecQuery;
 if trim(dm.quTmp.Fields[0].AsString)<>'' then
  MessageDialog('�� ��������� ������� �� ����� ���� ������� ������ �� �������: '+trim(dm.quTmp.Fields[0].AsString), mtWarning, [mbOk], 0);
 dm.quTmp.Transaction.CommitRetaining;
 dm.quTmp.Close;
 MessageDialog('������ �� ������� �����������!', mtinformation, [mbOk], 0);
 ReOpenDataSet(dmserv.quWHUID);
end;

procedure TfmWH.acApplDepClearExecute(Sender: TObject);
begin
 PostDataSet(dmServ.quWHUID);
 if MessageDialog('�������� ������ �� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
  ExecSQL('update fuidrest set appldepq=null where appldep_user='+inttostr(dmcom.User.UserId)+' and appldepq is not null', dmCom.quTmp);
  ReOpenDataSet(dmserv.quWHUID);
  MessageDialog('���� "������" �������!', mtInformation, [mbOk], 0);
 end
end;

procedure TfmWH.acApplDepCreateUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=not CenterDep;
end;

procedure TfmWH.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100302)
end;

procedure TfmWH.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.

