unit JSz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, StdCtrls, Menus, CheckLst, Buttons, Math,
  DBGridEh, DBCtrls,Variants, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmJSz = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitCalc: TSpeedItem;
    stbrJSz: TStatusBar;
    fmstrJsz: TFormStorage;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    spExport: TSpeedItem;
    pmPrint: TPopupMenu;
    mnitPrintArt: TMenuItem;
    mnitPrintZ: TMenuItem;
    pmJSz: TPopupMenu;
    nDouble: TMenuItem;
    pmText: TPopupMenu;
    miVed: TMenuItem;
    miZ: TMenuItem;
    nAllz: TMenuItem;
    svdFile: TOpenDialog;
    spbr4: TSpeedBar;
    lbMat: TLabel;
    lbGood: TLabel;
    lbComp: TLabel;
    lbSup: TLabel;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem8: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    laDep: TLabel;
    chbSup: TCheckBox;
    plTotal: TPanel;
    grTotal: TDBGridEh;
    chbxInfo: TCheckBox;
    lAll: TLabel;
    Panel1: TPanel;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    gr: TDBGridEh;
    chlbMat: TCheckListBox;
    chlbGrMat: TCheckListBox;
    chlbGood: TCheckListBox;
    chlbProd: TCheckListBox;
    cbIns: TCheckBox;
    siHelp: TSpeedItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    g1: TProgressBar;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure spitCalcClick(Sender: TObject);
    procedure grOldGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitPrintClick(Sender: TObject);
    procedure spitPeriodClick(Sender: TObject);
    procedure spExportClick(Sender: TObject);
    procedure chbSupClick(Sender: TObject);
    procedure miZClick(Sender: TObject);
    procedure nAllzClick(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
    procedure SpeedItem8Click(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure chlbMatExit(Sender: TObject);
    procedure chlbMatClickCheck(Sender: TObject);
    procedure chlbGrMatClickCheck(Sender: TObject);
    procedure chlbProdClickCheck(Sender: TObject);
    procedure chlbGoodClickCheck(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure grOldKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siExitClick(Sender: TObject);
    procedure grTotalGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure chbxInfoClick(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure cbInsClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
  private
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    function GetIdxByCode(sl: TStringList; Code: Variant): integer;
    procedure ShowPeriod;
    procedure CalcAnlz;
    procedure SaveAppl;
    procedure SaveInfo;
    procedure GetOParam(chlb:TCheckListBox; InStr: string; var OutDig: variant);
    procedure GetApplID(sourceStr: string);
    function  SetApplID: string;
    procedure CheckEdtAppl;
  public
    function ChlbClickCheck(chlb:TCheckListBox;var st:string):variant;
  end;
type
  OArr = array of string;
var
  fmJSz: TfmJSz;
  Appl_id: OArr;
  countAppl: integer;
  Flag_Per:boolean;
  IsCDM: boolean;
  ProcName: string;
  appStr: string;
  GrMat,Mat,Goods, Suppl: OArr;
  GrSz, MatSz, GoodsSz, SupplSz: integer;
  GrMatAll, MatAll, GoodsAll, SupplAll: boolean;

procedure CheckMultiContr(chlb:TCheckListBox;var arr: OArr;
                                            var sz: integer; var IsAll:boolean);
function SetAParam(arr: OArr; arrSz: integer; IsAll: boolean): string;

implementation

uses comdata, Data, ServData, M207Proc, ReportData, Period, RxDateUtil,RxStrUtils, dbTree,
   Data2, dbUtil, NewArt, Data3, JewConst, MsgDialog, uUtils;

{$R *.DFM}

procedure TfmJSz.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmJSz.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

function TfmJSz.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      exit;
    end;
end;
procedure TfmJSz.GetOParam(chlb:TCheckListBox; InStr: string; var OutDig: variant);
var i: integer;
    lem: string;
    IsFirst: boolean;
begin
  lem := '';
  IsFirst := true;
  for i:=1 to length(InStr) do
  begin
    if InStr[i] <> ',' then
       lem := lem + InStr[i]
    else
    begin
      chlb.ItemIndex := GetIdxByCode(TStringList(chlb.Items),lem);
      chlb.Checked[chlb.ItemIndex]:=true;
      if IsFirst then
      begin
       OutDig := lem;
       IsFirst := false;
      end;
      lem := '';
    end
 end;
  if lem = '*' then
     chlb.ItemIndex := 0
   else
     chlb.ItemIndex := GetIdxByCode(TStringList(chlb.Items),lem);
  chlb.Checked[chlb.ItemIndex]:=true;
  if IsFirst then
  begin
   OutDig := lem;
  end;

end;


procedure TfmJSz.GetApplID( sourceStr: string);
var i: integer;
    lem: string;
    cnt: integer;
begin

  if sourceStr <> '*' then
  begin
    lem := '';
    cnt := 1;
    for i:=1 to length(sourceStr) do
    begin
      if sourceStr[i] <> ',' then
         lem := lem + sourceStr[i]
      else
      begin
        SetLength(Appl_id, cnt);
        Appl_id[cnt-1] := lem;
        cnt := cnt + 1;
        lem := '';
      end
   end;
   SetLength(Appl_id, cnt);
   Appl_id[cnt-1] := lem;
   countAppl := cnt;
  end;

end;
procedure TfmJSz.FormCreate(Sender: TObject);
var
  tmpBD, tmpED: TDateTime;
  tmpIsSz, tmpGr, tmpMat, tmpGoods, tmpComp, tmpApplID: string;
  FGr, FMat, FGoods, FComp: variant;
  i: integer;
  c: TColumnEh;
begin
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  Appl_ID := nil;

  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;
  spbr4.Wallpaper := wp;
  
  stbrJSz.Panels[0].Text := dmCom.UserName;

  with dmServ.quTmp do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    Close;
    SQL.Text := 'select BD,ED,D_MATID,D_GRMATID, D_GOODID,D_COMPID,ISSZ, D_Appl ' +
                'from GetParamsLastAlz(' + IntToStr(dmCom.UserId) + ')';
    ExecQuery;
    tmpBD := Fields[0].AsDate;
    tmpED := Fields[1].AsDate;
    tmpMat:= trim(Fields[2].AsString);
    tmpGr := trim(Fields[3].AsString);
    tmpGoods := trim(Fields[4].AsString);
    tmpComp := trim(Fields[5].AsString);
    tmpIsSz := trim(Fields[6].AsString);
    tmpApplID := trim(Fields[7].AsString);
    Close;
    Transaction.CommitRetaining;
  end;

  if not IsCDM then
  begin
    dmServ.L3BD:=tmpBD;//dmCom.User.L3BD;
    dmServ.L3ED:=tmpED; //dmCom.User.L3ED;
  end
  else
  begin
    tmpMat := '*';
    tmpGr := '*';
    tmpGoods := '*';
    tmpComp := IntTOStr(GetIdxByCode(dm.slProd,dmCom.D_CompId));
  end;

  dmServ.GrMat:='';

  ShowPeriod;
(*****************����������, ���������, ���� ����������, ������*****)

  with dmCom, dm, dm2 do
    begin
      lMultiSelect:=false;
      lSellSelect:=false;
      AllUIDWH:=true;
      chlbProd.MultiSelect := True;
      chlbGrMat.MultiSelect := True;
      chlbMat.MultiSelect := True;
      chlbGood.MultiSelect := True;

      chlbProd.Items.Assign(dm.slProd);
      GetOParam(chlbProd,tmpComp,FComp);
      chlbProdClickCheck(chlbProd);

      chlbGrMat.Items.Assign(dm.slGrMat);
      GetOParam(chlbGrMat,tmpGr,FGr);
      chlbGrMatClickCheck(chlbGrMat);

      chlbMat.Items.Assign(dm.slMat);
      GetOParam(chlbMat,tmpMat,FMat);
      chlbMatClickCheck(chlbMat);

      chlbGood.Items.Assign(dm.slGood);
      GetOParam(chlbGood,tmpGoods,FGoods);
      chlbGoodClickCheck(chlbGood);

    end;
(********************************************************************)

  with dmServ do
  begin
    for i:=0 to Pred(taJSz.Fields.Count) do
      if taJSz.Fields[i].FieldKind = fkCalculated then
       with gr.Columns.Add do
       begin
         FieldName := taJSz.Fields[i].FieldName;
         Title.Caption := taJSz.Fields[i].DisplayName;
         Title.Font.Color := clNavy;
         ReadOnly := True;
//         if Pos('RQ_', FieldName)<>0 then Visible := False;
      end;

  end;
  CheckMultiContr(chlbProd,Suppl, SupplSz,SupplAll);
  CheckMultiContr(chlbGrMat,GrMat, GrSz, GrMatAll);
  CheckMultiContr(chlbMat,Mat,MatSz, MatAll);
  CheckMultiContr(chlbGood,Goods,GoodsSz, GoodsAll);

  for i:=0 to Pred(dmCom.DepCount) do
    begin
      c:=gr.Columns.Add;
      c.Field:= dmServ.taJSz.FieldByName('RW_' + IntToStr(dmcom.DepInfo[i].DepId));
      c.Width:=40;

      c:=gr.Columns.Add;
      c.Field:=dmServ.taJSz.FieldByName('RQ_' + IntToStr(dmcom.DepInfo[i].DepId));
      c.Width:=40;
    end;

  for i:=0 to Pred(dmCom.DepCount) do
    begin
      c:=gr.Columns.Add;
      c.Field:= dmServ.taJSz.FieldByName('RW1_' + IntToStr(dmcom.DepInfo[i].DepId));
      c.Width:=40;

      c:=gr.Columns.Add;
      c.Field:=dmServ.taJSz.FieldByName('RQ1_' + IntToStr(dmcom.DepInfo[i].DepId));
      c.Width:=40;
    end;
  AppStr := tmpApplID;

  GetApplID(tmpApplID);
 dmServ.IsEdtAppl := False;
 dmServ.IsIns :=0;
 if tmpIsSz = '1' then
   chbSup.Checked := True
 else
 begin
   chbSup.Checked := False;
   dmServ.SzMode :=1;
 end;
 cbIns.Checked := True;

 gr.RestoreColumnsLayoutIni(GetIniFileName, Name+'_gr', [crpColIndexEh, crpColWidthsEh]);

end;

procedure TfmJSz.SaveAppl;
var
    app_str: string;
begin
  if (appl_id = nil) or (appl_id[0]='*') or (appl_id[0]='')  then  app_str :=''
  else app_str := SetApplID;

  with dmServ.quTmp do
  begin
    if not Transaction.Active then Transaction.StartTransaction;

    Close;
    SQL.Text := 'select applstr from SaveAppl(' +IntToStr(dmCom.UserId) +
      ',' + IntToStr(dmServ.SzMode)+ ',''' + app_str + ''')' ;
    ExecQuery;
    AppStr :=Fields[0].AsString;
//    AppStr := Trim(AppStr);
    Close;
    Transaction.CommitRetaining;
  end;
  GetApplID(Appstr);
end;

function SetAParam(arr: OArr; arrSz: integer; IsAll: boolean): string;
var i: integer;
    tmpstr: string;
begin
  tmpstr := '';
  if IsAll then tmpstr := '*'
  else
  begin
    for i := 0 to arrSz - 1 do
      tmpstr := tmpstr + arr[i] + ',' ;
    delete(tmpstr,length(tmpstr),1);
  end;
  RESULT := tmpstr;
end;
function TfmJSz.SetApplID: string;
var i: integer;
    tmpstr : string;
begin
  tmpstr := '';
  for i:=0 to countAppl-1 do
        tmpstr := tmpstr + appl_id[i] + ',';
  delete(tmpstr,length(tmpstr),1);
  if tmpstr = '' then
    tmpstr := '*' ;
  RESULT := tmpstr;
end;
procedure TfmJSz.SaveInfo;
var
    tmpParams: string;
    tmpMat, tmpGr, tmpGoods, tmpComp, tmpIsSz, tmpAppStr: string;
begin
  if chbSup.Checked then tmpIsSz := '1'
  else tmpIsSz := '0';
  tmpMat := SetAParam(Mat,MatSz, MatAll);
  tmpGr := SetAParam(GrMat, GrSz, GrMatAll);
  tmpGoods := SetAParam(Goods,GoodsSz, GoodsAll);
  tmpComp := SetAParam(Suppl, SupplSz, SupplAll);
  if AppStr = '' then tmpAppStr := '*'
                 else tmpAppStr := AppStr;


  tmpParams := DateToStr(dmServ.L3BD) + ';'  + DateToStr(dmServ.L3ED)  + ';' +
                tmpMat + ';' + tmpGr + ';' + tmpGoods + ';' +
                tmpComp + ';' + tmpIsSz  + ';' + tmpAppStr + ';';
  with dmServ.quTmp do
  begin
    Close;
    if not Transaction.Active then Transaction.StartTransaction;
    Close;
    SQL.Text := 'update d_emp set A_INFO = ''' + tmpParams + '''where d_empid  =' +
                IntToStr(dmCom.UserId);
    ExecQuery;
    Close;
    Transaction.CommitRetaining;

   end;
end;
procedure CheckMultiContr(chlb:TCheckListBox;var arr: OArr;
                                                    var sz: integer; var IsAll:boolean);
var i:integer;
    s:string;
    count: integer;
begin
  arr := nil;
  if chlb.Checked[0] then
   begin
    IsAll := True;
    count := chlb.Items.Count-1;
    SetLength(arr, count);
    sz := count;
    for i:=1 to chlb.Items.Count-1 do
    begin
      chlb.Checked[i]:=False;
      s:=TNodeData(chlb.Items.Objects[I]).Code;
      arr[i-1] := s;
    end
   end
  else
   begin
    IsAll := False;
    count := 0;
    for i:=1 to chlb.Items.Count-1 do
     if chlb.Checked[i] then
     begin
       count := count + 1;
       SetLength(arr, count);
       sz := count;
       s:=TNodeData(chlb.Items.Objects[I]).Code;
       arr[count-1] := s;
     end;
   end;
end;

procedure TfmJSz.CalcAnlz;
var sqlstr,BCaption: string;
    i: integer;
begin
  Application.Minimize;
  finalize(GrMat);
  finalize(Mat);
  finalize(Goods);
  finalize(Suppl);
  finalize(Appl_id);
  sqlstr := 'delete from Anl_Prm_D_Comp where Userid  = ' + IntToStr(dmCom.UserID);
  ExecSQL(sqlstr,dmServ.quTmp);
  sqlstr := 'delete from Anl_Prm_D_Mat where Userid  = ' + IntToStr(dmCom.UserID);
  ExecSQL(sqlstr,dmServ.quTmp);
  sqlstr := 'delete from Anl_Prm_D_Goods where Userid  = ' + IntToStr(dmCom.UserID);
  ExecSQL(sqlstr,dmServ.quTmp);
  sqlstr := 'delete from Anl_Prm_GrMat where Userid  = ' + IntToStr(dmCom.UserID);
  ExecSQL(sqlstr,dmServ.quTmp);

  BCaption := fmJSz.Caption;
  CheckMultiContr(chlbProd,Suppl, SupplSz, SupplAll);
  CheckMultiContr(chlbGrMat,GrMat, GrSz, GrMatAll);
  CheckMultiContr(chlbMat,Mat,MatSz, MatAll);
  CheckMultiContr(chlbGood,Goods,GoodsSz, GoodsAll);
  AppStr := '*';
  SaveInfo; //���������� ������������� ���������� �������

{***���������� ������ ���������� �������, ������������ ����������  MainDocSz ***}
  for i:=0 to SupplSz-1 do
  begin
    sqlstr := 'insert into Anl_Prm_D_Comp(ParamValue, Userid)' +
              'values (''' + Suppl[i] + ''',' +
                             IntToStr(dmCom.UserID) + ')';
    ExecSQL(sqlstr,dmServ.quTmp);
  end;

  for i:=0 to GrSz-1 do
  begin
    sqlstr := 'insert into Anl_Prm_GrMat(ParamValue, Userid)' +
              'values (''' + GrMat[i] + ''', ' +
                               IntToStr(dmCom.UserID) + ')';
    ExecSQL(sqlstr,dmServ.quTmp);
  end;

  for i:= 0 to MatSz-1 do
  begin
    sqlstr := 'insert into Anl_Prm_D_Mat(ParamValue, Userid)' +
              'values (''' + Mat[i] + ''', ' +
                             IntToStr(dmCom.UserID) + ')';
    ExecSQL(sqlstr,dmServ.quTmp);
  end;

  for i:= 0 to GoodsSz-1 do
  begin
    sqlstr := 'insert into Anl_Prm_D_Goods(ParamValue, Userid)' +
              'values (''' + Goods[i] + ''', ' +
                               IntToStr(dmCom.UserID) + ')';
    ExecSQL(sqlstr,dmServ.quTmp);
  end;
{***********************************************************************}

  lAll.Caption:='';

  dmServ.SzMode := -1;
  g1.Position:=0;
{*************������ �������*************************}
  sqlstr := 'execute procedure MainDocSz('+IntToStr(dmCom.UserId)+', '#39+
              DateToStr(dmServ.L3BD)+#39', '#39+DateToStr(dmServ.L3ED)+#39')';
  ExecSQL(sqlstr,dmServ.quTmp);
{****************************************************}
  g1.Position:=100;

  if chbSup.Checked then
   dmServ.SzMode :=0
  else
   dmServ.SzMode :=1;
  chbSupClick(self);
  dmServ.taJSz.Close;
  dmServ.taJSz.Open;
  Application.Restore;
  Flag_Per:=true;
  dm.SetVisEnabled(chlbGrMat,False);
  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbProd,False);
  fmJSz.Caption := BCaption;
end;

procedure TfmJSz.spitCalcClick(Sender: TObject);
var LogOperationID:string;
begin
//  SaveAppl;
  if dm.countmaxpross then sysUtils.Abort;
  LogOperationID:=dm3.insert_operation(sLog_CalcJSz,LogOprIdForm);
  ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);
  SaveInfo;
  CalcAnlz;
  ReOpenDataSets([dmServ.taTotJSz]);
  ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);  
  dm3.update_operation(LogOperationID);
end;

procedure TfmJSz.grOldGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
{  if not HighLight or gr.ClearHighlight then}
  if (Field.FieldName='DEBTORS')or
     (Field.FieldName='DEBTORW')or
     (Field.FieldName='DEBTORQ')then begin  Background := dm.CU_SBD1; exit;  end;

  if (Field.FieldName='SALES')or
     (Field.FieldName='SALEW')or
     (Field.FieldName='SALEQ') then begin Background := dm.CU_SL; exit; end;

  if (Field.FieldName='OPTS')or
     (Field.FieldName='OPTW')or
     (Field.FieldName='OPTQ') then begin Background := dm.CU_SO1; exit; end;

  if (Field.FieldName='RETOPTS')or
     (Field.FieldName='RETOPTW')or
     (Field.FieldName='RETOPTQ')then begin Background := dm.CU_RO1; exit; end;

  if (Field.FieldName='RETS')or
     (Field.FieldName='RETW')or
     (Field.FieldName='RETQ')then begin Background := dm.CU_RT1; exit; end;

  if (Field.FieldName='RETVENDORS')or
     (Field.FieldName='RETVENDORW')or
     (Field.FieldName='RETVENDORQ')then Background := dm.CU_SR1;

    if Field.Tag>0 then Background:=dm2.GetDepColor(Field.Tag);

end;

procedure TfmJSz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmServ.taJSz do
  begin
    if State in [dsEdit] then Post;
    CheckEdtAppl;
    CloseDataSets([dmServ.taJSz]);
    Transaction.Commit;
  end;

  SaveInfo;
  Action := caFree;
  finalize(GrMat);
  finalize(Mat);
  finalize(Goods);
  finalize(Suppl);
  finalize(Appl_id);

  gr.SaveColumnsLayoutIni(GetIniFileName, Name+'_gr', true);

end;

procedure TfmJSz.spitPrintClick(Sender: TObject);
var
  arr : TarrDoc;
  i:integer;
  LogOperationID:string;
  count_ApplArt2: variant;
begin
  SaveAppl;
  if TWinControl(Sender).Tag=0 then
     dmReport.PrintDocumentB(maindoc)
  else
  begin
    if Appl_ID = nil then
      raise Exception.Create('������ ���');

  if TWinControl(Sender).Tag=0 then
   LogOperationID:=dm3.insert_operation(sLog_PrintListJSz,LogOprIdForm)
  else LogOperationID:=dm3.insert_operation(sLog_PrintApplJSz,LogOprIdForm);
        
  {  if (dmServ.taAppl.State in [dsEdit, dsInsert]) then dmServ.taAppl.Post;
  dmServ.taAppl.Transaction.CommitRetaining;}
    SetLength(arr, countAppl);
    for i:= 0 to countAppl-1 do
    begin
      if StrToInt(Appl_id[i]) <> -1 then
      begin
        arr[0] := StrToInt(Appl_id[i]); //dmServ.taApplNOAPPL.AsInteger;
        count_ApplArt2 := ExecSelectSQL('select count(*) from aitem where art2 is not null and applid='+ dmserv.taApplListAPPLID.AsString, dmCom.quTmp);
        if (count_ApplArt2=0) then
           PrintDocument(arr, appl_sup1) else
           PrintDocument(arr, appl_sup);
      end;
    end;
    finalize(arr);
  end;

  dm3.update_operation(LogOperationID);
end;

procedure TfmJSz.spitPeriodClick(Sender: TObject);
var
  d1, d2 : TDateTime;
begin
  d1 := dmServ.L3BD;
  d2 := dmServ.L3ED;
  if GetPeriod(d1, d2) then
  begin
    dmServ.L3BD := d1;
    dmServ.L3ED := d2;
    ShowPeriod;
  end;
end;

procedure TfmJSz.ShowPeriod;
begin
  if(dmServ.L3BD = StrToDate('05.02.1981'))or(not ValidDate(dmServ.L3BD)or
    (not ValidDate(dmServ.L3ED))) then laPeriod.Caption := '������ �� ���������';
  laPeriod.Caption := 'C '+DateToStr(dmServ.L3BD)+' �� '+DateToStr(dmServ.L3ED);//Com.User.L3ED);
end;

procedure TfmJSz.spExportClick(Sender: TObject);
var
  j  : Integer;
  ftext:textfile;
  sDir, Path,sd , art :string;
  LogOperationID:string;

  function Art_cdm (art_sup:string):string;
  begin
   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select first 1 art_cdm, art from d_art where d_compid=311 and art='#39+art_sup+#39;
   dm.quTmp.ExecQuery;
   if dm.qutmp.fields[0].isNull then result:=dm.qutmp.fields[1].AsString
   else Result:= dm.qutmp.fields[0].AsString;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
  end;

begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='nlz';
  svdFile.FileName:=ReplaceStr(datetostr(dmCom.GetServerTime),'.','');
  try
 //�������� � ��������� ����
   if svdFile.Execute then
    begin
     LogOperationID:=dm3.insert_operation(sLog_ExportListJSz,LogOprIdForm);

     Path:=svdFile.FileName;
     AssignFile(ftext, Path);
     try
       Rewrite(ftext);
       writeln(ftext,'��� "���������"');
// **************���� �� ����������� **********************
       for j:=0 to chlbProd.Items.Count -1 do
         if  (chlbProd.Checked[j])  then
         begin
           writeln(ftext,chlbProd.Items[j]);
           DateTimeToString(sd,'dd.mm.yyyy',dmServ.L3BD);
           writeln(ftext,sd);//datetostr(dmServ.L3BD));
           DateTimeToString(sd,'dd.mm.yyyy',dmServ.L3ED);
           writeln(ftext,sd);
    //       writeln(ftext,datetostr(dmServ.L3ED));
           with dmServ.quTmp do
             begin
             Close;
             Sql.Text := 'select ARTID, ART, D_COMPID, D_MATID, D_GOODID, UNITID, ' +
             'D_INSID, INS, INW, INQ, OUTS, OUTW, OUTQ, DEBTORS, DEBTORW, ' +
             'DEBTORQ, SALES, SALEW, SALEQ, RETS, RETW, RETQ, RETOPTS, ' +
             'RETOPTW, RETOPTQ, RETVENDORS, RETVENDORW, RETVENDORQ, OPTS, ' +
             'OPTW, OPTQ, SZ, CURRS, CURRW, CURRQ, ZQ '+
                ' from GROUPANLZ (' + IntToStr(dmServ.IsIns) + ', '+ IntToStr(dmServ.SzMode) +', '+ IntToStr(dmCom.UserId)+ ') ' +
                ' where Comp = '#39 + chlbProd.Items[j] +#39;
              ExecQuery;
              while not EOf do
                begin
                  art:=extractword(1,FieldByName('ART').asString,[' ']);
                  if dmServ.quTmp.FieldByName('D_COMPID').AsInteger=311 then art:=Art_cdm(art);
                                        (* 1          2           3          4            5           6*)
                  writeln(ftext,format('%8s %10s %10d %15.2f %10d %15.2f %10d %15.2f %10d %15.2f %10d %15.2f %10d %15.2f %10d %15.2f',
                  [art,FieldByName('SZ').asString,FieldByName('INQ').asInteger,FieldByName('INW').asFloat,
                  FieldByName('DEBTORQ').asInteger+FieldByName('RETOPTQ').asInteger, FieldByName('DEBTORW').asFloat+FieldByName('RETOPTW').asFloat,
                  FieldByName('SALEQ').asInteger+FieldByName('OPTQ').asInteger,FieldByName('SALEW').asFloat+FieldByName('OPTW').asFloat,
                  FieldByName('RETQ').asInteger, FieldByName('RETW').asFloat,
                  FieldByName('RETVENDORQ').asInteger,FieldByName('RETVENDORW').asFloat,
                  FieldByName('OUTQ').asInteger,FieldByName('OUTW').asFloat,
                  FieldByName('CURRQ').asInteger,FieldByName('CURRW').asFloat]));
                  next;
                 end;
            end;
         end;
       dmServ.quTmp.Close;
      finally
         CloseFile(fText);
      end;
     dm3.update_operation(LogOperationID);
    end;
    MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
  except
    on E:Exception do
      MessageDialog('������ �������� � ��������� ����:'#13+E.Message, mtError, [mbOk], 0);
  end;
end;

procedure TfmJSz.chbSupClick(Sender: TObject);
var Art: string;
begin

 if chbSup.Checked then dmServ.SzMode :=0
 else dmServ.SzMode :=1;

  try
    Screen.Cursor := crSQLWait;
    with dmServ.quTmp do
    begin
      if not Transaction.Active then Transaction.StartTransaction;
      Close;
      SQL.Text := 'execute procedure Update_Declr('+IntToStr(dmCom.UserId)+',1)';
      ExecQuery;
      Close;
      Transaction.Commit;
    end;
    Art := dmServ.taJSzArt.AsString;
    ReOpenDataSet(dmServ.taJSz);
    dmServ.taJSz.Locate('ART', Art,[]);
  finally
      Screen.Cursor := crDefault;
  end;

 if chbxInfo.Checked then
 begin
  ReOpenDataSets([dmServ.taTotJSz]);
 end
end;

procedure TfmJSz.miZClick(Sender: TObject);
var
    i,j: integer;
    LogOperationID:string;

begin
  SaveAppl;
  if Appl_ID = nil then
    raise Exception.Create('������ ���');

  LogOperationID:=dm3.insert_operation(sLog_ExportApplJSz,LogOprIdForm);

  i := 0;
  for j:=0 to chlbProd.Items.Count -1 do
    if  (chlbProd.Checked[j])  then
    begin
     if StrToInt(Appl_Id[i])<> -1 then
        dmReport.AppleText(StrToInt(Appl_Id[i]),chlbProd.Items[j]);
     i := i + 1;
    end;

  dm3.update_operation(LogOperationID);  
end;

procedure TfmJSz.nAllzClick(Sender: TObject);
var s, sqlstr, res, res1 :string;
    FieldsRes:Variant;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_ApplSumJSz,LogOprIdForm);

  with dmServ do
  begin
    sqlstr := 'select sum(zq) from J1tmp where userid='+IntToStr(dmCom.UserId) +
                 ' and  IsSzOpt = '  + IntToStr(SzMode);
    try
      FieldsRes := ExecSelectSQL(sqlstr, dmServ.quTmp);
      if FieldsRes=null then res := '0'
                       else res:=String(FieldsRes);
    finally
      Finalize(FieldsRes);
    end;


     sqlstr := ' select sum(j1.zq*ad.avg_w*ad.avg_spr) ' +
              ' from j1tmp j1, d_art ad               ' +
              ' where j1.artid = ad.d_artid and        ' +
              '       j1.userid = ' + IntToStr(dmCom.UserId) +
              '  and  j1.IsSzOpt = '  + IntToStr(SzMode);
    try
      FieldsRes := ExecSelectSQL(sqlstr, dmServ.quTmp);
      if FieldsRes=null then res1 := '0'
                        else res1  := FloatToStr(RoundTo((real(FieldsRes)), -2));
    finally
      Finalize(FieldsRes);
    end;

    s:='����� � ������ '+ res + ' �� ������. ����� ' + res1;

  end;
  lAll.Caption:=s;
  dm3.update_operation(LogOperationID);
end;

procedure TfmJSz.SpeedItem10Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbMat,True);
  chlbMat.SetFocus;
end;

procedure TfmJSz.SpeedItem7Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGrMat,True);
  chlbGrMat.SetFocus;
end;

procedure TfmJSz.SpeedItem8Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbProd,True);
  chlbProd.SetFocus;
end;

procedure TfmJSz.SpeedItem9Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGood,True);
  chlbGood.SetFocus;
end;

procedure TfmJSz.chlbMatExit(Sender: TObject);
begin
   dm.SetVisEnabled(TWinControl(sender),False);
end;

procedure TfmJSz.chlbMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_MatId:=ChlbClickCheck(chlbMat,s);
 SpeedItem10.BtnCaption:=s;

end;

procedure TfmJSz.chlbGrMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GrMatId:=ChlbClickCheck(chlbGrMat,s);
 SpeedItem7.BtnCaption:=s;
end;

procedure TfmJSz.chlbProdClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_CompId:=ChlbClickCheck(chlbProd,s);
 SpeedItem8.BtnCaption:=s;

end;

procedure TfmJSz.chlbGoodClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GoodId:=ChlbClickCheck(chlbGood,s);
 SpeedItem9.BtnCaption:=s;

end;
function TfmJSz.ChlbClickCheck(chlb:TCheckListBox;var st: string):variant;
var i: integer;
begin
  i:=chlb.ItemIndex;
  WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
  if chlb.Checked[i] then
    begin
     st:=chlb.Items[i];
     if (i<>0) then     chlb.Checked[0]:=false;
    end
  else
    begin
     i:=chlb.Items.Count-1;
     WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
     st:=chlb.Items[i];
     chlb.Checked[i]:=true;
    end;
  Result:=TNodeData(chlb.Items.Objects[i]).Code;
end;
procedure TfmJSz.FormActivate(Sender: TObject);
begin
  dm.ClickUserDepMenu(dm.pmJSz);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmJSz.grOldKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  if  ((key >=48) and (key <=57)) or ((key >=96) and (key <=105)) then
  begin
    if (Appl_ID = nil) then
      CreateNewAppl
    else
     if Not ExistsAppl then
       AddAppl;
  end;}
end;

procedure TfmJSz.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmJSz.grTotalGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin

if Column.Field<>NIL then
  Background:=grTotal.DataSource.DataSet.FieldByName('Color').AsInteger;
end;

procedure TfmJSz.chbxInfoClick(Sender: TObject);
begin
  if chbxInfo.Checked then
  begin
    ReOpenDataSets([dmServ.taTotJSz]);
    dm.SetVisEnabled(plTotal,True);
  end
  else
    dm.SetVisEnabled(plTotal,False);

end;

procedure TfmJSz.SpeedItem2Click(Sender: TObject);
var FieldsRes: t_array_var;
    sqlstr : string;
    CurrArt2ID, CurrSupID: integer;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_AddArtJSz,LogOprIdForm);

  sqlstr := 'select  first 1 Art2ID, SupID from Art2 where d_ArtId =  ' + dmServ.taJSzARTID.AsString;
  try
  dmCom.ExecuteQutmp(dmServ.quTmp, sqlstr,2, FieldsRes);
  if FieldsRes[0] = null then
    begin
      CurrArt2ID := -1000;
      CurrSupID := -1000;
    end
  else
   begin
     CurrArt2ID := integer(FieldsRes[0]);
     CurrSupID :=  integer(FieldsRes[1]);
   end
  finally
     Finalize(FieldsRes);
  end;

  dmServ.SearchArt := IntToStr(CurrArt2ID);
  dmServ.DicSupID := CurrSupID;

  if ShowAndFreeForm(TfmNewArt, Self, TForm(fmNewArt), True, False)=mrOK then
    with dm do
      if Art2Id<>-1 then
        begin
          if dmServ.taJSz.State<>dsInsert then
            dmServ.taJSz.Insert;
          dmServ.taJSz.Post;
          dmServ.taJSz.Locate('ARTID;SZ', VarArrayOf([dmServ.taJSzARTID.AsInteger,dmServ.taJSzSZ.AsString]) , []);
        end;
  dm3.update_operation(LogOperationID)
end;

procedure TfmJSz.SpeedItem3Click(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_DelArtJSz,LogOprIdForm);
  dmServ.taJSz.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmJSz.SpeedItem4Click(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_CreateApplJSz,LogOprIdForm);
  try
    SaveAppl;
    SaveInfo;
    dmServ.IsEdtAppl := false;
    MessageDialog('������ ������������', mtInformation, [mbOk], 0);
  except
    on E:Exception do
      MessageDialog('������ ��� ���������� ���������� ������� � ������:'#13+E.Message, mtError, [mbOk], 0);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmJSz.grGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  with Column do
  begin
    if (Field.FieldName='DEBTORS')or
       (Field.FieldName='DEBTORW')or
       (Field.FieldName='DEBTORQ')then begin  Background := dm.CU_SBD1; exit;  end;

    if (Field.FieldName='SALES')or
       (Field.FieldName='SALEW')or
       (Field.FieldName='SALEQ') then begin Background := dm.CU_SL; exit; end;

    if (Field.FieldName='OPTS')or
       (Field.FieldName='OPTW')or
       (Field.FieldName='OPTQ') then begin Background := dm.CU_SO1; exit; end;

    if (Field.FieldName='RETOPTS')or
       (Field.FieldName='RETOPTW')or
       (Field.FieldName='RETOPTQ')then begin Background := dm.CU_RO1; exit; end;

    if (Field.FieldName='RETS')or
       (Field.FieldName='RETW')or
       (Field.FieldName='RETQ')then begin Background := dm.CU_RT1; exit; end;

    if (Field.FieldName='RETVENDORS')or
       (Field.FieldName='RETVENDORW')or
       (Field.FieldName='RETVENDORQ')then Background := dm.CU_SR1;

    if (Field.FieldName='WRITE_OFFS')or
       (Field.FieldName='WRITE_OFFW')or
       (Field.FieldName='WRITE_OFFQ')then Background := dm.CU_AC;

    if (Field.FieldName='SURPLUSS')or
       (Field.FieldName='SURPLUSW')or
       (Field.FieldName='SURPLUSQ')then begin  Background := TColor(Integer(dm.CU_SBD1) + 1); exit;  end;

    if Field.Tag>0 then Background:=dm2.GetDepColor(Field.Tag);
  end;
end;

procedure TfmJSz.cbInsClick(Sender: TObject);
var Art: string;
begin
  try
    Screen.Cursor := crSQLWait;
    if cbIns.Checked then
      dmServ.IsIns :=0  // �� ���������
    else
    begin
      dmServ.IsIns :=1; // ��� �������
    end;
    Art := dmServ.taJSzArt.AsString;
    ReOpenDataSet(dmServ.taJSz);
    dmServ.taJSz.Locate('ART', Art,[]);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmJSz.CheckEdtAppl;
var FieldsRes: Variant;
    sqlstr: string;
begin
  if not dmServ.IsEdtAppl then exit;
  sqlstr := 'select D_Appl ' +
                'from GetParamsLastAlz(' + IntToStr(dmCom.UserId) + ')';
  FieldsRes := ExecSelectSQL(sqlstr,dmServ.quTmp);
  if (VarToStr(FieldsRes) <> '*') {and (trim(VarToStr(FieldsRes))<> AppStr) }then
    if MessageDialog('������ ���� �������������. ��������� ���������?', mtConfirmation,
         [mbYes, mbNo], 0) = mrYes then
        SpeedItem4Click(nil);
end;
procedure TfmJSz.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100401)
end;

end.

