unit SellUid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, StdCtrls, Buttons, db,
  rxPlacemnt;

type
  TfmSellUID = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dg1: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSellUID: TfmSellUID;

implementation

uses comdata, data;

{$R *.DFM}

procedure TfmSellUID.dg1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmSellUID.FormCreate(Sender: TObject);
begin
  //--------- ��������� ����� ��� ��������� �� �������� ----------------//
  dg1.ColumnByName['PRICE'].Visible := CenterDep;
  dg1.ColumnByName['NDSNAME'].Visible := CenterDep;
  //---------------------------------------------------------------------//
end;

end.
