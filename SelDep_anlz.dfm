object fmSelDep_anlz: TfmSelDep_anlz
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
  ClientHeight = 492
  ClientWidth = 1134
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 140
    Top = 379
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 0
  end
  object lcbDepName: TDBLookupComboBox
    Left = 9584
    Top = 3427
    Width = 207
    Height = 21
    DataField = 'NAME'
    DataSource = dmCom.dsDep
    ListSource = dmCom.dsDep
    TabOrder = 1
  end
  object cbSelDep: TDBComboBox
    Left = 48
    Top = 56
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    OnDropDown = cbSelDepDropDown
  end
  object qSelDep: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT D_DEPID, NAME, SNAME'
      'FROM D_DEP'
      'WHERE D_DEPID>0 and ISDELETE <> 1')
    Left = 208
    Top = 56
  end
end
