object fmDInv: TfmDInv
  Left = 149
  Top = 170
  HelpContext = 100223
  Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1074#1085#1091#1090#1088#1077#1085#1085#1077#1075#1086' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
  ClientHeight = 596
  ClientWidth = 874
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter6: TSplitter
    Left = 0
    Top = 293
    Width = 874
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 577
    Width = 874
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 874
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    PopupMenu = ppPrint
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siAddClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      BtnCaption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Hint = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siUIDClick
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object siUID2: TSpeedItem
      BtnCaption = #1048#1079#1076#1077#1083#1080#1103
      Caption = #1048#1079#1076#1077#1083#1080#1103
      Hint = #1048#1079#1076#1077#1083#1080#1103'|'
      ImageIndex = 42
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = siUID2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1058#1077#1082#1091#1097#1080#1077' '#13#10#1094#1077#1085#1099
      Caption = #1058#1077#1082#1091#1097#1080#1077' '#1094#1077#1085#1099
      Hint = #1058#1077#1082#1091#1097#1080#1077' '#1094#1077#1085#1099'|'
      Spacing = 1
      Left = 387
      Top = 3
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      BtnCaption = #1041#1080#1088#1082#1080
      Caption = #1041#1080#1088#1082#1080
      DropDownMenu = ppPrint
      Hint = #1054#1090#1084#1077#1095#1077#1085#1085#1099#1077
      ImageIndex = 67
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 451
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 874
    Height = 68
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 100
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 200
      Top = 8
      Width = 87
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 200
      Top = 28
      Width = 86
      Height = 13
      Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laCost: TLabel
      Left = 340
      Top = 28
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBTextTo: TDBText
      Left = 491
      Top = 28
      Width = 58
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'COST'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 290
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPFROM'
      DataSource = dm.dsDList
    end
    object DBText3: TDBText
      Left = 290
      Top = 28
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPTO'
      DataSource = dm.dsDList
    end
    object Label3: TLabel
      Left = 340
      Top = 48
      Width = 96
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1087#1088'. '#1094#1077#1085#1072#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TDBText
      Left = 436
      Top = 48
      Width = 113
      Height = 13
      Alignment = taRightJustify
      DataField = 'SCOST'
      DataSource = dm.dsDList
    end
    object laCostD: TLabel
      Left = 560
      Top = 28
      Width = 101
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1086#1087#1090'. '#1094#1077#1085#1072#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbtCostD: TDBText
      Left = 665
      Top = 28
      Width = 44
      Height = 13
      AutoSize = True
      DataField = 'SCOSTD'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laCost0: TLabel
      Left = 340
      Top = 8
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBTextFr: TDBText
      Left = 495
      Top = 8
      Width = 54
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'COST0'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 81
      Height = 21
      Cursor = crDrag
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsDList
      TabOrder = 0
    end
    object DBCheckBox1: TDBCheckBox
      Left = 676
      Top = 8
      Width = 117
      Height = 17
      Caption = #1053#1077' '#1089#1086#1079#1076'. '#1087#1088#1080#1082#1072#1079
      DataField = 'NOTPR'
      DataSource = dm.dsDList
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object dbSdate: TDBEditEh
      Left = 116
      Top = 26
      Width = 61
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsDList
      EditButtons = <>
      Enabled = False
      ReadOnly = True
      TabOrder = 2
      Visible = True
    end
    object btdate: TBitBtn
      Left = 177
      Top = 26
      Width = 20
      Height = 22
      TabOrder = 3
      OnClick = btdateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object pa2: TPanel
    Left = 0
    Top = 296
    Width = 874
    Height = 281
    Align = alClient
    BevelOuter = bvNone
    Caption = 'pa2'
    TabOrder = 3
    object Splitter2: TSplitter
      Left = 377
      Top = 0
      Height = 281
      ExplicitHeight = 274
    end
    object Panel3: TPanel
      Left = 380
      Top = 0
      Width = 494
      Height = 281
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dgWH: TM207IBGrid
        Left = 0
        Top = 29
        Width = 494
        Height = 252
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsD_WH
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pmWH
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = siAddClick
        IniStorage = fs
        TitleButtons = True
        OnGetCellParams = dgWHGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Expanded = False
            FieldName = 'PRODCODE'
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 34
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_MATID'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_GOODID'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_INSID'
            Title.Alignment = taCenter
            Title.Caption = #1054#1042
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_COUNTRYID'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 126
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2 '
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 87
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Caption = #1042#1089#1077#1075#1086' - '#1050#1086#1083'-'#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1089#1077#1075#1086' - '#1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object tb2: TSpeedBar
        Left = 0
        Top = 0
        Width = 494
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 70
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 1
        InternalVer = 1
        object Label2: TLabel
          Left = 304
          Top = 8
          Width = 52
          Height = 13
          Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label7: TLabel
          Left = 107
          Top = 8
          Width = 30
          Height = 13
          Caption = #1055#1086#1080#1089#1082
          Transparent = True
        end
        object edUID: TEdit
          Left = 360
          Top = 4
          Width = 73
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          OnKeyDown = edUIDKeyDown
        end
        object cbSearch: TCheckBox
          Left = 91
          Top = 9
          Width = 12
          Height = 12
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object ceArt: TComboEdit
          Left = 144
          Top = 4
          Width = 93
          Height = 21
          ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
          Color = clInfoBk
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000120B0000120B00001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
            DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
            9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
            9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
            90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
          NumGlyphs = 1
          TabOrder = 2
          OnButtonClick = ceArtButtonClick
          OnChange = ceArtChange
          OnKeyDown = ceArtKeyDown
          OnKeyUp = ceArtKeyUp
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1054#1090#1082#1088#1099#1090#1100
          Caption = #1054#1090#1082#1088#1099#1090#1100
          Hint = #1054#1090#1082#1088#1099#1090#1100
          ImageIndex = 74
          Layout = blGlyphLeft
          Spacing = 1
          Left = 3
          Top = 3
          Visible = True
          OnClick = SpeedItem1Click
          SectionName = 'Untitled (0)'
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 377
      Height = 281
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 49
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object Splitter4: TSplitter
        Left = 149
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object Splitter5: TSplitter
        Left = 101
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object Splitter1: TSplitter
        Left = 197
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object Splitter7: TSplitter
        Left = 265
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object Splitter8: TSplitter
        Left = 313
        Top = 1
        Height = 279
        ExplicitHeight = 272
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 48
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 52
        Top = 1
        Width = 49
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 152
        Top = 1
        Width = 45
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 200
        Top = 1
        Width = 65
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbcountry: TListBox
        Left = 104
        Top = 1
        Width = 45
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt1: TListBox
        Left = 268
        Top = 1
        Width = 45
        Height = 279
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 5
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt2: TListBox
        Left = 316
        Top = 1
        Width = 60
        Height = 279
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 6
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dgEl: TM207IBGrid
    Left = 0
    Top = 109
    Width = 874
    Height = 184
    Align = alTop
    Color = clBtnFace
    DataSource = dm.dsSEl
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgMultiSelect]
    PopupMenu = pmEl
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = siUIDClick
    OnEditButtonClick = dgElEditButtonClick
    FixedCols = 8
    ClearSelection = False
    IniStorage = fs
    MultiSelect = True
    TitleButtons = True
    OnGetCellParams = dgElGetCellParams
    MultiShortCut = 16472
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'RecNo'
        Title.Alignment = taCenter
        Title.Caption = #8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 26
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRODCODE'
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 32
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_MATID'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 33
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_GOODID'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_INSID'
        Title.Alignment = taCenter
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 41
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 117
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UNITID'
        Title.Alignment = taCenter
        Title.Caption = #1045#1048
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 25
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'QUANTITY'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cost2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1057#1091#1084#1084#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 78
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'TOTALWEIGHT'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1062#1077#1085#1072' - '#1062#1077#1085#1090#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 74
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'EP2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SSUM'
        Title.Alignment = taCenter
        Title.Caption = #1042' '#1087#1088'. '#1094#1077#1085#1072#1093
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SSUMD'
        Title.Alignment = taCenter
        Title.Caption = #1042' '#1086#1087#1090'. '#1094#1077#1085#1072#1093
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        Visible = True
      end>
  end
  object fs: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'pa2.Height'
      'cbSearch.Checked'
      'Panel5.Width'
      'lbcountry.Width'
      'lbComp.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 48
    Top = 196
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 436
    Top = 196
    object N1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelClick
    end
    object N3: TMenuItem
      Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      ShortCut = 114
      OnClick = siUIDClick
    end
    object N4: TMenuItem
      Caption = #1048#1079#1076#1077#1083#1080#1103
      ShortCut = 115
      OnClick = siUID2Click
    end
  end
  object pmWH: TPopupMenu
    Images = dmCom.ilButtons
    Left = 380
    Top = 410
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 13
      OnClick = siAddClick
    end
  end
  object ppPrint: TRxPopupMenu
    MenuAnimation = [maNone]
    Left = 336
    Top = 208
  end
  object aclisst: TActionList
    Images = dmCom.ilButtons
    Left = 488
    Top = 192
    object acClose: TAction
      Caption = #1042#1099#1073#1088#1072#1085#1085#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acCloseExecute
    end
    object acOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acOpenExecute
    end
    object acCreateInvFromPrord: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1076#1083#1103' '#1074#1089#1077#1093' '#1080#1079#1076#1077#1083#1080#1081' '#1085#1072' '#1089#1082#1083#1072#1076#1077
    end
    object acCreateInvAll: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1042#1086#1079#1074#1088#1072#1090' '#1074#1089#1077#1093' '#1080#1079#1076#1077#1083#1080#1081' '#1089' '#1092#1080#1083#1080#1072#1083#1072
    end
    object acCretaeOpt: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1087#1090#1086#1074#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1085#1072' '#1042#1055
    end
    object acCreateInvFromInv: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1103' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1085#1072'...'
      Hint = 
        #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1089' '#1080#1079#1076#1077#1083#1080#1103#1084#1080' '#1080#1079' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1085#1072' '#1074#1099#1073#1088#1072#1085#1085#1099 +
        #1081' '#1092#1080#1083#1080#1072#1083
      ShortCut = 113
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 608
    Top = 8
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   2'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 2'
      'order by HDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 664
    Top = 8
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
end
