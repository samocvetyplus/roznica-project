object fmFRest: TfmFRest
  Left = 244
  Top = 146
  Width = 783
  Height = 538
  Caption = #1054#1089#1090#1072#1090#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object dg1: TDBGridEh
    Left = 0
    Top = 71
    Width = 775
    Height = 414
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm2.dsFRest
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 186
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW'
        Footers = <>
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 46
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 775
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siCalc: TSpeedItem
      Action = acCalc
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090'[F4]'
      Caption = #1055#1077#1088#1077#1089#1095#1105#1090
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        7B7B7B0000000000000000000000000000000000000000000000000000007B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000BDBDBD000000BDBDBD0000FF0000FF0000FFBDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000000000000000000000000000000000000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000000000000000FF
        FF00000000FFFF00000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBD000000000000000000000000000000000000000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B00000000000000000000000000
        00000000000000000000000000007B7B7BFF00FFFF00FFFF00FF}
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siCalcClick
      SectionName = 'Untitled (0)'
    end
    object siUpdate: TSpeedItem
      Action = acUpdate
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100'[F5]'
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF008400000084000000FF00000084000000FF00
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0084000000FF000000840000008400000084000000FF0000008400
        00008400000084000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0084000000FF00000084000000008484000084840084000000840000008400
        0000840000008400000084000000FF00FF00FF00FF00FF00FF00FF00FF008400
        0000FF00000084000000FF000000008484000084840084000000FF0000008400
        0000FF000000840000000084840000848400FF00FF00FF00FF00FF00FF008400
        0000FF000000FF000000FF000000008484000084840000848400840000008400
        000084000000840000000084840000848400FF00FF00FF00FF00FF000000FF00
        0000FF000000FF000000FF000000008484000084840000848400FF000000FF00
        0000FF00000084000000FF0000008400000084000000FF00FF0084000000FF00
        0000FF0000000084840000848400008484000084840000848400FF000000FF00
        0000FF000000840000008400000084000000FF000000FF00FF00FF0000008400
        0000008484000084840000848400008484000084840000848400FF0000000084
        8400FF00000084000000FF000000FF000000FF000000FF00FF0084000000FF00
        0000FF0000000084840000848400008484000084840000848400008484000084
        840000848400FF00000084000000FF00000084000000FF00FF00FF000000FF00
        0000FF0000000084840000848400FF000000FF00000084000000008484000084
        840000848400FF000000FF00000084000000FF000000FF00FF00FF00FF00FF00
        0000FF000000FF000000FF000000FF0000000084840000848400008484000084
        8400008484000084840084000000FF000000FF00FF00FF00FF00FF00FF008400
        0000FF0000000084840000848400008484000084840000848400008484000084
        84000084840000848400FF000000FF000000FF00FF00FF00FF00FF00FF00FF00
        FF0000848400008484000084840000848400FF000000FF00000000848400FF00
        00000084840000848400FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000084840000848400FF000000FF00000084000000FF0000008400
        0000FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00000084000000FF000000FF000000FF00
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ImageIndex = 30
      Spacing = 1
      Left = 83
      Top = 3
      Visible = True
      OnClick = acUpdateExecute
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = 'siPrint'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000FF00FFFF00FF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00C6C6C6000000FF00FF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF
        FFFFFFFFFFFFFFFFC6C6C6C6C6C6000000000000000000FF00FF000000C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000FF0000FF0000FFC6C6C6C6C6C60000
        00C6C6C6000000FF00FF00000000000000000000000000000000000000000000
        0000000000000000000000000000000000C6C6C6C6C6C6000000000000C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6
        C6000000C6C6C6000000FF00FF00000000000000000000000000000000000000
        0000000000000000000000C6C6C6000000C6C6C6000000000000FF00FFFF00FF
        000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C6
        C6000000C6C6C6000000FF00FFFF00FFFF00FF000000FFFFFFFF0000FF0000FF
        0000FF0000FF0000FFFFFF000000000000000000000000FF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFF0000FF
        0000FF0000FF0000FF0000FFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
        0000000000000000000000000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 67
      Spacing = 1
      Left = 211
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 691
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 485
    Width = 775
    Height = 19
    Panels = <>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 42
    Width = 775
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 150
    BtnHeight = 23
    TabOrder = 3
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 43
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Transparent = True
    end
    object lbdep: TLabel
      Left = 616
      Top = 8
      Width = 3
      Height = 13
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 312
      Top = 7
      Width = 87
      Height = 13
      Caption = #1055#1086'-'#1088#1072#1079#1084#1077#1088#1072#1084' [F6]'
      Transparent = True
    end
    object edArt: TEdit
      Left = 64
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edArtKeyDown
    end
    object cbSZ: TCheckBox
      Left = 296
      Top = 8
      Width = 12
      Height = 12
      Action = acSz
      TabOrder = 1
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object pdbg: TPrintDBGridEh
    DBGridEh = dg1
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 156
    Top = 436
  end
  object aclist: TActionList
    Images = dmCom.ilButtons
    Left = 242
    Top = 435
    object acUpdate: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100'[F5]'
      ImageIndex = 30
      ShortCut = 116
      OnExecute = acUpdateExecute
    end
    object acCalc: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090'[F4]'
      ImageIndex = 8
      ShortCut = 115
      OnExecute = siCalcClick
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acSz: TAction
      Caption = #1055#1086'-'#1088#1072#1079#1084#1077#1088#1072#1084' [F6]'
      ShortCut = 117
      OnExecute = acSzExecute
    end
    object acSortDep: TAction
      Caption = #1057#1086#1088#1090#1088#1086#1074#1082#1072' '#1087#1086' '#1086#1089#1090'.'
      OnExecute = acSortDepExecute
    end
  end
end
