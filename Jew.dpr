program Jew;



uses
  ShareMem,
  MidasLib,
  Forms,
  cxGridStrs in 'cxGridStrs.pas',
  SysUtils,
  Controls,
  Windows,
  Dialogs,
  Classes,
  ShlObj,
  Main in 'Main.pas' {fmMain},
  comdata in 'comdata.pas' {dmCom: TDataModule},
  SList in 'SList.pas' {fmSList},
  SInv in 'SInv.pas' {fmSInv},
  DBTree in 'DBTree.pas',
  SItem in 'SItem.pas' {fmSItem},
  ArtNav in 'ArtNav.pas' {frArtNav: TFrame},
  ArtGrid in 'ArtGrid.pas' {frArtGrid: TFrame},
  RecSet in 'RecSet.pas' {fmRecSet},
  VisCol in 'VisCol.pas' {fmVisCol},
  Ins in 'Ins.pas' {fmIns},
  WHArt2 in 'WHArt2.pas' {fmWHArt2},
  WHUID in 'WHUID.pas' {fmWHUID},
  WHA2UID in 'WHA2UID.pas' {fmWHA2UID},
  WHSZ in 'WHSZ.pas' {fmWHSZ},
  DList in 'DList.pas' {fmDList},
  DInv in 'DInv.pas' {fmDInv},
  DItem in 'DItem.pas' {fmDItem},
  PrOrdLst in 'PrOrdLst.pas' {fmPrOrdList},
  PrOrd in 'PrOrd.pas' {fmPrOrd},
  SInvFind in 'SInvFind.pas' {fmSInvFind},
  SElFind in 'SElFind.pas' {fmSElFind},
  CopySIt in 'CopySIt.pas' {fmCopySItem},
  DepMarg in 'DepMarg.pas' {fmDepMargin},
  Distred in 'Distred.pas' {fmDistred},
  SelA2 in 'SelA2.pas' {fmSelA2},
  SetPrice in 'SetPrice.pas' {fmSetPrice},
  AddToPr in 'AddToPr.pas' {fmAddToPr},
  SellList in 'SellList.pas' {fmSellList},
  Sell in 'Sell.pas' {fmSell},
  SellEdit in 'SellEdit.pas' {fmSellEdit},
  Client in 'Client.pas' {fmClient},
  SellUid in 'SellUid.pas' {fmSellUID},
  ARest in 'ARest.pas' {fmARest},
  A2Rest in 'A2Rest.pas' {fmA2Rest},
  FindUID in 'FindUID.pas' {fmFindUID},
  CheckNo in 'CheckNo.pas' {fmCheckNo},
  OpenSell in 'OpenSell.pas' {fmOpenSell},
  OptList in 'OptList.pas' {fmOptList},
  Opt in 'Opt.pas' {fmOpt},
  OptItem in 'OptItem.pas' {fmOptItem},
  SplashFR in 'SplashFR.pas' {fmSplashFR},
  DataFR in 'DataFR.pas' {dmFR: TDataModule},
  OptTot in 'OptTot.pas' {frOptTotal: TFrame},
  OptBSel in 'OptBSel.pas' {fmOptBSel},
  DTotal in 'DTotal.pas' {frDTotal: TFrame},
  Rest in 'Rest.pas' {fmRest},
  RI in 'RI.pas' {fmRI},
  RInv in 'RInv.pas' {fmRInv},
  RestFind in 'RestFind.pas' {fmRestFind},
  AEdit in 'AEdit.pas' {fmAEdit},
  Dst in 'Dst.pas' {fmDst},
  DSZ in 'DSZ.pas' {fmDSZ},
  WHItem in 'WHItem.pas' {fmWHItem},
  Mat in 'Mat.pas' {fmMat},
  Sort in 'Sort.pas' {fmSort},
  Good in 'Good.pas' {fmGood},
  Comp in 'Comp.pas' {fmComp},
  Art in 'Art.pas' {fmArt},
  Depart in 'Depart.pas' {fmDepart},
  Access in 'Access.pas' {fmAccess},
  NDS in 'NDS.pas' {fmNDS},
  Edg in 'Edg.pas' {fmEdg},
  PayType in 'PayType.pas' {fmPayType},
  Discount in 'Discount.pas' {fmDiscount},
  DIns in 'DIns.pas' {fmDIns},
  Period in 'Period.pas' {fmPeriod},
  UIDFind in 'UIDFind.pas' {fmUIDFind},
  RetSInf in 'RetSInf.pas' {fmRetSInf},
  Err in 'Err.pas' {fmErr},
  Splash in 'Splash.pas' {fmSplash},
  AMerge in 'AMerge.pas' {fmAMerge},
  Dbg in 'Dbg.pas' {fmDbg},
  ServData in 'ServData.pas' {dmServ: TDataModule},
  PAct in 'PAct.pas' {fmPAct},
  PrSi in 'PrSi.pas' {fmPrSi},
  SRetLst in 'SRetLst.PAS' {fmSRetList},
  SRet in 'SRet.pas' {fmSRet},
  RetDict in 'RetDict.pas' {fmRetDict},
  SupInf in 'SupInf.pas' {fmSupInf},
  ORetList in 'ORetList.pas' {fmORetList},
  ORet in 'ORet.pas' {fmORet},
  WHInv in 'WHInv.pas' {fmWHInv},
  SPEdit in 'SPEdit.pas' {fmSPEdit},
  Col in 'Col.pas' {fmCol},
  TmpDir in 'TmpDir.pas' {fmTmpDir},
  RetCltList in 'RetCltList.pas' {fmRetCltList},
  ClSell in 'ClSell.pas' {fmClSell},
  SRetUID in 'SRetUID.pas' {fmSRetUID},
  HistItem in 'HistItem.pas' {fmHistItem},
  PHist in 'PHist.pas' {fmPHist},
  UIDHist in 'UIDHist.pas' {fmUIDHist},
  A2PHist in 'A2PHist.pas' {fmA2PHist},
  PrHist in 'PrHist.pas' {fmPrHist},
  A2PrHist in 'A2PrHist.pas' {fmA2prHist},
  SetSDate in 'SetSDate.pas' {fmSetSDate},
  Appl in 'Appl.pas' {fmAppl},
  PrFind in 'PrFind.pas' {fmPrFind},
  UIDPHist in 'UIDPHist.pas' {fmUIDPHist},
  List3 in 'List3.pas' {fmList3},
  HistArt in 'HistArt.pas' {fmHistArt},
  JSz in 'JSz.pas' {fmJSz},
  UIDEdit in 'UIDEdit.pas' {fmUIDEdit},
  ArtDict in 'ArtDict.pas' {fmArtDict},
  NewArt in 'NewArt.pas' {fmNewArt},
  SameUID in 'SameUID.pas' {fmSameUID},
  Dst2 in 'Dst2.pas' {fmDst2},
  DSZ2 in 'DSZ2.pas' {fmDSZ2},
  DstSel in 'DstSel.pas' {fmDstSel},
  Ancestor in 'Ancestor.pas' {fmAncestor},
  CurrStArt in 'CurrStArt.pas' {fmCurrStArt},
  ResQ in 'ResQ.pas' {fmRestQ},
  UIDWHFlt in 'UIDWHFlt.pas' {fmFlt},
  WH in 'WH.pas' {fmWH},
  ErrRq in 'ErrRq.pas' {fmErrRq},
  InvMarg in 'InvMarg.pas' {fmInvMargin},
  FRest in 'FRest.pas' {fmFRest},
  PriceMarg in 'PriceMarg.pas' {fmPrMargin},
  Country in 'Country.pas' {fmCountry},
  InsRepl in 'IBREP\InsRepl.pas',
  dst3 in 'dst3.pas' {fmDst3},
  JUID in 'JUID.pas' {fmJUID},
  AplList in 'AplList.pas' {fmApplList},
  A2M in 'A2M.pas' {fmA2M},
  AM in 'AM.pas' {fmAM},
  Data3 in 'Data3.pas' {dm3: TDataModule},
  SupCase in 'Report\SupCase.pas' {fmSupCase},
  RegExpr in 'Report\RegExpr.pas',
  ReportData in 'Report\ReportData.pas' {dmReport: TDataModule},
  ErrMsg in 'Report\ErrMsg.pas' {fmErrMsg},
  DepSel in 'DepSel.pas' {fmdepsel},
  err_sell in 'err_sell.pas' {fmErr_sell},
  HelpDins in 'HelpDins.pas' {fmHelpDins},
  goods_sam in 'goods_sam.pas' {fmgoods_sam},
  NodCard in 'NodCard.pas' {fmNodCard},
  address in 'address.pas' {fmAddress},
  ExpFileClient in 'ExpFileClient.pas' {fmExpFileClient},
  helpmain in 'helpmain.pas' {fmHelpMain},
  ErrClearDb in 'ErrClearDb.pas' {fmErrClearDb},
  DotMatrix in 'DotMatrix.pas' {Form1},
  NumEmp in 'NumEmp.pas' {fmNumEmp},
  DinvItem in 'DinvItem.pas' {fmDInvItem},
  UidWhSumGR in 'UidWhSumGR.pas' {fmuidwhsumgr},
  JDep in 'JDep.pas' {fmJDep},
  UIdRepeat in 'UIdRepeat.pas' {fmUidRepeat},
  getdata in 'getdata.pas' {fmData},
  NSellItem in 'NSellItem.pas' {fmNSellItem},
  Dlg in 'Dlg.pas' {FmDlg},
  JewConst in 'JewConst.pas',
  Inventory in 'Inventory.pas' {fmInventory},
  eClient in 'eClient.pas' {fmeClient},
  ListInventory in 'ListInventory.pas' {fmListInventory},
  InsideInfo in 'InsideInfo.pas' {fInsideInfo},
  InfoEditArtHist in 'InfoEditArtHist.pas' {fmInfoEditArtHist},
  UseComp in 'UseComp.pas' {fmUseComp},
  UseClient in 'UseClient.pas' {fmUseClient},
  ApplDep in 'ApplDep.pas' {fmApplDep},
  ApplDepItem in 'ApplDepItem.pas' {fmApplDepItem},
  AddApplItem in 'AddApplItem.pas' {fmAddApplDepItem},
  DiscountSum in 'DiscountSum.pas' {fmDiscountSum},
  WriteLogBase in 'WriteLogBase.pas' {fmWriteLogBase},
  RepSetting in 'RepSetting.pas' {fmRepSetting},
  VibCard in 'VibCard.pas' {fmVibcard},
  OptBuyerCase in 'OptBuyerCase.pas' {fmOptBuyerCase},
  ShopReport in 'ShopReport.pas' {fmShopReport},
  ShopReportList in 'ShopReportList.pas' {fmShopReportList},
  ActAllowancesList in 'ActAllowancesList.pas' {fmActAllowancesList},
  SelectDep in 'SelectDep.pas' {fmSelectDep},
  ActAllowances in 'ActAllowances.pas' {fmActAllowances},
  M207LoginEdit in 'Units\M207LoginEdit.pas' {fmLoginEdit},
  M207IBLogin in 'Units\M207IBLogin.pas' {fmDBLogin},
  AllowancesPrn in 'AllowancesPrn.pas' {fmActAllowancesPrn},
  ShopReport_CheckData in 'ShopReport_CheckData.pas' {fmShopReport_CheckData},
  UIDStoreList in 'UIDStoreList.pas' {fmUIDStoreList},
  UIDStore in 'UIDStore.pas' {fmUIDStore},
  UIDStore_Detail in 'UIDStore_Detail.pas' {fmUIDStore_Detail},
  UIDStore_T in 'UIDStore_T.pas' {fmUIDStore_T},
  UIDStoreCheck in 'UIDStoreCheck.pas' {fmUIDStoreCheck},
  UIDStoreItem in 'UIDStoreItem.pas' {fmUidItem},
  WriteOff_Param in 'WriteOff_Param.pas' {frmWriteoff_Param},
  PresetRecipient in 'PresetRecipient.pas' {frmPresetRecipient},
  SurPlus in 'SurPlus.pas' {fmSurPlus},
  DInvCheck in 'DInvCheck.pas' {frmDInvCheck},
  Equipment in 'Equipment.pas' {fmEquipment},
  EditEquipment in 'EditEquipment.pas' {fmEditEquipment},
  SInvImport in 'SInvImport.pas' {fmSInvImport},
  UIDStore_T_C in 'UIDStore_T_C.pas' {fmUIDStore_T_C},
  SameUidP in 'SameUidP.pas' {fmSameUIDP},
  Mol in 'Mol.pas' {fmMol},
  PredInventory in 'PredInventory.pas' {fmPredInverty},
  UidStoreDepList in 'UidStoreDepList.pas' {fmUidStoreDepList},
  UidStoreDepItem in 'UidStoreDepItem.pas' {fmUidStoreDepItem},
  UIDStoreDep_T_C in 'UIDStoreDep_T_C.pas' {fmUIDStoreDep_T_C},
  Payment in 'Payment.pas' {fmPayment},
  BalanceB in 'BalanceB.pas' {fmBalanceB},
  RepeatUidAppl in 'RepeatUidAppl.pas' {fmRepeatUidAppl},
  dAtt1 in 'dAtt1.pas' {fmdAtt1},
  dAtt2 in 'dAtt2.pas' {fmdAtt2},
  SnameSup in 'SnameSup.pas' {fmSnameSup},
  PayTypeG in 'PayTypeG.pas' {fmPayG},
  RespStoring in 'RespStoring.pas' {fmRespStoring},
  RepairList in 'RepairList.pas' {fmRepairList},
  Repair in 'Repair.pas' {fmRepair},
  BalanceInv in 'BalanceInv.pas' {fmBalanceInv},
  SuspItemList in 'SuspItemList.pas' {fmSuspItemList},
  SuspItem in 'SuspItem.pas' {fmSuspItem},
  TerminalCard in 'TerminalCard.pas' {fmTermCards},
  NotifClient in 'NotifClient.pas',
  AddUidAct in 'AddUidAct.pas' {fmAddUidAct},
  UserUidWh in 'UserUidWh.pas' {fmUserUIdWh},
  SelectUidWh in 'SelectUidWh.pas' {fmSelectUidWh},
  CostCard in 'CostCard.pas' {fmCostCard},
  TermCardUid in 'TermCardUid.pas' {fmTermCardUid},
  EditPswd in 'EditPswd.pas' {fmEditPswd},
  Return_Act_Print in 'Pol\Return\Act\Print\Return_Act_Print.pas' {dm_Return_Act_Print: TDataModule},
  uUtils in 'uUtils.pas',
  frmDemo in 'frmDemo.pas' {Form2},
  frmDialogJournalClient in 'frmDialogJournalClient.pas' {DialogJournalClient},
  frmUnConfirmedOrders in 'frmUnConfirmedOrders.pas' {DialogUnconfirmedOrders},
  SellItem in 'SellItem.pas' {fmSellItem},
  NotSaleItems in 'NotSaleItems.pas' {fmNotSaleItems},
  ListItem in 'ListItem.pas' {fmListItem},
  DetalSell in 'DetalSell.pas' {fmDetalSell},
  Unit3 in 'Unit3.pas' {ColorShema},
  AnlzClient_SellDep in 'AnlzClient_SellDep.pas' {fmAnlzClient_SellDep},
  uScanCode in 'uScanCode.pas',
  uPluginClients in 'Clients\uPluginClients.pas',
  ControlSplash in 'ControlSplash.pas' {fmControlSplash},
  frmAnalizeCDM in 'frmAnalizeCDM.pas' {DialogAnalizeCDM},
  Hist in 'Hist.pas' {fmHist},
  uApplication in 'uApplication.pas',
  Clsinv_Check in 'Clsinv_Check.pas' {fmCLSINV_CHECK},
  Sertificate in 'Sertificate.pas' {fmSertificate},
  SellSert in 'SellSert.pas' {fmSellSert},
  Lexems in 'Lexems.pas',
  SelDep_anlz in 'SelDep_anlz.pas' {fmSelDep_anlz},
  DepAndDate in 'DepAndDate.pas' {fmDepAndDate},
  SellDeviceSum in 'SellDeviceSum.pas' {fmDeviceSell},
  dmBuy in 'Buy\dmBuy.pas' {DataBuy: TDataModule},
  frmBuyJournal in 'Buy\frmBuyJournal.pas' {FrameBuyJournal: TFrame},
  frmBuyInvoice in 'Buy\frmBuyInvoice.pas' {FrameBuyInvoice: TFrame},
  frmPopup in 'Buy\frmPopup.pas' {Popup},
  frmBuy in 'Buy\frmBuy.pas' {DialogBuy},
  frmBuyJournalSession in 'Buy\frmBuyJournalSession.pas' {FrameBuyJournalSession: TFrame},
  frmBuyJournalMoving in 'Buy\frmBuyJournalMoving.pas' {FrameBuyJournalMoving: TFrame},
  frmBuyJournalAffinage in 'Buy\frmBuyJournalAffinage.pas' {FrameBuyJournalAffinage: TFrame},
  frmBuyJournalRecycling in 'Buy\frmBuyJournalRecycling.pas' {FrameBuyJournalRecycling: TFrame},
  frmBuyJournalTrade in 'Buy\frmBuyJournalTrade.pas' {FrameBuyJournalTrade: TFrame},
  frmBuyInvoiceSession in 'Buy\frmBuyInvoiceSession.pas' {FrameBuyInvoiceSession: TFrame},
  frmBuyInvoiceMoving in 'Buy\frmBuyInvoiceMoving.pas' {FrameBuyInvoiceMoving: TFrame},
  frmBuyInvoiceAffinage in 'Buy\frmBuyInvoiceAffinage.pas' {FrameBuyInvoiceAffinage: TFrame},
  frmBuyInvoiceRecycling in 'Buy\frmBuyInvoiceRecycling.pas' {FrameBuyInvoiceRecycling: TFrame},
  frmBuyInvoiceTrade in 'Buy\frmBuyInvoiceTrade.pas' {FrameBuyInvoiceTrade: TFrame},
  frmBuyStore in 'Buy\frmBuyStore.pas' {FrameBuyStore: TFrame},
  dmBuyCollation in 'Buy\dmBuyCollation.pas' {DataBuyCollation: TDataModule},
  uDialog in 'Buy\uDialog.pas',
  uSound in 'Buy\uSound.pas',
  frmBuyInvoiceReport in 'Buy\frmBuyInvoiceReport.pas' {DialogBuyInvoiceReport},
  frmBuyOrders in 'Buy\frmBuyOrders.pas' {FrameBuyOrders: TFrame},
  dmBuyOrders in 'Buy\dmBuyOrders.pas' {BuyOrdersData: TDataModule},
  frmBuyOrder in 'Buy\frmBuyOrder.pas' {FrameBuyOrder: TFrame},
  dmBuyDictionary in 'Buy\dmBuyDictionary.pas' {BuyDictionary: TDataModule},
  frmBuySetup in 'Buy\frmBuySetup.pas' {DialogBuySetup},
  uGridPrinter in 'Buy\uGridPrinter.pas',
  uBuy in 'Buy\uBuy.pas',
  frmAnalisisClientSell in 'Analysis\frmAnalisisClientSell.pas' {AnalisisClientSell},
  frmAnalisisSalary in 'Analysis\frmAnalisisSalary.pas' {AnalisisSalary},
  frmBuyInvoiceUnion in 'Buy\frmBuyInvoiceUnion.pas' {BuyInvoiceUnion},
  frmBuyDatePopup in 'Buy\frmBuyDatePopup.pas' {DialogBuyDatePopup},
  frmBuyRangePopup in 'Buy\frmBuyRangePopup.pas' {DialogBuyRangePopup},
  frmBuyMonthPopup in 'Buy\frmBuyMonthPopup.pas' {DialogBuyMonthPopup},
  uPrinter in 'Buy\uPrinter.pas',
  uInterface in 'Clients\uInterface.pas',
  uClient in 'Clients\uClient.pas',
  uView in 'Clients\uView.pas',
  uClientBalance in 'Clients\uClientBalance.pas',
  uClients in 'Clients\uClients.pas',
  frmBuyJournalCollation in 'Buy\frmBuyJournalCollation.pas' {DialogJournalCollation},
  dmBuyInvoice in 'Buy\dmBuyInvoice.pas' {DataBuyInvoice: TDataModule},
  frmBuyJournalCommon in 'Buy\frmBuyJournalCommon.pas' {FrameBuyJournalCommon: TFrame},
  frmBuyRange in 'Buy\frmBuyRange.pas' {DialogByRange},
  frmCertificateCollation in 'Buy\frmCertificateCollation.pas' {DialogCertificateCollation},
  Data in 'Data.pas' {dm: TDataModule},
  Data2 in 'Data2.pas' {dm2: TDataModule},
  frmWarehouse in 'frmWarehouse.pas' {Warehouse: TFrame},
  UIDWH in 'UIDWH.pas' {fmUIDWH},
  UIDWH2 in 'UIDWH2.pas' {fmUIDWH2},
  frmCampaign in 'frmCampaign.pas' {DialogCampaign},
  frmCertificateHistory in 'frmCertificateHistory.pas' {DialogCertificateHistory},
  frmBuyInvoiceCollation in 'Buy\frmBuyInvoiceCollation.pas' {DialogBuyInvoiceCollation},
  IndividualOrderData in 'IndividualOrderData.pas' {dmIndividualOrder: TDataModule},
  ind_order in 'ind_order.pas' {DialogOrder},
  otkaz in 'ORDER\otkaz.pas' {ot_form},
  SALE_JEW in 'ORDER\SALE_JEW.pas' {SALE_JEW_FORM},
  VP in 'ORDER\VP.pas' {VP_FORM},
  SALE_ART in 'SALE_ART.pas' {SALE_ART_FORM},
  frmBuyStart in 'Buy\frmBuyStart.pas' {FrameBuyStart: TFrame},
  frmAnalisisSalaryChart in 'Analysis\frmAnalisisSalaryChart.pas' {DialogSalaryChart},
  AnalizSkl in 'AnalizSkl.pas' {frmAnalizSklad},
  VP_UID in 'VP_UID.pas' {VP_UID_FORM},
  frmAnalisisSalaryHost in 'Analysis\frmAnalisisSalaryHost.pas' {DialogSalaryHost},
  frmAnalisisDateSell in 'Analysis\frmAnalisisDateSell.pas' {AnalisisDateSell},

  //CashRET in 'CashRET.pas' {fmCashRet},
  frmSalaryQuorter in 'Analysis\frmSalaryQuorter.pas' {SalaryQuorter},

  RetCash in 'RetCash.pas' {FmRetCash},
  ActionST in 'ActionST.pas' {FmActionST};

{$R *.RES}

{$R .\buy\resources\de.res}

{$R .\buy\resources\dialog.res}

var i: integer;
    cHelpFile : string = 'Help\����������� ������������.hlp';
//    log_jew: TStringList;
//    FilePath: string;
    Hook: TApplicationHook;

begin
  SetApplicationGUID(JewGUID);
//  log_jew:=TStringList.Create;

  //Application.MainFormOnTaskBar := True;

  with Forms.Application do
  begin
//     log_jew.Add('1 Initialize begin');

//     FilePath := GetSpecialFolderLocation(CSIDL_APPDATA) + '\jew\' + 'Log_jew_error.txt';
//     log_jew.SaveToFile(FilePath);

     Initialize;

     Hook := TApplicationHook.Create;

     NoWH:=False;
     cHelpFile := ExtractFilePath(ExeName)+cHelpFile;
     HelpFile := cHelpFile;
     Screen.Cursors[crSQLWait] := LoadCursor(hInstance, 'sql');
     Screen.Cursors[crFind] := LoadCursor(hInstance, 'find');
     if Screen.Cursors[crFind] = 0 then
        Screen.Cursors[crFind] := LoadCursor(hInstance, 'find');

     for i:=0 to ParamCount do
     begin
        if UpperCase(ParamStr(i))='SAFELOAD' then SafeLoad:=True;
        if UpperCase(ParamStr(i))='NOWH' then NoWH:=True;
        if UpperCase(ParamStr(i))='UPDATE' then AppUpdate := True;
        if UpperCase(ParamStr(i))='DEBUG' then AppDebug := True;
     end;
     Title := '���������';
     AccFunc:=AccFunction;

//     log_jew.Add('1 Initialize end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('2 create fmSplash begin');
//     log_jew.SaveToFile(FilePath);
     fmSplash:=TfmSplash.Create(NIL);
//     log_jew.Add('2 create fmSplash end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('3 create fmErr begin');
//     log_jew.SaveToFile(FilePath);
     fmErr:=TfmErr.Create(Forms.Application);
     fmSplash.StatusBar1.Panels[0].Text:='create fmErr ...';
//     log_jew.Add('3 create fmErr end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('4 create dmCom begin');
//     log_jew.SaveToFile(FilePath);
     dmCom:=TdmCom.Create(Forms.Application);
     fmSplash.StatusBar1.Panels[0].Text:='create dmCom ...';
//     log_jew.Add('4 create dmCom end');
//     log_jew.SaveToFile(FilePath);

     if NOT dmCom.Logined then
     begin
//        log_jew.Add('5 free dmCom begin');
//        log_jew.SaveToFile(FilePath);
        dmCom.Free;
        fmSplash.StatusBar1.Panels[0].Text:='dmCom.Logined ...';
//        log_jew.Add('5 free dmCom end');
//        log_jew.SaveToFile(FilePath);
        exit;
     end;
      {$IFDEF TRIALLOCAL}
     { if dmCom.db.IsRemoteConnect then
      begin
        dmCom.Free;
        MessageDialog('�������������������� ������!', mtInformation, [mbOk], 0);
        eXit;
      end;}
      {$ENDIF}

//     log_jew.Add('6 ProcessMessages begin');
//     log_jew.SaveToFile(FilePath);
     ProcessMessages;
//     log_jew.Add('6 ProcessMessages end');
//     log_jew.SaveToFile(FilePath);
     fmSplash.SplashBar.Stepit;
//     log_jew.Add('7 create dm begin');
//     log_jew.SaveToFile(FilePath);

     dm:=Tdm.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmSplash.StatusBar1.Panels[0].Text:='create dm ...';
//     log_jew.Add('7 create dm end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('8 create dm2 begin');
//     log_jew.SaveToFile(FilePath);
     dm2:=Tdm2.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmsplash.StatusBar1.Panels[0].Text:= 'create dm2 ...';
//     log_jew.Add('8 create dm2 end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('9 create dm3 begin');
//     log_jew.SaveToFile(FilePath);
     dm3:=Tdm3.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmsplash.StatusBar1.Panels[0].Text:= 'create dm3 ...';
//     log_jew.Add('9 create dm3 end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('10 create dmFR begin');
//     log_jew.SaveToFile(FilePath);
     dmFR:=TdmFR.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmsplash.StatusBar1.Panels[0].Text:= 'create dmFR ...';
//     log_jew.Add('10 create dmFR end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('11 create dmServ begin');
//     log_jew.SaveToFile(FilePath);
     dmServ := TdmServ.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmsplash.StatusBar1.Panels[0].Text:= 'create dmServ ...';
//     log_jew.Add('11 create dmServ end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('12 create dmReport begin');
//     log_jew.SaveToFile(FilePath);
     dmReport := TdmReport.Create(Forms.Application);
     fmSplash.SplashBar.Stepit;
     fmsplash.StatusBar1.Panels[0].Text:= 'create dmReport ...';
//     log_jew.Add('12 create dmReport end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('14 processMessages begin');
//     log_jew.SaveToFile(FilePath);
     fmSplash.SplashBar.Stepit;
     ProcessMessages;
//     log_jew.Add('14 processMessages end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('15 create fmMain begin');
//     log_jew.SaveToFile(FilePath);
     CreateForm(TfmMain, fmMain);
     fmsplash.StatusBar1.Panels[0].Text:= 'create fmMain ...';
//     log_jew.Add('15 create fmMain end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('16 processMessages begin');
//     log_jew.SaveToFile(FilePath);
     ProcessMessages;
     fmSplash.SplashBar.Stepit;
//     log_jew.Add('16 processMessages end');
//     log_jew.SaveToFile(FilePath);
//
//     log_jew.Add('17 free fmSpash begin');
//     log_jew.SaveToFile(FilePath);
     fmSplash.Free;
//     log_jew.Add('17 free fmSpash end');
//     log_jew.SaveToFile(FilePath);

     fmControlSplash:=TfmControlSplash.Create(NIL);
     Run;
     Hook.Free;
  end;
end.
