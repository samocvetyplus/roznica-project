unit WHInv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, ExtCtrls,
  db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmWHInv = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    dbgrWHInv: TM207IBGrid;
    fmplWHInv: TFormPlacement;
    fmstWHInv: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure dbgrWHInvGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHInv: TfmWHInv;

implementation

uses comdata, ServData, Data, M207Proc;

{$R *.DFM}

procedure TfmWHInv.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  Self.Caption := '�����. ������� '+dmServ.taWHART.AsString+' � ��������� �� ���������';
  KeyPreview := True;
  OnKeyPress := dm.CloseFormByEsc;
  dbgrWHInv.DataSource.DataSet.Active:=True;
end;

procedure TfmWHInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dbgrWHInv.DataSource.DataSet.Active then
    dbgrWHInv.DataSource.DataSet.Close;
  Action := caFree; 
end;


procedure TfmWHInv.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHInv.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHInv.dbgrWHInvGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
 if Assigned(Field)and(Field.FieldName='ART2') then
   Background:=dmCom.clMoneyGreen
end;

procedure TfmWHInv.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

end.
