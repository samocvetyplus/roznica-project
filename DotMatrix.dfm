object Form1: TForm1
  Left = 305
  Top = 191
  Width = 783
  Height = 505
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object zrCheck: TZReport
    Left = 0
    Top = 0
    Width = 109
    Height = 19
    DataOptions.AutoOpen = True
    DataSet = dmReport.ibdsTicketIt
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      #27'@'
      #12
      #27'P'
      #27'M'
      #15
      #18
      #27'G'
      #27'H'
      #27'4'
      #27'5'
      #27'-1'
      ''
      #27'S'#1
      #27'T'
      #27'S'#0
      #27'T'
      ''
      ''
      ''
      '')
    Options.PageFrom = 1
    Options.PageTo = 3
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    BeforePrint = zrCheckBeforePrint
    object zrvR_City: TZRField
      Format.Width = 20
      DataField = 'R_CITY'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvR_Address: TZRField
      Format.Width = 40
      DataField = 'R_ADDRESS'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvPhone: TZRField
      Format.Width = 20
      DataField = 'R_PHONE'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvChekNo: TZRField
      Format.Width = 10
      DataField = 'R_CHECKNO'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvSellDate: TZRField
      Format.Width = 18
      DataField = 'R_SELLDATE'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvR_Mat: TZRField
      Format.Width = 20
      DataField = 'R_MAT'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvR_Good: TZRField
      Format.Width = 20
      DataField = 'R_GOOD'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvR_Art: TZRField
      Format.Width = 20
      DataField = 'R_ART'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvR_Art2: TZRField
      Format.Width = 20
      DataField = 'R_ART2'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvUID: TZRField
      Format.Width = 10
      DataField = 'R_UID'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvW: TZRField
      Format.Width = 10
      DataField = 'R_W'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvSz: TZRField
      Format.Width = 10
      DataField = 'R_SIZE'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvUNIT: TZRField
      Format.Width = 10
      DataField = 'R_UNIT'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvPrice: TZRField
      Format.Width = 10
      DataField = 'R_PRICE'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvSum: TZRField
      Format.Width = 10
      DataField = 'R_COST'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvDiscount: TZRField
      Format.Width = 10
      DataField = 'R_DISCOUNT'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvSum1: TZRField
      Format.Width = 10
      DataField = 'R_DISCCOST'
      DataSet = dmReport.ibdsTicketIt
    end
    object zrvTotalSum: TZRAggregator
      Format.Width = 10
      Variable = zrvSum
    end
    object zrvTotalSum1: TZRAggregator
      Format.Width = 10
      Variable = zrvSum1
    end
    object zrCheckHeader: TZRBand
      Left = 0
      Top = 0
      Width = 109
      Height = 6
      Stretch = False
      BandType = zbtHeader
      object ZRLabel1: TZRLabel
        Left = 13
        Top = 4
        Width = 8
        Height = 1
        Caption = 'ZRLabel1'
        Variable = zrvChekNo
      end
      object ZRLabel2: TZRLabel
        Left = 0
        Top = 4
        Width = 12
        Height = 1
        Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1095#1077#1082
      end
      object ZRLabel4: TZRLabel
        Left = 0
        Top = 0
        Width = 18
        Height = 1
        FontStyles = [zfsItalic]
        Caption = #1070#1074#1077#1083#1080#1088#1085#1099#1081' '#1084#1072#1075#1072#1079#1080#1085
      end
      object zrslDate: TZRSystemLabel
        Left = 4
        Top = 5
        Width = 14
        Height = 1
        DataKind = zsdDate
      end
      object ZRSystemLabel1: TZRSystemLabel
        Left = 45
        Top = 5
        Width = 15
        Height = 1
        DataKind = zsdDate
      end
      object ZRLabel5: TZRLabel
        Left = 0
        Top = 1
        Width = 18
        Height = 1
        FontStyles = [zfsBold]
        Caption = '"'#1057#1072#1084#1086#1094#1074#1077#1090#1099'"'
      end
      object ZRLabel6: TZRLabel
        Left = 0
        Top = 2
        Width = 9
        Height = 1
        Caption = 'ZRLabel6'
        Variable = zrvR_City
      end
      object ZRLabel7: TZRLabel
        Left = 9
        Top = 2
        Width = 26
        Height = 1
        Caption = 'ZRLabel7'
        Variable = zrvR_Address
      end
      object ZRLabel8: TZRLabel
        Left = 4
        Top = 3
        Width = 14
        Height = 1
        Caption = 'ZRLabel8'
        Variable = zrvPhone
      end
      object ZRLabel9: TZRLabel
        Left = 41
        Top = 0
        Width = 18
        Height = 1
        FontStyles = [zfsItalic]
        Caption = #1070#1074#1077#1083#1080#1088#1085#1099#1081' '#1084#1072#1075#1072#1079#1080#1085
      end
      object ZRLabel10: TZRLabel
        Left = 41
        Top = 1
        Width = 18
        Height = 1
        FontStyles = [zfsBold]
        Caption = '"'#1057#1072#1084#1086#1094#1074#1077#1090#1099'"'
      end
      object ZRLabel11: TZRLabel
        Left = 41
        Top = 2
        Width = 10
        Height = 1
        Caption = 'ZRLabel11'
        Variable = zrvR_City
      end
      object ZRLabel12: TZRLabel
        Left = 51
        Top = 2
        Width = 28
        Height = 1
        Caption = 'ZRLabel12'
        Variable = zrvR_Address
      end
      object ZRLabel13: TZRLabel
        Left = 45
        Top = 3
        Width = 15
        Height = 1
        Caption = 'ZRLabel13'
        Variable = zrvPhone
      end
      object ZRLabel14: TZRLabel
        Left = 41
        Top = 4
        Width = 13
        Height = 1
        Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1095#1077#1082
      end
      object ZRLabel15: TZRLabel
        Left = 55
        Top = 4
        Width = 8
        Height = 1
        Caption = 'ZRLabel15'
        Variable = zrvChekNo
      end
    end
    object zrCheckDetail: TZRBand
      Left = 0
      Top = 6
      Width = 109
      Height = 9
      Stretch = False
      BandType = zbtDetail
      object ZRLabel16: TZRLabel
        Left = 0
        Top = 1
        Width = 40
        Height = 1
        Caption = 'ZRLabel16'
        Variable = zrvR_Mat
      end
      object ZRLabel17: TZRLabel
        Left = 0
        Top = 2
        Width = 15
        Height = 1
        Caption = 'ZRLabel17'
        Variable = zrvR_Good
      end
      object ZRLabel19: TZRLabel
        Left = 27
        Top = 2
        Width = 13
        Height = 1
        Caption = 'ZRLabel19'
        Variable = zrvR_Art2
      end
      object ZRLabel18: TZRLabel
        Left = 15
        Top = 2
        Width = 12
        Height = 1
        Caption = 'ZRLabel18'
        Variable = zrvR_Art
      end
      object ZRLabel3: TZRLabel
        Left = 0
        Top = 3
        Width = 10
        Height = 1
        Caption = 'ZRLabel3'
        Variable = zrvUID
      end
      object ZRLabel20: TZRLabel
        Left = 11
        Top = 3
        Width = 6
        Height = 1
        Caption = 'ZRLabel20'
        Variable = zrvUNIT
      end
      object ZRLabel21: TZRLabel
        Left = 18
        Top = 3
        Width = 7
        Height = 1
        Caption = 'ZRLabel21'
        Variable = zrvW
      end
      object ZRLabel22: TZRLabel
        Left = 27
        Top = 3
        Width = 8
        Height = 1
        Caption = 'ZRLabel22'
        Variable = zrvSz
      end
      object ZRLabel28: TZRLabel
        Left = 0
        Top = 4
        Width = 16
        Height = 1
        Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076#1080#1085#1080#1094#1091
      end
      object ZRLabel23: TZRLabel
        Left = 16
        Top = 4
        Width = 13
        Height = 1
        Caption = 'ZRLabel23'
        Variable = zrvPrice
      end
      object ZRLabel29: TZRLabel
        Left = 0
        Top = 5
        Width = 8
        Height = 1
        Caption = #1057#1091#1084#1084#1072
      end
      object ZRLabel24: TZRLabel
        Left = 16
        Top = 5
        Width = 13
        Height = 1
        Caption = 'ZRLabel24'
        Variable = zrvSum
      end
      object ZRLabel30: TZRLabel
        Left = 0
        Top = 6
        Width = 7
        Height = 1
        Caption = #1057#1082#1080#1076#1082#1072
      end
      object ZRLabel25: TZRLabel
        Left = 7
        Top = 6
        Width = 2
        Height = 1
        Alignment.X = zawRight
        Caption = 'ZRLabel25'
        Variable = zrvDiscount
      end
      object ZRLabel26: TZRLabel
        Left = 9
        Top = 6
        Width = 1
        Height = 1
        Caption = '%'
      end
      object ZRLabel31: TZRLabel
        Left = 13
        Top = 6
        Width = 18
        Height = 1
        Caption = #1057#1091#1084#1084#1072' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
      end
      object ZRLabel27: TZRLabel
        Left = 31
        Top = 6
        Width = 9
        Height = 1
        Caption = 'ZRLabel27'
        Variable = zrvSum1
      end
      object ZRLabel32: TZRLabel
        Left = 41
        Top = 1
        Width = 40
        Height = 1
        Caption = 'ZRLabel32'
        Variable = zrvR_Mat
      end
      object ZRLabel33: TZRLabel
        Left = 41
        Top = 2
        Width = 16
        Height = 1
        Caption = 'ZRLabel33'
        Variable = zrvR_Good
      end
      object ZRLabel34: TZRLabel
        Left = 57
        Top = 2
        Width = 15
        Height = 1
        Caption = 'ZRLabel34'
        Variable = zrvR_Art
      end
      object ZRLabel35: TZRLabel
        Left = 72
        Top = 2
        Width = 9
        Height = 1
        Caption = 'ZRLabel35'
        Variable = zrvR_Art2
      end
      object ZRLabel36: TZRLabel
        Left = 41
        Top = 3
        Width = 12
        Height = 1
        Caption = 'ZRLabel36'
        Variable = zrvUID
      end
      object ZRLabel37: TZRLabel
        Left = 54
        Top = 3
        Width = 6
        Height = 1
        Caption = 'ZRLabel37'
        Variable = zrvUNIT
      end
      object ZRLabel38: TZRLabel
        Left = 61
        Top = 3
        Width = 8
        Height = 1
        Caption = 'ZRLabel38'
        Variable = zrvW
      end
      object ZRLabel39: TZRLabel
        Left = 71
        Top = 3
        Width = 8
        Height = 1
        Caption = 'ZRLabel39'
        Variable = zrvSz
      end
      object ZRLabel40: TZRLabel
        Left = 41
        Top = 4
        Width = 16
        Height = 1
        Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076#1080#1085#1080#1094#1091
      end
      object ZRLabel41: TZRLabel
        Left = 57
        Top = 4
        Width = 16
        Height = 1
        Caption = 'ZRLabel41'
        Variable = zrvPrice
      end
      object ZRLabel42: TZRLabel
        Left = 41
        Top = 5
        Width = 8
        Height = 1
        Caption = #1057#1091#1084#1084#1072
      end
      object ZRLabel43: TZRLabel
        Left = 57
        Top = 5
        Width = 16
        Height = 1
        Caption = 'ZRLabel43'
        Variable = zrvSum
      end
      object ZRLabel44: TZRLabel
        Left = 41
        Top = 6
        Width = 7
        Height = 1
        Caption = #1057#1082#1080#1076#1082#1072
      end
      object ZRLabel45: TZRLabel
        Left = 48
        Top = 6
        Width = 2
        Height = 1
        Alignment.X = zawRight
        Caption = 'ZRLabel45'
        Variable = zrvDiscount
      end
      object ZRLabel46: TZRLabel
        Left = 50
        Top = 6
        Width = 1
        Height = 1
        Caption = '%'
      end
      object ZRLabel47: TZRLabel
        Left = 54
        Top = 6
        Width = 17
        Height = 1
        Caption = #1057#1091#1084#1084#1072' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
      end
      object ZRLabel48: TZRLabel
        Left = 71
        Top = 6
        Width = 10
        Height = 1
        Caption = 'ZRLabel48'
        Variable = zrvSum1
      end
    end
    object zrCheckFooter: TZRBand
      Left = 0
      Top = 15
      Width = 109
      Height = 3
      Stretch = False
      BandType = zbtFooter
      object ZRLabel49: TZRLabel
        Left = 0
        Top = 0
        Width = 26
        Height = 1
        Caption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1091#1084#1084#1072' '#1073#1077#1079' '#1089#1082#1080#1076#1082#1086#1081
      end
      object ZRTotalLabel1: TZRTotalLabel
        Left = 27
        Top = 0
        Width = 13
        Height = 1
        Kind = ztkSum
        Level = zrCheck
        Variable = zrvTotalSum
      end
      object ZRTotalLabel2: TZRTotalLabel
        Left = 27
        Top = 1
        Width = 13
        Height = 1
        Kind = ztkSum
        Level = zrCheck
        Variable = zrvTotalSum1
      end
      object ZRLabel50: TZRLabel
        Left = 0
        Top = 1
        Width = 26
        Height = 1
        Caption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1091#1084#1084#1072' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
      end
      object ZRLabel51: TZRLabel
        Left = 41
        Top = 0
        Width = 26
        Height = 1
        Caption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1091#1084#1084#1072' '#1073#1077#1079' '#1089#1082#1080#1076#1082#1086#1081
      end
      object ZRTotalLabel3: TZRTotalLabel
        Left = 68
        Top = 0
        Width = 13
        Height = 1
        Kind = ztkSum
        Level = zrCheck
        Variable = zrvTotalSum
      end
      object ZRTotalLabel4: TZRTotalLabel
        Left = 68
        Top = 1
        Width = 13
        Height = 1
        Kind = ztkSum
        Level = zrCheck
        Variable = zrvTotalSum1
      end
      object ZRLabel52: TZRLabel
        Left = 41
        Top = 1
        Width = 26
        Height = 1
        Caption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1091#1084#1084#1072' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
      end
    end
  end
  object Button1: TButton
    Left = 208
    Top = 400
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 1
    OnClick = Button1Click
  end
end
