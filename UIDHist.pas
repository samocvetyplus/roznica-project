{*******************************
  ������� �������
********************************}

unit UIDHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, RXSpin, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, Menus, DBCtrls, ActnList, DBGridEh,
  PrnDbgeh, PrntsEh, Printers, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar, Mask;

type
  TfmUIDHist = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    fr1: TFormStorage;
    seUID: TRxSpinEdit;
    Label1: TLabel;
    siOpen: TSpeedItem;
    siPrOrd: TSpeedItem;
    pm1: TPopupMenu;
    N1: TMenuItem;
    DBText1: TDBText;
    acList: TActionList;
    acInscounter: TAction;
    acClose: TAction;
    dg1: TDBGridEh;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure siOpenClick(Sender: TObject);
    procedure seUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siPrOrdClick(Sender: TObject);
    procedure acInscounterExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmUIDHist: TfmUIDHist;

implementation

uses comdata, Data, Data2, PrOrd, M207Proc, uUtils;

{$R *.DFM}

procedure TfmUIDHist.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmUIDHist.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dmCom, dm , dm2 do
    begin
      dsPrOrd.DataSet:=quPrOrd;
      if UID2Find<>-1 then
        begin
          seUID.Value:=UID2Find;
          siOpenClick(NIL);
        end;
    end;
  dg1.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dg1', [crpColIndexEh, crpColWidthsEh]);
   //--------- ��������� ����� ��� ��������� �� �������� ----------------//
   dg1.FieldColumns['SPRICE'].Visible := CenterDep;
  //---------------------------------------------------------------------//
end;

procedure TfmUIDHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 dg1.SaveColumnsLayoutIni(GetIniFileName, Name+'_dg1', true);
end;

procedure TfmUIDHist.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUIDHist.siOpenClick(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dm2, quUIDHist, Params do
    begin
      Active:=False;
      Params[0].AsInteger:=Round(seUID.Value);
      Open;
    end;

  with dm2, quGetUIDA2, Params do
    begin
      Active:=False;
      Params[0].AsInteger:=Round(seUID.Value);
      Open;
    end;

  Screen.Cursor:=crDefault;
end;

procedure TfmUIDHist.seUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then  siOpenClick(NIL);
  if Key=VK_F12 then Close;
end;

procedure TfmUIDHist.siPrOrdClick(Sender: TObject);
begin
  with dm, dm2 do
    if quUIDHistOP.AsInteger=4 then
      begin
        PrOrdId:=quUIDHistPrOrdId.AsInteger;
        quPrOrd.Active:=True;
        ShowAndFreeForm(TfmPrOrd, Self, TForm(fmPrOrd), True, False);
        quPrOrd.Active:=False;        
      end;
end;

procedure TfmUIDHist.acInscounterExecute(Sender: TObject);
begin
 dg1.FieldColumns['INSCOUNTER'].Visible:= not dg1.FieldColumns['INSCOUNTER'].Visible;
end;

procedure TfmUIDHist.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmUIDHist.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdg1.Title.Clear;
 pdg1.Title.Add('');
 pdg1.Title.Add('������� ������� '+ trim(seUid.Text)+' ('+trim(dm2.quGetUIDA2FULLART.AsString)+')');
 pdg1.Title.Add('');
 pdg1.Print;
end;

procedure TfmUIDHist.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if not dm2.quUIDHistOP.IsNull then
//  if Column.FieldName='StateInv' then
//  begin
    if dm2.quUIDHistISCLOSED.AsInteger=0 then Background:=clInfoBk
    else Background:=clBtnFace
//  end;
end;

procedure TfmUIDHist.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100510)
end;

procedure TfmUIDHist.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
