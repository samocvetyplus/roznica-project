object fmHistItem: TfmHistItem
  Left = 173
  Top = 168
  HelpContext = 100500
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103' #'
  ClientHeight = 518
  ClientWidth = 904
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 281
    Width = 904
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Label3: TLabel
    Left = 8
    Top = 1
    Width = 54
    Height = 13
    Caption = #1055#1086#1089#1090#1091#1087#1080#1083#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DBText1: TDBText
    Left = 100
    Top = 1
    Width = 697
    Height = 15
    DataField = 'TD'
    DataSource = dmServ.dsrHistArtSum
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 904
    Height = 41
    HelpContext = 123
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 483
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 419
      Top = 3
      Visible = True
      OnClick = spitHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object grHistArt: TM207IBGrid
    Left = 0
    Top = 70
    Width = 904
    Height = 211
    Align = alTop
    Color = clBtnFace
    DataSource = dmServ.dsrHistArt1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = grHistArtDrawColumnCell
    IniStorage = fmstHistItem
    TitleButtons = True
    OnGetCellParams = grHistArtGetCellParams
    MultiShortCut = 116
    ColorShortCut = 0
    InfoShortCut = 0
    MultiSelectMode = msmTeapot
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_GOODID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 52
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_MATID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 57
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_ART'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 79
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_ART2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_SN'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_SDATE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 64
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_DEPNAME'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 64
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_SUPNAME'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 64
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_W'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_C'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_PRICE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_PRICE2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Descr'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 209
        Visible = True
      end>
  end
  object grItem: TM207IBGrid
    Left = 0
    Top = 313
    Width = 904
    Height = 104
    Align = alClient
    Color = clBtnFace
    DataSource = dmServ.dsrHistItem
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = fmstHistItem
    TitleButtons = True
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_UID'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_W'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_SZ'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 119
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_ART'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_ART2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 64
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_UNITID'
        PickList.Strings = (
          #1043#1088
          #1064#1090)
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'R_PRICE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_PRICE2'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'R_CURRPRICE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'R_PRICE2OLD'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object stbrHistArt: TStatusBar
    Left = 0
    Top = 499
    Width = 904
    Height = 19
    Panels = <>
    SimplePanel = True
    SimpleText = 'Alt+Z - '#1087#1086#1080#1089#1082' '#1080#1079#1076#1077#1083#1080#1103' '#1087#1086' '#1085#1086#1084#1077#1088#1091';'
  end
  object tb3: TSpeedBar
    Left = 0
    Top = 284
    Width = 904
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 3
    InternalVer = 1
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 904
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbAllowResize, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 5
    ExplicitTop = 35
    InternalVer = 1
    object lbFind: TLabel
      Left = 256
      Top = 8
      Width = 44
      Height = 13
      Caption = #1048#1079#1076#1077#1083#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbArt: TLabel
      Left = 8
      Top = 8
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 560
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object spbtnPeriod: TSpeedButton
      Left = 476
      Top = 3
      Width = 81
      Height = 22
      Caption = #1055#1077#1088#1080#1086#1076
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000000000000000
        FF00FF000000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00
        FF000000000000000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FF00FFFF00FFFF00FFFF00
        FFFF00FF000000000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FF000000000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000000000000000000000FF00
        FFFF00FFFF00FFFF00FFFFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF00FFFF000000FF00FF0000FF0000FFFFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000000000FF00
        FFFF00FF0000FF0000FF00000000000000000000FFFFFFFFFF00FFFF000000FF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      OnClick = spbtnPeriodClick
    end
    object spitCase: TSpeedButton
      Tag = 2
      Left = 204
      Top = 2
      Width = 23
      Height = 24
      Hint = #1055#1086#1083#1085#1086#1077' '#1089#1086#1074#1087#1072#1076#1077#1085#1080#1077
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      OnClick = spitCaseClick
    end
    object edFindUID: TEdit
      Left = 304
      Top = 4
      Width = 81
      Height = 21
      TabOrder = 0
      Text = 'edFindUID'
      OnKeyDown = edFindUIDKeyDown
      OnKeyPress = edFindUIDKeyPress
    end
    object edFindArt: TEdit
      Left = 55
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'edFindArt'
      OnKeyDown = edFindArtKeyDown
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object spitFindFirst: TSpeedItem
      Caption = #1053#1072#1081#1090#1080
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888800000888880000080F000888880F00080F000888880F
        0008000000080000000800F000000F00000800F000800F00000800F000800F00
        00088000000000000088880F00080F0008888800000800000888888000888000
        88888880F08880F0888888800088800088888888888888888888}
      Hint = #1053#1072#1081#1090#1080'|'
      Spacing = 1
      Left = 387
      Top = 3
      Visible = True
      OnClick = spitFindFirstClick
      SectionName = 'Untitled (0)'
    end
    object spitFindNext: TSpeedItem
      Caption = #1044#1072#1083#1077#1077
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1000
        10001F7C1F7C1F7C1F7C1F7C10001F7C10001F7C10001F7C1000100010001000
        100010001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1000
        10001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000000000001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000000000001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C00001042000000001F7C00001042000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C00001F7C000000001F7C00001F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C0000000000000000000000000000000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C00001042000000000000104200001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C000000001F7C000000001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1044#1072#1083#1077#1077'|'
      Spacing = 1
      Left = 411
      Top = 3
      Visible = True
      OnClick = spitFindNextClick
      SectionName = 'Untitled (0)'
    end
    object spitFindArt: TSpeedItem
      Caption = #1053#1072#1081#1090#1080
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888800000888880000080F000888880F00080F000888880F
        0008000000080000000800F000000F00000800F000800F00000800F000800F00
        00088000000000000088880F00080F0008888800000800000888888000888000
        88888880F08880F0888888800088800088888888888888888888}
      Hint = #1053#1072#1081#1090#1080'|'
      Spacing = 1
      Left = 179
      Top = 3
      Visible = True
      OnClick = spitFindArtClick
      SectionName = 'Untitled (0)'
    end
  end
  object plTotal: TPanel
    Left = 0
    Top = 417
    Width = 904
    Height = 82
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 6
    object Label1: TLabel
      Left = 240
      Top = 32
      Width = 59
      Height = 13
      Caption = #1055#1077#1088#1077#1076#1072#1085#1086' '#1089
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lbR: TLabel
      Left = 8
      Top = 49
      Width = 42
      Height = 13
      Caption = #1054#1089#1090#1072#1090#1086#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbSel: TLabel
      Left = 8
      Top = 17
      Width = 44
      Height = 13
      Caption = #1055#1088#1086#1076#1072#1085#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtD: TDBText
      Left = 335
      Top = 32
      Width = 697
      Height = 15
      DataField = 'TD'
      DataSource = dmServ.dsrHistArtSum
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object txtS: TDBText
      Left = 104
      Top = 17
      Width = 701
      Height = 14
      DataField = 'TS'
      DataSource = dmServ.dsrHistArt1Sum
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtR: TDBText
      Left = 104
      Top = 49
      Width = 705
      Height = 13
      DataField = 'TR'
      DataSource = dmServ.dsrHistArt1Sum
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 65
      Width = 71
      Height = 13
      Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtRet: TDBText
      Left = 104
      Top = 65
      Width = 793
      Height = 13
      DataField = 'TRET'
      DataSource = dmServ.dsrHistArt1Sum
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 2
      Width = 73
      Height = 13
      Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtC: TDBText
      Left = 104
      Top = 1
      Width = 697
      Height = 15
      DataField = 'TC'
      DataSource = dmServ.dsrHistArt1Sum
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 32
      Width = 69
      Height = 13
      Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtAct: TDBText
      Left = 104
      Top = 32
      Width = 793
      Height = 15
      DataField = 'TACT'
      DataSource = dmServ.dsrHistArt1Sum
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object fmstHistItem: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'tb3.Height'
      'grHistArt.Height'
      'spitCase.Tag')
    StoredValues = <>
    Left = 160
    Top = 128
  end
end
