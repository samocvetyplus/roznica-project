unit EditPswd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, ActnList, MsgDialog;

type
  TfmEditPswd = class(TForm)
    pl: TPanel;
    edOldPswd: TEdit;
    LOldPswd: TLabel;
    LNewPswd: TLabel;
    edNewPswd: TEdit;
    LRepairNewPwsd: TLabel;
    edRepairNewPswd: TEdit;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    acList: TActionList;
    acEnter: TAction;
    acEsc: TAction;
    procedure acEscExecute(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEditPswd: TfmEditPswd;

implementation
uses comdata, db;
{$R *.dfm}

procedure TfmEditPswd.acEscExecute(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TfmEditPswd.acEnterExecute(Sender: TObject);
begin
  if ActiveControl=edOldPswd then ActiveControl:=edNewPswd
  else if ActiveControl=edNewPswd then ActiveControl:=edRepairNewPswd
  else if ActiveControl=edRepairNewPswd then ActiveControl:=btOk
  else if ActiveControl= btOk then ModalResult:=mrOk
  else if ActiveControl=btCancel then ModalResult:=mrCancel
end;

procedure TfmEditPswd.FormActivate(Sender: TObject);
begin
 if dmCom.UserAllWh=false then edOldpswd.Visible:=true else
 begin
 edOldpswd.Visible:=false;
 Loldpswd.Visible:=false;
 lNewPswd.Top:=8;
 edNewPswd.Top:=32;
 LRepairNewPwsd.Top:=61;
 edRepairNewPswd.Top:=80;
 btOk.Top:=109;
 btCancel.Top:=109;
 fmEditPswd.AutoSize:=false;
 fmEditPswd.Height:=178;
 //edNewPswd.Focused:=true;
 end;
end;

procedure TfmEditPswd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if ModalResult=mrOk then
 begin
 if (dmCom.UserAllWh=false) then
 begin
  if trim(dmcom.taEmpPSWD.AsString)<>edOldPswd.Text then
  begin
   MessageDialog('������ ������ ������ �� �����!!!', mtInformation, [mbOk], 0);
   ActiveControl:=edOldPswd;
   sysutils.Abort;
  end;
 end;

  if edNewPswd.Text<>edRepairNewPswd.Text then begin
   MessageDialog('�� ����� ������� ������������� ������', mtInformation, [mbOk], 0);
   ActiveControl:=edRepairNewPswd;
   sysutils.Abort;
  end;

  if not (dmcom.taEmp.State in [dsEdit, dsInsert]) then dmcom.taEmp.Edit;
  dmcom.taEmpPSWD.AsString:=edNewPswd.Text;
 end;
end;

end.
