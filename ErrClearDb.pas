unit ErrClearDb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfmErrClearDb = class(TForm)
    edtext: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    RadioGroup1: TRadioGroup;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtextKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrClearDb: TfmErrClearDb;

implementation

{$R *.dfm}
uses main;

procedure TfmErrClearDb.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_ESCAPE then
 begin
  ModalResult := mrCancel;
//  close;
 end;
end;

procedure TfmErrClearDb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 try
  fmMain.NumErrClear:=strtoint(edtext.Text);
  fmMain.NumErr:=RadioGroup1.ItemIndex;
 except
  fmMain.NumErrClear:=0;
 end;
{ if ModalResult=mrOK then fmMain.NumErrClear:=strtoint(edtext.Text)
 else fmMain.NumErrClear:=0;}
end;

procedure TfmErrClearDb.edtextKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then ModalResult := mrOk;
end;

end.
