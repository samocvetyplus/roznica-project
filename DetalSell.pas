unit DetalSell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, StdCtrls, dbUtil,
  ExtCtrls, jpeg, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmDetalSell = class(TForm)
    dsDetal: TDataSource;
    quDetal: TpFIBDataSet;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siclose: TSpeedItem;
    Label1: TLabel;
    quDetalDEP_NAME: TFIBStringField;
    quDetalUID: TFIBIntegerField;
    quDetalRN: TFIBIntegerField;
    quDetalADATE: TFIBDateTimeField;
    quDetalCOST: TFIBFloatField;
    quDetalCSELL: TFIBFloatField;
    quDetalCRET: TFIBFloatField;
    DB_Client_Sell: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure sicloseClick(Sender: TObject);
  private
    { Private declarations }
    Card: Variant;
  public
    { Public declarations }
  end;

var
  fmDetalSell: TfmDetalSell;

implementation

uses ComData, frmDialogJournalClient;
{$R *.dfm}

procedure TfmDetalSell.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  DB_Client_Sell.SelectedField := DialogJournalClient.DataSetJounalClientCARD;
  card := DialogJournalClient.GridDBBandedTableViewCARD.EditValue;
  label1.Caption:= '���������� ����� � '+ card;
  with TpFIBDataSet(quDetal) do
    begin
         ParamByName('CARD').AsString := Card;
    end;
 OpenDataSet(quDetal);
end;

procedure TfmDetalSell.sicloseClick(Sender: TObject);
begin
close;
end;

end.
