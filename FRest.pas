unit FRest;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ComCtrls, db, StdCtrls, DBGridEh, PrnDbgeh,
  ActnList, Menus, TB2Item, DBGridEhGrouping,
  rxSpeedbar, GridsEh;

type
  TfmFRest = class(TForm)
    dg1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    siCalc: TSpeedItem;
    tb2: TSpeedBar;
    edArt: TEdit;
    Label1: TLabel;
    cbSZ: TCheckBox;
    pdbg: TPrintDBGridEh;
    siPrint: TSpeedItem;
    siUpdate: TSpeedItem;
    aclist: TActionList;
    acUpdate: TAction;
    acCalc: TAction;
    acSz: TAction;
    SpeedbarSection2: TSpeedbarSection;
    acSortDep: TAction;
    lbdep: TLabel;
    acPrint: TAction;
    Label2: TLabel;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siCalcClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acUpdateExecute(Sender: TObject);
    procedure acSzExecute(Sender: TObject);
    procedure acSortDepExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmFRest: TfmFRest;

implementation

uses comdata, Data, Data2, M207Proc, uUtils;

{$R *.dfm}


procedure TfmFRest.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmFRest.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmFRest.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmFRest.FormCreate(Sender: TObject);
var i: integer;
    c: TColumnEh;
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dmCom, dm, dm2 do
    begin
      FilterArt:='';
      if not tr.Active then tr.StartTransaction;
      dsFRest.DataSet:=quFRest;

{*************************}
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dg1.Columns.Add;
          c.Field:=quFRest.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+'|���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dg1.Columns.Add;
          c.Field:=quFRest.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+'|�-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;
        end;
      quFRest.Active:=True;
    end;

  dg1.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dg1', [crpColIndexEh, crpColWidthsEh, crpColVisibleEh]);
  ActiveControl:=edArt;
  dg1.FieldColumns['SZ'].Visible:=False;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;  
end;



procedure TfmFRest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      quFRest.Active:=False;
      tr.CommitRetaining;
      FilterArt:='';
    end;
  dg1.FieldColumns['SZ'].Visible:=False;
  dg1.SaveColumnsLayoutIni(GetIniFileName, Name+'_dg1', true);
end;

procedure TfmFRest.siCalcClick(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dmCom, dm, dm2 do
    begin
      dg1.DataSource.DataSet.Active:=False;
      tr.CommitRetaining;
      if not tr.Active then tr.StartTransaction;
      with quTmp do
        begin
          SQL.Text:='execute procedure FILLRESTART';
          ExecQuery;
        end;
      tr.CommitRetaining;
      if not tr.Active then tr.StartTransaction;
      dg1.DataSource.DataSet.Active:=True;
    end;
  Screen.Cursor:=crDefault;
end;

procedure TfmFRest.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_RETURN :
       try
         Screen.Cursor := crSQLWait;
         dmCom.FilterArt := edArt.Text;
         ReOpenDataSets([dg1.DataSource.DataSet]);
       finally
         Screen.Cursor := crDefault;
       end;
     VK_DOWN : ActiveControl:=dg1;
    end;
end;

procedure TfmFRest.acUpdateExecute(Sender: TObject);
begin
  with dm2 do
    begin
      if cbSZ.Checked then dsFRest.DataSet:=quFSZRest
      else  dsFRest.DataSet:=quFRest;
      ReopenDataSets([dsFRest.DataSet]);
    end;
  dg1.FieldColumns['SZ'].Visible:=cbSZ.Checked;
end;

procedure TfmFRest.acSzExecute(Sender: TObject);
begin
 with dm2 do
   begin
     if cbSZ.Checked then dsFRest.DataSet:=quFSZRest
     else  dsFRest.DataSet:=quFRest;
     ReopenDataSets([dsFRest.DataSet]);
   end;
   dg1.FieldColumns['SZ'].Visible:=cbSZ.Checked;
end;

procedure TfmFRest.acSortDepExecute(Sender: TObject);
begin
//
end;

procedure TfmFRest.acPrintExecute(Sender: TObject);
begin
 pdbg.Print;
end;

procedure TfmFRest.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if (NOT (gdSelected in State)) and (Column.Field<>NIL) then
    if Column.Field.Tag>0 then Background:=dm2.GetDepColor(Column.Field.Tag);
end;

end.
