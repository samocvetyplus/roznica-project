unit OptBuyerCase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, Buttons;

type
  TfmOptBuyerCase = class(TForm)
    plAll: TPanel;
    plButton: TPanel;
    btCancel: TBitBtn;
    btOk: TBitBtn;
    LName: TLabel;
    LOptBuyer: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure LOptBuyerDblClick(Sender: TObject);
    procedure LOptBuyerKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOptBuyerCase: TfmOptBuyerCase;

implementation
uses data, DbUtil, comdata, FIBQuery;
{$R *.dfm}

procedure TfmOptBuyerCase.FormCreate(Sender: TObject);
begin
 with dm do
 begin
  quTmp.Close;
  quTmp.SQL.Text:='select name, d_compid From d_comp where d_compid<>-1000 and buyer=1';
  quTmp.ExecQuery;
  if quTmp.Fields[1].IsNull then dmcom.OptBuyerId:=-1
  else dmcom.OptBuyerId:=quTmp.Fields[1].AsInteger;

  while not quTmp.Eof do
  begin
   LOptBuyer.Items.Add(quTmp.Fields[0].AsString);
   qutmp.Next;
  end;
  quTmp.Close;
  quTmp.Transaction.CommitRetaining;
 end
end;

procedure TfmOptBuyerCase.LOptBuyerDblClick(Sender: TObject);
begin
 ModalResult:=mrOk;
end;

procedure TfmOptBuyerCase.LOptBuyerKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: ModalResult:=mrok;
 end;
end;

procedure TfmOptBuyerCase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 if ModalResult=mrOk then
 begin
  if LOptBuyer.Count=0 then dmcom.OptBuyerId:=-1
  else
   with dm do
   begin
    quTmp.Close;
    qutmp.SQL.Text:='select first 1 d_compid from d_comp where name='#39+LOptBuyer.Items[LOptBuyer.ItemIndex]+#39;
    quTmp.ExecQuery;
    if quTmp.Fields[0].IsNull then dmcom.OptBuyerId:=-2
    else dmcom.OptBuyerId:=qutmp.Fields[0].AsInteger;
    quTmp.Close;
    quTmp.Transaction.CommitRetaining;
   end;
 end
end;

end.
