object fmRInv: TfmRInv
  Left = 148
  Top = 159
  Width = 770
  Height = 375
  Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1074#1074#1086#1076#1072' '#1086#1089#1090#1072#1090#1082#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 327
    Width = 762
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 762
    Height = 39
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 0
      Spacing = 1
      Left = 691
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FF00FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00000000000000FFFFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B
        FFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFF7B7B7B000000FF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B000000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1072#1088#1090#1080#1082#1091#1083#1072' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1087#1086' '#1080#1079#1076#1077#1083#1080#1103#1084
      ImageIndex = 4
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 80
    Width = 762
    Height = 247
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsSEl
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = acViewExecute
    IniStorage = FormStorage1
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'FULLART'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 140
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 90
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'QUANTITY'
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'TOTALWEIGHT'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NdsName'
        Title.Alignment = taCenter
        Title.Caption = #1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Cost'
        Title.Alignment = taCenter
        Title.Caption = #1057#1091#1084#1084#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UNITID'
        Title.Alignment = taCenter
        Title.Caption = #1045#1048
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 39
    Width = 762
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 73
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 58
      Height = 13
      Caption = #8470' '#1087#1088#1080#1093#1086#1076#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 160
      Top = 4
      Width = 61
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 160
      Top = 24
      Width = 34
      Height = 13
      Caption = #1057#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 380
      Top = 4
      Width = 59
      Height = 13
      Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 380
      Top = 24
      Width = 74
      Height = 13
      Caption = #1054#1073#1097#1077#1077' '#1082#1086#1083'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 532
      Top = 4
      Width = 58
      Height = 13
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 84
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SDATE'
      DataSource = dm2.dsRInv
    end
    object DBText2: TDBText
      Left = 84
      Top = 24
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SSF'
      DataSource = dm2.dsRInv
    end
    object DBText3: TDBText
      Left = 224
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SUP'
      DataSource = dm2.dsRInv
    end
    object DBText4: TDBText
      Left = 224
      Top = 24
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm2.dsRInv
    end
    object DBText5: TDBText
      Left = 456
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TW'
      DataSource = dm2.dsRInv
    end
    object DBText6: TDBText
      Left = 456
      Top = 24
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TQ'
      DataSource = dm2.dsRInv
    end
    object DBText7: TDBText
      Left = 592
      Top = 4
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'COST'
      DataSource = dm2.dsRInv
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 340
    Top = 172
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 200
    Top = 144
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1072#1088#1090#1080#1082#1091#1083#1072' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1086#1089#1090#1072#1090#1082#1086#1074' '#1087#1086' '#1080#1079#1076#1077#1083#1080#1103#1084
      ImageIndex = 4
      OnExecute = acViewExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
  end
end
