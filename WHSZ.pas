unit WHSZ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid,  db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmWHSZ = class(TForm)
    StatusBar1: TStatusBar;
    fmstrWHSz: TFormStorage;
    dg1: TM207IBGrid;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHSZ: TfmWHSZ;

implementation

uses comdata, Data, ServData, Data2, M207Proc;

{$R *.DFM}

procedure TfmWHSZ.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWHSZ.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHSZ.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if  dm.WHSZMode in [1,3] then
    begin
      Caption:='����� - �������';
      dg1.DataSource:=dm.dsWHSZ;
    end
  else
    begin
      Caption:='�������� - �������';
      dg1.DataSource:=dm.dsWHA2SZ;
    end;
  if dm.WorkMode = 'WH' then
  begin
    dg1.DataSource := dmServ.dsrWHSZ;
    KeyPreview := True;
    OnKeyPress := dm.CloseFormByEsc;
  end;
  dg1.DataSource.DataSet.Active:=True;
end;

procedure TfmWHSZ.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHSZ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dg1.DataSource.DataSet.Active:=False;
end;

procedure TfmWHSZ.dg1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if dm.WorkMode='WH' then
  begin
    if Highlight or dg1.ClearHighlight then Background := dm2.GetDepColor(dmServ.quWHSZDEPID.AsInteger)
    else Background:=clHighlight
  end;
  if (Field<>NIL) and (Field.FieldName='SZ') then Background:=dmCom.clMoneyGreen;
end;

end.
