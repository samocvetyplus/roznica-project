unit Col;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Mask,  
  M207Ctrls, M207DBCtrls, rxPlacemnt, rxToolEdit;

type
  TfmCol = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    M207DBColor1: TM207DBColor;
    M207DBColor2: TM207DBColor;
    Label3: TLabel;
    M207DBColor3: TM207DBColor;
    M207DBColor4: TM207DBColor;
    M207DBColor5: TM207DBColor;
    M207DBColor8: TM207DBColor;
    M207DBColor9: TM207DBColor;
    M207DBColor10: TM207DBColor;
    M207DBColor11: TM207DBColor;
    M207DBColor12: TM207DBColor;
    M207DBColor13: TM207DBColor;
    M207DBColor14: TM207DBColor;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    M207DBColor15: TM207DBColor;
    M207DBColor16: TM207DBColor;
    Label6: TLabel;
    Label7: TLabel;
    M207DBColor6: TM207DBColor;
    M207DBColor7: TM207DBColor;
    Label17: TLabel;
    Label18: TLabel;
    M207DBColor17: TM207DBColor;
    M207DBColor18: TM207DBColor;
    fr1: TM207FormStorage;
    Label19: TLabel;
    M207DBColor19: TM207DBColor;
    Label20: TLabel;
    M207DBColor20: TM207DBColor;
    M207DBColor21: TM207DBColor;
    Label21: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCol: TfmCol;

implementation

uses Data2, comdata, Data, M207Proc;

{$R *.DFM}

procedure TfmCol.FormCreate(Sender: TObject);
begin
  with dmCom, dm, dm2 do
    begin
      if not tr.Active then tr.StartTransaction;
      OpenDataSets([quCol]);
    end;
 PageControl1.Enabled:= dmcom.Adm;
end;

procedure TfmCol.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      if ModalResult=mrOK then
        begin
          PostDataSets([quCol]);
          CU_SBD0:=quColCU_SBD0.AsInteger;
          CU_SBD1:=quColCU_SBD1.AsInteger;
          CU_R:=quColCU_R.AsInteger;
          CU_S0:=quColCU_S0.AsInteger;
          CU_S1:=quColCU_S1.AsInteger;
          CU_D0:=quColCU_D0.AsInteger;
          CU_D1:=quColCU_D1.AsInteger;
          CU_RT0:=quColCU_RT0.AsInteger;
          CU_RT1:=quColCU_RT1.AsInteger;
          CU_SL:=quColCU_SL.AsInteger;
          CU_SO0:=quColCU_SO0.AsInteger;
          CU_SO1:=quColCU_SO1.AsInteger;
          CU_SR0:=quColCU_SR0.AsInteger;
          CU_SR1:=quColCU_SR1.AsInteger;
          CU_RO0:=quColCU_RO0.AsInteger;
          CU_RO1:=quColCU_RO1.AsInteger;
          CU_IM0:=quColCU_IM0.AsInteger;
          CU_IM1:=quColCU_IM1.AsInteger;
          CU_AO:=quColCU_AO.AsInteger;
          CU_AC:=quColCU_AC.AsInteger;
        end
      else CancelDataSets([quCol]);
      CloseDataSets([quCol]);
      tr.CommitRetaining;
    end;
end;

end.
