unit AplList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, Menus, ExtCtrls, StdCtrls,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, db, DBGridEh, 
  DBCtrlsEh, ActnList, ComCtrls, PrnDbgeh, TB2Item, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;
type
  TfmApplList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siEdit: TSpeedItem;
    siCloseInv: TSpeedItem;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siPeriod: TSpeedItem;
    fs1: TFormStorage;
    cbSup: TComboBox;
    gridAppl: TDBGridEh;
    StatusBar1: TStatusBar;
    acEvent: TActionList;
    acClose: TAction;
    acAdd: TAction;
    acDel: TAction;
    acView: TAction;
    acPrintAppl: TAction;
    acExportToText: TAction;
    siExport: TSpeedItem;
    prdg1: TPrintDBGridEh;
    acPrint: TAction;
    siMerge: TSpeedItem;
    acMerge: TAction;
    pmdg1: TTBPopupMenu;
    acSend: TAction;
    acNotSend: TAction;
    biSend: TTBItem;
    biNotSend: TTBItem;
    acCloseAppl: TAction;
    siHelp: TSpeedItem;
    SpeedItem1: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure cbSupChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
    procedure acExportToTextExecute(Sender: TObject);
    procedure acPrintApplUpdate(Sender: TObject);
    procedure acExportToTextUpdate(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acMergeExecute(Sender: TObject);
    procedure acMergeUpdate(Sender: TObject);
    procedure acSendUpdate(Sender: TObject);
    procedure acNotSendUpdate(Sender: TObject);
    procedure gridApplGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acSendExecute(Sender: TObject);
    procedure acNotSendExecute(Sender: TObject);
    procedure acCloseApplUpdate(Sender: TObject);
    procedure acCloseApplExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure gridApplKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gridApplMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure gridApplMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedItem1Click(Sender: TObject);
  private
    LogOprIdForm : string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
    

  end;

var
  fmApplList: TfmApplList;
 // AView: TDBGridEh;
 // a_select : TDBGridEhSelection;
 // AView: TcxGridDBTableView;
implementation

uses Data, ServData, comdata, M207Proc, Period,DBTree, Appl, ReportData,
  Data3, JewConst, dbUtil, MsgDialog;

{$R *.dfm}

procedure TfmApplList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmApplList.FormCreate(Sender: TObject);
begin
  gridAppl.SelectedRows.Clear; // ��� ���������� �����

  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  dmServ.ApplSupid:=-1;
  with dmCom, dm do
    begin
      tr.Active:=True;
      SetBeginDate;
      ShowPeriod;
      WorkMode:='APPL';
      OpenDatasets([dmServ.taApplList]);
      cbSup.Items.Assign(dm.slProd);
    end;

  if not CenterDep then
  begin
   if not dmServ.taApplListAPPLID.IsNull then
    if dmServ.taApplListISCLOSED.AsInteger=1 then
    begin
     acCloseAppl.ImageIndex:=6;
     acCloseAppl.Caption:='�������';
     acCloseAppl.Hint:='������� ������';
    end
    else
    begin
     acCloseAppl.ImageIndex:=5;
     acCloseAppl.Caption:='�������';
     acCloseAppl.Hint:='������� ������';
    end
  end
  else acCloseAppl.Visible:=false;

  if CenterDep then gridAppl.FieldColumns['SDATE'].Title.Caption:='������|���� ��������'
  else gridAppl.FieldColumns['SDATE'].Title.Caption:='������|���� ��������';

  LogOprIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmApplList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dmServ.taApplList]);
      ShowPeriod;
    end;
end;

procedure TfmApplList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmApplList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmApplList.cbSupChange(Sender: TObject);
begin
 dmServ.ApplSupid:=TNodeData(cbSup.Items.Objects[cbSup.ItemIndex]).Code;
 ReOpenDataSets([dmServ.taApplList]); 
end;

procedure TfmApplList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dmServ do
    begin
      PostDataSet(taApplList);
      CloseDataSets([taApplList]);
      tr.CommitRetaining;
      dm.WorkMode:='';
    end;
end;

procedure TfmApplList.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmApplList.acAddExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplAdd, LogOprIdForm);
  dmServ.taApplList.Insert;
  dmServ.taApplList.Post;
  acView.Execute;
  dm3.update_operation(LogOperationID);
end;

procedure TfmApplList.acDelExecute(Sender: TObject);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
     gridAppl.SelectedRows.Delete
  else
     SysUtils.Abort;
end;

procedure TfmApplList.acViewExecute(Sender: TObject);
var
  LogOperationID: string;
  BookMark : TBookMark;
begin
  BookMark:=dmServ.taApplList.GetBookmark;
  LogOperationID := dm3.insert_operation(sLog_ApplView, LogOprIdForm);
  with dmServ do
  begin
    PostDataSets([taApplList]);
    NoAppl:=taApplListApplId.AsInteger;
    ApplSupid:=taApplListSupId.AsInteger;
    try
     ShowAndFreeForm(TfmAppl, Self, TForm(fmAppl), True, False);
     gridAppl.SumList.RecalcAll;
     taApplList.GotoBookmark(BookMark);
    finally
     taApplList.FreeBookmark(BookMark);
    end;
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmApplList.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dmServ.taApplList.Active;
end;

procedure TfmApplList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dmServ.taApplList.Active and (not dmServ.taApplList.IsEmpty) and
   ((CenterDep and (dmServ.taApplListISSEND.AsInteger=0)) or
    ((not CenterDep) and (dmServ.taApplListISCLOSED.AsInteger=0)));
end;

procedure TfmApplList.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dmServ.taApplList.Active and (not dmServ.taApplList.IsEmpty);
end;

procedure TfmApplList.acPrintApplExecute(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID: string;
  count_ApplArt2: variant;
begin
  SetLength(arr, 1);
  arr[0] := dmServ.taApplListAPPLID.asInteger;
  LogOperationID := dm3.insert_operation(sLog_ApplPrint, LogOprIdForm);
  try
    count_ApplArt2 := ExecSelectSQL('select count(*) from aitem where art2 is not null and applid='+ dmserv.taApplListAPPLID.AsString, dmCom.quTmp);
    if (count_ApplArt2=0) then
       PrintDocument(arr, appl_sup1) else
       PrintDocument(arr, appl_sup);
  finally
    finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmApplList.acExportToTextExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplExportTxt, LogOprIdForm);
  dmReport.AppleText(dmServ.taApplListAPPLID.AsInteger, dmServ.taApplListSNAME.AsString);
  dm3.update_operation(LogOperationID);
end;

procedure TfmApplList.acPrintApplUpdate(Sender: TObject);
begin
  acPrintAppl.Enabled := dmServ.taApplList.Active and (not dmServ.taApplList.IsEmpty);
end;

procedure TfmApplList.acExportToTextUpdate(Sender: TObject);
begin
  acExportToText.Enabled := dmServ.taApplList.Active and (not dmServ.taApplList.IsEmpty);
end;

procedure TfmApplList.acPrintExecute(Sender: TObject);
begin
 prdg1.Print;
end;

procedure TfmApplList.acMergeExecute(Sender: TObject);
var applid2, applid:integer;
    ffirst:boolean;
    serr, sup:string;
    countappl:integer;
begin
 PostDataSet(dmserv.taApplList);
 dmserv.taApplList.First;
 ffirst:=false;
 serr:='';
 applid:=-1;
 applid2:=-1;
 countappl:=0;
 while (not dmserv.taApplList.Eof) and (not ffirst) do
 begin
  if dmserv.taApplListF1.AsInteger=1 then
  begin

   applid:=dmserv.taApplListAPPLID.AsInteger;
   sup:=trim(dmserv.taApplListSNAME.AsString);
   ffirst:=true;
   inc(countappl);
  end else dmserv.taApplList.Next;
 end;

 if ffirst then begin
  dmserv.taApplList.First;
  while not dmserv.taApplList.Eof do
  begin
   if (dmserv.taApplListF2.AsInteger=1) and (applid<>dmserv.taApplListAPPLID.AsInteger) then
    applid2:=dmserv.taApplListAPPLID.AsInteger;

   if (applid<>-1) and (applid2<>-1) then
   begin
    ExecSQL('execute procedure Create_MergeAppl ('+inttostr(applid)+', '+
             inttostr(applid2)+')', dm.quTmp);
    inc(countappl);
    applid2:=-1;
   end;

   dmserv.taApplList.Next;
  end;
 end;


 if countappl=1 then serr:=serr+#13#10+'������ � ����������� '+sup+' �� ����� ���� ���������� (������� ������ ���� ������);'
 else if countappl=0 then serr:=serr+#13#10+'��� ������ ��� �����������;'
 else MessageDialog('����������� ��������� ������� ��� ���������� '+sup, mtInformation, [mbOk], 0);

 if serr<>'' then MessageDialog('������:'+serr, mtError, [mbOk], 0);

 dmserv.taApplList.First;
 ReOpenDataSet(dmserv.taApplList);
end;

procedure TfmApplList.acMergeUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= not dmServ.taApplListAPPLID.IsNull;
end;

procedure TfmApplList.acSendUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (dmServ.taApplListISSEND.AsInteger=0) and CenterDep;
end;

procedure TfmApplList.acNotSendUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (dmServ.taApplListISSEND.AsInteger=1) and CenterDep;
end;

procedure TfmApplList.gridApplGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  if (Column.FieldName='DEP') and (dmServ.taApplListCOLOR.AsInteger<>0) then
   Background:=dmServ.taApplListCOLOR.AsInteger
  else
  if CenterDep then
  begin
   if dmServ.taApplListISSEND.AsInteger=0 then Background:=clInfoBk
   else Background:=clBtnFace
  end
  else
  begin
   if dmServ.taApplListISCLOSED.AsInteger=0 then Background:=clInfoBk
   else Background:=clBtnFace
  end
 end
end;

procedure TfmApplList.acSendExecute(Sender: TObject);
begin
 dmServ.taApplList.Edit;
 dmServ.taApplListISSEND.AsInteger:=1;
 dmServ.taApplListSDATE.AsDateTime:=dmcom.GetServerTime; 
 dmServ.taApplList.Post;
 dmServ.taApplList.Refresh;
end;

procedure TfmApplList.acNotSendExecute(Sender: TObject);
begin
 dmServ.taApplList.Edit;
 dmServ.taApplListISSEND.AsInteger:=0;
 dmServ.taApplList.Post;
 dmServ.taApplList.Refresh;
end;

procedure TfmApplList.acCloseApplUpdate(Sender: TObject);
begin
 if not CenterDep then
 begin
  TAction(Sender).Enabled:= not dmServ.taApplListAPPLID.IsNull; 

  if not dmServ.taApplListAPPLID.IsNull then
   if dmServ.taApplListISCLOSED.AsInteger=1 then
   begin
    acCloseAppl.ImageIndex:=6;
    acCloseAppl.Caption:='�������';
    acCloseAppl.Hint:='������� ������';
   end
   else
   begin
    acCloseAppl.ImageIndex:=5;
    acCloseAppl.Caption:='�������';
    acCloseAppl.Hint:='������� ������';
   end
 end
end;

procedure TfmApplList.acCloseApplExecute(Sender: TObject);
begin
 if dmServ.taApplListISCLOSED.AsInteger=1 then
 begin
  if MessageDialog('������� ������?', mtInformation, [mbYes,mbNo], 0) = mrYes then
  begin
   dmServ.taApplList.Edit;
   dmServ.taApplListISCLOSED.AsInteger:=0;
   dmServ.taApplList.Post;
   dmServ.taApplList.Refresh;
  end;
 end
 else
 begin
  if MessageDialog('������� ������?', mtInformation, [mbYes,mbNo], 0) = mrYes then
  begin
   dmServ.taApplList.Edit;
   dmServ.taApplListISCLOSED.AsInteger:=1;
   dmServ.taApplListISUIDWH.AsInteger:=0;
   dmServ.taApplListISSELL.AsInteger:=0;
   dmServ.taApplListSDATE.AsDateTime:=dmcom.GetServerTime;   
   dmServ.taApplList.Post;
   dmServ.taApplList.Refresh;
  end;
 end
end;

procedure TfmApplList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100281)
end;

procedure TfmApplList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmApplList.gridApplKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: if (not dmserv.taApplListAPPLID.IsNull) then
   acViewExecute(nil);
 end;
end;

procedure TfmApplList.gridApplMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  gridAppl.SelectedRows.CurrentRowSelected:=True;
end;

procedure TfmApplList.gridApplMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
   gridAppl.SelectedRows.CurrentRowSelected:=True;
end;

procedure TfmApplList.SpeedItem1Click(Sender: TObject);
begin
with dmServ, dmCom do
  begin
  PostDataSet(taApplList);
  tr.CommitRetaining;
  ReOpenDataSet(taApplList);
  taApplList.Refresh;
  end;
end;

end.
