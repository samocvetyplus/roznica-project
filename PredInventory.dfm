object fmPredInverty: TfmPredInverty
  Left = 308
  Top = 195
  Width = 279
  Height = 205
  BorderIcons = [biSystemMenu]
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbInverDate: TLabel
    Left = 0
    Top = 4
    Width = 138
    Height = 16
    Caption = #1044#1072#1090#1072' '#1080#1085#1074#1077#1090#1072#1088#1080#1079#1072#1094#1080#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lbNPrord: TLabel
    Left = 0
    Top = 32
    Width = 101
    Height = 16
    Caption = #1053#1086#1084#1077#1088' '#1087#1088#1080#1082#1072#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lbProrddate: TLabel
    Left = 0
    Top = 64
    Width = 90
    Height = 16
    Caption = #1044#1072#1090#1072' '#1087#1088#1080#1082#1072#1079#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 0
    Top = 120
    Width = 140
    Height = 16
    Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 2
    Top = 92
    Width = 122
    Height = 16
    Caption = #1044#1072#1090#1072' '#1091#1090#1074#1077#1088#1078#1076#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btok: TBitBtn
    Left = 44
    Top = 144
    Width = 89
    Height = 25
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 5
    Kind = bkOK
  end
  object btCancel: TBitBtn
    Left = 144
    Top = 144
    Width = 89
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1080#1090#1100
    TabOrder = 6
    Kind = bkCancel
  end
  object edInverDate: TDBDateTimeEditEh
    Left = 152
    Top = 88
    Width = 113
    Height = 19
    DataField = 'NDATE'
    DataSource = dmServ.dsInvSinv
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 3
    Visible = True
  end
  object edInvPrordN: TDBEditEh
    Left = 152
    Top = 29
    Width = 109
    Height = 19
    DataField = 'Q'
    DataSource = dmServ.dsInvSinv
    EditButtons = <>
    Flat = True
    TabOrder = 1
    Visible = True
  end
  object edInvPrordDate: TDBDateTimeEditEh
    Left = 150
    Top = 60
    Width = 115
    Height = 19
    DataField = 'CRDATE'
    DataSource = dmServ.dsInvSinv
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 2
    Visible = True
  end
  object lcbxReponsibleId: TDBLookupComboboxEh
    Left = 152
    Top = 120
    Width = 113
    Height = 19
    DataField = 'CRUSERID'
    DataSource = dmServ.dsInvSinv
    DropDownBox.Rows = 15
    DropDownBox.Width = -1
    EditButtons = <>
    Flat = True
    KeyField = 'D_MOLID'
    ListField = 'FIO'
    ListSource = dmCom.dsMol
    TabOrder = 4
    Visible = True
  end
  object edInvDate: TDBDateTimeEditEh
    Left = 152
    Top = 4
    Width = 109
    Height = 19
    DataField = 'SDATE'
    DataSource = dmServ.dsInvSinv
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 0
    Visible = True
  end
end
