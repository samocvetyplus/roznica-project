unit frmCampaign;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, DBClient, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid, FIBDatabase,
  pFIBDatabase, Provider, dxLayoutControl, Menus, dxLayoutLookAndFeels,
  StdCtrls, cxButtons, DateUtils, dxBar, ActnList, cxTextEdit, cxBarEditItem,
  cxCurrencyEdit, cxDropDownEdit, StrUtils, pFIBScripter, cxCalendar, ImgList,
  cxDBLookupComboBox, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type

  TDialogCampaign = class(TForm)
    Campaign: TpFIBDataSet;
    Transaction: TpFIBTransaction;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    GridView: TcxGridDBBandedTableView;
    CampaignID: TFIBIntegerField;
    CampaignNAME: TFIBStringField;
    CampaignDATEBEGIN: TFIBDateField;
    CampaignDATEEND: TFIBDateField;
    CampaignDISCOUNTCLASS: TFIBIntegerField;
    CampaignLIMIT: TFIBIntegerField;
    GridViewNAME: TcxGridDBBandedColumn;
    GridViewDATEBEGIN: TcxGridDBBandedColumn;
    GridViewDATEEND: TcxGridDBBandedColumn;
    GridViewDISCOUNT: TcxGridDBBandedColumn;
    GridViewDISCOUNTNAME: TcxGridDBBandedColumn;
    LayoutGroup_Root: TdxLayoutGroup;
    Layout: TdxLayoutControl;
    LayoutItemGrid: TdxLayoutItem;
    LayoutLookAndFeel: TdxLayoutLookAndFeelList;
    LayoutOfficeLookAndFeel: TdxLayoutOfficeLookAndFeel;
    BarManager: TdxBarManager;
    BarManagerBar1: TdxBar;
    DockControl: TdxBarDockControl;
    LayoutItem3: TdxLayoutItem;
    Actions: TActionList;
    ActionExclude: TAction;
    ActionInclude: TAction;
    GridViewLIMIT: TcxGridDBBandedColumn;
    GridViewROUTE: TcxGridDBBandedColumn;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    Styles: TcxStyleRepository;
    SourceCampaign: TDataSource;
    Images32: TcxImageList;
    CampaignRANGEBEGIN: TFIBIntegerField;
    CampaignRANGEEND: TFIBIntegerField;
    GridViewRANGEBEGIN: TcxGridDBBandedColumn;
    GridViewRANGEEND: TcxGridDBBandedColumn;
    CampaignDISCOUNT: TFIBBCDField;
    dxBarLargeButton3: TdxBarLargeButton;
    CampaignSTATE: TIntegerField;
    GridViewSTATE: TcxGridDBBandedColumn;
    GridViewID: TcxGridDBBandedColumn;
    Style0: TcxStyle;
    Style1: TcxStyle;
    Style2: TcxStyle;
    Style3: TcxStyle;
    procedure ActionExcludeUpdate(Sender: TObject);
    procedure ActionExcludeExecute(Sender: TObject);
    procedure ActionIncludeExecute(Sender: TObject);
    procedure ActionIncludeUpdate(Sender: TObject);
    procedure CampaignBeforePost(DataSet: TDataSet);
    procedure CampaignAfterPost(DataSet: TDataSet);
    procedure CampaignAfterDelete(DataSet: TDataSet);
    procedure CampaignDISCOUNTCLASSGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure CampaignDISCOUNTCLASSSetText(Sender: TField; const Text: string);
    procedure CampaignAfterCancel(DataSet: TDataSet);
    procedure dxBarLargeButton3Click(Sender: TObject);
    procedure CampaignCalcFields(DataSet: TDataSet);
    procedure GridViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      out AStyle: TcxStyle);
  private
    function InternalExecute: Boolean;
  public
    class function Execute: Boolean;
  end;

implementation

{$R *.dfm}

uses

  ComData, Data, uDialog;

class function TDialogCampaign.Execute: Boolean;
var
  Dialog: TDialogCampaign;
begin
  Dialog := TDialogCampaign.Create(Application);

  Dialog.InternalExecute;

  Dialog.Free;
end;

procedure TDialogCampaign.GridViewStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
  Value: Variant;
  State: Integer;
begin
  AStyle := Style0;

  Index := GridViewID.Index;

  Value := ARecord.Values[Index];

  if not VarIsNull(Value) then
  begin
    Index := GridViewSTATE.Index;

    State := ARecord.Values[Index];

    case State of

      1: AStyle := Style1;

      2: AStyle := Style2;

      3: AStyle := Style3;

    end;
  end;
end;

function TDialogCampaign.InternalExecute: Boolean;
begin
  Transaction.StartTransaction;

  Campaign.Active := True;

  ShowModal;

  Campaign.Active := False;

  Transaction.Commit;
end;


procedure TDialogCampaign.ActionIncludeUpdate(Sender: TObject);
begin
  ActionInclude.Enabled := CenterDep;
end;

procedure TDialogCampaign.ActionExcludeUpdate(Sender: TObject);
begin
  ActionExclude.Enabled := CenterDep;
end;

procedure TDialogCampaign.ActionIncludeExecute(Sender: TObject);
begin
  GridView.NewItemRow.Visible := True;
  
  Campaign.Append;
end;

procedure TDialogCampaign.ActionExcludeExecute(Sender: TObject);
begin
  if TDialog.Confirmation('�� �������, ��� �� ������ ������� �����?') = mrYes then
  begin
    Campaign.Delete;
  end;
end;

procedure TDialogCampaign.CampaignAfterPost(DataSet: TDataSet);
begin
  Transaction.CommitRetaining;

  GridView.NewItemRow.Visible := False;
end;

procedure TDialogCampaign.CampaignAfterCancel(DataSet: TDataSet);
begin
  GridView.NewItemRow.Visible := False;
end;

procedure TDialogCampaign.CampaignAfterDelete(DataSet: TDataSet);
begin
  Transaction.CommitRetaining;
end;

procedure TDialogCampaign.CampaignBeforePost(DataSet: TDataSet);
var
  Flag: Boolean;
  SQL: string;
  Value: Variant;
  ID: Integer;
  Range: Integer;
begin
  Flag := True;

  if Flag  and (Trim(CampaignNAME.AsString) = '') then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "������������".');
  end;

  if Flag and CampaignDATEBEGIN.IsNull then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "������. ������".');
  end;

  if Flag and CampaignDATEEND.IsNull then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "������. �����".');
  end;

  if Flag and (CampaignDISCOUNT.AsCurrency = 0) then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "������. ��������".');
  end;

  if Flag and (CampaignDISCOUNTCLASS.AsInteger = 0) then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "������. ���".');
  end;

  if Flag and (CampaignLIMIT.AsInteger = 0) then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "�����".');
  end;

  if Flag and (CampaignRANGEBEGIN.AsInteger = 0) then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "��������. ������".');
  end;

  if Flag and (CampaignRANGEEND.AsInteger = 0) then
  begin
    Flag := False;

    TDialog.Error('�� ��������� "��������. �����".');
  end;

  if Flag then
  begin
    ID := CampaignID.AsInteger;

    Range := CampaignRANGEBEGIN.AsInteger;

    SQL := 'select first 1 0 from campaign c where c.id <> :id and c.range$begin <= :range and c.range$end >= :range';

    Value :=  Campaign.Database.QueryValue(SQL, 0,[ID, Range]);

    if not VarIsNull(Value) then
    begin
      Flag := False;

      TDialog.Error('��������� ����������.');
    end;
  end;

  if Flag then
  begin
    ID := CampaignID.AsInteger;

    Range := CampaignRANGEEND.AsInteger;

    SQL := 'select first 1 0 from campaign c where c.id <> :id and c.range$begin <= :range and c.range$end >= :range';

    Value :=  Campaign.Database.QueryValue(SQL, 0,[ID, Range]);

    if not VarIsNull(Value) then
    begin
      Flag := False;

      TDialog.Error('��������� ����������.');
    end;
  end;

  if Flag then
  begin

  end;

  if Flag then
  begin
    if CampaignID.IsNull then
    begin
      CampaignID.AsInteger := Campaign.Database.Gen_Id('G$CAMPAIGN', 1);
    end;
  end else
  begin
    Abort;
  end;
end;

procedure TDialogCampaign.CampaignCalcFields(DataSet: TDataSet);
var
  State: Integer;
  DateBegin: TDateTime;
  DateEnd: TDateTime;
  CurrentDate: TDateTime;
begin
  State := 0;

  CurrentDate := DateOf(Date);

  DateBegin := DateOf(CampaignDATEBEGIN.AsDateTime);

  DateEnd := DateOf(CampaignDATEEND.AsDateTime);

  if DateEnd < CurrentDate then
  begin
    State := 1;
  end else
  if DateBegin > CurrentDate then
  begin
    State := 2;
  end else
  begin
    State := 3;
  end;

  CampaignSTATE.AsInteger := State;
end;

procedure TDialogCampaign.CampaignDISCOUNTCLASSGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  if Sender.AsInteger = 1 then
  begin
    Text := '%';
  end else
  if Sender.AsInteger = 2 then
  begin
    Text := '�';
  end;
end;

procedure TDialogCampaign.CampaignDISCOUNTCLASSSetText(Sender: TField; const Text: string);
begin
  if Text = '%' then
  begin
    Sender.AsInteger := 1;
  end else
  if Text = '�' then
  begin
    Sender.AsInteger := 2;
  end;
end;

procedure TDialogCampaign.dxBarLargeButton3Click(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
