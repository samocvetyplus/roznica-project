object fmPrOrd: TfmPrOrd
  Left = 223
  Top = 251
  Caption = #1055#1088#1080#1082#1072#1079' '#1085#1072' '#1094#1077#1085#1099
  ClientHeight = 528
  ClientWidth = 1155
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 264
    Width = 1155
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 269
    ExplicitWidth = 776
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 507
    Width = 1155
    Height = 21
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1155
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 66
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00000000FF0000FF00000000FFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        000000000000FF0000FF000000000000000000007B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFF00000000FF0000FF0000FF0000FF0000FF0000FF
        0000FF007B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        FF0000FF0000FF0000FF0000FF0000FF0000FF007B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00000000000000000000FF0000FF000000000000
        000000007B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF00000000FF0000FF00000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000000000FF
        FFFFFFFF7B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF000000000000000000000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF7B7B7BFF00FFFF00FFFF00FF
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFF7B
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 2
      Spacing = 1
      Left = 69
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 707
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siClose: TSpeedItem
      Action = acClosed
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 5
      Spacing = 1
      Left = 179
      Top = 3
      Visible = True
      OnClick = acClosedExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 42
    Width = 1155
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 14
      Height = 13
      Caption = #8470' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 340
      Top = 8
      Width = 53
      Height = 13
      Caption = #1047#1072#1082#1088#1099#1090#1080#1077':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 68
      Top = 8
      Width = 34
      Height = 13
      Caption = #1057#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laDep: TLabel
      Left = 104
      Top = 8
      Width = 28
      Height = 13
      Caption = 'laDep'
    end
    object Label4: TLabel
      Left = 8
      Top = 28
      Width = 71
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 84
      Top = 28
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SNstr'
      DataSource = dm.dsPrOrd
    end
    object DBText3: TDBText
      Left = 396
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SETDATE'
      DataSource = dm.dsPrOrd
    end
    object Label6: TLabel
      Left = 340
      Top = 28
      Width = 42
      Height = 13
      Caption = #1047#1072#1082#1088#1099#1083':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText5: TDBText
      Left = 396
      Top = 28
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SETFIO'
      DataSource = dm.dsPrOrd
    end
    object DBEdit1: TDBEdit
      Left = 22
      Top = 4
      Width = 39
      Height = 21
      Color = clInfoBk
      DataField = 'PRORD'
      DataSource = dm.dsPrOrd
      TabOrder = 0
    end
  end
  object pa2: TPanel
    Left = 0
    Top = 267
    Width = 1155
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 326
      Top = 0
      Height = 240
      AutoSnap = False
      MinSize = 25
    end
    object Panel3: TPanel
      Left = 329
      Top = 0
      Width = 826
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dgWH: TM207IBGrid
        Left = 0
        Top = 27
        Width = 826
        Height = 213
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsDPrice
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm2
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleButtons = True
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = False
        SortOnTitleClick = True
        Columns = <
          item
            Expanded = False
            FieldName = 'PRODCODE'
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_MATID'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 33
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_GOODID'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_INSID'
            Title.Alignment = taCenter
            Title.Caption = #1054#1042
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_COUNTRYID'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 73
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2 '
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 82
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'SPRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clYellow
            Expanded = False
            FieldName = 'OPTPRICE'
            Title.Alignment = taCenter
            Title.Caption = #1054#1087#1090'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 826
        Height = 27
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label13: TLabel
          Left = 78
          Top = 6
          Width = 32
          Height = 13
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edArt: TEdit
          Left = 112
          Top = 2
          Width = 57
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          Text = 'edArt'
          OnChange = edArtChange
          OnKeyDown = edArtKeyDown
        end
        object seP1: TRxSpinEdit
          Left = 260
          Top = 2
          Width = 55
          Height = 21
          ValueType = vtFloat
          Color = clInfoBk
          TabOrder = 1
        end
        object cbP1: TCheckBox
          Left = 172
          Top = 4
          Width = 85
          Height = 17
          Caption = #1056#1072#1089'.'#1094#1077#1085#1072' >='
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object cbP2: TCheckBox
          Left = 318
          Top = 4
          Width = 83
          Height = 17
          Caption = #1056#1072#1089'.'#1094#1077#1085#1072' <='
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object seP2: TRxSpinEdit
          Left = 402
          Top = 2
          Width = 55
          Height = 21
          ValueType = vtFloat
          Color = clInfoBk
          TabOrder = 4
        end
        object cbFilter: TCheckBox
          Left = 460
          Top = 4
          Width = 65
          Height = 17
          Caption = #1060#1080#1083#1100#1090#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          OnClick = cbFilterClick
        end
        object Button1: TButton
          Left = 4
          Top = 4
          Width = 69
          Height = 21
          Caption = #1054#1090#1082#1088#1099#1090#1100
          TabOrder = 6
          OnClick = Button1Click
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 326
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 25
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 25
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 25
      end
      object Splitter6: TSplitter
        Left = 121
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 25
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 70
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dg1: TM207IBGrid
    Left = 0
    Top = 87
    Width = 1155
    Height = 177
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsPrOrdItem
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = pm1
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 2
    TitleButtons = True
    OnGetCellParams = dg1GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'FULLART'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 196
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 134
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CURPRICE'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1058#1077#1082#1091#1097#1072#1103' '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 80
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 76
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clYellow
        Expanded = False
        FieldName = 'OPTPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OLDP'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1057#1090'. '#1087#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OLDP2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1057#1090'. '#1088#1072#1089'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OLDOP'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1057#1090'. '#1086#1087#1090'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CURSP'
        Title.Alignment = taCenter
        Title.Caption = #1058#1077#1082#1091#1097#1072#1103' '#1087#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CUROP'
        Title.Caption = #1058#1077#1082#1091#1097#1072#1103' '#1086#1087'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 83
        Visible = True
      end>
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 106
    Top = 150
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
    end
    object N2: TMenuItem
      Action = acDel
    end
    object N5: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1089#1083#1077#1076#1085#1102#1102' '#1075#1088#1091#1087#1087#1091
      ShortCut = 119
      OnClick = N5Click
    end
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 392
    Top = 362
    object N3: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 32
    end
    object N4: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074#1089#1102' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 34
      ShortCut = 116
      OnClick = N4Click
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'laDep.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 384
    Top = 160
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 536
    Top = 176
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acAddUpdate
    end
    object acClosed: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 5
      OnExecute = acClosedExecute
      OnUpdate = acAddUpdate
    end
  end
end
