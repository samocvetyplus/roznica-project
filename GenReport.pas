unit GenReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls;

type
  TForm4 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    CheckBox1: TCheckBox;
    Button1: TButton;
    Button2: TButton;
    lcSup: TDBLookupComboBox;
    lcPayType: TDBLookupComboBox;
    ComboBox1: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation
    uses data, main, comdata;
{$R *.dfm}

procedure TForm4.Button2Click(Sender: TObject);
begin
    dm.quSup.Close;
    dm.quSup.Active:=false;
    dmCom.taPayType.Close;
    dmCom.taPayType.Active:=false;
    Close;
end;

procedure TForm4.FormActivate(Sender: TObject);
begin
dm.quSup.Open;
dm.quSup.Active:=true;
dm.quSup.FetchAll;
dmCom.taPayType.Open;
dmCom.taPayType.Active:=true;
dmCom.taPayType.FetchAll;
end;

end.

