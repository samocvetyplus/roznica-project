unit ShopReportList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrlsEh, DB, FIBDataSet,
  pFIBDataSet, Grids, DBGridEh, ActnList, ComCtrls, TB2Item, Menus,
  FR_DSet, FR_DBSet, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmShopReportList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siRep: TSpeedItem;
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    lbYear: TLabel;
    edYear: TDBNumberEditEh;
    taShopReportList: TpFIBDataSet;
    taShopReportListSINVID: TFIBIntegerField;
    taShopReportListSN: TFIBIntegerField;
    taShopReportListSDATE: TFIBDateTimeField;
    taShopReportListDEPNAME: TFIBStringField;
    taShopReportListMONTHNAME: TStringField;
    gridShopReportList: TDBGridEh;
    dsrShopReportList: TDataSource;
    taShopReportListDEPID: TFIBIntegerField;
    taShopReportListUSERID: TFIBIntegerField;
    taShopReportListUSERNAME: TFIBStringField;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    taShopReportListNDATE: TFIBDateTimeField;
    stbrMain: TStatusBar;
    taShopReportListITYPE: TFIBSmallIntField;
    taShopReportListDEPCOLOR: TFIBIntegerField;
    acExit: TAction;
    SpeedItem1: TSpeedItem;
    acView: TAction;
    SpeedItem2: TSpeedItem;
    taShopReportListCRDATE: TFIBDateTimeField;
    acRefresh: TAction;
    acFillEntryData: TAction;
    ppDoc: TTBPopupMenu;
    TBItem2: TTBItem;
    pmDep: TPopupMenu;
    acPrintShopReport: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    acCheckData: TAction;
    TBItem1: TTBItem;
    fms: TFormStorage;
    acPrintShopReportBadUID: TAction;
    taBadUID: TpFIBDataSet;
    frBadUID: TfrDBDataSet;
    TBItem3: TTBItem;
    taBadUIDUID: TFIBIntegerField;
    taBadUIDSNAME: TFIBStringField;
    taBadUIDSDATE: TFIBDateTimeField;
    taBadUIDWHBC2: TFIBFloatField;
    taBadUIDQ0: TFIBFloatField;
    taBadUIDWHEC2: TFIBFloatField;
    taBadUIDDC2: TFIBFloatField;
    taBadUIDDC22: TFIBFloatField;
    taBadUIDAPC2: TFIBFloatField;
    taBadUIDSELLC2: TFIBFloatField;
    taBadUIDRETC2: TFIBFloatField;
    taBadUIDC2: TFIBFloatField;
    acExport: TAction;
    siExport: TSpeedItem;
    taShopReportListENTRYCOST: TFIBFloatField;
    taShopReportListRESCOST: TFIBFloatField;
    siHelp: TSpeedItem;
    frShopReportList: TfrDBDataSet;
    pmPrint: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taShopReportListCalcFields(DataSet: TDataSet);
    procedure taShopReportListBeforeOpen(DataSet: TDataSet);
    procedure acAddExecute(Sender: TObject);
    procedure gridShopReportListGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acRefreshUpdate(Sender: TObject);
    procedure acFillEntryDataExecute(Sender: TObject);
    procedure acFillEntryDataUpdate(Sender: TObject);
    procedure acPrintShopReportExecute(Sender: TObject);
    procedure taShopReportListBeforeDelete(DataSet: TDataSet);
    procedure acPrintShopReportUpdate(Sender: TObject);
    procedure acCheckDataExecute(Sender: TObject);
    procedure acCheckDataUpdate(Sender: TObject);
    procedure acPrintShopReportBadUIDExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure acExportUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N7Click(Sender: TObject);

  private
    FYear : word;
    LogOperIdForm : string;
    procedure Create_ShopReport;
    procedure WriteTimeExec(CallTime : Cardinal);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure DepClick(Sender: TObject);
  end;

var
  fmShopReportList: TfmShopReportList;

implementation

{$R *.dfm}

uses dbUtil, DateUtils, comdata, UtilLib, Data3, Data, JewConst, fmUtils,
  ShopReport, ReportData, MsgDialog, ShopReport_CheckData, Period, InsRepl;

function Acc(q: TpFIBDataSet): boolean;
begin
  Result:=true;
end;

procedure TfmShopReportList.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  tb2.WallPaper := wp;
  edYear.OnChange := nil;
  FYear := YearOf(Now);
  edYear.Value := FYear;
  edYear.OnChange := edYearChange;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  dmCom.tr.StartTransaction;
  if CenterDep then
  begin
    dmCom.FillDepMenu(dmCom.quDep, pmDep, Acc, DepClick, True);
    dm.SDep := pmDep.Items[0].Caption;// '��� ������';
    dm.SDepId := pmDep.Items[0].Tag; // -1;
  end
  else begin
    //dmCom.FillDepMenu(dmCom.quDep, pmDep, Acc, DepClick, True);
    siDep.Visible := False;
    dm.SDep := dmCom.Dep[SelfDepId].SName;
    dm.SDepId := SelfDepId;
    laDep.Caption:=dm.SDep;
  end;
  OpenDataSet(taShopReportList);
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);

  N5.Visible := SelfDepId = 1;

  N4.Visible := SelfDepId = 1;

  if SelfDepId <> 1 then
  begin
    siRep.DropDownMenu := nil;
  end;
end;

procedure TfmShopReportList.edYearChange(Sender: TObject);
begin
  FYear := edYear.Value;
  ReOpenDataSet(taShopReportList);
end;

procedure TfmShopReportList.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmShopReportList.taShopReportListCalcFields(DataSet: TDataSet);
begin
  taShopReportListMONTHNAME.AsString := cMonth[MonthOf(taShopReportListSDATE.AsDateTime)];
end;

procedure TfmShopReportList.taShopReportListBeforeOpen(DataSet: TDataSet);
begin
  taShopReportList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(FYear));
  taShopReportList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(FYear));
  if dm.SDepId=-1 then
  begin
   taShopReportList.ParamByName('DEPID1').AsInteger := -MaxInt;
   taShopReportList.ParamByName('DEPID2').AsInteger := MaxInt;
  end
  else
  begin
   taShopReportList.ParamByName('DEPID1').AsInteger := dm.SDepId;
   taShopReportList.ParamByName('DEPID2').AsInteger := dm.SDepId;
  end
end;

procedure TfmShopReportList.acAddExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Month : byte;
  bd, ed: TDateTime;
  LogOperationID: string;
begin
  if (dm.SDepId = -1) then raise EWarning.Create('�������� �����');
  if not AppDebug then
  begin
    if CenterDep and (dm.SDepId <> CenterDepId) then raise EWarning.Create('�������� ����� ������ ����� �� ����������� �����');
  end;

  Bookmark := taShopReportList.GetBookmark;
  Month := 0;
  taShopReportList.DisableControls;
  try
    taShopReportList.First;
    while not taShopReportList.Eof do
    begin
      if CenterDep then
      begin
        if (MonthOf(taShopReportListSDATE.AsDateTime) > Month) and (taShopReportListDEPID.AsInteger = dm.SDepId)then
          Month := MonthOf(taShopReportListSDATE.AsDateTime);
      end
      else begin
        if (MonthOf(taShopReportListSDATE.AsDateTime) > Month) and (taShopReportListDEPID.AsInteger = dm.SDepId)then
          Month := MonthOf(taShopReportListSDATE.AsDateTime);
      end;
      taShopReportList.Next;
    end;
  finally
    taShopReportList.GotoBookmark(Bookmark);
    taShopReportList.EnableControls;
  end;
  Inc(Month);
  if Month=13 then
  begin
    MessageDialog('��� ������ �� '+IntToStr(FYear)+' ������������', mtInformation, [mbOk], 0);
    eXit;
  end;
  bd := StartOfAMonth(FYear, Month);
  ed := EndOfAMonth(FYear, Month);
  if not GetPeriod(bd, ed) then eXit;
  LogOperationID := dm3.insert_operation(sLog_ShopReportAdd, LogOperIdForm);
  taShopReportList.Append;
  taShopReportListSINVID.AsInteger := dmCom.GetId(8);
  taShopReportListSN.AsInteger := Month;
  taShopReportListSDATE.AsDateTime := bd;
  taShopReportListNDATE.AsDateTime := ed;
  if CenterDep then taShopReportListDEPID.AsInteger := dm.SDepId
  else taShopReportListDEPID.AsInteger := SelfDepId;
  taShopReportListUSERID.AsInteger := dmCom.UserId;
  taShopReportListCRDATE.AsDateTime := dmCom.GetServerTime;
  taShopReportList.Post;
  Application.ProcessMessages;
  Create_ShopReport;
  dm3.update_operation(LogOperationID);
end;

procedure TfmShopReportList.Create_ShopReport;
begin
  if (taShopReportListDEPID.AsInteger<>CenterDepId) then
     ExecSQL('execute procedure CREATE_SHOPREPORT('+taShopReportListSINVID.AsString+')', dmCom.quTmp)
  else
     begin
     ExecSQL('execute procedure CREATE_CENTERREPORT('+taShopReportListSINVID.AsString+')', dmCom.quTmp);

     ExecSQL('execute procedure CREATE_CENTERREPORT_COM('+taShopReportListSINVID.AsString+')', dmCom.quTmp);

     ExecSQL('execute procedure SHOP$REPORT$G('+taShopReportListSINVID.AsString+')', dmCom.quTmp);
     end;
  WriteTimeExec(dmCom.quTmp.CallTime);
  RefreshDataSet(taShopReportList);
end;

procedure TfmShopReportList.WriteTimeExec(CallTime: Cardinal);
begin
  stbrMain.SimpleText := '����� ����������: '+Format('%.2f', [CallTime/1000])+'�';
end;

procedure TfmShopReportList.gridShopReportListGetCellParams(
  Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'DEPNAME')and(not taShopReportListDEPCOLOR.IsNull)
  then Background := taShopReportListDEPCOLOR.AsInteger;
end;

procedure TfmShopReportList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmShopReportList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmShopReportList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taShopReportList);
  Action := caFree;
end;

procedure TfmShopReportList.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmShopReportList.acAddUpdate(Sender: TObject);
begin
  if AppDebug then acAdd.Enabled := taShopReportList.Active
  else acAdd.Enabled := taShopReportList.Active and (dm.SDepId = SelfDepId);
end;

procedure TfmShopReportList.acDelExecute(Sender: TObject);
begin
  taShopReportList.Delete;
end;

procedure TfmShopReportList.acDelUpdate(Sender: TObject);
begin
  if AppDebug then
    acDel.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty)
  else
    acDel.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty) and
                     (taShopReportListDEPID.AsInteger = SelfDepId);
end;

procedure TfmShopReportList.acViewExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmShopReport, Self, TForm(fmShopReport), True, False);
end;

procedure TfmShopReportList.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty);
end;

procedure TfmShopReportList.acRefreshExecute(Sender: TObject);
begin
  if (not AppDebug) and CenterDep and (taShopReportListDEPID.AsInteger <> CenterDepId) then
    raise EWarning.Create('�������� ����� ������ ����� �� ����������� �����');
  if MessageDialog('�������� �������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  Create_ShopReport;
end;

procedure TfmShopReportList.acRefreshUpdate(Sender: TObject);
begin
  acRefresh.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty);
end;

procedure TfmShopReportList.acFillEntryDataExecute(Sender: TObject);
begin
  if (not AppDebug) and CenterDep and (dm.SDepId <> CenterDepId) then
    raise EWarning.Create('��������� �������� ����� ������ ��� ������ �� ����������� �����');
  ExecSQL('execute procedure SHOPREPORT_FILLENTRY_BY_WH('+taShopReportListSINVID.AsString+')', dm3.quTmp);
  WriteTimeExec(dm3.quTmp.CallTime);
  taShopReportList.Refresh;
end;

procedure TfmShopReportList.acFillEntryDataUpdate(Sender: TObject);
begin
  acFillEntryData.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty);
end;

procedure TfmShopReportList.DepClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taShopReportList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    dm.SDepId:= TMenuItem(Sender).Tag;
    dm.SDep:= TMenuItem(Sender).Caption;
    laDep.Caption:=dm.SDep;
    taShopReportList.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TfmShopReportList.acPrintShopReportExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := taShopReportListSINVID.AsInteger;
    if taShopReportListDEPID.AsInteger=CenterDepId then
     PrintDocument(arr, center_report)
    else PrintDocument(arr, shop_report);
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReportList.taShopReportListBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� �������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TfmShopReportList.acPrintShopReportUpdate(Sender: TObject);
begin
  acPrintShopReport.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty);
end;

procedure TfmShopReportList.acCheckDataExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmShopReport_CheckData, TForm(fmShopReport_CheckData));
end;

procedure TfmShopReportList.acCheckDataUpdate(Sender: TObject);
begin
  acCheckData.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty) and (taShopReportListDEPID.AsInteger<>CenterDepId);
end;

procedure TfmShopReportList.acPrintShopReportBadUIDExecute(Sender: TObject);
begin
  dmReport.PrintDocumentB(shop_report_baduid);
end;

procedure TfmShopReportList.acExportExecute(Sender: TObject);
var NeedSPR, NeedSPR1, PacketId:integer;
    CodeDep, CodedepC:string;
    flDescription:boolean;
begin
  with dm do
 begin
  qutmp.SQL.Text:='select r_code from d_dep where WH=1';
  qutmp.ExecQuery;
  CodeDepC:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_code from d_dep where d_depid='+inttostr(SelfDepId);
  qutmp.ExecQuery;
  CodeDep:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_packetid from r_packet where PacketState=0 and r_item='#39
                  +'DESCRIPTION'+#39' and sourceid='#39+CodeDep+#39;
  qutmp.ExecQuery;
  if qutmp.Fields[0].IsNull then  flDescription:=false else flDescription:=true;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
 end;

 NeedSPR1:=0;
 NeedSPR:=0;

 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);

  ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
  InsRPacket(dmCom.db, 'DELRECORDS', CodeDepC, 4, CodeDepC+';3;', '��������� ������ �� �������� �������', 1,NeedSPR, PacketId);

  InsRPacket(dmCom.db, 'SHOPREPORT', CodeDepC, 2,
               taShopReportListSINVID.AsString, '�������� ����� �'+ taShopReportListSN.AsString, 1,NeedSPR, PacketId);

 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);

 MessageDialog('������������ ������ ��� �������� ��������� ������ ���������!', mtInformation, [mbOk], 0);
end;

procedure TfmShopReportList.acExportUpdate(Sender: TObject);
begin
  acExport.Enabled := taShopReportList.Active and (not taShopReportList.IsEmpty) and (not CenterDep);
end;

procedure TfmShopReportList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100430)
end;

procedure TfmShopReportList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;


procedure TfmShopReportList.N3Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := taShopReportListSINVID.AsInteger;
    if taShopReportListDEPID.AsInteger=CenterDepId then
     PrintDocument(arr, center_report)
    else PrintDocument(arr, shop_report);
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReportList.N4Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := taShopReportListSINVID.AsInteger;
    PrintDocument(arr, center_report_com)
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReportList.N5Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := taShopReportListSINVID.AsInteger;

    //arr[0] := -1;

    PrintDocument(arr, center_report_tol)
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReportList.N7Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := taShopReportListSINVID.AsInteger;
    PrintDocument(arr, center_registry_com)
  finally
    Finalize(arr);
  end;
end;



end.
