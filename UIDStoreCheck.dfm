object fmUIDStoreCheck: TfmUIDStoreCheck
  Left = 197
  Top = 198
  Width = 537
  Height = 354
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1076#1072#1085#1085#1099#1093
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 529
    Height = 39
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      Spacing = 1
      Left = 458
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object spitUIDHistory: TSpeedItem
      Action = acUIDHistory
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acUIDHistoryExecute
      SectionName = 'Untitled (0)'
    end
    object spitUpdate: TSpeedItem
      Action = acUpdate
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acUpdateExecute
      SectionName = 'Untitled (0)'
    end
    object siRepeat: TSpeedItem
      BtnCaption = #1055#1086#1074#1090#1086#1088#1099#13#10#1085#1072' '#1089#1082#1083#1072#1076#1077
      Caption = #1055#1086#1074#1090#1086#1088#1099
      Hint = #1055#1086#1074#1090#1086#1088#1099'|'
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = siRepeatClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 306
    Width = 529
    Height = 19
    Panels = <>
  end
  object gridUIDStoreCheck: TDBGridEh
    Left = 0
    Top = 39
    Width = 529
    Height = 267
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsrCheckUIDStoreData
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.FieldName = 'UID'
        Footer.ValueType = fvtCount
        Footers = <>
        Width = 63
      end
      item
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 107
      end
      item
        EditButtons = <>
        FieldName = 'FLAG'
        Footers = <>
        Width = 214
      end
      item
        EditButtons = <>
        FieldName = 'WHCOST'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1089#1082#1083#1072#1076
      end
      item
        EditButtons = <>
        FieldName = 'UIDSTORECOST'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1086#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 24
    Top = 108
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acUIDHistory: TAction
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      OnExecute = acUIDHistoryExecute
      OnUpdate = acUIDHistoryUpdate
    end
    object acUpdate: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = acUpdateExecute
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = gridUIDStoreCheck
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    PrintFontName = 'Times New Roman'
    Title.Strings = (
      #1055#1088#1086#1074#1077#1088#1082#1072' '#1076#1072#1085#1085#1099#1093)
    Units = MM
    Left = 312
    Top = 120
  end
end
