unit ActAllowances;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RXDBCtrl, Mask, DBCtrls, ExtCtrls, 
  Grids, DBGridEh, ActnList, ComDrv32, M207Ctrls, DateUtils,
  PrnDbgeh, PrntsEh, Printers, DBCtrlsEh, Buttons, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar, DB, FIBDataSet, pFIBDataSet;

type
  TfmActAllowances = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siCloseInv: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBText2: TDBText;
    Label3: TLabel;
    DBText4: TDBText;
    laCost0: TLabel;
    DBTextFr: TDBText;
    edSN: TDBEdit;
    Panel2: TPanel;
    Label2: TLabel;
    edUid: TEdit;
    dg1: TDBGridEh;
    fr1: TM207FormStorage;
    acList: TActionList;
    acDel: TAction;
    acCloseInv: TAction;
    acCose: TAction;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    dbedSdate: TDBEditEh;
    btDate: TBitBtn;
    siHelp: TSpeedItem;
    DBLookupComboBox1: TDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure edUidKeyPress(Sender: TObject; var Key: Char);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acDelExecute(Sender: TObject);
    procedure acCoseExecute(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acCloseInvUpdate(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure btDateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBLookupComboBox1CloseUp(Sender: TObject);
  private
    LogOprIdForm, s_close, s_closed:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmActAllowances: TfmActAllowances;

implementation

uses data, data3, servdata, comdata, M207Proc, dbUtil, JewConst,
     SetSDate, MsgDialog;

{$R *.dfm}

procedure TfmActAllowances.FormCreate(Sender: TObject);
begin
  dm3.DBProbe.Active := True;
  
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
  
  dm.WorkMode:='ACTALLOW';
  tb1.WallPaper:=wp;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  ReOpenDataSets([dm3.quActAllowances]);
  ActiveControl:=edUid;
  s_close:='';
  s_closed:='';
  
{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;

  dmServ.ComScan.Connect;
  dmcom.SScanZ:='';     }
end;

procedure TfmActAllowances.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=(not dm3.quActAllowancesSITEMID.IsNull) and
                          (dm3.quActAllowancesListISCLOSED.AsInteger=0);
end;

procedure TfmActAllowances.edUidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '0'..'9',#8:;
  else SysUtils.Abort;
 end
end;

procedure TfmActAllowances.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmActAllowances.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if not dm.closeinv(dm3.quActAllowancesListSINVID.AsInteger,0) then
  begin
   if MessageDialog('��������� �������', mtInformation, [mbOK, mbCancel], 0) = mrOk then
   begin
    PostDataSets([dm3.quActAllowances, dm3.quActAllowancesList]);
    dm3.quActAllowancesList.Refresh;
    CloseDataSets([dm3.quActAllowances]);
//    if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;   **********
   end
   else SysUtils.Abort;
  end
  else
  begin
   PostDataSets([dm3.quActAllowances, dm3.quActAllowancesList]);
   CloseDataSets([dm3.quActAllowances]);
//   if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;   ************
  end;
end;

procedure TfmActAllowances.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var sitemid, fres:integer;
begin
 case Key of
  VK_RETURN:
   if dm3.quActAllowancesListISCLOSED.AsInteger=1 then
   begin
     MessageDialog('��������� �������', mtWarning, [mbOk], 0);
     edUid.Text:='';
   end
   else
   if edUid.Text='' then MessageDialog('������� ����� �������!', mtWarning, [mbOk], 0)
   else begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select fres, sitemid from ActAllowances_I ('+edUid.Text+', '+
                       dm3.quActAllowancesListSINVID.AsString+')';
    dm.quTmp.ExecQuery;
    fres:=dm.quTmp.Fields[0].AsInteger;
    sitemid:=dm.quTmp.Fields[1].AsInteger;
    dm.quTmp.Close;
    dm.quTmp.Transaction.CommitRetaining;
    case fres of
     0: begin ReOpenDataSets([dm3.quActAllowances]);
              dm3.quActAllowances.Locate('SITEMID', sitemid, []) end;
     1: MessageDialog('������� ������� � �������', mtInformation, [mbOk], 0);
     2: MessageDialog('������� ������� �����', mtInformation, [mbOk], 0);
     3: MessageDialog('������� ���������� ����������', mtInformation, [mbOk], 0);
     4: MessageDialog('������� �� �������', mtInformation, [mbOk], 0);
     5: MessageDialog('������� �� �������', mtInformation, [mbOk], 0);     
    end;
    edUid.Text:='';
   end;
  VK_DOWN: ActiveControl:=dg1; 
 end;
end;

procedure TfmActAllowances.acDelExecute(Sender: TObject);
begin
 if dm3.quActAllowancesListISCLOSED.AsInteger=1 then
   MessageDialog('��������� �������!', mtWarning, [mbOk], 0)
 else dm3.quActAllowances.Delete;
end;

procedure TfmActAllowances.acCoseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmActAllowances.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN:ActiveControl:=edUid;
 end;
end;

procedure TfmActAllowances.acCloseInvUpdate(Sender: TObject);
begin
 if dm3.quActAllowancesListISCLOSED.AsInteger=0 then
 begin
  TAction(Sender).Caption:='�������';
  TAction(Sender).Hint:='������� ���';
  TAction(Sender).ImageIndex:=5;
  edSn.Enabled:=true;
  dbedSdate.Enabled:=true;
 end
 else
 begin
  if (s_close='') and (s_closed='') then
  begin
   TAction(Sender).Caption:='�������';
   TAction(Sender).Hint:='������� ���';
   TAction(Sender).ImageIndex:=6;
   edSn.Enabled:=false;
   dbedSdate.Enabled:=false;
  end
 end
end;

procedure TfmActAllowances.acCloseInvExecute(Sender: TObject);
var i:integer;
    LogOperationID:string;
begin
 if dm3.quActAllowancesListISCLOSED.AsInteger=0 then
 begin
  with dm3, dm, quTmp do
   begin
    if quActAllowancesListSN.IsNull then
    begin
     close;
     SQL.Text:='SELECT max(sn) FROM SINV '+
               'WHERE ITYPE=19 '+
               ' AND DepFromId='+quActAllowancesListDEPFROMID.AsString+
               ' AND FYEAR(SDATE)='+GetSYear(quActAllowancesListSDATE.AsDateTime);
     ExecQuery;
     if Fields[0].IsNull then i:=1
     else i:=Fields[0].AsInteger+1;
     Close;
     Transaction.CommitRetaining;
     quActAllowancesList.Edit;
     quActAllowancesListSN.AsInteger:=i;
     quActAllowancesList.Post;
     quActAllowancesList.Refresh;
    end;

    SQL.Text:='SELECT COUNT(*) FROM SINV '+
              'WHERE ITYPE=19 AND SN='+quActAllowancesListSN.AsString+
              ' AND DepFromId='+quActAllowancesListDEPFROMID.AsString+
              ' AND FYEAR(SDATE)='+GetSYear(quActAllowancesListSDATE.AsDateTime);

    ExecQuery;
    i:=Fields[0].AsInteger;
    Close;
    Transaction.CommitRetaining;
    if i>1 then raise Exception.Create('������������ ����� ���������!!!');
   end;

  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
  begin
   LogOperationID:=dm3.insert_operation(sLog_ActAllowancesClose,LogOprIdForm);  
   with dm, dm3, quTmp do
    begin
    {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
     SQL.Text:='select noedit, noedited from Edit_Date_Inv ('+quActAllowancesListSINVID.AsString+')';
     ExecQuery;

     {s_close:=trim(Fields[0].AsString);
     s_closed:=trim(Fields[1].AsString);}
     Transaction.CommitRetaining;
     close;
    end;
   if (s_close='') and (s_closed='') then
   begin
    Screen.Cursor:=crSQLWait;
    ExecSQL('execute procedure CLOSEINV('+dm3.quActAllowancesListSINVID.AsString+', '+
            '19, '+inttostr(dmcom.UserId)+')', dm.quTmp);
    Screen.Cursor:=crDefault;
   end
   else begin
         if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
         if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
   end;
   dm3.quActAllowancesList.Refresh;

   dm3.update_operation(LogOperationID);
  end
 end
 else
 begin
  if MessageDialog('������� ���������', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
  begin
   LogOperationID:=dm3.insert_operation(sLog_ActAllowancesOpen,LogOprIdForm);
   Screen.Cursor:=crSQLWait;
   {�������� ���������}
   ExecSQL('execute procedure OPENINV('+dm3.quActAllowancesListSINVID.AsString+', '+
           '19, '+inttostr(dmcom.UserId)+')', dm.quTmp);
   {��������� ������� tmpdata ���� ���� ������� ������� ����� �����������}
   ExecSQL('execute procedure Before_Open_InvRet('+dm3.quActAllowancesListSINVID.AsString+
           ', 2, '+inttostr(dmcom.UserId)+')', dm.quTmp);
   with dm, quTmp do
   begin
     sql.Text:='select id1 from tmpdata where datatype=8 and '+
               ' userid='+inttostr(dmcom.UserId)+' and id2='+dm3.quActAllowancesListSINVID.AsString;
     ExecQuery;
     s_close:='';
     while not eof do
     begin
      s_close:=s_close+#13#10+Fields[0].AsString;
      Next;
     end;
     Transaction.CommitRetaining;
     close;
    end;
    Screen.Cursor:=crDefault;
    if s_close<>'' then
    begin
      s_close:='���� ��������� ���� �� �������� �� ���������. �������:'+s_close+
               #13#10+'��������� �����������.';
      if MessageDialog(s_close, mtConfirmation, [mbYes, mbNo], 0)=mrYes then
      begin
       {������� ���� ����������}
        Screen.Cursor:=crSQLWait;
        ExecSQL('execute procedure Before_Open_InvRet('+dm3.quActAllowancesListSINVID.AsString+
                ', 3, '+inttostr(dmcom.UserId)+')', dm.quTmp);
        Screen.Cursor:=crDefault;
        MessageDialog('��� ���������� ������!', mtInformation, [mbOk], 0);
      end;
     {�������� ������� tmpdata}
      ExecSQL('delete from tmpdata where datatype=8 and id2='+dm3.quActAllowancesListSINVID.AsString, dm.quTmp);
    end;
    dm3.quActAllowancesList.Refresh;
    dm3.update_operation(LogOperationID);
  end
 end
end;

procedure TfmActAllowances.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdg1.Title.Clear;
 pdg1.Title.Add('');
 pdg1.Title.Add('��� �������� �'+ dm3.quActAllowancesListSN.AsString);
 pdg1.Title.Add('');
 pdg1.Print;
end;

procedure TfmActAllowances.btDateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm3.quActAllowancesListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm3.quActAllowancesListSDATE1.AsDateTime;
   fmSetSDate.tp1.Time:=dm3.quActAllowancesListSDATE1.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm3.quActAllowancesList.Edit;
    dm3.quActAllowancesListSDATE.AsDateTime:=d;
    dm3.quActAllowancesList.Post;
    dm3.quActAllowances.Refresh;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmActAllowances.DBLookupComboBox1CloseUp(Sender: TObject);
begin
  if dm3.quActAllowances.State = dsEdit then
  begin
    dm3.quActAllowances.Post;
  end;
  
end;

procedure TfmActAllowances.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100272)
end;

procedure TfmActAllowances.FormActivate(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmActAllowances.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp else inherited;
end;

end.
