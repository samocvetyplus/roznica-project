object fmInfoEditArtHist: TfmInfoEditArtHist
  Left = 163
  Top = 183
  Width = 867
  Height = 540
  HelpContext = 100560
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082' '#1072#1088#1090#1080#1082#1091#1083#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 859
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 43
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Lperiod: TLabel
      Left = 264
      Top = 8
      Width = 35
      Height = 13
      Caption = 'Lperiod'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 518
      Top = 8
      Width = 142
      Height = 13
      Caption = #1058#1086#1083#1100#1082#1086' '#1086#1090#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1085#1099#1077
      Transparent = True
    end
    object edArt: TEdit
      Left = 54
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edArtKeyDown
    end
    object chbOnlyEdit: TCheckBox
      Left = 503
      Top = 9
      Width = 12
      Height = 12
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = chbOnlyEditClick
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 683
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siperiod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076'  '
      Caption = #1055#1077#1088#1080#1086#1076'  '
      Hint = #1055#1077#1088#1080#1086#1076'  |'
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 179
      Top = 3
      Visible = True
      OnClick = siperiodClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Layout = blGlyphLeft
      Spacing = 1
      Left = 763
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object pc1: TPageControl
    Left = 0
    Top = 29
    Width = 859
    Height = 482
    ActivePage = teditart
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Style = tsFlatButtons
    TabOrder = 1
    object teditart: TTabSheet
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1087#1077#1088#1074#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072
      object dgEditArt: TDBGridEh
        Left = 0
        Top = 0
        Width = 851
        Height = 451
        Align = alClient
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm3.dsEditArt
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'FULLART'
            Footers = <>
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Width = 150
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083
            Width = 114
          end
          item
            EditButtons = <>
            FieldName = 'COMP'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1055#1086#1089#1090'.'
            Width = 104
          end
          item
            EditButtons = <>
            FieldName = 'D_MATID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1052#1072#1090'.'
          end
          item
            EditButtons = <>
            FieldName = 'D_GOODID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1053#1072#1080#1084'. '#1080#1079#1076'.'
          end
          item
            EditButtons = <>
            FieldName = 'D_INSID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1054#1042
          end
          item
            EditButtons = <>
            FieldName = 'D_COUNTRYID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1057#1090#1088'.'
          end
          item
            EditButtons = <>
            FieldName = 'UNIT'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1045#1076'.'#1080#1079#1084'.'
          end
          item
            EditButtons = <>
            FieldName = 'ATT1'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1090#1088'.1'
          end
          item
            EditButtons = <>
            FieldName = 'ATT2'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1090#1088'.2'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ART'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083
            Width = 116
          end
          item
            EditButtons = <>
            FieldName = 'OLD_COMP'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1055#1086#1089#1090'.'
            Width = 107
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_MATID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1052#1072#1090'.'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_GOODID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1053#1072#1080#1084'. '#1080#1079#1076'.'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_INSID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1054#1042
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_COUNTRYID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1057#1090#1088'.'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_UINT'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1045#1076'. '#1080#1079#1084'.'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ATT1'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1090#1088'.1'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ATT2'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1040#1090#1088'.2'
          end
          item
            EditButtons = <>
            FieldName = 'ADATE'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103
          end
          item
            EditButtons = <>
            FieldName = 'D_COMPID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1087#1086#1089#1090'.'
          end
          item
            EditButtons = <>
            FieldName = 'ATT1ID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1072#1090#1088#1080#1073#1091'1'
          end
          item
            EditButtons = <>
            FieldName = 'ATT2ID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1072#1090#1088#1080#1073#1091'2'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_COMPID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1087#1086#1089#1090'.'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ATT1ID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1072#1090#1088#1080#1073#1091'1'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ATT2ID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1048#1044' '#1072#1090#1088#1080#1073#1091'2'
          end
          item
            EditButtons = <>
            FieldName = 'AHSTID'
            Footers = <>
            Title.Caption = #1048#1044' '#1080#1089#1090#1086#1088#1080#1080
          end
          item
            EditButtons = <>
            FieldName = 'D_ARTID'
            Footers = <>
            Title.Caption = #1048#1044' '#1072#1088#1090#1080#1082#1091#1083#1072
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object teditart2: TTabSheet
      Caption = #1042#1090#1086#1088#1086#1081' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      object dgEditArt2: TDBGridEh
        Left = 0
        Top = 0
        Width = 851
        Height = 453
        Align = alClient
        AllowedOperations = []
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm3.dsEditArt2
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'FULLART'
            Footers = <>
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Width = 165
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          end
          item
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2|'#1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ART2'
            Footers = <>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2|'#1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
          end
          item
            EditButtons = <>
            FieldName = 'ADATE'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103
          end
          item
            EditButtons = <>
            FieldName = 'A2HIST_ID'
            Footers = <>
            Title.Caption = #1048#1044' '#1080#1089#1090#1086#1088#1080#1080
          end
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
            Title.Caption = #1048#1044' '#1072#1088#1090#1080#1082#1091#1083#1072' 2'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object teditins: TTabSheet
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 2
      object dgEditIns: TDBGridEh
        Left = 0
        Top = 0
        Width = 851
        Height = 453
        Align = alClient
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm3.dsEditIns
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'FULLART'
            Footers = <>
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Width = 179
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          end
          item
            EditButtons = <>
            FieldName = 'D_INSID'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1042#1089#1090#1072#1074#1082#1072
          end
          item
            EditButtons = <>
            FieldName = 'QUANTITY'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1050#1086#1083'-'#1074#1086
          end
          item
            EditButtons = <>
            FieldName = 'WEIGHT'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1042#1077#1089
          end
          item
            EditButtons = <>
            FieldName = 'COLOR'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1062#1074#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'CHROMATICITY'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1062#1074#1077#1090#1085#1086#1089#1090#1100
          end
          item
            EditButtons = <>
            FieldName = 'CLEANNES'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1063#1080#1089#1090#1086#1090#1072
          end
          item
            EditButtons = <>
            FieldName = 'GR'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1043#1088#1091#1087#1087#1072
          end
          item
            EditButtons = <>
            FieldName = 'SHAPE'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1060#1086#1088#1084#1072
          end
          item
            EditButtons = <>
            FieldName = 'EDGETION'
            Footers = <>
            Title.Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1054#1075#1088#1072#1085#1082#1072
          end
          item
            EditButtons = <>
            FieldName = 'OLD_D_INSID'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1042#1089#1090#1072#1074#1082#1072
          end
          item
            EditButtons = <>
            FieldName = 'OLD_QUANTITY'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1050#1086#1083'-'#1074#1086
          end
          item
            EditButtons = <>
            FieldName = 'OLD_WEIGHT'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1042#1077#1089
          end
          item
            EditButtons = <>
            FieldName = 'OLD_COLOR'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1062#1074#1077#1090
          end
          item
            EditButtons = <>
            FieldName = 'OLD_CHROMATICITY'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1062#1074#1077#1090#1085#1086#1089#1090#1100
          end
          item
            EditButtons = <>
            FieldName = 'OLD_CLEANNES'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1063#1080#1089#1090#1086#1090#1072
          end
          item
            EditButtons = <>
            FieldName = 'OLD_GR'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1043#1088#1091#1087#1087#1072
          end
          item
            EditButtons = <>
            FieldName = 'OLD_SHAPE'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1060#1086#1088#1084#1072
          end
          item
            EditButtons = <>
            FieldName = 'OLD_EDGETION'
            Footers = <>
            Title.Caption = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077'|'#1054#1075#1088#1072#1085#1082#1072
          end
          item
            EditButtons = <>
            FieldName = 'ADATE'
            Footers = <>
            Title.Caption = #1044#1072#1090#1072' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103
          end
          item
            AutoFitColWidth = False
            EditButtons = <>
            FieldName = 'Operation'
            Footers = <>
            Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
          end
          item
            EditButtons = <>
            FieldName = 'Where_'
            Footers = <>
            Title.Caption = #1055#1088#1080#1082#1072#1079
          end
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
            Title.Caption = 'Art2id|new'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_ART2ID'
            Footers = <>
            Title.Caption = 'Art2id|old'
          end
          item
            EditButtons = <>
            FieldName = 'MAIN'
            Footers = <>
            Title.Caption = 'Main|new'
          end
          item
            EditButtons = <>
            FieldName = 'OLD_MAIN'
            Footers = <>
            Title.Caption = 'Main|old'
          end
          item
            EditButtons = <>
            FieldName = 'INSHIST_ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'INSID'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 432
    Top = 168
  end
end
