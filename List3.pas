unit List3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, db, DBCtrls, Buttons, rxPlacemnt,
  rxSpeedbar;

type
  TFieldInfo = class(TObject)
    FTitleName : string;
  end;

  TList3Kind = (l3kMain, l3kOther);

  TfmList3 = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    stbrList3: TStatusBar;
    spitCalc: TSpeedItem;
    fmstrJSz: TFormStorage;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    spitPeriod: TSpeedItem;
    grlist3: TM207IBGrid;
    spbrFilter: TSpeedBar;
    lbMat: TLabel;
    cmbxMat: TComboBox;
    lbGood: TLabel;
    cmbxGood: TComboBox;
    lbComp: TLabel;
    cmbxProd: TComboBox;
    btnDo: TSpeedButton;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure grlist3GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure spitCalcClick(Sender: TObject);
    procedure grlist3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure spitPrintClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure btnDoClick(Sender: TObject);
    procedure spitPeriodClick(Sender: TObject);
    procedure cmbxMatCloseUp(Sender: TObject);
    procedure cmbxGoodCloseUp(Sender: TObject);
    procedure cmbxProdCloseUp(Sender: TObject);
  private
    FFieldList : TStringList; // ������ ����� ������� ���� ��������� �� �������
    FColumnList : TStringList;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure TranslateDescr(Template : string);
    procedure ShowPeriod;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
  end;



var
  fmList3: TfmList3;
  (* ��� ��������� ��������� *)
  List3Kind : TList3Kind;

implementation

uses comdata, ServData, Data, ReportData, bsStrUtils, RegExpr, M207Proc,
    Variants, StrUtils, Period, RxDateUtil, dbTree;

{$R *.DFM}

procedure TfmList3.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmList3.FormCreate(Sender: TObject);
var
  Descr : string;
  i, j  : integer;
//  TabSheet : TTabSheet;
//  gr : TM207IbGrid;
//  DepInfo : TDepInfo;
begin
  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;
  spbrFilter.Wallpaper := wp;
  dmServ.L3BD := dmCom.User.L3BD;
  dmServ.L3ED := dmCom.User.L3ED;
  ShowPeriod;
  case List3Kind of
    l3kOther : Caption := '��������� ��������� (3-� �����)';
    l3kMain : Caption := '��������� ���������';
    else Caption := '��������� ���������';
  end;

  with dmServ, dmCom do
  begin
    if dmCom.tr.Active then dmCom.tr.CommitRetaining;
    if not dmCom.tr.Active then dmCom.tr.StartTransaction;
    (* ��������� ������ ���������� *)
    dm.LoadArtSL(ALL_DICT);
    cmbxMat.Items.Assign(dm.slMat);
    cmbxMat.ItemIndex := 0;
    cmbxGood.Items.Assign(dm.slGood);
    cmbxGood.ItemIndex := 0;
    cmbxProd.Items.Assign(dm.slProd);
    cmbxProd.ItemIndex := 0;
    D_MatId := TNodeData(cmbxMat.Items.Objects[0]).Code;
    D_GoodId := TNodeData(cmbxGood.Items.Objects[0]).Code;
    D_CompId := TNodeData(cmbxProd.Items.Objects[0]).Code;
    (* ����� ���� ���������� � ����� *)
    quTmp.Close;
    quTmp.SQL.Text := 'select Descr from GetDescr(1)';
    quTmp.ExecQuery;
    Descr := quTmp.Fields[0].AsString;
    TranslateDescr(Descr);
    quTmp.Close;
    for i := 0 to Pred(FFieldList.Count) do
    begin
      if Assigned(taList3.FindField(FFieldList[i])) then
        for j:=0 to Pred(dmCom.DepCount) do if dmCom.DepInfo[j].Shop = 1 then
          with grList3.Columns.Add do
          begin
            Title.Caption := TFieldInfo(FFieldList.Objects[i]).FTitleName+'-'+
              dmCom.DepInfo[j].SName;
            Title.Font.Color := clNavy;
            ReadOnly := True;
            FColumnList.Add(Title.Caption);
          end;
    end;
    FColumnList.Sort;
    dmServ.taList3.Open;
    dmCom.tr.CommitRetaining;
  end;
end;

procedure TfmList3.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmList3.grlist3GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Assigned(Field) then Exit;
  if not HighLight or grList3.ClearHighlight then
  if (Field.FieldName='DEBTORS')or
     (Field.FieldName='DEBTORW')or
     (Field.FieldName='DEBTORQ')then begin Background := dm.CU_SBD1; exit; end;

  if (Field.FieldName='SALES')or
     (Field.FieldName='SALEW')or
     (Field.FieldName='SALEQ') then begin Background := dm.CU_SL; exit; end;

  if (Field.FieldName='OPTS')or
     (Field.FieldName='OPTW')or
     (Field.FieldName='OPTQ') then begin Background := dm.CU_SO1; exit; end;

  if (Field.FieldName='RETOPTS')or
     (Field.FieldName='RETOPTW')or
     (Field.FieldName='RETOPTQ')then begin Background := dm.CU_RO1; exit; end;

  if (Field.FieldName='RETS')or
     (Field.FieldName='RETW')or
     (Field.FieldName='RETQ')then begin Background := dm.CU_RT1; exit; end;

  if (Field.FieldName='RETVENDORS')or
     (Field.FieldName='RETVENDORW')or
     (Field.FieldName='RETVENDORQ')then Background := dm.CU_SR1;
end;

procedure TfmList3.spitCalcClick(Sender: TObject);
begin
  Screen.Cursor := crSQLWait;
  try
    with dmServ.quJTmp, Params do
    begin
      if not Transaction.Active then Transaction.StartTransaction;
      ByName['USERID'].AsInteger := dmCom.UserId;
      ByName['BD'].AsDate := dmServ.L3BD;
      ByName['ED'].AsDate := dmServ.L3ED;
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
      ExecQuery;
      Transaction.CommitRetaining;
    end;
    with dmServ.taList3 do
    begin
      if not Transaction.Active then Transaction.StartTransaction;
      dmServ.taList3.Open;
      Transaction.CommitRetaining;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmList3.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmList3.TranslateDescr(Template: string);
const
  expr = '((\w*)=([�-�\s.\-]*);)';
var
  Reg : TRegExpr;
//  fi : TFieldInfo;
  i : integer;
begin
  Reg := TRegExpr.Create;
  with Reg do
  try
    Expression := expr;
    Compile;
    if Exec(Template)and(SubExprMatchCount=3) then
    begin
      FFieldList.Add(UpperCase(Match[2]));
      FFieldList.Objects[0] := TFieldInfo.Create;
      TFieldInfo(FFieldList.Objects[0]).FTitleName := Match[3];
    end;
    i := 1;
    while(ExecNext)and(SubExprMatchcount=3) do
    begin
      FFieldList.Add(UpperCase(Match[2]));
      FFieldList.Objects[i] := TFieldInfo.Create;
      TFieldInfo(FFieldList.Objects[i]).FTitleName := Match[3];
      inc(i);
    end;
  finally
    Reg.Free;
  end;
end;

constructor TfmList3.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFieldList := TStringList.Create;
  FColumnList := TStringList.Create;
end;

destructor TfmList3.Destroy;
begin
  FFieldList.Free;
  FColumnList.Free;
  inherited Destroy;
end;

procedure TfmList3.grlist3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  pos, i, NoDep : integer;
  Col, s : string;
  DefaultSeparator : char;
begin
  NoDep := 0;
  if FColumnList.Find(Column.Title.Caption, pos)  then
    with grList3.Canvas, dmServ do
    begin
      if AnsiContainsStr(Column.Title.Caption, '��.')then Brush.Color := clAqua else
      if AnsiContainsStr(Column.Title.Caption, '���.')then Brush.Color := clWhite else
      if AnsiContainsStr(Column.Title.Caption, '����')then Brush.Color := dm.CU_SBD1 else
      if AnsiContainsStr(Column.Title.Caption, '����')then Brush.Color := dm.CU_SL else
      if AnsiContainsStr(Column.Title.Caption, '����')then Brush.Color := dm.CU_RT1 else
      if AnsiContainsStr(Column.Title.Caption, '����')then Brush.Color := clMoneyGreen;

      for i:=0 to Pred(dmCom.DepCount) do if dmCom.DepInfo[i].Shop = 1 then
      begin
         NoDep := i;
         pos := ScanF(Column.Title.Caption, dmCom.DepInfo[i].SName, 1);
         if pos <> 0 then break;
      end;
      Col := Column.Title.Caption;
      Delete(Col, pos-1, Length(Col));
      FillRect(Rect);

      pos := -1;
      for i:=0 to Pred(FFieldList.Count) do
        if TFieldInfo(FFieldList.Objects[i]).FTitleName = Col then
        begin
          pos := i;
          break;
        end;
      if pos = -1 then s := '0'
      else begin
        DefaultSeparator := DecimalSeparator;
        DecimalSeparator := '.';
        s := dm.getCommaBlob(dmServ.taList3.FieldByName(FFieldList[pos]).AsString, NoDep+1);
        if s <> '' then s := FloatToStrF(StrToFloat(s), ffGeneral, 6, 2);
        DecimalSeparator := DefaultSeparator;
      end;
      TextOut(Rect.Right - 2-TextWidth(s), Rect.Top + 2, s);
    end;
end;

procedure TfmList3.spitPrintClick(Sender: TObject);
var arr : TarrDoc;
begin
  Setlength(arr, 1);
  arr[0] := dmCom.UserId;
  PrintDocument(arr, maindoc3);
end;

procedure TfmList3.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  ClosedataSets([dmServ.taList3, dmServ.quProducer, dmCom.taMat, dmCom.taGood]);
  dmCom.tr.CommitRetaining;
  grlist3.Columns.BeginUpdate;
  for i:=0 to Pred(grlist3.Columns.Count) do
    grlist3.Columns[i].Visible := True;
  grlist3.Columns.EndUpdate;
  Action := caFree;
end;

procedure TfmList3.FormActivate(Sender: TObject);
begin
  laDep.Caption := '��� ������';
  dmServ.List3DepId := -1;
end;

procedure TfmList3.btnDoClick(Sender: TObject);
begin
  ReOpenDataSets([dmServ.taList3]);
end;

procedure TfmList3.spitPeriodClick(Sender: TObject);
var
  d1, d2 : TDateTime;
begin
  d1 := dmServ.L3BD;
  d2 := dmServ.L3ED;
  if GetPeriod(d1, d2) then
  begin
    dmServ.L3BD := d1;
    dmServ.L3ED := d2;
    ShowPeriod;
  end;
end;

procedure TfmList3.ShowPeriod;
begin
  if(dmServ.L3BD = StrToDate('05.02.1981'))or(not ValidDate(dmServ.L3BD)or
    (not ValidDate(dmServ.L3ED))) then laPeriod.Caption := '������ �� ���������';
  laPeriod.Caption := 'C '+DateToStr(dmServ.L3BD)+' �� '+DateToStr(dmCom.User.L3ED);
end;

procedure TfmList3.cmbxMatCloseUp(Sender: TObject);
begin
  with cmbxMat, Items do
    dmCom.D_MatId := TNodeData(Objects[ItemIndex]).Code;
end;

procedure TfmList3.cmbxGoodCloseUp(Sender: TObject);
begin
  with cmbxGood, Items do
    dmCom.D_GoodId := TNodeData(Objects[ItemIndex]).Code;
end;

procedure TfmList3.cmbxProdCloseUp(Sender: TObject);
begin
  with cmbxProd, Items do
    dmCom.D_CompId := TNodeData(Objects[ItemIndex]).Code;
end;

end.
