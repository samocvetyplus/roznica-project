unit SellItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, Menus, db, Mask, DBCtrls,
  Buttons, ActnList, Variants, ZRCtrls, ZReport, Printers, WinsPool,
  DBGridEh, TB2Item, ComDrv32, PrnDbgeh, PrntsEh, M207Ctrls, 
  jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar, FIBDataSet,
  pFIBDataSet;

type
  DocInfo = record
   pdocname:string;
   poutputfile:string;
   pdatatype:string;
  end;

  TfmSellItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    laDep: TLabel;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    siView: TSpeedItem;
    Label1: TLabel;
    siSell: TSpeedItem;
    laSell: TLabel;
    siOpenSell: TSpeedItem;
    siCloseSell: TSpeedItem;
    siRet: TSpeedItem;
    pa1: TPanel;
    siRetToWH: TSpeedItem;
    edUID: TEdit;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    Label3: TLabel;
    dbtDep: TDBText;
    Label2: TLabel;
    dbtN: TDBText;
    Label4: TLabel;
    dbtBD: TDBText;
    Label5: TLabel;
    dbtED: TDBText;
    Panel3: TPanel;
    Label8: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    dbtCNT: TDBText;
    dbtCost: TDBText;
    dbtW: TDBText;
    Panel5: TPanel;
    Panel6: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    dbtRCnt: TDBText;
    dbtRCost: TDBText;
    dbtRW: TDBText;
    Panel7: TPanel;
    Panel8: TPanel;
    dbtCass: TDBText;
    Label12: TLabel;
    edArt: TEdit;
    Label13: TLabel;
    siXRep: TSpeedItem;
    siCheck: TSpeedItem;
    spitPrint: TSpeedItem;
    ppPrint: TPopupMenu;
    mnitTag: TMenuItem;
    mnitAppl: TMenuItem;
    mnitDisv: TMenuItem;
    mnitTicket: TMenuItem;
    tb3: TSpeedBar;
    Label14: TLabel;
    Label15: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label16: TLabel;
    SpeedbarSection3: TSpeedbarSection;
    siSetSDate: TSpeedItem;
    edRN: TDBEdit;
    mnitCmena: TMenuItem;
    acEvent: TActionList;
    acFRDateTime: TAction;
    mnitSep2: TMenuItem;
    acOpenSell: TAction;
    acPrintCheck: TAction;
    acPrintDisv: TAction;
    acPrintAppl: TAction;
    mnitCmenaOutPrice: TMenuItem;
    siOpenAll: TSpeedItem;
    pmcheck: TPopupMenu;
    NewCheck: TMenuItem;
    OldCheck: TMenuItem;
    acNewCheck: TAction;
    acCurCheck: TAction;
    acClient: TAction;
    acLastCheck: TAction;
    NSellArt: TMenuItem;
    pmEditDate: TPopupMenu;
    NDataWithSell: TMenuItem;
    NDataWithoutSell: TMenuItem;
    NDataWithRetSell: TMenuItem;
    acAdd: TAction;
    acRet: TAction;
    acDel: TAction;
    dg2: TDBGridEh;
    acView: TAction;
    acRetToWh: TAction;
    acCloseSell: TAction;
    acSellNew: TAction;
    acCheck: TAction;
    acXRep: TAction;
    acOpenAll: TAction;
    acSellEnd: TAction;
    ppSellItem: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    mnitRet: TTBItem;
    mnitRetToWh: TTBItem;
    fs: TM207FormStorage;
    acScanConnect: TAction;
    acActiveEdUid: TAction;
    acDataWithSell: TAction;
    acDataWithoutSell: TAction;
    acDataWithRetSell: TAction;
    pdg2: TPrintDBGridEh;
    acPrintGrid: TAction;
    acPrintTag: TAction;
    N3: TMenuItem;
    acPrintSellInPrice: TAction;
    acPrintSellOutPrice: TAction;
    acPrintSellArt: TAction;
    acPrintSellN: TAction;
    acActiveedArt: TAction;
    acActiveedRN: TAction;
    acActiveBd: TAction;
    acActiveEd: TAction;
    acActiveGrid: TAction;
    siApplDep: TSpeedItem;
    acCreateAplldep: TAction;
    acClearApplDep: TAction;
    pmApplDep: TTBPopupMenu;
    ibCreateApplDep: TTBItem;
    ibClearApplDep: TTBItem;
    acCreateAppl: TAction;
    acClearAppl: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    biCreateAppl: TTBItem;
    biClearAppl: TTBItem;
    biRefreshAppl: TTBItem;
    acRefreshAppl: TAction;
    TBSeparatorItem3: TTBSeparatorItem;
    biCard: TTBItem;
    acCard: TAction;
    acNotCard: TAction;
    TBItem5: TTBItem;
    siHelp: TSpeedItem;
    NApplDepSZ: TTBItem;
    acCreateApplDepSZ: TAction;
    plCostCard: TPanel;
    LCostCard: TLabel;
    dbCostCard: TDBText;
    taHist: TpFIBDataSet;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    dsrDlist_H: TDataSource;
    dg1: TDBGridEh;
    taSert: TpFIBDataSet;
    dsrSert: TDataSource;
    Panel2: TPanel;
    Label18: TLabel;
    Label17: TLabel;
    taSertID: TFIBIntegerField;
    taSertSNAME: TFIBStringField;
    taSertNOMINAL: TFIBIntegerField;
    taSertSDATE: TFIBDateTimeField;
    LSert: TLabel;
    taSertCOLOR: TFIBIntegerField;
    LSum: TLabel;
    Panel4: TPanel;
    Label19: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    csert: TDBText;
    crsert: TDBText;
    Label23: TLabel;
    sadd: TDBText;
    taSertRET: TFIBIntegerField;
    Label20: TLabel;
    rcsert: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ceUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceUIDButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acFRDateTimeExecute(Sender: TObject);
    procedure acOpenSellExecute(Sender: TObject);
    procedure acPrintCheckExecute(Sender: TObject);
    procedure acPrintDisvExecute(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
    procedure ppPrintPopup(Sender: TObject);
    procedure edUIDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acNewCheckExecute(Sender: TObject);
    procedure acCurCheckExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acClientExecute(Sender: TObject);
    procedure acLastCheckExecute(Sender: TObject);
    procedure siDateEdit (Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure dg2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acViewExecute(Sender: TObject);
    procedure acRetToWhExecute(Sender: TObject);
    procedure acCloseSellExecute(Sender: TObject);
    procedure acSellNewExecute(Sender: TObject);
    procedure acCheckExecute(Sender: TObject);
    procedure acXRepExecute(Sender: TObject);
    procedure acOpenAllExecute(Sender: TObject);
    procedure acSellEndExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acRetToWhUpdate(Sender: TObject);
    procedure edUIDKeyPress(Sender: TObject; var Key: Char);
    procedure acViewUpdate(Sender: TObject);
//    procedure acScanConnectExecute(Sender: TObject);     ***********
    procedure acAddUpdate(Sender: TObject);
    procedure acSellEndUpdate(Sender: TObject);
    procedure acRetUpdate(Sender: TObject);
    procedure acActiveEdUidUpdate(Sender: TObject);
    procedure acActiveEdUidExecute(Sender: TObject);
    procedure acDataWithSellExecute(Sender: TObject);
    procedure acDataWithSellUpdate(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
    procedure acPrintCheckUpdate(Sender: TObject);
    procedure acPrintApplUpdate(Sender: TObject);
    procedure acPrintDisvUpdate(Sender: TObject);
    procedure acPrintTagExecute(Sender: TObject);
    procedure acActiveedArtExecute(Sender: TObject);
    procedure acActiveedArtUpdate(Sender: TObject);
    procedure acActiveedRNExecute(Sender: TObject);
    procedure acActiveedRNUpdate(Sender: TObject);
    procedure acActiveBdExecute(Sender: TObject);
    procedure acActiveBdUpdate(Sender: TObject);
    procedure acActiveEdExecute(Sender: TObject);
    procedure acActiveGridExecute(Sender: TObject);
    procedure acActiveEdUpdate(Sender: TObject);
    procedure acClearApplDepExecute(Sender: TObject);
    procedure acCreateAplldepUpdate(Sender: TObject);
    procedure acCreateAplldepExecute(Sender: TObject);
    procedure acCreateApplExecute(Sender: TObject);
    procedure acClearApplExecute(Sender: TObject);
    procedure acRefreshApplExecute(Sender: TObject);
    procedure acCardExecute(Sender: TObject);
    procedure acNotCardExecute(Sender: TObject);
    procedure acCardUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure acCreateApplDepSZExecute(Sender: TObject);
    procedure acNotCardUpdate(Sender: TObject);
    procedure siCloseSellClick(Sender: TObject);
    procedure dsSellItemDataChange(Sender: TObject; Field: TField);
    procedure FormDestroy(Sender: TObject);
    procedure taSertBeforeOpen(DataSet: TDataSet);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont;
      var Background: TColor; State: TGridDrawState);


  private
    { Private declarations }
    SellState : string[10];
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure CreateFillFields;
    procedure DestroyFields;
    procedure PrintTagSellitem (Sender: TObject);
  public
    { Public declarations }
    procedure ShowSell;
    procedure RetSell(const sell:string);
  end;

var
  fmSellItem: TfmSellItem;
  WMode: string[10];
implementation

uses comdata, Data, Data2, SellEdit, SellUid, FindUID, CheckNo, DataFR, RxsTRuTILS,
  SplashFR, RetSInf, ReportData, ClSell, SetSDate, M207Proc, List3,
  main, NodCard, IDGlobal, Client, NumEmp, ServData, Data3,
  JewConst, dbUtil, FIBQuery, pFIBQuery, MsgDialog, CostCard,
  uUtils, SellList, Hist;

const
  rcInvalidUID = '�� ������ ������ ��. ������';

{$R *.DFM}
//����� ����� � �������������, ���� ���� ������� �� �����������
procedure TfmSellItem.dsSellItemDataChange(Sender: TObject; Field: TField); //��� ��������� ������������
begin

 If (dm.taSellItemTransact_id.IsNull) then
  begin
  dg1.Visible:=false;
  Panel2.Visible:=false;
  taSert.Close;
  end
 else
 begin
 with dm, quTmp do
  begin
    taSert.Open;
    close;
    sql.Text:='select sum(summ) from sert_addpay where ret=1 and transact_id='+dm.taSellItemTransact_id.AsString;
    ExecQuery;
    LSum.Caption:=FloatToStr(Fields[0].Value);
    Transaction.CommitRetaining;
    close;
    sql.Text:='select sum(nominal) from sertificate s, sert_info si where s.id=si.sert_id and ret=1 and si.transact_id='+dm.taSellItemTransact_id.AsString;
    ExecQuery;
    LSert.Caption:=Fields[0].AsString;
    Transaction.CommitRetaining;
    close;
  end;
    dg1.Visible:=true;
    Panel2.Visible:=true;
    taSert.Active:=true;
 end;
end;

procedure TfmSellItem.PrintTagSellitem (Sender: TObject);
var arr : TarrDoc;
begin
 arr := nil;
 if dm.taSellItem.IsEmpty then System.eXit;
 if dm.taSellItemRETSITEMID.IsNull or(dm.taSellItemRETSITEMID.AsInteger = 0)
 then raise Exception.Create('����� ����� �������� ������ �� ������������ �������');
 arr:=gen_arr(dg2, dm.taSellItemRETSITEMID);
 PrintTag(arr, ret_tag, TAction(Sender).Tag);
end;

procedure TfmSellItem.CreateFillFields;
var c1: TColumnEh;
    stsql:string;
    fi: TFIBIntegerField;
    i:integer;
begin
{ TODO : ���������� data ����� � ������ ��� ������ �� ������� }
 {���������� data ����� � ������ ��� ������ �� �������}
 if (dm.WorkMode='ALLSELL') and CenterDep then
  begin
   dm.quTmp.close;
   dm.quTmp.SQL.Text:='select d_depid, ssname from d_dep where d_depid<>-1000 and '+
                      ' isdelete<>1 order by d_depid';
   dm.quTmp.ExecQuery;

   stsql:='';
   dm.taSellItem.Active:=false;
   i:=1;
   while not dm.quTmp.Eof do
   begin
{    stsql1:='coalesce ((select sum(aa.q) from sinv s, appldepart aa  '+
            'where s.refretsinvid=sl.sellid and s.sinvid=aa.sinvid and s.itype=20 and '+
            'aa.artid=sl.artid and depid = '+dm.quTmp.Fields[0].AsString+'), 0)';}
    stsql:=stsql+', cast(get_word(sl.sqappl, '#39+';'+#39', '+inttostr(i)+') as integer)  QART_'+dm.qutmp.fields[0].Asstring;

    fi:=TFIBIntegerField.Create(dm.taSellItem);
    fi.FieldName:='QART_'+dm.qutmp.fields[0].Asstring;
    fi.Name:=dm.taSellItem.Name+fi.FieldName;
    fi.DataSet:=dm.taSellItem;
    fi.Tag:=dm.qutmp.fields[0].AsInteger;
    fi.Size:=0;
    fi.FieldKind:=fkData;

    c1:=dg2.Columns.Add;
    c1.Field:=dm.taSellItem.FieldByName('QART_'+dm.qutmp.fields[0].Asstring);
    c1.Title.Caption:='������ �� �������|'+trim(dm.qutmp.fields[1].Asstring);
    c1.Title.Alignment:=taCenter;
    c1.Title.Font.Color:=clNavy;
    c1.Width:=80;
    //if dm.qutmp.fields[0].AsInteger=dm.taSellListDEPID.AsInteger then  c1.ReadOnly:=false;

    dm.quTmp.Next;
    inc(i);
   end;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
   dm.taSellItem.SelectSQL[1]:=stsql;
//   dm.taSellItem.SelectSQL.Add(fromsql);
  end;
 {****************************************************}
end;

procedure TfmSellItem.DestroyFields;
begin
 if (dm.WorkMode='ALLSELL') and CenterDep then
  begin
   dm.quTmp.close;
   dm.quTmp.SQL.Text:='select d_depid, Sname from d_dep where d_depid<>-1000 and '+
                      {'d_depid<>'+dm.taSellListDEPID.AsString+' and '+}' isdelete<>1';
   dm.quTmp.ExecQuery;
   while not dm.quTmp.Eof do
   begin
    dm.taSellItem.FieldByName('QART_'+dm.qutmp.fields[0].Asstring).Destroy;
    dm.quTmp.Next;
   end;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
   dm.taSellItem.SelectSQL[1]:='';
//   while dm.taSellItem.SelectSQL.Count>2 do dm.taSellItem.SelectSQL.Delete(dm.taSellItem.SelectSQL.Count-2);
  end;
end;

procedure TfmSellItem.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmSellItem.FormCreate(Sender: TObject);
var i: integer;
    c1: TColumnEh;
begin
  dm.dsSellItem.OnDataChange:=dsSellItemDataChange;
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  tb3.WallPaper:=wp;
  dmcom.IsActSellList:=false;
  with dmCom, dm, dm2 do
    begin
      PActAfterSell:=false;
//    tr.StartTransaction;
      Sells:=True;
      if WorkMode<>'ALLSELL' then WorkMode:='SELLCH'
      else
      begin
       dg2.Font.Size:=8;
       dg2.FieldColumns['CLIENTNAME'].Font.Size:=8;
      end;
      OpenDataSets([quClient, taRet]);
    end;

    if dm.WorkMode='ALLSELL' then
    begin
      acOpenSell.Visible:=False;
      acOpenSell.Enabled:=False;

      acCloseSell.Visible:=False;
      acCloseSell.Enabled:=False;

      laDep.Visible:=False;
      siDep.Visible:=False;
      laSell.Visible:=False;


      CreateFillFields;
      ArrangeSB(tb1, 0);
      Caption:='�'+dm.taSellListRN.AsString;
      dm.taSellItem.Active:=True;
      acXRep.Visible:=False;
      acXRep.Enabled:=False;
      acCheck.Visible:=False;
      acCheck.Enabled:=False;
      tb3.Visible:=True;
      acOpenAll.Visible:=false;
      acOpenAll.Enabled:=false;
      SellState:='ALLSELL';
      dg2.FieldColumns['APPL_Q'].Visible:=CenterDep;
      dg2.FieldColumns['SPRICE'].Visible:=CenterDep;
      {���������� ����}
      acView.Enabled:= boolean(dmCom.User.ALLSeLL);
      edRN.Enabled := boolean(dmCom.User.ALLSeLL);
      DBEdit1.Enabled:= boolean(dmCom.User.ALLSeLL);
      DBEdit2.Enabled:= boolean(dmCom.User.ALLSeLL);
      siSetSDate.Enabled:= boolean(dmCom.User.ALLSeLL);
      edUID.Enabled:= boolean(dmCom.User.ALLSeLL);
      dm.SellitemCard:=0;
      dg2.FieldColumns['DIFQ'].Visible:=CenterDep;
     end
  else
    with dm do
      begin
        if (dmcom.User.ALLSeLL=0) then edRN.Enabled:=false;
        ArrangeSB(tb1, 0);

        dmserv.RepeatClick:=false;
        SellState:='NEWCHECK';

        dg2.FieldColumns['APPL_Q'].Visible:=false;
        dbtDep.DataSource:=dsCurSellCheck;
        dbtN.DataSource:=dsCurSellCheck;
        dbtBD.DataSource:=dsCurSellCheck;
        dbtED.DataSource:=dsCurSellCheck;
        dbtCnt.DataSource:=dsCurSellCheck;
        dbtW.DataSource:=dsCurSellCheck;
        dbtCost.DataSource:=dsCurSellCheck;
        dbtRCnt.DataSource:=dsCurSellCheck;
        dbtRW.DataSource:=dsCurSellCheck;
        dbtRCost.DataSource:=dsCurSellCheck;
        dbtCass.DataSource:=dsCurSellCheck;
        edRN.DataSource:=dsCurSellCheck;
        dbCostCard.DataSource:=dsCurSellCheck;        

        acOpenAll.Visible:=true;
        acOpenAll.Visible:=true;
        siOpenAll.DropDownMenu.AutoPopup:= false;
        Caption:='������� �����';
        acXRep.Visible:=dm.UseFReg;
        acXRep.Enabled:=dm.UseFReg;
        acCheck.Visible:=dm.UseFReg;
        acCheck.Enabled:=dm.UseFReg;

        tb3.Visible:=False;
        dg2.FieldColumns['DIFQ'].Visible:=false;        
      end;

  fmSellEdit:=TfmSellEdit.Create(NIL);
  fmSellUID:=TfmSellUID.Create(NIL);
  fmFindUID:=TfmFindUID.Create(NIL);
  fmCheckNo:=TfmCheckNo.Create(NIL);
  fmSplashFR:=TfmSplashFR.Create(NIL);

  with dm, dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c1:=dg2.Columns.Add;
          c1.Field:=taSellItem.FieldByName('RQ_'+slDepDepId[i]);
          c1.Title.Caption:='�������|�� ��������� � ��������|'+slDepSName[i]+' - ���-��';
          c1.Title.Alignment:=taCenter;
          c1.Title.Font.Color:=clNavy;
          c1.Width:=80;

          c1:=dg2.Columns.Add;
          c1.Field:=taSellItem.FieldByName('RW_'+slDepDepId[i]);
          c1.Title.Caption:='�������|�� ��������� � ��������|'+slDepSName[i]+' - ���';
          c1.Title.Alignment:=taCenter;
          c1.Title.Font.Color:=clNavy;
          c1.Width:=80;
         end;

      for i:=0 to slDepDepId.Count-1 do
        begin
          c1:=dg2.Columns.Add;
          c1.Field:=taSellItem.FieldByName('RQA_'+slDepDepId[i]);
          c1.Title.Caption:='�������|�� ������ ���������|'+slDepSName[i]+' - ���-��';
          c1.Title.Alignment:=taCenter;
          c1.Title.Font.Color:=clNavy;
          c1.Width:=80;

          c1:=dg2.Columns.Add;
          c1.Field:=taSellItem.FieldByName('RWA_'+slDepDepId[i]);
          c1.Title.Caption:='�������|�� ������ ���������|'+slDepSName[i]+' - ���';
          c1.Title.Alignment:=taCenter;
          c1.Title.Font.Color:=clNavy;
          c1.Width:=80;
        end;

      if CenterDep and (dm.WorkMode='ALLSELL') then
      for i:=0 to slDepDepId.Count-1 do
        begin
          c1:=dg2.Columns.Add;
          c1.Field:=taSellItem.FieldByName('APPLQ_'+slDepDepId[i]);
          c1.Title.Caption:='������ �� �������|��������������|'+slDepSName[i];
          c1.Title.Alignment:=taCenter;
          c1.Title.Font.Color:=clNavy;
          c1.Visible:=false;
          c1.Width:=80;
        end;
    end;

 if (dm.WorkMode<>'ALLSELL')then
 begin
{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;

  dmServ.ComScan.Connect;
  dmcom.SScanZ:='';    }
 end;

 dmcom.FillTagMenu(mnitTag, PrintTagSellitem, 0);
 dm.SellRet:=false;
 dg2.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dg2', [crpColIndexEh, crpColWidthsEh]);
 dm.dsSellItem.DataSet.First;
 LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmSellItem.FormDestroy(Sender: TObject);
begin
dm.dsSellItem.OnDataChange:=nil;
end;

procedure TfmSellItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSellItem.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSellItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
WMode:='';
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taSellList, taSellItem]);

 {  ���������� �� ���������� ������������ ������� � ���� ������ �� �����.
    ��� ��������� ������ ������������ � �� ����� ������������ �������������! Ann

      if WorkMode='ALLSELL' then
      begin
       with qutmp do
       begin
        sql.Text:='select FlagOpenAct from DefineOpenActSell_(' + inttostr (taSellListSELLID.AsInteger) +')';
        ExecQuery;
        col:= Fields[0].AsInteger;
        close;
        qutmp.Transaction.CommitRetaining;
       end;
       if col>0 then
       begin
        MessageDialog('��������� ��������� ���.', mtWarning, [mbOk], 0);
        dmcom.IsActSellList:=true;
       end;
      end;   }

      if (WorkMode='SELL')or(WorkMode='SELLCH') then
      begin
       if not taCurSellSELLID.IsNull then
       begin
        if taCurSellED.IsNull then
          if MessageDialog('����� �� �������', mtWarning, [mbOK, mbCancel], 0)=mrCancel then Sysutils.Abort;
        fmmain.NumSellID:= taCurSellSELLID.AsInteger
       end;
      end
      else fmmain.NumSellID:= 0;
      CloseDataSets([taSellItem, quEmp, quClient, taRet]);
//    dm.taSellItem.SelectSQL.Delete(dm.taSellItem.SelectSQL.Count-1);
      Sells:=False;
      if (WorkMode='SELL') or (WorkMode='SELLCH') then WorkMode:='';
    end;

  fmSellEdit.Free;
  fmSellUID.Free;
  fmFindUID.Free;
  fmCheckNo.Free;
  fmSplashFR.Free;

//  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;   *******
  dg2.SaveColumnsLayoutIni(GetIniFileName, Name+'_dg2', true);

  DestroyFields;
end;

procedure TfmSellItem.ceUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    fl1:boolean;
    LogOperationID:string;
begin
  case Key of
    VK_RETURN:
        with dm, dm2, dm3 do
          if Shift=[] then
            try

              if ((WorkMode<>'SELLCH')and(dmcom.User.ALLSeLL=0)) then
               raise Exception.Create('��� ���� �� ����������');

              {if (WorkMode='SELLCH')
                 and (taSellItemRET.AsInteger=1) then RepeatClick:=true;}

              if ((edUID.Text='')or(edUID.Text=' ')or(edUID.Text='  '))
                   and (WorkMode='SELLCH') then
              begin
               if ((dmserv.RepeatClick and (taSellItemret.AsInteger=0)) or
                    (taSellItemret.AsInteger=1)) then
               begin
                acSellEndExecute(NIL);
                System.Exit;
               end;
              end;

              {� ���� ��������� ������� � ������� ������}
              if ((dm.WorkMode='SELLCH')) then
              begin
                dm.taSellItem.First;
                fl1:=false;
                while not dm.taSellItem.Eof do
                 begin
                  if (dm.taSellItemRet.AsInteger=1) then fl1:=true;
                  dm.taSellItem.Next;
                 end;
                 if fl1 then raise Exception.Create('��������� � ����� ���� � ��������� ������');
              end;

              dmserv.RepeatClick:=false;
              If SellState='OLDCHECK' then
              begin
               edUID.Text:='';
               raise Exception.Create('������� ��� ����������');
              end;

               SellRet:=False;
               UID := StrToIntDef(edUID.Text, -1);
               if(UID = -1)then
               begin
                 ActiveControl := edUID;
                 raise Exception.Create(rcInvalidUID);
               end;

               LogOperationID:=dm3.insert_operation(sLog_AddSell,LogOprIdForm);

               with quGetUID do
                 begin
                   Active:=False;
                   ParamByName('AUID').AsInteger:=StrToInt(edUID.Text);
                   if WorkMode='ALLSELL' then  ParamByName('ADEPID').AsInteger:=taSellListDepId.AsInteger
                     else   ParamByName('ADEPID').AsInteger:=taCurSellDepId.AsInteger;
                   Active:=True;
                 end;
               edUID.Text:='';
               case quGetUIDRes.AsInteger of
                   1: begin
                        MessageDialog('������� �������', mtInformation, [mbOK], 0);
                        System.exit;
                      end;
                   2: begin
                        MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
                        System.exit;
                      end;
                   3: begin
                        MessageDlg('������� �� �������', mtInformation, [mbOK], 0);
                        System.exit;
                      end;
                 end;

               if quGetUIDSItemId.IsNull then raise Exception.Create('������� �� �������!!!');

               {������� �� ����� ���� ������� ��� ���������� � ����� � ������� sellid ��� ����
                ������� ��� ���������� �����}
               quGetSmallestSell.Close;
               quGetSmallestSell.ParamByName('AUID').AsInteger:=quGetUIDUID.AsInteger;
               quGetSmallestSell.ParamByName('ASELLID').AsInteger:=taSellItemSELLID.AsInteger;
               quGetSmallestSell.ParamByName('ARet').AsInteger:=0;
               quGetSmallestSell.ExecQuery;
               if quGetSmallestSell.Fields[0].AsInteger=1 then
               begin
                quGetSmallestSell.Transaction.CommitRetaining;
                quGetSmallestSell.close;

                dm3.update_operation(LogOperationID);

                raise Exception.Create(trim(quGetSmallestSell.Fields[1].AsString));
               end;
               quGetSmallestSell.Transaction.CommitRetaining;
               quGetSmallestSell.close;


               taSellItem.Append;
               taSellItemArt2Id.AsInteger:=quGetUIDArt2Id.AsInteger;
               taSellItemFullArt.AsString:=quGetUIDFullArt.AsString;
               taSellItemArt2.AsString:=quGetUIDArt2.AsString;
               taSellItemPrice.AsFloat:=quGetUIDPrice.AsFloat;
               taSellItemPrice0.AsFloat:=quGetUIDPrice.AsFloat;

               taSellItemSZ.AsString:=quGetUIDSZ.AsString;
               taSellItemW.AsFloat:=quGetUIDW.AsFloat;
               taSellItemUID.AsString:=quGetUIDUID.AsString;
               taSellItemSItemId.AsInteger:=quGetUIDSItemId.AsInteger;
               taSellItemUnitId.AsInteger:=quGetUIDUnitId.AsInteger;
               taSellItem.Post;
               dmCom.tr.CommitRetaining;

               with qutmp do
               begin
                sql.Text:='update sitem set busytype=2 where sitemid='+inttostr(taSellItemSItemId.AsInteger);
                ExecQuery;
               end;
               dmCom.tr.CommitRetaining;
                ActiveControl:=dg2;
                Application.ProcessMessages;
                ActiveControl:=edUID;
              if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.CurrentRowSelected:=True;

             finally
              quGetUID.Active:=False;
              dm3.update_operation(LogOperationID);
             end;
      VK_DOWN:
        begin
          ActiveControl:=dg2;
          dg2.SelectedField:=dm.taSellItemDISCOUNT;
          if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.CurrentRowSelected:=True;
        end;
      VK_SPACE:
        begin
         dmserv.RepeatClick:=false;
         acSellEndExecute(NIL);
        end;
    end;
end;

procedure TfmSellItem.ceUIDButtonClick(Sender: TObject);
begin
  if edUID.Text='' then System.exit;
  with dm do
  try
    with quFindUID do
      begin
        Active:=False;
        ParamByName('AUID').AsInteger:=StrToInt(edUID.Text);
        ParamByName('ADEPID').AsInteger:=SDepId;
        Active:=True;
      end;

    if fmFindUID.ShowModal=mrOK then
      begin
        taSellItem.First;
        taSellItem.Append;
        taSellItemArt2Id.AsInteger:=quFindUIDArt2Id.AsInteger;
        taSellItemFullArt.AsString:=quFindUIDFullArt.AsString;
        taSellItemArt2.AsString:=quFindUIDArt2.AsString;
        taSellItemPrice.AsFloat:=quFindUIDPrice.AsFloat;
        taSellItemPrice0.AsFloat:=quFindUIDPrice.AsFloat;

        taSellItemSZ.AsString:=quFindUIDSZ.AsString;
        taSellItemW.AsFloat:=quFindUIDW.AsFloat;
        taSellItemUID.AsString:=quFindUIDUID.AsString;
        taSellItemSItemId.AsInteger:=quFindUIDSItemId.AsInteger;
        taSellItemUnitId.AsInteger:=quFindUIDUnitId.AsInteger;
        ActiveControl:=dg2;
      end;
  finally
    quFindUID.Active:=False;
  end;
end;



////////////////////////////////////////////////////////
function GetDefaultPrinter: string;
var
  ResStr: array[0..255] of Char;
begin
  GetProfileString('Windows', 'device', '', ResStr, 255);
  Result := StrPas(ResStr);
end;

procedure SetDefaultPrinter1(NewDefPrinter: string);
var
  ResStr: array[0..255] of Char;
begin
  StrPCopy(ResStr, NewdefPrinter);
  WriteProfileString('windows', 'device', ResStr);
  StrCopy(ResStr, 'windows');
  SendMessage(HWND_BROADCAST, WM_WININICHANGE, 0, Longint(@ResStr));
end;

procedure SetDefaultPrinter2(PrinterName: string);
var
  I: Integer;
  Device: PChar;
  Driver: PChar;
  Port: PChar;
  HdeviceMode: THandle;
  aPrinter: TPrinter;
begin
  Printer.PrinterIndex := -1;
  GetMem(Device, 255);
  GetMem(Driver, 255);
  GetMem(Port, 255);
  aPrinter := TPrinter.Create;
  try
    for I := 0 to Printer.Printers.Count - 1 do
    begin
      if Printer.Printers.Strings[i] = PrinterName then
      begin
        aprinter.PrinterIndex := i;
        aPrinter.getprinter(device, driver, port, HdeviceMode);
        StrCat(Device, ',');
        StrCat(Device, Driver);
        StrCat(Device, Port);
        WriteProfileString('windows', 'device', Device);
        StrCopy(Device, 'windows');
        SendMessage(HWND_BROADCAST, WM_WININICHANGE,
          0, Longint(@Device));
      end;
    end;
  finally
    aPrinter.Free;
  end;
  FreeMem(Device, 255);
  FreeMem(Driver, 255);
  FreeMem(Port, 255);
end;
///////////////////////////////////////////////

procedure TfmSellItem.ShowSell;
begin
  with dm, laSell do
    if WorkMode<>'ALLSELL' then
      begin
        acXrep.Visible  := UseFReg;
        acXrep.Enabled  := UseFReg;
        acCheck.Visible := UseFReg;
        acCheck.Enabled := UseFReg;
{ TODO : �������� ������� ����� ��� ��� //���� }
        if (taCurSellSellId.IsNull) or (not taCurSellED.IsNull) or
           (taCurSellEMP1ID.AsInteger <> dmcom.User.UserId) then
        begin
          Font.Color:=clRed;
          Caption:='����� �� �������';
          acOpenSell.Visible:=True;
          acOpenSell.Enabled:=True;
          acCloseSell.Visible:=False;
          acCloseSell.Enabled:=False;
                    
          ArrangeSB(tb1, 0);
          dm.SellitemCard:=0
        end
        else begin
          Font.Color:=clGreen;
          Caption:='� �����: '+taCurSellN.AsString;
          acOpenSell.Visible:=False;
          acOpenSell.Enabled:=False;
          acCloseSell.Visible:=True;
          acCloseSell.Enabled:=True;
          // � ���������� �������������
          if taCurSellFREG.IsNull then Caption := Caption+' ������'
          else if(taCurSellFREG.AsInteger = 0) then Caption := Caption+ ' (��� ��)'
          else Caption := Caption + ' (� ��)';
          ArrangeSB(tb1, 0);

          with dm, qutmp do
          begin
           close;
           sql.Text:='select first 1 card, sellitemid from sellitem where sellid='+taCurSellSELLID.AsString+
                     ' and checkno=0';
           ExecQuery;
           if Fields[1].IsNull then SellitemCard:=0
           else SellitemCard:=Fields[0].AsInteger;
           close;
           Transaction.CommitRetaining;
          end

        end;
      end
    else
      begin
        laSell.Visible:=False;
        acXrep.Visible:=False;
        acXrep.Enabled:=False;
        acCheck.Visible:=False;
        acCheck.Enabled:=False;
        if taSellListClosed.AsInteger=1 then
          begin
            acOpenSell.Visible:=True;
            acOpenSell.Enabled:=True;            
            acCloseSell.Visible:=False;
            acCloseSell.Enabled:=False;            
            ArrangeSB(tb1, 0);
          end
        else
          begin
            acOpenSell.Visible:=False;
            acOpenSell.Enabled:=False;            
            acCloseSell.Visible:=True;
            acCloseSell.Enabled:=True;
            ArrangeSB(tb1, 0);
          end;
      end;
end;

procedure TfmSellItem.RetSell(const sell:string);
begin
  Application.Minimize;
  with dmCom do
   begin
    with quTmp do
     begin
      Screen.Cursor:=crSQLWait;
      SQL.Text:='SELECT PR FROM RetSell('+sell+')';
      try
       ExecQuery;
      finally
       Screen.Cursor:=crDefault;
       Application.Restore;
      end;
      dm.PActAfterSell:=Fields[0].AsInteger>0;
      Close;
     end;
    tr.CommitRetaining;
   end;
end;

procedure TfmSellItem.FormShow(Sender: TObject);
var i:integer;
    fl:boolean;
begin
  with dm do
    if WorkMode<>'ALLSELL' then
    begin
     i:=0;     fl:=false;
     while (i<dm.pmSellItem.Items.Count)and (not fl) do
     begin
      if ((CenterDep)and(dm.pmSellItem.Items[i].Tag=dmcom.User.DepId)) then fl:=true;
      if ((not CenterDep)and(dm.pmSellItem.Items[i].Tag=SelfDepId)) then fl:=true;
      inc(i);
     end;
     if fl then dm.pmSellItem.Items[i-1].Click
    end
    else ShowSell;
 edUID.Text:='';
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;

end;

procedure TfmSellItem.edArtChange(Sender: TObject);
begin
  dm.taSellItem.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmSellItem.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: edArt.SelectAll;
      VK_DOWN: ActiveControl:=dg2;
    end
end;

procedure TfmSellItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_SPACE :  dmserv.RepeatClick:=false;
  end;
end;

procedure TfmSellItem.acFRDateTimeExecute(Sender: TObject);
var
  dt : TDateTime;
begin
  if dmFR.GetFRDateTime(dt) then
  begin
    //lbFRDateTime.Visible := True;
    //lbFRDateTime.Caption := '����� � ���� ��: '+DateTimeToStr(dt);
  end;
end;

procedure TfmSellItem.acOpenSellExecute(Sender: TObject);
var IsCalc: Integer;
    LogOperationID:string;
begin
  if (dm.SDepId <> dmcom.User.DepId) and (not dmcom.USerAllWh) then
     raise Exception.Create('������������ �� ����� ������� ����� �� ������� �����');

  LogOperationID:=dm3.insert_operation('������� �����',LogOprIdForm);

  with dm do
  begin
    if WorkMode<>'ALLSELL' then
      begin
        acCloseSellExecute(NIL);
        OpenSell;
      end
    else
         with taSellList do
          begin
            if NOT (State in [dsInsert, dsEdit]) then Edit;
            taSellListClosed.AsInteger:=0;
            Post;
            ShowSell;
          end ;
    with quSellElDel do
    begin
        SQL.Text := 'select sellelcalc  from d_Rec ';
        ExecQuery;
        IsCalc := Fields[0].AsInteger;
        close;
    end;

    if IsCalc = 0 then
    begin
      Screen.Cursor:=crSQLWait;
      with quSellElDel do
      begin
        SQL.Text := 'update d_Rec  set sellelcalc = 1';
        ExecQuery;
        Close;
        dmCom.tr.CommitRetaining;
      end ;
      with quToCheckShiftTime do
      begin
        SQL.Text := 'execute procedure RETTIMECHECK(?BD, ?ED, ?CLIENTID1, ?CLIENTID2,?UserID)';
        Prepare;
        ParamByName('ED').AsTimeStamp:= DateTimeToTimeStamp(dmCom.GetServerTime);
        ParamByName('BD').AsTimeStamp:= DateTimeToTimeStamp(IncMonth(dmCom.GetServerTime, -3));
        ParamByName('CLIENTID1').AsInteger:=-MAXINT;
        ParamByName('CLIENTID2').AsInteger:=MAXINT;
        ParamByName('UserID').AsInteger := -1000;
        if not transaction.Active then transaction.StartTransaction;
        ExecQuery;
        close;
        Transaction.CommitRetaining;
      end;
      with quSellElDel do
      begin
        SQL.Text := 'update d_Rec  set sellelcalc = 0';
        ExecQuery;
        Close;
        Transaction.CommitRetaining;
      end;
      Screen.Cursor:=crDefault;
    end;
  end;
  siAdd.Enabled:=true;
  siDel.Enabled:=true;
  siSell.Enabled:=true;
  siRet.Enabled:=true;
  dm.SetCloseInvBtn(siCloseSell, 0);
  dm3.update_operation(LogOperationID);
  dg2.ReadOnly:=false;
end;


procedure TfmSellItem.acPrintCheckExecute(Sender: TObject);
var
  i, j, p, l : integer;
  fl  : boolean;
  arr : TarrDoc;
  BookMark : TBookMark;
begin
  // ������ ��������� ����

  if dm.taSellItem.IsEmpty then System.Exit;
  if (dm.taSellItemRET.AsInteger<>0) then
    raise Exception.Create('�������� ��� ����� ����������� ������ �� ��������� �������');

  //if dmcom.Printtype=0 then
 // begin
   if dm.taSellItemCHECKNO.IsNull then
    raise Exception.Create('����������� �������� ��� ������ ���� ����� ������ �� ������� � ������� ����');
   arr := nil;
   dmReport.ibdsTicketIt1.Close;
   dmReport.ibdsTicketIt1.ParamByName('I_ID').AsInteger:=dm.taSellItemSELLID.AsInteger;
   dmReport.ibdsTicketIt1.ParamByName('I_KIND').AsInteger:=3;
   dmReport.ibdsTicketIt1.ParamByName('I_CHEKNO').AsInteger:=dm.taSellItemCHECKNO.AsInteger;
   dmReport.ibdsTicketIt1.Open;
   //SetDefaultPrinter2('EPSON TM-U950 No cut');
   dmcom.Printtype:=2;
  // Reportdata.PrintDocument(arr,ticket);
  /// exit;
 // end;

  if Assigned(dmReport.Discount) then Finalize(dmReport.Discount);
  BookMark := dm.taSellItem.GetBookmark;
  with dm.taSellItem, dg2 do
    if SelectedRows.Count = 0 then
    begin
       SetLength(dmReport.arr_sell, 1);
       SetLength(dmReport.arr_sell[0], 2);
       dmReport.arr_sell[0][0] := dm.taSellItemCHECKNO.AsVariant;
       dmReport.arr_sell[0][1] := dm.taSellItemSELLITEMID.AsInteger;
       SetLength(arr, 1);
       arr[0] := dm.taSellItemSELLITEMID.AsInteger;
    end
    else begin
      SetLength(dmReport.arr_sell, 1);
      SetLength(dmReport.arr_sell[0], 2);
      GotoBookmark(Pointer(dg2.SelectedRows.Items[0]));
      dmReport.arr_sell[0][0] := dm.taSellItemCHECKNO.AsVariant;
      dmReport.arr_sell[0][1] := dm.taSellItemSELLITEMID.AsInteger;
      SetLength(arr, 1);
      arr[0] := dm.taSellItemSELLITEMID.AsInteger;

      for i:=1 to Pred(dg2.SelectedRows.Count) do
      begin
        p := -1;
        GotoBookmark(Pointer(dg2.SelectedRows.Items[i]));
        for j:=Low(dmReport.arr_sell) to High(dmReport.arr_sell) do
          if(dmReport.arr_sell[j][0] = dm.taSellItemCHECKNO.AsVariant)then
          begin
            p := j;
            break;
          end;

        if(p <> -1)then
        begin
          l := Length(dmReport.arr_sell[p]);
          SetLength(dmReport.arr_sell[p], l+1);
          dmReport.arr_sell[p][l] := dm.taSellItemSELLITEMID.AsInteger;
        end
        else begin
          l := Length(dmReport.arr_sell);
          SetLength(dmReport.arr_sell, l+1);
          SetLength(dmReport.arr_sell[l], 2);
          dmReport.arr_sell[l][0] := dm.taSellItemCHECKNO.AsVariant;
          dmReport.arr_sell[l][1] := dm.taSellItemSELLITEMID.AsInteger;

          SetLength(arr, Length(arr)+1);
          arr[Length(arr)-1] := dm.taSellItemSELLITEMID.AsInteger;
        end;
      end;
    end;

  //   �������� � ��������� ���� (� ����������� �������� �������), ������ �������,
  // ������� �� ������ �� �������
  for i:=Low(dmReport.arr_sell) to High(dmReport.arr_sell) do // ��� ������ ����
    if not VarIsNull(dmReport.arr_sell[i]) then
    begin
      dm.taSellItem.DisableControls;
      try
        dm.taSellItem.First;             // �������� �� �����
        while not dm.taSellItem.Eof do
        begin
          if((dmReport.arr_sell[i][0])=dm.taSellItemCHECKNO.AsVariant)and
            (dm.taSellItemRET.AsInteger <>1) then
          begin
            fl := False; // ��������� ���� �� ����� SellItemId � ��� ����������
            for j:=Low(dmReport.arr_sell[i]) to High(dmReport.arr_sell[i]) do
              if(dmReport.arr_sell[i][j] = dm.taSellItemSELLITEMID.AsVariant)then
              begin
                //fl := True;
                break;

              end;
            if not fl then
            begin
              l := Length(dmReport.arr_sell[i]);
              SetLength(dmReport.arr_sell[i], l+1);
              dmReport.arr_sell[i][l] := dm.taSellItemSELLITEMID.AsInteger;
            end ;
          end;
          dm.taSellItem.Next;
        end;
      finally
        dm.taSellItem.EnableControls;
      end;
    end;

  dm.taSellItem.GotoBookmark(BookMark);

  try
   PrintDocument(arr, ticket);
  finally
    Finalize(arr);
    FreeMem(PChar(arr));
    Finalize(dmReport.arr_sell);
    FreeMem(PChar(dmReport.arr_sell));
    dmcom.Printtype:=0;
  end;
end;

procedure TfmSellItem.acPrintDisvExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  if dm.taSellItemRETSITEMID.IsNull or(dm.taSellItemRETSITEMID.AsInteger = 0)
  then raise Exception.Create('��������� ����� ����� �������� ������ �� ������������ �������');
  arr:=gen_arr(dg2, dm.taSellItemRETSITEMID);
  if dmcom.Printtype=0 then SetDefaultPrinter2('EPSON TM-U950(Slip)');
  try
   PrintDocument(arr, disv);
  finally
    Finalize(arr);
    Finalize(dmReport.arr_sell);
  end;
end;

procedure TfmSellItem.acPrintApplExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  if(dm.taSellItemRETSITEMID.IsNull)or(dm.taSellItemRETSITEMID.AsInteger = 0)
  then raise Exception.Create('��������� ����� �������� ������ �� ������������ �������');
  arr:=gen_arr(dg2, dm.taSellItemRETSITEMID);

  if dmcom.Printtype=0 then SetDefaultPrinter2('EPSON TM-U950(Slip)');
  try
   PrintDocument(arr, appl);
  finally
    Finalize(arr);
    Finalize(dmReport.arr_sell);    
  end;
end;

procedure TfmSellItem.ppPrintPopup(Sender: TObject);
begin
  if dm.taSellItem.IsEmpty then SysUtils.Abort;
  if(dm.taSellItemRET.AsInteger = 1)then
  begin
    acPrintCheck.Visible := False;
    mnitTag.Visible := true;
    acPrintAppl.Visible := True;
    acPrintDisv.Visible := True;
    mnitSep2.Visible := True;
  end
  else begin
    acPrintCheck.Visible := True;
    mnitTag.Visible := False;
    acPrintAppl.Visible := False;
    acPrintDisv.Visible := False;
    mnitSep2.Visible := True;
  end;
end;

procedure TfmSellItem.edUIDKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if edUID.Text=' ' then edUID.Text:='';
end;

procedure TfmSellItem.acNewCheckExecute(Sender: TObject);
begin
 if dm.WorkMode <> 'SELL' then SySUtils.Abort;

  with dmCom, dm, dm2 do
   if WorkMode = 'SELL' then
    begin
     PostDataSets([taSellList, taSellItem]);
     D_CHECK0:=0;
     CloseDataSets([taSellItem, quEmp, quClient, taRet]);

     SellState := 'NEWCHECK';
     WorkMode := 'SELLCH';
//     dm.taSellItem.SelectSQL[dm.taSellItem.SelectSQL.Count-1]:='where checkno = '+inttostr(dm.D_CHECK0);

     OpenDataSets([quEmp, quClient, taRet, taSellItem, quCurSellCheck]);
     dg2.Font.Size:=14;
     dg2.FieldColumns['CLIENTNAME'].Font.Size:=14;

     siOpenAll.ImageIndex := 22;
     siOpenAll.BtnCaption := '��� �����';
     siOpenAll.DropDownMenu.AutoPopup:= false;

     dbtDep.DataSource:=dsCurSellCheck;
     dbtN.DataSource:=dsCurSellCheck;
     dbtBD.DataSource:=dsCurSellCheck;
     dbtED.DataSource:=dsCurSellCheck;
     dbtCnt.DataSource:=dsCurSellCheck;
     dbtW.DataSource:=dsCurSellCheck;
     dbtCost.DataSource:=dsCurSellCheck;
     dbtRCnt.DataSource:=dsCurSellCheck;
     dbtRW.DataSource:=dsCurSellCheck;
     dbtRCost.DataSource:=dsCurSellCheck;
     dbtCass.DataSource:=dsCurSellCheck;
     edRN.DataSource:=dsCurSellCheck;
     dbCostCard.DataSource:=dsCurSellCheck;

      {���������� ����}
//      acAdd.Enabled := true;
//      acDel.Enabled := true;
      acView.Enabled:= true;
//      acSellEnd.Enabled:= true;
//      acRet.Enabled := true;
      edRN.Enabled := boolean(dmCom.User.ALLSeLL);;
      edUID.Enabled := true;

      if taCurSellSELLID.IsNull then SellitemCard:=0
      else with dm, qutmp do
      begin
       close;
       sql.Text:='select first 1 card, sellitemid from sellitem where sellid='+taCurSellSELLID.AsString+
                 ' and checkno=0';
       ExecQuery;
       if Fields[1].IsNull then SellitemCard:=0
       else SellitemCard:=Fields[0].AsInteger;
       close;
       Transaction.CommitRetaining;
      end
    end;
end;

procedure TfmSellItem.acCurCheckExecute(Sender: TObject);
begin
 if dm.WorkMode <> 'SELL' then SysUtils.Abort;

   with dmCom, dm, dm2 do
    if WorkMode = 'SELL' then
     begin
      PostDataSets([taSellList, taSellItem]);
      D_CHECK0 := taSellItemCHECKNO.AsInteger;
      CloseDataSets([taSellItem, quEmp, quClient, taRet]);

      dg2.Font.Size:=14;
      dg2.FieldColumns['CLIENTNAME'].Font.Size:=14;

      SellState := 'OLDCHECK';
      WorkMode := 'SELLCH';
//      dm.taSellItem.SelectSQL[dm.taSellItem.SelectSQL.Count-1]:='where checkno = '+inttostr(dm.D_CHECK0);
            
      OpenDataSets([quEmp, quClient, taRet, taSellItem, quCurSellCheck]);
      siOpenAll.ImageIndex := 22;
      siOpenAll.BtnCaption := '��� �����';
      siOpenAll.DropDownMenu.AutoPopup:= false;

      dbtDep.DataSource:=dsCurSellCheck;
      dbtN.DataSource:=dsCurSellCheck;
      dbtBD.DataSource:=dsCurSellCheck;
      dbtED.DataSource:=dsCurSellCheck;
      dbtCnt.DataSource:=dsCurSellCheck;
      dbtW.DataSource:=dsCurSellCheck;
      dbtCost.DataSource:=dsCurSellCheck;
      dbtRCnt.DataSource:=dsCurSellCheck;
      dbtRW.DataSource:=dsCurSellCheck;
      dbtRCost.DataSource:=dsCurSellCheck;
      dbtCass.DataSource:=dsCurSellCheck;
      edRN.DataSource:=dsCurSellCheck;
      dbCostCard.DataSource:=dsCurSellCheck;
      
      {���������� ����}
//      acAdd.Enabled := boolean(dmCom.User.ALLSeLL);
//      acDel.Enabled := boolean(dmCom.User.ALLSeLL);
      acView.Enabled:= boolean(dmCom.User.ALLSeLL);
//      acSellEnd.Enabled:= boolean(dmCom.User.ALLSeLL);
//      acRet.Enabled := boolean(dmCom.User.ALLSeLL);
      edRN.Enabled := boolean(dmCom.User.ALLSeLL);
      edUID.Enabled := boolean(dmCom.User.ALLSeLL);
      dm.SellitemCard:=0;
    end;
end;

procedure TfmSellItem.FormActivate(Sender: TObject);
begin
  {dg1.Visible:=false;
  Panel2.Visible:=false;}
  
  WMode:='SELLITEM';
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
  if dm.WorkMode='ALLSELL' then fmSellItem.HelpContext:=100233
  else fmSellItem.HelpContext:=100231;
  If (dm.taSellListCLOSED.AsInteger=1) or (dm.taSellListCLOSED.IsNull) then
  begin
  dm.SetCloseInvBtn(siCloseSell, 1);
  siAdd.Enabled:=false;
  siDel.Enabled:=false;
  siSell.Enabled:=false;
  siRet.Enabled:=false;
  dg2.ReadOnly:=true;
     end
  else
  begin
  dm.SetCloseInvBtn(siCloseSell, 0);
  siAdd.Enabled:=true;
  siDel.Enabled:=true;
  siSell.Enabled:=true;
  siRet.Enabled:=true;
  dg2.ReadOnly:=false;
     end;

end;

procedure TfmSellItem.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 18 then
    edUID.SetFocus;

end;

procedure TfmSellItem.acClientExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('���������� �����������',LogOprIdForm);
  with dmCom do
  begin
   if not tr.Active then tr.StartTransaction
   else tr.CommitRetaining;
   ShowAndFreeForm(TfmClient, Self, TForm(fmClient), True, False);
   tr.CommitRetaining;
  end;
  ReOpenDataSets([dm2.quClient]);
  dm3.update_operation(LogOperationID);
end;

procedure TfmSellItem.acLastCheckExecute(Sender: TObject);
begin
 if dm.WorkMode='SELLCH' then
 with dm, qutmp do
 begin
  close;
  sql.Text:='select max(checkno) from sellitem where sellid = '+
             taCurSellSELLID.AsString;
  ExecQuery;
  if Fields[0].AsInteger<>0 then
  begin
   dm.D_CHECK0:=Fields[0].AsInteger;
   ReOpenDataSets([taSellItem]);

   if  dmcom.Printtype=0  then
   begin
    dmReport.ibdsTicketIt1.Close;
    dmReport.ibdsTicketIt1.ParamByName('I_ID').AsInteger:=dm.taSellItemSELLID.AsInteger;
    dmReport.ibdsTicketIt1.ParamByName('I_KIND').AsInteger:=3;
    dmReport.ibdsTicketIt1.ParamByName('I_CHEKNO').AsInteger:=dm.taSellItemCHECKNO.AsInteger;
    dmReport.ibdsTicketIt1.Open;

    //zrCheck.Print;
   end
   else
   begin
    tasellitem.First;
    while not tasellitem.Eof do
    begin
     if taSellItemRET.AsInteger=0 then dg2.SelectedRows.CurrentRowSelected:=true;
     taSellItem.Next;
    end;
    acPrintCheck.Execute;
   end;
   dm.D_CHECK0:=0;
   ReOpenDataSets([taSellItem]);
  end;
  close;
 end;
end;

procedure TfmSellItem.siDateEdit(Sender: TObject);
var d: TDateTime;
begin
 if (dmcom.User.ALLSeLL=0) then
   raise Exception.Create('��� ���� �� ��������� ����');

  fmSetSDate:=TfmSetSDate.Create(Application);
  try

    if fmSetSDate.ShowModal=mrOK then
      with dm do
        begin
          d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
          with quTmp do
            begin
              close;
              sql.Text := 'select str from exime_date_insell ( '+taSellListSellId.AsString+', '''
                          +DateTimeToStr(d)+''',' + IntToStr(TPopupMenu(Sender).Tag) + ' )';
              ExecQuery;
              if trim(Fields[0].AsString)<>'' then
              Application.MessageBox(pchar(Fields[0].AsString),'��������!!!',0);
              close;
            end;
          dmCom.tr.CommitRetaining;
          ReOpenDataSets([taSellItem]);
        end;
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmSellItem.acAddExecute(Sender: TObject);
var
  SaveFocus : TWinControl;
  Flag: Integer;
  fl1:boolean;
  LogOperationID:string;
begin
  with dm do
    begin
      if ((WorkMode<>'SELLCH')and(dmcom.User.ALLSeLL=0)) then
      begin
       if (not sellRet) then
          raise Exception.Create('��� ���� �� ���������� �������')
       else
          raise Exception.Create('��� ���� �� ���������� ��������');
      end;
    {� ���� ��������� ������� � ������� ������}
      if ((dm.WorkMode='SELLCH')) then
      begin
       dm.taSellItem.First;
       fl1:=false;
       while not dm.taSellItem.Eof do
         begin
          if (((dm.taSellItemRet.AsInteger=0) and (TComponent(Sender).Tag=2)) or
              ((dm.taSellItemRet.AsInteger=1) and (TComponent(Sender).Tag<>2))) then fl1:=true;
            dm.taSellItem.Next;
         end;
         if (fl1 and (TComponent(Sender).Tag=2)) then raise Exception.Create('���������� � ����� ���� � �������� ������');
         if (fl1 and (TComponent(Sender).Tag<>2)) then raise Exception.Create('��������� � ����� ���� � ��������� ������');
       end;

      // basile
      // �������� ���. ����� ������� � ����� �������� �����
      if(dmCom.GetServerTime < dm.taSellListBD.AsDateTime)then
      begin
        raise Exception.Create('����� ������� '+DateTimeToStr(dm.taSellListBD.AsDateTime)+#13#10+
           '������� ���� � �����: '+DateTimeToStr(dm.taSellListBD.AsDateTime)+'.'+#13#10+
           '����������� ����������');
      end;

      If SellState='OLDCHECK' then raise Exception.Create('������� ��� ����������');


      SellElUser := -1000;
      // �������� ����� �� �������� ������� SellElem  - ������� ��������� �������
      with quSellElDel do
      begin
        SQL.Text := 'select SellElCalc from d_rec' ;
        ExecQuery;
        Flag:= Fields[0].AsInteger;
        close;
        if Flag = 1 then
            raise Exception.Create('�������� ��������� ������� ������ ������������� �� ��������');

      end;
      SellRet:=TComponent(Sender).Tag=2;

      if (not SellRet) then
       LogOperationID:=dm3.insert_operation(sLog_SelectSell,LogOprIdForm)
      else
       LogOperationID:=dm3.insert_operation(sLog_Ret,LogOprIdForm);
      taSellItem.Last;
      { TODO : ���������� � ����� �� ������ }
      taSellItem.Append;
      SaveFocus := ActiveControl;
      if WorkMode<>'SELLCH' then fmSellEdit.ShowModal
      else
       if fmSellEdit.ShowModal = mrOK then
       begin
        taSellItem.Edit;
        taSellItemCHECKNO.AsInteger:=0;
        taSellItem.Post;
        dmcom.tr.CommitRetaining;
{        if not dmcom.ShowNumEmp
        then RepeatClick:=true
        else
        begin
         if (ShowAndFreeForm(TfmNumEmp, Self, TForm(fmNumEmp), True, False)= mrok)
         then RepeatClick:=true
         else RepeatClick:=false;
        end;} 
       end
       else
       dm3.update_operation(LogOperationID);
       ReOpenDataSets([taSellItem]);

      ActiveControl:=SaveFocus;
      if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.CurrentRowSelected:=True;
    end;
end;

procedure TfmSellItem.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  with dm do
  begin
   if ((WorkMode<>'SELLCH')and(dmcom.User.ALLSeLL=0)) then
               raise Exception.Create('��� ���� �� ��������');

    If SellState='OLDCHECK' then raise Exception.Create('������� ��� ����������');

    LogOperationID:=dm3.insert_operation('������� �������',LogOprIdForm);
    taSellItem.Delete;
    dmCom.tr.CommitRetaining;
    dm3.update_operation(LogOperationID);
  end;
end;

procedure TfmSellItem.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
if tasert.Fields.FieldByName('RET').Value=-1 then  Background:=clAqua;
if column.Field.FieldName='SNAME' then Background:=taSertColor.AsInteger;


end;

procedure TfmSellItem.dg2GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var fldName: string;
    fldColour: TColor;
    ADecimalSeparator: Char;
begin
{ TODO : ����������� ������ }

  if (Column.Field.Tag>0) then
    begin
      fldName := Column.Field.FieldName;
      fldName := copy(fldName,1,3);
      if (fldName = 'RWA') or (fldName = 'RQA') then
      begin
        fldColour :=ColorToRGB(dm2.GetDepColor(Column.Field.Tag));
        Background:= RGB(GetRValue(fldColour)-20,GetGValue(fldColour)-40,GetBValue(fldColour));
      end
      else
       Background:=dm2.GetDepColor(Column.Field.Tag);
      if (fldName = 'QAR') and (Column.Field.Tag=dm.taSellListDEPID.asInteger) then
        Background:=clBtnFace else
        if (not dm.tasellItemTransact_Id.IsNull) then Background:= $FFCCFF;
      end
  else
    begin
      if (Column.Field.FieldName='APPL_Q') and (dm.WorkMode='ALLSELL') then
       Background:=clWhite
      else
      begin
       if dm.taSellItemRet.AsInteger=0 then
        begin
          if (Column.Field.FieldName='PRICE') and (dm.WorkMode='ALLSELL') then
          begin
           if Abs(dm.taSellItemPrice0.AsFloat-dm.taSellItemPrice.AsFloat)>1e-6 then
           begin
             if dm.taSellItemDiscount.AsFloat=0 then Background:=clRed
             else with dm, qutmp do
             begin
              close;
              ADecimalSeparator := DecimalSeparator;
              DecimalSeparator := '.';
              sql.Text:='select d_discountid from d_discount where '+
                        'showsell=1 and discount='+dm.taSellItemDiscount.AsString;
             // ShowMessage(sql.Text);
              ExecQuery;
              if Fields[0].IsNull then Background:=clRed else BackGround:=dmCom.clPink;
              close;
              Transaction.CommitRetaining;
              DecimalSeparator := ADecimalSeparator;
             end
           end
           else
             if (not dm.tasellItemTransact_Id.IsNull) then Background:=$AACCFF else
             Background:=clBtnFace;
           if (dm.taSellItemDiscount.AsFloat<>0) and (dm.taSellItemCLIENTID.AsInteger=-1) then
               Background:=clRed;
          end
         else
          if ((dm.taSellItemCheckNo.IsNull)or(dm.taSellItemCheckNo.AsInteger=0)) then
           Background:=clYellow
          else
          begin
           if ((Column.Field.FieldName='SZ') or (Column.Field.FieldName='W') or (Column.Field.FieldName='UID')) then
           begin
            if dm.taSellItemSItemId.IsNull then Background:=clRed
            else   if (not dm.tasellItemTransact_Id.IsNull) then Background:=$AACCFF
       else Background:=clBtnFace
           end
           else
             if (not dm.tasellItemTransact_Id.IsNull) then Background:=$AACCFF
       else
           Background:=clBtnFace;
          end
        end
        else
         begin
           if (dm.taSellItemRETSITEMID.IsNull) then
               Background:=clFuchsia
           else Background:=clAqua;
         end;
       if Column.Field.FieldName='UID' then
         if dm.taSellItemUID.IsNull then Background:=clRed
         else if dm.taSellItemCARD.AsInteger<>0 then Background:=clBlue
              else Background:=dmCom.clCream;

       if Column.Field.FieldName='DIFQ' then
         if dm.taSellItemDIFQ.AsInteger<>0 then Background:=clRed
         else
           if (not dm.tasellItemTransact_Id.IsNull) then Background:=$AACCFF
           else Background:=clBtnFace;

     end

    end;

//  end
end;

procedure TfmSellItem.dg2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var  CharKey: char;
begin
   case Key of
    VK_RETURN : begin
      PostDataSets([dm.taSellItem]);

     if (((dm.taSellItemRet.AsInteger=1) or
          ((dm.taSellItemRet.AsInteger=0)and dmserv.RepeatClick))) then
      begin
       acSellEndExecute(NIL);
       System.Exit;
      end;
//      ActiveControl:=edUID;
    end;
    VK_SPACE: begin
       if ((dm.taSellItem.State=dsInsert) or (dm.taSellItem.State=dsEdit)) then
        PostDataSets([dm.taSellItem])
       else
       if (dm.taSellItemRet.AsInteger=0)then
       begin
        acSellEndExecute(NIL);
        System.exit;
       end;
    end;
  end;
  CharKey :=Chr(key);


  dm.IsSellEmpty:=(dm.WorkMode ='SELLCH') and
   ((IsNumeric(CharKey)) or ((CharKey>='!') and (CharKey<='�')));
end;

procedure TfmSellItem.acViewExecute(Sender: TObject);
var j,i:integer;
    LogOperationID:string;
begin
 if ((dmcom.User.ALLSeLL=0)and(dm.WorkMode<>'SELLCH')) then
   raise Exception.Create('��� ���� �� ��������');

  if ((dm.WorkMode='SELL')and (dm.taCurSellFREG.AsInteger=0)) or
     ((dm.WorkMode='ALLSELL') and (dm.taSellListFREG.AsInteger=0)) then
  BEGIN
   if dm.taSellItem.IsEmpty or dm.taSellItemSELLITEMID.IsNull then System.Exit;
      dm.SellRet := dm.taSellItemRet.AsInteger=1;

   LogOperationID:=dm3.insert_operation('�������� ���������� � �������',LogOprIdForm);
   fmSellEdit.ShowModal;

   if dm.WorkMode = 'SELLCH' then
   with dm, qutmp  do
   begin
    close;
    sql.text:='update sellitem set clientid = '+taSellItemCLIENTID.AsString+#13#10+
              ' where checkno = '+taSellItemCHECKNO.AsString;
    ExecQuery;
    dmcom.tr.CommitRetaining;
    close;
    j:=taSellItemSELLITEMID.AsInteger;
    i:=dg2.SelectedIndex;
    ReOpenDataSets([taSellItem]);
    taSellItem.Locate('SELLITEMID',j,[]);
    dg2.SelectedIndex:=i;
   end;

   dm3.update_operation(LogOperationID);
  END;
end;

procedure TfmSellItem.acRetToWhExecute(Sender: TObject);
var i, SR, SinvId : integer;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������� ������� �� �����',LogOprIdForm);

  if dg2.SelectedRows.Count = 0 then SR := 0 else SR := dg2.SelectedRows.Count - 1;
  SInvId := -777;
  for i:=0 to SR do
    with dm, dm2 do
    begin
      if dg2.SelectedRows.Count > 1 then dg2.DataSource.DataSet.GotoBookmark(Pointer(dg2.SelectedRows.Items[i]));
      if(taSellItemRet.AsInteger = 1)then
      begin
        if NOT taSellItemRetSItemId.IsNull then continue; // '������ ������� ��� ����������
        fmRetSInf := TfmRetSInf.Create(nil);
        try
          if taSellItemSItemId.IsNull then
          begin
            if(fmRetSInf.ShowModal = mrCancel)then continue;
            with quInsClRet1, Params do
            begin
              ByName['SDATE'].AsDate:=fmRetSInf.deSDate.Date;
              ByName['NDATE'].AsDate:=fmRetSInf.deNDate.Date;
              ByName['SN'].AsString:=fmRetSInf.edSN.Text;
              ByName['SSF'].AsString:=fmRetSInf.edSSF.Text;
              ByName['SUPID'].AsInteger:=fmRetSInf.lcSup.KeyValue;
              ByName['SPRICE'].AsFloat:=fmRetSInf.cePrice.Value;
              ByName['NDSID'].AsInteger:=fmRetSInf.lcNDS.KeyValue;
            end;
          end;
          with quInsClRet1, Params do
          begin
            if(SInvId = -777)then ByName['SINVID'].IsNull := True
            else ByName['SINVID'].AsInteger := SInvId;
            if (i=SR) then ByName['ISEND'].AsInteger := 1 else ByName['ISEND'].AsInteger := 0;
            ByName['USERID'].AsInteger := dmcom.UserId;
            ByName['CLIENTID'].AsInteger := taSellItemCLIENTID.AsInteger;
            ByName['SELLITEMID'].AsInteger:=taSellItemSellItemId.AsInteger;
            ByName['RETID'].AsString := taSellItemD_RETID.AsString;
            ExecQuery;
            SInvId := Fields[0].AsInteger;
          end;

          dmCom.tr.CommitRetaining;
          taSellItem.Refresh;
        finally
          fmRetsInf.Free;
          ActiveControl:=edUID;
        end;
      end;
    end;
  dm3.update_operation(LogOperationID)
end;

procedure TfmSellItem.acCloseSellExecute(Sender: TObject);
var fl:boolean;
    LogOperationID:string;
begin
if (SellList.WMode='SELLLIST') then
begin
fmSellItem.taHist.Active:=true;
  fmSellItem.taHist.Open;
  with fmSellItem.taHist do
  begin
  fmSellItem.taHist.Append;
  fmSellItem.taHistDOCID.AsInteger := dm.taSellListSellid.AsInteger;
  If (Hist.log='') then fmSellItem.taHistFIO.Value := dmCom.User.Alias else
  fmSellItem.taHistFIO.Value :=Hist.log;

  fmSellItem.taHistSTATUS.Value := '�������';
  fmSellItem.taHist.Post;
  fmSellItem.taHist.Transaction.CommitRetaining;
  end;
  fmSellItem.taHist.Close;
  fmSellItem.taHist.Active:=false;
end;
  with dm do
    if WorkMode<>'ALLSELL' then
      begin
        if NOT taCurSellSellId.IsNull and taCurSellED.IsNull then
          begin
            fl:=false;
            if (taCurSellFREG.AsInteger = 1) and (not dmcom.FReg_)
            then raise Exception.Create('������������ �� ����� ������� �����. �� ���������� ������� ����������� ������������.')
            else if (taCurSellFREG.AsInteger = 0) and (dmcom.FReg_)
                 then raise Exception.Create('������������ �� ����� ������� �����. ���������� ������� ����������� ������������.')
                 else if (taCurSellFREG.AsInteger = 1) and (dmcom.FReg_) and (not dmfr.FRReady) then
                  begin
                   while (MessageDialog('���������� ����������� �� �����. ����� �� ����� ���� �������. ���������?', mtInformation, [mbOK,mbCancel],0) = mrOk)and(not fl) do
                   if dmfr.FrReady then fl:=true;
                   if not fl then raise Exception.Create('������������ �� ����� ������� �����. ���������� ����������� �� �����.')
                  end;

            LogOperationID:=dm3.insert_operation('������� �����',LogOprIdForm);
            if ShowAndFreeForm(TfmClSell, Self, TForm(fmClSell), True, False)=mrOK then
             begin

               if (taCurSellFREG.AsInteger = 1) then UseFreg := true
                                                else UseFreg :=false;

               CloseSell;
             end;
            dm3.update_operation(LogOperationID)
          end;
      end
    else
    begin
      LogOperationID:=dm3.insert_operation('������� �����',LogOprIdForm);
      if ShowAndFreeForm(TfmClSell, Self, TForm(fmClSell), True, False)=mrOK then
        with taSellList do
          begin
            if NOT (State in [dsInsert, dsEdit]) then Edit;
            taSellListClosed.AsInteger:=1;
            Post;
            ShowSell;
          end;
       dm3.update_operation(LogOperationID);
        siAdd.Enabled:=false;
        siDel.Enabled:=false;
        siSell.Enabled:=false;
        siRet.Enabled:=false;
       dm.SetCloseInvBtn(siCloseSell, 1);
    end;
    dg2.ReadOnly:=true;
end;

procedure TfmSellItem.acSellNewExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('������� ����� �������',LogOprIdForm);
 if acOpenSell.Enabled then acOpenSellExecute(nil)
 else  acCloseSellExecute(nil);
 dm3.update_operation(LogOperationID);
end;

procedure TfmSellItem.acCheckExecute(Sender: TObject);
var DetailLine: Byte;
    Check: TStrings;
    i, j: Integer;
    DetailStr, S: String;
    Sum: Extended;
begin
 DetailLine := 0;

 with dm do
  begin
   if taSellItemSELLITEMID.IsNull then System.Exit;
   if taSellItemRet.AsInteger=1 then System.exit;
   if taSellItemCheckNo.IsNull then System.exit;
   if (WorkMode<>'SELL')or(WorkMode<>'SELLCH') then System.exit;
  end;

//�������� ���
 with dmCom, taRec do
  begin
   Check:=TStringList.Create;
   Active:=True;
   Check.Text:=taRecACheck.AsString;
   Active:=False;
  end;
 Check.Text:=ReplaceStr(Check.Text, '<n>', dm.taSellItemCheckNo.AsString);
 if dg2.SelectedRows.Count > 0 then j:=dg2.SelectedRows.Count
  else j:=1;

 DetailStr:='';

 for i:=0 to Check.Count-1 do
  if Copy(Check.Strings[i], 1, 1) = '|' then
   begin
    Check.Strings[i]:=Copy(Check.Strings[i], 2, Length(Check.Strings[i])-1);
    if DetailStr = '' then
     begin
      DetailStr:=Check.Strings[i];
      DetailLine:=i;
     end else DetailStr:=DetailStr + #13#10 + Check.Strings[i];
   end else if DetailStr <> '' then break;

 Sum:=0;
 with dm do
  begin
   for i:=0 to j-1 do
    begin
     if dg2.SelectedRows.Count > 0 then taSellItem.Bookmark:=dg2.SelectedRows[i];
     Sum:=Sum + taSellItemCost.AsFloat;
     Check.Text:=ReplaceStr(Check.Text, '<id>', taSellItemUID.AsString);
     Check.Text:=ReplaceStr(Check.Text, '<name>', taSellItemName.AsString);
     Check.Text:=ReplaceStr(Check.Text, '<art>', taSellItemFullArt.AsString);
     Check.Text:=ReplaceStr(Check.Text, '<art2>', taSellItemArt2.AsString);
     Check.Text:=ReplaceStr(Check.Text, '<mat>', taSellItemMat.AsString);
     Check.Text:=ReplaceStr(Check.Text, '<size>', taSellItemSZ.AsString);
     with dmFR, taIns do
      begin
       Active:=True;
       S:=taInsN.AsString;
       Next;
       while not eof do
        begin
         S:=S + #13#10 + taInsN.AsString;
         Next;
        end;
       Active:=False;
      end;
     Check.Text:=ReplaceStr(Check.Text, '<ins>', S);
     Check.Text:=ReplaceStr(Check.Text, '<w>', Format('%0.2f', [taSellItemW.AsFloat]));
     S:=Format('%0.2f', [taSellItemPrice0.AsFloat]);
     if taSellItemUnitId.AsInteger = 1 then S:=S + ' �� 1 ��.';
     Check.Text:=ReplaceStr(Check.Text, '<cost>', S);
     if taSellItemUnitId.AsInteger = 0 then S:=Format('%0.2f', [taSellItemPrice0.AsFloat])
      else S:=Format('%0.2f', [taSellItemPrice0.AsFloat*taSellItemW.AsFloat]);
     Check.Text:=ReplaceStr(Check.Text, '<sum>', ReplaceStr(S, ',', '.'));
     Check.Text:=ReplaceStr(Check.Text, '<disc>', Format('%0.2f', [taSellItemDisc.AsFloat]) + '�.(' + Format('%0.2f', [taSellItemDiscount.AsFloat]) + '%)');
     Check.Text:=ReplaceStr(Check.Text, '<sum_d>', Format('%0.2f', [taSellItemCost.AsFloat]));
     if i<>j-1 then Check.Insert(DetailLine, DetailStr);
    end;
   Check.Text:=ReplaceStr(Check.Text, '<itog>', Format('%0.2f', [Sum]));
   Check.Text:=ReplaceStr(Check.Text, '<date>', DateToStr(taSellItemADate.AsDateTime));
  end;

 with dmFR do
  begin
   if not com.Connected then com.Connect;
   //��������� � ������������ �����
   FillChar(OutBuf, SizeOf(OutBuf), 0);
   SendPacket($36, 10, OutBuf);
   fmSplashFR.Close;
   if (Status > 0) then
    begin
     MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
     Enabled:=True;
     System.exit;
    end;
   S:=StrToOem(Check.Text);
   for i:=1 to Length(S)+1 do
    begin
     SendNext:=False;
     Err:=False;
     if i<=Length(S) then com.SendString(S[i])
      else for j:=1 to 7 do
            begin
             com.SendString(#13);
             while not SendNext and not Err do
              Application.ProcessMessages;
             SendNext:=False;
             com.SendString(#10);
            end;
     while not SendNext and not Err do
      Application.ProcessMessages;
    end;
   Answer:=False;
   com.SendString(#27#27);
   while not Answer do
    Application.ProcessMessages;
  end;
 Check.Free;
end;

procedure TfmSellItem.acXRepExecute(Sender: TObject);
begin
 if dm.UseFReg then
   with dmFR do
     begin
       fmSplashFR.Show;
       if not com.Connected then com.Connect;
     //��������� �����
       FillChar(OutBuf, SizeOf(OutBuf), 0);
       OutBuf[8]:=$39;
       SendPacket($35, 12, OutBuf);
       fmSplashFR.Close;
       if (Status > 0) and (Status <> 12) then
        begin
        MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
       System.exit;
      end;
     com.Disconnect;
    end;
end;

procedure TfmSellItem.acOpenAllExecute(Sender: TObject);
begin
 if dm.WorkMode<>'SELLCH' then SysUtils.Abort;

  with dmCom, dm, dm2 do
   if WorkMode = 'SELLCH' then
      begin
       WorkMode := 'SELL';
       PostDataSets([taSellList]);
       CloseDataSets([quEmp, taRet, taSellItem, quCurSellCheck]);

//       dm.taSellItem.SelectSQL[dm.taSellItem.SelectSQL.Count-1]:='';

       OpenDataSets([quEmp, taRet, taSellItem]);

       dg2.Font.Size:=8;
       dg2.FieldColumns['CLIENTNAME'].Font.Size:=8;

       {�������� �� ���� �������}
       taSellItem.First;
       taCurSell.Refresh;

       siOpenAll.ImageIndex := 71;
       siOpenAll.BtnCaption := '���';
       siOpenAll.DropDownMenu.AutoPopup:= true;

       dbtDep.DataSource:=dsCurSell;
       dbtN.DataSource:=dsCurSell;
       dbtBD.DataSource:=dsCurSell;
       dbtED.DataSource:=dsCurSell;
       dbtCnt.DataSource:=dsCurSell;
       dbtW.DataSource:=dsCurSell;
       dbtCost.DataSource:=dsCurSell;
       dbtRCnt.DataSource:=dsCurSell;
       dbtRW.DataSource:=dsCurSell;
       dbtRCost.DataSource:=dsCurSell;
       dbtCass.DataSource:=dsCurSell;
       edRN.DataSource:=dsCurSell;
       dbCostCard.DataSource:=dsCurSell;

      {���������� ����}
//      acAdd.Enabled := boolean(dmCom.User.ALLSeLL);
//      acDel.Enabled := boolean(dmCom.User.ALLSeLL);
      acView.Enabled:= boolean(dmCom.User.ALLSeLL);
//      acSellEnd.Enabled:= boolean(dmCom.User.ALLSeLL);
//      acRet.Enabled := boolean(dmCom.User.ALLSeLL);
      edRN.Enabled := boolean(dmCom.User.ALLSeLL);
      edUID.Enabled := boolean(dmCom.User.ALLSeLL);
      dm.SellitemCard:=0;
    end;
end;

procedure TfmSellItem.acSellEndExecute(Sender: TObject);

  procedure SetChecks(Nc: Integer; D: TDateTime);

    procedure SetCheckNoSELL;
    begin
      with dm, taSellItem do
       begin
        D_CHECK0:=Nc;
        first;
        while not eof do
        begin
          Edit;
          taSellItemCheckNo.AsInteger:=Nc;
          if taSellItemRET.AsInteger=0 then
          taSellItemADate.AsDateTime:=D;
          Post;
          Next;
        end;
       end;
    end;

    procedure SetCheckNo;
    begin
      with dm, taSellItem do
        if(taSellItemRet.AsInteger = 0)then
        begin
          Edit;
          taSellItemCheckNo.AsInteger:=Nc;
          taSellItemADate.AsDateTime:=D;
          Post;
        end;
    end;

  var k: Integer;
  begin
    if dm.WorkMode<>'SELLCH' then
    begin
     with dg2 do
       if SelectedRows.Count>0 then
         for k:=0 to SelectedRows.Count-1 do
         begin
           dm.taSellItem.Bookmark:=SelectedRows[k];
           SetCheckNo;
         end
       else SetCheckNo;
    end
    else SetCheckNoSELL;
  end;

var
  DetailLine: Byte;
  i, n, j, c: Integer;
  s: String;
  Sep: Char;
  PSum, Sum: Extended;
  fl1:boolean;
  clientid:integer;
  clientname, nodcard:string;
  LogOperationID:string;
  Bookmark : Pointer;
begin
  fl1 := False;
  DetailLine := 0;
  if ((dmcom.User.ALLSeLL=0)and(dm.WorkMode<>'SELLCH')) then
    raise Exception.Create('��� ���� �� �������');

  PostDataSets([dm.taSellItem]);
  if dm.taSellItem.IsEmpty or dm.taSellItemSELLITEMID.IsNull then System.eXit;

  // ������� ���� ��� ����� ������ ���������� �����
  if (dm.UseFReg) then raise Exception.Create('��������� ����������� ������������ �� �����������');
  if (dm.WorkMode<>'SELLCH') then
  begin
    if ShowAndFreeForm(TfmNodCard, Self, TForm(fmNodCard), True, False) <> mrOk then System.eXit;
    LogOperationID:=dm3.insert_operation('������� �� � ������ <����> ��� ����. ���.',LogOprIdForm);
    // �������� ������� � ����� ���������� �����
    clientid:=dm.taSellItemCLIENTID.AsInteger;
    clientname:=dm.taSellItemCLIENTNAME.AsString;
    nodcard:=dm.taSellItemNODCARD.AsString;
    if dg2.SelectedRows.Count > 0 then
      for i:=0 to dg2.SelectedRows.Count-1 do
      begin
        dm.taSellItem.Bookmark:=dg2.SelectedRows[i];
        dm.taSellItem.Edit;
        dm.taSellItemCLIENTID.AsInteger:=clientid;
        dm.taSellItemCLIENTNAME.AsString:=clientname;
        dm.taSellItemNODCARD.AsString:=nodcard;
        dm.taSellItem.Post;
      end;

    // ���� �������
    if(dm.taSellItemRET.AsInteger = 1)then
    begin
      if(dm.taSellItemRETSITEMID.IsNull) then acRetToWh.Execute;// ������� �� �����
      // ���� ������� �� �����
      if(not dm.taSellItemRETSITEMID.IsNull)and(dm.taSellItemRETSITEMID.AsInteger <> 0)then
      begin
        // ��������� ����� � ���������
        acPrintDisv.Execute;
        acPrintAppl.Execute;
      end;
      // ����� ��������� � ������� �����
      if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
      dm3.update_operation(LogOperationID);
      System.eXit;
    end
    else begin// ������� �������
      if not dm.taSellItemCheckNo.IsNull then// ���� ����� ����
      begin
        // �������� ���
        acPrintCheck.Execute;
        // ����� ��������� � ������� �����
        if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
        dm3.update_operation(LogOperationID);
        System.eXit;
      end;
    end
  end
  // ������� �� ������ ����
  else begin
    if(dm.WorkMode <> 'SELLCH') then raise Exception.Create('������ [001]. ���������� � �������������');
    // ��������� ���� �� �� �������������� ��������
    if (dm.taSellItemRET.AsInteger=1) then
    begin
      Bookmark := dm.taSellItem.GetBookmark;
      dm.taSellItem.DisableScrollEvents;
      dm.taSellItem.DisableControls;
      try
        dm.taSellItem.First;
        fl1:=false;
        while not dm.taSellItem.Eof do
        begin
          if (dm.taSellItemRetSITEMID.IsNull) then fl1:=true;
          dm.taSellItem.Next;
        end;
        if fl1 then raise Exception.Create('����������� ��������');
      finally
        dm.taSellItem.EnableScrollEvents;
        dm.taSellItem.EnableControls;
        dm.taSellItem.GotoBookmark(Bookmark);
      end;
    end;

    /////////////////////////////////

    fl1:=false;
    {����� ���� ��� ����� ����. ����� ���������� � ��� ���}
    if not dmserv.RepeatClick then
    begin
      LogOperationID:=dm3.insert_operation('����. ����� �/��� ����� ��������',LogOprIdForm);
      if(dm.taSellItemRET.AsInteger = 0)then //�������
      begin
        if (ShowAndFreeForm(TfmNodCard, Self, TForm(fmNodCard), True, False)= mrOk) then
        begin
          if not dmcom.ShowNumEmp then dmserv.RepeatClick:=true
          else begin
            if (ShowAndFreeForm(TfmNumEmp, Self, TForm(fmNumEmp), True, False)= mrok)
            then dmserv.RepeatClick:=true else dmserv.RepeatClick:=false;
          end
        end
        else dmServ.RepeatClick:=false
      end
      else begin //�������
        if not dmcom.ShowNumEmp then dmserv.RepeatClick:=true
        else begin
          if (ShowAndFreeForm(TfmNumEmp, Self, TForm(fmNumEmp), True, False)= mrok)
          then dmserv.RepeatClick:=true else dmserv.RepeatClick:=false;
        end
      end;
      dm3.update_operation(LogOperationID);
    end // not dmserv.RepeatClick
    else begin
      LogOperationID:=dm3.insert_operation('������� � ������ <����> ��� ����. ���.',LogOprIdForm);
      if (dm.taSellItemCheckNo.IsNull)or(dm.taSellItemCheckNo.AsInteger=0) then
      {�������������� ������, ���� ��� ����������� ������������}
      with dm.quTmp do
      begin
       dm.D_CHECK0:=dmcom.GetID(36);
       dm.quTmp.close;
       sql.Text:='update sellitem set checkno='+inttostr(dm.D_CHECK0)+
                 'where checkno=0 and sellid='+dm.taCurSellSELLID.AsString;
       ExecQuery;
       dmcom.tr.CommitRetaining;
       dm.quTmp.Close;
      end;
      ReOpenDataSets([dm.taSellItem]);

      // ������ ������������ ���� ���������� ����
      if dmCom.IsPrintCheck then
      begin
       {����� ������� �������� � ������� ��� ������ ��������� ����}
       dm.taSellItem.First;
       while not dm.taSellItem.Eof do
        begin
        // ���� �������
         if(dm.taSellItemRET.AsInteger = 1)then
         begin
          if(dm.taSellItemRETSITEMID.IsNull) then acRetToWhExecute(nil);// ������� �� �����
          if(not dm.taSellItemRETSITEMID.IsNull)and(dm.taSellItemRETSITEMID.AsInteger <> 0)then
          begin
           if dmcom.Printtype=0 then
           begin
            SetDefaultPrinter2('EPSON TM-U950(Slip)');
            acPrintDisv.Execute;
            acPrintAppl.Execute;
           end
          end;
         if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
        end
        else fl1:=true;
        dm.taSellItem.Next;
       end;

       if fl1 then// ������� �������
        begin
         if dmcom.Printtype<>0 then
         begin
          dm.taSellItem.Locate('RET',0,[]);
          dm.taSellItemSELLITEMID.FocusControl;
          // �������� ���
          acPrintCheck.Execute;
         end
         else
         begin
          dmReport.ibdsTicketIt1.Close;
          dmReport.ibdsTicketIt1.ParamByName('I_ID').AsInteger:=dm.taSellItemSELLID.AsInteger;
          dmReport.ibdsTicketIt1.ParamByName('I_KIND').AsInteger:=3;
          dmReport.ibdsTicketIt1.ParamByName('I_CHEKNO').AsInteger:=dm.taSellItemCHECKNO.AsInteger;
          dmReport.ibdsTicketIt1.Open;


          SetDefaultPrinter2('EPSON TM-U950 No cut');
          //zrCheck.Print;
//          zrCheck.Preview;

////////////////////////////////////////////////
////////////////////////////////////////////////
         end
        end;
      end;

      if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
      beep;
      {����� �����}
      dmserv.RepeatClick:=false;
      SellState:='NEWCHECK';
//      if dm.taSellItemRET.AsInteger =0 then sleep(5000);

      dm.D_CHECK0:= 0;
      ReOpenDataSets([dm.taSellItem,dm.quCurSellCheck]);
      dm3.update_operation(LogOperationID);
     end;
    ActiveControl:=edUID;
    dm.SellitemCard:=0;    
    System.exit;
  end;


  {������� � ���������� �������������}
  //*****************************************************//
  with dm do
  begin
    if(WorkMode<>'SELL')and(WorkMode<>'SELLCH')then System.eXit;  // ����� ������ �������

    if (WorkMode='SELLCH') and (SellState='OLDCHECK') then System.exit;
    if ((not taSellItemCHECKNO.IsNull)and(taSellItemCHECKNO.AsInteger<>0)) then System.exit;

//    LogOperationID:=dm3.insert_operation('������� � ����. ���.',LogOprIdForm);

    if WorkMode='SELLCH' then
      if not dmserv.RepeatClick then begin if ShowAndFreeForm(TfmNodCard, Self, TForm(fmNodCard), True, False)= mrOk
         then dmserv.RepeatClick:=true else dmserv.RepeatClick:=false; System.exit end;

    if(WorkMode='SELLCH')and(taSellItemRET.AsInteger=1) then
    begin
      // ��������� ����� � ���������
     if dmCom.IsPrintCheck then
     begin
      acPrintDisv.Execute;
      acPrintAppl.Execute;
     end;
      if (dm.taSellItemCheckNo.IsNull)or(dm.taSellItemCheckNo.AsInteger=0) then
      {�������������� ������, ���� ��� ����������� ������������}
      with dm.quTmp do
      begin
       dm.D_CHECK0:=dmcom.GetID(36);
       close;
       sql.Text:='update sellitem set checkno='+inttostr(dm.D_CHECK0)+
                 'where checkno=0';
       ExecQuery;
       dmcom.tr.CommitRetaining;
       close;
      end;
      ReOpenDataSets([dm.taSellItem]);

      dm.D_CHECK0:=0;
      ReOpenDataSets([dm.taSellItem, dm.quCurSellCheck]);

     dmserv.RepeatClick:=false;
     ActiveControl:=edUID;
     System.eXit;
    end;

    if WorkMode='SELLCH' then
     with dm do
      begin
       taSellItem.First;
       while not taSellItem.Eof do
       begin
        if taSellItemRET.AsInteger=0 then
        dg2.SelectedRows.CurrentRowSelected:=true;
        taSellItem.Next;
       end;
      taSellItem.First;
     end;

//    dm3.update_operation(LogOperationID);

    {������ ������ ����� �� �������}
      Sum:=0;
      if dg2.SelectedRows.Count > 0 then
        for i:=0 to dg2.SelectedRows.Count-1 do
        begin
          taSellItem.Bookmark:=dg2.SelectedRows[i];
          Sum:=Sum+taSellItemCost.AsFloat;
         end
      else Sum:=taSellItemCost.AsFloat;

    if UseFReg then
    begin
      while not dmFR.FRReady do
        if(MessageDialog(rcFRNotReady+#13#10+' ���������?', mtWarning, [mbOK, mbCancel], 0) = mrOk)then continue
        else SysUtils.Abort;
    end;

    with fmCheckNo do
    begin
      ceP.Enabled:=true;
      ceS.Value:=Sum;
      ceP.Value:=Sum;
      if ShowModal=mrCancel then System.exit;
      PSum:=ceP.Value;
    end;

    // ���� ������������ ��, �������� �������� ���
    if UseFReg then
    begin
     with dmFR, taCheck do
     begin
       fmSplashFR.Show;
       if not com.Connected then com.Connect;
       Enabled:=False;
       Active:=True;
       while not EOF do
       begin
          if (taCheckAType.AsInteger = 15) then DetailLine:=taCheckLine.AsInteger
          else if (taCheckAType.AsInteger = 5) then DetailLine:=taCheckLine.AsInteger - DetailLine + 1;
          Next;
       end;

       FillChar(OutBuf, SizeOf(OutBuf), 0);
       c:=0; i:=0;
       First;
       Sep:=DecimalSeparator;
       DecimalSeparator:='.';
       while not EOF do
        begin
         inc(i);
         if dmFR.taCheckId.Asinteger = -1000 then
         begin
           Next;
           continue;
         end;

         if (taCheckAType.AsInteger = 15) and (dg2.SelectedRows.Count > 0) then taSellItem.Bookmark:=dg2.SelectedRows[c];
         if (taCheckAType.AsInteger = 15) or (taCheckAType.AsInteger = 16) then n:=99
         else n:=taCheckAType.AsInteger;


         OutBuf[(i-1)*52+13]:=Ord(Format('%.2d', [n])[1]);
         OutBuf[(i-1)*52+14]:=Ord(Format('%.2d', [n])[2]);

         OutBuf[(i-1)*52+16]:=Ord(taCheckFlag.AsString[1]);

         OutBuf[(i-1)*52+18]:=Ord(Format('%.2d', [taCheckPos.AsInteger])[1]);
         OutBuf[(i-1)*52+19]:=Ord(Format('%.2d', [taCheckPos.AsInteger])[2]);

         OutBuf[(i-1)*52+21]:=Ord(Format('%.2d', [taCheckLine.AsInteger + c*DetailLine])[1]);
         OutBuf[(i-1)*52+22]:=Ord(Format('%.2d', [taCheckLine.AsInteger + c*DetailLine])[2]);


         case taCheckAType.AsInteger of
           5: S:=ReplaceStr(Format('%0.2f', [taSellItemCost.AsFloat]), ',', '.');
           9: S:=taCurSellN.AsString;
          11: S:=FloatToStr(PSum);
          15: S:=StrToOem(taSellItemName.AsString);
          16: if taSellItemUID.AsString = '' then S:=StrToOem('�/�')
               else S:=taSellItemUID.AsString;
          99: S:=StrToOem(taCheckName.AsString);
            else S:='';
         end;

         if S <> '' then for j:=1 to Length(S) do OutBuf[(i-1)*52+23+j]:=Ord(S[j]);
         if (taCheckAType.AsInteger = 5) and (c < dg2.SelectedRows.Count-1) then
         begin
           inc(c);
           Locate('AType', 15, []);
         end
         else Next;
       end;

       DecimalSeparator:=Sep;
       OutBuf[8]:=$30;
       OutBuf[10]:=Ord(Format('%.2d', [i])[1]);
       OutBuf[11]:=Ord(Format('%.2d', [i])[2]);
       OutBuf[13+i*52]:=$30;
       SendPacket($33, 17+i*52, OutBuf);
       if (Status > 0) then
       begin
         MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
         Enabled:=True;
         fmSplashFR.Close;
         System.eXit;
       end;
       FillChar(OutBuf, SizeOf(OutBuf), 0);
       SendPacket($4A, 10, OutBuf);
       if (Status > 0) then
        begin
         MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
         Enabled:=True;
         fmSplashFR.Close;
         System.exit;
        end;
       n:=StrToInt(chr(Buf[19])+chr(Buf[20])+chr(Buf[21])+chr(Buf[22])+ chr(Buf[23]));
       SetChecks(n, StrToDateTime(chr(Buf[25])+chr(Buf[26])+'.'+chr(Buf[27])+chr(Buf[28])+'.'+chr(Buf[29])+chr(Buf[30])+chr(Buf[31])+chr(Buf[32])+' ' +
           chr(Buf[34])+chr(Buf[35])+':'+chr(Buf[36])+chr(Buf[37])));
       Active:=False;
       Enabled:=True;
       fmSplashFR.Close;
      end // ��������� ������ � ���������� �������������
    end
    else begin // ���� ��� �������� ����������
      n:=StrToInt(fmCheckNo.edCheck.Text);
      SetChecks(n, dmCom.GetServerTime);
    end;


    if (dm.WorkMode<>'SELLCH') then
    begin
    if dm.SellRet then
    begin
      // ��������� ����� � ���������
      acPrintDisv.Execute;
      acPrintAppl.Execute;
    end
    else begin
      acPrintCheck.Execute; // �������� ���
     end;
    end;
      if dmCom.IsPrintCheck and (dm.WorkMode='SELLCH') then
      begin
       {����� ������� �������� � ������� ��� ������ ��������� ����}
       ReOpenDataSets([dm.taSellItem]);
        dm.taSellItem.First;
        while not dm.taSellItem.Eof do
         begin
         // ���� �������
          if(dm.taSellItemRET.AsInteger = 1)then
          begin
           if(dm.taSellItemRETSITEMID.IsNull) then acRetToWhExecute(nil);// ������� �� �����
           if(not dm.taSellItemRETSITEMID.IsNull)and(dm.taSellItemRETSITEMID.AsInteger <> 0)then
           begin
            acPrintDisv.Execute;
            acPrintAppl.Execute;
           end;
          if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
         end
         else fl1:=true;
         dm.taSellItem.Next;
        end;

        if fl1 then// ������� �������
         begin
         dm.taSellItem.Locate('RET',0,[]);
         dm.taSellItemSELLITEMID.FocusControl;
        // �������� ���
        acPrintCheck.Execute;
        // ����� ��������� � ������� �����
        end;
      end;
    end;


  // ����� ��������� � ������� �����
  if dg2.SelectedRows.Count > 0 then dg2.SelectedRows.Clear;
  if dm.WorkMode = 'SELLCH' then
   begin
    with fmCheckNo do
    begin
      ceS.Value:=Sum;
      ceP.Value:=PSum;
      ceR.Value:=PSum-Sum;
      ceP.Enabled:=false;
      ShowModal;
    end;
    dm.D_CHECK0:=0;
    ReOpenDataSets([dm.taSellItem, dm.quCurSellCheck]);
   end;
  //*****************************************************//

  dmserv.RepeatClick:=false;
  ActiveControl:=edUID;
end;

procedure TfmSellItem.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := (dm.taSellItem.Active and (not dm.taSellItem.IsEmpty) and
                    (dm.taSellListDEPID.AsInteger = SelfDepId) and
                    boolean(dmCom.User.ALLSeLL)) or
                   ((dm.WorkMode = 'SELL') and (not dm.taCurSell.IsEmpty) and
                    boolean(dmCom.User.ALLSeLL)) or
                   ((dm.WorkMode = 'SELLCH') and (not dm.taCurSell.IsEmpty))
end;

procedure TfmSellItem.acRetToWhUpdate(Sender: TObject);
begin
 acRetToWh.Enabled := dm.taSellItem.Active and (not dm.taSellItem.IsEmpty) and
                     (dm.taSellItemRET.AsInteger=1) and (dm.taSellItemRETSITEMID.IsNull)
end;

procedure TfmSellItem.edUIDKeyPress(Sender: TObject; var Key: Char);
begin
case key of
 '0'..'9', #8: ;
 else sysutils.Abort;
end; 
end;

procedure TfmSellItem.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dm.taSellItem.Active and (not dm.taSellItemSELLITEMID.IsNull);
end;

{procedure TfmSellItem.acScanConnectExecute(Sender: TObject);
begin
 if dmserv.ComScan.Connected then lscan.Caption:='������ ���������'
 else  lscan.Caption:='������ ��������';
 lscan.Visible:=not lscan.Visible;
end; }

procedure TfmSellItem.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := (dm.taSellItem.Active and (not dm.taSellItem.IsEmpty) and
                    (dm.taSellListDEPID.AsInteger = SelfDepId) and
                    boolean(dmCom.User.ALLSeLL)) or
                   ((dm.WorkMode = 'SELL') and (not dm.taCurSell.IsEmpty) and
                    boolean(dmCom.User.ALLSeLL)) or
                   ((dm.WorkMode = 'SELLCH') and (not dm.taCurSell.IsEmpty))
end;

procedure TfmSellItem.acSellEndUpdate(Sender: TObject);
begin
  acSellEnd.Enabled := (dm.taSellItem.Active and (not dm.taSellItem.IsEmpty) and
                        (dm.taSellListDEPID.AsInteger = SelfDepId) and
                         boolean(dmCom.User.ALLSeLL)) or
                       ((dm.WorkMode = 'SELL') and (not dm.taCurSell.IsEmpty) and
                         boolean(dmCom.User.ALLSeLL)) or
                       ((dm.WorkMode = 'SELLCH') and (not dm.taCurSell.IsEmpty))
end;

procedure TfmSellItem.acRetUpdate(Sender: TObject);
begin
  acRet.Enabled := (dm.taSellItem.Active and (not dm.taSellItem.IsEmpty) and
                        (dm.taSellListDEPID.AsInteger = SelfDepId) and
                         boolean(dmCom.User.ALLSeLL)) or
                       ((dm.WorkMode = 'SELL') and (not dm.taCurSell.IsEmpty) and
                         boolean(dmCom.User.ALLSeLL)) or
                       ((dm.WorkMode = 'SELLCH') and (not dm.taCurSell.IsEmpty))
end;

procedure TfmSellItem.acActiveEdUidUpdate(Sender: TObject);
begin
 if edUID.Enabled and edUID.Visible then acActiveEdUid.Enabled:=true
 else acActiveEdUid.Enabled:=false;
end;

procedure TfmSellItem.acActiveEdUidExecute(Sender: TObject);
begin
 ActiveControl:=edUID;
 eduid.SelectAll;
end;

procedure TfmSellItem.acDataWithSellExecute(Sender: TObject);
var d: TDateTime;
begin
 if (dmcom.User.ALLSeLL=0) then
   raise Exception.Create('��� ���� �� ��������� ����');

  fmSetSDate:=TfmSetSDate.Create(Application);
  try
    fmSetSDate.mc1.Date:=dm.taSellListBD.AsDateTime;
    fmSetSDate.tp1.Time:=dm.taSellListBD.AsDateTime;    
    if fmSetSDate.ShowModal=mrOK then
      with dm do
        begin
          d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
          with quTmp do
            begin
              close;
              sql.Text := 'select str from exime_date_insell ( '+taSellListSellId.AsString+', '''
                          +DateTimeToStr(d)+''',' + IntToStr(TAction(Sender).Tag) + ' )';
              ExecQuery;
              if trim(Fields[0].AsString)<>'' then
              Application.MessageBox(pchar(Fields[0].AsString),'��������!!!',0);
              close;
            end;
          dmCom.tr.CommitRetaining;
          ReOpenDataSets([taSellItem]);
        end;
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmSellItem.acDataWithSellUpdate(Sender: TObject);
begin
 if (tb3.Enabled and tb3.Visible and siSetSDate.Enabled and siSetSDate.Visible)
 then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false
end;

procedure TfmSellItem.acPrintGridExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdg2.Print;
end;

procedure TfmSellItem.acPrintCheckUpdate(Sender: TObject);
begin
 if dm.WorkMode<>'SELLCH' then acPrintCheck.Enabled:=true
 else acPrintCheck.Enabled:=false
end;

procedure TfmSellItem.acPrintApplUpdate(Sender: TObject);
begin
{ if dm.WorkMode<>'SELLCH' then acPrintAppl.Enabled:=true
 else acPrintAppl.Enabled:=false}
end;

procedure TfmSellItem.acPrintDisvUpdate(Sender: TObject);
begin
{ if dm.WorkMode<>'SELLCH' then acPrintDisv.Enabled:=true
 else acPrintDisv.Enabled:=false}
end;

procedure TfmSellItem.acPrintTagExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  if dm.taSellItem.IsEmpty then System.eXit;
  try
    case (Sender as TAction).Tag of
      0,1 : begin
            if dm.taSellItemRETSITEMID.IsNull or(dm.taSellItemRETSITEMID.AsInteger = 0)
            then raise Exception.Create('����� ����� �������� ������ �� ������������ �������');
            arr:=gen_arr(dg2, dm.taSellItemRETSITEMID);
            try
             PrintTag(arr, ret_tag, TAction(Sender).Tag);
            finally
             Finalize(arr);
             Finalize(dmReport.arr_sell);
            end;
          end;
      5: begin
           if dm.WorkMode<>'ALLSELL' then  arr:=gen_arr(dg2, dm.taCurSellSellId)
           else arr:=gen_arr(dg2, dm.taSellItemSellID);
           try
            PrintDocument(arr,sell_ticket);
           finally
            Finalize(arr);
            Finalize(dmReport.arr_sell);
           end;
         end;
     6:  begin
           if dm.WorkMode<>'ALLSELL' then arr:=gen_arr(dg2, dm.taCurSellSellId)
           else arr:=gen_arr(dg2, dm.taSellItemSellID);
           try
            PrintDocument(arr,sell_ticketOutlay);
           finally
            Finalize(arr);
            Finalize(dmReport.arr_sell);
           end;
         end;
     7:  begin
           if dm.WorkMode<>'ALLSELL' then arr:=gen_arr(dg2, dm.taCurSellSellId)
           else arr:=gen_arr(dg2, dm.taSellItemSellID);
           try
            PrintDocument(arr,sell_ticketart);
           finally
            Finalize(arr);
            Finalize(dmReport.arr_sell);
           end;
         end;
     8:  begin
           if dm.WorkMode<>'ALLSELL' then arr:=gen_arr(dg2, dm.taCurSellSellId)
           else arr:=gen_arr(dg2, dm.taSellItemSellID);
           try
            PrintDocument(arr,sell_ticketsinv);
           finally
            Finalize(arr);
            Finalize(dmReport.arr_sell);
           end;
         end;
      9,10 : begin
            if dm.taSellItemRETSITEMID.IsNull or(dm.taSellItemRETSITEMID.AsInteger = 0)
            then raise Exception.Create('����� ����� �������� ������ �� ������������ �������');
            arr:=gen_arr(dg2, dm.taSellItemRETSITEMID);
            try
             PrintTag(arr, ret_tag, TAction(Sender).Tag-7);
            finally
             Finalize(arr);
             Finalize(dmReport.arr_sell);
            end;
          end;
    end;
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellItem.acActiveedArtExecute(Sender: TObject);
begin
 ActiveControl:=edArt;
 edArt.SelectAll;
end;

procedure TfmSellItem.acActiveedArtUpdate(Sender: TObject);
begin
 if edArt.Enabled and edArt.Visible then acActiveedArt.Enabled:=true
 else acActiveedArt.Enabled:=false;
end;

procedure TfmSellItem.acActiveedRNExecute(Sender: TObject);
begin
 ActiveControl:=edRN;
 edRN.SelectAll;
end;

procedure TfmSellItem.acActiveedRNUpdate(Sender: TObject);
begin
 if edRN.Enabled and edRN.Visible then acActiveedRN.Enabled:=true
 else acActiveedRN.Enabled:=false;
end;

procedure TfmSellItem.acActiveBdExecute(Sender: TObject);
begin
 ActiveControl:=DBEdit1;
end;

procedure TfmSellItem.acActiveBdUpdate(Sender: TObject);
begin
 if DBEdit1.Enabled and DBEdit1.Visible and tb3.Visible then acActiveBd.Enabled:=true
 else acActiveBd.Enabled:=false;
end;

procedure TfmSellItem.acActiveEdExecute(Sender: TObject);
begin
 ActiveControl:=DBEdit2;
end;

procedure TfmSellItem.acActiveGridExecute(Sender: TObject);
begin
 ActiveControl:=dg2
end;

procedure TfmSellItem.acActiveEdUpdate(Sender: TObject);
begin
 if DBEdit2.Enabled and DBEdit2.Visible and tb3.Visible then acActiveEd.Enabled:=true
 else acActiveEd.Enabled:=false;
end;

procedure TfmSellItem.acClearApplDepExecute(Sender: TObject);
begin
 if MessageDialog('�������� ���� ������?', mtWarning, [mbOK, mbCancel], 0)=mrok then
 begin
  dm.taSellItem.First;
  while not dm.taSellItem.Eof do
  begin
   dm.quTmp1.Close;
   dm.quTmp1.SQL.Text:='select d_depid, sname from d_dep where d_depid<>-1000 and d_depid<>'+
     dm.taSellListDEPID.AsString+' and isdelete<>1 ';
   dm.quTmp1.ExecQuery;
   while not dm.quTmp1.Eof do
   begin
    if dm.taSellItem.FieldByName('QART_'+dm.qutmp1.fields[0].Asstring).AsInteger<>0 then
    begin
      dm.taSellItem.Edit;
      dm.taSellItem.FieldByName('QART_'+dm.qutmp1.fields[0].Asstring).AsInteger:=0;
      dm.taSellItem.Post;
    end;
    dm.quTmp1.Next;
   end;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
   dm.taSellItem.Next;
  end;
 end;
end;

procedure TfmSellItem.acCreateAplldepUpdate(Sender: TObject);
begin
 //TAction(Sender).Enabled:= (dm.taSellListCLOSED.AsInteger=0) and (CenterDep) and (dm.WorkMode='ALLSELL')
end;

procedure TfmSellItem.acCreateAplldepExecute(Sender: TObject);
var i:string;
    j:integer;
begin
  dm.taSellItem.First;
  while not dm.taSellItem.Eof do
  begin
   dm.quTmp1.Close;
   dm.quTmp1.SQL.Text:='select d_depid, sname from d_dep where d_depid<>-1000 and d_depid<>'+
     dm.taSellListDEPID.AsString+' and isdelete<>1 ';
   dm.quTmp1.ExecQuery;
   while not dm.quTmp1.Eof do
   begin
    dm.quTmp.Close;
    if dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).IsNull then i:='0'
    else begin
     j:=dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).AsInteger-
        dm.taSellItem.FieldByName('APPLQ_'+dm.quTmp1.Fields[0].AsString).AsInteger;
     if j<0 then begin
      MessageDialog('��� �������� '+trim(dm.taSellItemART.AsString)+' ������������ ��� '+
       dm.taSellItem.FieldByName('APPLQ_'+dm.quTmp1.Fields[0].AsString).AsString+' ������, '+
       #13#10+' ������� ���������� '+dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).AsString+
       ' �� ����� ���� �������.' , mtWarning, [mbOk], 0);
      j:=0;
     end;
     i:=inttostr(j);
    end;

    dm.quTmp.SQL.Text:='select fres from CREATE_APPLDEPSELL('+dm.taSellListSELLID.AsString+
     ', '+dm.taSellItemSELLITEMID.AsString+', '+i+
     ', '+dm.quTmp1.Fields[0].AsString+','+inttostr(dmcom.UserId)+')';
    dm.quTmp.ExecQuery;
    if dm.quTmp.Fields[0].AsInteger=1 then
     MessageDialog('�� '+trim(dm.quTmp1.Fields[1].AsString)+' ��� �������� '+
                 trim(dm.taSellItemART.AsString)+' �� ����� ������� ������ �� ����� ���-�� �������!', mtWarning, [mbOk], 0);
    dm.quTmp1.Next;
    dm.quTmp.Close;
    dm.quTmp.Transaction.CommitRetaining;
   end;
   dm.taSellItem.Next;
  end;
  dm.taSellItem.First;
  MessageDialog('������ �� ������� ��������������!', mtInformation, [mbOk], 0);
  ReOpenDataSet(dm.taSellItem);
end;

procedure TfmSellItem.acCreateApplExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('������������ ������ �����������',LogOprIdForm);
 PostDataSet(dm.taSellItem);
 ExecSQL('execute procedure Create_Appl_FromSell ('+inttostr(dmcom.UserId)+', '+
         dm.taSellListSELLID.AsString+', 1)', dmCom.quTmp);
 MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;

procedure TfmSellItem.acClearApplExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('�������� ���� ������',LogOprIdForm);
 PostDataSet(dm.taSellItem);
 ExecSQL('update sellitem set appl_q=null where sellid='+dm.taSellListSELLID.AsString+
          ' and appl_q<>0 and appl_q is not null ', dmCom.quTmp);
 ReOpenDataSet(dm.taSellItem);
 MessageDialog('���� "������ �����������" �������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;

procedure TfmSellItem.acRefreshApplExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('�������� ������ �����������',LogOprIdForm);
 PostDataSet(dm.taSellItem);
 ExecSQL('execute procedure Create_Appl_FromSell ('+inttostr(dmcom.UserId)+', '+
         dm.taSellListSELLID.AsString+', 0)', dmCom.quTmp);
 MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;

procedure TfmSellItem.acCardExecute(Sender: TObject);
var sellitemid:integer;
    qu:TFIBDataSet;
    f:boolean;
begin
 PostDataSet(dm.taSellItem);
 f:=false;
 if dm.WorkMode='ALLSELL' then qu:=dm.taSellList
 else if dm.WorkMode='SELL' then qu:=dm.taCurSell
 else begin qu:=dm.quCurSellCheck; f:=true end;

 if MessageDialog('�������� ��� ����� �� ���������?', mtWarning, [mbYes, mbNo], 0)=mrYes then
 begin
  if dm.taSellItemCHECKNO.IsNull then begin
   ExecSQL('update sellitem set card=1 where sellitemid='+dm.taSellItemSELLITEMID.AsString, dm.quTmp);
   dm.taSellItem.Refresh;
  end else begin
   ExecSQL('update sellitem set card=1 where sellid='+string(qu.FieldValues['SELLID'])+
           ' and checkno='+dm.taSellItemCHECKNO.AsString, dm.quTmp);
   sellitemid:=dm.taSellItemSELLITEMID.AsInteger;
   ReOpenDataSet(dm.taSellItem);
   dm.taSellItem.Locate('SELLITEMID', sellitemid, []);
  end;
   if f then dm.SellitemCard:=1;
   qu.Refresh;
  end else if ShowAndFreeForm(TfmCostCard, Self, TForm(fmCostCard), True, False)=mrOk then
  begin
   if dm.taSellItemCHECKNO.IsNull then begin
    ExecSQL('update sellitem set card=2 where sellitemid='+dm.taSellItemSELLITEMID.AsString, dm.quTmp);
    dm.taSellItem.Refresh;
   end else begin
    ExecSQL('update sellitem set card=2 where sellid='+string(qu.FieldValues['SELLID'])+
            ' and checkno='+dm.taSellItemCHECKNO.AsString, dm.quTmp);
    sellitemid:=dm.taSellItemSELLITEMID.AsInteger;
    ReOpenDataSet(dm.taSellItem);
    dm.taSellItem.Locate('SELLITEMID', sellitemid, []);
   end;
   if f then dm.SellitemCard:=2;   
   qu.Refresh;
  end;
end;

procedure TfmSellItem.acNotCardExecute(Sender: TObject);
var sellitemid:integer;
    qu:TFIBDataSet;
begin
 PostDataSet(dm.taSellItem);
 if dm.WorkMode='ALLSELL' then qu:=dm.taSellList
 else if dm.WorkMode='SELL' then qu:=dm.taCurSell
 else qu:=dm.quCurSellCheck;

 if dm.taSellItemCHECKNO.IsNull then begin
  ExecSQL('update sellitem set card=0 where sellitemid='+dm.taSellItemSELLITEMID.AsString, dm.quTmp);
  dm.taSellItem.Refresh;
 end else begin
  ExecSQL('update sellitem set card=0 where sellid='+string(qu.FieldValues['SELLID'])+
          ' and checkno='+dm.taSellItemCHECKNO.AsString, dm.quTmp);
  sellitemid:=dm.taSellItemSELLITEMID.AsInteger;
  ReOpenDataSet(dm.taSellItem);
  dm.taSellItem.Locate('SELLITEMID', sellitemid, []);
 end;
 dm.SellitemCard:=0;
 qu.Refresh; 
end;

procedure TfmSellItem.acCardUpdate(Sender: TObject);
begin
 if dm.WorkMode='ALLSELL' then
  TAction(Sender).Enabled:= (not dm.taSellListSELLID.IsNull) and
   (not dm.taSellItemSELLITEMID.IsNull) and (dm.taSellItemCARD.AsInteger=0) and
   (dm.taSellListCLOSED.AsInteger=0) and (dm.taSellListDEPID.Asinteger=SelfDepId)
 else
  TAction(Sender).Enabled:= not dm.taCurSellSELLID.IsNull and
   (not dm.taSellItemSELLITEMID.IsNull) and (dm.taSellItemCARD.AsInteger=0) and
   (dm.taCurSellCLOSED.AsInteger=0) and (dm.taCurSellDEPID.Asinteger=SelfDepId)
end;

procedure TfmSellItem.siHelpClick(Sender: TObject);
begin
 if dm.WorkMode='ALLSELL' then Application.HelpContext(100233)
 else Application.HelpContext(100231);
end;

procedure TfmSellItem.taSertBeforeOpen(DataSet: TDataSet);
begin
 taSert.ParamByName('TRANSACT_ID').AsInteger:=dm.taSellItemTransact_id.AsInteger;
end;

procedure TfmSellItem.acCreateApplDepSZExecute(Sender: TObject);
var i:string;
    j:integer;
begin
  dm.taSellItem.First;
  while not dm.taSellItem.Eof do
  begin
   dm.quTmp1.Close;
   dm.quTmp1.SQL.Text:='select d_depid, sname from d_dep where d_depid<>-1000 and d_depid<>'+
     dm.taSellListDEPID.AsString+' and isdelete<>1 ';
   dm.quTmp1.ExecQuery;
   while not dm.quTmp1.Eof do
   begin
    dm.quTmp.Close;
    if dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).IsNull then i:='0'
    else begin
     j:=dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).AsInteger-
        dm.taSellItem.FieldByName('APPLQ_'+dm.quTmp1.Fields[0].AsString).AsInteger;
     if j<0 then begin
      MessageDialog('��� �������� '+trim(dm.taSellItemART.AsString)+' ������������ ��� '+
       dm.taSellItem.FieldByName('APPLQ_'+dm.quTmp1.Fields[0].AsString).AsString+' ������, '+
       #13#10+' ������� ���������� '+dm.taSellItem.FieldByName('QART_'+dm.quTmp1.Fields[0].AsString).AsString+
       ' �� ����� ���� �������.' , mtWarning, [mbOk], 0);
      j:=0;
     end;
     i:=inttostr(j);
    end;

    dm.quTmp.SQL.Text:='select fres from CREATE_APPLDEPSELL_SZ('+dm.taSellListSELLID.AsString+
     ', '+dm.taSellItemSELLITEMID.AsString+', '+i+
     ', '+dm.quTmp1.Fields[0].AsString+','+inttostr(dmcom.UserId)+')';
    dm.quTmp.ExecQuery;
    if dm.quTmp.Fields[0].AsInteger=1 then
     MessageDialog('�� '+trim(dm.quTmp1.Fields[1].AsString)+' ��� �������� '+
                 trim(dm.taSellItemART.AsString)+' �� ����� ������� ������ �� ����� ���-�� �������!', mtWarning, [mbOk], 0);
    dm.quTmp1.Next;
    dm.quTmp.Close;
    dm.quTmp.Transaction.CommitRetaining;
   end;
   dm.taSellItem.Next;
  end;
  dm.taSellItem.First;
  MessageDialog('������ �� ������� ��������������!', mtInformation, [mbOk], 0);
  ReOpenDataSet(dm.taSellItem);
end;

procedure TfmSellItem.acNotCardUpdate(Sender: TObject);
begin
 if dm.WorkMode='ALLSELL' then
  TAction(Sender).Enabled:= (not dm.taSellListSELLID.IsNull) and
   (not dm.taSellItemSELLITEMID.IsNull) and (dm.taSellItemCARD.AsInteger<>0) and
   (dm.taSellListCLOSED.AsInteger=0) and (dm.taSellListDEPID.Asinteger=SelfDepId)
 else
  TAction(Sender).Enabled:= not dm.taCurSellSELLID.IsNull and
   (not dm.taSellItemSELLITEMID.IsNull) and (dm.taSellItemCARD.AsInteger<>0) and
   (dm.taCurSellCLOSED.AsInteger=0) and (dm.taCurSellDEPID.Asinteger=SelfDepId)   
end;

procedure TfmSellItem.siCloseSellClick(Sender: TObject);
begin
If (dm.taSellListCLOSED.AsInteger=1) and (SellList.WMode='SELLLIST') then
   ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False)
 else
   if (dm.taSellListCLOSED.AsInteger=0) and (SellList.WMode='SELLLIST') and (MessageDialog('������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrYes) then
   acCloseSell.Execute;
 if (SellList.WMode='') then  acOpenSell.Execute else
 if (SellList.WMode='') and (MessageDialog('������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrYes) then
   acCloseSell.Execute;
end;
end.




