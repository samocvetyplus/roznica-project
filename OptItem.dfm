object fmOptItem: TfmOptItem
  Left = 191
  Top = 129
  AutoScroll = False
  Caption = #1042#1074#1086#1076' '#1080#1079#1076#1077#1083#1080#1081' '#1076#1083#1103' '#1086#1087#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1072#1078#1080
  ClientHeight = 468
  ClientWidth = 306
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object sb1: TStatusBar
    Left = 0
    Top = 449
    Width = 306
    Height = 19
    Panels = <
      item
        Text = #1050#1086#1083'-'#1074#1086':'
        Width = 100
      end
      item
        Text = #1042#1077#1089':'
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 306
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 187
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = sbAddClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = sbDelClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 42
    Width = 306
    Height = 407
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object dg1: TRxDBGrid
      Left = 0
      Top = 0
      Width = 306
      Height = 407
      Align = alClient
      Color = clBtnFace
      DataSource = dm.dsDItem
      PopupMenu = pm1
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      FixedCols = 1
      OnGetCellParams = M207IBGrid1GetCellParams
      Columns = <
        item
          Expanded = False
          FieldName = 'UID'
          Title.Alignment = taCenter
          Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 119
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'W'
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 81
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'SZ'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1079#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 160
    Top = 188
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      GroupIndex = 1
      ImageIndex = 1
      ShortCut = 45
      OnClick = sbAddClick
    end
    object N3: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      GroupIndex = 1
      ShortCut = 16430
      OnClick = sbDelClick
    end
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 500
    Top = 132
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 13
      OnClick = sbDelClick
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 256
    Top = 214
  end
end
