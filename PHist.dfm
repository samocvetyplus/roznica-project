object fmPHist: TfmPHist
  Left = 221
  Top = 280
  HelpContext = 100520
  Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1094#1077#1085#1099
  ClientHeight = 576
  ClientWidth = 770
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 321
    Top = 29
    Height = 528
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 557
    Width = 770
    Height = 19
    Panels = <>
  end
  object Panel2: TPanel
    Left = 324
    Top = 29
    Width = 446
    Height = 528
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter2: TSplitter
      Left = 0
      Top = 225
      Width = 446
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object dg1: TM207IBGrid
      Left = 0
      Top = 0
      Width = 446
      Height = 225
      Align = alTop
      Color = clBtnFace
      DataSource = dm.dsDPrice
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dg1DblClick
      OnKeyDown = dg1KeyDown
      IniStorage = FormStorage1
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = False
      Columns = <
        item
          Expanded = False
          FieldName = 'PRODCODE'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1048#1079#1075'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'D_MATID'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1052#1072#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 31
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'D_GOODID'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1053#1072#1080#1084'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 33
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'D_INSID'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1054#1042
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'D_COUNTRYID'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ART'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ART2'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNITID'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1045#1048
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 27
          Visible = True
        end
        item
          Color = clYellow
          Expanded = False
          FieldName = 'OPTPRICE'
          Title.Alignment = taCenter
          Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 71
          Visible = True
        end
        item
          Color = clAqua
          Expanded = False
          FieldName = 'SPRICE'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1055#1088'.'#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRICE'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end>
    end
    object M207IBGrid2: TM207IBGrid
      Left = 0
      Top = 228
      Width = 446
      Height = 300
      Align = alClient
      Color = clBtnFace
      DataSource = dm2.dsPHist
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      IniStorage = FormStorage1
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = False
      Columns = <
        item
          Expanded = False
          FieldName = 'SETDATE'
          Title.Alignment = taCenter
          Title.Caption = #1044#1072#1090#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRORD'
          Title.Alignment = taCenter
          Title.Caption = #8470' '#1087#1088'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SNStr'
          Title.Alignment = taCenter
          Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CLOSEEMP'
          Title.Alignment = taCenter
          Title.Caption = #1047#1072#1082#1088#1099#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SETEMP'
          Title.Alignment = taCenter
          Title.Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 91
          Visible = True
        end
        item
          Color = clAqua
          Expanded = False
          FieldName = 'PRICE'
          Title.Alignment = taCenter
          Title.Caption = #1055#1088'. '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRICE2'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clYellow
          Expanded = False
          FieldName = 'OPTPRICE'
          Title.Alignment = taCenter
          Title.Caption = #1054#1087#1090' '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 29
    Width = 321
    Height = 528
    Align = alLeft
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 53
      Top = 1
      Height = 526
    end
    object Splitter4: TSplitter
      Left = 189
      Top = 1
      Height = 526
    end
    object Splitter5: TSplitter
      Left = 252
      Top = 1
      Height = 526
    end
    object Splitter6: TSplitter
      Left = 121
      Top = 1
      Height = 526
    end
    object lbComp: TListBox
      Left = 1
      Top = 1
      Width = 52
      Height = 526
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbMat: TListBox
      Left = 124
      Top = 1
      Width = 65
      Height = 526
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 1
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbGood: TListBox
      Left = 192
      Top = 1
      Width = 60
      Height = 526
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 2
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbIns: TListBox
      Left = 255
      Top = 1
      Width = 65
      Height = 526
      Align = alClient
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 3
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbCountry: TListBox
      Left = 56
      Top = 1
      Width = 65
      Height = 526
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 4
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 770
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 3
    InternalVer = 1
    object laDep: TLabel
      Left = 92
      Top = 8
      Width = 69
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 392
      Top = 7
      Width = 32
      Height = 13
      Caption = #1055#1086#1080#1089#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edArt: TEdit
      Left = 432
      Top = 4
      Width = 79
      Height = 21
      Color = clInfoBk
      TabOrder = 0
      OnChange = edArtChange
      OnKeyDown = edArtKeyDown
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmPHist
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 68
      Layout = blGlyphLeft
      Spacing = 1
      Left = 251
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 675
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Layout = blGlyphLeft
      Spacing = 1
      Left = 547
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 240
    Top = 133
  end
end
