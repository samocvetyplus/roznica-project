object fmShopReportList: TfmShopReportList
  Left = 247
  Top = 173
  HelpContext = 100430
  Caption = #1057#1087#1080#1089#1086#1082' '#1090#1086#1074#1072#1088#1085#1099#1093' '#1086#1090#1095#1077#1090#1086#1074
  ClientHeight = 667
  ClientWidth = 639
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 639
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      ImageIndex = 0
      Spacing = 1
      Left = 570
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siRep: TSpeedItem
      Action = acPrintShopReport
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C000018631863186318631863186318631863186300001863
        00001F7C1F7C0000000000000000000000000000000000000000000000000000
        186300001F7C0000186318631863186318631863FF7FFF7FFF7F186318630000
        000000001F7C0000186318631863186318631863007C007C007C186318630000
        186300001F7C0000000000000000000000000000000000000000000000000000
        1863186300000000186318631863186318631863186318631863186300001863
        0000186300001F7C000000000000000000000000000000000000000018630000
        1863000000001F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001863
        0000186300001F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F00000000
        000000001F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 67
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = acPrintShopReportExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7CE07FEF3D00001F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00000000E07F1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DFF7F007C007C007C007C007CFF7FEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088'|'
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acRefresh
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C16421642164216421642164216421642164216421642
        164216421F7C1F7C1F7C1642BF6B7A577B577E5B5D535D4F3E4F3E4B3E4F3E4F
        5E4F16421F7C1F7C1F7C1642BF6F354BA81EF13686168616CF2A1A473D4B3D4B
        5E4F16421F7C1F7C1F7C163ADF737957840E6002600260026002A8163C4B3D4B
        5E4F16421F7C1F7C1F7C163ADF77795B840E600285123747F1366106F2323D4F
        5E4F16421F7C1F7C1F7C3742FF7B795F820A60026106133F7E5F1747F0365E53
        5E4F16421F7C1F7C1F7C3742FF7FBD6F7653554F354B354B7E637E5F564B5E57
        5E5316421F7C1F7C1F7C7942FF7F975BDF77BE6F564F354B354736477D5B7E5B
        7E5716421F7C1F7C1F7C7942FF7F5147985FDF77334761066002820A7E637E5F
        7F5B16421F7C1F7C1F7CBB42FF7F534F61065247785BA6166002840E9E679E63
        7E5B16421F7C1F7C1F7CBB42FF7FFE7BC8226002600260026002840EBF6F7C63
        F85616421F7C1F7C1F7CDC46FF7FFF7FDC733043C71EA71A3143A81E153A173E
        173E16421F7C1F7C1F7CDC46FF7FFF7FFF7FFF7FDD77DC73FF7F17573436DD3A
        9D2618361F7C1F7C1F7CFD4AFF7FFF7FFF7FFF7FFF7FFF7FFF7F5C6B173E1F3B
        593A1F7C1F7C1F7C1F7CFD4ADF7BDF7BDF7BDF7BDF7BDF7BDF7B5C67173E7942
        1F7C1F7C1F7C1F7C1F7CFD4ABB42BB42BB42BB42BB42BB42BB42BB42173E1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acRefreshExecute
      SectionName = 'Untitled (0)'
    end
    object siExport: TSpeedItem
      Action = acExport
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1700170017001700
        1700170017001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1700FF7FFF7FFF7F
        FF7FFF7F17001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C005C1700FF7F10421042
        1042FF7F170017001700170017001700170017001F7C005C1700FF7FFF7FFF7F
        FF7FFF7F17001700FF7FFF7FFF7FFF7FFF7F1700005C1F7C1700170017001700
        1700170017001700FF7F104210421042FF7F1700005C1F7C17001F7C17001F7C
        17001F7C17001700FF7FFF7FFF7FFF7FFF7F17001F7C1F7C1700170017001700
        17001700170017001700170017001700170017001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C17001F7C17001F7C17001F7C17001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C17001700170017001700170017001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      OnClick = acExportExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 506
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 639
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDep: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbYear: TLabel
      Left = 272
      Top = 8
      Width = 22
      Height = 13
      Caption = #1043#1086#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object edYear: TDBNumberEditEh
      Left = 300
      Top = 3
      Width = 77
      Height = 21
      DecimalPlaces = 0
      EditButton.ShortCut = 0
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      MinValue = 1999.000000000000000000
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = edYearChange
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = pmDep
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object gridShopReportList: TDBGridEh
    Left = 0
    Top = 69
    Width = 639
    Height = 579
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dsrShopReportList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    PopupMenu = ppDoc
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridShopReportListGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.EndEllipsis = True
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'MONTHNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 130
      end
      item
        EditButtons = <>
        FieldName = 'CRDATE'
        Footers = <>
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOST'
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
        Title.EndEllipsis = True
        Width = 66
      end
      item
        EditButtons = <>
        FieldName = 'RESCOST'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
        Title.EndEllipsis = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object stbrMain: TStatusBar
    Left = 0
    Top = 648
    Width = 639
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object taShopReportList: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    SINV'
      'WHERE'
      '        SINVID = :OLD_SINVID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO SINV('
      '    SINVID,'
      '    SN,'
      '    SDATE,'
      '    NDATE,'
      '    DEPID,'
      '    USERID,'
      '    ITYPE'
      ')'
      'VALUES('
      '    :SINVID,'
      '    :SN,'
      '    :SDATE,'
      '    :NDATE,'
      '    :DEPID,'
      '    :USERID,'
      '    18'
      ')')
    RefreshSQL.Strings = (
      
        'select i.SINVID, i.SN, i.SDATE, i.NDATE, i.DEPID, d.NAME DEPNAME' +
        ','
      '  i.UserId, e.FIO USERNAME, i.ITYPE, d.COLOR DEPCOLOR, i.CRDATE,'
      
        '  (select COST from ShopReport r1 where r1.SINVID=i.SINVID and r' +
        '1.T=0) ENTRYCOST,'
      
        '  (select COST from ShopReport r2 where r2.SINVID=i.SINVID and r' +
        '2.T=999) RESCOST'
      'from SInv i, D_Dep d, D_Emp e'
      'where i.SINVID=:SINVID and '
      '      i.DepId = d.D_DepId and'
      '      i.UserId = e.D_EmpId'
      '      ')
    SelectSQL.Strings = (
      
        'select i.SINVID, i.SN, i.SDATE, i.NDATE, i.DEPID, d.NAME DEPNAME' +
        ','
      '  i.UserId, e.FIO USERNAME, i.ITYPE, d.COLOR DEPCOLOR,'
      '  i.CRDATE,'
      
        '  (select COST from ShopReport r1 where r1.SINVID=i.SINVID and r' +
        '1.T=0) ENTRYCOST,'
      
        '  (select COST from ShopReport r2 where r2.SINVID=i.SINVID and r' +
        '2.T=999) RESCOST'
      'from SInv i, D_Dep d, D_Emp e'
      'where i.DepId = d.D_DepId and'
      '      i.UserId = e.D_EmpId and'
      '      i.SDATE between :BD and :ED and'
      '      i.ITYPE = 18 and'
      '      i.DepId between :DEPID1 and :DEPID2'
      'order by i.SN')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = taShopReportListBeforeDelete
    BeforeOpen = taShopReportListBeforeOpen
    OnCalcFields = taShopReportListCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 44
    Top = 92
    poSQLINT64ToBCD = True
    object taShopReportListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taShopReportListSN: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'SN'
    end
    object taShopReportListMONTHNAME: TStringField
      DisplayLabel = #1052#1077#1089#1103#1094
      FieldKind = fkCalculated
      FieldName = 'MONTHNAME'
      Calculated = True
    end
    object taShopReportListSDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taShopReportListDEPNAME: TFIBStringField
      DisplayLabel = #1052#1072#1075#1072#1079#1080#1085
      FieldName = 'DEPNAME'
      EmptyStrToNull = True
    end
    object taShopReportListDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taShopReportListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taShopReportListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taShopReportListNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taShopReportListITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taShopReportListDEPCOLOR: TFIBIntegerField
      DisplayLabel = #1062#1074#1077#1090
      FieldName = 'DEPCOLOR'
    end
    object taShopReportListCRDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1088#1072#1089#1095#1077#1090#1072
      FieldName = 'CRDATE'
    end
    object taShopReportListENTRYCOST: TFIBFloatField
      FieldName = 'ENTRYCOST'
      currency = True
    end
    object taShopReportListRESCOST: TFIBFloatField
      FieldName = 'RESCOST'
      currency = True
    end
  end
  object dsrShopReportList: TDataSource
    DataSet = taShopReportList
    Left = 68
    Top = 148
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 128
    Top = 92
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      ShortCut = 27
      OnExecute = acExitExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 85
      ShortCut = 113
      OnExecute = acRefreshExecute
      OnUpdate = acRefreshUpdate
    end
    object acFillEntryData: TAction
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1074#1093#1086#1076#1103#1097#1080#1077
      OnExecute = acFillEntryDataExecute
      OnUpdate = acFillEntryDataUpdate
    end
    object acPrintShopReport: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      OnExecute = acPrintShopReportExecute
      OnUpdate = acPrintShopReportUpdate
    end
    object acCheckData: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100
      OnExecute = acCheckDataExecute
      OnUpdate = acCheckDataUpdate
    end
    object acPrintShopReportBadUID: TAction
      Caption = #1056#1072#1089#1087#1077#1095#1072#1090#1072#1090#1100' '#1085#1077#1082#1086#1088#1088#1077#1082#1090#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
      OnExecute = acPrintShopReportBadUIDExecute
    end
    object acExport: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090
      ImageIndex = 70
      OnExecute = acExportExecute
      OnUpdate = acExportUpdate
    end
  end
  object ppDoc: TTBPopupMenu
    Left = 128
    Top = 140
    object TBItem2: TTBItem
      Action = acFillEntryData
    end
    object TBItem1: TTBItem
      Action = acCheckData
    end
    object TBItem3: TTBItem
      Action = acPrintShopReportBadUID
    end
  end
  object pmDep: TPopupMenu
    AutoHotkeys = maManual
    Left = 181
    Top = 92
    object N1: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object fms: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 180
    Top = 148
  end
  object taBadUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.UID, d.SNAME, i.SDATE, it.WHBC2, '
      '     it.Q0, it.WHEC2, it."D+C2", it."D-C2",'
      '     it.APC2, it.SELLC2, it.RETC2, it.C2 '
      'from Tmp_CheckData_ShopReport it, SInv i, D_Dep d'
      'where iseq(it.WHEC2, it.C2)=0 and'
      '      it.USERID=i.SINVID and'
      '      i.DEPID=d.D_DEPID '
      'order by d.D_DEPID, i.SDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 40
    Top = 196
    poSQLINT64ToBCD = True
    object taBadUIDUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taBadUIDSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taBadUIDSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taBadUIDWHBC2: TFIBFloatField
      FieldName = 'WHBC2'
    end
    object taBadUIDQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taBadUIDWHEC2: TFIBFloatField
      FieldName = 'WHEC2'
    end
    object taBadUIDDC2: TFIBFloatField
      FieldName = 'D+C2'
    end
    object taBadUIDDC22: TFIBFloatField
      FieldName = 'D-C2'
    end
    object taBadUIDAPC2: TFIBFloatField
      FieldName = 'APC2'
    end
    object taBadUIDSELLC2: TFIBFloatField
      FieldName = 'SELLC2'
    end
    object taBadUIDRETC2: TFIBFloatField
      FieldName = 'RETC2'
    end
    object taBadUIDC2: TFIBFloatField
      FieldName = 'C2'
    end
  end
  object frBadUID: TfrDBDataSet
    DataSet = taBadUID
    Left = 40
    Top = 244
  end
  object frShopReportList: TfrDBDataSet
    DataSet = taShopReportList
    Left = 16
    Top = 128
  end
  object pmPrint: TPopupMenu
    Left = 176
    Top = 184
    object N3: TMenuItem
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090
      OnClick = N3Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090' - '#1050#1086#1084#1080#1089#1089#1080#1103
      OnClick = N4Click
    end
    object N7: TMenuItem
      Caption = #1056#1077#1077#1089#1090#1088' - '#1050#1086#1084#1080#1089#1089#1080#1103
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090' - '#1058#1086#1083#1083#1080#1085#1075
      OnClick = N5Click
    end
  end
end
