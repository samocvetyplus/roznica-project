unit Equipment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, StdCtrls, ActnList,
  PrnDbgeh, PrntsEh, Printers, DBCtrlsEh, M207Ctrls,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmEquipment = class(TForm)
    dg1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    siExit: TSpeedItem;
    siCloseInv: TSpeedItem;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    SpeedbarSection1: TSpeedbarSection;
    siperiod: TSpeedItem;
    aclist: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acEdit: TAction;
    acClose: TAction;
    acPrint: TAction;
    acCloseInv: TAction;
    siEdit: TSpeedItem;
    prdg1: TPrintDBGridEh;
    fr1: TM207FormStorage;
    siHelp: TSpeedItem;
    procedure acEditUpdate(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siperiodClick(Sender: TObject);
    procedure acCloseInvUpdate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
   procedure ShowPeriod; 
  public
    { Public declarations }
  end;

var
  fmEquipment: TfmEquipment;

implementation

uses comdata, data3, DB, Data, M207Proc, Period, editequipment, MsgDialog;

{$R *.dfm}

procedure TfmEquipment.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.begindate) + ' �� ' +DateToStr(dm.enddate);
end;

procedure TfmEquipment.acEditUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=(not dm3.quEquipmentSINVID.IsNull) and
  (dm3.quEquipmentISCLOSED.AsInteger=0)
end;

procedure TfmEquipment.acEditExecute(Sender: TObject);
begin
 if dm3.quEquipmentISCLOSED.AsInteger=1 then MessageDialog('��������� �������', mtWarning, [mbOk], 0)
 else begin
  dm3.quEquipment.Edit;
  if ShowAndFreeForm(TfmEditEquipment, Self, TForm(fmEditEquipment), True, False)=mrok then
   PostDataSets([dm3.quEquipment])
  else CancelDataSet(dm3.quEquipment);
  dm3.quEquipment.Refresh;
 end
end;

procedure TfmEquipment.acDelExecute(Sender: TObject);
begin
 dm3.quEquipment.Refresh;
 if dm3.quEquipmentISCLOSED.AsInteger=1 then MessageDialog('��������� �������', mtWarning, [mbOk], 0)
 else if MessageDialog('������� ���������', mtInformation, [mbOK, mbCancel], 0) = mrOk then
  dm3.quEquipment.Delete;
end;

procedure TfmEquipment.acAddExecute(Sender: TObject);
begin
 dm3.quEquipment.Insert;
 if ShowAndFreeForm(TfmEditEquipment, Self, TForm(fmEditEquipment), True, False)=mrok then
  PostDataSets([dm3.quEquipment])
 else CancelDataSet(dm3.quEquipment);
 dm3.quEquipment.Refresh;
end;

procedure TfmEquipment.acCloseInvExecute(Sender: TObject);
begin
 if dm3.quEquipmentISCLOSED.AsInteger=0 then
 begin
  dm3.quEquipment.Edit;
  dm3.quEquipmentISCLOSED.AsInteger:=1;
  dm3.quEquipmentUSERID.AsInteger:=dmcom.UserId;
  dm3.quEquipment.Post;
  dm3.quEquipment.Refresh;
 end
 else
 begin
  dm3.quEquipment.Edit;
  dm3.quEquipmentISCLOSED.AsInteger:=0;
  dm3.quEquipment.Post;
  dm3.quEquipment.Refresh;
 end
end;

procedure TfmEquipment.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 prdg1.Print;
end;

procedure TfmEquipment.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 tb2.WallPaper:=wp;

 OpenDataSets([dm3.quEquipment]);

 if not dm3.quEquipmentSINVID.IsNull then
 begin
  if dm3.quEquipmentISCLOSED.AsInteger=0 then
  begin
   siCloseInv.ImageIndex:=5;
   siCloseInv.Hint:='������� ���������';
   siCloseInv.Caption:='�������';
   siCloseInv.BtnCaption:='�������';
  end
  else
  begin
   siCloseInv.ImageIndex:=6;
   siCloseInv.Hint:='������� ���������';
   siCloseInv.Caption:='�������';
   siCloseInv.BtnCaption:='�������';
  end
 end;

 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 ShowPeriod;
end;

procedure TfmEquipment.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm3.quEquipment]);
end;

procedure TfmEquipment.siperiodClick(Sender: TObject);
begin
 if GetPeriod(dm.BeginDate, dm.EndDate) then
  begin
   ReOpenDataSets([dm3.quEquipment]);
   ShowPeriod;
  end;
end;

procedure TfmEquipment.acCloseInvUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=not dm3.quEquipmentSINVID.IsNull;
 if not dm3.quEquipmentSINVID.IsNull then
 begin
  if dm3.quEquipmentISCLOSED.AsInteger=0 then
  begin
   siCloseInv.ImageIndex:=5;
   siCloseInv.Hint:='������� ���������';
   siCloseInv.Caption:='�������';
   siCloseInv.BtnCaption:='�������';
  end
  else
  begin
   siCloseInv.ImageIndex:=6;
   siCloseInv.Hint:='������� ���������';
   siCloseInv.Caption:='�������';
   siCloseInv.BtnCaption:='�������';
  end
 end
end;

procedure TfmEquipment.acCloseExecute(Sender: TObject);
begin
 PostDataSet(dm3.quEquipment);
 CloseDataSets([dm3.quEquipment]); 
 close;
end;

procedure TfmEquipment.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmEquipment.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if not dm3.quEquipmentSINVID.IsNull then
 begin
  if column.FieldName='DEPNAME' then Background:=dm3.quEquipmentCOLOR.AsInteger
  else
  if dm3.quEquipmentISCLOSED.AsInteger=1 then Background:=clBtnFace
  else Background:=clWindow;
 end
end;

procedure TfmEquipment.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100392);
end;

procedure TfmEquipment.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
