unit SelectDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ActnList;

type
  TfmSelectDep = class(TForm)
    grDep: TRadioGroup;
    plButton: TPanel;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    acList: TActionList;
    acEsc: TAction;
    acEnter: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acEscExecute(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelectDep: TfmSelectDep;

implementation
uses data, data3;
{$R *.dfm}

procedure TfmSelectDep.FormCreate(Sender: TObject);
begin
 with dm do
 begin
  qutmp.Close;
  qutmp.SQL.Text:='select d_depid, Sname from d_dep where centerdep<>1 and d_depid<>-1000';
  qutmp.ExecQuery;
  while not qutmp.Eof do
  begin
   grDep.Items.Add(qutmp.Fields[1].asString);
   qutmp.Next;
  end;
  qutmp.Close;
  qutmp.Transaction.CommitRetaining;
  grDep.ItemIndex:=0;
 end;
end;

procedure TfmSelectDep.acEscExecute(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TfmSelectDep.acEnterExecute(Sender: TObject);
begin
 ModalResult:=mrOk;
end;

procedure TfmSelectDep.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 if ModalResult=mrok then
 begin
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select d_depid from d_dep where sname='#39+grDep.Items[grDep.ItemIndex]+#39;
  dm.quTmp.ExecQuery;
  dm3.DinvDepId:=dm.qutmp.Fields[0].asinteger;
  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
 end
end;

end.
