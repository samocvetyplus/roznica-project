unit RecSet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, RxDBComb, ExtCtrls,
  db, RXDBCtrl, rxToolEdit, rxCurrEdit;

type
  TfmRecSet = class(TForm)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    RxDBComboBox1: TRxDBComboBox;
    Label4: TLabel;
    lcRestWH: TDBLookupComboBox;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    RxDBCalcEdit1: TRxDBCalcEdit;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rdgrPreviewClick(Sender: TObject);
    procedure chbxReCalcClick(Sender: TObject);
    procedure deDocAfterDialog(Sender: TObject; var Name: String;
      var Action: Boolean);
  private
  public
  end;

var
  fmRecSet: TfmRecSet;

implementation

uses comdata, data, M207Proc;

{$R *.DFM}

procedure TfmRecSet.FormCreate(Sender: TObject);
begin
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      OpenDataSets([quDep,taRec]);
      SetCBDropDown(lcRestWH);
    end;
end;

procedure TfmRecSet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      if ModalResult=mrOK then
      begin
        if not(taRec.State in [dsEdit, dsInsert]) then taRec.Edit;
        PostDataSets([dmCom.taRec])
      end
      else CancelDataSet(dmCom.taRec);
      TmpDir:=taRecTmpDir.AsString;
      TmpDB:=taRecTmpDB.AsString;
      PTrNds:=taRecPTrNds.AsFloat;
      MaxCountWH:=taRecMAXCOUNTWH.AsInteger;
      MaxCountUIDWH:=taRecMAXCOUNTUIDWH.AsInteger;
      CloseDataSets([quDep, taRec]);
      tr.CommitRetaining;
    end;
end;

procedure TfmRecSet.rdgrPreviewClick(Sender: TObject);
begin
//  FIni.WriteInteger('Report', 'Prv', rdgrPreview.ItemIndex);
  //DocPreview := TdcPreview(rdgrPreview.ItemIndex);
end;

procedure TfmRecSet.chbxReCalcClick(Sender: TObject);
begin
  //FIni.WriteBool('Sum', 'IsReCalc', chbxReCalc.Checked);
  //IsReCalc := chbxReCalc.Checked;
end;

procedure TfmRecSet.deDocAfterDialog(Sender: TObject; var Name: String;
  var Action: Boolean);
begin
  {if not(dmCom.taRec.State in [dsEdit, dsInsert]) then dmCom.taRec.Edit;
  dmCom.taRecDOCPATH.AsString := Name;
  dmCom.taRec.Post;}
  Action := True;
end;

end.
