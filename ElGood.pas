unit ElGood;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Grids, DBGrids, RXDBCtrl, Placemnt, SpeedBar, ExtCtrls;

type
  TfmElGood = class(TForm)
    StatusBar1: TStatusBar;
    RxDBGrid1: TRxDBGrid;
    FormStorage1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siExit: TSpeedItem;
    procedure ToolButton3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;    
  public
    { Public declarations }
  end;

var
  fmElGood: TfmElGood;

implementation

uses Data, comdata;

{$R *.DFM}

procedure TfmElGood.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmElGood.ToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmElGood.FormActivate(Sender: TObject);
begin
  Caption:=dm.taElName.AsString;
  dm.taElGood.Active:=True;
end;

procedure TfmElGood.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      if taElGood.RecordCount<>taElQuantity.AsInteger then
        if MessageDlg('�o�������� ��������� ���.������� �� ����� ���������� ������ � ���������. ����������?',
                      mtConfirmation,  [mbOK, mbCancel], 0)= mrCancel then Sysutils.Abort;
      PostDataSet(taElGood);
      taElGood.Active:=False;
    end;
end;

procedure TfmElGood.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
end;

procedure TfmElGood.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

end.
