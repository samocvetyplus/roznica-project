unit f1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, Placemnt, ComCtrls,
  StdCtrls, SpeedBar, ExtCtrls;

type
  Tfm1 = class(TForm)
    FormStorage1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    laDep: TLabel;
    StatusBar1: TStatusBar;
    M207IBGrid1: TM207IBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fm1: Tfm1;

implementation

uses comdata, Data;

{$R *.DFM}


procedure Tfm1.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure Tfm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      tr.Commit;
      taArt2.Active:=False;
    end
end;

procedure Tfm1.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  with dmCom, dm do
    begin
      tr.StartTransaction;
    end
end;

procedure Tfm1.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure Tfm1.FormActivate(Sender: TObject);
begin
  dm.pmArt2.Items[0].Click;
end;

procedure Tfm1.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

end.

