unit SuspItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, M207Ctrls,
  ActnList, DBCtrls, StdCtrls, Mask, PrnDbgeh, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmSuspItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siDel: TSpeedItem;
    siHelp: TSpeedItem;
    siExit: TSpeedItem;
    plInfo: TPanel;
    dg1: TDBGridEh;
    fr1: TM207FormStorage;
    acList: TActionList;
    acDel: TAction;
    edSn: TDBEdit;
    dbUser: TDBLookupComboBox;
    LNumInv: TLabel;
    LUser: TLabel;
    Luid: TLabel;
    edUid: TEdit;
    acClose: TAction;
    acHelp: TAction;
    prdg1: TPrintDBGridEh;
    acPrint: TAction;
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure edUidKeyPress(Sender: TObject; var Key: Char);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
  end;

var
  fmSuspItem: TfmSuspItem;

implementation
uses comdata, data, data3, pFIBQuery, FIBQuery, M207Proc, dbUtil, MsgDialog;
{$R *.dfm}

procedure TfmSuspItem.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (not dm3.taSuspItemSITEMID.IsNull) and
  (dm3.taSuspItemListDEPID.AsInteger = SelfDepId);
end;

procedure TfmSuspItem.acDelExecute(Sender: TObject);
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then  SysUtils.Abort;
  dm3.taSuspItem.Delete;
end;

procedure TfmSuspItem.edUidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8: ;
  else sysUtils.Abort;
 end
end;

procedure TfmSuspItem.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:integer;
begin
 case key of
  VK_DOWN : ActiveControl := dg1;
  VK_RETURN: begin
   if edUid.Text='' then raise Exception.Create('������� ��. �����');

   with dm, qutmp do
   begin
    close;
    sql.Text:='select  fres from suspitem_i ('+edUid.Text+', '+dm3.taSuspItemListSINVID.AsString+')';
    ExecQuery;
    i:=Fields[0].AsInteger;
    Transaction.CommitRetaining;
    close;
   end;

   case i of
    0: begin
        ReOpenDataSets([dm3.taSuspItem]);
        dm3.taSuspItem.Locate('UID', edUid.Text, []);
        ActiveControl:= edUid;
        dg1.SumList.RecalcAll;
       end;
    1: MessageDialog('������� �������', mtInformation, [mbOk], 0);
    2: MessageDialog('������� �� �������', mtInformation, [mbOk], 0);
    3: MessageDialog('������� ��� ��������', mtInformation, [mbOk], 0);
   end;
   eduid.Text:='';
  end;
 end;
end;

procedure TfmSuspItem.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 ReOpenDataSets([dm3.taSuspItem, dmcom.taEmp]);
 ActiveControl:= edUid;
end;

procedure TfmSuspItem.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSuspItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSuspItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 PostDataSets ([dm3.taSuspItem, dm3.taSuspItemList]);
 if not dm.closeinv(dm3.taSuspItemListSINVID.AsInteger,0) then
 begin
  if dm.EmptyInv(dm3.taSuspItemListSINVID.AsInteger,0) then
  begin
   ExecSQL('delete from sinv where sinvid='+dm3.taSuspItemListSINVID.AsString, dm.qutmp);
   ReOpenDataSet(dm3.taSuspItemList);
  end
 end;
 dm3.taSuspItemList.Refresh;
 CloseDataSets([dm3.taSuspItem, dmcom.taEmp]);
end;

procedure TfmSuspItem.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmSuspItem.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  if column.FieldName='SCOST' then Background:=clAqua;
  if column.FieldName='SPRICE' then Background:=clAqua;
  if column.FieldName='PRORDCODE' then Background:=clMoneyGreen;
  if column.FieldName='ART' then Background:=clMoneyGreen;
  if column.FieldName='D_MATID' then Background:=clMoneyGreen;
  if column.FieldName='D_GOODID' then Background:=clMoneyGreen;
  if column.FieldName='D_INSID' then Background:=clMoneyGreen;
  if column.FieldName='UID' then Background:=clBtnFace;    
 end
end;

procedure TfmSuspItem.acPrintExecute(Sender: TObject);
begin
 prdg1.Print;
end;

procedure TfmSuspItem.acPrintUpdate(Sender: TObject);
begin
 TAction (Sender).Enabled:= (not dm3.taSuspItemSITEMID.IsNull)
end;

end.
