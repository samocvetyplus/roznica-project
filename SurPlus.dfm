object fmSurPlus: TfmSurPlus
  Left = 168
  Top = 139
  Width = 897
  Height = 480
  HelpContext = 100204
  Caption = #1048#1079#1083#1080#1096#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 889
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDelete
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acDeleteExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      Action = acCloseInv
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acCloseInvExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1041#1080#1088#1082#1080
      Caption = #1041#1080#1088#1082#1080
      Hint = #1041#1080#1088#1082#1080'|'
      ImageIndex = 67
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 818
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 754
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object paHeader: TPanel
    Left = 0
    Top = 40
    Width = 889
    Height = 89
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 4
      Top = 28
      Width = 83
      Height = 13
      Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 4
      Top = 52
      Width = 112
      Height = 13
      Caption = #1044#1072#1090#1072' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 4
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 244
      Top = 8
      Width = 28
      Height = 13
      Caption = #1055#1086#1089#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 244
      Top = 36
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 532
      Top = 54
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 632
      Top = 54
      Width = 121
      Height = 17
      DataField = 'TOTALCOST'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 280
      Top = 36
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm.dsSList
    end
    object Label16: TLabel
      Left = 532
      Top = 29
      Width = 59
      Height = 13
      Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 532
      Top = 8
      Width = 62
      Height = 13
      Caption = #1054#1073#1097#1077#1077' '#1082'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 632
      Top = 29
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TW'
      DataSource = dm.dsSList
    end
    object DBText5: TDBText
      Left = 632
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TQ'
      DataSource = dm.dsSList
    end
    object dbSup: TDBText
      Left = 277
      Top = 8
      Width = 31
      Height = 13
      AutoSize = True
      DataField = 'SUP'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText7: TDBText
      Left = 632
      Top = 68
      Width = 113
      Height = 17
      DataField = 'Cost2'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 532
      Top = 70
      Width = 79
      Height = 13
      Caption = #1074' '#1088#1072#1089#1093#1086#1076#1085#1099#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object dbSsf: TDBText
      Left = 244
      Top = 64
      Width = 265
      Height = 17
      DataField = 'SSF'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edSN: TM207DBEdit
      Left = 92
      Top = 4
      Width = 133
      Height = 21
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsSList
      TabOrder = 0
    end
    object deNDate: TDBDateEdit
      Left = 124
      Top = 48
      Width = 101
      Height = 21
      DataField = 'NDATE'
      DataSource = dm.dsSList
      Color = clAqua
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
    end
    object dbedSdate: TDBEditEh
      Left = 92
      Top = 25
      Width = 114
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsSList
      EditButtons = <>
      ReadOnly = True
      TabOrder = 2
      Visible = True
    end
    object btDate: TBitBtn
      Left = 206
      Top = 25
      Width = 20
      Height = 22
      TabOrder = 3
      OnClick = btDateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 164
    Width = 889
    Height = 287
    Align = alClient
    AllowedOperations = []
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsSurPlusItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'Rno'
        Footers = <>
        Title.Caption = #8470
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1048#1079#1075'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1052#1072#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1053#1072#1080#1084'. '#1080#1079#1076'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1054#1042
      end
      item
        EditButtons = <>
        FieldName = 'D_COUNTRYID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1057#1090#1088'.'
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1040#1088#1090#1080#1082#1091#1083
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
      end
      item
        EditButtons = <>
        FieldName = 'Unit'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.'
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'|'#1074' '#1087#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'PRICE2'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'|'#1074' '#1088#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SCost'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1074' '#1087#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'Cost2'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1074' '#1088#1072#1089#1093'.'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object puid: TPanel
    Left = 0
    Top = 129
    Width = 889
    Height = 35
    Align = alTop
    TabOrder = 3
    object Label3: TLabel
      Left = 61
      Top = 13
      Width = 52
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object eduid: TEdit
      Left = 120
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = eduidKeyDown
      OnKeyPress = eduidKeyPress
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 616
    Top = 248
    object acDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDeleteExecute
      OnUpdate = acDeleteUpdate
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
      OnUpdate = acPrintUpdate
    end
    object acCloseInv: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      OnExecute = acCloseInvExecute
    end
    object acOpenInv: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 6
      OnExecute = acOpenInvExecute
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 720
    Top = 248
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 568
    Top = 224
  end
end
