{******************************************************************************
 * ����� ������������� ��� ������� ������, ����������� ������ �� ���������
 * ������� �� ��������� ������.
 * ������ ������ ������������ ��� ������� �.�.�. � ������� �������� �����������
 * ���������.
 * ���������� ����� ����������� ������ �� ������.

 * � ����������� �������� ��������� ������ � ������������ �������� ����������
 * ���������� � �������������� ��������� ������ � ����������� �� ����������.
 *
 ������� - ����� �. 2010.04.05 -
 ******************************************************************************}
unit AnlzSelEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar;

type
  TfmAnlzSelEmp = class(TForm)
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem4: TSpeedItem;
    quAnlzSelEmp: TpFIBDataSet;
    dsAnlzSelEmp: TDataSource;
    dg1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siPrint: TSpeedItem;
    quAnlzSelEmpSEL_Q: TFIBIntegerField;
    quAnlzSelEmpSEL_COST: TFIBFloatField;
    quAnlzSelEmpSELLERNAME: TFIBStringField;
    quAnlzSelEmpCASH_Q: TFIBIntegerField;
    quAnlzSelEmpCASH_COST: TFIBIntegerField;
    quAnlzSelEmpUSERID: TFIBIntegerField;
    procedure quAnlzSelEmpBeforeOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siPrintClick(Sender: TObject);
  private

    { Private declarations }
  public
 
    { Public declarations }
  end;

var
  fmAnlzSelEmp: TfmAnlzSelEmp;

implementation

uses comdata, M207Proc, Data, Period, ReportData;
{$R *.dfm}

procedure TfmAnlzSelEmp.quAnlzSelEmpBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params, dm do
    begin
      if SDepId=-1 then
        begin
          ByName['i_depid'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['i_depid'].AsInteger:=SDepId;

        end;
      ByName['i_bd'].AsString:=DateTimeToStr(BeginDate);
      ByName['i_ed'].AsString:=DateTimeToStr(EndDate);
      ByName['i_userid'].AsInteger:=dmCom.UserId;
     end;

end;

procedure TfmAnlzSelEmp.FormActivate(Sender: TObject);
begin
dm.WorkMode:='AnlzSelEmp';
ladep.Caption:='';
   dm.ClickUserDepMenu(dm.pmAnlzSelDed);
     with dmCom, dm do
    begin
      SetBeginDate;
      laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
    end;
end;


procedure TfmAnlzSelEmp.SpeedItem4Click(Sender: TObject);
begin
 if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([quAnlzSelEmp]);
      laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);

    end;
end;

procedure TfmAnlzSelEmp.siExitClick(Sender: TObject);
begin
closedatasets([quAnlzSelEmp]);
quAnlzSelEmp.Active:=false;
   close;
end;

procedure TfmAnlzSelEmp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   dm.WorkMode:='';
   CloseDataSets([quAnlzSelEmp]);
end;

procedure TfmAnlzSelEmp.siPrintClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(dg1, quAnlzselEmpUserID);
  try
    PrintDocument(arr, report_sel_emp);
  finally
    Finalize(arr);
  end;
end;

end.

