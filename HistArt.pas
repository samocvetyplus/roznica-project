unit HistArt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, StdCtrls,
  ComCtrls, db, DBCtrls, rxSpeedbar;

type
  TfmHistArt = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    lbFind: TLabel;
    edFind: TEdit;
    SpeedbarSection2: TSpeedbarSection;
    spitFindFirst: TSpeedItem;
    spitFindNext: TSpeedItem;
    grHistArt: TM207IBGrid;
    grItem: TM207IBGrid;
    tb3: TSpeedBar;
    Splitter1: TSplitter;
    stbrHistArt: TStatusBar;
    spitHelp: TSpeedItem;
    plTotal: TPanel;
    Label1: TLabel;
    lbR: TLabel;
    lbSel: TLabel;
    txtD: TDBText;
    txtS: TDBText;
    txtR: TDBText;
    Label2: TLabel;
    txtRet: TDBText;
    Label4: TLabel;
    txtC: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edFindKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFindKeyPress(Sender: TObject; var Key: Char);
    procedure spitFindFirstClick(Sender: TObject);
    procedure spitFindNextClick(Sender: TObject);
    procedure grHistArtGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    FSearchEnable : boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure FindFirst;
    procedure FindNext;
  public
    { Public declarations }
  end;

var
  fmHistArt: TfmHistArt;

implementation

uses ServData, comdata, Data, M207Proc, MsgDialog;

{$R *.DFM}

procedure TfmHistArt.FindFirst;
begin
  dmServ.quHistArt.First;
  while not dmServ.quHistArt.Eof do
  begin
    if dmServ.quHistItem.Locate('R_UID', edFind.Text, [])
    then begin
      FSearchEnable := True;
      exit;
    end;
    dmServ.quHistArt.Next;
  end;
  MessageDialog('������� �� �������', mtWarning, [mbOk], 0);
  FSearchEnable := False;
end;

procedure TfmHistArt.FindNext;
begin
  dmServ.quHistItem.Next;
  while not dmServ.quHistArt.Eof do
  begin
    if dmServ.quHistItem.FindNext then
       if dmServ.quHistItem.LocateNext('R_UID', edFind.Text, []) then
       begin
         FSearchEnable := True;
         exit;
       end
       else begin
       end
    else begin
      dmServ.quHistArt.Next;
      if dmServ.quHistItem.Locate('R_UID', edFind.Text, [])
      then begin
        FSearchEnable := True;
        exit;
      end;
    end;
  end;
end;

procedure TfmHistArt.FormCreate(Sender: TObject);
var
  d,m,y : word;
begin
  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;
  tb3.Wallpaper := wp;
  if not dmCom.tr.Active then dmCom.tr.StartTransaction
  else dmCom.tr.CommitRetaining;
  Caption := '������� ��������';
  dmServ.quHistItem.DataSource := dmServ.dsrHistArt;
  DecodeDate(dm.BeginDate, y, m, d);
  dmServ.HistBD := EncodeDate(y-1, m, d);
  dmServ.HistED := dm.EndDate;
  stbrHistArt.SimpleText := '������: c '+ DateToStr(dmServ.HistBD)+
    ' �� '+DateToStr(dmServ.HistED);
  OpenDataSets([dmServ.quHistArt, dmServ.quHistItem, dmServ.quHistArtSum]);
  FSearchEnable := False;
  edFind.Text := '';
end;

procedure TfmHistArt.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmHistArt.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmHistArt.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmHistArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  CloseDataSets([dmServ.quHistArt, dmServ.quHistItem,dmServ.quHistArtSum]);
  Action := caFree;
end;

procedure TfmHistArt.edFindKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN : ActiveControl := grHistArt;
  end;
end;

procedure TfmHistArt.edFindKeyPress(Sender: TObject; var Key: Char);
begin
  case key of
    #13 : spitFindFirstClick(nil);
  end;
end;

procedure TfmHistArt.spitFindFirstClick(Sender: TObject);
begin
  dmServ.UID := StrToIntDef(edFind.Text, 0);
  if dmServ.UID = 0 then
  begin
    ActiveControl := edFind;
    raise Exception.Create('������� ���������� ����������������� �����!');
  end;
  FindFirst;
end;

procedure TfmHistArt.spitFindNextClick(Sender: TObject);
begin
  if edFind.Text='' then eXit;
  FindNext;
end;

procedure TfmHistArt.grHistArtGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  with grHistArt.Canvas do
  begin
    if Field.FieldName  = 'Descr' then
    begin
      case dmServ.quHistArtR_I.AsInteger of
        1  : Background := dm.CU_SBD1; // ������
        2  : Background := dm.CU_D1;   // ��
        3  : Background := dm.CU_SO1;  // ������� �������
        5  : Background := dm.CU_R;    // ������ (�������)
        6  : Background := dm.CU_RT1;  // ������ (������� �� ����������)
        7  : Background := dm.CU_SR1;  // ������� �����������
        8  : Background := dm.CU_RO1;  // ������� �� ���. �����������
        10 : Background := dm.CU_SL;   // ��������� �������;
      end;
    end;
  end;
end;

end.
