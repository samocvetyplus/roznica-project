object fmDItem: TfmDItem
  Left = 260
  Top = 108
  AutoScroll = False
  Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1076#1083#1103' '#1074#1085#1091#1090#1088#1077#1085#1085#1077#1075#1086' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
  ClientHeight = 568
  ClientWidth = 786
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 439
    Top = 67
    Width = 2
    Height = 482
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 549
    Width = 786
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 786
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 467
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = sbAddClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = sbDelClick
      SectionName = 'Untitled (0)'
    end
    object siPrice: TSpeedItem
      BtnCaption = #1062#1077#1085#1099
      Caption = #1062#1077#1085#1099
      Hint = #1062#1077#1085#1099
      ImageIndex = 32
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siPriceClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 603
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 441
    Top = 67
    Width = 36
    Height = 482
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 2
    object sbAdd: TSpeedButton
      Left = 8
      Top = 116
      Width = 23
      Height = 22
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FF3333333333333447333333333333377FFF33333333333744473333333
        333337773FF3333333333444447333333333373F773FF3333333334444447333
        33333373F3773FF3333333744444447333333337F333773FF333333444444444
        733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
        999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
        33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
        333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
        33333777333333333333CC333333333333337733333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbAddClick
    end
    object sbDel: TSpeedButton
      Left = 8
      Top = 148
      Width = 23
      Height = 22
      Hint = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF3333333333333744333333333333F773333333333337
        44473333333333F777F3333333333744444333333333F7733733333333374444
        4433333333F77333733333333744444447333333F7733337F333333744444444
        433333F77333333733333744444444443333377FFFFFFF7FFFFF999999999999
        9999733777777777777333CCCCCCCCCC33333773FF333373F3333333CCCCCCCC
        C333333773FF3337F333333333CCCCCCC33333333773FF373F3333333333CCCC
        CC333333333773FF73F33333333333CCCCC3333333333773F7F3333333333333
        CCC333333333333777FF33333333333333CC3333333333333773}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = sbDelClick
    end
  end
  object Panel2: TPanel
    Left = 477
    Top = 67
    Width = 309
    Height = 482
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object sp1: TSplitter
      Left = 0
      Top = 209
      Width = 309
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object paDI: TPanel
      Left = 0
      Top = 0
      Width = 309
      Height = 209
      Align = alClient
      Caption = 'paDI'
      TabOrder = 0
      object paDepTo: TPanel
        Left = 1
        Top = 1
        Width = 307
        Height = 21
        Align = alTop
        BevelOuter = bvLowered
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object dg1: TM207IBGrid
        Left = 1
        Top = 22
        Width = 307
        Height = 166
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsDItem
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        PopupMenu = pm2
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = sbDelClick
        FixedCols = 1
        OnGetCellParams = dgWHGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = False
        Columns = <
          item
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 119
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 81
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'SPRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'BUSYTYPE'
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 1
        Top = 188
        Width = 307
        Height = 20
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 2
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 62
          Height = 13
          Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086':'
        end
        object laQ: TLabel
          Left = 76
          Top = 4
          Width = 16
          Height = 13
          Caption = 'laQ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 140
          Top = 4
          Width = 22
          Height = 13
          Caption = #1042#1077#1089':'
        end
        object laW: TLabel
          Left = 164
          Top = 4
          Width = 19
          Height = 13
          Caption = 'laW'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object paRest: TPanel
      Left = 0
      Top = 212
      Width = 309
      Height = 270
      Align = alBottom
      Caption = 'paRest'
      TabOrder = 1
      object paRest1: TPanel
        Left = 1
        Top = 1
        Width = 307
        Height = 17
        Align = alTop
        BevelOuter = bvLowered
        Caption = #1054#1089#1090#1072#1090#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object pc1: TPageControl
        Left = 1
        Top = 18
        Width = 307
        Height = 251
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = #1056#1072#1079#1084#1077#1088#1099
          object dgSZ: TM207IBGrid
            Left = 0
            Top = 0
            Width = 299
            Height = 223
            Align = alClient
            Color = clBtnFace
            DataSource = dm2.dsAllDepSz
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            IniStorage = FormStorage1
            TitleButtons = True
            OnGetCellParams = dgSZGetCellParams
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = True
            SortOnTitleClick = True
            Columns = <
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SZ'
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1079#1084#1077#1088
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 45
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'DEP'
                Title.Alignment = taCenter
                Title.Caption = #1057#1082#1083#1072#1076
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'QUANTITY'
                Title.Alignment = taCenter
                Title.Caption = #1050#1086#1083'-'#1074#1086
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 56
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'WEIGHT'
                Title.Alignment = taCenter
                Title.Caption = #1042#1077#1089
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 61
                Visible = True
              end>
          end
        end
        object TabSheet2: TTabSheet
          Caption = #1048#1079#1076#1077#1083#1080#1103
          ImageIndex = 1
          object dgUID: TM207IBGrid
            Left = 0
            Top = 0
            Width = 299
            Height = 223
            Align = alClient
            Color = clBtnFace
            DataSource = dm2.dsAllDepUID
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
            ReadOnly = True
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleButtons = True
            OnGetCellParams = dgUIDGetCellParams
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = True
            SortOnTitleClick = True
            Columns = <
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'UID'
                Title.Alignment = taCenter
                Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 54
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SZ'
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1079#1084#1077#1088
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 53
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'W'
                Title.Alignment = taCenter
                Title.Caption = #1042#1077#1089
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 73
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'DEP'
                Title.Alignment = taCenter
                Title.Caption = #1057#1082#1083#1072#1076
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 74
                Visible = True
              end>
          end
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 42
    Width = 786
    Height = 25
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 4
    object Label2: TLabel
      Left = 6
      Top = 6
      Width = 29
      Height = 13
      Caption = #1062#1077#1085#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 166
      Top = 6
      Width = 61
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 378
      Top = 6
      Width = 44
      Height = 13
      Caption = #1055#1088'.'#1094#1077#1085#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laSup: TLabel
      Left = 228
      Top = 6
      Width = 27
      Height = 13
      Caption = 'laSup'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laPrice: TLabel
      Left = 424
      Top = 6
      Width = 27
      Height = 13
      Caption = 'laSup'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 691
      Top = 8
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'ART'
      DataSource = dm.dsSEl
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBEdit1: TDBEdit
      Left = 40
      Top = 2
      Width = 63
      Height = 21
      Color = clInfoBk
      DataField = 'PRICE2'
      DataSource = dm.dsSEl
      TabOrder = 0
    end
  end
  object Panel6: TPanel
    Left = 0
    Top = 67
    Width = 439
    Height = 482
    Align = alLeft
    Caption = 'Panel6'
    TabOrder = 5
    object Label6: TLabel
      Left = 6
      Top = 465
      Width = 62
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086':'
    end
    object Label7: TLabel
      Left = 198
      Top = 466
      Width = 22
      Height = 13
      Caption = #1042#1077#1089':'
    end
    object DBText1: TDBText
      Left = 72
      Top = 466
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'Q'
      DataSource = dm2.dsWHA2UIDT
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 222
      Top = 466
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'W'
      DataSource = dm2.dsWHA2UIDT
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dgWH: TM207IBGrid
      Left = 1
      Top = 17
      Width = 437
      Height = 440
      Align = alTop
      Anchors = [akLeft, akTop, akRight, akBottom]
      Color = clBtnFace
      DataSource = dm.dsWHA2UID
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
      PopupMenu = pm1
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = sbAddClick
      FixedCols = 1
      TitleButtons = True
      OnGetCellParams = dgWHGetCellParams
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = True
      SortOnTitleClick = True
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'UID'
          Title.Alignment = taCenter
          Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 57
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'W'
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 37
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'SZ'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1079#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 44
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'SUP'
          Title.Alignment = taCenter
          Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 128
          Visible = True
        end
        item
          Color = clAqua
          Expanded = False
          FieldName = 'PRICE'
          Title.Alignment = taCenter
          Title.Caption = #1055#1088'.'#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'NDSNAME'
          Title.Alignment = taCenter
          Title.Caption = #1053#1044#1057
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 67
          Visible = True
        end>
    end
    object paDepFrom: TPanel
      Left = 1
      Top = 1
      Width = 437
      Height = 16
      Align = alTop
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 160
    Top = 188
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      GroupIndex = 1
      ImageIndex = 1
      ShortCut = 13
      OnClick = sbAddClick
    end
    object miSupFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1091
      GroupIndex = 1
      ShortCut = 116
      OnClick = SetFilter
    end
    object miPriceFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1094#1077#1085#1077
      GroupIndex = 1
      ShortCut = 117
      OnClick = SetFilter
    end
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 160
    Top = 232
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 13
      OnClick = sbDelClick
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel6.Width'
      'paRest.Height')
    StoredValues = <>
    Left = 160
    Top = 278
  end
  object acEvent: TActionList
    Left = 228
    Top = 190
    object acShowId: TAction
      ShortCut = 24649
      OnExecute = acShowIdExecute
    end
  end
end
