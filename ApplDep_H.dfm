object fmApplDep_H: TfmApplDep_H
  Left = 226
  Top = 56
  Cursor = crArrow
  ActiveControl = LogEd
  BorderStyle = bsToolWindow
  ClientHeight = 387
  ClientWidth = 519
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 120
    Width = 134
    Height = 19
    Caption = #1048#1089#1090#1086#1088#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaLogin: TLabel
    Left = 26
    Top = 320
    Width = 78
    Height = 15
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaPsw: TLabel
    Left = 58
    Top = 357
    Width = 44
    Height = 15
    Caption = #1055#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object OpSBtn: TSpeedButton
    Left = 272
    Top = 322
    Width = 73
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    OnClick = OpSBtnClick
  end
  object ClSBtn: TSpeedButton
    Left = 356
    Top = 322
    Width = 81
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    OnClick = ClSBtnClick
  end
  object CnlSBtn: TSpeedButton
    Left = 446
    Top = 322
    Width = 65
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    OnClick = CnlSBtnClick
  end
  object Label9: TLabel
    Left = 8
    Top = 288
    Width = 294
    Height = 17
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1074#1086#1077' '#1080#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1087#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 144
    Width = 521
    Height = 137
    Color = clBtnHighlight
    DataGrouping.GroupLevels = <>
    DataSource = dsrDlist_H
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 185
      end
      item
        DisplayFormat = 'dd.mm.yyyy  hh:nn'
        EditButtons = <>
        FieldName = 'HDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Width = 154
      end
      item
        EditButtons = <>
        FieldName = 'STATUS'
        Footers = <>
        Title.Caption = #1044#1077#1081#1089#1090#1074#1080#1077
        Width = 161
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object PswEd: TEdit
    Left = 109
    Top = 351
    Width = 145
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = PswEdKeyDown
  end
  object LogEd: TEdit
    Left = 109
    Top = 318
    Width = 145
    Height = 21
    TabOrder = 2
    OnKeyDown = LogEdKeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 519
    Height = 68
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    object Label2: TLabel
      Left = 24
      Top = 8
      Width = 61
      Height = 13
      Caption = #1047#1072#1082#1072#1079#1072#1085#1086' '#1089':'
    end
    object Label3: TLabel
      Left = 19
      Top = 24
      Width = 67
      Height = 13
      Caption = #1047#1072#1082#1072#1079#1072#1085#1086' '#1085#1072':'
    end
    object DBText1: TDBText
      Left = 96
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SNAMEFROM'
      DataSource = dm3.dsApplDepList
    end
    object DBText2: TDBText
      Left = 96
      Top = 24
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SNAME'
      DataSource = dm3.dsApplDepList
    end
    object Label4: TLabel
      Left = 8
      Top = 40
      Width = 80
      Height = 13
      Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103':'
    end
    object DBText3: TDBText
      Left = 96
      Top = 40
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'SDATE'
      DataSource = dm3.dsApplDepList
    end
    object Label6: TLabel
      Left = 428
      Top = 12
      Width = 37
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 444
      Top = 36
      Width = 22
      Height = 13
      Caption = #1042#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText6: TDBText
      Left = 471
      Top = 12
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'C'
      DataSource = dm3.dsApplDepList
    end
    object DBText7: TDBText
      Left = 471
      Top = 36
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'W'
      DataSource = dm3.dsApplDepList
    end
    object Panel5: TPanel
      Left = 169
      Top = 1
      Width = 72
      Height = 66
      BevelOuter = bvLowered
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 241
      Top = 1
      Width = 184
      Height = 66
      BevelOuter = bvLowered
      TabOrder = 1
      object Label8: TLabel
        Left = 4
        Top = 12
        Width = 69
        Height = 13
        Caption = #1056#1072#1089#1093'. '#1094#1077#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 4
        Top = 36
        Width = 70
        Height = 13
        Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBText4: TDBText
        Left = 95
        Top = 12
        Width = 42
        Height = 13
        AutoSize = True
        DataField = 'COST'
        DataSource = dm3.dsApplDepList
      end
      object DBText5: TDBText
        Left = 95
        Top = 36
        Width = 42
        Height = 13
        AutoSize = True
        DataField = 'COSTP'
        DataSource = dm3.dsApplDepList
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 72
    Width = 519
    Height = 33
    BevelOuter = bvNone
    TabOrder = 4
    object Panel4: TPanel
      Left = 0
      Top = -3
      Width = 89
      Height = 34
      BevelOuter = bvLowered
      Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel6: TPanel
      Left = 89
      Top = -2
      Width = 136
      Height = 33
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      object DBText8: TDBText
        Left = 2
        Top = 9
        Width = 121
        Height = 17
        DataField = 'COMMENT'
        DataSource = dm3.dsApplDepList
      end
    end
    object Panel7: TPanel
      Left = 225
      Top = -3
      Width = 89
      Height = 34
      BevelOuter = bvLowered
      Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel8: TPanel
      Left = 314
      Top = -2
      Width = 203
      Height = 33
      BevelOuter = bvLowered
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      object DBText9: TDBText
        Left = 10
        Top = 9
        Width = 121
        Height = 17
        DataField = 'RSTATE_S'
        DataSource = dm3.dsApplDepList
      end
    end
  end
  object fs1: TFormStorage
    Active = False
    Options = [fpState]
    UseRegistry = False
    StoredValues = <>
    Left = 468
    Top = 108
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :INVID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   17'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 17'
      'order by HDATE')
    BeforeOpen = taHistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 344
    Top = 112
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 312
    Top = 112
  end
  object taEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ALIAS, FIO, PSWD'
      'FROM D_EMP')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 424
    Top = 112
    object taEmpALIAS: TFIBStringField
      FieldName = 'ALIAS'
      EmptyStrToNull = True
    end
    object taEmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taEmpPSWD: TFIBStringField
      FieldName = 'PSWD'
      EmptyStrToNull = True
    end
  end
  object dsEmp: TDataSource
    DataSet = taEmp
    Left = 392
    Top = 112
  end
end
