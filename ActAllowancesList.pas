unit ActAllowancesList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGridEh, ActnList, Menus,
  TB2Item, M207Ctrls, jpeg, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar, FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TfmActAllowancesList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siEdit: TSpeedItem;
    siCloseInv: TSpeedItem;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    siPeriod: TSpeedItem;
    dg1: TDBGridEh;
    acList: TActionList;
    acAdd: TAction;
    pmdep: TTBPopupMenu;
    acClose: TAction;
    fr: TM207FormStorage;
    acDel: TAction;
    acView: TAction;
    acCloseInv: TAction;
    acPrnGoods: TAction;
    acPrnGift: TAction;
    acParamGoods: TAction;
    spitChr: TSpeedItem;
    acParamGift: TAction;
    pmChr: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    acPrnInvent: TAction;
    pmPrn: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    sitActInvent: TTBSubmenuItem;
    subAct: TTBSubmenuItem;
    siHelp: TSpeedItem;
    acParamAffinage: TAction;
    BuyInvoiceD: TpFIBStoredProc;
    procedure acAddExecute(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acCloseInvUpdate(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure acPrnGoodsExecute(Sender: TObject);
    procedure acPrnGiftExecute(Sender: TObject);
    procedure acParamGoodsExecute(Sender: TObject);
    procedure acParamGiftExecute(Sender: TObject);
    procedure acPrnInventExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acParamAffinageExecute(Sender: TObject);
  private
    FParentLogId: string;
    procedure ShowPeriod;
    procedure pmDepClick (Sender: TObject);
    procedure ActPrintClick(Sender: TObject);
  end;

var
  fmActAllowancesList: TfmActAllowancesList;

implementation

uses comdata, data3, Period, data, M207Proc, DB, ActAllowances, DbUtil, ReportData,
  AllowancesPrn, PresetRecipient, ServData, JewConst, MsgDialog;

{$R *.dfm}

procedure TfmActAllowancesList.pmDepClick (Sender: TObject);
begin
 if TTBItem(Sender).Tag=0 then
 begin
  dm3.ActDepid1:=-MaxInt; dm3.ActDepID2:=MaxInt;
 end
 else
 begin
  dm3.ActDepid1:=TTBItem(Sender).Tag; dm3.ActDepID2:=TTBItem(Sender).Tag;
 end;
 laDepFrom.Caption:=TTBItem(Sender).Caption;
 ReOpenDataSets([dm3.quActAllowancesList]);
end;

procedure TfmActAllowancesList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm3.ActBD) + ' �� ' +DateToStr(dm3.ActEd);
end;

procedure TfmActAllowancesList.acAddExecute(Sender: TObject);
var sinvid:integer;
begin
 dm3.quActAllowancesList.Append;
 dm3.quActAllowancesList.Post;
 dm3.quActAllowancesList.Refresh;

 ShowAndFreeForm(TfmActAllowances, Self, TForm(fmActAllowances), True, False);
 dm3.quActAllowancesList.Refresh;
 if (dm3.quActAllowancesListISCLOSED.AsInteger=0) and
    (dm3.quActAllowancesListQ.AsInteger=0) then
  begin
    dm3.quActAllowancesList.Delete;
    dg1.SumList.RecalcAll;
  end
 else
 begin
  sinvid := dm3.quActAllowancesListSINVID.AsInteger;
  dg1.SumList.RecalcAll;
  dm3.quActAllowancesList.Locate('SINVID', sinvid, []);
 end; 
end;

procedure TfmActAllowancesList.siPeriodClick(Sender: TObject);
begin
 if GetPeriod(dm3.ActBD, dm3.ActEd) then
  begin
   ReOpenDataSets([dm3.quActAllowancesList]);
   ShowPeriod;
  end;
end;

procedure TfmActAllowancesList.FormCreate(Sender: TObject);
var pm:TTBItem;
    pmit: TTBSubmenuItem;
    SubPm: TTBItem;
    i, icenterdep:integer;
    FRes: Variant;
begin
 icenterdep := 0;
 tb1.WallPaper:=wp;
 tb2.WallPaper:=wp;

 dm3.ActBD:=dm.BeginDate;
 dm3.ActEd:=dm.EndDate;

 if CenterDep then
 begin

  pm:=TTBItem.Create(pmdep);
  pm.Caption:='��� ������';
  pm.Tag:=0;
  pm.OnClick:=pmDepClick;

  pmdep.Items.Add(pm);

  {��������� �������}
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid, Centerdep from d_dep where d_depid<>-1000 and ISDelete <>1 ';
  dm.quTmp.ExecQuery;

  i:=0;
  while not (dm.qutmp.Eof) do
  begin
   pm:=TTBItem.Create(pmdep);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;

   pmdep.Items.Add(pm);

    if dm.quTmp.Fields[1].AsInteger = CenterDepId then
    begin
      pmit:=TTBSubmenuItem.Create(sitActInvent);
      pmit.Caption:=dm.quTmp.Fields[0].AsString;
      pmit.Tag:=dm.quTmp.Fields[1].AsInteger;

      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      sitActInvent.Add(pmit);
    end
    else
    begin
      pm:=TTBItem.Create(sitActInvent);
      pm.Caption:=dm.quTmp.Fields[0].AsString;
      pm.Tag:=dm.quTmp.Fields[1].AsInteger;
      pm.OnClick:=ActPrintClick;
      sitActInvent.Add(pm);
    end;

   inc(i);
   if (dm.quTmp.Fields[2].AsInteger=1)  then icenterdep:=i;

   dm.quTmp.Next;
  end;

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;

  pmdep.Items[icenterdep].Click;
 end
 else
 begin
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid='+inttostr(SelfDepId);
  dm.quTmp.ExecQuery;

  while not (dm.qutmp.Eof) do
  begin
   pm:=TTBItem.Create(pmdep);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;

   pmdep.Items.Add(pm);


   pm:=TTBItem.Create(sitActInvent);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=ActPrintClick;
   sitActInvent.Add(pm);

  dm.quTmp.Next;
  end;

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;

  pmdep.Items[0].Click;
 end;

 FRes := ExecSelectSQL('select c.Name from d_comp c, d_rec r where c.d_compid=r.d_compid', dm3.quTmp);
 if not VarIsNull(FRes) then
 begin
   subAct.Caption := string(FRes);
    SubPm :=TTBItem.Create(subAct);
    SubPm.Caption:='����.����';
    SubPm.Tag:= 0;
    SubPm.OnClick:=ActPrintClick;
    subAct.Add(SubPm);
    SubPm :=TTBItem.Create(subAct);
    SubPm.Caption:='����.����';
    SubPm.Tag:=0;
    SubPm.OnClick:=ActPrintClick;
    subAct.Add(SubPm);
 end;
 {����������}
  dmcom.D_MatId:='*';
  dmcom.D_GoodId:='*';
  dmcom.D_CompId:=-1;
  dmcom.D_SupId := -1;
  dmcom.D_Att1ID:=ATT1_DICT_ROOT;
  dmcom.D_Att2ID:=ATT2_DICT_ROOT;
  dmcom.D_Note1:=-1;
  dmcom.D_Note2:=-1;
  dm.SD_MatID_I:='';
  dm.SD_GoodID_I:='';
  dm.SD_SupID_I:='';
  dm.SD_CompID_I:='';
  dm.SD_Note1_I:='';
  dm.SD_Note2_I:='';
  dm.SD_Att1_I:='';
  dm.SD_Att2_I:='';
 {**********}
 

 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 ShowPeriod;
 FParentLogId := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmActAllowancesList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 PostDataSets([dm3.quActAllowancesList]);
 CloseDataSets([dm3.quActAllowancesList]); 
end;

procedure TfmActAllowancesList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmActAllowancesList.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmActAllowancesList.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  if Column.FieldName='DEPNAME' then
  begin
   if dm3.quActAllowancesListCOLOR.AsInteger<>0 then Background:=dm3.quActAllowancesListCOLOR.AsInteger;
  end
  else
  if dm3.quActAllowancesListISCLOSED.AsInteger=0 then Background:=clInfoBk
  else Background:=clBtnFace;
 end
end;

procedure TfmActAllowancesList.acAddUpdate(Sender: TObject);
begin
 if dm3.ActDepid1=dm3.ActDepID2 then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false;
end;

procedure TfmActAllowancesList.acDelUpdate(Sender: TObject);
begin
 if not dm3.quActAllowancesListSINVID.IsNull then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false;
end;

procedure TfmActAllowancesList.acDelExecute(Sender: TObject);
var
  SQL: string;
  Value: Variant;
  BuyInvoiceID: Integer;
  BuyInvoiceState: Integer;
begin
 if dm3.quActAllowancesListISCLOSED.AsInteger=1 then MessageDialog('��������� �������', mtConfirmation, [mbOk], 0)
 else
 begin

  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then  SysUtils.Abort;

  Value := dmCom.db.QueryValue('select buy$invoice$id from sinv where sinvid = :sinvid', 0, [dm3.quActAllowancesListSINVID.AsInteger]);

  if not VarIsNull(Value) then
  begin
    BuyInvoiceID := Value;

    Value := dmCom.db.QueryValue('select state$code from buy$invoice where id = :id', 0, [BuyInvoiceID]);

    if not VarIsNull(Value) then
    begin
      BuyInvoiceState := Value;

      if BuyInvoiceState in [0, 1] then
      begin
        SQL := 'execute procedure buy$invoice$d(' + IntToStr(BuyInvoiceID) + ')';

        dbUtil.ExecSQL(SQL,BuyInvoiceD);
      end;
    end;

    dm3.quActAllowancesList.Delete;

  end else
  begin
    dm3.quActAllowancesList.Delete;
  end;
 end 
end;

procedure TfmActAllowancesList.acViewUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quActAllowancesListSINVID.IsNull);
end;

procedure TfmActAllowancesList.acViewExecute(Sender: TObject);
var sinvid:integer;
begin
 ShowAndFreeForm(TfmActAllowances, Self, TForm(fmActAllowances), True, False);
 dm3.quActAllowancesList.Refresh;
 if (dm3.quActAllowancesListISCLOSED.AsInteger=0) and
    (dm3.quActAllowancesListQ.AsInteger=0) then
  begin
    dm3.quActAllowancesList.Delete;
    dg1.SumList.RecalcAll;
  end
 else
 begin
  sinvid := dm3.quActAllowancesListSINVID.AsInteger;
  dg1.SumList.RecalcAll;
  dm3.quActAllowancesList.Locate('SINVID', sinvid, []);
 end;
end;

procedure TfmActAllowancesList.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acViewExecute(nil);
 end;
end;

procedure TfmActAllowancesList.acCloseInvUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quActAllowancesListSINVID.IsNull);
 if dm3.quActAllowancesListISCLOSED.AsInteger=0 then
 begin
  TAction(Sender).Caption:='�������';
  TAction(Sender).Hint:='������� ���';
  TAction(Sender).ImageIndex:=5;
 end
 else
 begin
  TAction(Sender).Caption:='�������';
  TAction(Sender).Hint:='������� ���';
  TAction(Sender).ImageIndex:=6;
 end
end;

procedure TfmActAllowancesList.acCloseInvExecute(Sender: TObject);
var sinvid:integer;
begin
 if dm3.quActAllowancesListISCLOSED.AsInteger=0 then
 begin
  ExecSQL('execute procedure CLOSE$INVOICE('+dm3.quActAllowancesListSINVID.AsString+', '+
          '19, '+inttostr(dmcom.UserId)+')', dm.quTmp);
  dm3.quActAllowancesList.Refresh;
  sinvid := dm3.quActAllowancesListSINVID.AsInteger;
  dg1.SumList.RecalcAll;
  dm3.quActAllowancesList.Locate('SINVID', sinvid, []);
 end
 else
 begin
  ExecSQL('execute procedure OPENINV('+dm3.quActAllowancesListSINVID.AsString+', '+
          '19, '+inttostr(dmcom.UserId)+')', dm.quTmp);
  dm3.quActAllowancesList.Refresh;
  sinvid := dm3.quActAllowancesListSINVID.AsInteger;
  dg1.SumList.RecalcAll;
  dm3.quActAllowancesList.Locate('SINVID', sinvid, []);
 end
end;

procedure TfmActAllowancesList.acPrnGoodsExecute(Sender: TObject);
begin
  dmReport.PrintDocumentB(ActAllowance_Doc);
end;

procedure TfmActAllowancesList.acPrnGiftExecute(Sender: TObject);
begin
    dmReport.PrintDocumentB(PresentActAllowance_Doc);
end;

procedure TfmActAllowancesList.acParamGoodsExecute(Sender: TObject);
begin
  if ShowAndFreeForm(TfmActAllowancesPrn, Self, TForm(fmActAllowancesPrn), True, False) = mrOk then
     dm3.quActAllowancesList.Refresh;
end;

procedure TfmActAllowancesList.acParamAffinageExecute(Sender: TObject);
begin
  if ShowAndFreeForm(TfmActAllowancesPrn, Self, TForm(fmActAllowancesPrn), True, False) = mrOk then
     dm3.quActAllowancesList.Refresh;
end;

procedure TfmActAllowancesList.acParamGiftExecute(Sender: TObject);
begin
  if ShowAndFreeForm(TfrmPresetRecipient, Self, TForm(frmPresetRecipient), True, False) = mrOk then
     dm3.quActAllowancesList.Refresh;
end;

procedure TfmActAllowancesList.ActPrintClick(Sender: TObject);
var
  Tag, DepId1, DepId2 : integer;
  LogOperationID: string;
begin
  Tag := (Sender as TComponent).Tag;
  if (Tag=0) then
  begin
    DepId1 := -MAXINT;
    DepId2 := MAXINT;
  end
  else begin
    DepId1 := Tag;
    DepId2 := Tag;
  end;
  if (Sender as TTbItem).Caption = '����.����' then
    dmReport.InventOutPrice := 0
  else
    dmReport.InventOutPrice := 1;

  LogOperationID := dm3.insert_operation(sLog_InventoryPrintBefore, FParentLogId);

  if dm3.quActAllowancesListREF_SINVID.IsNull  then
        MessageDialog('��� �� �������� ����� �������� � ��������������', mtInformation, [mbOk], 0)
  else
  begin
    dmserv.CurInventory := ExecSelectSQL('select HID from Inventory_Head ih where SInvID = ' +
                       dm3.quActAllowancesListREF_SINVID.AsString, dm3.quTmp);
    ReOpenDataSet(dmserv.quInventory);
    dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([Tag]), VarArrayOf([DepId1, DepId2]),
                           VarArrayOf([DepId1, DepId2])]), writeoff_inventory_act);
    CloseDataSet(dmserv.quInventory);
    dm3.update_operation(LogOperationID);
  end;
end;


procedure TfmActAllowancesList.acPrnInventExecute(Sender: TObject);
begin
///
end;

procedure TfmActAllowancesList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100271)
end;

procedure TfmActAllowancesList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
