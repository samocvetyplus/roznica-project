//==============================================================
//  Copyright (c) 2008 NPP "LTT"                   Date: 01.2008
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [01.2008]
//==============================================================

(***************************************************************
      ���������� �������
****************************************************************)
unit zshutil;

interface

uses SysUtils, Registry, Windows, Dialogs;

function NewFileName(const ARepTitle: string) : String;
function MonthToStr (AMonth : Integer) : String;
function MoneyToString(ASum: Double): ShortString;

implementation

//����������� ���������� �������� �����
//��� ������������� � ���������� �������� Fast Report
function NewFileName(const ARepTitle: string) : String;
var
  VRepDir: string;
  VReg: TRegistry;
  VName, VRepTime: ShortString;

  procedure BuildGoodStr(var AStr: ShortString);
  begin
    AStr:= Trim(AStr);
    while Pos('.',AStr) > 0 do
      AStr[Pos('.',AStr)]:= '-';
    while Pos(':',AStr) > 0 do
      AStr[Pos(':',AStr)]:= '-';
    while Pos(' ',AStr) > 0 do
      AStr[Pos(' ',AStr)]:= '-';
    while Pos('\',AStr) > 0 do
      AStr[Pos('\',AStr)]:= '-';
  end;

begin
  if Length(ARepTitle) > 0 then
  begin
    //I. ������������ �����
    VName:= ARepTitle;
    BuildGoodStr(VName);

    //II. ����
    //========
    VRepDir:= 'C:\Project\FastRep\';

    //��������� ���������� �� ����� ������� � ���� ����, �� ������� �����
    if not DirectoryExists(VRepDir) then
    begin
      if not ForceDirectories(VRepDir) then
      begin
        //���� �� ������� ������� �������, �� ��������� �� ������� ����
        VRepDir:= 'C:\';
        VReg:= TRegistry.Create;
        try
          //��������� �������� ����� "��� ���������"
          VReg.RootKey:= HKEY_CURRENT_USER;
          if VReg.OpenKeyReadOnly('Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders') then
          begin
            if VReg.ValueExists('Personal') then
              VRepDir:= VReg.ReadString('Personal');
            VReg.CloseKey;
          end;
        finally
          VReg.Free;
        end; //finally
      end;
    end;

    //III. ���� � ����� ������
    //========================
    VRepTime:= DateTimeToStr(Now);
    BuildGoodStr(VRepTime);

    Result:= VRepDir + VName + '_' + VRepTime;
  end
  else
  begin
    Result:= '����� �����';
  end;
end;

//��� �������� �������� ������ � ������ �� �����
function MonthToStr (AMonth : Integer) : String;
begin
  case AMonth of
    1: Result := '������';
    2: Result := '��������';
    3: Result := '����';
    4: Result := '������';
    5: Result := '���';
    6: Result := '����';
    7: Result := '����';
    8: Result := '������';
    9: Result := '��������';
    10: Result := '�������';
    11: Result := '������';
    12: Result := '�������';
    else
       Result := '������ ������';
  end;
end;

//������� ��������� �������� �������� ����� � ��������� ���
function MoneyToString(ASum: Double): ShortString;
var
  s,ss: string;
  sk: ShortString;
  cword,cwordk: string;
  a1: array [1..5,0..9] of string;
  a2: array [1..2,1..3,0..9] of string;
  a3: array [1..1,0..9] of string;
  c,di,i,j: integer;
  cif: string;
  VNeg,              //������� "������������� ���������"
  VNulRub: Boolean;     //������� "���� ������"  evgenius 30.07.2005

  procedure Processin(S:string;i:integer);
  var
    cc:integer;
  begin
    if StrToInt(s[Length(s)-1])=1 then c:=0 else c:=StrToInt(s[Length(s)]);
    if not((i<>1)and(StrToInt(s)=0))then
      cword:=a1[i,c]+cword;
    for cc:=3 downto 1 do
    begin
      di:=1;
      if((cc=3)and(s[2]='1')) then continue;
      if((i=2)and(cc=3)and(s[2]<>'1')) then if s[3] in ['1','2'] then di:=2;
      if ((cc=2)and(s[cc]='1')) then cif:=a3[1,StrToInt(s[cc+1])]
      else cif:=a2[di,cc,StrToInt(s[cc])];
      cword:=cif+cword;
    end;
  end;

  procedure ProcessinK(S:string);
  begin
    if StrToInt(s[Length(s)-1])=1 then c:=0 else c:=StrToInt(s[Length(s)]);
    cwordk:=s+' '+a1[5,c];
  end;

begin
  a1[1,0]:=' ������';a1[2,0]:=' �����'; a1[3,0]:=' ���������';a1[4,0]:=' ����������';a1[5,0]:=' ������';
  a1[1,1]:=' �����'; a1[2,1]:=' ������';a1[3,1]:=' �������';  a1[4,1]:=' ��������';  a1[5,1]:=' �������';
  a1[1,2]:=' �����'; a1[2,2]:=' ������';a1[3,2]:=' ��������'; a1[4,2]:=' ���������'; a1[5,2]:=' �������';
  a1[1,3]:=' �����'; a1[2,3]:=' ������';a1[3,3]:=' ��������'; a1[4,3]:=' ���������'; a1[5,3]:=' �������';
  a1[1,4]:=' �����'; a1[2,4]:=' ������';a1[3,4]:=' ��������'; a1[4,4]:=' ���������'; a1[5,4]:=' �������';
  a1[1,5]:=' ������';a1[2,5]:=' �����'; a1[3,5]:=' ���������';a1[4,5]:=' ����������';a1[5,5]:=' ������';
  a1[1,6]:=' ������';a1[2,6]:=' �����'; a1[3,6]:=' ���������';a1[4,6]:=' ����������';a1[5,6]:=' ������';
  a1[1,7]:=' ������';a1[2,7]:=' �����'; a1[3,7]:=' ���������';a1[4,7]:=' ����������';a1[5,7]:=' ������';
  a1[1,8]:=' ������';a1[2,8]:=' �����'; a1[3,8]:=' ���������';a1[4,8]:=' ����������';a1[5,8]:=' ������';
  a1[1,9]:=' ������';a1[2,9]:=' �����'; a1[3,9]:=' ���������';a1[4,9]:=' ����������';a1[5,9]:=' ������';

  a2[1,3,0]:= '';    a2[1,2,0]:= '';             a2[1,1,0]:= '';
  a2[1,3,1]:= ' ����';   a2[1,2,1]:= '';             a2[1,1,1]:= ' ���';
  a2[1,3,2]:= ' ���';    a2[1,2,2]:= ' ��������';    a2[1,1,2]:= ' ������';
  a2[1,3,3]:= ' ���';    a2[1,2,3]:= ' ��������';    a2[1,1,3]:= ' ������';
  a2[1,3,4]:= ' ������'; a2[1,2,4]:= ' �����';       a2[1,1,4]:= ' ���������';
  a2[1,3,5]:= ' ����';   a2[1,2,5]:= ' ���������';   a2[1,1,5]:= ' �������';
  a2[1,3,6]:= ' �����';  a2[1,2,6]:= ' ����������';  a2[1,1,6]:= ' ��������';
  a2[1,3,7]:= ' ����';   a2[1,2,7]:= ' ���������';   a2[1,1,7]:= ' �������';
  a2[1,3,8]:= ' ������'; a2[1,2,8]:= ' �����������'; a2[1,1,8]:= ' ���������';
  a2[1,3,9]:= ' ������'; a2[1,2,9]:= ' ���������';   a2[1,1,9]:= ' ���������';

  a2[2,3,1]:= ' ����';
  a2[2,3,2]:= ' ���';

  a3[1,0]:= ' ������';
  a3[1,1]:= ' �����������';
  a3[1,2]:= ' ����������';
  a3[1,3]:= ' ����������';
  a3[1,4]:= ' ������������';
  a3[1,5]:= ' ����������';
  a3[1,6]:= ' �����������';
  a3[1,7]:= ' ����������';
  a3[1,8]:= ' ������������';
  a3[1,9]:= ' ������������';

  VNeg:= (ASum < 0);      //oleg (09.06.2005)
  ASum:= abs(ASum);       //oleg (09.06.2005)

  VNulRub:= ASum < 1;     // evgenius 30.07.2005 ���������� �������, ���� ���� ������

  s:= FloatToStr(ASum);
  sk:= FloatToStr(ASum);

  //oleg (07.06.2005)
  delete(s,Pos(DecimalSeparator,s),Length(s));
  //delete(s,Pos(',',s),Length(s));

  cword:= '';
  i:= 1;
  while Length(s)>3 do
  begin
    ss:=copy(s,Length(s)-2,3);
    Delete(s,Length(s)-2,3);
    Processin(ss,i);
    inc(i);
  end;

  for j:= 0 to 2 - Length(s) do
    Insert('0', S, 1);
  if Length(s) > 0 then Processin(s,i);

    cwordk:= '';
  for j:= 0 to 1 - Length(s) do
    Insert('0', S, 1);
  if Length(s) > 0 then
    ProcessinK(s);
  cword:= cword + '  ' + cwordk;
  cwordk:= cword[2];
  cwordk:= AnsiUpperCase(cwordk);
  cword[2]:= cwordk[1];
  if VNulRub then                // evgenius 30.07.2005  � ���� ���� ������
    Result:= ' ����' + cword     // evgenius 30.07.2005  ��
  else                           // evgenius 30.07.2005
    Result:= cword;

  //oleg (09.06.2005)
  if VNeg then
    Result:= ' �����' + cword;
end;
//������� ��������� �������� � ��������� ���
function NumToString(ASum:double):ShortString;
var s,ss:string;
    cword,cwordk:string;
    a1:array [1..5,0..9] of string;
    a2:array [1..2,1..3,0..9] of string;
    a3:array [1..1,0..9] of string;
    c,di,i,j:integer;
    cif:string;
 procedure Processin(S:string;i:integer);
 var cc:integer;
 begin
  if StrToInt(s[Length(s)-1])=1 then c:=0 else c:=StrToInt(s[Length(s)]);
  if not((i<>1)and(StrToInt(s)=0))then
  cword:=a1[i,c]+cword;
  for cc:=3 downto 1 do
  begin
   di:=1;
   if((cc=3)and(s[2]='1')) then continue;
   if((i=2)and(cc=3)and(s[2]<>'1')) then if s[3] in ['1','2'] then di:=2;
   if ((cc=2)and(s[cc]='1')) then cif:=a3[1,StrToInt(s[cc+1])]
   else cif:=a2[di,cc,StrToInt(s[cc])];
   cword:=cif+cword;
  end;
 end;
 procedure ProcessinK(S:string);
 begin
  if StrToInt(s[Length(s)-1])=1 then c:=0 else c:=StrToInt(s[Length(s)]);
  cwordk:=s+' '+a1[5,c];
 end;

begin
 a1[1,0]:=' ';a1[2,0]:=' �����'; a1[3,0]:=' ���������';a1[4,0]:=' ����������';a1[5,0]:=' ';
 a1[1,1]:=' '; a1[2,1]:=' ������';a1[3,1]:=' �������';  a1[4,1]:=' ��������';  a1[5,1]:=' ';
 a1[1,2]:=' '; a1[2,2]:=' ������';a1[3,2]:=' ��������'; a1[4,2]:=' ���������'; a1[5,2]:=' ';
 a1[1,3]:=' '; a1[2,3]:=' ������';a1[3,3]:=' ��������'; a1[4,3]:=' ���������'; a1[5,3]:=' ';
 a1[1,4]:=' '; a1[2,4]:=' ������';a1[3,4]:=' ��������'; a1[4,4]:=' ���������'; a1[5,4]:=' ';
 a1[1,5]:=' ';a1[2,5]:=' �����'; a1[3,5]:=' ���������';a1[4,5]:=' ����������';a1[5,5]:=' ';
 a1[1,6]:=' ';a1[2,6]:=' �����'; a1[3,6]:=' ���������';a1[4,6]:=' ����������';a1[5,6]:=' ';
 a1[1,7]:=' ';a1[2,7]:=' �����'; a1[3,7]:=' ���������';a1[4,7]:=' ����������';a1[5,7]:=' ';
 a1[1,8]:=' ';a1[2,8]:=' �����'; a1[3,8]:=' ���������';a1[4,8]:=' ����������';a1[5,8]:=' ';
 a1[1,9]:=' ';a1[2,9]:=' �����'; a1[3,9]:=' ���������';a1[4,9]:=' ����������';a1[5,9]:=' ';

 a2[1,3,0]:='';       a2[1,2,0]:='';            a2[1,1,0]:='';
 a2[1,3,1]:=' ����';  a2[1,2,1]:='';            a2[1,1,1]:=' ���';
 a2[1,3,2]:=' ���';   a2[1,2,2]:=' ��������';   a2[1,1,2]:=' ������';
 a2[1,3,3]:=' ���';   a2[1,2,3]:=' ��������';   a2[1,1,3]:=' ������';
 a2[1,3,4]:=' ������';a2[1,2,4]:=' �����';      a2[1,1,4]:=' ���������';
 a2[1,3,5]:=' ����';  a2[1,2,5]:=' ���������';  a2[1,1,5]:=' �������';
 a2[1,3,6]:=' �����'; a2[1,2,6]:=' ����������'; a2[1,1,6]:=' ��������';
 a2[1,3,7]:=' ����';  a2[1,2,7]:=' ���������';  a2[1,1,7]:=' �������';
 a2[1,3,8]:=' ������';a2[1,2,8]:=' �����������';a2[1,1,8]:=' ���������';
 a2[1,3,9]:=' ������';a2[1,2,9]:=' ���������';  a2[1,1,9]:=' ���������';

 a2[2,3,1]:=' ����';
 a2[2,3,2]:=' ���';

 a3[1,0]:=' ������';
 a3[1,1]:=' �����������';
 a3[1,2]:=' ����������';
 a3[1,3]:=' ����������';
 a3[1,4]:=' ������������';
 a3[1,5]:=' ����������';
 a3[1,6]:=' �����������';
 a3[1,7]:=' ����������';
 a3[1,8]:=' ������������';
 a3[1,9]:=' ������������';
 s:=FloatToStr(ASum);
 delete(s,Pos(DecimalSeparator,s),Length(s));
 cword:='';
 i:=1;
 while Length(s)>3 do
 begin
  ss:=copy(s,Length(s)-2,3);
  Delete(s,Length(s)-2,3);
  Processin(ss,i);
  inc(i);
 end;
 for j:=0 to 2-Length(s) do Insert('0', S, 1);
 if Length(s)>0 then Processin(s,i);
// cword - �������� �����
 if(ASum-Round(ASum))=0 then s:='00'
 else
  begin
   s:=FloatToStr(ASum);
   delete(s,1,Pos(DecimalSeparator,s));
   if Length(s)>2 then delete(s,3,Length(s));
   if Length(s)<2 then s:=s+'0';
  end;
 cwordk:='';
 for j:=0 to 1-Length(s) do Insert('0', S, 1);
 if Length(s)>0 then ProcessinK(s);
 cword:=cword+'  '+cwordk;
 cwordk:=cword[2];
 cwordk:=AnsiUpperCase(cwordk);
 cword[2]:=cwordk[1];
 Result:=cword;
end;


end.
