object fmMainDemo: TfmMainDemo
  Left = 0
  Top = 0
  Caption = #1054#1090#1095#1077#1090#1099
  ClientHeight = 127
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object bReport1: TButton
    Left = 23
    Top = 24
    Width = 242
    Height = 25
    Caption = #171#1055#1083#1072#1090#1077#1078#1080' '#1087#1086' '#1074#1080#1076#1072#1084' '#1086#1087#1083#1072#1090' '#1079#1072' '#1087#1077#1088#1080#1086#1076#187
    TabOrder = 0
    OnClick = bReport1Click
  end
  object bReport2: TButton
    Left = 23
    Top = 64
    Width = 242
    Height = 25
    Caption = #171#1057#1091#1084#1084#1072' '#1086#1087#1083#1072#1090' '#1087#1086' '#1076#1085#1103#1084' '#1079#1072' '#1087#1077#1088#1080#1086#1076#187
    TabOrder = 1
    OnClick = bReport2Click
  end
  object idbName: TIBDatabase
    Params.Strings = (
      'lc_ctype=WIN1251'
      'user_name=SYSDBA'
      'password=masterkey')
    LoginPrompt = False
    AllowStreamedConnected = False
    Left = 120
    Top = 4
  end
end
