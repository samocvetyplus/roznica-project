//==============================================================
//  Copyright (c) 2008 NPP "LTT"                   Date: 01.2008
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [01.2008]
//==============================================================

(***************************************************************
   ������-������ ��� ������������ � ������ ������� � Fast Reports
****************************************************************)

unit anstask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Menus, DB, IBCustomDataSet, frxClass, frxDBSet,
  IBDatabase, ComCtrls, zshfrexp, frxPreview, zshutil, DBAccess, MyAccess, MemDS;

type
  TfmAnUniAsk = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    bbPrint: TBitBtn;
    bbClose: TBitBtn;
    bbExport: TBitBtn;
    stReportName: TStaticText;
    frxMainReport: TfrxReport;
    Connection: TMyConnection;
    dtOrder: TMyQuery;
    dtOrderID_ORDER: TIntegerField;
    dtOrderFIO: TStringField;
    dtOrderPHONE: TStringField;
    dtOrderDEP_SEND: TStringField;
    dtOrderART: TStringField;
    dtOrderCOMP: TStringField;
    dtOrderGOOD: TStringField;
    dtOrderMAT: TStringField;
    dtOrderSZ: TStringField;
    dtOrderEMP_CREATE: TStringField;
    dtOrderDATE_CREATE: TDateTimeField;
    dtOrderDEP_CUSTOM: TStringField;
    dtOrderNAME: TStringField;
    dtOrderID_STATE: TIntegerField;
    dtOrderTYPE_ORDER: TIntegerField;
    dtOrderEMP_STATE: TStringField;
    dtOrderDATE_STATE: TDateTimeField;
    dtOrderNUMBER_DOC: TIntegerField;
    dtOrderDATE_DOC: TStringField;
    dtOrderCOMMENT: TStringField;
    dtOrderPRICE: TFloatField;
    dtOrderDocumentName: TStringField;
    dtOrderDEL: TIntegerField;
    dtOrderART_FACTORY: TStringField;
    dtOrderUID: TIntegerField;
    dtOrderINS_FACTORY: TStringField;
    dsOrder: TMyDataSource;
    Command: TMyCommand;
    dtOrder_in: TMyQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    DateTimeField1: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField12: TStringField;
    DateTimeField2: TDateTimeField;
    IntegerField4: TIntegerField;
    StringField13: TStringField;
    StringField14: TStringField;
    FloatField1: TFloatField;
    StringField15: TStringField;
    dtOrder_inID_PREVIEW: TIntegerField;
    dtOrder_inUID: TIntegerField;
    dtOrder_inART_FACTORY: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure bbExportClick(Sender: TObject);
    procedure bbPrintClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure frxMainReportLoadTemplate(Report: TfrxReport;
      const TemplateName: string);
    procedure frxMainReportPreview(Sender: TObject);
    function frxMainReportUserFunction(const AMethodName: string;
      var Params: Variant): Variant;
  private
    { Private declarations }
    FFRReportTitle: ShortString;   //��������� ������
    FFRPatternName: string;        //��� ����� (������ fr3)
    procedure SetASKCaption(AValue: ShortString);
    procedure SetFRPatternName(const AValue: string);
    procedure SetFRReportTitle(const AValue: ShortString);
    procedure ButtonSQL(Sender: TObject);
    procedure SetMainDatabase(const AValue: TIBDatabase);
  protected
    { Protected declarations }
    function UniFRepPrepare: Boolean; virtual;
    procedure WriteExportFileName(AReportTitle: String);
    procedure ServiceShowSQLText(ADataSet: TIBDataSet);
    function MoneyToStringFR(ASumma: ShortString): ShortString;
  public
    { Public declarations }
    FOperMonth, FOperYear: Integer;       //������� �����
    FTitle,                               //������ "���������"
    FPeriod, FRestriction: ShortString;   //������ "������" � �����������
    FDateCreate: TDate;                   //���� ������ ������
    FReportSQL: TStrings;                 //SQL ����� ��������� ������� � ������
    property FRReportTitle: ShortString read FFRReportTitle write SetFRReportTitle;
    property FRPatternName: string read FFRPatternName write SetFRPatternName;
    property MainDatabase: TIBDatabase write SetMainDatabase;
    property ASKCaption: ShortString write SetASKCaption;
  end;


implementation

{$R *.dfm}

function TfmAnUniAsk.UniFRepPrepare: Boolean;
var
  VMemo: TfrxMemoView;
begin
  if FileExists(FFRPatternName) then
  begin
    try
      frxMainReport.LoadFromFile(FFRPatternName);
    except
      ShowMessage('������ ��� �������� ������� ������.'#13 + FFRPatternName);
      Result:= False;
      Abort;
    end;  //except
    frxMainReport.AddFunction('function MoneyToStringFR(ASumma: String): String');

    if Length(FRReportTitle) < 1 then
      FRReportTitle:= '�������� ��������';

    FRReportTitle:= FTitle + FPeriod;
    //������ ������������ ������
    VMemo:= frxMainReport.FindObject('memFullReportName') as TfrxMemoView;
    if Assigned(VMemo) then
      VMemo.Text:= FRReportTitle;

    frxMainReport.ReportOptions.Name:= FRReportTitle;
    WriteExportFileName(FRReportTitle);

    //��������� ������
    VMemo:= frxMainReport.FindObject('memReportName') as TfrxMemoView;
    if Assigned(VMemo) then
      VMemo.Text:= FTitle;

    //������ ������
    VMemo:= frxMainReport.FindObject('memPeriod') as TfrxMemoView;
    if Assigned(VMemo) then
    begin
      VMemo.Text:= FPeriod;
      //����������� (������������ ��������� ������������� �����������)
      if Trim(FRestriction) <> '' then
         VMemo.Lines.Strings[1]:=  FRestriction;
    end;

    //���� ������������ ������
    VMemo:= frxMainReport.FindObject('memCreateInfo') as TfrxMemoView;
    if Assigned(VMemo) then
      VMemo.Text:= '����������� ' + DateToStr(FDateCreate);

    with idsFRReport  do
    begin
      if Active then Close;
      SelectSQL.Clear;
      SelectSQL.AddStrings(FReportSQL);
      Open;
    end;
    Result:= True;
  end
  else
  begin
    ShowMessage('������ ��� ������������ ������, ���������� � ��������������.'#13 +
      '�� ������ ������ ������ �� ���������� ����:'#13 + FFRPatternName);
    Result:= False;
  end;
end;

procedure TfmAnUniAsk.WriteExportFileName(AReportTitle: String);
begin
  //� ������ ���������� ������ �������� ��� ����� �� ��������� 
  dmFastRepExport.WriteFRPath(NewFileName(AReportTitle));
end;

procedure TfmAnUniAsk.bbExportClick(Sender: TObject);
begin
  if UniFRepPrepare then
  begin
    try
      frxMainReport.PrepareReport(true);
      frxMainReport.Export(dmFastRepExport.frxXLSExport);
      idsFRReport.Close;
    except
      ShowMessage('������ ��� �������� ������ � MS Excel');
    end; //except
  end;
end;

procedure TfmAnUniAsk.bbPrintClick(Sender: TObject);
begin
  if UniFRepPrepare then
  begin
    try
      idsFRReport.FetchAll;
      if idsFRReport.RecordCount = 0 then
         ShowMessage('� ��������� ����������� �������� ������')
      else
         frxMainReport.ShowReport();
      idsFRReport.Close;
    except
      ShowMessage('������ ��� ������ ������ ');
    end; //except
  end;
end;

procedure TfmAnUniAsk.ButtonSQL(Sender: TObject);
begin
  ServiceShowSQLText(idsFRReport);
end;

procedure TfmAnUniAsk.FormCreate(Sender: TObject);
begin
  FFRReportTitle:= '�������� ��������';
  FFRPatternName:= '';
  FTitle:= '';
  FPeriod:= '';
  FRestriction := '';
  FDateCreate:= Now;
  FReportSQL:= TStringList.Create;
end;

procedure TfmAnUniAsk.FormDestroy(Sender: TObject);
begin
  FReportSQL.Free;
end;

procedure TfmAnUniAsk.FormHide(Sender: TObject);
begin
  traMainAsk.Commit;
end;

procedure TfmAnUniAsk.FormShow(Sender: TObject);
begin
   //����������
  if traMainAsk.Active then traMainAsk.Rollback;
  traMainAsk.StartTransaction;
end;

procedure TfmAnUniAsk.frxMainReportLoadTemplate(Report: TfrxReport;
  const TemplateName: string);
begin
  Report.LoadFromFile(ExtractFilePath(Application.ExeName) + 'FRep\' + TemplateName);
end;

procedure TfmAnUniAsk.frxMainReportPreview(Sender: TObject);
var
  VButton: TSpeedButton;
  VPopupMenu: TPopupMenu;
  VMenuItem: TMenuItem;
begin
  //������ ��������� SQL-�������
  VButton:= TSpeedButton.Create(TfrxPreviewForm(frxMainReport.PreviewForm).ToolBar);
  VButton.Parent:= TfrxPreviewForm(frxMainReport.PreviewForm).ToolBar;
  VButton.Caption:= 'SQL';
  VButton.Width:= 60;
  VButton.Left:= 650;
  VButton.OnClick:= ButtonSQL;

  //�������� SQL-������� ����� ������� ���������� ������
  VPopupMenu:= TfrxPreviewForm(frxMainReport.PreviewForm).PopupMenu;
  VMenuItem:= TMenuItem.Create(VPopupMenu); //create the new item
  VPopupMenu.Items.Add(VMenuItem);
  VMenuItem.Name:= 'mcServiceSQL';
  VMenuItem.Caption:= 'SQL text';
  VMenuItem.Visible:= False;
  VMenuItem.ShortCut:= ShortCut(Ord('Q'), [ssShift,ssCtrl,ssAlt]);
  VMenuItem.OnClick:= ButtonSQL;
end;

function TfmAnUniAsk.frxMainReportUserFunction(const AMethodName: string;
  var Params: Variant): Variant;
begin
  if AMethodName = 'MONEYTOSTRINGFR' then
    Result:= MoneyToStringFR(Params[0]);
end;

function TfmAnUniAsk.MoneyToStringFR(ASumma: ShortString): ShortString;
var
  VSumma: Double;
begin //����� �������� � FR
  if TryStrToFloat(ASumma,VSumma) then
    Result:= MoneyToString(VSumma)
  else
    Result:= '-';
end;

procedure TfmAnUniAsk.ServiceShowSQLText(ADataSet: TIBDataSet);
begin
  ShowMessage(ADataSet.SelectSQL.Text);
end;

procedure TfmAnUniAsk.SetASKCaption(AValue: ShortString);
begin
  Caption:= AValue;
end;

procedure TfmAnUniAsk.SetFRPatternName(const AValue: string);
begin
  FFRPatternName:= ExtractFilePath(Application.ExeName) + 'FRep\' + AValue;
end;

procedure TfmAnUniAsk.SetFRReportTitle(const AValue: ShortString);
begin
  FFRReportTitle:= trim(AValue);
end;

procedure TfmAnUniAsk.SetMainDatabase(const AValue: TIBDatabase);
var
  i: Integer;
begin
  //���������� ���������� � ���� ������
  traMainAsk.DefaultDatabase:= AValue;
  for i:= 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[i] is TIBCustomDataSet then
    begin
      if TIBCustomDataSet(Self.Components[i]).Active then
        TIBCustomDataSet(Self.Components[i]).Close;
      if TIBCustomDataSet(Self.Components[i]).Database = nil then
      begin
        TIBCustomDataSet(Self.Components[i]).Database:= AValue;
        TIBCustomDataSet(Self.Components[i]).Transaction:= traMainAsk;
      end;
    end;
  end;
end;

end.
