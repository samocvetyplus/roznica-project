inherited fmReport2: TfmReport2
  Caption = #1047#1072#1082#1072#1079' '#1086#1090#1095#1077#1090#1072' '#1092#1086#1088#1084#1099' '#8470'2:'
  ClientHeight = 188
  ExplicitHeight = 224
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Height = 144
    ExplicitHeight = 144
    object lMonth: TLabel [0]
      Left = 8
      Top = 72
      Width = 31
      Height = 13
      Caption = #1052#1077#1089#1103#1094
    end
    object lYear: TLabel [1]
      Left = 135
      Top = 72
      Width = 19
      Height = 13
      Caption = #1043#1086#1076
    end
    inherited stReportName: TStaticText
      Caption = #171#1057#1091#1084#1084#1072' '#1086#1087#1083#1072#1090' '#1087#1086' '#1076#1085#1103#1084' '#1079#1072' '#1087#1077#1088#1080#1086#1076#187
    end
    object cbMonth: TComboBox
      Left = 8
      Top = 87
      Width = 119
      Height = 21
      DropDownCount = 12
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        #1103#1085#1074#1072#1088#1100
        #1092#1077#1074#1088#1072#1083#1100
        #1084#1072#1088#1090
        #1072#1087#1088#1077#1083#1100
        #1084#1072#1081
        #1080#1102#1085#1100
        #1080#1102#1083#1100
        #1072#1074#1075#1091#1089#1090
        #1089#1077#1085#1090#1103#1073#1088#1100
        #1086#1082#1090#1103#1073#1088#1100
        #1085#1086#1103#1073#1088#1100
        #1076#1077#1082#1072#1073#1088#1100)
    end
    object edYear: TEdit
      Left = 135
      Top = 87
      Width = 66
      Height = 21
      TabOrder = 2
    end
  end
  inherited Panel2: TPanel
    Top = 144
    ExplicitTop = 144
  end
  inherited traMainAsk: TIBTransaction
    Left = 8
  end
  inherited frxMainReport: TfrxReport
    ParentReport = 'BaseReport.fr3'
    PrintOptions.Printer = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    ReportOptions.Author = ''
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 40
  end
  inherited frxMainDBDataset: TfrxDBDataset
    Left = 72
  end
  inherited idsFRReport: TIBDataSet
    SelectSQL.Strings = (
      '')
    Left = 104
  end
  object dsDate: TDataSource
    DataSet = idsFRReport
    Left = 136
    Top = 24
  end
  object ibqBank: TIBQuery
    Transaction = traMainAsk
    DataSource = dsDate
    SQL.Strings = (
      'SELECT SUM(D.PL_SUMMA)'
      'FROM ABEPAYDIVERSITYLIST D'
      'WHERE D.PL_OPDATE = :PL_OPDATE'
      '  AND D.PL_CODE_SERVCHARGE = 1')
    Left = 168
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PL_OPDATE'
        ParamType = ptUnknown
      end>
  end
  object frxBank: TfrxDBDataset
    UserName = 'frxBank'
    CloseDataSource = False
    DataSet = ibqBank
    Left = 200
    Top = 24
  end
  object ibqTill: TIBQuery
    Transaction = traMainAsk
    DataSource = dsDate
    SQL.Strings = (
      'SELECT SUM(D.PL_SUMMA)'
      'FROM ABEPAYDIVERSITYLIST D'
      'WHERE D.PL_OPDATE = :PL_OPDATE'
      '  AND D.PL_CODE_SERVCHARGE = 2')
    Left = 232
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PL_OPDATE'
        ParamType = ptUnknown
      end>
  end
  object frxTill: TfrxDBDataset
    UserName = 'frxTill'
    CloseDataSource = False
    DataSet = ibqTill
    Left = 264
    Top = 24
  end
  object ibqAcc: TIBQuery
    Transaction = traMainAsk
    DataSource = dsDate
    SQL.Strings = (
      'SELECT SUM(D.PL_SUMMA)'
      'FROM ABEPAYDIVERSITYLIST D'
      'WHERE D.PL_OPDATE = :PL_OPDATE'
      '  AND D.PL_CODE_SERVCHARGE in (3,4)')
    Left = 232
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PL_OPDATE'
        ParamType = ptUnknown
      end>
  end
  object frxAcc: TfrxDBDataset
    UserName = 'frxAcc'
    CloseDataSource = False
    DataSet = ibqAcc
    Left = 264
    Top = 56
  end
end
