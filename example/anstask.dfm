object fmAnUniAsk: TfmAnUniAsk
  Left = 0
  Top = 0
  Caption = #1050#1083#1072#1089#1089'-'#1087#1088#1077#1076#1086#1082' '#1092#1086#1088#1084' "'#1047#1072#1082#1072#1079' '#1086#1090#1095#1077#1090#1072'"'
  ClientHeight = 183
  ClientWidth = 339
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 339
    Height = 139
    Align = alClient
    TabOrder = 0
    object stReportName: TStaticText
      Left = 2
      Top = 2
      Width = 336
      Height = 57
      Alignment = taCenter
      AutoSize = False
      BevelKind = bkTile
      BorderStyle = sbsSunken
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1086#1090#1095#1077#1090#1072
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 139
    Width = 339
    Height = 44
    Align = alBottom
    TabOrder = 1
    object bbPrint: TBitBtn
      Left = 167
      Top = 6
      Width = 75
      Height = 25
      Caption = #1055#1077#1095#1072#1090#1100
      TabOrder = 0
      OnClick = bbPrintClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object bbClose: TBitBtn
      Left = 248
      Top = 6
      Width = 75
      Height = 25
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 1
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object bbExport: TBitBtn
      Left = 8
      Top = 6
      Width = 89
      Height = 25
      Caption = 'MS Excel'
      TabOrder = 2
      OnClick = bbExportClick
      NumGlyphs = 2
    end
  end
  object frxMainReport: TfrxReport
    Version = '4.9.72'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42683.448561979200000000
    ReportOptions.LastChange = 42683.465960358800000000
    ScriptLanguage = 'PascalScript'
    StoreInDFM = False
    OnPreview = frxMainReportPreview
    OnUserFunction = frxMainReportUserFunction
    OnLoadTemplate = frxMainReportLoadTemplate
    Left = 112
    Top = 104
  end
  object Connection: TMyConnection
    Database = 'order2'
    ConnectionTimeout = 30
    Options.Charset = 'cp1251'
    Username = 'order'
    Password = '9V4x2Z8r'
    Server = '138.201.247.35'
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 32
  end
  object dtOrder: TMyQuery
    SQLInsert.Strings = (
      
        'call IND_ORDER_I (:DEP_CUSTOM, :DEP_SEND, 1 , :EMP_CREATE, :EMP_' +
        'STATE );')
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY, '
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.DEP_CUSTOM = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY, '
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.DEP_CUSTOM = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Active = True
    Left = 240
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEPARTMENT'
      end>
    object dtOrderID_ORDER: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object dtOrderFIO: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object dtOrderPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object dtOrderDEP_SEND: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object dtOrderART: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object dtOrderCOMP: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object dtOrderGOOD: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object dtOrderMAT: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object dtOrderSZ: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
    end
    object dtOrderEMP_CREATE: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object dtOrderDATE_CREATE: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object dtOrderDEP_CUSTOM: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object dtOrderNAME: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object dtOrderID_STATE: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object dtOrderTYPE_ORDER: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object dtOrderEMP_STATE: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'IND_ORDER.EMP_STATE'
      Size = 30
    end
    object dtOrderDATE_STATE: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'IND_ORDER.DATE_STATE'
    end
    object dtOrderNUMBER_DOC: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'IND_ORDER.NUMBER_DOC'
    end
    object dtOrderDATE_DOC: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'IND_ORDER.DATE_DOC'
      Size = 60
    end
    object dtOrderCOMMENT: TStringField
      FieldName = 'COMMENT'
      Origin = 'IND_ORDER.COMMENT'
      Size = 10000
    end
    object dtOrderPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'IND_ORDER.PRICE'
    end
    object dtOrderDocumentName: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object dtOrderDEL: TIntegerField
      FieldName = 'DEL'
      Origin = 'IND_ORDER.DEL'
    end
    object dtOrderART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Origin = 'IND_ORDER.ART_FACTORY'
      Size = 30
    end
    object dtOrderUID: TIntegerField
      FieldName = 'UID'
      Origin = 'IND_ORDER.UID'
    end
    object dtOrderINS_FACTORY: TStringField
      FieldName = 'INS_FACTORY'
      Origin = 'IND_ORDER.INS_FACTORY'
      Size = 200
    end
  end
  object dsOrder: TMyDataSource
    DataSet = dtOrder
    Left = 208
    Top = 40
  end
  object Command: TMyCommand
    Connection = Connection
    Left = 32
    Top = 24
  end
  object dtOrder_in: TMyQuery
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      ''
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.ID_PREVIEW,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.ID_STATE <> 1) and'
      '      (IND_ORDER.DEP_SEND = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      ''
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.ID_PREVIEW,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.ID_STATE <> 1) and'
      '      (IND_ORDER.DEP_SEND = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Active = True
    Left = 176
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEPARTMENT'
      end>
    object IntegerField1: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object StringField1: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object StringField2: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object StringField5: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object StringField6: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object StringField7: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object StringField8: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
    end
    object StringField9: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object StringField10: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object StringField11: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object IntegerField2: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object IntegerField3: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object StringField12: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'IND_ORDER.EMP_STATE'
      Size = 30
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'IND_ORDER.DATE_STATE'
    end
    object IntegerField4: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'IND_ORDER.NUMBER_DOC'
    end
    object StringField13: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'IND_ORDER.DATE_DOC'
      Size = 60
    end
    object StringField14: TStringField
      FieldName = 'COMMENT'
      Origin = 'IND_ORDER.COMMENT'
      Size = 10000
    end
    object FloatField1: TFloatField
      FieldName = 'PRICE'
      Origin = 'IND_ORDER.PRICE'
    end
    object StringField15: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object dtOrder_inID_PREVIEW: TIntegerField
      FieldName = 'ID_PREVIEW'
      Origin = 'IND_ORDER.ID_PREVIEW'
    end
    object dtOrder_inUID: TIntegerField
      FieldName = 'UID'
      Origin = 'IND_ORDER.UID'
    end
    object dtOrder_inART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Origin = 'IND_ORDER.ART_FACTORY'
      Size = 30
    end
  end
end
