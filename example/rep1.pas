//==============================================================
//  Copyright (c) 2008 NPP "LTT"                   Date: 01.2008
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [01.2008]
//==============================================================

(***************************************************************
      ����� "������� �� ����� ����� �� ������"
****************************************************************)

unit rep1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, anstask, DB, IBCustomDataSet, frxClass, frxDBSet, IBDatabase,
  StdCtrls, Buttons, ExtCtrls, IBQuery, zshutil;

type
  TfmReport1 = class(TfmAnUniAsk)
    cbMonth: TComboBox;
    edYear: TEdit;
    lMonth: TLabel;
    lYear: TLabel;
    rOrder: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function UniFRepPrepare: Boolean; override;
  end;

implementation

{$R *.dfm}

procedure TfmReport1.FormCreate(Sender: TObject);
begin
  inherited;
  FTitle:= '������� �� ����� ����� �� ������';
  FRPatternName:= 'Report1.fr3';
end;

procedure TfmReport1.FormShow(Sender: TObject);
begin
  inherited;
  //�� ��������� ������� ������
  cbMonth.ItemIndex := FOperMonth - 1;
  edYear.Text := IntToStr(FOperYear);
  stReportName.Caption := '"������� �� ����� ����� �� ������"';
end;

function TfmReport1.UniFRepPrepare: Boolean;
begin
  FPeriod:= ' �� ' + MonthToStr(cbMonth.ItemIndex+1) +' '+ edYear.Text + ' �.';

  FReportSQL.Clear;
  FReportSQL.Append('SELECT D1.FR_AGREE_NUM, D1.NAME_FULL, D.PL_CODE_SERVCHARGE SERVCHARGE, D.PL_OPDATE, D.PL_SUMMA');
  FReportSQL.Append('FROM ABEPAYDIVERSITYLIST D');
  FReportSQL.Append('LEFT OUTER JOIN SPRCLIENT D1 ON D.PL_CODE_ABON = D1.CODE_NO');
  FReportSQL.Append('WHERE D.PL_OPYEAR = ' + edYear.Text);
  FReportSQL.Append('AND D.PL_OPMONTH = ' + IntToStr(cbMonth.ItemIndex+1));

  case rOrder.ItemIndex of
    0: FReportSQL.Append('ORDER BY D.PL_CODE_SERVCHARGE, D1.FR_AGREE_NUM');
    1: FReportSQL.Append('ORDER BY D.PL_CODE_SERVCHARGE, D.PL_SUMMA');
  end;

  Result:= inherited UniFRepPrepare;

end;

end.
