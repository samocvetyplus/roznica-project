//==============================================================
//  Copyright (c) 2008 NPP "LTT"                   Date: 01.2008
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [01.2008]
//==============================================================

(***************************************************************
      ������� ������
****************************************************************)
unit report;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, rep1, rep2, DB, IBDatabase, ComCtrls, ToolWin;

type
  TfmMainDemo = class(TForm)
    bReport1: TButton;
    bReport2: TButton;
    idbName: TIBDatabase;
    procedure bReport1Click(Sender: TObject);
    procedure bReport2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Year, Month, Day: Word;
    function IBConn: Boolean;
  public
    { Public declarations }
  end;

var
  fmMainDemo: TfmMainDemo = nil;

implementation

{$R *.dfm}

procedure TfmMainDemo.bReport1Click(Sender: TObject);
var
  VfmRep : TfmReport1;
begin
  // ����� �������� �� ����� ����� �� ������
  if IBConn then
  begin
    VfmRep:= TfmReport1.Create(Self);
    try
      VfmRep.MainDatabase:= idbName;
      VfmRep.FOperYear:= Year;
      VfmRep.FOperMonth:= Month;
      VfmRep.ShowModal;
    finally
      VfmRep.Free;
    end;
  end;
end;

procedure TfmMainDemo.bReport2Click(Sender: TObject);
var
  VfmRep : TfmReport2;
begin
  //����n ������ ����� �� ���� �� ������
  if IBConn then
  begin
    VfmRep:= TfmReport2.Create(Self);
    try
      VfmRep.MainDatabase:= idbName;
      VfmRep.FOperYear:= Year;
      VfmRep.FOperMonth:= Month;
      VfmRep.ShowModal;
    finally
      VfmRep.Free;
    end;
  end;
end;

procedure TfmMainDemo.FormCreate(Sender: TObject);
begin
  idbName.DatabaseName := ExtractFilePath(Application.ExeName) + 'DEMODB\DEMO.FDB';
  //���������� ������� ����� � ���
  DecodeDate(Int(Date), Year, Month, Day);
end;

function TfmMainDemo.IBConn: Boolean;
begin
  with idbName, Params do
  begin
    try
      if not Connected then
        Open;
      Result:= True;
    except
      ShowMessage('������ ��� ����������� � ���� ������');
      Result:= False;
    end; //except
  end;
end;

end.
