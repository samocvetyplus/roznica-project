//==============================================================
//  Copyright (c) 2008 NPP "LTT"                   Date: 01.2008
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [01.2008]
//==============================================================

(***************************************************************
      ����� "����� ����� �� ���� �� ������"
****************************************************************)

unit rep2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, anstask, DB, IBCustomDataSet, frxClass, frxDBSet, IBDatabase,
  StdCtrls, Buttons, ExtCtrls, IBQuery, zshutil;

type
  TfmReport2 = class(TfmAnUniAsk)
    cbMonth: TComboBox;
    edYear: TEdit;
    lMonth: TLabel;
    lYear: TLabel;
    dsDate: TDataSource;
    ibqBank: TIBQuery;
    frxBank: TfrxDBDataset;
    ibqTill: TIBQuery;
    frxTill: TfrxDBDataset;
    ibqAcc: TIBQuery;
    frxAcc: TfrxDBDataset;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function UniFRepPrepare: Boolean; override;
  end;

implementation

{$R *.dfm}

procedure TfmReport2.FormCreate(Sender: TObject);
begin
  inherited;
  FTitle:= '����� ����� �� ���� �� ������';
  FRPatternName:= 'Report2.fr3';
end;

procedure TfmReport2.FormShow(Sender: TObject);
begin
  inherited;
  // �� ��������� �������  ������
  cbMonth.ItemIndex := FOperMonth - 1;
  edYear.Text := IntToStr(FOperYear);
  stReportName.Caption := '"����� ����� �� ���� �� ������"';
end;

function TfmReport2.UniFRepPrepare: Boolean;
begin
  FPeriod:= ' �� ' + MonthToStr(cbMonth.ItemIndex+1) +' '+ edYear.Text + ' �.';

  FReportSQL.Clear;
  FReportSQL.Append('SELECT D.PL_OPDATE');
  FReportSQL.Append('FROM ABEPAYDIVERSITYLIST D');
  FReportSQL.Append('WHERE D.PL_OPYEAR = ' + edYear.Text);
  FReportSQL.Append('AND D.PL_OPMONTH = ' + IntToStr(cbMonth.ItemIndex+1));
  FReportSQL.Append('GROUP BY D.PL_OPDATE');
  FReportSQL.Append('ORDER BY D.PL_OPDATE');
  
  Result:= inherited UniFRepPrepare;

end;

end.
