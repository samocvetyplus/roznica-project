//==============================================================
//  Copyright (c) 2007 NPP "LTT"                   Date: 10.2007
//  Version RAD: Delphi 2006,         [SQL Server: Firebird 2.0]
//  Designed by: Irina Tsybulnikova                    [10.2007]
//==============================================================

(***************************************************************
������������:
==============
  ������ ���������� �������� Fast Reports

****************************************************************)
unit zshfrexp;

interface

uses
  SysUtils, Classes, Forms, frxDesgn, frxDCtrl, frxExportMail, frxExportImage,
  frxExportODF, frxExportTXT, frxExportHTML, frxExportPDF, frxExportRTF,
  frxClass, frxExportXLS;

type
  TdmFastRepExport = class(TDataModule)
    frxXLSExport: TfrxXLSExport;
    frxRTFExport: TfrxRTFExport;
    frxPDFExport: TfrxPDFExport;
    frxHTMLExport: TfrxHTMLExport;
    frxTXTExport: TfrxTXTExport;
    frxODTExport: TfrxODTExport;
    frxODSExport: TfrxODSExport;
    frxJPEGExport: TfrxJPEGExport;
    frxTIFFExport: TfrxTIFFExport;
    frxGIFExport: TfrxGIFExport;
    frxBMPExport: TfrxBMPExport;
    frxMailExport: TfrxMailExport;
    frxUniDialog: TfrxDialogControls;
    frxDesigner: TfrxDesigner;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure WriteFRPath(ANameFile: String);
  end;

var
  dmFastRepExport: TdmFastRepExport = nil;

implementation

{$R *.dfm}
procedure TdmFastRepExport.WriteFRPath(ANameFile: String);
var
  i: Integer;
  VfrxExportClass: TfrxCustomExportFilter;
begin
  //��� ���� ������ ��������
  if ComponentCount > 0 then
  begin
    for i:= 0 to ComponentCount - 1 do
    begin
      if Components[i] is TfrxCustomExportFilter then
      begin
        VfrxExportClass:= Components[i] as TfrxCustomExportFilter;
        VfrxExportClass.FileName:= Trim(ANameFile);
      end;
    end;
  end;
end;

initialization

  dmFastRepExport:= TdmFastRepExport.Create(Application);

end.
