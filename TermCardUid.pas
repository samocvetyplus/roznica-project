unit TermCardUid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, ExtCtrls,
  DBGridEhGrouping, GridsEh;

type
  TfmTermCardUid = class(TForm)
    quTermUid: TpFIBDataSet;
    dsTermUid: TDataSource;
    quTermUidUID: TFIBIntegerField;
    quTermUidADATE: TFIBDateTimeField;
    quTermUidSZ: TFIBStringField;
    quTermUidW: TFIBFloatField;
    quTermUidFULLART: TFIBStringField;
    quTermUidART2: TFIBStringField;
    quTermUidCOST: TFIBFloatField;
    quTermUidCOST0: TFIBFloatField;
    plTermCard: TPanel;
    dg1: TDBGridEh;
    procedure quTermUidBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTermCardUid: TfmTermCardUid;

implementation
uses comdata, data;
{$R *.dfm}

procedure TfmTermCardUid.quTermUidBeforeOpen(DataSet: TDataSet);
begin
 if dm.taCardListCHECKNO.isnull then
   quTermUid.SelectSQL[4]:='where (sl.checkno is null) and sl.card in (1, 2) and '+
    'sl.sellid='+dm.taCardListSELLID.AsString+' and '
 else quTermUid.SelectSQL[4]:='where sl.checkno ='+dm.taCardListCHECKNO.AsString+' and'
end;

end.
