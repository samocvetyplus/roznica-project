object fmUIDHist: TfmUIDHist
  Left = 292
  Top = 225
  HelpContext = 100510
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
  ClientHeight = 453
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 51
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object DBText1: TDBText
      Left = 340
      Top = 8
      Width = 50
      Height = 13
      AutoSize = True
      DataField = 'FULLART'
      DataSource = dm2.dsGetUIDA2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object seUID: TRxSpinEdit
      Left = 64
      Top = 4
      Width = 89
      Height = 21
      Color = clInfoBk
      TabOrder = 0
      OnKeyDown = seUIDKeyDown
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1080#1079#1076#1077#1083#1080#1080
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 595
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siOpen: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 163
      Top = 3
      Visible = True
      OnClick = siOpenClick
      SectionName = 'Untitled (0)'
    end
    object siPrOrd: TSpeedItem
      BtnCaption = #1055#1088#1080#1082#1072#1079
      Caption = #1055#1088#1080#1082#1072#1079
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1094#1077#1085#1099
      ImageIndex = 4
      Layout = blGlyphLeft
      Spacing = 1
      Left = 243
      Top = 3
      Visible = True
      OnClick = siPrOrdClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Layout = blGlyphLeft
      Spacing = 1
      Left = 499
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 434
    Width = 688
    Height = 19
    Panels = <>
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 29
    Width = 688
    Height = 405
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.Alignment = taCenter
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm2.dsUIDHist
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pm1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'ADATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Width = 97
      end
      item
        EditButtons = <>
        FieldName = 'SOp'
        Footers = <>
        Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        Width = 129
      end
      item
        EditButtons = <>
        FieldName = 'SRC'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'CONTRACT'
        Footers = <>
        Title.Caption = #1044#1086#1075#1086#1074#1086#1088' '#1087#1086#1089#1090#1072#1074#1082#1080
        Width = 104
      end
      item
        EditButtons = <>
        FieldName = 'DST'
        Footers = <>
        Title.Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
        Width = 109
      end
      item
        EditButtons = <>
        FieldName = 'SDoc'
        Footers = <>
        Title.Caption = #8470' '#1076#1086#1082'.'
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'SPRICE'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093'. '#1094#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'SSF'
        Footers = <>
        Title.Caption = #1074#1085#1077#1096'. '#8470'  '#1076#1086#1082'.'
      end
      item
        EditButtons = <>
        FieldName = 'NDATE'
        Footers = <>
        Title.Caption = #1042#1085#1077#1096'. '#1076#1072#1090#1072
      end
      item
        EditButtons = <>
        FieldName = 'StateInv'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      end
      item
        EditButtons = <>
        FieldName = 'INSCOUNTER'
        Footers = <>
        Visible = False
        Width = 97
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 96
    Top = 104
  end
  object pm1: TPopupMenu
    Left = 96
    Top = 160
    object N1: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1094#1077#1085#1099
      ShortCut = 114
      OnClick = siPrOrdClick
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 212
    Top = 192
    object acInscounter: TAction
      Caption = 'acInscounter'
      ShortCut = 113
      OnExecute = acInscounterExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1080#1079#1076#1077#1083#1080#1080
      ImageIndex = 0
      ShortCut = 123
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 392
    Top = 112
  end
end
