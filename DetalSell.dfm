object fmDetalSell: TfmDetalSell
  Left = 393
  Top = 166
  Width = 582
  Height = 426
  Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1080' '#1074#1086#1079#1074#1088#1072#1097#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 574
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 23
      Top = 11
      Width = 48
      Height = 16
      Caption = 'Label1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siclose: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = 'siclose'
      Hint = 'siclose|'
      ImageIndex = 0
      Spacing = 1
      Left = 498
      Top = 2
      Visible = True
      OnClick = sicloseClick
      SectionName = 'Untitled (0)'
    end
  end
  object DB_Client_Sell: TDBGridEh
    Left = 0
    Top = 41
    Width = 574
    Height = 356
    Align = alClient
    AutoFitColWidths = True
    Color = clBtnFace
    DataGrouping.GroupLevels = <>
    DataSource = dsDetal
    Flat = True
    FooterColor = clInfoBk
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 2
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    RowSizingAllowed = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DEP_NAME'
        Footers = <>
        Title.Caption = #1060#1080#1083#1080#1072#1083
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <
          item
            Alignment = taLeftJustify
            Value = #1055#1088#1086#1076#1072#1078#1080
            ValueType = fvtStaticText
            WordWrap = True
          end
          item
            Alignment = taLeftJustify
            Value = #1042#1086#1079#1074#1088#1072#1090#1099
            ValueType = fvtStaticText
            WordWrap = True
          end>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Width = 93
      end
      item
        EditButtons = <>
        FieldName = 'RN'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1089#1084#1077#1085#1099
        Width = 111
      end
      item
        EditButtons = <>
        FieldName = 'ADATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080'/'#1074#1086#1079#1074#1088#1072#1090#1072
        Width = 110
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.DisplayFormat = '0,00'
        Footer.ValueType = fvtFieldValue
        Footer.WordWrap = True
        Footers = <
          item
            EndEllipsis = True
            FieldName = 'CSELL'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNone
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ToolTips = True
            ValueType = fvtFieldValue
            WordWrap = True
          end
          item
            EndEllipsis = True
            FieldName = 'CRET'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMenuText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ToolTips = True
            ValueType = fvtFieldValue
            WordWrap = True
          end>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1080
        Width = 118
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object dsDetal: TDataSource
    DataSet = quDetal
    Left = 168
    Top = 216
  end
  object quDetal: TpFIBDataSet
    SelectSQL.Strings = (
      'select dep_name, uid, rn, adate, cost, csell, cret'
      'from CLIENT_DETAL_SELL(:CARD)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 128
    Top = 216
    object quDetalDEP_NAME: TFIBStringField
      FieldName = 'DEP_NAME'
      EmptyStrToNull = True
    end
    object quDetalUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quDetalRN: TFIBIntegerField
      FieldName = 'RN'
    end
    object quDetalADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quDetalCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quDetalCSELL: TFIBFloatField
      FieldName = 'CSELL'
    end
    object quDetalCRET: TFIBFloatField
      FieldName = 'CRET'
    end
  end
end
