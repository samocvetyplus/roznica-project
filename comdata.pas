unit comdata;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  pFIBQuery, pFIBDatabase, Db, pFIBDataSet, AppEvnts, ImgList, ComCtrls, dbctrls, menus, stdctrls,
  FR_DSet, FR_DBSet, FR_Class, ActnList, xmldom,
  Provider, Xmlxform, Printers, RxAppUtils,
  fib, FIBDatabase, FIBQuery, FIBDataSet,IniFiles, IB_Services, ComDrv32, TB2Item,
  rxPlacemnt, cxLocalization, ExtCtrls, ComObj;

const
  crFind = 90;

const TTagName : array [0..6] of string[30] = ('������� (��� ���������)', '��������� (��� ���������)', '������ (��� ���������)', '�� ����������', '����������� ����', '���� �� �������', '���� �� ����');

type TDepAccFunc=function(qu: TpFIBDataSet): boolean;

type
  t_array_int = array of integer;
  t_array_var = array of Variant;
  TDepInfo = record
    DepId   : integer;
    SortInd : integer;
    Name    : string;
    SName   : string;
    SSName  : string;
    Color   : TColor;
    Shop    : byte;
    Wh      : byte;
    Urf     : byte;
    City    : string;
    Address : string;
    PostInd : string;
    IsProg  : byte;
    Margin  : double;
    NotSetPr: boolean;
    R_UseDep: integer;
  end;

  TUserInfo = record
    UserId : integer;
    FIO : string;
    FName : string;
    LName : string;
    SName : string;
    Alias : string;
    Pswd : string;
    DepId : integer;
    Acc : integer;
    Adm : boolean;
    Manager : boolean;
    Seller :boolean;
    AllWh : boolean;
    UIDWHDate : TDateTime;
    UIDWHBD : TDateTime;
    UIDWHED : TDateTime;
    CalcRest : integer;
    RestDate : TDateTime;
    WHDate : TDateTime;
    L3BD : TDateTime;
    L3ED : TDateTime;
    L3Sup:smallint;
    ApplId: integer;
    PRWKIND:integer;
    ALLSeLL:integer;
  end;

  TNotifServerParam = record
    Notif_Ip : string;
    Notif_Port : integer;
    Notif_Use : boolean; // �������/�������� ������ ���������
  end;

  TdmCom = class(TDataModule)
    taRec: TpFIBDataSet;
    ilGoodTree: TImageList;
    ilButtons: TImageList;
    ae1: TApplicationEvents;
    quGetID: TpFIBQuery;
    dsRec: TDataSource;
    taRecD_RECID: TIntegerField;
    taRecD_COMPID: TIntegerField;
    taRecCOMP: TFIBStringField;
    quDep: TpFIBDataSet;
    dsDep: TDataSource;
    taMat: TpFIBDataSet;
    taMatD_MATID: TFIBStringField;
    taMatNAME: TFIBStringField;
    taMatSNAME: TFIBStringField;
    dsMat: TDataSource;
    taGood: TpFIBDataSet;
    taGoodD_GOODID: TFIBStringField;
    taGoodNAME: TFIBStringField;
    dsGood: TDataSource;
    taIns: TpFIBDataSet;
    taInsD_INSID: TFIBStringField;
    taInsNAME: TFIBStringField;
    dsIns: TDataSource;
    taComp: TpFIBDataSet;
    taCompD_COMPID: TIntegerField;
    taCompNAME: TStringField;
    taCompBOSS: TStringField;
    taCompOFIO: TStringField;
    taCompBUH: TStringField;
    taCompBOSSPHONE: TStringField;
    taCompOFIOPHONE: TStringField;
    taCompBUHPHONE: TStringField;
    taCompADDRESS: TStringField;
    taCompPHONE: TStringField;
    taCompFAX: TStringField;
    taCompEMAIL: TStringField;
    taCompWWW: TStringField;
    taCompINN: TStringField;
    taCompBIK: TStringField;
    taCompOKPO: TStringField;
    taCompOKONH: TStringField;
    taCompBANK: TStringField;
    taCompBILL: TStringField;
    taCompKBANK: TStringField;
    taCompKBILL: TStringField;
    taCompADRBILL: TStringField;
    taCompPASPSER: TStringField;
    taCompPASPNUM: TStringField;
    taCompDISTRDATE: TDateTimeField;
    taCompDISTRPLACE: TStringField;
    taCompPHIS: TIntegerField;
    taCompSELLER: TIntegerField;
    taCompBUYER: TIntegerField;
    taCompPSWD: TStringField;
    taCompWHDISPATCH: TIntegerField;
    taCompANCDISPATCH: TIntegerField;
    taCompCONFIDENCE: TFloatField;
    taCompBASKET: TIntegerField;
    taCompHASDISCOUNT: TIntegerField;
    taCompALREADYBUYED: TIntegerField;
    taCompLOGIN: TStringField;
    taCompLASTUSER: TStringField;
    taCompCITY: TStringField;
    taCompPOSTINDEX: TStringField;
    taCompSHOP: TSmallintField;
    taCompLOGO: TBlobField;
    taCompPRODUCER: TSmallintField;
    dsComp: TDataSource;
    taMol: TpFIBDataSet;
    dsMol: TDataSource;
    taArt: TpFIBDataSet;
    taArtD_ARTID: TIntegerField;
    taArtD_COMPID: TIntegerField;
    taArtD_MATID: TFIBStringField;
    taArtD_GOODID: TFIBStringField;
    taArtART: TFIBStringField;
    taArtUNITID: TIntegerField;
    taArtMEMO: TMemoField;
    taArtPICT: TBlobField;
    taArtComp: TStringField;
    taArtMat: TStringField;
    taArtGood: TStringField;
    taArtD_INSID: TFIBStringField;
    taArtIns2: TStringField;
    taArtFULLART: TFIBStringField;
    dsArt: TDataSource;
    quTmp: TpFIBQuery;
    taRecAKCIZ: TFloatField;
    taCompSNAME: TFIBStringField;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    dsNDS: TDataSource;
    taEdg: TpFIBDataSet;
    dsEdg: TDataSource;
    taEdgNAME: TFIBStringField;
    taEdgT: TSmallintField;
    taNDSLONGNAME: TFIBStringField;
    quServerTime: TpFIBQuery;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    dsPayType: TDataSource;
    taRecOPMARGIN: TFloatField;
    taCompCODE: TFIBStringField;
    taDiscount: TpFIBDataSet;
    dsDiscount: TDataSource;
    taDiscountD_DISCOUNTID: TIntegerField;
    taDiscountNAME: TFIBStringField;
    taDiscountSNAME: TFIBStringField;
    taDiscountDISCOUNT: TFloatField;
    taRecOPTRNORM: TSmallintField;
    taRecACHECK: TMemoField;
    fsCom: TFormStorage;
    taCompNDOG: TFIBStringField;
    taCompDOGBD: TDateTimeField;
    taCompDOGED: TDateTimeField;
    taCompREUED: TDateTimeField;
    taCompCERTN: TFIBStringField;
    taCompCERTDATE: TDateTimeField;
    taCompRecNo: TIntegerField;
    taMatGR: TFIBStringField;
    frReport: TfrReport;
    taRecRESTWHID: TIntegerField;
    taInsSNAME: TFIBStringField;
    taMatSORTIND: TSmallintField;
    taMatUNITID: TSmallintField;
    taInsSORTIND: TSmallintField;
    taInsUNITID: TSmallintField;
    quEdgT: TpFIBDataSet;
    quEdgTNAME: TFIBStringField;
    dsEdgT: TDataSource;
    quEdgS: TpFIBDataSet;
    quEdgSNAME: TFIBStringField;
    dsEdgS: TDataSource;
    taInsEdgT: TStringField;
    quAllComp: TpFIBDataSet;
    quAllCompD_COMPID: TIntegerField;
    quAllCompNAME: TFIBStringField;
    dsAllComp: TDataSource;
    taDep: TpFIBDataSet;
    taDepD_DEPID: TIntegerField;
    taDepNAME: TFIBStringField;
    taDepPARENT: TIntegerField;
    taDepCHILD: TSmallintField;
    taDepSHOP: TSmallintField;
    taDepWH: TSmallintField;
    taDepROUNDNORM: TSmallintField;
    taDepROUNDDIR: TSmallintField;
    taDepDISTRIBP: TSmallintField;
    taDepPOSTIND: TFIBStringField;
    taDepCITY: TFIBStringField;
    taDepADDRESS: TFIBStringField;
    taDepPHONE: TFIBStringField;
    taDepMARGIN: TFloatField;
    taDepSNAME: TFIBStringField;
    taDepCOLOR: TIntegerField;
    dsDep1: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpD_EMPID: TIntegerField;
    taEmpFNAME: TFIBStringField;
    taEmpLNAME: TFIBStringField;
    taEmpSNAME: TFIBStringField;
    taEmpALIAS: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    taEmpD_DEPID: TIntegerField;
    dsEmp: TDataSource;
    quDepT: TpFIBQuery;
    taClient: TpFIBDataSet;
    dsClient: TDataSource;
    taClientCLIENTID: TIntegerField;
    taClientNAME: TFIBStringField;
    taCompProd: TStringField;
    taCompPRODID: TIntegerField;
    quComp: TpFIBDataSet;
    quCompD_COMPID: TIntegerField;
    quCompNAME: TFIBStringField;
    quCompBOSS: TFIBStringField;
    quCompOFIO: TFIBStringField;
    quCompBUH: TFIBStringField;
    quCompBOSSPHONE: TFIBStringField;
    quCompOFIOPHONE: TFIBStringField;
    quCompBUHPHONE: TFIBStringField;
    quCompADDRESS: TFIBStringField;
    quCompPHONE: TFIBStringField;
    quCompFAX: TFIBStringField;
    quCompEMAIL: TFIBStringField;
    quCompWWW: TFIBStringField;
    quCompINN: TFIBStringField;
    quCompBIK: TFIBStringField;
    quCompOKPO: TFIBStringField;
    quCompOKONH: TFIBStringField;
    quCompBANK: TFIBStringField;
    quCompBILL: TFIBStringField;
    quCompKBANK: TFIBStringField;
    quCompKBILL: TFIBStringField;
    quCompADRBILL: TFIBStringField;
    quCompPASPSER: TFIBStringField;
    quCompPASPNUM: TFIBStringField;
    quCompDISTRDATE: TDateTimeField;
    quCompDISTRPLACE: TFIBStringField;
    quCompPHIS: TSmallintField;
    quCompSELLER: TSmallintField;
    quCompBUYER: TSmallintField;
    quCompPSWD: TFIBStringField;
    quCompWHDISPATCH: TSmallintField;
    quCompANCDISPATCH: TSmallintField;
    quCompCONFIDENCE: TFloatField;
    quCompBASKET: TSmallintField;
    quCompHASDISCOUNT: TSmallintField;
    quCompALREADYBUYED: TSmallintField;
    quCompLOGIN: TFIBStringField;
    quCompLASTUSER: TFIBStringField;
    quCompCITY: TFIBStringField;
    quCompPOSTINDEX: TFIBStringField;
    quCompSHOP: TSmallintField;
    quCompLOGO: TBlobField;
    quCompPRODUCER: TSmallintField;
    quCompSNAME: TFIBStringField;
    quCompCODE: TFIBStringField;
    quCompNDOG: TFIBStringField;
    quCompDOGBD: TDateTimeField;
    quCompDOGED: TDateTimeField;
    quCompREUED: TDateTimeField;
    quCompCERTN: TFIBStringField;
    quCompCERTDATE: TDateTimeField;
    dsrComp: TDataSource;
    taEmpALLWH: TSmallintField;
    taRet: TpFIBDataSet;
    taRetD_RETID: TFIBStringField;
    taRetRET: TFIBStringField;
    dsRet: TDataSource;
    taEdgEDGETIONID: TFIBStringField;
    taRecTMPDIR: TFIBStringField;
    quEdgTEDGETIONID: TFIBStringField;
    taInsEDGSHID: TFIBStringField;
    taInsEDGTID: TFIBStringField;
    quEdgSEDGETIONID: TFIBStringField;
    tr1: TpFIBTransaction;
    taRecTMPDB: TFIBStringField;
    taDepSPR: TSmallintField;
    taRecPTRNDS: TFloatField;
    taRecDEFDEP: TIntegerField;
    taRecDOCPATH: TFIBStringField;
    taRecLASTCALC: TDateTimeField;
    taDepISPROG: TSmallintField;
    frdsDict: TfrDBDataSet;
    taEmpACC: TIntegerField;
    taDepSSNAME: TFIBStringField;
    taDepURF: TSmallintField;
    taDepCOMPID: TIntegerField;
    taShop: TpFIBDataSet;
    taShopD_DEPID: TIntegerField;
    taShopSNAME: TFIBStringField;
    taShopNAME: TFIBStringField;
    taDepById: TpFIBDataSet;
    trUser: TpFIBTransaction;
    quUser: TpFIBDataSet;
    taDepNDSURF: TIntegerField;
    taPayTypeDAYSCOUNT: TSmallintField;
    taCompPAYTYPEID: TIntegerField;
    taCompNDSID: TIntegerField;
    taEmpPRWKIND: TSmallintField;
    taClientADDRESS: TFIBStringField;
    taDepNOTSETPR: TSmallintField;
    quHere: TpFIBQuery;
    taCompPADDRESS: TFIBStringField;
    taRecTOPMARGIN: TFloatField;
    taRecTOPTRNORM: TSmallintField;
    taCountry: TpFIBDataSet;
    dsCountry: TDataSource;
    taCountryD_COUNTRYID: TFIBStringField;
    taCountryNAME: TFIBStringField;
    taCompD_COUNTRYID: TFIBStringField;
    taArtD_COUNTRYID: TFIBStringField;
    taArtCOUNTRY: TFIBStringField;
    taDepCENTERDEP: TSmallintField;
    quCompD_COUNTRYID: TFIBStringField;
    quDepD_DEPID: TIntegerField;
    quDepNAME: TFIBStringField;
    quDepSNAME: TFIBStringField;
    quDepCOLOR: TIntegerField;
    quDepPARENT: TIntegerField;
    quDepCHILD: TSmallintField;
    quDepSHOP: TSmallintField;
    quDepWH: TSmallintField;
    quDepROUNDNORM: TSmallintField;
    quDepROUNDDIR: TSmallintField;
    quDepDISTRIBP: TSmallintField;
    quDepPOSTIND: TFIBStringField;
    quDepCITY: TFIBStringField;
    quDepADDRESS: TFIBStringField;
    quDepPHONE: TFIBStringField;
    quDepMARGIN: TFloatField;
    quDepFREG: TSmallintField;
    quDepISPROG: TSmallintField;
    quDepSPR: TSmallintField;
    quDepSSNAME: TFIBStringField;
    quDepURF: TSmallintField;
    quDepCOMPID: TIntegerField;
    quDepCENTERDEP: TSmallintField;
    quGrMAt: TpFIBQuery;
    tacomprep: TpFIBDataSet;
    dscomprep: TDataSource;
    tacomprepADDRESS: TFIBStringField;
    tacomprepD_COMPID: TIntegerField;
    tacomprepBOSS: TFIBStringField;
    tacomprepCODE: TFIBStringField;
    tacomprepPAYTYPEID: TIntegerField;
    tacomprepPAYTYPE: TFIBStringField;
    tacomprepFAX: TFIBStringField;
    tacomprepPHONE: TFIBStringField;
    tacomprepOFIO: TFIBStringField;
    tacomprepNAMECOMP: TFIBStringField;
    taClientNODCARD: TFIBStringField;
    taGoodsSam: TpFIBDataSet;
    dsGoodsSam: TDataSource;
    taGoodsSamD_GOODSID_SAM: TIntegerField;
    taGoodsSamNAME_SAM: TFIBStringField;
    taGoodsSamSNAME_SAM: TFIBStringField;
    taGoodsSamSORTIND: TSmallintField;
    taAddress: TpFIBDataSet;
    dsAddress: TDataSource;
    taAddressD_ADDRESS_ID: TIntegerField;
    taAddressADDRESS: TFIBStringField;
    taClientADDRESSID: TIntegerField;
    taClientHOME_FLAT: TFIBStringField;
    taAddressDEPID: TIntegerField;
    taAddressCOLOR: TIntegerField;
    taAddressSNAME: TFIBStringField;
    taClientSNAME: TFIBStringField;
    taClientCOLOR: TIntegerField;
    taClientDEPID: TIntegerField;
    taEmpNUMEMP: TIntegerField;
    taClientF1: TBooleanField;
    taClientR_INSERTD: TDateTimeField;
    taClientFMAIN: TIntegerField;
    quNSellitem: TpFIBDataSet;
    dsNsellItem: TDataSource;
    quNSellitemRN: TIntegerField;
    quNSellitemADATE: TDateTimeField;
    quNSellitemUID: TIntegerField;
    quNSellitemSNAME: TFIBStringField;
    quNSellitemCOLOR: TIntegerField;
    quNSellitemRET: TSmallintField;
    taCompDOGUNLIMITED: TSmallintField;
    taEmpALLSELL: TSmallintField;
    bk: TpFIBBackupService;
    rr: TpFIBRestoreService;
    dbUpdate: TpFIBDatabase;
    trUpd: TpFIBTransaction;
    quLoadUpd: TpFIBQuery;
    taAtt1: TpFIBDataSet;
    taAtt2: TpFIBDataSet;
    dsrAtt1: TDataSource;
    dsrAtt2: TDataSource;
    taAtt1ID: TFIBIntegerField;
    taAtt1NAME: TFIBStringField;
    taAtt1SNAME: TFIBStringField;
    taAtt1SORTIND: TFIBIntegerField;
    taAtt2ID: TFIBIntegerField;
    taAtt2NAME: TFIBStringField;
    taAtt2SNAME: TFIBStringField;
    taAtt2SORTIND: TFIBIntegerField;
    taSnameSup: TpFIBDataSet;
    dsSnameSup: TDataSource;
    taSnameSupD_SNAMESUPID: TFIBIntegerField;
    taSnameSupSNAME: TFIBStringField;
    taSnameSupFSUP: TFIBSmallIntField;
    taSnameSupFTABLE: TFIBSmallIntField;
    taSnameSupA_PATTERN: TFIBStringField;
    taSnameSupSupName: TFIBStringField;
    taSnameSupSNAME_SUP: TFIBStringField;
    tr2: TpFIBTransaction;
    taClientFDEL: TFIBSmallIntField;
    taAddressFDEL: TFIBSmallIntField;
    taRecNOTIF_IP: TFIBStringField;
    taRecNOTIF_PORT: TFIBStringField;
    taRecNOTIF_USE: TFIBSmallIntField;
    taGr: TpFIBDataSet;
    dsgr: TDataSource;
    taMatNameGR: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taMolD_MOLID: TFIBIntegerField;
    taMolD_COMPID: TFIBIntegerField;
    taMolFIO: TFIBStringField;
    quDepISFTPREP: TFIBSmallIntField;
    taDepISFTPREP: TFIBSmallIntField;
    taEmpPERMISSIONS_USER: TFIBSmallIntField;
    taEmpVIEWREFBOOK: TFIBIntegerField;
    taEmpEDITREFBOOK: TFIBIntegerField;
    quDischarge: TpFIBDataSet;
    dsDischarge: TDataSource;
    quDischargeD_EMPID: TFIBIntegerField;
    quDischargeFNAME: TFIBStringField;
    quDischargeLNAME: TFIBStringField;
    quDischargeSNAME: TFIBStringField;
    quDischargeDISCHARGE: TFIBSmallIntField;
    quDischargeD_DEPID: TFIBIntegerField;
    taEmpDISCHARGE: TFIBSmallIntField;
    quDischargeNameDep: TStringField;
    quDischargeColor: TIntegerField;
    taCompD_EMPID: TFIBIntegerField;
    taCompFIOUSER: TStringField;
    quUseClient: TpFIBDataSet;
    dsUseClient: TDataSource;
    quUseClientCLIENTID: TFIBIntegerField;
    quUseClientADDRESSID: TFIBIntegerField;
    quUseClientNODCARD: TFIBStringField;
    quUseClientNAME: TFIBStringField;
    taClientBIRTHDAY: TFIBDateTimeField;
    quDiscountSum: TpFIBDataSet;
    dsDiscountSum: TDataSource;
    quDiscountSumD_DISCOUNTSUMID: TFIBIntegerField;
    quDiscountSumLOWERLIMIT: TFIBFloatField;
    quDiscountSumHIGHLIMIT: TFIBFloatField;
    quDiscountSumD_DISCOUNTID: TFIBIntegerField;
    quDiscountSumCOLOR: TFIBIntegerField;
    quDiscountSumDiscountName: TStringField;
    quDiscountSumColorFiled: TStringField;
    taClientQRET: TFIBIntegerField;
    taClientCRET: TFIBFloatField;
    taClientQSELL: TFIBIntegerField;
    taClientCSELL: TFIBFloatField;
    taClientCOST: TFIBFloatField;
    taClientCOLORCOST: TFIBIntegerField;
    quNSellitemCOST: TFIBFloatField;
    quNSellitemCOSTSELL: TFIBFloatField;
    quNSellitemCOSTRET: TFIBFloatField;
    quLogGetID: TpFIBQuery;
    taClientISLETTER: TFIBSmallIntField;
    taClientF_LOSE: TFIBSmallIntField;
    taClientOLDNODCARD: TFIBStringField;
    taClientF2: TFIBSmallIntField;
    taClientF3: TFIBSmallIntField;
    taAddressUPDATEDATE: TFIBDateTimeField;
    taClientUPDATEDATE: TFIBDateTimeField;
    taRecINVENTORY_GLOBAL: TFIBSmallIntField;
    taDepAPPLDEPNUM: TFIBSmallIntField;
    taDepISDELETE: TFIBSmallIntField;
    taCompBOSSPOST: TFIBStringField;
    DepSortWithDel: TpFIBDataSet;
    DepSortWithDelR_NO: TFIBBCDField;
    DepSortWithDelD_DEPID: TFIBIntegerField;
    DepSortWithDelSNAME: TFIBStringField;
    DepSortWithDelCOLOR: TFIBIntegerField;
    taDiscountSHOWSELL: TFIBSmallIntField;
    taCompKPP: TFIBStringField;
    taCompCOMMISSION: TFIBSmallIntField;
    taGoodSORTIND: TFIBSmallIntField;
    taGoodCONTRSIZE: TFIBSmallIntField;
    taRecMAXCOUNTWH: TFIBSmallIntField;
    taCompWORKORG: TFIBSmallIntField;
    taCompRETORG: TFIBSmallIntField;
    taCompWORKPROD: TFIBSmallIntField;
    taCompWORKBUYER: TFIBSmallIntField;
    taRecMAXCOUNTUIDWH: TFIBSmallIntField;
    quDiscountSumT: TFIBSmallIntField;
    quDiscountSumVALID_BD: TFIBDateTimeField;
    quDiscountSumVALID_ED: TFIBDateTimeField;
    quDiscountSumINFINITE_ED: TFIBSmallIntField;
    quDiscountSumCLOSED: TFIBSmallIntField;
    quDiscountSumQSELL: TFIBSmallIntField;
    taUpdComponent: TpFIBDataSet;
    taUpdComponentNAME: TFIBStringField;
    taUpdComponentURL: TFIBStringField;
    taUpdComponentVERSION: TFIBStringField;
    taUpdComponentCLIENTLOCATION: TFIBStringField;
    taUpdComponentNEEDREG: TFIBIntegerField;
    quDepAPPLDEPNUM: TFIBSmallIntField;
    quDepDEPARTMENTNAME: TFIBStringField;
    taDiscountLOWERLIMIT: TFIBFloatField;
    taDiscountHIGHLIMIT: TFIBFloatField;
    taCompIsBank: TIntegerField;
    taContractType: TpFIBDataSet;
    dsContractType: TDataSource;
    taContractTypeID: TFIBIntegerField;
    taContractTypeNAME: TFIBStringField;
    Localizer: TcxLocalizer;
    taCompFORMATEDNAME: TFIBStringField;
    taCompADDRESSPRINT: TFIBIntegerField;
    taCompADDRESSPOST: TFIBStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure ae1Restore(Sender: TObject);
    procedure ae1Exception(Sender: TObject; E: Exception);
    procedure DoCommitRetaining(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taCompAfterScroll(DataSet: TDataSet);
    procedure taCompNewRecord(DataSet: TDataSet);
    procedure taMolBeforeInsert(DataSet: TDataSet);
    procedure taMolNewRecord(DataSet: TDataSet);
    procedure taArtAfterScroll(DataSet: TDataSet);
    procedure taArtBeforePost(DataSet: TDataSet);
    procedure taArtNewRecord(DataSet: TDataSet);
    procedure taArtBeforeInsert(DataSet: TDataSet);
    procedure taEdgNewRecord(DataSet: TDataSet);
    procedure taEdgBeforeOpen(DataSet: TDataSet);
    procedure taPayTypeNewRecord(DataSet: TDataSet);
    procedure taDiscountNewRecord(DataSet: TDataSet);
    procedure taCompCalcFields(DataSet: TDataSet);
    procedure frReportGetValue(const ParName: String;
      var ParValue: Variant);
    procedure UnitGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure UnitSetText(Sender: TField; const Text: String);
    procedure taDepBeforeOpen(DataSet: TDataSet);
    procedure taEmpBeforeInsert(DataSet: TDataSet);
    procedure taEmpNewRecord(DataSet: TDataSet);
    procedure taClientNewRecord(DataSet: TDataSet);
    procedure quUserBeforeOpen(DataSet: TDataSet);
    procedure taGoodsSamBeforeDelete(DataSet: TDataSet);
    procedure taGoodsSamBeforeEdit(DataSet: TDataSet);
    procedure taAddressBeforeClose(DataSet: TDataSet);
    procedure taAddressBeforeDelete(DataSet: TDataSet);
    procedure taClientBeforeOpen(DataSet: TDataSet);
    procedure taClientPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taClientCalcFields(DataSet: TDataSet);
    procedure quNSellitemBeforeOpen(DataSet: TDataSet);
    procedure taCompBeforePost(DataSet: TDataSet);
    procedure taRetNewRecord(DataSet: TDataSet);
    procedure taRetBeforeDelete(DataSet: TDataSet);
    procedure taGoodsSamNewRecord(DataSet: TDataSet);
    procedure taAddressNewRecord(DataSet: TDataSet);
    procedure taAtt1NewRecord(DataSet: TDataSet);
    procedure taAtt2NewRecord(DataSet: TDataSet);
    procedure taArtBeforeOpen(DataSet: TDataSet);
    procedure taSnameSupBeforeOpen(DataSet: TDataSet);
    procedure taSnameSupNewRecord(DataSet: TDataSet);
    procedure taSnameSupCalcFields(DataSet: TDataSet);
    procedure taEmpBeforeDelete(DataSet: TDataSet);
    procedure taMolBeforeOpen(DataSet: TDataSet);
    procedure quUseClientBeforeOpen(DataSet: TDataSet);
    procedure quDiscountSumNewRecord(DataSet: TDataSet);
    procedure quNSellitemCalcFields(DataSet: TDataSet);
    procedure taMatBeforePost(DataSet: TDataSet);
    procedure taCompAfterPost(DataSet: TDataSet);
  private
    FDepInfo : array of TDepInfo;
    FNotifServerParam: TNotifServerParam;
    FSelfCompId: integer;
    FSelfCompName: string;
    FGlobalInventoryEnabled: byte;
    function LoginFunction(UsersTable: TpFIBDataSet; const Password: String): Boolean;
    function GetDepInfo(Index: integer): TDepInfo;
    function GetDepCount: integer;
    function GetDepInfo1(Index: integer): TDepInfo;
    function GetUser: TUserInfo;
    function GetDefaultDep: TDepInfo;
    procedure SetGlobalInventoryEnabled(const Value: byte);
  public
    TypeTag:string;
    {����������� � �������������� �����}
    IsActSellList:boolean;
    IsLoseCard :Boolean;
    OptBuyerId :integer;
    FReg_ : boolean; {���������� ����������� � ini}
    IsActive : boolean; {���� ����������� � true, �� �� ��������� ��� ��������� ���������� ����������}
    IsPrintCheck:boolean; {�������� ��������� ��� ��� ������� �������������� ��������� ��� ������}
    Printtype:integer;
    IsMakeLog, IsExistsLog : boolean;
  //  SScanZ, ScanComZ, SScanM, ScanComM,
    FregCom:string; // ��� ���������� �������� 1-����������� ��������; 2- com ����
    FBan1, FBan2: boolean; //���������� ��� 2 ��������, ������� ���������� ����� �� ��������� ��������� ��������
    FSup1, Fsup2, FTable, NewFsup:integer;
    Bdata,Edata:tdatetime;
    fdata, ClientUpDate:boolean;
    FilterFmain1,FilterFmain2, FilterDepID, FilterRepDepID :integer;
    FilterFIO: string;
    Logined: boolean;
    SelectDB: boolean;
    clMoneyGreen: TColor;
    clCream: TColor;
    clPink: TColor;
    ConnectionError: boolean;
    UserName: string;
    UserId: integer;
    UserDepId: integer;
    SelfName: string;
    SelfId: integer;
    CompCat: integer;
    D_DepId: integer;
    D_MatId: string[10];
    D_GrMatID: string[10];
    D_GoodId: string[10];
    D_CompId: integer;
    D_SupId: integer;
    D_Note1 : integer;
    D_Note2 : integer;
   { D_Att1 : integer;
    D_Att2 : integer;}
    D_InsId: string[10];
    D_CountryId: string[10];
    D_Att1Id: integer;
    D_Att2Id: integer;
    UIDWHSZ_:string;
    FullArt: string;
    Art2: string;
    FlagErrGoods:boolean;
    (* �������� ������� ��� ������� ���� ����� ��-��������, ��-���������� *)
    FilterArt : string;
    EdgT: integer;
    FirstMonthDate: TDateTime;
    UserAllWh: boolean;
    UserSID: string;
    FDocId : integer; // ��. ����� (�����������)
    Adm: boolean;
    Manager : boolean;
    Seller :boolean; //���-�� �������� � ���������� ������
    Acc: integer;
    ViewRefBook:integer;
    EditRefBook:integer;     
    EntryCalcRest: integer;
    HereName: string;
    ShowNumEmp :boolean;
    ShowInfClient :boolean;
    flcall_delsel : integer; {���� ������ ���������� ����� DelSel}
    mas_dep: t_array_int;
    ProducerChanged: Boolean;
    IsBookKeeper: Boolean;

    {��� ��������������}
    D_MatId_I, D_InsId_I, D_CountryId_I, D_GrMatID_I, D_GoodId_I: string[10];
    D_CompId_I, D_SupId_I, D_Note1_I, D_Note2_I, D_Att1Id_I, D_Att2Id_I: integer;

    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    // ��������� ���������� � ������ �� D_DepId
    property Dep[Index : integer] : TDepInfo read GetDepInfo; default;
    // ��������� ���������� � ������ �� �������
    property DepInfo[Index : integer] : TDepInfo read GetDepInfo1;
    property DefaultDep : TDepInfo read GetDefaultDep;
    property DepCount : integer read GetDepCount;
    //property IsProg : integer read GetIsProg;
    property  User : TUserInfo read GetUser;
    // �����������, � ������� �������� ��������� 
    property SelfCompId : integer read FSelfCompId;
    property SelfCompName : string read FSelfCompName;
    property GlobalInventoryEnabled : byte read FGlobalInventoryEnabled write SetGlobalInventoryEnabled;

    // ��������� ������� ���������
    property NotifServerParam : TNotifServerParam read FNotifServerParam write FNotifServerParam;

    function  LogGetID(TableID: integer): integer;
    function  GetID(TableID: integer): integer;        {TPopUpMenu}
    procedure FillDepMenu(q: TpFIBDataSet; pm: TComponent; f: TDepAccFunc; ev: TNotifyEvent;Gr: boolean);
    procedure FillTagMenu(pm: TComponent; ev: TNotifyEvent; TypePm: integer);
    procedure ArtTvChange(Node: TTreeNode);
    procedure ArtTreeExpanding(tv: TTreeView; Node: TTreeNode);
    function  GetServerTime: TDateTime;
    procedure SetArtFilter(Params: TFIBXSQLDA);
    procedure InitDepInfo;
    procedure ExecuteQutmp(Q1:TpFIBQuery; TextSql:String; CountPar:integer; var FieldsRes:t_array_var);
  end;

var
  dmCom: TdmCom;
  wp: TPicture;

type TAccFunc=function:boolean;
var   AccFunc: TAccFunc;

function TranslateMsg(var E:Exception): boolean;
procedure XChange(i, j: integer);
procedure InitArtTree(tv: TTreeView);
procedure ArtBtnClick(D: integer; lb: TListBox; tv: TTreeView);
procedure PrintDict(ds : TDataSource; rptName : integer);
function  GetSYear(ADate: TDateTime): string;
function DefaultIniName : string;

const LevelCount=5;

const TreeSQL: array[0..LevelCount-1] of string=
('SELECT D_COMPID, NAME FROM D_COMP WHERE PRODUCER=1 ORDER BY NAME',
 'SELECT D_MATID, NAME FROM D_MAT ORDER BY NAME',
 'SELECT D_GOODID, NAME FROM D_GOOD ORDER BY NAME',
 'SELECT D_INSID, NAME FROM D_INS ORDER BY NAME',
 'SELECT D_COUNTRYID, NAME FROM D_COUNTRY ORDER BY NAME');

var TreeInd: array[0..LevelCount-1] of integer=(0,1,2,3,4);


resourcestring
  rcFRNotReady =  '���������� ����������� �� �����.';

implementation

uses M207IBLogin, rxStrUtils, RxMaxMin, Math, Variants, DBTree , Err,
     DIns, ServData, ReportData, M207Proc, UtilLib, dbUtil,
     Comp, MsgDialog, DateUtils, IbErr, uUtils, uApplication, Data, Data2;

{$R *.DFM}

procedure TdmCom.ExecuteQutmp(Q1:TpFIBQuery; TextSql:String; CountPar:integer; var FieldsRes:t_array_var);
var i:integer;
begin
 with q1 do
  begin
   close;
   SQL.Text:=TextSql;
   ExecQuery;
   if CountPar>0 then
   begin
    SetLength(FieldsRes,CountPar);
    for i:=0 to CountPar-1 do FieldsRes[i]:=Fields[i].AsVariant;
   end;
   Transaction.CommitRetaining;
   Close;
 end;
end;

function  GetSYear(ADate: TDateTime): string;
var y, m, d: word;
begin
  DecodeDate(ADate, y, m, d);
  Result:=IntToStr(y);
end;

procedure TdmCom.ArtTreeExpanding(tv: TTreeView; Node: TTreeNode);
var pN: TTreeNode;
    ND: TNodeData;
    NText: string[60];
begin
  ND:=TNodeData(Node.Data);
  if NOT ND.Loaded then
    with quTmp do
      begin
        ND.Loaded:=True;
        SQL.Text:=TreeSQL[TreeInd[Node.Level]];
        ExecQuery;
        while NOT EOF do
          begin
            NText:=DelRSpace(quTmp.Fields[1].AsString);
            pN:=tv.Items.AddChild(Node,NText) ;
            pN.StateIndex:=1;
            pN.HasChildren:=Node.Level<LevelCount-1;
            ND:=TNodeData.Create;
            pN.Data:=ND;
            ND.Loaded:=False;
            ND.Code:=DelRSpace(quTmp.Fields[0].AsString);
            ND.Name:=NText;
            Next;
          end;
        Close;
      end;
end;

procedure TdmCom.ArtTvChange(Node: TTreeNode);
var s, s1: string;
    i: integer;
    ND: TTreeNode;
begin
  with dmCom do
    begin
      s:='';
      ND:=Node;
      D_CompID:=-1;
      D_MatId:='';
      D_GoodID:='';
      D_CountryID:='';
      D_InsID:='';
      for i:=Node.Level-1 downto 0 do
        begin
          case TreeInd[i] of
              0: begin
                   s1:=' A.D_COMPID='+VarToStr(TNodeData(ND.Data).Code);
                   D_CompId:=TNodeData(ND.Data).Code;
                 end;
              1: begin
                   s1:=' A.D_MATID='''+VarToStr(TNodeData(ND.Data).Code)+'''';
                   D_MatId:=TNodeData(ND.Data).Code;
                 end;
              2: begin
                   s1:=' A.D_GOODID='''+VarToStr(TNodeData(ND.Data).Code)+'''';
                   D_GoodId:=TNodeData(ND.Data).Code;
                end;
              3: begin
                   s1:=' A.D_INSID='''+VarToStr(TNodeData(ND.Data).Code)+'''';
                   D_InsId:=TNodeData(ND.Data).Code;
                end;
               4: begin
                   s1:=' A.D_COUNTRYID='''+VarToStr(TNodeData(ND.Data).Code)+'''';
                   D_COUNTRYId:=TNodeData(ND.Data).Code;
                end;
            end;
          if s='' then s:=s1
          else s:=s+' AND '+s1;
          ND:=ND.Parent;
        end;
      if s<>'' then s:=' AND '+s+' ';
      with taArt do
        begin
          Active:=False;
          SelectSQL[9]:=s;
          Open;
        end;
  end;
end;



procedure ArtBtnClick(D: integer; lb: TListBox; tv: TTreeView);
var i: integer;
begin
  with lb do
    begin
      i:=ItemIndex;
      if i=-1 then exit;
      case D of
          1: if i>0 then
               begin
                 Items.Move(i, i-1);
                 //t:=TreeInd[i-1];
                 XChange(i, i-1);
                 InitArtTree(tv);
               end;
          2: if ItemIndex<Items.Count-1 then
               begin
                 Items.Move(i, i+1);
                 XChange(i, i+1);
                 InitArtTree(tv);
               end;
        end
    end;
end;


procedure InitArtTree(tv: TTreeView);
begin
  tv.Items.Clear;
  InitTree('��� ��������', tv, NULL);
  tv.Selected:=tv.TopItem;
end;


procedure XChange(i, j: integer);
var t: integer;
begin
  t:=TreeInd[j];
  TreeInd[j]:=TreeInd[i];
  TreeInd[i]:=t;
end;

function GetDefaultPrinter: string;
var
  ResStr: array[0..255] of Char;
begin
  GetProfileString('Windows', 'device', '', ResStr, 255);
  Result := StrPas(ResStr);
end;

procedure SetDefaultPrinter1(NewDefPrinter: string);
var
  ResStr: array[0..255] of Char;
begin
  StrPCopy(ResStr, NewdefPrinter);
  WriteProfileString('windows', 'device', ResStr);
  StrCopy(ResStr, 'windows');
  SendMessage(HWND_BROADCAST, WM_WININICHANGE, 0, Longint(@ResStr));
end;

procedure SetDefaultPrinter2(PrinterName: string);
var
  I: Integer;
  Device: PChar;
  Driver: PChar;
  Port: PChar;
  HdeviceMode: THandle;
  aPrinter: TPrinter;
begin
  Printer.PrinterIndex := -1;
  GetMem(Device, 255);
  GetMem(Driver, 255);
  GetMem(Port, 255); 
  aPrinter := TPrinter.Create; 
  try
    for I := 0 to Printer.Printers.Count - 1 do 
    begin 
      if Printer.Printers.Strings[i] = PrinterName then
      begin
        aprinter.PrinterIndex := i;
        aPrinter.getprinter(device, driver, port, HdeviceMode); 
        StrCat(Device, ',');
        StrCat(Device, Driver); 
        StrCat(Device, Port);
        WriteProfileString('windows', 'device', Device);
        StrCopy(Device, 'windows'); 
        SendMessage(HWND_BROADCAST, WM_WININICHANGE, 
          0, Longint(@Device)); 
      end;
    end; 
  finally
    aPrinter.Free; 
  end; 
  FreeMem(Device, 255); 
  FreeMem(Driver, 255); 
  FreeMem(Port, 255); 
end;

procedure TdmCom.DataModuleCreate(Sender: TObject);
var i: integer;
    ClientLocation, ClientLocationDir :string;
    IniF: TIniFile;
    St1,St2, St3:tstringList;
    r : Variant;
    exeurl : string;
begin
  OnGetDefaultIniName := DefaultIniName;
  Logined:=False;


  ConnectionError:=False;
  SelectDB:=False;
  for i:=1 to ParamCount do
    if UpperCase(ParamStr(i))='SELECTDB' then SelectDB:=True ;

  if NOT LoginDB(db, 'D_Emp', 'Alias', LoginFunction, 3, SelectDB) then
     begin
       dmCom.Free;
       exit;
     end;
  db.Connected:=True;
 
  SetApplicationUserID(UserID);
  clMoneyGreen:=RGB(192, 220, 192);
  clCream:=RGB(230, 210, 180);
  clPink:=RGB(250, 170, 170);
  Logined:=True;
  FilterArt := '';

  if not tr.Active then  tr.StartTransaction;
  quHere.ExecQuery;
  HereName:=DelRSpace(quHere.Fields[0].AsString);
  quHere.Close;
  GetUser;
  tr.CommitRetaining;
  Application.Title:=HereName;
  with quTmp do
    begin
      SQL.Text:='UPDATE D_REC SET GDBNAME='''+UpperCase(db.DatabaseName)+'''';
      if not tr.Active then tr.StartTransaction;
      ExecQuery;
      tr.CommitRetaining;
    end;

  {���������� ���������� �� ini �����}
  IniF := TIniFile.Create(GetIniFileName);
  st1:= TStringList.Create;
  st2:= TStringList.Create;
  st3:= TStringList.Create;

  IniF.ReadSectionValues('SELL', st1);
  IniF.ReadSectionValues('SCAN', st2);
  IniF.ReadSectionValues('TAGTYPE', st3);

  ShowNumEmp:=false;
  ShowInfClient:=false;
  IsPrintCheck:=false;
  FReg_:=false;

  if st1.Values['PRINTTYPE']<>'' then Printtype:=strtoint(st1.Values['PRINTTYPE'])
  else Printtype:=0;

  if st1.Values['SHOWNUMEMP']='1' then ShowNumEmp:=true;
  if st1.Values['SHOWINFCLIENT']='1' then ShowInfClient:=true;
  if st1.Values['ISPRINTCHECK']='1' then IsPrintCheck:=true;
  if st1.Values['FREG']='1' then FReg_:=true;
  FregCom :=st1.Values['FREGCOM'];
  if FregCom='' then FregCom:='COM1';
  if st3.Values['TAGTYPE']='' then TypeTag:='3;' else TypeTag:=st3.Values['TAGTYPE'];

  st1.Free;
  st2.Free;
  st3.Free;
  IniF.Free;
  if not taRec.Transaction.Active then taRec.Transaction.StartTransaction
  else taRec.Transaction.CommitRetaining;
  OpenDataSets([taRec]);
  taRec.Transaction.CommitRetaining;
  FNotifServerParam.Notif_Ip := taRecNOTIF_IP.AsString;
  if taRecNOTIF_PORT.IsNull then FNotifServerParam.Notif_Port := 0
  else FNotifServerParam.Notif_Port := taRecNOTIF_PORT.AsInteger;
  if taRecNOTIF_USE.IsNull then FNotifServerParam.Notif_Use := False
  else FNotifServerParam.Notif_Use := taRecNOTIF_USE.AsInteger = 1;
  FSelfCompId := taRecD_COMPID.AsInteger;
  FSelfCompName := taRecCOMP.AsString;
  FGlobalInventoryEnabled := taRecINVENTORY_GLOBAL.AsInteger;
  CloseDataSets([taRec]);
  taRec.Transaction.CommitRetaining;

  Localizer.Active := True;

  Localizer.Locale := 1049;

  Localizer.Translate;
end;


function TdmCom.LoginFunction(UsersTable: TpFIBDataSet; const Password: String): Boolean;
var     LName, SName: string;
begin
  Result:=False;
  if Password=DelESpace(UsersTable.FieldByName('Pswd').AsString) then
     begin
       with UsersTable do
         begin
           UserName:=FieldByName('FName').AsString;
           LName:=FieldByName('LName').AsString;
           SName:=FieldByName('SName').AsString;

           if (Length(LName)>0) AND (Length(SName)>0) then
             UserName:=UserName+' '+LName[1]+'.'+SName[1]+'.';
           UserId:=FieldByName('D_EmpId').AsInteger;

           if UserId<0 then UserSID:='_'+IntToStr(Abs(UserId))
           else UserSID:=IntToStr(UserId);
                                                        
           if NOT FieldByName('D_DepId').IsNull then
              UserDepId:=FieldByName('D_DepId').AsInteger
           else UserDepId:=-1;

           UserAllWh:=FieldByName('ALLWH').AsInteger=1;
           Acc:=FieldByName('ACC').AsInteger;
           ViewRefBook:=FieldByName('ViewRefBook').AsInteger;
           EditRefBook:=FieldByName('EditRefBook').AsInteger;

           if FieldByName('PERMISSIONS_USER').IsNull then Manager:=true
           else begin
            Adm:=FieldByName('PERMISSIONS_USER').AsInteger=0;
            Manager:=FieldByName('PERMISSIONS_USER').AsInteger=1;
            Seller :=FieldByName('PERMISSIONS_USER').AsInteger=2;
            IsBookKeeper := FieldByName('PERMISSIONS_USER').AsInteger=3;
           end;


           DocPreview := TdcPreview(FieldByName('PRWKIND').AsInteger);

           if FieldByName('DISCHARGE').AsInteger=1 then Result:=false
           else Result:=True;
        end;
    end;
end;

procedure TdmCom.DataModuleDestroy(Sender: TObject);
begin
  if tr1.Active then tr1.Commit;
  if Assigned(FDepInfo) then Finalize(FDepInfo);
  db.Connected:=False;
end;

procedure TdmCom.ae1Restore(Sender: TObject);
begin
  RestoreApp;
end;

procedure TdmCom.ae1Exception(Sender: TObject;
  E: Exception);
var s: TString255;
begin
  if E.InheritsFrom(fib.EFIBError) then ShowIbError(EFIBError(E))
  else begin
    s:=E.Message;
    TranslateMsg(E);
    ShowError([E.Message, s]);
  end;
end;

function TranslateMsg(var E:Exception): boolean;
begin
  Result:=True;

  if Pos('violation of PRIMARY', E.Message)>0 then
    E.Message:='��� ������ ����� ���������� ��������'
  else
  if Pos('Cannot perform this operation on an empty dataset', E.Message)>0 then
    begin
      E.Message:='���������� ������� ������ �� ������ �������';
      Result:=False;
    end
  else
  if (Pos('CANT_DELETE', E.Message)>0) or (Pos('CANNOT DELETE THIS RECORD', E.Message)>0) then
    E.Message:='���������� ������� ������ ������'
  else
  if Pos('EDIT_ERR', E.Message)>0 then
    E.Message:='���������� ������������� ������ ������'
  else
  if (Pos('Key violation', E.Message)>0) or  (Pos('DUP_CODE', E.Message)>0)  then
    E.Message:='��� ������ ����� ���������� ��������'
  else
  if (Pos('Key violation', E.Message)>0) or  (Pos('EMPTY_CODE', E.Message)>0)  then
    E.Message:='��� �� ����� ���� ������!'
  else
  if Pos('Error writing data to the connection', E.Message)>0 then
    begin
      MessageDialog('������ ��� �������� ������ �������'#10#13'���������� ������ ��������� ����������',
                  mtError, [mbOK], 0);
      Halt(1);
    end
  else
  if Pos('Error reading data from the connection', E.Message)>0 then
    begin
      MessageDialog('������ ��� ��������� ������ �� �������'#10#13'���������� ������ ��������� ����������',
                  mtError, [mbOK], 0);
      Halt(1);
    end
  else
  if Pos('UNAVAILABLE DATABASE', UpperCase(E.Message))>0 then
    begin
      MessageDialog('���������� ���������� ���������� � ��������� ����� ������'#10#13'���������� ������ ��������� ����������',
                  mtError, [mbOK], 0);
      Halt(1);
    end
  else
  if Pos('arithmetic exception, numeric overflow, or string truncation', E.Message)>0 then
    E.Message:='��������������, ��������� ��� ��������� ������������'
  else
  if Pos(UpperCase('Unable to complete network request to host'), UpperCase(E.Message))<>0 then
      begin
        MessageDialog('������ ��� ���������� ������� � �������.'#10#13'���������� ������ ��������� ����������.',
                    mtError, [mbOK], 0);
        Halt(1);
      end;
  if E.Message='Cannot perform this operation on an empty dataset' then
     E.Message:='���������� ������� ������ �� ������ �������'
  else
  if Pos('violation of FOREIGN KEY constraint', E.Message)<>0 then E.Message:='���������� ������� ������ ������'
  else
  if Pos('DEMO_VERSION', E.Message)<>0 then E.Message:='����-������!!!'
  else
  if (Pos('update conflicts with concurrent update', E.Message)<>0) or
     (Pos('deadlock', E.Message)<>0) then
     E.Message:='������ �������� ������ �������������'#10#13'�������������� ����������'
  else
  if Pos('A deadlock was detected', E.Message)<>0 then E.Message:='������� ����������'
  else
  if Pos('LITTLE_QUANTITY', E.Message)<>0 then E.Message:='��������� ���������� ������ �����������'
  else
  if Pos('LITTLE_REST', E.Message)<>0 then E.Message:='��������� ���������� ��������� ����������'
  else
  if Pos('Could not find object', E.Message)<>0 then E.Message:='����������� �� �����������'
  else
  if Pos('DUPLICATE_VALUE', E.Message)<>0 then E.Message:='������������ ������'
  else
  if Pos('validation error for column SZ, value "*** null ***"', E.Message)<>0 then E.Message:='���������� ������ �������� � ���� ������'
  else
  if Pos('unique index "ART2_IDX1"', E.Message)<>0 then E.Message:='������������ ������� 2'
  else
  if Pos('D_COMP_CODE', E.Message)<>0 then E.Message:='������������ ���'
  else
  if Pos('RESTWH', E.Message)<>0 then E.Message:='����������� ������ ����� ��� ��������'
  else
  if Pos('DUP_A', E.Message)<>0 then E.Message:='������������ �������'
  else
  if Pos('DUP_A2', E.Message)<>0 then E.Message:='������������ ������� 2'
  else
  if Pos('SPRICE0', E.Message)<>0 then E.Message:='� ���������� ������ �������� ��������� ���� ����� �������� ����,'+#13#10+
                                                  '������� �������� �� �����������.'+#13#10+
                                                  '���������, ����������, ��� ������������������ ��������, ������� ������� � ���� ��������'+#13#10+
                                                  '� �������� �� ���� ������������, ���.35-95-80.'+#13#10+
                                                  '�������.'
  else
  if Pos('PRICE0', E.Message)<>0 then E.Message:='� ���������� ������ �������� ��������� ���� ����� �������� ����,'+#13#10+
                                                  '������� �������� �� �����������.'+#13#10+
                                                  '���������, ����������, ��� ������������������ ��������, ������� ������� � ���� ��������'+#13#10+
                                                  '� �������� �� ���� ������������, ���.35-95-80.'+#13#10+
                                                  '�������.'
  else
  if Pos('OPENEDPR', E.Message)<>0 then e.Message:='���������� ������� ������ ���������, '+#13#10+
                                                   '�.�. ���������� ���������� ������� '+#13#10+
                                                   '� ������������� � ��������� ����������'
  else
  if Pos('UIDWHCALC', E.Message)<>0 then e.Message:='��������� ������ ����������� ������ �������������';
end;

function TdmCom.GetID(TableID: integer): integer;
var b: boolean;
begin
  with quGetID do
    begin
      b := Transaction.Active;
      if b then Transaction.CommitRetaining
      else Transaction.StartTransaction;
      if Open then Close;

      Params[0].AsInteger:=TableID;
      ExecQuery;
      Result:=Fields[0].AsInteger;
      Close;
      if b then Transaction.CommitRetaining
      else Transaction.Commit;
    end;
end;

function TdmCom.LogGetID(TableID: integer): integer;
var b: boolean;
begin
  with quLogGetID do
    begin
      b := Transaction.Active;
      if b then Transaction.CommitRetaining
      else Transaction.StartTransaction;
      if Open then Close;

      Params[0].AsInteger:=TableID;
      ExecQuery;
      Result:=Fields[0].AsInteger;
      Close;
      if b then Transaction.CommitRetaining
      else Transaction.Commit;
    end;
end;

procedure TdmCom.FillTagMenu(pm: TComponent; ev: TNotifyEvent; TypePm:integer);
var mi: TMenuItem;
    pi: TTBitem;
    i, j:integer;
begin
  i:=1;
 while ExtractWord(i, typetag, [';'])<>'' do
 begin
  j:=strtoint(ExtractWord(i, typetag, [';']));
  case TypePm of
  0: begin
      mi:=TMenuItem.Create(pm);
      mi.Caption:=TTagName[j];
      mi.Tag:=j;
      mi.OnClick:=ev;
      if (pm is TPopupMenu) then TPopupMenu(pm).Items.Add(mi)
      else TMenuItem(pm).Add(mi);
      //������������
      if j=3 then begin
      for j:=4 to 6 do
      begin
      mi:=TMenuItem.Create(pm);
      mi.Caption:=TTagName[j];
      mi.Tag:=j;
      mi.OnClick:=ev;
      if (pm is TPopupMenu) then TPopupMenu(pm).Items.Add(mi)
      else TMenuItem(pm).Add(mi);
      end;

      end;
     end;
  1: begin
      pi:=TTBitem.Create(pm);
      pi.Caption:=TTagName[j];
      pi.Tag:=j;
      pi.OnClick:=ev;
      if (pm is TTBSubmenuItem) then TTBSubmenuItem(pm).Add(pi)
      else TTBPopupMenu(pm).Items.Add(pi);
     end;
   end;
  inc(i);
 end;
end;

                                                {TPopupMenu}
procedure TdmCom.FillDepMenu(q: TpFIBDataSet; pm: TComponent; f: TDepAccFunc; ev: TNotifyEvent; Gr: boolean);
var i: integer;
    mi: TMenuItem;
begin
  try
    tr.Active:=True;
  with q do
    begin
      Active:=True;
      i:=0;
      while NOT EOF do
        begin
          if  f(q) then
            begin
              mi:=TMenuItem.Create(pm);
              mi.Caption:=q.Fields[2].AsString;
              mi.Tag:=q.Fields[0].AsInteger;
              mi.OnClick:=ev;
              if Gr then mi.GroupIndex:=1
              else mi.GroupIndex:=0;
              mi.RadioItem:=True;
              Inc(i);
              if i>30 then
                begin
                  mi.Break:=mbBreak;
                  i:=0;
                end;
              if (pm is TPopupMenu) then TPopupMenu(pm).Items.Add(mi)
              else TMenuItem(pm).Add(mi);
            end;
          Next;
        end;
      Active:=False;
    end;
  finally
    tr.Active:=False;
  end;
end;


procedure TdmCom.DoCommitRetaining(DataSet: TDataSet);
begin
  tr.CommitRetaining;
  if ProducerChanged then
  begin

  end;
end;

procedure TdmCom.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TdmCom.DelConf(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
end;

procedure TdmCom.PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (Pos('update conflicts with concurrent update', E.Message)<>0) or
     (Pos('deadlock', E.Message)<>0) then DataSet.Cancel;

  if (Pos('�������������� ������� ���������', E.Message)<>0) then
   raise Exception.Create('� ������ ���� ������� �� ������� ����������. �������������� ������� ���������.');

end;

procedure TdmCom.taCompAfterPost(DataSet: TDataSet);
begin
  tr.CommitRetaining;
  if ProducerChanged then
  begin
    ReOpenDataSet(dm2.quProd);
    taComp.Refresh;
  end;
end;

procedure TdmCom.taCompAfterScroll(DataSet: TDataSet);
begin
{$ifdef DICT}
//  if fmComp<>NIL then
//    with fmComp.pc1 do
//      if taCompPhis.AsInteger=1 then ActivePage:=Pages[1]
//      else ActivePage:=Pages[0];
{$endif}
end;

procedure TdmCom.taCompNewRecord(DataSet: TDataSet);
begin
  taCompD_CompID.AsInteger:=GetID(1);
  taCompPhis.AsInteger:=0;
  taCompD_COUNTRYID.AsString := '��';
  taCompCODE.AsString := '<�� �����>';
  taCompCOMMISSION.AsInteger:=0;

  with fmComp.pc1 do ActivePage:=Pages[0];
  case CompCat of
      1: taCompSeller.AsInteger:=1;
      2: taCompBuyer.AsInteger:=1;
      3: taCompProducer.AsInteger:=1;
      4: taCompIsBank.AsInteger := 1;
      else taCompSeller.AsInteger:=1;
    end;

  case CompCat of
      2: taCompWORKBUYER.AsInteger:=1;
      3: taCompWORKPROD.AsInteger:=1;
      else taCompWORKORG.AsInteger:=1;
    end;
end;

procedure TdmCom.taMolBeforeInsert(DataSet: TDataSet);
begin
  PostDataSet(taComp);
end;

procedure TdmCom.taMolNewRecord(DataSet: TDataSet);
begin
  taMolD_MolID.AsInteger:=GetID(2);
  taMolD_CompId.AsInteger:=taCompD_CompId.AsInteger;
end;

procedure TdmCom.taArtAfterScroll(DataSet: TDataSet);
begin
{$ifdef DICT}
  if fmArt<>NIL then
    fmArt.LoadPicture(nil);
{$endif}
end;

procedure TdmCom.taArtBeforePost(DataSet: TDataSet);
begin
  if taArtD_COMPId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������������');
  if taArtD_MATId.IsNull  then raise Exception.Create('���������� ������ �������� � ���� ��������');
  if taArtD_GOODId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������������');
  if taArtD_CountryId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������');
  if taArtD_InsId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������� �������');
  if taArtArt.AsString='' then raise Exception.Create('���������� ������ �������� � ���� �������');
end;

procedure TdmCom.taArtNewRecord(DataSet: TDataSet);
begin
  taArtD_ArtID.AsInteger:=GetID(5);
  if D_CompID<>-1 then taArtD_CompId.AsInteger:=D_CompId;
  if D_MatId<>'' then taArtD_MatId.AsString:=D_MatId;
  if D_GoodId<>'' then taArtD_GoodId.AsString:=D_GoodId;
  if D_InsId<>'' then taArtD_InsId.AsString:=D_InsId;
  if D_CountryId<>'' then taArtD_CountryId.AsString:=D_CountryId;
  taArtUnitId.AsInteger:=1;
end;

procedure TdmCom.taArtBeforeInsert(DataSet: TDataSet);
begin
  if (D_CompId=-1) or (D_MatId='*') or (D_GoodId='*') or (D_InsId='*') then SysUtils.Abort;
end;

procedure TdmCom.taEdgNewRecord(DataSet: TDataSet);
begin
  taEdgEdgetionId.AsInteger:=dmCOm.GetId(20);
  taEdgT.AsInteger:=EdgT;
end;

procedure TdmCom.taEdgBeforeOpen(DataSet: TDataSet);
begin
  taEdg.Params[0].AsInteger:=EdgT;
end;

function  TdmCom.GetServerTime: TDateTime;
var b:boolean;
begin
  with quServerTime do
    begin
      b := Transaction.Active;
      if b then Transaction.CommitRetaining
      else Transaction.StartTransaction;
      if Open then Close;

      ExecQuery;
      Result:=Fields[0].AsDateTime;

      Close;
      if b then Transaction.CommitRetaining
      else Transaction.Commit;
    end
end;

procedure TdmCom.taPayTypeNewRecord(DataSet: TDataSet);
begin
  taPayTypePayTypeId.AsInteger:=dmCom.GetId(25);
end;

procedure TdmCom.taDiscountNewRecord(DataSet: TDataSet);
begin
  taDiscountD_DiscountId.AsInteger:=dmCom.GetId(26);
end;

procedure TdmCom.SetArtFilter(Params: TFIBXSQLDA);
begin
  with Params do
    begin
      if (FindParam('COMPID1') <> nil) then
        if (D_COMPID=-1) then
          try
            ByName['COMPID1'].AsInteger:=-MAXINT;
            ByName['COMPID2'].AsInteger:=MAXINT;
          except
          end
        else
          try
            ByName['COMPID1'].AsInteger:=D_COMPID;
            ByName['COMPID2'].AsInteger:=D_COMPID;
          except
          end;
      if (FindParam('MATID1') <> nil) then
        if (D_MATID='*')  then
          try
            ByName['MATID1'].AsString:='!';
            ByName['MATID2'].AsString:='��';
          except
          end
        else
          try
            ByName['MATID1'].AsString:=D_MATID;
            ByName['MATID2'].AsString:=D_MATID;
          except
          end;
      if (FindParam('GOODID1') <> nil) then
        if (D_GOODID='*') then
          try
            ByName['GOODID1'].AsString:='!';
            ByName['GOODID2'].AsString:='��';
          except
          end
        else
          try
            ByName['GOODID1'].AsString:=D_GOODID;
            ByName['GOODID2'].AsString:=D_GOODID;
          except
          end;
      if (FindParam('INSID1') <> nil) then
        if (D_INSID='*')  then
          try
            ByName['INSID1'].AsString:='!';
            ByName['INSID2'].AsString:='��';
          except
          end
        else
          try
            ByName['INSID1'].AsString:=D_INSID;
            ByName['INSID2'].AsString:=D_INSID;
          except
          end;
      if (FindParam('SUPID1') <> nil) then
        if (D_SUPID=-1) then
          try
            ByName['SUPID1'].AsInteger := -MAXINT;
            ByName['SUPID2'].AsInteger := MAXINT;
          except
          end
        else
          try
            ByName['SUPID1'].AsInteger := D_SUPID;
            ByName['SUPID2'].AsInteger := D_SUPID;
          except
          end;
      if (FindParam('COUNTRYID1') <> nil) then
        if D_COUNTRYID='*' then
          try
            ByName['COUNTRYID1'].AsString:='!';
            ByName['COUNTRYID2'].AsString:='��';
          except
          end
        else
          try
            ByName['COUNTRYID1'].AsString:=D_CountryID;
            ByName['COUNTRYID2'].AsString:=D_COUNTRYID;
          except
          end;
      if (FindParam('GOODSID1_1') <> nil) then
        if D_Note1=-1 then
          try
            ByName['GOODSID1_1'].AsInteger := -MAXINT;
            ByName['GOODSID1_2'].AsInteger := MAXINT;
          except
          end
        else
          try
            ByName['GOODSID1_1'].AsInteger := D_Note1;
            ByName['GOODSID1_2'].AsInteger := D_Note1;
          except
          end;
      if (FindParam('GOODSID2_1') <> nil) then
        if D_Note2=-1 then
          try
            ByName['GOODSID2_1'].AsInteger := -MAXINT;
            ByName['GOODSID2_2'].AsInteger := MAXINT;
          except
          end
        else
          try
            ByName['GOODSID2_1'].AsInteger := D_Note2;
            ByName['GOODSID2_2'].AsInteger := D_Note2;
          except
          end;
      if (FindParam('ATT1_1') <> nil) then
        if D_Att1Id=ATT1_DICT_ROOT then
          try
            ByName['ATT1_1'].AsInteger := -MAXINT;
            ByName['ATT1_2'].AsInteger := MAXINT;
          except
          end
        else
          try
            ByName['ATT1_1'].AsInteger := D_Att1Id;
            ByName['ATT1_2'].AsInteger := D_Att1Id;
          except
          end;
      if (FindParam('ATT2_1') <> nil) then
        if D_Att2Id=ATT2_DICT_ROOT then
         try
           ByName['ATT2_1'].AsInteger := -MAXINT;
           ByName['ATT2_2'].AsInteger := MAXINT;
         except
         end
        else
         try
           ByName['ATT2_1'].AsInteger := D_Att2Id;
           ByName['ATT2_2'].AsInteger := D_Att2Id;
         except
         end;
      if (FindParam('SZ1') <> nil) then
        if (UIDWHSZ_='*')  then
          try
            ByName['SZ1'].AsString:='';
            ByName['SZ2'].AsString:='��';
          except
          end
        else
          try
            ByName['SZ1'].AsString:=UIDWHSZ_;
            ByName['SZ2'].AsString:=UIDWHSZ_;
          except
          end;
    end;
end;

procedure TdmCom.taCompCalcFields(DataSet: TDataSet);
begin
  taCompRecNo.AsInteger:=taComp.RecNo;
end;

procedure TdmCom.frReportGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if(ParName = '���.����')then
    if(taCompPHIS.Value=1)then
      ParValue := '����� � ����� ��������: '+taCompPASPSER.AsString+' '+ taCompPASPNUM.AsString+#13+
                  '��� � ����� �����: '+taCompDISTRPLACE.AsString+' '+taCompDISTRDATE.AsString+#13+
                  '� � ���� �������������: '+taCompCERTN.AsString+' '+taCompCERTDATE.AsString
    else ParValue := '';
end;

procedure PrintDict(ds : TDataSource; rptName : integer);
var
  bIsOpened : boolean;
begin
  bIsOpened := False;
  if not ds.DataSet.InheritsFrom(TFIbDataSet) then eXit;
  try
    dmCom.frdsDict.DataSource := ds;
    with TFIbDataSet(ds.DataSet).Transaction do
      if not Active then StartTransaction;
    bIsOpened := ds.DataSet.Active;
    if not bIsOpened then ds.DataSet.Open;
    dmReport.taDoc.Open;
    dmReport.FDocId := rptName;
    dmCom.frReport.LoadFromDB(dmReport.taDoc, rptName);
    dmReport.taDoc.Close;
    ds.DataSet.DisableControls;
    with dmCom.frReport do
    case DocPreview of
      pvPreview : ShowReport;
      pvDesign  : DesignReport;
      pvPrint   : begin
        PrepareReport;
        PrintPreparedReport('', DefaultCopies, DefaultCollate, frAll);
      end;
    end;
    ds.DataSet.EnableControls;
  finally
    if not bIsOpened then CloseDataSets([ds.DataSet]);
  end;
end;
                         
procedure TdmCom.UnitGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:='���'
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure TdmCom.UnitSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.Clear
  else if Text='��' then Sender.AsInteger:=0
       else if Text='��' then Sender.AsInteger:=1;
end;

procedure TdmCom.taDepBeforeOpen(DataSet: TDataSet);
begin
  taDep.Params[0].AsInteger:=dmCom.D_DepId;
end;

procedure TdmCom.taEmpBeforeInsert(DataSet: TDataSet);
begin
  PostDataSet(taEmp);
  if not CenterDep then raise Exception.Create('���������� �� �������� ���������!');
end;

procedure TdmCom.taEmpNewRecord(DataSet: TDataSet);
begin
  taEmpD_EmpID.AsInteger:=dmCom.GetID(4);
  taEmpALLSELL.AsInteger:=0;
end;

procedure TdmCom.taClientNewRecord(DataSet: TDataSet);
begin
  taClientClientId.AsInteger:=dmCom.GetId(23);
  taClientDEPID.AsInteger:=SelfDepId;
  taClientFMAIN.AsInteger:=1;
  taClientFDEL.AsInteger:=0;
  taClientF_LOSE.AsInteger:=0;  
end;

function TdmCom.GetDepInfo(Index: integer): TDepInfo;
var
  i : integer;
begin
  for i:=0 to Length(FDepInfo)-1 do
    if Index = FDepInfo[i].DepId then
    begin
      Result := FDepInfo[i];
      exit;
    end;
  raise Exception.Create('����� �� ������� ������� (FDepInfo)');
end;

procedure TdmCom.InitDepInfo;
var
  b : boolean;
  i : integer;
begin
  b := tr.Active;
  if not b then tr.StartTransaction;
  OpenDataSets([taDepById]);
  taDepById.Last;
  Finalize(FDepInfo);
  SetLength(FDepInfo, taDepById.RecordCount);
  taDepById.First;
  i := 0;
  while not taDepById.Eof do with FDepInfo[i] do
  begin
    DepId := taDepById.FieldByName('D_DEPID').AsInteger;
    SortInd := taDepById.FieldByName('SORTIND').AsInteger;
    Name := taDepById.FieldByName('NAME').AsString;
    SName := DelESpace(taDepById.FieldByName('SNAME').AsString);
    SSName := DelESpace(taDepById.FieldByName('SSNAME').AsString);
    Color := taDepById.FieldByName('COLOR').AsInteger;
    Shop := taDepById.FieldByName('SHOP').AsInteger;
    WH := taDepById.FieldByName('WH').AsInteger;
    Urf := taDepById.FieldByName('URF').AsInteger;
    City := taDepById.FieldByName('CITY').AsString;
    Address := taDepById.FieldByName('ADDRESS').AsString;
    PostInd := taDepById.FieldByName('POSTIND').ASString;
    Isprog := taDepById.FieldByName('ISPROG').AsInteger;
    Margin := taDepById.FieldByName('MARGIN').AsFloat;
    NotSetPr := taDepById.FieldByName('NOTSETPR').AsInteger=1;
    R_UseDep := taDepById.FieldByName('R_USEDEP').AsInteger;
    inc(i);
    taDepById.Next;
  end;
  CloseDataSets([taDepById]);
  if b then dmCom.tr.CommitRetaining
  else dmCom.tr.CommitRetaining;
end;

function TdmCom.GetDepCount: integer;
begin
  if Assigned(FDepInfo) then Result := Length(FDepInfo)
  else Result:=0;
end;

function TdmCom.GetDepInfo1(Index: integer): TDepInfo;
begin
  Result := FDepInfo[Index];
end;

function TdmCom.GetUser: TUserInfo;
var
  UserInfo : TUserInfo;
begin
  with quUser, UserInfo  do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    Open;
    UserId := FieldByName('D_EMPID').AsInteger;
    FIO := FieldByName('FIO').AsString;
    FName := FieldByName('FNAME').AsString;
    LName := FieldByName('LNAME').AsString;
    SName := FieldByName('SNAME').AsString;
    Alias := FieldByName('ALIAS').AsString;
    Pswd := FieldByName('PSWD').AsString;
    if not FieldByName('D_DEPID').IsNull then DepId:=FieldByName('D_DEPID').AsInteger
    else DepId:=-1;

    if FieldByName('PERMISSIONS_USER').IsNull then Manager:=true
    else begin
     Adm:=FieldByName('PERMISSIONS_USER').AsInteger=0;
     Manager:=FieldByName('PERMISSIONS_USER').AsInteger=1;
     Seller :=FieldByName('PERMISSIONS_USER').AsInteger=2;
    end;

    Acc := FieldByName('ACC').AsInteger;
    AllWh := FieldByName('ALLWH').AsInteger>0;
    UIDWHDate := FieldByName('UIDWHDATE').AsDateTime;
    UIDWHBD := FieldByName('UIDWHBD').AsDateTime;
    UIDWHED := FieldByName('UIDWHED').AsDateTime;
    RestDate := FieldByName('RESTDATE').AsDateTime;
    WHDate := FieldByName('WHDATE').AsDateTime;

    if FieldByName('ALLSELL').isNull then AllSell:=0
    else AllSell:=FieldByName('AllSell').AsInteger;

    if FieldByName('L3BD').isNull then L3BD := StrToDate('05.02.1981')
    else L3BD := FieldByName('L3BD').AsDateTime;
    if FieldByName('L3ED').IsNull then L3ED := StrToDate('05.02.1981')
    else L3ED := FieldByName('L3ED').AsDateTime;
    L3Sup:=FieldByName('L3Sup').asInteger;
    try
     ApplId:=FieldByName('Applz_id').asInteger;
    except
    end;
    Close;
    Transaction.Commit;
  end;

  Result := UserInfo;
end;

procedure TdmCom.quUserBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
    ByName['D_EMPID'].AsInteger := UserId;
end;




constructor TdmCom.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TdmCom.Destroy;
begin
  inherited;
end;

function TdmCom.GetDefaultDep: TDepInfo;
var
  i : integer;
begin
  for i:=0 to Length(FDepInfo)-1 do
    if FDepInfo[i].IsProg = 1 then
    begin
      Result := FDepInfo[i];
      eXit;
    end;
  raise Exception.Create('������������� �� ��������� �� �����������!');
end;

procedure TdmCom.taGoodsSamBeforeDelete(DataSet: TDataSet);
begin
 if(taGoodsSamD_GOODSID_SAM.AsInteger = 12) then   raise Exception.Create('������� ������ ������')
  else
  if(taGoodsSamD_GOODSID_SAM.AsInteger <> -1) then
  begin
   DelConf(DataSet);
   (*���� ������� ������� ������, �� �������� �� ��������*)
   with dm, quTmp do
   begin
    close;
    sql.Text := 'select uid from uid_info where (D_GOODS_SAM1='+
                dmCom.taGoodsSamD_GOODSID_SAM.AsString+')or (D_GOODS_SAM2='+
                dmCom.taGoodsSamD_GOODSID_SAM.AsString+')';
    ExecQuery;            
    if fields[0].AsInteger<>0 then raise Exception.Create('������� ������ ������');
    close;
   end
  end;
end;

procedure TdmCom.taGoodsSamBeforeEdit(DataSet: TDataSet);
begin
 if(taGoodsSamD_GOODSID_SAM.AsInteger = 12) then   raise Exception.Create('������������� ������ ������')
end;

procedure TdmCom.taAddressBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TdmCom.taAddressBeforeDelete(DataSet: TDataSet);
begin
 if taAddressD_ADDRESS_ID.AsInteger=1 then SysUtils.Abort
 else
  if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort
  else
   with dm, qutmp do
   begin
    close;
    sql.Text:='select addressid from client where fdel=0 and addressid='+taAddressD_ADDRESS_ID.AsString;
    ExecQuery;
    if Fields[0].AsInteger<>0 then SysUtils.Abort; 
    close;
   end
end;

procedure TdmCom.taClientBeforeOpen(DataSet: TDataSet);
var depid:integer;
begin
 if FilterFIO<>'' then taClient.SelectSQL[9]:=' and name like '''+FilterFIO+'%'''
 else taClient.SelectSQL[9]:='';

 if fdata then taClient.SelectSQL[10]:=' and  R_INSERTD between '#39+datetimetostr(bdata)+#39+
                                      ' and '#39+datetimetostr(edata)+#39
 else if ClientUpDate then taClient.SelectSQL[10]:=' and  UPDATEDATE between '#39+datetimetostr(bdata)+#39+
                                      ' and '#39+datetimetostr(edata)+#39
 else taClient.SelectSQL[10]:='';

 if  IsLoseCard then taClient.SelectSQL[11]:=' and F_LOSE=1'
 else taClient.SelectSQL[11]:=' ';

 taClient.Prepare;

 taClient.ParamByName('FMAIN1').AsInteger:=FilterFmain1;
 taClient.ParamByName('FMAIN2').AsInteger:=FilterFmain2;

 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select max(d_depid) from d_dep where d_depid<>-1000';
 dm.quTmp.ExecQuery;
 depid:=dm.quTmp.Fields[0].AsInteger;
 dm.quTmp.Close;

 if FilterDepID=0 then
 begin
  taClient.ParamByName('DEPID1').AsInteger:=0;
  taClient.ParamByName('DEPID2').AsInteger:=depid+1;
 end
 else
 begin
  taClient.ParamByName('DEPID1').AsInteger:=FilterDepID;
  taClient.ParamByName('DEPID2').AsInteger:=FilterDepID;
 end

end;

procedure TdmCom.taClientPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (Pos('update conflicts with concurrent update', E.Message)<>0) or
     (Pos('deadlock', E.Message)<>0) then DataSet.Cancel;
  if (Pos('"CLIENT_IDX4"'+#13#10+'null segment of UNIQUE KEY', E.Message)<>0) then
   raise Exception.Create('���� ����� ����. ����� �� ����� ���� ������');

  if (Pos('in unique index "CLIENT_IDX4"', E.Message)<>0) then
   raise Exception.Create('����� ����. ����� ��� ����������');
end;

procedure TdmCom.taClientCalcFields(DataSet: TDataSet);
begin
 taClientF1.AsBoolean:=boolean(taClientFMAIN.AsInteger);
end;

procedure TdmCom.quNSellitemBeforeOpen(DataSet: TDataSet);
begin
 if not CenterDep then
  quNSellitem.SelectSQL[6]:='si.clientid = '+taClientCLIENTID.AsString
 else
  quNSellitem.SelectSQL[6]:='cl.nodcard = '#39+taClientNODCARD.AsString+#39;
end;

procedure TdmCom.taCompBeforePost(DataSet: TDataSet);
var
  i, k:integer;
  OldSName: string;
  OldProducer: Integer;
  NewProducer: Integer;
  NewSName: string;
begin
  ProducerChanged := False;

  if VarIsNull(taCompSNAME.OldValue) then OldSName := ''
  else OldSName := Trim(taCompSNAME.OldValue);

  if VarIsNull(taCompSNAME.NewValue) then NewSName := ''
  else NewSName := Trim(taCompSNAME.NewValue);

  if VarIsNull(taCompProducer.OldValue) then OldProducer := 0
  else OldProducer := taCompProducer.OldValue;

  if VarIsNull(taCompProducer.NewValue) then NewProducer := 0
  else NewProducer := taCompProducer.NewValue;

  ProducerChanged := ((NewProducer <> OldProducer) or ((NewProducer = 1) and (OldSName <> NewSName)));

   k := 0;
   for i := 0 to taComp.Fields.Count - 1 do
   begin
     if ((not varisnull(taCompD_COMPID.OldValue)) and
         (taComp.Fields[i].Name <> 'taCompRecNo') and
         (taComp.Fields[i].Name <> 'taCompFIOUSER') and
         (taComp.Fields[i].Name <> 'taCompProd')) then
     begin
        if (taComp.FieldList.Fields[i].NewValue <> taComp.FieldList.Fields[i].OldValue) then
        //    (not VarIsNull(taComp.FieldList.Fields[i].OldValue)))
           if MessageDialog('��������� ���������?', mtInformation, [mbYes, mbNo], 0) = mrNo then
           begin
              for k := 0 to taComp.Fields.Count - 1 do
                  taComp.FieldList.Fields[k].NewValue := taComp.FieldList.Fields[k].OldValue
           end
             else
           begin
              // ��������� ������������ ���������� ����� ��������
              if (taCompD_EMPID.OldValue=taCompD_EMPID.NewValue) then
              begin
                 if(not taCompDOGBD.IsNull)then
                 begin
                    if(not taCompDOGED.IsNull)and(taCompDOGUNLIMITED.AsInteger=1)then
                       raise Exception.Create('������������ �� ����� ���� ������ ���� ��������� �������� � ������� �� ������������� �����');
                    if(taCompDOGED.IsNull)and(taCompDOGUNLIMITED.AsInteger=0)then
                       raise Exception.Create('���������� ������� ���� ��������� �������� ��� ������� �� ������������� �����');
                 end;
                 if(not taCompDOGED.IsNull)then
                 begin
                    if(taCompDOGBD.IsNull) then
                       raise Exception.Create('���������� ������� ���� ������ ��������');
                    if(taCompDOGUNLIMITED.AsInteger = 1) then
                       raise Exception.Create('������������ �� ����� ���� ������ ���� ��������� �������� � ������� �� ������������� �����');
                 end;
                 if(taCompDOGUNLIMITED.AsInteger = 1)then
                 begin
                    if(not taCompDOGED.IsNull)then
                       raise Exception.Create('������������ �� ����� ���� ������ ���� ��������� �������� � ������� �� ������������� �����');
                    if(taCompDOGBD.IsNull)then
                       raise Exception.Create('���������� ������� ���� ������ ��������');
                 end;
              end;
              k:=1;
           end;
     end;
     if (k=1) then exit;
  end;
end;



function DefaultIniName : string;
begin
  Result := GetIniFileName;
end;

procedure TdmCom.taRetNewRecord(DataSet: TDataSet);
begin
 taRetD_RETID.AsString := IntToStr(dmCom.GetId(35));
end;

procedure TdmCom.taRetBeforeDelete(DataSet: TDataSet);
begin
 if not CenterDep then SysUtils.Abort
 else DelConf(DataSet);
end;

procedure TdmCom.taGoodsSamNewRecord(DataSet: TDataSet);
begin
 tagoodsSamD_GoodsID_Sam.asinteger:=GetID(38);
end;

procedure TdmCom.taAddressNewRecord(DataSet: TDataSet);
begin
  taAddressd_address_id.asinteger:=GetID(39);
  taAddressFDEL.AsInteger:=0;
end;

procedure TdmCom.taAtt1NewRecord(DataSet: TDataSet);
begin
  taAtt1ID.AsInteger := GetId(40);
  taAtt1SNAME.AsString := '';
end;

procedure TdmCom.taAtt2NewRecord(DataSet: TDataSet);
begin
  taAtt2ID.AsInteger := GetId(41);
  taAtt2SNAME.AsString := '';
end;

procedure TdmCom.taArtBeforeOpen(DataSet: TDataSet);
begin
  beep;
end;

procedure TdmCom.taSnameSupBeforeOpen(DataSet: TDataSet);
begin
 taSnameSup.ParamByName('Fsup1').AsInteger:=fsup1;
 taSnameSup.ParamByName('Fsup2').AsInteger:=fsup2;
 taSnameSup.ParamByName('Ftable').AsInteger:=ftable;
end;

procedure TdmCom.taSnameSupNewRecord(DataSet: TDataSet);
begin
 if newfsup=-1 then raise Exception.Create('�������� ����������');

 taSnameSupD_SNAMESUPID.AsInteger := GetID(42);
 
 taSnameSupFSUP.AsInteger:=newfsup;
 taSnameSupFTABLE.AsInteger:=ftable;
end;

procedure TdmCom.taSnameSupCalcFields(DataSet: TDataSet);
begin
 case taSnameSupFSUP.AsInteger of
  0: taSnameSupSupName.AsString:='������';
  1: taSnameSupSupName.AsString:='���';
  2: taSnameSupSupName.AsString:='�����';
  3: taSnameSupSupName.AsString:='�����������';
  4: taSnameSupSupName.AsString:='��������';  
 end;
end;

procedure TdmCom.taEmpBeforeDelete(DataSet: TDataSet);
begin
 if not CenterDep then raise Exception.Create('�������� �� �������� ���������!');
  if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
end;

procedure TdmCom.taMolBeforeOpen(DataSet: TDataSet);
begin
  dmCom.taMol.ParamByName('D_COMPID').AsInteger := SelfCompId;//18;//dmCom.taRecD_COMPID.AsInteger;
end;

procedure TdmCom.quUseClientBeforeOpen(DataSet: TDataSet);
begin
 quUseClient.ParamByName('addressid').AsInteger:=taAddressD_ADDRESS_ID.AsInteger;
end;

procedure TdmCom.quDiscountSumNewRecord(DataSet: TDataSet);
begin
  quDiscountSumD_DISCOUNTSUMID.AsInteger:=GetID(49);
  quDiscountSumCOLOR.AsInteger:=clInfoBk;
  quDiscountSumLOWERLIMIT.AsFloat := 0;
  quDiscountSumHIGHLIMIT.AsFloat := 0;
  quDiscountSumT.AsInteger := 0;
  quDiscountSumVALID_BD.AsDateTime := ToDay;
  quDiscountSumVALID_ED.AsVariant := Null;
  quDiscountSumINFINITE_ED.AsInteger := 1;
  quDiscountSumCLOSED.AsInteger := 0;
end;

procedure TdmCom.quNSellitemCalcFields(DataSet: TDataSet);
begin
  if quNSellitemRET.AsInteger = 1 then
  begin
    quNSellitemCOSTSELL.AsFloat := 0;
    quNSellitemCOSTRET.AsFloat := quNSellitemCOST.AsFloat;
  end
  else begin
    quNSellitemCOSTSELL.AsFloat := quNSellitemCOST.AsFloat;
    quNSellitemCOSTRET.AsFloat := 0;
  end;
end;

procedure TdmCom.SetGlobalInventoryEnabled(const Value: byte);
begin
  if FGlobalInventoryEnabled = Value then eXit;
  ExecSQL('update D_Rec set INVENTORY_GLOBAL='+IntToStr(Value), quTmp);
  if (Value = 0) then ExecSQL('execute procedure CloseGlobalInventory', quTmp);
  FGlobalInventoryEnabled := Value;
end;

procedure TdmCom.taMatBeforePost(DataSet: TDataSet);
var
  mat : Integer;
begin
  if taMatD_MATID.NewValue <> taMatD_MATID.OldValue then
     mat := ExecSelectSQL('select count(*) from d_mat where d_matid= ''' +taMatD_MATID.AsString+'''', dmCom.quTmp);
  if mat = 1 then
     raise Exception.Create('�� ���������� ��� �������:  '''+ taMatD_MATID.AsString+''''+'. ��� ������� ������ ���� ����������!');
end;

initialization
  OnGetDefaultIniName := DefaultIniName;
  // �������� ������������ ���������� �� ������ Interbase
  fib.SetIBErrorMessages([ShowSQLCode, ShowIBMessage, ShowSQLMessage]);

end.


