unit SellList_H;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmSellList_H = class(TForm)
    Label1: TLabel;
    dg1: TDBGridEh;
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    OpSBtn: TSpeedButton;
    ClSBtn: TSpeedButton;
    CnlSBtn: TSpeedButton;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    DataSource1: TDataSource;
    LogEd: TEdit;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    pa1: TPanel;
    Panel1: TPanel;
    Label3: TLabel;
    dbtDep: TDBText;
    Label2: TLabel;
    dbtN: TDBText;
    Label4: TLabel;
    dbtBD: TDBText;
    Label5: TLabel;
    dbtED: TDBText;
    Panel3: TPanel;
    Label8: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    dbtCNT: TDBText;
    dbtCost: TDBText;
    dbtW: TDBText;
    Panel5: TPanel;
    Panel2: TPanel;
    Panel7: TPanel;
    Panel6: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    dbtRCnt: TDBText;
    dbtRCost: TDBText;
    dbtRW: TDBText;
    plCostCard: TPanel;
    LCostCard: TLabel;
    dbCostCard: TDBText;
    Panel8: TPanel;
    dbtCass: TDBText;
    Label13: TLabel;
    Label12: TLabel;
    procedure OpSBtnClick(Sender: TObject);
    procedure ClSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ClAllSBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmSellList_H: TfmSellList_H;
  var log,s:string;
  procedure Hist(var user, st: string);
implementation

   uses comdata, data, SellList, MsgDialog, DInvItem, M207IBLogin, SellItem,
  SInv, SInv_H;

   {$R *.dfm}
   procedure Hist(var user, st: string); //���������� ������ � ������� INVHIST
begin
     fmSellList_H.taHist.Append;
     fmSellList_H.taHistDOCID.Value:=dm.taSellListSELLID.AsInteger;
     fmSellList_H.taHistFIO.Value := user;
     fmSellList_H.taHistSTATUS.Value := st;
     fmSellList_H.taHist.Post;
end;


procedure TfmSellList_H.OpSBtnClick(Sender: TObject); //�������� ���������
begin
 taEmp.Active:=true;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin
   log:=LogEd.Text;
   s:='�������';
   Hist(log, s);
  //  If (SellList.f=true) or (SellItem.f=true) then fmSellList.acOpen.Execute else fmSellItem.acOpenSell.Execute;
   close;
   end;
     taEmp.Active:=false;
end;

procedure TfmSellList_H.ClSBtnClick(Sender: TObject);  //�������� ���������
begin
 if MessageDialog('������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
 if LogEd.Text='' then log:=dmCom.User.Alias;
     s:='�������';
     Hist(log, s);
     //If SellList.f=true then fmSellList.acClose.Execute else
     fmSellItem.acCloseSell.Execute;
     close;
    end;
end;

procedure TfmSellList_H.CnlSBtnClick(Sender: TObject);  //������
begin
 taHist.Active:=false;
 close;
end;

procedure TfmSellList_H.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  taHist.ParamByName('INVID').AsInteger := dm.taSellListSELLID.AsInteger;
end;

procedure TfmSellList_H.FormCreate(Sender: TObject);
begin
   taHist.Active := True;
   dg1.DataSource.DataSet.Last;    //��������� ���������
end;

procedure TfmSellList_H.FormActivate(Sender: TObject);
begin
  Pswed.SetFocus;
  Caption:='�'+(dm.taSellListRN.AsString);
  if (dm.taSellListCLOSED.AsInteger=1) or (dm.taSellListCLOSED.IsNull) then
   begin
   fmSellList_H.ClSBtn.Enabled:=false;//���������� ���� �����������
    Label12.Visible:=true;
    LogEd.Visible:=true;
    LogEd.Text:= dmCom.User.Alias;
    PswEd.Visible:=true;
    LaLogin.Visible:=true;
    LaPsw.Visible:=true;
   end
  else
   begin
   fmSellList_H.OpSBtn.Enabled:=false;
   //�������� ���� �����������
   Label12.Visible:=false;
   LogEd.Visible:=false;
   PswEd.Visible:=false;
   LaLogin.Visible:=false;
   LaPsw.Visible:=false;
   end;
end;

procedure TfmSellList_H.ClAllSBtnClick(Sender: TObject);
begin
  taHist.Active := false;
  close;
end;

procedure TfmSellList_H.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 taHist.Active := false;
end;

procedure TfmSellList_H.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSellList_H.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSellList_H.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (key=vk_return) and (ClSBtn.Enabled=true) then ClSBtn.Click;
if key=vk_escape then cnlSbtn.Click;
end;

end.
