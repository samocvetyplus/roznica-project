{����� ������}
unit ArtDict;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, DBCtrls, StdCtrls, Buttons, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, db, Menus, comdata, M207Ctrls,
  DBGridEh, PrnDbgeh, ActnList, Variants, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmArtDict = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    paDict: TPanel;
    Splitter1: TSplitter;
    Splitter7: TSplitter;
    Panel3: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Panel6: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Label18: TLabel;
    Label19: TLabel;
    DBText4: TDBText;
    DBText6: TDBText;
    pmA: TPopupMenu;
    miAddArt: TMenuItem;
    miInsFromSearch: TMenuItem;
    miAEdit: TMenuItem;
    N22: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N23: TMenuItem;
    N28: TMenuItem;
    N27: TMenuItem;
    N26: TMenuItem;
    N25: TMenuItem;
    N24: TMenuItem;
    DArtId1: TMenuItem;
    pmA2: TPopupMenu;
    miIns: TMenuItem;
    N4: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    DArt2ID1: TMenuItem;
    lbCountry: TListBox;
    Splitter2: TSplitter;
    NCountry: TMenuItem;
    Splitter6: TSplitter;
    lbAtt1: TListBox;
    Splitter8: TSplitter;
    lbAtt2: TListBox;
    N11: TMenuItem;
    N21: TMenuItem;
    siHelp: TSpeedItem;
    fr1: TM207FormStorage;
    dgA2: TDBGridEh;
    prdgA2: TPrintDBGridEh;
    acList: TActionList;
    acPrintA2: TAction;
    dgArt: TDBGridEh;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    siMergeComplex: TSpeedItem;
    edArt: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure edArtChange(Sender: TObject);
    procedure edArtEnter(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure miAddArtClick(Sender: TObject);
    procedure miInsFromSearchClick(Sender: TObject);
    procedure miAEditClick(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure DArtId1Click(Sender: TObject);
    procedure miInsClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N19Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure DArt2ID1Click(Sender: TObject);
    procedure NCountryClick(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acPrintA2Execute(Sender: TObject);
    procedure dgArtEditButtonClick(Sender: TObject);
    procedure dgArtEnter(Sender: TObject);
    procedure dgArtExit(Sender: TObject);
    procedure dgArtGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgArtMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SpeedItem1Click(Sender: TObject);
    procedure siMergeComplexClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FieldsRes:t_array_var;    
    SearchEnable: boolean;
    LogOprIdForm: string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmArtDict: TfmArtDict;

implementation

uses Data, Data2, DBTree, AEdit, Comp, Mat, Good, DIns, ARest, Ins,
  SPEdit, M207Proc, Country, FIBQuery, dAtt1, dAtt2, data3, dbUtil, MsgDialog;

{$R *.DFM}

procedure TfmArtDict.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmArtDict.FormCreate(Sender: TObject);
var Res: Variant;
begin
  tb1.WallPaper:=wp;
  with dm, dmCom do
    begin
      dgArt.ReadOnly:=not Centerdep;
      dgA2.ReadOnly:=not Centerdep;
      dgA2.FieldColumns['PRICE1'].Visible := CenterDep;
      AllA2:=False;
      Old_D_MatId:='.';
      OpenDataSets([quSup, quDep, taNDS, taSEl, taPayType, taIns, taA2, quMaxArtPrice]);
      D_CompId:=-1;
      D_MatId:='';
      D_GoodId:='';
    end;  

  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;

  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry,lbAtt1,lbAtt2);

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbAtt1.ItemIndex:=0;
  lbAtt2.ItemIndex:=0;
  lbCompClick(NIL);
  edArtEnter(NIL);
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);  
  DArtId1.Visible:= dmcom.Adm;
  DArt2ID1.Visible:= dmcom.Adm;

  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                       #39' and Is_Merge_Run = current_connection',dm.quTmp);

  if not VarIsNull(Res) then siMergeComplex.ImageIndex := 10
  else siMergeComplex.ImageIndex := 53;

end;

procedure TfmArtDict.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmArtDict.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmArtDict.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id:=TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id:=TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
  ReOpenDataSets([dm.quArt]);
end;

procedure TfmArtDict.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmArtDict.edArtChange(Sender: TObject);
begin
  if SearchEnable then
    with dm do
     quArt.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmArtDict.edArtEnter(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId)or
       (Old_D_CountryID<>D_CountryID)or
       (Old_D_Att1Id<>D_Att1Id)or
       (Old_D_Att2Id<>D_Att2Id) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryID:=D_CountryID;
         Old_D_Att1Id:=D_Att1Id;
         Old_D_Att2Id:=D_Att2Id;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quArt]);
         Screen.Cursor:=crDefault;
       end;

end;

procedure TfmArtDict.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     VK_RETURN: begin
                 edArt.SelectAll;
                 if (dm.quArt.FieldByName('ART').asString<>edArt.Text) then miInsFromSearchClick(Sender);
                end;
     VK_DOWN: ActiveControl:=dgArt;
     VK_INSERT: if Shift=[ssCtrl] then  miInsfromSearchClick(nil)
    end;
end;

procedure TfmArtDict.miAddArtClick(Sender: TObject);
begin
  with dm, quArt do
    begin
      Insert;
      quArtArt.AsString:=NewArt;
      ShowAndFreeForm(TfmAEdit, Self, TForm(fmAEdit), True, False);
    end;
end;

procedure TfmArtDict.miInsFromSearchClick(Sender: TObject);
begin
  if edArt.Text='' then
  begin
    MessageDialog('������� �������', mtWarning, [mbOk], 0);
    ActiveControl:=edArt;
  end
  else
  begin
    dm.quArt.Append;
    dm.quArtArt.AsString:=edArt.Text;
  end
end;

procedure TfmArtDict.miAEditClick(Sender: TObject);
var LogOperationID: string;
begin
  LogOperationID:=dm3.insert_operation('�������������� ��������',LogOprIdForm);

  if ShowAndFreeForm(TfmAEdit, Self, TForm(fmAEdit), True, False)=mrOK then
    with dm.taSEl do
      begin
        Active:=False;
        Open;
      end;

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N16Click(Sender: TObject);
begin
  dm.quArt.Delete;
end;

procedure TfmArtDict.N18Click(Sender: TObject);
var ArtId1, ArtId2: integer;
    Dup: integer;
    LogOperationID: string;
    Res : Variant;
begin
  ArtID1 := 0;

  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                        #39' and Is_Merge_Run = current_connection',dm.quTmp);
  if VarIsNull(Res) then
   ShowMessage('���������� ������ �������� ����������� ��������� (������ � �������������)')
  else if MessageDialog('���������� ��������?', mtConfirmation, [mbOK, mbCancel], 0)=mrOK then
    with dmCom, dm , dm2 do
      begin
        LogOperationID:=dm3.insert_operation('����������� ������ ���������',LogOprIdForm);

        Dup:=0;
        PostDataSets([quArt]);
        try
          quArt.DisableControls;
          if NOT quArt.Locate('F1', 1, [])  then raise Exception.Create('���������� ������� ��� ��������');

          ArtId1:=quArtD_ArtId.AsInteger;

          if NOT quArt.Locate('F2', 1, [])  then raise Exception.Create('���������� ������� ��� ��������');
          Screen.Cursor:=crSQLWait;
          while not quArt.EOF do
            begin
              if quArtF2.AsInteger=1 then
                begin
                  ArtId2:=quArtD_ArtId.AsInteger;
                  try
                   ExecuteQutmp(quTmp, 'SELECT DUP FROM AMERGE('+IntToStr(ArtId1)+', '+
                                                                 IntToStr(ArtId2)+', '+
                                                                 IntToStr(UserId)+', '+
                                                                 #39+' '+#39','#39+' '+#39')',
                                1, FieldsRes);
                   Dup:=FieldsRes[0];
                  finally
                   Finalize(FieldsRes);
                  end;
                end;
              quArt.Next;
            end;
          quArt.Active:=False;
          tr.CommitRetaining;
          if Dup<>0 then MessageDialog('� �������� ����������� ��������� ������ �������� ���� ��������', mtWarning, [mbOK], 0);
        finally
          quArt.Active:=True;
          quArt.EnableControls;
          quArt.Locate('D_ARTID', ArtId1, []);
          Screen.Cursor:=crDefault;
        end;

        dm3.update_operation(LogOperationID);
      end

end;

procedure TfmArtDict.N28Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� �����������',LogOprIdForm);

  b:=GetBit(dmCom.EditRefBook, 4) and CenterDep;
  ShowAndFreeFormEnabled(TfmComp, Self, TForm(fmComp), True, False, b,
        'siHelp;siExit;tb1;spitPrint;siSort;edCode;siCategory;tb2;PageControl1;'+
        'pc1;TabSheet1;TabSheet2;TabSheet3;TabSheet4;TabSheet5;TabSheet6;TabSheet7;'+
        'Panel1;tsMol;mnitAll;st1;mnitCur;N6;N1;N2;N3;N4;N5;edCode;chAllWorkOrg;'+
        'laCompCat;Label36;Splitter1;',
        'dg1;M207IBGrid1;');

  dm.FillListBoxes(lbComp, nil, nil, nil, nil,nil,nil);

   dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N27Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� ����������',LogOprIdForm);

//  ShowAndFreeForm(TfmMat, Self, TForm(fmMat), True, False);
  b:=GetBit(dmCom.EditRefBook, 0) and CenterDep;
  ShowAndFreeFormEnabled(TfmMat, Self, TForm(fmMat), True, False, b, 'siExit;tb1;spitPrint;siSort;', 'dg1;');
  dm.FillListBoxes(nil, lbMat, nil, nil, nil,nil,nil);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N26Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);

//  ShowAndFreeForm(TfmGood, Self, TForm(fmGood), True, False);
  b:=GetBit(dmCom.EditRefBook, 1) and CenterDep;
  ShowAndFreeFormEnabled(TfmGood, Self, TForm(fmGood), True, False, b, 'siExit;tb1;spitPrint;siSort;','dg1;');

  dm.FillListBoxes(nil, nil, lbGood, nil,nil,nil,nil);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N25Click(Sender: TObject);
var  LogOperationID:string;
     b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);

//  ShowAndFreeForm(TfmDIns, Self, TForm(fmDIns), True, False);
  b:=GetBit(dmCom.EditRefBook, 3) and CenterDep;
  ShowAndFreeFormEnabled(TfmDIns, Self, TForm(fmDIns), True, False, b, 'siExit;tb1;spitPrint;siSort;','dg1;');

  dm.FillListBoxes(nil, nil, nil, lbIns, nil,nil,nil);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.DArtId1Click(Sender: TObject);
begin
  MessageDialog(dm.quArtD_ArtId.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmArtDict.miInsClick(Sender: TObject);
var LogOperationID:string;
begin
  with dmCom, dm do
    begin
      PostDataSets([quArt, taA2, taSEl]);
      if taA2Art2Id.IsNull then raise Exception.Create('���������� ������ �������');
      Art2Id:=taA2Art2Id.AsInteger;
      FullArt:=taArtFullArt.AsString;
      Art2:=taA2Art2.AsString;

      LogOperationID:=dm3.insert_operation('�������',LogOprIdForm);

      ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);

      dm3.update_operation(LogOperationID);
    end;
end;

procedure TfmArtDict.N4Click(Sender: TObject);
var i: integer;
begin
  dm.taA2.Insert;
  with dgA2 do
    begin
      i:=0;
      while Columns[i].Field.FieldName<>'ART2' do Inc(i);
      SelectedIndex:=i;
    end
end;

procedure TfmArtDict.N14Click(Sender: TObject);
begin
  dm.taA2.Delete;
end;

procedure TfmArtDict.N19Click(Sender: TObject);
var Art2Id1: integer;
    Art2Id2: integer;
    LogOperationID :string;
    Res : Variant;
begin
  Art2Id1 := 0;

  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                        #39' and Is_Merge_Run = current_connection',dm.quTmp);
  if VarIsNull(Res) then
   ShowMessage('���������� ������ �������� ����������� ��������� (������ � �������������)')
  else if MessageDialog('���������� ��������?', mtConfirmation, [mbOK, mbCancel], 0)=mrOK then
    with dmCom, dm , dm2 do
      begin
        LogOperationID:=dm3.insert_operation('����������� ������ ���������',LogOprIdForm);

        PostDataSets([taA2]);
        try
          if not taA2.Locate('F1', 1, []) then raise Exception.Create('���������� ������� ��� ��������');

          Art2Id1:=taA2Art2Id.AsInteger;

          if not taA2.Locate('F2', 1, []) then raise Exception.Create('���������� ������� ��� ��������');

          Screen.Cursor:=crSQLWait;

          ExecuteQutmp(quTmp,'EXECUTE PROCEDURE PrOrdOnA2Merge '+IntToStr(UserId), 0, FieldsRes);

          while not taA2.EOF do
            begin
              if taA2F2.AsInteger=1 then
                begin
                  Art2Id2:=taA2Art2Id.AsInteger;
                  ExecuteQutmp(quTmp,'EXECUTE PROCEDURE PrOrdItemA2Merge '+IntToStr(UserId)+', '+IntToStr(Art2Id1)+', '+IntToStr(Art2Id2), 0, FieldsRes);
                end;
              taA2.Next;
            end;

          Screen.Cursor:=crSQLWait;
          ExecuteQutmp(quTmp,'EXECUTE PROCEDURE ClosePrOrdA2Merge '+IntToStr(UserId), 0, FieldsRes);

          taA2.Locate('F2', 1, []);

          while not taA2.EOF do
            begin
              if taA2F2.AsInteger=1 then
                begin
                  Art2Id2:=taA2Art2Id.AsInteger;
                  ExecuteQutmp(quTmp,'EXECUTE PROCEDURE A2MERGE '+IntToStr(Art2Id1)+
                     ', '+IntToStr(Art2Id2)+', '+IntToStr(UserId) + ', '' '', '' '', -1000', 0, FieldsRes);
                end;
              taA2.Next;
            end;
          taA2.Active:=False;
          tr.CommitRetaining;
        finally
          taA2.Active:=True;
          taA2.Locate('ART2ID', Art2Id1, []);
          ExecuteQutmp(quTmp,'DELETE FROM TMPDATA WHERE DATATYPE=1 and USERID='+IntToSTr(UserId), 0, FieldsRes);
          Screen.Cursor:=crDefault;
        end;

        dm3.update_operation(LogOperationID);
      end
end;

procedure TfmArtDict.N20Click(Sender: TObject);
var  LogOperationID :string;
begin
  LogOperationID:=dm3.insert_operation('�������������� ��������� ����',LogOprIdForm);

  with dm, dm2, dmCom do
    try
      fmSPEdit:=TfmSPEdit.Create(Application);
      with fmSPEdit do
        begin
           ce1.Value:=taA2Price1.AsFloat;
           if ShowModal=mrOK then
             begin
               with quSetSP do
                 begin
                   Params.ByName['PRICE'].AsFloat:=ce1.Value;
                   Params.ByName['TSPRICE'].AsFloat:=ce1.Value;
                   Params.ByName['ART2ID'].AsInteger:=taA2ARt2Id.AsInteger;
                   ExecQuery;
                 end;
               taA2.Refresh;
             end
        end
    finally
      fmSPEdit.Free;
    end;

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.DArt2ID1Click(Sender: TObject);
begin
  MessageDialog(dm.taA2ART2ID.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmArtDict.NCountryClick(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� �����',LogOprIdForm);

//  ShowAndFreeForm(TfmCountry, Self, TForm(fmCountry), True, False);
  b:=GetBit(dmCom.EditRefBook, 12) and CenterDep;
  ShowAndFreeFormEnabled(TfmCountry, Self, TForm(fmCountry), True, False, b,'siExit;tb1;spitPrint;siSort;', 'dg1;');
  dm.FillListBoxes(nil, nil, nil, nil,lbCountry,nil,nil);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N11Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� ������� 1',LogOprIdForm);

//  ShowAndFreeForm(TfmdAtt1, Self, TForm(fmdAtt1), True, False);
  b:=GetBit(dmCom.EditRefBook, 14) and CenterDep;
  ShowAndFreeFormEnabled(TfmdAtt1, Self, TForm(fmdAtt1), True, False, b, 'acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt1;');

  dm.FillListBoxes(nil, nil, nil, nil, nil,lbAtt1,nil);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.N21Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� ������� 2',LogOprIdForm);

//  ShowAndFreeForm(TfmdAtt2, Self, TForm(fmdAtt2), True, False);
  b:=GetBit(dmCom.EditRefBook, 15) and CenterDep;
  ShowAndFreeFormEnabled(TfmdAtt2, Self, TForm(fmdAtt2), True, False, b, 'acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt2;');
  dm.FillListBoxes(nil, nil, nil, nil, nil,nil,lbAtt2);

  dm3.update_operation(LogOperationID);
end;

procedure TfmArtDict.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100101)
end;

procedure TfmArtDict.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmArtDict.acPrintA2Execute(Sender: TObject);
begin
 if ActiveControl=dgA2 then begin prdgA2.DBGridEh:=dgA2; prdgA2.Print; end
 else begin prdgA2.DBGridEh:=dgArt; prdgA2.Print; end;
end;

procedure TfmArtDict.dgArtEditButtonClick(Sender: TObject);
begin
  with dm do
    D_ARTID:=quArtD_ArtId.AsInteger;
  ShowAndFreeForm(TfmARest, Self, TForm(fmARest), True, False);
end;

procedure TfmArtDict.dgArtEnter(Sender: TObject);
begin
  dgArt.SelectedIndex:=0;
end;

procedure TfmArtDict.dgArtExit(Sender: TObject);
begin
  PostDataSets([dm.quArt]);
end;

procedure TfmArtDict.dgArtGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if Column.Field<>NIL then
    if Column.Field.FieldName='FULLART' then
      Background:=dmCom.clMoneyGreen;
end;

procedure TfmArtDict.dgArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) and (dgArt.SelectedField.FieldName='UNITID') then
    with dm, quArt do
      begin
        if NOT (State in [dsInsert, dsEdit]) then Edit;
        with quArtUnitId do
          AsInteger:=Integer(NOT Boolean(AsInteger));
      end;
  if (Key=VK_RIGHT) and (Shift=[ssCtrl]) then
    begin
      ActiveControl:=dgA2;
      Key:=0;
    end;
end;

procedure TfmArtDict.dgArtMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var s: string[10];
    i: integer;
begin
 if Button= mbLeft then
 begin
  s:=dgArt.SelectedField.FieldName;
  if (s='F1') or (s='F2') then
    with dm, quArt do
      begin
        i:=FieldByName(s).AsInteger;
        if i=0 then i:=1
        else i:=0;
        Edit;
        FieldByName(s).AsInteger:=i;
        Post;
      end;
 end;     
end;

procedure TfmArtDict.SpeedItem1Click(Sender: TObject);
begin
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbAtt1.ItemIndex:=0;
  lbAtt2.ItemIndex:=0;
  with dmCom, dm do
    begin
      D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
      D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
      D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
      D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
      D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
      D_Att1Id:=TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
      D_Att2Id:=TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
      Old_D_CompId:=D_CompId;
      Old_D_MatId:=D_MatId;
      Old_D_GoodId:=D_GoodId;
      Old_D_InsId:=D_InsId;
      Old_D_CountryId:=D_countryId;
      Old_D_Att1Id:=D_Att1Id;
      Old_D_Att2Id:=D_Att2Id;
    end;

  Screen.Cursor:=crSQLWait;
  ReopenDataSets([dm.quArt]);
  Screen.Cursor:=crDefault;
end;

procedure TfmArtDict.siMergeComplexClick(Sender: TObject);
var fl: string;
    Res: Variant;
begin
  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                       #39' and Is_Merge_Run = current_connection',dm.quTmp);

  if VarIsNull(Res) then
  begin
    Res := ExecSelectSQL('Select first 1 FIO from D_EMP ' +
                         'where Is_Merge_Run <> current_connection and Is_Merge_Run <>-1',dm.quTmp);
    if not VarIsNull(Res)  then
     ShowMessage('����������� ������ ������ '+ VarToStr(Res) + ' � ������ ����������. ��������� ��������� ��������')
    else
    begin
      Res := ExecSelectSQL('Select FIO from D_EMP '+
                           'where ((Is_Merge_Run = current_connection) or (Is_Merge_Run = -1)) and '+
                                  ' FIO = '#39 + dmCom.User.FIO + #39,dm.quTmp);
      if not VarIsNull(Res)  then
      begin
        fl := 'current_connection';
        siMergeComplex.ImageIndex := 10;
      end
    end
  end
  else
  begin
   fl := '-1';
   siMergeComplex.ImageIndex := 53;
  end;
  if fl <> '' then ExecSQL('update D_Emp Set Is_Merge_Run = '+ fl+' where D_EmpID = ' + IntToStr(dmCom.UserId) ,dm.quTmp);
end;

procedure TfmArtDict.FormClose(Sender: TObject; var Action: TCloseAction);
var Res: Variant;
begin
   Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                            #39' and Is_Merge_Run= current_connection',dm.quTmp);
   if (not VarIsNull(Res)) then
      if MessageDialog('���� ������ �������� �����������. ���������', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then siMergeComplexClick(nil);
end;

end.
