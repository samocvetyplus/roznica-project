object fmDepAndDate: TfmDepAndDate
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072' '#1076#1083#1103' '#1072#1085#1072#1083#1080#1079#1072
  ClientHeight = 90
  ClientWidth = 262
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 22
    Width = 44
    Height = 18
    Caption = #1057#1082#1083#1072#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object cbDep: TDBComboBox
    Left = 61
    Top = 24
    Width = 193
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnDropDown = cbDepDropDown
  end
  object BitBtn1: TBitBtn
    Left = 98
    Top = 57
    Width = 75
    Height = 25
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 179
    Top = 57
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    Kind = bkCancel
  end
  object qDep: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT D_DEPID, NAME, SNAME'
      'FROM D_DEP'
      'WHERE D_DEPID>1 and ISDELETE <> 1'
      ''
      'ORDER BY SORTIND')
  end
end
