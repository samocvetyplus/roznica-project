unit PredInventory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, DBCtrlsEh, DBLookupEh, Grids, DBGridEh,
  Db;

type
  TfmPredInverty = class(TForm)
    btok: TBitBtn;
    btCancel: TBitBtn;
    lbInverDate: TLabel;
    edInverDate: TDBDateTimeEditEh;
    lbNPrord: TLabel;
    lbProrddate: TLabel;
    edInvPrordN: TDBEditEh;
    edInvPrordDate: TDBDateTimeEditEh;
    Label1: TLabel;
    lcbxReponsibleId: TDBLookupComboboxEh;
    edInvDate: TDBDateTimeEditEh;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FDataSource : TDataSource;
  end;

var
  fmPredInverty: TfmPredInverty;

implementation

uses comdata, M207Proc, Inventory, Data, dbUtil, ServData;

{$R *.dfm}

procedure TfmPredInverty.FormCreate(Sender: TObject);
begin
  CloseDataSets([dmCom.taMol, dmserv.quInvSinv]);
  OpenDataSet(dmserv.quInvSinv);
  dmCom.taMol.ParamByName('D_COMPID').AsInteger := dmserv.quInvSinvCOMPID.AsInteger;
  FDataSource := dmCom.taMol.DataSource;
  dmCom.taMol.DataSource := nil;
  OpenDataSet(dmCom.taMol);
end;

procedure TfmPredInverty.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult=mrOk then PostDataSets([dmserv.quInvSinv])
  else CancelDataSets([dmserv.quInvSinv]);
  dmCom.taMol.Close;
  dmCom.taMol.DataSource := FDataSource;
end;

end.
