unit frmDialogJournalClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, FIBDataSet, pFIBDataSet,
  FIBDatabase, pFIBDatabase, dxStatusBar, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxGridBandedTableView, cxGridDBBandedTableView,
  ImgList, dxBar, dxBarExtItems, ActnList, Grids, DBGrids, 
  ExtCtrls, StdCtrls, Mask, M207Proc, dbUtil, cxCheckBox,
  cxCheckGroup, cxCheckComboBox, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, dxSkinsdxStatusBarPainter, dxSkinsdxBarPainter,
  rxToolEdit, rxSpeedbar;

type
  TDialogJournalClient = class(TForm)
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    dxStatusBar: TdxStatusBar;
    DatabaseAddress: TpFIBDatabase;
    TransactionAddress: TpFIBTransaction;
    DataSetJounalClient: TpFIBDataSet;
    GridDBBandedTableView: TcxGridDBBandedTableView;
    DataSourceJounalClient: TDataSource;
    GridDBBandedTableViewCARD: TcxGridDBBandedColumn;
    GridDBBandedTableViewTITLE: TcxGridDBBandedColumn;
    GridDBBandedTableViewBIRTHDAY: TcxGridDBBandedColumn;
    GridDBBandedTableViewADDRESSTITLE: TcxGridDBBandedColumn;
    GridDBBandedTableViewADDRESSHOMEFLAT: TcxGridDBBandedColumn;
    GridDBBandedTableViewPASSPORTNUMBER: TcxGridDBBandedColumn;
    GridDBBandedTableViewPASSPORTSERIES: TcxGridDBBandedColumn;
    GridDBBandedTableViewPASSPORTDATE: TcxGridDBBandedColumn;
    GridDBBandedTableViewPASSPORTORGANIZATION: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn1: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn2: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn3: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn4: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn5: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn6: TcxGridDBBandedColumn;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    GridDBBandedTableViewColumn7: TcxGridDBBandedColumn;
    dxBarManager1: TdxBarManager;
    dxBarLargeButton1: TdxBarLargeButton;
    EnabledImageList: TImageList;
    ActionList: TActionList;
    ActionBalance: TAction;
    DataSetClientBalance: TpFIBDataSet;
    DataSetClientBalanceSELLQUANTITY: TFIBIntegerField;
    DataSetClientBalanceSELLSUMM: TFIBFloatField;
    DataSetClientBalanceRETURNQUANTITY: TFIBIntegerField;
    DataSetClientBalanceRETURNSUMM: TFIBFloatField;
    DataSetClientBalanceBALANCEQUANTITY: TFIBIntegerField;
    DataSetClientBalanceBALANCESUMM: TFIBFloatField;
    DataSetClientBalanceDEPARTMENTTITLE: TFIBStringField;
    DataSetClientBalanceDEPARTMENTID: TFIBIntegerField;
    DataSourceClientBalance: TDataSource;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siEdit: TSpeedItem;
    SpeedItem2: TSpeedItem;
    tb3: TSpeedBar;
    lbFind: TLabel;
    lbFindUID: TLabel;
    edFIO: TComboEdit;
    edNodCard: TComboEdit;
    SpeedbarSection4: TSpeedbarSection;
    cxStyle3: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    SpeedItem1: TSpeedItem;
    GridDBBandedTableViewColumn8: TcxGridDBBandedColumn;
    GridDBBandedTableViewColumn9: TcxGridDBBandedColumn;
    DataSetJounalClientID: TFIBIntegerField;
    DataSetJounalClientCARD: TFIBStringField;
    DataSetJounalClientTITLE: TFIBStringField;
    DataSetJounalClientBIRTHDAY: TFIBDateField;
    DataSetJounalClientADDRESSID: TFIBIntegerField;
    DataSetJounalClientADDRESSTITLE: TFIBStringField;
    DataSetJounalClientADDRESSHOMEFLAT: TFIBStringField;
    DataSetJounalClientPASSPORTNUMBER: TFIBStringField;
    DataSetJounalClientPASSPORTSERIES: TFIBStringField;
    DataSetJounalClientPASSPORTDATE: TFIBDateField;
    DataSetJounalClientPASSPORTORGANIZATION: TFIBStringField;
    DataSetJounalClientSELLQUANTITY: TFIBIntegerField;
    DataSetJounalClientSELLSUMM: TFIBFloatField;
    DataSetJounalClientRETURNQUANTITY: TFIBIntegerField;
    DataSetJounalClientRETURNSUMM: TFIBFloatField;
    DataSetJounalClientTOTALQUANTITY: TFIBIntegerField;
    DataSetJounalClientTOTALSUMM: TFIBFloatField;
    DataSetJounalClientDISCOUNT: TFIBStringField;
    DataSetJounalClientFLAG: TFIBIntegerField;
    DataSetJounalClientF2: TFIBSmallIntField;
    DataSetJounalClientF3: TFIBSmallIntField;
    cxStyleRepository3: TcxStyleRepository;
    cxStyle6: TcxStyle;
    cxStyleRepository4: TcxStyleRepository;
    cxStyle7: TcxStyle;
    cxStyleRepository5: TcxStyleRepository;
    cxStyle8: TcxStyle;
    SpeedItem3: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure DataSetJounalClientAfterPost(DataSet: TDataSet);
    procedure siExitClick(Sender: TObject);
    procedure siEditClick(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edNodCardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNodCardKeyPress(Sender: TObject; var Key: Char);
    procedure edNodCardKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFIOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFIOKeyPress(Sender: TObject; var Key: Char);
    procedure DataSetJounalClientBeforeOpen(DataSet: TDataSet);
    procedure SpeedItem1Click(Sender: TObject);
    procedure edNodCardButtonClick(Sender: TObject);
    procedure edFIOButtonClick(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }
     CanselFl: boolean; {� ���� ����� ����. ����� ������� "�" ��� "�"}
     Filter_FIO: string;
  public
    { Public declarations }
  end;

var
  DialogJournalClient: TDialogJournalClient;

implementation

uses ComData, Data, DetalSell, AnlzClient_SellDep; //, AnlzClient_SellDep


{$R *.dfm}

procedure TDialogJournalClient.FormCreate(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  OpenDataSet(DataSetJounalClient);
  Screen.Cursor:=crDefault;
  edNodcard.Text := '';
  edFIO.Text := '';
  Filter_FIO := '';
  SpeedItem2.Enabled:= not CenterDep;
  SpeedItem1.Enabled:= CenterDep;
  SpeedItem3.Enabled:= CenterDep;
  GridDBBandedTableView.Bands[5].Visible:=CenterDep;
  tb1.Wallpaper:=wp;
  tb3.Wallpaper:=wp;
  GridDBBandedTableViewColumn1.Styles.Content.Color:=dmcom.clMoneyGreen;
  GridDBBandedTableViewColumn2.Styles.Content.Color:=dmcom.clMoneyGreen;
  GridDBBandedTableViewColumn3.Styles.Content.Color:=dmcom.clMoneyGreen;
  GridDBBandedTableViewColumn4.Styles.Content.Color:=dmcom.clMoneyGreen;
  GridDBBandedTableViewColumn5.Styles.Content.Color:=dmcom.clMoneyGreen;
  GridDBBandedTableViewColumn6.Styles.Content.Color:=dmcom.clMoneyGreen;
end;

procedure TDialogJournalClient.DataSetJounalClientAfterPost(
  DataSet: TDataSet);
begin
  DataSetJounalClient.Transaction.CommitRetaining;
end;

procedure TDialogJournalClient.siExitClick(Sender: TObject);
begin
close;
end;

procedure TDialogJournalClient.siEditClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmDetalSell, Self, TForm(fmDetalSell), True, False);
end;

procedure TDialogJournalClient.SpeedItem2Click(Sender: TObject);
begin
 with dmCom do
 begin
  ExecSQL('EXECUTE PROCEDURE FILL_CLIENT ', quTmp);
 end;
 ReOpenDataSet(DataSetJounalClient);
end;

procedure TDialogJournalClient.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    with DialogJournalClient, dmCom do
    begin
      CloseDataSets([DataSourceJounalClient.DataSet]);
      if not tr.Active then tr.StartTransaction;
      tr.CommitRetaining;
      if not tr1.Active then tr1.StartTransaction;
      tr1.CommitRetaining;
    end;
    Deactivate;
end;

procedure TDialogJournalClient.edNodCardKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var FieldsRes:t_array_var;
begin
  case key of
  VK_RETURN:
  with dmcom do
  begin
    if (edNodcard.Text='') then
       Application.MessageBox('������� ����� ���������� �����', '��������!', 0);
    try
    {����� ������ ����.����� �� ������ ������ ������}
       Screen.Cursor:=crSQLWait;
       if CenterDep then
       ExecuteQutmp(quTmp, 'select card, flag from client$center where '+
                'card = '''+edNodcard.Text+'''' + 'and IsDel is null', 2, FieldsRes)
          else
          ExecuteQutmp(quTmp, 'select nodcard, fmain from client where '+
                'nodcard = '''+edNodcard.Text+'''' + 'and fdel=0', 2, FieldsRes);
       if (FieldsRes[0] <> null) then
       begin
          DataSetJounalClient.Locate('CARD',ednodcard.Text,[]);
          edNodCard.Text := '';
          Screen.Cursor:=crDefault;
       end
         else
       begin
          Application.MessageBox(pchar('����� '+edNodcard.Text+' �� �������!'), '��������!', 0);
          edNodCard.Text := '';
          Screen.Cursor:=crDefault;
       end;
    finally
    Finalize(FieldsRes);
    end;
  end;
  end;
end;

procedure TDialogJournalClient.edNodCardKeyPress(Sender: TObject;
  var Key: Char);
begin
  case key of
  '0'..'9','/',#8 : ;
  '�', '�': CanselFl:=true;
  else sysUtils.Abort;
  end;
end;

procedure TDialogJournalClient.edNodCardKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if CanselFl then
 begin
   edNodCard.Text:='��� ����. �����';
   CanselFl:=false;
 end;
end;

procedure TDialogJournalClient.edFIOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
  VK_RETURN:
  begin
    Filter_FIO:= edfio.Text;
    Screen.Cursor:=crSQLWait;
    ReOpenDataSet(DataSetJounalClient);
    Screen.Cursor:=crDefault;
  end;
  vk_DOWN :
  begin
    ActiveControl := Grid;
    Grid.FieldAddress(DataSetJounalClientTITLE.AsString);
  end;
  end
end;

procedure TDialogJournalClient.edFIOKeyPress(Sender: TObject;
  var Key: Char);
begin
 case key of
 '�'..'�',#8,'�'..'�','a'..'z','A'..'Z',' ', '.' : ;
 else sysUtils.Abort;
 end;
end;

procedure TDialogJournalClient.DataSetJounalClientBeforeOpen(
  DataSet: TDataSet);
begin
  if Filter_FIO<>'' then
     DataSetJounalClient.SelectSQL[24]:=' WHERE TITLE LIKE '''+ AnsiUpperCase(Filter_FIO) +'%'''
     else DataSetJounalClient.SelectSQL[24] := '';
end;

procedure TDialogJournalClient.SpeedItem1Click(Sender: TObject);
var newclientid, oldclientid:integer;
    nodcard:string;
begin
 newclientid:=-2; oldclientid:=-2;
 Screen.Cursor:=crSQLWait;
 PostDataSet(DataSetJounalClient);
 if not DataSetJounalClient.Locate('F2',1,[]) then
        begin
           MessageDlg('�������� ����. ����� �� ����������� � ���� *', mtWarning, [mbOk], 0);
           Screen.Cursor:=crDefault;
        end
    else if DataSetJounalClientID.AsInteger=-1 then
            begin
               MessageDlg('����� �� ����� ���� ���������� � "��� ����. �����"', mtWarning, [mbOk], 0);
               Screen.Cursor:=crDefault;
            end
        else
        begin
           newclientid:=DataSetJounalClientID.AsInteger;
           nodcard:=DataSetJounalClientCARD.AsString;
        end;

 if not DataSetJounalClient.Locate('F3',1,[]) then
        begin
           MessageDlg('�������� ����. ����� �� ����������� � ���� **', mtWarning, [mbOk], 0);
           Screen.Cursor:=crDefault;
        end
    else if DataSetJounalClientID.AsInteger=-1 then
            begin
               MessageDlg('������ ����� "��� ����. �����" ����������', mtWarning, [mbOk], 0);
               Screen.Cursor:=crDefault;
            end
         else
            oldclientid:=DataSetJounalClientID.AsInteger;
 if (newclientid<>-2) and (oldclientid<>-2) then
 begin
  Screen.Cursor:=crSQLWait;
  ExecSQL('execute procedure CLIENT$UNION ('+inttostr(oldclientid)+', '+ inttostr(newclientid)+')', dm.quTmp);
  ReOpenDataSet(DataSetJounalClient);
  DataSetJounalClient.Locate('CARD',nodcard,[]);
 end;
end;

procedure TDialogJournalClient.edNodCardButtonClick(Sender: TObject);
var
  Key : word;
begin
 Key := VK_RETURN;
   edNodCardKeyDown(Sender, Key, []);
end;

procedure TDialogJournalClient.edFIOButtonClick(Sender: TObject);
var
  Key : word;
begin
 Key := VK_RETURN;
   edFIOKeyDown(Sender, Key, []);
end;

procedure TDialogJournalClient.SpeedItem3Click(Sender: TObject);
begin
   ShowAndFreeForm(TfmAnlzClient_SellDep, Self, TForm(fmAnlzClient_SellDep), True, False);
end;

end.

