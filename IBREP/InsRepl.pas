unit InsRepl;

interface

uses pFIBDatabase, pFIBQuery, SysUtils, pFIBDataSet, Menus, Classes, Dialogs, FIBDataBase;

procedure  InsRPacket(db: TpFIBDatabase; R_Item, TargetID: string; ParamType: integer; KeyValue, Memo: string;
                      NeedAck: integer; var NeedSPR: integer; var PacketId:integer; SourceID: string = '');

//procedure  InsRPacket(db: TIBDatabase; R_Item, DestId: string; ParamType: integer; KeyValue, Memo: string; NeedAck: integer);
procedure  FillReplMenu(db: TpFIBDatabase; pm: TPopUpMenu; ev: TNotifyEvent);
procedure  GetHereId(db: TpFIbDatabase);

var slDep: TStringList;
    slNDep: TStringLIst;

var HereId: string;
    NHereId: integer;
    PacketCount: integer;



implementation

uses RxStrUtils, comdata, data, uDialog;


procedure InsRPacket(db: TpFIBDatabase; R_Item, TargetID: string; ParamType: integer; KeyValue, Memo: string;
                      NeedAck: integer; var NeedSPR: integer; var PacketId:integer; SourceID: string = '');
var q: TpFIBQuery;
    tr: TFIBTransaction;
    InTr, DoIns: boolean;
    Table: string;
    R_Date: TDateTime;
    DepID: Integer;
begin


  {
  if not ((R_Item = 'DESCRIPTION') or (R_Item = 'CLIENT')) then
  begin
    exit;
  end;
  }

  {
  if ((R_Item = 'BUY_INVOICE') or (R_Item = 'BUY_ORDER')) then
  begin
    if not (
            (Trim(TargetID) = 'R') or
            (Trim(TargetID) = 'BG') or
            (Trim(TargetID) = 'CN') or
            (Trim(TargetID) = 'BG-GR') or
            (Trim(TargetID) = 'O') or
            (Trim(TargetID) = 'KR') or
            (Trim(TargetID) = 'KE')
            ) then
    begin
      exit;
    end;
  end;
  }

  DoIns := False;
  if R_Item = '' then raise Exception.Create('R_ITEM is empty');
  //if (R_Item='CLIENT') then exit;
  //if (R_Item='DINV') or (R_Item='DELRECORDS') or (R_Item='PRORD')or (R_Item='ACTALLOWANCES') then exit;
  //if not((R_Item<>'DESCRIPTION') or (R_Item<>'SERTIFICATE')) then exit;
  q:=TpFIBQuery.Create(NIL);
  try
    q.Database:=db;
    tr:=db.DefaultTransaction;
    q.Transaction:=tr;
    InTr:=not tr.Active;
    if InTr then tr.StartTransaction;
    case ParamType of
    11:
    with q do
      begin
        ParamType := 1;
        Close;
        SQL.Text:='SELECT TABLENAME FROM R_ITEM WHERE R_ITEM='''+R_Item+'''';

        ExecQuery;
        Table:=Fields[0].AsString;
        Close;
        SQL.Text := 'SELECT d_depid from d_dep where r_code = :r_code';
        Params[0].AsString := TargetID;

        ExecQuery;
        Depid := Fields[0].AsInteger;
        Close;
        SQL.Text:='select MAX(FDate) FDate '+
                  'from R_Packet '+
                  'where R_Item=?RItem and '+
                  'SourceId=?SourceId and '+
                  'DestId= ?DestId and '+
                  'Direction=1 and '+
                  'PacketState=-1 ';
        ParamByName('RITEM').AsString:=R_Item;
        ParamByName('SOURCEID').AsString:=HereId;
        ParamByName('DESTID').AsString:=TargetID;

        ExecQuery;
        if RecordCount=0 then R_Date:=EncodeDate(1900, 1, 1)
        else R_Date:=Fields[0].AsDateTime;
        Close;
        SQL.Text:='SELECT COUNT(*) FROM '+Table+' WHERE UPDATE$DATE>?R_DATE';
        Params[0].AsTimeStamp:= DateTimeToTimeStamp(R_Date);

        ExecQuery;
        DoIns:=Fields[0].AsInteger>0;
        Close;
      end;
    // !!!
    9:with q do     // ���������� �������� � ������
      begin
        Close;
        SQL.Text:='SELECT TABLENAME FROM R_ITEM WHERE R_ITEM='''+R_Item+'''';
        ExecQuery;
        Table:=Fields[0].AsString;
        Close;
        SQL.Text := 'SELECT d_depid from d_dep where r_code = :r_code';
        Params[0].AsString := TargetID;
        ExecQuery;
        Depid := Fields[0].AsInteger;
        Close;

        SQL.Text:='select MAX(FDate) FDate '+
                  'from R_Packet '+
                  'where R_Item=?RItem and '+
                  'SourceId=?SourceId and '+
                  'DestId= ?DestId and '+
                  'Direction=1 and '+
                  'PacketState=-1 ';

        ParamByName('RITEM').AsString:=R_Item;
        ParamByName('SOURCEID').AsString:=HereId;
        ParamByName('DESTID').AsString:=TargetID;
        ExecQuery;
        if RecordCount=0 then R_Date:=EncodeDate(1900, 1, 1)
        else R_Date:=Fields[0].AsDateTime;
        Close;

        if AnsiUpperCase(Trim(HereId)) = 'CN'  then
        begin
          if AnsiUpperCase(Trim(r_item)) = 'CLIENT' then
          begin
            SQL.Text := 'select count(*) from r$client$c$s(:depid, :r_date)';

            //SQL.Text := 'select count(*) from r$client$center$s2(:depid, :r_date)';

            Params[1].AsTimeStamp:= DateTimeToTimeStamp(R_Date);

            Params[0].AsInteger := DepID;
          end else

          begin
           SQL.Text:='SELECT COUNT(*) FROM '+Table+' WHERE R_DATE>?R_DATE and DEPID = ?DEPID';

           Params[1].AsTimeStamp:= DateTimeToTimeStamp(R_Date);

           Params[0].AsInteger := DepID;
          end;
        end else

        begin
          if AnsiUpperCase(Trim(r_item)) = 'CLIENT' then
          begin
            SQL.Text := 'select count(*) from r$client$o$s(:r_date)';

            Params[0].AsTimeStamp := DateTimeToTimeStamp(R_Date);
          end else

          begin
            SQL.Text:='SELECT COUNT(*) FROM '+Table+' WHERE R_DATE>?R_DATE and DEPID = ?DEPID';

            Params[1].AsTimeStamp:= DateTimeToTimeStamp(R_Date);

            Params[0].AsInteger := DepID;
          end;
        end;

        ExecQuery;

        DoIns := Fields[0].AsInteger > 0;

        Close;
      end;
    // !!!
    0: with q do
       begin
        if NeedSPR=0 then DoIns:=true
        else
        begin
         close;
         sql.Text := 'select p.r_packetid from r_packet p '+
                     'where p.packetstate<>-1 and R_item<>'#39+R_Item+#39' and p.DestID ='#39+TargetID+#39;
         ExecQuery;
         if Fields[0].AsInteger<>0 then DoIns:=True;
         close;
        end
       end;
    // ���������� �������� � �������
    1: with q do      
       begin
        SQL.Text:='SELECT TABLENAME FROM R_ITEM WHERE R_ITEM='''+R_Item+'''';
        ExecQuery;
        Table:=Fields[0].AsString;
        Close;

        SQL.Text:='select MAX(FDate) FDate '+
                  'from R_Packet '+
                  'where R_Item=?RItem and '+
                  'SourceId=?SourceId and '+
                  'DestId= ?DestId and '+
                  'Direction=1 and '+
                  'PacketState=-1 ';

        ParamByName('RITEM').AsString:=R_Item;
        ParamByName('SOURCEID').AsString:=HereId;
        ParamByName('DESTID').AsString:=TargetID;
        ExecQuery;
        if RecordCount=0 then R_Date:=EncodeDate(1900, 1, 1)
        else R_Date:=Fields[0].AsDateTime;
        Close;
        //������� ���������� ������� ��� ����������
        SQL.Text:='SELECT COUNT(*) FROM '+Table+' WHERE R_DATE>?R_DATE ';

        Params[0].AsTimeStamp:= DateTimeToTimeStamp(R_Date);
        ExecQuery;
        DoIns:=Fields[0].AsInteger>0; // true - ���� ���-� ��� ����������
        Close;
       end;
    4: with q do
       begin
        close;
        sql.Text := 'select dl.delrecordsid from delrecords dl, d_dep dp '+
                    'where dp.d_depid = dl.depid and dl.rstate<=2 and '+
                    ' dl.flag ='+ ExtractWord(2,KeyValue,[';'])+' and dp.r_code = '#39+TargetID+#39;
        ExecQuery;
        if Fields[0].AsInteger<>0 then DoIns:=True;
        close;
       end;
    5: with q do
       begin
        close;
        if R_Item='EDTSEL' then
         sql.Text := 'select dl.edtselid from edtsel dl, d_dep dp '+
                     'where dp.d_depid = dl.depid and dl.rstate<=2 and '+
                     ' dp.r_code = '#39+TargetID+#39
        else
         sql.Text := 'select dl.r_clientcnid from r_clientcn dl, d_dep dp '+
                     'where dp.d_depid = dl.depid and dl.rstate<=2 and '+
                     ' dp.r_code = '#39+TargetID+#39;
        ExecQuery;
        if Fields[0].AsInteger<>0 then DoIns:=True;
        close;
       end;
    6: with q do
       begin
        close;
        sql.Text := 'select dl.sinfoid from rep_sinfo dl, d_dep dp '+
                    'where dp.d_depid = dl.depid and dl.rstate<=2 and '+
                    ' dp.r_code = '''+TargetID+'''';
        ExecQuery;
        if Fields[0].AsInteger<>0 then DoIns:=True;
        close;
       end;
    7: with q do
       begin
        Close;
        SQL.Text:='SELECT TABLENAME FROM R_ITEM WHERE R_ITEM='''+R_Item+'''';
        ExecQuery;
        Table:=Fields[0].AsString;
        Close;

        if (trim(Table)='D_ADDRESS') or (trim(Table)='CLIENT') then
         SQL.Text:='select MAX(c.R_Date) '+
                   'from ' +Table+' c, d_dep d '+
                   'where c.r_cn=1 and '+
                   'd.d_depid = c.depid and '+
                   'd.r_code= ?DestId '
        else
         SQL.Text:='select MAX(FDate) FDate '+
                   'from R_Packet '+
                   'where R_Item='#39+R_Item+#39' and '+
                   'SourceId='#39+HereId+#39' and '+
                   'DestId= ?DestId and '+
                   'Direction=1 and '+
                   'PacketState=-1 ';
        ParamByName('DESTID').AsString:=TargetID;
        ExecQuery;
        if Fields[0].IsNull then R_Date:=EncodeDate(1900, 1, 1)
        else R_Date:=Fields[0].AsDateTime;
        Close;
        KeyValue:=KeyValue+';'+datetimetostr(r_date)+';';
        if (trim(Table)='D_ADDRESS') or (trim(Table)='CLIENT') then
         SQL.Text:='SELECT COUNT(*) FROM '+Table+' c, d_dep d '+
                   'WHERE c.R_CN=1 and c.depid=d.d_depid and d.r_code=?R_CODE'
        else
         SQL.Text:='SELECT COUNT(*) FROM '+Table+' c, d_dep d '+
                   'WHERE c.depid=d.d_depid and d.r_code=?R_CODE and c.R_Date>'#39+datetimetostr(R_Date)+#39' ';
        Params[0].AsString:= TargetID;
        ExecQuery;
        DoIns:=RecordCount>0;
        Close;
       end;

    else DoIns:=True;
    end;
    // ����� ����
    if NeedSPR=0 then
    begin
      if SourceID = '' then
      begin
        SourceID := HereId;
      end;
    if DoIns then
      begin
        q.Close;
        q.SQL.Text:='INSERT INTO R_PACKET(R_ITEM, SOURCEID, DESTID, PARAMTYPE, DIRECTION, CDATE, KEYVAL, MEMO, NEEDACK,NEEDSPR) '+
                     'VALUES ('''+R_Item+''','''+SourceID+''','''+TargetID+''','+
                              IntToStr(ParamType)+', 1, ''NOW'', '''+KeyValue+''', '''+Memo+''','+IntToStr(NeedAck)+' , '+ IntToStr(NeedSPR)+')';
        q.ExecQuery;
        Inc(PacketCount);
        if ParamType=0 then NeedSPR:=dmcom.GetId(43);
        PacketId:=dmcom.GetId(43);
      end;
    end
    else
    begin
     if not DoIns then
     begin
      q.Close;
      q.SQL.Text:='Delete from R_PACKET where R_PACKETID='+inttostr(NeedSPR);
      q.ExecQuery;
      Dec(PacketCount);
     end;
    end;

    if InTr then tr.Commit
    else tr.CommitRetaining;
  finally
    q.Free;
  end;
end;


procedure FillReplMenu(db: TpFIBDatabase; pm: TPopUpMenu; ev: TNotifyEvent);
var i, t: integer;
    mi: TMenuItem;
    b: boolean;
    q, quROpt: TpFIBDataSet;
    tr: TFIBTransaction;
    s1, s2: string;

begin
  q:=TpFIBDataSet.Create(NIL);
  quROpt:=TpFIBDataSet.Create(NIL);
  tr:=db.DefaultTransaction;
  b:=not tr.Active;
  if b then tr.StartTransaction;

  try

    with quROpt do
      begin
        SelectSQL.Text:='select * from R_Opt';
        Database:=db;
        Transaction:=tr;
        Active:=True;
        s2:='select '+FieldByName('FID').AsString+' ID, '+
                      FieldByName('FDEP').AsString+' DEP, '+
                      FieldByName('FID0').AsString+' NCODE'+
            ' from '+FieldByName('DEPTABLE').AsString;

        s1:=s2+' where '+FieldByName('FUSEDEP').AsString+'=1';
        Active:=False;
      end;

    q.Transaction:=tr;
    q.Database:=db;
    q.SelectSQL.Text:=s1;
    with q do
      begin
        try
          Active:=True;
        except
        end;
        i:=0;
        t:=0;
        while NOT EOF do
          begin
            if HereId<>Fields[0].AsString then
              begin
                mi:=TMenuItem.Create(pm);
                mi.Caption:=q.Fields[1].AsString;
                mi.Tag:=t;
                mi.OnClick:=ev;
                mi.GroupIndex:=0;
                mi.RadioItem:=True;
                slDep.Values[IntToStr(t)]:=Fields[0].AsString;
                slNDep.Values[Fields[2].AsString]:=Fields[0].AsString;
                Inc(i);
                Inc(t);
                if i>30 then
                  begin
                    mi.Break:=mbBreak;
                    i:=0;
                  end;
                pm.Items.Add(mi);
              end;
            Next;
          end;
        Active:=False;
      end;

    q.SelectSQL.Text:=s2;
    with q do
      begin
        try
          Active:=True;
        except
        end;
        while NOT EOF do
          begin
            slNDep.Values[Fields[2].AsString]:=Fields[0].AsString;
            Next;
          end;
        Active:=False;
      end;

  finally
    if b then tr.Commit
    else tr.CommitRetaining;
    q.Free;
    quROpt.Free;
  end;
end;

procedure  GetHereId(db: TpFIBDatabase);
var q: TpFIBQuery;
    tr: TFIBTransaction;
    b: boolean;
    quROpt: TpFIBDataSet;
    s1: string;
begin
  q:=TpFIBQuery.Create(NIL);
  quROpt:=TpFIBDataSet.Create(NIL);

  try
    tr:=db.DefaultTransaction;
    b:=not tr.Active;
    if b then tr.StartTransaction;

    with quROpt do
      begin
        SelectSQL.Text:='select * from R_Opt';
        Database:=db;
        Transaction:=tr;
        Active:=True;
        s1:='select '+FieldByName('FID').AsString+' ID, '+
                      FieldByName('FID0').AsString+' NCODE'+
            ' from '+FieldByName('DEPTABLE').AsString+
            ' where '+FieldByName('FHERE').AsString+'=1';
        Active:=False;
      end;

    q.Database:=db;
    q.Transaction:=tr;
    q.SQL.Text:=s1;
    q.ExecQuery;
    HereId:=DelRSpace(q.Fields[0].AsString);
    NHereId:=q.Fields[1].AsInteger;
    q.Close;
    if b then tr.Commit
    else tr.CommitRetaining;
  finally
    q.Free;
    quROpt.Free;
  end;
end;

initialization

  slDep:=TStringList.Create;
  slNDep:=TStringList.Create;

finalization

  slNDep.Free;
  slDep.Free;

end.
