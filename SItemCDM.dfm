object fmSItemCDM: TfmSItemCDM
  Left = 183
  Top = 164
  Width = 727
  Height = 524
  Caption = #1047#1072#1103#1074#1082#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter6: TSplitter
    Left = 0
    Top = 293
    Width = 719
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object ibgrWH: TM207IBGrid
    Left = 0
    Top = 45
    Width = 719
    Height = 248
    Align = alClient
    Color = clBtnFace
    DataSource = dmServ.dsrSItemCDM
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 1
    IniStorage = fmstrSItemCDM
    TitleButtons = True
    MultiShortCut = 116
    ColorShortCut = 0
    InfoShortCut = 0
    MultiSelectMode = msmTeapot
    PrintDataSet = 0
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'recno'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 34
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UID'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UID_SUP'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'FULLART'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 192
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SZ'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'W'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'COST'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 719
    Height = 45
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object Label2: TLabel
      Left = 196
      Top = 18
      Width = 115
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081': '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 197
      Top = 4
      Width = 201
      Height = 14
      DataField = 'SNAME'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 343
      Top = 18
      Width = 98
      Height = 14
      DataField = 'PR'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 343
      Top = 31
      Width = 65
      Height = 15
      DataField = 'ALLQ'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 197
      Top = 30
      Width = 45
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 618
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = siAddClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 296
    Width = 719
    Height = 201
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 291
      Top = 2
      Width = 3
      Height = 197
      Cursor = crHSplit
    end
    object Panel5: TPanel
      Left = 2
      Top = 2
      Width = 289
      Height = 197
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Tag = 1
        Left = 33
        Top = 1
        Width = 3
        Height = 195
        Cursor = crHSplit
        AutoSnap = False
        MinSize = 20
      end
      object Splitter4: TSplitter
        Tag = 2
        Left = 157
        Top = 1
        Width = 3
        Height = 195
        Cursor = crHSplit
        AutoSnap = False
        MinSize = 20
      end
      object Splitter5: TSplitter
        Tag = 3
        Left = 209
        Top = 1
        Width = 3
        Height = 195
        Cursor = crHSplit
        AutoSnap = False
        MinSize = 20
      end
      object Splitter8: TSplitter
        Tag = 2
        Left = 85
        Top = 1
        Width = 3
        Height = 195
        Cursor = crHSplit
        AutoSnap = False
        MinSize = 20
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 32
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 160
        Top = 1
        Width = 49
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 88
        Top = 1
        Width = 69
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 212
        Top = 1
        Width = 76
        Height = 195
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 36
        Top = 1
        Width = 49
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object Panel2: TPanel
      Left = 294
      Top = 2
      Width = 423
      Height = 197
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 1
      object tb2: TSpeedBar
        Left = 2
        Top = 2
        Width = 419
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 0
        InternalVer = 1
        object cbSearch: TCheckBox
          Left = 4
          Top = 8
          Width = 53
          Height = 17
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = cbSearchClick
        end
        object ceArt: TComboEdit
          Left = 58
          Top = 4
          Width = 93
          Height = 21
          ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
          Color = clInfoBk
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000120B0000120B00001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
            DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
            9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
            9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
            90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
          NumGlyphs = 1
          TabOrder = 1
          OnButtonClick = ceArtButtonClick
          OnChange = ceArtChange
          OnEnter = ceArtEnter
          OnKeyDown = ceArtKeyDown
          OnKeyUp = ceArtKeyUp
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1042#1089#1077
          Caption = 'SpeedItem1'
          Enabled = False
          Hint = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Spacing = 1
          Left = 163
          Top = 3
          OnClick = SpeedItem1Click
          SectionName = 'Untitled (0)'
        end
      end
      object dgArt: TM207IBGrid
        Left = 2
        Top = 31
        Width = 419
        Height = 164
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsArt
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = siAddClick
        OnExit = dgArtExit
        OnKeyDown = dgArtKeyDown
        TitleButtons = True
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        PrintDataSet = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FULLART'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Alignment = taLeftJustify
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            PickList.Strings = (
              #1043#1088
              #1064#1090)
            Title.Alignment = taCenter
            Title.Caption = #1045#1048
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 30
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RQ'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 35
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RW'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 49
            Visible = True
          end
          item
            Color = 16776176
            Expanded = False
            FieldName = 'AVG_W'
            Title.Caption = #1057#1088'.'#1074#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = 16776176
            Expanded = False
            FieldName = 'AVG_SPR'
            Title.Caption = #1054#1088#1080#1077#1085#1090'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
  end
  object fmstrSItemCDM: TFormStorage
    StoredValues = <>
    Left = 144
    Top = 88
  end
  object pmPrint: TPopupMenu
    Left = 296
    Top = 132
    object mnitPrList: TMenuItem
      Tag = 3
      Caption = #1047#1072#1103#1074#1082#1072
      OnClick = siPrintClick
    end
    object N1: TMenuItem
      Caption = #1058#1077#1082#1089#1090
      OnClick = N1Click
    end
  end
end
