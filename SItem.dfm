object fmSItem: TfmSItem
  Left = 312
  Top = 270
  HelpContext = 100203
  Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
  ClientHeight = 499
  ClientWidth = 690
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poMainFormCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object sp1: TSplitter
    Left = 0
    Top = 319
    Width = 690
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    Visible = False
    ExplicitTop = 296
    ExplicitWidth = 613
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 690
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = sbAddSItemClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = siDelSItemClick
      SectionName = 'Untitled (0)'
    end
    object spitTag: TSpeedItem
      BtnCaption = #1041#1080#1088#1082#1080
      Caption = #1041#1080#1088#1082#1080
      DropDownMenu = ppPrint
      Hint = #1041#1080#1088#1082#1080'|'
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = spitTagClick
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 538
      Top = 2
      Visible = True
      OnClick = ToolButton3Click
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 474
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object sb1: TStatusBar
    Left = 0
    Top = 474
    Width = 690
    Height = 25
    Panels = <
      item
        Text = #1050#1086#1083'-'#1074#1086
        Width = 100
      end
      item
        Text = #1042#1077#1089
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object paItBuf: TPanel
    Left = 0
    Top = 323
    Width = 690
    Height = 151
    Align = alBottom
    Caption = 'paItBuf'
    TabOrder = 2
    Visible = False
    object tb2: TSpeedBar
      Left = 1
      Top = 1
      Width = 688
      Height = 29
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      BoundLines = [blTop, blBottom, blLeft, blRight]
      Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
      BtnOffsetHorz = 3
      BtnOffsetVert = 3
      BtnWidth = 24
      BtnHeight = 23
      Images = dmCom.ilButtons
      BevelOuter = bvNone
      BorderWidth = 1
      TabOrder = 0
      InternalVer = 1
      object SpeedbarSection1: TSpeedbarSection
        Caption = 'Untitled (0)'
      end
      object siInBuf: TSpeedItem
        Hint = #1059#1076#1072#1083#1080#1090#1100' '#1074' '#1073#1091#1092#1077#1088
        ImageIndex = 76
        Spacing = 1
        Left = 3
        Top = 3
        Visible = True
        OnClick = siInBufClick
        SectionName = 'Untitled (0)'
      end
      object siFromBuf: TSpeedItem
        Caption = 'siFromBuf'
        Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079' '#1073#1091#1092#1077#1088#1072
        ImageIndex = 75
        Spacing = 1
        Left = 27
        Top = 3
        Visible = True
        OnClick = siFromBufClick
        SectionName = 'Untitled (0)'
      end
      object SpeedItem1: TSpeedItem
        Caption = 'SpeedItem1'
        Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1073#1091#1092#1077#1088
        ImageIndex = 39
        Spacing = 1
        Left = 51
        Top = 3
        Visible = True
        OnClick = SpeedItem1Click
        SectionName = 'Untitled (0)'
      end
    end
    object dgItBuf: TM207IBGrid
      Left = 1
      Top = 30
      Width = 688
      Height = 120
      Align = alClient
      Color = clBtnFace
      DataSource = dm2.dsItBuf
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      IniStorage = fs1
      TitleButtons = True
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = True
      SortOnTitleClick = True
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRODCODE'
          Title.Alignment = taCenter
          Title.Caption = #1048#1079#1075'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 32
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'D_MATID'
          Title.Alignment = taCenter
          Title.Caption = #1052#1072#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 32
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'D_GOODID'
          Title.Alignment = taCenter
          Title.Caption = #1053#1072#1080#1084'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 36
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'D_INSID'
          Title.Alignment = taCenter
          Title.Caption = #1054#1042
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 28
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART'
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 51
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART2'
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'UID'
          Title.Alignment = taCenter
          Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 61
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'W'
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 31
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'SZ'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1079#1084#1077#1088
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 50
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'SPRICE'
          Title.Alignment = taCenter
          Title.Caption = #1055#1088'. '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRICE'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1089#1093'. '#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Visible = True
        end>
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 40
    Width = 690
    Height = 279
    Align = alClient
    Color = clBtnFace
    ColumnDefValues.AutoDropDown = True
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSItem
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FrozenCols = 2
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmSItem
    RowDetailPanel.Color = clBtnFace
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 2
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    OnKeyDown = dg1KeyDown
    OnKeyPress = dg1KeyPress
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        ReadOnly = True
        Title.Caption = '#'
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
        Width = 72
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1042#1077#1089
        Width = 63
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SS'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Width = 66
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'EUID'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1103
        Width = 191
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'WEIGHT$INSERTION'
        Footers = <>
        Title.Caption = #1042#1089#1090#1072#1074#1082#1080'||'#1042#1077#1089' '
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COST$INSERTION'
        Footers = <>
        Title.Caption = #1042#1089#1090#1072#1074#1082#1080'||'#1057#1090#1086#1080#1084#1086#1089#1090#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 396
    Top = 102
  end
  object pmSItem: TPopupMenu
    Images = dmCom.ilButtons
    Left = 34
    Top = 102
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 1
      ShortCut = 45
      OnClick = sbAddSItemClick
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelSItemClick
    end
    object N3: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1086#1076#1085#1086' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 52
      ShortCut = 116
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1085#1077#1089#1082#1086#1083#1100#1082#1086' '#1080#1079#1076#1077#1083#1080#1081
      ImageIndex = 51
      ShortCut = 117
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072' '#1087#1086' '#1088#1072#1079#1084#1077#1088#1072#1084
      ShortCut = 118
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object miBuf: TMenuItem
      Caption = #1041#1091#1092#1077#1088
      ShortCut = 16500
      OnClick = miBufClick
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1076#1086#1087'. '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
      ShortCut = 119
      OnClick = N8Click
    end
  end
  object ppPrint: TRxPopupMenu
    Style = msBtnLowered
    ShowCheckMarks = False
    Left = 104
    Top = 104
  end
end
