unit uApplication;

interface

uses

  Windows, SysUtils, Forms;

const

  GUID_NULL: TGUID = '{00000000-0000-0000-0000-000000000000}';

  BuyGUID: TGUID = '{29EF8792-6347-4331-9079-BFE1813D40D5}';
  SellGUID: TGUID = '{96518E63-F833-4838-A9F7-5FA7A6E117CB}';
  JewGUID: TGUID = '{1AEC8644-0E1E-4375-80DC-A48638A291C3}';



  function IsJew: Boolean;
  function IsBuy: Boolean;
  function IsSell: Boolean;

  procedure SetApplicationGUID(Value: TGUID);
  function GetApplicationGUID: TGUID;

  procedure SetApplicationUserID(Value: Integer);
  function GetApplicationUserID: Integer;

implementation

var
  UserPropertyAtom: ATOM;
  ApplicationPropertyAtom: ATOM;

  UserID: Integer;
  ApplicationGUID: TGUID;

const

  ApplicationPropertyGUID: TGUID = '{F47AE4EC-DC6E-45E6-B070-6B3A6B837117}';
  UserPropertyGUID: TGUID = '{4B034C6D-EA46-4939-8BD6-0C715BA94ACF}';

function IsJew: Boolean;
begin
  Result := IsEqualGUID(GetApplicationGUID, JewGUID);
end;

function IsBuy: Boolean;
begin
  Result := IsEqualGUID(GetApplicationGUID, BuyGUID);
end;

function IsSell: Boolean;
begin
  Result := IsEqualGUID(GetApplicationGUID, SellGUID);
end;

procedure SetApplicationGUID(Value: TGUID);
var
  PropertyName: string;
  PropertyValue: string;
begin
  if not IsLibrary then
  begin
    if Application.Handle <> 0 then
    begin
      if IsEqualGUID(ApplicationGUID, GUID_NULL) then
      begin
        PropertyName := GUIDToString(ApplicationPropertyGUID);
        PropertyValue := GUIDToString(Value);
        ApplicationPropertyAtom := GlobalAddAtom(PChar(PropertyValue));
        if ApplicationPropertyAtom <> 0 then
        begin
          if SetProp(Application.Handle, PChar(PropertyName), ApplicationPropertyAtom) then ApplicationGUID := Value
          else GlobalDeleteAtom(ApplicationPropertyAtom);
        end;
      end;
    end;
  end;
end;

procedure SetApplicationUserID(Value: Integer);
var
  PropertyName: string;
  PropertyValue: string;
begin
  if not IsLibrary then
  begin
    if Application.Handle <> 0 then
    begin
      if UserID = 0 then
      begin
        PropertyName := GUIDToString(UserPropertyGUID);
        PropertyValue := IntToStr(Value);
        UserPropertyAtom := GlobalAddAtom(PChar(PropertyValue));
        if UserPropertyAtom <> 0 then
        begin
          if SetProp(Application.Handle, PChar(PropertyName), UserPropertyAtom) then UserID := Value
          else GlobalDeleteAtom(UserPropertyAtom);
        end;
      end;
    end;
  end;
end;



function GetApplicationGUID: TGUID;
var
  PropertyAtom: ATOM;
  PropertyName: string;
  PropertyValue: string;
  PropertySize: Integer;
  Buffer: PChar;
begin
  if IsEqualGUID(ApplicationGUID, GUID_NULL) then
  begin
    PropertyName := GUIDToString(ApplicationPropertyGUID);
    PropertyAtom := GetProp(Application.Handle, PChar(PropertyName));
    if PropertyAtom <> 0 then
    begin
      PropertySize := Length(PropertyName) + 1;
      GetMem(Buffer, PropertySize);
      if GlobalGetAtomName(PropertyAtom, Buffer, PropertySize) <> 0 then
      begin
        PropertyValue := StrPas(Buffer);
        try
          Result := StringToGUID(PropertyValue);
        except
          Result := GUID_NULL;
        end;
      end
      else Result := GUID_NULL;
      FreeMem(Buffer);
    end
    else Result := GUID_NULL;
    ApplicationGUID := Result;
  end
  else Result := ApplicationGUID;
end;

function GetApplicationUserID: Integer;
var
  PropertyAtom: ATOM;
  PropertyName: string;
  PropertyValue: string;
  PropertySize: Integer;
  Buffer: PChar;
begin
  if UserID = 0 then
  begin
    PropertyName := GUIDToString(UserPropertyGUID);
    PropertyAtom := GetProp(Application.Handle, PChar(PropertyName));
    if PropertyAtom <> 0 then
    begin
      PropertySize := Length(PropertyName) + 1;
      GetMem(Buffer, PropertySize);
      if GlobalGetAtomName(PropertyAtom, Buffer, PropertySize) <> 0 then
      begin
        PropertyValue := StrPas(Buffer);
        try
          Result := StrToInt(PropertyValue);
        except
          Result := 0;
        end;
      end
      else Result := 0;
      FreeMem(Buffer);
    end
    else Result := 0;
    UserID := Result;
  end
  else Result := UserID;
end;


initialization

  UserID := 0;
  ApplicationGUID := GUID_NULL;

finalization

  if not IsLibrary then
  begin
    if not IsEqualGUID(ApplicationGUID, GUID_NULL) then
    begin
      RemoveProp(Application.Handle, PChar(GUIDToString(ApplicationPropertyGUID)));
      GlobalDeleteAtom(ApplicationPropertyAtom);
    end;

    if UserID <> 0 then
    begin
      RemoveProp(Application.Handle, PChar(GUIDToString(UserPropertyGUID)));
      GlobalDeleteAtom(UserPropertyAtom);
    end;

  end;

end.
