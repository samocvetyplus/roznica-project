unit Mat;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, RXSpin, Mask, rxSpeedbar;

type
  TfmMat = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    spitPrint: TSpeedItem;
    siSort: TSpeedItem;
    Panel1: TPanel;
    se: TRxSpinEdit;
    Button1: TButton;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure siSortClick(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmMat: TfmMat;

implementation

uses comdata, Sort, DBTree, Data, M207Proc;

{$R *.DFM}

procedure TfmMat.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmMat.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      dg1.ReadOnly:=not Centerdep;
      SpeedItem2.Enabled:=Centerdep;
      SpeedItem1.Enabled:=Centerdep;
      OpenDataSets([taMat, taGr]);
      R_Item:='D_MAT';
    end;
end;

procedure TfmMat.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      PostDataSets([taMat]);
      CloseDataSets([taMat, taGr]);
      dm.LoadArtSL(MAT_DICT);      
      tr.CommitRetaining;
      R_Item:='';
    end;
end;

procedure TfmMat.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmMat.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmMat.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taMat.Append;
end;

procedure TfmMat.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taMat.Delete;
end;

procedure TfmMat.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsMat, 32); //'mt'
end;

procedure TfmMat.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_MATID, NAME '+
                       'FROM D_MAT '+
                       'Where D_MATID<>'#39+'-1000'+#39' '+                       
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_MAT set SortInd=?SortInd where D_MatId=?D_MatId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taMat]);
            end
        end;
    finally
      fmSort.Free;
    end;

end;

procedure TfmMat.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var l: integer;  
begin
  case Key of
     VK_RIGHT:
        with dg1, InplaceEditor do
          begin
            l:=Length(Text);
            if (l=1) and (Text[1]='-') and
               ((SelStart=0) and (SelLength=l) or (SelStart=l) and (SelLength=0)) then
              if SelectedIndex<Columns.Count-1 then
                 begin
                   SelectedIndex:=SelectedIndex+1;
                   PostMessage(Handle, WM_KEYDOWN, VK_RETURN, 0);
                 end
              end;
          end;
end;

procedure TfmMat.Button1Click(Sender: TObject);
var d, i: integer;
    NewRow: integer;
    OldRow: integer;
begin
  with dg1, DataSource, DataSet do
    begin
      NewRow:=Round(se.Value);
      OldRow:=Row;
      if (OldRow>NewRow) then
        begin
          d:=VisibleRowCount-NewRow;
          i:=0;
          while (i<=d) and not EOF do
            begin
              Next;
              Inc(i);
            end;
          MoveBy(-i+1);
        end
      else
      if (OldRow<NewRow) then
        begin
          d:=NewRow-1;
          i:=0;
          while (i<d) and not BOF do
            begin
              Prior;
              Inc(i);
            end;
          MoveBy(i);
        end;
    end;
end;

procedure TfmMat.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100103)
end;

procedure TfmMat.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
