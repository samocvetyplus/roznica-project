object fmRespStoring: TfmRespStoring
  Left = 241
  Top = 106
  Width = 696
  Height = 480
  Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1093#1088#1072#1085#1077#1085#1080#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      ImageIndex = 0
      Spacing = 1
      Left = 626
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6000000C6C6C6000000FF00FFFF00FF000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00C6C6C6000000FF00FF000000C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF
        FFFFFFFFFFFFFFFFC6C6C6C6C6C6000000000000000000FF00FF000000C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C60000FF0000FF0000FFC6C6C6C6C6C60000
        00C6C6C6000000FF00FF00000000000000000000000000000000000000000000
        0000000000000000000000000000000000C6C6C6C6C6C6000000000000C6C6C6
        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6000000C6C6
        C6000000C6C6C6000000FF00FF00000000000000000000000000000000000000
        0000000000000000000000C6C6C6000000C6C6C6000000000000FF00FFFF00FF
        000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000C6C6
        C6000000C6C6C6000000FF00FFFF00FFFF00FF000000FFFFFFFF0000FF0000FF
        0000FF0000FF0000FFFFFF000000000000000000000000FF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFF0000FF
        0000FF0000FF0000FF0000FFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000000000
        0000000000000000000000000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1055#1077#1095#1072#1090#1100' '#1079#1072#1103#1074#1083#1077#1085#1080#1103' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
      ImageIndex = 67
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      Action = acHelp
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B393939FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B397B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B007B7B007B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD00393900
        7B7B007B7B007B7B003939848484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B7BFFFF7BFFFF7BFFFF003939397B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B007B7B9C9C9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFBDBDBD7BBDBD7BFFFF7BFFFF7BBDBD003939848484FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF397B7B7B
        FFFF7BFFFF7BFFFF7BBDBD6363639C9C9CFF00FFFF00FFFF00FFFF00FFFF00FF
        393939397B7B397B7B7B7B7BFF00FF397B7B7BFFFF7BFFFF7BFFFF0039396363
        639C9C9CFF00FFFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7B7B7BFF00FFFF
        00FF397B7B7BFFFF7BFFFF397B7B007B7B848484FF00FFFF00FFFF00FFFF00FF
        397B7B7BFFFF7BFFFF397B7BFF00FFBDBDBD39BDBD7BFFFF7BFFFF397B7B00FF
        FF424242BDBDBDFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7BFFFF397B7B39
        BDBD7BFFFF7BFFFF7BFFFF397B7B00FFFF424242BDBDBDFF00FFFF00FFFF00FF
        BDBDBD7BBDBD7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF39BDBD00BDBD00FF
        FF424242BDBDBDFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B7BFFFF7BFFFF7B
        FFFF7BFFFF397B7B00BDBD00FFFF007B7B848484FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFBDBDBD003939007B7B007B7B007B7B00FFFF00FFFF00BDBD6363
        63DEDEDEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B00
        7B7B007B7B007B7B215A5A7B7B7BDEDEDEFF00FFFF00FFFF00FF}
      ImageIndex = 73
      Spacing = 1
      Left = 562
      Top = 2
      Visible = True
      OnClick = acHelpExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = 'siDel'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siRespStoring: TSpeedItem
      Action = acRespStoring
      BtnCaption = #1057#1085#1103#1090#1100' '#1089#13#10#1086#1090#1074'. '#1093#1088'.'
      Caption = 'siRespStoring'
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acRespStoringExecute
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 688
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = pmDep
      Hint = #1042#1099#1073#1086#1088' '#1080#1089#1093#1086#1076#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 306
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb3: TSpeedBar
    Left = 0
    Top = 69
    Width = 688
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 2
    InternalVer = 1
    object lbFindUID: TLabel
      Left = 8
      Top = 8
      Width = 105
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1080#1076'. '#1085#1086#1084#1077#1088#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbFindArt: TLabel
      Left = 232
      Top = 8
      Width = 70
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1072#1088#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edFindUID: TEdit
      Left = 120
      Top = 4
      Width = 97
      Height = 21
      TabOrder = 0
      OnKeyDown = edFindUIDKeyDown
      OnKeyPress = edFindUIDKeyPress
    end
    object edFindArt: TEdit
      Left = 304
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 1
      OnKeyDown = edFindArtKeyDown
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 98
    Width = 688
    Height = 353
    Align = alClient
    AllowedOperations = []
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsRespStoring
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 5
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RN'
        Footers = <>
        Title.Caption = #8470' '#1057#1084#1077#1085#1099
      end
      item
        EditButtons = <>
        FieldName = 'ADATE'
        Footers = <
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            Value = #1054#1090#1074#1077#1090'. '#1093#1088#1072#1085#1077#1085#1080#1077
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            Value = #1056#1077#1084#1086#1085#1090
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            Value = #1042#1077#1088#1085#1091#1083#1086#1089#1100' '#1080#1079' '#1088#1077#1084#1086#1085#1090#1072
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsItalic]
            Value = #1057#1085#1103#1090#1086' '#1089' '#1086#1090#1074'. '#1093#1088#1072#1085#1077#1085#1080#1103
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1042#1089#1077#1075#1086
            ValueType = fvtStaticText
          end>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1040#1088#1090#1080#1082#1091#1083
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1048#1079#1075'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1053#1072#1080#1084'. '#1080#1079#1076'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1054#1042
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1052#1072#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'D_COUNTRYID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1057#1090#1088'.'
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083'2'
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'UID'
        Footers = <
          item
            FieldName = 'SQ0'
            ValueType = fvtSum
          end
          item
            FieldName = 'SQ1'
            ValueType = fvtSum
          end
          item
            FieldName = 'SQ2'
            ValueType = fvtSum
          end
          item
            FieldName = 'SQ3'
            ValueType = fvtSum
          end
          item
            FieldName = 'UID'
            ValueType = fvtCount
          end>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1074#1077#1094
        Width = 197
      end
      item
        EditButtons = <>
        FieldName = 'RETCLIENT'
        Footers = <>
        Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Width = 153
      end
      item
        EditButtons = <>
        FieldName = 'RETADDRESS'
        Footers = <>
        Title.Caption = #1040#1076#1088#1077#1089' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        Width = 286
      end
      item
        EditButtons = <>
        FieldName = 'RET'
        Footers = <>
        Title.Caption = #1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
        Width = 146
      end
      item
        EditButtons = <>
        FieldName = 'DATERESP'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1077#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        Title.Caption = #1060#1080#1083#1080#1072#1083
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <
          item
            FieldName = 'SW0'
            ValueType = fvtSum
          end
          item
            FieldName = 'SW1'
            ValueType = fvtSum
          end
          item
            FieldName = 'SW2'
            ValueType = fvtSum
          end
          item
            FieldName = 'SW3'
            ValueType = fvtSum
          end
          item
            FieldName = 'W'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1088#1077#1072#1083'.'
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1088#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <
          item
            FieldName = 'SCOST0'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST1'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST2'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST3'
            ValueType = fvtSum
          end
          item
            FieldName = 'COST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' ('#1088#1077#1072#1083'.)'
      end
      item
        EditButtons = <>
        FieldName = 'COST0'
        Footers = <
          item
            FieldName = 'SCOST00'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST01'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST02'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST03'
            ValueType = fvtSum
          end
          item
            FieldName = 'COST0'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' ('#1088#1072#1089#1093'.)'
      end
      item
        EditButtons = <>
        FieldName = 'Sate'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1090#1085#1080#1077
        Width = 170
      end
      item
        EditButtons = <>
        FieldName = 'DATENRESP'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1089#1085#1103#1090#1080#1103' '#1089' '#1086#1090#1074'. '#1093#1088#1072#1085#1077#1085#1080#1103
        Width = 97
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 104
    Top = 168
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 600
    Top = 152
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      ShortCut = 27
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100' '#1079#1072#1103#1074#1083#1077#1085#1080#1103' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
      ImageIndex = 67
      OnExecute = acPrintExecute
      OnUpdate = acPrintUpdate
    end
    object acHelp: TAction
      Caption = #1057#1087#1088#1072#1074#1082#1072
      ImageIndex = 73
      OnExecute = acHelpExecute
    end
    object acPrintGrid: TAction
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
      OnUpdate = acPrintGridUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acRespStoring: TAction
      Caption = #1057#1085#1103#1090#1100' '#1089' '#1086#1090#1074'. '#1093#1088'.'
      OnExecute = acRespStoringExecute
      OnUpdate = acRespStoringUpdate
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 264
    Top = 232
  end
  object pmDep: TTBPopupMenu
    Left = 384
    Top = 192
  end
end
