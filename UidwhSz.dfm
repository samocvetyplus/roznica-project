object fmUidwhSz: TfmUidwhSz
  Left = 308
  Top = 230
  Width = 329
  Height = 92
  AutoSize = True
  BorderIcons = []
  Caption = #1056#1072#1079#1084#1077#1088
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 123
    Height = 16
    Caption = #1059#1082#1072#1078#1080#1090#1077' '#1088#1072#1079#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edSz: TEdit
    Left = 136
    Top = 0
    Width = 185
    Height = 21
    TabOrder = 0
  end
  object btOk: TBitBtn
    Left = 40
    Top = 40
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object btCancel: TBitBtn
    Left = 200
    Top = 40
    Width = 75
    Height = 25
    TabOrder = 2
    Kind = bkCancel
  end
end
