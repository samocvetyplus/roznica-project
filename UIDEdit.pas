unit UIDEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Buttons, RXDBCtrl, db, dbUtil;

type
  TfmUIDEdit = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    BitBtn3: TBitBtn;
    Label12: TLabel;
    Label13: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    BitBtn4: TBitBtn;
    Label14: TLabel;
    DBLookupComboBox3: TDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUIDEdit: TfmUIDEdit;

implementation

uses Data, M207Proc, ArtDict, NewArt, Data2, goods_sam, comdata, ServData;

{$R *.DFM}

procedure TfmUIDEdit.FormCreate(Sender: TObject);
var i: integer;
begin
  with dm, dm2 do
  begin
    OpenDataSets([dmCom.taGoodsSam, quUnitId]);
    with quSItemIType do
    begin
      Active:=False;
      Params[0].AsInteger:=quUIDWHSItemId.AsInteger;
      Open;
      i:=Fields[0].AsInteger;
      Close;
    end;
    SetCBDropDown(DBLookupComboBox1);
    SetCBDropDown(DBLookupComboBox2);
    SetCBDropDown(DBLookupComboBox3);
    Caption:='�������������� ������� '+ quUIDWHUID.AsString;
  end;
end;


procedure TfmUIDEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  close;
end;

procedure TfmUIDEdit.BitBtn3Click(Sender: TObject);

begin
  dmServ.SearchArt := dm.quUIDWHArt2Id.AsString;
  if ShowAndFreeForm(TfmNewArt, Self, TForm(fmNewArt), True, False)=mrOK then
    with dm do
      if Art2Id<>-1 then
        begin
          quGetArt.ACtive:=False;
          quGetArt.Params[0].AsInteger:=Art2Id;
          quGetArt.ACtive:=True;

          if quUIDWH.State<>dsEdit then quUIDWH.Edit;
          quUIDWHArt2Id.AsInteger:=Art2Id;
          quUIDWHProdCode.AsString:=quGetArtProdCode.AsString;
          quUIDWHMatId.AsString:=quGetArtD_MatId.AsString;
          quUIDWHGoodId.AsString:=quGetArtD_GoodId.AsString;
          quUIDWHInsId.AsString:=quGetArtD_InsId.AsString;
          quUIDWHArt.AsString:=quGetArtArt.AsString;
          quUIDWHArt2.AsString:=quGetArtArt2.AsString;
          quUIDWHUnitId.AsInteger:=quGetArtUnitId.AsInteger;
          quGetArt.Active:=False;

          with dm2, quUIDPrice, Params do
            begin
              Active:=False;
              ByName['ART2ID'].AsInteger:=Art2Id;
              ByName['DEPID'].AsInteger:=quUIDWHDepId.AsInteger;
              Active:=True;
              if Fields[0].AsFloat>1e-2 then quUIDWHPrice.AsFloat:=Fields[0].AsFloat;
              Active:=False;
            end;
        end;
end;

procedure TfmUIDEdit.BitBtn4Click(Sender: TObject);
var b:boolean;
begin
 b:=GetBit(dmCom.EditRefBook, 2) and CenterDep;
 ShowAndFreeFormEnabled(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False, b, 'siExit;tb1;spitPrint;siSort;','dg1;');
 OpenDataSets([dmCom.taGoodsSam]);
end;

procedure TfmUIDEdit.BitBtn1Click(Sender: TObject);
var
  r, p1, p2: variant;

  Function GetUNIT(UINTID:variant):string;
  begin
    if UINTID=1 then result:='1'
    else result:='0'
  end;

begin
  with dm, quADict, quUIDWH do
  begin
   OpenDataSets([dm.quUIDWH, dm.quADict]);
    r := ExecSelectSQL('select unitid from sitem si, art2 a2 where a2.art2id=si.art2id and si.sitemid='+
                        quUIDWHSITEMID.AsString, dmCom.quTmp);
    p1:= ExecSelectSQL('select goodsid1 from uidwh_t u where u.sitemid='+
                        quUIDWHSITEMID.AsString, dmCom.quTmp); //������ ����������1
    p2:= ExecSelectSQL('select goodsid2 from uidwh_t u where u.sitemid='+
                        quUIDWHSITEMID.AsString, dmCom.quTmp); //������ ����������2
//�����: ���� ����������� ����� ��������, �� ��������� update                        
    if r <> DBLookupComboBox3.KeyValue then
    begin
      ExecSQL('update d_art set unitid='+ GetUNIT(DBLookupComboBox3.KeyValue)+
              'where d_artid='+quUIDWHD_ARTID.AsString, dm.qutmp);
    end;
      if p1 <> DBLookupComboBox1.KeyValue then
    begin
      ExecSQL('update uidwh_t set goodsid1='+ GetUNIT(DBLookupComboBox1.KeyValue)+
              'where d_artid='+quUIDWHD_ARTID.AsString, dm.qutmp);
    end;
      if p2 <> DBLookupComboBox2.KeyValue then
    begin
      ExecSQL('update uidwh_t set goodsid2='+ GetUNIT(DBLookupComboBox2.KeyValue)+
              'where d_artid='+quUIDWHD_ARTID.AsString, dm.qutmp);
      end;
    dmcom.tr.CommitRetaining;
    dmcom.tr1.CommitRetaining;
    ReOpenDataSet(quUIDWH);
  end;

end;

end.
