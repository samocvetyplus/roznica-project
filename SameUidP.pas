unit SameUIDP;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  DBGridEh;

type
  TfmSameUIDP = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dg1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSameUIDP: TfmSameUIDP;

implementation

uses Data2, Data, M207Proc;

{$R *.DFM}

procedure TfmSameUIDP.FormCreate(Sender: TObject);
begin
  with dm2 do
    OpenDataSets([quSameUIDP]);
end;

procedure TfmSameUIDP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dm2 do
    begin
      UID:=quSameUIDPUID.AsInteger;
      CloseDataSets([quSameUIDP]);
    end;  
end;

end.
