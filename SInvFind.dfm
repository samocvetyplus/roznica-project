object fmSInvFind: TfmSInvFind
  Left = 357
  Top = 154
  BorderStyle = bsDialog
  Caption = #1055#1086#1080#1089#1082' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1086#1089#1090#1072#1074#1082#1080
  ClientHeight = 167
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 56
    Height = 13
    Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 28
    Width = 83
    Height = 13
    Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 50
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 72
    Width = 31
    Height = 13
    Caption = #1057#1082#1083#1072#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 94
    Width = 24
    Height = 13
    Caption = #1057'.'#1060'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 118
    Width = 49
    Height = 13
    Caption = #1044#1072#1090#1072' '#1089'.'#1092'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 140
    Width = 55
    Height = 13
    Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edN: TEdit
    Left = 94
    Top = 2
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 0
    Text = 'edN'
  end
  object deNDate: TDateEdit
    Left = 94
    Top = 24
    Width = 109
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 1
  end
  object edSF: TEdit
    Left = 94
    Top = 90
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 2
    Text = 'edN'
  end
  object edCost: TEdit
    Left = 94
    Top = 134
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 3
    Text = 'edN'
  end
  object deSFDate: TDateEdit
    Left = 94
    Top = 112
    Width = 109
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 4
  end
  object lcSup: TDBLookupComboBox
    Left = 94
    Top = 46
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_COMPID'
    ListField = 'NAME'
    ListSource = dm.dsSup
    TabOrder = 5
    OnKeyDown = lcSupKeyDown
  end
  object lcDep: TDBLookupComboBox
    Left = 94
    Top = 68
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_DEPID'
    ListField = 'NAME'
    ListSource = dmCom.dsDep
    TabOrder = 6
    OnKeyDown = lcSupKeyDown
  end
  object bbFind: TBitBtn
    Tag = 1
    Left = 220
    Top = 8
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080
    TabOrder = 7
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF6000F000050FFFFFFFFFFFF6660FF000050FFF000000FF6666FFF
      000050FF000000000666FFFF000050F00EEEEEE0066FFFFF00005500EEEEEEEE
      00FFFFFF0000500E99E99EEEE00F999F0000500E99999EE9E00F99F90000500E
      99E99E99999F999F0000500E99E99EE9E00F99F90000500EE999EEEEE00F999F
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object bbFindNext: TBitBtn
    Tag = 2
    Left = 220
    Top = 36
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 8
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF2200F000050FFFFFFFFFFFF2222FF000050FFF000000FF2222FFF
      000050FF000000000222FFFF000050F00EEEEEE0022FFFFF00005500EEEEEEEE
      00FF00FF0000500E0E0E000EE00FFFFF0000500E000E000EE00FFFFF0000500E
      0E0E00EEE00FFFFF0000500EE0EE000EE00FFFFF0000500EEEEEEEEEE00FFFFF
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object BitBtn3: TBitBtn
    Left = 220
    Top = 64
    Width = 75
    Height = 25
    TabOrder = 9
    Kind = bkOK
  end
end
