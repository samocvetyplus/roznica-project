object fmShopReport: TfmShopReport
  Left = 284
  Top = 166
  HelpContext = 100431
  Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090
  ClientHeight = 447
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      ImageIndex = 0
      Spacing = 1
      Left = 618
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C000018631863186318631863186318631863186300001863
        00001F7C1F7C0000000000000000000000000000000000000000000000000000
        186300001F7C0000186318631863186318631863FF7FFF7FFF7F186318630000
        000000001F7C0000186318631863186318631863007C007C007C186318630000
        186300001F7C0000000000000000000000000000000000000000000000000000
        1863186300000000186318631863186318631863186318631863186300001863
        0000186300001F7C000000000000000000000000000000000000000018630000
        1863000000001F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001863
        0000186300001F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F00000000
        000000001F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 554
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridShopReport: TDBGridEh
    Left = 0
    Top = 40
    Width = 688
    Height = 388
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsrShopReport
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghColumnResize, dghColumnMove]
    PopupMenu = pmgr
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridShopReportGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'TNAME'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Footers = <>
        Title.Caption = ' '
        Width = 144
      end
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.Caption = #8470#1042#1055
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        MaxWidth = 110
        MinWidth = 30
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'COST2'
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072' '#1074' '#1088#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'BUHREP'
        Footers = <>
        Title.Caption = #1054#1090#1084#1077#1090#1082#1072' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1080#1080
        Width = 114
      end
      item
        EditButtons = <>
        FieldName = 'COST3'
        Footers = <>
        Title.Caption = #1090#1088#1072#1085#1089#1087' . '#1088#1072#1089#1093'.'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object stbrMain: TStatusBar
    Left = 0
    Top = 428
    Width = 688
    Height = 19
    Panels = <>
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 44
    Top = 68
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      OnExecute = acPrintExecute
    end
    object acExport: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090
    end
    object acSet: TAction
      Caption = #1053#1077#1090' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1074' '#1085#1072#1083#1080#1095#1080#1080
      OnExecute = acSetExecute
      OnUpdate = acNotSetUpdate
    end
    object acNotSet: TAction
      Caption = #1057#1085#1103#1090#1100' '#1086#1090#1084#1077#1090#1082#1091
      OnExecute = acNotSetExecute
      OnUpdate = acNotSetUpdate
    end
    object acPrintGrid: TAction
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
  end
  object taShopReport: TpFIBDataSet
    UpdateSQL.Strings = (
      'update shopreport'
      'set buhrep=:buhrep,'
      '    color=:color'
      'where id=:id')
    RefreshSQL.Strings = (
      
        'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDO' +
        'C, cost2, cost3, buhrep, depsel, color, sn'
      'from ShopReport_R(:id, :t, :sinvid, :depsel)')
    SelectSQL.Strings = (
      
        'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDO' +
        'C, cost2, cost3, buhrep, depsel, color, sn'
      'from ShopReport_S(:SINVID)')
    AfterPost = taShopReportAfterPost
    BeforeOpen = taShopReportBeforeOpen
    AllowedUpdateKinds = [ukModify]
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 104
    Top = 69
    poSQLINT64ToBCD = True
    object taShopReportID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taShopReportSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taShopReportT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taShopReportDOCNO: TFIBStringField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object taShopReportDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taShopReportQ: TFIBFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taShopReportW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taShopReportCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taShopReportK: TFIBSmallIntField
      FieldName = 'K'
    end
    object taShopReportTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taShopReportERRDOC: TFIBSmallIntField
      FieldName = 'ERRDOC'
    end
    object taShopReportCOST2: TFIBFloatField
      FieldName = 'COST2'
      currency = True
    end
    object taShopReportCOST3: TFIBFloatField
      FieldName = 'COST3'
      currency = True
    end
    object taShopReportBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
    object taShopReportDEPSEL: TFIBIntegerField
      FieldName = 'DEPSEL'
    end
    object taShopReportCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taShopReportSN: TFIBIntegerField
      FieldName = 'SN'
    end
  end
  object dsrShopReport: TDataSource
    DataSet = taShopReport
    Left = 104
    Top = 120
  end
  object fms: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 44
    Top = 116
  end
  object pmgr: TTBPopupMenu
    Left = 264
    Top = 160
    object biSet: TTBItem
      Action = acSet
    end
    object biNoSet: TTBItem
      Action = acNotSet
    end
  end
  object prgrid: TPrintDBGridEh
    DBGridEh = gridShopReport
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 392
    Top = 120
  end
  object pmPrint: TPopupMenu
    Left = 56
    Top = 32
    object N1: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100' ('#1050#1086#1084#1080#1089#1089#1080#1103')'
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100' ('#1058#1086#1083#1083#1080#1085#1075')'
      OnClick = N3Click
    end
  end
end
