object fmSelectUidWh: TfmSelectUidWh
  Left = 576
  Top = 247
  AutoSize = True
  BorderIcons = []
  Caption = #1042#1099#1073#1086#1088' '#1088#1072#1089#1095#1077#1090#1072' '#1089#1082#1083#1072#1076#1072
  ClientHeight = 121
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btAnyPeriod: TButton
    Tag = 2
    Left = 0
    Top = 0
    Width = 257
    Height = 41
    Caption = #1057#1050#1051#1040#1044' '#1053#1040' '#1055#1056#1054#1048#1047#1042#1054#1051#1068#1053#1067#1049' '#1055#1045#1056#1048#1054#1044
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = acEnterExecute
  end
  object btBeginMonth: TButton
    Tag = 1
    Left = 0
    Top = 40
    Width = 257
    Height = 41
    Caption = #1057#1050#1051#1040#1044' '#1053#1040' '#1053#1040#1063#1040#1051#1054' '#1052#1045#1057#1071#1062#1040
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = acEnterExecute
  end
  object btAnyDate: TButton
    Tag = 4
    Left = 0
    Top = 80
    Width = 257
    Height = 41
    Caption = #1057#1050#1051#1040#1044' '#1053#1040' '#1044#1040#1058#1059
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = acEnterExecute
  end
  object acList: TActionList
    Left = 120
    Top = 64
    object acEsc: TAction
      Caption = 'Esc'
      ShortCut = 27
      OnExecute = acEscExecute
    end
    object acEnter: TAction
      Caption = 'acEnter'
      ShortCut = 13
      OnExecute = acEnterExecute
    end
  end
end
