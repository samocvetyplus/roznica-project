unit DepAndDate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, FIBQuery, pFIBQuery, Buttons;

type
  TfmDepAndDate = class(TForm)
    cbDep: TDBComboBox;
    qDep: TpFIBQuery;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure cbDepDropDown(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDepAndDate: TfmDepAndDate;

implementation

{$R *.dfm}

uses ComData, data;

procedure TfmDepAndDate.BitBtn1Click(Sender: TObject);
begin
 dm.SDep:=cbDep.Items[cbDep.ItemIndex];
 dm.qutmp.Close;
 dm.qutmp.SQL.Text:='select d_depid from d_dep where name = ''' + trim(dm.SDep)+'''';
 dm.qutmp.ExecQuery;
 dm.SDepId:=dm.qutmp.Fields[0].AsInteger;
end;

procedure TfmDepAndDate.cbDepDropDown(Sender: TObject);
begin
  cbdep.Clear;
  qDep.ExecQuery;
  while not qDep.eof do begin
   cbDep.items.add(qDep.fieldbyname('NAME').asstring);
   qDep.next;
  end;
end;

end.
