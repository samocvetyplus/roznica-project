unit Hist;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmHist = class(TForm)
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    DataSource1: TDataSource;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    Label9: TLabel;
    dg1: TDBGridEh;
    Button1: TButton;
    OpSBtn: TBitBtn;
    CnlSBtn: TBitBtn;
    LogEd: TComboBox;
    procedure OpSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
   

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmHist: TfmHist;
  var log,s:string;
  Check_Detal: boolean;
  procedure History(var user, st: string);
implementation

   uses comdata, data, DList, MsgDialog, DInvItem, M207IBLogin, SInv,
  SellItem, SellList, DInv;

   {$R *.dfm}
procedure History(var user, st: string); //���������� ������ � ������� INVHIST
begin

with  fmHist.taHist do
begin
     fmHist.taHist.Append;
     fmHist.taHistFIO.Value := user;
     fmHist.taHistSTATUS.Value := st;
     fmHist.taHist.Post;
     fmHist.taHist.Transaction.CommitRetaining;
end;

end;



procedure TfmHist.OpSBtnClick(Sender: TObject);
 var i: integer; //�������� ���������
begin
 taEmp.Active:=true;
  with LogEd, Items do
              begin
                 i:=IndexOf(LogEd.Text);
                 if i=-1 then
                    Insert(0, LogEd.Text)
                    else Move(i, 0);
              end;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin

   log:=LogEd.Text;
   s:='�������';
   History(log, s);
   if DList.WMode='DLIST' then fmDlist.acOpen.Execute;
   If DinvItem.WMode='DINVITEM' then   fmDInvItem.acOpen.Execute;
   if DInv.WMode='DINV' then fmDInv.acOpen.Execute;
   if Sinv.WMode='SINV' then   fmSinv.acOpen.Execute;
   if SellList.WMode='SELLLIST' then   fmSellList.acOpen.Execute;
   If SellItem.WMode='SELLITEM' then  fmSellItem.acOpenSell.Execute;
  end;
     taEmp.Active:=false;
     close;
end;

procedure TfmHist.CnlSBtnClick(Sender: TObject);  //������
begin
 fmHist.taHist.Close;
 fmHist.taHist.Active:=false;
 close;
end;

procedure TfmHist.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  //��
  if (Dlist.WMode='DLIST') or (DinvItem.WMode='DINVITEM') or (DInv.WMode='DINV') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taDListSINVID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 2;
  end;
  //��������
   if (Sinv.WMode='SINV') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taSListSINVID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 1;
  end;
  //������� � �����
     if (SellItem.WMode='SELLITEM') or (SellList.WMode='SELLLIST') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taSellListSELLID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 8;
  end;
end;

procedure TfmHist.FormCreate(Sender: TObject);
begin
  // �� ��������� ������� ������
  dg1.Visible:=False;
  dg1.Enabled:=False;
  Check_Detal:=False;
end;

procedure TfmHist.FormActivate(Sender: TObject);
begin
 fmHist.taHist.Active := True;
 fmHist.taHist.Open;
 LogEd.Color:=clHighlightText;  //LogEd.Text:=dmCom.UserName;
  with LogEd do
    Text:=Items[0];
  Pswed.SetFocus;
  if (DList.WMode='DLIST') or (DinvItem.WMode='DINVITEM') or (DInv.WMode='DINV') then Caption:='�'+(dm.taDListSN.AsString);
   if (Sinv.WMode='SINV') then Caption:='�'+(dm.taSListSN.AsString);
   if (SellItem.WMode='SELLITEM')or (SellList.WMode='SELLLIST') then Caption:='�'+(dm.taSellListRN.AsString);
 end;


procedure TfmHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 fmHist.taHist.Close;
 fmHist.taHist.Active := false;
end;



procedure TfmHist.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmHist.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmHist.Button1Click(Sender: TObject);
begin
if not Check_Detal then // �������� �����������
  begin
     dg1.Enabled:=True;
     dg1.Visible:=True;  // ������������� ������
     Button1.Caption:='������� ��������� <<';
     Check_Detal:=True;
     dg1.DataSource.DataSet.Last; //��������� ���������
  end
  else // ��������� �����������
  begin
     dg1.Enabled:=False;
     dg1.Visible:=False;   // ������������� ������
     Button1.Caption:='������� ��������� >>';
     Check_Detal:=False;
  end
end;

end.
