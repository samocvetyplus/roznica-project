unit SameUid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid;

type
  TfmSameUID = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    M207IBGrid1: TM207IBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSameUID: TfmSameUID;

implementation

uses Data2, Data, M207Proc;

{$R *.DFM}

procedure TfmSameUID.FormCreate(Sender: TObject);
begin
  with dm2 do
    OpenDataSets([quSameUID]);
end;

procedure TfmSameUID.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dm2 do
    begin
      UID:=quSameUIDUID.AsInteger;
      CloseDataSets([quSameUID]);
    end;  
end;

end.
