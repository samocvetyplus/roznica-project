object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 702
  Width = 959
  object taSList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SINV'
      'SET SUPID=?SUPID,'
      '    DEPID=?DEPID,'
      '    SDATE=?SDATE,'
      '    NDATE=?NDATE,'
      '    SSF=?SSF,'
      '    SN=?SN,'
      '    PMARGIN=?PMARGIN,'
      '    TR=?TR,'
      '    AKCIZ=?AKCIZ,'
      '    ISCLOSED=?ISCLOSED,'
      '    NDSID=?NDSID,'
      '    NDS=?NDS,'
      '    PAYTYPEID=?PAYTYPEID,'
      '    PTR=?PTR,'
      '   TRNDS=?TRNDS,'
      '   ISIMPORT=?ISIMPORT,'
      '   PTRNDS= ?PTRNDS,'
      '   USERID=?USERID,'
      '   OPTMARGIN=?OPTMARGIN,'
      '   OPTRNORM=?OPTRNORM,'
      '   NOTPR= ?NOTPR,'
      '   edittr= ?edittr,'
      '   comission$scheme =?comission$scheme,'
      '   add$nds$to$price = ?add$nds$to$price,'
      '   is$tolling = ?is$tolling,'
      '   contract$id = ?contract$id'
      'WHERE SINVID=?OLD_SINVID')
    DeleteSQL.Strings = (
      'DELETE FROM SINV'
      'WHERE SINVID=?OLD_SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      '(SINVID, SUPID, DEPID, SDATE, NDATE, SSF, SN,  PMARGIN, '
      
        'TR,  AKCIZ, ISCLOSED, ITYPE, NDSID, NDS, PAYTYPEID, PTR, TRNDS, ' +
        'PTRNDS, USERID,NotPR, edittr, comission$scheme, is$tolling, add$' +
        'nds$to$price)'
      'VALUES'
      '(?SINVID, ?SUPID, ?DEPID, ?SDATE, ?NDATE, ?SSF, ?SN,  '
      
        '?PMARGIN, ?TR,  ?AKCIZ, ?ISCLOSED, ?itype, ?NDSID, ?NDS, ?PAYTYP' +
        'EID, ?PTR, ?TRNDS, ?PTRNDS, :USERID,0, 0,:comission$scheme, :is$' +
        'tolling, :add$nds$to$price)'
      '')
    RefreshSQL.Strings = (
      'SELECT SINVID, SUPID, DEPID, SDATE, NDATE, SSF, SN,'
      
        '               PMARGIN, NDS, TR, COST, AKCIZ, ISCLOSED, SUP,  DE' +
        'P,'
      
        '               NDSID, PAYTYPEID, TW, TQ, PTR, PNDS, TRNDS, ISIMP' +
        'ORT, USERID, FIO, PTRNDS, OPTMARGIN,'
      
        '              OPTRNORM, COST2,  NotPr, namepaytype, edittr, ityp' +
        'e, cltid, comission$scheme, add$nds$to$price, is$tolling, contra' +
        'ct$id, contract$class$id, contract$class$name'
      'FROM SLIST_R2(?SINVID)')
    SelectSQL.Strings = (
      'SELECT SINVID, SUPID, DEPID, SDATE, NDATE, SSF, SN,'
      
        '               PMARGIN, NDS, TR, COST, AKCIZ, ISCLOSED, SUP,  DE' +
        'P,'
      
        '               NDSID, PAYTYPEID, TW, TQ, PTR, PNDS, TRNDS, ISIMP' +
        'ORT, USERID, FIO, PTRNDS, OPTMARGIN, OPTRNORM, '
      
        'COST2 , NotPr, namepaytype, edittr, itype, cltid, color, comissi' +
        'on$scheme, Curr_color, add$nds$to$price, is$tolling, contract$id' +
        ', contract$class$id, contract$class$name, contract$number, contr' +
        'act$date'
      ' '
      'FROM SLIST_S2(?DEPID1, ?DEPID2, ?BD, ?ED)'
      'ORDER BY SN DESC, NDATE ')
    AfterCancel = quA2DictAfterPost
    AfterDelete = taSListAfterDelete
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = BeforeDelete
    BeforeEdit = taSListBeforeEdit
    BeforeOpen = taSListBeforeOpen
    BeforePost = taSListBeforePost
    OnCalcFields = taSListCalcFields
    OnNewRecord = taSListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 10
    Top = 188
    oPersistentSorting = True
    object taSListSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taSListSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taSListDEPID: TIntegerField
      FieldName = 'DEPID'
      OnChange = taSListDEPIDChange
    end
    object taSListSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taSListNDATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'NDATE'
    end
    object taSListSSF: TFIBStringField
      DisplayWidth = 400
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taSListPMARGIN: TFloatField
      FieldName = 'PMARGIN'
      DisplayFormat = '#0.00%'
    end
    object taSListNDS: TFloatField
      FieldName = 'NDS'
      currency = True
    end
    object taSListTR: TFloatField
      DisplayWidth = 20
      FieldName = 'TR'
      currency = True
    end
    object taSListCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taSListTOTALCOST: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALCOST'
      currency = True
      Calculated = True
    end
    object taSListSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object taSListAKCIZ: TFloatField
      FieldName = 'AKCIZ'
    end
    object taSListISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taSListDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taSListSN: TIntegerField
      FieldName = 'SN'
    end
    object taSListNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taSListPAYTYPEID: TIntegerField
      FieldName = 'PAYTYPEID'
    end
    object taSListTW: TFloatField
      FieldName = 'TW'
      OnChange = taSListTWChange
      DisplayFormat = '0.##'
    end
    object taSListTQ: TSmallintField
      FieldName = 'TQ'
    end
    object taSListPTR: TFloatField
      FieldName = 'PTR'
    end
    object taSListPNDS: TFloatField
      FieldName = 'PNDS'
    end
    object taSListTRNDS: TFloatField
      FieldName = 'TRNDS'
      currency = True
    end
    object taSListISIMPORT: TSmallintField
      FieldName = 'ISIMPORT'
    end
    object taSListUSERID: TIntegerField
      FieldName = 'USERID'
    end
    object taSListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSListPTRNDS: TFloatField
      FieldName = 'PTRNDS'
    end
    object taSListOPTMARGIN: TFloatField
      FieldName = 'OPTMARGIN'
      Origin = 'SLIST_S.OPTMARGIN'
    end
    object taSListOPTRNORM: TSmallintField
      FieldName = 'OPTRNORM'
      Origin = 'SLIST_S.OPTRNORM'
    end
    object taSListCost2: TCurrencyField
      DisplayLabel = #1056#1072#1089#1093'.'#1089#1091#1084#1084#1072
      FieldName = 'Cost2'
    end
    object taSListNOTPR: TSmallintField
      FieldName = 'NOTPR'
      Origin = 'SLIST_S.NOTPR'
    end
    object taSListNAMEPAYTYPE: TFIBStringField
      FieldName = 'NAMEPAYTYPE'
      Origin = 'SLIST_S.NAMEPAYTYPE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taSListSnOpen: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'SnOpen'
      Calculated = True
    end
    object taSListSnClose: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'SnClose'
      Calculated = True
    end
    object taSListTotalCostOpen: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotalCostOpen'
      currency = True
      Calculated = True
    end
    object taSListTotalCostClose: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotalCostClose'
      currency = True
      Calculated = True
    end
    object taSListCostOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostOpen'
      currency = True
      Calculated = True
    end
    object taSListCostClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostClose'
      currency = True
      Calculated = True
    end
    object taSListTrOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TrOpen'
      currency = True
      Calculated = True
    end
    object taSListTrClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TrClose'
      currency = True
      Calculated = True
    end
    object taSListTqOpen: TFIBSmallIntField
      FieldKind = fkCalculated
      FieldName = 'TqOpen'
      Calculated = True
    end
    object taSListTQClose: TFIBSmallIntField
      FieldKind = fkCalculated
      FieldName = 'TQClose'
      Calculated = True
    end
    object taSListTWOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TWOpen'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taSListTWClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TWClose'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taSListNDSOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'NDSOpen'
      Calculated = True
    end
    object taSListNDSClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'NDSClose'
      Calculated = True
    end
    object taSListTrNDSOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TrNDSOpen'
      currency = True
      Calculated = True
    end
    object taSListTrNDSClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'TrNDSClose'
      currency = True
      Calculated = True
    end
    object taSListEDITTR: TFIBSmallIntField
      FieldName = 'EDITTR'
    end
    object taSListITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object taSListCLTID: TFIBIntegerField
      FieldName = 'CLTID'
    end
    object taSListColor: TFIBIntegerField
      FieldName = 'Color'
    end
    object taSListcomissionscheme: TFIBIntegerField
      FieldName = 'comission$scheme'
    end
    object taSListCURR_COLOR: TFIBIntegerField
      FieldName = 'CURR_COLOR'
    end
    object taSListADDNDSTOPRICE: TFIBSmallIntField
      FieldName = 'ADD$NDS$TO$PRICE'
      OnChange = taSListADDNDSTOPRICEChange
    end
    object taSListISTOLLING: TFIBSmallIntField
      FieldName = 'IS$TOLLING'
    end
    object taSListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACT$ID'
    end
    object taSListCONTRACTCLASSID: TFIBIntegerField
      FieldName = 'CONTRACT$CLASS$ID'
    end
    object taSListCONTRACTCLASSNAME: TFIBStringField
      FieldName = 'CONTRACT$CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object taSListcontractnumber: TStringField
      FieldName = 'contract$number'
    end
    object taSListcontractdate: TDateTimeField
      FieldName = 'contract$date'
      DisplayFormat = 'dd.mm.yyyy'
    end
  end
  object dsSList: TDataSource
    DataSet = taSList
    Left = 12
    Top = 232
  end
  object quSup: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, NAME, SNAME, CODE, PAYTYPEID, NDSID'
      'FROM D_COMP'
      'WHERE SELLER>=1 and d_compid<>-1 and workorg=1'
      'ORDER BY SNAME')
    OnCalcFields = quSupCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 4
    object quSupD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quSupNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quSupSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quSupCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSupFNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'FNAME'
      Size = 80
      Calculated = True
    end
    object quSupPAYTYPEID: TIntegerField
      FieldName = 'PAYTYPEID'
      Origin = 'D_COMP.PAYTYPEID'
    end
    object quSupNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'D_COMP.NDSID'
    end
  end
  object dsSup: TDataSource
    DataSet = quSup
    Left = 20
    Top = 52
  end
  object quTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 14
    Top = 282
  end
  object taSEl: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure SEL_U (?SELID, ?PRICE, ?PRICE2, ?NDSID, ?USEMA' +
        'RGIN,'
      
        '        ?OLD_PRICE1, ?OLD_PRICE2, ?OLD_USEMARGIN, ?EP2, ?PRICE$N' +
        'DS)'
      ''
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM SEL'
      'WHERE SELID=?OLD_SELID')
    InsertSQL.Strings = (
      
        'INSERT INTO SEL (SELID, SINVID, ART2ID, PRICE, PRICE2, NDSID, WH' +
        'SELL, PRICE$NDS)'
      
        'VALUES (?SELID, ?SINVID, ?ART2ID,  ?PRICE, ?PRICE2, ?NDSID, ?WHS' +
        'ELL, ?PRICE$NDS)'
      ''
      ' ')
    RefreshSQL.Strings = (
      'SELECT SELID, SINVID, ART2ID, ART2, FULLART, PRICE, QUANTITY,'
      
        '               TOTALWEIGHT, Q, PRICE2, PNDS, UNITID, NDSID, ART,' +
        ' '
      '               DISTRED, USEMARGIN, WHSELL,'
      
        '              PRODCODE, SUPCODE, D_MATID, D_GOODID, D_INSID, D_C' +
        'OMPID, '
      
        '              D_ARTID, EP2, EX_PRICE2, SSUM, SSUMD,D_COUNTRYID, ' +
        'COLOR, PRICE - PRICE$NDS PRICE$HACK, PRICE$NDS'
      'FROM SEL_R(?SELID)'
      ' ')
    SelectSQL.Strings = (
      
        'SELECT SELID, SINVID, ART2ID, ART2, FULLART, PRICE, QUANTITY,   ' +
        'TOTALWEIGHT,'
      
        '              Q, PRICE2, PNDS,  UNITID, NDSID, ART,  DISTRED, US' +
        'EMARGIN, WHSELL,'
      
        '              PRODCODE, SUPCODE, D_MATID, D_GOODID, D_INSID, D_C' +
        'OMPID, D_ARTID, EP2, EX_PRICE2, SSUM, SSUMD,D_COUNTRYID, color, ' +
        'PRICE - PRICE$NDS PRICE$HACK, PRICE$NDS'
      'FROM SEL_S(?ASINVID)'
      'ORDER BY ART')
    AfterDelete = SElAfterPost
    AfterPost = SElAfterPost
    BeforeClose = taSElBeforeClose
    BeforeDelete = BeforeDelete
    BeforeEdit = taSElBeforeEdit
    BeforeInsert = taSElBeforeInsert
    BeforeOpen = taSElBeforeOpen
    BeforePost = taSElBeforePost
    OnCalcFields = taSElCalcFields
    OnNewRecord = taSElNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 48
    Top = 188
    object taSElSELID: TIntegerField
      FieldName = 'SELID'
    end
    object taSElSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taSElART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taSElPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object taSElQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
      OnChange = taSElQUANTITYChange
    end
    object taSElFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taSElTOTALWEIGHT: TFloatField
      FieldName = 'TOTALWEIGHT'
      OnChange = taSElTOTALWEIGHTChange
    end
    object taSElQ: TFloatField
      FieldName = 'Q'
    end
    object taSElCost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Cost'
      currency = True
      Calculated = True
    end
    object taSElPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taSElPNDS: TFloatField
      FieldName = 'PNDS'
    end
    object taSElART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taSElUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
      OnSetText = UNITIDSetText
    end
    object taSElRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSElNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taSElNdsName: TStringField
      FieldKind = fkLookup
      FieldName = 'NdsName'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Lookup = True
    end
    object taSElDISTRED: TSmallintField
      FieldName = 'DISTRED'
    end
    object taSElART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taSElUSEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
      OnChange = taSElUSEMARGINChange
      OnGetText = YesNoGetText
      OnSetText = YesNoSetText
    end
    object taSElWHSELL: TSmallintField
      FieldName = 'WHSELL'
    end
    object taSElCost2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Cost2'
      currency = True
      Calculated = True
    end
    object taSElPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSElD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSElD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSElD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSElD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object taSElD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object taSElD_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      DisplayWidth = 3
      FieldName = 'D_COUNTRYID'
      Origin = 'SEL_S.D_COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSElEP2: TFloatField
      FieldName = 'EP2'
      currency = True
    end
    object taSElEX_PRICE2: TFloatField
      FieldName = 'EX_PRICE2'
      currency = True
    end
    object taSElSSUM: TFloatField
      FieldName = 'SSUM'
      Origin = 'SEL_S.SSUM'
      currency = True
    end
    object taSElSSUMD: TFloatField
      FieldName = 'SSUMD'
      Origin = 'SEL_S.SSUMD'
      currency = True
    end
    object taSElSUPCODE: TFIBStringField
      FieldName = 'SUPCODE'
      Origin = 'SEL_S.SUPCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSElCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taSElPRICEHACK: TFIBFloatField
      FieldName = 'PRICE$HACK'
      OnChange = taSElPRICEHACKChange
      currency = True
    end
    object taSElPRICENDS: TFIBFloatField
      FieldName = 'PRICE$NDS'
    end
  end
  object dsSEl: TDataSource
    DataSet = taSEl
    Left = 48
    Top = 236
  end
  object taSItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure sitem_update'
      
        '(:UID, :OLD_UID, :W, :SS, :d_goods_sam1, :d_goods_sam2, :OLD_SIT' +
        'EMID, ?WEIGHT$INSERTION, ?COST$INSERTION, :euid)')
    DeleteSQL.Strings = (
      'execute procedure sitem_delete (?OLD_SITEMID)')
    InsertSQL.Strings = (
      'execute procedure sitem_insert(?SITEMID, ?SELID, ?UID, ?W, ?SS,'
      
        ' ?BUSYTYPE, ?d_goods_sam1, ?d_goods_sam2, ?WEIGHT$INSERTION, ?CO' +
        'ST$INSERTION, ?EUID)')
    RefreshSQL.Strings = (
      
        'SELECT S.SITEMID, S.SELID, S.W, S.SZ SS, S.UID, S.BUSYTYPE, S.WE' +
        'IGHT$INSERTION, S.COST$INSERTION,  '
      '               u.d_goods_sam1, u.d_goods_sam2 , u.euid'
      'FROM SITEM S, uid_info u'
      'WHERE S.SITEMID=?SITEMID and'
      '               s.uid = u.uid')
    SelectSQL.Strings = (
      
        'SELECT S.SITEMID, S.SELID, S.W, S.SZ ss, S.UID, S.BUSYTYPE, S.WE' +
        'IGHT$INSERTION, S.COST$INSERTION,  '
      '               u.d_goods_sam1, u.d_goods_sam2, u.euid'
      'FROM SITEM S, Uid_Info u'
      'WHERE S.SELID=?SELID and'
      '      s.uid = u.uid'
      'ORDER BY S.SITEMID')
    AfterDelete = taSItemAfterDelete
    AfterInsert = taSItemAfterInsert
    AfterPost = taSItemAfterDelete
    BeforeDelete = taSItemBeforeDelete
    BeforeEdit = taSItemBeforeEdit
    BeforeInsert = taSItemBeforeInsert
    BeforeOpen = taSItemBeforeOpen
    BeforePost = taSItemBeforePost
    OnCalcFields = taSItemCalcFields
    OnNewRecord = taSItemNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 86
    Top = 186
    object taSItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taSItemSELID: TIntegerField
      FieldName = 'SELID'
    end
    object taSItemW: TFloatField
      FieldName = 'W'
    end
    object taSItemRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSItemUID: TIntegerField
      FieldName = 'UID'
    end
    object taSItemBUSYTYPE: TSmallintField
      FieldName = 'BUSYTYPE'
    end
    object taSItemSS: TFIBStringField
      FieldName = 'SS'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSItemD_GOODS_SAM1: TIntegerField
      FieldName = 'D_GOODS_SAM1'
      Origin = 'SITEM.D_GOODS_SAM1'
    end
    object taSItemD_GOODS_SAM2: TIntegerField
      FieldName = 'D_GOODS_SAM2'
      Origin = 'SITEM.D_GOODS_SAM2'
    end
    object taSItemgoods1: TStringField
      FieldKind = fkLookup
      FieldName = 'goods1'
      LookupDataSet = dmCom.taGoodsSam
      LookupKeyFields = 'D_GOODSID_SAM'
      LookupResultField = 'NAME_SAM'
      KeyFields = 'D_GOODS_SAM1'
      Size = 100
      Lookup = True
    end
    object taSItemgoods2: TStringField
      FieldKind = fkLookup
      FieldName = 'goods2'
      LookupDataSet = dmCom.taGoodsSam
      LookupKeyFields = 'D_GOODSID_SAM'
      LookupResultField = 'NAME_SAM'
      KeyFields = 'D_GOODS_SAM2'
      Size = 100
      Lookup = True
    end
    object taSItemWEIGHTINSERTION: TFIBBCDField
      FieldName = 'WEIGHT$INSERTION'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object taSItemCOSTINSERTION: TFIBFloatField
      FieldName = 'COST$INSERTION'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSItemEUID: TFIBStringField
      FieldName = 'EUID'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object dsSItem: TDataSource
    DataSet = taSItem
    Left = 84
    Top = 232
  end
  object taDefDist: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE DefDist'
      'SET DistribP= ?DistribP'
      'WHERE DefDistId= ?OLD_DefDistId')
    SelectSQL.Strings = (
      'SELECT DefDistId,  DepId, Name, DistribP,  Quantity,  Weight'
      'FROM DefDist_S(?UserId, ?D_ArtId)'
      ''
      '')
    AfterPost = CommitRetaining
    BeforeOpen = taDefDistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 132
    Top = 188
    object taDefDistDEFDISTID: TIntegerField
      FieldName = 'DEFDISTID'
    end
    object taDefDistNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object taDefDistDISTRIBP: TSmallintField
      FieldName = 'DISTRIBP'
    end
    object taDefDistDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taDefDistQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
    object taDefDistWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
  end
  object dsDefDist: TDataSource
    DataSet = taDefDist
    Left = 132
    Top = 232
  end
  object quWHUID: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT si.SITEMID, si.UID, si.W, si.SZ,'
      '       i.DepID, a2.Art2, a2.Art2Id,'
      '       P.Price2,'
      '       sf.Price SPrice, sf.SDate, sf.SN, sf.SSF,'
      '       d.SName dep,'
      '       c.SName sup'
      
        '    FROM SITEM si, SINV i, SEL e, Art2 a2, SInfo sf, Price P, D_' +
        'Dep d, D_Comp c'
      '    where i.DepId between  :DepId1 and :DepId2 AND'
      '          i.IsClosed=1 AND'
      '          e.SInvId=i.SInvId AND'
      '          a2.Art2Id=e.Art2Id and'
      '          a2.D_ArtId= :D_ArtId and'
      '          p.Art2Id=a2.Art2Id and'
      '          p.DepId=i.DepId and'
      '          si.SElId= e.SElId and'
      '          si.BusyType=0 and'
      '          sf.SInfoId=si.SInfoId and'
      '          d.D_DepId=i.DepId and'
      '          c.D_CompId=sf.SupId')
    BeforeOpen = quWHUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 298
    Top = 2
    object quWHUIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quWHUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quWHUIDW: TFloatField
      FieldName = 'W'
    end
    object quWHUIDPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quWHUIDART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quWHUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quWHUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHUIDSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object quWHUIDSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object quWHUIDDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quWHUIDSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quWHUIDSN: TIntegerField
      FieldName = 'SN'
    end
    object quWHUIDSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quWHUIDDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
  end
  object dsWHUID: TDataSource
    DataSet = quWHUID
    Left = 300
    Top = 48
  end
  object taA2Ins: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE INS'
      'SET D_INSID=?D_INSID,'
      '    QUANTITY=?QUANTITY,'
      '    WEIGHT=?WEIGHT,'
      '    COLOR=?COLOR,'
      '    CHROMATICITY=?CHROMATICITY,'
      '    CLEANNES=?CLEANNES,'
      '    EDGTYPEID=?EDGTYPEID,'
      '    EDGSHAPEID=?EDGSHAPEID,'
      '    GR=?GR'
      'WHERE INSID=?OLD_INSID'
      ''
      ''
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM INS'
      'WHERE INSID=?OLD_INSID')
    InsertSQL.Strings = (
      
        'INSERT INTO INS (INSID, ART2ID, D_INSID, QUANTITY, WEIGHT, COLOR' +
        ','
      
        '                 CHROMATICITY, CLEANNES, EDGTYPEID, EDGSHAPEID, ' +
        'GR)'
      'VALUES'
      '(?INSID, ?ART2ID, ?D_INSID, ?QUANTITY, ?WEIGHT, ?COLOR, '
      
        '                  ?CHROMATICITY, ?CLEANNES, ?EDGTYPEID, ?EDGSHAP' +
        'EID, ?GR)'
      '')
    RefreshSQL.Strings = (
      'SELECT INSID,  ART2ID,  D_INSID, QUANTITY,  WEIGHT,'
      
        '              COLOR, CHROMATICITY, CLEANNES, EDGTYPEID, EDGSHAPE' +
        'ID,'
      '              GR, Main'
      'FROM INS'
      'WHERE INSID= ?INSID'
      'ORDER BY Main desc'
      '')
    SelectSQL.Strings = (
      'SELECT INSID,  ART2ID,  D_INSID, QUANTITY,  WEIGHT,'
      
        '              COLOR, CHROMATICITY, CLEANNES, EDGTYPEID, EDGSHAPE' +
        'ID,'
      '              GR, Main'
      'FROM INS'
      'WHERE ART2ID= ?ART2ID'
      'ORDER BY Main desc')
    AfterDelete = CommitRetaining
    AfterPost = taA2InsAfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = taA2InsBeforeDelete
    BeforeOpen = taA2InsBeforeOpen
    BeforePost = taA2InsBeforePost
    OnNewRecord = taA2InsNewRecord
    OnPostError = PostError
    AfterRefresh = taA2InsAfterRefresh
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 176
    Top = 188
    object taA2InsINSID: TIntegerField
      FieldName = 'INSID'
    end
    object taA2InsART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taA2InsQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
    end
    object taA2InsWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object taA2InsCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object taA2InsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object taA2InsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object taA2InsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taA2InsIns: TStringField
      FieldKind = fkLookup
      FieldName = 'Ins'
      LookupDataSet = dmCom.taIns
      LookupKeyFields = 'D_INSID'
      LookupResultField = 'NAME'
      KeyFields = 'D_INSID'
      Lookup = True
    end
    object taA2InsEdgType: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGTYPE'
      LookupDataSet = dmCom.quEdgT
      LookupKeyFields = 'EDGETIONID'
      LookupResultField = 'NAME'
      KeyFields = 'EDGTYPEID'
      Lookup = True
    end
    object taA2InsEdgShape: TStringField
      FieldKind = fkLookup
      FieldName = 'EDGSHAPE'
      LookupDataSet = dmCom.quEdgS
      LookupKeyFields = 'EDGETIONID'
      LookupResultField = 'NAME'
      KeyFields = 'EDGSHAPEID'
      Lookup = True
    end
    object taA2InsGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object taA2InsEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Origin = 'INS.EDGTYPEID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taA2InsEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Origin = 'INS.EDGSHAPEID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taA2InsMAIN: TFIBSmallIntField
      FieldName = 'MAIN'
    end
  end
  object dsA2Ins: TDataSource
    DataSet = taA2Ins
    Left = 176
    Top = 232
  end
  object quIns: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT I.INSID, I.ART2ID, I.D_INSID, I.QUANTITY, I.WEIGHT,'
      '       I.COLOR, I.SHAPE, I.CHROMATICITY, I.CLEANNES, I.EDGETION,'
      '       DI.NAME INS'
      'FROM INS I, D_INS DI'
      'WHERE I.ART2ID= ?ART2ID and DI.D_INSID=I.D_INSID'
      'ORDER BY I.INSID')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 156
    Top = 4
    object quInsINSID: TIntegerField
      FieldName = 'INSID'
    end
    object quInsART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quInsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInsQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
    end
    object quInsWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quInsCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object quInsSHAPE: TFIBStringField
      FieldName = 'SHAPE'
      EmptyStrToNull = True
    end
    object quInsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object quInsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object quInsEDGETION: TFIBStringField
      FieldName = 'EDGETION'
      EmptyStrToNull = True
    end
    object quInsINS: TFIBStringField
      FieldName = 'INS'
      EmptyStrToNull = True
    end
  end
  object dsIns: TDataSource
    DataSet = quIns
    Left = 158
    Top = 48
  end
  object pmWH: TPopupMenu
    AutoHotkeys = maManual
    Left = 73
    Top = 328
    object MenuItem3: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = WHClick
    end
    object MenuItem4: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object quWH: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select a.D_ArtId, a.Art, a.UnitId, a.D_MatId Mat, a.D_GoodId Goo' +
        'd, a.D_InsId Ins, c.SName Comp,'
      
        '       count(*) Quantity, sum(si.W) Weight, sum(si.Q0*p.Price2) ' +
        'Cost'
      
        'from sitem si, sel e, sinv i, art2 a2, d_art a, d_comp c, Price ' +
        'p'
      'where i.DepId between :DepId1 and :DepId2 and'
      '      i.IsClosed=1 and'
      '      e.SInvId= i.SInvId and'
      '      a2.Art2Id=e.Art2Id and'
      '      si.SElId=e.SElId and'
      '      si.BusyType=0 and'
      '      a.D_ArtId=a2.D_ArtId and'
      '      c.D_CompId=a.D_CompId and'
      '      p.DepId=i.DepId and'
      '      p.Art2Id=a2.Art2Id'
      
        'group by a.D_ArtId, a.Art, a.UnitId, a.D_MatId, a.D_GoodId, a.D_' +
        'InsId, c.SName'
      ' ')
    BeforeOpen = quWHBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 198
    Top = 4
    object quWHD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object quWHART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quWHUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object quWHCOMP: TFIBStringField
      FieldName = 'COMP'
      EmptyStrToNull = True
    end
    object quWHMAT: TFIBStringField
      FieldName = 'MAT'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHGOOD: TFIBStringField
      FieldName = 'GOOD'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHINS: TFIBStringField
      FieldName = 'INS'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quWHQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
  end
  object dsWH: TDataSource
    DataSet = quWH
    Left = 200
    Top = 48
  end
  object quWHArt2: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.Art2Id, a2.Art2, p.Price2, d.SName Dep,'
      
        '       count(*) Quantity, sum(si.w) Weight, sum(si.Q0*p.Price2) ' +
        'Cost'
      'from sitem si, sel e, sinv i, Art2 a2, Price p, D_Dep d'
      'where i.DepId between :DepId1 and :DepId2 and'
      '      i.IsClosed=1 and'
      '      e.SInvId= i.SInvId and'
      '      a2.D_ArtId=:D_ArtId and'
      '      e.Art2Id= a2.Art2Id and'
      '      si.SElId=e.SElId  and'
      '      si.BusyType=0 and'
      '      p.Art2Id= a2.Art2Id and'
      '      p.DepId = i.DepId and'
      '      d.D_DepId=i.DepId'
      ''
      'group by  a2.Art2Id, a2.Art2, p.Price2, d.SName'
      '   ')
    BeforeOpen = quWHArt2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 242
    Top = 4
    object quWHArt2ART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quWHArt2ART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quWHArt2WEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHArt2COST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quWHArt2QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object quWHArt2PRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quWHArt2DEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
  end
  object dsWHArt2: TDataSource
    DataSet = quWHArt2
    Left = 242
    Top = 48
  end
  object quWHA2UID: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub 0')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'SELECT SITEMID, UID, W, SZ, DEPID, PRICE2, SINFOID, PRICE,'
      '               SUPID, NDSID, SN, SSF, SDATE, SUP, NDSNAME'
      'FROM  WHA2UID_R(?SITEMID)')
    SelectSQL.Strings = (
      'SELECT SITEMID, UID, W, SZ, DEPID, PRICE2, SINFOID, PRICE,'
      '               SUPID, NDSID, SN, SSF, SDATE, SUP, NDSNAME'
      'FROM  WHA2UID(?DEPID1, ?DEPID2, ?ART2Id, ?DDATE )'
      'ORDER BY UID')
    BeforeOpen = quWHA2UIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Filtered = True
    OnFilterRecord = quWHA2UIDFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 368
    Top = 4
    object quWHA2UIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quWHA2UIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quWHA2UIDW: TFloatField
      FieldName = 'W'
    end
    object quWHA2UIDPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quWHA2UIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHA2UIDSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object quWHA2UIDSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quWHA2UIDNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quWHA2UIDNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object quWHA2UIDDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quWHA2UIDSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object quWHA2UIDPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quWHA2UIDSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quWHA2UIDSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quWHA2UIDSN: TIntegerField
      FieldName = 'SN'
    end
  end
  object dsWHA2UID: TDataSource
    DataSet = quWHA2UID
    Left = 368
    Top = 46
  end
  object quWHSZ: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select si.sz, count(*) Quantity, sum(si.W) Weight, sum(si.Q0*p.P' +
        'rice2) Cost'
      'from SItem si, SEl e, SInv i, Art2 a2, Price P'
      'where i.IsClosed=1 and'
      '      i.DepId between ?DepId1 and ?DepId2 and'
      '      e.SInvId=i.SInvId and'
      '      a2.Art2Id=e.Art2Id and'
      '      a2.D_ArtId= ?D_ArtId and'
      '      si.SElId=e.SElId and'
      '      si.BusyType=0 and'
      '      p.Art2Id=a2.Art2Id and'
      '      p.DepId=i.DepId'
      'group by si.sz')
    BeforeOpen = quWHSZBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 424
    Top = 2
    object quWHSZSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHSZWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHSZCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quWHSZQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
  end
  object dsWHSZ: TDataSource
    DataSet = quWHSZ
    Left = 428
    Top = 44
  end
  object quWHA2SZ: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select si.sz, count(*) Quantity, sum(si.W) Weight, sum(si.Q0*p.P' +
        'rice2) Cost'
      'from SItem si, SEl e, SInv i, Art2 a2, Price P'
      'where i.IsClosed=1 and'
      '      i.DepId between ?DepId1 and ?DepId2 and'
      '      e.SInvId=i.SInvId and'
      '      a2.Art2Id=e.Art2Id and'
      '      a2.Art2Id= ?Art2Id and'
      '      si.SElId=e.SElId and'
      '      si.BusyType=0 and'
      '      p.Art2Id=a2.Art2Id and'
      '      p.DepId=i.DepId'
      'group by si.sz')
    BeforeOpen = quWHA2SZBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 482
    Top = 4
    object quWHA2SZSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quWHA2SZWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quWHA2SZCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quWHA2SZQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
  end
  object dsWHA2SZ: TDataSource
    DataSet = quWHA2SZ
    Left = 478
    Top = 50
  end
  object pmDListTo: TPopupMenu
    AutoHotkeys = maManual
    Left = 17
    Top = 420
    object MenuItem5: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = DListToClick
    end
    object MenuItem6: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object pmDListFrom: TPopupMenu
    AutoHotkeys = maManual
    Left = 133
    Top = 328
    object MenuItem7: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = DListFromClick
    end
    object MenuItem8: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object taDList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SINV'
      'SET DEPFROMID=?DEPFROMID,'
      '    DEPID=?DEPID,'
      '    SDATE=?SDATE,'
      '    NDATE=?SDATE,'
      '    SN=?SN,'
      '    PMARGIN=?PMARGIN,'
      '    TR=?TR,'
      '    AKCIZ=?AKCIZ,'
      '    ISCLOSED=?ISCLOSED,'
      '    OPT=?OPT,'
      '    NOTPR=?NOTPR,'
      '    CLTID=?CLTID'
      'WHERE SINVID=?OLD_SINVID '
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM SINV'
      'WHERE SINVID=?OLD_SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      
        '(SINVID, DEPFROMID, DEPID, SDATE, NDATE,   SN,  PMARGIN, TR,  AK' +
        'CIZ, ISCLOSED, ITYPE, '
      'CRUSERID, OPT, OPTRET)'
      'VALUES'
      
        '(?SINVID, ?DEPFROMID, ?DEPID, ?SDATE, ?SDATE, ?SN, ?PMARGIN,   ?' +
        'TR,  ?AKCIZ, ?ISCLOSED, 2, '
      '  ?CRUSERID, ?OPT, ?OPTRET)'
      ' '
      ' '
      ' ')
    RefreshSQL.Strings = (
      
        'SELECT SINVID, DEPFROMID, DEPID, SDATE, NDATE, SN, PMARGIN, NDS,' +
        ' TR, COST, AKCIZ, ISCLOSED,'
      
        '       DEPFROM, DEPTO, COLOR, SCOST, USERID, FIO, CRUSERID, CRFI' +
        'O,'
      '       URF_FROM, URF_TO, SCOSTD, OPT, OPTRET, RSTATE, NOTPR,'
      '      COST0,  COST1, QUID, WUID, CLTID'
      'FROM DLIST_R(?SINVID)')
    SelectSQL.Strings = (
      
        'SELECT SINVID, DEPFROMID, DEPID, SDATE, NDATE, SN, PMARGIN, NDS,' +
        ' TR, COST, AKCIZ, ISCLOSED,'
      
        '       DEPFROM, DEPTO, COLOR, SCOST, CRUSERID, CRFIO, USERID, FI' +
        'O,  URF_FROM, URF_TO, '
      
        '       SCOSTD, OPT, OPTRET, RSTATE, NOTPR, COST0,  COST1, QUID, ' +
        'WUID, CLTID'
      'FROM DLIST_S(?DEPFROMID1, ?DEPFROMID2, '
      '                           ?DEPTOID1,?DEPTOID2, '
      '                           ?BD, ?ED)'
      'ORDER BY SDATE DESC')
    AfterDelete = taDListAfterDelete
    AfterOpen = taDListAfterOpen
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = BeforeDelete
    BeforeEdit = taDListBeforeEdit
    BeforeOpen = taDListBeforeOpen
    BeforePost = taDListBeforePost
    OnCalcFields = taDListCalcFields
    OnNewRecord = taDListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 214
    Top = 188
    object taDListSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taDListDEPFROMID: TIntegerField
      FieldName = 'DEPFROMID'
      OnChange = taDListDEPFROMIDChange
    end
    object taDListDEPID: TIntegerField
      FieldName = 'DEPID'
      OnChange = taDListDEPIDChange
    end
    object taDListSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taDListPMARGIN: TFloatField
      FieldName = 'PMARGIN'
    end
    object taDListNDS: TFloatField
      FieldName = 'NDS'
    end
    object taDListTR: TFloatField
      FieldName = 'TR'
    end
    object taDListCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taDListAKCIZ: TFloatField
      FieldName = 'AKCIZ'
    end
    object taDListISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taDListDEPFROM: TFIBStringField
      FieldName = 'DEPFROM'
      Size = 60
      EmptyStrToNull = True
    end
    object taDListDEPTO: TFIBStringField
      FieldName = 'DEPTO'
      Size = 60
      EmptyStrToNull = True
    end
    object taDListSN: TIntegerField
      FieldName = 'SN'
    end
    object taDListCOLOR: TIntegerField
      FieldName = 'COLOR'
    end
    object taDListSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object taDListUSERID: TIntegerField
      FieldName = 'USERID'
    end
    object taDListCRFIO: TFIBStringField
      FieldName = 'CRFIO'
      Origin = 'DLIST_S.CRFIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taDListCRUSERID: TIntegerField
      FieldName = 'CRUSERID'
      Origin = 'DLIST_S.CRUSERID'
    end
    object taDListURF_FROM: TSmallintField
      FieldName = 'URF_FROM'
      Origin = 'DLIST_S.URF_FROM'
    end
    object taDListURF_TO: TSmallintField
      FieldName = 'URF_TO'
      Origin = 'DLIST_S.URF_TO'
    end
    object taDListSCOSTD: TFloatField
      FieldName = 'SCOSTD'
      Origin = 'DLIST_S.SCOSTD'
      currency = True
    end
    object taDListOPT: TSmallintField
      FieldName = 'OPT'
      Origin = 'DLIST_S.OPT'
    end
    object taDListOPTRET: TSmallintField
      FieldName = 'OPTRET'
      Origin = 'DLIST_S.OPTRET'
    end
    object taDListRSTATE: TSmallintField
      FieldName = 'RSTATE'
      Origin = 'DLIST_S.RSTATE'
      OnGetText = RSTATEGetText
    end
    object taDListNOTPR: TSmallintField
      FieldName = 'NOTPR'
      Origin = 'DLIST_S.NOTPR'
    end
    object taDListCOST0: TFloatField
      FieldName = 'COST0'
      Origin = 'DLIST_S.COST0'
      currency = True
    end
    object taDListCost1: TCurrencyField
      FieldName = 'Cost1'
      Origin = 'DLIST_S.COST1'
    end
    object taDListQUID: TFIBIntegerField
      FieldName = 'QUID'
    end
    object taDListWUID: TFIBFloatField
      FieldName = 'WUID'
      DisplayFormat = '0.##'
    end
    object taDListInCost1: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'InCost1'
      currency = True
      Calculated = True
    end
    object taDListOutCost1: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'OutCost1'
      currency = True
      Calculated = True
    end
    object taDListInQuid: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'InQuid'
      Calculated = True
    end
    object taDListOutQuid: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'OutQuid'
      Calculated = True
    end
    object taDListInSn: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'InSn'
      Calculated = True
    end
    object taDListOutSn: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'OutSn'
      Calculated = True
    end
    object taDListInSCost: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'InSCost'
      Calculated = True
    end
    object taDListOutSCost: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'OutSCost'
      Calculated = True
    end
    object taDListInWUid: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'InWUid'
      Calculated = True
    end
    object taDListOutWuid: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'OutWuid'
      Calculated = True
    end
    object taDListCLTID: TFIBIntegerField
      FieldName = 'CLTID'
    end
    object taDListInCost0: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'InCost0'
      currency = True
      Calculated = True
    end
    object taDListOutCost0: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'OutCost0'
      currency = True
      Calculated = True
    end
  end
  object dsDList: TDataSource
    DataSet = taDList
    Left = 216
    Top = 232
  end
  object quD_WH: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ART2'
      'SET TOPTPRICE= ?OPTPRICE'
      'WHERE ART2ID=?OLD_ART2Id')
    RefreshSQL.Strings = (
      
        'SELECT  D_ARTID, ART2ID, FULLART, ART2, QUANTITY, WEIGHT,  PRICE' +
        '2, UNITID,'
      
        '        OPTPRICE, PRICE, ART, D_COMPID,  D_MATID, D_GOODID, D_IN' +
        'SID, D_COUNTRYID,'
      
        '        PRODCODE, PEQ, DQ, DW, DEPID, OPEQ, RESTQ, RESTW,S_INS,A' +
        'TT1,ATT2, FLAG, RESTA'
      'FROM D_WH_R (?DEPID, ?ART2ID, ?FLAG)')
    SelectSQL.Strings = (
      
        'SELECT  D_ARTID, ART2ID, FULLART, ART2, QUANTITY, WEIGHT,  PRICE' +
        '2, UNITID, OPTPRICE, PRICE,'
      
        '                ART, D_COMPID,  D_MATID, D_GOODID, D_INSID, D_CO' +
        'UNTRYID, PRODCODE, PEQ, DQ, DW, DEPID, OPEQ, RESTQ, RESTW,S_INS,' +
        'ATT1,ATT2, FLAG, RESTA'
      'FROM'
      ' D_WH'
      '(?ADEPID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?COUNTRYID1,'
      
        '?COMPID2, ?MATID2, ?GOODID2, ?INSID2,?COUNTRYID2, ?SHOWALL, ?USE' +
        'RID,'
      '?ART_1, ?ART_2, ?ATT1_1, ?ATT1_2, ?ATT2_1, ?ATT2_2)'
      ' ')
    AfterOpen = quD_WHAfterOpen
    BeforeOpen = quD_WHBeforeOpen
    OnCalcFields = quD_WHCalcFields
    AfterRefresh = quD_WHAfterRefresh
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 544
    Top = 6
    object quD_WHD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quD_WHART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quD_WHFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quD_WHART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quD_WHQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object quD_WHWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
    object quD_WHPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object quD_WHUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quD_WHOPTPRICE: TFIBFloatField
      FieldName = 'OPTPRICE'
    end
    object quD_WHPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quD_WHART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quD_WHD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quD_WHD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WHD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WHD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WHD_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088#1072#1085#1072
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WHPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WHPEQ: TFIBSmallIntField
      FieldName = 'PEQ'
    end
    object quD_WHDQ: TFIBIntegerField
      FieldName = 'DQ'
    end
    object quD_WHDW: TFIBFloatField
      FieldName = 'DW'
    end
    object quD_WHDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quD_WHOPEQ: TFIBSmallIntField
      FieldName = 'OPEQ'
    end
    object quD_WHRESTQ: TFIBStringField
      FieldName = 'RESTQ'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WHRESTW: TFIBStringField
      FieldName = 'RESTW'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WHS_INS: TFIBStringField
      FieldName = 'S_INS'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WHATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quD_WHATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quD_WHFLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object quD_WHRESTA: TFIBStringField
      FieldName = 'RESTA'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsD_WH: TDataSource
    DataSet = quD_WH
    Left = 546
    Top = 52
  end
  object taDItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SITEM'
      'SET W=?W,'
      '        SZ=?SZ'
      'WHERE SITEMID=?OLD_SITEMID')
    DeleteSQL.Strings = (
      'DELETE FROM SITEM'
      'WHERE SITEMID=?OLD_SITEMID')
    InsertSQL.Strings = (
      'INSERT INTO SITEM(SITEMID, SELID,W, SZ, UID, BUSYTYPE, REF)'
      'VALUES (?SITEMID, ?SELID, ?W, ?SZ, ?UID, ?BUSYTYPE, ?REF)'
      ' ')
    SelectSQL.Strings = (
      
        'SELECT si.SITEMID, si.SELID, si.W, si.SZ, si.UID, si.BUSYTYPE, s' +
        'i.REF,'
      '              sin.Price SPrice'
      'FROM SITEM si, SInfo sin'
      'WHERE si.SELID=?SELID and'
      '      sin.SInfoId=si.SInfoId'
      'ORDER BY si.UID')
    BeforeDelete = taDItemBeforeDelete
    BeforeEdit = taDItemBeforeEdit
    BeforeInsert = taDItemBeforeInsert
    BeforeOpen = taDItemBeforeOpen
    BeforePost = taDItemBeforePost
    OnNewRecord = taDItemNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 250
    Top = 188
    object taDItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taDItemSELID: TIntegerField
      FieldName = 'SELID'
    end
    object taDItemW: TFloatField
      FieldName = 'W'
    end
    object taDItemUID: TIntegerField
      FieldName = 'UID'
    end
    object taDItemBUSYTYPE: TSmallintField
      FieldName = 'BUSYTYPE'
    end
    object taDItemREF: TIntegerField
      FieldName = 'REF'
    end
    object taDItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taDItemSPRICE: TFloatField
      FieldName = 'SPRICE'
      Origin = 'SINFO.PRICE'
      currency = True
    end
  end
  object dsDItem: TDataSource
    DataSet = taDItem
    Left = 256
    Top = 236
  end
  object quElQW: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select count(*), ff(sum(w),2)'
      'from SItem'
      'where SElId= :SElId')
    Left = 132
    Top = 282
  end
  object quD_UID: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub 0')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      
        'SELECT SITEMID, UID, W,  PRICE, SUP, SUPID, SZ, NDS, ISCLOSED, F' +
        'REPAIR'
      'from D_UID_R(:SITEMID)')
    SelectSQL.Strings = (
      
        'SELECT SITEMID,  UID, W, SZ, Price, SUP, SUPID, NDS, ISCLOSED, F' +
        'REPAIR'
      'FROM D_UID_S(?ADepId, ?Art2Id, ?ISCLOSED)'
      'ORDER BY UID')
    AfterOpen = quD_UIDAfterOpen
    BeforeOpen = quD_UIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Filtered = True
    OnFilterRecord = quD_UIDFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 24
    Top = 96
    object quD_UIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quD_UIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quD_UIDW: TFloatField
      FieldName = 'W'
    end
    object quD_UIDSUP: TFIBStringField
      DisplayWidth = 20
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quD_UIDPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quD_UIDSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quD_UIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_UIDNDS: TFIBStringField
      FieldName = 'NDS'
      EmptyStrToNull = True
    end
    object quD_UIDISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object quD_UIDFREPAIR: TFIBSmallIntField
      FieldName = 'FREPAIR'
    end
  end
  object dsD_UID: TDataSource
    DataSet = quD_UID
    Left = 24
    Top = 140
  end
  object quD_SZ: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SZ, QUANTITY, WEIGHT, DQ, DW, T'
      'FROM WHA2SZ_D( ?ART2ID,  ?DEPID, ?INVID)'
      ' ')
    BeforeOpen = quD_SZBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 70
    Top = 96
    object quD_SZSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quD_SZQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
    object quD_SZWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quD_SZDQ: TFloatField
      FieldName = 'DQ'
    end
    object quD_SZDW: TFloatField
      FieldName = 'DW'
    end
    object quD_SZT: TSmallintField
      FieldName = 'T'
    end
  end
  object dsD_SZ: TDataSource
    DataSet = quD_SZ
    Left = 70
    Top = 140
  end
  object quInsDistr: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'INSERT INTO DISTR (UserId, DepId, SItemId, T)'
      'VALUES (?UserId, ?DepId, ?SItemId, ?T)')
    Left = 184
    Top = 282
  end
  object quD_T: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT QUANTITY, WEIGHT, DQ, DW  FROM WHA2SZ_DTOTAL'
      '( ?ART2ID,   ?ADEPID, ?SUPID1, ?SUPID2, ?P1, ?P2, ?SZ1, ?SZ2)')
    BeforeOpen = quD_UIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 112
    Top = 100
    object quD_TQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object quD_TWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quD_TDQ: TIntegerField
      FieldName = 'DQ'
    end
    object quD_TDW: TFloatField
      FieldName = 'DW'
    end
  end
  object dsD_T: TDataSource
    DataSet = quD_T
    Left = 112
    Top = 142
  end
  object pmPrOrd: TPopupMenu
    AutoHotkeys = maManual
    Left = 131
    Top = 422
    object MenuItem9: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = PrOrdClick
    end
    object MenuItem10: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object taPrOrd: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PrOrd'
      'SET CloseDate=?CloseDate,'
      '        SetDate=?SetDate,'
      '    IsClosed=?IsClosed,'
      '    PrOrd=?PrOrd,'
      '    CloseEmpId=?CloseEmpId,'
      '    SetEmpId=?SetEmpId,'
      '    PrDate=?PrDate'
      'WHERE PrOrdId= ?OLD_PrOrdId')
    DeleteSQL.Strings = (
      'DELETE FROM PrOrd'
      'WHERE PrOrdId= ?OLD_PrOrdId')
    InsertSQL.Strings = (
      
        'INSERT INTO PrOrd(PrOrdId, DepId, IsClosed, PrOrd, IsClosed, IsS' +
        'et, PrDate)'
      'VALUES (?PrOrdId, ?DepId, ?IsClosed, ?PrOrd, 0, 0, ?PrDate)')
    RefreshSQL.Strings = (
      
        'select PrOrdId, DepId, CloseDate, SetDate, IsClosed, PrOrd, IsSe' +
        't,'
      '          PrDate, CloseEmpId, SetEmpId, IType, Dep, SN,  SSF,'
      '          CLOSEFIO,  SETFIO, SINVID, Q, Cost, OldCost, DCost,'
      
        '         PActClosed, NAct, RState_P, RState_A, OnlyAct, IsReVal,' +
        ' UID'
      'from PRORD_R(?PRORDID)')
    SelectSQL.Strings = (
      
        'select PrOrdId, DepId, CloseDate, SetDate, IsClosed, PrOrd, IsSe' +
        't,'
      '          PrDate, CloseEmpId, SetEmpId, IType, Dep, SN,  SSF,'
      '          CLOSEFIO,  SETFIO, SINVID, Q, Cost, OldCost, DCost,'
      
        '          PActClosed, NAct, RState_P, RState_A, OnlyACt, IsReVal' +
        ', UID'
      'from PRORD_S(?DepId1, ?DepId2, ?BD, ?ED)'
      ''
      ''
      ''
      '')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taPrOrdBeforeDelete
    BeforeEdit = taPrOrdBeforeEdit
    BeforeOpen = taPrOrdBeforeOpen
    OnCalcFields = taPrOrdCalcFields
    OnDeleteError = taPrOrdDeleteError
    OnNewRecord = taPrOrdNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 290
    Top = 192
    object taPrOrdPRORDID: TIntegerField
      FieldName = 'PRORDID'
    end
    object taPrOrdDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taPrOrdISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taPrOrdDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object taPrOrdSN: TIntegerField
      FieldName = 'SN'
    end
    object taPrOrdPRORD: TIntegerField
      FieldName = 'PRORD'
    end
    object taPrOrdISSET: TSmallintField
      FieldName = 'ISSET'
    end
    object taPrOrdCLOSEDATE: TDateTimeField
      FieldName = 'CLOSEDATE'
    end
    object taPrOrdSETDATE: TDateTimeField
      FieldName = 'SETDATE'
    end
    object taPrOrdCLOSEEMPID: TIntegerField
      FieldName = 'CLOSEEMPID'
    end
    object taPrOrdSETEMPID: TIntegerField
      FieldName = 'SETEMPID'
    end
    object taPrOrdCLOSEFIO: TFIBStringField
      FieldName = 'CLOSEFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taPrOrdSETFIO: TFIBStringField
      FieldName = 'SETFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taPrOrdITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object taPrOrdSNstr: TStringField
      FieldKind = fkCalculated
      FieldName = 'SNstr'
      Size = 30
      Calculated = True
    end
    object taPrOrdSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taPrOrdPRDATE: TDateTimeField
      FieldName = 'PRDATE'
    end
    object taPrOrdSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taPrOrdQ: TIntegerField
      FieldName = 'Q'
    end
    object taPrOrdCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taPrOrdOLDCOST: TFloatField
      FieldName = 'OLDCOST'
      currency = True
    end
    object taPrOrdDCOST: TFloatField
      FieldName = 'DCOST'
      currency = True
    end
    object taPrOrdPACTCLOSED: TSmallintField
      FieldName = 'PACTCLOSED'
    end
    object taPrOrdNACT: TIntegerField
      FieldName = 'NACT'
      Origin = 'PRORD_S.NACT'
    end
    object taPrOrdRSTATE_P: TSmallintField
      FieldName = 'RSTATE_P'
      Origin = 'PRORD_S.RSTATE_P'
      OnGetText = RSTATEGetText
    end
    object taPrOrdRSTATE_A: TSmallintField
      FieldName = 'RSTATE_A'
      Origin = 'PRORD_S.RSTATE_A'
      OnGetText = RSTATEGetText
    end
    object taPrOrdONLYACT: TSmallintField
      FieldName = 'ONLYACT'
      Origin = 'PRORD_S.ONLYACT'
    end
    object taPrOrdISREVAL: TIntegerField
      FieldName = 'ISREVAL'
      Origin = 'PRORD_S.ISREVAL'
    end
    object taPrOrdUID: TIntegerField
      FieldName = 'UID'
      Origin = 'PRORD_S.UID'
    end
  end
  object dsPrOrd: TDataSource
    DataSet = taPrOrd
    Left = 300
    Top = 236
  end
  object taPrOrdItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PrOrdItem'
      'SET  Price2=?Price2,'
      '         Price=?Price,'
      '         OptPrice=?OptPrice,'
      '         Q=?Q,'
      '        W=?W'
      'WHERE PrOrdItemId= ?OLD_PrOrdItemId')
    DeleteSQL.Strings = (
      'DELETE FROM PRORDITEM'
      'WHERE PRORDITEMID=?OLD_PRORDITEMID')
    InsertSQL.Strings = (
      
        'INSERT INTO PrOrdItem(PRORDITEMID, PRORDID, ART2ID,  Price2, Pri' +
        'ce, OptPrice, Q, W)'
      
        'VALUES (?PRORDITEMID, ?PRORDID, ?ART2ID,  ?Price2, ?Price, ?OptP' +
        'rice, ?Q, ?W)'
      ' ')
    RefreshSQL.Strings = (
      'select PRORDITEMID, PRORDID, ART2ID, CURPRICE, RQ, RW, Price2,'
      '          FullArt, Art2, WHRQ, WHRW,'
      
        '          CUROP, CURSP,  PRICE, OPTPRICE, OLDP, OLDP2, OLDOP, Q,' +
        ' W, UNITID, Q0, COST, OLDCOST, dPrice,  dCost, IsReVal'
      'from PrOrdItem_R(?PrOrdItemId)')
    SelectSQL.Strings = (
      'select PRORDITEMID, PRORDID, ART2ID, CURPRICE, RQ, RW, Price2,'
      
        '          FullArt, Art2, WHRQ, WHRW, CUROP, CURSP,  PRICE, OPTPR' +
        'ICE, OLDP, OLDP2,'
      
        '          OLDOP, Q, W, UNITID, Q0, COST, OLDCOST, dPrice,  dCost' +
        ', IsReVal, CloseDate'
      'from PrOrdItem_S(?PrOrdId)'
      ' '
      'order by FullArt, Art2')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = taPrOrdItemBeforeDelete
    BeforeEdit = taPrOrdItemBeforeEdit
    BeforeInsert = taPrOrdItemBeforeInsert
    BeforeOpen = taPrOrdItemBeforeOpen
    BeforePost = taPrOrdItemBeforePost
    OnCalcFields = taPrOrdItemCalcFields
    OnNewRecord = taPrOrdItemNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 370
    Top = 192
    object taPrOrdItemPRORDITEMID: TIntegerField
      FieldName = 'PRORDITEMID'
    end
    object taPrOrdItemPRORDID: TIntegerField
      FieldName = 'PRORDID'
    end
    object taPrOrdItemART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taPrOrdItemFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taPrOrdItemART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taPrOrdItemPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taPrOrdItemRQ: TFloatField
      FieldName = 'RQ'
    end
    object taPrOrdItemRW: TFloatField
      FieldName = 'RW'
    end
    object taPrOrdItemWHRQ: TFloatField
      FieldName = 'WHRQ'
    end
    object taPrOrdItemWHRW: TFloatField
      FieldName = 'WHRW'
    end
    object taPrOrdItemCURPRICE: TFloatField
      FieldName = 'CURPRICE'
      currency = True
    end
    object taPrOrdItemCUROP: TFloatField
      FieldName = 'CUROP'
      currency = True
    end
    object taPrOrdItemCURSP: TFloatField
      FieldName = 'CURSP'
      currency = True
    end
    object taPrOrdItemPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object taPrOrdItemOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object taPrOrdItemOLDP: TFloatField
      FieldName = 'OLDP'
      currency = True
    end
    object taPrOrdItemOLDP2: TFloatField
      FieldName = 'OLDP2'
      currency = True
    end
    object taPrOrdItemOLDOP: TFloatField
      FieldName = 'OLDOP'
      currency = True
    end
    object taPrOrdItemQ: TIntegerField
      FieldName = 'Q'
    end
    object taPrOrdItemW: TFloatField
      FieldName = 'W'
    end
    object taPrOrdItemUNITID: TSmallintField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object taPrOrdItemCost: TFloatField
      FieldName = 'Cost'
      currency = True
    end
    object taPrOrdItemOldCost: TFloatField
      FieldName = 'OldCost'
      currency = True
    end
    object taPrOrdItemdPrice: TFloatField
      FieldName = 'dPrice'
      currency = True
    end
    object taPrOrdItemdCost: TFloatField
      FieldName = 'dCost'
      currency = True
    end
    object taPrOrdItemQ0: TFloatField
      FieldName = 'Q0'
      Origin = 'PRORDITEM_S.Q0'
    end
    object taPrOrdItemISREVAL: TIntegerField
      FieldName = 'ISREVAL'
      Origin = 'PRORDITEM_S.ISREVAL'
    end
    object taPrOrdItemCLOSEDATE: TFIBDateTimeField
      FieldName = 'CLOSEDATE'
    end
  end
  object dsPrOrdItem: TDataSource
    DataSet = taPrOrdItem
    Left = 368
    Top = 236
  end
  object taDepMargin: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE  D_DEP'
      'SET MARGIN=?MARGIN,'
      '        ROUNDNORM=?ROUNDNORM'
      'WHERE D_DEPID=?OLD_D_DEPID')
    SelectSQL.Strings = (
      'SELECT  D_DEPID, NAME, SNAME, MARGIN, ROUNDNORM'
      'FROM D_DEP'
      'where d_depid>0'
      'ORDER BY NAME')
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 422
    Top = 190
    object taDepMarginD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
    end
    object taDepMarginNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object taDepMarginMARGIN: TFloatField
      FieldName = 'MARGIN'
    end
    object taDepMarginROUNDNORM: TSmallintField
      FieldName = 'ROUNDNORM'
      OnGetText = taDepMarginROUNDNORMGetText
      OnSetText = taDepMarginROUNDNORMSetText
    end
    object taDepMarginSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object dsDepMargin: TDataSource
    DataSet = taDepMargin
    Left = 422
    Top = 234
  end
  object pmPrice: TPopupMenu
    AutoHotkeys = maManual
    Left = 71
    Top = 374
  end
  object taSetPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure SerPrice_U ?PriceId, ?Price2, ?OptPrice, ?InvI' +
        'd')
    SelectSQL.Strings = (
      'select PRICEID, NAME, Price1, Price2, DepId, OptPrice, InvId'
      'from  SETPRICE_S(?ART2ID, ?INVID)'
      'order by NAME')
    AfterPost = taSetPriceAfterPost
    BeforePost = taSetPriceBeforePost
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 476
    Top = 192
    object taSetPricePRICEID: TIntegerField
      FieldName = 'PRICEID'
      Required = True
    end
    object taSetPriceNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taSetPricePRICE1: TFloatField
      FieldName = 'PRICE1'
      currency = True
    end
    object taSetPricePRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taSetPriceDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taSetPriceOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
  end
  object dsSetPrice: TDataSource
    DataSet = taSetPrice
    Left = 476
    Top = 236
  end
  object taSellList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SELL'
      'SET ED= ?ED,'
      '        CLOSED=?CLOSED,'
      '        BD=?BD,'
      '        USESDATE=?USESDATE,'
      '       RN=?RN'
      'WHERE SELLID=?OLD_SELLID')
    DeleteSQL.Strings = (
      'delete from Sell'
      'where SellId=?Old_SellId')
    InsertSQL.Strings = (
      
        'INSERT INTO SELL(SELLID, BD, ED, DEPID, N, EMP1ID, EMP2ID, CLOSE' +
        'D, USESDATE)'
      
        'VALUES (?SELLID, ?BD, ?ED, ?DEPID, ?N, ?EMP1ID, ?EMP2ID, ?CLOSED' +
        ', ?USESDATE)')
    RefreshSQL.Strings = (
      'SELECT SELLID,  BD,  ED, DEPID, N, DEP, CNT, W, COST, RCNT, RW, '
      'RCOST, RN, CLOSED, USESDATE, RSTATE,EMP1ID, FREG,COST0,RCOST0, '
      
        'SCOST,OPENEMP, actdiscount, actdiscountret, costcard, total, rsc' +
        'ost, rcsert'
      'FROM SELL_R(:SELLID)')
    SelectSQL.Strings = (
      
        'SELECT SELLID,  BD,  ED, DEPID, N, DEP, CNT, W, COST, RCNT, RW, ' +
        'RCOST, RN, CLOSED, USESDATE, RSTATE,EMP1ID, FREG, COST0, RCOST0,' +
        ' SCOST, RSCOST, OPENEMP, actdiscount, actdiscountret, costcard, '
      'sertsell, csert, sert_add, total, rcsert, return_cost'
      'FROM SELL_S(?DEPID1, ?DEPID2, ?BD, ?ED)'
      'ORDER BY BD DESC')
    AfterDelete = taSellListAfterDelete
    AfterPost = CommitRetaining
    BeforeDelete = taSellListBeforeDelete
    BeforeEdit = taSellListBeforeEdit
    BeforeInsert = taSellListBeforeInsert
    BeforeOpen = taSellListBeforeOpen
    BeforePost = taSellListBeforePost
    OnCalcFields = taSellListCalcFields
    OnNewRecord = taSellListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 528
    Top = 194
    object taSellListSELLID: TIntegerField
      FieldName = 'SELLID'
    end
    object taSellListBD: TDateTimeField
      FieldName = 'BD'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSellListED: TDateTimeField
      FieldName = 'ED'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSellListDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taSellListN: TIntegerField
      FieldName = 'N'
    end
    object taSellListDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taSellListCNT: TIntegerField
      FieldName = 'CNT'
    end
    object taSellListW: TFloatField
      FieldName = 'W'
      DisplayFormat = '#.##'
    end
    object taSellListCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taSellListRCNT: TIntegerField
      FieldName = 'RCNT'
    end
    object taSellListRW: TFloatField
      FieldName = 'RW'
    end
    object taSellListRCOST: TFloatField
      FieldName = 'RCOST'
      currency = True
    end
    object taSellListCASS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CASS'
      currency = True
      Calculated = True
    end
    object taSellListRN: TIntegerField
      FieldName = 'RN'
    end
    object taSellListCLOSED: TSmallintField
      FieldName = 'CLOSED'
    end
    object taSellListUSESDATE: TSmallintField
      FieldName = 'USESDATE'
    end
    object taSellListRSTATE: TSmallintField
      FieldName = 'RSTATE'
      Origin = 'SELL_S.RSTATE'
      OnGetText = RSTATEGetText
    end
    object taSellListEMP1ID: TIntegerField
      FieldName = 'EMP1ID'
      Origin = 'SELL_S.EMP1ID'
    end
    object taSellListFREG: TIntegerField
      FieldName = 'FREG'
      Origin = 'SELL_S.FREG'
    end
    object taSellListCOST0: TFloatField
      FieldName = 'COST0'
      Origin = 'SELL_S.COST0'
      currency = True
    end
    object taSellListRCOST0: TFloatField
      FieldName = 'RCOST0'
      Origin = 'SELL_S.RCOST0'
      currency = True
    end
    object taSellListSCOST: TFloatField
      FieldName = 'SCOST'
      Origin = 'SELL_S.SCOST'
      currency = True
    end
    object taSellListRSCOST: TFloatField
      FieldName = 'RSCOST'
      Origin = 'SELL_S.RSCOST'
      currency = True
    end
    object taSellListOPENEMP: TFIBStringField
      FieldName = 'OPENEMP'
      Size = 60
      EmptyStrToNull = True
    end
    object taSellListACTDISCOUNT: TFIBFloatField
      FieldName = 'ACTDISCOUNT'
      currency = True
    end
    object taSellListACTDISCOUNTRET: TFIBFloatField
      FieldName = 'ACTDISCOUNTRET'
      currency = True
    end
    object taSellListCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
      currency = True
    end
    object taSellListSERTSELL: TFIBFloatField
      FieldName = 'SERTSELL'
      currency = True
    end
    object taSellListCSERT: TFIBFloatField
      FieldName = 'CSERT'
      currency = True
    end
    object taSellListSERT_ADD: TFIBFloatField
      FieldName = 'SERT_ADD'
      currency = True
    end
    object taSellListTOTAL: TFIBFloatField
      FieldName = 'TOTAL'
      currency = True
    end
    object taSellListreal_vych: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'real_vych'
      Calculated = True
    end
    object taSellListreal_razn: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'real_razn'
      Calculated = True
    end
    object taSellListRCSERT: TFIBIntegerField
      FieldName = 'RCSERT'
    end
    object taSellListRETURN_COST: TFIBFloatField
      FieldName = 'RETURN_COST'
    end
  end
  object dsSellList: TDataSource
    DataSet = taSellList
    Left = 530
    Top = 238
  end
  object pmSell: TPopupMenu
    AutoHotkeys = maManual
    Left = 71
    Top = 418
    object MenuItem11: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = SellClick
    end
    object MenuItem12: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object pmSellItem: TPopupMenu
    AutoHotkeys = maManual
    Left = 133
    Top = 380
  end
  object taSellItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure SELLITEM_U (?OLD_SELLITEMID, ?DEP, ?CLIENTID,'
      
        '?ART2ID, ?PRICE, ?PRICE0, ?SITEMID, ?W, ?SZ, ?UID, ?EMPID, ?CHEC' +
        'KNO,'
      '?DISCOUNT, ?DISCMEMO, ?D_RETID, ?RETDATE, ?RETDONE, '
      
        '?RETSELLITEMID,  ?RETPRICE, ?OPT, ?RETCLIENT, ?RETADDRESS, ?RET,' +
        ' ?OLD_APPL_Q, '
      '?APPL_Q)')
    DeleteSQL.Strings = (
      'execute procedure delsellitem (?OLD_SELLITEMID, ?F)')
    InsertSQL.Strings = (
      'insert into SELLITEM(SELLITEMID, SELLID, ADATE, DEP, CLIENTID, '
      
        '                 ART2ID, PRICE, PRICE0, SITEMID, W, SZ, UID, EMP' +
        'ID, MEMO,'
      
        '                 CHECKNO, DISCOUNT, DISCMEMO, RET, D_RETID, RETD' +
        'ATE,'
      
        '                 RETSELLITEMID, RETDONE, RETPRICE,OPT, RetClient' +
        ', '
      '                 RETADDRESS, CASHIERID, card)'
      'values     ( ?SELLITEMID, ?SELLID, ?ADATE, ?DEP, ?CLIENTID,'
      
        '                 ?ART2ID, ?PRICE, ?PRICE0, ?SITEMID, ?W, ?SZ, ?U' +
        'ID, ?EMPID, ?MEMO, '
      
        '                 ?CHECKNO, ?DISCOUNT, ?DISCMEMO, ?RET, ?D_RETID,' +
        ' ?RETDATE,'
      
        '                 ?RETSELLITEMID, ?RETDONE, ?RETPRICE, ?OPT, ?RET' +
        'Client,'
      '                 ?RETADDRESS, ?CASHIERID, ?card)')
    RefreshSQL.Strings = (
      
        'SELECT SELLITEMID, SELLID, ADATE, DEP, CLIENTID, PRICE, PRICE0, ' +
        'ART2ID,'
      '       SITEMID, EMPID, W, SZ, UID, MEMO, FULLART, ART2, UNITID,'
      '       CHECKNO, NAME, MAT, DISCOUNT, EMP, DISCMEMO, RET,'
      
        '       SDATE,  SPRICE, SUP, SINFOID, RETSITEMID, SN, RQ, RW, USE' +
        'RID, ART, '
      '       D_RETID, RETDATE, RETSELLITEMID, RETDONE,'
      
        '       SQ, SW,SQ_DART,SW_DART,RETPRICE, OPT, SQR, SWR,CLIENTNAME' +
        ',F,'
      
        '       NODCARD, CODE,INSID, GOODID, MATID, retclient, retaddress' +
        ', retcause, '
      
        '       CASHIERID, CASHIERNAME, ARTID, APPL_Q, SQAPPL, actdiscoun' +
        't, card, cost,'
      '       cost0, difq, costcard'
      'FROM SELLITEM_R(?SELLITEMID, ?USERID, ?F)')
    SelectSQL.Strings = (
      
        'SELECT sl.SELLITEMID, sl.SELLID, sl.ADATE, sl.DEP, sl.CLIENTID, ' +
        'sl.PRICE, sl.PRICE0, sl.ART2ID, sl.SITEMID, sl.EMPID, sl.W, sl.S' +
        'Z, sl.UID, sl.MEMO, sl.FULLART, sl.ART2, sl.UNITID, sl.CHECKNO, ' +
        'sl.NAME, sl.MAT, fif(sl.discount, sl.discount, ((sl.price0 - sl.' +
        'price) * 100) / sl.price0) discount, sl.EMP, sl.DISCMEMO, sl.RET' +
        ', sl.SDATE, sl.SPRICE, sl.SUP, sl.SINFOID, sl.RETSITEMID, sl.SN,' +
        ' sl.RQ, sl.RW, sl.USERID, sl.ART, sl.D_RETID, sl.RETDATE, sl.RET' +
        'SELLITEMID, sl.RETDONE, sl.SQ, sl.SW, sl.SQ_DART, sl.SW_DART, sl' +
        '.RETPRICE, sl.OPT, sl.SQR, sl.SWR, sl.CLIENTNAME, sl.F, sl.NODCA' +
        'RD, sl.CODE, sl.INSID, sl.GOODID, sl.MATID, sl.retclient, sl.ret' +
        'address, sl.retcause, sl.CASHIERID, sl.CASHIERNAME, sl.Artid, sl' +
        '.Appl_Q, sl.SQAPPL, sl.actdiscount, sl.Card, sl.Cost, sl.Cost0, ' +
        'sl.difq, sl.costcard, sl.TRANSACT_ID'
      ''
      'FROM SELLITEM_S(?SELLID, ?USERID) sl'
      ''
      ''
      ''
      '')
    AutoUpdateOptions.CanChangeSQLs = True
    AfterDelete = taSellItemAfterDelete
    AfterPost = taSellItemAfterPost
    BeforeDelete = taSellItemBeforeDelete
    BeforeEdit = taSellItemBeforeEdit
    BeforeInsert = taSellItemBeforeInsert
    BeforeOpen = taSellItemBeforeOpen
    BeforePost = taSellItemBeforePost
    OnCalcFields = taSellItemCalcFields
    OnNewRecord = taSellItemNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 580
    Top = 200
    object taSellItemSELLITEMID: TIntegerField
      FieldName = 'SELLITEMID'
    end
    object taSellItemSELLID: TIntegerField
      FieldName = 'SELLID'
    end
    object taSellItemADATE: TDateTimeField
      FieldName = 'ADATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSellItemDEP: TSmallintField
      FieldName = 'DEP'
    end
    object taSellItemCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
    end
    object taSellItemPRICE: TFloatField
      FieldName = 'PRICE'
      OnChange = taSellItemPRICEChange
      currency = True
    end
    object taSellItemFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taSellItemART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taSellItemPRICE0: TFloatField
      FieldName = 'PRICE0'
      currency = True
    end
    object taSellItemART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taSellItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taSellItemW: TFloatField
      FieldName = 'W'
      DisplayFormat = '#.##'
    end
    object taSellItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemUID: TIntegerField
      FieldName = 'UID'
    end
    object taSellItemEMPID: TIntegerField
      FieldName = 'EMPID'
    end
    object taSellItemMEMO: TMemoField
      FieldName = 'MEMO'
      BlobType = ftMemo
      Size = 8
    end
    object taSellItemUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object taSellItemCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
    end
    object taSellItemNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object taSellItemMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object taSellItemDISC: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DISC'
      Calculated = True
    end
    object taSellItemDISCMEMO: TFIBStringField
      FieldName = 'DISCMEMO'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemRET: TSmallintField
      FieldName = 'RET'
    end
    object taSellItemSDATE: TDateTimeField
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSellItemSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object taSellItemSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object taSellItemSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object taSellItemRETSITEMID: TIntegerField
      FieldName = 'RETSITEMID'
    end
    object taSellItemSN: TIntegerField
      FieldName = 'SN'
    end
    object taSellItemRQ: TIntegerField
      FieldName = 'RQ'
    end
    object taSellItemRW: TFloatField
      FieldName = 'RW'
      DisplayFormat = '0.##'
    end
    object taSellItemUSERID: TIntegerField
      FieldName = 'USERID'
    end
    object taSellItemART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taSellItemOp: TStringField
      FieldKind = fkCalculated
      FieldName = 'Op'
      Size = 10
      Calculated = True
    end
    object taSellItemD_RETID: TFIBStringField
      FieldName = 'D_RETID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemRETSELLITEMID: TIntegerField
      FieldName = 'RETSELLITEMID'
    end
    object taSellItemRETDONE: TSmallintField
      FieldName = 'RETDONE'
    end
    object taSellItemSQ: TFIBStringField
      FieldName = 'SQ'
      Origin = 'SELLITEM_S.SQ'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemSW: TFIBStringField
      FieldName = 'SW'
      Origin = 'SELLITEM_S.SW'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemRETPRICE: TFloatField
      FieldName = 'RETPRICE'
      Origin = 'SELLITEM_S.RETPRICE'
      currency = True
    end
    object taSellItemopt: TSmallintField
      FieldName = 'opt'
    end
    object taSellItemSQR: TFIBStringField
      FieldName = 'SQR'
      Origin = 'SELLITEM_S.SQR'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemSWR: TFIBStringField
      FieldName = 'SWR'
      Origin = 'SELLITEM_S.SWR'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Origin = 'SELLITEM_S.CLIENTNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSellItemF: TIntegerField
      FieldName = 'F'
      Origin = 'SELLITEM_S.F'
    end
    object taSellItemNODCARD: TFIBStringField
      FieldName = 'NODCARD'
      Origin = 'SELLITEM_S.NODCARD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSellItemEMP: TFIBStringField
      FieldName = 'EMP'
      Origin = 'SELLITEM_S.EMP'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSellItemCODE: TFIBStringField
      FieldName = 'CODE'
      Origin = 'SELLITEM_S.CODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemINSID: TFIBStringField
      FieldName = 'INSID'
      Origin = 'SELLITEM_S.INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemGOODID: TFIBStringField
      FieldName = 'GOODID'
      Origin = 'SELLITEM_S.GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'SELLITEM_S.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSellItemRETCLIENT: TFIBStringField
      FieldName = 'RETCLIENT'
      Origin = 'SELLITEM_S.RETCLIENT'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSellItemSQ_DART: TFIBStringField
      FieldName = 'SQ_DART'
      Origin = '"SELLITEM_S"."SQ_DART"'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemSW_DART: TFIBStringField
      FieldName = 'SW_DART'
      Origin = '"SELLITEM_S"."SW_DART"'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemRETDATE: TDateTimeField
      FieldName = 'RETDATE'
      Origin = 'SELLITEM_S.RETDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSellItemRETADDRESS: TFIBStringField
      FieldName = 'RETADDRESS'
      Origin = 'SELLITEM_S.RETADDRESS'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSellItemRETCAUSE: TFIBStringField
      FieldName = 'RETCAUSE'
      Size = 40
      EmptyStrToNull = True
    end
    object taSellItemCASHIERID: TFIBIntegerField
      FieldName = 'CASHIERID'
    end
    object taSellItemCASHIERNAME: TFIBStringField
      FieldName = 'CASHIERNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taSellItemARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taSellItemAPPL_Q: TFIBIntegerField
      FieldName = 'APPL_Q'
    end
    object taSellItemSQAPPL: TFIBStringField
      FieldName = 'SQAPPL'
      Size = 200
      EmptyStrToNull = True
    end
    object taSellItemACTDISCOUNT: TFIBFloatField
      FieldName = 'ACTDISCOUNT'
      currency = True
    end
    object taSellItemCARD: TFIBSmallIntField
      FieldName = 'CARD'
    end
    object taSellItemCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taSellItemCOST0: TFIBFloatField
      FieldName = 'COST0'
      currency = True
    end
    object taSellItemDIFQ: TFIBIntegerField
      FieldName = 'DIFQ'
    end
    object taSellItemCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
    end
    object taSellItemTRANSACT_ID: TFIBIntegerField
      FieldName = 'TRANSACT_ID'
    end
    object taSellItemDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
      DisplayFormat = '0.##'
    end
    object taSellItemCLIENTCARD: TStringField
      FieldKind = fkCalculated
      FieldName = 'CLIENT$CARD'
      Calculated = True
    end
  end
  object dsSellItem: TDataSource
    DataSet = taSellItem
    Left = 576
    Top = 242
  end
  object taA2: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure A2_U (?Art2Id, ?Art2, ?InvId,'
      
        '                              ?NEW_UseMargin, ?NEW_Price1, ?NEW_' +
        'Price2, ?NDSID,'
      
        '                              ?OLD_UseMargin, ?OLD_Price1, ?OLD_' +
        'Price2, ?PRICE$NDS)                              '
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM ART2 WHERE ART2ID=?ART2ID')
    InsertSQL.Strings = (
      
        'execute procedure A2_I (?Art2Id, ?Art2, ?ArtId, ?SupId, ?Price1,' +
        ' ?UseMargin, ?Price2, ?InvId, ?PRICE$NDS)')
    RefreshSQL.Strings = (
      'SELECT ART2Id, ArtId, ART2, SupId, INVID, USEMARGIN, '
      '              PRICE1, PRICE2, NDSID, OPTPRICE, PDIF, RQ, RW, '
      
        '              W, USERID, F1, F2, THISINV, PRICE1 - PRICE$NDS PRI' +
        'CE$HACK, PRICE$NDS'
      'FROM A2_R(?ART2ID, ?INVID, ?USERID, ?F1, ?F2, ?THISINV)')
    SelectSQL.Strings = (
      'SELECT ART2Id, ArtId, ART2, SupId, INVID, USEMARGIN, '
      
        '              PRICE1, PRICE2, NDSID, OPTPRICE, PDIF, RQ, RW, W, ' +
        'USERID, F1, F2, THISINV, PRICE1 - PRICE$NDS PRICE$HACK, PRICE$ND' +
        'S'
      'FROM A2_S(?D_ARTID, ?AINVID, ?USERID, 1)'
      'ORDER BY THISINV DESC, RQ DESC')
    AfterCancel = taA2AfterCancel
    AfterDelete = CommitRetaining
    AfterPost = taA2AfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taA2BeforeEdit
    BeforeInsert = taA2BeforeInsert
    BeforeOpen = taA2BeforeOpen
    BeforePost = taA2BeforePost
    OnCalcFields = taA2CalcFields
    OnNewRecord = taA2NewRecord
    OnPostError = PostError
    AfterRefresh = taA2AfterRefresh
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsArt
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 288
    Top = 100
    dcForceOpen = True
    object taA2ART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taA2ARTID: TIntegerField
      FieldName = 'ARTID'
    end
    object taA2SUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taA2INVID: TIntegerField
      FieldName = 'INVID'
    end
    object taA2USEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
      OnChange = taA2USEMARGINChange
      OnGetText = YesNoGetText
      OnSetText = YesNoSetText
    end
    object taA2PRICE1: TFloatField
      FieldName = 'PRICE1'
      OnChange = taA2PRICE1Change
      currency = True
    end
    object taA2PRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taA2NDSID: TFloatField
      FieldName = 'NDSID'
    end
    object taA2nds: TStringField
      FieldKind = fkLookup
      FieldName = 'nds'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Lookup = True
    end
    object taA2OPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object taA2PDIF: TSmallintField
      FieldName = 'PDIF'
    end
    object taA2RQ: TFloatField
      FieldName = 'RQ'
    end
    object taA2RW: TFloatField
      FieldName = 'RW'
      DisplayFormat = '0.##'
    end
    object taA2W: TFloatField
      FieldName = 'W'
    end
    object taA2USERID: TIntegerField
      FieldName = 'USERID'
    end
    object taA2F1: TSmallintField
      FieldName = 'F1'
    end
    object taA2F2: TSmallintField
      FieldName = 'F2'
    end
    object taA2ART2: TStringField
      FieldName = 'ART2'
      Origin = 'A2_S.ART2'
      FixedChar = True
    end
    object taA2THISINV: TSmallintField
      FieldName = 'THISINV'
      Origin = 'A2_S.THISINV'
    end
    object taA2TollingPrice: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'Tolling$Price'
      Calculated = True
    end
    object taA2PRICEHACK: TFIBFloatField
      FieldName = 'PRICE$HACK'
      OnChange = taA2PRICEHACKChange
      currency = True
    end
    object taA2PRICENDS: TFIBFloatField
      FieldName = 'PRICE$NDS'
    end
  end
  object dsA2: TDataSource
    DataSet = taA2
    Left = 286
    Top = 142
  end
  object quPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure stub 0')
    SelectSQL.Strings = (
      'SELECT  ARTID, ART2ID, FULLART, ART2, ART, D_COMPID, PRODCODE,'
      '              D_MATID, D_GOODID, D_INSID'
      'FROM PRICEH_S(?D_COMPID1, ?D_MATID1, ?D_INSID1, ?D_GOODID1,'
      '?D_COMPID2, ?D_MATID2, ?D_INSID2, ?D_GOODID2)'
      'order by art')
    AfterPost = CommitRetaining
    BeforeOpen = quPriceBeforeOpen
    BeforePost = quPriceBeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    OnFilterRecord = quPriceFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 328
    Top = 96
    object quPriceARTID: TIntegerField
      FieldName = 'ARTID'
    end
    object quPriceART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quPriceART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quPriceART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quPriceD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quPricePRODCODE: TFIBStringField
      FieldName = 'APRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quPriceD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quPriceD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quPriceD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsPrice: TDataSource
    DataSet = quPrice
    Left = 330
    Top = 140
  end
  object quArt: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_ART'
      'SET ART=?ART,  '
      '       UNITID=?UNITID,'
      '       D_COMPID=?D_COMPID,'
      '       D_MATID=?D_MATID,'
      '       D_GOODID=?D_GOODID,'
      '       D_INSID=?D_INSID,'
      '       D_COUNTRYID=?D_COUNTRYID,'
      '       AVG_W=?AVG_W, '
      '       AVG_SPR=?AVG_SPR,'
      '       ATT1=:ATT1,'
      '       ATT2=:ATT2         '
      'WHERE D_ARTID=?OLD_D_ARTID')
    DeleteSQL.Strings = (
      'DELETE FROM D_ART'
      'WHERE D_ARTID= ?D_ARTID')
    InsertSQL.Strings = (
      'INSERT INTO D_ART(D_ARTID,  D_COMPID,  D_MATID,   D_GOODID, '
      
        '                                     ART,   UNITID,   D_INSID, D' +
        '_CountryID, AVG_W, AVG_SPR, ATT1, ATT2)'
      'VALUES(?D_ARTID,  ?D_COMPID,  ?D_MATID,   ?D_GOODID, '
      
        '                ?ART,   ?UNITID,  ?D_INSID, ?D_CountryID, ?AVG_W' +
        ', ?AVG_SPR, ?ATT1, ?ATT2)')
    RefreshSQL.Strings = (
      
        'SELECT D_ARTID,  D_COMPID,  D_MATID,  D_GOODID,  ART, D_COUNTRYI' +
        'D,'
      
        '               UNITID,     D_INSID ,  FULLART , RQ, RW, USERID, ' +
        'F1, F2, AVG_W, AVG_SPR, ATT1, ATT2'
      'FROM   A_R(?D_ARTID, ?USERID, ?F1, ?F2)'
      ' ')
    SelectSQL.Strings = (
      
        'SELECT D_ARTID,  D_COMPID,  D_MATID,  D_GOODID,  ART, D_COUNTRYI' +
        'D,'
      
        '               UNITID,     D_INSID ,  FULLART , RQ, RW, USERID, ' +
        'F1, F2, AVG_W, AVG_SPR, ATT1, ATT2,    VALATT1, VALATT2'
      'FROM   '
      'A_S'
      '(?DEPID, ?COMPID1, '
      
        ' ?MATID1, ?GOODID1, ?INSID1, ?COUNTRYID1, ?COMPID2, ?MATID2, ?GO' +
        'ODID2, ?INSID2, ?COUNTRYID2, '
      ' ?USERID, ?ART_1, ?ART_2, ?ATT1_1, ?ATT1_2, ?ATT2_1, ?ATT2_2)'
      'ORDER BY ART')
    AfterCancel = quArtAfterCancel
    AfterDelete = CommitRetaining
    AfterInsert = quArtAfterInsert
    AfterOpen = quArtAfterOpen
    AfterPost = quArtAfterPost
    AfterScroll = quArtAfterScroll
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeInsert = quArtBeforeInsert
    BeforeOpen = quArtBeforeOpen
    BeforePost = quArtBeforePost
    OnNewRecord = quArtNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1047#1072#1103#1074#1082#1072' (Appl), '#1055#1088#1080#1093#1086#1076' (SInv)'
    Left = 70
    Top = 4
    object quArtD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object quArtD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quArtD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quArtD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quArtART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quArtUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
      OnSetText = UNITIDSetText
    end
    object quArtD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quArtFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quArtRQ: TFloatField
      FieldName = 'RQ'
    end
    object quArtRW: TFloatField
      FieldName = 'RW'
      DisplayFormat = '0.##'
    end
    object quArtUSERID: TIntegerField
      FieldName = 'USERID'
    end
    object quArtF1: TSmallintField
      FieldName = 'F1'
    end
    object quArtF2: TSmallintField
      FieldName = 'F2'
    end
    object quArtD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Origin = 'A_S.D_COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quArtAVG_W: TFloatField
      FieldName = 'AVG_W'
      Origin = 'A_S.AVG_W'
      DisplayFormat = '0.##'
    end
    object quArtAVG_SPR: TFloatField
      FieldName = 'AVG_SPR'
      Origin = 'A_S.AVG_SPR'
      currency = True
    end
    object quArtATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quArtATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quArtVALATT1: TFIBStringField
      FieldName = 'VALATT1'
      EmptyStrToNull = True
    end
    object quArtVALATT2: TFIBStringField
      FieldName = 'VALATT2'
      EmptyStrToNull = True
    end
  end
  object dsArt: TDataSource
    AutoEdit = False
    DataSet = quArt
    OnDataChange = dsArtDataChange
    Left = 68
    Top = 52
  end
  object quDPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Art2'
      'set TOptPrice= :OptPrice'
      'where Art2Id= ?OLD_Art2Id')
    RefreshSQL.Strings = (
      
        'SELECT ARTID, ART2ID, FULLART, ART2, ART, PRICE, OPTPRICE, SPRIC' +
        'E, UNITID,'
      
        '              D_COMPID, D_MATID, D_GOODID, D_INSID, D_COUNTRYID,' +
        ' PRODCODE,'
      '              OPEQ, DEPID, ATT1, ATT2'
      'FROM DPRICE_R'
      '(?ART2ID, ?DEPID)')
    SelectSQL.Strings = (
      
        'SELECT ARTID, ART2ID, FULLART, ART2, ART, PRICE, OPTPRICE, SPRIC' +
        'E, UNITID,'
      
        '              D_COMPID, D_MATID, D_GOODID, D_INSID, D_COUNTRYID,' +
        ' PRODCODE,'
      '              OPEQ, DEPID, ATT1, ATT2'
      'FROM DRICE_S'
      '(?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?COUNTRYID1,'
      ' ?COMPID2, ?MATID2, ?GOODID2, ?INSID2, ?COUNTRYID2,'
      ' ?DEPID, ?ATT1_1, ?ATT1_2, ?ATT2_1, ?ATT2_2)'
      '')
    BeforeOpen = quDPriceBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1094#1077#1085#1099' (PHist)'
    Left = 372
    Top = 94
    object quDPriceARTID: TIntegerField
      FieldName = 'ARTID'
    end
    object quDPriceART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quDPriceART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quDPriceART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quDPricePRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quDPriceOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object quDPriceSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object quDPriceUNITID: TSmallintField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object quDPriceD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quDPriceD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDPriceD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDPriceD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDPriceD_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      DisplayWidth = 3
      FieldName = 'D_COUNTRYID'
      Origin = 'DRICE_S.D_COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDPricePRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quDPriceFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quDPriceOPEQ: TFloatField
      FieldName = 'OPEQ'
    end
    object quDPriceDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quDPriceATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quDPriceATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
  end
  object dsDPrice: TDataSource
    DataSet = quDPrice
    Left = 374
    Top = 142
  end
  object quARest: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.D_DepId DepId, d.SName Dep, count(*) rq, sum(si.w) rw'
      'from D_Dep d, SItem si, SEl e, SInv i, Art2 a2'
      'where i.DepID= d.D_DepId and'
      ''
      '      e.SInvId= i.SInvId and'
      '      a2.Art2Id=e.Art2Id and'
      '      a2.D_ArtId= ?D_ArtId and'
      '      si.SElId=e.SElId and'
      '      si.BusyType=0'
      'group by d.D_DepId, d.SName')
    BeforeOpen = quARestBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 240
    Top = 96
    object quARestDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quARestDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object quARestRW: TFloatField
      FieldName = 'RW'
    end
    object quARestRQ: TIntegerField
      FieldName = 'RQ'
    end
  end
  object dsARest: TDataSource
    DataSet = quARest
    Left = 240
    Top = 142
  end
  object quA2Rest: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.D_DepId DepId, d.SName Dep, count(*) rq, sum(si.w) rw'
      'from D_Dep d, SItem si, SEl e, SInv i'
      'where i.DepID= d.D_DepId and'
      ''
      '      e.SInvId= i.SInvId and'
      '      e.Art2Id= ?Art2Id and'
      '      si.SElId=e.SElId and'
      '      si.BusyType=0'
      'group by d.D_DepId, d.SName'
      '')
    BeforeOpen = quA2RestBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 422
    Top = 94
    object quA2RestDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quA2RestDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object quA2RestRW: TFloatField
      FieldName = 'RW'
    end
    object quA2RestRQ: TIntegerField
      FieldName = 'RQ'
    end
  end
  object dsA2Rest: TDataSource
    DataSet = quA2Rest
    Left = 422
    Top = 142
  end
  object dsEmp: TDataSource
    DataSet = quEmp
    Left = 476
    Top = 146
  end
  object quEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_EMPID, FIO'
      'FROM D_EMP'
      'WHERE D_DEPID=?DEPID')
    BeforeOpen = quEmpBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 474
    Top = 98
    object quEmpD_EMPID: TIntegerField
      FieldName = 'D_EMPID'
    end
    object quEmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object quFindUID: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT SItemId, SZ, W, UID, Art2Id, Art2, FullArt, UnitId, Price'
      'FROM FINDUID(?AUID, ?ADEPID)'
      '')
    SelectSQL.Strings = (
      'SELECT SItemId, SZ, W, UID, Art2Id, Art2, FullArt, UnitId, Price'
      'FROM FINDUID(?AUID, ?ADEPID)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 524
    Top = 100
    object quFindUIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quFindUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quFindUIDW: TFloatField
      FieldName = 'W'
    end
    object quFindUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quFindUIDART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quFindUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quFindUIDFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quFindUIDUNITID: TSmallintField
      FieldName = 'UNITID'
    end
    object quFindUIDPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
  end
  object dsFindUID: TDataSource
    DataSet = quFindUID
    Left = 526
    Top = 146
  end
  object taCurSell: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SELL'
      'SET N=?N,'
      '        EMP1ID=?EMP1ID,'
      '        EMP2ID=?EMP1ID,'
      '        ED=?ED,'
      '        CLOSED=?CLOSED,'
      '       RN=?RN,'
      '       FREG=:FREG'
      'WHERE SELLID=?OLD_SELLID')
    InsertSQL.Strings = (
      
        'INSERT INTO SELL(SELLID, BD, N, DEPID, EMP1ID, EMP2ID, ED, CLOSE' +
        'D, FREG)'
      
        'VALUES (?SELLID, '#39'NOW'#39', ?N, ?DEPID, ?EMP1ID, ?EMP1ID, ?ED, ?CLOS' +
        'ED, :FREG)')
    RefreshSQL.Strings = (
      'SELECT SELLID, BD, ED, DEPID, N, EMP1ID, EMP2ID, DEP,'
      
        '               CNT, W, COST,  RCNT, RW, RCOST, RN, CLOSED, FREG,' +
        ' costcard'
      'FROM CURSELL_R(?SELLID)')
    SelectSQL.Strings = (
      'SELECT SELLID, BD, ED, DEPID, N, EMP1ID, EMP2ID, DEP,'
      
        '               CNT, W, COST, RCNT, RW, RCOST, RN, CLOSED, FREG, ' +
        'costCard'
      'FROM CURSELL_S(?DepId, ?EmpID)')
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taCurSellBeforeOpen
    BeforePost = taCurSellBeforePost
    OnCalcFields = taCurSellCalcFields
    OnNewRecord = taCurSellNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 576
    Top = 98
    object taCurSellSELLID: TIntegerField
      FieldName = 'SELLID'
    end
    object taCurSellBD: TDateTimeField
      FieldName = 'BD'
    end
    object taCurSellED: TDateTimeField
      FieldName = 'ED'
    end
    object taCurSellDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taCurSellN: TIntegerField
      FieldName = 'N'
    end
    object taCurSellEMP1ID: TIntegerField
      FieldName = 'EMP1ID'
    end
    object taCurSellEMP2ID: TIntegerField
      FieldName = 'EMP2ID'
    end
    object taCurSellDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taCurSellCNT: TIntegerField
      FieldName = 'CNT'
    end
    object taCurSellW: TFloatField
      FieldName = 'W'
    end
    object taCurSellCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taCurSellRCNT: TIntegerField
      FieldName = 'RCNT'
    end
    object taCurSellRW: TFloatField
      FieldName = 'RW'
    end
    object taCurSellRCOST: TFloatField
      FieldName = 'RCOST'
      currency = True
    end
    object taCurSellCASS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CASS'
      currency = True
      Calculated = True
    end
    object taCurSellRN: TIntegerField
      FieldName = 'RN'
    end
    object taCurSellCLOSED: TSmallintField
      FieldName = 'CLOSED'
    end
    object taCurSellFREG: TSmallintField
      FieldName = 'FREG'
      Origin = 'CURSELL_S.FREG'
    end
    object taCurSellCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
      currency = True
    end
  end
  object dsCurSell: TDataSource
    DataSet = taCurSell
    Left = 580
    Top = 148
  end
  object quDistrPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute  procedure STUB 1')
    SelectSQL.Strings = (
      'SELECT  DEPID, DEP, PRICE2, T'
      'FROM DISTRPRICE_S(?ART2ID, ?USERID)'
      'ORDER BY DEP')
    BeforeClose = PostBeforeClose
    BeforeEdit = quDistrPriceBeforeEdit
    BeforeOpen = quDistrPriceBeforeOpen
    BeforePost = quDistrPriceBeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 602
    Top = 2
  end
  object dsDistrPrice: TDataSource
    DataSet = quDistrPrice
    Left = 604
    Top = 52
  end
  object pmOptList: TPopupMenu
    AutoHotkeys = maManual
    Left = 195
    Top = 330
    object MenuItem13: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = OptDepClick
    end
    object MenuItem14: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object taOptList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SINV'
      'SET DEPFROMID=?DEPFROMID,'
      '    COMPID=?COMPID,'
      '    SDATE=?SDATE,'
      '    SN=?SN,'
      '    PMARGIN=?PMARGIN,'
      '    TR=?TR,'
      '    AKCIZ=?AKCIZ,'
      '    ISCLOSED=?ISCLOSED,'
      '    OPT=?OPT'
      'WHERE SINVID=?OLD_SINVID '
      ' ')
    DeleteSQL.Strings = (
      'DELETE FROM SInv'
      'WHERE SINVID=?OLD_SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      '(SINVID, DEPFROMID, SDATE, SN, PMARGIN, TR, AKCIZ,'
      ' ISCLOSED, COMPID, ITYPE, OPT)'
      'VALUES'
      '(?SINVID, ?DEPFROMID, ?SDATE, ?SN, ?PMARGIN, ?TR, ?AKCIZ,'
      ' ?ISCLOSED, ?COMPID, 3, ?OPT)'
      ''
      ' ')
    RefreshSQL.Strings = (
      
        'SELECT SINVID, DEPFROMID, SDATE, SN, PMARGIN, NDS, TR, COST, AKC' +
        'IZ,'
      
        '       ISCLOSED, DEPFROM, COMPID, COMP, SCOST, SUPID, OPT, ITYPE' +
        ', Q, W'
      'FROM OPTLIST_R(?SINVID)')
    SelectSQL.Strings = (
      
        'SELECT SINVID, DEPFROMID, SDATE,  SN, PMARGIN, NDS, TR, COST, AK' +
        'CIZ,'
      '       ISCLOSED, DEPFROM, COMPID, COMP, SCOST, SUPID, OPT,'
      '       ITYPE, Q, W'
      'FROM OPTLIST_S(?DEPID1, ?DEPID2, ?BD, ?ED)'
      'ORDER BY SDATE DESC')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taOptListBeforeEdit
    BeforeInsert = taOptListBeforeInsert
    BeforeOpen = taOptListBeforeOpen
    BeforePost = taOptListBeforePost
    OnNewRecord = taOptListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 634
    Top = 200
    object taOptListSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taOptListDEPFROMID: TIntegerField
      FieldName = 'DEPFROMID'
    end
    object taOptListSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taOptListSN: TIntegerField
      FieldName = 'SN'
    end
    object taOptListPMARGIN: TFloatField
      FieldName = 'PMARGIN'
    end
    object taOptListNDS: TFloatField
      FieldName = 'NDS'
      currency = True
    end
    object taOptListTR: TFloatField
      FieldName = 'TR'
      currency = True
    end
    object taOptListCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taOptListAKCIZ: TFloatField
      FieldName = 'AKCIZ'
      currency = True
    end
    object taOptListISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taOptListDEPFROM: TFIBStringField
      FieldName = 'DEPFROM'
      Size = 60
      EmptyStrToNull = True
    end
    object taOptListCOMPID: TIntegerField
      FieldName = 'COMPID'
    end
    object taOptListCOMP: TFIBStringField
      FieldName = 'COMP'
      Size = 60
      EmptyStrToNull = True
    end
    object taOptListSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object taOptListSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taOptListOPT: TSmallintField
      FieldName = 'OPT'
      Origin = 'OPTLIST_S.OPT'
    end
    object taOptListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'OPTLIST_S.ITYPE'
    end
    object taOptListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'OPTLIST_S.Q'
    end
    object taOptListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'OPTLIST_S.W'
      DisplayFormat = '0.##'
    end
  end
  object dsOptList: TDataSource
    DataSet = taOptList
    Left = 630
    Top = 242
  end
  object pmRetList: TPopupMenu
    AutoHotkeys = maManual
    Left = 15
    Top = 374
    object MenuItem15: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
    end
    object MenuItem16: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object quUIDWH: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure UIDWHEdt2'
      '(?goodsid1, ?OLD_goodsid1,'
      '?goodsid2, ?OLD_goodsid2,'
      '?UID, ?OLD_UID,'
      '?SZ, ?OLD_SZ,'
      '?W, ?OLD_W,'
      '?SSF0, ?OLD_SSF0,'
      '?SN0, ?OLD_SN0,'
      '?SPRICE0, ?OLD_SPRICE0,'
      '?SDATE0, ?OLD_SDATE0,'
      '?NDATE, ?OLD_NDATE,'
      '?SUPID0, ?OLD_SUPUID0,'
      '?ART2ID, ?OLD_ART2ID,'
      '?NDSID, ?OLD_NDSID,'
      '?PRICE, ?OLD_PRICE,'
      '?APPL_Q, ?OLD_APPL_Q,'
      '?USERID, ?USERID_WH,'
      '?APPLDEP_Q, ?OLD_APPLDEP_Q)')
    RefreshSQL.Strings = (
      
        'SELECT u.SITEMID, u.UID, a2.D_COMPID COMPID, a2.D_MATID MATID, a' +
        '2.D_GOODID GOODID,'
      
        '       a2.D_INSID INSID, a2.ART, a2.ART2, a2.s_ins, a2.UNITID,  ' +
        'u.NDSID, u.W, u.SZ,'
      
        '       FIF(a2.UnitId, u.Price*u.W, u.Price) COST, u.ART2ID ART2I' +
        'D, u.DEPID, u.SN0,'
      
        '       u.SSF0, u.SPRICE0, u.SUPID0, u.SDATE0, u.SN, u.SDATE, onl' +
        'ydate(u.SELLDATE)'
      
        '       SELLDATE, u.T, u.ISCLOSED, u.NDATE, u.ITYPE, u.PRODID,  a' +
        '2.PRODCODE ,'
      
        '       c2.Code SUPCODE, c2.Name SupName, u.PRICE, 1 USERID, u.Re' +
        'tDate, d.SName Dep,'
      
        '       d.Color,a2.D_ARTID,FIF(a2.UnitId, u.SPrice0*u.W, u.SPrice' +
        '0) COST1,'
      
        '       A2.d_countryid, u.UIDWHID, u.NAmeDepFrom, u.colordf, u.go' +
        'ods1, u.goods2,'
      
        '       u.goodsid1, u.goodsid2, a2.ATT1, a2.ATT2, at1.name NameAt' +
        't1, '
      
        '       at2.name NameAtt2, dg.name NameGood, u.Appl_Q, u.USERID U' +
        'SERID_WH, '
      '       u.ApplDep_q, u.ApplDep_user, u.priceInv,'
      '       FIF(a2.UnitId, u.Priceinv*u.W, u.Price) COSTInv,c2.ndog'
      
        'FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 ' +
        'at2, d_good dg'
      'WHERE u.uidwhid= ?uidwhid and'
      '      d.D_DepId=u.DepId and'
      '      a2.Art2Id=u.Art2Id and'
      '      c2.D_CompId=u.SupId0 and'
      '      at1.id = a2.att1 and at2.id = a2.att2 and'
      '      dg.d_goodid = a2.d_goodid'
      '')
    SelectSQL.Strings = (
      
        'SELECT u.SITEMID, u.UID, a2.D_COMPID COMPID, a2.D_MATID MATID, a' +
        '2.D_GOODID GOODID, a2.D_INSID INSID, a2.ART, a2.ART2, a2.s_ins, ' +
        'a2.UNITID,  u.NDSID, u.W, u.SZ,'
      
        '       FIF(a2.UnitId, u.Price*u.W, u.Price) COST, u.ART2ID ART2I' +
        'D, u.DEPID, u.SN0, u.SSF0, u.SPRICE0, u.SUPID0, u.SDATE0, u.SN, ' +
        'u.SDATE, onlydate(u.SELLDATE) SELLDATE,'
      
        '       u.T, u.ISCLOSED, u.NDATE, u.ITYPE, u.PRODID,  a2.PRODCODE' +
        ' , c2.Code SUPCODE, c2.Name SupName, u.PRICE, 1 USERID, u.RetDat' +
        'e,'
      
        '       d.SName Dep, d.Color,a2.D_ARTID,FIF(a2.UnitId, u.SPrice0*' +
        'u.W, u.SPrice0) COST1, A2.d_countryid, u.UIDWHID, u.NAmeDepFrom,' +
        ' u.colordf, u.goods1, u.goods2,'
      
        '       u.goodsid1, u.goodsid2, a2.ATT1, a2.ATT2, at1.name NameAt' +
        't1, at2.name NameAtt2, dg.name NameGood, u.Appl_Q, u.USERID USER' +
        'ID_WH, u.ApplDep_q, u.ApplDep_user, u.priceInv, u.Priceinv*u.q0 ' +
        'COSTInv, a2.d_compid ProducerID, c2.NDOg, u.Flag, u.euid'
      
        'FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 ' +
        'at2, d_good dg'
      'WHERE          u.USERID= ?USERID and'
      '               u.DEPID BETWEEN ?DEPID1 AND ?DEPID2 and'
      ''
      '               a2.D_MATID BETWEEN :MATID1 and :MATID2 and'
      ''
      '               a2.D_GOODID BETWEEN :GOODID1 and :GOODID2 and'
      ''
      '               a2.D_COMPID between :COMPID1 and :COMPID2 and'
      '               d.D_DepId=u.DepId and'
      '               a2.Art2Id=u.Art2Id and'
      '               c2.D_CompId=u.SupId0  and '
      '               a2.Art between :ART1 and :ART2 and'
      ''
      '               u.SupId0 between :SUPID1 and :SUPID2 and'
      
        '               u.goodsid1 between :goodsid1_1 and :goodsid1_2 an' +
        'd'
      
        '               u.goodsid2 between :goodsid2_1 and :goodsid2_2 an' +
        'd'
      ''
      '               a2.ATT1 between :ATT1_1 and :ATT1_2 and'
      '               a2.ATT2 between :ATT2_1 and :ATT2_2 and'
      '               at1.id = a2.att1 and at2.id = a2.att2 and     '
      '               dg.d_goodid = a2.d_goodid and'
      ''
      ''
      ''
      ''
      '               u.SZ BETWEEN :SZ1 and :SZ2 and'
      ''
      ''
      ''
      '               a2.Art2 between :ART21 and :ART22'
      ''
      ''
      ' '
      ''
      'Order by a2.art, u.SZ, a2.art2')
    AfterOpen = quUIDWHAfterOpen
    AfterPost = quUIDWHAfterPost
    AfterScroll = quUIDWHAfterScroll
    BeforeClose = quUIDWHBeforeClose
    BeforeOpen = quUIDWHBeforeOpen
    BeforePost = quUIDWHBeforePost
    OnCalcFields = quUIDWHCalcFields
    Transaction = dmCom.tr1
    Database = dmCom.db
    OnFilterRecord = quUIDWHFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 236
    Top = 288
    object quUIDWHSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quUIDWHUID: TIntegerField
      DisplayLabel = #1048#1076'.'#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object quUIDWHMATID: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHGOODID: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084'.'
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHINSID: TFIBStringField
      DisplayLabel = #1054#1042
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quUIDWHART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083' 2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quUIDWHUNITID: TIntegerField
      DisplayLabel = #1045#1048
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object quUIDWHNDSNAME: TFIBStringField
      DisplayLabel = #1053#1044#1057
      FieldKind = fkLookup
      FieldName = 'NDSNAME'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      EmptyStrToNull = True
      Lookup = True
    end
    object quUIDWHNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quUIDWHPRICE: TFloatField
      DisplayLabel = #1056#1072#1089'. '#1094#1077#1085#1072
      FieldName = 'PRICE'
      currency = True
    end
    object quUIDWHW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quUIDWHSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084'.'
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHDEP: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEP'
      LookupDataSet = dmCom.quDep
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'SNAME'
      KeyFields = 'DEPID'
      EmptyStrToNull = True
    end
    object quUIDWHCOST: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      currency = True
    end
    object quUIDWHART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quUIDWHDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quUIDWHCOLOR: TIntegerField
      FieldName = 'COLOR'
      LookupDataSet = dmCom.quDep
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'COLOR'
      KeyFields = 'DEPID'
    end
    object quUIDWHPRODCODE: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHSUPCODE: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      FieldName = 'SUPCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHSN: TIntegerField
      DisplayLabel = #1042#1055
      FieldName = 'SN'
    end
    object quUIDWHSN0: TIntegerField
      DisplayLabel = #1042#1085#1091#1090'.'#8470
      FieldName = 'SN0'
    end
    object quUIDWHSSF0: TFIBStringField
      DisplayLabel = #1042#1085#1077#1096'.'#8470
      FieldName = 'SSF0'
      EmptyStrToNull = True
    end
    object quUIDWHSPRICE0: TFloatField
      DisplayLabel = #1055#1088'.'#1094#1077#1085#1072
      FieldName = 'SPRICE0'
      currency = True
    end
    object quUIDWHSUPID0: TIntegerField
      FieldName = 'SUPID0'
    end
    object quUIDWHSDATE0: TDateTimeField
      DisplayLabel = #1042#1085#1091#1090'.'#1076#1072#1090#1072
      FieldName = 'SDATE0'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDWHSDATE: TDateTimeField
      DisplayLabel = #1042#1055' '#1076#1072#1090#1072
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDWHSELLDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'SELLDATE'
      OnGetText = quUIDWHSELLDATEGetText
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDWHT: TSmallintField
      FieldName = 'T'
    end
    object quUIDWHISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object quUIDWHNDATE: TDateTimeField
      DisplayLabel = #1042#1085#1077#1096'.'#1076#1072#1090#1072
      FieldName = 'NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDWHMemo: TStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldKind = fkCalculated
      FieldName = 'Memo'
      Size = 120
      Calculated = True
    end
    object quUIDWHITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quUIDWHPRODID: TIntegerField
      FieldName = 'PRODID'
    end
    object quUIDWHUSERID: TIntegerField
      FieldName = 'USERID'
      Required = True
    end
    object quUIDWHRETDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'RETDATE'
      Origin = 'UIDWH_T.RETDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDWHD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'ART2.D_ARTID'
    end
    object quUIDWHCOST1: TFloatField
      FieldName = 'COST1'
      Required = True
    end
    object quUIDWHS_INS: TFIBStringField
      DisplayLabel = #1042#1089#1090'.'
      DisplayWidth = 20
      FieldName = 'S_INS'
      Origin = 'ART2.S_INS'
      Size = 200
      EmptyStrToNull = True
    end
    object quUIDWHD_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      DisplayWidth = 3
      FieldName = 'D_COUNTRYID'
      Origin = 'ART2.D_COUNTRYID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHCOMPID: TIntegerField
      FieldName = 'COMPID'
      Origin = 'ART2.D_COMPID'
      Required = True
    end
    object quUIDWHUIDWHID: TIntegerField
      FieldName = 'UIDWHID'
      Origin = 'UIDWH_T.UIDWHID'
      Required = True
    end
    object quUIDWHNAMEDEPFROM: TFIBStringField
      FieldName = 'NAMEDEPFROM'
      Origin = 'UIDWH_T.NAMEDEPFROM'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDWHCOLORDF: TIntegerField
      FieldName = 'COLORDF'
      Origin = 'UIDWH_T.COLORDF'
    end
    object quUIDWHSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074'.'
      FieldName = 'SUPNAME'
      Origin = 'D_COMP.SNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDWHGOODS1: TFIBStringField
      FieldName = 'GOODS1'
      Origin = 'UIDWH_T.GOODS1'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHGOODS2: TFIBStringField
      FieldName = 'GOODS2'
      Origin = 'UIDWH_T.GOODS2'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDWHPRODNAME: TStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldKind = fkLookup
      FieldName = 'PRODNAME'
      LookupDataSet = dmCom.quComp
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'SNAME'
      KeyFields = 'COMPID'
      Lookup = True
    end
    object quUIDWHGOODSID1: TIntegerField
      FieldName = 'GOODSID1'
      Origin = 'UIDWH_T.GOODSID1'
    end
    object quUIDWHGOODSID2: TIntegerField
      FieldName = 'GOODSID2'
      Origin = 'UIDWH_T.GOODSID2'
    end
    object quUIDWHATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quUIDWHATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quUIDWHNAMEATT1: TFIBStringField
      FieldName = 'NAMEATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object quUIDWHNAMEATT2: TFIBStringField
      FieldName = 'NAMEATT2'
      Size = 80
      EmptyStrToNull = True
    end
    object quUIDWHNAMEGOOD: TFIBStringField
      FieldName = 'NAMEGOOD'
      EmptyStrToNull = True
    end
    object quUIDWHAPPL_Q: TFIBIntegerField
      FieldName = 'APPL_Q'
      OnChange = quUIDWHAPPL_QChange
    end
    object quUIDWHUSERID_WH: TFIBIntegerField
      FieldName = 'USERID_WH'
    end
    object quUIDWHAPPLDEP_Q: TFIBIntegerField
      FieldName = 'APPLDEP_Q'
      OnChange = quUIDWHAPPLDEP_QChange
    end
    object quUIDWHAPPLDEP_USER: TFIBIntegerField
      FieldName = 'APPLDEP_USER'
    end
    object quUIDWHPRICEINV: TFIBFloatField
      FieldName = 'PRICEINV'
      currency = True
    end
    object quUIDWHCOSTINV: TFIBFloatField
      FieldName = 'COSTINV'
      currency = True
    end
    object quUIDWHProducerID: TIntegerField
      FieldName = 'ProducerID'
    end
    object quUIDWHDepartmentName: TStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldKind = fkLookup
      FieldName = 'DepartmentName'
      LookupDataSet = dmCom.quDep
      LookupKeyFields = 'APPLDEPNUM'
      LookupResultField = 'DEPARTMENT$NAME'
      KeyFields = 'APPLDEP_Q'
      Size = 32
      Lookup = True
    end
    object quUIDWHNDOG: TFIBStringField
      DisplayWidth = 10
      FieldName = 'NDOG'
      EmptyStrToNull = True
    end
    object quUIDWHTollingPrice: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'Tolling$Price'
      Calculated = True
    end
    object quUIDWHFLAG: TIntegerField
      FieldName = 'FLAG'
    end
    object quUIDWHFLAGNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'FLAG$NAME'
      Size = 16
      Calculated = True
    end
    object quUIDWHEUID: TFIBStringField
      FieldName = 'EUID'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object dsUIDWH: TDataSource
    DataSet = quUIDWH
    Left = 268
    Top = 328
  end
  object quDst: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SItem'
      'where SItemId= ?OLD_SItemId')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'select d.SName Dep,  i.IsClosed, si.UID,si.SItemID,  si.W, '
      '          si.SZ, i.DepId, si.Ref, d.Color, n.Name NDS'
      'from SItem SI, SEl e, SInv i, D_Dep d, SINfo sin, D_NDS n'
      'where si.SItemId=?SItemId and'
      '      si.SElId=e.SElId and'
      '      e.SInvId=i.SInvId and'
      '      d.D_DepId=i.DepId and'
      '      sin.SInfoId=si.SInfoId and'
      '      n.NDSID=sin.NDSID')
    SelectSQL.Strings = (
      'select d.SName Dep,  i.IsClosed, si.UID,si.SItemID,  si.W, '
      '          si.SZ, i.DepId, si.Ref, d.Color, n.Name NDS'
      'from SItem SI, SEl e, SInv i, D_Dep d, SINfo sin, D_NDS n'
      'where i.DepId<>?DepFromId and '
      '           i.DepId between ?DepId1 and ?DepId2 and'
      '           e.SInvId=i.SInvId and'
      '           e.Art2Id=?Art2Id and'
      '           si.SElId=e.SElId and'
      '           si.BusyType=0 and'
      '           d.D_DepId=i.DepId and'
      '           sin.SInfoId=si.SInfoId and'
      '           n.NDSID=sin.NDSID'
      ''
      'order by d.SName, i.IsClosed, si.UID')
    BeforeDelete = quDstBeforeDelete
    BeforeOpen = quDstBeforeOpen
    BeforePost = quDstBeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 148
    Top = 96
    object quDstSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Required = True
    end
    object quDstUID: TIntegerField
      FieldName = 'UID'
    end
    object quDstW: TFloatField
      FieldName = 'W'
    end
    object quDstSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quDstDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quDstISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object quDstDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quDstCOLOR: TIntegerField
      FieldName = 'COLOR'
    end
    object quDstREF: TIntegerField
      FieldName = 'REF'
    end
    object quDstNDS: TFIBStringField
      FieldName = 'NDS'
      Origin = 'D_NDS.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsDst: TDataSource
    DataSet = quDst
    Left = 148
    Top = 140
  end
  object quDistred: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  PRODCODE, D_COMPID, D_MATID, D_GOODID,  D_INSID, '
      '               ART, ART2, SZ ,Q,  W, PRICE, ART2ID, NDS'
      'FROM DISTRED_S(?SINVID)'
      'ORDER BY ART')
    BeforeOpen = quDistredBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 192
    Top = 96
    object quDistredPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quDistredD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quDistredD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDistredD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDistredD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quDistredART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quDistredART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quDistredSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quDistredQ: TIntegerField
      FieldName = 'Q'
    end
    object quDistredW: TFloatField
      FieldName = 'W'
    end
    object quDistredPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quDistredART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quDistredNDS: TFIBStringField
      FieldName = 'NDS'
      Origin = 'DISTRED_S.NDS'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsDistred: TDataSource
    DataSet = quDistred
    Left = 192
    Top = 140
  end
  object quD_Q: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure DST_U ?ART2ID, ?DEPID, ?PRICE')
    RefreshSQL.Strings = (
      'SELECT ART2ID, DEPID, DEP, Q1, W1, Q2, W2, PRICE, PEQ'
      'FROM DST_R (?ART2ID, ?DEPID, ?DEP, ?Q1, ?W1, ?Q2, ?W2)'
      '')
    SelectSQL.Strings = (
      'select Art2Id, DepId, Dep, Q1, W1, Q2,  W2, Price, PEQ'
      'from Dst_S (?Art2Id, ?DepFromId)')
    BeforeOpen = quD_QBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 360
    Top = 288
    object quD_QDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quD_QDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quD_QQ1: TIntegerField
      FieldName = 'Q1'
    end
    object quD_QW1: TFloatField
      FieldName = 'W1'
    end
    object quD_QQ2: TIntegerField
      FieldName = 'Q2'
    end
    object quD_QW2: TFloatField
      FieldName = 'W2'
    end
    object quD_QPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quD_QART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quD_QPEQ: TSmallintField
      FieldName = 'PEQ'
    end
  end
  object dsD_Q: TDataSource
    DataSet = quD_Q
    Left = 360
    Top = 332
  end
  object quPrepUIDWH: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select UIDWHCALC, UIDWHDATE'
      'from D_Rec')
    Left = 248
    Top = 472
  end
  object dsSelled: TDataSource
    DataSet = quSelled
    Left = 408
    Top = 332
  end
  object quSListT: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select  sum(C) Cost, cast(sum(Q) as integer) Q, sum(W) W, sum(Tr' +
        ') Tr'
      'from SInv I'
      'where i.IType=1 and'
      '           i.SDate between :BD AND :ED and'
      '           i.DepId between :DepId1 and :DepId2')
    BeforeOpen = taSListBeforeOpen
    OnCalcFields = quSListTCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 360
    Top = 376
    object quSListTCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quSListTQ: TIntegerField
      FieldName = 'Q'
      Required = True
      DisplayFormat = ',0'
    end
    object quSListTW: TFloatField
      FieldName = 'W'
      DisplayFormat = ',#.##'
    end
    object quSListTTR: TFloatField
      FieldName = 'TR'
      currency = True
    end
    object quSListTCNOTR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CNOTR'
      currency = True
      Calculated = True
    end
  end
  object dsSListT: TDataSource
    DataSet = quSListT
    Left = 360
    Top = 424
  end
  object quDListT: TpFIBDataSet
    SelectSQL.Strings = (
      'select   sum(C) Cost,'
      '         sum(SC) SCost,'
      '         cast(sum(Q) as integer) Q,'
      '         sum(W) W'
      'from  SInv I'
      'where IType=2 and'
      '      SDate between ?BD AND ?ED and'
      '      DepId between ?DepToId1 and ?DepToId2 and'
      '      DepFromId between ?DepFromId1 and ?DepFromId2')
    BeforeOpen = taDListBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 404
    Top = 376
    object quDListTCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quDListTSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object quDListTQ: TIntegerField
      FieldName = 'Q'
      Required = True
    end
    object quDListTW: TFloatField
      FieldName = 'W'
      DisplayFormat = '#.##'
    end
  end
  object dsDListT: TDataSource
    DataSet = quDListT
    Left = 408
    Top = 424
  end
  object quPrSI: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  PRSIID, SITEMID,  UID,  W,  SZ, SDATE, SUP, SN,'
      
        '               SUPID,  SPRICE, NEWPRICE, OLDPRICE, newcost, oldc' +
        'ost'
      'FROM PRSI_S( ?PRORDITEMID)'
      'ORDER BY UID')
    BeforeOpen = quPrSIBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 460
    Top = 288
    object quPrSIPRSIID: TIntegerField
      FieldName = 'PRSIID'
    end
    object quPrSISITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quPrSIUID: TIntegerField
      FieldName = 'UID'
    end
    object quPrSIW: TFloatField
      FieldName = 'W'
    end
    object quPrSISZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quPrSISDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quPrSISUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quPrSISN: TIntegerField
      FieldName = 'SN'
    end
    object quPrSISUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quPrSISPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object quPrSINEWPRICE: TFloatField
      FieldName = 'NEWPRICE'
      Origin = 'PRSI_S.NEWPRICE'
      currency = True
    end
    object quPrSIOLDPRICE: TFloatField
      FieldName = 'OLDPRICE'
      Origin = 'PRSI_S.OLDPRICE'
      currency = True
    end
    object quPrSINEWCOST: TFIBFloatField
      FieldName = 'NEWCOST'
      currency = True
    end
    object quPrSIOLDCOST: TFIBFloatField
      FieldName = 'OLDCOST'
      currency = True
    end
  end
  object dsPrSI: TDataSource
    DataSet = quPrSI
    Left = 460
    Top = 340
  end
  object pmSRetList: TPopupMenu
    AutoHotkeys = maManual
    Left = 187
    Top = 374
  end
  object taSRetList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SInv'
      'set   '
      '  CompID = :CompID, '
      '  SDate = :SDate,'
      '  Sn = :Sn,'
      '  PMargin = :PMargin,'
      '  TR = :TR,'
      '  Akciz = :Akciz,'
      '  IsClosed = :IsClosed,'
      '  PTR=:PTR,'
      '  PTRNDS=:PTRNDS,'
      '  TRNDS=:TRNDS,'
      '  Ret_NoNDS = :Ret_NoNDS,'
      '  Gruzootprav_ID = :Gruzootprav_ID,'
      '  Proizvoditel_ID = :Proizvoditel_ID,'
      '  contract$id= :contract$id'
      'where'
      '  SInvID = :Old_SInvID'
      ' ')
    DeleteSQL.Strings = (
      'delete from SInv'
      'where SInvID = :SInvID'
      '')
    InsertSQL.Strings = (
      'insert into SInv('
      '  SInvID,'
      '  DepFromID,'
      '  SDate,'
      '  Sn,'
      '  PMargin,'
      '  TR,'
      '  Akciz,'
      '  IsClosed,'
      '  CompID,'
      '  IType,'
      '  UserID,'
      '  TR,'
      '  PTR,'
      '  PTRNDS,'
      '  TRNDS,'
      '  Ret_NoNDS,'
      '  Gruzootprav_ID,'
      '  Proizvoditel_ID)'
      'values('
      '  :SInvID,'
      '  :DepFromID,'
      '  :SDate,'
      '  :Sn,'
      '  :PMargin,'
      '  :TR,'
      '  :Akciz,'
      '  :IsClosed,'
      '  :CompID,'
      '  7,'
      '  :UserID,'
      '  :TR,'
      '  :PTR,'
      '  :PTRNDS,'
      '  :TRNDS,'
      '  :Ret_NoNDS,'
      '  :Gruzootprav_ID,'
      '  :Proizvoditel_ID)'
      ''
      ' ')
    RefreshSQL.Strings = (
      'select'
      '  SInvID, '
      '  DepFromID, '
      '  SDate, '
      '  Sn, '
      '  PMargin, '
      '  TR,'
      '  Cost, '
      '  R_Count, '
      '  AKciz, '
      '  IsClosed, '
      '  DepFrom, '
      '  CompID, '
      '  Comp,'
      '  UserID, '
      '  FIO, '
      '  Def_CDM, '
      '  W, '
      '  PTR, '
      '  PTRNDS, '
      '  TRNDS, '
      '  Ret_NoNDS,'
      '  Gruzootprav_ID,'
      '  Gruzootprav,'
      '  Proizvoditel_ID,'
      '  Proizvoditel,'
      '  contract$id,'
      '  contract$class$id,'
      '  contract$class$name'
      'from SRetList_R(:SInvID)')
    SelectSQL.Strings = (
      'select'
      '  SInvID, '
      '  DepFromID, '
      '  SDate, '
      '  Sn, '
      '  PMargin, '
      '  TR,'
      '  Cost, '
      '  R_Count, '
      '  AKciz, '
      '  IsClosed, '
      '  DepFrom, '
      '  CompID, '
      '  Comp,'
      '  UserID, '
      '  FIO, '
      '  Def_CDM, '
      '  W, '
      '  PTR, '
      '  PTRNDS, '
      '  TRNDS, '
      '  Ret_NoNDS,'
      '  Gruzootprav_ID,'
      '  Gruzootprav,'
      '  Proizvoditel_ID,'
      '  Proizvoditel,'
      '  contract$id,'
      '  contract$class$id,'
      '  contract$class$name'
      'from SRetList_S(:DepID1, :DepID2, :BD, :ED)'
      'order by '
      '  SDate desc')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = BeforeDelete
    BeforeEdit = taSRetListBeforeEdit
    BeforeOpen = taSRetListBeforeOpen
    BeforePost = taSRetListBeforePost
    OnCalcFields = taSRetListCalcFields
    OnNewRecord = taSRetListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 692
    Top = 192
    object taSRetListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taSRetListDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
    object taSRetListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taSRetListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object taSRetListPMARGIN: TFIBFloatField
      FieldName = 'PMARGIN'
    end
    object taSRetListTR: TFIBFloatField
      DisplayLabel = #1058#1056
      FieldName = 'TR'
      DisplayFormat = '0.##'
    end
    object taSRetListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taSRetListR_COUNT: TFIBIntegerField
      FieldName = 'R_COUNT'
    end
    object taSRetListAKCIZ: TFIBFloatField
      FieldName = 'AKCIZ'
    end
    object taSRetListISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object taSRetListDEPFROM: TFIBStringField
      FieldName = 'DEPFROM'
      Size = 60
      EmptyStrToNull = True
    end
    object taSRetListCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taSRetListCOMP: TFIBStringField
      FieldName = 'COMP'
      Size = 60
      EmptyStrToNull = True
    end
    object taSRetListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taSRetListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSRetListDEF_CDM: TFIBSmallIntField
      FieldName = 'DEF_CDM'
    end
    object taSRetListW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taSRetListPTR: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090' '#1058#1056
      FieldName = 'PTR'
      DisplayFormat = '0.##'
    end
    object taSRetListPTRNDS: TFIBFloatField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1053#1044#1057' '#1058#1056
      FieldName = 'PTRNDS'
      DisplayFormat = '0.##'
    end
    object taSRetListTRNDS: TFIBFloatField
      DisplayLabel = #1058#1056' '#1053#1044#1057
      FieldName = 'TRNDS'
      DisplayFormat = '0.##'
    end
    object taSRetListRET_NONDS: TFIBSmallIntField
      FieldName = 'RET_NONDS'
    end
    object taSRetListCountClose: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CountClose'
      Calculated = True
    end
    object taSRetListCountOpen: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CountOpen'
      Calculated = True
    end
    object taSRetListCostClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostClose'
      currency = True
      Calculated = True
    end
    object taSRetListCostOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostOpen'
      currency = True
      Calculated = True
    end
    object taSRetListQClose: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'QClose'
      Calculated = True
    end
    object taSRetListQOpen: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'QOpen'
      Calculated = True
    end
    object taSRetListWClose: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'WClose'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taSRetListWOpen: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'WOpen'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taSRetListGruzootprav_ID: TIntegerField
      FieldName = 'Gruzootprav_ID'
    end
    object taSRetListProizvoditel_ID: TIntegerField
      FieldName = 'Proizvoditel_ID'
    end
    object taSRetListGruzootprav: TStringField
      FieldName = 'Gruzootprav'
      Size = 60
    end
    object taSRetListProizvoditel: TStringField
      FieldName = 'Proizvoditel'
      Size = 60
    end
    object taSRetListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACT$ID'
    end
    object taSRetListCONTRACTCLASSID: TFIBIntegerField
      FieldName = 'CONTRACT$CLASS$ID'
    end
    object taSRetListCONTRACTCLASSNAME: TFIBStringField
      FieldName = 'CONTRACT$CLASS$NAME'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object dsSRetList: TDataSource
    DataSet = taSRetList
    Left = 692
    Top = 236
  end
  object taSRet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SRET'
      'SET  UID=?UID,  '
      '        W=?W,  '
      '        SZ=?SZ,  '
      '        SPRICE=?SPRICE,  '
      '        SDATE=?SDATE, '
      '        SUPID=?SUPID,  '
      '        SSF=?SSF, '
      '        SN=?SN, '
      '        NDATE=?NDATE,  '
      '        NDSID=?NDSID, '
      '        D_RETID=?D_RETID'
      'WHERE SRETID= ?OLD_SRETID')
    DeleteSQL.Strings = (
      'DELETE FROM SRET'
      'WHERE SRETID= ?OLD_SRETID')
    InsertSQL.Strings = (
      'INSERT INTO SRET'
      
        ' (SRETID, SINVID,  ART2ID, SITEMID, UID,  W,  SZ,  SPRICE,  SDAT' +
        'E, '
      '  SUPID,  SSF, SN, NDATE,  NDSID, D_RETID)'
      'VALUES'
      
        ' (?SRETID, ?SINVID,  ?ART2ID, ?SITEMID, ?UID, ?W, ?SZ, ?SPRICE, ' +
        '?SDATE,'
      '  ?SUPID, ?SSF, ?SN, ?NDATE,  ?NDSID, ?D_RETID)'
      ''
      '')
    RefreshSQL.Strings = (
      'SELECT  SRETID, SINVID,  ART2ID, SITEMID, UID,  W,'
      '                SZ, Q0,  SPRICE, COST, SDATE, SUPID,'
      '                SSF, SN, NDATE,  NDSID, D_RETID, PRODID,'
      '                PROD, GOODID, INSID, MATID,  ART,  ART2,'
      '               SUP, UNITID'
      'FROM SRET_R(?SRETID)'
      'ORDER BY SRETID')
    SelectSQL.Strings = (
      'SELECT SRETID, SINVID,  ART2ID, SITEMID, UID,  W,'
      '                SZ, Q0,  SPRICE, COST, SDATE, SUPID,'
      '                SSF, SN, NDATE,  NDSID, D_RETID, PRODID,'
      '                PROD, GOODID, INSID, MATID,  ART,  ART2,'
      '               SUP, UNITID'
      'FROM SRET_S(?INVID)'
      'ORDER BY SRETID')
    AfterDelete = taSRetAfterDelete
    AfterOpen = taSRetAfterOpen
    AfterPost = taSRetAfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taSRetBeforeEdit
    BeforeInsert = taSRetBeforeInsert
    BeforeOpen = taSRetBeforeOpen
    BeforePost = taSRetBeforePost
    OnCalcFields = taSRetCalcFields
    OnNewRecord = taSRetNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 744
    Top = 184
    oCacheCalcFields = True
    object taSRetSRETID: TIntegerField
      FieldName = 'SRETID'
    end
    object taSRetSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taSRetART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taSRetSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taSRetUID: TIntegerField
      FieldName = 'UID'
    end
    object taSRetW: TFloatField
      FieldName = 'W'
    end
    object taSRetSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taSRetQ0: TFloatField
      FieldName = 'Q0'
    end
    object taSRetSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object taSRetCOST: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COST'
      currency = True
      Calculated = True
    end
    object taSRetSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taSRetSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taSRetSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taSRetSN: TIntegerField
      FieldName = 'SN'
    end
    object taSRetNDATE: TDateTimeField
      FieldName = 'NDATE'
    end
    object taSRetNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taSRetPRODID: TIntegerField
      FieldName = 'PRODID'
    end
    object taSRetPROD: TFIBStringField
      FieldName = 'PROD'
      EmptyStrToNull = True
    end
    object taSRetGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSRetINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSRetMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSRetART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taSRetART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taSRetSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object taSRetUNITID: TSmallintField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object taSRetRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSRetD_RETID: TFIBStringField
      FieldName = 'D_RETID'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsSRet: TDataSource
    DataSet = taSRet
    Left = 744
    Top = 236
  end
  object taORetList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SINV'
      'SET '
      '    COMPID=?COMPID,'
      '    SDATE=?SDATE,'
      '    SN=?SN,'
      '    PMARGIN=?PMARGIN,'
      '    TR=?TR,'
      '    AKCIZ=?AKCIZ,'
      '    ISCLOSED=?ISCLOSED'
      'WHERE SINVID=?OLD_SINVID '
      ' '
      '')
    DeleteSQL.Strings = (
      'DELETE FROM SInv'
      'WHERE SINVID=?OLD_SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      '(SINVID, DEPID, SDATE, SN, PMARGIN, TR, AKCIZ,'
      ' ISCLOSED, COMPID, ITYPE, OPTRET)'
      'VALUES'
      '(?SINVID, ?DEPID, ?SDATE, ?SN, ?PMARGIN, ?TR, ?AKCIZ,'
      ' ?ISCLOSED, ?COMPID, 8, ?OPTRET)')
    RefreshSQL.Strings = (
      'SELECT SINVID, DEPID, SDATE, SN, PMARGIN, NDS, TR, COST, AKCIZ,'
      '       ISCLOSED, DEP, COMPID, COMP, SCOST, OPTRET, ITYPE, Q, W'
      'FROM ORETLIST_R(?SINVID)')
    SelectSQL.Strings = (
      'SELECT SINVID, DEPID, SDATE,  SN, PMARGIN, NDS, TR, COST, AKCIZ,'
      '       ISCLOSED, DEP, COMPID, COMP, SCOST, OPTRET, ITYPE, Q, W'
      'FROM ORETLIST_S(?DEPID1, ?DEPID2, ?BD, ?ED)'
      'ORDER BY SDATE DESC')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeOpen = taORetListBeforeOpen
    OnCalcFields = taORetListCalcFields
    OnNewRecord = taORetListNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 640
    Top = 96
    object taORetListSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taORetListDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taORetListSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taORetListSN: TIntegerField
      FieldName = 'SN'
    end
    object taORetListPMARGIN: TFloatField
      FieldName = 'PMARGIN'
    end
    object taORetListNDS: TFloatField
      FieldName = 'NDS'
    end
    object taORetListTR: TFloatField
      FieldName = 'TR'
    end
    object taORetListCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taORetListAKCIZ: TFloatField
      FieldName = 'AKCIZ'
    end
    object taORetListISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taORetListDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taORetListCOMPID: TIntegerField
      FieldName = 'COMPID'
    end
    object taORetListCOMP: TFIBStringField
      FieldName = 'COMP'
      Size = 60
      EmptyStrToNull = True
    end
    object taORetListSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object taORetListOPTRET: TSmallintField
      FieldName = 'OPTRET'
      Origin = 'ORETLIST_S.OPTRET'
    end
    object taORetListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'ORETLIST_S.ITYPE'
    end
    object taORetListQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taORetListW: TFIBFloatField
      FieldName = 'W'
    end
    object taORetListCostO: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostO'
      currency = True
      Calculated = True
    end
    object taORetListCostC: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'CostC'
      currency = True
      Calculated = True
    end
    object taORetListQO: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'QO'
      Calculated = True
    end
    object taORetListQC: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'QC'
      Calculated = True
    end
    object taORetListWO: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'WO'
      Calculated = True
    end
    object taORetListWC: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'WC'
      Calculated = True
    end
    object taORetListSNO: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'SNO'
      Calculated = True
    end
    object taORetListSNC: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'SNC'
      Calculated = True
    end
  end
  object dsORetList: TDataSource
    DataSet = taORetList
    Left = 636
    Top = 144
  end
  object pmORetList: TPopupMenu
    AutoHotkeys = maManual
    Left = 187
    Top = 422
    object MenuItem17: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = ORetClick
    end
    object MenuItem18: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object quOSelled: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute PROCEDURE STUB 0')
    SelectSQL.Strings = (
      
        'SELECT SITEMID, UID, PRODCODE, MATID, GOODID, INSID,  ART, ART2,' +
        ' '
      
        '               UNITID,  NDSNAME, NDSVALUE, NDSID, SPRICE, W, SZ,' +
        ' COST,'
      
        '               ART2ID, SN0, SSF0, SDATE0, SN, SDATE, NDATE0, FUL' +
        'LART,'
      
        '              OPTPRICE, SUPID, SINFOID, SUP, PRODID,  BUYERID, B' +
        'UYER, OPRICE, PRICE2'
      'FROM OPTSELLED_S(?BD, ?ED, ?DEPID, ?BUYERID1, ?BUYERID2, '
      '                                      ?COMPID1, ?COMPID2, '
      '                                      ?MATID1, ?MATID2,'
      '                                      ?GOODID1, ?GOODID2,'
      '                                      ?INSID1,  ?INSID2)'
      'ORDER BY ART')
    BeforeOpen = quOSelledBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1086#1087#1090#1086#1084' '#1080#1079#1076#1077#1083#1080#1103
    Left = 554
    Top = 384
    object quOSelledSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quOSelledUID: TIntegerField
      FieldName = 'UID'
    end
    object quOSelledPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quOSelledMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quOSelledGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quOSelledINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quOSelledART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quOSelledART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quOSelledUNITID: TIntegerField
      FieldName = 'UNITID'
    end
    object quOSelledNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object quOSelledNDSVALUE: TFloatField
      FieldName = 'NDSVALUE'
    end
    object quOSelledNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quOSelledSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object quOSelledW: TFloatField
      FieldName = 'W'
    end
    object quOSelledSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quOSelledCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quOSelledART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quOSelledSN0: TIntegerField
      FieldName = 'SN0'
    end
    object quOSelledSSF0: TFIBStringField
      FieldName = 'SSF0'
      EmptyStrToNull = True
    end
    object quOSelledSDATE0: TDateTimeField
      FieldName = 'SDATE0'
    end
    object quOSelledSN: TIntegerField
      FieldName = 'SN'
    end
    object quOSelledSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quOSelledNDATE0: TDateTimeField
      FieldName = 'NDATE0'
    end
    object quOSelledFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quOSelledOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object quOSelledSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quOSelledSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object quOSelledSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quOSelledPRODID: TIntegerField
      FieldName = 'PRODID'
    end
    object quOSelledBUYERID: TIntegerField
      FieldName = 'BUYERID'
    end
    object quOSelledBUYER: TFIBStringField
      FieldName = 'BUYER'
      EmptyStrToNull = True
    end
    object quOSelledOPRICE: TFloatField
      FieldName = 'OPRICE'
      currency = True
    end
    object quOSelledPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
  end
  object dsOSelled: TDataSource
    DataSet = quOSelled
    Left = 552
    Top = 436
  end
  object taORet: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute  procedure ORET_U'
      
        '?OLD_SITEMID , ?OPRICE, ?PRICE2, ?D_RETID, ?NDSID, ?UID, ?W, ?SZ' +
        ', ?SPRICE, ?SDATE,'
      '?SSF, ?SN, ?NDATE, ?SUPID, ?PRICE0'
      ''
      ' ')
    DeleteSQL.Strings = (
      'execute procedure ORET_D ?OLD_SITEMID')
    InsertSQL.Strings = (
      'execute procedure ORET_I'
      
        '?SITEMID, ?SINVID, ?ART2ID, ?UID, ?W, ?SZ, ?SPRICE, ?SDATE, ?SSF' +
        ', ?SN, ?NDATE,'
      '?NDSID, ?SUPID, ?D_RETID, ?OPRICE, ?PRICE2, ?SINFOID, ?PRICE0'
      '')
    RefreshSQL.Strings = (
      'SELECT  SITEMID, SINVID, ART2ID, UID,  W, SZ, Q0,'
      '                SPRICE, SDATE, SSF, SN, NDATE, NDSID,'
      '                D_RETID, PRODID, PROD, GOODID, INSID, '
      '                MATID, ART, ART2, UNITID, D_ARTID, SINFOID,'
      '                SUPID, OPRICE, PRICE2, PRICE0'
      'FROM ORET_R(?SITEMID)')
    SelectSQL.Strings = (
      'SELECT  SITEMID, SINVID, ART2ID, UID,  W, SZ, Q0,'
      '                SPRICE, SDATE, SSF, SN, NDATE, NDSID,'
      '                D_RETID, PRODID, PROD, GOODID, INSID, '
      '                MATID, ART, ART2, UNITID, D_ARTID, SINFOID,'
      '                SUPID, OPRICE, PRICE2, PRICE0'
      'FROM ORET_S(?INVID)'
      'ORDER BY SITEMID')
    AfterDelete = taORetAfterDelete
    AfterPost = taORetAfterDelete
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeOpen = taORetBeforeOpen
    OnCalcFields = taORetCalcFields
    OnNewRecord = taORetNewRecord
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 606
    Top = 384
    object taORetSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object taORetSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taORetART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taORetUID: TIntegerField
      FieldName = 'UID'
    end
    object taORetW: TFloatField
      FieldName = 'W'
    end
    object taORetSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taORetQ0: TFloatField
      FieldName = 'Q0'
    end
    object taORetSPRICE: TFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object taORetSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taORetSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taORetSN: TIntegerField
      FieldName = 'SN'
    end
    object taORetNDATE: TDateTimeField
      FieldName = 'NDATE'
    end
    object taORetNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taORetD_RETID: TFIBStringField
      FieldName = 'D_RETID'
      Size = 10
      EmptyStrToNull = True
    end
    object taORetPRODID: TIntegerField
      FieldName = 'PRODID'
    end
    object taORetPROD: TFIBStringField
      FieldName = 'PROD'
      EmptyStrToNull = True
    end
    object taORetGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taORetINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taORetMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taORetART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taORetART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taORetUNITID: TSmallintField
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object taORetD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object taORetSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object taORetSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taORetOPRICE: TFloatField
      FieldName = 'OPRICE'
      currency = True
    end
    object taORetPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taORetRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taORetNDS: TStringField
      FieldKind = fkLookup
      FieldName = 'NDS'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Lookup = True
    end
    object taORetRET: TStringField
      FieldKind = fkLookup
      FieldName = 'RET'
      LookupDataSet = dmCom.taRet
      LookupKeyFields = 'D_RETID'
      LookupResultField = 'RET'
      KeyFields = 'D_RETID'
      Size = 40
      Lookup = True
    end
    object taORetSup: TStringField
      FieldKind = fkLookup
      FieldName = 'Sup'
      LookupDataSet = quSup
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'SNAME'
      KeyFields = 'SUPID'
      Lookup = True
    end
    object taORetPRICE0: TFloatField
      FieldName = 'PRICE0'
      currency = True
    end
    object taORetCost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Cost'
      currency = True
      Calculated = True
    end
  end
  object dsORet: TDataSource
    DataSet = taORet
    Left = 610
    Top = 436
  end
  object quFCReate: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure FCReate ?FileName')
    Left = 16
    Top = 468
  end
  object quFRemove: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure FRemove ?FileName')
    Left = 76
    Top = 472
  end
  object quCreateExtA_1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'CREATE TABLE '
      'AREST_E '
      'EXTERNAL FILE'
      ' '#39'/tmp/e'#39
      '('
      '    USERID IDPRIMARY,'
      '    D_ARTID IDPRIMARY,'
      '    Q LONG_INT,'
      '    W DOUBLE_PREC,'
      '    NL NL)')
    Left = 20
    Top = 524
  end
  object quDropExtA_1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'DROP TABLE '
      'AREST_E')
    Left = 80
    Top = 524
  end
  object quAToExt: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      '  insert into '
      'AREST_E'
      '(USERID, D_ARTID, Q, W)'
      '  select '
      '?UserId '
      ',a2.D_ArtId, count(*), sum(si.w)'
      '  from sitem si, sel e, sinv i, art2 a2'
      '  where i.IsClosed=1 and'
      '        e.SInvId= i.SInvId and'
      '        a2.Art2Id=e.Art2Id and'
      '        si.SElId=e.SElId and'
      '        si.BusyType=0'
      '  group by a2.D_ArtId;')
    Left = 132
    Top = 528
  end
  object quSRetUID: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SITEMID, UID, W, SZ, DEPID, PRICE2, SINFOID, PRICE,'
      
        '               SUPID, NDSID, SN, SSF, SDATE, SUP, NDSNAME, IS$SA' +
        'FE'
      'FROM  SRETUID(?DEPID1, ?DEPID2, ?ART2Id, ?SUPID, ?CONTRACT$ID)'
      'ORDER BY UID')
    BeforeOpen = quSRetUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 632
    Top = 292
    object quSRetUIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quSRetUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quSRetUIDW: TFloatField
      FieldName = 'W'
    end
    object quSRetUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quSRetUIDDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quSRetUIDPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quSRetUIDSINFOID: TIntegerField
      FieldName = 'SINFOID'
    end
    object quSRetUIDPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quSRetUIDSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object quSRetUIDNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quSRetUIDSN: TIntegerField
      FieldName = 'SN'
    end
    object quSRetUIDSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quSRetUIDSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quSRetUIDSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object quSRetUIDNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object quSRetUIDISSAFE: TFIBIntegerField
      FieldName = 'IS$SAFE'
    end
  end
  object dsSRetUID: TDataSource
    DataSet = quSRetUID
    Left = 628
    Top = 340
  end
  object pmPHist: TPopupMenu
    Left = 132
    Top = 577
  end
  object quMaxArtPrice: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.Price, p.Price2'
      'from Art2 a2, Price p'
      'where a2.D_ArtId= :D_ArtId and'
      '      p.Art2Id=a2.Art2Id and'
      '      p.DepId= :DepId'
      'order by  p.Price2 DESC')
    AfterOpen = quMaxArtPriceAfterOpen
    BeforeOpen = quMaxArtPriceBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsArt
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 244
    Top = 380
  end
  object dsMaxArtPrice: TDataSource
    DataSet = quMaxArtPrice
    Left = 252
    Top = 424
  end
  object quInsTmpUID: TpFIBQuery
    SQL.Strings = (
      'INSERT INTO UIDWH_T'
      '(UIDWHID, USERID, SITEMID, UID, PRODCODE, MATID, GOODID, INSID,'
      ' ART, ART2, UNITID, NDSNAME, NDSVALUE, NDSID, PRICE, W, SZ,'
      ' DEP, SUPCODE, COST, ART2ID, DEPID, COLOR, SN0, SSF0, SPRICE0,'
      ' SUPID0, SDATE0, SN, SDATE, SELLDATE, T,  ISCLOSED, NDATE,'
      ' ITYPE, PRODID)'
      'VALUES'
      
        '(GEN_ID(GEN_UIDWH_ID, 1), ?USERID, ?SITEMID, ?UID, ?PRODCODE, ?M' +
        'ATID, ?GOODID, ?INSID,'
      
        ' ?ART, ?ART2, ?UNITID, ?NDSNAME, ?NDSVALUE, ?NDSID, ?PRICE, ?W, ' +
        '?SZ,'
      
        ' ?DEP, ?SUPCODE, ?COST, ?ART2ID, ?DEPID, ?COLOR, ?SN0, ?SSF0, ?S' +
        'PRICE0,'
      
        ' ?SUPID0, ?SDATE0, ?SN, ?SDATE, ?SELLDATE, ?T, ?ISCLOSED, ?NDATE' +
        ','
      ' ?ITYPE, ?PRODID)')
    Left = 252
    Top = 532
  end
  object quTmp2: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 376
    Top = 476
  end
  object pmRepl: TPopupMenu
    Left = 184
    Top = 472
    object N1: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      OnClick = ReplClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object quPrOrd: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select PrOrdId, DepId, CloseDate, SetDate, IsClosed, PrOrd, IsSe' +
        't,'
      '          PrDate, CloseEmpId, SetEmpId, IType, Dep, SN,  SSF,'
      '          CLOSEFIO,  SETFIO, SINVID, Q, Cost, OldCost, DCost,'
      '         PActClosed, NAct, IsReVal'
      'from PRORD_R(?PRORDID)')
    BeforeOpen = quPrOrdBeforeOpen
    OnCalcFields = quPrOrdCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 328
    Top = 196
    object quPrOrdPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'PRORD_R.PRORDID'
    end
    object quPrOrdDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'PRORD_R.DEPID'
    end
    object quPrOrdISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
      Origin = 'PRORD_R.ISCLOSED'
    end
    object quPrOrdDEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'PRORD_R.DEP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quPrOrdSN: TIntegerField
      FieldName = 'SN'
      Origin = 'PRORD_R.SN'
    end
    object quPrOrdPRORD: TIntegerField
      FieldName = 'PRORD'
      Origin = 'PRORD_R.PRORD'
    end
    object quPrOrdISSET: TSmallintField
      FieldName = 'ISSET'
      Origin = 'PRORD_R.ISSET'
    end
    object quPrOrdCLOSEDATE: TDateTimeField
      FieldName = 'CLOSEDATE'
      Origin = 'PRORD_R.CLOSEDATE'
    end
    object quPrOrdSETDATE: TDateTimeField
      FieldName = 'SETDATE'
      Origin = 'PRORD_R.SETDATE'
    end
    object quPrOrdCLOSEEMPID: TIntegerField
      FieldName = 'CLOSEEMPID'
      Origin = 'PRORD_R.CLOSEEMPID'
    end
    object quPrOrdSETEMPID: TIntegerField
      FieldName = 'SETEMPID'
      Origin = 'PRORD_R.SETEMPID'
    end
    object quPrOrdCLOSEFIO: TFIBStringField
      FieldName = 'CLOSEFIO'
      Origin = 'PRORD_R.CLOSEFIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quPrOrdSETFIO: TFIBStringField
      FieldName = 'SETFIO'
      Origin = 'PRORD_R.SETFIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quPrOrdITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'PRORD_R.ITYPE'
    end
    object quPrOrdSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'PRORD_R.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quPrOrdPRDATE: TDateTimeField
      FieldName = 'PRDATE'
      Origin = 'PRORD_R.PRDATE'
    end
    object quPrOrdSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'PRORD_R.SINVID'
    end
    object quPrOrdQ: TIntegerField
      FieldName = 'Q'
      Origin = 'PRORD_R.Q'
    end
    object quPrOrdCOST: TFloatField
      FieldName = 'COST'
      Origin = 'PRORD_R.COST'
    end
    object quPrOrdOLDCOST: TFloatField
      FieldName = 'OLDCOST'
      Origin = 'PRORD_R.OLDCOST'
    end
    object quPrOrdDCOST: TFloatField
      FieldName = 'DCOST'
      Origin = 'PRORD_R.DCOST'
    end
    object quPrOrdPACTCLOSED: TIntegerField
      FieldName = 'PACTCLOSED'
      Origin = 'PRORD_R.PACTCLOSED'
    end
    object quPrOrdSNStr: TStringField
      FieldKind = fkCalculated
      FieldName = 'SNStr'
      Size = 30
      Calculated = True
    end
    object quPrOrdNACT: TIntegerField
      FieldName = 'NACT'
      Origin = 'PRORD_R.NACT'
    end
    object quPrOrdISREVAL: TIntegerField
      FieldName = 'ISREVAL'
      Origin = 'PRORD_R.ISREVAL'
    end
  end
  object quTemp: TpFIBDataSet
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 324
    Top = 338
  end
  object fs: TFormStorage
    Options = []
    UseRegistry = False
    StoredValues = <>
    Left = 644
    Top = 484
  end
  object quADict: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_ART'
      'SET ART=?ART,'
      '       UNITID=?UNITID'
      'WHERE D_ARTID=?OLD_D_ARTID'
      '')
    DeleteSQL.Strings = (
      'DELETE FROM D_ART'
      'WHERE D_ARTID=?OLD_D_ARTID')
    InsertSQL.Strings = (
      
        'INSERT INTO D_ART(D_ARTID, D_COMPID, D_MATID, D_GOODID, ART, UNI' +
        'TID, D_INSID,D_COUNTRYID, ATT1, ATT2)'
      
        'VALUES (?D_ARTID, ?D_COMPID, ?D_MATID, ?D_GOODID, ?ART, ?UNITID,' +
        ' ?D_INSID, ?D_COUNTRYID, ?ATT1, ?ATT2)'
      ' ')
    RefreshSQL.Strings = (
      'SELECT A.D_ARTID,  A.D_COMPID,  A.D_MATID,   A.D_GOODID,'
      
        '             A.ART,   A.UNITID,   A.D_INSID , A.FULLART,a.D_COUN' +
        'TRYID'
      'FROM  D_ART A'
      'where  A.D_ARTID=?D_ARTID')
    SelectSQL.Strings = (
      'SELECT A.D_ARTID,  A.D_COMPID,  A.D_MATID,   A.D_GOODID,'
      
        '       A.ART,   A.UNITID,   A.D_INSID , A.FULLART, a.D_COUNTRYID' +
        ',A.ATT1,A.ATT2'
      'FROM  D_ART A'
      'where A.D_COMPID BETWEEN :COMPID1 and :COMPID2 and'
      '      A.D_MATID BETWEEN :MATID1 and  :MATID2 and'
      '      A.D_GOODID BETWEEN :GOODID1 and :GOODID2 and'
      '      A.D_INSID BETWEEN :INSID1 and :INSID2 and'
      '      A.D_COUNTRYID BETWEEN :COUNTRYID1 AND :COUNTRYID2 and'
      '      A.ATT1 BETWEEN :ATT1_1 AND :ATT1_2 and'
      '      A.ATT2 BETWEEN :ATT2_1 AND :ATT2_2 '
      'ORDER BY A.ART')
    AfterDelete = CommitRetaining
    AfterPost = quADictAfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeInsert = quADictBeforeInsert
    BeforeOpen = quADictBeforeOpen
    BeforePost = quADictBeforePost
    OnNewRecord = quADictNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 656
    Top = 8
    object quADictD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_ART.D_ARTID'
      Required = True
    end
    object quADictD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_ART.D_COMPID'
      Required = True
    end
    object quADictD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quADictD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quADictART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quADictUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'D_ART.UNITID'
      Required = True
      OnGetText = UNITIDGetText
      OnSetText = UNITIDSetText
    end
    object quADictD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quADictFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_ART.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quADictD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Origin = 'D_ART.D_COUNTRYID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quADictATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quADictATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
  end
  object dsADict: TDataSource
    DataSet = quADict
    Left = 660
    Top = 52
  end
  object quA2Dict: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Art2'
      'set Art2=?Art2'
      'where Art2Id=?Old_Art2Id'
      '')
    DeleteSQL.Strings = (
      'delete from Art2'
      'where ARt2Id=?OLD_Art2Id')
    InsertSQL.Strings = (
      'insert into ARt2( Art2Id, D_ArtId, Art2, SupId)'
      'values (?Art2Id, ?D_ArtId, ?Art2, ?SupId)'
      ''
      ' ')
    SelectSQL.Strings = (
      'select Art2Id, D_ArtId, Art2, SupId, D_INSID'
      'from Art2'
      'where D_ArtId=?D_ARTID'
      'order by Art2')
    AfterDelete = CommitRetaining
    AfterPost = quA2DictAfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeInsert = quA2DictBeforeInsert
    BeforePost = quA2DictBeforePost
    OnNewRecord = quA2DictNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsADict
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 708
    Top = 8
    dcForceMasterRefresh = True
    dcForceOpen = True
    object quA2DictART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'ART2.ART2ID'
      Required = True
    end
    object quA2DictD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'ART2.D_ARTID'
    end
    object quA2DictART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quA2DictSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'ART2.SUPID'
    end
    object quA2DictD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsA2Dict: TDataSource
    DataSet = quA2Dict
    Left = 712
    Top = 52
  end
  object quGetArt: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select a.D_ArtId, a.D_MatId, a.D_GoodId, a.D_InsId, a.Art, a.Uni' +
        'tId,'
      '       a2.ARt2, c.Code ProdCode'
      'from D_Art a, Art2 a2, D_Comp c'
      'where a2.Art2Id=?Art2Id and'
      '      a.D_ArtId=a2.D_ArtId and'
      '      c.D_CompId=a.D_CompId'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 432
    Top = 472
    object quGetArtD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_ART.D_ARTID'
      Required = True
    end
    object quGetArtD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetArtD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetArtD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetArtART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quGetArtUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'D_ART.UNITID'
      Required = True
    end
    object quGetArtART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quGetArtPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'D_COMP.CODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object quNewA2Ins: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      
        'insert into ins (insID, Art2ID, D_InsID, Quantity, Weight, Color' +
        ',Chromaticity,'
      
        '    Cleannes, gr,  Shape, EDGETION, main,  EdgTypeID, EdgShapeid' +
        ')'
      
        'select  GEN_ID(GEN_INS_ID, 1), :NEWART2ID, D_INSID, QUANTITY, WE' +
        'IGHT, COLOR, CHROMATICITY,'
      '      CLEANNES, GR, SHAPE, EDGETION, MAIN, EDGTYPEID, EDGSHAPEID'
      'from Ins'
      'where Art2Id=:OldArt2Id;'
      '')
    Left = 508
    Top = 488
  end
  object taReturn_URF: TpFIBDataSet
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 708
    Top = 488
  end
  object dsReturn_Urf: TDataSource
    DataSet = taReturn_URF
    Left = 708
    Top = 540
  end
  object quUIDSelled: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      
        '    SITEMID,  UID, PRODCODE,  MATID, GOODID, INSID,  ART,       ' +
        '            ART2,   UNITID,  NDSNAME, NDSVALUE,    NDSID,'
      
        '    PRICE,   W,  SZ,  SUPCODE, COST,  ART2ID,  SN0, SSF0,  SPRIC' +
        'E0 ,'
      '    SUPID0, SDATE0, SN,  SDATE,  SELLDATE, T,  MAT, SELLITEMID ,'
      '    CLIENTID, CLIENT,  CHECKNO, FULLART, PRICE0, RETDONE'
      'FROM  SELLED_S_UID(?DEPID, ?UID)'
      '')
    BeforeOpen = quUIDSelledBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 312
    Top = 376
    object quUIDSelledSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SELLED_S_UID.SITEMID'
    end
    object quUIDSelledUID: TIntegerField
      FieldName = 'UID'
      Origin = 'SELLED_S_UID.UID'
    end
    object quUIDSelledPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'SELLED_S_UID.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'SELLED_S_UID.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledGOODID: TFIBStringField
      FieldName = 'GOODID'
      Origin = 'SELLED_S_UID.GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledINSID: TFIBStringField
      FieldName = 'INSID'
      Origin = 'SELLED_S_UID.INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledART: TFIBStringField
      FieldName = 'ART'
      Origin = 'SELLED_S_UID.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDSelledART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'SELLED_S_UID.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDSelledUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'SELLED_S_UID.UNITID'
    end
    object quUIDSelledNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      Origin = 'SELLED_S_UID.NDSNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDSelledNDSVALUE: TFloatField
      FieldName = 'NDSVALUE'
      Origin = 'SELLED_S_UID.NDSVALUE'
    end
    object quUIDSelledNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SELLED_S_UID.NDSID'
    end
    object quUIDSelledPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SELLED_S_UID.PRICE'
    end
    object quUIDSelledW: TFloatField
      FieldName = 'W'
      Origin = 'SELLED_S_UID.W'
    end
    object quUIDSelledSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'SELLED_S_UID.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledSUPCODE: TFIBStringField
      FieldName = 'SUPCODE'
      Origin = 'SELLED_S_UID.SUPCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quUIDSelledCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SELLED_S_UID.COST'
    end
    object quUIDSelledART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SELLED_S_UID.ART2ID'
    end
    object quUIDSelledSN0: TIntegerField
      FieldName = 'SN0'
      Origin = 'SELLED_S_UID.SN0'
    end
    object quUIDSelledSSF0: TFIBStringField
      FieldName = 'SSF0'
      Origin = 'SELLED_S_UID.SSF0'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDSelledSPRICE0: TFloatField
      FieldName = 'SPRICE0'
      Origin = 'SELLED_S_UID.SPRICE0'
    end
    object quUIDSelledSUPID0: TIntegerField
      FieldName = 'SUPID0'
      Origin = 'SELLED_S_UID.SUPID0'
    end
    object quUIDSelledSDATE0: TDateTimeField
      FieldName = 'SDATE0'
      Origin = 'SELLED_S_UID.SDATE0'
    end
    object quUIDSelledSN: TIntegerField
      FieldName = 'SN'
      Origin = 'SELLED_S_UID.SN'
    end
    object quUIDSelledSDATE: TDateTimeField
      FieldName = 'SDATE'
      Origin = 'SELLED_S_UID.SDATE'
    end
    object quUIDSelledSELLDATE: TDateTimeField
      FieldName = 'SELLDATE'
      Origin = 'SELLED_S_UID.SELLDATE'
    end
    object quUIDSelledT: TSmallintField
      FieldName = 'T'
      Origin = 'SELLED_S_UID.T'
    end
    object quUIDSelledMAT: TFIBStringField
      FieldName = 'MAT'
      Origin = 'SELLED_S_UID.MAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDSelledSELLITEMID: TIntegerField
      FieldName = 'SELLITEMID'
      Origin = 'SELLED_S_UID.SELLITEMID'
    end
    object quUIDSelledCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
      Origin = 'SELLED_S_UID.CLIENTID'
    end
    object quUIDSelledCLIENT: TFIBStringField
      FieldName = 'CLIENT'
      Origin = 'SELLED_S_UID.CLIENT'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDSelledCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
      Origin = 'SELLED_S_UID.CHECKNO'
    end
    object quUIDSelledFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'SELLED_S_UID.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDSelledPRICE0: TFloatField
      FieldName = 'PRICE0'
      Origin = 'SELLED_S_UID.PRICE0'
    end
    object quUIDSelledRETDONE: TSmallintField
      FieldName = 'RETDONE'
      Origin = 'SELLED_S_UID.RETDONE'
    end
  end
  object dsUIDSelled: TDataSource
    DataSet = quUIDSelled
    Left = 312
    Top = 424
  end
  object quTempUIDWH: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select tu.d_Depid, count(tu.d_depid) c, cast(sum(tu.ALL_COUNT) a' +
        's integer) sac, sum(TW) stw,sum(TP) stp,sum(TP2) stp2, sum(TP3) ' +
        'stp3'
      'from T_UIDWH  tu'
      'where tu.GEN_USER= ?USERID '
      'group by tu.d_Depid')
    BeforeOpen = quTempUIDWHBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 720
    Top = 288
    object quTempUIDWHD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Required = True
    end
    object quTempUIDWHC: TIntegerField
      DisplayLabel = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      FieldName = 'C'
      Required = True
    end
    object quTempUIDWHSAC: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'SAC'
    end
    object quTempUIDWHSTW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'STW'
      DisplayFormat = '#.##'
    end
    object quTempUIDWHSTP: TFloatField
      DisplayLabel = #1055#1088#1080#1093#1086#1076#1085#1099#1077' '#1094#1077#1085#1099
      FieldName = 'STP'
      currency = True
    end
    object quTempUIDWHSTP2: TFloatField
      DisplayLabel = #1056#1072#1089#1093#1086#1076#1085#1099#1077' '#1094#1077#1085#1099
      FieldName = 'STP2'
      currency = True
    end
    object quTempUIDWHsname: TStringField
      FieldKind = fkLookup
      FieldName = 'sname'
      LookupDataSet = dmCom.DepSortWithDel
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'SNAME'
      KeyFields = 'D_DEPID'
      Lookup = True
    end
    object quTempUIDWHCOLOR: TStringField
      FieldKind = fkLookup
      FieldName = 'COLOR'
      LookupDataSet = dmCom.DepSortWithDel
      LookupKeyFields = 'D_DEPID'
      LookupResultField = 'COLOR'
      KeyFields = 'D_DEPID'
      Lookup = True
    end
    object quTempUIDWHSTP3: TFIBFloatField
      FieldName = 'STP3'
      currency = True
    end
  end
  object dsTempUIDWH: TDataSource
    DataSet = quTempUIDWH
    Left = 720
    Top = 332
  end
  object taPrMargin: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE  D_DEP'
      'SET TMARGIN=?TMARGIN,'
      '        TROUNDNORM=?TROUNDNORM'
      'WHERE D_DEPID=?OLD_D_DEPID')
    SelectSQL.Strings = (
      'SELECT  D_DEPID, NAME, TMARGIN, TROUNDNORM'
      'FROM D_DEP'
      'where d_depid>0 and'
      '      ISDelete <> 1'
      'ORDER BY NAME')
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    OnPostError = PostError
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 714
    Top = 94
    object taPrMarginD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Origin = 'D_DEP.D_DEPID'
    end
    object taPrMarginNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      EmptyStrToNull = True
    end
    object taPrMarginTMARGIN: TFloatField
      FieldName = 'TMARGIN'
      Origin = 'D_DEP.TMARGIN'
      currency = True
    end
    object taPrMarginTROUNDNORM: TSmallintField
      FieldName = 'TROUNDNORM'
      Origin = 'D_DEP.TROUNDNORM'
      OnGetText = taDepMarginROUNDNORMGetText
      OnSetText = taDepMarginROUNDNORMSetText
    end
  end
  object dsPrMargin: TDataSource
    DataSet = taPrMargin
    Left = 710
    Top = 138
  end
  object pmAddTopr: TPopupMenu
    Left = 132
    Top = 472
    object N5: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      RadioItem = True
      OnClick = AddtoPrClick
    end
    object N6: TMenuItem
      Caption = '-'
    end
  end
  object translTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure  DelNotTransl')
    Left = 392
    Top = 532
  end
  object dsInOutPrice: TDataSource
    DataSet = quInOutPrice
    Left = 328
    Top = 584
  end
  object NoCloseRetAct: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT count(P.PrOrdId)'
      '        FROM  PrOrd P'
      '        where  P.DepID BETWEEN  ?DepId1 and ?DepId2 and'
      '               P.PrDate between ?BD and ?ED'
      '               and p.itype = 6'
      '               and p.pactclosed is Null '
      '               and p.nact is not null')
    Left = 248
    Top = 580
  end
  object quInOutPrice: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT   PRICE1, PRICE2'
      'FROM ExInOutPrice(?AINVID, ?DepID,?Art2ID)')
    SelectSQL.Strings = (
      'SELECT   PRICE1, PRICE2 , SN,  ART'
      'FROM ExInOutPrice(?AINVID,?DEPID, ?ART2ID)'
      '')
    AfterOpen = quInOutPriceAfterOpen
    BeforeOpen = quInOutPriceBeforeOpen
    AfterRefresh = quInOutPriceAfterRefresh
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsA2
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 396
    Top = 584
    dcForceMasterRefresh = True
    dcForceOpen = True
    object quInOutPricePRICE1: TFloatField
      FieldName = 'PRICE1'
      Origin = 'EXINOUTPRICE.PRICE1'
    end
    object quInOutPricePRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'EXINOUTPRICE.PRICE2'
    end
    object quInOutPriceSN: TIntegerField
      FieldName = 'SN'
      Origin = 'EXINOUTPRICE.SN'
    end
    object quInOutPriceART: TFIBStringField
      FieldName = 'ART'
      Origin = 'EXINOUTPRICE.ART'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
  end
  object pmJSz: TPopupMenu
    Left = 24
    Top = 576
    object N7: TMenuItem
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
    end
    object N8: TMenuItem
      Caption = '-'
    end
  end
  object quToCheckShiftTime: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      
        'execute procedure RETTIMECHECK(?BD, ?ED, ?CLIENTID1, ?CLIENTID2,' +
        '?UserID)')
    Left = 484
    Top = 584
  end
  object quSelled: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub(0);')
    SelectSQL.Strings = (
      'SELECT'
      '  SITEMID,UID,PRODCODE,MATID,GOODID,INSID,ART, ART2,'
      
        '   UNITID,PRICE,W,SZ,SUPCODE,COST, ART2ID,   SN0,SSF0, SPRICE0, ' +
        'SUPID0, SDATE0,SN,SDATE,SELLDATE,  SELLITEMID,CLIENTID, CLIENT, ' +
        'CHECKNO, NDATE,FULLART, PRICE0, RETDONE'
      'FROM  SellElem'
      'WHERE depId =   ?DEPID and  '
      '               UserId = ?UserId  and'
      '               IsNotProve = 0'
      'ORDER BY UID')
    BeforeOpen = quSelledBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 410
    Top = 287
    object quSelledSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SELLELEM.SITEMID'
    end
    object quSelledUID: TIntegerField
      FieldName = 'UID'
      Origin = 'SELLELEM.UID'
    end
    object quSelledPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'SELLELEM.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'SELLELEM.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledGOODID: TFIBStringField
      FieldName = 'GOODID'
      Origin = 'SELLELEM.GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledINSID: TFIBStringField
      FieldName = 'INSID'
      Origin = 'SELLELEM.INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledART: TFIBStringField
      FieldName = 'ART'
      Origin = 'SELLELEM.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quSelledART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'SELLELEM.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quSelledUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'SELLELEM.UNITID'
    end
    object quSelledPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SELLELEM.PRICE'
    end
    object quSelledW: TFloatField
      FieldName = 'W'
      Origin = 'SELLELEM.W'
    end
    object quSelledSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'SELLELEM.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledSUPCODE: TFIBStringField
      FieldName = 'SUPCODE'
      Origin = 'SELLELEM.SUPCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSelledCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SELLELEM.COST'
    end
    object quSelledART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SELLELEM.ART2ID'
    end
    object quSelledSN0: TIntegerField
      FieldName = 'SN0'
      Origin = 'SELLELEM.SN0'
    end
    object quSelledSSF0: TFIBStringField
      FieldName = 'SSF0'
      Origin = 'SELLELEM.SSF0'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quSelledSPRICE0: TFloatField
      FieldName = 'SPRICE0'
      Origin = 'SELLELEM.SPRICE0'
    end
    object quSelledSUPID0: TIntegerField
      FieldName = 'SUPID0'
      Origin = 'SELLELEM.SUPID0'
    end
    object quSelledSDATE0: TDateTimeField
      FieldName = 'SDATE0'
      Origin = 'SELLELEM.SDATE0'
    end
    object quSelledSN: TIntegerField
      FieldName = 'SN'
      Origin = 'SELLELEM.SN'
    end
    object quSelledSDATE: TDateTimeField
      FieldName = 'SDATE'
      Origin = 'SELLELEM.SDATE'
    end
    object quSelledSELLDATE: TDateTimeField
      FieldName = 'SELLDATE'
      Origin = 'SELLELEM.SELLDATE'
    end
    object quSelledSELLITEMID: TIntegerField
      FieldName = 'SELLITEMID'
      Origin = 'SELLELEM.SELLITEMID'
    end
    object quSelledCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
      Origin = 'SELLELEM.CLIENTID'
    end
    object quSelledCLIENT: TFIBStringField
      FieldName = 'CLIENT'
      Origin = 'SELLELEM.CLIENT'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quSelledCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
      Origin = 'SELLELEM.CHECKNO'
    end
    object quSelledNDATE: TDateTimeField
      FieldName = 'NDATE'
      Origin = 'SELLELEM.NDATE'
    end
    object quSelledFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'SELLELEM.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quSelledPRICE0: TFloatField
      FieldName = 'PRICE0'
      Origin = 'SELLELEM.PRICE0'
    end
    object quSelledRETDONE: TSmallintField
      FieldName = 'RETDONE'
      Origin = 'SELLELEM.RETDONE'
    end
  end
  object quSellElDel: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 572
    Top = 584
  end
  object quTmp1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 568
    Top = 536
  end
  object quCurSellCheck: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SELL'
      'SET N=?N,'
      '        EMP1ID=?EMP1ID,'
      '        EMP2ID=?EMP1ID,'
      '        ED=?ED,'
      '        CLOSED=?CLOSED,'
      '       RN=?RN,'
      '       FREG=:FREG'
      'WHERE SELLID=?OLD_SELLID')
    InsertSQL.Strings = (
      
        'INSERT INTO SELL(SELLID, BD, N, DEPID, EMP1ID, EMP2ID, ED, CLOSE' +
        'D, FREG)'
      
        'VALUES (?SELLID, '#39'NOW'#39', ?N, ?DEPID, ?EMP1ID, ?EMP1ID, ?ED, ?CLOS' +
        'ED, :FREG)'
      '')
    RefreshSQL.Strings = (
      'SELECT SELLID, BD, ED, DEPID, N, EMP1ID, EMP2ID, DEP,'
      
        '               CNT, W, COST,  RCNT, RW, RCOST, RN, CLOSED, FREG,' +
        ' CHECKNO, costcard'
      'FROM CURSELL_CHECK_R(?SELLID, ?CHECKNO)'
      '')
    SelectSQL.Strings = (
      'SELECT SELLID, BD, ED, DEPID, N, EMP1ID, EMP2ID, DEP,'
      
        '               CNT, W, COST, RCNT, RW, RCOST, RN, CLOSED, FREG, ' +
        'CHECKNO, costcard'
      'FROM CURSELL_CHECK_S(?DepId, ?EmpID, ?CHECKNO)')
    BeforeOpen = quCurSellCheckBeforeOpen
    OnCalcFields = quCurSellCheckCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 720
    Top = 400
    object quCurSellCheckSELLID: TIntegerField
      FieldName = 'SELLID'
      Origin = 'CURSELL_CHECK_S.SELLID'
    end
    object quCurSellCheckBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'CURSELL_CHECK_S.BD'
    end
    object quCurSellCheckED: TDateTimeField
      FieldName = 'ED'
      Origin = 'CURSELL_CHECK_S.ED'
    end
    object quCurSellCheckDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'CURSELL_CHECK_S.DEPID'
    end
    object quCurSellCheckN: TIntegerField
      FieldName = 'N'
      Origin = 'CURSELL_CHECK_S.N'
    end
    object quCurSellCheckEMP1ID: TIntegerField
      FieldName = 'EMP1ID'
      Origin = 'CURSELL_CHECK_S.EMP1ID'
    end
    object quCurSellCheckEMP2ID: TIntegerField
      FieldName = 'EMP2ID'
      Origin = 'CURSELL_CHECK_S.EMP2ID'
    end
    object quCurSellCheckDEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'CURSELL_CHECK_S.DEP'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quCurSellCheckCNT: TIntegerField
      FieldName = 'CNT'
      Origin = 'CURSELL_CHECK_S.CNT'
    end
    object quCurSellCheckW: TFloatField
      FieldName = 'W'
      Origin = 'CURSELL_CHECK_S.W'
    end
    object quCurSellCheckCOST: TFloatField
      FieldName = 'COST'
      Origin = 'CURSELL_CHECK_S.COST'
      currency = True
    end
    object quCurSellCheckRCNT: TIntegerField
      FieldName = 'RCNT'
      Origin = 'CURSELL_CHECK_S.RCNT'
    end
    object quCurSellCheckRW: TFloatField
      FieldName = 'RW'
      Origin = 'CURSELL_CHECK_S.RW'
    end
    object quCurSellCheckRCOST: TFloatField
      FieldName = 'RCOST'
      Origin = 'CURSELL_CHECK_S.RCOST'
      currency = True
    end
    object quCurSellCheckRN: TIntegerField
      FieldName = 'RN'
      Origin = 'CURSELL_CHECK_S.RN'
    end
    object quCurSellCheckCLOSED: TSmallintField
      FieldName = 'CLOSED'
      Origin = 'CURSELL_CHECK_S.CLOSED'
    end
    object quCurSellCheckFREG: TSmallintField
      FieldName = 'FREG'
      Origin = 'CURSELL_CHECK_S.FREG'
    end
    object quCurSellCheckCASS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CASS'
      currency = True
      Calculated = True
    end
    object quCurSellCheckCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
      Origin = 'CURSELL_CHECK_S.CHECKNO'
    end
    object quCurSellCheckCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
      currency = True
    end
  end
  object dsCurSellCheck: TDataSource
    DataSet = quCurSellCheck
    Left = 720
    Top = 448
  end
  object quDInvItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update sel'
      'set price = ?DEPFROMPRICE,'
      '    price2 = ?DEPPRICE'
      'where selid = ?OLD_SELID')
    DeleteSQL.Strings = (
      'execute procedure dinvitem_d (?OLD_SITEMID)')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      
        'select prodcode, d_matid, d_goodid, d_insid, art, art2, uid, q0,' +
        ' sz, pprice,'
      
        'depfromprice, depprice, optprice, sitemid, cost, RQ, RW, SQ, SW,' +
        ' selid, unit'
      'from dinvitem_r ( ?SITEMID)')
    SelectSQL.Strings = (
      
        'select prodcode, d_matid, d_goodid, d_insid, art, art2, uid, q0,' +
        ' sz, pprice,'
      
        'depfromprice, depprice, optprice, sitemid, cost, RQ, RW, SQ, SW,' +
        ' selid, unit'
      'from dinvitem_s ( ?ASINVID)'
      'Order by sitemid')
    AfterDelete = quDInvItemAfterDelete
    AfterPost = quDInvItemAfterPost
    BeforeDelete = quDInvItemBeforeDelete
    BeforeEdit = quDInvItemBeforeEdit
    BeforeOpen = quDInvItemBeforeOpen
    BeforePost = quDInvItemBeforePost
    OnCalcFields = quDInvItemCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 800
    Top = 504
    object quDInvItemPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'DINVITEM_S.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'DINVITEM_S.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'DINVITEM_S.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'DINVITEM_S.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemART: TFIBStringField
      FieldName = 'ART'
      Origin = 'DINVITEM_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDInvItemART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'DINVITEM_S.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDInvItemUID: TIntegerField
      FieldName = 'UID'
      Origin = 'DINVITEM_S.UID'
    end
    object quDInvItemQ0: TFloatField
      FieldName = 'Q0'
      Origin = 'DINVITEM_S.Q0'
      DisplayFormat = '#.00'
    end
    object quDInvItemSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'DINVITEM_S.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemPPRICE: TFloatField
      FieldName = 'PPRICE'
      Origin = 'DINVITEM_S.PPRICE'
      DisplayFormat = '#.00'
    end
    object quDInvItemDEPFROMPRICE: TFloatField
      FieldName = 'DEPFROMPRICE'
      Origin = 'DINVITEM_S.DEPFROMPRICE'
      DisplayFormat = '#.00'
    end
    object quDInvItemDEPPRICE: TFloatField
      FieldName = 'DEPPRICE'
      Origin = 'DINVITEM_S.DEPPRICE'
      DisplayFormat = '#.00'
    end
    object quDInvItemOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      Origin = 'DINVITEM_S.OPTPRICE'
    end
    object quDInvItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'DINVITEM_S.SITEMID'
    end
    object quDInvItemRQ: TIntegerField
      FieldName = 'RQ'
      Origin = 'DINVITEM_S.RQ'
    end
    object quDInvItemRW: TFloatField
      FieldName = 'RW'
      Origin = 'DINVITEM_S.RW'
    end
    object quDInvItemSQ: TFIBStringField
      FieldName = 'SQ'
      Origin = 'DINVITEM_S.SQ'
      Size = 200
      EmptyStrToNull = True
    end
    object quDInvItemSW: TFIBStringField
      FieldName = 'SW'
      Origin = 'DINVITEM_S.SW'
      Size = 200
      EmptyStrToNull = True
    end
    object quDInvItemSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'DINVITEM_S.SELID'
    end
    object quDInvItemCOST: TFloatField
      FieldName = 'COST'
      Origin = 'DINVITEM_S.COST'
      DisplayFormat = '#.00'
    end
    object quDInvItemUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvItemrecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'recNo'
      Calculated = True
    end
  end
  object dsDInvItem: TDataSource
    DataSet = quDInvItem
    Left = 800
    Top = 560
  end
  object quUidWhSumGr: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select SNAME,COLOR,C,SAC,STW,   STP, STP2, GR_SAC, GR_SAW,   GR_' +
        'SAP, GR_SAP2 from '
      'UIDWH_ST_GR'
      '(:USERID, :COMPID1, :COMPID2, :MATID1,'
      '    :MATID2, :GOODID1, :GOODID2, :SUPID1, :SUPID2,'
      
        '    :ART1, :ART2, :DEPID1, :DEPID2, :T1, :T2, :T3, :T4, :T5, :go' +
        'odsid1_1, :goodsid1_2,      :goodsid2_1, :goodsid2_2 )'
      'order by sname')
    BeforeOpen = quUidWhSumGrBeforeOpen
    OnCalcFields = quUidWhSumGrCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 800
    Top = 400
    object quUidWhSumGrSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'UIDWH_ST_GR.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUidWhSumGrCOLOR: TIntegerField
      FieldName = 'COLOR'
      Origin = 'UIDWH_ST_GR.COLOR'
    end
    object quUidWhSumGrC: TIntegerField
      FieldName = 'C'
      Origin = 'UIDWH_ST_GR.C'
    end
    object quUidWhSumGrSAC: TIntegerField
      FieldName = 'SAC'
      Origin = 'UIDWH_ST_GR.SAC'
    end
    object quUidWhSumGrSTW: TFloatField
      FieldName = 'STW'
      Origin = 'UIDWH_ST_GR.STW'
      DisplayFormat = '#.00'
    end
    object quUidWhSumGrSTP: TFloatField
      FieldName = 'STP'
      Origin = 'UIDWH_ST_GR.STP'
      DisplayFormat = '#.000'
    end
    object quUidWhSumGrSTP2: TFloatField
      FieldName = 'STP2'
      Origin = 'UIDWH_ST_GR.STP2'
      DisplayFormat = '#.000'
    end
    object quUidWhSumGrGR_SAC: TFIBStringField
      FieldName = 'GR_SAC'
      Origin = 'UIDWH_ST_GR.GR_SAC'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quUidWhSumGrGR_SAW: TFIBStringField
      FieldName = 'GR_SAW'
      Origin = 'UIDWH_ST_GR.GR_SAW'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quUidWhSumGrGR_SAP: TFIBStringField
      FieldName = 'GR_SAP'
      Origin = 'UIDWH_ST_GR.GR_SAP'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quUidWhSumGrGR_SAP2: TFIBStringField
      FieldName = 'GR_SAP2'
      Origin = 'UIDWH_ST_GR.GR_SAP2'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
  end
  object dsUidWhSumGr: TDataSource
    DataSet = quUidWhSumGr
    Left = 800
    Top = 448
  end
  object quSupR: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, NAME, SNAME, CODE, PAYTYPEID, NDSID'
      'FROM D_COMP'
      'WHERE SELLER>=1 and workorg=1 and d_compid<>-1000'
      'ORDER BY SNAME')
    OnCalcFields = quSupRCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 712
    Top = 600
    poSQLINT64ToBCD = True
    object quSupRD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quSupRNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quSupRSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quSupRCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSupRPAYTYPEID: TFIBIntegerField
      FieldName = 'PAYTYPEID'
    end
    object quSupRNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object quSupRFNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'FNAME'
      Size = 80
      Calculated = True
    end
  end
  object dsSupR: TDataSource
    DataSet = quSupR
    Left = 664
    Top = 600
  end
  object quMaxUIDWH: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure UIDWHEdt'
      '(?goodsid1, ?OLD_goodsid1,'
      '?goodsid2, ?OLD_goodsid2,'
      '?UID, ?OLD_UID,'
      '?SZ, ?OLD_SZ,'
      '?W, ?OLD_W,'
      '?SSF0, ?OLD_SSF0,'
      '?SN0, ?OLD_SN0,'
      '?SPRICE0, ?OLD_SPRICE0,'
      '?SDATE0, ?OLD_SDATE0,'
      '?NDATE, ?OLD_NDATE,'
      '?SUPID0, ?OLD_SUPUID0,'
      '?ART2ID, ?OLD_ART2ID,'
      '?NDSID, ?OLD_NDSID,'
      '?PRICE, ?OLD_PRICE,'
      '?APPL_Q, ?OLD_APPL_Q,'
      '?USERID, ?USERID_WH,'
      '?APPLDEP_Q, ?OLD_APPLDEP_Q)')
    RefreshSQL.Strings = (
      
        'SELECT u.SITEMID, u.UID, a2.D_COMPID COMPID, a2.D_MATID MATID, a' +
        '2.D_GOODID GOODID,'
      
        '       a2.D_INSID INSID, a2.ART, a2.ART2, a2.s_ins, a2.UNITID,  ' +
        'u.NDSID, u.W, u.SZ,'
      
        '       FIF(a2.UnitId, u.Price*u.W, u.Price) COST, u.ART2ID ART2I' +
        'D, u.DEPID, u.SN0,'
      
        '       u.SSF0, u.SPRICE0, u.SUPID0, u.SDATE0, u.SN, u.SDATE, onl' +
        'ydate(u.SELLDATE)'
      
        '       SELLDATE, u.T, u.ISCLOSED, u.NDATE, u.ITYPE, u.PRODID,  a' +
        '2.PRODCODE ,'
      
        '       c2.Code SUPCODE, c2.SName SupName, u.PRICE, 1 USERID, u.R' +
        'etDate, d.SName Dep,'
      
        '       d.Color,a2.D_ARTID,FIF(a2.UnitId, u.SPrice0*u.W, u.SPrice' +
        '0) COST1,'
      
        '       A2.d_countryid, u.UIDWHID, u.NAmeDepFrom, u.colordf, u.go' +
        'ods1, u.goods2,'
      
        '       u.goodsid1, u.goodsid2, a2.ATT1, a2.ATT2, at1.name NameAt' +
        't1, '
      
        '       at2.name NameAtt2, dg.name NameGood, u.Appl_Q, u.USERID U' +
        'SERID_WH, u.ApplDep_q,'
      '       u.priceInv, u.Priceinv*u.q0 COSTInv'
      
        'FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 ' +
        'at2, d_good dg'
      'WHERE u.uidwhid= ?uidwhid and'
      '      d.D_DepId=u.DepId and'
      '      a2.Art2Id=u.Art2Id and'
      '      c2.D_CompId=u.SupId0 and'
      '      at1.id = a2.att1 and at2.id = a2.att2 and'
      '      dg.d_goodid = a2.d_goodid'
      '')
    SelectSQL.Strings = (
      'SELECT SITEMID, UID, UNITID, NDSID, PRICE, W,'
      '       SZ, COST, ART2ID, DEPID, SN0, SSF0,'
      '       SPRICE0, SUPID0, SDATE0, SN, SDATE, SELLDATE,'
      '       T, ISCLOSED, NDATE, ITYPE, PRODID, D_ARTID,'
      '       RETDATE, COMPID, MATID, GOODID, INSID,'
      '       ART, ART2, S_INS, PRODCODE, SUPCODE,'
      '       SUPNAME, DEP, COLOR, COST1, D_COUNTRYID,'
      '       USERID, UIDWHID, NAMEDEPFROM, colordf,'
      '       goods1, goods2, goodsid1, goodsid2, ATT1,'
      '       ATT2, NameAtt1, NameAtt2, NameGood, Appl_Q,'
      '       USERID_WH, ApplDep_q, APPLDEP_USER, priceinv, costinv'
      'FROM UIDWHARTPRICE(?USERID, ?DEPID1, ?DEPID2, ?I_T,'
      '     ?I_MAT, ?I_Art, ?I_good, ?I_comp, ?I_Supid0,'
      
        '     ?I_Goodsid1, ?I_Goodsid2, ?I_Att1, ?I_Att2, ?I_COST, ?I_SZ,' +
        ' ?I_ART2, ?I_W) '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    AfterOpen = quMaxUIDWHAfterOpen
    AfterPost = quMaxUIDWHAfterPost
    BeforeClose = quMaxUIDWHBeforeClose
    BeforeOpen = quMaxUIDWHBeforeOpen
    BeforePost = quMaxUIDWHBeforePost
    OnCalcFields = quMaxUIDWHCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 310
    Top = 279
    poSQLINT64ToBCD = True
    object quMaxUIDWHSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object quMaxUIDWHUID: TFIBIntegerField
      DisplayLabel = #1048#1076'.'#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object quMaxUIDWHUNITID: TFIBIntegerField
      DisplayLabel = #1045#1048
      FieldName = 'UNITID'
    end
    object quMaxUIDWHNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object quMaxUIDWHPRICE: TFIBFloatField
      DisplayLabel = #1056#1072#1089'. '#1094#1077#1085#1072
      FieldName = 'PRICE'
      currency = True
    end
    object quMaxUIDWHW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quMaxUIDWHSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084'.'
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      currency = True
    end
    object quMaxUIDWHART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quMaxUIDWHDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quMaxUIDWHSN0: TFIBIntegerField
      DisplayLabel = #1042#1085#1091#1090'.'#8470
      FieldName = 'SN0'
    end
    object quMaxUIDWHSSF0: TFIBStringField
      DisplayLabel = #1042#1085#1077#1096'.'#8470
      FieldName = 'SSF0'
      EmptyStrToNull = True
    end
    object quMaxUIDWHSPRICE0: TFIBFloatField
      DisplayLabel = #1055#1088'.'#1094#1077#1085#1072
      FieldName = 'SPRICE0'
      currency = True
    end
    object quMaxUIDWHSUPID0: TFIBIntegerField
      FieldName = 'SUPID0'
    end
    object quMaxUIDWHSDATE0: TFIBDateTimeField
      DisplayLabel = #1042#1085#1091#1090'.'#1076#1072#1090#1072
      FieldName = 'SDATE0'
    end
    object quMaxUIDWHSN: TFIBIntegerField
      DisplayLabel = #1042#1055
      FieldName = 'SN'
    end
    object quMaxUIDWHSDATE: TFIBDateTimeField
      DisplayLabel = #1042#1055' '#1076#1072#1090#1072
      FieldName = 'SDATE'
    end
    object quMaxUIDWHSELLDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'SELLDATE'
    end
    object quMaxUIDWHT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quMaxUIDWHISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quMaxUIDWHNDATE: TFIBDateTimeField
      DisplayLabel = #1042#1085#1077#1096'.'#1076#1072#1090#1072
      FieldName = 'NDATE'
    end
    object quMaxUIDWHITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quMaxUIDWHPRODID: TFIBIntegerField
      FieldName = 'PRODID'
    end
    object quMaxUIDWHD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quMaxUIDWHRETDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'RETDATE'
    end
    object quMaxUIDWHCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object quMaxUIDWHMATID: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHGOODID: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084'.'
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHINSID: TFIBStringField
      DisplayLabel = #1054#1042
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quMaxUIDWHART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083' 2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quMaxUIDWHS_INS: TFIBStringField
      DisplayLabel = #1042#1089#1090'.'
      FieldName = 'S_INS'
      Size = 200
      EmptyStrToNull = True
    end
    object quMaxUIDWHPRODCODE: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHSUPCODE: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      FieldName = 'SUPCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074'.'
      FieldName = 'SUPNAME'
      EmptyStrToNull = True
    end
    object quMaxUIDWHDEP: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quMaxUIDWHCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object quMaxUIDWHCOST1: TFIBFloatField
      FieldName = 'COST1'
      currency = True
    end
    object quMaxUIDWHD_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quMaxUIDWHUIDWHID: TFIBIntegerField
      FieldName = 'UIDWHID'
    end
    object quMaxUIDWHNAMEDEPFROM: TFIBStringField
      FieldName = 'NAMEDEPFROM'
      EmptyStrToNull = True
    end
    object quMaxUIDWHCOLORDF: TFIBIntegerField
      FieldName = 'COLORDF'
    end
    object quMaxUIDWHGOODS1: TFIBStringField
      FieldName = 'GOODS1'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHGOODS2: TFIBStringField
      FieldName = 'GOODS2'
      Size = 10
      EmptyStrToNull = True
    end
    object quMaxUIDWHGOODSID1: TFIBIntegerField
      FieldName = 'GOODSID1'
    end
    object quMaxUIDWHGOODSID2: TFIBIntegerField
      FieldName = 'GOODSID2'
    end
    object quMaxUIDWHATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quMaxUIDWHATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quMaxUIDWHNAMEATT1: TFIBStringField
      FieldName = 'NAMEATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object quMaxUIDWHNAMEATT2: TFIBStringField
      FieldName = 'NAMEATT2'
      Size = 80
      EmptyStrToNull = True
    end
    object quMaxUIDWHNAMEGOOD: TFIBStringField
      FieldName = 'NAMEGOOD'
      EmptyStrToNull = True
    end
    object quMaxUIDWHAPPL_Q: TFIBIntegerField
      FieldName = 'APPL_Q'
    end
    object quMaxUIDWHUSERID_WH: TFIBIntegerField
      FieldName = 'USERID_WH'
    end
    object quMaxUIDWHAPPLDEP_Q: TFIBIntegerField
      FieldName = 'APPLDEP_Q'
    end
    object quMaxUIDWHmemo: TStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldKind = fkCalculated
      FieldName = 'memo'
      Size = 31
      Calculated = True
    end
    object quMaxUIDWHNDSNAME: TStringField
      DisplayLabel = #1053#1044#1057
      FieldKind = fkLookup
      FieldName = 'NDSNAME'
      LookupDataSet = dmCom.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Size = 30
      Lookup = True
    end
    object quMaxUIDWHPRORDNAME: TStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldKind = fkLookup
      FieldName = 'PRODNAME'
      LookupDataSet = dmCom.quComp
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'snAME'
      KeyFields = 'COMPID'
      Size = 30
      Lookup = True
    end
    object quMaxUIDWHAPPLDEP_USER: TFIBIntegerField
      FieldName = 'APPLDEP_USER'
    end
    object quMaxUIDWHPRICEINV: TFIBFloatField
      FieldName = 'PRICEINV'
      currency = True
    end
    object quMaxUIDWHCOSTINV: TFIBFloatField
      FieldName = 'COSTINV'
      currency = True
    end
  end
  object quCountAppl: TpFIBDataSet
    SelectSQL.Strings = (
      'select first 1 ai.sq, ai.sq1'
      'from appldep_item ai'
      'where ai.artid=:artid and ai.sz=:sz')
    BeforeOpen = quCountApplBeforeOpen
    OnCalcFields = quCountApplCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsUIDWH
    Left = 808
    Top = 288
    poSQLINT64ToBCD = True
    dcForceMasterRefresh = True
    dcForceOpen = True
    object quCountApplSQ: TFIBStringField
      FieldName = 'SQ'
      Size = 200
      EmptyStrToNull = True
    end
    object quCountApplStSQ: TStringField
      FieldKind = fkCalculated
      FieldName = 'StSQ'
      Size = 200
      Calculated = True
    end
    object quCountApplSQ1: TFIBStringField
      FieldName = 'SQ1'
      Size = 200
      EmptyStrToNull = True
    end
    object quCountApplStSQ1: TStringField
      FieldKind = fkCalculated
      FieldName = 'StSQ1'
      Size = 200
      Calculated = True
    end
  end
  object dsCountAppl: TDataSource
    DataSet = quCountAppl
    Left = 808
    Top = 336
  end
  object taCardList: TpFIBDataSet
    SelectSQL.Strings = (
      'select sl.checkno, s.rn , d.SNAME, sum(sl.costcard) costcard,'
      '       sum(sl.q0*sl.price) cost, sum(sl.q0*sl.price0) cost0,'
      '       sl.sellid'
      'from SellItem sl, Sell s , D_DEP D'
      'where s.SellID=sl.SellID and'
      '      S.DEPID=D.D_DEPID and'
      '      D.D_DEPID between :depid1 and  :depid2 and'
      '      S.BD between :bd and :ed and'
      '      Card in (1, 2)'
      'group by sl.sellid, s.rn, d.sname, s.bd, sl.checkno')
    BeforeOpen = taSListBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 784
    Top = 8
    poSQLINT64ToBCD = True
    object taCardListCHECKNO: TFIBIntegerField
      FieldName = 'CHECKNO'
    end
    object taCardListRN: TFIBIntegerField
      FieldName = 'RN'
    end
    object taCardListSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taCardListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taCardListCOST0: TFIBFloatField
      FieldName = 'COST0'
      currency = True
    end
    object taCardListCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
      currency = True
    end
    object taCardListSELLID: TFIBIntegerField
      FieldName = 'SELLID'
    end
  end
  object dsrCardList: TDataSource
    DataSet = taCardList
    Left = 784
    Top = 52
  end
  object frDList: TfrDBDataSet
    DataSet = taDList
    Left = 800
    Top = 144
  end
  object frDinvitem: TfrDBDataSet
    DataSet = quDInvItem
    Left = 800
    Top = 192
  end
  object trCommission: TpFIBTransaction
    DefaultDatabase = dmCom.db
    TimeoutAction = TARollback
    Left = 64
    Top = 286
  end
  object spSetCommission: TpFIBStoredProc
    Transaction = trCommission
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE SET$COMMISION(?SINVID, ?COMMISSION)')
    StoredProcName = 'SET$COMMISION'
    Left = 24
    Top = 574
  end
  object dsUnitId: TDataSource
    DataSet = quUnitId
    Left = 120
    Top = 40
  end
  object quUnitId: TpFIBDataSet
    SelectSQL.Strings = (
      'select unitid, unit from get_unit_for_unitid')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 120
    Top = 8
    object quUnitIdUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quUnitIdUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object pmAnlzSelDed: TPopupMenu
    AutoHotkeys = maManual
    Left = 192
    Top = 528
    object MenuItem2: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
  object taAnlzSell_days: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select d_day, w_d, h10_cl,h11_cl,h12_cl,h13_cl,h14_cl,h15_cl,h16' +
        '_cl,h17_cl,h18_cl,h19_cl,h20_cl,h21_cl,'
      
        'h10_s,h11_s,h12_s,h13_s,h14_s,h15_s,h16_s,h17_s,h18_s,h19_s,h20_' +
        's,h21_s, itogo_cl, itogo_sell'
      'from sales_today_tohour(?adate,?bdate,?dep)')
    BeforeOpen = taAnlzSell_daysBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 864
    Top = 16
    object taAnlzSell_daysD_DAY: TFIBDateField
      FieldName = 'D_DAY'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAnlzSell_daysW_D: TFIBStringField
      FieldName = 'W_D'
      Size = 5
      EmptyStrToNull = True
    end
    object taAnlzSell_daysH10_CL: TFIBIntegerField
      FieldName = 'H10_CL'
    end
    object taAnlzSell_daysH11_CL: TFIBIntegerField
      FieldName = 'H11_CL'
    end
    object taAnlzSell_daysH12_CL: TFIBIntegerField
      FieldName = 'H12_CL'
    end
    object taAnlzSell_daysH13_CL: TFIBIntegerField
      FieldName = 'H13_CL'
    end
    object taAnlzSell_daysH14_CL: TFIBIntegerField
      FieldName = 'H14_CL'
    end
    object taAnlzSell_daysH15_CL: TFIBIntegerField
      FieldName = 'H15_CL'
    end
    object taAnlzSell_daysH16_CL: TFIBIntegerField
      FieldName = 'H16_CL'
    end
    object taAnlzSell_daysH17_CL: TFIBIntegerField
      FieldName = 'H17_CL'
    end
    object taAnlzSell_daysH18_CL: TFIBIntegerField
      FieldName = 'H18_CL'
    end
    object taAnlzSell_daysH19_CL: TFIBIntegerField
      FieldName = 'H19_CL'
    end
    object taAnlzSell_daysH20_CL: TFIBIntegerField
      FieldName = 'H20_CL'
    end
    object taAnlzSell_daysH21_CL: TFIBIntegerField
      FieldName = 'H21_CL'
    end
    object taAnlzSell_daysH10_S: TFIBIntegerField
      FieldName = 'H10_S'
    end
    object taAnlzSell_daysH11_S: TFIBIntegerField
      FieldName = 'H11_S'
    end
    object taAnlzSell_daysH12_S: TFIBIntegerField
      FieldName = 'H12_S'
    end
    object taAnlzSell_daysH13_S: TFIBIntegerField
      FieldName = 'H13_S'
    end
    object taAnlzSell_daysH14_S: TFIBIntegerField
      FieldName = 'H14_S'
    end
    object taAnlzSell_daysH15_S: TFIBIntegerField
      FieldName = 'H15_S'
    end
    object taAnlzSell_daysH16_S: TFIBIntegerField
      FieldName = 'H16_S'
    end
    object taAnlzSell_daysH17_S: TFIBIntegerField
      FieldName = 'H17_S'
    end
    object taAnlzSell_daysH18_S: TFIBIntegerField
      FieldName = 'H18_S'
    end
    object taAnlzSell_daysH19_S: TFIBIntegerField
      FieldName = 'H19_S'
    end
    object taAnlzSell_daysH20_S: TFIBIntegerField
      FieldName = 'H20_S'
    end
    object taAnlzSell_daysH21_S: TFIBIntegerField
      FieldName = 'H21_S'
    end
    object taAnlzSell_daysITOGO_CL: TFIBIntegerField
      FieldName = 'ITOGO_CL'
    end
    object taAnlzSell_daysITOGO_SELL: TFIBIntegerField
      FieldName = 'ITOGO_SELL'
    end
  end
  object dsrAnzSell_days: TDataSource
    DataSet = taAnlzSell_days
    Left = 864
    Top = 64
  end
  object dsCassa: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  ch.department$id, '
      '  ch.device,'
      '  d.name,'
      '  count(*) Checks$Count,'
      '  smartround(sum(ch.s),1, 100) as Summa'
      'from '
      '  cash ch'
      '  left join d_dep d on (d.d_depid =  ch.department$id)'
      'where '
      '  ch.session$date between :bd and :ed and '
      '  ch.department$id between :depid1 and :depid2 and '
      '  ch.device > 0'
      'group by '
      '  ch.device, '
      '  ch.department$id,d.name'
      'order by '
      '  d.name'
      ''
      '')
    BeforeOpen = dsCassaBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 880
    Top = 144
    object dsCassaDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object dsCassaDEVICE: TFIBBCDField
      FieldName = 'DEVICE'
      Size = 0
      RoundByScale = True
    end
    object dsCassaSUMMA: TFIBFloatField
      FieldName = 'SUMMA'
    end
    object dsCassaNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object dsCassaCHECKSCOUNT: TFIBIntegerField
      FieldName = 'CHECKS$COUNT'
    end
  end
  object taTollingParams: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Tolling$Params set'
      '  Average$Price = :Average$Price,'
      '  Total$Losses$Weight = :Total$Losses$Weight,'
      '  Total$Insertions$Weight = :Total$Insertions$Weight,'
      '  Total$Insertions$Cost = :Total$Insertions$Cost,'
      '  Total$Invoice$Weight = :Total$Invoice$Weight,'
      '  Pure$K = :Pure$K,'
      '  Include$Insertions = :Include$Insertions'
      'where Invoice$ID = :Invoice$ID')
    InsertSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      'select'
      '  Invoice$ID,'
      '  Average$Price,'
      '  Total$Losses$Weight,'
      '  Total$Insertions$Weight,'
      '  Total$Insertions$Cost,'
      '  Total$Invoice$Weight,'
      '  Pure$K,'
      '  cast(Pure$K * 1000 as Integer) Pure,'
      '  Include$Insertions'
      'from Tolling$Params$s(:Invoice$ID)')
    SelectSQL.Strings = (
      'select'
      '  Invoice$ID, '
      '  Average$Price,'
      '  Total$Losses$Weight,'
      '  Total$Insertions$Weight,'
      '  Total$Insertions$Cost,'
      '  Total$Invoice$Weight,'
      '  Pure$K,'
      '  cast(Pure$K * 1000 as Integer) Pure,'
      '  Include$Insertions'
      'from Tolling$Params$s(:Invoice$ID)')
    BeforeClose = taTollingParamsBeforeClose
    BeforeOpen = taTollingParamsBeforeOpen
    BeforePost = taTollingParamsBeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 888
    Top = 272
    object taTollingParamsAVERAGEPRICE: TFIBFloatField
      FieldName = 'AVERAGE$PRICE'
    end
    object taTollingParamsTOTALLOSSESWEIGHT: TFIBBCDField
      FieldName = 'TOTAL$LOSSES$WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object taTollingParamsTOTALINSERTIONSWEIGHT: TFIBBCDField
      FieldName = 'TOTAL$INSERTIONS$WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object taTollingParamsTOTALINSERTIONSCOST: TFIBBCDField
      FieldName = 'TOTAL$INSERTIONS$COST'
      Size = 2
      RoundByScale = True
    end
    object taTollingParamsTOTALINVOICEWEIGHT: TFIBBCDField
      FieldName = 'TOTAL$INVOICE$WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object taTollingParamsINVOICEID: TFIBIntegerField
      FieldName = 'INVOICE$ID'
    end
    object taTollingParamsPUREK: TFIBFloatField
      FieldName = 'PURE$K'
    end
    object taTollingParamsPURE: TFIBIntegerField
      FieldName = 'PURE'
    end
    object taTollingParamsINCLUDEINSERTIONS: TFIBIntegerField
      FieldName = 'INCLUDE$INSERTIONS'
    end
  end
  object dsTollingParams: TDataSource
    DataSet = taTollingParams
    Left = 888
    Top = 328
  end
  object PmRET: TPopupMenu
    AutoHotkeys = maManual
    Left = 527
    Top = 322
    object MenuItem1: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      GroupIndex = 1
      RadioItem = True
      OnClick = SellClick
    end
    object MenuItem19: TMenuItem
      Caption = '-'
      GroupIndex = 1
      RadioItem = True
    end
  end
end
