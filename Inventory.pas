unit Inventory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls,  
  RXCtrls, Menus, ActnList, PrnDbgeh, Printers, PrntsEh, ComDrv32, TB2Item,
  StdCtrls, Buttons, CheckLst, DBTree, cxProgressBar, M207Ctrls,
  cxControls, cxContainer, cxEdit, DBGridEhGrouping, dxSkinsCore,
  dxSkinsDefaultPainters, rxPlacemnt, GridsEh, rxSpeedbar, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters;

type
  TfmInventory = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    dg1: TDBGridEh;
    fr: TM207FormStorage;
    sidescription: TSpeedItem;
    rlbfilter: TRxCheckListBox;
    pdg1: TPrintDBGridEh;
    acList: TActionList;
    acPrintGrid: TAction;
    pmChange: TTBPopupMenu;
    bidel: TTBItem;
    biisin: TTBItem;
    biisnotin: TTBItem;
    pmPrint: TTBPopupMenu;
    ibInventoryB: TTBItem;
    siSetting: TSpeedItem;
    ibsubscriptionB: TTBItem;
    ibsubscriptionE: TTBItem;
    ibprord: TTBSubmenuItem;
    iborganisation: TTBItem;
    sitAct: TTBSubmenuItem;
    SpeedBar: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    Label1: TLabel;
    edUID: TEdit;
    acSetting: TAction;
    siMol: TSpeedItem;
    acAddUID: TAction;
    acMOL: TAction;
    spbr4: TSpeedBar;
    lbMat: TLabel;
    lbGood: TLabel;
    lbComp: TLabel;
    lbSup: TLabel;
    ScrollBar1: TScrollBar;
    SpeedbarSection3: TSpeedbarSection;
    sdicomp: TSpeedItem;
    sdisup: TSpeedItem;
    sdiGood: TSpeedItem;
    sdiMat: TSpeedItem;
    spbr5: TSpeedBar;
    lbnote1: TLabel;
    lbnote2: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ScrollBar2: TScrollBar;
    SpeedbarSection5: TSpeedbarSection;
    sdinote2: TSpeedItem;
    sdinote1: TSpeedItem;
    sdiAtr1: TSpeedItem;
    sdiAtr2: TSpeedItem;
    chlbMat: TCheckListBox;
    chlbGood: TCheckListBox;
    chlbComp: TCheckListBox;
    chlbSup: TCheckListBox;
    btnDo: TSpeedButton;
    chlbNote1: TCheckListBox;
    chlbNote2: TCheckListBox;
    chlbAtr1: TCheckListBox;
    chlbAtr2: TCheckListBox;
    acPrintInventoryB: TAction;
    acPrintInventoryE: TAction;
    acPrintSubscriptionB: TAction;
    acPrintSubscriptionE: TAction;
    prbar1: TcxProgressBar;
    siClearInventory: TSpeedItem;
    acClear: TAction;
    siClosed: TSpeedItem;
    acClosed: TAction;
    siText: TSpeedItem;
    acText: TAction;
    svdFile: TSaveDialog;
    siDep: TSpeedItem;
    pmdep: TTBPopupMenu;
    siFactInv: TTBSubmenuItem;
    ibFactorganisation: TTBItem;
    acTextpr: TAction;
    pmtext: TTBPopupMenu;
    biprice: TTBItem;
    bisprice: TTBItem;
    siActLack: TSpeedItem;
    acActLack: TAction;
    siSurplus: TSpeedItem;
    acSurPlus: TAction;
    itActOrganization: TTBSubmenuItem;
    siHelp: TSpeedItem;
    lbFind: TLabel;
    edFindUid: TEdit;
    siFindAll: TSpeedItem;
    acFindAll: TAction;
    chFindSelect: TCheckBox;
    acChangeStateTo1: TAction;
    acChangeStateTo2: TAction;
    acChangeStateTo0: TAction;
    TBItem1: TTBItem;
    acDelExcessUID: TAction;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure sidescriptionClick(Sender: TObject);
    procedure rlbfilterClickCheck(Sender: TObject);
    procedure dg1MouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure acPrintGridExecute(Sender: TObject);
    procedure edUIDKeyPress(Sender: TObject; var Key: Char);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acSettingExecute(Sender: TObject);
    procedure acAddUIDUpdate(Sender: TObject);
    procedure acMOLExecute(Sender: TObject);
    procedure acMOLUpdate(Sender: TObject);
    procedure sdiMatClick(Sender: TObject);
    procedure sdiGoodClick(Sender: TObject);
    procedure sdicompClick(Sender: TObject);
    procedure sdisupClick(Sender: TObject);
    procedure sdinote1Click(Sender: TObject);
    procedure sdinote2Click(Sender: TObject);
    procedure sdiAtr1Click(Sender: TObject);
    procedure sdiAtr2Click(Sender: TObject);
    procedure chlbMatClickCheck(Sender: TObject);
    procedure chlbGoodClickCheck(Sender: TObject);
    procedure chlbCompClickCheck(Sender: TObject);
    procedure chlbSupClickCheck(Sender: TObject);
    procedure chlbNote1ClickCheck(Sender: TObject);
    procedure chlbNote2ClickCheck(Sender: TObject);
    procedure chlbAtr1ClickCheck(Sender: TObject);
    procedure chlbAtr2ClickCheck(Sender: TObject);
    procedure btnDoClick(Sender: TObject);
    procedure chlbMatExit(Sender: TObject);
    procedure acPrintInventoryBExecute(Sender: TObject);
    procedure acPrintSubscriptionBExecute(Sender: TObject);
    procedure acPrintSubscriptionEExecute(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure acClosedUpdate(Sender: TObject);
    procedure acClosedExecute(Sender: TObject);
    procedure acClearUpdate(Sender: TObject);
    procedure acTextExecute(Sender: TObject);
    procedure acTextprExecute(Sender: TObject);
    procedure acActLackUpdate(Sender: TObject);
    procedure acActLackExecute(Sender: TObject);
    procedure acSurPlusExecute(Sender: TObject);
    procedure acSurPlusUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edFindUidKeyPress(Sender: TObject; var Key: Char);
    procedure edFindUidKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acFindAllExecute(Sender: TObject);
    procedure acFindAllUpdate(Sender: TObject);
    procedure acChangeStateTo1Execute(Sender: TObject);
    procedure acChangeStateTo1Update(Sender: TObject);
    procedure acChangeStateTo2Execute(Sender: TObject);
    procedure acChangeStateTo2Update(Sender: TObject);
    procedure acChangeStateTo0Execute(Sender: TObject);
    procedure acChangeStateTo0Update(Sender: TObject);
    procedure acDelExcessUIDUpdate(Sender: TObject);
    procedure acDelExcessUIDExecute(Sender: TObject);
  private
    FParentLogId : string;
    procedure pmDepClick(Sender: TObject);
    procedure ActPrintClick(Sender: TObject);
    function GetIdxByCode(sl: TStringList; Code: Variant): integer;
    procedure pmDepSelClick(Sender: TObject);
    procedure FactPrintClick(Sender: TObject);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;    
  end;

var
  fmInventory: TfmInventory;
  select_str: boolean;

implementation

uses servdata, M207Proc, comdata, data, reportdata, PredInventory, dbUtil, InsRepl, Mol,
     UIDWH, JewConst, Data3, FIBQuery, MsgDialog, DB;

{$R *.dfm}
procedure TfmInventory.pmDepSelClick(Sender: TObject);
begin
 dmcom.FilterDepID:=(Sender as TComponent).Tag;
 sidep.BtnCaption:=TTBItem(Sender).Caption;
 ReOpenDataSet(dmserv.quInventory);
end;

procedure TfmInventory.FactPrintClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintAfter, FParentLogId);
  dmcom.FilterRepDepID:=TTBItem(Sender).Tag;
  dmReport.PrintDocumentA(VarArrayOf([TTBItem(Sender).Tag]), inventory_e);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.pmDepClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintOrder, FParentLogId);
  dmReport.PrintDocumentA(VarArrayOf([(Sender as TComponent).Tag]), inventory_order);
  dm3.update_operation(LogOperationID);
end;

function TfmInventory.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      eXit;
    end;
end;



procedure TfmInventory.FormCreate(Sender: TObject);
var
  pm: TTBItem;
  pmit: TTBSubmenuItem;
  SubPm: TTBItem;
begin
  dm.WorkMode:='INVENTORY';
  tb1.Wallpaper:=wp;
  SpeedBar.Wallpaper:=wp;
  spbr4.Wallpaper:=wp;
  spbr5.Wallpaper:=wp;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
   {����������� ������������ ����������� ��� ����������� ������ ������}
  with dmserv, dmcom, quTmp do
   begin
    if not dmcom.tr.Active then dmcom.tr.StartTransaction else dmcom.tr.CommitRetaining;

    close;
    sql.Text:='select precent from Precent_Execution_Inventory ('+inttostr(dmserv.CurInventory)+')';
    ExecQuery;
    Inventory_Precent:=round(Fields[0].AsFloat);
    Transaction.CommitRetaining;
    close;
    prbar1.Position:=Inventory_Precent;
   end;


  (* ��� ������ �������� ������� ����.���� *)
  dg1.FieldColumns['PRICE0'].Visible := CenterDep;
  dg1.FieldColumns['COSTP'].Visible := CenterDep;
  (* ���������� ������� ������� *)
//  SpeedBar.Visible := CenterDep;

 if dmserv.quListInventoryISCLOSED.AsInteger=0 then
 begin
{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;

  dmServ.ComScan.Connect;
  dmcom.SScanZ:='';   

  if dmServ.ComScan2.Connected then dmServ.ComScan2.Disconnect;
  if dmcom.ScanComM='COM1' then dmserv.ComScan2.ComPort:=pnCOM1
   else  if dmcom.ScanComM='COM2' then dmserv.ComScan2.ComPort:=pnCOM2
    else  if dmcom.ScanComM='COM3' then dmserv.ComScan2.ComPort:=pnCOM3
     else  dmserv.ComScan2.ComPort:=pnCOM4;

  dmServ.ComScan2.Connect;
  dmcom.SScanM:='';      }
 end;

 dmserv.FInvIn:='';
 {����������}
   dmcom.D_MatId_I:=dmcom.D_MatId;
   dmcom.D_GoodId_I:=dmcom.D_GoodId;
   dmcom.D_CompId_I:=dmcom.D_CompId;
   dmcom.D_SupId_I :=dmcom.D_SupId;
   dmcom.D_Att1ID_I:=dmcom.D_Att1ID;
   dmcom.D_Att2ID_I:=dmcom.D_Att2ID;
   dmcom.D_Note1_I:=dmcom.D_Note1;
   dmcom.D_Note2_I:=dmcom.D_Note2;

   dmcom.D_MatId:='*';
   dmcom.D_GoodId:='*';
   dmcom.D_CompId:=-1;
   dmcom.D_SupId := -1;
   dmcom.D_Att1ID:=ATT1_DICT_ROOT;
   dmcom.D_Att2ID:=ATT2_DICT_ROOT;
   dmcom.D_Note1:=-1;
   dmcom.D_Note2:=-1;
   dm.SD_MatID_I:='';
   dm.SD_GoodID_I:='';
   dm.SD_SupID_I:='';
   dm.SD_CompID_I:='';
   dm.SD_Note1_I:='';
   dm.SD_Note2_I:='';
   dm.SD_Att1_I:='';
   dm.SD_Att2_I:='';
 {**********}
 /////////////////////////////////////
  if CenterDep then
  begin

   pm:=TTBItem.Create(pmdep);
   pm.Caption:='��� ������';
   pm.Tag:=0;
   pm.OnClick:=pmDepSelClick;

   pmdep.Items.Add(pm);

   {��������� �������}
   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid<>-1000 and IsDelete <> 1';
   dm.quTmp.ExecQuery;

   while not (dm.qutmp.Eof) do
   begin
    pm:=TTBItem.Create(pmdep);
    pm.Caption:=dm.quTmp.Fields[0].Asstring;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=pmDepSelClick;

    pmdep.Items.Add(pm);

    dm.quTmp.Next;
   end;

   dm.quTmp.Transaction.CommitRetaining;
   dm.quTmp.Close;
   dmcom.FilterDepID:=0;
  end
  else
  begin
   sidep.Visible:=false;
   dmcom.FilterDepID:=SelfDepId;
  end;

 ReOpenDataSets([dmServ.quInventory]);
 /////////////////////////////////////
 if CenterDep then
 begin
  {��������� �������}
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid, wh from d_dep where d_depid<>-1000 and IsDelete <> 1';
  dm.quTmp.ExecQuery;

  while not (dm.qutmp.Eof) do
  begin
{   if dm.quTmp.Fields[2].AsInteger<>1 then
   begin}
    // ��� ��������
    pm:=TTBItem.Create(ibprord);
    pm.Caption:=dm.quTmp.Fields[0].Asstring;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=pmDepClick;
    ibprord.Add(pm);

    // ��� �����

    if {dm.quTmp.Fields[1].AsInteger = CenterDepId}true then
    begin
      pmit:=TTBSubmenuItem.Create(sitAct);
      pmit.Caption:=dm.quTmp.Fields[0].AsString;
      pmit.Tag:=dm.quTmp.Fields[1].AsInteger;

      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      sitAct.Add(pmit);
    end
    else
    begin
      pm:=TTBItem.Create(sitAct);
      pm.Caption:=dm.quTmp.Fields[0].AsString;
      pm.Tag:=dm.quTmp.Fields[1].AsInteger;
      pm.OnClick:=ActPrintClick;
      sitAct.Add(pm);
    end;

    //���������� �� ������
    pm:=TTBItem.Create(siFactInv);
    pm.Caption:=dm.quTmp.Fields[0].AsString;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=FactPrintClick;
    siFactInv.Add(pm);

//   end;
   dm.quTmp.Next;
  end;

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select c.Name from d_comp c, d_rec r where c.d_compid=r.d_compid';
  dm.quTmp.ExecQuery;
  iborganisation.Caption:=dm.quTmp.Fields[0].Asstring;
  iborganisation.OnClick := pmDepClick;

  itActOrganization.Caption := dm.quTmp.Fields[0].Asstring;

  SubPm :=TTBItem.Create(itActOrganization);
  SubPm.Caption:='����.����';
  SubPm.Tag:= 0;
  SubPm.OnClick:=ActPrintClick;
  itActOrganization.Add(SubPm);
  SubPm :=TTBItem.Create(itActOrganization);
  SubPm.Caption:='����.����';
  SubPm.Tag:=0;
  SubPm.OnClick:=ActPrintClick;
  itActOrganization.Add(SubPm);

//  itActOrganization.OnClick := ActPrintClick;

  ibFactorganisation.Caption:=dm.quTmp.Fields[0].Asstring;
  ibFactorganisation.OnClick :=FactPrintClick;

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
 end
 else
 begin
   iborganisation.Visible:=false;
   itActOrganization.Visible := False;
   ibFactorganisation.Visible := False;

   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid='+inttostr(selfdepid);
   dm.quTmp.ExecQuery;

   // ��� ��������
   pm:=TTBItem.Create(ibprord);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;
   ibprord.Add(pm);


   // ��� �����
   pm:=TTBItem.Create(sitAct);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=ActPrintClick;
   sitAct.Add(pm);


   // ���������� �� ������
   pm:=TTBItem.Create(siFactInv);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=FactPrintClick;
   siFactInv.Add(pm);

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
 end;

 {****************}
   dm.lMultiSelect_I:=false;

   chlbComp.Items.Assign(dm.slProd);
   chlbComp.ItemIndex := GetIdxByCode(dm.slProd, dmcom.D_CompId);
   chlbComp.Checked[chlbComp.ItemIndex]:=true;
   chlbCompClickCheck(chlbComp);

   chlbSup.Items.Assign(dm.slSup);
   chlbSup.ItemIndex := GetIdxByCode(dm.slSup, dmcom.D_SupId);
   chlbSup.Checked[chlbSup.ItemIndex]:=true;
   chlbSupClickCheck(chlbSup);

   chlbMat.Items.Assign(dm.slMat);
   chlbMat.ItemIndex := GetIdxByCode(dm.slMat, dmcom.D_MatId);
   chlbMat.Checked[chlbMat.ItemIndex]:=true;
   chlbMatClickCheck(chlbMat);

   chlbGood.Items.Assign(dm.slGood);
   chlbGood.ItemIndex := GetIdxByCode(dm.slGood, dmcom.D_GoodId);
   chlbGood.Checked[chlbGood.ItemIndex]:=true;
   chlbGoodClickCheck(chlbGood);

   chlbnote1.Items.Assign(dm.slNote1);
   chlbnote1.ItemIndex := GetIdxByCode(dm.slNote1, dmcom.D_Note1);
   chlbnote1.Checked[chlbnote1.ItemIndex]:=true;
   chlbNote1ClickCheck(chlbnote1);

   chlbnote2.Items.Assign(dm.slNote2);
   chlbnote2.ItemIndex := GetIdxByCode(dm.slNote2, dmcom.D_Note2);
   chlbnote2.Checked[chlbnote2.ItemIndex]:=true;
   chlbnote2ClickCheck(chlbnote2);

   chlbAtr1.Items.Assign(dm.slAtt1);
   chlbAtr1.ItemIndex := GetIdxByCode(dm.slAtt1, dmcom.D_Att1Id);
   chlbAtr1.Checked[chlbAtr1.ItemIndex]:=true;
   chlbAtr1ClickCheck(chlbAtr1);

   chlbAtr2.Items.Assign(dm.slAtt2);
   chlbAtr2.ItemIndex := GetIdxByCode(dm.slAtt2, dmcom.D_Att2Id);
   chlbAtr2.Checked[chlbAtr2.ItemIndex]:=true;
   chlbAtr2ClickCheck(chlbAtr2);
 {****************}
  if (CenterDep) and (dmserv.quListInventoryISCLOSED.AsInteger=0) then edUID.Enabled:=true
  else edUID.Enabled:=false;

  FParentLogId := dm3.defineOperationId(dm3.LogUserID); 
end;

procedure TfmInventory.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;     ************
// if dmServ.ComScan2.Connected then dmServ.ComScan2.Disconnect;   ************
 CloseDataSets([dmServ.quInventory]);
 dm.WorkMode:='UIDWH';
 dmcom.D_MatId:=dmcom.D_MatId_I;
 dmcom.D_GoodId:=dmcom.D_GoodId_I;
 dmcom.D_CompId:=dmcom.D_CompId_I;
 dmcom.D_SupId :=dmcom.D_SupId_I;
 dmcom.D_Att1ID:=dmcom.D_Att1ID_I;
 dmcom.D_Att2ID:=dmcom.D_Att2ID_I;
 dmcom.D_Note1:=dmcom.D_Note1_I;
 dmcom.D_Note2:=dmcom.D_Note2_I;
end;

procedure TfmInventory.siExitClick(Sender: TObject);
begin
 dm.quTmp.Close;
 close;
end;

procedure TfmInventory.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmInventory.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);

begin
 if Column.Field<>NIL then
 begin
 {if dg1.Focused then
   Background:=clMoneyGreen
   else
   begin}

  if Column.Field.FieldName='SNAME' then Background:= dmserv.quInventoryCOLOR.AsInteger//���������
  else
  begin
    if dmserv.quInventoryISIN.AsInteger=0 then Background:=clInfoBk//������-������
    else if dmserv.quInventoryISIN.AsInteger=1 then
    begin
    if ((dmserv.quInventoryOrder_.AsInteger) mod 2=0) then Background:=clMedGray//�����
      else
      begin
      Background:=clTeal;
      Afont.Color:=clWhite;
      end;
    end
        else if dmserv.quInventoryISIN.AsInteger=2 then Background:=clRed//�������
  end
  //end

 end;

end;

procedure TfmInventory.sidescriptionClick(Sender: TObject);
begin
 rlbfilter.Visible:=true;
end;

procedure TfmInventory.rlbfilterClickCheck(Sender: TObject);
var i:integer;
begin
 if rlbfilter.ItemIndex=0 then
 begin
  for i:=1 to rlbfilter.Items.Count-1 do rlbfilter.Checked[i]:=false;
  dmserv.FInvIn:='';
 end
 else
 begin
  rlbfilter.Checked[0]:=false;
  dmserv.FInvIn:='';
  for i:=1 to rlbfilter.Items.Count-1 do
   if rlbfilter.Checked[i] then
   case i of
   1: dmserv.FInvIn:=dmserv.FInvIn+'1,';
   2: dmserv.FInvIn:=dmserv.FInvIn+'0,';
   3: dmserv.FInvIn:=dmserv.FInvIn+'2,';
   end;
  if dmserv.FInvIn<>'' then
  begin
   delete(dmserv.FInvIn, length(dmserv.FInvIn), 1);
   dmserv.FInvIn:='('+dmserv.FInvIn+')'
  end;
 end;
end;

procedure TfmInventory.dg1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if rlbfilter.Visible then
 begin
  rlbfilter.Visible:=false;
  ReOpenDataSets([dmserv.quInventory]);
 end
end;

procedure TfmInventory.acPrintGridExecute(Sender: TObject);
var i:integer;
    s:string;
begin
 s:='';
 for i:=0 to rlbfilter.Items.Count-1 do
 if rlbfilter.Checked[i] then
 case i of
 0: s:='��� �������;';
 1: s:=s+' ������� ��������� �� ������;';
 2: s:=s+' ������� �� ��������� �� ������;';
 3: s:=s+' ������� �� �������� �� ������;';
 end;

 if s<>'' then pdg1.Title.Add(s);

 VirtualPrinter.Orientation := poLandscape;
 pdg1.Print;
end;

procedure TfmInventory.ActPrintClick(Sender: TObject);
var
  Tag, c, DepId1, DepId2 : integer;
  LogOperationID: string;
  s: string;
begin
  Tag := (Sender as TComponent).Tag;
  // ��������� ����� ����� ���� ��������
  if (Tag=0) then
  begin
    DepId1 := -MAXINT;
    DepId2 := MAXINT;
  end
  else begin
    DepId1 := Tag;
    DepId2 := Tag;
  end;
  s := (Sender as TTbItem).Caption;
  if (Sender as TTbItem).Caption = '����.����' then
    dmReport.InventOutPrice := 0
  else
    dmReport.InventOutPrice := 1;
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintBefore, FParentLogId);
  if centerdep then
   c := ExecSelectSQL('select count(*) from Inventory inv, Inventory_Head ih where inv.ISIN in (0,2) and '+
     'inv.DepId between '+IntToStr(DepId1)+' and '+IntToStr(DepId2)+' and '+
     'inv.sinvid=ih.sinvid and ih.hid='+inttostr(dmserv.CurInventory), dm.quTmp)
  else
   c := ExecSelectSQL('select count(*) from Inventory where ISIN in (0,2) and '+
     'DepId between '+IntToStr(DepId1)+' and '+IntToStr(DepId2)+' and '+
     'sinvid = '+inttostr(dmServ.CurInventory), dm.quTmp);

  if(c <> 0)then
     dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([Tag]), VarArrayOf([DepId1, DepId2]), VarArrayOf([DepId1, DepId2])]), inventory_act)
  else
    dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([Tag]), VarArrayOf([DepId1, DepId2])]), inventory_act_null);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.edUIDKeyPress(Sender: TObject; var Key: Char);
begin
  case key of
    '0'..'9', #8: ;
    else sysutils.Abort;
  end;
end;

procedure TfmInventory.edUIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var FExists, order_, isin:integer;
    r:variant;
    inventoryid:integer;
begin
 with dmserv do
  case key of
    VK_RETURN:begin
     if CenterDep and (dmserv.quListInventoryISCLOSED.AsInteger=0) then
     begin
      with dmcom, quTmp do
      begin
       close;
       sql.Text:='select FExists, order_, isin from EDIT_INVENTORY('+eduid.Text+', 0, '+inttostr(CurInventory)+')';
       ExecQuery;
       FExists:=Fields[0].AsInteger;
       order_:=Fields[1].AsInteger;
       isin:=Fields[2].AsInteger;
       close;
       Transaction.CommitRetaining;
      end;
      if FExists=1 then
      begin
       if quInventory.Locate('UID',eduid.Text,[]) then
       begin
        quInventory.Edit;
        quInventoryORDER_.AsInteger:=order_;
        quInventoryISIN.AsInteger:=isin;
        quInventory.Post;
        quInventory.Refresh;
        inventoryid:=quInventoryINVENTORYID.AsInteger;
        quInventory.Delete;
        quInventory.Append;
        quInventoryINVENTORYID.AsInteger:=inventoryid;
        quInventoryFL.AsInteger:=-1;
        quInventory.Post;
        quInventory.Refresh;
       end
       else
       with dmcom, quTmp do
       begin
        close;
        sql.Text:='select FExists, order_, isin from EDIT_INVENTORY('+eduid.Text+', 1, '+inttostr(CurInventory)+')';
        ExecQuery;
        Transaction.CommitRetaining;
        close;
       end;
      end
      else
      begin
       if CenterDep then
       begin
        r:=ExecSelectSQL('select sinvid from find_sinvid ('+IntToStr(dmServ.CurInventory )+')', dmCom.quTmp);
        if VarIsNull(r) then
         raise Exception.Create('�� ������� ������ � sinv');
       end;

       quInventory.Append;
       quInventoryINVENTORYID.AsInteger:=dmcom.GetID(45);
       quInventoryORDER_.AsInteger:=order_;
       quInventoryUID.AsInteger:=strtoint(eduid.Text);
       quInventoryFL.AsInteger:=0;
       if CenterDep then quInventorySINVID.AsInteger:=r
       else quInventorySINVID.AsInteger:=quListInventorySINVID.AsInteger;
       quInventory.Post;
       quInventory.Refresh;
       quInventory.Edit;
       quInventoryFL.AsInteger:=-1;
       quInventory.Post;
       quInventory.Refresh;
      end;

    {����������� ������������ ����������� ��� ����������� ������ ������}
     with dmserv, dmcom, quTmp do
     begin
      close;
      sql.Text:='select precent from Precent_Execution_Inventory('+inttostr(dmserv.CurInventory)+')';
      ExecQuery;
      Inventory_Precent:=round(Fields[0].AsFloat);
      Transaction.CommitRetaining;
      close;
      prbar1.Position:=Inventory_Precent;
     end;
     eduid.Text:='';
    end
  end;
end;
end;

procedure TfmInventory.acSettingExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventorySetting, FParentLogId);
  ShowAndFreeForm(TfmPredInverty, Self, TForm(fmPredInverty), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acAddUIDUpdate(Sender: TObject);
begin
  acAddUID.Visible := CenterDep;
end;

procedure TfmInventory.acMOLExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryMOL, FParentLogId);
  ShowAndFreeForm(TfmMol, Self, TForm(fmMol), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acMOLUpdate(Sender: TObject);
begin
  acMOL.Visible := CenterDep;
end;

procedure TfmInventory.sdiMatClick(Sender: TObject);
begin
  dm.SetVisEnabled(chlbMat,True);
  chlbMat.SetFocus;
end;

procedure TfmInventory.sdiGoodClick(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGood,True);
  chlbGood.SetFocus;
end;

procedure TfmInventory.sdicompClick(Sender: TObject);
begin
  dm.SetVisEnabled(chlbComp,True);
  chlbComp.SetFocus;
end;

procedure TfmInventory.sdisupClick(Sender: TObject);
begin
  dm.SetVisEnabled(chlbSup,True);
  chlbSup.SetFocus;
end;

procedure TfmInventory.sdinote1Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbNote1,True);
  chlbNote1.SetFocus;
end;

procedure TfmInventory.sdinote2Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbNote2,True);
  chlbNote2.SetFocus;
end;

procedure TfmInventory.sdiAtr1Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbAtr1,True);
  chlbAtr1.SetFocus;
end;

procedure TfmInventory.sdiAtr2Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbAtr2,True);
  chlbAtr2.SetFocus;
end;

procedure TfmInventory.chlbMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_MatId:=fmUIDWH.ChlbClickCheck(chlbMat,s);
 sdiMat.BtnCaption:=s;
end;

procedure TfmInventory.chlbGoodClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GoodId:=fmUIDWH.ChlbClickCheck(chlbGood,s);
 sdiGood.BtnCaption:=s;
end;

procedure TfmInventory.chlbCompClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_CompId:=fmUIDWH.ChlbClickCheck(chlbComp,s);
 sdicomp.BtnCaption:=s;
end;

procedure TfmInventory.chlbSupClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_SupId:=fmUIDWH.ChlbClickCheck(chlbSup,s);
 sdisup.BtnCaption:=s;
end;

procedure TfmInventory.chlbNote1ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Note1:=fmUIDWH.ChlbClickCheck(chlbNote1,s);
 sdinote1.BtnCaption:=s;
end;

procedure TfmInventory.chlbNote2ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Note2:=fmUIDWH.ChlbClickCheck(chlbNote2,s);
 sdinote2.BtnCaption:=s;
end;

procedure TfmInventory.chlbAtr1ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Att1Id:=fmUIDWH.ChlbClickCheck(chlbAtr1,s);
 sdiAtr1.BtnCaption:=s;
end;

procedure TfmInventory.chlbAtr2ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Att2Id:=fmUIDWH.ChlbClickCheck(chlbAtr2,s);
 sdiAtr2.BtnCaption:=s;
end;

procedure TfmInventory.btnDoClick(Sender: TObject);
var l1,l2,l3,l4,l5,l6,l7,l8:boolean;
begin
  l1:=fmUIDWH.CheckMultiContr(chlbComp,dm.SD_CompId_I);
  if l1 then dmCom.D_CompId:=-1;
  l2:=fmUIDWH.CheckMultiContr(chlbSup,dm.SD_SupId_I);
  if l2 then dmCom.D_SupId:=-1;
  l3:=fmUIDWH.CheckMultiContr(chlbMat,dm.SD_MatId_I,true);
  if l3 then dmCom.D_MatId:='*';
  l4:=fmUIDWH.CheckMultiContr (chlbGood,dm.SD_GoodId_I,true);
  if l4 then dmCom.D_GoodId:='*';
  l5:=fmUIDWH.CheckMultiContr(chlbNote1,dm.SD_Note1_I);
  if l5 then dmCom.D_Note1:=-1;
  l6:=fmUIDWH.CheckMultiContr(chlbNote2,dm.SD_Note2_I);
  if l6 then dmCom.D_Note2:=-1;
  l7:=fmUIDWH.CheckMultiContr(chlbAtr1,dm.SD_Att1_I);
  if l7 then dmCom.D_Att1Id:=ATT1_DICT_ROOT;
  l8:=fmUIDWH.CheckMultiContr(chlbAtr2,dm.SD_Att2_I);
  if l8 then dmCom.D_Att2Id:=ATT2_DICT_ROOT;
  dm.lMultiSelect_I:=l1 or l2 or l3 or l4 or l5 or l6 or l7 or l8;
{  CheckCalcing;}
  rlbfilter.Visible:=false;
  ReOpenDataSets([dmServ.quInventory]);
//  ShowPeriod;
  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbSup,False);
  dm.SetVisEnabled(chlbComp,False);
  dm.SetVisEnabled(chlbnote1,False);
  dm.SetVisEnabled(chlbnote2,False);
  dm.SetVisEnabled(chlbAtr1,False);
  dm.SetVisEnabled(chlbAtr2,False);
  Application.Restore;
end;

procedure TfmInventory.chlbMatExit(Sender: TObject);
begin
 dm.SetVisEnabled(TWinControl(sender),False);
end;

procedure TfmInventory.acPrintInventoryBExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintBefore, FParentLogId);
  dmreport.PrintDocumentA(VarArrayOf([0]), inventory_b);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acPrintSubscriptionBExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintSubsB, FParentLogId);
  dmReport.PrintDocumentB(subscription_b);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acPrintSubscriptionEExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_inventoryPrintSubsE, FParentLogId);
  dmReport.PrintDocumentB(subscription_e);
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acClearExecute(Sender: TObject);
var LogOperationID:string;
begin
 if MessageDialog('������� ��� ������ �� ��������������?', mtInformation, [mbOK, mbCancel], 0)=mrOk then
 begin
  LogOperationID := dm3.insert_operation('�������� ��������������', FParentLogId);
  ExecSQL('execute procedure DELETE_INVENTORY(0, '+dmServ.quInventorySINVID.AsString+')', dmCom.quTmp);
  ReOpenDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
 end
end;

procedure TfmInventory.acClosedUpdate(Sender: TObject);
begin
 acClosed.Enabled:=dmServ.quListInventoryISCLOSED.AsInteger=0;
end;

procedure TfmInventory.acClosedExecute(Sender: TObject);
begin
 if CenterDep then
 begin
  ExecSQL('update Inventory_Head set isclosed=1 where hid='+IntToStr(dmServ.CurInventory ), dmcom.quTmp);
  if dmCom.GlobalInventoryEnabled=1 then dmCom.GlobalInventoryEnabled:=0
 end
 else
  ExecSQL('update sinv set isclosed=1 where sinvid='+IntToStr(dmServ.CurInventory ), dmcom.quTmp);
 dmserv.quListInventory.Refresh;
end;

procedure TfmInventory.acClearUpdate(Sender: TObject);
begin
 acClear.Enabled:= dmserv.quListInventoryISCLOSED.AsInteger=0;
end;

procedure TfmInventory.acTextExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='�����';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    AssignFile(ftext, Path);
    try
     Rewrite(ftext);

   dmserv.quTextInventory.ParamByName('SINVID').AsInteger:= dmserv.CurInventory;
   dmserv.quTextInventory.ParamByName('FPRICE').AsInteger:= 0;
   if dmcom.FilterDepID=0 then
   begin
    dmserv.quTextInventory.ParamByName('DEPID1').AsInteger:=-MaxInt;
    dmserv.quTextInventory.ParamByName('DEPID2').AsInteger:=MaxInt;
   end
   else
   begin
    dmserv.quTextInventory.ParamByName('DEPID1').AsInteger:=dmcom.FilterDepID;
    dmserv.quTextInventory.ParamByName('DEPID2').AsInteger:=dmcom.FilterDepID;
   end;
   OpenDataSet(dmServ.quTextInventory);
     with dmServ, quTextInventory do
      begin
       First;
       while not EOf do
       begin
       writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
         [quTextInventoryArt.AsString,'|', quTextInventoryArt2.AsString, '|', quTextInventoryPrice.AsFloat, '|',
          GetUNIT(quTextInventoryUNITID.AsInteger), '|', GetUNITQ(quTextInventoryUNITID.AsInteger,
          quTextInventoryQ.AsInteger,quTextInventoryW.AsFloat), '|']));
         next;
        end;
     end;
    finally
     CloseFile(fText);
     CloseDataSet(dmServ.quTextInventory);
    end;
   end;
  Screen.Cursor:=crDefault;
  MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
end;

procedure TfmInventory.acTextprExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='�����';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    AssignFile(ftext, Path);
    try
     Rewrite(ftext);

   dmserv.quTextInventory.ParamByName('SINVID').AsInteger:= dmserv.CurInventory;
   dmserv.quTextInventory.ParamByName('FPRICE').AsInteger:= 1;
   if dmcom.FilterDepID=0 then
   begin
    dmserv.quTextInventory.ParamByName('DEPID1').AsInteger:=-MaxInt;
    dmserv.quTextInventory.ParamByName('DEPID2').AsInteger:=MaxInt;
   end
   else
   begin
    dmserv.quTextInventory.ParamByName('DEPID1').AsInteger:=dmcom.FilterDepID;
    dmserv.quTextInventory.ParamByName('DEPID2').AsInteger:=dmcom.FilterDepID;
   end;
   OpenDataSet(dmServ.quTextInventory);
     with dmServ, quTextInventory do
      begin
       First;
       while not EOf do
       begin
       writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
         [quTextInventoryArt.AsString,'|', quTextInventoryArt2.AsString, '|', quTextInventoryPrice.AsFloat, '|',
          GetUNIT(quTextInventoryUNITID.AsInteger), '|', GetUNITQ(quTextInventoryUNITID.AsInteger,
          quTextInventoryQ.AsInteger,quTextInventoryW.AsFloat), '|']));
         next;
        end;
     end;
    finally
     CloseFile(fText);
     CloseDataSet(dmServ.quTextInventory);
    end;
   end;
  Screen.Cursor:=crDefault;
  MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
end;

procedure TfmInventory.acActLackUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep;
 TAction(Sender).Visible:=CenterDep;
end;

procedure TfmInventory.acActLackExecute(Sender: TObject);
begin
  if MessageDialog('������� ���� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then eXit;
  Self.Update;
 try
   ExecSQL('execute procedure CreateActAllow_FromInven ('+dmServ.quInventorySINVID.AsString+
           ', '+inttostr(dmCom.UserId)+')', dm.quTmp);
   MessageDialog('���� ��������� ������� �������', mtInformation, [mbOk], 0);
 except
   on E:Exception do
     MessageDialog('������ ��� �������� ����� ���������'#13+E.Message, mtError, [mbOk], 0);
 end
end;

procedure TfmInventory.acSurPlusExecute(Sender: TObject);
var serr:string;
begin
  if MessageDialog('������������ �������� �� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  try
    ExecSQL('execute procedure CREATE_SURPLUS('+dmServ.quListInventorySINVID.AsString+
            ', '+inttostr(dmCom.UserId)+')', dm.quTmp);
    MessageDialog('�������� �� ������� ������� �������', mtInformation, [mbOk], 0);
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select id1, id2 from tmpdata where datatype=6 and userid='+dmServ.quListInventorySINVID.AsString;
    dm.quTmp.ExecQuery;
    serr:='';
    while not dm.quTmp.Eof do
    begin
      if dm.quTmp.Fields[1].AsInteger=1 then
        serr:=serr+#13#10+dm.quTmp.Fields[0].AsString+'- �� �������'
      else
        serr:=serr+#13#10+dm.quTmp.Fields[0].AsString+'- �� ������';
       dm.quTmp.Next;
    end;
    dm.quTmp.Close;
    dm.quTmp.Transaction.CommitRetaining;
    if serr<>'' then MessageDialog('�������:'+serr, mtInformation, [mbOk], 0);
  except
    on E:Exception do
      MessageDialog('������ ��� �������� �������� �� �������'#13+E.Message, mtError, [mbOk], 0);
  end;
end;

procedure TfmInventory.acSurPlusUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep;
end;

procedure TfmInventory.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100410)
end;

procedure TfmInventory.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmInventory.edFindUidKeyPress(Sender: TObject; var Key: Char);
begin
  case key of
    '0'..'9', #8: ;
    else sysutils.Abort;
  end;
end;

procedure TfmInventory.edFindUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN:begin
   if not dmserv.quInventory.Locate('UID',edFindUid.Text,[]) then
    MessageDialog('������� � ������� '+edFindUid.Text+' �� �������!', mtWarning, [mbOk], 0)
   else if chFindSelect.Checked then dg1.SelectedRows.CurrentRowSelected:=true; 
   edFindUid.Text:='';
  end
 end;  
end;

procedure TfmInventory.acFindAllExecute(Sender: TObject);
begin
  if MessageDialog('��� ��������� ������� ����� �������� ��� ���������', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  with dmserv do
  begin
    quInventory.First;
    while not quInventory.Eof do
    begin
      if (not dg1.SelectedRows.CurrentRowSelected) and (quInventoryISIN.AsInteger=0) then
      begin
        quInventory.Edit;
        quInventoryISIN.AsInteger:=1;
        quInventory.Post;
      end;
      quInventory.Next;
    end
  end;
end;

procedure TfmInventory.acFindAllUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= CenterDep and (dmserv.quListInventoryISCLOSED.AsInteger=0)
end;

procedure TfmInventory.acChangeStateTo1Execute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryMarkFind, FParentLogId);
  PostDataSet(dmServ.quInventory);
  dmServ.quInventory.Edit;
  dmServ.quInventoryISIN.AsInteger := 1;
  dmServ.quInventory.Post;
  dmServ.quInventory.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acChangeStateTo1Update(Sender: TObject);
begin
  acChangeStateTo1.Visible := CenterDep;
  if acChangeStateTo1.Visible then
      acChangeStateTo1.Enabled := dmServ.quInventory.Active and (not dmServ.quInventory.IsEmpty) and
                                  dmServ.quListInventory.Active and (dmServ.quListInventoryISCLOSED.AsInteger = 0);
end;

procedure TfmInventory.acChangeStateTo2Execute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryMarkExcess, FParentLogId);
  PostDataSet(dmServ.quInventory);
  dmServ.quInventory.Edit;
  dmServ.quInventoryISIN.AsInteger := 2;
  dmServ.quInventory.Post;
  dmServ.quInventory.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acChangeStateTo2Update(Sender: TObject);
begin
  acChangeStateTo2.Visible := CenterDep;
  if acChangeStateTo2.Visible then
      acChangeStateTo2.Enabled := dmServ.quInventory.Active and (not dmServ.quInventory.IsEmpty) and
                                  dmServ.quListInventory.Active and (dmServ.quListInventoryISCLOSED.AsInteger = 0);
end;

procedure TfmInventory.acChangeStateTo0Execute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryMarkFind, FParentLogId);
  PostDataSet(dmServ.quInventory);
  dmServ.quInventory.Edit;
  dmServ.quInventoryISIN.AsInteger := 0;
  dmServ.quInventory.Post;
  dmServ.quInventory.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.acChangeStateTo0Update(Sender: TObject);
begin
  acChangeStateTo0.Visible := CenterDep;
  if acChangeStateTo0.Visible then
    acChangeStateTo0.Enabled := dmServ.quInventory.Active and (not dmServ.quInventory.IsEmpty) and
                                dmServ.quListInventory.Active and (dmServ.quListInventoryISCLOSED.AsInteger = 0);
end;

procedure TfmInventory.acDelExcessUIDUpdate(Sender: TObject);
begin
  acDelExcessUID.Visible := CenterDep;
  if acDelExcessUID.Visible then
    acDelExcessUID.Enabled := dmServ.quListInventory.Active and (not dmServ.quListInventory.IsEmpty) and
                        (dmServ.quListInventoryISCLOSED.AsInteger = 0) and
                        dmServ.quInventory.Active and (not dmServ.quInventory.IsEmpty) and
                        (dmServ.quInventoryISIN.AsInteger = 2);
end;

procedure TfmInventory.acDelExcessUIDExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  //MessageDialog('������� ����� ������ ������ �������', mtInformation, [mbOk], 0);
  LogOperationID := dm3.insert_operation(sLog_InventoryDelUID, FParentLogId);
  dmServ.quInventory.Edit;
  dmServ.quInventoryFL.AsInteger:=0;
  dmServ.quInventory.Post;
  dmServ.quInventory.Refresh;
  dmServ.quInventory.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmInventory.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

end.





