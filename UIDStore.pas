unit UIDStore;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, StdCtrls, Mask, DBCtrlsEh,
  DBLookupEh, ComCtrls, ActnList, Db, jpeg, DBGridEhGrouping, rxSpeedbar,
  GridsEh;

type
  TfmUIDStore = class(TForm)
    gridUIDStore: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siEdit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    Label1: TLabel;
    StatusBar1: TStatusBar;
    cmbxDep: TDBComboBoxEh;
    acEvent: TActionList;
    Label2: TLabel;
    cmbxGrMat: TDBComboBoxEh;
    acExit: TAction;
    acView: TAction;
    acShowId: TAction;
    chbxBadRecord: TDBCheckBoxEh;
    acShowBadArt: TAction;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cmbxDepChange(Sender: TObject);
    procedure cmbxGrMatChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure gridUIDStoreGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridUIDStoreColumns15EditButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure acShowBadArtExecute(Sender: TObject);
  private
    LogOperIdForm : string;
    procedure FilterBadArt(DataSet: TDataSet; var Accept: boolean);
  end;

var
  fmUIDStore: TfmUIDStore;

implementation

uses comdata, Data2, Data3, dbTree, m207Proc, Data, UIDStore_Detail,
  JewConst;

{$R *.dfm}

procedure TfmUIDStore.FormCreate(Sender: TObject);
var
  nd : TNodeData;
begin
  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;

  dm3.FilterUIDStoreDepId := -1;
  dm3.FilterUIDStoreGrMat := '*���';
  // ������������� ��������
  cmbxDep.OnChange := nil;
  cmbxDep.Clear;
  nd := TNodeData.Create;
  nd.Code := -1;
  nd.Name := '*���';
  cmbxDep.Items.Assign(dm2.slDepSName);
  cmbxDep.Items.InsertObject(0,'*���',nd);
  cmbxDep.ItemIndex := 0;
  cmbxDep.OnChange := cmbxDepChange;

  cmbxGrMat.OnChange := nil;
  cmbxGrMat.Clear;
  nd := TNodeData.Create;
  nd.Code := '*���';
  nd.Name := '*���';
  cmbxGrMat.Items.Assign(dm2.slGrName);
  cmbxGrMat.Items.InsertObject(0,'*���',nd);
  cmbxGrMat.ItemIndex := 0;
  cmbxGrMat.OnChange := cmbxGrMatChange;
  OpenDataSets([dm3.taUIDStore]);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStore.cmbxDepChange(Sender: TObject);
begin
  dm3.FilterUIDStoreDepId := TNodeData(cmbxDep.Items.Objects[cmbxDep.ItemIndex]).Code;
  ReOpenDataSets([dm3.taUIDStore]);
end;

procedure TfmUIDStore.cmbxGrMatChange(Sender: TObject);
begin
  dm3.FilterUIDStoreGrMat := cmbxGrMat.Items[cmbxGrMat.ItemIndex];
  ReOpenDataSets([dm3.taUIDStore]);
end;

procedure TfmUIDStore.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  gridUIDStore.FieldColumns['ARTID'].Visible := False;
  gridUIDStore.FieldColumns['DEPID'].Visible := False;
  CloseDataSets([dm3.taUIDStore]);
  dm3.taUIDStore.Filtered := False;
  dm3.taUIDStore.OnFilterRecord := nil;
  Action := caFree;
end;

procedure TfmUIDStore.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUIDStore.acExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfmUIDStore.gridUIDStoreGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  // ���������� ���-��
  if(Column.FieldName = 'ENTRYNUM') then
  begin
    if(Column.Field.AsInteger <> 0)then Background := dm.CU_SBD1;
  end
  else if (Column.FieldName = 'SINVNUM')then
  begin
    if(Column.Field.AsInteger <> 0) then Background := dm.CU_SBD1;//CU_S1
  end
  else if (Column.FieldName = 'DINVNUM')or(Column.FieldName='DINVNUMFROM') then
  begin
    if (Column.Field.AsInteger <> 0) then Background := dm.CU_IM1
  end
  else if (Column.FieldName = 'RETNUM')then
  begin
    if (Column.Field.AsInteger <> 0)then Background := dm.CU_RT1;
  end
  else if (Column.FieldName = 'OPTRETNUM') then
  begin
    if (Column.Field.AsInteger <> 0)then Background := dm.CU_RO1;
  end
  else if (Column.FieldName = 'SELLNUM') then
  begin
    if (Column.Field.AsInteger <> 0)then Background := dm.CU_SL;
  end
  else if (Column.FieldName = 'OPTSELLNUM') then
  begin
    if(Column.Field.AsInteger <> 0)then Background := dm.CU_SO1;
  end
  else if(Column.FieldName = 'SINVRETNUM') then
  begin
    if (Column.Field.AsInteger <> 0) then Background := dm.CU_SR1;
  end;
  if(Column.FieldName = 'DEPNAME')then
    Background := dmCom.Dep[dm3.taUIDStoreDEPID.AsInteger].Color;
  if(Column.FieldName = 'RESNUM')and(Column.Field.AsInteger<0) then Background := clRed;
  if(Column.FieldName = 'F')and(dm3.taUIDStoreRESNUM.AsInteger <> dm3.taUIDStoreF.AsInteger) then Background := clRed;
end;

procedure TfmUIDStore.gridUIDStoreColumns15EditButtonClick(Sender: TObject;
  var Handled: Boolean);
begin
  acView.Execute;
end;

procedure TfmUIDStore.acViewExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreArtDetail, LogOperIdForm);
  ShowAndFreeForm(TfmUIDStore_Detail, Self, TForm(fmUIDStore_Detail), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStore.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dm3.taUIDStore.Active and (not dm3.taUIDStore.IsEmpty); 
end;

procedure TfmUIDStore.acShowIdExecute(Sender: TObject);
begin
  gridUIDStore.FieldColumns['ARTID'].Visible := not gridUIDStore.FieldColumns['ARTID'].Visible;
  gridUIDStore.FieldColumns['DEPID'].Visible := not gridUIDStore.FieldColumns['DEPID'].Visible;
end;

procedure TfmUIDStore.acShowBadArtExecute(Sender: TObject);
begin
  if chbxBadRecord.Checked then
  begin
    dm3.taUIDStore.Filtered := False;
    dm3.taUIDStore.OnFilterRecord := FilterBadArt;
    dm3.taUIDStore.Filtered := True;
  end
  else begin
    dm3.taUIDStore.Filtered := False;
    dm3.taUIDStore.OnFilterRecord := nil;  
  end;
end;

procedure TfmUIDStore.FilterBadArt(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (DataSet.FieldByName('RESNUM').AsInteger < 0) or
            (DataSet.FieldByName('RESNUM').AsInteger <> DataSet.FieldByName('F').AsInteger);
end;

end.
