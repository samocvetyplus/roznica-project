unit Mol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls, StdCtrls, Mask, DBCtrlsEh,
  DBLookupEh, DB, DBGridEhGrouping, GridsEh, rxSpeedbar, ActnList;

type
  TfmMol = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siadd: TSpeedItem;
    siDel: TSpeedItem;
    dg1: TDBGridEh;
    pdescription: TPanel;
    Splitter1: TSplitter;
    LCBDepid: TDBLookupComboboxEh;
    Label1: TLabel;
    DBEditEh1: TDBEditEh;
    Label2: TLabel;
    gr: TGroupBox;
    cbChairMan: TDBCheckBoxEh;
    cbMember: TDBCheckBoxEh;
    SpeedBar1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    Label3: TLabel;
    cmbxFilterDep: TDBComboBoxEh;
    cbMol: TDBCheckBoxEh;
    ActionList1: TActionList;
    ActionAdd: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure LCBDepidKeyValueChanged(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure cbmemberMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbpresedentMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure siaddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure LCBDepidDropDownBoxGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure cmbxFilterDepChange(Sender: TObject);
    procedure LCBDepidExit(Sender: TObject);
    procedure ActionAddUpdate(Sender: TObject);
  private
    procedure FilterMol(DataSet: TDataSet; var Accept: boolean);
  end;

var
  fmMol: TfmMol;

implementation

uses servdata, comdata, M207Proc, data, FIBQuery, pFIBQuery, dbTree, dbUtil;

{$R *.dfm}

procedure TfmMol.FormCreate(Sender: TObject);
begin
  // ����������
  cmbxFilterDep.OnChange := nil;
  cmbxFilterDep.Items.Assign(dm.slDep);
  cmbxFilterDep.ItemIndex := 0;
  cmbxFilterDep.OnChange := cmbxFilterDepChange;
  dmServ.taMol.Filtered := True;
  dmServ.taMol.OnFilterRecord := FilterMol;
  OpenDataSets([dmcom.quDep, dmserv.taMol]);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  tb1.Wallpaper:=wp;
  SpeedBar1.Wallpaper:=wp;
  if not CenterDep then
  begin
   siadd.Enabled:=false;
   siDel.Enabled:=false;
   dg1.ReadOnly:=true;
   pdescription.Enabled:=false;
  end;

end;

procedure TfmMol.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSet(dmServ.taMol);
  CloseDataSets([dmcom.quDep, dmserv.taMol]);
  dmServ.taMol.Filtered := False;
  dmServ.taMol.OnFilterRecord := nil;
end;

procedure TfmMol.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if not dmserv.taMolCOLORSHOP.IsNull then Background:=dmserv.taMolCOLORSHOP.AsInteger;
end;

procedure TfmMol.LCBDepidKeyValueChanged(Sender: TObject);
begin
//  LCBDepid.Color:=dmserv.taMolCOLORSHOP.AsInteger;
end;

procedure TfmMol.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmMol.FormResize(Sender: TObject);
begin
   siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmMol.ActionAddUpdate(Sender: TObject);
begin
  ActionAdd.Enabled := True;

  cbMol.Enabled := True;

  cbChairMan.Enabled := not cbMember.Checked;

  cbMember.Enabled := not cbChairMan.Checked;
end;

procedure TfmMol.cbmemberMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if (dmserv.taMolMEMBER.AsInteger=0)and(dmserv.taMolPRESEDENT.AsInteger=1) then
  raise Exception.Create('��� �� ����� ���� ������������� � ������ ������������������ ��������!');
end;

procedure TfmMol.cbpresedentMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var s:string;
begin
 if (dmserv.taMolMEMBER.AsInteger=1)and(dmserv.taMolPRESEDENT.AsInteger=0) then
  raise Exception.Create('��� �� ����� ���� ������������� � ������ ������������������ ��������!');
 if dmserv.taMolPRESEDENT.AsInteger=0 then
 with dm, qutmp do
 begin
  close;
  sql.Text:='select d_molid, FIO from d_mol where presedent=1';
  ExecQuery;
  if (not Fields[0].IsNull) then
  begin
   s:=trim(Fields[1].AsString);
   Transaction.CommitRetaining;
   close;
   raise Exception.Create(trim(dmserv.taMolFIO.AsString)+' �� ����� ���� ������������� �����. ��������, �.�. '+
                          s+' - ������������' );
  end;
  Transaction.CommitRetaining;
  close;
 end;
end;

procedure TfmMol.siaddClick(Sender: TObject);
begin
  dmServ.taMol.Append;
  with cmbxFilterDep do
    if (ItemIndex>0)and(TNodeData(Items.Objects[ItemIndex]).Code <> DEP_DICT_ROOT) then
    begin
      dmServ.taMolD_DEPID.AsInteger := TNodeData(Items.Objects[ItemIndex]).Code;
      dmServ.taMolCOLORSHOP.AsInteger := dmCom.Dep[TNodeData(Items.Objects[ItemIndex]).Code].Color;
    end;
  ActiveControl := dg1;
end;

procedure TfmMol.siDelClick(Sender: TObject);
begin
  dmServ.taMol.Delete;
end;

procedure TfmMol.LCBDepidDropDownBoxGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  Background := dmCom.quDepCOLOR.AsInteger;
end;

procedure TfmMol.FilterMol(DataSet: TDataSet; var Accept: boolean);
begin
  if (cmbxFilterDep.ItemIndex=-1)then
  begin
    Accept := True;
    eXit;
  end;
  with cmbxFilterDep do
    if TNodeData(Items.Objects[ItemIndex]).Code=DEP_DICT_ROOT then Accept:=True
    else Accept:= dmServ.taMolD_DEPID.AsInteger=TNodeData(Items.Objects[ItemIndex]).Code;
end;

procedure TfmMol.cmbxFilterDepChange(Sender: TObject);
begin
  dmServ.taMol.Filtered := False;
  dmServ.taMol.Filtered := True;
  {with cmbxFilterDep do
    if TNodeData(Items.Objects[ItemIndex]).Code = DEP_DICT_ROOT then
    begin
      LCBDepid.KeyValue := Null;
      LCBDepid.Enabled := False;
    end
    else begin
      LCBDepid.KeyValue := TNodeData(Items.Objects[ItemIndex]).Code;
      LCBDepid.Enabled := True;
    end;}
end;

procedure TfmMol.LCBDepidExit(Sender: TObject);
begin
  PostDataSet(dmServ.taMol);
end;

end.
