unit Country;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, jpeg, rxSpeedbar;

type
  TfmCountry = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    spitPrint: TSpeedItem;
    siSort: TSpeedItem;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure siSortClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmCountry: TfmCountry;

implementation

uses comdata, DBTree, Sort, Data, M207Proc;

{$R *.DFM}

procedure TfmCountry.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmCountry.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom do
    begin
//      tr.Active:=True;
      dg1.ReadOnly:=not Centerdep;
      SpeedItem2.Enabled:=Centerdep;
      SpeedItem1.Enabled:=Centerdep;
      taCountry.Active:=True;
    end;
end;

procedure TfmCountry.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSet(taCountry);
      taCountry.Active:=False;
      dm.LoadArtSL(Country_DICT);
      tr.CommitRetaining;
    end;
end;

procedure TfmCountry.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmCountry.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmCountry.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taCountry.Append;
end;

procedure TfmCountry.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taCountry.Delete;
end;

procedure TfmCountry.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsCountry, 64); //'gd'
end;

procedure TfmCountry.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_CountryID, NAME '+
                       'FROM D_Country '+
                       ' where d_countryid<>'#39+'-1000'+#39' '+
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_Country set SortInd=?SortInd where D_CountryId=?D_CountryId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taCountry]);
            end
        end;
    finally
      fmSort.Free;
    end;


end;

procedure TfmCountry.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100106)
end;

procedure TfmCountry.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
