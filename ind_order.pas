unit ind_order;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridDBTableView, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxClasses, cxGridLevel,
  cxGrid, cxPC, dxSkinsdxBarPainter, ImgList, dxBar, ActnList,
  cxDBLookupComboBox, cxButtonEdit, FIBDataSet, pFIBDataSet, dxRibbon, Menus,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, cxMemo,
  cxContainer, cxTextEdit, cxBarEditItem,  StdCtrls, MyAccess, cxDBEdit, uGridPrinter,StrUtils,
  dxPScxCommon, dxPScxGrid6Lnk, FR_Class, FR_DSet, FR_DBSet, cxPropertiesStore, frmPopup,
  PrnDbgeh, FR_Desgn, uUtils, ShlObj, cxLabel, dmBuy;

type
  TDialogOrder = class(TForm)
    PageControl: TcxPageControl;
    Page_out: TcxTabSheet;
    Actions: TActionList;
    ActionIncludeFactory: TAction;
    ActionExcludeFactory: TAction;
    ActionRefreshFactory: TAction;
    ActionExit: TAction;
    ActionIncludeOUT: TAction;
    ActionExcludOUT: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    Images32: TcxImageList;
    add: TdxBarLargeButton;
    del: TdxBarLargeButton;
    refresh: TdxBarLargeButton;
    ActionRefreshOUT: TAction;
    ActionEditStateOUT: TAction;
    editState: TdxBarLargeButton;
    ActionEditOffOUT: TAction;
    EditOff: TdxBarLargeButton;
    ExitForm: TdxBarLargeButton;
    Page_in: TcxTabSheet;
    Grid: TcxGrid;
    LevelOrders: TcxGridLevel;
    LevelHistory: TcxGridLevel;
    Sourse_ART: TDataSource;
    ActionEditStateIN: TAction;
    ActionExcludIn: TAction;
    ActionRefreshIN: TAction;
    ActionIncludeIN: TAction;
    ViewOrders: TcxGridDBBandedTableView;
    ViewHistory: TcxGridDBTableView;
    ViewOrdersID_ORDER: TcxGridDBBandedColumn;
    ViewOrdersFIO: TcxGridDBBandedColumn;
    ViewOrdersPHONE: TcxGridDBBandedColumn;
    ViewOrdersDEP_SEND: TcxGridDBBandedColumn;
    ViewOrdersART: TcxGridDBBandedColumn;
    ViewOrdersCOMP: TcxGridDBBandedColumn;
    ViewOrdersGOOD: TcxGridDBBandedColumn;
    ViewOrdersMAT: TcxGridDBBandedColumn;
    ViewOrdersSZ: TcxGridDBBandedColumn;
    ViewOrdersDATE_CREATE: TcxGridDBBandedColumn;
    ViewOrdersDEP_CUSTOM: TcxGridDBBandedColumn;
    ViewOrdersNAME: TcxGridDBBandedColumn;
    ViewOrdersID_STATE: TcxGridDBBandedColumn;
    ViewOrdersTYPE_ORDER: TcxGridDBBandedColumn;
    ViewOrdersEMP_STATE: TcxGridDBBandedColumn;
    ViewOrdersDATE_STATE: TcxGridDBBandedColumn;
    ViewOrdersNUMBER_DOC: TcxGridDBBandedColumn;
    ViewOrdersDATE_DOC: TcxGridDBBandedColumn;
    ViewOrdersCOMMENT: TcxGridDBBandedColumn;
    ViewOrdersPRICE: TcxGridDBBandedColumn;
    ViewOrdersDocumentName: TcxGridDBBandedColumn;
    ViewHistoryID_ORDER: TcxGridDBColumn;
    ViewHistoryNAME: TcxGridDBColumn;
    ViewHistoryID_STATE: TcxGridDBColumn;
    ViewHistoryEMP_STATE: TcxGridDBColumn;
    ViewHistoryDATE_STATE: TcxGridDBColumn;
    ViewHistoryNUMBER_DOC: TcxGridDBColumn;
    ViewHistoryDATE_DOC: TcxGridDBColumn;
    ViewHistoryCOMMENT: TcxGridDBColumn;
    ViewHistoryPRICE: TcxGridDBColumn;
    ViewHistoryDocumentName: TcxGridDBColumn;
    Sourse_DEP: TDataSource;
    Page_Factory: TcxTabSheet;
    Page_off: TcxTabSheet;
    BarFilter: TdxBar;
    BtnOrderOut: TdxBarLargeButton;
    TypeOrder: TAction;
    TypeOrderPopupMenu: TdxBarPopupMenu;
    MenuGrid: TPopupMenu;
    N2: TMenuItem;
    Type_Out: TdxBarLargeButton;
    Type_In: TdxBarLargeButton;
    ActionRefreshOFF: TAction;
    Type_Factory: TdxBarLargeButton;
    BtnOffice: TdxBarLargeButton;
    Office: TAction;
    OfficePopupMenu: TdxBarPopupMenu;
    Center: TdxBarLargeButton;
    Kursk: TdxBarLargeButton;
    Olimp: TdxBarLargeButton;
    Belgorod: TdxBarLargeButton;
    Vester: TdxBarLargeButton;
    Oskol: TdxBarLargeButton;
    Moll: TdxBarLargeButton;
    Rio: TdxBarLargeButton;
    Orel: TdxBarLargeButton;
    Europe: TdxBarLargeButton;
    ViewOrdersCOST: TcxGridDBBandedColumn;
    ViewOrdersINFO_ORDER: TcxGridDBBandedColumn;
    ViewOrdersTIME_ORDER: TcxGridDBBandedColumn;
    ViewOrdersPREPAY: TcxGridDBBandedColumn;
    ActionEditStateFactory: TAction;
    Page_Center: TcxTabSheet;
    ActionRefreshCenter: TAction;
    Printer: TdxComponentPrinter;
    Preview: TdxBarLargeButton;
    ActionPreview: TAction;
    ActionCOMMENT: TAction;
    dxBarLargeButton3: TdxBarLargeButton;
    N3: TMenuItem;
    EditorNote: TcxDBMemo;
    Print: TdxBarLargeButton;
    PrinterLink1: TdxGridReportLink;
    ActionPrint: TAction;
    ART_NAME_S: TpFIBDataSet;
    ART_NAME_SART: TFIBStringField;
    ViewOrdersART_FACTORY: TcxGridDBBandedColumn;
    N4: TMenuItem;
    ViewOrdersINS_FACTORY: TcxGridDBBandedColumn;
    EMP_DEP_S: TpFIBDataSet;
    Source_EMP: TDataSource;
    EMP_DEP_SFIO: TFIBStringField;
    EMP_DEP_SNAME: TFIBStringField;
    cxPropertiesStore_Grid: TcxPropertiesStore;
    N6: TMenuItem;
    DEP_NAME_S: TpFIBDataSet;
    DEP_NAME_SNAME: TFIBStringField;
    ReportDataSet: TfrDBDataSet;
    Report: TfrReport;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    Cancel: TdxBarLargeButton;
    ActionCancel: TAction;
    editotkaz: TdxBarLargeButton;
    ViewOrdersEMP_CREATE: TcxGridDBBandedColumn;
    ActionOtkaz: TAction;
    ViewOrdersID_PREVIEW: TcxGridDBBandedColumn;
    ViewOrdersOLD_STATE: TcxGridDBBandedColumn;
    ViewHistoryUID: TcxGridDBColumn;
    ViewOrdersColumn1: TcxGridDBBandedColumn;
    laPeriod: TdxBarLargeButton;
    ActionlaPeriod: TAction;
    procedure ActionExitExecute(Sender: TObject);
    procedure ColumnEditingClose;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ActionEditOffOUTExecute(Sender: TObject);
    procedure ActionRefreshOUTExecute(Sender: TObject);
    procedure ActionIncludeOUTExecute(Sender: TObject);
    procedure ActionExcludOUTExecute(Sender: TObject);
    procedure PageControlClick(Sender: TObject);
    procedure ActionEditStateOUTExecute(Sender: TObject);
    procedure ActionEditStateINExecute(Sender: TObject);
    procedure ActionRefreshINExecute(Sender: TObject);
    procedure ViewOrdersARTPropertiesCloseUp(Sender: TObject);
    procedure BtnOrderOutClick(Sender: TObject);
    procedure ViewOrdersFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure TypeOrderPopupMenuPopup(Sender: TObject);
    procedure ActionRefreshOFFExecute(Sender: TObject);
    procedure Type_OutClick(Sender: TObject);
    procedure Type_InClick(Sender: TObject);
    procedure Type_FactoryClick(Sender: TObject);
    procedure OfficeExecute(Sender: TObject);
    procedure OfficePopupMenuPopup(Sender: TObject);
    procedure Office_select (Button: TdxBarLargeButton);
    procedure OfficeClick(Sender: TObject);
    procedure ActionRefreshFactoryExecute(Sender: TObject);
    procedure ActionIncludeFactoryExecute(Sender: TObject);
    procedure ActionExcludeFactoryExecute(Sender: TObject);
    procedure ActionEditStateFactoryExecute(Sender: TObject);
    procedure OtkazFormOpen;
    procedure ViewOrdersCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure StateNext(DataSet: TDataSet; State_n: integer);
    procedure FormShow_VP(Form: TForm);
    procedure OffOrder;
    procedure ActionRefreshCenterExecute(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure ActionPreviewExecute(Sender: TObject);
    procedure ActionCOMMENTExecute(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure BufferToClipboard(Buffer: WideString);
    procedure ViewOrdersCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ActionPrintExecute(Sender: TObject);
    procedure OnCustomDrawBandCell(Sender: TdxGridReportLink; ACanvas: TCanvas; AView: TcxGridBandedTableView; ABand: TcxGridBand;  AnItem: TdxReportCellString; var ADone: Boolean);
    procedure ViewHistoryCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ViewHistoryFocusedRecordChanged(Sender: TcxCustomGridTableView;
      APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure N4Click(Sender: TObject);
    procedure EMP_DEP_SBeforeOpen(DataSet: TDataSet);
    procedure N6Click(Sender: TObject);
    procedure ActionCancelExecute(Sender: TObject);
    procedure ActionOtkazExecute(Sender: TObject);
    procedure ActionlaPeriodExecute(Sender: TObject);
    procedure OnDateOk(Popup: TPopup);

  private
    Department: String;
    state:Integer;
   
  public
   TypeOrd: integer;
   
  end;

var
  DialogOrder: TDialogOrder;
  x:integer;
  y:integer;
  i:integer;

 function IniFileName: string;


implementation
 uses  IndividualOrderData, comdata, otkaz, sale_jew, vp, uDialog, ClipBrd,  frmBuyRange,
  frmBuyRangePopup;
{$R *.dfm}


function IniFileName: string;
var
  i: Integer;
  FilePath: string;
  ApplicationName: string;
begin
  ApplicationName := ExtractFileName(Application.ExeName);
  SetLength(ApplicationName, Length(ApplicationName) - 4);
  FilePath := GetSpecialFolderLocation(CSIDL_APPDATA);
  FilePath := FilePath + '\' + ApplicationName;
  if not DirectoryExists(FilePath) then CreateDir(FilePath);
  Result := FilePath + '\' + 'ind_order' + '.ini';
end;
 procedure TDialogOrder.BufferToClipboard(Buffer: WideString);
var WideBuffer: WideString;
    BuffSize: Cardinal;
    Data: THandle;
    DataPtr: Pointer;
begin
  if Buffer <> '' then begin
    WideBuffer := Buffer;
    BuffSize := length(Buffer) * SizeOf(WideChar);
    Data := GlobalAlloc(GMEM_MOVEABLE+GMEM_DDESHARE+GMEM_ZEROINIT, BuffSize + 2);
    try
      DataPtr := GlobalLock(Data);
      try
        Move(PWideChar(WideBuffer)^, Pointer(Cardinal(DataPtr))^, BuffSize);
      finally
        GlobalUnlock(Data);
      end;
      Clipboard.SetAsHandle(CF_UNICODETEXT, Data);
    except
      GlobalFree(Data);
      raise;
    end;
  end;
end;
procedure TDialogOrder.ColumnEditingClose;
begin
     
     ViewOrdersFIO.Options.Editing := False;
     ViewOrdersPHONE.Options.Editing := False;
     ViewOrdersART.Options.Editing := False;
     ViewOrdersCOMP.Options.Editing := False;
     ViewOrdersGOOD.Options.Editing := False;
     ViewOrdersMAT.Options.Editing :=  False;
     ViewOrdersSZ.Options.Editing :=  False;
     ViewOrdersDEP_CUSTOM.Options.Editing :=  False;
     ViewOrdersDEP_SEND.Options.Editing :=  False;
     ViewOrdersCOMMENT.Options.Editing :=  False;

     ViewOrdersCOST.Options.Editing :=  False;
     ViewOrdersINFO_ORDER.Options.Editing :=  False;
     ViewOrdersTIME_ORDER.Options.Editing :=  False;
     ViewOrdersPREPAY.Options.Editing :=  False;
     ViewOrdersART_FACTORY.Options.Editing:=false;
     ViewOrdersINS_FACTORY.Options.Editing:=false;
     ViewOrdersEMP_CREATE.Options.Editing:=false;
end;



procedure TDialogOrder.EMP_DEP_SBeforeOpen(DataSet: TDataSet);
begin
EMP_DEP_S.ParamByName('DEP').AsString := dmCom.HereName;
end;

procedure TDialogOrder.BtnOrderOutClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 40;

  ButtonRect := BtnOrderOut.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if TypeOrderPopupMenu.ItemLinks.VisibleItemCount <> 0 then
  begin
    TypeOrderPopupMenu.Popup(X, Y);
  end else

  begin
    TDialog.Information('��� ���������.');
  end;

end;

 procedure TDialogOrder.Office_select (Button: TdxBarLargeButton);
begin



    if BtnOrderOut.Tag=0 then
      Begin
        ViewOrders.DataController.Filter.Root.Clear;

        if dmCom.HereName <> '�����' then
        ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, dmCom.HereName, dmCom.HereName );
        ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '1', '1' );

        BtnOffice.Caption:= Button.Caption;
        ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, Button.Caption, Button.Caption );
        ViewOrders.DataController.Filter.Active:=True;
      End;

    if BtnOrderOut.Tag = 1 then
      Begin
          ViewOrders.DataController.Filter.Root.Clear;

          if dmCom.HereName <> '�����' then
          ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, dmCom.HereName, dmCom.HereName );
          ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '1', '1' );

        BtnOffice.Caption:= Button.Caption;
        ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, Button.Caption, Button.Caption);
        ViewOrders.DataController.Filter.Active:=True;
      End;

  if  BtnOrderOut.Tag =2 then
    Begin
    if dmCom.HereName <> '�����' then
      begin
     BtnOrderOut.Caption := '�� �����';
     ViewOrders.DataController.Filter.Root.Clear;
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, dmCom.HereName, dmCom.HereName );
     BtnOffice.Caption:= '�����';
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, '�����', '�����' );
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '3', '3' );
     ViewOrders.DataController.Filter.Active:=True;
      end
  else
      begin
       BtnOrderOut.Caption := '�� �����';
       ViewOrders.DataController.Filter.Root.Clear;
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, dmCom.HereName, dmCom.HereName );
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '3', '3' );
       BtnOffice.Caption:= Button.Caption;
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, Button.Caption, Button.Caption);
       ViewOrders.DataController.Filter.Active:=True;
      end;
    End;
end;


procedure TDialogOrder.OfficeClick(Sender: TObject);
begin
 Office_select(TdxBarLargeButton(Sender));
end;



procedure TDialogOrder.TypeOrderPopupMenuPopup(Sender: TObject);
begin
TypeOrderPopupMenu.PopupFromCursorPos;
end;

procedure TDialogOrder.Type_FactoryClick(Sender: TObject);
begin
  ViewOrdersINS_FACTORY.Visible :=true;
  ViewOrdersART_FACTORY.Visible :=true;
  ViewOrders.Bands[1].Visible:= True;
  //BtnOffice.Enabled:=False;
  BtnOrderOut.Tag:=2;
  TypeOrd:=2;

  dmIndividualOrder.dtOrder_off.Close;

  dmIndividualOrder.dtOrder_off.Open;
  dmIndividualOrder.dtHistory.Close;
  dmIndividualOrder.dtHistory.Open;

  BtnOrderOut.Caption:= '�� �����';

  if dmCom.HereName <> '�����' then
      begin
     BtnOffice.Caption:='������';
     BtnOffice.enabled:=false;
     ViewOrders.DataController.Filter.Root.Clear;
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, dmCom.HereName, dmCom.HereName );
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, '�����', '�����' );
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '3', '3' );
     ViewOrders.DataController.Filter.Active:=True;
     ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := False;
      end
  else
      begin
       BtnOffice.Caption:='������';
       BtnOffice.enabled:=true;
       ViewOrders.DataController.Filter.Root.Clear;
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, dmCom.HereName, dmCom.HereName );
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '3', '3' );
       ViewOrders.DataController.Filter.Active:=True;
      end;
end;

procedure TDialogOrder.Type_InClick(Sender: TObject);
begin
  ViewOrdersART_FACTORY.Visible :=false;
  ViewOrdersINS_FACTORY.Visible :=false;
  ViewOrders.Bands[1].Visible:= False;
  BtnOrderOut.Tag:=1;
  TypeOrd:=1;

  dmIndividualOrder.dtOrder_off.Close;

  dmIndividualOrder.dtOrder_off.Open;
  dmIndividualOrder.dtHistory.Close;
  dmIndividualOrder.dtHistory.Open;

  BtnOffice.Enabled:=True;
  BtnOrderOut.Caption:= '��������';
  BtnOffice.enabled:=true;
  BtnOffice.Caption:='������';
  ViewOrders.DataController.Filter.Root.Clear;

  if dmCom.HereName <> '�����' then
  ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_SEND, foEqual, dmCom.HereName, dmCom.HereName );
  ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '1', '1' );
  ViewOrders.DataController.Filter.Active:=True;

end;

procedure TDialogOrder.Type_OutClick(Sender: TObject);
begin
     ViewOrdersART_FACTORY.Visible :=false;
     ViewOrdersINS_FACTORY.Visible :=false;
     ViewOrders.Bands[1].Visible:= False;
     BtnOrderOut.Tag:=0;
     TypeOrd:=0;

     dmIndividualOrder.dtHistory.Close;
     dmIndividualOrder.dtHistory.Open;

     BtnOffice.Enabled:=True;


     if dmCom.HereName <> '�����' then
     BtnOrderOut.Caption:= '���������'
     else
     BtnOrderOut.Caption:='������-������';
     BtnOffice.enabled:=true;
     BtnOffice.Caption:='������';
     ViewOrders.DataController.Filter.Root.Clear;

     if dmCom.HereName <> '�����' then
     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, dmCom.HereName, dmCom.HereName );

     ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '1', '1' );
     ViewOrders.DataController.Filter.Active:=True;
     

end;


procedure TDialogOrder.OtkazFormOpen;
var
 OtkazForm: Tot_Form;
begin

OtkazForm := Tot_Form.Create(Application);

                      Screen.Cursor:=crHourGlass;

                          try
                            try
                              OtkazForm.ShowModal;
                            except
                                On E: Exception do
                                   begin
                                      Screen.Cursor := crDefault;
                                      MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                                   end;
                             end;
                          finally
                              FreeAndNil(OtkazForm);
                              Screen.Cursor := crDefault;
                          end;

end;

procedure TDialogOrder.OffOrder;
Begin
    with dmIndividualOrder.dtHistory do
                      begin
                        Append;
                        Post;
                      end;


                      if dmIndividualOrder.mode = 2  then
                           Begin
                      with dmIndividualOrder.dtOrder_factory do
                      begin
                        Edit;
                        FieldByName('ID_State').AsInteger := 11;
                        FieldByName('COMMENT').Clear;
                        FieldByName('DATE_DOC').Clear;
                        FieldByName('NUMBER_DOC').Clear;
                        FieldByName('PRICE').Clear;
                        FieldByName('Emp_State').AsString := dmCom.UserName;
                        FieldByName('Date_State').AsDateTime := now;
                        Post;
                      end;
                           End;

                      if dmIndividualOrder.mode = 0  then
                          Begin
                      with dmIndividualOrder.dtOrder do
                      begin
                        Edit;
                        FieldByName('ID_State').AsInteger := 11;
                        FieldByName('COMMENT').Clear;
                        FieldByName('DATE_DOC').Clear;
                        FieldByName('NUMBER_DOC').Clear;
                        FieldByName('PRICE').Clear;
                        FieldByName('Emp_State').AsString := dmCom.UserName;
                        FieldByName('Date_State').AsDateTime := now;
                        Post;
//                        refresh;
                      end;
                           End;


                      refresh.Click;

End;

procedure TDialogOrder.ActionCancelExecute(Sender: TObject);
var

DataSet: TDataSet;
begin

dmIndividualOrder.CancelFlag := 1;
if dmIndividualOrder.mode=0 then    DataSet:= dmIndividualOrder.dtOrder;
if dmIndividualOrder.mode=1 then    DataSet:= dmIndividualOrder.dtOrder_in;

if dmIndividualOrder.mode=2 then    DataSet:= dmIndividualOrder.dtOrder_factory;

 OtkazFormOpen;


//TDialog.Information('����� �'+DataSet.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');
//
//      with dmIndividualOrder.dtHistory do
//        begin
//          Append;
//          Post;
//      end;
//
//      with DataSet do
//        begin
//          Edit;
//          FieldByName('ID_State').AsInteger := 11;
//          FieldByName('DATE_DOC').Clear;
//          FieldByName('NUMBER_DOC').Clear;
//          FieldByName('Emp_State').AsString := dmCom.UserName;
//          FieldByName('Date_State').AsDateTime := now;
//          FieldByName('PRICE').Clear;
//          Post;
//          Refresh;
//        end;
//
//          dmIndividualOrder.dthistory.Refresh;



end;

procedure TDialogOrder.ActionCOMMENTExecute(Sender: TObject);
begin
  if EditorNote.Visible=false then

        EditorNote.Visible:=True

  else
        EditorNote.Visible:=false;
end;

procedure TDialogOrder.ActionEditOffOUTExecute(Sender: TObject);
var
  State: Integer;
  NameState: String;
  buttonSelected : Integer;
begin
 Screen.Cursor:=crHourGlass;

if dmIndividualOrder.mode = 2  then
    Begin
     State := dmIndividualOrder.dtOrder_factory.FieldByName('ID_STATE').AsInteger;
     NameState := dmIndividualOrder.dtOrder_factory.FieldByName('NAME').AsString;
    End

else
    Begin
     State := dmIndividualOrder.dtOrder.FieldByName('ID_STATE').AsInteger;
     NameState := dmIndividualOrder.dtOrder.FieldByName('NAME').AsString;
    End;

case State of

            8, 9, 10, 4 :
             begin

                    buttonSelected := MessageBox(Handle,PChar('����� ��������� � ��������� ���������: "' + NameState + '"'+ ' ��������� ������ ��������� "��������"?' ),PChar('���������!'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);
                     if buttonSelected =  MB_OKCANCEL  then
                    begin


                    OffOrder;

                    end;
          end
        else
              TDialog.Information('����� �� � ����������� ���������! �������� ��������� ������.')  ;
        end;

  Screen.Cursor:=crDefault;
end;



procedure TDialogOrder.StateNext(DataSet: TDataSet; State_n: integer);
Begin

            Screen.Cursor:=crHourGlass;
            if DataSet.State in [dsEdit, dsInsert] then
                      begin

                          DataSet.Post;

                      end;

                      with dmIndividualOrder.dtHistory do
                      begin
                        Append;
                        Post;
                      end;

                      with DataSet do
                      begin
                        Edit;
                        FieldByName('ID_State').AsInteger := State_n;
                        FieldByName('Emp_State').AsString := dmCom.UserName;
                        FieldByName('Date_State').AsDateTime := now;
                        FieldByName('COMMENT').Clear;
                        Post;
                      end;
            refresh.Click;
            Screen.Cursor:=crDefault;


End;



procedure TDialogOrder.ActionEditStateFactoryExecute(Sender: TObject);
var
 buttonSelected : Integer;
 SaleForm: TSALE_JEW_FORM;
 VPForm: TVP_FORM;
 F:TForm;
begin
if (dmCom.HereName <> '�����') then
Begin
  case State of  4,8, 9,10 :
    begin
      ActionEditOffOUT.Execute;
      exit;
    end;
  end;
End;

if dmIndividualOrder.dtOrder_factory.FieldByName('ID_State').AsInteger = 1 then
    begin
      buttonSelected := MessageBox(Handle,PChar('�� ������������� ����� �������?'),PChar('������������� ������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

      if buttonSelected =  MB_OKCANCEL  then
        begin
            dmIndividualOrder.dtOrder_factory.edit;
            dmIndividualOrder.dtOrder_factory.Post;
        if dmIndividualOrder.dtOrder_factory.FieldByName('EMP_CREATE').asString = '' then
           begin
            ShowMessage('������� � ���� "������ ����� �� �������" ����, ��� ��������������� ������� � ��������');
           end
        else  StateNext(dmIndividualOrder.dtOrder_factory, 2);

        exit;
        end;
    end;


  if (dmIndividualOrder.dtOrder_factory.FieldByName('ID_State').AsInteger = 2 ) and (dmCom.HereName = '�����') then
              begin

//              F:=CreateMessageDialog('�� ���������� ����� �������?',mtConfirmation,[mbYes,mbNo]);
//for I:=0 to F.ControlCount-1 do
// if F.Controls[I].ClassName = 'TButton' then
// begin
// if (F.Controls[I] as TButton).Caption = '&Yes' then
//
//
// (F.Controls[I] as TButton).Caption := '&�������';
// if (F.Controls[I] as TButton).Caption = '&No' then
// (F.Controls[I] as TButton).Caption := '&�����';
// end;
// F.Caption:= PChar('����� ������ � �������');
//buttonSelected :=F.ShowModal;

      buttonSelected :=MessageBox(Handle,PChar('�� ���������� ����� �������?'),PChar('����� ������ � �������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

                if buttonSelected =  MB_OKCANCEL then
                    begin

    // ������ � ��������� "������ �� ������"
                     Preview.Click;
                     StateNext(dmIndividualOrder.dtOrder_factory, 3);
                     exit;
                    end
                else exit;

                end;


    if (dmIndividualOrder.dtOrder_factory.FieldByName('id_state').AsString='3') and (dmCom.HereName <> '�����')  then
                   begin
                      buttonSelected := MessageBox(Handle,PChar('����� ���������� � �����������?'),PChar('������������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

                      if (buttonSelected = MB_OKCANCEL) then
                          begin

                // ������ � ��������� "���������� � �����������"

                            StateNext(dmIndividualOrder.dtOrder_factory, 5);
                            exit;
                          end;

                    end;

 if (dmIndividualOrder.dtOrder_factory.FieldByName('id_state').AsString='5') and (dmCom.HereName = '�����') then
            begin
              buttonSelected := MessageBox(Handle,PChar('����� ����������� � �������������?'),PChar('�������������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

              if (buttonSelected = MB_OKCANCEL) then
                  begin

              // ������ � ��������� "����������� � �������������"

                  StateNext(dmIndividualOrder.dtOrder_factory, 6);
                  exit;
                  end;

              end;


if (dmIndividualOrder.dtOrder_factory.FieldByName('id_state').AsString='6') and (dmCom.HereName = '�����')  then
      begin

              buttonSelected := MessageBox(Handle,PChar('�� ������������� �������� �������?'),PChar('������������� ��������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

              if (buttonSelected = MB_OKCANCEL) then
                  begin
                    VPForm := TVP_FORM.Create(Application);
                    formShow_VP(VP_FORM);
                    exit;
                  end
               else     exit;

      end;



if (dmIndividualOrder.dtOrder_factory.FieldByName('ID_State').AsInteger = 7) and (dmCom.HereName <>'�����') then
    begin
      buttonSelected := MessageBox(Handle,PChar('�� ������������� ������� �������?'),PChar('������������� �������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

     if (buttonSelected = MB_OKCANCEL) then
            begin

              SaleForm := TSALE_JEW_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;

              try
                try
                  SaleForm.ShowModal;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SaleForm);
                Screen.Cursor := crDefault;
              end;
            end;

       // TDialog.Information('����� �'+dmIndividualOrder.dtOrder_factory.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

        exit;

    end;



end;

procedure TDialogOrder.FormShow_VP(Form: TForm);
begin

              Screen.Cursor:=crHourGlass;
              Form := TVP_FORM.Create(Application);
              try
                try
                  Form.ShowModal;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(Form);
                Screen.Cursor := crDefault;
              end;
end;



procedure TDialogOrder.N2Click(Sender: TObject);
var i:integer;
begin
ViewOrders.DataController.Filter.Root.Clear;

for i := 0 to ViewOrders.ColumnCount - 1 do
begin
  ViewOrders.Columns[i].GroupIndex := -1;
  ViewOrders.Columns[i].Visible := true;
end;

if dmIndividualOrder.mode = 3 then
Refresh.Click;

ViewOrders.Columns[ViewOrdersNUMBER_DOC.Index].Visible := False;
ViewOrders.Columns[ViewOrdersDATE_DOC.Index].Visible := False;
ViewOrders.Columns[ViewOrdersID_STATE.Index].Visible := False;
ViewOrders.Columns[ViewOrdersTYPE_ORDER.Index].Visible := False;
ViewOrders.Columns[ViewOrdersID_PREVIEW.Index].Visible := False;

Refresh.Click;
end;

procedure TDialogOrder.N3Click(Sender: TObject);
var
DataSet: TMyQuery;
TextOrder: string;
begin
  if dmIndividualOrder.mode = 0 then
     DataSet:=dmIndividualOrder.dtOrder;

  if dmIndividualOrder.mode = 1 then
     DataSet:=dmIndividualOrder.dtOrder_in;

  if dmIndividualOrder.mode = 2 then
     DataSet:=dmIndividualOrder.dtOrder_factory;

  if dmIndividualOrder.mode = 3 then
     DataSet:=dmIndividualOrder.dtOrder_off;

  if dmIndividualOrder.mode = 4 then
     DataSet:=dmIndividualOrder.dtCenter;

  TextOrder:='';

  if DataSet.FieldByName('GOOD').AsString <>'' then
     TextOrder:= DataSet.FieldByName('GOOD').AsString + ' ';

  if DataSet.FieldByName('ART').AsString <> '' then
  begin
  if (DataSet.FieldByName('ART').AsString <> '') and (DataSet.FieldByName('ART_FACTORY').AsString <> '') then
      TextOrder:= TextOrder+ DataSet.FieldByName('ART').AsString+ ' ('+ DataSet.FieldByName('ART_FACTORY').AsString+ ')' +'; ';
   if (DataSet.FieldByName('ART').AsString <> '') and (DataSet.FieldByName('ART_FACTORY').AsString = '') then
      TextOrder:= TextOrder+ DataSet.FieldByName('ART').AsString+ '; ';


  end
     else
  if (DataSet.FieldByName('ART').AsString = '') and (DataSet.FieldByName('ART_FACTORY').AsString <> '') then
      TextOrder:= TextOrder+ DataSet.FieldByName('ART_FACTORY').AsString+ '; ';

  if DataSet.FieldByName('COMP').AsString <>'' then
     TextOrder:= TextOrder+ '������������: ' + DataSet.FieldByName('COMP').AsString+ '; ';

  if DataSet.FieldByName('MAT').AsString <> '' then
     TextOrder:= TextOrder+ '��������: ' + DataSet.FieldByName('MAT').AsString+ '; ';

  if DataSet.FieldByName('sz').AsString <> '' then
     TextOrder:= TextOrder+  '������: '+ DataSet.FieldByName('sz').AsString + ' ';

   if DataSet.FieldByName('INS_FACTORY').AsString <>'' then
     TextOrder:= TextOrder+ '�������: '+ DataSet.FieldByName('INS_FACTORY').AsString + ' ';

  BufferToClipboard(TextOrder);
end;

procedure TDialogOrder.N4Click(Sender: TObject);
begin

  if dmCom.HereName = '�����' then
        begin
          N4.Caption := '�������� ���������� � ������ (���., ���������, ������ � ��.)';
          ViewOrdersART_Factory.Options.Editing := True;
          ViewOrdersCOMP.Options.Editing := True;
          ViewOrdersGOOD.Options.Editing := True;
          ViewOrdersMAT.Options.Editing := True;
          ViewOrdersSZ.Options.Editing := True;
          ViewOrdersINS_FACTORY.Options.Editing := True;
        end
        else
          ViewOrdersART_Factory.Options.Editing := True;
end;



procedure TDialogOrder.N6Click(Sender: TObject);
begin
  if dmIndividualOrder.dtOrder_factory.FieldByName('REMARK').AsInteger = 1 then
     Begin
  dmIndividualOrder.Command.SQL.Text:='update `IND_ORDER` set `IND_ORDER`.`REMARK` = 0 where IND_ORDER.ID_ORDER = :ID_ORDER;';

   dmIndividualOrder.Command.ParamByName('ID_ORDER').AsInteger:= dmIndividualOrder.dtOrder_factory.FieldByName('ID_ORDER').AsInteger;
   dmIndividualOrder.Command.Execute;

   refresh.Click;
     End
          else
             Begin
   dmIndividualOrder.Command.SQL.Text:='update `IND_ORDER` set `IND_ORDER`.`REMARK` = 1 where IND_ORDER.ID_ORDER = :ID_ORDER;';

   dmIndividualOrder.Command.ParamByName('ID_ORDER').AsInteger:= dmIndividualOrder.dtOrder_factory.FieldByName('ID_ORDER').AsInteger;
   dmIndividualOrder.Command.Execute;

   refresh.Click;
             End;
end;


function MyMessageDialog(const Msg: string; DlgType: TMsgDlgType;
   Buttons: TMsgDlgButtons; Captions: array of string): Integer;
 var
   aMsgDlg: TForm;
   i: Integer;
   dlgButton: TButton;
   CaptionIndex: Integer;
 begin
   { Create the Dialog }
   { Dialog erzeugen }
   aMsgDlg := CreateMessageDialog(Msg, DlgType, Buttons);
   captionIndex := 0;
   { Loop through Objects in Dialog }
   { Uber alle Objekte auf dem Dialog iterieren}
   for i := 0 to aMsgDlg.ComponentCount - 1 do
   begin
    { If the object is of type TButton, then }
    { Wenn es ein Button ist, dann...}
     if (aMsgDlg.Components[i] is TButton) then
     begin
       dlgButton := TButton(aMsgDlg.Components[i]);
       if CaptionIndex > High(Captions) then Break;
       { Give a new caption from our Captions array}
       { Schreibe Beschriftung entsprechend Captions array}
       dlgButton.Caption := Captions[CaptionIndex];
       Inc(CaptionIndex);
     end;
   end;
   Result := aMsgDlg.ShowModal;
 end;
procedure TDialogOrder.ActionEditStateINExecute(Sender: TObject);
 var
 buttonSelected : Integer;
 VPForm: TVP_FORM;
 F: TForm;
begin

if (dmIndividualOrder.dtOrder_in.FieldByName('id_state').AsString='2')  then
   begin
//    F:=CreateMessageDialog('�� ������������� �������� �������?',mtConfirmation,[mbYes,mbNo]);
//for I:=0 to F.ControlCount-1 do
// if F.Controls[I].ClassName = 'TButton' then
// begin
// if (F.Controls[I] as TButton).Caption = '&Yes' then
//
//
// (F.Controls[I] as TButton).Caption := '&��������';
// if (F.Controls[I] as TButton).Caption = '&No' then
// (F.Controls[I] as TButton).Caption := '&�����';
// end;
// F.Caption:= PChar('������������� ��������');
//buttonSelected :=F.ShowModal;


       buttonSelected :=MessageBox(Handle,PChar('�� ������������� �������� �������?'),PChar('������������� ��������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

        if (buttonSelected = MB_OKCANCEL) then
            begin
          Preview.Click;
          VPForm := TVP_FORM.Create(Application);
          formShow_VP(VP_FORM);
          exit;
            end
        else    exit;


    end;


end;

procedure TDialogOrder.ActionEditStateOUTExecute(Sender: TObject);
var
 buttonSelected : Integer;
 SaleForm: TSALE_JEW_FORM;
 F:TForm;
begin
if State = 2 then
       TDialog.Information('���������� �������� ���������! ��������� ��������� �� �����������.');

case State of  4,8, 9,10 :
  begin
    ActionEditOffOUT.Execute;
    exit;
  end;
end;

if (State = 1) and (dmIndividualOrder.mode = 0) then
    begin
      buttonSelected := MessageBox(Handle,PChar('�� ������������� ����� �������?'),PChar('������������� ������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

      if buttonSelected =  MB_OKCANCEL  then
        begin
            dmIndividualOrder.dtOrder.edit;
            dmIndividualOrder.dtOrder.Post;
        if dmIndividualOrder.dtOrder.FieldByName('EMP_CREATE').asString = '' then
           begin
            ShowMessage('������� � ���� "������ ����� �� �������" ����, ��� ��������������� ������� � ��������');
           end
        else  StateNext(dmIndividualOrder.dtOrder, 2);

        exit;
        end;
    end;


    
if State = 7 then
    begin
buttonSelected :=MessageBox(Handle,PChar('�� ������������� ������� �������?'),PChar('������������� �������'),MB_ICONINFORMATION+MB_OKCANCEL+MB_DEFBUTTON2);

        if (buttonSelected = MB_OKCANCEL) then
            begin

              SaleForm := TSALE_JEW_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;

              try
                try
                  SaleForm.ShowModal;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SaleForm);
                Screen.Cursor := crDefault;
              end;

            end;

       exit;
    end;


end;



procedure TDialogOrder.ActionExcludeFactoryExecute(Sender: TObject);
begin
    Screen.Cursor:=crHourGlass;
    dmIndividualOrder.dtOrder_factory.Delete;
    Refresh.Click;
    Screen.Cursor:=crDefault;
end;

procedure TDialogOrder.ActionExcludOUTExecute(Sender: TObject);
begin

    Screen.Cursor:=crHourGlass;
    dmIndividualOrder.dtOrder.Delete;
    Refresh.Click;
    Screen.Cursor:=crDefault;
end;

procedure TDialogOrder.ActionExitExecute(Sender: TObject);
begin
    Close;
end;



procedure TDialogOrder.ActionIncludeFactoryExecute(Sender: TObject);
begin
  Screen.Cursor:=crHourGlass;

  try
    dmIndividualOrder.dtOrder_factory.Append;
    dmIndividualOrder.dtOrder_factory.Post;
    dmIndividualOrder.dtOrder_factory.Refresh;

  except
    On E: Exception do
    begin
      ShowMessage(E.Message);
      Screen.Cursor:=crDefault;
    end;
  end;

  Screen.Cursor:=crDefault;
end;

procedure TDialogOrder.ActionIncludeOUTExecute(Sender: TObject);
begin

  Screen.Cursor:=crHourGlass;

  try
    dmIndividualOrder.dtOrder.Append;
    dmIndividualOrder.dtOrder.Post;
    dmIndividualOrder.dtOrder.Refresh;
  except
    On E: Exception do
    begin
      ShowMessage(E.Message);
      Screen.Cursor:=crDefault;
    end;
  end;

  ViewOrders.StoreToIniFile(ExtractFilePath(Application.ExeName) + 'viewconfig.ini',true,[gsoUseSummary],'OrderView');

  Screen.Cursor:=crDefault;

end;

procedure TDialogOrder.OnDateOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
   Dialog := TDialogBuyRangePopup(Popup);

  dmIndividualOrder.BD := Dialog.RangeBegin;
  dmIndividualOrder.ED :=Dialog.RangeEnd;
  laPeriod.Caption:= 'C ' + Datetostr(dmIndividualOrder.BD) + ' �� ' + Datetostr(dmIndividualOrder.ED);
  refresh.click;
end;



procedure TDialogOrder.ActionlaPeriodExecute(Sender: TObject);
var
  DialogModern: TDialogBuyRangePopup;
  Dialog: TDialogByRange;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin

    GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

    Y := BarWindowRect.Bottom + 4;

   // ButtonRect := ButtonRange.ClickItemLink.ItemRect;

    X := BarWindowRect.Left + 300;//ButtonRect.Left;

    DialogModern := TDialogBuyRangePopup.Create(Self);

    DialogModern.BarManager.ImageOptions.LargeImages:= dmIndividualOrder.ImageCalendar;

    DialogModern.RangeBegin := dmIndividualOrder.BD;

    DialogModern.RangeEnd := dmIndividualOrder.ED;

    DialogModern.OnOk := OnDateOk;

    DialogModern.Popup(X, Y);
//  end else
//
//  begin
//    Dialog := TDialogByRange.Create(Self);
//
//    Dialog.RangeBegin := dmIndividualOrder.BD;
//
//    Dialog.RangeEnd := dmIndividualOrder.ED;
//
//    if  Dialog.Execute then
//    begin
//     dmIndividualOrder.BD := Dialog.RangeBegin;
//
//     dmIndividualOrder.ED := Dialog.RangeEnd;
//
//     refresh.click;
//    end;

  //  Dialog.Free;
  //end;
end;

procedure TDialogOrder.ActionOtkazExecute(Sender: TObject);
begin
 dmIndividualOrder.CancelFlag := 0;
 Preview.Click;
 OtkazFormOpen;
 exit;


end;

procedure TDialogOrder.ActionPreviewExecute(Sender: TObject);
begin
  if dmIndividualOrder.mode = 1 then
   begin
   dmIndividualOrder.Command.SQL.Text:='update IND_ORDER set IND_ORDER.ID_PREVIEW = 1 where IND_ORDER.ID_ORDER = :ID_ORDER;';

   dmIndividualOrder.Command.ParamByName('ID_ORDER').AsInteger:= dmIndividualOrder.dtOrder_in.FieldByName('ID_ORDER').AsInteger;
   dmIndividualOrder.Command.Execute;

   refresh.Click;
    end;

  if ((dmIndividualOrder.mode = 2)and (dmCom.HereName = '�����' )) then
   begin
   dmIndividualOrder.Command.SQL.Text:='update IND_ORDER set IND_ORDER.ID_PREVIEW = 1 where IND_ORDER.ID_ORDER = :ID_ORDER;';

   dmIndividualOrder.Command.ParamByName('ID_ORDER').AsInteger:= dmIndividualOrder.dtOrder_factory.FieldByName('ID_ORDER').AsInteger;
   dmIndividualOrder.Command.Execute;
   refresh.Click;
  // ViewOrdersCustomDrawCell(ViewOrders, ViewOrders.ViewInfo.Canvas, ViewOrders.ViewInfo, true ) ;


    end;


end;

procedure TDialogOrder.ActionPrintExecute(Sender: TObject);
var
 bCellAutoHeight, bCellAutoHeight1: boolean;
Caption: string;
  Left, Right, Top, Bottom: Integer;
  MinLeft, MinRight, MinTop, MinBottom: Integer;
begin
ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := False;
ViewOrders.Columns[ViewOrdersCOST.Index].Visible := False;
ViewOrders.Columns[ViewOrdersDATE_STATE.Index].Visible := False;
ViewOrders.Columns[ViewOrdersDocumentName.Index].Visible := False;
ViewOrders.Columns[ViewOrdersPrice.Index].Visible := False;
ViewOrders.Columns[ViewOrdersTIME_ORDER.Index].Visible := False;

if dmCom.HereName = '�����' then
  begin
    ViewOrders.Columns[ViewOrdersFIO.Index].Visible := False;
    ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := False;
    ViewOrders.Columns[ViewOrdersEMP_CREATE.Index].Visible := False;
  end;


ViewOrders.Columns[ViewOrdersDATE_CREATE.Index].Visible := False;
ViewOrders.Columns[ViewOrdersCOMMENT.Index].Visible := False;
ViewOrders.Columns[ViewOrdersEMP_STATE.Index].Visible := False;
ViewOrders.Columns[ViewOrdersPrepay.Index].Visible := False;
ViewOrders.Bands[1].Visible:=false;
levelHistory.Visible:=false;
bCellAutoHeight := ViewOrders.OptionsView.CellAutoHeight;
ViewOrders.OptionsView.CellAutoHeight := true;

//bCellAutoHeight1 := ViewHistory.OptionsView.CellAutoHeight;
//ViewHistory.OptionsView.CellAutoHeight := true;

Caption := Grid.ActiveLevel.Caption;

Printer.PrintTitle := AnsiReplaceStr(Caption, #13#10, ' - ');

Printer.PreviewOptions.Caption := '��������������� ��������';

PrinterLink1.ReportTitle.Text := Caption;

PrinterLink1.ReportDocument.Caption := '';

PrinterLink1.ReportDocument.CreationDate := Date;

PrinterLink1.PrinterPage.DMPaper := DMPAPER_A4;



  PrinterLink1.PrinterPage.ScaleMode := smFit;

  PrinterLink1.ShowPageHeader := True;

  PrinterLink1.ShowPageFooter := True;

  PrinterLink1.ShowPageRowHeader := True;

  PrinterLink1.PrinterPage.AutoSwapMargins := True;

  PrinterLink1.PrinterPage.PageFooter.RightTitle.Text := '[Page #]';

  PrinterLink1.PrinterPage.GetRealMinMargins(MinLeft, MinRight, MinTop, MinBottom);

  PrinterLink1.PrinterPage.Margins.Top := Max(MinTop, 5 * 1000);

  PrinterLink1.PrinterPage.Margins.Left := Max(MinLeft, 10 * 1000);

  PrinterLink1.PrinterPage.Margins.Bottom := Max(MinBottom, 5 * 1000);

  PrinterLink1.PrinterPage.Margins.Right := Max(MinRight, 5 * 1000);

  PrinterLink1.OptionsView.FilterBar := False;

  PrinterLink1.OptionsView.Caption := True;

  PrinterLink1.OptionsView.ExpandButtons := True;
  
  PrinterLink1.OptionsOnEveryPage.Caption := True;

  PrinterLink1.OptionsOnEveryPage.Footers := True;

  PrinterLink1.Component:= Grid;

  PrinterLink1.OnCustomDrawBandCell := OnCustomDrawBandCell;

  PrinterLink1.SupportedCustomDraw := True;

  PrinterLink1.Preview(True);
  ViewOrders.OptionsView.CellAutoHeight := bCellAutoHeight;
  ViewHistory.OptionsView.CellAutoHeight := bCellAutoHeight1;


ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := True;
ViewOrders.Columns[ViewOrdersDATE_STATE.Index].Visible := True;
ViewOrders.Columns[ViewOrdersDocumentName.Index].Visible := True;
ViewOrders.Columns[ViewOrdersTIME_ORDER.Index].Visible := True;
ViewOrders.Columns[ViewOrdersFIO.Index].Visible := True;
ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := True;
ViewOrders.Columns[ViewOrdersEMP_CREATE.Index].Visible := True;
ViewOrders.Columns[ViewOrdersDATE_CREATE.Index].Visible := True;
ViewOrders.Columns[ViewOrdersCOMMENT.Index].Visible := True;
ViewOrders.Columns[ViewOrdersEMP_STATE.Index].Visible := True;

 if dmIndividualOrder.mode = 2 then
     ViewOrders.Bands[1].Visible:= True;

levelHistory.Visible:= True; 

end;

procedure TDialogOrder.OnCustomDrawBandCell(Sender: TdxGridReportLink; ACanvas: TCanvas; AView: TcxGridBandedTableView; ABand: TcxGridBand;  AnItem: TdxReportCellString; var ADone: Boolean);
begin
  AnItem.Multiline := True;
end;

procedure TDialogOrder.ActionRefreshCenterExecute(Sender: TObject);
var
x:integer;
y:integer;
begin
 x := ViewOrders.Controller.TopRecordIndex;
 if x<>-1 then y := dmIndividualOrder.dtCenter.FieldByName('ID_ORDER').AsInteger;
Screen.Cursor:=crHourGlass;
      x := ViewOrders.Controller.TopRecordIndex;
      dmIndividualOrder.dtCenter.Close;
      dmIndividualOrder.dtCenter.Open;
      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;
      
      

 if x<>-1 then
        Begin
          ViewOrders.DataController.DataSource.DataSet.Locate('ID_ORDER', y, []);
        End;

        dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;      
Screen.Cursor:=crDefault;
end;

procedure TDialogOrder.ActionRefreshFactoryExecute(Sender: TObject);
var
x:integer;
y:integer;
begin
 x := ViewOrders.Controller.TopRecordIndex;
 if x<>-1 then y := dmIndividualOrder.dtOrder_factory.FieldByName('ID_ORDER').AsInteger;
 Screen.Cursor:=crHourGlass;
      dmIndividualOrder.dtOrder_factory.Close;
      dmIndividualOrder.dtOrder_factory.Open;
      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;
      //ViewHistory.OptionsView.ColumnAutoWidth := true;  // �������������� ������ �������

 if x<>-1 then
        Begin
          ViewOrders.DataController.DataSource.DataSet.Locate('ID_ORDER', y, []);
        End;
       dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;
Screen.Cursor:=crDefault;
end;

procedure TDialogOrder.ActionRefreshINExecute(Sender: TObject);
var
x:integer;
y:integer;
begin
 x := ViewOrders.Controller.TopRecordIndex;
 if x<>-1 then y := dmIndividualOrder.dtOrder_in.FieldByName('ID_ORDER').AsInteger;
Screen.Cursor:=crHourGlass;
      x := ViewOrders.Controller.TopRecordIndex;
      dmIndividualOrder.dtOrder_in.Close;
      dmIndividualOrder.dtOrder_in.Open;
      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;

if x<>-1 then
        Begin
          ViewOrders.DataController.DataSource.DataSet.Locate('ID_ORDER', y, []);
        End;

      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;
Screen.Cursor:=crDefault;

end;

procedure TDialogOrder.ActionRefreshOFFExecute(Sender: TObject);

begin
 x := ViewOrders.Controller.TopRecordIndex;
 if x<>-1 then y := dmIndividualOrder.dtOrder_off.FieldByName('ID_ORDER').AsInteger;
 Screen.Cursor:=crHourGlass;


      dmIndividualOrder.dtOrder_off.Close;

      dmIndividualOrder.dtOrder_off.Open;
      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;

      if TypeOrd=0 then      Type_Out.Click;
      if TypeOrd=1 then      Type_In.Click;
      if TypeOrd=2 then      Type_Factory.Click;

      if x<>-1 then
        Begin
          ViewOrders.DataController.DataSource.DataSet.Locate('ID_ORDER', y, []);
        End;

      Screen.Cursor:=crDefault;

      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;
end;

procedure TDialogOrder.ActionRefreshOUTExecute(Sender: TObject);
var
x:integer;
y:integer;
begin
 x := ViewOrders.Controller.TopRecordIndex;
 if x<>-1 then y := dmIndividualOrder.dtOrder.FieldByName('ID_ORDER').AsInteger;

Screen.Cursor:=crHourGlass;


      Screen.Cursor:=crHourGlass;
      dmIndividualOrder.dtOrder.Close;
      dmIndividualOrder.dtOrder.Open;
      dmIndividualOrder.dtHistory.Close;
      dmIndividualOrder.dtHistory.Open;

      

 if x<>-1 then
        Begin
           ViewOrders.DataController.DataSource.DataSet.Locate('ID_ORDER', y, []);
        End;

      Screen.Cursor:=crDefault;

end;


procedure TDialogOrder.PageControlClick(Sender: TObject);
begin
// ViewOrders.Columns[ViewOrdersOLD_STATE.Index].Visible := True;
 dmIndividualOrder.mode:= pageControl.ActivePageIndex;
 ViewOrders.DataController.Filter.Root.Clear;
 BarFilter.Visible:=False;
  editotkaz.Action:=ActionOtkaz;
 //Refresh.Click;  ���������� (��������� ������ ������) ������ ������
 if ((dmIndividualOrder.mode = 2) and (dmCom.HereName = '�����')) then
    begin
     N6.Visible:=true;
     Cancel.Enabled:=true;
    end
     else
    begin
     N6.Visible:=false;
     Cancel.Enabled:=False;
    end;

 if ((dmIndividualOrder.mode = 3) or (dmIndividualOrder.mode = 4)) then
    begin
     Cancel.Enabled:=False;
     editotkaz.Enabled:=False;
     editState.Enabled:=False;
    end;

  if ((dmIndividualOrder.mode = 0) or (dmIndividualOrder.mode = 1)) then
    begin
     Cancel.Enabled:=true;
    end;
  //
 ViewOrders.Bands[1].Visible:= False;

 if dmIndividualOrder.mode = 4 then
   begin
          Preview.Enabled:=False;
          ActionOtkaz.Enabled:=False;
          refresh.Action:=ActionRefreshCenter;
          del.Enabled:=False;
          add.Enabled:=False;
          editState.Enabled:=False;
          editoff.Enabled:=False;

          ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := True;
          ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := True;
          ViewOrdersART_FACTORY.Visible :=false;
          ViewOrdersINS_FACTORY.Visible :=false;
          ViewOrders.DataController.DataSource.DataSet:=dmIndividualOrder.dtCenter;

          Refresh.Click;

          ColumnEditingClose;
          ViewOrders.Columns[ViewOrdersOLD_State.Index].Visible := False;
   end;


 if dmIndividualOrder.mode = 2 then
   begin

     ViewOrders.Bands[1].Visible:= True;
     ViewOrders.DataController.Filter.Root.Clear;
     Add.Action:=ActionIncludeFactory;
     refresh.Action:=ActionRefreshFactory;
     editState.Action:=ActionEditStateFactory;
     del.Action:=ActionExcludeFactory;
     ViewOrdersART_FACTORY.Visible:=true;
     ViewOrdersINS_FACTORY.Visible :=true;
     ViewOrders.Columns[ViewOrdersOLD_State.Index].Visible := False;
     if dmCom.HereName <> '�����' then
        begin
          Preview.Enabled:=False;
          ViewOrders.Columns[ViewOrdersFIO.Index].Visible := True;
          ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := True;
          ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := False;
          ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := True;
          ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := False;
          del.Enabled:=true;
          add.Enabled:=true;
          editState.Enabled:=true;
          editoff.Enabled:=true;
        end
     else
        begin
          N4.Caption := '�������� ���������� � ������ (���., ���������, ������ � ��.)';
          Preview.Enabled:=True;
          ViewOrders.Columns[ViewOrdersFIO.Index].Visible := False;
          ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := False;
          ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := False;
          ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := True;
          ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := True;
          del.Enabled:=false;
          add.Enabled:=false;
          editState.Enabled:=false;
          editoff.Enabled:=false;
        end;

     ViewOrders.DataController.DataSource.DataSet:=dmIndividualOrder.dtOrder_factory;


   end;


if dmIndividualOrder.mode = 1 then
   begin
     Preview.Enabled:=True;
     Refresh.Action := ActionRefreshIn;
     editState.Action := ActionEditStateIN;
     ViewOrdersART_FACTORY.Visible :=false;
     ViewOrdersINS_FACTORY.Visible :=false;

     ViewOrders.Columns[ViewOrdersFIO.Index].Visible := False;
     ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := False;
     ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := False;
     ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := True;

     del.Enabled:=false;
     add.Enabled:=false;
     editoff.Enabled:=false;

     ViewOrders.DataController.DataSource.DataSet:=dmIndividualOrder.dtOrder_in;

     Refresh.Click;
     ViewOrders.Columns[ViewOrdersOld_State.Index].Visible := False;
   end;


if dmIndividualOrder.mode = 0 then
   begin

      Preview.Enabled:=False;
      ViewOrders.Columns[ViewOrdersFIO.Index].Visible := True;
      ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := True;
      ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := True;
      ViewOrdersART_FACTORY.Visible :=false;
      ViewOrdersINS_FACTORY.Visible :=false;

      Add.Action:=ActionIncludeOUT;
      refresh.Action:=ActionRefreshOUT;
      editState.Action:=ActionEditStateOUT;
      del.Action:=ActionExcludOUT;

      del.Enabled:=true;
      add.Enabled:=true;
      editState.Enabled:=true;
      editoff.Enabled:=true;

      ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := True;
      ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := False;

      ViewOrders.DataController.DataSource.DataSet:=dmIndividualOrder.dtOrder;

     Refresh.Click;
     ViewOrders.Columns[ViewOrdersOld_State.Index].Visible := False;
   end;


if dmIndividualOrder.mode = 3 then
   begin
     ViewOrders.Columns[ViewOrdersOld_State.Index].Visible := True;
     ActionOtkaz.Enabled:=False;
     Refresh.Click;
     Preview.Enabled:=False;
     BarFilter.Visible:=True;
     BtnOrderOut.Tag:=0;
     TypeOrd:=0;
     Refresh.Action := ActionRefreshOFF;

     ViewOrders.Columns[ViewOrdersFIO.Index].Visible := True;
     ViewOrders.Columns[ViewOrdersPHONE.Index].Visible := True;
     ViewOrders.Columns[ViewOrdersDEP_SEND.Index].Visible := True;
     ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := True;

     del.Enabled:=false;
     add.Enabled:=false;
     editoff.Enabled:=false;
     editState.Enabled:=false;
     editState.Caption:= '�������� ���������';

     ViewOrders.DataController.DataSource.DataSet:=dmIndividualOrder.dtOrder_off;

     Refresh.Click;

     ColumnEditingClose;

     if dmCom.HereName <> '�����' then
      begin
       BtnOrderOut.Caption := '���������';
       BtnOffice.Caption:='������';
       ViewOrders.DataController.Filter.Root.Clear;
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersDEP_CUSTOM, foEqual, dmCom.HereName, dmCom.HereName );
       ViewOrders.DataController.Filter.Root.AddItem(ViewOrdersType_Order, foEqual, '1', '1' );
       ViewOrders.DataController.Filter.Active:=True;
       ViewOrders.Columns[ViewOrdersINFO_ORDER.Index].Visible := False;
      end
  else
      begin
          Type_In.Visible:=ivNever;
          Type_Out.Caption:='������-������';
          Type_Factory.Click;
      end;


   end;

end;











procedure TDialogOrder.ViewHistoryCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
EditorNote.DataBinding.DataSource:= dmIndividualOrder.dsHistory;

end;

procedure TDialogOrder.ViewHistoryFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
 EditorNote.DataBinding.DataSource:= dmIndividualOrder.dsHistory;
end;

procedure TDialogOrder.ViewOrdersARTPropertiesCloseUp(Sender: TObject);
begin
if dmIndividualOrder.mode=2 then

 dmIndividualOrder.dtOrder_factory.Post

else

dmIndividualOrder.dtOrder.Post;


end;







procedure TDialogOrder.ViewOrdersCellClick(Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
EditorNote.DataBinding.DataSource:= dmIndividualOrder.dsOrder;

end;

procedure TDialogOrder.ViewOrdersCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin



if ((dmIndividualOrder.mode = 0) or ((dmIndividualOrder.mode = 2) and (dmCom.HereName<>'�����')))  then
  begin
		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of  10, 8, 4 :
			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clRed;
			end;
		end;

		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of 1, 3, 7, 9 :
			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clGreen;
			end;
		end;

		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of  2, 5, 6 :

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clSilver;
			end;
		end;
  end;




 if ((dmIndividualOrder.mode = 1) or ((dmIndividualOrder.mode = 2) and (dmCom.HereName='�����')))   then
  begin
 case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of 9, 10 :
		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_PREVIEW').Index],varInteger) of 1 :
     Begin

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clRed;
			end;

     End
		else
		begin


			if AViewInfo.Item.Name = 'ViewOrdersID_ORDER' then
			begin
				ACanvas.Brush.Color := clFuchsia;
			end;

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clRed;
			end;

		end;
		end;
	end;

	case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of 2, 5, 6 :

		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_PREVIEW').Index],varInteger) of 1 :
      Begin

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clGreen;
			end;
      End


		else

		begin


			if AViewInfo.Item.Name = 'ViewOrdersID_ORDER' then
			begin
				ACanvas.Brush.Color := clFuchsia;
			end;

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clGreen;
			end;

		end;
		end;

	end;

	case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of 3, 4, 8 :
		case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_PREVIEW').Index],varInteger) of 1 :
       Begin


			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clSilver;
			end;


       End
		else
		begin




			if AViewInfo.Item.Name = 'ViewOrdersID_ORDER' then
			begin
				ACanvas.Brush.Color := clFuchsia;
			end;

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clSilver;
			end;

		end;
		end;
	end;



  	case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_STATE').Index],varInteger) of  7 :
    case VarAsType(AviewInfo.GridRecord.Values[ViewOrders.GetColumnByFieldName ('ID_PREVIEW').Index],varInteger) of 1 :

				ACanvas.Brush.Color := clSilver



		else
		begin
      ACanvas.Brush.Color := clSilver;
			if AViewInfo.Item.Name = 'ViewOrdersID_ORDER' then
			begin
				ACanvas.Brush.Color := clFuchsia;
			end;

			if AViewInfo.Item.Name = 'ViewOrdersNAME' then
			begin
				ACanvas.Brush.Color := clSilver;
			end;

		end;


    end;
    end; 

  end;



end;




procedure TDialogOrder.ViewOrdersFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin

if ((dmIndividualOrder.mode = 3) or (dmIndividualOrder.mode = 4)) then
   begin
     ColumnEditingClose;
     if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
        EditorNote.Enabled:=false;
        Cancel.Enabled:=false;
   end;
if dmIndividualOrder.mode=2 then
  begin
   State:=dmIndividualOrder.dtOrder_factory.FieldByName('ID_STATE').AsInteger;
    Cancel.Enabled:=false;
   if (State = 1)  then
    begin
     del.Enabled:= True;

     ViewOrdersFIO.Options.Editing := True;
     ViewOrdersPHONE.Options.Editing := True;
     ViewOrdersART.Options.Editing := True;
     ViewOrdersART_Factory.Options.Editing := True;
     ViewOrdersINS_Factory.Options.Editing := True;
     ViewOrdersCOMP.Options.Editing := True;
     ViewOrdersGOOD.Options.Editing := True;
     ViewOrdersMAT.Options.Editing :=  True;
     ViewOrderssz.Options.Editing :=  True;
     ViewOrdersEMP_CREATE.Options.Editing :=  True;
     ViewOrdersCOMMENT.Options.Editing :=  True;
     ViewOrdersPREPAY.Options.Editing :=  True;
     editState.Enabled:=True;
     editState.Caption:= '��������';


     if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
        EditorNote.Enabled:=false;



    end

   else

    begin
      del.Enabled:= False;

      if  (dmCom.HereName <> '�����') then

          Begin

           case State of

            7,8,2,3,4,6 : begin ColumnEditingClose; ViewOrdersPREPAY.Options.Editing :=  True; end

           else
            Cancel.Enabled:=False;
            editOtkaz.Enabled:=False;
            ColumnEditingClose;
            ViewOrdersCOMMENT.Options.Editing :=  True;
            ViewOrdersPREPAY.Options.Editing :=  True;

           end;
		   
            case State of  2,5,6:
              begin
                ViewOrdersPREPAY.Options.Editing :=  True;
                Cancel.Enabled:=False;
                editState.Enabled:=False;
                editOtkaz.Enabled:=False;
                editState.Caption:= '�������� ���������';
              end;
            end;


            if  State = 3 then
                begin
                  Cancel.Enabled:=False;
                  editOtkaz.Enabled:=False;
                  editState.Enabled:=True;
                  editState.Caption:= '����������� � �����������';
                end;

            if  State = 7 then
                begin
                  editState.Enabled:=True;
                  editState.Caption:= '������� ������';
                  editOtkaz.Enabled:=True;
                  editOtkaz.Caption:= '����� �������';
                end;


            case State of  8,9,10,4 :
                begin
                  editOtkaz.Enabled:=False;
                  editState.Enabled:=True;
                  editState.Caption:= '��������� �����';
                end;
            end;
             if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
                EditorNote.Enabled:=false;
          End

      else    // ���� �����

          begin

            Cancel.Enabled:=true;

          if dmIndividualOrder.dtOrder_factory.FieldByName('REMARK').AsInteger = 1 then
             N6.Caption:='����� ���������'
          else
             N6.Caption:='�������� ����� ������';
          



           if dmIndividualOrder.dtOrder_factory.FieldByName('ID_PREVIEW').AsInteger = 1 then
               preview.Enabled:= false
           else
               preview.Enabled:= true;


            case State of

            2,5,9,10 :
            begin ColumnEditingClose;   if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
              EditorNote.Enabled:=false;
              Cancel.Enabled:=false;
            end

           else

            ColumnEditingClose;
            ViewOrdersCOMMENT.Options.Editing :=  True;
            if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
            EditorNote.Enabled:=false;
            end;

            ViewOrdersCOST.Options.Editing :=  True;
            ViewOrdersINFO_ORDER.Options.Editing :=  True;
            ViewOrdersTIME_ORDER.Options.Editing :=  True;

            if  State = 2 then
              begin
                editState.Enabled:=True;
                editState.Caption:= '������� �����';
                editOtkaz.Enabled:=True;
                editOtkaz.Caption:= '����� �� ����������';
              end;

            if  State = 5 then
              begin
                editState.Enabled:=True;
                editOtkaz.Enabled:=false;
                editState.Caption:= '����������� � �������������';
              end;

            if  State = 6 then
              begin
                editState.Enabled:=True;
                editState.Caption:= '�������� ������';
                editOtkaz.Enabled:=True;
                editOtkaz.Caption:= '����� ��������';
              end;

            case State of  3,7,8,9,10,4 :
              begin
                editState.Enabled:=False;
                editOtkaz.Enabled:=false;
                editState.Caption:= '�������� ���������';
                if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
                 EditorNote.Enabled:=false;
              end;


            end;
          end;

    end;

end;
	
if dmIndividualOrder.mode=1 then
begin
 Cancel.Enabled:=false;
 State:=dmIndividualOrder.dtOrder_in.FieldByName('ID_STATE').AsInteger;

 if dmIndividualOrder.dtOrder_in.FieldByName('ID_PREVIEW').AsInteger = 1 then
    preview.Enabled:= false
 else
    preview.Enabled:= true;

 case State of 10,2,9 : ColumnEditingClose;

 else

    ColumnEditingClose;
    ViewOrdersCOMMENT.Options.Editing :=  True;

 end;

 if  State = 2 then
  begin
    editState.Enabled:=True;
    editState.Caption:= '�������� ������';
    editOtkaz.Enabled:=True;
    editOtkaz.Caption:= '����� ��������';

  end;

 case State of  7,8,9,10 :
  begin
    editOtkaz.Enabled:=False;
    editState.Enabled:=False;
    editState.Caption:= '�������� ���������';
  end;
 end;
 
 if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
    EditorNote.Enabled:=false;



end;


if dmIndividualOrder.mode=0 then
begin
  Cancel.Enabled:=true;
  State:=dmIndividualOrder.dtOrder.FieldByName('ID_STATE').AsInteger;

  if (State = 1)  then
    begin
     del.Enabled:= True;

     ViewOrdersFIO.Options.Editing := True;
     ViewOrdersPHONE.Options.Editing := True;
     ViewOrdersART.Options.Editing := True;
     ViewOrdersCOMP.Options.Editing := True;
     ViewOrdersGOOD.Options.Editing := True;
     ViewOrdersMAT.Options.Editing :=  True;
     ViewOrderssz.Options.Editing :=  True;
     ViewOrdersDEP_SEND.Options.Editing :=  True;
     ViewOrdersCOMMENT.Options.Editing :=  True;
     ViewOrdersEMP_CREATE.Options.Editing :=  True;
     editState.Enabled:=True;
     editState.Caption:= '��������';
    end

   else

    begin
      del.Enabled:= False;

      case State of 7,8 : ColumnEditingClose;

      else

        ColumnEditingClose;
        ViewOrdersCOMMENT.Options.Editing :=  True;

      end;



	if  State = 2 then
	  begin
       editState.Enabled:=False;
        editOtkaz.Enabled:=False;
       editState.Caption:= '�������� ���������';
      end;


	if  State = 7 then
      begin
        editState.Enabled:=True;
        editOtkaz.Enabled:=true;
        editState.Caption:= '������� ������';
        editOtkaz.Caption:= '����� �������';
      end;


    case State of  8,9,10 :
      begin
        editOtkaz.Enabled:=false;
        editState.Enabled:=True;
        editState.Caption:= '��������� �����';
      end;
	end;
	
   end;
   if ViewOrdersCOMMENT.Options.Editing=true then  EditorNote.Enabled:=true else
      EditorNote.Enabled:=false;
end;



end;








procedure TDialogOrder.FormCreate(Sender: TObject);
begin
if dmIndividualOrder.dtOrder.Active = false then
   begin
    TDialog.Information('�� ������ ������ ������ ����������. ���������� ������������ �������!');
    Close;
   end
else
 begin
 ViewOrders.RestoreFromIniFile(IniFileName,true, true, [gsoUseFilter, gsoUseSummary], 'ind_order');
  if dmCom.HereName = '�����' then
    Begin

    pageControl.Pages[0].TabVisible:=False;
    pageControl.Pages[1].TabVisible:=false;

    End
  else
      pageControl.Pages[4].TabVisible:=false;

  dmIndividualOrder.COMP_NAME_S.Open;
  dmIndividualOrder.Good_NAME_S.Open;
  dmIndividualOrder.Mat_NAME_S.Open;

  ART_NAME_S.Open;
  DEP_NAME_S.Close;
  DEP_NAME_S.ParamByName('main_dep').AsString := dmCom.HereName;
  DEP_NAME_S.Open;
  EMP_DEP_S.Open;
  Department := dmCom.HereName;
  dmIndividualOrder.mode:= pageControl.ActivePageIndex;
  ViewOrders.Columns[ViewOrdersDEP_CUSTOM.Index].Visible := False;
  ViewOrders.Columns[ViewOrdersPrice.Index].Visible := True;

  BarFilter.Visible:=False;
  BtnOrderOut.Tag:= 0;
  TypeOrd:=0;
  dmIndividualOrder.dtOrder_off.Close;
  dmIndividualOrder.dtOrder_off.Open;

  PageControl.OnClick(pageControl);
    //EditorNote.DataBinding.DataField := 'COMMENT';

    ViewOrders.Columns[ViewOrdersCOMP.Index].Caption:= '������������';
    ViewOrders.Columns[ViewOrdersMAT.Index].Caption:= '��������';

    ViewOrders.Columns[ViewOrdersEMP_CREATE.Index].Caption:= '������ ����� �� �������';
end;



 laPeriod.Caption:= 'C ' + Datetostr(dmIndividualOrder.BD) + ' �� ' + Datetostr(dmIndividualOrder.ED);

end;



procedure TDialogOrder.FormDestroy(Sender: TObject);
begin
try
  Refresh.Click;
except

end;
  ART_NAME_S.Close;
  DEP_NAME_S.Close;
  ViewOrders.StoreToIniFile(IniFileName,true, [gsoUseFilter, gsoUseSummary], 'ind_order');
  dmIndividualOrder.COMP_NAME_S.Close;
  dmIndividualOrder.Good_NAME_S.Close;
  dmIndividualOrder.Mat_NAME_S.Close;
end;


procedure TDialogOrder.OfficeExecute(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 40;

  ButtonRect := BtnOffice.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  if officePopupMenu.ItemLinks.VisibleItemCount <> 0 then
  begin
    officePopupMenu.Popup(X, Y);
  end else

  begin
    TDialog.Information('��� ���������.');
  end;
end;

procedure TDialogOrder.OfficePopupMenuPopup(Sender: TObject);
begin
OfficePopupMenu.PopupFromCursorPos;
end;

end.
