object fmAddApplDepItem: TfmAddApplDepItem
  Left = 201
  Top = 269
  Width = 919
  Height = 542
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1103' '#1074' '#1079#1072#1103#1074#1082#1091' '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 449
    Top = 42
    Width = 4
    Height = 471
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 42
    Width = 449
    Height = 471
    Align = alLeft
    AllowedOperations = []
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsSelectUid
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.FieldName = 'UID'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 90
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1056#1072#1089#1093'. '#1094#1077#1085#1072
      end
      item
        EditButtons = <>
        FieldName = 'COSTP'
        Footer.FieldName = 'COSTP'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1080#1093'. '#1094#1077#1085#1072
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 911
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object LArt: TLabel
      Left = 32
      Top = 16
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object dbart: TDBText
      Left = 104
      Top = 16
      Width = 289
      Height = 17
      DataField = 'FULLART'
      DataSource = dm2.dsD_WH2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 467
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
  object pButton: TPanel
    Left = 453
    Top = 42
    Width = 458
    Height = 471
    Align = alClient
    TabOrder = 2
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 40
      Height = 469
      Align = alLeft
      TabOrder = 0
      object sbAdd: TSpeedButton
        Left = 8
        Top = 116
        Width = 23
        Height = 22
        Hint = #1044#1086#1073#1072#1074#1080#1090#1100
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333FF3333333333333447333333333333377FFF33333333333744473333333
          333337773FF3333333333444447333333333373F773FF3333333334444447333
          33333373F3773FF3333333744444447333333337F333773FF333333444444444
          733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
          999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
          33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
          333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
          33333777333333333333CC333333333333337733333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = sbAddClick
      end
      object sbDel: TSpeedButton
        Left = 8
        Top = 148
        Width = 23
        Height = 22
        Hint = #1059#1076#1072#1083#1080#1090#1100
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          333333333333333333FF3333333333333744333333333333F773333333333337
          44473333333333F777F3333333333744444333333333F7733733333333374444
          4433333333F77333733333333744444447333333F7733337F333333744444444
          433333F77333333733333744444444443333377FFFFFFF7FFFFF999999999999
          9999733777777777777333CCCCCCCCCC33333773FF333373F3333333CCCCCCCC
          C333333773FF3337F333333333CCCCCCC33333333773FF373F3333333333CCCC
          CC333333333773FF73F33333333333CCCCC3333333333773F7F3333333333333
          CCC333333333333777FF33333333333333CC3333333333333773}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = sbDelClick
      end
    end
    object dg2: TDBGridEh
      Left = 41
      Top = 1
      Width = 416
      Height = 469
      Align = alClient
      AllowedOperations = []
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dm3.dsApplDepArt
      Flat = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
      ParentFont = False
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clNavy
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          EditButtons = <>
          FieldName = 'UID'
          Footer.FieldName = 'UID'
          Footer.ValueType = fvtCount
          Footers = <>
          Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        end
        item
          EditButtons = <>
          FieldName = 'W'
          Footer.FieldName = 'W'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.Caption = #1042#1077#1089
        end
        item
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.Caption = #1056#1072#1079#1084#1077#1088
        end
        item
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
          Width = 65
        end
        item
          EditButtons = <>
          FieldName = 'COST'
          Footer.FieldName = 'COST'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1056#1072#1089#1093'. '#1094#1077#1085#1072
        end
        item
          EditButtons = <>
          FieldName = 'COSTP'
          Footer.FieldName = 'COSTP'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1080#1093'. '#1094#1077#1085#1072
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object fr: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 136
    Top = 208
  end
end
