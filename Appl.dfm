object fmAppl: TfmAppl
  Left = 231
  Top = 231
  HelpContext = 100282
  Caption = #1047#1072#1103#1074#1082#1072
  ClientHeight = 490
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter6: TSplitter
    Left = 0
    Top = 286
    Width = 761
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 291
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 761
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1092#1086#1088#1084#1091
      ImageIndex = 0
      Spacing = 1
      Left = 618
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      Action = acPrintAppl
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C000018631863186318631863186318631863186300001863
        00001F7C1F7C0000000000000000000000000000000000000000000000000000
        186300001F7C0000186318631863186318631863FF7FFF7FFF7F186318630000
        000000001F7C0000186318631863186318631863007C007C007C186318630000
        186300001F7C0000000000000000000000000000000000000000000000000000
        1863186300000000186318631863186318631863186318631863186300001863
        0000186300001F7C000000000000000000000000000000000000000018630000
        1863000000001F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00001863
        0000186300001F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F00000000
        000000001F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7F1F001F001F001F001F00FF7F0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 67
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acPrintApplExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acExportToTxt
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      Caption = 'SpeedItem2'
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acExportToTxtExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 498
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 289
    Width = 761
    Height = 201
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 357
      Top = 2
      Height = 197
    end
    object plFilter: TPanel
      Left = 2
      Top = 2
      Width = 355
      Height = 197
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Tag = 1
        Left = 33
        Top = 1
        Height = 195
        AutoSnap = False
        MinSize = 20
      end
      object Splitter4: TSplitter
        Tag = 2
        Left = 137
        Top = 1
        Height = 195
        AutoSnap = False
        MinSize = 20
      end
      object Splitter5: TSplitter
        Tag = 3
        Left = 247
        Top = 1
        Height = 195
        AutoSnap = False
        MinSize = 20
      end
      object Splitter8: TSplitter
        Tag = 2
        Left = 85
        Top = 1
        Height = 195
        AutoSnap = False
        MinSize = 20
      end
      object Splitter11: TSplitter
        Left = 302
        Top = 1
        Width = 4
        Height = 195
      end
      object Splitter10: TSplitter
        Left = 195
        Top = 1
        Height = 195
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 32
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 198
        Top = 1
        Width = 49
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 88
        Top = 1
        Width = 49
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 140
        Top = 1
        Width = 55
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 36
        Top = 1
        Width = 49
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt1: TListBox
        Left = 250
        Top = 1
        Width = 52
        Height = 195
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 5
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt2: TListBox
        Left = 306
        Top = 1
        Width = 48
        Height = 195
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 6
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object plArt: TPanel
      Left = 360
      Top = 2
      Width = 399
      Height = 197
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 1
      object tb2: TSpeedBar
        Left = 2
        Top = 2
        Width = 395
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 0
        InternalVer = 1
        object Label2: TLabel
          Left = 8
          Top = 8
          Width = 43
          Height = 13
          Caption = #1040#1088#1090#1080#1082#1091#1083
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cbSearch: TCheckBox
          Left = 156
          Top = 8
          Width = 53
          Height = 17
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = cbSearchClick
        end
        object ceArt: TComboEdit
          Left = 54
          Top = 4
          Width = 93
          Height = 21
          ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
          Color = clInfoBk
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000120B0000120B00001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
            DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
            9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
            9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
            90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
          NumGlyphs = 1
          TabOrder = 1
          OnButtonClick = ceArtButtonClick
          OnChange = ceArtChange
          OnClick = ceArtClick
          OnEnter = ceArtEnter
          OnKeyDown = ceArtKeyDown
          OnKeyUp = ceArtKeyUp
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1042#1089#1077
          Caption = 'SpeedItem1'
          Enabled = False
          Hint = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Spacing = 1
          Left = 163
          Top = 3
          SectionName = 'Untitled (0)'
        end
      end
      object dgArt: TM207IBGrid
        Left = 2
        Top = 31
        Width = 395
        Height = 164
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsArt
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnExit = dgArtExit
        OnKeyDown = dgArtKeyDown
        TitleButtons = True
        InsertOnInsKey = False
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FULLART'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Alignment = taLeftJustify
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            PickList.Strings = (
              #1043#1088
              #1064#1090)
            Title.Alignment = taCenter
            Title.Caption = #1045#1048
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 30
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RQ'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 35
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RW'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 49
            Visible = True
          end
          item
            Color = 16776176
            Expanded = False
            FieldName = 'AVG_W'
            Title.Caption = #1057#1088'.'#1074#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 45
            Visible = True
          end
          item
            Color = 16776176
            Expanded = False
            FieldName = 'AVG_SPR'
            Title.Caption = #1054#1088#1080#1077#1085#1090'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
  end
  object gridAppl: TDBGridEh
    Left = 0
    Top = 67
    Width = 761
    Height = 219
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsrAppl
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'ART'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.EndEllipsis = True
        Width = 85
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.EndEllipsis = True
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        Title.EndEllipsis = True
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.EndEllipsis = True
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.EndEllipsis = True
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.EndEllipsis = True
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Width = 60
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'PR'
        Footer.FieldName = 'PR'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ARTID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
        Title.Caption = #1054#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1072#1084
      end
      item
        DisplayFormat = '0.00'
        EditButtons = <>
        FieldName = 'AVG_W'
        Footers = <>
        Title.Caption = #1054#1088#1080#1077#1085#1090'.'#1074#1077#1089
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object TBDock1: TTBDock
    Left = 0
    Top = 42
    Width = 761
    Height = 25
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = Label1
      end
      object TBControlItem2: TTBControlItem
        Control = cbGrMat
      end
      object Label1: TLabel
        Left = 0
        Top = 4
        Width = 96
        Height = 13
        Caption = #1043#1088#1091#1087#1087#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1072' '
      end
      object cbGrMat: TComboBox
        Left = 96
        Top = 0
        Width = 145
        Height = 21
        Color = 11269361
        ItemHeight = 13
        TabOrder = 0
        OnClick = cbGrMatClick
      end
    end
  end
  object fmstrAppl: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'plFilter.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 24
    Top = 120
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 76
    Top = 120
    object acPrintAppl: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      OnExecute = acPrintApplExecute
      OnUpdate = acPrintApplUpdate
    end
    object acExportToTxt: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      OnExecute = acExportToTxtExecute
      OnUpdate = acExportToTxtUpdate
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1092#1086#1088#1084#1091
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acPrintgrid: TAction
      Caption = 'acPrintgrid'
      ShortCut = 16464
      OnExecute = acPrintgridExecute
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = gridAppl
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 200
    Top = 168
  end
end
