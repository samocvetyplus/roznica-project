unit Dlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFmDlg = class(TForm)
    edres: TEdit;
    btok: TBitBtn;
    btcancel: TBitBtn;
    procedure edresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    ResaltValue :string;
  end;

var
  FmDlg: TFmDlg;

implementation

{$R *.dfm}

procedure TFmDlg.edresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_RETURN then ModalResult := mrOk;
end;

procedure TFmDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if key = VK_ESCAPE then   ModalResult := mrCancel;
end;

end.
