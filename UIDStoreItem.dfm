object fmUidItem: TfmUidItem
  Left = 148
  Top = 277
  Width = 908
  Height = 540
  Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object dg1: TDBGridEh
    Left = 0
    Top = 69
    Width = 900
    Height = 423
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsUidItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 47
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 63
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 44
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1082#1083#1072#1076
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 47
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'GR'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1043#1088#1091#1087#1087#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUM'
        Footer.FieldName = 'ENTRYNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1093#1086#1076'.|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOST'
        Footer.FieldName = 'ENTRYCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1093#1086#1076'.|'#1089#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'SINVNUM'
        Footer.FieldName = 'SINVNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1086#1089#1090#1072#1074#1082#1080
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSNUM'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1048#1079#1083#1080#1096#1082#1080
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUM'
        Footer.FieldName = 'DINVNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 36
      end
      item
        EditButtons = <>
        FieldName = 'RETNUM'
        Footer.FieldName = 'RETNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETNUM'
        Footer.FieldName = 'OPTSELLNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'SINVCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1086#1089#1090#1072#1074#1082#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1048#1079#1083#1080#1096#1082#1080
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DINVCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMFROM'
        Footer.FieldName = 'DINVNUMFROM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'SELLNUM'
        Footer.FieldName = 'SELLNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1088#1086#1076#1072#1078#1072'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLNUM'
        Footer.FieldName = 'OPTSELLNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1088#1086#1076#1072#1078#1072'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETNUM'
        Footer.FieldName = 'SINVRETNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESNUM'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'DINVFROMCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1086#1076#1072#1078#1072'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1086#1076#1072#1078#1072'|'#1054#1087#1090
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1040#1082#1090#1099' '#1089#1087#1080#1089#1072#1085#1080#1103
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'RESNUM'
        Footer.FieldName = 'RESNUM'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 54
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'RESCOST'
        Footer.FieldName = 'RESCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'| '#1089#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 49
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'F'
        Footer.FieldName = 'F'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1060#1072#1082#1090'.|'#1085#1072#1083#1080#1095#1080#1077
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 54
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'FCOST'
        Footers = <>
        Title.Caption = #1060#1072#1082#1090'.|'#1089#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1042#1093#1086#1076'.|'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1042#1093#1086#1076'.|'#1089#1090#1086#1080#1084#1086#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'SINVNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1086#1089#1090#1072#1074#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
      end
      item
        EditButtons = <>
        FieldName = 'RETNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'SINVCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1086#1089#1090#1072#1074#1082#1072
      end
      item
        EditButtons = <>
        FieldName = 'DINVCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1055
      end
      item
        EditButtons = <>
        FieldName = 'RETCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
      end
      item
        EditButtons = <>
        FieldName = 'DINVFROMCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1086#1076#1072#1078#1072'|'#1056#1086#1079#1085#1080#1094#1072
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1086#1076#1072#1078#1072'|'#1054#1087#1090
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1040#1082#1090#1099' '#1089#1087#1080#1089#1072#1085#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'RESNUMCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RESCOSTCOM'
        Footers = <>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'| '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 900
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1042#1099#1093#1086#1076'|'
      ImageIndex = 0
      Spacing = 1
      Left = 826
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100'|'
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100'|'
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 900
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object Label2: TLabel
      Left = 172
      Top = 8
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
      Transparent = True
    end
    object Label3: TLabel
      Left = 328
      Top = 8
      Width = 99
      Height = 13
      Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103
      Transparent = True
    end
    object Label4: TLabel
      Left = 560
      Top = 8
      Width = 77
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1080#1079#1076#1077#1083#1080#1103
      Transparent = True
    end
    object Label5: TLabel
      Left = 736
      Top = 8
      Width = 51
      Height = 13
      Caption = #1050#1086#1084#1080#1089#1089#1080#1103
      Transparent = True
    end
    object cmbxDep: TDBComboBoxEh
      Left = 44
      Top = 4
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxDepChange
    end
    object cmbxGrMat: TDBComboBoxEh
      Left = 212
      Top = 4
      Width = 105
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 1
      Visible = True
      OnChange = cmbxGrMatChange
    end
    object cbstate: TComboBox
      Left = 432
      Top = 4
      Width = 113
      Height = 21
      Style = csDropDownList
      Ctl3D = True
      ItemHeight = 13
      ItemIndex = 0
      ParentCtl3D = False
      TabOrder = 2
      Text = '*'#1042#1089#1077
      OnChange = cbstateChange
      Items.Strings = (
        '*'#1042#1089#1077
        #1087#1088#1080#1089#1091#1090#1089#1090#1074#1091#1077#1090
        #1086#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        #1085#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086)
    end
    object eduid: TEdit
      Left = 640
      Top = 4
      Width = 81
      Height = 21
      TabOrder = 3
      OnKeyDown = eduidKeyDown
    end
    object cbCom: TComboBox
      Left = 795
      Top = 5
      Width = 97
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      OnChange = cbComChange
      Items.Strings = (
        '*'#1042#1089#1077
        #1082#1086#1084#1080#1089#1089#1080#1103
        #1073#1077#1079' '#1082#1086#1084#1080#1089#1089#1080#1080)
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 492
    Width = 900
    Height = 19
    Panels = <>
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    PrintFontName = 'Times New Roman'
    Title.Strings = (
      #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086)
    Units = MM
    Left = 212
    Top = 176
  end
  object acEvent: TActionList
    Left = 160
    Top = 176
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
  end
end
