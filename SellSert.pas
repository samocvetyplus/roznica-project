unit SellSert;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, FIBDataSet,
  pFIBDataSet, cxGridLevel, cxClasses, cxGridCustomView, cxGrid;

type
  TfmSellSert = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    taSert: TpFIBDataSet;
    dsSert: TDataSource;
    taSertRN: TFIBIntegerField;
    taSertDEPNAME: TFIBStringField;
    taSertSERT_ID: TFIBIntegerField;
    taSertNOMINAL: TFIBIntegerField;
    taSertCOST: TFIBFloatField;
    taSertCLNAME: TFIBStringField;
    taSertSDATE: TFIBDateTimeField;
    taSertCOLOR: TFIBIntegerField;
    taSertSTATE: TFIBStringField;
    cxGrid1DBTableView1RN: TcxGridDBColumn;
    cxGrid1DBTableView1DEPNAME: TcxGridDBColumn;
    cxGrid1DBTableView1SERT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1NOMINAL: TcxGridDBColumn;
    cxGrid1DBTableView1COST: TcxGridDBColumn;
    cxGrid1DBTableView1CLNAME: TcxGridDBColumn;
    cxGrid1DBTableView1SDATE: TcxGridDBColumn;
    cxGrid1DBTableView1COLOR: TcxGridDBColumn;
    cxGrid1DBTableView1STATE: TcxGridDBColumn;
    procedure taSertBeforeOpen(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1DEPNAMECustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSellSert: TfmSellSert;

implementation
 uses comdata, data, period;
{$R *.dfm}


procedure TfmSellSert.cxGrid1DBTableView1DEPNAMECustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
   var  columnId: integer;
       Cellvalue: integer;
begin
  columnId:=cxGrid1DBTableView1.GetColumnByFieldName('COLOR').Index;
   CellValue:=AviewInfo.GridRecord.Values[ColumnId];
   if (cellvalue<>null) then
    begin
      if (CellValue<>-1677721) then ACanvas.Brush.Color:=CellValue;
    end;
end;

procedure TfmSellSert.FormCreate(Sender: TObject);
begin
 taSert.Active:=true;
taSert.Open;
end;

procedure TfmSellSert.FormDestroy(Sender: TObject);
begin
taSert.Close;
taSert.Active:=false;
end;

procedure TfmSellSert.taSertBeforeOpen(DataSet: TDataSet);
begin
  taSert.ParamByName('BD').AsTimeStamp :=DateTimeToTimeStamp(dm.BeginDate);
  taSert.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
end;

end.
