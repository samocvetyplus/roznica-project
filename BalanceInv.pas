unit BalanceInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGridEh, FIBDataSet, pFIBDataSet, ExtCtrls,
  DBGridEhGrouping, GridsEh;

type
  TfmBalanceInv = class(TForm)
    pnlBalancInv: TPanel;
    taPayInv: TpFIBDataSet;
    FIBDateTimeField2: TFIBDateTimeField;
    FIBFloatField2: TFIBFloatField;
    dsrPayInv: TDataSource;
    taRInv: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBDateTimeField1: TFIBDateTimeField;
    FIBFloatField1: TFIBFloatField;
    taRInvTR: TFIBFloatField;
    taRInvTOTCOST: TFloatField;
    dsrRInv: TDataSource;
    taSInv: TpFIBDataSet;
    taSInvSN: TFIBIntegerField;
    taSInvSDATE: TFIBDateTimeField;
    taSInvCOST: TFIBFloatField;
    taSInvTR: TFIBFloatField;
    taSInvTotCost: TFloatField;
    dsrSInv: TDataSource;
    taPayInvTOTCOST: TFloatField;
    gr: TDBGridEh;
    taSInvDISCRIPTION: TFIBStringField;
    procedure taSInvCalcFields(DataSet: TDataSet);
    procedure taRInvCalcFields(DataSet: TDataSet);
    procedure taPayInvBeforeOpen(DataSet: TDataSet);
    procedure taPayInvCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmBalanceInv: TfmBalanceInv;

implementation

{$R *.dfm}
uses ComData, Payment, dbUtil, Data;

procedure TfmBalanceInv.taSInvCalcFields(DataSet: TDataSet);
begin
  taSInv.FieldByName('TOTCOST').AsFloat := taSInv.FieldByName('COST').AsFloat +
                                           taSInv.FieldByName('TR').AsFloat;
end;

procedure TfmBalanceInv.taRInvCalcFields(DataSet: TDataSet);
begin
  taRInv.FieldByName('TOTCOST').AsFloat := taRInv.FieldByName('COST').AsFloat +
                                           taRInv.FieldByName('TR').AsFloat;
end;

procedure TfmBalanceInv.taPayInvBeforeOpen(DataSet: TDataSet);
begin
  with DataSet as TpFIBDataSet do
  begin
    ParamByName('BD').AsTimeStamp  := DateTimeToTimeStamp(dm.BeginDate);
    ParamByName('ED').AsTimeStamp  := DateTimeToTimeStamp(dm.EndDate);
    ParamByName('COMPID').AsInteger := fmPayment.taBalanceCOMPID.AsInteger;
  end
end;

procedure TfmBalanceInv.taPayInvCalcFields(DataSet: TDataSet);
begin
  taPayInv.FieldByName('TOTCOST').AsFloat := taPayInv.FieldByName('COST').AsFloat;
end;

end.
