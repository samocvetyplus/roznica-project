object fmClient: TfmClient
  Left = 111
  Top = 208
  Width = 961
  Height = 562
  HelpContext = 100110
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 514
    Width = 953
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 953
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 68
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
      ImageIndex = 2
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object sisellitem: TSpeedItem
      Action = acSellItem
      BtnCaption = #1055#1088#1086#1076'.'#13#10#1080#1079#1076#1077#1083#1080#1103' [F2]'
      Caption = 'sisellitem'
      Spacing = 1
      Left = 70
      Top = 2
      Visible = True
      OnClick = acSellItemExecute
      SectionName = 'Untitled (0)'
    end
    object siCalSum: TSpeedItem
      Action = acCalcSum
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090#13#10#1089#1091#1084#1084' [F3]'
      Caption = 'siCalSum'
      Hint = #1087#1077#1088#1077#1089#1095#1077#1090' '#1082#1086#1083'-'#1074#1072' '#1080' '#1089#1091#1084#1084' '#1087#1088#1086#1076#1072#1085#1085#1099#1093' '#1080' '#1074#1086#1079#1074#1088#1072#1097#1077#1085#1085#1080#1093' '#1080#1079#1076#1077#1083#1080#1081' '
      Spacing = 1
      Left = 138
      Top = 2
      OnClick = acCalcSumExecute
      SectionName = 'Untitled (0)'
    end
    object siVibNodcard: TSpeedItem
      Action = acVibcard
      BtnCaption = #1047#1072#1084#1077#1085#1080#1090#1100' '#13#10#1082#1072#1088#1090#1091' [F4]'
      Caption = 'siVibNodcard'
      Spacing = 1
      Left = 206
      Top = 2
      Visible = True
      OnClick = acVibcardExecute
      SectionName = 'Untitled (0)'
    end
    object siLose: TSpeedItem
      Action = acLose
      BtnCaption = #1050#1072#1088#1090#1072' '#13#10#1091#1090#1077#1088#1103#1085#1072' [F5]'
      Caption = 'siLose'
      Spacing = 1
      Left = 274
      Top = 2
      Visible = True
      OnClick = acLoseExecute
      SectionName = 'Untitled (0)'
    end
    object siOvereating: TSpeedItem
      Action = acOvereating
      BtnCaption = #1054#1073#1098#1077#1076'. '#1089'... '#13#10'[F7]'
      Caption = 'siOvereating'
      Spacing = 1
      Left = 342
      Top = 2
      Visible = True
      OnClick = acOvereatingExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1080#1089#1100#1084#1072
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppLetter
      Spacing = 1
      Left = 410
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 801
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 495
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 99
    Width = 953
    Height = 415
    Align = alClient
    AllowedOperations = [alopUpdateEh, alopDeleteEh]
    Color = cl3DLight
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmCom.dsClient
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = dg1DblClick
    OnGetCellParams = dg1GetCellParams
    OnKeyDown = dg1KeyDown
    OnKeyPress = dg1KeyPress
    OnKeyUp = dg1KeyUp
    OnMouseUp = dg1MouseUp
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'NODCARD'
        Footers = <>
        Title.Caption = #8470#1082#1072#1088#1090#1099
        Width = 126
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Title.Caption = #1060#1048#1054
        Width = 219
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ADDRESS'
        Footers = <>
        Title.Caption = #1040#1076#1088#1077#1089
        Width = 201
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'HOME_FLAT'
        Footers = <>
        Title.Caption = #1044#1086#1084'/'#1050#1074'.'
        Width = 108
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1043#1086#1088#1086#1076
        Width = 127
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'BIRTHDAY'
        Footers = <>
        Title.Caption = #1044#1077#1085#1100' '#1088#1086#1078#1076#1077#1085#1080#1103
        Width = 107
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'F1'
        Footers = <>
        Title.Caption = #1060#1080#1083#1080#1072#1083
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'R_INSERTD'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'QRET'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090
      end
      item
        EditButtons = <>
        FieldName = 'QSELL'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1050#1086#1083'-'#1074#1086'|'#1055#1088#1086#1076#1072#1078#1072
      end
      item
        EditButtons = <>
        FieldName = 'CRET'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1091#1084#1084#1072'|'#1042#1086#1079#1074#1088#1072#1090
      end
      item
        EditButtons = <>
        FieldName = 'CSELL'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1091#1084#1084#1072'|'#1055#1088#1086#1076#1072#1078#1072
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1091#1084#1084#1072'|'#1054#1073#1097#1072#1103
      end
      item
        Alignment = taCenter
        Checkboxes = True
        EditButtons = <>
        FieldName = 'ISLETTER'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1055#1077#1095#1072#1090#1100' '#1087#1080#1089#1100#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'OLDNODCARD'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1077#1076'. '#1079#1085#1072#1095#1077#1085#1080#1077' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      end
      item
        Alignment = taCenter
        Checkboxes = True
        EditButtons = <>
        FieldName = 'F2'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = '*'
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'F3'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = '**'
      end
      item
        EditButtons = <>
        FieldName = 'UPDATEDATE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1044#1072#1090#1072' '#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1103
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object lbaddress: TListBox
    Left = 560
    Top = 144
    Width = 201
    Height = 104
    BevelInner = bvLowered
    BorderStyle = bsNone
    Color = clInfoBk
    ItemHeight = 13
    Items.Strings = (
      '')
    TabOrder = 3
    Visible = False
    OnDblClick = lbaddressDblClick
    OnKeyDown = lbaddressKeyDown
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 41
    Width = 953
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 150
    BtnHeight = 25
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 4
    InternalVer = 1
    object Label1: TLabel
      Left = 31
      Top = 8
      Width = 98
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      Transparent = True
    end
    object Label2: TLabel
      Left = 279
      Top = 8
      Width = 27
      Height = 13
      Caption = #1060#1048#1054
      Transparent = True
    end
    object Label3: TLabel
      Left = 528
      Top = 7
      Width = 91
      Height = 13
      Caption = #1059#1090#1077#1088#1103#1085#1085#1099#1077' '#1082#1072#1088#1090#1099
      Transparent = True
    end
    object edNodcard: TEdit
      Left = 143
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edNodcardKeyDown
      OnKeyPress = edNodcardKeyPress
      OnKeyUp = edNodcardKeyUp
    end
    object edfio: TEdit
      Left = 312
      Top = 4
      Width = 161
      Height = 21
      TabOrder = 1
      OnKeyDown = edfioKeyDown
      OnKeyPress = edfioKeyPress
    end
    object chblosecard: TCheckBox
      Left = 513
      Top = 8
      Width = 12
      Height = 12
      TabOrder = 2
      OnClick = chblosecardClick
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object SpeedBar2: TSpeedBar
    Left = 0
    Top = 70
    Width = 953
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 200
    BtnHeight = 25
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 5
    InternalVer = 1
    object Ldate: TLabel
      Left = 616
      Top = 8
      Width = 3
      Height = 13
      Transparent = True
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object sifmain: TSpeedItem
      BtnCaption = #1042#1089#1077' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      Caption = 'sifmain'
      DropDownMenu = pmfmain
      Hint = 'sifmain|'
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object sidep: TSpeedItem
      BtnCaption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Caption = 'sidep'
      DropDownMenu = pmdep
      Hint = 'sidep|'
      Spacing = 1
      Left = 202
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object sidate: TSpeedItem
      BtnCaption = #1042#1089#1077' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      Caption = 'sidate'
      DropDownMenu = pmdate
      Hint = 'sidate|'
      Spacing = 1
      Left = 402
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object ActionList1: TActionList
    Left = 16
    Top = 296
    object acNewClient: TAction
      Caption = 'acNewClient'
      ShortCut = 45
      OnExecute = SpeedItem2Click
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 16430
      OnExecute = acDelExecute
    end
    object acPrintLetter: TAction
      Caption = #1053#1072#1087#1077#1095#1072#1090#1072#1090#1100
      ShortCut = 16460
      OnExecute = acPrintLetterExecute
    end
    object acUncheckLetter: TAction
      Caption = #1057#1085#1103#1090#1100' '#1087#1086#1084#1077#1090#1082#1080
      OnExecute = acUncheckLetterExecute
    end
    object acSellItem: TAction
      Caption = #1055#1088#1086#1076'. '#1080#1079#1076#1077#1083#1080#1103
      ShortCut = 113
      OnExecute = acSellItemExecute
    end
    object acCalcSum: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1091#1084#1084' [F3]'
      Hint = #1087#1077#1088#1077#1089#1095#1077#1090' '#1082#1086#1083'-'#1074#1072' '#1080' '#1089#1091#1084#1084' '#1087#1088#1086#1076#1072#1085#1085#1099#1093' '#1080' '#1074#1086#1079#1074#1088#1072#1097#1077#1085#1085#1080#1093' '#1080#1079#1076#1077#1083#1080#1081' '
      ShortCut = 114
      OnExecute = acCalcSumExecute
    end
    object acVibcard: TAction
      Caption = #1047#1072#1084#1077#1085#1072' '#1082#1072#1088#1090#1099' [F4]'
      ShortCut = 115
      OnExecute = acVibcardExecute
      OnUpdate = acVibcardUpdate
    end
    object acLose: TAction
      Caption = #1050#1072#1088#1090#1072' '#1091#1090#1077#1088#1103#1085#1072' [F5]'
      ShortCut = 116
      OnExecute = acLoseExecute
      OnUpdate = acLoseUpdate
    end
    object acOvereating: TAction
      Caption = #1054#1073#1098#1077#1076'. '#1089'... [F7]'
      ShortCut = 118
      OnExecute = acOvereatingExecute
      OnUpdate = acOvereatingUpdate
    end
  end
  object pmfmain: TPopupMenu
    Left = 16
    Top = 120
    object nallfmain: TMenuItem
      Caption = #1042#1089#1077' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      OnClick = nallfmainClick
    end
    object Nfmain: TMenuItem
      Tag = 1
      Caption = #1044#1080#1089#1082'. '#1082#1072#1088#1090#1099' '#1092#1080#1083#1080#1072#1083#1072
      OnClick = nallfmainClick
    end
    object Nnotfmain: TMenuItem
      Tag = 2
      Caption = #1044#1080#1089#1082'. '#1082#1072#1088#1090#1099' '#1076#1088'. '#1092#1080#1083#1080#1072#1083#1072
      OnClick = nallfmainClick
    end
  end
  object pmdep: TPopupMenu
    Left = 16
    Top = 176
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 16
    Top = 352
  end
  object pmdate: TPopupMenu
    Left = 16
    Top = 240
    object Nall: TMenuItem
      Caption = #1042#1089#1077' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
      OnClick = NallClick
    end
    object NDateUpdate: TMenuItem
      Caption = #1044#1080#1089#1082'. '#1082#1072#1088#1090#1099' '#1086#1090#1088#1077#1076#1072#1082'. '#1079#1072' '#1087#1077#1088#1080#1086#1076
      OnClick = NDateUpdateClick
    end
    object Ndate: TMenuItem
      Caption = #1044#1080#1089#1082'. '#1082#1072#1088#1090#1099' '#1089#1086#1079#1076#1072#1085#1085#1099#1077' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      OnClick = NdateClick
    end
  end
  object fr: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 400
    Top = 176
  end
  object ppLetter: TPopupMenu
    Left = 80
    Top = 240
    object acPrintLetter1: TMenuItem
      Action = acPrintLetter
    end
    object N1: TMenuItem
      Action = acUncheckLetter
    end
  end
end
