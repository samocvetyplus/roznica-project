unit SInv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, 
  M207IBGrid, StdCtrls, Mask, RxToolEdit, DBCtrls, Buttons, db,
  ArtNav, ArtGrid, Menus, RXSpin, RxMenus, Variants,
  DBCtrlsEh, DateUtils, ActnList, M207Ctrls, M207DBCtrls, FIBDataSet,
  pFIBDataSet, jpeg, rxPlacemnt, rxSpeedbar, DBGridEh, DBLookupEh, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinsDefaultPainters, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxButtons, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxDBExtLookupComboBox, DBClient, dxmdaset,
  cxCheckBox, cxDBEdit, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinscxPCPainter;

const WM_POSTA2=WM_USER+1;

type
  TfmSInv = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    deNDate: TDBDateEdit;
    Label3: TLabel;
    edSSF: TM207DBEdit;
    Label4: TLabel;
    edSN: TM207DBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    Splitter2: TSplitter;
    siIdNum: TSpeedItem;
    edTr: TM207DBEdit;
    pmVisCol: TPopupMenu;
    miVisCol: TMenuItem;
    fs1: TM207FormStorage;
    paDict: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    dgArt: TM207IBGrid;
    plFilter: TPanel;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter6: TSplitter;
    pmEl: TPopupMenu;
    miSElItem: TMenuItem;
    pmA2: TPopupMenu;
    miIns: TMenuItem;
    siIns: TSpeedItem;
    siCloseInv: TSpeedItem;
    DBText3: TDBText;
    N1: TMenuItem;
    siFind: TSpeedItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Label7: TLabel;
    lcNDS: TDBLookupComboBox;
    edNDS: TM207DBEdit;
    siPrice: TSpeedItem;
    N5: TMenuItem;
    plArt2: TPanel;
    pmA: TPopupMenu;
    miAddArt: TMenuItem;
    Label15: TLabel;
    lcPayType: TDBLookupComboBox;
    N10: TMenuItem;
    N11: TMenuItem;
    Label16: TLabel;
    Label17: TLabel;
    DBText2: TDBText;
    DBText5: TDBText;
    edPTr: TM207DBEdit;
    siRecalc: TSpeedItem;
    Label14: TLabel;
    DBEdit1: TM207DBEdit;
    edTrans: TM207DBEdit;
    miInsFromSearch: TMenuItem;
    spitPrint: TSpeedItem;
    ppPrint: TRxPopupMenu;
    miAEdit: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    Panel1: TPanel;
    dgA2: TM207IBGrid;
    Panel2: TPanel;
    N12: TMenuItem;
    N21: TMenuItem;
    Label18: TLabel;
    Label19: TLabel;
    edPTrNDS: TM207DBEdit;
    dgEl: TM207IBGrid;
    N22: TMenuItem;
    N23: TMenuItem;
    DArtId1: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N9: TMenuItem;
    SpeedItem2: TSpeedItem;
    pmOpt: TPopupMenu;
    N31: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    Art2Id1: TMenuItem;
    N38: TMenuItem;
    SElId1: TMenuItem;
    tb2: TSpeedBar;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N110: TMenuItem;
    N41: TMenuItem;
    N42: TMenuItem;
    Label8: TLabel;
    dbSup: TDBText;
    lbCountry: TListBox;
    N7: TMenuItem;
    N8: TMenuItem;
    nHistart: TMenuItem;
    N43: TMenuItem;
    DBText7: TDBText;
    Label13: TLabel;
    chbxNoCreatePr: TDBCheckBox;
    DBText8: TDBText;
    DBText9: TDBText;
    Label20: TLabel;
    Label21: TLabel;
    Splitter9: TSplitter;
    Splitter3: TSplitter;
    Splitter7: TSplitter;
    Splitter10: TSplitter;
    lbAtt1: TListBox;
    lbAtt2: TListBox;
    Splitter11: TSplitter;
    plFilter2: TPanel;
    Label22: TLabel;
    cmbxComp: TDBComboBoxEh;
    cmbxCountry: TDBComboBoxEh;
    cmbxIns: TDBComboBoxEh;
    cmbxGood: TDBComboBoxEh;
    cmbxAtt1: TDBComboBoxEh;
    cmbxMat: TDBComboBoxEh;
    cmbxAtt2: TDBComboBoxEh;
    lbFilterCountry: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    acEvent: TActionList;
    acDictAtt1: TAction;
    acDictAtt2: TAction;
    N111: TMenuItem;
    N210: TMenuItem;
    chbxNoRecalcTR: TDBCheckBox;
    acidNum: TAction;
    dbedSadte: TDBEditEh;
    btdate: TBitBtn;
    acShowArtId: TAction;
    siHelp: TSpeedItem;
    siMergeComplex: TSpeedItem;
    acBegEndMerge: TAction;
    cbOwner: TDBCheckBoxEh;
    lOpenInv: TLabel;
    SpeedItem3: TSpeedItem;
    dsArtUpdate: TpFIBDataSet;
    dsArtUpdatePRICE_NEW: TFIBFloatField;
    dsArtUpdatePRICE_CURR: TFIBFloatField;
    dsArtUpdateART2ID: TFIBIntegerField;
    dsArtUpdateSINVID: TFIBIntegerField;
    DataSource1: TDataSource;
    acOpen: TAction;
    acClose: TAction;
    taHist: TpFIBDataSet;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    dsrDlist_H: TDataSource;
    acCloseWithAct: TAction;
    Splitter4: TSplitter;
    lcSup: TcxDBLookupComboBox;
    edMargin: TEdit;
    Action1: TAction;
    acSetMargin: TAction;
    btnSetMargin: TcxButton;
    GroupBoxTolling: TGroupBox;
    edTotalLossesWeight: TDBEdit;
    edBuhPrice585: TDBEdit;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    edTotalInsertionsWeight: TDBEdit;
    edTotalInvoiceWeight: TDBEdit;
    Label32: TLabel;
    cxButton1: TcxButton;
    chbxAddNDS: TDBCheckBox;
    Label33: TLabel;
    dsContract: TDataSource;
    taContract: TpFIBDataSet;
    taContractID: TFIBIntegerField;
    taContractCLASSID: TFIBIntegerField;
    taContractNAME: TFIBStringField;
    taContractNAMEFORMATED: TFIBStringField;
    taContractCONTRACTDATE: TFIBDateTimeField;
    taContractNUMBER: TFIBStringField;
    lcContract: TcxDBExtLookupComboBox;
    GridContractView: TcxGridDBTableView;
    GridContractLevel: TcxGridLevel;
    GridContract: TcxGrid;
    GridContractViewNUMBER: TcxGridDBColumn;
    GridContractViewCONTRACTDATE: TcxGridDBColumn;
    GridContractViewNAME: TcxGridDBColumn;
    GridContractViewNAMEFORMATED: TcxGridDBColumn;
    taContractCONTRACTENDDATE: TFIBDateTimeField;
    taContractISUNLIMITED: TFIBSmallIntField;
    Label34: TLabel;
    edPureK: TDBComboBox;
    DBEdit2: TDBEdit;
    Label35: TLabel;
    cxDBCheckBox1: TcxDBCheckBox;
    lbMaterial: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure miVisColClick(Sender: TObject);
    procedure dgArtMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbCompClick(Sender: TObject);
    procedure dgElGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dgArtGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure miInsClick(Sender: TObject);
    procedure miAddClick(Sender: TObject);
    procedure dgElEditButtonClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siCloseInvClick(Sender: TObject);
    procedure edTrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siFindClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure dgArtEditButtonClick(Sender: TObject);
    procedure siPriceClick(Sender: TObject);
    procedure sePTrKeyPress(Sender: TObject; var Key: Char);
    procedure dgA2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure miAddArtClick(Sender: TObject);
    procedure dgA2EditButtonClick(Sender: TObject);
    procedure lcNDSCloseUp(Sender: TObject);
    procedure dgA2DblClick(Sender: TObject);
    procedure dgA2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dgA2KeyPress(Sender: TObject; var Key: Char);
    procedure dgArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgElKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgA2Enter(Sender: TObject);
    procedure dgArtEnter(Sender: TObject);
    procedure edPTrKeyPress(Sender: TObject; var Key: Char);
    procedure siRecalcClick(Sender: TObject);
    procedure miInsFromSearchClick(Sender: TObject);
    procedure mnitCurClick(Sender: TObject);
    procedure miAEditClick(Sender: TObject);
    procedure pmMargPopup(Sender: TObject);
//    procedure lcSup_oldCloseUp(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure edTransEnter(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure dgArtExit(Sender: TObject);
    procedure N19Click(Sender: TObject);
    function dgA2GetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;
    function dgArtGetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;
    procedure N20Click(Sender: TObject);
    procedure dgArtDblClick(Sender: TObject);
    procedure dgArtShowEditor(Sender: TObject; Field: TField;
      var AllowEdit: Boolean);
    procedure N21Click(Sender: TObject);
    procedure dgA2ColExit(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure N27Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure N32Click(Sender: TObject);
    procedure Art2Id1Click(Sender: TObject);
    procedure SElId1Click(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtEnter(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
    procedure cbSearchClick(Sender: TObject);
    procedure N39Click(Sender: TObject);
    procedure N40Click(Sender: TObject);
    procedure N110Click(Sender: TObject);
    procedure N41Click(Sender: TObject);
    procedure dgArtCellClick(Column: TColumn);
    procedure ceMarginButtonClick(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure nHistartClick(Sender: TObject);
    procedure dgElColExit(Sender: TObject);
    procedure dgElColEnter(Sender: TObject);
    procedure ceArtClick(Sender: TObject);
    procedure dgA2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbGoodMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbInsMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cbSearchMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edSNMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edSSFClick(Sender: TObject);
    procedure deNDateClick(Sender: TObject);
    procedure ceMarginKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lcSup_oldClick(Sender: TObject);
    procedure lcNDSClick(Sender: TObject);
    procedure lcPayTypeClick(Sender: TObject);
    procedure edTrClick(Sender: TObject);
    procedure edNDSClick(Sender: TObject);
    procedure edPTrNDSClick(Sender: TObject);
    procedure DBEdit1Click(Sender: TObject);
    procedure edPTrMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure acDictAtt1Execute(Sender: TObject);
    procedure acDictAtt2Execute(Sender: TObject);
    procedure acidNumExecute(Sender: TObject);
    procedure acidNumUpdate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dgA2Exit(Sender: TObject);
    procedure btdateClick(Sender: TObject);
    procedure acShowArtIdUpdate(Sender: TObject);
    procedure acShowArtIdExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure acBegEndMergeExecute(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure dsArtUpdateBeforeOpen(DataSet: TDataSet);
    procedure acOpenExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acCloseWithActExecute(Sender: TObject);
    procedure lcSupCloseUp(Sender: TObject);
    procedure acSetMarginExecute(Sender: TObject);
    procedure acSetMarginUpdate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure taContractBeforeOpen(DataSet: TDataSet);
    procedure taContractAfterOpen(DataSet: TDataSet);
    procedure taContractCalcFields(DataSet: TDataSet);
    procedure lcPayTypeCloseUp(Sender: TObject);
    procedure edPureKChange(Sender: TObject);
  private
    { Private declarations }
    s_close, s_closed:string;
    SearchEnable: boolean;
    StopFlag: boolean;
    IsPriceChng: integer;
    LogOprIdForm: string;
    Curr_ID:Variant;
    StoredSupplierID: Integer;
//    NDSState: TCheckBoxState;
    FhintComp, FhintCountry, FhintGood, FhintIns, FhintMat, FhintAtt1, FhintAtt2 : THintWindow;
//    Timer: TTimer;
//    StringFilterSup : AnsiString;
    DocumentID: Integer;
    Commission: Integer;
    procedure GetCommission;
    procedure SetCommission;
//    procedure ClearSupFilter(Sender: TObject);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure SetMargin;
    procedure ExecQ(q: string);
    procedure WMPostA2(var Msg: TMessage);message WM_POSTA2;
    procedure FindProd(ProdId: integer);
    procedure CMDialogKey(var Message: TCMDialogKey); message CM_DIALOGKEY;
    procedure WMActivate(var Message: TWMActivate); message wm_Activate;
    function  Stop: boolean;
    procedure CheckNAct;
    procedure MaterialInitialize;
  public
//    procedure OnScanCode(ScanCode: string);
    { Public declarations }
  end;

var
  fmSInv: TfmSInv;
  WMode: string[10];
  IsCheckAct, NoPrord: integer;
  LogOperationID: string;
  sinvid:integer;

implementation

uses comdata, Data, DBTree, StrUtils, SItem, VisCol, Ins,
  SElFind, DepMarg, SelA2, PrOrdLst, SetPrice, ARest, A2Rest, Data2,
  ReportData, AEdit, Dst, SPEdit, A2PHist, DIns, Good, Mat, Comp, M207Proc,
  InvMarg, Country, HistItem, dAtt1, dAtt2, Data3, dbUtil, Types, SetSDate, MsgDialog,
  FIBQuery, SList, pFIBQuery, uScanCode, Hist, uDialog;

{$R *.DFM}

procedure TfmSInv.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
    else inherited;
end;

procedure TfmSInv.MaterialInitialize;
begin
  case edPureK.ItemIndex of
    0,1: lbMaterial.Caption := '������';
    2,3: lbMaterial.Caption := '�������';
  end;
end;


procedure TfmSInv.FormCreate(Sender: TObject);
var
  i : integer;
  Res, Curr_text: Variant;

  begin

  StoredSupplierID := dm.taSListSUPID.AsInteger;

  taContract.Active := True;

  FhintComp := nil; FhintCountry := nil; FhintGood := nil; FhintIns := nil; FhintMat := nil;
  FhintAtt1 := nil; FhintAtt2 := nil;
  tb1.WallPaper:=wp;

  s_close:='';
  s_closed:='';

  with dm, dmCom, dm.quTmp do
  begin
     Curr_text:=ExecSelectSQL('select first 1 sinvid from curr_user_inv where sinvid='+ dm.taSListSINVID.AsString + 'and Curr_UserId != '+IntToStr(dmCom.UserId), quTmp);
     if VarIsNull(Curr_text) then
        lOpenInv.Visible:=False else lOpenInv.Caption:='��������� ������� ������ �������������!';

     close;
     Curr_ID := ExecSelectSQL('select gen_id(gen_curr_id,1) ID from rdb$database', dm.quTmp);
     SQL.Text := 'insert into curr_user_inv (id_curr, sinvid, curr_userid)'+
                 'values ('+ IntToStr(Curr_ID)+','+ dm.taSListSInvId.AsString+','+IntToStr(dmCom.UserId)+')';
     ExecQuery;
     Transaction.CommitRetaining;
     close;

     Art:='';
     AllA2:=False;
     Old_D_MatId:='.';
     Old_D_MatId:='*';

     PostDataSet(taSList);
     taSList.Refresh;


     if taSListIsClosed.AsInteger = 1 then MessageDialog('��������� �������', mtInformation, [mbOK], 0);

     CloseDataSets([taSel, quSup]);

     taSEl.SelectSQL[4] := 'ORDER BY SELID';

     quSup.SelectSQL[2] := 'WHERE SELLER=1 and d_compid<>-1 and workorg=1';

     OpenDataSets([quSup, quDep, taNDS, taSEl, taPayType, taIns, taA2, quMaxArtPrice, quInOutPrice]);

     if taSListISIMPORT.AsInteger = 1 then
        for i:=0 to Pred(dgEl.Columns.Count) do
            if dgEl.Columns[i].FieldName = 'EX_PRICE2' then dgEl.Columns[i].Visible := True;
     SetCBDropDown(lcSup);
     SetCBDropDown(lcNDS);
     SetCBDropDown(lcPayType);
     D_CompId:=-1;
     D_MatId:='';
     D_GoodId:='';
     D_CountryId:='*';
     dm.SetVisEnabled(TWinControl(dbSup),taSListISCLOSED.AsInteger=1);
     dm.SetVisEnabled(lcSup,taSListISCLOSED.AsInteger <> 1);

  end;

  fmSItem:=TfmSItem.Create(Application);
  SearchEnable:=False;
  ceArt.Text:='';
  SearchEnable:=True;
  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbcountry, lbAtt1, lbAtt2);
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbAtt1.ItemIndex := 0;
  lbAtt2.ItemIndex := 0;
  lbCompClick(NIL);

  // ��������� ���������� �������������� ����������
  dm.FillComboBoxes(cmbxComp, cmbxMat, cmbxGood, cmbxIns, cmbxCountry, cmbxAtt1, cmbxAtt2);
  cmbxComp.ItemIndex := 0;
  cmbxMat.ItemIndex := 0;
  cmbxGood.ItemIndex := 0;
  cmbxIns.ItemIndex := 0;
  cmbxCountry.ItemIndex := 0;
  cmbxAtt1.ItemIndex := 0;
  cmbxAtt2.ItemIndex := 0;

  SetMargin;
  ActiveControl:=edSSF;
  with dm do
  begin
     SetCloseInvBtn(siCloseInv, dm.taSListIsClosed.AsInteger);
     ClosedInvId:=-1;
  end;
  StopFlag:=False;
 {�������� ������ �����}
  dmCom.FillTagMenu(ppPrint, mnitCurClick, 0);
 {*********************}

  Res := execSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                       #39' and Is_Merge_Run = current_connection', dm.quTmp);

  if not VarIsNull(Res) then siMergeComplex.ImageIndex := 10 else
     siMergeComplex.ImageIndex := 53;

  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  GetCommission;

  dgA2.ColumnByName['Tolling$Price'].Visible := dm.taSlistISTOLLING.AsInteger = 1;

  if dm.taSListISTOLLING.AsInteger = 1 then
  begin
    dm.taTollingParams.Open;

    dm.taTollingParams.Edit;

    GroupBoxTolling.Visible := True;

    GroupBoxTolling.Enabled := dm.taSListISCLOSED.AsInteger <> 1;

    MaterialInitialize;
  end;

end;

procedure TfmSInv.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Res : Variant;
begin
  //SetCommission;

  if dm.taTollingParams.State in [dsEdit, dsInsert] then
    dm.taTollingParams.Post;
  dm.taTollingParams.Close;

  with dm, dmCom, dm.quTmp do
  begin
  SInvId:=taSlist.FieldByName('SINVID').AsInteger;
     {��� �������� ������ ��������� �������������, ������� �� ������, ��� ���������.
     ���� ��������� ����������� ��������������� � ������ �������������,
     �� ������ ��� �������� �� ������ �� ��������� (��� �� ��������). }
     if not taSListSINVID.IsNull then // ���� ���-�� ���������, ��� ��� �� �������� �����
     begin
       if not closeinv(taSListSINVID.AsInteger,0) then // ���� ���� ������ � ���� �� �����
       begin
          if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel
          then SysUtils.Abort;
       end;
     end;
     ExecSQL('delete from Curr_User_inv where ID_Curr='+ VarToStr(Curr_ID), qutmp);
     tr.CommitRetaining;

     PostDataSets([taSList, taSEl, quArt, taA2]);
     tr.CommitRetaining;
     ReOpenDataSets([taSEl, taSList]);
     taSList.Refresh;
     //taSlist.ReopenLocate('SINVID');
     
     Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                            #39' and Is_Merge_Run= current_connection',dm.quTmp);

     if (not VarIsNull(Res)) then
     begin
       if MessageDialog('���� ������ �������� �����������. ���������', mtConfirmation, [mbYes, mbCancel], 0)=mrYes then acBegEndMergeExecute(nil);
     end;
     dm.taSList.Refresh;
     //taSlist.Locate('SINVID',SinvId,[]);
     CloseDataSets([quSup, dmCom.quDep, taSEl, taComp, taMat, taGood, quArt, taIns, taA2, taPayType,  quMaxArtPrice, quInOutPrice]);
     quSup.SelectSQL[2]:='WHERE SELLER>=1 and d_compid<>-1 and workorg=1';
     tr.CommitRetaining;
     ReopenDataSets([taSList,quSListT]);
     Art:='';
  end;

  if fmSItem <> nil then
  begin
  // ��� ���???
  fmSItem.Free;
  fmSInv:=NIL;
  end;

  dmcom.ae1.OnActivate := nil;
  dmcom.ae1.OnDeactivate := nil;
  if Assigned(FhintComp) then FhintComp.ReleaseHandle;
  if Assigned(FhintCountry) then FhintCountry.ReleaseHandle;
  if Assigned(FhintGood) then FhintGood.ReleaseHandle;
  if Assigned(FhintIns) then FhintIns.ReleaseHandle;
  if Assigned(FhintMat) then FhintMat.ReleaseHandle;
  if Assigned(FhintAtt1) then FhintAtt1.ReleaseHandle;
  if Assigned(FhintAtt2) then FhintAtt2.ReleaseHandle;
  FhintComp := nil; FhintCountry := nil; FhintGood := nil; FhintIns := nil;
  FhintMat := nil; FhintAtt1 := nil; FhintAtt2 := nil;
  WMode:='';
end;

procedure TfmSInv.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInv.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSInv.siDelClick(Sender: TObject);
var
  LogOperationID: string;
begin
  Screen.Cursor:=crSQLWait;
  {log}
  LogOperationID:=dm3.insert_operation('�������� �������� �� ��������',LogOprIdForm);
  {***}

  dm.taSEl.Delete;

  {log}
  dm3.update_operation(LogOperationID);
  Screen.Cursor:=crDefault;
  {***}
end;

procedure TfmSInv.miVisColClick(Sender: TObject);
begin
  case TMenuItem(Sender).Tag of
      1: SetVisCol(dgEl);
      2: SetVisCol(dgArt);
  end;
end;

procedure TfmSInv.dgArtMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  p: TPoint;
begin
  if (X<12) and (Y<16) and (Button=mbRight) then
  begin
     p:=TM207IBGrid(Sender).ClientToScreen(Point(X,Y));
     if Sender=dgEl then miVisCol.Tag:=1 else miVisCol.Tag:=2;
     pmVisCol.PopUp(P.X,P.Y);
  end;
  ReopenDataSets([dm.quMaxArtPrice]);
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then
   begin
   dm.taSEl.Post;
   end;
end;

procedure TfmSInv.lbCompClick(Sender: TObject);
var
  ScrollBarWidth : longint;
  rect: TRect;
  hinttxt : string;
  lb : TListBox;
  hintWnd : THintWindow;
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
  lb := (Sender as TListBox);
  if not Assigned(lb) then eXit;
  if (lb.Name = 'lbComp') then hintWnd := FhintComp
  else if (lb.Name = 'lbCountry') then hintWnd := FhintCountry
  else if (lb.Name = 'lbGood') then hintWnd := FhintGood
  else if (lb.Name = 'lbIns') then hintWnd := FhintIns
  else if (lb.Name = 'lbMat') then hintWnd := FhintMat
  else if (lb.Name = 'lbAtt1') then hintWnd := FhintAtt1
  else if (lb.Name = 'lbAtt2') then hintWnd := FhintAtt2
  else eXit;
  with lb do
  begin
    if(lb.ItemIndex < 0)then eXit;
    if ((GetWindowLong(lb.Handle, GWL_STYLE) and WS_VSCROLL) <> 0)
    then ScrollBarWidth := GetSystemMetrics(SM_CXVSCROLL)
    else ScrollBarWidth := 0;
    if Canvas.TextWidth(lb.Items[lb.ItemIndex]) > Width - 4 - ScrollBarWidth then HintTxt:=lb.Items[lb.ItemIndex];
    if(HintTxt <> '')then
    begin
      if Assigned(hintWnd) then hintWnd.ReleaseHandle;
      //hintWnd := nil;
      hintWnd := THintWindow.Create(Self);
      if (lb.Name = 'lbComp') then FhintComp := hintWnd
      else if (lb.Name = 'lbCountry') then FhintCountry := hintWnd
      else if (lb.Name = 'lbGood') then FhintGood := hintWnd
      else if (lb.Name = 'lbIns') then FhintIns := hintWnd
      else if (lb.Name = 'lbMat') then FhintMat := hintWnd
      else if (lb.Name = 'lbAtt1') then FhintAtt1 := hintWnd 
      else if (lb.Name = 'lbAtt2') then FhintAtt2 := hintWnd; 
      rect := hintWnd.CalcHintRect( Screen.Width, hinttxt, nil);
      rect.TopLeft := lb.ClientToScreen(ItemRect(ItemIndex).TopLeft);
      rect.BottomRight := lb.ClientToScreen(ItemRect(ItemIndex).BottomRight);
      rect.Right :=  hintWnd.CalcHintRect( Screen.Width, hinttxt, nil).Right+rect.Left;
      hintWnd.ActivateHint(rect, hinttxt);
    end
    else if Assigned(hintWnd) then hintWnd.ReleaseHandle;
  end;
end;

procedure TfmSInv.dgElGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin{
if ((dm.taSElCOLOR.AsInteger=1) and (Field.FieldName='PRICE2')) then
    Background:=clRed else
    begin
       if Field<>NIL then
          if (Field.FieldName='ART') or
             (Field.FieldName='PRODCODE') or
             (Field.FieldName='D_MATID') or
             (Field.FieldName='D_GOODID') or
             (Field.FieldName='D_COUNTRYID') or
             (Field.FieldName='D_INSID') then
                 if dm.taSElDistred.AsInteger=0 then Background:=dmCom.clMoneyGreen
                    else  Background:=clAqua;
    end;
 }
    if ((dm.taSElCOLOR.AsInteger <> 0 ) and (Field.FieldName='PRICE2')) then
    begin
      if ((dm.taSElCOLOR.AsInteger and 1) = 1) then
      begin
        Background := clYellow;
      end;

      if ((dm.taSElCOLOR.AsInteger and 2) = 2) then
      begin
        Background := clRed;
      end;

    end else

    begin
       if Field<>NIL then
          if (Field.FieldName='ART') or
             (Field.FieldName='PRODCODE') or
             (Field.FieldName='D_MATID') or
             (Field.FieldName='D_GOODID') or
             (Field.FieldName='D_COUNTRYID') or
             (Field.FieldName='D_INSID') then
                 if dm.taSElDistred.AsInteger=0 then Background:=dmCom.clMoneyGreen
                    else  Background:=clAqua;
    end;
end;


procedure TfmSInv.dgArtGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field<>NIL then
     if Field.FieldName='FULLART' then
        Background:=dmCom.clMoneyGreen;
end;

procedure TfmSInv.miInsClick(Sender: TObject);
var  LogOperationID: string;
begin
    with dmCom, dm do
    begin
      PostDataSets([quArt, taA2, taSEl]);
      case TComponent(Sender).Tag of
        1 : begin
              if taSElSElId.IsNull then raise Exception.Create('���������� ������ ������ � ���������');
              Art2Id:=taSElArt2Id.AsInteger;
              FullArt:=taSElFullArt.AsString;
              Art2:=taSElArt2.AsString;
            end;
        2 : begin
              if taA2Art2Id.IsNull then raise Exception.Create('���������� ������ �������');
              Art2Id:=taA2Art2Id.AsInteger;
              FullArt:=dm.quArtFULLART.AsString;
              Art2:=taA2Art2.AsString;
            end;
        else eXit;    
      end;
      {log}
      LogOperationID:=dm3.insert_operation('�������',LogOprIdForm);
      {***}
      ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);
      {log}
      dm3.update_operation(LogOperationID);
      {***}
    end;
end;

procedure TfmSInv.miAddClick(Sender: TObject);
var
  SElId: integer;
  CheckRes: integer;
  LogOperationID: string;
begin
  //!!!
  with dmCom, dm, dm2, taSEl do
  begin
     tr.CommitRetaining;
     with quTmp do
     begin
    {������ �� ���������� ������� ���, ���� ��������� ����������� �� ����� ��� ()}
        try
          SQL.Text := 'select IsImport from SInv where SInvID = ' +taSListSInvId.AsString +
                      ' and supid = 311' ;
          ExecQuery;
          CheckRes := Fields[0].AsInteger;
          if CheckRes = 1 then
             raise Exception.Create('� ��������� ������� ��� ������� �� ����� ���� ��������� �������');
        finally
          Close;
        end;
        if dm.quArt.Active=False then
           ShowMessage('�������� ������� ��� ���������� � ��������!') else
           begin
             {log}
             LogOperationID:=dm3.insert_operation('���������� �������� � ��������',LogOprIdForm);
             {***}
             SQL.Text:='select SElId from SEl where Art2Id='+taA2ART2ID.AsString+' and SInvId='+taSListSInvId.AsString;
             ExecQuery;
             SElId:=Fields[0].AsInteger;
             Close;
           end;
      end;
      if SelId<>0 then
      begin
      ReOpenDataSets([taSEl]);
      dm.taSEl. Locate('RecNo', p, []);
      end;
      tr.CommitRetaining;
      if NOT Locate('ART2ID', taA2ART2ID.AsInteger, []) then
      begin
         Append;
         taSElQuantity.AsInteger:=0;
         Post;
      end;
      acidNumExecute(nil);
     {log}
      dm3.update_operation(LogOperationID);
     {***}
  end;
end;


procedure TfmSInv.dgElEditButtonClick(Sender: TObject);
begin
  with dgEl.SelectedField do
    if (FieldName='TOTALWEIGHT') OR (FieldName='QUANTITY') then  acidNumExecute(NIL) else
        if (FieldName='PRICE') or (FieldName='PRICE2') then siPriceClick(NIL);
end;

procedure TfmSInv.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmSInv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Shift=[ssCtrl] then
  begin
      case Key of
          $31: ActiveControl:=edSN;
          $32: ActiveControl:=dgEl;
          $33: ActiveControl:=lbComp;
          $34: ActiveControl:=dgArt;
      end;
  end;
  if Key=VK_F12 then Close;
end;

procedure TfmSInv.CheckNAct;
begin
  with dm, dm.quTmp do
  begin
     try
       close;
       SQL.Text := 'update D_Rec Set  NActDef = 1'; //���������� ��������� ���������
                                                    //������� �������� � �����
       ExecQuery;
       Transaction.CommitRetaining;
       close;
       {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ���������� ��������� ������ �������
        ��� ����� ��� ���������� ��������� ���� ���������� }
       SQL.Text:='select noedit, noedited from Edit_Date_Inv ('+taSListSINVID.AsString+')';
       ExecQuery;
       s_close:=trim(Fields[0].AsString); //��������� �� ����� ���� ������� ������ ��������� ����
       s_closed:=trim(Fields[1].AsString); //��������� �� ����� ���� ������� ����� ��������� ����
       Transaction.CommitRetaining;
       close;
       Screen.Cursor := crDefault;
       if (s_close='') and (s_closed='') then
       begin
          Screen.Cursor := crSQLWait;
          SQL.Text:='SELECT PR FROM CloseInv('+taSListSInvId.AsString+', 1, '+IntToStr(dmCom.UserId)+')';
          ExecQuery;
          IsPriceChng := Fields[0].AsInteger;
          Close;
          Transaction.CommitRetaining;
          Screen.Cursor := crDefault;
       end
         else
       begin
          if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
          if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
       end;
       SQL.Text := 'execute procedure CheckNAct  ' + taSListSInvId.AsString +
                   ' , ' +  IntToStr(dmCom.UserId);
       ExecQuery;
       Close;
       Transaction.CommitRetaining;
     finally
     close;
     Transaction.CommitRetaining;
     SQL.Text := 'update D_Rec Set  NActDef = 0';
     ExecQuery;
     Transaction.CommitRetaining;
     Close;
     end;
  end;
end;

procedure TfmSInv.siCloseInvClick(Sender: TObject);
var
  ArticleList: String;
begin

  If dm.taSListISCLOSED.AsInteger = 1 then
  begin
    ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False);
  end else
  begin
    if cxButton1.Visible = True then
    begin
      if dm.taTollingParams.State in [dsEdit, dsInsert] then dm.taTollingParams.Post;
      dm.taA2.CloseOpen(True);
    end;
    ArticleList := dmCom.db.QueryValue('select Article$List from Check$Supply$Markup(:Invoice$ID)', 0, [dm.taSListSINVID.AsInteger]);

    if ArticleList <> '' then
    begin
      if MessageDialog('��������:'#13#10 +
                        ArticleList +
                        '����� ������� ����� 50%.'#13#10 +
                        '����������?', mtWarning, [mbYes, mbNo], 0) = mrNO then
      begin
        Exit;
      end;
    end;

    if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
    begin

        if dm.taTollingParams.State in [dsEdit, dsInsert] then dm.taTollingParams.Post;
        dm.taA2.CloseOpen(True);
      acClose.Execute;
    end;
  end;

end;


procedure TfmSInv.edTrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then siRecalcClick(NIL);
end;

procedure TfmSInv.siFindClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmSElFind, Self, TForm(fmSElFind), True, False);
end;

procedure TfmSInv.N4Click(Sender: TObject);
var
  i: integer;
  LogOperationID: string;
begin
 {log}
  LogOperationID:=dm3.insert_operation('�������� ����� �������',LogOprIdForm);
 {***}

  dm.taA2.Insert;
  with dgA2 do
  begin
     i:=0;
     while Columns[i].Field.FieldName<>'ART2' do Inc(i);
     SelectedIndex:=i;
  end;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.dgArtEditButtonClick(Sender: TObject);
begin
  with dm do
    D_ARTID:=quArtD_ArtId.AsInteger;
  ShowAndFreeForm(TfmARest, Self, TForm(fmARest), True, False);
end;

procedure TfmSInv.siPriceClick(Sender: TObject);
var
  LogOperationID: string;
begin
  with dm, dmCom, dm2 do
  begin
     if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
     PostDataSets([taSList, taSEl, {taArt,} taA2]);
     if ActiveControl=dgEl then
     begin
        if NOT taSElSElId.IsNull then
        begin
           {log}
           LogOperationID:=dm3.insert_operation('����',LogOprIdForm);
           {***}
           try
              fmSetPrice:=TfmSetPrice.Create(NIL);
              fmSetPrice.Caption:='��������� ��� - '+taSElFullArt.AsString+' '+taSElArt2.AsString;
              fmSetPrice.sb1.SimpleText:='��������� ����: '+dm.taSElPrice.DisplayText;
              with taSetPrice do
              begin
                  Active:=False;
                  ParamByName('ART2ID').AsInteger:=taSElArt2Id.AsInteger;
                  ParamByName('INVID').AsInteger:=taSListSInvId.AsInteger;
                  Open;
              end;
              Art2Id:=taSElArt2Id.AsInteger;
              UseMargin:=taSElUseMargin.AsInteger=1;
              fmSetPrice.ShowModal;
              RefreshAllSEl(taSElArt2Id.AsInteger);
              with taA2 do
                if Locate('ART2ID', taSElArt2Id.AsInteger, []) then taA2.Refresh;
              SetMargin;
           finally
              fmSetPrice.Free;
           end;
           {log}
           dm3.update_operation(LogOperationID);
           {***}
        end
     end else
         if ActiveControl=dgA2 then
            if NOT taA2Art2Id.IsNull then
            try
              fmSetPrice:=TfmSetPrice.Create(NIL);
              fmSetPrice.Caption:='��������� ��� - '+quArtFullArt.AsString+' '+taA2Art2.AsString;
              fmSetPrice.sb1.SimpleText:='��������� ����: '+dm.taA2Price1.DisplayText;
              with taSetPrice do
              begin
                  Active:=False;
                  ParamByName('ART2ID').AsInteger:=taA2Art2Id.AsInteger;
                  ParamByName('INVID').AsInteger:=taSListSInvId.AsInteger;
                  Open;
              end;
              Art2Id:=taA2Art2Id.AsInteger;
              UseMargin:=taA2UseMargin.AsInteger=1;                              
              fmSetPrice.ShowModal;
              RefreshAllSEl(taA2Art2Id.AsInteger);
              taA2.Refresh;
              SetMargin;
            finally
              fmSetPrice.Free;
            end;
  end;
end;

procedure TfmSInv.sePTrKeyPress(Sender: TObject; var Key: Char);
var
  e: extended;
begin
  e := 0;
  if Key=#13 then
     with dm, taSList do
     begin
        if not (State in [dsInsert, dsEdit]) then Edit;
        taSListTr.AsFloat:=taSListCost.AsFloat*e/(100+e);
     end;
end;

procedure TfmSInv.SetMargin;
var
  Margin: extended;
  RoundNorm: integer;
  b: boolean;
begin
  b:=dm.GetMargin(dm.taSListDepId.AsInteger, Margin, RoundNorm, dm.taSListSInvId.AsInteger);
//  laMargin.Caption:=FloatToStr(Margin);
  edMargin.Text := FloatToStr(Margin);
  if b then edMargin.Font.Color:=clBlack else edMargin.Font.Color:=clRed;
end;

procedure TfmSInv.dgA2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  l: integer;
begin
  case Key of
     VK_RIGHT:
        with dgA2, InplaceEditor do
        begin
           l:=Length(Text);
           if (l=1) and (Text[1]='-') and
              ((SelStart=0) and (SelLength=l) or (SelStart=l) and (SelLength=0)) then
              if SelectedIndex<Columns.Count-1 then
              begin
                 SelectedIndex:=SelectedIndex+1;
                 PostMessage(Handle, WM_KEYDOWN, VK_RETURN, 0);
              end
         end;
     VK_LEFT:
        if Shift=[ssCtrl] then ActiveControl:=dgArt;
     VK_SPACE:
        if dgA2.SelectedField.FieldName='USEMARGIN' then
           with dm, taA2 do
           begin
               if NOT (State in [dsInsert, dsEdit]) then Edit;
               with taA2UseMargin do
                    AsInteger:=Integer(NOT Boolean(AsInteger));
           end;
     VK_F3:
        miAddClick(NIL);
  end;
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;   
end;

procedure TfmSInv.miAddArtClick(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('�������� �������',LogOprIdForm);
  {***}
  with dm, quArt do
  begin
     Insert;
     quArtArt.AsString:=NewArt;
  end;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.dgA2EditButtonClick(Sender: TObject);
var
  fn: string[32];
begin
  fn:=dgA2.SelectedField.FieldName;
  if (fn='PRICE1') or (fn='PRICE2') then siPriceClick(NIL)
     else if (fn='RQ') or (fn='RW') then
          with dm do
          begin
             ART2ID:=taA2ART2ID.AsInteger;
             ShowAndFreeForm(TfmA2Rest, Self, TForm(fmA2Rest), True, False);
          end;
end;

procedure TfmSInv.lcNDSCloseUp(Sender: TObject);
begin
  with dm do
  begin
     if not(dm.taSList.State in [dsInsert, dsEdit]) then dm.taSList.Edit;
     if VarIsNull(lcNDS.KeyValue) then taSListNDSID.AsVariant := Null else
        taSListNDSID.AsInteger := lcNDS.KeyValue;
     PostDataSets([taSList]);
     with taSEl do
     begin
        Active:=False;
        Open;
     end;
     with quTmp do
     begin
        SQL.Text:='execute procedure RecalcNDS '+taSListSInvId.AsString;
        ExecQuery;
     end;
     taSList.Refresh;
  end;
end;

procedure TfmSInv.dgA2DblClick(Sender: TObject);
begin
  if dgA2.SelectedField.FieldName='W' then miAddClick(NIL);
end;

procedure TfmSInv.dgA2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='PRICE2') then
  begin
     if Highlight then Background:=clNavy else
        if dm.taA2PDIF.AsInteger>1 then Background:=clRed else Background:=clInfoBk
  end;
end;

procedure TfmSInv.dgA2KeyPress(Sender: TObject; var Key: Char);
begin
  if (dgA2.SelectedField.FieldName='W') and (Key in ['0'..'9']) then
  begin
    if dm.taA2.State = dsEdit then
    begin
      dm.taA2W.AsFloat := -1;
    end;

    PostDataSets([dm.taA2]); //!!!
    dm.WChar:=Key;
    miAddClick(NIL);
    PostMessage(Handle, WM_PostA2, 0, 0);
  end
end;

procedure TfmSInv.WMPostA2(var Msg: TMessage);
begin
  PostDataSets([dm.taA2]);
end;

procedure TfmSInv.dgArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) and (dgArt.SelectedField.FieldName='UNITID') then
      with dm, quArt do
      begin
         if NOT (State in [dsInsert, dsEdit]) then Edit;
         with quArtUnitId do
              AsInteger:=Integer(NOT Boolean(AsInteger));
      end;
  if (Key=VK_RETURN) or ((Key=VK_RIGHT) and (Shift=[ssCtrl])) then
  begin
      ActiveControl:=dgA2;
      dgA2.ColumnByName['PRICE1'].Field.FocusControl;
      Key:=0;
      PostMessage(Handle, WM_KEYDOWN, VK_RIGHT, 0);
      PostMessage(Handle, WM_KEYDOWN, VK_LEFT, 0);
  end;
  ReopenDataSets([dm.quMaxArtPrice]);
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;     
end;

procedure TfmSInv.dgElKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) and (dgEl.SelectedField.FieldName='USEMARGIN') then
  with dm, taSEl do
  begin
     if NOT (State in [dsInsert, dsEdit]) then Edit;
     with taSElUseMargin do
          AsInteger:=Integer(NOT Boolean(AsInteger));
  end;
  if (key=VK_LEft)or(key=VK_Right)or(key=VK_UP)or(key=VK_DOWN)or(key=VK_RETURN) then
     if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.ExecQ(q: string);
begin
  with dm, dmCom, quTmp do
  begin
      PostDataSets([taSEl]);
      SQL.Text:=q;
      ExecQuery;
      tr.CommitRetaining;
      with taSEl do
      begin
          Active:=False;
          Open;
      end;
      with taA2 do
      begin
          Active:=False;
          Open;
      end;
  end;
end;

procedure TfmSInv.dgA2Enter(Sender: TObject);
begin
  dgA2.SelectedIndex:=0;
end;

procedure TfmSInv.dgArtEnter(Sender: TObject);
begin
  dgArt.SelectedIndex:=0;
end;

procedure TfmSInv.edPTrKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#13 then
  with dm, taSList do
  begin
     if not (State in [dsInsert, dsEdit]) then Edit;
  end;
end;

procedure TfmSInv.siRecalcClick(Sender: TObject);
var LogOperationID: string;
begin
  with dm do
  begin
     if dm.taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
     {log}
     LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
     {***}
     PostDataSets([taSEl, taSList]);
     with quTmp do
     begin
        SQL.Text:='execute procedure RECALCINV '+taSListSInvId.AsString;
        ExecQuery;
     end;
     taSList.Refresh;
     with taSEl do
     begin
        Active:=False;
        Open;
     end;
     {log}
     dm3.update_operation(LogOperationID);
     {***}
  end;
end;

procedure TfmSInv.miInsFromSearchClick(Sender: TObject);
var
  LogOperationID: string;
begin
 {log}
  LogOperationID:=dm3.insert_operation('�������� ������� �� ������',LogOprIdForm);
 {***}
  with dm, quArt do
  begin
     Append;
     quArtArt.AsString:=ceArt.Text;
  end;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.mnitCurClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  try
    gen_arr(arr, dgEl, dm.taSElSELID);
    PrintTag(arr, s_tag, TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSInv.miAEditClick(Sender: TObject);
var
  LogOperationID: string;
begin
 {log}
  LogOperationID:=dm3.insert_operation('�������������� ������� ��������',LogOprIdForm);
 {***}
  PostDataSets([dm.taA2, dm.quArt]);
  if ShowAndFreeForm(TfmAEdit, Self, TForm(fmAEdit), True, False)=mrOK then
     with dm.taSEl do
     begin
        Active:=False;
        Open;
     end;
  if dm.WorkMode='SINV' then dm.taSList.Refresh;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;



procedure TfmSInv.pmMargPopup(Sender: TObject);
begin
  if dm.taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;



procedure TfmSInv.FindProd(ProdId: integer);
var
  i: integer;
begin
  with dm , quTmp do
  begin
      SQL.Text:='SELECT PRODID FROM D_COMP WHERE D_COMPID='+IntToSTr(ProdId);
      ExecQuery;
      ProdId:=Fields[0].AsInteger;
      Close;
  end;
  with lbComp do
    for i:=0 to Items.Count-1 do
      if TNodeData(Items.Objects[i]).Code=ProdId then
      begin
          ItemIndex:=i;
          lbCompClick(lbComp);
          break;
      end
end;

procedure TfmSInv.N14Click(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('������� ������ �������',LogOprIdForm);
  {***}
  dm.taA2.Delete;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N16Click(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('������� �������', LogOprIdForm);
  {***}
  dm.quArt.Delete;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.edTransEnter(Sender: TObject);
begin
  with edTrans do
  begin
     SelectAll;
  end;
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.N18Click(Sender: TObject);
var
  ArtId1, ArtId2: integer;
  Dup: integer;
  LogOperationID: string;
  Res : Variant;
begin
  ArtId1 := 0;
  if not dm.CheckForUIDWHCalc then raise Exception.Create('����������� �� ��������. ���� �������� ������');

  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                        #39' and Is_Merge_Run = current_connection',dm.quTmp);
  if VarIsNull(Res) then
     ShowMessage('���������� ������ �������� ����������� ��������� (������ � �������������)')
     else if MessageDialog('���������� ��������?', mtConfirmation, [mbOK, mbCancel], 0)=mrOK then
          with dmCom, dm , dm2 do
          begin
            Dup:=0;
            PostDataSets([quArt]);
            try
               quArt.DisableControls;
               if NOT quArt.Locate('F1', 1, [])  then raise Exception.Create('���������� ������� ��� ��������');

               ArtId1:=quArtD_ArtId.AsInteger;

               if NOT quArt.Locate('F2', 1, [])  then raise Exception.Create('���������� ������� ��� ��������');

               {log}
               LogOperationID:=dm3.insert_operation('����������� ������ ���������',LogOprIdForm);
               {***}


              dm2.CheckArtOnFil(quArtD_ArtId.AsInteger);
              Screen.Cursor:=crSQLWait;
              while not quArt.EOF do
              begin
                 if quArtF2.AsInteger=1 then
                 begin
                    dm2.CheckArtOnFil(quArtD_ArtId.AsInteger);
                    ArtId2:=quArtD_ArtId.AsInteger;
                    with quTmp do
                    begin
                      SQL.Text:='SELECT DUP FROM AMERGE('+IntToStr(ArtId1)+', '+
                                                          IntToStr(ArtId2)+', '+
                                                          IntToStr(UserId)+', '+
                                                          #39+' '+#39', '#39+' '+#39')';
                      ExecQuery;
                      Dup:=Fields[0].AsInteger;
                      Close;
                    end;
                 end;
                 quArt.Next;
              end;
              quArt.Active:=False;
              tr.CommitRetaining;
              if Dup<>0 then
                 MessageDialog('� �������� ����������� ��������� ������ �������� ���� ��������', mtWarning, [mbOK], 0);
            finally
            quArt.Active:=True;
            quArt.EnableControls;
            quArt.Locate('D_ARTID', ArtId1, []);
            ReOpenDataSets([taSEl]);
            dm.taSEl. Locate('RecNo', p, []);
            ReOpenDataSets([taA2]);
            Screen.Cursor:=crDefault;

            {log}
            dm3.update_operation(LogOperationID);
            {***}

            end;
         end
end;

procedure TfmSInv.dgArtExit(Sender: TObject);
begin
  PostDataSets([dm.quArt]);
end;

procedure TfmSInv.N19Click(Sender: TObject);
var
  Art2Id1, Art2Id2: integer;
  LogOperationID: string;
  Res : Variant;
begin
  Art2Id1 := 0;
  if not dm.CheckForUIDWHCalc then raise Exception.Create('����������� �� ��������. ���� �������� ������');

  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                        #39' and Is_Merge_Run = current_connection',dm.quTmp);
  if VarIsNull(Res) then
     ShowMessage('���������� ������ �������� ����������� ��������� (������ � �������������)')
     else if MessageDialog('���������� ��������?', mtConfirmation, [mbOK, mbCancel], 0)=mrOK then
          with dmCom, dm , dm2 do
          begin
             PostDataSets([taA2]);
             try
                if not taA2.Locate('F1', 1, []) then raise Exception.Create('���������� ������� ��� ��������');

                Art2Id1:=taA2Art2Id.AsInteger;
                if not taA2.Locate('F2', 1, []) then raise Exception.Create('���������� ������� ��� ��������');

                {log}
                LogOperationID:=dm3.insert_operation('����������� ������ ���������',LogOprIdForm);
                {***}

                dm2.CheckArt2OnFil(taA2Art2Id.AsInteger);

                Screen.Cursor:=crSQLWait;
                with quTmp do
                begin
                   Close;
                   SQL.Text:='EXECUTE PROCEDURE PrOrdOnA2Merge '+IntToStr(UserId);
                   ExecQuery;
                   Transaction.CommitRetaining;
                end;

                while not taA2.EOF do
                begin
                   if taA2F2.AsInteger=1 then
                   begin
                      dm2.CheckArt2OnFil(taA2Art2Id.AsInteger);
                      Art2Id2:=taA2Art2Id.AsInteger;
                      Res := ExecSelectSQL('Select IsNeed from IsMakePrordA2('+IntToStr(Art2Id2) + ')',quTmp);
                      if VarToStr(Res) = '1' then
                      begin
                         with quTmp do
                         begin
                            Close;
                            SQL.Text:='EXECUTE PROCEDURE PrOrdItemA2Merge '+
                            IntToStr(UserId)+', '+IntToStr(Art2Id1)+', '+IntToStr(Art2Id2);
                            ExecQuery;
                            Transaction.CommitRetaining;
                         end;
                      end;
                 end;
                 taA2.Next;
          end;

          Screen.Cursor:=crSQLWait;

          taA2.Locate('F2', 1, []);
          while not taA2.EOF do
          begin
             if taA2F2.AsInteger=1 then
             begin
                Art2Id2:=taA2Art2Id.AsInteger;
                with quTmp do
                begin
                    SQL.Text:='EXECUTE PROCEDURE A2MERGE '+IntToStr(Art2Id1)+', '+
                    IntToStr(Art2Id2)+', '+IntToStr(UserId)+ ', '' '' , '' '', -1000' ;
                    ExecQuery;
                    Transaction.CommitRetaining;
                end;
             end;
             taA2.Next;
          end;
          with quTmp do
          begin
             Close;
             SQL.Text:='EXECUTE PROCEDURE ClosePrOrdA2Merge '+IntToStr(UserId);
             ExecQuery;
             Transaction.CommitRetaining;
          end;

          quInOutPrice.Active := False;
          taA2.Active:=False;
          tr.CommitRetaining;
        finally
          taA2.Active:=True;
          quInOutPrice.Active := True;
          taA2.Locate('ART2ID', Art2Id1, []);
          with quTmp do
          begin
              SQL.Text:='DELETE FROM TMPDATA WHERE DATATYPE=1 and USERID='+IntToSTr(UserId);
              ExecQuery;
          end;

          ReOpenDataSets([taSEl]);
          dm.taSEl. Locate('RecNo', p, []);
          Screen.Cursor:=crDefault;

          {log}
          dm3.update_operation(LogOperationID);
          {***}
        end;
      end

end;

function TfmSInv.dgA2GetCellCheckBox(Sender: TObject; Field: TField;
var
  StateCheckBox: Integer): Boolean;
begin
  Result:=(Field.FieldName='F1') or (Field.FieldName='F2')
end;

function TfmSInv.dgArtGetCellCheckBox(Sender: TObject; Field: TField;
var
  StateCheckBox: Integer): Boolean;
begin
  Result:=(Field.FieldName='F1') or (Field.FieldName='F2')
end;

procedure TfmSInv.N20Click(Sender: TObject);
var
  LogOperationID: string;
begin
 {log}
  LogOperationID:=dm3.insert_operation('�������������� ��������� ����',LogOprIdForm);
 {***}
  with dm, dm2, dmCom do
  try
     fmSPEdit:=TfmSPEdit.Create(Application);
     with fmSPEdit do
     begin
        ce1.Value:=taA2Price1.AsFloat;
        if ShowModal=mrOK then
        begin
            with quSetSP do
            begin
               ParamByName('PRICE').AsFloat:=ce1.Value;
               ParamByName('TSPRICE').AsFloat:=ce1.Value;
               ParamByName('ART2ID').AsInteger:=taA2ARt2Id.AsInteger;
               ExecQuery;
             end;
             taA2.Refresh;
         end
     end
  finally
      fmSPEdit.Free;
  end;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.dgArtDblClick(Sender: TObject);
var
  s: string[10];
  i: integer;
begin
  s:=dgArt.SelectedField.FieldName;
  if (s='F1') or (s='F2') then
    with dm, quArt do
    begin
        i:=FieldByName(s).AsInteger;
        if i=0 then i:=1
        else i:=0;
        Edit;
        FieldByName(s).AsInteger:=i;
        Post;
    end;
end;

procedure TfmSInv.dgArtShowEditor(Sender: TObject; Field: TField;
var
  AllowEdit: Boolean);
begin
  AllowEdit:=(Field.FieldName<>'F1') and (Field.FieldName<>'F2');
end;

procedure TfmSInv.N21Click(Sender: TObject);
begin
  with dm do
  begin
      PrHistArt2Id:=taA2Art2Id.AsInteger;
      PrHistDepId:=taSListDepId.AsInteger;
  end;
  ShowAndFreeForm(TfmA2PHist, Self, TForm(fmA2PHist), True, False);
end;

procedure TfmSInv.dgA2ColExit(Sender: TObject);
var
  i: integer;
begin
  if dgA2.SelectedField.FieldName='ART2' then
    with dm, dm2.quCheckA2Exist, Params do
      if taA2.State=dsInsert then
      begin
          ByName['ART2'].AsString:=taA2Art2.AsString;
          ByName['D_ARTID'].AsInteger:=taA2ArtID.AsInteger;
          ExecQuery;
          i:=Fields[0].AsInteger;
          Close;
          if i<>0 then if MessageDialog('������� 2 ��� ����������. ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
      end;
end;

procedure TfmSInv.N25Click(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
 {log}
 LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);
 {***}
  b:=GetBit(dmCom.EditRefBook, 3) and CenterDep;
  ShowAndFreeFormEnabled(TfmDIns, Self, TForm(fmDIns), True, False, b, 'siExit;tb1;spitPrint;siSort;','dg1;');

  dm.FillListBoxes(nil, nil, nil, lbIns,nil,nil,nil);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N26Click(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);
  {***}

  b:=GetBit(dmCom.EditRefBook, 1) and CenterDep;
  ShowAndFreeFormEnabled(TfmGood, Self, TForm(fmGood), True, False, b, 'siExit;tb1;spitPrint;siSort;','dg1;');

  dm.FillListBoxes(nil, nil, lbGood, nil,nil,nil,nil);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N27Click(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������� ����������',LogOprIdForm);
  {***}

  b:=GetBit(dmCom.EditRefBook, 0) and CenterDep;
  ShowAndFreeFormEnabled(TfmMat, Self, TForm(fmMat), True, False, b, 'siExit;tb1;spitPrint;siSort;', 'dg1;');

  dm.FillListBoxes(nil, lbMat, nil, nil,nil,nil,nil);

 {log}
  dm3.update_operation(LogOperationID);
 {***}
end;

procedure TfmSInv.N28Click(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������� �����������',LogOprIdForm);
  {***}

  b:=GetBit(dmCom.EditRefBook, 4) and CenterDep;
  ShowAndFreeFormEnabled(TfmComp, Self, TForm(fmComp), True, False, b,
        'siHelp;siExit;tb1;spitPrint;siSort;edCode;siCategory;tb2;PageControl1;'+
        'pc1;TabSheet1;TabSheet2;TabSheet3;TabSheet4;TabSheet5;TabSheet6;TabSheet7;'+
        'Panel1;tsMol;mnitAll;st1;mnitCur;N6;N1;N2;N3;N4;N5;edCode;chAllWorkOrg;'+
        'laCompCat;Label36;Splitter1;',
        'dg1;M207IBGrid1;');
  dm.FillListBoxes(lbComp, nil, nil, nil,nil,nil,nil);
  ReopenDatasets([dm.quSup]);
  SetCBDropDown(lcSup);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N30Click(Sender: TObject);
begin
  PostDataSets([dm.quArt]);
  ActiveControl:=dgA2;
  dgA2.ColumnByName['PRICE1'].Field.FocusControl;
end;

procedure TfmSInv.CMDialogKey(var Message: TCMDialogKey);
begin
  if (Message.CharCode=VK_TAB) and (ActiveControl=dgArt) then
  begin
      ActiveControl:=dgA2;
      PostMessage(Handle, WM_KEYDOWN, VK_RIGHT, 0);
      PostMessage(Handle, WM_KEYDOWN, VK_LEFT, 0);
  end
    else inherited;
end;

procedure TfmSInv.cxButton1Click(Sender: TObject);

begin
 dm.taTollingParams.Open;
 dm.taTollingParams.Edit;
  //if dm.taTollingParams.State in [dsEdit, dsInsert] then
  //begin
  dm.taTollingParams.Post;
  dm.taTollingParams.Transaction.CommitRetaining;
  dm.taA2.CloseOpen(True);
//end;
end;

procedure TfmSInv.N31Click(Sender: TObject);
var
  s :string;
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������������', LogOprIdForm);
  {***}

  with dm do
  begin
      PostDataSet(taSEl);
      DstDepId:=taSListDepId.AsInteger;
      s:=''''+DateToStr(dmCom.GetServerTime)+'''';
      with quTmp do
      begin
         SQL.Text:='EXECUTE PROCEDURE CREATEDINV '+IntToStr(DstDepId)+','+s+','+IntToStr(dmCom.UserId);
         ExecQuery;
      end;
      DstOne2Many:= True;
      OpenDataSets([dm.taDList]);
      ShowAndFreeForm(TfmDst, Self, TForm(fmDst), True, False);
      CloseDataSets([dm.taDList]);
  end;

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N32Click(Sender: TObject);
var LogOperationID: string;
begin
 {log}
 LogOperationID:=dm3.insert_operation('������� �� ������', LogOprIdForm);
 {***}
  with dmCom, dm, dm2 do
    try
      Screen.Cursor:=crSQLWait;
      OpenDataSets([taItBuf]);
      taItBuf.First;
      NotCalcUID:=True;
      while not taItBuf.EOF do
      begin
         if NOT taSel.Locate('ART2ID', taItBufArt2Id.AsInteger, []) then
         begin
            taSEl.Insert;
            taSElArt2Id.AsInteger:= taItBufArt2Id.AsInteger;
            taSElPrice.AsFloat:=taItBufSPrice.AsFloat;
            taSElPrice2.AsFloat:=taItBufPrice.AsFloat;
            taSEl.Post;
          end;
          OpenDataSets([taSItem]);
          taSItem.Insert;
          taSItemUID.AsInteger:=taItBufUID.AsInteger;
          taSItemW.AsFloat:=taItBufW.AsFloat;
          taSItemSS.AsString:=taItBufSZ.AsString;
          PostDataSets([taSItem]);
          CloseDataSets([taSItem]);
          taSEl.Refresh;
          dm.taSEl. Locate('RecNo', p, []);
          taItBuf.Next;
      end;
      ClearItBuf;
      tr.CommitRetaining;
      CloseDataSets([taItBuf]);
      taSList.Refresh;
    finally
      NotCalcUID:=False;
      Screen.Cursor:=crDefault;
    end;
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.Art2Id1Click(Sender: TObject);
begin
  MessageDialog(dm.taA2ART2ID.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmSInv.SElId1Click(Sender: TObject);
begin
  MessageDialog(dm.taSElSELID.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmSInv.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if SearchEnable then
  begin
     if cbSearch.Checked then
     begin
        StopFlag:=False;
        with dm, quArt do
          if quArt.Active then
             LocateF(dm.quArt, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
     end;
  end;
end;

procedure TfmSInv.ceArtEnter(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_CountryId<>D_CountryId) or
       (Old_D_InsId<>D_InsId) or
       (Old_D_Att1Id<>D_Att1Id)or
       (Old_D_Att2Id<>D_Att2Id) then
        begin
           Old_D_CompId:=D_CompId;
           Old_D_MatId:=D_MatId;
           Old_D_GoodId:=D_GoodId;
           Old_D_InsId:=D_InsId;
           Old_D_countryId:=D_countryId;
           Old_D_Att1Id := D_Att1Id;
           Old_D_Att2Id := D_Att2Id;
           if cbSearch.Checked then Art:=''
              else Art:=ceArt.Text;
           Screen.Cursor:=crSQLWait;
           ReopenDataSets([quArt]);
           Screen.Cursor:=crDefault;
         end;
end;

procedure TfmSInv.ceArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
     VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then
                     begin
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                           if (quArt.FieldByName('ART').asString<>ceArt.Text) then miInsFromSearchClick(Sender);
                      end
                     else
                      begin
                         if cbSearch.Tag=0 then
                         begin
                            Art:=ceArt.Text;
                            ReopenDataSets([quArt]);
                            cbSearch.Tag:=1;
                            if (quArt.FieldByName('ART').asString<>ceArt.Text) then miInsFromSearchClick(Sender);
                         end;
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                      end;
                      ActiveControl:=dgArt;
                   end;
     VK_RIGHT: if Shift=[ssCtrl] then ActiveControl:=dgA2;
     VK_DOWN: ActiveControl:=dgArt;     
     VK_INSERT: if Shift=[ssCtrl] then  miInsfromSearchClick(nil);
     VK_ESCAPE: StopFlag:=True;
  end;
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmSInv.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then ActiveControl:=dgArt;
end;

procedure TfmSInv.SpeedItem1Click(Sender: TObject);
begin
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  with dmCom, dm do
  begin
      Art:='';
      D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
      D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
      D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
      D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
      D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
      D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
      D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
      Old_D_CompId:=D_CompId;
      Old_D_MatId:=D_MatId;
      Old_D_GoodId:=D_GoodId;
      Old_D_InsId:=D_InsId;
      Old_D_CountryId:=D_CountryId;
      Old_D_Att1Id:= D_Att1Id;
      Old_D_Att2Id:= D_Att2Id;
  end;
  ReopenDataSets([dm.quArt]);
end;

function TfmSInv.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmSInv.taContractAfterOpen(DataSet: TDataSet);
var
  i :integer;
begin
  i := taContract.RecordCount;
end;

procedure TfmSInv.taContractBeforeOpen(DataSet: TDataSet);
var
  ContractClassID: Integer;
begin
  ContractClassID := 0;

  if dm.taSListISTOLLING.AsInteger = 1 then
  begin
    ContractClassID := 4;
  end else
  begin
    if dm.taSListPAYTYPEID.AsInteger <> 0 then
    begin
      if dm.taSListPAYTYPEID.AsInteger = 7 then
      begin
        ContractClassID := 2;
      end else

      if dm.taSListPAYTYPEID.AsInteger = 1000000001 then
      begin
        ContractClassID := -1;
      end else

      begin
        ContractClassID := 1;
      end;
    end;
  end;

  taContract.ParamByName('agent$id').AsInteger := StoredSupplierID;

  taContract.ParamByName('contract$class$id').AsInteger := ContractClassID;

  taContract.ParamByName('invoice$date').AsDate := dm.taSListNDATE.AsDateTime;

end;

procedure TfmSInv.taContractCalcFields(DataSet: TDataSet);
var
  N: string;
  Date: string;
  Name: string;
begin
  N := taContractNUMBER.AsString;

  Name := taContractNAME.AsString;

  Name := Name + ' � ' + N;

  Date := FormatDateTime('dd.mm.yyyy', taContractCONTRACTDATE.AsDateTime);

  Name := Name + ' �� ' + Date;

  if taContractISUNLIMITED.AsInteger = 0 then
  begin
    Date := FormatDateTime('dd.mm.yyyy', taContractCONTRACTENDDATE.AsDateTime);

    Name := Name + ' �� ' + Date;
  end;

  taContractNAMEFORMATED.AsString := Name;
end;

procedure TfmSInv.cbSearchClick(Sender: TObject);
begin
  if cbSearch.Checked then
    with dm do
    begin
        Art:='';
        ReopenDataSets([quArt]);
    end;
  cbSearch.Tag:=0;
end;

procedure TfmSInv.N39Click(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('������� ��� ������ ���������',LogOprIdForm);
  {***}

  ShowAndFreeForm(TfmInvMargin, Self, TForm(fmInvMargin), True, False);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N40Click(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('����� �������',LogOprIdForm);
  {***}

  ShowAndFreeForm(TfmDepMargin, Self, TForm(fmDepMargin), True, False);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N110Click(Sender: TObject);
var
  LogOperationID: string;
begin
  {log}
  LogOperationID:=dm3.insert_operation('������������ ���� �� ����������� ��� ���� ���������',LogOprIdForm);
  {***}

  ExecQ('EXECUTE PROCEDURE INVRETURNPRICE '+dm.taSListSInvId.AsString);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.N41Click(Sender: TObject);
var
  LogOperationID: string;
begin
 {log}
  LogOperationID:=dm3.insert_operation('��������� ������� ��� ���� ���������',LogOprIdForm);
 {***}

  ExecQ('EXECUTE PROCEDURE INVAPPLYMARGIN '+dm.taSListSInvId.AsString);
  ReOpenDataSets([dm.taSEl]);
  dm.taSEl. Locate('RecNo', p, []);
  dm.taSList.Refresh;
  SetMargin;

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.dgArtCellClick(Column: TColumn);
begin
 ReopenDataSets([dm.quMaxArtPrice]);
end;

procedure TfmSInv.ceMarginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
  begin

  end;
end;

procedure TfmSInv.ceMarginButtonClick(Sender: TObject);
var
  Key: Word;
begin
  Key:=VK_RETURN;
  ceMarginKeyDown(Sender,Key,[]);
end;

procedure TfmSInv.N7Click(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
 {log}
  LogOperationID:=dm3.insert_operation('���������� �����',LogOprIdForm);
 {***}

  b:=GetBit(dmCom.EditRefBook, 12) and CenterDep;
  ShowAndFreeFormEnabled(TfmCountry, Self, TForm(fmCountry), True, False, b,'siExit;tb1;spitPrint;siSort;', 'dg1;');
  dm.FillListBoxes(nil, nil, nil, nil,lbcountry,nil,nil);

  {log}
  dm3.update_operation(LogOperationID);
 {***}
end;

procedure TfmSInv.nHistartClick(Sender: TObject);
begin
  if TWinControl(Sender).Tag=1 then dm.FindArt:=dm.quArtART.AsString else dm.FindArt:='';
  ShowAndFreeForm(TfmHistItem, Self, TForm(fmHistItem), True, False);
end;


procedure TfmSInv.dgElColExit(Sender: TObject);
begin
   with dm do
   begin
      if (taSEl.State = dsInsert) or (taSEl.State =dsEdit) then taSEl.Post;
   end;
end;

procedure TfmSInv.dgElColEnter(Sender: TObject);
begin
  with dm do
  begin
    if (taSEl.State = dsInsert) or (taSEl.State =dsEdit) then taSEl.Post;
  end;
end;

procedure TfmSInv.ceArtClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.dgA2MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lbGoodMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lbInsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.cbSearchMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;



procedure TfmSInv.edSNMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.edSSFClick(Sender: TObject);
begin
   if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.deNDateClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lcSup_oldClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lcNDSClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lcPayTypeClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lcPayTypeCloseUp(Sender: TObject);
var
  ContractClassID: Integer;
  StoredContractClassID: Integer;
begin
  ReOpenDataSet(taContract);

  StoredContractClassID := dm.taSListCONTRACTCLASSID.AsInteger;

  ContractClassID := taContractCLASSID.AsInteger;

  if ContractClassID <> StoredContractClassID then
  begin
    if not (dm.taSList.State in [dsInsert, dsEdit]) then
    begin
      dm.taSList.Edit;
    end;

    if ContractClassID = 0 then
    begin
      dm.taSListCONTRACTID.AsInteger  := 0;

      dm.taSListCONTRACTCLASSID.AsInteger := 0;
    end else
    begin
      if taContract.RecordCount = 1 then
      begin
        dm.taSListCONTRACTID.AsInteger  := taContractID.AsInteger;

        dm.taSListCONTRACTCLASSID.AsInteger := taContractCLASSID.AsInteger;

      end else
      begin
        dm.taSListCONTRACTID.AsInteger  := 0;

        dm.taSListCONTRACTCLASSID.AsInteger := 0;
      end;
    end;
  end;
end;

procedure TfmSInv.edTrClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State = dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.edNDSClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.edPTrNDSClick(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.edPureKChange(Sender: TObject);
begin
  MaterialInitialize;
end;

procedure TfmSInv.DBEdit1Click(Sender: TObject);
begin
  if (dm.taSEl.State = dsInsert) or (dm.taSEl.State = dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.lcSupCloseUp(Sender: TObject);
var
  b: boolean;
  SupplierID: Integer;
begin
  if lcSup.EditValue<>NULL then
     with dm do
     begin
        FindProd(lcSup.EditValue);
        if quSupD_CompId.AsInteger<>lcSup.EditValue then
           b:=quSup.Locate('D_CompId', lcSup.EditValue, [])
           else b:=True;
        if b then
        begin
           {
           if (not quSupNDSID.IsNull or not quSupPayTypeId.IsNull) then
           begin
              if not (taSList.State in [dsInsert, dsEdit]) then taSList.Edit;
              if not quSupPayTypeId.IsNull then taSListPayTypeId.AsInteger:=quSupPayTypeId.AsInteger;
              if not quSupNDSID.IsNull then taSListNDSID.AsInteger:=quSupNDSID.AsInteger;

           end;
           }

           if (not quSupNDSID.IsNull) then
           begin
              if not (taSList.State in [dsInsert, dsEdit]) then taSList.Edit;

              if not quSupNDSID.IsNull then taSListNDSID.AsInteger:=quSupNDSID.AsInteger;
           end;


           SupplierID := lcSup.EditValue;

           if SupplierID <> StoredSupplierID then
           begin
             if not (taSList.State in [dsInsert, dsEdit]) then taSList.Edit;

             taSListCONTRACTID.AsVariant := Null;

             taSListCONTRACTCLASSID.AsVariant := Null;

             StoredSupplierID := SupplierID;

             ReOpenDataSet(taContract);

             if taContract.RecordCount = 1 then
             begin
               taSListCONTRACTID.AsInteger  := taContractID.AsInteger;

               taSListCONTRACTCLASSID.AsInteger := taContractCLASSID.AsInteger;
             end;
           end;
        end;
     end;
end;

procedure TfmSInv.edPTrMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
   if (dm.taSEl.State = dsInsert) or (dm.taSEl.State =dsEdit) then dm.taSEl.Post;
end;

procedure TfmSInv.acDictAtt1Execute(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������� ������� 1',LogOprIdForm);
 {***}

  b:=GetBit(dmCom.EditRefBook, 14) and CenterDep;
  ShowAndFreeFormEnabled(TfmdAtt1, Self, TForm(fmdAtt1), True, False, b, 'acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt1;');

  dm.FillListBoxes(nil,nil,nil,nil,nil,lbAtt1,nil);

  {log}
  dm3.update_operation(LogOperationID)
  {***}
end;

procedure TfmSInv.acDictAtt2Execute(Sender: TObject);
var
  LogOperationID: string;
  b:boolean;
begin
  {log}
  LogOperationID:=dm3.insert_operation('���������� ������� 2',LogOprIdForm);
 {***}

  b:=GetBit(dmCom.EditRefBook, 15) and CenterDep;
  ShowAndFreeFormEnabled(TfmdAtt2, Self, TForm(fmdAtt2), True, False, b, 'acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt2;');

  dm.FillListBoxes(nil,nil,nil,nil,nil,nil,lbAtt2);

  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSInv.acidNumExecute(Sender: TObject);
begin
   with dm do
   begin
      PostDataSets([taSEl, taA2]);

      if NOT taSElSElId.IsNull then ShowAndFreeForm(TfmSItem, Self, TForm(fmSItem), True, False);
      if taSElQuantity.AsFloat=0 then
         with taSEl do
         begin
            Tag:=1;
            Delete;
            Tag:=0;
         end;
      taSList.Refresh;
      ActiveControl:=ceArt;

      if dm.taSListISTOLLING.AsInteger = 1 then
      begin
        ReOpenDataSet(dm.taTollingParams);
      end;
   end;
end;

procedure TfmSInv.acidNumUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  if (not dm.taSEl.IsEmpty) then acidNum.Enabled:=true else acidNum.Enabled := false;
  cbOwner.Visible := dm.taSList.Active and (dm.taSListPAYTYPEID.AsInteger in [7, 8]);
  dgA2.ColumnByName['PRICE1'].Visible := chbxAddNDS.Checked;

  Enabled := taContract.Active;

  if Enabled then
  begin
    if dm.taSListCONTRACTID.AsInteger = 0 then
    begin
      Enabled := taContract.RecordCount > 0;
    end else
    begin
      Enabled := taContract.RecordCount > 1;
    end;
  end;

  lcContract.Enabled :=  Enabled;
end;

procedure TfmSInv.FormDeactivate(Sender: TObject);
begin
  if Assigned(FhintComp) then FhintComp.ReleaseHandle;
  if Assigned(FhintCountry) then FhintCountry.ReleaseHandle;
  if Assigned(FhintGood) then FhintGood.ReleaseHandle;
  if Assigned(FhintIns) then FhintIns.ReleaseHandle;
  if Assigned(FhintMat) then FhintMat.ReleaseHandle;
  if Assigned(FhintAtt1) then FhintAtt1.ReleaseHandle;
  if Assigned(FhintAtt2) then FhintAtt2.ReleaseHandle;
  FhintComp := nil; FhintCountry := nil; FhintGood := nil; FhintIns := nil;
  FhintMat := nil; FhintAtt1 := nil; FhintAtt2 := nil;
end;

procedure TfmSInv.FormActivate(Sender: TObject);
begin
  WMode:='SINV';
  lbCompClick(lbComp);
  lbCompClick(lbCountry);
  lbCompClick(lbGood);
  lbCompClick(lbIns);
  lbCompClick(lbMat);
  lbCompClick(lbAtt1);
  lbCompClick(lbAtt2);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSInv.WMActivate(var Message: TWMActivate);
begin
  if (Message.Active = WA_ACTIVE) or (Message.Active = WA_CLICKACTIVE) then
      FormActivate(Self) else FormDeactivate(Self);
  inherited;
end;

procedure TfmSInv.dgA2Exit(Sender: TObject);
begin
 if not dm.A2Err then PostDataSet(dm.taA2);
end;

procedure TfmSInv.btdateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taSListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
     fmSetSDate.mc1.Date := dm.taSListSDATE.AsDateTime;
     fmSetSDate.tp1.Time := dm.taSListSDATE.AsDateTime;
     if fmSetSDate.ShowModal=mrOK then
     begin
        d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
        dm.taSlist.Edit;
        dm.taSlistSDATE.AsDateTime:=d;
        dm.taSlist.Post;
     end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmSInv.acShowArtIdUpdate(Sender: TObject);
begin
  acShowArtId.Visible := AppDebug;
  if acShowArtId.Visible then
     acShowArtId.Enabled := dm.quArt.Active and (not dm.quArt.IsEmpty);
end;

procedure TfmSInv.acSetMarginExecute(Sender: TObject);
var
  Margin, CurrentMargin: Extended;
  RoundNorm : Integer;
begin

  try
    Margin := StrToFloat(edMargin.Text);
  except
    MessageDialog('������� ���������� �������� �������!', mtError, [mbOk], 1);
    eXit;
  end;

  dm.GetMargin(dm.taSListDepId.AsInteger, CurrentMargin, RoundNorm, dm.taSListSInvId.AsInteger);

  if (Margin < 50) or ((Margin = 40) and (dm.taSListSUPID.AsInteger <> 311)) then
    if MessageDialog('������� ������ �������! ����������?', mtWarning, [mbYes, mbNo], 1) = mrNo then
      SysUtils.Abort;

  with dm.quTmp do
    begin
      SQL.Text := 'update InvMarg set Margin = :Margin ' + #10#13
                + 'where InvID = :InvID';
      ParamByName('Invid').AsInteger := dm.taSListSINVID.AsInteger;
      ParamByName('Margin').AsFloat := Margin;
      ExecQuery;
      Transaction.CommitRetaining;
      Close;
    end;
end;

procedure TfmSInv.acSetMarginUpdate(Sender: TObject);
begin
//  Enabled := dm.taSList.IsEmpty;
 chbxAddNDS.Enabled := (dm.taSListISCLOSED.AsInteger = 0) and dm.taSEl.IsEmpty;


end;

procedure TfmSInv.acShowArtIdExecute(Sender: TObject);
begin
  MessageDialog(dm.quArtD_ARTID.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmSInv.siHelpClick(Sender: TObject);
begin
  Application.HelpContext(100202);
end;

procedure TfmSInv.acBegEndMergeExecute(Sender: TObject);
var
  fl: string;
  Res: Variant;
begin
  Res := ExecSelectSQL('Select Is_Merge_Run from D_EMP where FIO = '#39 + dmCom.User.FIO +
                       #39' and Is_Merge_Run = current_connection',dm.quTmp);

  if VarIsNull(Res) then
  begin
    Res := ExecSelectSQL('Select first 1 FIO from D_EMP ' +
                         'where Is_Merge_Run <> current_connection and Is_Merge_Run <>-1',dm.quTmp);
    if not VarIsNull(Res)  then
       ShowMessage('����������� ������ ������ '+ VarToStr(Res) + ' � ������ ����������. ��������� ��������� ��������')
       else
       begin
          Res := ExecSelectSQL('Select FIO from D_EMP '+
                               'where ((Is_Merge_Run = current_connection) or (Is_Merge_Run = -1)) and '+
                               ' FIO = '#39 + dmCom.User.FIO + #39,dm.quTmp);
          if not VarIsNull(Res)  then
          begin
             fl := 'current_connection';
             siMergeComplex.ImageIndex := 10;
          end
       end
  end
   else
  begin
     fl := '-1';
     siMergeComplex.ImageIndex := 53;
  end;
  if fl <> '' then ExecSQL('update D_Emp Set Is_Merge_Run = '+ fl+' where D_EmpID = ' + IntToStr(dmCom.UserId) ,dm.quTmp);
end;

procedure TfmSInv.SetCommission;
var
  ACommission: Integer;
  PayTypeID: Integer;
begin
  if VarIsNull(lcPayType.KeyValue) then PayTypeID := 0 else PayTypeID := lcPayType.KeyValue;
  if (PayTypeID = 7) or (PayTypeID = 1000000001) then ACommission := 1 else ACommission := 0;
  dm.trCommission.StartTransaction;
  dm.spSetCommission.ParamByName('SINVID').AsInteger := DocumentID;
  dm.spSetCommission.ParamByName('COMMISSION').AsInteger := ACommission;
  dm.spSetCommission.ExecProc;
  dm.trCommission.Commit;
end;

procedure TfmSInv.GetCommission;
var
  PayTypeID: Integer;
begin
  DocumentID := dm.taSListSINVID.AsInteger;
  if VarIsNull(lcPayType.KeyValue) then PayTypeID := 0 else PayTypeID := lcPayType.KeyValue;
  if PayTypeID = 7 then Commission := 1 else Commission := 0;
end;

procedure TfmSInv.SpeedItem3Click(Sender: TObject);
var I:integer;
begin

 I:=dgEl.Row;
  with dm, dmCom do
  begin
     PostDataSet(taSEl);
     tr.CommitRetaining;
     ReOpenDataSet(taSEl);

     ReOpenDataSet(taSList); //������ �������� ��� ��������� ����� ����� �� ���������
     dm.taSEl.Locate('RecNo', p, []);
    // taSEl.Refresh;

     
     taSEl.MoveBy(I-1);
	   dgEl.SetFocus;

     tr.CommitRetaining;
     dm.taSEl.Locate('RecNo', p, []);
  end;
  dmcom.tr.CommitRetaining;
end;

procedure TfmSInv.dsArtUpdateBeforeOpen(DataSet: TDataSet);
begin
  dsArtUpdateSINVID.AsInteger:=dm.taSElSINVID.AsInteger;
end;

procedure TfmSInv.acOpenExecute(Sender: TObject);
var
  LogOperationID: string;
begin
SetCommission;
with dm do
      begin
        LogOperationID:=dm3.insert_operation('������� ���������',LogOprIdForm);
        with quTmp do
        begin
          SQL.Text:='UPDATE SInv SET ISCLOSED=0 WHERE SINVID='+taSListSInvId.AsString;
          ExecQuery;
        end;
        dmCom.tr.CommitRetaining;
        taSList.Refresh;
        taSList.Edit;
        taSListNotPr.AsInteger:=0;
        taSList.Post;
        SetCloseInvBtn(siCloseInv, 0);
        ClosedInvId:=-1;
        dm3.update_operation(LogOperationID);
     end;
dm.SetVisEnabled(TWinControl(dbSup),dm.taSListISCLOSED.AsInteger=1);
dm.SetVisEnabled(lcSup,dm.taSListISCLOSED.AsInteger <> 1);
GroupBoxTolling.Enabled := dm.taSListISCLOSED.AsInteger <> 1;

end;

procedure TfmSInv.acCloseExecute(Sender: TObject);
var
  i: integer;
{  IsCheckAct,} NoPrord: integer;
//  count: integer;
  LogOperationID: string;
  smsg:string;
  ButtonSelected:integer;
  Ignore50: Boolean;
begin
  if dm.taSListPAYTYPEID.AsInteger = 0 then
  begin
    TDialog.Error('�� ������ "��� ������"!');

    Exit;
  end;

  if dm.taSListCONTRACTID.AsInteger = 0 then
  begin

    if dm.taSListPAYTYPEID.AsInteger <> 1000000001 then
    begin
      TDialog.Error('�� ������ "�������"!');

      Exit;
    end;

  end;

if lcpaytype.Text='' then
  ShowMessage('��������! �������� ��� ������!')
else
begin
  Ignore50 := False;

  dm.taSEl.DisableControls;

  dm.taSEl.First;

  while not dm.taSEl.Eof do
  begin
    if not Ignore50 then
    begin
      if (dm.taSElCOLOR.AsInteger and 2) <> 0 then
      begin
        dm.taSEl.EnableControls;

        if MessageDialog('��������� ����� ��������� ���� ����� 50% �� ���������!'#13#10'����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          dm.taSEl.DisableControls;

          Ignore50 := True;
        end else

        begin
          exit;
        end;
      end;
    end;

    dm.taSEl.Next;
  end;

  dm.taSEl.First;

  dm.taSEl.EnableControls;


  SetCommission;
  fmSinv.taHist.Active:=true;
  fmSinv.taHist.Open;
  with fmSinv.taHist do
  begin
  fmSinv.taHist.Append;
  fmSinv.taHistDOCID.AsInteger := dm.taSListSINVID.AsInteger;
 If (Hist.log='') then fmSinv.taHistFIO.Value := dmCom.User.Alias else
 fmSinv.taHistFIO.Value := Hist.log;
  fmSinv.taHistSTATUS.Value := '�������';
  fmSinv.taHist.Post;
  fmSinv.taHist.Transaction.CommitRetaining;
  end;
  fmSinv.taHist.Close;
  fmSinv.taHist.Active:=false;

  with dm do
  begin
     if taSListIsClosed.AsInteger=0 then
     begin
        PostDataSets([quArt, taSEl, taSList, taA2]);
        if taSListNOTPR.AsInteger=1 then
           if MessageDialog('�� ����� ������� �������� ��������. ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then
              SysUtils.Abort;
         with taSEl do // Price = Sel.Price,
                       // Price2 - ���� IType=2 (�.�. ��) �� = Price.Price2, �����
                       //                                    = Price.TPrice;
                       //          ���� IsClosed = 1 (����.�������) ��  = Sel.Price2;

         //���� ������� Sel �������� ������������ � ������������ � ��������� ������ ���� � ���� ��� � ��������),
         //������ �� �������� �������� ������ � ��������� ��������� ����/����

         //� ���� Price �������� ���������� ������ ������ �� ��������,
         //���� ������� ���������������
         //     TPrice ���������� ������������ � ������������ � ��������� ����.�. � ��������
         //     Price2 ������ ������� ��������� ����, ������������ ��������
         begin
            First;
            while NOT EOF do
            begin
               if taSElPrice.AsFloat=0 then //����.���� � ��������� ����� ����
                  if MessageDialog('��.���� ����� ����. ����������?',
                               mtConfirmation, [mbYes, mbNo], 0)=mrNo then Exit;

               if taSElPrice2.AsFloat=0 then //����.���� � ��������� ����� ����
                  if MessageDialog('���.���� ����� ����. ����������?',
                               mtConfirmation, [mbYes, mbNo], 0)=mrNo then Exit;

               if taSElPrice.AsFloat>taSElPrice2.AsFloat then //� ���� �������� ����.�. < ����.�. (�� ���� ����.�. ������� ������� ��������, ����������� �� ������) - �����!!!
                  if MessageDialog('��������� ���� ������ ���������. ����������?',
                               mtConfirmation, [mbYes, mbNo], 0)=mrNo then Exit;
               Next;
            end;
         end;

        if taSListSupId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���������');
        if taSListSN.AsString='' then raise Exception.Create('���������� ������ �������� � ���� � ���������');

        with quTmp do
         begin
              SQL.Text:='SELECT COUNT(*) FROM SINV WHERE ITYPE in (1, 21) AND DEPID='+inttostr(taSListDEPID.AsInteger)+' and SN='+taSListSN.AsString+' AND FYEAR(SDATE)='+GetSYear(taSListSDate.AsDateTime);
              ExecQuery;
              i:=Fields[0].AsInteger;
              Close;
              if i>1 then raise Exception.Create('������������ ����� ���������!!!');
         end;
        siRecalcClick(NIL);

         with quTmp do
          begin // �������� �-�� ������� � ���������, � ������� ���������� ����.���� �� ��������� � �������
           if dm.taSElSINVID.AsString='' then NoPrord := 0 else
            begin
             close;
             SQL.Text := 'select result from COUNT_ITEM_PRORD('+dm.taSElSINVID.AsString+')';
             ExecQuery;
             NoPrord := Fields[0].AsInteger;
             close;
            end;
           end;

           if (NoPrord=0) then //���� ����� ������� ���, ��
            begin
             LogOperationID:=dm3.insert_operation('������� ���������',LogOprIdForm);
             with dm, quTmp do
             begin
              Screen.Cursor := crSQLWait;
              SQL.Text:='SELECT PR FROM CloseInv('+taSListSInvId.AsString+', 1, '+IntToStr(dmCom.UserId)+')';
              ExecQuery;
              IsPriceChng := Fields[0].AsInteger;
              Close;
              Transaction.CommitRetaining;
              Screen.Cursor := crDefault;
             end;
             ClosedInvId:=taSListSInvId.AsInteger;
             SetCloseInvBtn(siCloseInv, 1);
             taSList.Refresh;
             dm3.update_operation(LogOperationID);
             dmCom.tr.CommitRetaining;
            end
           else
            begin

             MessageDialog('��������� ��������� ���. ����� ������������ ������� �� ����������!', mtInformation, [mbOK], 0);

             //ButtonSelected:=MessageDialog('��������� ��������� ���. ������������ ������� �� ����������?', mtWarning, [mbYes, mbNo, mbCancel], 0);

             ButtonSelected := mrYes;

             if ButtonSelected=mrYes then
               begin
               with dm, dm.quTmp do
                begin
                 Screen.Cursor := crSQLWait;
                 SQL.Text := 'select WARN from CR_PRORD('+dm.taSListSINVID.AsString+')';
                 ExecQuery;
                 smsg := trim(Fields[0].AsString);
                 Close;
                 Transaction.CommitRetaining;
                 Screen.Cursor := crDefault;
                end;
                 if smsg='' then
                 acCloseWithAct.Execute
                 else
                 begin
                  if (MessageDialog(smsg, mtWarning, [mbYes, mbNo, mbCancel], 0)=mrYes) then acCloseWithAct.Execute;
                 end ;
               end
              else
             if ButtonSelected=mrNo then
             begin
             LogOperationID:=dm3.insert_operation('������� ���������',LogOprIdForm);
             with dm, quTmp do
             begin
              Screen.Cursor := crSQLWait;
              SQL.Text:='update SInv set isclosed = 1, RState=0, UserId= '+inttostr(dmCom.UserId)+' where SInvId= '+taSListSInvId.AsString+';';
              ExecQuery;
              Close;
              Transaction.CommitRetaining;
              SQL.Text:=' update SInv set IsImport = 2 where SInvId= '+taSListSInvId.AsString+' and IsImport = 1;';
              ExecQuery;
              Close;
              Transaction.CommitRetaining;
              Screen.Cursor := crDefault;
             end;
             ClosedInvId:=taSListSInvId.AsInteger;
             SetCloseInvBtn(siCloseInv, 1);
             taSList.Refresh;
             dm3.update_operation(LogOperationID);
             dmCom.tr.CommitRetaining;
            end;
            dmCom.tr.CommitRetaining;
          end;
  end;
   dm.SetVisEnabled(TWinControl(dbSup),dm.taSListISCLOSED.AsInteger=1);
   dm.SetVisEnabled(lcSup,dm.taSListISCLOSED.AsInteger <> 1);
  end;
end;

  GroupBoxTolling.Enabled := dm.taSListISCLOSED.AsInteger <> 1;
end;

procedure TfmSInv.acCloseWithActExecute(Sender: TObject);
var
 count: integer;
begin
  LogOperationID:=dm3.insert_operation('������� ���������',LogOprIdForm);
  Application.Minimize;
  with dm, dm.quTmp do
   begin
    Screen.Cursor:=crSQLWait;
   try
    close;
    SQL.Text := 'select NActDef from D_Rec'; //����� �������� ������/���������
                                            //��������� ������� �������� � �����
    ExecQuery;
    IsCheckAct := Fields[0].AsInteger;
    if IsCheckAct = 0 then CheckNAct else // ���� � ������ ��������� �������, �� �����������
     begin
      count := 1;
      while (IsCheckAct <> 0 ) and (count < 21) do
       begin
        close;
        SQL.Text := 'select NActDef from D_Rec';
        ExecQuery;
        IsCheckAct := Fields[0].AsInteger;
        count:= count + 1;
        Close;
       end;
       CheckNAct
      end;
     finally
      Screen.Cursor:=crDefault;
      Application.Restore;
     end;
    if IsPriceChng > 0 then
     begin
      PActAfterSInv:=True;
     end;
    Close;
    if (s_close='') and (s_closed='') then
            begin
             ClosedInvId:=taSListSInvId.AsInteger;
             SetCloseInvBtn(siCloseInv, 1);
            end;
            taSList.Refresh;
            dm3.update_operation(LogOperationID);
            dmCom.tr.CommitRetaining;
   end;

end;

initialization

  fmSInv:=NIL;



end.




