unit EditEquipment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, DBLookupEh,
  DBGridEh;

type
  TfmEditEquipment = class(TForm)
    plInfo: TPanel;
    plButton: TPanel;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    lbsup: TLabel;
    Lbsdate: TLabel;
    lbndate: TLabel;
    lbssf: TLabel;
    lbsn: TLabel;
    lbcost: TLabel;
    dbsn: TDBEditEh;
    dbndate: TDBDateTimeEditEh;
    dbSup: TDBLookupComboboxEh;
    dbsdate: TDBDateTimeEditEh;
    dbssf: TDBEditEh;
    dbcost: TDBEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmEditEquipment: TfmEditEquipment;

implementation
uses data3, data, M207Proc;
{$R *.dfm}

procedure TfmEditEquipment.FormCreate(Sender: TObject);
begin
 ReOpenDataSets([dm.quSup]);
end;

procedure TfmEditEquipment.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm.quSup]);
end;

end.
