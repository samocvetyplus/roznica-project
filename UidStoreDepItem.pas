unit UidStoreDepItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ComCtrls, StdCtrls, Mask, DBCtrlsEh, 
  ExtCtrls, ActnList, M207Ctrls, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmUidStoreDepItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    cmbxGrMat: TDBComboBoxEh;
    cbstate: TComboBox;
    eduid: TEdit;
    SpeedbarSection2: TSpeedbarSection;
    StatusBar1: TStatusBar;
    fr1: TM207FormStorage;
    aclist: TActionList;
    acClose: TAction;
    dg1: TDBGridEh;
    siHelp: TSpeedItem;
    procedure acCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure cbstateChange(Sender: TObject);
    procedure eduidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cmbxGrMatChange(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUidStoreDepItem: TfmUidStoreDepItem;

implementation
uses data3, comdata, Data2, dbTree, m207Proc, MsgDialog;
{$R *.dfm}

procedure TfmUidStoreDepItem.cmbxGrMatChange(Sender: TObject);
begin
  dm3.FilterUIDStoreGrMat := cmbxGrMat.Items[cmbxGrMat.ItemIndex];
  ReOpenDataSets([dm3.taUidStoreDepItem]);
end;

procedure TfmUidStoreDepItem.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmUidStoreDepItem.FormCreate(Sender: TObject);
var nd : TNodeData;
begin
 tb1.Wallpaper:=wp;
 tb2.Wallpaper:=wp;
  dm3.FilterUIDStoreGrMat := '*���';
  // ������������� ��������
  cmbxGrMat.OnChange := nil;
  cmbxGrMat.Clear;
  nd := TNodeData.Create;
  nd.Code := '*���';
  nd.Name := '*���';
  cmbxGrMat.Items.Assign(dm2.slGrName);
  cmbxGrMat.Items.InsertObject(0,'*���',nd);
  cmbxGrMat.ItemIndex := 0;
  cmbxGrMat.OnChange := cmbxGrMatChange;

  dm3.FilterUIDStoreF:=0;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;

  dm3.FilterUIDStoreUID:=-1;
  OpenDataSets([dm3.taUIDStoreDepItem]);

end;

procedure TfmUidStoreDepItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUidStoreDepItem.cbstateChange(Sender: TObject);
begin
 dm3.FilterUIDStoreF:=cbstate.ItemIndex;
 ReOpenDataSets([dm3.taUidStoreDepItem]);
end;

procedure TfmUidStoreDepItem.eduidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_RETURN: begin
   if eduid.Text='' then dm3.FilterUIDStoreUID:=-1
   else
    try
     dm3.FilterUIDStoreUID := strtoint(eduid.text);
    except
     MessageDialog('�� ������ ������ ��. ������', mtError, [mbOk], 0);
    end;

   ReOpenDataSets([dm3.taUidStoreDepItem]);
   eduid.Text :='';
  end
 end
end;

procedure TfmUidStoreDepItem.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100441)
end;

procedure TfmUidStoreDepItem.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
