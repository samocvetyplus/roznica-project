object fmSetPrice: TfmSetPrice
  Left = 271
  Top = 334
  Width = 578
  Height = 333
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1094#1077#1085
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object sb1: TStatusBar
    Left = 0
    Top = 285
    Width = 570
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 570
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 72
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086'|'
      ImageIndex = 0
      Spacing = 1
      Left = 491
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siCopy: TSpeedItem
      BtnCaption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1094#1077#1085#1091' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 56
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siCopyClick
      SectionName = 'Untitled (0)'
    end
    object siMargin: TSpeedItem
      BtnCaption = #1053#1072#1094#1077#1085#1082#1080
      Caption = #1053#1072#1094#1077#1085#1082#1080
      Hint = #1053#1072#1094#1077#1085#1082#1080' '#1080' '#1085#1086#1088#1084#1099' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103
      ImageIndex = 55
      Spacing = 1
      Left = 75
      Top = 3
      Visible = True
      OnClick = siMarginClick
      SectionName = 'Untitled (0)'
    end
    object siSet: TSpeedItem
      BtnCaption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Hint = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091
      ImageIndex = 32
      Spacing = 1
      Left = 147
      Top = 3
      Visible = True
      OnClick = siSetClick
      SectionName = 'Untitled (0)'
    end
    object siBack: TSpeedItem
      BtnCaption = #1042#1077#1088#1085#1091#1090#1100
      Caption = #1042#1077#1088#1085#1091#1090#1100
      Hint = #1042#1077#1088#1085#1091#1090#1100' '#1094#1077#1085#1099' '#1080#1079' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072
      ImageIndex = 27
      Spacing = 1
      Left = 219
      Top = 3
      Visible = True
      OnClick = siBackClick
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 68
    Width = 570
    Height = 217
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsSetPrice
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = pm1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 1
    OnGetCellParams = M207IBGrid1GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = False
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NAME'
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 295
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1089#1093'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE1'
        Visible = True
      end>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 570
    Height = 28
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 3
    InternalVer = 1
    object Label1: TLabel
      Left = 10
      Top = 6
      Width = 73
      Height = 13
      Caption = #1054#1087#1090#1086#1074#1072#1103' '#1094#1077#1085#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object DBEdit1: TDBEdit
      Left = 86
      Top = 2
      Width = 79
      Height = 21
      Color = clYellow
      DataField = 'OPTPRICE'
      DataSource = dm.dsSetPrice
      TabOrder = 0
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 106
    Top = 138
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 248
    Top = 120
    object N1: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 56
      ShortCut = 113
      OnClick = siCopyClick
    end
    object N4: TMenuItem
      Caption = #1053#1072#1094#1077#1085#1082#1080
      ImageIndex = 55
      ShortCut = 114
      OnClick = siMarginClick
    end
    object miApplyMargin: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ShortCut = 115
      OnClick = siSetClick
    end
    object miReturnPrice: TMenuItem
      Caption = #1042#1077#1088#1085#1091#1090#1100
      ShortCut = 116
      OnClick = siBackClick
    end
  end
end
