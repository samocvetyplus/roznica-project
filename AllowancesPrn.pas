unit AllowancesPrn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, DBCtrls, Buttons;

type
  TfmActAllowancesPrn = class(TForm)
    btnOk: TButton;
    bntCancel: TButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtWRITEOFFRESON: TDBRichEdit;
    edtDEFECT: TDBRichEdit;
    edtSPOILING: TDBRichEdit;
    Label4: TLabel;
    edtSOLUTION: TDBRichEdit;
    Label5: TLabel;
    edtActName: TDBRichEdit;
    btnActName: TBitBtn;
    btnSpoiling: TBitBtn;
    btnDeffect: TBitBtn;
    btnWriteReson: TBitBtn;
    btnSolution: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure bntCancelClick(Sender: TObject);
    procedure btnActNameClick(Sender: TObject);
  private
    { Private declarations }
    FItype: integer;
  public
    { Public declarations }
    property Itype: integer read FItype write FItype;
  end;

var
  fmActAllowancesPrn: TfmActAllowancesPrn;

implementation

{$R *.dfm}
uses data3, dbUtil, db, WriteOff_Param, M207Proc;

procedure TfmActAllowancesPrn.FormCreate(Sender: TObject);
begin
  OpenDataSets([dm3.quWriteoff_Param, dm3.quActAllowParamPrn]);
end;

procedure TfmActAllowancesPrn.FormDestroy(Sender: TObject);
begin
  CloseDataSets([dm3.quWriteoff_Param, dm3.quActAllowParamPrn]);
end;

procedure TfmActAllowancesPrn.btnOkClick(Sender: TObject);
begin
  if (dm3.quActAllowParamPrn.State = dsInsert) or (dm3.quActAllowParamPrn.State = dsEdit) then
     dm3.quActAllowParamPrn.Post;
end;

procedure TfmActAllowancesPrn.bntCancelClick(Sender: TObject);
begin
  if (dm3.quActAllowParamPrn.State = dsInsert) or (dm3.quActAllowParamPrn.State = dsEdit) then
    dm3.quActAllowParamPrn.Cancel;
end;

procedure TfmActAllowancesPrn.btnActNameClick(Sender: TObject);
begin
  if (Sender as TBitBtn).Name = 'btnActName' then FItype := 1
  else if (Sender as TBitBtn).Name = 'btnSpoiling' then FItype := 2
  else if (Sender as TBitBtn).Name =  'btnDeffect' then FItype :=  3
  else if (Sender as TBitBtn).Name =  'btnWriteReson' then FItype := 4
  else if (Sender as TBitBtn).Name =  'btnSolution' then FItype := 5;
  ShowAndFreeForm(TfrmWriteoff_param, Self, TForm(frmWriteoff_param), True, False);
  ReopenDataSets([dm3.quWriteoff_Param, dm3.quActAllowParamPrn]);
end;

end.
