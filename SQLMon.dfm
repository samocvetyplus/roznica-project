object fmSQLMon: TfmSQLMon
  Left = 154
  Top = 216
  Width = 553
  Height = 506
  Caption = 'SQL Monitor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object me: TMemo
    Left = 0
    Top = 0
    Width = 429
    Height = 479
    Align = alLeft
    TabOrder = 0
  end
  object cbEn: TCheckBox
    Left = 440
    Top = 12
    Width = 97
    Height = 17
    Caption = 'Enabled'
    TabOrder = 1
  end
  object cbTop: TCheckBox
    Left = 440
    Top = 32
    Width = 97
    Height = 17
    Caption = 'Always on top'
    TabOrder = 2
    OnClick = cbTopClick
  end
  object Button1: TButton
    Left = 444
    Top = 420
    Width = 75
    Height = 25
    Caption = 'Clear log'
    TabOrder = 3
    OnClick = Button1Click
  end
end
