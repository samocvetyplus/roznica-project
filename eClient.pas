{***************************************}
{ �������� ����������                   }
{ ���������� dm2.quClient               }
{ Copyright (c) basile for ���������    }
{ **************************************}
unit eClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrlsEh, Mask, DBLookupEh, DBCtrls, Buttons;

type
  tarray = array of integer;
  TfmeClient = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edFIO: TDBEditEh;
    btnOk: TButton;
    btnNo: TButton;
    edAddress: TDBEditEh;
    lbaddress: TListBox;
    edHome_Flat: TDBEditEh;
    Label4: TLabel;
    edNoDCard: TDBEditEh;
    DBfotherDep: TDBCheckBoxEh;
    Label5: TLabel;
    dbBirthday: TDBDateTimeEditEh;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edFIOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edAddressKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edAddressMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure edFIOMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnOkMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbaddressDblClick(Sender: TObject);
    procedure lbaddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edHome_FlatMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnOkKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edHome_FlatKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edAddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edHome_FlatKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnNoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnNoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnOkKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFIOKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNoDCardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNoDCardKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNoDCardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dbBirthdayKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
   flFIO:boolean; //������� �������
   Fladdress:boolean; //������� �����
   flhome_flat:boolean; // ������� ���/��
   flbtnok :boolean; // ������� ���������
   flbtnNo:boolean; // ������� ������
   FirstST:boolean;
   flNodCard:boolean;
   flField: boolean;
   value:integer;
   addressid : tarray;
   Function CreatePmAddress(str:string):boolean;
  end;

var
  fmeClient: TfmeClient;

implementation

uses Data2, M207Proc, Data, comdata, pFIBQuery, DB;

{$R *.dfm}

{����� ������ �������}
Function TfmeClient.CreatePmAddress(str:string):boolean;
begin
 lbaddress.Clear;
 SetLength(addressid,0);
// lbid.Clear;
 with dm2, dm, qutmp do
  begin
   close;
   if CenterDep then
    sql.Text:='select d_address_id, address from d_address ' +#13#10+
              'where d_address_id<>-1000 and fdel=0 and address like '''+str+'%'+''''+#13#10+
              ' order by address'
   else
    sql.Text:='select d_address_id, address from d_address ' +#13#10+
              'where d_address_id<>-1000 and fdel=0 and address like '''+str+'%'+''''+#13#10+
              'and depid = '+QUClientDEPID.AsString+#13#10+
              ' order by address';

   ExecQuery;
   if Fields[0].AsInteger=0 then result:=false else result:=true;

   while not eof do
   begin
    lbaddress.Items.Add(TrimRight(Fields[1].AsString));
    SetLength(addressid,length(addressid)+1);
    addressid[Length(addressid)-1]:=Fields[0].Asinteger;
//    lbid.Items.Add(inttostr(Fields[0].Asinteger));
    next
   end;
   close;
  end;
end;

procedure TfmeClient.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
   VK_RETURN : begin
     lbaddress.Visible:=false;
     if(ActiveControl = btnOk)then ModalResult := mrOk
     else if(ActiveControl = btnNo)then ModalResult := mrCancel;
//     else ActiveControl := FindNextControl(ActiveControl, True, True, True);
   end;
   VK_ESCAPE : ModalResult := mrCancel;
  end;
end;

procedure TfmeClient.FormShow(Sender: TObject);
begin
  // � ����������� �� ���� ��� ����� ��������������� ��� ������� ��������� ���������
  if(dm2.quClient.State in [dsInsert])then
  begin
    if(dm2.quClientNAME.AsString <> '')then edFIO.Enabled := False;
    if(dm2.quClientNODCARD.AsString <> '')then edNoDCard.Enabled := False;
  end;
  if edFIO.Enabled then ActiveControl := edFIO;
  if edNoDCard.Enabled then ActiveControl := edNoDCard;
  if not dm2.quClientBIRTHDAY.IsNull then dbBirthday.Enabled:=false
  else dbBirthday.Enabled:=true;

end;

procedure TfmeClient.FormCreate(Sender: TObject);
begin
 ReOpenDataSets([dm2.quaddress, dmCom.taClient]);
 flFIO:=false;
 Fladdress:=false;
 flhome_flat:=false;
 flbtnok := false;
 flbtnNO := false;
 flNodCard := false;
 ActiveControl:= edFIO;
 if ActiveControl= edNoDCard then flNodCard := true
 else flFIO:=true;
 ///
 FirstST:=true;
end;

procedure TfmeClient.edFIOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 //������� �������� ������ ����
 if FirstST then FirstST:= not FirstST
 else
 if not flFIO then
 begin
  flNodCard:=false; 
  flFIO:=true;
  Fladdress:=false;
  flhome_flat:=false;
  flbtnok := false;
  flbtnNO := false;
 end;


end;

procedure TfmeClient.edAddressKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i:integer;
begin
 with dm2, dm, dmcom do
  case key of
  VK_RETURN:
   begin
     lbaddress.Visible:=false;
     if Fladdress then
     begin
     {���� �� ������ ��������}
     if edAddress.EditText<>'' then
      with qutmp do
      begin
       {����� �������� �������� � ���������� ��� ���� ������ ���}
       close;
       if WorkMode<>'ALLSELL' then
       sql.Text:='select d_address_id from d_address where address='''+
                 edAddress.EditText+''' and fdel=0 and depid = '+dm.taCurSellDEPID.AsString
       else
       sql.Text:='select d_address_id from d_address where address='''+
                 edAddress.EditText+''' and fdel=0 and depid = '+dm.taSellListDEPID.AsString;

       ExecQuery;
       if Fields[0].AsInteger=0 then
        begin
         OpenDataSets([taAddress]);
         taAddress.Insert;
         taAddressADDRESS.AsString:=edAddress.EditText;
         taAddress.Post;
         i:=taAddressD_ADDRESS_ID.AsInteger;
         CloseDataSets([taAddress]);
         quClient.Edit;
         quClientADDRESS.AsString:=edAddress.EditText;
         quClientADDRESSID.AsInteger:=i;{&&&&&}
         quClient.Post;
         quClient.Refresh;
        end
       else
        begin
        {���������� ����, ���� ����� ����� ��� ����}
         quClient.Edit;
         quClientADDRESS.AsString:=edAddress.EditText;
         quClientADDRESSID.AsInteger:=Fields[0].AsInteger;
         quClient.Post;
         quClient.Refresh;
        end;
       close;
      end;
     ActiveControl:=edHome_Flat;
     flField:=false;
    END; 
   end;
  VK_DOWN: begin if lbaddress.Visible then ActiveControl:=lbaddress  end;
  else
  begin
   if CreatePmAddress(edAddress.EditText) then lbaddress.Visible:=true
   else lbaddress.Visible:=false;
   flField:=true;
   value:=dm2.quClientCLIENTID.Asinteger;
  end
 end
end;

procedure TfmeClient.edAddressMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 with dmcom do
  {����� ������ �������}
  if CreatePmAddress(edAddress.EditText) then
  begin
   lbaddress.Visible:=true;
//   lbaddress.Left:=dg1.SelectedRect.Right+1;
  end
  else lbaddress.Visible:=false
end;

procedure TfmeClient.edFIOMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if flField then
  begin
   ActiveControl:=edAddress;
   if CreatePmAddress(edAddress.EditText) then lbaddress.Visible:=true
   else lbaddress.Visible:=false;
  end
 else lbaddress.Visible:=false;
end;

procedure TfmeClient.btnOkMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 lbaddress.Visible:=false;
end;

procedure TfmeClient.lbaddressDblClick(Sender: TObject);
begin
  with dm2 do
  begin
   quClient.Edit;
   if lbaddress.ItemIndex=-1 then lbaddress.ItemIndex:=0;
   quClientADDRESS.AsString:=lbaddress.Items[lbaddress.ItemIndex];
   quClientADDRESSID.AsInteger:=addressid[lbaddress.ItemIndex];
   //strtoint(lbid.Items[lbaddress.ItemIndex]);
   quClient.Post;
   quClient.Refresh;
   flField:=false;
   lbaddress.Visible:=false;
   ActiveControl:=edHome_Flat;
  end;
end;

procedure TfmeClient.lbaddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_RETURN:
  with dm2 do
  begin
   quClient.Edit;
   if lbaddress.ItemIndex=-1 then lbaddress.ItemIndex:=0;
   quClientADDRESS.AsString:=lbaddress.Items[lbaddress.ItemIndex];
   quClientADDRESSID.AsInteger:=addressid[lbaddress.ItemIndex];
   //strtoint(lbid.Items[lbaddress.ItemIndex]);
   quClient.Post;
   quClient.Refresh;
   flField:=false;
   lbaddress.Visible:=false;
   ActiveControl:=edHome_Flat;
  end;
 end;
end;

procedure TfmeClient.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if edHome_Flat.Text='' then edHome_Flat.Text:=' ';
{ if ModalResult = mrCancel then
 begin
  with dm, qutmp do
  begin
   sql.Text:='update sellitem set clientid=-1 where checkno = '+taSellItemCHECKNO.AsString;
   ExecQuery;
   dmcom.tr.CommitRetaining;
  end;
 end}
end;

procedure TfmeClient.edHome_FlatMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
lbaddress.Visible:=false;
end;

procedure TfmeClient.btnOkKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if not flbtnok then
 begin
  flNodCard:=false; 
  flFIO:=false;
  Fladdress:=false;
  flhome_flat:=false;
  flbtnok := true;
  flbtnNO := false;
 end;
end;

procedure TfmeClient.edHome_FlatKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flhome_flat then
 case key of
  VK_RETURN:  if dbBirthday.Enabled then  ActiveControl:=dbBirthday
              else ActiveControl:=btnOk;
  VK_UP:  ActiveControl:=edFIO;
//  else ActiveControl:=edHome_Flat;
 end;
end;

procedure TfmeClient.edAddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if not Fladdress then
 begin
  flNodCard:=false; 
  flFIO:=false;
  Fladdress:=true;
  flhome_flat:=false;
  flbtnok := false;
  flbtnNO := false;
 end;
end;

procedure TfmeClient.edHome_FlatKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if not flhome_flat then
 begin
  flNodCard:=false; 
  flFIO:=false;
  Fladdress:=false;
  flhome_flat:=true;
  flbtnok := false;
  flbtnNO := false;
 end;
end;

procedure TfmeClient.btnNoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if not flbtnNo then
 begin
  flNodCard:=false; 
  flFIO:=false;
  Fladdress:=false;
  flhome_flat:=false;
  flbtnok := false;
  flbtnNO := true;
 end;
end;

procedure TfmeClient.btnNoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flbtnNo then
 case key of
  VK_UP: ActiveControl:=edHome_Flat;
  VK_LEFT: ActiveControl:= btnOk;
 end;
end;

procedure TfmeClient.btnOkKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flbtnok then
 case key of
  VK_UP: ActiveControl:=edHome_Flat;
  VK_RIGHT: ActiveControl:= btnNo;
 end;
end;

procedure TfmeClient.edFIOKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flFIO and (not FirstST)then
 case key of
  VK_RETURN: begin
   ActiveControl:=edAddress;
   if CreatePmAddress(edAddress.EditText) then lbaddress.Visible:=true
   else lbaddress.Visible:=false;
  end;
  VK_UP:  if edNoDCard.Enabled then ActiveControl:=edNoDCard;
 end;
end;

procedure TfmeClient.edNoDCardKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 //������� �������� ������ ����
 if FirstST then FirstST:= not FirstST
 else
 if not flNodCard then
 begin
  flNodCard:=true;
  flFIO:=false;
  Fladdress:=false;
  flhome_flat:=false;
  flbtnok := false;
  flbtnNO := false;
 end;
end;

procedure TfmeClient.edNoDCardKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flNodCard and (not FirstST)then
 case key of
  VK_RETURN: IF edFIO.Enabled then ActiveControl:=edFIO
   else ActiveControl:=edAddress
 end;
end;

procedure TfmeClient.edNoDCardMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if flField then
  begin
   ActiveControl:=edAddress;
   if CreatePmAddress(edAddress.EditText) then lbaddress.Visible:=true
   else lbaddress.Visible:=false;
  end
 else lbaddress.Visible:=false;
end;

procedure TfmeClient.dbBirthdayKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if flhome_flat then
 case key of
  VK_RETURN:  ActiveControl:=btnOk;
  VK_UP:  ActiveControl:=edHome_Flat;
//  else ActiveControl:=edHome_Flat;
 end;
end;

end.
