object fmApplList: TfmApplList
  Left = 173
  Top = 260
  HelpContext = 100281
  Caption = #1047#1072#1103#1074#1082#1080
  ClientHeight = 446
  ClientWidth = 804
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 804
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1092#1086#1088#1084#1091
      ImageIndex = 0
      Spacing = 1
      Left = 602
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7CE07FEF3D00001F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00000000E07F1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DFF7F007C007C007C007C007CFF7FEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      Action = acCloseAppl
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      ImageIndex = 5
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      OnClick = acCloseApplExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrintAppl
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acPrintApplExecute
      SectionName = 'Untitled (0)'
    end
    object siExport: TSpeedItem
      Action = acExportToText
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      Caption = 'siExport'
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = acExportToTextExecute
      SectionName = 'Untitled (0)'
    end
    object siMerge: TSpeedItem
      Action = acMerge
      BtnCaption = #1054#1073#1098#1077#1076'.'
      Caption = 'siMerge'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFF00FFFF00FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000FF0000
        FF0000FF000000FFFFFFFF00FFFF00FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF0000000000FF0000FF0000FF000000FFFFFFFF00FFFF00FF
        FF00FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000FF0000
        FF0000FF000000FFFFFFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000000000000000000000000000FFFFFFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000000000000000000000000000FF00FFFF00FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00
        FFFF00FFFF00000000FF00FFFF00FFFF00FFFFFFFFFFFFFFFFFFFF0000000000
        00000000FFFFFFFFFFFF000000FFFF00FFFF00FFFF00000000000000000000FF
        00FFFF00FFFFFFFF000000000000000000000000000000FFFFFF000000FFFF00
        FFFF00FFFF00000000FFFF00000000FF00FFFF00FF0000000000000000000000
        00000000000000000000000000000000000000000000000000FFFF0000000000
        0000000000FF00FFFF00FF000000000000000000FFFFFFFFFFFFFF00FFFF00FF
        000000FFFF00FFFF00FFFF00000000FFFF00000000FF00FFFF00FF0000000000
        00000000FFFFFFFFFFFFFF00FFFF00FF000000000000000000000000000000FF
        FF00000000FF00FFFF00FF000000000000000000FFFFFFFFFFFFFF00FFFF00FF
        FF00FFFF00FF000000FFFF00FFFF00FFFF00000000FF00FF0000000000000000
        00FF00FFFFFFFFFFFFFFFF00FFFF00FFFF00FFFF00FF00000000000000000000
        0000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFFFF}
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      OnClick = acMergeExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1072#1088#1072#1074#1082#1072
      Hint = #1057#1087#1072#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 538
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      Spacing = 1
      Left = 450
      Top = 2
      Visible = True
      WordWrap = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 804
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object cbSup: TComboBox
      Left = 10
      Top = 3
      Width = 215
      Height = 21
      Color = clInfoBk
      ItemHeight = 13
      TabOrder = 0
      Text = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
      OnChange = cbSupChange
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 306
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridAppl: TDBGridEh
    Left = 0
    Top = 70
    Width = 804
    Height = 357
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsrApplList
    Flat = True
    FooterColor = clBtnFace
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    PopupMenu = pmdg1
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = acViewExecute
    OnGetCellParams = gridApplGetCellParams
    OnKeyDown = gridApplKeyDown
    OnMouseDown = gridApplMouseDown
    OnMouseMove = gridApplMouseMove
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NOAPPL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footer.FieldName = 'NOAPPL'
        Footer.ValueType = fvtCount
        Footers = <>
        ReadOnly = True
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 50
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ZDATE'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footers = <>
        ReadOnly = True
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1044#1072#1090#1072' '#1089#1086#1089#1090#1072#1074#1083#1077#1085#1080#1103
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1044#1072#1090#1072' '#1086#1090#1087#1088#1072#1074#1082#1080
        Width = 58
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SNAME'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1086#1079#1076#1072#1083
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 174
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ALLQ'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footer.FieldName = 'ALLQ'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1054#1088#1080#1077#1085#1090'. '#1074#1077#1089
      end
      item
        DisplayFormat = '0.00'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footer.FieldName = 'PR'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1088#1080#1077#1085#1090'.'#1089#1091#1084#1084#1072
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 86
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'F1'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077'|*'
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'F2'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077'|**'
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'ISUIDWH'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1047#1072#1103#1074#1082#1072' '#1089#1086#1079#1076#1072#1085#1072' '#1080#1079' '#1089#1082#1083#1072#1076#1072' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
        Title.EndEllipsis = True
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clBlack
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 70
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'ISSELL'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1047#1072#1103#1074#1082#1072' '#1089#1086#1079#1076#1072#1085#1072' '#1080#1079' '#1089#1084#1077#1085#1099
      end
      item
        EditButtons = <>
        FieldName = 'OP'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        Width = 144
      end
      item
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Title.Caption = #1057#1086#1079#1076#1072#1085#1072' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1077
        Width = 110
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 427
    Width = 804
    Height = 19
    Panels = <>
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 84
    Top = 132
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 84
    Top = 180
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1092#1086#1088#1084#1091
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acPrintAppl: TAction
      Caption = #1047#1072#1103#1074#1082#1072
      ImageIndex = 67
      OnExecute = acPrintApplExecute
      OnUpdate = acPrintApplUpdate
    end
    object acExportToText: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      OnExecute = acExportToTextExecute
      OnUpdate = acExportToTextUpdate
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acMerge: TAction
      Caption = #1054#1073#1098#1077#1076'.'
      ImageIndex = 27
      OnExecute = acMergeExecute
      OnUpdate = acMergeUpdate
    end
    object acSend: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1082#1072#1082' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1091#1102
      OnExecute = acSendExecute
      OnUpdate = acSendUpdate
    end
    object acNotSend: TAction
      Caption = #1057#1085#1103#1090#1100' '#1087#1086#1084#1077#1090#1082#1091' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1085#1086#1081
      OnExecute = acNotSendExecute
      OnUpdate = acNotSendUpdate
    end
    object acCloseAppl: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 5
      OnExecute = acCloseApplExecute
      OnUpdate = acCloseApplUpdate
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = gridAppl
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Title.Strings = (
      
        '                                                                ' +
        ' '#1057#1087#1080#1089#1086#1082' '#1079#1072#1103#1074#1086#1082)
    Units = MM
    Left = 312
    Top = 184
  end
  object pmdg1: TTBPopupMenu
    Left = 456
    Top = 200
    object biSend: TTBItem
      Action = acSend
    end
    object biNotSend: TTBItem
      Action = acNotSend
    end
  end
end
