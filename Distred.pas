unit Distred;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, 
  ExtCtrls, ComCtrls, db, M207DBCtrls, rxPlacemnt, rxSpeedbar;

type
  TfmDistred = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    M207IBGrid1: TM207IBGrid;
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Panel2: TPanel;
    M207DBPanel1: TM207DBPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    M207DBPanel2: TM207DBPanel;
    Panel5: TPanel;
    M207DBPanel3: TM207DBPanel;
    Panel6: TPanel;
    M207DBPanel4: TM207DBPanel;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;    
  public
    { Public declarations }
  end;

var
  fmDistred: TfmDistred;

implementation

uses Data, Data2, comdata, M207Proc;

{$R *.DFM}

procedure TfmDistred.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDistred.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDistred.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  Screen.Cursor:=crSQLWait;
  with dm, dm2 do
    begin
     with quDistred do
       begin
         Active:=False;
         Open;
       end;
     with quDistredT do
       begin
         Active:=False;
         Params[0].AsInteger:=quDistred.Tag;
         Active:=True;
       end;
    end;
  Screen.Cursor:=crDefault;    
end;

procedure TfmDistred.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmDistred.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key  of
      VK_F12: Close;
    end;  
end;

procedure TfmDistred.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dm2 do
    CloseDataSets([quDistred, quDistredT]);
end;

end.
