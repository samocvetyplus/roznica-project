unit WriteLogBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, FIBDatabase, pFIBDatabase,
  FIBQuery, pFIBQuery, IniFiles, DB, FIBDataSet, pFIBDataSet, rxToolEdit;

type
  TfmWriteLogBase = class(TForm)
    cbDB: TGroupBox;
    pbutton: TPanel;
    pInfo: TPanel;
    btok: TButton;
    btClose: TButton;
    btTest: TButton;
    filenamedblog: TFilenameEdit;
    LBD: TLabel;
    lbUserName: TLabel;
    edtUserName: TEdit;
    lbPsw: TLabel;
    edtPw: TEdit;
    lbRole: TLabel;
    edtRole: TEdit;
    lbCharSet: TLabel;
    cbCharset: TComboBox;
    memAddInfo: TMemo;
    lbAddInf: TLabel;
    dbLog: TpFIBDatabase;
    trLog: TpFIBTransaction;
    qutmpLog: TpFIBQuery;
    chbDel: TCheckBox;
    debd: TDateEdit;
    deed: TDateEdit;
    LBDate: TLabel;
    LEDate: TLabel;
    quLogUser: TpFIBDataSet;
    quLogOperation: TpFIBDataSet;
    quLogSysSql: TpFIBDataSet;
    quLogBlobFields: TpFIBDataSet;
    quLogUserL_USERID: TFIBIntegerField;
    quLogUserHOST: TFIBStringField;
    quLogUserCONNCTTIME: TFIBStringField;
    quLogUserSYSUSER: TFIBStringField;
    quLogUserDISCRIPT: TFIBStringField;
    quLogUserCURR_CONTID: TFIBIntegerField;
    quLogUserUSERNAME: TFIBStringField;
    quLogOperationL_USERID: TFIBIntegerField;
    quLogOperationL_OPRID: TFIBIntegerField;
    quLogOperationOPERTIME: TFIBDateTimeField;
    quLogOperationOPERTYPE: TFIBStringField;
    quLogOperationOPER_ENTER: TFIBIntegerField;
    quLogOperationREF: TFIBIntegerField;
    quLogOperationOPERTIME_ED: TFIBDateTimeField;
    quLogOperationTREE_LEVEL: TFIBIntegerField;
    quLogOperationCHILD: TFIBIntegerField;
    quLogSysSqlL_SYSSQL: TFIBIntegerField;
    quLogSysSqlTABLENAME: TFIBStringField;
    quLogSysSqlL_OPRID: TFIBIntegerField;
    quLogSysSqlL_USERID: TFIBIntegerField;
    quLogSysSqlDATA: TFIBStringField;
    quLogSysSqlOPTYPE: TFIBStringField;
    quLogSysSqlOPDATE: TFIBDateTimeField;
    quLogBlobFieldsL_BLOBFIELDSID: TFIBIntegerField;
    quLogBlobFieldsOLDVALUE: TFIBBlobField;
    quLogBlobFieldsNEWVALUE: TFIBBlobField;
    quLogBlobFieldsSYSSQL_ID: TFIBIntegerField;
    qutmp: TpFIBQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btTestClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure btCloseClick(Sender: TObject);
  private
   Fdel:boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmWriteLogBase: TfmWriteLogBase;

implementation

uses data3, comdata, dbUtil, MsgDialog;

{$R *.dfm}

procedure TfmWriteLogBase.FormCreate(Sender: TObject);
var IniF: TIniFile;
    St1:tstringList;
    i:integer;
    st:string;
begin
  {���������� ���������� �� ini �����}
  IniF := TIniFile.Create(  ExtractFilePath(Application.ExeName)+'setting.ini');
  st1:= TStringList.Create;

  IniF.ReadSectionValues('DATABASELOG', st1);

  filenamedblog.Text:=st1.Values['DBFILE'];
  edtUserName.Text:=st1.Values['USERNAME'];
  edtPw.Text:=st1.Values['PASSWORD'];
  edtRole.Text:=st1.Values['ROLE'];
  memAddInfo.Text:=st1.Values['ADDPARAM'];
  st:=st1.Values['CHARSET'];
  i:=cbCharset.Items.IndexOf(st);
  if i>-1 then cbCharset.ItemIndex:=i else cbCharset.ItemIndex:=0;
  fdel:=false;
  if st1.Values['FDELLOG']='1' then fdel:=true;
  chbDel.Checked:=fdel;

  st1.Free;
  IniF.Free;

  debd.Date:=dmCom.FirstMonthDate;
  deed.Date:=dmcom.GetServerTime;
end;

procedure TfmWriteLogBase.FormClose(Sender: TObject;
  var Action: TCloseAction);
var IniF: TIniFile;
begin
  dbLog.Connected:=false;
  {���������� ���������� �� ini �����}
  IniF := TIniFile.Create(  ExtractFilePath(Application.ExeName)+'setting.ini');
  IniF.WriteString('DATABASELOG','DBFILE',filenamedblog.Text);
  IniF.WriteString('DATABASELOG','USERNAME',edtUserName.Text);
  IniF.WriteString('DATABASELOG','PASSWORD',edtPw.Text);
  IniF.WriteString('DATABASELOG','ROLE',edtRole.Text);
  IniF.WriteString('DATABASELOG','ADDPARAM',memAddInfo.Text);
  IniF.WriteString('DATABASELOG','CHARSET',cbCharset.Text);
  if fdel then IniF.WriteString('DATABASELOG','FDELLOG','1')
  else IniF.WriteString('DATABASELOG','FDELLOG','0');

  IniF.Free;
end;

procedure TfmWriteLogBase.btTestClick(Sender: TObject);
begin
  with dbLog,DBParams do
  begin
   try
    Connected:=False;
    DatabaseName:=filenamedblog.Text;
    Values['user_name'] := edtUserName.Text;
    Values['password'] := edtPw.Text;
    Values['lc_ctype'] := cbCharset.Text;
    Values['sql_role_name'] := edtRole.Text;
    Add(memAddInfo.Text);
    Connected:=True;
    MessageDialog('������������� � ���� ������ �������', mtInformation, [mbOk], 0);
    Connected:=False;
   except
     on E:Exception do
       MessageDialog('������ ��� ����������� � ��������� ���� ������:'#13+E.Message, mtError, [mbOk], 0);
   end
  end;
end;

procedure TfmWriteLogBase.btokClick(Sender: TObject);
var fl:boolean;
begin
  with dbLog,DBParams do
  begin
   try
    Connected:=False;
    DatabaseName:=filenamedblog.Text;
    Values['user_name'] := edtUserName.Text;
    Values['password'] := edtPw.Text;
    Values['lc_ctype'] := cbCharset.Text;
    Values['sql_role_name'] := edtRole.Text;
    Add(memAddInfo.Text);
    Connected:=True;
   except
    on E:Exception do
      MessageDialog('������ ��� ����������� � ��������� ��:'#13+E.Message, mtError, [mbOk], 0);
   end
  end;
 fdel:=chbDel.Checked;
 
 if not trLog.Active then trLog.StartTransaction;

 fl:=false;
 try
  quLogUser.Active:=false;
  quLogUser.ParamByName('BD').AsDateTime:=debd.Date;
  quLogUser.ParamByName('ED').AsDateTime:=deed.Date+0.9999;
  quLogUser.Active:=true;
  while not quLogUser.Eof do
  begin
   qutmpLog.close;
   qutmpLog.ParamByName('FTABLE').AsInteger:=0;
   qutmpLog.ParamByName('L_USERID').Value:=quLogUserL_USERID.Value;
   qutmpLog.ParamByName('HOST').Value:=quLogUserHOST.Value;
   qutmpLog.ParamByName('CONNCTTIME').Value:=quLogUserCONNCTTIME.Value;
   qutmpLog.ParamByName('SYSUSER').Value:=quLogUserSYSUSER.Value;
   qutmpLog.ParamByName('DISCRIPT').Value:=quLogUserDISCRIPT.Value;
   qutmpLog.ParamByName('CURR_CONTID').Value:=quLogUserCURR_CONTID.Value;
   qutmpLog.ParamByName('USERNAME').Value:=quLogUserUSERNAME.Value;

   qutmpLog.ExecQuery;
   qutmpLog.Transaction.CommitRetaining;
   qutmpLog.Close;
   quLogUser.Next;
  end;
  quLogUser.Active:=false;
  dmcom.tr.CommitRetaining;
 except
  MessageDialog('������ ��� ���������� �������� � ������� LOG$USER', mtError, [mbOk], 0);
  fl:=true;
 end;

 try
  quLogOperation.Active:=false;
  quLogOperation.ParamByName('BD').AsDateTime:=debd.Date;
  quLogOperation.ParamByName('ED').AsDateTime:=deed.Date+0.9999;
  quLogOperation.Active:=true;
  while not quLogOperation.Eof do
  begin
   qutmpLog.close;
   qutmpLog.ParamByName('FTABLE').AsInteger:=1;
   qutmpLog.ParamByName('L_USERID').Value:=quLogOperationL_USERID.Value;
   qutmpLog.ParamByName('L_OPRID').Value:=quLogOperationL_OPRID.Value;
   qutmpLog.ParamByName('OPERTIME').Value:=quLogOperationOPERTIME.Value;
   qutmpLog.ParamByName('OPERTYPE').Value:=quLogOperationOPERTYPE.Value;
   qutmpLog.ParamByName('OPER_ENTER').Value:=quLogOperationOPER_ENTER.Value;
   qutmpLog.ParamByName('REF').Value:=quLogOperationREF.Value;
   qutmpLog.ParamByName('OPERTIME_ED').Value:=quLogOperationOPERTIME_ED.Value;
   qutmpLog.ParamByName('TREE_LEVEL').Value:=quLogOperationTREE_LEVEL.Value;
   qutmpLog.ParamByName('CHILD').Value:=quLogOperationCHILD.Value;

   qutmpLog.ExecQuery;
   qutmpLog.Transaction.CommitRetaining;
   qutmpLog.Close;
   quLogOperation.Next;
  end;
  quLogOperation.Active:=false;
  dmcom.tr.CommitRetaining;
 except
   MessageDialog('������ ��� ���������� �������� � ������� LOG$OPERATION', mtError, [mbOk], 0);
  fl:=true;
 end;

 try
  quLogSysSql.Active:=false;
  quLogSysSql.ParamByName('BD').AsDateTime:=debd.Date;
  quLogSysSql.ParamByName('ED').AsDateTime:=deed.Date+0.9999;
  quLogSysSql.Active:=true;
  while not quLogSysSql.Eof do
  begin
   qutmpLog.close;
   qutmpLog.ParamByName('FTABLE').AsInteger:=2;
   qutmpLog.ParamByName('L_USERID').Value:=quLogSysSqlL_USERID.Value;
   qutmpLog.ParamByName('L_OPRID').Value:=quLogSysSqlL_OPRID.Value;
   qutmpLog.ParamByName('L_SYSSQL').Value:=quLogSysSqlL_SYSSQL.Value;
   qutmpLog.ParamByName('TABLENAME').Value:=quLogSysSqlTABLENAME.Value;
   qutmpLog.ParamByName('DATA').Value:=quLogSysSqlDATA.Value;
   qutmpLog.ParamByName('OPTYPE').Value:=quLogSysSqlOPTYPE.Value;
   qutmpLog.ParamByName('OPDATE').Value:=quLogSysSqlOPDATE.Value;
   qutmpLog.ExecQuery;
   qutmpLog.Transaction.CommitRetaining;
   quLogSysSql.Next;
  end;
  quLogSysSql.Active:=false;
  dmcom.tr.CommitRetaining;
 except
   MessageDialog('������ ��� ���������� �������� � ������� LOG$SYSSQL', mtError, [mbOk], 0);
  fl:=true;
 end;

 try
  quLogBlobFields.Active:=false;
  quLogBlobFields.ParamByName('BD').AsDateTime:=debd.Date;
  quLogBlobFields.ParamByName('ED').AsDateTime:=deed.Date+0.9999;
  quLogBlobFields.Active:=true;
  while not quLogBlobFields.Eof do
  begin
   qutmpLog.close;
   qutmpLog.ParamByName('FTABLE').AsInteger:=3;
   qutmpLog.ParamByName('L_BLOBFIELDSID').Value:=quLogBlobFieldsL_BLOBFIELDSID.Value;
   qutmpLog.ParamByName('OLDVALUE').Value:=quLogBlobFieldsOLDVALUE.Value;
   qutmpLog.ParamByName('NEWVALUE').Value:=quLogBlobFieldsNEWVALUE.Value;
   qutmpLog.ParamByName('SYSSQL_ID').Value:=quLogBlobFieldsSYSSQL_ID.Value;
   qutmpLog.ExecQuery;
   qutmpLog.Transaction.CommitRetaining;
   qutmpLog.Close;
   quLogBlobFields.Next;
  end;
  quLogBlobFields.Active:=false;
  dmcom.tr.CommitRetaining;
 except
   MessageDialog('������ ��� ���������� �������� � ������� LOG$BLOBFIELDS', mtError, [mbOk], 0);
  fl:=true;
 end;

 qutmp.Close;
 if fdel then qutmp.SQL.Text:='delete from log$user where rstate=3'
 else qutmp.SQL.Text:='update log$user set rstate=0 where rstate=3';
 qutmp.ExecQuery;
 qutmp.Transaction.CommitRetaining;
 qutmp.Close;

 if not fl then MessageDialog('�������� ������ ��������� �������!', mtInformation, [mbOk], 0)
 else MessageDialog('������ ��� ��������', mtError, [mbOk], 0);

 dblog.Connected:=false;
end;

procedure TfmWriteLogBase.btCloseClick(Sender: TObject);
begin
 close;
end;

end.
