unit RepSetting;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, StdCtrls, DBCtrls, Mask, ActnList,
  DBGridEhGrouping, GridsEh;

type
  TfmRepSetting = class(TForm)
    dg1: TDBGridEh;
    pInfo: TPanel;
    dbIsrecord: TDBEdit;
    dbR_GenOffSet: TDBEdit;
    dbR_UserDep: TDBCheckBox;
    dbHere: TDBCheckBox;
    LIsRecord: TLabel;
    btAddRecord: TButton;
    btDelRecord: TButton;
    acList: TActionList;
    acAddRecord: TAction;
    acDelRecord: TAction;
    LGenOffSet: TLabel;
    dbIsftpRep: TDBCheckBox;
    btGenSetting: TButton;
    acGenStting: TAction;
    procedure acAddRecordUpdate(Sender: TObject);
    procedure acDelRecordUpdate(Sender: TObject);
    procedure acAddRecordExecute(Sender: TObject);
    procedure acDelRecordExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acGenSttingUpdate(Sender: TObject);
    procedure acGenSttingExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepSetting: TfmRepSetting;

implementation
uses comdata, data3, M207Proc, DB, dbUtil, data;
{$R *.dfm}

procedure TfmRepSetting.acAddRecordUpdate(Sender: TObject);
begin
 if dm3.quRepSettingISRECORD.AsInteger=0 then acAddRecord.Enabled:=true
 else acAddRecord.Enabled:=false;

 dbR_GenOffSet.Enabled:=not acAddRecord.Enabled;
 dbR_UserDep.Enabled:=not acAddRecord.Enabled;
 dbHere.Enabled:=not acAddRecord.Enabled;
 dbIsftpRep.Enabled:=not acAddRecord.Enabled; 
end;

procedure TfmRepSetting.acDelRecordUpdate(Sender: TObject);
begin
 if (dm3.quRepSettingISRECORD.AsInteger=1) and
    (dm3.quRepSettingHERE.AsInteger<>1) then acDelRecord.Enabled:=true
 else acDelRecord.Enabled:=false;
end;

procedure TfmRepSetting.acAddRecordExecute(Sender: TObject);
begin
 PostDataSets([dm3.quRepSetting]);
 ExecSQL('execute procedure REPSETTING_I ('+dm3.quRepSettingD_DEPID.AsString+')', dm3.quTmp);
 dm3.quRepSetting.Refresh;
end;

procedure TfmRepSetting.acDelRecordExecute(Sender: TObject);
begin
 PostDataSets([dm3.quRepSetting]);
 ExecSQL('execute procedure REPSETTING_D ('+dm3.quRepSettingD_DEPID.AsString+')', dm3.quTmp);
 dm3.quRepSetting.Refresh;
end;

procedure TfmRepSetting.FormCreate(Sender: TObject);
begin
 ReOpenDataSets([dm3.quRepSetting]);
end;

procedure TfmRepSetting.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 PostDataSets([dm3.quRepSetting]);
 CloseDataSets([dm3.quRepSetting]);
end;

procedure TfmRepSetting.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<> nil then
 begin
  Background:=dm3.quRepSettingCOLOR.AsInteger;
 end
end;

procedure TfmRepSetting.acGenSttingUpdate(Sender: TObject);
begin
 if dm3.quRepSettingD_DEPID.AsInteger=SelfDepId then acGenStting.Enabled:=true
 else acGenStting.Enabled:=false;
end;

procedure TfmRepSetting.acGenSttingExecute(Sender: TObject);
var d, val:integer;
begin
 with dm3 do
 begin
  quGenSetting.close;
  quGenSetting.ExecQuery;
  while not quGenSetting.Eof do
  begin
   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select gen_id('+quGenSetting.Fields[0].AsString+', 0) from d_rec';
   dm.quTmp.ExecQuery;
   d:=dm.quTmp.Fields[0].AsInteger;
   val:=quRepSettingR_GENOFFSET.AsInteger-d;
   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select gen_id('+quGenSetting.Fields[0].AsString+', '+inttostr(val)+') from d_rec';
   dm.quTmp.ExecQuery;
   dm.quTmp.Transaction.CommitRetaining;
   dm.quTmp.Close;
   quGenSetting.Next;
  end;
  quGenSetting.Close;
 end
end;

end.
