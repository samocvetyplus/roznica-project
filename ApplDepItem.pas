unit ApplDepItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, StdCtrls, DBTree, 
  M207Ctrls, ActnList, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmApplDepItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    pSelect: TPanel;
    dg1: TDBGridEh;
    sp1: TSplitter;
    lbComp: TListBox;
    Splitter3: TSplitter;
    lbCountry: TListBox;
    Splitter8: TSplitter;
    lbGood: TListBox;
    Splitter4: TSplitter;
    lbIns: TListBox;
    Splitter10: TSplitter;
    lbMat: TListBox;
    Splitter5: TSplitter;
    lbAtt1: TListBox;
    Splitter11: TSplitter;
    lbAtt2: TListBox;
    Splitter1: TSplitter;
    pSelectUid: TPanel;
    Panel1: TPanel;
    edArt: TEdit;
    LArt: TLabel;
    LUID: TLabel;
    edUid: TEdit;
    dgWH: TDBGridEh;
    fr: TM207FormStorage;
    acList: TActionList;
    acDel: TAction;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure lbCompClick(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edUidKeyPress(Sender: TObject; var Key: Char);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgWHDblClick(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmApplDepItem: TfmApplDepItem;

implementation

uses comdata, data3, M207Proc, data, data2, AddApplItem, DB, MsgDialog,
  uUtils;

{$R *.dfm}

procedure TfmApplDepItem.FormCreate(Sender: TObject);
var c: TColumnEh;
    i:integer;
begin
 tb1.WallPaper:=wp;
 REOpenDataSets([dm3.quApplDepItem]);
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
//**********************//
 dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, lbAtt1, lbAtt2);
 lbComp.ItemIndex:=0;
 lbMat.ItemIndex:=0;
 lbGood.ItemIndex:=0;
 lbIns.ItemIndex:=0;
 lbCountry.ItemIndex:=0;
 lbAtt1.ItemIndex := 0;
 lbAtt2.ItemIndex := 0;
 dmcom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
 dmcom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
 dmcom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
 dmcom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
 dmcom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
 dmcom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
 dmcom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
 //*******************//
  with dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dgWH.Columns.Add;
          c.Field:=quD_WH2.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RW_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;

          c:=dgWH.Columns.Add;
          c.Field:=quD_WH2.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Items[0].FieldName:='A_'+slDepDepId[i];
          c.Footers.Items[0].ValueType:=fvtSum;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RQ_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;

        end;
    end;

 if (dm3.quApplDepListMODEID.AsInteger<>SelfDepId) and Centerdep then
 begin
  dg1.FieldColumns['REFUSAL'].Visible:=true;
  if (dm3.quApplDepListISCLOSED.AsInteger=0) and (dm3.quApplDepListEDITTR.AsInteger=0) then
   dg1.FieldColumns['REFUSAL'].ReadOnly:=false
  else
   dg1.FieldColumns['REFUSAL'].ReadOnly:=true;  
 end
 else begin
  dg1.FieldColumns['REFUSAL'].Visible:=false;
  dg1.FieldColumns['REFUSAL'].ReadOnly:=true;
 end;
 dgWh.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dgwh', [crpColIndexEh, crpColWidthsEh]);
end;

procedure TfmApplDepItem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 PostDataSets([dm3.quApplDepItem]);
 CloseDataSets([dm3.quApplDepItem, dm2.quD_WH2]);
 dgwh.SaveColumnsLayoutIni(GetIniFileName, Name+'_dgwh', true);
end;

procedure TfmApplDepItem.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmApplDepItem.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
end;

procedure TfmApplDepItem.dgWHGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 with dm2 do
  if Column.Field.Tag>0 then Background:=GetDepColor(Column.Field.Tag)
end;

procedure TfmApplDepItem.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
  VK_RETURN: begin
              dm.D_WHArt:=edArt.Text;
              ReopenDataSets([dm2.quD_WH2]);
             end
 end;
end;

procedure TfmApplDepItem.edUidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else SysUtils.Abort;
 end
end;

procedure TfmApplDepItem.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN:
  begin
   if edUid.Text='' then exit;
   if  dm3.quApplDepListISCLOSED.asInteger=1 then
   begin
    MessageDialog('������ �������!', mtError, [mbOk], 0);
    edUid.Text:='';
    exit;
   end;
   if  dm3.quApplDepListMODEID.AsInteger<>SelfDepId then
   begin
    MessageDialog('����� ���� ������� �� ������ �������!', mtInformation, [mbOk], 0);
    edUid.Text:='';
    exit;
   end;
   dm3.quSelectUid.Close;
   dm3.quSelectUid.ParamByName('UID').AsString:=eduid.Text;
   dm3.FUIDApllDep:=true;
   dm3.quSelectUid.Open;
   if dm3.quSelectUidUID.IsNull then
   begin
    MessageDialog('������� �� ������� �� �������', mtError, [mbOk], 0);
    dm3.quSelectUid.Close;
    edUid.Text:='';
    exit;
   end;
   {************}
   if dm3.quSelectUidFEXISTS.AsInteger=0 then
   begin
    MessageDialog('������ ������� ��� ����������� �� ������� �� �������', mtError, [mbOk], 0);
    dm3.quSelectUid.Close;
    edUid.Text:='';
    exit;
   end;
   {************}
   dm3.quApplDepItem.Insert;
   dm3.quApplDepItemART2ID.AsInteger:=dm3.quSelectUidART2ID.AsInteger;
   dm3.quApplDepItemUID.AsInteger:=dm3.quSelectUidUID.AsInteger;
   dm3.quApplDepItemSZ.AsString:=dm3.quSelectUidSZ.AsString;
   dm3.quApplDepItemW.AsFloat:=dm3.quSelectUidW.AsFloat;
   dm3.quApplDepItemCOST.AsFloat:=dm3.quSelectUidCOST.AsFloat;
   dm3.quApplDepItemCOSTP.AsFloat:=dm3.quSelectUidCOSTP.AsFloat;
   dm3.quApplDepItem.Post;
   dm3.quSelectUid.Close;
   edUid.Text:='';
  end;
 end;
end;

procedure TfmApplDepItem.dgWHDblClick(Sender: TObject);
var bookMark: TBookmark;
begin
 BookMark:=dm3.quApplDepItem.GetBookmark;
 try
   ShowAndFreeForm(TfmAddApplDepItem, Self, TForm(fmAddApplDepItem), True, False);
   dm3.FUIDApllDep:=false;
   ReOpenDataSets([dm3.quApplDepItem]);
   dm3.quApplDepItem.GotoBookmark(bookMark);
 finally
   dm3.quApplDepItem.FreeBookmark(bookMark);
 end;
end;

procedure TfmApplDepItem.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmApplDepItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmApplDepItem.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=(dm3.quApplDepListISCLOSED.AsInteger=0) and
  (dm3.quApplDepListMODEID.AsInteger=SelfDepId) and
  (not dm3.quApplDepItemAPPLDEPID.IsNull);
end;

procedure TfmApplDepItem.acDelExecute(Sender: TObject);
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort
 else dm3.quApplDepItem.Delete;
end;

procedure TfmApplDepItem.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if dm3.quApplDepItemREFUSAL.AsInteger=0 then Background:=clWindow
 else Background:=$008000FF
end;

procedure TfmApplDepItem.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100292)
end;

procedure TfmApplDepItem.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
