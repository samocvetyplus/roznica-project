object fmSRetList: TfmSRetList
  Left = 206
  Top = 148
  HelpContext = 100211
  Caption = #1042#1086#1079#1074#1088#1072#1090#1099' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
  ClientHeight = 538
  ClientWidth = 786
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 786
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 690
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAddJew
      BtnCaption = #1057#1072#1084#1086#1094#1074#1077#1090#1099
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddJewExecute
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7CE07FEF3D00001F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00000000E07F1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DFF7F007C007C007C007C007CFF7FEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1044#1077#1081#1089#1090#1074#1080#1077
      Caption = #1044#1077#1081#1089#1090#1074#1080#1077
      DropDownMenu = ppAdditional
      Hint = #1044#1077#1081#1089#1090#1074#1080#1077'|'
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 626
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object siCheck: TSpeedItem
      Action = acCheck
      BtnCaption = #1055#1088#1086#1074#1077#1088#1082#1072
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FF00FF00FF00FF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF0000008400FF00FF00FF00FF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00000084000000840000008400FF00FF00FF00
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF000000840000008400FFFFFF000000840000008400FF00
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084008484
        8400FF00FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
        8400FF00FF00FF00FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF0000008400FF00FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000008400FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF0084848400FFFFFF00FFFFFF00000000000000000000000000000000000000
        000000000000FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00848484008484840000000000FFFFFF00FF00FF00FF00FF00FF00
        FF00000000008484840084848400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF008484840084848400848484008484
        8400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acCheckExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      WordWrap = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 786
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbDepName: TLabel
      Left = 8
      Top = 8
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 306
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridSRetList: TDBGridEh
    Left = 0
    Top = 69
    Width = 786
    Height = 450
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSRetList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 3
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    PopupMenu = ppSRetList
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = gridSRetListDblClick
    OnGetCellParams = gridSRetListGetCellParams
    OnKeyDown = gridSRetListKeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SINVID'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.EndEllipsis = True
        Title.TitleButton = True
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'CountClose'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'CountOpen'
            ValueType = fvtSum
          end
          item
            FieldName = 'SINVID'
            ValueType = fvtCount
          end>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <
          item
            Color = clBtnFace
            Value = #1047#1072#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Color = clRed
            Value = #1054#1090#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Value = #1042#1089#1077#1075#1086
            ValueType = fvtStaticText
          end>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 69
      end
      item
        EditButtons = <>
        FieldName = 'DEPFROM'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1060#1080#1083#1080#1072#1083
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 120
      end
      item
        EditButtons = <>
        FieldName = 'COMP'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 123
      end
      item
        EditButtons = <>
        FieldName = 'R_COUNT'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'QClose'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'QOpen'
            ValueType = fvtSum
          end
          item
            FieldName = 'R_COUNT'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'WClose'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'WOpen'
            ValueType = fvtSum
          end
          item
            FieldName = 'W'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1077#1089
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'CostClose'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'CostOpen'
            ValueType = fvtSum
          end
          item
            FieldName = 'COST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'TR'
        Footer.FieldName = 'TR'
        Footer.ValueType = fvtSum
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1058#1088#1072#1085#1089#1087'. '#1088#1072#1089#1093#1086#1076#1099
        Title.EndEllipsis = True
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'CONTRACT$CLASS$NAME'
        Footers = <>
        Title.Caption = #1044#1086#1075#1086#1074#1086#1088
        Title.TitleButton = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 519
    Width = 786
    Height = 19
    Panels = <>
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 20
    Top = 132
  end
  object ppRet: TPopupMenu
    Left = 176
    Top = 156
    object N6: TMenuItem
      Action = acAddJew
      Default = True
    end
    object mnitCDM: TMenuItem
      Tag = 311
      Caption = #1062#1044#1052
    end
    object mnitOther: TMenuItem
      Caption = #1044#1088#1091#1075#1086#1081
    end
  end
  object svdFile: TSaveDialog
    DefaultExt = 'jew'
    Left = 20
    Top = 228
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 24
    Top = 180
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acAddJew: TAction
      Caption = #1057#1072#1084#1086#1094#1074#1077#1090#1099
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddJewExecute
      OnUpdate = acAddJewUpdate
    end
    object acCreateSInvBuSRet: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1087#1086#1089#1090#1072#1074#1082#1091' '#1087#1086' '#1074#1086#1079#1074#1088#1072#1090#1091
      OnExecute = acCreateSInvBuSRetExecute
    end
    object acExportToTxt: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      OnExecute = acExportToTxtExecute
      OnUpdate = acExportToTxtUpdate
    end
    object acChangeSDate: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1076#1072#1090#1091' '#1086#1090#1082#1088#1099#1090#1099#1093' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1076#1072#1090#1091' '#1074' '#1086#1090#1082#1088#1099#1090#1099#1093' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
      OnExecute = acChangeSDateExecute
    end
    object acPrintFacture: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072
      OnExecute = acPrintExecute
      OnUpdate = acPrintUpdate
    end
    object acPrintInvoiceUID: TAction
      Tag = 1
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086
      OnExecute = acPrintExecute
    end
    object acPrintInvoiceArt: TAction
      Tag = 2
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1087#1086'-'#1072#1088#1090#1080#1082#1091#1083#1100#1085#1086
      OnExecute = acPrintExecute
    end
    object acPrintInvoiceUID2: TAction
      Tag = 3
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086' (+0,01%)'
      OnExecute = acPrintExecute
    end
    object acPrintGrid: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
    object acPrintActRet: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1040#1082#1090' '#1074#1086#1079#1074#1088#1072#1090#1072
      OnExecute = acPrintActRetExecute
      OnUpdate = acPrintActRetUpdate
    end
    object acCheck: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072
      ImageIndex = 71
      OnExecute = acCheckExecute
    end
    object acDelCheck: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
      OnExecute = acDelCheckExecute
    end
    object acPrintInvoice12sup: TAction
      Tag = 4
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1058#1054#1056#1043'12 ('#1089' '#1087#1086#1089#1090#1072#1074#1082#1086#1081')'
      OnExecute = acPrintExecute
    end
    object acPrintInvoiceArt2: TAction
      Tag = 5
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1058#1054#1056#1043'12('#1074' '#1088#1072#1089#1093'.'#1094#1077#1085#1072#1093')'
      OnExecute = acPrintExecute
    end
  end
  object ppSRetList: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 116
    Top = 156
    object TBSubmenuItem1: TTBSubmenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      object TBItem8: TTBItem
        Action = acAddJew
      end
    end
    object TBItem6: TTBItem
      Action = acView
    end
    object TBItem5: TTBItem
      Action = acDel
    end
    object TBSubmenuItem2: TTBSubmenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      object TBItem19: TTBItem
        Action = acPrintInvoiceArt
      end
      object TBItem18: TTBItem
        Action = acPrintInvoiceUID
      end
      object TBItem17: TTBItem
        Action = acPrintInvoiceUID2
      end
      object TBItem20: TTBItem
        Action = acPrintFacture
      end
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object TBItem11: TTBItem
      Caption = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem10: TTBItem
      Action = acChangeSDate
      Hint = #1048#1079#1084#1077#1085#1080#1090#1100' '#1076#1072#1090#1091' '#1086#1090#1082#1088#1099#1090#1099#1093' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
    end
    object TBItem12: TTBItem
      Action = acCreateSInvBuSRet
    end
    object TBItem13: TTBItem
      Action = acExportToTxt
    end
  end
  object ppPrint: TTBPopupMenu
    Left = 292
    Top = 24
    object TBItem4: TTBItem
      Action = acPrintInvoiceArt
    end
    object TBItem21: TTBItem
      Action = acPrintActRet
    end
    object TBItem9: TTBItem
      Action = acPrintInvoice12sup
    end
    object TBItem22: TTBItem
      Action = acPrintInvoiceArt2
    end
  end
  object ppAdditional: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 176
    Top = 208
    object TBItem16: TTBItem
      Action = acChangeSDate
    end
    object TBItem14: TTBItem
      Action = acCreateSInvBuSRet
    end
    object TBItem15: TTBItem
      Action = acExportToTxt
    end
    object TBItem7: TTBItem
      Action = acDelCheck
    end
  end
  object pgPrint: TPrintDBGridEh
    DBGridEh = gridSRetList
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 392
    Top = 224
  end
end
