unit BalanceB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TB2Item, TB2Dock, TB2Toolbar, ActnList, ComCtrls, Grids,
  DBGridEh, DB, FIBDataSet, pFIBDataSet, StdCtrls, Mask, DBCtrlsEh,
  M207Ctrls, DBGridEhGrouping, rxPlacemnt, GridsEh;

type
  TfmBalanceB = class(TForm)
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    gridBalanceB: TDBGridEh;
    StatusBar1: TStatusBar;
    taBalanceB: TpFIBDataSet;
    dsrBalanceB: TDataSource;
    taComp: TpFIBDataSet;
    taCompD_COMPID: TFIBIntegerField;
    taCompNAME: TFIBStringField;
    taBalanceBID: TFIBIntegerField;
    taBalanceBCOMPID: TFIBIntegerField;
    taBalanceBC: TFIBFloatField;
    taBalanceBCOMPNAME: TFIBStringField;
    taRec: TpFIBDataSet;
    taRecBB: TFIBDateTimeField;
    dsrRec: TDataSource;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    edBB: TDBDateTimeEditEh;
    TBControlItem2: TTBControlItem;
    taCompSNAME: TFIBStringField;
    acClose: TAction;
    fr1: TM207FormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure PastBeforeClose(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taBalanceBNewRecord(DataSet: TDataSet);
    procedure acCloseExecute(Sender: TObject);
  end;

var
  fmBalanceB: TfmBalanceB;

implementation

uses comdata, dbUtil;

{$R *.dfm}

procedure TfmBalanceB.FormCreate(Sender: TObject);
begin
  OpenDataSets([taComp, taRec, taBalanceB]);
end;

procedure TfmBalanceB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taComp, taBalanceB, taRec]);
  Action := caFree;
end;

procedure TfmBalanceB.acAddExecute(Sender: TObject);
begin
  ActiveControl:=gridBalanceB;
  gridBalanceB.SelectedField:=taBalanceBCOMPNAME;
  taBalanceB.Append;
end;

procedure TfmBalanceB.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := taBalanceB.Active;
end;

procedure TfmBalanceB.acDelExecute(Sender: TObject);
begin
  taBalanceB.Delete;
end;

procedure TfmBalanceB.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := taBalanceB.Active and (not taBalanceB.IsEmpty);
end;

procedure TfmBalanceB.PastBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TfmBalanceB.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmBalanceB.taBalanceBNewRecord(DataSet: TDataSet);
begin
  taBalanceBID.AsInteger := dmCom.GetID(56);
  taBalanceBC.AsFloat := 0;
end;

procedure TfmBalanceB.acCloseExecute(Sender: TObject);
begin
 if not (taBalanceB.State in [dsEdit, dsInsert]) then close
 else taBalanceB.Cancel;
end;

end.
