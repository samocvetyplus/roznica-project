object fmTmpDir: TfmTmpDir
  Left = 387
  Top = 264
  Width = 671
  Height = 359
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MSettings
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PButton: TPanel
    Left = 558
    Top = 0
    Width = 105
    Height = 305
    Align = alRight
    TabOrder = 0
    object btok: TBitBtn
      Left = 22
      Top = 4
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object btcancel: TBitBtn
      Left = 22
      Top = 40
      Width = 75
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object PC: TPageControl
    Left = 0
    Top = 0
    Width = 558
    Height = 305
    ActivePage = tsell
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object tsell: TTabSheet
      Caption = #1055#1088#1086#1076#1072#1078#1072
      object Shape1: TShape
        Left = 8
        Top = 152
        Width = 500
        Height = 1
      end
      object Label2: TLabel
        Left = 8
        Top = 184
        Width = 210
        Height = 13
        Caption = 'Com '#1087#1086#1088#1090' '#1076#1083#1103' '#1092#1080#1089#1082#1072#1083#1100#1085#1086#1075#1086' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1086#1088#1072
      end
      object cbShowNumEmp: TCheckBox
        Left = 8
        Top = 88
        Width = 281
        Height = 17
        Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1085#1086#1084#1077#1088' '#1087#1088#1086#1076#1072#1074#1094#1072' '#1074' '#1057#1052#1045#1053#1045
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object cbShowInfClient: TCheckBox
        Left = 8
        Top = 108
        Width = 473
        Height = 17
        Caption = 
          #1042#1089#1077#1075#1076#1072' '#1087#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1077' '#1087#1088#1080' '#1074#1074#1086#1076#1077' '#8470' '#1076#1080#1089#1082'. '#1082#1072 +
          #1088#1090#1099' '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object cbPrintCheck: TCheckBox
        Left = 8
        Top = 128
        Width = 417
        Height = 17
        Caption = #1055#1077#1095#1072#1090#1100' '#1090#1086#1074#1072#1088#1085#1086#1075#1086' '#1095#1077#1082#1072' '#1073#1077#1079' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1086#1075#1086' '#1087#1088#1086#1089#1084#1086#1090#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object rgPrint: TRadioGroup
        Left = 8
        Top = 8
        Width = 409
        Height = 73
        Caption = #1055#1077#1095#1072#1090#1100
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemIndex = 0
        Items.Strings = (
          #1055#1086#1076#1082#1083#1102#1095#1077#1085' '#1087#1088#1080#1085#1090#1077#1088' U950 ('#1095#1077#1082' '#1074' '#1076#1074#1086#1081#1085#1086#1084'  '#1101#1082#1079#1077#1084#1087#1083#1103#1088#1077')'
          #1063#1077#1082'  '#1092#1086#1088#1084#1072#1090#1072' A5 ('#1074' '#1076#1074#1086#1081#1085#1086#1084' '#1101#1082#1079#1077#1084#1087#1083#1103#1088#1077')'
          #1063#1077#1082'  '#1092#1086#1088#1084#1072#1090#1072' A4  ('#1074' '#1086#1076#1085#1086#1084' '#1101#1082#1079#1077#1084#1087#1083#1103#1088#1077')')
        ParentFont = False
        TabOrder = 3
      end
      object cbFreg: TCheckBox
        Left = 8
        Top = 160
        Width = 409
        Height = 17
        Caption = #1055#1086#1076#1082#1083#1102#1095#1080#1090#1100' '#1092#1080#1089#1082#1072#1083#1100#1085#1099#1081' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1086#1088
        TabOrder = 4
      end
      object cbcomfreg: TComboBox
        Left = 224
        Top = 184
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 5
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
      end
    end
    object tscan: TTabSheet
      Caption = #1057#1082#1072#1085#1077#1088#1099
      ImageIndex = 1
      object Label5: TLabel
        Left = 16
        Top = 8
        Width = 182
        Height = 13
        Caption = 'COM '#1087#1086#1088#1090' '#1076#1083#1103' '#1089#1082#1072#1085#1077#1088#1072' '#1096#1090#1088#1080#1093#1082#1086#1076#1072' 1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object Label3: TLabel
        Left = 16
        Top = 40
        Width = 182
        Height = 13
        Caption = 'COM '#1087#1086#1088#1090' '#1076#1083#1103' '#1089#1082#1072#1085#1077#1088#1072' '#1096#1090#1088#1080#1093#1082#1086#1076#1072' 2'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cbComScan: TComboBox
        Left = 240
        Top = 8
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
      end
      object cbComScan2: TComboBox
        Left = 240
        Top = 40
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 1
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
      end
      object cbWithOutActiveForm: TCheckBox
        Left = 24
        Top = 88
        Width = 505
        Height = 17
        Caption = 
          #1054#1090#1084#1077#1085#1080#1090#1100' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077'/'#1086#1090#1082#1083#1102#1095#1077#1085#1080#1077' '#1089#1082#1072#1085#1077#1088#1072' '#1085#1072' '#1072#1082#1090#1080#1074#1080#1079#1072#1094#1080#1102'/'#1076#1077#1079#1072#1082#1090#1080#1074#1080 +
          #1079#1072#1094#1080#1102' '#1087#1088#1080#1083#1086#1078#1077#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
    object tTmp: TTabSheet
      Caption = #1042#1088#1077#1084#1077#1085#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      ImageIndex = 2
      object Label4: TLabel
        Left = 8
        Top = 4
        Width = 163
        Height = 13
        Caption = #1050#1072#1090#1072#1083#1086#1075' '#1076#1083#1103' '#1074#1088#1077#1084#1077#1085#1085#1099#1093' '#1092#1072#1081#1083#1086#1074
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 8
        Top = 48
        Width = 137
        Height = 13
        Caption = #1041#1044' '#1076#1083#1103' '#1074#1088#1077#1084#1077#1085#1085#1099#1093' '#1076#1072#1085#1085#1099#1093
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 20
        Width = 281
        Height = 21
        Color = clInfoBk
        DataField = 'TMPDIR'
        DataSource = dmCom.dsRec
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 64
        Width = 281
        Height = 21
        Color = clInfoBk
        DataField = 'TMPDB'
        DataSource = dmCom.dsRec
        TabOrder = 1
      end
    end
    object tbTag: TTabSheet
      Caption = #1041#1080#1088#1082#1080
      ImageIndex = 3
      object chbTagOld: TCheckBox
        Tag = 1
        Left = 24
        Top = 16
        Width = 337
        Height = 17
        Caption = #1052#1072#1083#1077#1085#1100#1082#1080#1077' '#1073#1080#1088#1082#1080' '#1073#1077#1079' '#1096#1090#1088#1080#1093#1082#1086#1076#1072
        TabOrder = 1
      end
      object chbTagNew: TCheckBox
        Left = 24
        Top = 40
        Width = 385
        Height = 17
        Caption = #1041#1086#1083#1100#1096#1080#1077' '#1073#1080#1088#1082#1080' '#1073#1077#1079' '#1096#1090#1088#1080#1093#1082#1086#1076#1072
        TabOrder = 0
      end
      object chbTagBlack: TCheckBox
        Tag = 2
        Left = 24
        Top = 64
        Width = 465
        Height = 17
        Caption = #1063#1077#1088#1085#1099#1077' '#1073#1080#1088#1082#1080' '#1073#1077#1079' '#1096#1090#1088#1080#1093#1082#1086#1076#1072
        TabOrder = 2
      end
      object chbTagCode: TCheckBox
        Tag = 3
        Left = 24
        Top = 88
        Width = 329
        Height = 17
        Caption = #1041#1080#1088#1082#1080' '#1089#1086' '#1096#1090#1088#1080#1093#1082#1086#1076#1086#1084
        TabOrder = 3
      end
    end
  end
  object MSettings: TMainMenu
    Left = 616
    Top = 96
    object NHelp: TMenuItem
      Caption = #1055#1086#1084#1086#1097#1100
    end
  end
end
