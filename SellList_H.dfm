object fmSellList_H: TfmSellList_H
  Left = 320
  Top = 42
  Cursor = crArrow
  ActiveControl = LogEd
  BorderStyle = bsToolWindow
  ClientHeight = 394
  ClientWidth = 446
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 135
    Width = 134
    Height = 19
    Caption = #1048#1089#1090#1086#1088#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaLogin: TLabel
    Left = 26
    Top = 330
    Width = 78
    Height = 15
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaPsw: TLabel
    Left = 58
    Top = 361
    Width = 44
    Height = 15
    Caption = #1055#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object OpSBtn: TSpeedButton
    Left = 225
    Top = 331
    Width = 65
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    OnClick = OpSBtnClick
  end
  object ClSBtn: TSpeedButton
    Left = 299
    Top = 330
    Width = 68
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    OnClick = ClSBtnClick
  end
  object CnlSBtn: TSpeedButton
    Left = 377
    Top = 329
    Width = 61
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    OnClick = CnlSBtnClick
  end
  object Label12: TLabel
    Left = 8
    Top = 304
    Width = 294
    Height = 17
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1074#1086#1077' '#1080#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1087#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 160
    Width = 449
    Height = 137
    Color = clBtnHighlight
    DataGrouping.GroupLevels = <>
    DataSource = dsrDlist_H
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 145
      end
      item
        DisplayFormat = 'dd.mm.yyyy   hh:nn'
        EditButtons = <>
        FieldName = 'HDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Width = 130
      end
      item
        EditButtons = <>
        FieldName = 'STATUS'
        Footers = <>
        Title.Caption = #1044#1077#1081#1089#1090#1074#1080#1077
        Width = 141
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object PswEd: TEdit
    Left = 108
    Top = 359
    Width = 109
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = PswEdKeyDown
  end
  object LogEd: TEdit
    Left = 108
    Top = 326
    Width = 109
    Height = 21
    TabOrder = 2
    OnKeyDown = LogEdKeyDown
  end
  object pa1: TPanel
    Left = 0
    Top = 0
    Width = 446
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 201
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 0
      object Label3: TLabel
        Left = 4
        Top = 4
        Width = 34
        Height = 13
        Caption = #1057#1082#1083#1072#1076':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtDep: TDBText
        Left = 40
        Top = 4
        Width = 35
        Height = 13
        AutoSize = True
        DataField = 'DEP'
        DataSource = dm.dsSellList
      end
      object Label2: TLabel
        Left = 144
        Top = 4
        Width = 14
        Height = 13
        Caption = #8470':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtN: TDBText
        Left = 160
        Top = 4
        Width = 23
        Height = 13
        AutoSize = True
        DataField = 'RN'
        DataSource = dm.dsSellList
      end
      object Label4: TLabel
        Left = 4
        Top = 20
        Width = 77
        Height = 13
        Caption = #1053#1072#1095#1072#1083#1086' '#1089#1084#1077#1085#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtBD: TDBText
        Left = 100
        Top = 20
        Width = 30
        Height = 13
        AutoSize = True
        DataField = 'BD'
        DataSource = dm.dsSellList
      end
      object Label5: TLabel
        Left = 4
        Top = 36
        Width = 95
        Height = 13
        Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1089#1084#1077#1085#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtED: TDBText
        Left = 100
        Top = 36
        Width = 30
        Height = 13
        AutoSize = True
        DataField = 'ED'
        DataSource = dm.dsSellList
      end
    end
    object Panel3: TPanel
      Left = 262
      Top = 0
      Width = 187
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 1
      object Label8: TLabel
        Left = 4
        Top = 4
        Width = 44
        Height = 13
        Caption = #1057#1091#1084#1084#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 4
        Top = 20
        Width = 37
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 4
        Top = 36
        Width = 22
        Height = 13
        Caption = #1042#1077#1089':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtCNT: TDBText
        Left = 48
        Top = 20
        Width = 37
        Height = 13
        AutoSize = True
        DataField = 'CNT'
        DataSource = dm.dsSellList
      end
      object dbtCost: TDBText
        Left = 48
        Top = 4
        Width = 44
        Height = 13
        AutoSize = True
        DataField = 'COST'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtW: TDBText
        Left = 48
        Top = 36
        Width = 26
        Height = 13
        AutoSize = True
        DataField = 'W'
        DataSource = dm.dsSellList
      end
    end
    object Panel5: TPanel
      Left = 201
      Top = 0
      Width = 61
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1055#1088#1086#1076#1072#1078#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 56
    Width = 449
    Height = 57
    BevelOuter = bvNone
    TabOrder = 4
    object Panel7: TPanel
      Left = 0
      Top = 0
      Width = 56
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1042#1086#1079#1074#1088#1072#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object Panel6: TPanel
      Left = 56
      Top = 0
      Width = 145
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 1
      object Label9: TLabel
        Left = 4
        Top = 4
        Width = 44
        Height = 13
        Caption = #1057#1091#1084#1084#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 4
        Top = 20
        Width = 37
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 4
        Top = 36
        Width = 22
        Height = 13
        Caption = #1042#1077#1089':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtRCnt: TDBText
        Left = 48
        Top = 20
        Width = 39
        Height = 13
        AutoSize = True
        DataField = 'RCNT'
        DataSource = dm.dsSellList
      end
      object dbtRCost: TDBText
        Left = 48
        Top = 4
        Width = 53
        Height = 13
        AutoSize = True
        DataField = 'RCOST'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtRW: TDBText
        Left = 48
        Top = 36
        Width = 34
        Height = 13
        AutoSize = True
        DataField = 'RW'
        DataSource = dm.dsSellList
      end
    end
    object plCostCard: TPanel
      Left = 201
      Top = 0
      Width = 112
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 2
      object LCostCard: TLabel
        Left = 10
        Top = 4
        Width = 94
        Height = 26
        Alignment = taCenter
        Caption = #1057#1091#1084#1084#1072', '#1087#1088#1086#1074#1077#1076'.'#13#10#1087#1086' '#1082#1072#1088#1090#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbCostCard: TDBText
        Left = 21
        Top = 34
        Width = 66
        Height = 13
        AutoSize = True
        DataField = 'COSTCARD'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object Panel8: TPanel
      Left = 313
      Top = 0
      Width = 134
      Height = 57
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 3
      object dbtCass: TDBText
        Left = 3
        Top = 20
        Width = 46
        Height = 13
        AutoSize = True
        DataField = 'CASS'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 24
        Top = 4
        Width = 37
        Height = 13
        Caption = #1050#1072#1089#1089#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object fs1: TFormStorage
    Active = False
    Options = [fpState]
    UseRegistry = False
    StoredValues = <>
    Left = 292
    Top = 132
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   8'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 8'
      'order by HDATE')
    BeforeOpen = taHistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 184
    Top = 128
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 144
    Top = 128
  end
  object taEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ALIAS, FIO, PSWD'
      'FROM D_EMP')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 256
    Top = 128
    object taEmpALIAS: TFIBStringField
      FieldName = 'ALIAS'
      EmptyStrToNull = True
    end
    object taEmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taEmpPSWD: TFIBStringField
      FieldName = 'PSWD'
      EmptyStrToNull = True
    end
  end
  object DataSource1: TDataSource
    DataSet = taEmp
    Left = 216
    Top = 128
  end
end
