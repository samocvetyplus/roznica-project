object fmCheckNo: TfmCheckNo
  Left = 263
  Top = 190
  BorderStyle = bsDialog
  Caption = #1055#1088#1086#1076#1072#1078#1072
  ClientHeight = 163
  ClientWidth = 465
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object laCheck: TLabel
    Left = 6
    Top = 126
    Width = 109
    Height = 24
    Caption = #1053#1086#1084#1077#1088' '#1095#1077#1082#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 6
    Top = 6
    Width = 63
    Height = 24
    Caption = #1057#1091#1084#1084#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 6
    Top = 30
    Width = 108
    Height = 48
    Caption = #1055#1088#1080#1085#1103#1090#1086' '#1086#1090' '#13#10#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 6
    Top = 86
    Width = 60
    Height = 24
    Caption = #1057#1076#1072#1095#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object bbOK: TBitBtn
    Left = 360
    Top = 8
    Width = 97
    Height = 33
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 360
    Top = 60
    Width = 97
    Height = 37
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Kind = bkCancel
  end
  object edCheck: TEdit
    Left = 132
    Top = 124
    Width = 165
    Height = 32
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnChange = edCheckChange
  end
  object ceP: TCurrencyEdit
    Left = 132
    Top = 44
    Width = 165
    Height = 29
    AutoSize = False
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChange = cePChange
  end
  object ceS: TCurrencyEdit
    Left = 132
    Top = 4
    Width = 165
    Height = 29
    TabStop = False
    AutoSize = False
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
  end
  object ceR: TCurrencyEdit
    Left = 132
    Top = 84
    Width = 165
    Height = 29
    TabStop = False
    AutoSize = False
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 5
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 372
    Top = 112
  end
  object tm1: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = tm1Timer
    Left = 320
    Top = 104
  end
end
