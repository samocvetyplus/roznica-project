unit ImportEstet_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// PASTLWTR : $Revision: 1.2 $
// File generated on 05.05.2005 17:10:53 from Type Library described below.

// ************************************************************************  //
// Type Lib: E:\Projects\Jew\Plugins\Import\Estet\ImportEstet.tlb (1)
// LIBID: {CD986DCD-ED86-4376-B15A-56016C812B0A}
// LCID: 0
// Helpfile:
// DepndLst:
//   (1) v2.0 stdole, (C:\WINNT\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  JewImportMajorVersion = 1;
  JewImportMinorVersion = 0;

  LIBID_JewImport: TGUID = '{CD986DCD-ED86-4376-B15A-56016C812B0A}';

  IID_IEstet: TGUID = '{D9F972FF-A0CA-4B79-9B48-19A1231CFFAF}';
  CLASS_Estet: TGUID = '{620820D5-945D-44EF-A5E4-FF42803CE245}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IEstet = interface;
  IEstetDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
   Estet = IEstet;


// *********************************************************************//
// Interface: IEstet
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D9F972FF-A0CA-4B79-9B48-19A1231CFFAF}
// *********************************************************************//
  IEstet = interface(IDispatch)
    ['{D9F972FF-A0CA-4B79-9B48-19A1231CFFAF}']
    procedure Import(const NameDb: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IEstetDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D9F972FF-A0CA-4B79-9B48-19A1231CFFAF}
// *********************************************************************//
  IEstetDisp = dispinterface
    ['{D9F972FF-A0CA-4B79-9B48-19A1231CFFAF}']
    procedure Import(const NameDb: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoEstet provides a Create and CreateRemote method to
// create instances of the default interface IEstet exposed by
// the CoClass Estet. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoEstet = class
    class function Create: IEstet;
    class function CreateRemote(const MachineName: string): IEstet;
  end;

implementation

uses ComObj;

class function CoEstet.Create: IEstet;
begin
  Result := CreateComObject(CLASS_Estet) as IEstet;
end;

class function CoEstet.CreateRemote(const MachineName: string): IEstet;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Estet) as IEstet;
end;

end.
