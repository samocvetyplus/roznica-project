library ImportEstet;

uses
  ExceptionLog,
  ComServ,
  ImportEstet_TLB in 'ImportEstet_TLB.pas',
  ImportEstet_Auto in 'ImportEstet_Auto.pas',
  SInvImp in 'SInvImp.pas' {fmSInvImp};

const
  ClassName : PChar = 'JewImport.Estet';
  Version : PChar = '1.0';
  PluginName : PChar = '������ ��������� �����';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� �����. ��� 0411056747';

{$R *.TLB}

{$R *.RES}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
