unit ImportEstet_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportEstet_TLB, Contnrs;

const
  INN : PChar = '0411056747';

type
  TJewImportEstet = class(TAutoObject, IEstet)
    FAppHandle: integer;
  private
    // from IJewImportEstet
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportEstet}

uses ComServ, SInvImp, Forms;

procedure TJewImportEstet.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportEstet.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportEstet,CLASS_Estet, ciMultiInstance, tmApartment);

end.
