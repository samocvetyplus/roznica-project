unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolEdit, DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, Dbf;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    mmLog: TMemo;
    dbfEstet: TDbf;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure lcSupChange(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������

implementation

uses dbUtil, RegExpr, Variants, RxStrUtils, Math, MsgDialog, ImportEstet_Auto,
     fmUtils, UtilLib;

resourcestring
  rcOrganizationNotFound = '������������� ����� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:Variant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ��� � ��, ��� ������ ����������� ���
    CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST("NOW" AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger := SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;
procedure TfmSInvImp.Import;
var Art_P, art:String;
    d_matid, d_goodid, d_insid, ins, CHROMATICITY, CLEANNES, CountIns, insw:string;
    unitid, art2id,  countItem:integer;
    i:integer;
    d_goodssam1, d_goodssam2:string;
    price:real;
    cost, w :real;
    res: Variant;
    DefaultSeparator:char;
    CountStr: integer;
    TotSum, TotW: real;

  procedure AddLog(Msg : string);
  begin
    mmLog.Lines.Add(Msg);
  end;

  Function FindId (supname:string; fsup, ftable:integer):string;
  var res: Variant;
  begin
    res := ExecSelectSQL('select first 1 SNAME,D_SNAMESUPID from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
              ' and fsup='+inttostr(fsup)+' and ftable='+inttostr(ftable)+' order by SNAME ', quTmp);
    if VarIsNull(res[0]) then
      result:='-1'
    else
      result:= VarToStr(res[0]);
  end;
 Procedure InsertItem;
 var st, st1:string;
 begin
   st:=ReplaceStr(floattostr(price), ',', '.');
   st1:=ReplaceStr(floattostr(w), ',', '.');

   ExecSQL('execute procedure InsertItem__Estet ('+inttostr(Docid)+', '+inttostr(art2id) +
          ', ' +inttostr(unitid)+', ' + FloatToStr(w) + ', '#39+ d_goodssam1 + #39', '#39+d_goodssam2+#39', '
           +FloatToStr(Price)+')', quTmp);
 end;

begin
  CountStr :=0; TotSum := 0; TotW:= 0;
  // �������� ���
  mmLog.Lines.Clear;
  Screen.Cursor := crHourGlass;

  dbfEstet.Active:=false;
  if not FileExists(fledImp.FileName) then
  begin
    AddLog('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end
  else dbfEstet.TableName:=fledImp.FileName;
  DefaultSeparator := DecimalSeparator;
  DecimalSeparator := '.';
  try

    dbfEstet.Active:=true;
    prbrImp.Min := 0;
    prbrImp.Max := dbfEstet.RecordCount;
    prbrImp.Step := 1;
    i:=1;
    ExecSQL('delete from Tmp where UseType = 2', quTmp);

    dbfEstet.First;
    while not dbfestet.Eof do
    begin
      if dbfEstet.FieldByName('ARTICUL').AsString <> '' then //��������� �������
      begin
        CountStr := CountStr + dbfEstet.FieldByName('COUNT').AsInteger;
        TotSum := TotSum + dbfEstet.FieldByName('COST').AsFloat;;
        TotW := TotW + dbfEstet.FieldByName('VES').AsFloat;;

        art_p:=dbfEstet.FieldByName('ARTICUL').AsString;
        cost:=dbfEstet.FieldByName('COST').AsFloat;
        w:=dbfEstet.FieldByName('VES').AsFloat;
        countItem := dbfEstet.FieldByName('COUNT').AsInteger;
        if art_p[4]='6'then
          Unitid:=0
        else
          Unitid:=1;
        art := AnsiLowerCase(art_p);
        case art_p[4] of
          '0', '7': d_insid:='-';
          '1': d_insid:='�';
          '2': d_insid:='��';
          '4': d_insid:='��';
          '6': begin d_insid:='�'; Unitid:=0 end
          else  d_insid:='-'
        end;

        case art_p[5] of
            '1','6','2','3' : if trim(d_insid)='�' then d_matid:='��' else d_matid:='�';
            '4','7','8': if trim(d_insid)='�' then d_matid:='��' else d_matid:='�';
        end;
        d_goodid := FindId(art_p[3],2,0);
        d_goodssam1:='-';
        case art_p[5] of
          '2','7': d_goodssam1:='�����';
          '3','4': d_goodssam1:='������';
        end;

        d_goodssam2:='-';
//        if art_p[3]='�' then d_goodid:='�������';

      end
      else                                                       //��������� ��������� ������� ��������
      begin
        ins:=AnsiLowerCase(trim(dbfEstet.FieldByName('NAME_STN').AsString));
        ins:=FindId(ins,2,1);
        if d_insID = '' then //�������� �������( ���� ��� �� ������������ �����)
        begin
          d_insID := ins;
          if (trim(ins) = '�') and (d_matid = '�') then d_matid := '��'
          else if (trim(ins) = '�') and (d_matid = '�') then d_matid := '��'
        end;
        res := ExecSelectSQL('select ISFULLDISCRIP from D_ins where D_InsID = '#39 +
                              ins + #39, quTmp);
        if not VarIsNull(res) then
        begin
          if Integer(res) = 1 then
          begin
            CountIns := dbfEstet.FieldByName('COUNT').AsString;
            insw := replacestr(dbfEstet.FieldByName('VESC_STN').AsString, ',','.');
            CHROMATICITY := dbfEstet.FieldByName('PURT_COLOR').AsString;
            CLEANNES := dbfEstet.FieldByName('PURT_WHITE').AsString;
            ExecSQL('insert into Tmp(UseType, FIELD1, FIELD2, FIELD3, FIELD4, FIELD5, FIELD6)'+
                   'values(2,'#39 + art + #39','#39 + ins + #39','#39 +
                     insw  + #39','#39 +  CountIns +  #39','#39 +
                    CLEANNES  + #39','#39 +  CHROMATICITY + #39')', quTmp);
          end
          else
          ExecSQL('insert into Tmp(UseType, FIELD1, FIELD2, FIELD3, FIELD4, FIELD5, FIELD6)'+
                'values(2,'#39 + art + #39','#39 + ins + #39',null,null,null,null)', quTmp);
        end
        else
        begin
          ExecSQL('insert into Tmp(UseType, FIELD1, FIELD2, FIELD3, FIELD4, FIELD5, FIELD6)'+
                'values(2,'#39 + art + #39','#39 + ins + #39',null,null,null,null)', quTmp);
        end;
      end;

      dbfEstet.next;
      prbrImp.StepIt;
      // ���� ��� ������� �������, �� ��������� ���� ������� �� ������� ��� � ����
      // � ���������, ���� ���������, � ���������� � � ����.
      if (dbfEstet.FieldByName('ARTICUL').AsString <> '') or (dbfestet.Eof) then
      begin
        if (d_insid='�') or (d_insid='��') or (d_insid='-') or (d_insid='��') then
        begin
          res := ExecSelectSQL('select FIELD2 from Tmp where usetype = 2 and field1 = '#39 +
                              art + #39, quTmp);
          if VarIsNull(res) then
            ExecSQL('insert into Tmp(UseType, FIELD1, FIELD2, FIELD3, FIELD4, FIELD5, FIELD6)'+
                'values(2,'#39 + art + #39','#39 + d_insid + #39',null,null,null,null)', quTmp)
          else
             d_insid := VarToStr(res);
        end
        else if (d_insID = '-1') or (d_insID = '') then
        begin
         AddLog(inttostr(i)+' ���. - �� ��������� ������� '+art_p);
         continue;
        end;
        if (d_matid = '-1') or(d_matID = '') then
        begin
          AddLog(inttostr(i)+' ���. - �� �������� �������� '+art_p);
          continue;
        end;
        if d_goodid = '-1' then
        begin
          AddLog(inttostr(i)+' ���. - �� �������� ����� '+art_p);
          continue;
        end;
        res := ExecSelectSQL('select O_Art2ID,O_UnitID, O_Price from CheckArtEstet('+ IntToStr(CompID) + ','#39 +
         d_matid + #39','#39 +d_goodid + #39','#39 + d_insid + #39','#39 + art +
            #39',' + intToStr(unitid)+ ',' + FloatToStr(cost) + ','+ FloatToStr(w)+ ')', quTmp);
        Art2ID := integer(res[0]);
        UnitID := integer(res[1]);
        Price := real(res[2]);
        for i:=1 to CountItem do
            InsertItem;
      end;
    end
  finally
    ExecSQL('delete from Tmp where UseType = 2', quTmp);
    DecimalSeparator:=DefaultSeparator;

    Screen.Cursor := crDefault;
//    ShowMessage(IntToStr(CountStr) + '/ ' + FloatToStr(TotSum) + '/ ' + FloatToStr(TotW));
  end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
  DocNo, DocDate, SupINN : string;
  r : Variant;
begin
    try
      taSList.Edit;
      taSListSSF.AsString := ExtractWord(1,ExtractFileName(Name), ['.']);
      taSList.Post;
    except
    end;

{  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    ReadLn(ftext, DocNo);
    ReadLn(ftext, DocDate);
    ReadLn(ftext);
    ReadLn(ftext, SupINN);
    try
      taSList.Edit;
      taSListSSF.AsString := DocNo;
      taSListNDATE.AsDateTime := StrToDateTime(DocDate);
      if(SupINN <> '')then
      begin
        r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+SupINN+'"', quTmp);
        if not VarIsNull(r) then taSListSUPID.AsInteger := r;
      end;
      taSList.Post;
    except
    end;
  finally
    CloseFile(ftext);
  end;}
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

procedure TfmSInvImp.lcSupChange(Sender: TObject);
var r: Variant;
    SName: string;
begin
  SName := Trim(ExtractWord(1,lcSup.Text,['-']));
  if SName = '' then exit;
  taSList.Edit;
  r := ExecSelectSQL('select PAYTYPEID,NDSID from D_Comp where SName='#39 + SName + #39'', quTmp);
  if not VarIsNull(r[0]) then taSListPAYTYPEID.AsInteger := Integer(r[0]);
  if not VarIsNull(r[1]) then taSListNDSID.AsInteger := Integer(r[1]);
  taSList.Post;
end;

end.
