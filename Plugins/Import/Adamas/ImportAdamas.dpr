library ImportAdamas;

uses
  {ExceptionLog,}
  ComServ,
  ImportAdamas_Auto in 'ImportAdamas_Auto.pas',
  ImportAdamas_TLB in 'ImportAdamas_TLB.pas',
  SInvImp in 'SInvImp.pas' {fmSInvImp};

const
  ClassName : PChar = 'JewImport.Adamas';
  Version : PChar = '1.0';
  PluginName : PChar = '������ ��������� ������';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ������. ��� 7716148554';

{$R *.TLB}

{$R *.res}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.

