unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ToolEdit,} DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons, 
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, IdGlobal,
  rxToolEdit;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    Memo1: TMemo;
    ConvertWin: TCheckBox;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure headingToFill(Sender: TObject; var Name: String);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
    Function MakeMainFile(Path: string; var strCount: integer): string;
    function  strtofloat2(const s:string):double;
    function  FloatToStr2(const f:double):String;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

const   NM_PROGRESSBAR = $60f1;
var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������

implementation

uses dbUtil, Variants, RxStrUtils, Math, MsgDialog,
     fmUtils, UtilLib, bsUtils;

resourcestring
  rcOrganizationNotFound = '������������� ������ �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

const
  INN : PChar = '7716148554';


procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:Variant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ������ � ��, ��� ������ ����������� ���
    CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := Integer(r);
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST("NOW" AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger := SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

//--- ������ �������� ������� ��������� �� ������� ---//
procedure TfmSInvImp.btnImpClick(Sender: TObject);
var
TmpFile:textfile;
begin
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  btnClose.Enabled:=false;
  Import;
  btnClose.Enabled:=true;
  btnClose.SetFocus;
  if ListBox1.Items.Text='' then
     ListBox1.Items.Text:='�������������� ��������� ������ �������!';
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.Import;
const
  sEnd = '�����:';
var Path, Tmp, Tmp2, Tmp3,Tmp4, Tmp2_ :String;
    Art, Art2,Art_, Art2_,Good,Mat, MainIns, MIns,Ins, Gr,Country, Pattern, Sz, Chrcs: String;
    Art3: string;
    j: Integer;
    DefaultSeparator:char;
    i,k, EndMarker, Step, Contrsize, Writesize,StrHelpL: integer;
    UnitId, D_Ins_ID, Art2Id, SelId, SInfoId, StrCount : integer;
    IsIns: Boolean;
    QUANTITY: smallint; WEIGHT, W, Price: double;
    COLOR,CHROMATICITY,CLEANNES,EDGTYPE,EDGSHAPE, Un, StrHelp,StrHelp_: string;
    fText: TextFile;
    res: Variant;
    s: string;

  procedure AddLog(Msg : string);
  begin
    ListBox1.Items.Add(Msg);
    ListBox1.Refresh;
  end;
  Function FindId (supname:string; fsup, ftable:integer):string;
  //����������� ���������� ������, ������� � ��.
  var res: Variant;
  begin
     res := ExecSelectSQL('select first 1 SNAME, D_SNAMESUPID  from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
                  ' and fsup='+inttostr(fsup)+' and ftable='+inttostr(ftable)+' order by SNAME ',quTmp);
     if res[0]=null then result:='-1'
     else result:=VarToStr(res[0]);
  end;

  function CreateArt(const Tmp : string; Wrsz:integer) : string;
  // ������������ �������� ������� ��������
  var
     Art : string;
  begin
      Art:=ExtractWord(1,Tmp,[' ']);
      Art := ReplaceStr(Art, #39, '');
      case WrSz of  {��. ��� ������� ���. ������� � ����. D_Good}
        0: Result := AnsiLowerCase(Art);
        1: if Art<>''  then
            if (not IsNumeric(Art[1])) and ((Trim(Good) = '��') or (trim(Good) ='�'))  then  Result := ExtractWord(2,Tmp,[' '])+AnsiLowerCase(Art)
            else Result :=AnsiLowerCase(Art)
           else result:='';
        2: Result := AnsiLowerCase(Art);
        5: Result := AnsiLowerCase(Art);
        else result:='';
      end;
  end;
  procedure CreateGoods(var Tmp: string);
  //���� � ���� ��������� ����� � ����������, ����������� �� � ���� ������
  var goodstmp, ins1: string;
      res: Variant;
  begin
    goodstmp:=AnsiUpperCase(copy(tmp,1,21));
    delete(tmp,1,21);
    Good:=GetFirstWord(goodstmp);
    ins1:=FindId(good,0,0);
    if ins1<>'-1' then
    begin
      res := ExecSelectSQL('select Contrsize, WriteSz from D_Good where d_goodid like '#39+ins1+#39,quTmp);
      if VarIsNull(res[0]) then
      begin
        AddLog('�� ������� ���������� ����� '+Good+' � '+inttostr(i));
        readln(ftext,Tmp);
        Tmp:=ReplaceStr(Tmp, ',', '.');
        inc(i);
      end
      else
      begin
        Good := ins1;
        Contrsize:=Integer(res[0]);
        Writesize:=Integer(res[1]);
      end
    end
  end;

  procedure CheckArt(Art:String);
  // ���������� ����� �� ��������� ��������
  var res: Variant;
      sqlstr: string;
      isBASEGOODS: integer;
  begin
    sqlstr := ' select BASEGOODS from D_Good ' +
              '  where d_goodid = ''' +  Good + '''';
    res := ExecSelectSQL(sqlstr , quTmp);
    if Not VarIsNull(res) then isBASEGOODS := integer(res);

    if isBASEGOODS = 0 then exit;
    sqlstr := ' select min(g.d_goodid) from D_Good g, D_art a ' +
                           '  where a.d_goodid = g.d_goodid and  a.art = ''' +
                           Art + '''  and  a.d_compid = ' + IntToStr(CompId);
    res := ExecSelectSQL(sqlstr , quTmp);
    if Not VarIsNull(res) then
       if VarToStr(res) <> '' then
         Good :=VarToStr(res);
  end;

  Function CountWords (Tmp_1:String):integer;
  // ������� ���-�� ���� � ������
  var i:integer;
  begin
   i:=1;
   while ExtractWord(i,Tmp_1,[' '])<>'' do inc(i);
   result:=i-1;
  end;

  Function ReadFistNum (Str:string; var Pos :integer):string;
  //���������� �����
  var i:integer;
  begin
   i:=Pos;
   result:='';
   while (str[i] in ['0'..'9']) and (i<=length(str)) do
   begin result:=result+str[i]; inc(i) end;
   Pos:=I;
  end;


  Procedure DecodePattern (StrBeginning:string);
 // ������ �� �����
  var str1, strval, strnum:string;
      str2:string;
      nbeg, nend, nval, t_:integer;
      snbeg:integer;
  begin
   nbeg:=1;
   nend:=1;
   snbeg:=1;
   str1:=ExtractSubstr(Pattern,nend, [')']);
   while str1<>'' do
   begin
    nval:=nbeg;
    strval:=ExtractSubstr(Pattern, nval, ['(']);
    strnum:=copy(str1,nval-nbeg+1,nend-nval);
    If strval[1]='\' then
     case strval[2] of
      'd','D': if trim(strval[3])<>'' then str2:=ExtractSubstr(StrBeginning,snbeg, [strval[3]])
               else str2:=ReadFistNum (StrBeginning,snbeg);
      's','S': if (length(strval)>=3) then str2:=ExtractSubstr(StrBeginning,snbeg, [strval[3]])
       else begin
        t_:=snbeg;
        while (t_<=length(StrBeginning)) and (not IsNumeric(StrBeginning[t_])) do
        inc(t_);
        str2:=copy(StrBeginning,snbeg,t_-1);
        snbeg:= snbeg + t_-1;
       end
     end
    else begin
      str2:=copy(StrBeginning,snbeg,nval-nbeg-1);
      snbeg:= snbeg + nval-nbeg-1;
     end;

    case strtoint(strnum) of
     1: begin
         if trim(MainIns)='�' then
         begin
          if trim(str2)='��' then EdgShape:='��.' else
          if trim(str2)='��' then EdgShape:='��.' else
          if trim(str2)='�'  then EdgShape:='��.' else
          EdgShape:=trim(str2);
         end
         else EdgShape:='';
        end;
     2: EDGTYPE:=str2;
     4: CLEANNES:=str2;
     3: CHROMATICITY:=str2;
     5: COLOR:=str2;
     6: GR:=str2;
    end;

    nbeg:=nend;
    str1:=ExtractSubstr(Pattern, nend, [')']);
   end;
  end;

  Procedure FindIdIns (supname:string; var sname, pr:string; var idins:integer);
  var res: Variant;
  begin
     res := ExecSelectSQL('select SNAME, A_pattern from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
                     ' and fsup=0 and ftable=1'+' order by SNAME ' ,quTmp);
     if VarIsNull(res) then sname:='-1'
     else
     begin
       sname:= VarToStr(res[0]);

       pr:=VarToStr(res[1]);
       res := ExecSelectSQL('select first 1 d_ins_id from d_ins where d_insid like '#39+Trim(sname)+'%'#39+
                            ' order by d_insid ' ,quTmp);
       if Not VarIsNull(res) then idins:= Integer(res);
     end
  end;

  function DosToWin(st:String):String;
  var
    Ch: PChar;
  begin
    Ch := StrAlloc(length(st)+1);
    OemToAnsi(pchar(st),ch);
    Result:=ch;
    StrDispose(ch);
  end;

begin
  // �������� ���
  ListBox1.Items.Clear;

  // ��������� ���������� �� ���� � �������
  if not FileExists(fledImp.FileName) then
  begin
    AddLog('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Path := fledImp.FileName;
    DefaultSeparator := DecimalSeparator;

    Path := MakeMainFile(Path, StrCount); // �������� ��������� � Temp - ����
    AssignFile(ftext, Path);

    if ConvertWin.Checked then // ���� ���������� ��������������� ������ Temp- �����
    begin
       Memo1.Lines.LoadFromFile(Path); // ��������� ��������� ����
       s:=DosToWin(Memo1.Lines.Text);  // ������� Temp - �����
       Memo1.Lines.Text:=s;
       Memo1.Lines.SaveToFile(Path);
    end;

    headingToFill(Self, Path); // ���������� ���� ����-������� ��������� ��������

    reset(ftext);
    for i:=1 to 29 do readln(ftext); //�� ������������� ����������  //28
    EndMarker:=200;
    prbrImp.Min := 0;
    prbrImp.Max := StrCount - 50;
    Application.ProcessMessages;
    prbrImp.Step := 1;
    i:=1;
    readln(ftext,Tmp);
    Tmp:=ReplaceStr(Tmp, ',', '.');
    prbrImp.StepIt;

    CreateGoods(Tmp);
    IsIns := false;
    Tmp3 := '';

    //���� �� ���� ��������
    while (trim(Tmp)='') or (ExtractWord(1,Tmp,[' '])<>sEnd) do
    begin
      if ((trim(Tmp)='')or(Tmp[1]=' ')) and (not IsIns)  then
      begin
        readln(ftext,Tmp);
        prbrImp.StepIt;
        Tmp:=ReplaceStr(Tmp, ',', '.');
        continue;
      end;
     //  ����������� ��������
      Art:=CreateArt( Tmp, WriteSize);
      tmp4:=trim(Tmp);
      if Tmp4 = '' then begin
       tmp:=sEnd;
       continue;
      end;
     // ���������� ������� ������� ����������� ����� � "��� �����:"
      if (Tmp2 = '') or (Tmp2 = ' ') then
      begin
       readln(ftext,tmp3);
       prbrImp.StepIt;
       Tmp2:=ReplaceStr(Tmp3, ',', '.');
       Tmp3:=Tmp2;
       Tmp2_:=Tmp2;
       if tmp2_<>'' then delete(tmp2_,1,21);
       if ((CountWords (Tmp)-CountWords (Tmp2_))>1)and (trim(Tmp2)<>'') then
         IsIns:=true
       else
         IsIns:=false;

       //����������� ������� �������� � ������, ��������� �� ������� ������� ��������
       if IsIns then
       begin
         if pos('����',Tmp2)>0 then Art2:='-'
         else
          begin
           Art2:=(ExtractWord(3, Tmp2, [' ']));
           j := AnsiPos(Art2, Tmp2);
           Art2 := '';
           while (j <= Length(Tmp2)) and (Tmp2[j] <> '(') do
           begin
             if Tmp2[j] = ' ' then Art2 := Art2 + '0'
             else Art2 := Art2 + Tmp2[j];
             Inc(j);
           end;
           readln(ftext,Tmp2);
           prbrImp.StepIt;
       end;
       end
       else Art2:='-';
      end;

      // ����������� �������������: ������� ����, ��.�������, ����
      if Contrsize=1 then Sz:=ExtractWord(2, Tmp, [' '])
      else Sz:='-';
      W:=strtofloat2(ExtractWord(9+Contrsize, Tmp, [' ']));
      Un:=ansilowercase(ExtractWord(7+Contrsize, Tmp, [' ']));
      UnitId:=integer(pos('��',Un)>0);
      Price:=strtofloat2(ExtractWord(15+Contrsize, Tmp, [' ']));

     //����������� �������
      if ISIns then
      begin
        readln(ftext,Tmp2);
        prbrImp.StepIt;
        Tmp2:=ReplaceStr(Tmp2, ',', '.');
        MAinIns:=AnsiUpperCase(ExtractWord(1, Tmp2, [' ']));
        if (MAinIns = '�������') and (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) = '��') then
        MAinIns:='������� ��';
        // ���� ������ ������� ������� � ������ ��������
        if (Writesize=5) and (AnsiUpperCase(ExtractWord(2, Tmp2, [' ']))<>MainIns)  then
          begin
            if Not(MainIns = '���������') and Not(AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) = '��') then
              SZ:=ReplaceStr(AnsiLowerCase(ExtractWord(2, Tmp2, [' '])),'"','')
          end;
      end
      else MAinIns:='-';

      FindIdIns (MAinIns, Chrcs, Pattern, D_Ins_Id);
      if Chrcs<>'-1' then
       begin
        MAinIns := Chrcs;
        Pattern:=trim(Pattern);
       end
      else begin
        AddLog('�� ������� ���������� ������� '+MAinIns+' � '+inttostr(I));
        readln(ftext,Tmp);
        prbrImp.StepIt;
        Tmp:=ReplaceStr(Tmp, ',', '.');
        inc(i);
        continue;
      end;
      // ��������
      Mat :=ExtractWord(4+Contrsize, Tmp, [' ']);
      MIns:=trim(MAinIns);
      if IsIns and (mins='�') then mat:=mat+mins;

      Chrcs:=FindId(Mat,0,2);
      if Chrcs<>'-1' then mat:=Chrcs
      else begin
        AddLog('�� ������� ���������� �������� '+Mat);
        readln(ftext,Tmp);
        prbrImp.StepIt;
        inc(i);
        continue;
      end;
      //������
      Country := ExtractWord(5+Contrsize, Tmp, [' ']);
      Chrcs:=FindId(Country,0,3);
      if Chrcs<>'-1' then Country:= Chrcs
      else begin
        AddLog('�� ������� ���������� ������ '+Country);
        readln(ftext,Tmp);
        prbrImp.StepIt;
        inc(i);
        continue;
      end;
     //���� ����� ������� ������� � ����, �� ������������ ������� �������� ���,
     // ��� ���������� � ����, ����� �����, ����� �������� � �����
      CheckArt(Art);
      try
        res := ExecSelectSQL('select R_ART2ID,R_SELID,R_SINFOID from InsertItem_Adamas('#39+Art+#39','#39+Art2+#39',' + IntToStr(CompId)+ ','#39+Good+#39','#39+Mat+#39','#39+'��'#39+','+
             IntToStr(UnitId)+','#39+MainIns+#39', '+ FloatToStr2(W)+','#39+Sz+#39','+FloatToStr2(Price)+','+IntToStr(DocId) +','+FloatToStr2(FMargin)+', null)',quTmp);

        if Not VarIsNull(res) then
        begin
          Art2Id := Integer(res[0]);
          SelId := Integer(res[1]);
          SInfoId := Integer(res[2]);
        end;
        k:=1;
        Ins:=MainIns;
        if IsIns then
        begin
          while (k>0) do
            try
              if (trim(MainIns) = '�') and (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) = '��') then
              begin
                Tmp2 := ReplaceStr(Tmp2,'�� ','');
                Tmp2 := ReplaceStr(Tmp2,'�P ','');
              end;

              delete(tmp2,1,23);
              Tmp2:=ansilowercase(Tmp2);
              if Pattern<>'' then
              //����������
              begin
                StrHelp:=copy(Tmp2,1,30);
                StrHelpL:=1;
                StrHelp_:=ExtractSubstr(StrHelp,StrHelpL,[' ']);
                EdgShape:='';
                EDGTYPE:='';
                CLEANNES:='';
                CHROMATICITY:='';
                COLOR:='';
                GR:='';
                DecodePattern (StrHeLp);
                If trim(Ins)= '�' then EdgShape:='';
              end
              else
              begin
                EDGTYPE:='';
                EdgShape:='';
                CLEANNES:='';
                CHROMATICITY:='';
                COLOR:='';
                Gr:='';
              end;
              delete(tmp2,1,30);
              Tmp2:=ReplaceStr(Tmp2, ',', '.');
              QUANTITY:=strtoint(ExtractWord(1, Tmp2, [' ']));
              tmp2:=ReplaceStr(tmp2,'���.', ' ');
              Weight:=strtofloat2(ExtractWord(3, Tmp2, [' ']));
              if D_Ins_ID = 0 then
              begin
                D_Ins_ID := 0;
              end;
              if trim(ins)<>'�' then
              begin
                     ExecSQL('execute procedure ADDINS_Pr('+ inttostr(ART2ID)+','+inttostr(D_Ins_Id)+','+
                           inttostr(QUANTITY)+','+FloatToStr2(WEIGHT)+',null,'''+
                           CHROMATICITY+''','''+CLEANNES+''','''+EDGTYPE+''','''+EDGSHAPE+''','''+Gr+''', '+
                           inttostr(k)+' )', quTmp);
              end
              else
                     ExecSQL('execute procedure ADDINS_Pr('+ inttostr(ART2ID)+','+inttostr(D_Ins_Id)+', null'+
                           ', null, null,'''+
                           CHROMATICITY+''','''+CLEANNES+''','''+EDGTYPE+''','''+EDGSHAPE+''','''+Gr+''', '+
                           inttostr(k)+' )', quTmp);
              readln(ftext,Tmp2);
              prbrImp.StepIt;
              Tmp4 := trim(Tmp2);
              if (Tmp2[1]=' ') and (Tmp4<> '') then
              begin
                Ins:=AnsiUpperCase(ExtractWord(1, Tmp2, [' ']));
                if (Ins = '�������') and (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) = '��') then
                Ins:='������� ��';
                FindIdIns (Ins, Chrcs, Pattern, D_Ins_Id);
                if Chrcs <>'-1' then
                begin
                  Ins := Chrcs;
                  Pattern:=trim(Pattern);
                end
                else begin
                  AddLog('�� ������� ���������� ������� '+Ins+' � '+inttostr(I));
                  continue;
                end;
                inc(k);
              end
              else k:=0;
            except
              k:=0;
            end;
        end
        else Tmp2:='';

        // ������� ������� � ����� �� ���������,� ����� �� ����������������
        inc(i);

        if (IsIns) and (Tmp4 = '') then begin
          tmp:=sEnd;
          continue;
        end;
        if tmp2='' then tmp:=tmp3 else tmp:=tmp2;

        Tmp:=ReplaceStr(Tmp, ',', '.');
        CreateGoods(Tmp);

        Art_:=CreateArt( Tmp, WriteSize);
        CheckArt(Art_);

        readln(ftext,tmp3);
        prbrImp.StepIt;

        if ExtractWord(1,tmp3,[':']) = '�����' then
        begin
          tmp:=tmp3;
          continue;
        end;

        Tmp2:=ReplaceStr(Tmp3, ',', '.');
        Tmp2_:=Tmp3;
        if tmp2_<>'' then delete(tmp2_,1,21);
        if (CountWords (Tmp) - CountWords (Tmp2_) > 1)and (trim(Tmp3)<>'') then
           IsIns:=true
        else
           IsIns:=false;

        if IsIns then
        begin
          if pos('����',Tmp2)>0 then
            Art2_:='-'
          else
          begin
            Art2_:=ExtractWord(3, Tmp2, [' ']);
            j := AnsiPos(Art2_, Tmp2);
            Art2_ := '';
            while (j <= Length(Tmp2)) and (Tmp2[j] <> '(') do
            begin
              if Tmp2[j] = ' ' then Art2_ := Art2_ + '0'
              else Art2_ := Art2_ + Tmp2[j];
              Inc(j);
            end;
            readln(ftext,Tmp2);
            prbrImp.StepIt;
          end;
        end
        else Art2_:='-';

        while (Art=Art_)and(Art2=Art2_)  do
        begin
          if Contrsize=1 then Sz:=ExtractWord(2, Tmp, [' '])
          else Sz:='-';

          W:=strtofloat2(ExtractWord(9+Contrsize, Tmp, [' ']));
          Un:=ansilowercase(ExtractWord(7+Contrsize, Tmp, [' ']));
          UnitId:=integer(pos('��',Un)>0);
          ExecSQL('execute procedure InsNItem('+IntToStr(Art2Id)+','+
              IntToStr(SelId)+','+IntToStr(SInfoId)+','+FloatToStr2(W)+
              ','#39+Sz+#39','+IntToStr(UnitId)+', 0)' ,quTmp);
          inc(i);
          if  (not IsIns ) then
            Tmp := Tmp3
          else
          begin
            k := 1;
            while k>0 do
            begin
              readln(ftext,tmp2);
              prbrImp.StepIt;
              if (Tmp2[1]<>' ') or(Tmp2[1] = '�') or  (Tmp2[1] = '�') then
              begin
                k := 0;
                tmp := tmp2;
              end;
            end;
          end;

          if (Tmp2[1] = '�') or (Tmp2[1] = '�') then break;
          Tmp:=ReplaceStr(Tmp, ',', '.');
          CreateGoods(Tmp);
          Art_:=CreateArt( Tmp, WriteSize);
          CheckArt(Art_);
          readln(ftext,Tmp3);
          prbrImp.StepIt;
          Tmp2:=ReplaceStr(Tmp3, ',', '.');
          Tmp2_:=Tmp2;
          if tmp2_<>'' then delete(tmp2_,1,21);
          if (CountWords (Tmp) - CountWords (Tmp2_) > 1)and(trim(Tmp2)<>'') then
           IsIns:=true
          else
           IsIns:=false;
          tmp4:=trim(Tmp);
          if Tmp4 = '' then
          begin
            tmp:=sEnd;
            continue;
          end;

          if IsIns then
          begin
            if pos('����',Tmp2)>0 then Art2:='-'
            else
            begin
              Art2_:=Tmp2;
              readln(ftext,Tmp2);
              prbrImp.StepIt;
            end;
          end
          else Art2_:='-';
        end;
      except
        AddLog('������ ��� ���������� ������� � ���.'+Art+' ('+IntToStr(i)+')');
        Tmp:=ReplaceStr(Tmp, ',', '.');
        inc(i);
      end;
      Art2:=Art2_;
    end
  finally
    CloseFile(ftext);
    DecimalSeparator:=DefaultSeparator;
    Screen.Cursor := crDefault;
    prbrImp.Position := prbrImp.Max;
  end
end;

//--- ���������� ���� ����-������� ����������� � Temp-�����,  ----//
//--- �.�. ������ ��� ������ ������ ��������� � ��������� WIN ---//
procedure TfmSInvImp.headingToFill(Sender: TObject; var Name: String);
var
  ftext: textfile;
  DocNo, DocDate, SupINN : string;
  r : Variant;
  i : integer;
begin
  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    for i:=1  to 14 do
    begin
      ReadLn(ftext,SupINN);
      if i= 9 then
         DocNo := ExtractWord(3, SupINN, [' ']);
    end;
    try
      taSList.Edit;
      if DocNo <> '' then
         taSListSSF.AsString := DocNo;
      taSList.Post;
    except
    end;
  finally
    CloseFile(ftext);
  end;
end;

//--- ���������� ����� ��������� ��� ������� (����� ������ �.-�.) ����������� �� ---//
//--- ��������� ����� ��������� (�� � ��������� DOS, �� ��� �� ������������� ��� ������ ---//
//--- �������� ������). ����� �������, ��� ���� ����������� ���� ����������� ������������� ---//
procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
  DocNo, DocDate, SupINN : string;
  r : Variant;
  i : integer;
begin
  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    for i:=1  to 14 do        //28
    begin
      ReadLn(ftext,SupINN);
      if i= 9 then            //17
      begin
        DocDate := ExtractWord(5, SupINN, [' ']);
      end
    end;
    ReadLn(ftext, SupINN);
    SupINN := ExtractWord(4, SupINN, [' ']);
    try
      taSList.Edit;
      if DocDate <> '' then
        taSListNDATE.AsDateTime := StrToDateTime(DocDate);
      if(SupINN <> '')then
      begin
        r := ExecSelectSQL('select D_CompId,PAYTYPEID,NDSID from D_Comp where INN="'+SupINN+'"', quTmp);
        if not VarIsNull(r[0]) then
        begin
         taSListSUPID.AsInteger := Integer(r[0]);
         taSListPAYTYPEID.AsInteger := Integer(r[1]);
         taSListNDSID.AsInteger := Integer(r[2]);
        end
      end;
      taSList.Post;
    except
    end;
  finally
    CloseFile(ftext);
  end;
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

//--- �������� Temp- ����� � ����������� ������ ��������� � ���� ����.    ---//
//--- ��� �������� ��� ����, ����� � �������� ������ ��������� � �������� ---//
//--- �� ������������� ������� �������� � �� ���������� � ���������� ���� ---//
Function TfmSInvImp.MakeMainFile(Path: string; var strCount: integer): string ;
var  ftext, TmpFile:textfile;
     currStr, N: string;
     i:integer;
begin
  if not FileExists(Path) then
  begin
    ListBox1.Items.Add('���� '+Path+' �� ����������!');
    eXit;
  end;
  AssignFile(ftext, Path);
  AssignFile(TmpFile, ExtractFilePath(Path) + 'AdamasTemp.Txt');
  ReWrite(TmpFile);
  Reset(ftext);
  strCount := 0;
  try
    i:=2;
    while not eof(ftext) do
    begin
      readln(ftext,currStr);
      currStr:=ReplaceStr(currStr, chr(39), ' ');
      Writeln(TmpFile,currStr);
      inc(i);
    end;
    strCount := i;
    RESULT := ExtractFilePath(Path) + 'AdamasTemp.Txt';
  finally
    CloseFile(ftext);
    CloseFile(TmpFile);
  end;
end;

function TfmSInvImp.strtofloat2(const s:string):double;
var tmps: string;
begin
 tmps := ReplaceStr(s, '.', ',');
 if s='' then result:=0
 else result:=strtofloat(tmps);
end;

function TfmSInvImp.FloatToStr2(const f:double):String;
var tmps: string;
begin
 tmps := FloatToStr(f);
 RESULT := ReplaceStr(tmps, ',', '.');
end;


procedure TfmSInvImp.Button1Click(Sender: TObject);
var s:string;

  function DosToWin(st:String):String;
  var
    Ch: PChar;
  begin
    Ch := StrAlloc(length(st)+1);
    OemToAnsi(pchar(st),ch);
    Result:=ch;
    StrDispose(ch);
  end;
begin
  s:=DosToWin(Memo1.Lines.Text);
  Memo1.Lines.Text:=s;
  Memo1.Lines.SaveToFile(fledImp.FileName);
end;

procedure TfmSInvImp.Button2Click(Sender: TObject);
begin
  Memo1.Lines.LoadFromFile(fledImp.FileName);
end;

procedure TfmSInvImp.Button3Click(Sender: TObject);
var s:string;

  function WinToDos(st:String):String;
  var
    Ch: PChar;
  begin
    Ch := StrAlloc(length(st)+1);
    AnsiToOem(pchar(st),ch);
    Result:=ch;
    StrDispose(ch);
  end;
begin
  s:=WinToDos(Memo1.Lines.Text);
  Memo1.Lines.Text:=s;
  Memo1.Lines.SaveToFile(fledImp.FileName);

end;

procedure TfmSInvImp.FormCreate(Sender: TObject);
begin
   ConvertWin.Checked:=True;
end;

end.





