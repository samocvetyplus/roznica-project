unit ImportAdamas_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 29.11.2011 14:05:46 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Documents and Settings\�������������\��� ���������\��� �������\Work\�������\Plugins\Import\Adamas\ImportAdamas.tlb (1)
// LIBID: {B2C7DFC5-C439-465E-B41D-BE94272DA312}
// LCID: 0
// Helpfile: 
// HelpString: ������ ��������� ������
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportAdamasMajorVersion = 1;
  ImportAdamasMinorVersion = 0;

  LIBID_ImportAdamas: TGUID = '{B2C7DFC5-C439-465E-B41D-BE94272DA312}';

  IID_IAdamas: TGUID = '{00C28DEA-EBEA-49DF-9C05-F52AA65D310F}';
  CLASS_Adamas: TGUID = '{1788869E-809E-4D28-A375-8F9784EFA031}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAdamas = interface;
  IAdamasDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Adamas = IAdamas;


// *********************************************************************//
// Interface: IAdamas
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {00C28DEA-EBEA-49DF-9C05-F52AA65D310F}
// *********************************************************************//
  IAdamas = interface(IDispatch)
    ['{00C28DEA-EBEA-49DF-9C05-F52AA65D310F}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IAdamasDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {00C28DEA-EBEA-49DF-9C05-F52AA65D310F}
// *********************************************************************//
  IAdamasDisp = dispinterface
    ['{00C28DEA-EBEA-49DF-9C05-F52AA65D310F}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoAdamas provides a Create and CreateRemote method to          
// create instances of the default interface IAdamas exposed by              
// the CoClass Adamas. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAdamas = class
    class function Create: IAdamas;
    class function CreateRemote(const MachineName: string): IAdamas;
  end;

implementation

uses ComObj;

class function CoAdamas.Create: IAdamas;
begin
  Result := CreateComObject(CLASS_Adamas) as IAdamas;
end;

class function CoAdamas.CreateRemote(const MachineName: string): IAdamas;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Adamas) as IAdamas;
end;

end.
