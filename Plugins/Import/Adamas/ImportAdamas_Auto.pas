unit ImportAdamas_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportAdamas_TLB, Contnrs;

const
  INN : PChar = '7716148554';

type
  TJewImportAdamas = class(TAutoObject, IAdamas)
    FAppHandle : integer;
  private
    // from IJewImportAdamas
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportAdamas }

uses ComServ, SInvImp, Forms;

procedure TJewImportAdamas.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportAdamas.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportAdamas, Class_Adamas, ciMultiInstance, tmApartment);
end.
