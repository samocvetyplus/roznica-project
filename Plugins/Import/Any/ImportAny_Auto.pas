unit ImportAny_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, Contnrs, ImportAny_TLB;

const
  INN : PChar = '';

type
  TJewImportAny = class(TAutoObject, IAny)
    FAppHandle : integer;
  private
    // from IJewImportCDM
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

  TImportAutoObjectFactory = class(TAutoObjectFactory)
  protected
    function GetProgID: string; override;
  end;

implementation

{ TJewImportCDM }

uses ComServ, SInvImp, Forms;

procedure TJewImportAny.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportAny.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

{ TImportAutoObjectFactory }

function TImportAutoObjectFactory.GetProgID: string;
begin
 if ClassName <> '' then Result := 'JewImport' + '.' + ClassName
 else Result := '';
end;

initialization

  TImportAutoObjectFactory.Create(ComServer, TJewImportAny, Class_Any, ciMultiInstance, tmApartment);

end.
