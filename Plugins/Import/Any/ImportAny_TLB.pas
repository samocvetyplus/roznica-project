unit ImportAny_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 31.01.2011 13:20:15 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Documents and Settings\�������������\��� ���������\��� �������\Work\�������\Plugins\Import\Any\ImportAny.tlb (1)
// LIBID: {E128F22D-91C2-4710-9E71-78E0EC3BB094}
// LCID: 0
// Helpfile: 
// HelpString: ������ ���������
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportAnyMajorVersion = 1;
  ImportAnyMinorVersion = 0;

  LIBID_ImportAny: TGUID = '{E128F22D-91C2-4710-9E71-78E0EC3BB094}';

  IID_IANY: TGUID = '{427137CC-33A0-4D08-B8B5-6E58FE1EC7D2}';
  CLASS_ANY: TGUID = '{6B030ADA-FEDB-4987-9BBE-03A296E7D8C3}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IANY = interface;
  IANYDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ANY = IANY;


// *********************************************************************//
// Interface: IANY
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {427137CC-33A0-4D08-B8B5-6E58FE1EC7D2}
// *********************************************************************//
  IANY = interface(IDispatch)
    ['{427137CC-33A0-4D08-B8B5-6E58FE1EC7D2}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IANYDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {427137CC-33A0-4D08-B8B5-6E58FE1EC7D2}
// *********************************************************************//
  IANYDisp = dispinterface
    ['{427137CC-33A0-4D08-B8B5-6E58FE1EC7D2}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoANY provides a Create and CreateRemote method to          
// create instances of the default interface IANY exposed by              
// the CoClass ANY. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoANY = class
    class function Create: IANY;
    class function CreateRemote(const MachineName: string): IANY;
  end;

implementation

uses ComObj;

class function CoANY.Create: IANY;
begin
  Result := CreateComObject(CLASS_ANY) as IANY;
end;

class function CoANY.CreateRemote(const MachineName: string): IANY;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ANY) as IANY;
end;

end.
