unit ImportCDM_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, JewImport_TLB, Contnrs;

const
  INN : PChar = '';

type
  TJewImportCDM = class(TAutoObject, ICDM)
    FAppHandle : integer;
  private
    // from IJewImportCDM
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportCDM }

uses ComServ, SInvImp, Forms;

procedure TJewImportCDM.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportCDM.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportCDM, Class_CDM, ciMultiInstance, tmApartment);

end.
