unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolEdit, DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons, 
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    mmLog: TMemo;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
  private
    FMargin : extended;

    aProducer: string;
    aProducerID: string;
    aSupplier: string;
    aSupplierID: string;
    aExternalDocument: string;
    aExternalDocumentDate: string;
    aCountryID: string;
    aFiscal: string;

    bProducer: string;
    bProducerID: Integer;
    bSupplier: string;
    bSupplierID: Integer;
    bExternalDocument: string;
    bExternalDocumentDate: TDateTime;
    bCountryID: string;
    bFiscal: Integer;


    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

type
  // ��� �������
  TMounting = record
    Name : string;
    Q : integer;
    W : double;
    EdgShape : string; // ����� �������
    EdgT : string; // ��� �������
    Cleannes : string; // �������
    Chromaticity : string; // ���������
    Igroup : string; // ������
    Color : string; // ����
  end;


var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������

implementation

uses dbUtil, RegExpr, Variants, RxStrUtils, Math, MsgDialog, ImportCDM_Auto,
     fmUtils, UtilLib;

resourcestring
  rcOrganizationNotFound = '������������� ��� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:VAriant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ��� � ��, ��� ������ ����������� ���
    //CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    //if VarIsNull(CompId) then
    //begin
    //  MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
    //  eXit;
    //end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
//    if VarIsNull(r) then
//    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
//    end
//    else begin
//      fmSInvImp.FMargin := r;
//      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
//      fmSInvImp.laMargin.Font.Color:=clBlack;
//    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    fmSInvImp.FMargin := 0;
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    {
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST(''NOW'' AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    fmSInvImp.taSListPAYTYPEID.AsInteger := ?;

    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger :=SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    }
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
 {
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  }
  // ������ ������
  Import;
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.Import;
var
  i: Integer;
  j: Integer;
  Art2ID: Integer;
  ftext:textfile;
  Line: string;
  Line2: string;
  ArticleCount: Integer;

  aUID: string;              // ������������� �������                 %10d
  aGOODID: string;           // ������������� ������                  %10s
  aMATID: string;            // ������������� ���������               %10s
  aART: string;              // �������                               %20s
  aART2: string;             // ������ �������                        %20s
  aSZ: string;               // ������                                %10s
  aUNITID: string;           // ������������� ������� ���������       %1d
  aW: string;                // ���                                   %8.2f
  aINSID: string;            // ������������� �������                 %10s
  aSPRICE0: string;          // ��������� ����                        %8.2f
  aPRICE: string;            // ��������� ����                        %8.2f
  aInsQ: string;              // ���������� �������                    %2d

  bUID: Integer;             // ������������� �������                 %10d
  bGOODID: string;           // ������������� ������                  %10s
  bMATID: string;            // ������������� ���������               %10s
  bART: string;              // �������                               %20s
  bART2: string;             // ������ �������                        %20s
  bSZ: string;               // ������                                %10s
  bUNITID: Integer;          // ������������� ������� ���������       %1d
  bW: Double;                // ���                                   %8.2f
  bINSID: string;            // ������������� �������                 %10s
  bSPRICE0: Double;          // ��������� ����                        %8.2f
  bPRICE: Double;            // ��������� ����                        %8.2f
  bInsQ: Integer;            // ���������� �������                    %2d

  aD_INSID: string;      // �������
  aQUANTITY: string;     // ����������
  aWEIGHT: string;       // ���
  aCOLOR: string;        // ����
  aCHROMATICITY: string; // ���������
  aCLEANNES: string;     // �������
  aGR: string;           // ������
  aEDGTYPEID: string;    // �������
  aEDGSHAPEID: string;   // �����

  bD_INSID: string;      // �������
  bQUANTITY: Integer;    // ����������
  bWEIGHT:Double;        // ���
  bCOLOR: string;        // ����
  bCHROMATICITY: string; // ���������
  bCLEANNES: string;     // �������
  bGR: string;           // ������
  bEDGTYPEID: string;    // �������
  bEDGSHAPEID: string;   // �����
  FirstD_INSID: string;
  bMainIns: string;

begin
  mmLog.Lines.Clear;
  if not FileExists(fledImp.FileName) then
  begin
    mmLog.Lines.Add('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
  AssignFile(ftext, fledImp.FileName);
  Reset(ftext);
  for i := 1 to 8 do ReadLn(ftext);
  ArticleCount := 0;
  while not eof(ftext) do
  begin
    Readln(ftext, Line);
    if (Line <> '') and (Line[1] <> '*') then Inc(ArticleCount);
  end;
  try
    Reset(ftext);
    for i := 1 to 8 do ReadLn(ftext);
    prbrImp.Min := 0;
    prbrImp.Max := ArticleCount;
    prbrImp.Step := 1;

    DecimalSeparator:='.';
    for i := 1 to ArticleCount do
    begin
      ReadLn(ftext, Line);
      Line := ReplaceStr(Line, ',', '.');

      aUID     := Trim(ExtractWord(2, Line, ['|']));  // ������������� �������                 %10d
      aGOODID  := Trim(ExtractWord(3, Line, ['|']));  // ������������� ������                  %10s
      aMATID   := Trim(ExtractWord(4, Line, ['|']));  // ������������� ���������               %10s
      aART     := Trim(ExtractWord(5, Line, ['|']));  // �������                               %20s
      aART2    := Trim(ExtractWord(6, Line, ['|']));  // ������ �������                        %20s
      aSZ      := Trim(ExtractWord(7, Line, ['|']));  // ������                                %10s
      aUNITID  := Trim(ExtractWord(8, Line, ['|']));  // ������������� ������� ���������       %1d
      aW       := Trim(ExtractWord(9, Line, ['|']));  // ���                                   %8.2f
      aINSID   := Trim(ExtractWord(10, Line, ['|'])); // ������������� �������                 %10s
      aSPRICE0 := Trim(ExtractWord(11, Line, ['|'])); // ��������� ����                        %8.2f
      aPRICE   := Trim(ExtractWord(12, Line, ['|'])); // ��������� ����                        %8.2f
      aInsQ    := Trim(ExtractWord(13, Line, ['|'])); // ���������� �������                    %2d

      bUID := StrToInt(aUID);
      bGOODID := aGOODID;
      bMATID := aMATID;
      bART := aART;
      bART2 := aART2;
      bSZ := aSZ;
      bUNITID := StrToInt(aUNITID);
      bW := StrToFloat(aW);
      bINSID := aINSID;
      bSPRICE0 := StrToFloat(aSPRICE0);
      bPRICE  := StrToFloat(aPRICE);
      bInsQ := StrToInt(aInsQ);

      Line2 := '';
      if bInsQ > 0  then
      begin
        ReadLn(ftext, Line2);
        Line2 := ReplaceStr(Line2, ',', '.');
        bMainIns := Trim(ExtractWord( 3, Line2, ['|'])); // �������
        bINSID := bMainIns;
      end;

      quTmp.Close;

      quTmp.SQL.Text := 'execute procedure IMPORT_ARTCILE2(:ART, :ART2, :GOOD, :MAT, :COUNTRY, :UNITID, :COMPID, :SUPID, :MAININS, :W, :SZ, :PRICE, :SINVID,       :DEPID, :PNDS, :NDSID, :MARGIN, :SSF, :SDATE, :SN, :NDATE, :UID_SUP, :I_PRICE_OUT, :I_Art_Point, :I_ART2_POINT)';

      quTmp.Prepare;
      quTmp.ParamByName('ART').AsString := bART;
      quTmp.ParamByName('ART2').AsString := bART2;
      quTmp.ParamByName('GOOD').AsString := bGOODID;
      quTmp.ParamByName('MAT').AsString := bMATID;
      quTmp.ParamByName('COUNTRY').AsString := bCountryID;
      quTmp.ParamByName('UNITID').AsInteger := bUNITID;
      quTmp.ParamByName('COMPID').AsInteger := bProducerID;
      quTmp.ParamByName('SUPID').AsInteger := bSupplierID;
      quTmp.ParamByName('MAININS').AsString := bINSID;
      quTmp.ParamByName('W').AsDouble := bW;
      quTmp.ParamByName('SZ').AsString := bSZ;
      quTmp.ParamByName('PRICE').AsDouble := bSPRICE0;
      quTmp.ParamByName('SINVID').AsInteger := taSListSINVID.AsInteger;
      quTmp.ParamByName('DEPID').AsInteger := taSListDepId.AsInteger;
      quTmp.ParamByName('PNDS').AsInteger := 0;
      quTmp.ParamByName('NDSID').AsInteger := taSListNDSID.AsInteger;
      quTmp.ParamByName('MARGIN').AsDouble := 0;
      quTmp.ParamByName('SSF').AsString := taSListSSF.AsString;
      quTmp.ParamByName('SDATE').AsDateTime := taSListSDATE.AsDateTime;
      quTmp.ParamByName('SN').AsInteger := taSListSN.AsInteger;
      quTmp.ParamByName('NDATE').AsDateTime := taSListNDATE.AsDateTime;
      quTmp.ParamByName('UID_SUP').AsInteger := bUID;
      quTmp.ParamByName('I_PRICE_OUT').AsDouble := bPRICE;
      quTmp.ParamByName('I_Art_Point').AsString := bART + '.';
      quTmp.ParamByName('I_Art2_Point').AsString := bART2 + '.';
      try
        quTmp.ExecQuery;
        tr.CommitRetaining;
        Art2ID := quTmp.Fields[0].AsInteger;
        quTmp.Close;
      except
        on E:Exception do
          begin
            prbrImp.StepIt;
            Continue;
          end
      end;
      FirstD_INSID := '';

      if Art2ID > 0 then
      begin
        j:=1;
        while j <= bInsQ do
        begin
          if Line2 = '' then
          begin
            ReadLn(ftext, Line);
            Line := ReplaceStr(Line, ',', '.');
          end
          else
          begin
            Line := Line2;
            Line2 := '';
          end;

          aD_INSID      := Trim(ExtractWord( 3, Line, ['|'])); // �������
          aQUANTITY     := Trim(ExtractWord( 4, Line, ['|'])); // ����������
          aWEIGHT       := Trim(ExtractWord( 5, Line, ['|'])); // ���
          aCOLOR        := Trim(ExtractWord( 6, Line, ['|'])); // ����
          aCHROMATICITY := Trim(ExtractWord( 7, Line, ['|'])); // ���������
          aCLEANNES     := Trim(ExtractWord( 8, Line, ['|'])); // �������
          aGR           := Trim(ExtractWord( 9, Line, ['|'])); // ������
          aEDGTYPEID    := Trim(ExtractWord(10, Line, ['|'])); // �������
          aEDGSHAPEID   := Trim(ExtractWord(11, Line, ['|'])); // �����

          bD_INSID      := aD_INSID;
          bQUANTITY     := StrToInt(aQUANTITY);
          bWEIGHT       := StrToFloat(aWEIGHT);
          bCOLOR        := aCOLOR;
          bCHROMATICITY := aCHROMATICITY;
          bCLEANNES     := aCLEANNES;
          bGR           := aGR;
          bEDGTYPEID    := aEDGTYPEID;
          bEDGSHAPEID   := aEDGSHAPEID;

          if (bInsQ > 1) and (j = 1) then FirstD_INSID := bD_INSID;

          quTmp.Close;
          quTmp.SQL.Text := 'execute procedure IMPORT_ARTCILE_INSERTION (:I_Art2id, :I_Insid, :I_Quantity, :I_Weight, :I_Color, :I_Chromaticity, :I_Cleannes, :I_Edgtypeid, :I_Edgshapeid, :I_Gr, :I_K)';
          quTmp.Prepare;
          quTmp.ParamByName('I_Art2id').AsInteger := Art2Id;
          quTmp.ParamByName('I_Insid').AsString := bD_INSID;
          quTmp.ParamByName('I_Quantity').AsInteger := bQUANTITY;
          quTmp.ParamByName('I_Weight').AsDouble := bWEIGHT;

          if bCOLOR ='' then quTmp.ParamByName('I_Color').AsVariant := Null
          else quTmp.ParamByName('I_Color').AsString := bCOLOR;

          if bCHROMATICITY ='' then quTmp.ParamByName('I_Chromaticity').AsVariant := Null
          else quTmp.ParamByName('I_Chromaticity').AsString := bCHROMATICITY;

          if bCLEANNES = '' then quTmp.ParamByName('I_Cleannes').AsVariant := Null
          else quTmp.ParamByName('I_Cleannes').AsString := bCLEANNES;

          if bEDGTYPEID = '' then quTmp.ParamByName('I_Edgtypeid').AsVariant := Null
          else quTmp.ParamByName('I_Edgtypeid').AsString := bEDGTYPEID;

          if bEDGSHAPEID = '' then quTmp.ParamByName('I_Edgshapeid').AsVariant := Null
          else quTmp.ParamByName('I_Edgshapeid').AsString := bEDGSHAPEID;

          if bGR = '' then quTmp.ParamByName('I_Gr').AsVariant := Null
          else quTmp.ParamByName('I_Gr').AsString := bGR;

          quTmp.ParamByName('I_K').AsInteger := j;

          quTmp.ExecQuery;
          tr.CommitRetaining;
          Inc(j);
        end;
        {if FirstD_INSID <> '' then
        ExecSQL('execute procedure SetMainIns('+IntToStr(Art2Id)+',' + FirstD_INSID + ')', quTmp);}
      end
      else
      begin
        j:=1;
        while j <= bInsQ do
        begin
          ReadLn(ftext, Line);
          Inc(j);
        end;
      end;
      prbrImp.StepIt;
    end;
  finally
    CloseFile(fText);
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
begin
  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    ReadLN(ftext, aSupplier);
    ReadLN(ftext, aSupplierID);
    ReadLN(ftext, aExternalDocument);
    ReadLN(ftext, aExternalDocumentDate);
    ReadLN(ftext, aProducer);
    ReadLN(ftext, aProducerID);
    ReadLN(ftext, aCountryID);
    ReadLN(ftext, aFiscal);
    bSupplier := Trim(aSupplier);
    bSupplierID := StrToInt(Trim(aSupplierID));
    bProducer := Trim(aProducer);
    bProducerID := StrToInt(Trim(aProducerID));
    bExternalDocument := Trim(aExternalDocument);
    bExternalDocumentDate := StrToDate(Trim(aExternalDocumentDate));
    bCountryID := aCountryID;
    bFiscal := StrToInt(aFiscal);
    {
    try
      taSList.Edit;
      taSListSSF.AsString := bExternalDocument;
      taSListNDATE.AsDateTime := bExternalDocumentDate;
      taSListSUPID.AsInteger := bSupplierID;
      taSListNDSID.AsInteger := bFiscal;
      taSList.Post;
    except
    end;
    }
  finally
    CloseFile(ftext);
  end;
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

end.




























