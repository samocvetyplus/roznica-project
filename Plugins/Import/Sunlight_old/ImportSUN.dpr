library ImportSUN;

uses
  ComServ,
  SInvImp in 'SInvImp.pas' {fmSInvImp},
  ImportSUN_Auto in 'ImportSUN_Auto.pas',
  ImportSUN_TLB in 'ImportSUN_TLB.pas';

const
  ClassName : PChar = 'JewImport.SUN';
  Version : PChar = '1.0';
  PluginName : PChar = '������ ��������� SUNLIGHT';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� SUNLIGHT. ��� 7726664484';

{$R *.TLB}

{$R *.RES}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
 