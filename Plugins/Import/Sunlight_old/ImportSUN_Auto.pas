unit ImportSUN_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, JewImport_TLB, Contnrs;

const
  INN : PChar = '7726664484';

type
  TJewImportSUN = class(TAutoObject, ISUN)
    FAppHandle : integer;
  private
    // from IJewImportSUN
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportSUN }

uses ComServ, SInvImp, Forms;

procedure TJewImportSUN.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportSUN.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportSUN, Class_SUN, ciMultiInstance, tmApartment);

end.
