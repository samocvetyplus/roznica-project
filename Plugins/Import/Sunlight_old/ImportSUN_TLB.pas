unit ImportSUN_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 31.10.2012 14:06:44 from Type Library described below.

// ************************************************************************  //
// Type Lib: E:\��� ���������\��� �������\Work\�������\Plugins\Import\Sunlight_old\ImportSUN.tlb (1)
// LIBID: {D3E99034-21F7-4574-BCB8-C29904994191}
// LCID: 0
// Helpfile: 
// HelpString: ������ ��������� SUNLIGHT
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportSUNMajorVersion = 1;
  ImportSUNMinorVersion = 0;

  LIBID_ImportSUN: TGUID = '{D3E99034-21F7-4574-BCB8-C29904994191}';

  IID_ISUN: TGUID = '{33A102BE-9759-410E-82BB-BFFC3B750FA6}';
  CLASS_SUN: TGUID = '{30C74206-CE7F-444E-9F81-545384C6E41B}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISUN = interface;
  ISUNDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SUN = ISUN;


// *********************************************************************//
// Interface: ISUN
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {33A102BE-9759-410E-82BB-BFFC3B750FA6}
// *********************************************************************//
  ISUN = interface(IDispatch)
    ['{33A102BE-9759-410E-82BB-BFFC3B750FA6}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  ISUNDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {33A102BE-9759-410E-82BB-BFFC3B750FA6}
// *********************************************************************//
  ISUNDisp = dispinterface
    ['{33A102BE-9759-410E-82BB-BFFC3B750FA6}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoSUN provides a Create and CreateRemote method to          
// create instances of the default interface ISUN exposed by              
// the CoClass SUN. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSUN = class
    class function Create: ISUN;
    class function CreateRemote(const MachineName: string): ISUN;
  end;

implementation

uses ComObj;

class function CoSUN.Create: ISUN;
begin
  Result := CreateComObject(CLASS_SUN) as ISUN;
end;

class function CoSUN.CreateRemote(const MachineName: string): ISUN;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SUN) as ISUN;
end;

end.
