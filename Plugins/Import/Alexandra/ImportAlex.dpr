library ImportAlex;

uses
  ComServ,
  ImportAlex_TLB in 'ImportAlex_TLB.pas',
  ImportAlex_Auto in 'ImportAlex_Auto.pas',
  SinvImp in 'SinvImp.pas' {fmSInvImp},
  SNameSup in 'SNameSup.pas' {fmSNameSup};

const
  ClassName : PChar = 'ImportALEX.ALEX';
  Version : PChar = '1.1';
  PluginName : PChar = '������ ��������� ����������';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ����������. ��� 3123026420';

{$R *.TLB}

{$R *.RES}
function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
