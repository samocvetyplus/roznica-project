unit ImportAlex_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportAlex_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '440112710372';

type
  TALEX = class(TAutoObject, IALEX)
  FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TALEX.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TALEX.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TALEX, Class_ALEX, ciMultiInstance, tmApartment);
end.
