unit ImportAlex_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 23.05.2014 12:51:47 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\Alexandra\ImportAlex.tlb (1)
// LIBID: {BC4AF0E6-9F71-4336-A6DF-741BE835E83B}
// LCID: 0
// Helpfile: 
// HelpString: ImportAlex Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportAlexMajorVersion = 1;
  ImportAlexMinorVersion = 0;

  LIBID_ImportAlex: TGUID = '{BC4AF0E6-9F71-4336-A6DF-741BE835E83B}';

  IID_IALEX: TGUID = '{5A4871C7-F9DA-4C5C-8BAD-EAEBB17E4D2B}';
  CLASS_ALEX: TGUID = '{F4160862-1E9F-4CC1-BC04-F982BEB353F4}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IALEX = interface;
  IALEXDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ALEX = IALEX;


// *********************************************************************//
// Interface: IALEX
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5A4871C7-F9DA-4C5C-8BAD-EAEBB17E4D2B}
// *********************************************************************//
  IALEX = interface(IDispatch)
    ['{5A4871C7-F9DA-4C5C-8BAD-EAEBB17E4D2B}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IALEXDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {5A4871C7-F9DA-4C5C-8BAD-EAEBB17E4D2B}
// *********************************************************************//
  IALEXDisp = dispinterface
    ['{5A4871C7-F9DA-4C5C-8BAD-EAEBB17E4D2B}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoALEX provides a Create and CreateRemote method to          
// create instances of the default interface IALEX exposed by              
// the CoClass ALEX. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoALEX = class
    class function Create: IALEX;
    class function CreateRemote(const MachineName: string): IALEX;
  end;

implementation

uses ComObj;

class function CoALEX.Create: IALEX;
begin
  Result := CreateComObject(CLASS_ALEX) as IALEX;
end;

class function CoALEX.CreateRemote(const MachineName: string): IALEX;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ALEX) as IALEX;
end;

end.
