unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ToolEdit,} DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, rxToolEdit,
  ADODB, DBF, BDE, IdGlobal;

const fsup = 7;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    dsDBFimp: TDataSource;
    DBFconnect: TADOConnection;
    taDBFimp: TADOTable;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

type
  // ��� �������
  TMounting = record
    Name : string;
    Q : integer;
    W : double;
    EdgShape : string; // ����� �������
    EdgT : string; // ��� �������
    Cleannes : string; // �������
    Chromaticity : string; // ���������
    Igroup : string; // ������
    Color : string; // ����
  end;

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������
  SSF: string;

  Kategory: integer; // ��������� ������� ������������ (�� D_DName_Sup)
  NameSup: string; // ������� ������������ � ����������
  

implementation


uses dbutil, Variants, RxStrUtils, Math, MsgDialog, ImportAlex_Auto,
     fmUtils, UtilLib, DBTables, SNameSup;

resourcestring
  rcOrganizationNotFound = '������������� ���������� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';
  rsSelectFile='�� ������ ���� �������';

{$R *.DFM}


procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:VAriant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;

    CompId := ExecSelectSQL('select D_CompId from D_Comp where d_compid=2100000224', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;

    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST(''NOW'' AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger :=SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
//ShowMessage('������ �������');    ����������������!!!
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  if ListBox1.Items.Text='' then ListBox1.Items.Text:='��������� ��������������� �������!';
  btnClose.SetFocus;
end;

procedure TfmSInvImp.Button1Click(Sender: TObject);
begin
ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
fmSNameSup.pgctrl.ActivePageIndex:=2;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.FormShow(Sender: TObject);
begin

end;

{******* ������ ��������� *******}
procedure TfmSInvImp.Import;
var
  mi: string;
  m1, m2, ii: integer;
  st1, st2, st3, st4 :string;

  ftext:textfile;
  UnitId,EndMarker, i, k, j : integer;
  Art2Id: integer;
  Tmp, Tmp2,
  Art, Art2, tmpArt, tmpArt2, ostArt,
  Good, Ins1,
  Mat, MIns,
  Sz ,Un, Country, SupId, UID_Sup,
  MainIns, tmpIns, Ins : string;
  Price, W : double;
  Log: TStringlist;
  {********* ������� ��� - ����� ********************************************}
  procedure AddLog(Msg : string);
  begin
     ListBox1.Items.Add(Msg);
     ListBox1.Refresh;
  end;
  {** �������� ������������ �������� ������������, ������������� ������-��.
      D_SNameSup - ������� ������������� ������� ������������, �������������
      ����������, � �������� �������������� ���������� *********************}
  function FindId (supname:string; fsup, ftable:integer):string;
  var
    r :Variant;
    table, id: string;
  begin
  case ftable of
   0: begin table:='d_good'; id:='d_goodid' end;
   1: begin table:='d_ins'; id:='d_insid' end;
   2: begin table:='d_mat'; id:='d_matid' end;
   3: begin table:='d_country'; id:='d_countryid' end;
  end;  // ������� ���� ������� ������������ � ����� �����-�
    r := ExecSelectSQL('select first 1 '+ id + ' from ' + table + ' where NAME = ''' +supname+'''', quTmp);
    if r=null then
    begin // ���� �� ����� - �� � ��������� ����������� ������������� ��� �����������
      r := ExecSelectSQL('select first 1 SNAME from D_SNAMESUP where SNAME_SUP = ''' +supname+
                         ''' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable)+' order by SNAME', quTmp);
      if r=null then Result:='-1' else Result:= trim(VarToStr(r));
    end
      else Result:= trim(VarToStr(r));
  end;
  {** ����-��� ���������� true, ���� ����� ���������� � ��������� ����� *******}
  function RegisterLowerCase(Ch: string):Boolean;
  var
    CH2: string;
  begin
    CH2 := AnsiLowerCase(Ch); // �������� � ������� ��������
  if CH2 = Ch then
    Result := false
  else
    Result := true;
  end;
  {** ����-��� ���������� true, ���� ����� �� ����� - �������������� **********}
  function ActivAdjective(s: string):boolean;
  var
    c: integer;
    s2: string;
    SL: TStringList;
  begin
  SL :=  TStringList.Create;
  SL.Delimiter := ',';
  SL.StrictDelimiter := true;
  SL.DelimitedText:='��, ��, ��';
  if length(s)>2 then
  begin
    s2:=copy(s, length(s)-1, length(s));
    for c := 0 to SL.Count - 1 do
    begin
      if (s2=SL[c]) then
         if StrPos(pchar(s2), pchar(SL[c])) = nil then
            result:=false else result:=true;
    end;
  end else result:=false;
  end;
{******************************   IMPORT   ************************************}
var
  Mounting : TMounting;
  p,pi:integer;
  tmpEdgT:double;
  ogr:string;
  f: boolean; // ������� - ��������������
  BrillIns, insert_ins: boolean; // ������� ��������� ��� ���� ����.������
  res, r_ins: variant;
  TS, TSF, TSWord, TSChars : TStringList;
  str, un_tmp : string;
begin
  TS :=  TStringList.Create;
  TS.Delimiter := ';';
  TS.StrictDelimiter := true;
  TSF :=  TStringList.Create;
  TSF.Delimiter := ' ';
  TSF.StrictDelimiter := true;
  TSWord := TStringList.Create;
  TSWord.Delimiter := ' ';
  TSWord.StrictDelimiter := true;
  TSChars := TStringList.Create;
  TSChars.Delimiter := '/';
  TSChars.StrictDelimiter := true;
  // �������� ���
  ListBox1.Clear; //**************
  Log:=TStringList.Create;
  // ��������� ���������� �� ���� � �������
  if not FileExists(fledImp.FileName) then
  begin
    ListBox1.Items.Add('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
  try
    //����������� � ����� dbf
    taDBFimp.Active:=false;
    DBFconnect.Connected:=false;
    DBFconnect.ConnectionString:='Data Source="'
                                 +trim(ExtractFilePath(fledImp.FileName)) +
                                 '";Extended Properties="DBASE IV;";"';
    DBFconnect.LoginPrompt:=false;
    DBFconnect.Mode:=cmReadWrite;
    DBFconnect.Connected:=true;
    taDBFimp.TableName:=SSF;
    taDBFimp.Active:=true;
    //����������� �����������
    //��������� �������
    prbrImp.Min := 0;
    prbrImp.Max := taDBFimp.RecordCount;
    prbrImp.Step := 1;
    ShowMessage('���������� ������� - '+IntToStr(taDBFimp.RecordCount));
    DecimalSeparator:='.';
    SupId:=taSupD_COMPID.AsString;
    // �������� ���� �� ��������
    try
      with taDBFimp do
      begin
        first;
        while not taDBFimp.eof do
        begin
          Uid_Sup:= trim(taDBFimp.FieldByName('SH_KOD').AsString);  // ����. ��� �������
          Log.Add('Uid_Sup '+Uid_Sup);
          Sz := trim(taDBFimp.FieldByName('RAZMER').AsString);      // ������
          //W := StrToFloat(StringReplace(taDBFimp.FieldByName('VES').AsString, ',', '.', []));  // ���
          W := taDBFimp.FieldByName('VES').Asfloat;  // ���
          Country:='��';  // ������ ������������
          try  // �� ������ � ����� ������� ��. ���� ������ ���� ��� - �� �� ��������� ��.
            un:= ansilowercase(trim(taDBFimp.FieldByName('ED').AsString));
            UnitId := integer(pos('��',Un)>0);
          except
            UnitId := 1;
          end;
          Price:= taDBFimp.FieldByName('CENA').AsFloat;        // ����. ����
          // �������� ��������
          Mat := trim(taDBFimp.FieldByName('PROBA').AsString);
          if (Mat='0') or (Mat='585') then Mat:='������ 585';
          if length(taDBFimp.FieldByName('KAMNI').AsString) > 0 then Mat:='�������     585';
          MIns:= FindId(Mat,fsup, 2);
          Log.Add('MIns '+MIns);
          if MIns='-1' then
          begin
            if MessageDialog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup+
                             '. ����������� ������ �������� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes
            then begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=2;
                NameSup:=Mat;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                MIns:= FindId(Mat,fsup, 2);
                if (MIns = '-1') then
                   AddLog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup)
                   else Mat := MIns;
                Screen.Cursor:=crDefault;
              end;
            end
            else begin
              AddLog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup);
            end;
            end
            else Mat := MIns;
          // �������� ��� � ���2
          if mat='��' then
          begin
            tmpArt :=trim(taDBFimp.FieldByName('UNIKAL').AsString);
            if WordCount(tmpArt, [' ']) > 1 then
            begin
              Art:=copy(tmpArt, 1, pos(' ',tmpArt)-1); // 1-� ����� - �������
              tmpArt2 := copy(tmpArt, pos('�',tmpArt), (Length(tmpArt)-pos('�',tmpArt)+1));
              if tmpArt2[1] = '�' then System.Delete(tmpArt2, 1, 1);
                 Art2:=StringReplace(tmpArt2,' ','',[RFReplaceall]);
            end
          end else
          begin
            Art :=trim(taDBFimp.FieldByName('TOVAR').AsString);
            Art2:='-';
          end;
          // �������� �������� �������
          MainIns := '';
          mi := AnsiLowerCase(taDBFimp.FieldByName('KAMNI').AsString); // ����������� � �������
          if mi='' then
          begin
            mi := taDBFimp.FieldByName('VSTAVKI').AsString; //����������� � ������.��.
            TSF.DelimitedText := mi; // ��������� ������ �������
            TS.DelimitedText := '';
            // ��� ������.��. ������� ��������� �������� �������. ��� ���� ���
            // ������� ������ � �������� ����������: ����������� ������ �����-��
            // ���������, � ����� ������� ����������� ��� ������������.
            // �������� �� ������ ��� ����� � ������ ��������� ������, �������
            // �� ������ �������� � ������ ���������� - ������� �� ���������.
            f:=false;
            for k := 0 to TSF.Count-1 do
            begin
              if RegisterLowerCase(TSF[k]) or (f) then // ���� ����� � ��������� �����
              begin                                  // ��� ������-� ����� - ������-�
                if ActivAdjective(TSF[k]) then // ���� ����� - ��������������
                begin
                  f:=true;
                  next;
                end
                else begin
                   if ((length(TSF[k])>3) or (f)) then // ���� ��.>3 ����. ��� ��� ��. - ��������� �� ������-��
                   begin
                     if ((TSF[k]<>'�����') and (TSF[k]<>'������')) then
                     begin
                       TS.Add(AnsiLowerCase(trim(TSF[k])));
                       f:=false;
                     end;
                   end;
                end;
              end;
            end;
          end else TS.DelimitedText := AnsiLowerCase(mi);

          if (TS.Count = 0) then  MainIns:='-'  // ���� ������� ��� �������
          else  // ����� ��������� �������� �������...
          begin
            TSWord.DelimitedText := trim(TS[0]);
            if Mat <> '��' then // ��� ���������� ������ ���. ������� - 1� ������
            begin
             MainIns := TSWord.DelimitedText;
            end else // ��� ������������� ������:
            // ���� ������� ����� ����-� � ������ ������, �� ���.���.- ���������,
            //����� - ������ ������ �� ������
            begin
            if StrPos(pchar(mi), '���������') <> nil then  MainIns := '���������' else
            begin
                 MainIns := TSWord[1];
            end;
          end;

          tmpIns:= FindId(MainIns, fsup, 1);
          if(tmpIns = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup+
                             '. ����������� ������ ������� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes
            then begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=1;
                NameSup:=MainIns;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                tmpIns:= trim(FindId(MainIns, fsup, 1));
                if (tmpIns = '-1') then
                   AddLog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup)
                   else MainIns := tmpIns;
                Screen.Cursor:=crDefault;
              end;
            end
            else begin
              AddLog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup);
            end;
          end
          else MainIns := tmpIns;
          end;
          // �������� �����
          Good:= AnsiLowerCase(taDBFimp.FieldByName('TIP').AsString);
          Ins1:= FindId(Good,fsup, 0);
          if(Ins1 = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ����� '+Good+' � '+UID_Sup+
                             '. ����������� ������ ����� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes
            then begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=0;
                NameSup:=Good;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                Ins1:= FindId(Good,fsup, 0);
                if (Ins1 = '-1') then
                   AddLog('�� ������� ���������� ����� '+Good+' � '+UID_Sup)
                   else Good := Ins1;
                Screen.Cursor:=crDefault;
              end;
            end
            else begin
              AddLog('�� ������� ���������� ����� '+Good+' � '+UID_Sup);
            end;
            end
            else Good := Ins1;

     // ���� ����� ������� ������� � ����, �� ���2 �������� ���,
     // ��� ���������� � ����, ����� �����, ����� �������� � �����
     // *������ ��� ���������� ������. ��� ����-�� ��. ���2 ������ ����� ����-��
       {  if (mat <> '��') then
         begin
         res := ExecSelectSQL('select r_art2, r_ins, r_good from InsertItem_Diamant('#39+Art+#39','#39+Art2+#39','
                              + VarToStr(CompId) + ','#39 + trim(Good)+ #39','#39 + trim(Mat) +
                              #39','#39 +trim(Country) + #39 + ',' + IntToStr(UnitId) +','#39 + MainIns +
                              #39', '+ FloatToStr(Price) + ',' +IntToStr(DocId) +')', quTmp);

         if not VarIsNull(res[0]) then // ������ ��� ���������� ������
            Art2 := res[0];
         if not VarIsNull(res[1]) then
            MainIns := res[1];
         if not VarIsNull(res[2]) then
            Good := res[2];
         end;   }                     

      // ������� ������� � ���������
      quTmp.Close;
      quTmp.SQL.Text := 'execute procedure InsFItem(:ART, :ART2, :GOOD, :MAT, :COUNTRY,'+
        ':UNITID, :COMPID, :SUPID, :MAININS, :W, :SZ, :PRICE, :SINVID, :DEPID, :PNDS, :NDSID,'+
        ':MARGIN, :SSF, :SDATE, :SN, :NDATE, :UID_SUP)';
      quTmp.Prepare;
      quTmp.ParamByName('ART').AsString := Art;
      quTmp.ParamByName('ART2').AsString := Art2;
      quTmp.ParamByName('GOOD').AsString := Good;
      quTmp.ParamByName('MAT').AsString := Mat;
      quTmp.ParamByName('COUNTRY').AsString := Country;
      quTmp.ParamByName('UNITID').AsInteger := integer(UnitId);
      quTmp.ParamByName('COMPID').AsInteger := StrToInt(CompId);
      quTmp.ParamByName('SUPID').AsInteger := taSListSUPID.AsInteger;
      quTmp.ParamByName('MAININS').AsString := MainIns;
      quTmp.ParamByName('W').AsDouble := W;
      quTmp.ParamByName('SZ').AsString := Sz;
      quTmp.ParamByName('PRICE').AsDouble := Price;
      quTmp.ParamByName('SINVID').AsInteger := taSListSINVID.AsInteger;
      quTmp.ParamByName('DEPID').AsInteger := taSListDepId.AsInteger;
      quTmp.ParamByName('PNDS').AsInteger := taNDSNDS.AsInteger;
      quTmp.ParamByName('NDSID').AsInteger := taSListNDSID.AsInteger;
      quTmp.ParamByName('MARGIN').AsDouble := FMargin;
      quTmp.ParamByName('SSF').AsString := taSListSSF.AsString;
      quTmp.ParamByName('SDATE').AsDateTime := taSListSDATE.AsDateTime;
      quTmp.ParamByName('SN').AsInteger := taSListSN.AsInteger;
      quTmp.ParamByName('NDATE').AsDateTime := taSListNDATE.AsDateTime;

      try
        quTmp.ExecQuery;
        tr.CommitRetaining;
        Art2Id := quTmp.Fields[0].AsInteger;
        quTmp.Close;
      except
        on E:Exception do
          begin
            AddLog('������ ��� ���������� ������� � ���.'+Art+' ('+IntToStr(i)+')'+' - '+e.Message);
            log.Add('������'+e.Message);
          end
      end;
      // ����� ���������� � ���������
      // ������ �������� �������
        for j := 0 to TS.Count - 1 do
        begin
          TSWord.DelimitedText := trim(TS[j]);
          if(j=0)then
          begin
            quTmp.Close;
            quTmp.SQL.Text := 'delete from Ins where Art2Id='+IntToStr(Art2Id);
            Log.Add('if(j=0)then '+ quTmp.SQL.Text);
            quTmp.Prepare;
            quTmp.ExecQuery;
            quTmp.Transaction.CommitRetaining;
          end;
          if Mat = '��' then
          begin // � ������������� ����������� ��� ��������������
            Ins:= TSWord[1]; // ����. ������� - 2� �����
            Mounting.Q:= StrToInt(TSWord[0]); //����������
            if TSWord[2] = '�.' then
            begin
              Mounting.EdgShape:=TSWord[3]; //����� �����
              Mounting.EdgT:=TSWord[4]; //�������
              TSWord[5] := StringReplace(TSWord[5], ',', '.', []);
              TSWord[5] := StringReplace(TSWord[5], '-', '.', []);
              Mounting.W:= StrToFloatDef(TSWord[5],0); //���
              TSChars.DelimitedText := TSWord[6];
            end
            else
            begin
              Mounting.EdgShape:=TSWord[2]; //����� �����
              Mounting.EdgT:=TSWord[3]; //�������
              TSWord[4] := StringReplace(TSWord[4], ',', '.', []);
              TSWord[4] := StringReplace(TSWord[4], '-', '.', []);
              Mounting.W:= StrToFloatDef(TSWord[4],0); //���
              TSChars.DelimitedText := TSWord[5];
            end;
            //�������� �������� � ����������������� ��������
            Mounting.EdgT:=StringReplace(Mounting.EdgT,'x','*',[]);
            Mounting.EdgT:=StringReplace(Mounting.EdgT,',','.',[]);
            Mounting.Cleannes:=TSChars[1];  // �������
            Mounting.Chromaticity:=TSChars[0]; // ���������
            if not IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)]) then
            begin
               Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)]; // ������
               System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
            end;
            if Mounting.Chromaticity='�' then
            begin
               Ins:='������ ���������';
               Mounting.Cleannes:='';  // �������
               Mounting.Chromaticity:=''; // ���������
            end;
            Mounting.Color:='';
          end else
          begin // ��� ���������� ������ ����������� � ���� ������ ������������ �������
            Ins:=TSWord.DelimitedText;
            Mounting.Q:=0;
            Mounting.W:=0;
            Mounting.EdgShape:='';
            Mounting.EdgT:='';
            Mounting.Cleannes:='';
            Mounting.Chromaticity:='';
            Mounting.Igroup:='';
            Mounting.Color:='';
          end;

          tmpIns:=findId(Ins,fsup,1); // ���� � ������� �������������
          if(tmpIns = '-1')then
          begin
          if MessageDialog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup+
                           '. ����������� ������ ������� ������������������ ��������?',
                           mtWarning, [mbYes, mbNo], 0)=mrYes then
          begin
            try
              Screen.Cursor:=crSQLWait;
              Kategory:=1;
              NameSup:=Ins;
              ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
            finally
              tmpIns:= trim(FindId(Ins, fsup, 1));
              if (tmpIns = '-1') then
                 AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup)
                 else Ins := tmpIns;
              Screen.Cursor:=crDefault;
            end;
          end
            else begin
              AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup);
              end;
          end
            else Ins := tmpIns;

          Mounting.Name:=Ins;

          if mat<>'��' then // ��� ���������� ������
          begin
          r_ins := ExecSelectSQL('select first 1 d_insid from Ins where Art2Id = ' +
                   Inttostr(Art2Id) + ' and d_insid = ''' + Ins+'''', quTmp);

          if r_ins = null then // ���� ������� ��� �� �������� � ��������, �� ��������
             insert_ins:=true
             else insert_ins:=false;
          end
          else insert_ins:=true;

          if insert_ins then
          begin
          quTmp.Close;
          quTmp.SQL.Text := 'execute procedure AddIns_Pr2(:ART2ID, :INSID, :Q, :W, :COLOR,'+
                            ':CHROMATICITY, :CLEANNES, :EDGTYPE, :EDGSHAPE, :IGROUP)';
          quTmp.Prepare;
          quTmp.ParamByName('ART2ID').AsInteger := Art2Id;
          quTmp.ParamByName('INSID').AsString := Mounting.Name;
          quTmp.ParamByName('Q').AsInteger := Mounting.Q;
          quTmp.ParamByName('W').AsDouble := Mounting.W;
          if Mounting.Color='' then
             quTmp.ParamByName('COLOR').AsVariant := Null
             else quTmp.ParamByName('COLOR').AsString := Mounting.Color;
          if (Mounting.Chromaticity='') then
              quTmp.ParamByName('CHROMATICITY').AsVariant := Null
              else quTmp.ParamByName('CHROMATICITY').AsString := Mounting.Chromaticity;
          if (Mounting.Cleannes='') then
             quTmp.ParamByName('CLEANNES').AsVariant := Null
             else quTmp.ParamByName('CLEANNES').AsString := Mounting.Cleannes;
          if (Mounting.EdgT='') then
             quTmp.ParamByName('EDGTYPE').AsVariant := Null
             else quTmp.ParamByName('EDGTYPE').AsString := Mounting.EdgT;
          if (Mounting.EdgShape='') then
              quTmp.ParamByName('EDGSHAPE').AsVariant := Null
              else quTmp.ParamByName('EDGSHAPE').AsString := Mounting.EdgShape;
          if (Mounting.Igroup='') then
             quTmp.ParamByName('IGROUP').AsVariant := Null
             else quTmp.ParamByName('IGROUP').AsString := Mounting.Igroup;
          quTmp.ExecQuery;
          tr.CommitRetaining;
          end;
        end;
        quTmp.close;
        ExecSQL('execute procedure SetMainIns('+IntToStr(Art2Id)+','''+ MainIns +''')', quTmp);
        tr.CommitRetaining;
        qutmp.Close;

        prbrImp.StepIt;
        Repaint;
        Application.ProcessMessages;

        Next;
    end;
 end;
 finally
    Screen.Cursor := crDefault;
 end;
 except
   Log.SaveToFile('C:\log_alexandra.txt');
   log.Destroy;
 end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  DocNo, DocDate, SupINN: string;
  r: Variant;
  i:byte;
begin
  if not FileExists(Name) then eXit;
  SSF := ExtractFileName(Name);
  i := pos('.',SSF);
  delete(SSF,i,strlen(pchar(SSF)));
  try
    taSList.Edit;
    taSListSSF.AsString := SSF;
    if(SupINN <> '')then
    begin
      r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+ SupINN +'"', quTmp);
      if not VarIsNull(r) then taSListSUPID.AsInteger := r;
    end;
    taSList.Post;
  except
  end;
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

end.


