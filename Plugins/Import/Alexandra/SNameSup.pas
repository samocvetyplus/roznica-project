unit SNameSup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, DB, FIBDataSet, pFIBDataSet, GridsEh, DBGridEh,
  DBCtrls, StdCtrls, Mask, M207DBCtrls, FIBQuery, pFIBQuery, ExtCtrls, ComCtrls,
  MsgDialog;

type
  TfmSNameSup = class(TForm)
    pgctrl: TPageControl;
    tsUsrView: TTabSheet;
    tsFullView: TTabSheet;
    gUsrView: TDBGridEh;
    lcKat: TDBLookupComboBox;
    eSNameSup: TM207DBEdit;
    lcSName: TDBLookupComboBox;
    dsSName: TDataSource;
    taSName: TpFIBDataSet;
    quTmp: TpFIBQuery;
    bCompare2: TButton;
    taSNameSup: TpFIBDataSet;
    taSNameSupSNAME_SUP: TFIBStringField;
    taSNameSupSNAME: TFIBStringField;
    taSNameSupNAME_KAT: TFIBStringField;
    taSNameSupFTABLE: TFIBSmallIntField;
    dsSNameSup: TDataSource;
    gFullView: TDBGridEh;
    dsKat: TDataSource;
    taKat: TpFIBDataSet;
    taKatNAME_KAT: TFIBStringField;
    taKatKAT: TFIBSmallIntField;
    bClose1: TButton;
    bCompare1: TButton;
    bClose2: TButton;
    Label1: TLabel;
    eName: TEdit;
    dsSNameSam: TDataSource;
    taSNameSam: TpFIBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure lcKatCloseUp(Sender: TObject);
    procedure bCompare2Click(Sender: TObject);
    procedure pgctrlChange(Sender: TObject);
    procedure bCompare1Click(Sender: TObject);
    procedure bClose1Click(Sender: TObject);
    procedure bClose2Click(Sender: TObject);
    procedure eNameChange(Sender: TObject);
    procedure gUsrViewCellMouseClick(Grid: TCustomGridEh; Cell: TGridCoord;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    function Stop: boolean;
    procedure bCompare1KeyPress(Sender: TObject; var Key: Char);
  private
    procedure OpenCurrDS;
  public
    { Public declarations }
  end;

var
  fmSNameSup: TfmSNameSup;
  StopFlag: Boolean;

implementation

{$R *.dfm}
uses SInvImp, M207Proc;

{**** �� ��������� ����������� ������� ����� ��� ���������� ������ �������� ***}
procedure TfmSNameSup.FormCreate(Sender: TObject);
begin
  pgctrl.ActivePage := tsUsrView;
  OpenCurrDS;
end;
{************* ������ � DataSourse � ����������� �� ��������� ������� *********}
procedure TfmSNameSup.OpenCurrDS;
begin
  case pgctrl.ActivePage.Tag of
    1 : begin {������� ��������. ����������� ����������� ��� ������ ��������}
        taSName.Active:=False;
        taSName.Close;
        case Kategory of
          0: taSName.SelectSQL.Text := 'select d_goodid, name from d_good order by name'; // ������������ �������
          1: taSName.SelectSQL.Text := 'select d_insid, name from d_ins order by name';   // �������
          2: taSName.SelectSQL.Text := 'select d_matid, name from d_mat order by name';   // ��������
          3: taSName.SelectSQL.Text := 'select d_countryid, name from d_country order by name';// ������
        end;
        taSName.Active:=True;
        taSName.Open;
        gUsrView.Columns[0].Title.Caption:='������� ����.';
        gUsrView.Columns[1].Title.Caption:='������������';
        taSName.FetchAll;
        end;
    2 : begin {����������� ��������. ����������� ������ �������� ��� �������������}
        takat.open;
        taKat.FetchAll;
        lcKat.KeyValue:= Kategory;
        eSNameSup.Text:=NameSup;
        taSNameSam.Active:=False;
        taSNameSam.Close;
        case Kategory of
          0: begin
             taSNameSam.SelectSQL.Text := 'select d_goodid from d_good'; // ������������ �������
             taSNameSam.Active:=True;
             taSNameSam.Open;
             lcSName.KeyField:='D_GOODID';
             lcSName.ListField:='D_GOODID';
             end;
          1: begin
             taSNameSam.SelectSQL.Text := 'select d_insid from d_ins';           // �������
             taSNameSam.Active:=True;
             taSNameSam.Open;
             lcSName.KeyField:='D_INSID';
             lcSName.ListField:='D_INSID';
             end;
          2: begin
             taSNameSam.SelectSQL.Text := 'select d_matid from d_mat';           // ��������
             taSNameSam.Active:=True;
             taSNameSam.Open;
             lcSName.KeyField:='D_MATID';
             lcSName.ListField:='D_MATID';
             end;
          3: begin
             taSNameSam.SelectSQL.Text := 'select d_countryid from d_country';   // ������
             taSNameSam.Active:=True;
             taSNameSam.Open;
             lcSName.KeyField:='D_COUNTRYID';
             lcSName.ListField:='D_COUNTRYID';
             end;
         end;
         taSNameSam.FetchAll;
         taSNameSup.Active:=False;
         taSNameSup.Close;
         taSNameSup.SelectSQL.Text:='select s.sname_sup, s.sname, s.ftable, sk.name_kat' +
                                    ' from D_SNameSup s, D_SNameSup_Kat sk '+
                                    ' where s.ftable = sk.kat and s.fsup = 2 and sk.kat= '+
                                    IntToStr(lcKat.KeyValue);
         taSNameSup.Active:=True;
         taSNameSup.Open;
         end;
  end;
end;

procedure TfmSNameSup.pgctrlChange(Sender: TObject);
begin
   OpenCurrDS;
end;
{******* ���������� ������ ������������� ������������ ���������� � ������ *****}
{******* ������� "������� ��������" *******************************************}
procedure TfmSNameSup.bCompare1Click(Sender: TObject);
var
  sname: string;
begin
 try
  sname:= gUsrView.DataSource.DataSet.FieldByName(gUsrView.Columns[0].FieldName).AsString;
  quTmp.Close;
  quTmp.SQL.Text:='INSERT INTO D_SNameSup (D_SNAMESUPID, SNAME_SUP, SNAME, FSUP, FTABLE, A_PATTERN)'+
                  ' VALUES (gen_id(gen_snamesupid,1),''' + NameSup + ''','''+ sname + ''',' +
                   IntToStr(fsup) + ',' + IntToStr(kategory) + ', null)';
  quTmp.ExecQuery;
 finally
  bClose1.Click;
 end;
end;

procedure TfmSNameSup.bCompare1KeyPress(Sender: TObject; var Key: Char);
begin

end;

{******* ���������� ������ ������������� ������������ ���������� � ������ *****}
{******* ������� "����������� ��������" *******************************************}
procedure TfmSNameSup.bCompare2Click(Sender: TObject);
begin
  if lcSName.Text <> '' then   // ���� ������� ������� ������������ ��� �������������
  begin
    quTmp.Close;
    quTmp.SQL.Text:='INSERT INTO D_SNameSup (D_SNAMESUPID, SNAME_SUP, SNAME, FSUP, FTABLE, A_PATTERN)'+
    ' VALUES (gen_id(gen_snamesupid,1),''' + NameSup + ''','''+ lcSName.text + ''',' + IntToStr(fsup) + ',' + IntToStr(kategory) + ', null)';
    quTmp.ExecQuery;
    taSNameSup.close;
    taSNameSup.open;
  end
  else MessageDialog('�� ������� ������� ������������ ��� �������������.', mtError, [mbOk], 0);
end;
{***** �������� ����� � ������� "������� ��������" ****************************}
procedure TfmSNameSup.bClose1Click(Sender: TObject);
begin
  taSName.Close;
  taSNameSup.Close;
  taKat.Close;
  quTmp.Close;
  close;
end;
{***** �������� ����� � ������� "����������� ��������" ****************************}
procedure TfmSNameSup.bClose2Click(Sender: TObject);
begin
  taSNameSam.Close;
  taSNameSup.Close;
  taKat.Close;
  quTmp.Close;
  close;
end;
{* ���������� ���-� � ����� � ������������ � ��������� ���������� ������������*}
procedure TfmSNameSup.lcKatCloseUp(Sender: TObject);
begin
  taSNameSup.Active:=False;
  taSNameSup.Close;
  taSNameSup.SelectSQL.Text:='select s.sname_sup, s.sname, s.ftable, sk.name_kat' +
      ' from D_SNameSup s, D_SNameSup_Kat sk '+
      ' where s.ftable = sk.kat and s.fsup = 2 and sk.kat= '+ IntToStr(lcKat.KeyValue);
  taSNameSup.Active:=True;
  taSNameSup.Open;
  taSNameSup.FetchAll;
end;
{* ������������ ����� ������������ ��� ������������� �� ������� "����. �����" *}
procedure TfmSNameSup.eNameChange(Sender: TObject);
begin
 with taSName do begin
   if taSName.Active then
      LocateF(taSName, 'NAME', eName.Text, [loBeginingPart], False, Stop);
 end;
end;
function TfmSNameSup.Stop: boolean;
begin
  Result:=StopFlag;
end;
{********** �������� ����� ����� ������ � ��������� ����� ������ **************}
procedure TfmSNameSup.gUsrViewCellMouseClick(Grid: TCustomGridEh;
  Cell: TGridCoord; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  eName.Text:=taSName.FieldByName('NAME').AsString;
end;

end.
