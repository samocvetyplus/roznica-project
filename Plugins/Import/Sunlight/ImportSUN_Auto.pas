unit ImportSUN_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportSUN_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '7726664484';
{const
  INN : PChar = '3123026420'; }

type
  TSUN = class(TAutoObject, ISUN)
   FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TSUN.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport('10.1.0.252:e:\JEW.FDB', 1);
  Application.Handle := 0;
end;

procedure  TSUN.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TSUN, Class_SUN, ciMultiInstance, tmApartment);
end.
