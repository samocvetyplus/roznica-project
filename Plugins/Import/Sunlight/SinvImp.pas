unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ToolEdit,} DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, rxToolEdit,
  ADODB, DBF;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    dsDBFimp: TDataSource;
    DBFconnect: TADOConnection;
    taDBFimp: TADOTable;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

type
  // ��� �������
  TMounting = record
    Name : string;
    Q : integer;
    W : double;
    EdgShape : string; // ����� �������
    EdgT : string; // ��� �������
    Cleannes : string; // �������
    Chromaticity : string; // ���������
    Igroup : string; // ������
    Color : string; // ����
  end;

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������
  SSF: string;

implementation


uses dbUtil, {RegExpr,} Variants, RxStrUtils, Math, MsgDialog, ImportSUN_Auto,
     fmUtils, UtilLib;

resourcestring
  rcOrganizationNotFound = '������������� SUNLIGHT �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';
  rsSelectFile='�� ������ ���� �������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:VAriant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    ShowMessage(DbName);
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ����� �������� � ��, ��� ������ ����������� ���
   // CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(ImportSun_Auto.INN)+'"', fmSInvImp.quTmp);
    //if VarIsNull(CompId) then
    //begin
     // MessageDialog(Format(rcOrganizationNotFound, [ImportSun_Auto.INN]), mtError, [mbOk], 0);
      //eXit;
    //end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST(''NOW'' AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger :=SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
ShowMessage('������ �������');
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  if ListBox1.Items.Text='' then ListBox1.Items.Text:='��������� ��������������� �������!';
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.FormCreate(Sender: TObject);
begin
//ShowMessage('Create');
end;

procedure TfmSInvImp.Import;
var
  ftext:textfile;
  UnitId,EndMarker, i,k : integer;
  Art2Id: integer;
  Tmp, Tmp2, Art, Art2, Good, Mat, Sz ,Un,
  MainIns,MIns, tmpIns, Ins1, UID_Sup : string;
  Price, W : double;
  Log: TStringlist;
  procedure AddLog(Msg : string);
  begin
     ListBox1.Items.Add(Msg);
     ListBox1.Refresh;
  end;

  function FindId (supname:string; fsup, ftable:integer):string;
  var
    r :Variant;
  begin
    r := ExecSelectSQL('select first 1 SNAME from D_SNAMESUP where SNAME_SUP like '#39'%'+supname+'%'#39+
                   ' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable)+' order by SNAME', quTmp);
  // if UID_Sup=495337 then
   //ShowMessage('select first 1 SNAME from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
    //' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable));
    if r=null then Result:='-1' else Result:= VarToStr(r);
  end;



var
  Mounting : TMounting;
  p,pi:integer;
  ogr:string;
begin
 // �������� ���
  ListBox1.Clear; //**************
  Log:=TStringList.Create;
  // ��������� ���������� �� ���� � �������
  if not FileExists(fledImp.FileName) then
  begin
   ShowMessage(fledImp.FileName);
    ListBox1.Items.Add('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
 try
 ShowMessage(fledImp.FileName);
 //����������� � ����� dbf
  taDBFimp.Active:=false;
  DBFconnect.Connected:=false;
 DBFconnect.ConnectionString:='Data Source="'
  +trim(ExtractFilePath(fledImp.FileName)) + '";Extended Properties="DBASE IV;";"';
  DBFconnect.LoginPrompt:=false;
  DBFconnect.Mode:=cmReadWrite;
  DBFconnect.Connected:=true;
  taDBFimp.TableName:=SSF;
  taDBFimp.Active:=true;
  //����������� �����������
  //��������� �������
    prbrImp.Min := 0;
    prbrImp.Max := taDBFimp.RecordCount;
    ShowMessage('���������� ������� - '+IntToStr(taDBFimp.RecordCount));
    prbrImp.Step := 1;
    DecimalSeparator:='.';
//-----------------
  // �������� ���� �� ��������
 try
   with taDBFimp do
   begin
      first;
    while not taDBFimp.eof do
   begin
     Uid_Sup:= trim(taDBFimp.FieldByName('TOVAR').AsString);
    //ShowMessage(Uid_Sup);
    Log.Add('Uid_Sup '+Uid_Sup);
    Art :=trim(taDBFimp.FieldByName('UNIARTIKUL').AsString);
    Log.Add('Art '+Art);
    Art2 := trim(taDBFimp.FieldByName('ARTIKUL').AsString);
    Log.Add('Art2 '+Art2);
    Sz := trim(taDBFimp.FieldByName('RAZMER').AsString);
    Log.Add('Sz '+Sz);
    W := taDBFimp.FieldByName('OBVES').AsFloat;
    un:= ansilowercase(trim(taDBFimp.FieldByName('ED').AsString));
    UnitId := integer(pos('��',Un)>0);
    Price:=taDBFimp.FieldByName('CENA').AsFloat;
    // �������� �������� �������
    MainIns:=taDBFimp.FieldByName('KAM_1').AsString;
    Log.Add('KAM_1 = '+MainIns);
    tmpIns:= trim(FindId(MainIns, 5, 1)); //!!!
      if(tmpIns = '-1')then
      begin
        AddLog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup);
        prbrImp.StepIt;
        //continue;
      end
      else MainIns := tmpIns;
      // �������� �����
      Good:= AnsiLowerCase(taDBFimp.FieldByName('TIP').AsString);
      Log.Add('Good '+Good);
      Ins1:= FindId(Good,5, 0);

      Log.Add('Ins1 '+Ins1);
      if(Ins1 = '-1')then
      begin
        AddLog('�� ������� ���������� ����� '+Good+' � '+UID_Sup);
        prbrImp.StepIt;
      end
      else Good:=Ins1;

     // �������� ��������
      Mat := trim(taDBFimp.FieldByName('PROBA').AsString);//AnsiLowerCase(ExtractWord(9, Tmp, [' ']));
      MIns := Trim(MainIns);
      Ins1:= FindId(Mat,5, 2);
      Log.Add('Mat '+Mat);
      Log.Add('Ins1 '+Ins1);
      if Ins1='-1' then
      begin
        AddLog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup);
        prbrImp.StepIt;
        //continue;
      end
      else Mat:=Ins1;
      // ������� ������� � ���������
      quTmp.Close;
      quTmp.SQL.Text := 'execute procedure InsFItem(:ART, :ART2, :GOOD, :MAT, :COUNTRY,'+
        ':UNITID, :COMPID, :SUPID, :MAININS, :W, :SZ, :PRICE, :SINVID, :DEPID, :PNDS, :NDSID,'+
        ':MARGIN, :SSF, :SDATE, :SN, :NDATE, :UID_SUP)';
      quTmp.Prepare;
      quTmp.ParamByName('ART').AsString := Art;
      Log.Add('quTmp.ParamByName(''ART'') '+quTmp.ParamByName('ART').AsString);
      quTmp.ParamByName('ART2').AsString := Art2;
      Log.Add('quTmp.ParamByName(''ART2'') '+quTmp.ParamByName('ART2').AsString);
      quTmp.ParamByName('GOOD').AsString := Good;
      Log.Add('quTmp.ParamByName(''GOOD'') '+quTmp.ParamByName('GOOD').AsString);
      quTmp.ParamByName('MAT').AsString := Mat;
      Log.Add('quTmp.ParamByName(''MAT'') '+quTmp.ParamByName('MAT').AsString);
      quTmp.ParamByName('COUNTRY').AsString := '�';  { TODO : ���������� � ���� ��� ����� ������ }
      Log.Add('quTmp.ParamByName(''COUNTRY'') '+quTmp.ParamByName('COUNTRY').AsString);
      quTmp.ParamByName('UNITID').AsInteger := integer(UnitId);
      log.Add('quTmp.ParamByName(''UNITID'') '+quTmp.ParamByName('UNITID').AsString);
      quTmp.ParamByName('COMPID').AsInteger := 2000020127; // ���
      log.Add('quTmp.ParamByName(''COMPID'') '+quTmp.ParamByName('COMPID').AsString);
      quTmp.ParamByName('SUPID').AsInteger := taSListSUPID.AsInteger;
      log.Add('quTmp.ParamByName(''SUPID'') '+quTmp.ParamByName('SUPID').AsString);
      quTmp.ParamByName('MAININS').AsString := MainIns;
      log.Add('quTmp.ParamByName(''MAININS'') '+quTmp.ParamByName('MAININS').AsString);
      quTmp.ParamByName('W').AsDouble := W;
      log.Add('quTmp.ParamByName(''W'') '+quTmp.ParamByName('W').AsString);
      quTmp.ParamByName('SZ').AsString := Sz;
      log.Add('quTmp.ParamByName(''Sz'') '+quTmp.ParamByName('Sz').AsString);
      quTmp.ParamByName('PRICE').AsDouble := Price;
      log.Add('quTmp.ParamByName(''PRICE'') '+quTmp.ParamByName('PRICE').AsString);
      quTmp.ParamByName('SINVID').AsInteger := taSListSINVID.AsInteger;
      log.Add('quTmp.ParamByName(''SINVID'') '+quTmp.ParamByName('SINVID').AsString);
      quTmp.ParamByName('DEPID').AsInteger := taSListDepId.AsInteger;
      log.Add('quTmp.ParamByName(''DEPID'') '+quTmp.ParamByName('DEPID').AsString);
      quTmp.ParamByName('PNDS').AsInteger := 0;
      log.Add('quTmp.ParamByName(''PNDS'') '+quTmp.ParamByName('PNDS').AsString);
      quTmp.ParamByName('NDSID').AsInteger := taSListNDSID.AsInteger;
      log.Add('quTmp.ParamByName(''NDSID'') '+quTmp.ParamByName('NDSID').AsString);
      quTmp.ParamByName('MARGIN').AsDouble := FMargin;
      log.Add('quTmp.ParamByName(''MARGIN'') '+quTmp.ParamByName('MARGIN').AsString);
      quTmp.ParamByName('SSF').AsString := taSListSSF.AsString;
      log.Add('quTmp.ParamByName(''SSF'') '+quTmp.ParamByName('SSF').AsString);
      quTmp.ParamByName('SDATE').AsDateTime := taSListSDATE.AsDateTime;
      log.Add('quTmp.ParamByName(''SDATE'') '+quTmp.ParamByName('SDATE').AsString);
      quTmp.ParamByName('SN').AsInteger := taSListSN.AsInteger;
      log.Add('quTmp.ParamByName(''SN'') '+quTmp.ParamByName('SN').AsString);
      quTmp.ParamByName('NDATE').AsDateTime := taSListNDATE.AsDateTime;
      log.Add('quTmp.ParamByName(''NDATE'') '+quTmp.ParamByName('NDATE').AsString);
      //quTmp.ParamByName('UID_SUP').AsInteger := UID_Sup;

      try
      log.Add(quTmp.SQL.Text);
        quTmp.ExecQuery;
        log.Add('quTmp.ExecQuery;');
        tr.CommitRetaining;
        log.Add('tr.CommitRetaining;');
        Art2Id := quTmp.Fields[0].AsInteger;
        log.Add('Art2Id '+inttostr(Art2id));
        quTmp.Close;
        log.Add('tr.CommitRetaining;');
      except
        on E:Exception do
          begin
            AddLog('������ ��� ���������� ������� � ���.'+Art+' ('+IntToStr(i)+')'+' - '+e.Message);
            log.Add('������'+e.Message);
            prbrImp.StepIt;
             log.Add('prbrImp.StepIt;');
          end
      end;
      // ����� ���������� � ���������
      // ������ �������� �������
      k:=(taDBFimp.FieldCount-19) div 6;
      Log.Add('k '+inttostr(k));
      i:=1;
      repeat
      begin
       if(i=1)then
        begin
          quTmp.Close;
          quTmp.SQL.Text := 'delete from Ins where Art2Id='+IntToStr(Art2Id);
           Log.Add('if(i=1)then '+ quTmp.SQL.Text);
          quTmp.Prepare;
          quTmp.ExecQuery;
          quTmp.Transaction.CommitRetaining;
        end;
        Mounting.EdgShape:='';
        Mounting.EdgT:='';
        Mounting.Name := trim(taDBFimp.FieldByName('KAM_'+trim(IntTostr(i))).AsString);

        log.Add('Mounting.Name '+Mounting.Name);
        Mounting.Q := taDBFimp.FieldByName('KOL_'+trim(IntTostr(i))).AsInteger;
         log.Add('Mounting.Q '+inttostr(Mounting.Q));
        Mounting.W := taDBFimp.FieldByName('VES_'+trim(IntTostr(i))).AsFloat;
         log.Add('Mounting.W '+floattostr(Mounting.W));
        //===============================================//
        //����� ����� ������������� ���� � ������� �������
        ogr:= AnsiLowerCase(trim(taDBFimp.FieldByName('OGR_'+inttostr(i)).AsString));
        p:= pos('-',ogr);
        if (p=0) then
        begin
        Mounting.EdgShape :=ogr;
        if Mounting.EdgShape='�����' then Mounting.EdgShape:='��.';
        if Mounting.EdgShape='�������' then Mounting.EdgShape:='��.';
        if Mounting.EdgShape='����' then Mounting.EdgShape:='��.';
        end
           else
         begin
          for pi := 1 to p-1 do
             Mounting.EdgShape:=Mounting.EdgShape+ogr[pi];
          for pi := p+1 to length(ogr) do
            Mounting.EdgT:= Mounting.EdgT+ogr[pi];
              Mounting.EdgShape:=Mounting.EdgShape+'.';
         end;
         //��������� �������� ���� � �������
         //===============================================//
        Mounting.Cleannes := trim(taDBFimp.FieldByName('CHIST_'+IntTostr(i)).AsString);
        log.Add('Mounting.Cleannes '+Mounting.Cleannes);
        Mounting.Chromaticity := trim(taDBFimp.FieldByName('TCVET_'+IntTostr(i)).AsString);
        log.Add('Mounting.Chromaticity '+Mounting.Chromaticity);
       Ins1 := FindId(Mounting.Name,5,1);//!!
       //������ ����� �������=============================//
      {  p:= 1;
        Mounting.Color:='';
            for pi := 1 to length(Mounting.Name) do
             if (Mounting.Name[i]=' ') then
             
            Mounting.Color:= Mounting.Color+Mounting.Name[pi];
        if Mounting.Color='���������' then Mounting.Color:=''; }
           
       // ================================================//
	     log.add('ins1 '+ins1);
       if(Ins1='-1')then
        begin
          AddLog('�� ������� ���������� ������������ �������. ���='+Art+', UID='+trim(UID_Sup));
          ShowMessage(Ins1);
          ShowMessage(Mounting.Name);
          inc(i);
          prbrImp.StepIt;
        end;
        quTmp.Close;
        quTmp.SQL.Text := 'execute procedure AddIns_Pr2(:ART2ID, :INSID, :Q, :W, :COLOR,'+
              ':CHROMATICITY, :CLEANNES, :EDGTYPE, :EDGSHAPE, :IGROUP)';
              log.Add(quTmp.SQL.Text);
         log.Add('quTmp.Prepare begin');
        quTmp.Prepare;
         log.Add('quTmp.Prepare end');
        quTmp.ParamByName('ART2ID').AsInteger := Art2Id;
        log.add('quTmp.ParamByName(''ART2ID'') '+intTostr(Art2Id));
        quTmp.ParamByName('INSID').AsString := Ins1;
        log.add('quTmp.ParamByName(''INSID'') '+Ins1);
        quTmp.ParamByName('Q').AsInteger := Mounting.Q;
        log.add('quTmp.ParamByName(''Q'') '+intTostr(Mounting.Q));
        quTmp.ParamByName('W').AsDouble := Mounting.W;
         log.add('quTmp.ParamByName(''W'') '+floatTostr(Mounting.W));
          if Mounting.Color='' then quTmp.ParamByName('COLOR').AsVariant := Null
        else quTmp.ParamByName('COLOR').AsString := Mounting.Color;
         if (Mounting.Chromaticity='-') then quTmp.ParamByName('CHROMATICITY').AsVariant := Null
        else quTmp.ParamByName('CHROMATICITY').AsString := Mounting.Chromaticity;
          log.add('quTmp.ParamByName(''CHROMATICITY'') '+Mounting.Chromaticity);
        if (Mounting.Cleannes='-') then quTmp.ParamByName('CLEANNES').AsVariant := Null
                else quTmp.ParamByName('CLEANNES').AsString := Mounting.Cleannes;
       log.add('quTmp.ParamByName(''CLEANNES'') '+Mounting.Cleannes);
        if (Mounting.EdgT='-') then quTmp.ParamByName('EDGTYPE').AsVariant := Null
        else quTmp.ParamByName('EDGTYPE').AsString := Mounting.EdgT;
        log.add('quTmp.ParamByName(''EDGTYPE'') '+Mounting.EdgT);
        if (Mounting.EdgShape='-') then quTmp.ParamByName('EDGSHAPE').AsVariant := Null
        else quTmp.ParamByName('EDGSHAPE').AsString := Mounting.EdgShape;

          log.add('quTmp.ParamByName(''EDGSHAPE'') '+Mounting.EDGSHAPE);
         log.add('quTmp.ExeQuery '+quTmp.SQL.Text);
        quTmp.ExecQuery;
        tr.CommitRetaining;
           inc(i);
      if(i<>1)then ExecSQL('execute procedure SetMainIns('+IntToStr(Art2Id)+',"�")', quTmp);
      prbrImp.StepIt;
      end;
      until ((trim(taDBFimp.FieldByName('KAM_'+IntTostr(i)).AsString))='') or (i=k);
      //��������� �������
    Next;
   end;
   end;
 finally
    //CloseFile(fText);
    Screen.Cursor := crDefault;
  end;
 except
   Log.SaveToFile('C:\log_sun.txt');
   log.Destroy;
 end;
end;


procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  //ftext: textfile;
  DocNo, DocDate, SupINN: string;
  r: Variant;
  i:byte;
begin
  if not FileExists(Name) then eXit;
  ShowMessage(ExtractFileName(Name));
  SSF := ExtractFileName(Name);
 i := pos('.',SSF);
 delete(SSF,i,strlen(pchar(SSF)));
    try
      taSList.Edit;
      taSListSSF.AsString := SSF;
      //taSListNDATE.AsDateTime := StrToDateTime();
      if(SupINN <> '')then
      begin
        r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+SupINN+'"', quTmp);
        if not VarIsNull(r) then taSListSUPID.AsInteger := r;
      end;
      taSList.Post;
    except
    end;
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

end.




























