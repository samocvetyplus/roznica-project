unit ImportSUN_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 27.11.2012 13:47:01 from Type Library described below.

// ************************************************************************  //
// Type Lib: E:\��� ���������\��� �������\Work\�������\Plugins\Import\Sunlight\ImportSUN.tlb (1)
// LIBID: {1029BE89-8DCF-4A0D-A15E-B17A938E94E3}
// LCID: 0
// Helpfile: 
// HelpString: ImportSUN Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportSUNMajorVersion = 1;
  ImportSUNMinorVersion = 0;

  LIBID_ImportSUN: TGUID = '{1029BE89-8DCF-4A0D-A15E-B17A938E94E3}';

  IID_ISUN: TGUID = '{C3E67FB4-4AB5-4B70-A274-68D8745BCBE0}';
  CLASS_SUN: TGUID = '{861102B7-5196-4E80-99EB-77F02D56124E}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISUN = interface;
  ISUNDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SUN = ISUN;


// *********************************************************************//
// Interface: ISUN
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C3E67FB4-4AB5-4B70-A274-68D8745BCBE0}
// *********************************************************************//
  ISUN = interface(IDispatch)
    ['{C3E67FB4-4AB5-4B70-A274-68D8745BCBE0}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  ISUNDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {C3E67FB4-4AB5-4B70-A274-68D8745BCBE0}
// *********************************************************************//
  ISUNDisp = dispinterface
    ['{C3E67FB4-4AB5-4B70-A274-68D8745BCBE0}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoSUN provides a Create and CreateRemote method to          
// create instances of the default interface ISUN exposed by              
// the CoClass SUN. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSUN = class
    class function Create: ISUN;
    class function CreateRemote(const MachineName: string): ISUN;
  end;

implementation

uses ComObj;

class function CoSUN.Create: ISUN;
begin
  Result := CreateComObject(CLASS_SUN) as ISUN;
end;

class function CoSUN.CreateRemote(const MachineName: string): ISUN;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SUN) as ISUN;
end;

end.
