unit ImportGulliver_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportGulliver_TLB, Contnrs;

const
  INN : PChar = '7733126800';

type
  TJewImportGulliver = class(TAutoObject, IGulliver)
    FAppHandle: integer;
  private
    // from IImportGulliver
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportGulliver}

uses ComServ, SInvImp, Forms;

procedure TJewImportGulliver.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportGulliver.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportGulliver,CLASS_Gulliver, ciMultiInstance, tmApartment);

end.
