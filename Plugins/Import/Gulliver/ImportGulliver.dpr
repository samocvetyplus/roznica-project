library ImportGulliver;

uses
  ExceptionLog,
  ComServ,
  JewImport_TLB in 'JewImport_TLB.pas',
  SInvImp in 'SInvImp.pas' {fmSInvImp},
  ImportGulliver_Auto in 'ImportGulliver_Auto.pas',
  ImportGulliver_TLB in 'ImportGulliver_TLB.pas';

const
  ClassName : PChar = 'JewImport.Gulliver';
  Version : PChar = '1.0';
  PluginName : PChar = '������ ��������� ��������';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ��������. ��� 7733126800';

{$R *.TLB}

{$R *.RES}
function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.

