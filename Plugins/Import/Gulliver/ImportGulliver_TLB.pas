unit ImportGulliver_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 1.3 $
// File generated on 31.05.2005 13:38:17 from Type Library described below.

// ************************************************************************  //
// Type Lib: E:\Projects\Jew\Plugins\Import\Gulliver\ImportGulliver.tlb (1)
// LIBID: {E86CC537-26AD-4592-B637-D8791C15FA85}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportGulliverMajorVersion = 1;
  ImportGulliverMinorVersion = 0;

  LIBID_ImportGulliver: TGUID = '{E86CC537-26AD-4592-B637-D8791C15FA85}';

  IID_IGulliver: TGUID = '{3A109D62-8703-4B6D-BE07-9F92667AC25C}';
  CLASS_Gulliver: TGUID = '{2E3B337E-3C03-4A9E-9DD7-8E5AC3DA48B8}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IGulliver = interface;
  IGulliverDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Gulliver = IGulliver;


// *********************************************************************//
// Interface: IGulliver
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3A109D62-8703-4B6D-BE07-9F92667AC25C}
// *********************************************************************//
  IGulliver = interface(IDispatch)
    ['{3A109D62-8703-4B6D-BE07-9F92667AC25C}']
    procedure Import(const NameDb: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IGulliverDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3A109D62-8703-4B6D-BE07-9F92667AC25C}
// *********************************************************************//
  IGulliverDisp = dispinterface
    ['{3A109D62-8703-4B6D-BE07-9F92667AC25C}']
    procedure Import(const NameDb: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoGulliver provides a Create and CreateRemote method to          
// create instances of the default interface IGulliver exposed by              
// the CoClass Gulliver. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoGulliver = class
    class function Create: IGulliver;
    class function CreateRemote(const MachineName: string): IGulliver;
  end;

implementation

uses ComObj;

class function CoGulliver.Create: IGulliver;
begin
  Result := CreateComObject(CLASS_Gulliver) as IGulliver;
end;

class function CoGulliver.CreateRemote(const MachineName: string): IGulliver;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Gulliver) as IGulliver;
end;

end.
