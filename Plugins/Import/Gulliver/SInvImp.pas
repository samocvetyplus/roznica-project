unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolEdit, DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, Dbf,
  OleServer, Excel2000, IdGlobal;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    mmLog: TMemo;
    xl: TExcelApplication;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure lcSupChange(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������

implementation

uses dbUtil, RegExpr, Variants, RxStrUtils, Math, MsgDialog, ImportGulliver_Auto,
     fmUtils, UtilLib;

resourcestring
  rcOrganizationNotFound = '������������� �������� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:Variant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ��������� � ��, ��� ������ ����������� ���
    CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST("NOW" AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger := SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;
procedure TfmSInvImp.Import;
var Path, tmp,good_sup, good, art, art2, tmp1: string;
    ins, ins1, mat , tmpnextpage, tmplast, sz : string;
    i,j, n, unitid: integer;
    w, price: real;
    IsEnd, flIns: boolean;
    DefaultSeparator: char;
  procedure AddLog(Msg : string);
  begin
    mmLog.Lines.Add(Msg);
  end;
  function find_val (inputval:string; ftable:string):string;
  var res: Variant;
  begin
    res := ExecSelectSQL('select first 1 sname, D_Snamesupid from d_snamesup where fsup=4 and '+
                         ' ftable='+ftable+' and sname_sup like'#39+inputval+'%'#39+
                         ' order by sname_sup desc', quTmp);
    if VarIsNull(res[0]) then
      result:='-1'
    else
      Result:=VarToStr(res[0]);
  end;
  procedure sort_mass;
  var jval, kval, newins1:string;
      j,k,l:integer;
  begin
    j:=1;
    newins1:='';
    while trim(ExtractWord(j, ins1, [';']))<>'' do
    begin
      k:=j+1;
      while trim(ExtractWord(k, ins1, [';']))<>'' do
      begin
        jval:=ExtractWord(j, ins1, [';']);
        kval:=ExtractWord(k, ins1, [';']);
        if kval<jval then
        begin
          l:=1;
          newins1:='';
          while ExtractWord(l, ins1, [';'])<>'' do
          begin
            if ExtractWord(l, ins1, [';'])=jval then newins1:=newins1+kval+';'
            else if ExtractWord(l, ins1, [';'])=kval then newins1:=newins1+jval+';'
            else newins1:=newins1+ExtractWord(l, ins1, [';'])+';';
            inc(l);
          end;
          ins1:=newins1;
        end;
        inc(k);
      end;
      inc(j);
    end;
//  if newins1<>'' then  ins1:=newins1  else ins1:=trim(ins1)+';';
    l:=1;
    newins1:='';
    while ExtractWord(l, ins1, [';'])<>'' do
    begin
      newins1:=newins1+trim(ExtractWord(l, ins1, [';']))+';';
      inc(l);
    end;
    ins1:=newins1;
  end;

begin
  // �������� ���
  mmLog.Lines.Clear;
  Path := fledImp.FileName;
  DefaultSeparator := DecimalSeparator;
  DecimalSeparator := '.';

  if not FileExists(Path) then
  begin
    AddLog('���� '+Path+' �� ����������!');
    eXit;
  end;

  try
    xl.ConnectKind := ckRunningOrNew;
    xl.Connect;
    xl.Workbooks.Add(Path, LOCALE_USER_DEFAULT);
    prbrImp.Min := 0;
    prbrImp.Max := ( xl.ActiveWorkbook.WorkSheets[1] as _Worksheet).Columns.Count - 38;
    prbrImp.Step := 1;
  except
    AddLog('�� �������������� ���������� Excel ��� ���� ��� �����');
    eXit;
  end;
  try
    with (xl.ActiveWorkbook.WorkSheets[1] as _Worksheet) do
    begin
      i:=19; j:=1;
      IsEnd:=false;
      while not IsEnd do
      begin
        Tmp := Range['C'+IntToStr(i), 'C'+IntToStr(i)].Value;
        if Trim(Tmp) = '' then break;
        {����� ������������ �������}
        good_sup:=ExtractWord(1,tmp,[' ']);
        if AnsiStrPos(PChar(tmp),PChar('��.������'))<> nil then good_sup:='��.������';
        good:=find_val(good_sup,'0');
        if good='-1' then
        begin
          AddLog('�� ������� ���������� ����. ������� '+good_sup+' {'+inttostr(j)+'}');
          continue;
        end;
      {����� �������� 1}
        art:=ExtractWord(2,tmp,[' ']);
        if good_sup='��.������' then System.Delete(art, 6, length(art)-5);
        art2:='';
        if art[1]<>'�' then
        begin
          art2:=ExtractWord(2,art,['-']);
          art:=ExtractWord(1,art,['-']);
        end
        else
          art:=ExtractWord(2,art,['-']);
        if art = '' then art := ExtractWord(2,tmp,[' ']);
        if art2='' then art2:='-';

        tmp1:=ExtractWord(3,tmp,[' ']);
        if length(tmp1)>=2 then
        begin
          n:=4;
          if (tmp1[1]='�') and (tmp1[2]='.') then
          begin
            System.delete(tmp1, 1, 2);
            sz:=floattostr(strtofloat(tmp1));
            tmp1:=ExtractWord(4,tmp,[' ']); inc(n)
          end
          else
            sz := '-';
          if tmp1<>'' then
            if IsNumeric(tmp1[1]) then
            begin
              tmp1:=ExtractWord(n,tmp,[' ']);
              flIns:=false;
              if tmp1[length(tmp1)]=',' then
              begin
                System.delete(tmp1, length(tmp1), 1);
                flIns:=true
              end;
              ins:=find_val(tmp1,'1');
              if ins='-1' then
              begin
                AddLog('�� ������� ���������� ������� '+tmp1+' {'+inttostr(j)+'}');
                continue;
              end;
              ins1:='';
              while (flIns)  do
              begin
                inc(n);
                tmp1:=ExtractWord(n,tmp,[' ']);
                if IsNumeric(tmp1) then continue;
                flIns:=false;
                if tmp1[length(tmp1)]=',' then
                begin
                  System.delete(tmp1, length(tmp1), 1);
                  flIns:=true
                end;
                if trim(tmp1)<>'' then  ins1:=ins1+find_val(tmp1,'1')+';';
              end;
              if ins1<>'' then sort_mass;
            end
            else
              ins:='-'
          else
            ins:='-'
        end
        else
        begin
          sz:='-';
          if tmp1<>'' then
            if IsNumeric(tmp1[1]) then
            begin
              tmp1:=ExtractWord(4,tmp,[' ']);
              flIns:=false;
              if tmp1[length(tmp1)]=',' then
              begin
                System.delete(tmp1, length(tmp1), 1);
                flIns:=true
              end;
              ins:=find_val(tmp1,'1');
              if ins='-1' then
              begin
                AddLog('�� ������� ���������� ������� '+tmp1+' {'+inttostr(j)+'}');
                continue;
              end;
              n:=4;
              ins1:='';
              while flIns do
              begin
                inc(n);
                tmp1:=ExtractWord(n,tmp,[' ']);
                flIns:=false;
                if tmp1[length(tmp1)]=',' then
                begin
                  System.delete(tmp1, length(tmp1), 1);
                  flIns:=true
                end;
                if trim(tmp1)<>'' then
                  ins1:=ins1+find_val(tmp1,'1')+';';
              end;
              if ins1<>'' then
                sort_mass;
            end
            else
              ins:='-'
          else
            ins:='-'
        end;

        tmp:=Range['E'+IntToStr(i), 'E'+IntToStr(i)].Value;
        mat:=find_val(tmp,'2');
        if mat='-1' then
        begin
          addLog('�� ������� ���������� �������� '+tmp1+' {'+inttostr(j)+'}');
          continue;
        end;

        tmp:=Range['F'+IntToStr(i), 'F'+IntToStr(i)].Value;
        if tmp='��.' then unitid:=1
        else unitid:=0;

        tmp:=Range['K'+IntToStr(i), 'K'+IntToStr(i)].Value;
        w:=strtofloat(tmp);

        tmp:=Range['M'+IntToStr(i), 'M'+IntToStr(i)].Value;
        price:=strtofloat(tmp);
        tmp := 'execute procedure InsertItem__Guliver (' + IntToStr(CompID) + #39+good+#39', '#39+art+#39', '#39+
                art2+#39', '#39+sz+#39', '#39+ins+#39', '#39+mat+#39', '+inttostr(unitid)+', '+
                FloatToStr(w)+', '+FloatToStr(price)+', '+IntToStr(DocId) +', '
                +FloatToStr(Fmargin)+', '#39+ins1+#39')';
        ExecSQL('execute procedure InsertItem__Guliver (' + IntToStr(CompID)+',' + #39+good+#39', '#39+art+#39', '#39+
                art2+#39', '#39+sz+#39', '#39+ins+#39', '#39+mat+#39', '+inttostr(unitid)+', '+
                FloatToStr(w)+', '+FloatToStr(price)+', '+IntToStr(DocId) +', '
                +FloatToStr(Fmargin)+', '#39+ins1+#39')', quTmp);
        inc(i);
        inc(j);
        prbrImp.StepIt;

        tmpnextpage:=Range['G'+IntToStr(i), 'G'+IntToStr(i)].Value;
        if tmpnextpage<>'' then
        if ExtractWord(1,tmpnextpage,[' '])='�����' then
        begin
          inc(i);
          prbrImp.StepIt;
          tmplast:=Range['I'+IntToStr(i), 'I'+IntToStr(i)].Value;
          if tmplast='' then
            i:=i+4
          else
          if ExtractWord(1,tmplast,[' '])='�����' then
            IsEnd:=true;
        end;
      end;
    end;
  finally
    prbrImp.Position := prbrImp.Max;
    Screen.Cursor := crDefault;
    xl.Disconnect;
    DecimalSeparator := DefaultSeparator;
  end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
  DocNo, DocDate, SupINN, tmp : string;
  r : Variant;
begin
    try
      taSList.Edit;
      taSListSSF.AsString := ExtractWord(1,ExtractFileName(Name), ['.']);
      taSList.Post;
    except
    end;

  xl.ConnectKind := ckRunningOrNew;
  xl.Connect;
  xl.Workbooks.Add(Name, LOCALE_USER_DEFAULT);
  with (xl.ActiveWorkbook.WorkSheets[1] as _Worksheet) do
  begin
    Tmp := Range['C'+IntToStr(3), 'C'+IntToStr(3)].Value;
    SupINN := extractWord(2, extractWord(2, Tmp, [',', '/']), [' ']);
    Tmp := Range['G'+IntToStr(13), 'G'+IntToStr(13)].Value;
    DocNo := ExtractWord(2, Tmp, ['-']);
    DocDate := Range['J'+IntToStr(13), 'J'+IntToStr(13)].Value;
  end;  
  xl.Disconnect;
  try
   taSList.Edit;
   taSListSSF.AsString := DocNo;
   taSListNDATE.AsDateTime := StrToDateTime(DocDate);
   if(SupINN <> '')then
   begin
     r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+SupINN+'"', quTmp);
     if not VarIsNull(r) then taSListSUPID.AsInteger := Integer(r);
   end;
   if taSList.State in [dsEdit, dsInsert] then
     taSList.Post;
  except
  end;
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

procedure TfmSInvImp.lcSupChange(Sender: TObject);
var r: Variant;
    SName: string;
begin
  SName := Trim(ExtractWord(1,lcSup.Text,['-']));
  if SName = '' then exit;
  taSList.Edit;
  r := ExecSelectSQL('select PAYTYPEID,NDSID from D_Comp where SName='#39 + SName + #39'', quTmp);
  if not VarIsNull(r[0]) then taSListPAYTYPEID.AsInteger := Integer(r[0]);
  if not VarIsNull(r[1]) then taSListNDSID.AsInteger := Integer(r[1]);
  taSList.Post;
end;

end.
