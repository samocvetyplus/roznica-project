unit ImportKabarovsky_Auto;

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportKabarovsky_TLB, Contnrs;

const
  INN : PChar = '444300367317';

type
  TJewImportAdamas = class(TAutoObject, IKabarovsky)
    FAppHandle: integer;
  private
    // from IJewImportKabarovsky
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

implementation

{ TJewImportKabarovsky }

uses ComServ, SInvImp, Forms;

procedure TJewImportAdamas.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure TJewImportAdamas.SetAppHandle(AppHandle: Integer);
begin
  FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TJewImportAdamas, Class_Kabarovsky, ciMultiInstance, tmApartment);
end.
