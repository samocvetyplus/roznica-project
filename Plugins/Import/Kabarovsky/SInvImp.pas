//    compid:=200001134;

unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolEdit, DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons, 
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, IdGlobal,
  Excel2000, OleServer,ShellAPI;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    mmLog: TMemo;
    xl: TExcelApplication;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
  private
    FMargin : extended;
    procedure MakeMainFile(var FileName: string; var LineCount: integer);
    function  strtofloat2(str: string):real;
    procedure checkStr(var sourc_str: string);
    procedure InsDelim(var sourc_str: string);
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

const   NM_PROGRESSBAR = $60f1;
        key = chr(9);
        DELAY=500;
        sEnd = '�����';
        pEnd = '�����';
        shEnd = '��������';

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������
  LineCount: integer; //���-�� ����� � ������������� �����
  DepRecurs: integer;
implementation

uses dbUtil, RegExpr, Variants, RxStrUtils, Math, MsgDialog, ImportKabarovsky_Auto,
     fmUtils, UtilLib, bsUtils;

resourcestring
  rcOrganizationNotFound = '������������� ���������� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:Variant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ���������� � ��, ��� ������ ����������� ���
    CompId := 200001134; // ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := Integer(r);
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST("NOW" AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger := SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
//    fmSInvImp.taSListSUPID.AsInteger :=
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.Import;

var ftext: TextFile;
    Tmp, Tmp2, Tmp3, FName: String;
    Art, Art2, Art_, Art2_, Good, Mat, D_matid, MainIns, MIns, Ins, CurIns,PredIns,Country: String;
    IsIns: boolean;
    i,k,ContrSize, WriteSize, D_ins_Id, fl, ART2ID, SelID, SInfoID, UnitId: integer;
    QUANTITY: smallint;
    COLOR,CHROMATICITY,CLEANNES,EDGTYPE,EDGSHAPE,sz, tmp_:string;
    WEIGHT, W, Price, PredPrice: double;
    res: Variant;
    DefaultSeparator: char;
    StartInfo: TStartupInfo;
    ProcInfo: TProcessInformation;
    CycleCount: integer;
    tm: string;

  procedure AddLog(Msg : string);
  begin
    mmLog.Lines.Add(Msg);
  end;

  Function FindId (supname:string; fsup, ftable:integer):string;
  //����������� ���������� ������, ������� � ��.
  var res: Variant;
  begin
     res := ExecSelectSQL('select first 1 SNAME, D_SNAMESUPID from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
                           ' and fsup='+inttostr(fsup)+' and ftable='+inttostr(ftable)+' order by SNAME ', quTmp);
     if  VarIsNull(res[0]) then
       result:='-1'
     else
       result:=VarToStr(res[0]);
  end;
  procedure CreateGoods;
  //���� � ���� ��������� ����� � ����������, ����������� �� � ���� ������
  var goodstmp, goodstmp1, id: string;
      res: Variant;
  begin
    goodstmp1:=ExtractWord(2,tmp,[key]);
    Good:=AnsiUpperCase(ExtractWord(1,goodstmp1,[' ']));

    id:=FindId(good,3,0);
    if id<>'-1' then
    begin
      res := ExecSelectSQL('select Contrsize, WriteSz from D_Good where d_goodid like '#39+id+#39,quTmp);
      if VarIsNull(res[0]) then
      begin
        AddLog('�� ������� ���������� ����� '+Good+' � '+inttostr(i));
        readln(ftext,Tmp);
        Good := '-1';
        Tmp:=ReplaceStr(Tmp, ',', '.');
        inc(i);
      end
      else
      begin
        Good := id;
        Contrsize:=Integer(res[0]);
        Writesize:=Integer(res[1]);
      end;
    end;
  end;
  Function CountCharTillWord (s:string; pos:integer; kl:char):integer;
  var l, m:integer;
  begin
    l:=1;
    m:=1;
    while m < pos do
    begin
      while s[l] = kl do inc(l);
      while s[l]<> kl do inc(l);
      while s[l] = kl do inc(l);
      inc(m);
    end;
    result:=l-1;
  end;
  Procedure CreateMat;
  //����������� ���������
  var id: string;
  begin
    Mat:=ExtractWord(3,Tmp,[key]);
    Mat:='Au585'; //(?)
    id:=FindId(Mat,3,2);
    if id<>'-1' then
    begin
     D_matid:=id
    end
    else
    begin
      AddLog('�� ������� ���������� �������� '+mat+' � '+inttostr(i));
      readln(ftext,Tmp);
      Tmp:=ReplaceStr(Tmp, ',', '.');
      inc(i);
    end
  end;
  Procedure DefineIns;
  //����������� �������
  var id: string;

  begin
    IsIns:=false;
    MIns := ExtractWord(2,tmp,[key]);
    if trim(ExtractWord(3,MIns,[' ']))<>'' then
      IsIns:=true;
    if IsIns then
    begin
      Ins:=ExtractWord(3,MIns,[' ']);
      if IsNumeric(Ins[1]) then
        MainIns:='�'
      else
      begin
        id:=FindId(Ins,3,1);
        if id<>'-1' then
          MainIns:=id
        else
        begin
          AddLog('�� ������� ���������� ������ '+Ins+' � '+inttostr(i));
          readln(ftext,Tmp);
          Tmp:=ReplaceStr(Tmp, ',', '.');
          inc(i);
        end;
      end;
    end;
  end;

  Procedure CreateIns (fl:integer);
  var Id: String;
      res: Variant;
      flins: integer;
      ISFULLDISCRIP: boolean;
  begin
    CurIns:='';
    if IsIns then
    begin
      delete(MIns,1,CountCharTillWord(MIns,3,' '));
      k:=1;
      Tmp2:=trim(ansilowercase(ExtractWord(k, MIns, [')'])));
      while Tmp2<>''  do
      begin
        res := ExecSelectSQL('select ISFULLDISCRIP from D_Ins where Name = '#39+ExtractWord(1,tmp2,[' '])+#39, quTmp);
        if not VarIsNull(res) then
        begin
          if trim(VarToStr(res)) = '1' then ISFULLDISCRIP:= true
          else ISFULLDISCRIP := false;
        end;
        if IsNumeric(tmp2[1]) then
        begin
          Ins:='�';
          QUANTITY:=strtoint(ExtractWord(2, Tmp2, ['(']));
          EdgShape:=ExtractWord(2, tmp2, [' '])+'.';
          EDGTYPE :=ExtractWord(3, Tmp2, [' ']);
          Weight:=strtofloat (ExtractWord(4, Tmp2, [' ']));
          tmp3:=ExtractWord(5, Tmp2, [' ']);
          CHROMATICITY:=ExtractWord(1,tmp3,['/']);
          CLEANNES:=ExtractWord(2,tmp3,['(','/'])[1];
          flins:=0;
          CurIns:=CurIns+'|'+Ins+' '+inttostr(QUANTITY)+' '+EdgShape+' '+
                  EDGTYPE+' '+floattostr(Weight)+' '+ CLEANNES+' '+CHROMATICITY;
        end
        else if (tmp2[1]='.') then
        begin

          QUANTITY:=strtoint(ExtractWord(2, Tmp2, ['(']));
          delete(tmp2,1,1);
          CLEANNES:=ExtractWord(1,tmp2,['/']);
          delete(tmp2,1,length(CLEANNES)+1);
          CHROMATICITY:=ExtractWord(1,tmp2,['(']);
          CurIns:=CurIns+'|'+Ins+' '+inttostr(QUANTITY)+' '+EdgShape+' '+
                  EDGTYPE+' '+floattostr(Weight)+' '+ CLEANNES+' '+CHROMATICITY;
        end
        else
        begin
          Ins := ExtractWord(1,tmp2,[' ']);
          id:=FindId(AnsiLowerCase(ins),3,1);
          QUANTITY := 0;
          if id<>'-1' then
          begin
            res := ExecSelectSQL('select D_Ins_Id from D_Ins where d_insid like '#39+id+#39, quTmp);
            if not VarIsNull(res) then
            begin
              D_Ins_Id:=Integer(res);
              ins := id;
            end
            else
            begin
              AddLog('�� ������� ���������� ������ '+Ins+' � '+inttostr(i));
              readln(ftext,Tmp);
              Tmp:=ReplaceStr(Tmp, ',', '.');
              inc(i);
            end;
            if (ISFULLDISCRIP)  then
            begin
              QUANTITY:=strtoint(ExtractWord(2, Tmp2, ['(']));
              if  (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) <> '��') and
                  (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) <> '��') and
                  (AnsiUpperCase(ExtractWord(2, Tmp2, [' '])) <> '��') then
              begin
                EdgShape := ExtractWord(3, Tmp2, [' ']);
                Weight:=strtofloat (ExtractWord(4, Tmp2, [' ']));
                Tmp2 := ExtractWord(6, Tmp2, [' ']);
              end
              else
              begin
                EdgShape := ExtractWord(2, Tmp2, [' ']);
                Weight:=strtofloat (ExtractWord(3, Tmp2, [' ']));
                Tmp2 := ExtractWord(5, Tmp2, [' ']);
              end;
              CLEANNES:=ExtractWord(1,tmp2,['/']);
              CHROMATICITY:=ExtractWord(2,tmp2,['/']);
              flins := 0;
            end
          end
          else
            flins:=1;

          tmp3:=ExtractWord(2, Tmp2, ['(']);
          if QUANTITY = 0 then
          begin
            if trim(ExtractWord(2, Tmp3, [' ']))='' then
              QUANTITY:=strtoint(Tmp3)
            else
              QUANTITY:=strtoint(ExtractWord(2, Tmp3, [' ']));
          end;
          if flins=1 then
           CurIns:=CurIns+'|'+Ins+' '+inttostr(QUANTITY)
          else
           CurIns:=CurIns+'|'+Ins+' '+inttostr(QUANTITY)+' '+EdgShape+' '+
                  EDGTYPE+' '+floattostr(Weight)+' '+ CLEANNES+' '+CHROMATICITY;
        end;

        if fl = 1 then
        begin
          if flins=0 then
            ExecSql('execute procedure ADDINSA2(' +inttostr(k)+','+inttostr(ART2ID)+','#39+Ins+#39','+
                      inttostr(QUANTITY)+','+FloatToStr(WEIGHT)+',null,'''+
                      CHROMATICITY+''','''+CLEANNES+''', '''+EDGTYPE+''','''+EDGSHAPE+''',null)', quTmp)
          else
            ExecSQL('execute procedure ADDINSA2(' +inttostr(k)+','+inttostr(ART2ID)+','#39+Ins+#39','+
                      inttostr(QUANTITY)+', null, null, null, null, null, null, null)', quTmp);
        end;
        inc(k);
        Tmp2:=trim(ansilowercase(ExtractWord(k, MIns, [')'])));
      end
    end;
  end;


begin
  // �������� ���
  mmLog.Lines.Clear;
  Screen.Cursor := crHourGlass;
  FName := ExtractFilePath(fledImp.FileName) + 'TempKabar.txt';
  FillChar(StartInfo, SizeOf(TStartupInfo), #0);
  FillChar(ProcInfo, SizeOf(TProcessInformation), #0);
  // ����������� ��������� ���� �� ���������
  CreateProcess(nil, PChar('sort /+3 '+ FName+'1' + ' /O '+ FName), nil, nil, False,
                CREATE_NEW_PROCESS_GROUP+NORMAL_PRIORITY_CLASS,
                nil, nil, StartInfo, ProcInfo);

  CycleCount := 1;
  repeat
    Application.ProcessMessages;
    Sleep(DELAY);
    Application.ProcessMessages;
    inc(CycleCount);
  until (FileExists(FName) or (CycleCount> 1000));

  CloseHandle(ProcInfo.hProcess);
  try
    if not FileExists(FName) then
    begin
      AddLog('���� '+FName+' �� ����������!');
      eXit;
    end;

    AssignFile(ftext, FName);
    Reset(ftext);

    prbrImp.Min := 0;
    prbrImp.Max := LineCount;
    prbrImp.Step := 1;
    DefaultSeparator := DecimalSeparator;
    DecimalSeparator:='.';
    i:=1;
    readln(ftext,Tmp);
    prbrImp.StepIt;
    Tmp:=ReplaceStr(Tmp, ',', '.');
    Art := '';

    CreateGoods;
    PredIns:='';
    Price := 0;
    //���� �� ���� ��������
    while (Tmp<>'') do
    begin
      Art_ := Art;
     //  ����������� ��������

      Art:=AnsiLowerCase(ExtractWord(2,ExtractWord(2,Tmp,[key]),[' ']));
      Art:=ReplaceStr(Art, '(','-');
      Art:=ReplaceStr(Art, ')', '');

      CreateMat;

      Country := '��';                                                  {������}
      if (trim(ExtractWord(4,tmp,[key]))='��.') or (trim(ExtractWord(4,tmp,[key]))='����') then
        UnitId:=0
      else
        UnitId:=1;                {��. ���������}
      w:=strtofloat2(ExtractWord(9,tmp,[key]));                             {���}
      sz :='-';                                                         {������}
      PredPrice := Price;
      Price:=strtofloat2(ExtractWord(11,tmp,[key]));                 {���� � ���}
      DefineIns;
      if MainIns = '' then MainIns := '�';
      {������� 2}
      if IsIns then
        if IsNumeric(ExtractWord(3,tmp,[key])[1]) then
           art2:= ExtractWord(3,tmp,[key])
        else
        if Art=Art_ then
           art2:=inttostr(strtoint(art2)+1)
        else art2:='1'
      else
      begin
{        res := ExecSelectSQL(' select min(a.art2) from art2 a ' +
                             ' where a.d_compid = '+ IntToStr(CompID)+ 'and  a.art = ''' +
                                     Art + '''  and  a.d_goodid = ''' + trim(good)+''' and '+
                                    ' d_matid = '''+trim(d_matid)+''' and d_insid = '''+MainIns+'''', quTmp);
        if (not VarIsNull(res)) then
        begin
          if VarToStr(res) <> '' then
             art2 := VarToStr(res[0])
        end
        else }art2:= ExtractWord(3,tmp,[key])// '-';
      end;
      if good <> '-1' then
      begin
        res := ExecSelectSQL('select R_ART2ID,R_SELID,R_SINFOID from InsertItem_Kabarovsky('#39+Art+#39','#39+Art2+#39','
               + IntToStr(CompId)+ ','#39 + trim(Good)+ #39','#39 + trim(D_MatID) + #39','#39+'��'#39+','+
               IntToStr(UnitId)+','#39+MainIns+#39', '+ FloatToStr(W)+','#39+Sz+#39','+
               FloatToStr(Price)+','+IntToStr(DocId) +','+FloatToStr(FMargin)+', null)',quTmp);

        if Not VarIsNull(res) then
        begin
          Art2Id := Integer(res[0]);
          SelId := Integer(res[1]);
          SInfoId := Integer(res[2]);
        end;
      end;
      CreateIns(1);
////////////////////////////////////

      // ������� ������� � ����� �� ���������,� ����� �� ����������������
      inc(i);
      if eof(ftext) then break;
      Readln(ftext,tmp);
      prbrImp.StepIt;
      Tmp:=ReplaceStr(Tmp, ',', '.');
      CreateGoods;


      Art_:=AnsiLowerCase(ExtractWord(2,ExtractWord(2,Tmp,[key]),[' ']));
      Art_:=ReplaceStr(Art_, '(','-');
      Art_:=ReplaceStr(Art_, ')', '');
      PredPrice := Price;
      Price:=strtofloat2(ExtractWord(11,tmp,[key]));

      CreateMat;
      PredIns:=CurIns;

      DefineIns;
////////////////////////////////////////
      CreateIns(0);
//////////////////////////////////////

      {������� 2}
      if IsIns then
        if (Art=Art_) and (PredIns=CurIns)and (PredPrice = Price) then
          art2_:=art2
        else
          art2_:=''
      else
      begin
{        art2_:=art2;
        res := ExecSelectSQL(' select min(a.art2) from art2 a ' +
                      ' where a.d_compid = ' + IntToStr(CompID) + ' and  a.art = ''' +
                             Art + '''  and  a.d_goodid = ''' + trim(good)+''' and '+
                            ' d_matid = '''+trim(d_matid)+''' and d_insid = '''+MainIns+'''', quTmp);
        if (Not VarIsNull(res))then
        begin
          if VarToStr(res) <> '' then
            art2 := VarToStr(res[0])
        end
        else} art2:= ExtractWord(3,tmp,[key]); //'-';
      end;

      while ((Art=Art_)and(Art2=Art2_))and(trim(ExtractWord(1,Tmp,[key]))<>sEnd)  do
      begin
        Country := '��';                                          {������}
        if (trim(ExtractWord(4,tmp,[key]))='��.') or
          (trim(ExtractWord(4,tmp,[key]))='����') then
            UnitId:=0
        else
            UnitId:=1;                                     {��. ���������}
        w:=strtofloat2(ExtractWord(9,tmp,[key]));                     {���}
        sz :='-';                                                 {������}
        ExecSQL('execute procedure InsNItem('+IntToStr(Art2Id)+','+
                 IntToStr(SelId)+','+IntToStr(SInfoId)+','+FloatToStr(W)+
                 ','#39+Sz+#39','+IntToStr(UnitId)+', 0)' ,  quTmp);
        Readln(ftext,tmp);
        prbrImp.StepIt;
        Tmp:=ReplaceStr(Tmp, ',', '.');
        tmp_:=trim(ExtractWord(1,Tmp,[key]));
        if (tmp_=sEnd)
        then break;

        CreateGoods;

        Art_:=AnsiLowerCase(ExtractWord(2,ExtractWord(2,Tmp,[key]),[' ']));
        Art_:=ReplaceStr(Art_, '(','-');
        Art_:=ReplaceStr(Art_, ')', '');
        PredPrice := Price;
        Price:=strtofloat2(ExtractWord(11,tmp,[key]));            {���� � ���}
        CreateMat;
        PredIns:=CurIns;
        DefineIns;
////////////////////////////////////////
        CreateIns(0);
//////////////////////////////////////

        {������� 2}
        if IsIns then
          if (Art=Art_) and (PredIns=CurIns) and (PredPrice = Price)then
            art2_:=art2
          else
            art2_:=''
        else
        begin
{          res := ExecSelectSQL(' select min(a.art2) from art2 a ' +
                             ' where a.d_compid = '+ IntToStr(CompID)+ ' and  a.art = ''' +
                             Art + '''  and  a.d_goodid = ''' + good+''' and '+
                             ' d_matid = '''+d_matid+''' and d_insid = '''+MainIns+'''', quTmp);
          if Not VarIsNull(res) then
          begin
             if VarToStr(res)<>'' then
               art2 := VarToStr(res);
          end
          else } art2:= ExtractWord(3,tmp,[key]); // '-';
        end;
        inc(i);
      end;
    end;
    CloseFile(fText);
    DeleteFile(FName+'1');
    DeleteFile(FName);
   finally
     DecimalSeparator:=DefaultSeparator;
     Screen.Cursor := crDefault;
  end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
  DocNo, DocDate, SupINN, tmpstr : string;
  r : Variant;
  i : integer;
begin
  MakeMainFile(Name,LineCount);
  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    for i:=1  to {16}14 do     // ������ ����� ���������
    begin
      ReadLn(ftext,tmpstr);
      if i= 4 then
        SupINN :=  ExtractWord(1, tmpstr, [#9]) //ExtractWord(2, tmpstr, [','])
      else if i= {16}14 then
      begin
        DocNo := ExtractWord({1}2, tmpstr, [#9]); //ExtractWord(2, tmpstr, [#9]);
        DocDate := ExtractWord({2}3, tmpstr, [#9]); //ExtractWord(3, tmpstr, [#9]);
      end
    end;
    SupINN := copy(SupInn,Length(SupInn)-11,12); //ExtractWord(2, SupINN, [' ']);

    try
      taSList.Edit;
      taSListSSF.AsString := DocNo;
      taSListNDATE.AsDateTime := StrToDateTime(DocDate);
      if(SupINN <> '')then
      begin
        r := ExecSelectSQL('select D_CompId,PAYTYPEID,NDSID from D_Comp where INN="'+SupINN+'"', quTmp);
        if not VarIsNull(r[0]) then taSListSUPID.AsInteger := Integer(r[0]);
        if not VarIsNull(r[1]) then  taSListPAYTYPEID.AsInteger := Integer(r[1]);
        if not VarIsNull(r[2]) then  taSListNDSID.AsInteger := Integer(r[2]);
      end;
      taSList.Post;
    except
    end;
  finally
    CloseFile(ftext);
  end;
  if ExtractWord(2,Name, ['.']) <> 'xls' then
     DeleteFile(Name);
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;
procedure TfmSInvImp.MakeMainFile(var FileName: string; var LineCount: integer);
var ftext, ftext_source: TextFile;
    i,j: integer;
    tmpstr,tm: string;
begin
//����������� ��������� ����, � ������� ��������� ������������ ����������� ������ �� ������ �����
// ����� ��� �������� => �����
   try
    xl.ConnectKind := ckRunningOrNew;
    xl.Connect;
    xl.Workbooks.Add(FileName, LOCALE_USER_DEFAULT);
    FileName := ExtractFilePath(FileName) + 'TempKabar.txt';
    if FileExists(FileName) then DeleteFile(FileName);
   except
    ShowMessage('�� �������������� ���������� Excel');
    eXit;
   end;
   xl.ActiveWorkbook.SaveAs(FileName, 3, EmptyParam, EmptyParam, EmptyParam, EmptyParam,
                                       0, EmptyParam, False, EmptyParam, EmptyParam,0);
   xl.ActiveWorkbook.Close(0,EmptyParam, EmptyParam, 0);
   xl.Disconnect;
   xl.Quit;
   AssignFile(ftext,FileName+'1');
   AssignFile(ftext_source, FileName);
   Reset(ftext_source);
   ReWrite(ftext);
   try
     i := 1;
     LineCount:=0;
     while not eof(ftext_source) do
     begin
       readLn(ftext_source, tmpstr);
       tm :=ExtractWord(1,ExtractWord(1,tmpstr, [#9]), [' ']);
       if AnsiUpperCase(tm) = sEnd  then break;
       if AnsiUpperCase(tm) = pEnd then continue;
       if AnsiUpperCase(tm) = shEnd then
       begin
         for j:=1 to 18  do
           readLn(ftext_source, tmpstr);
         tm :=ExtractWord(1,ExtractWord(1,tmpstr, [#9]), [' ']);
       end;
       if (i >= {23}34) then // ������ ����� ���������
       begin
         DepRecurs := 0;
         InsDelim(Tmpstr);
         checkStr(Tmpstr);
         tmpstr := '1'+ copy(tmpstr, length(tm)+1, length(tmpstr));
         writeLn(ftext, tmpstr);
         inc(LineCount);
       end;
       inc(i);
     end;
   finally
     CloseFile(ftext);
     CloseFile(ftext_source);
   end;
end;
procedure TfmSInvImp.InsDelim(var sourc_str: string);
var i: integer;
    outstr: string;
    isFirst: boolean;
    count: integer;
begin
  isFirst := true;
  count := 0;
  for i:= 1  to Length(sourc_str) do
  begin
    if sourc_str[i] = key then
    begin
      if isFirst then
      begin
        outstr := outstr + sourc_str[i];
        isFirst:= false
      end
      else
      begin
        if count <> 0 then
          outstr := outstr  + '-' + sourc_str[i]
        else
          inc(count);
      end
    end
    else
    begin
      if outstr<> '' then
      begin
        if (sourc_str[i-1]= key)and (sourc_str[i]= ' ') and (sourc_str[i+1]= key) then outstr := outstr + '-'
        else
          outstr := outstr + sourc_str[i];
      end
      else
        outstr := outstr + sourc_str[i];
      isFirst:= true;
    end;
  end;
  sourc_str := outstr;
  sourc_str := replaceStr(Sourc_str,'*','');
end;
procedure TfmSInvImp.checkStr(var sourc_str: string);
var    tmpstr,outstr: string;
       i: integer;
begin
  if sourc_str = '' then exit;
  if (DepRecurs) > 100 then
  begin
    ShowMessage('�� ������ ������� ����� ����� �������:' + sEnd);
    exit;
  end;
  outstr := ExtractWord(1,sourc_str,[' ']);
  tmpstr := AnsiLowerCase(ExtractWord(2,ExtractWord(2,sourc_str,[key]),[' ']));
  for i:=1 to Length(tmpstr) do
      if IsNumeric(tmpstr[i]) then exit;
  sourc_str :=  outstr + copy(sourc_str, Length(outstr)+Length(tmpstr)+2, Length(sourc_str)-Length(tmpstr));
  inc(DepRecurs);
  checkStr(sourc_str);
end;
function TfmSInvImp.strtofloat2(str: string):real;
begin
  result := strtofloat(replacestr(str,' ',''));
end;

end.
