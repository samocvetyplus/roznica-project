library ImportKabarovsky;


uses
  ExceptionLog,
  ComServ,
  ImportKabarovsky_TLB in 'ImportKabarovsky_TLB.pas',
  ImportKabarovsky_Auto in 'ImportKabarovsky_Auto.pas',
  SInvImp in 'SInvImp.pas' {fmSInvImp};

const
  ClassName : PChar = 'JewImport.Kabarovsky';
  Version : PChar = '1.0';
  PluginName : PChar = '������ ��������� �����������';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� �����������. ��� 444300367317';

{$R *.TLB}

{$R *.res}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
