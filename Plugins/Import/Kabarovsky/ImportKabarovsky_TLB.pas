unit ImportKabarovsky_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision: 1.3 $
// File generated on 26.04.2005 13:39:04 from Type Library described below.

// ************************************************************************  //
// Type Lib: E:\Projects\Jew\Plugins\Import\Kabarovsky\ImportKabarovsky.tlb (1)
// LIBID: {AD7D839C-54A6-4094-81F6-6785B9B8DA49}
// LCID: 0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  JewImportMajorVersion = 1;
  JewImportMinorVersion = 0;

  LIBID_JewImport: TGUID = '{AD7D839C-54A6-4094-81F6-6785B9B8DA49}';

  IID_IKabarovsky: TGUID = '{52CB28A0-2B2B-4A92-9028-9D68F55B9762}';
  CLASS_Kabarovsky: TGUID = '{9C59195A-5A0D-4610-B66C-D39C0D864E31}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IKabarovsky = interface;
  IKabarovskyDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Kabarovsky = IKabarovsky;


// *********************************************************************//
// Interface: IKabarovsky
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {52CB28A0-2B2B-4A92-9028-9D68F55B9762}
// *********************************************************************//
  IKabarovsky = interface(IDispatch)
    ['{52CB28A0-2B2B-4A92-9028-9D68F55B9762}']
    procedure Import(const DBName: WideString; DepID: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IKabarovskyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {52CB28A0-2B2B-4A92-9028-9D68F55B9762}
// *********************************************************************//
  IKabarovskyDisp = dispinterface
    ['{52CB28A0-2B2B-4A92-9028-9D68F55B9762}']
    procedure Import(const DBName: WideString; DepID: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoKabarovsky provides a Create and CreateRemote method to
// create instances of the default interface IKabarovsky exposed by
// the CoClass Kabarovsky. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoKabarovsky = class
    class function Create: IKabarovsky;
    class function CreateRemote(const MachineName: string): IKabarovsky;
  end;

implementation

uses ComObj;

class function CoKabarovsky.Create: IKabarovsky;
begin
  Result := CreateComObject(CLASS_Kabarovsky) as IKabarovsky;
end;

class function CoKabarovsky.CreateRemote(const MachineName: string): IKabarovsky;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Kabarovsky) as IKabarovsky;
end;

end.
