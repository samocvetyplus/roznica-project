unit ImportKOYUZ_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportKOYUZ_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '4401001009';

type
  TKOYUZ = class(TAutoObject, IKOYUZ)
   FAppHandle : integer;
  private
 //   procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  protected
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TKOYUZ.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TKOYUZ.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TKOYUZ, Class_KOYUZ, ciMultiInstance, tmApartment);
end.
