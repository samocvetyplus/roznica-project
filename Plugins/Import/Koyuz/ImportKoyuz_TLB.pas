unit ImportKoyuz_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 13.05.2014 9:14:41 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\Koyuz\ImportKoyuz.tlb (1)
// LIBID: {788833F7-DCFA-4EFC-9086-749DC6EA07B3}
// LCID: 0
// Helpfile: 
// HelpString: ImportKoyuz Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportKoyuzMajorVersion = 1;
  ImportKoyuzMinorVersion = 0;

  LIBID_ImportKoyuz: TGUID = '{788833F7-DCFA-4EFC-9086-749DC6EA07B3}';

  IID_IKOYUZ: TGUID = '{4A89B79F-E569-495A-9F7B-7EDF3356A0E8}';
  CLASS_KOYUZ: TGUID = '{4F5763DC-92F5-4467-86AE-01A7FBBD7CDC}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IKOYUZ = interface;
  IKOYUZDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  KOYUZ = IKOYUZ;


// *********************************************************************//
// Interface: IKOYUZ
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4A89B79F-E569-495A-9F7B-7EDF3356A0E8}
// *********************************************************************//
  IKOYUZ = interface(IDispatch)
    ['{4A89B79F-E569-495A-9F7B-7EDF3356A0E8}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IKOYUZDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {4A89B79F-E569-495A-9F7B-7EDF3356A0E8}
// *********************************************************************//
  IKOYUZDisp = dispinterface
    ['{4A89B79F-E569-495A-9F7B-7EDF3356A0E8}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoKOYUZ provides a Create and CreateRemote method to          
// create instances of the default interface IKOYUZ exposed by              
// the CoClass KOYUZ. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoKOYUZ = class
    class function Create: IKOYUZ;
    class function CreateRemote(const MachineName: string): IKOYUZ;
  end;

implementation

uses ComObj;

class function CoKOYUZ.Create: IKOYUZ;
begin
  Result := CreateComObject(CLASS_KOYUZ) as IKOYUZ;
end;

class function CoKOYUZ.CreateRemote(const MachineName: string): IKOYUZ;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_KOYUZ) as IKOYUZ;
end;

end.
