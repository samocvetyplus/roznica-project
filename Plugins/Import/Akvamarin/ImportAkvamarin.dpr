library ImportAkvamarin;

uses
  ComServ,
  ImportAkvamarin_TLB in 'ImportAkvamarin_TLB.pas',
  ImportAkvamarin_Auto in 'ImportAkvamarin_Auto.pas',
  SinvImp in 'SinvImp.pas' {fmSInvImp},
  SNameSup in 'SNameSup.pas' {fmSNameSup};

const
  ClassName : PChar = 'ImportAKVAMARIN.AKVAMARIN';
  Version : PChar = '1.1';
  PluginName : PChar = '������ ��������� ���������';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ���������. ��� 4415004722';

{$R *.TLB}

{$R *.RES}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
