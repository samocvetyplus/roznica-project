object fmSInvImp: TfmSInvImp
  Left = 345
  Top = 163
  BorderStyle = bsDialog
  Caption = #1047#1072#1076#1072#1085#1080#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074' '#1080#1084#1087#1086#1088#1090#1072'. '#1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100' '#1040#1082#1074#1072#1084#1072#1088#1080#1085
  ClientHeight = 236
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object paHeader: TPanel
    Left = 0
    Top = 0
    Width = 424
    Height = 121
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      424
      121)
    object Label1: TLabel
      Left = 8
      Top = 60
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 104
      Width = 49
      Height = 13
      Caption = #1044#1072#1090#1072' '#1089'.'#1092'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 80
      Width = 34
      Height = 13
      Caption = #8470' '#1089'.'#1092'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 38
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 188
      Top = 40
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 188
      Top = 105
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 321
      Top = 106
      Width = 44
      Height = 13
      Caption = #1053#1072#1094#1077#1085#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 188
      Top = 60
      Width = 24
      Height = 13
      Caption = #1053#1044#1057
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laMargin: TLabel
      Left = 384
      Top = 106
      Width = 40
      Height = 13
      Caption = 'laMargin'
    end
    object Label15: TLabel
      Left = 188
      Top = 80
      Width = 59
      Height = 13
      Caption = #1042#1080#1076' '#1086#1087#1083#1072#1090#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 8
      Width = 87
      Height = 13
      Caption = #1060#1072#1081#1083' '#1089' '#1076#1072#1085#1085#1099#1084#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbDep: TLabel
      Left = 252
      Top = 106
      Width = 28
      Height = 13
      Caption = 'lbDep'
    end
    object Bevel1: TBevel
      Left = 2
      Top = 28
      Width = 416
      Height = 5
      Anchors = [akLeft, akTop, akRight]
      Shape = bsTopLine
      ExplicitWidth = 423
    end
    object edSSF: TDBEdit
      Left = 88
      Top = 78
      Width = 93
      Height = 21
      DataField = 'SSF'
      DataSource = dsrSList
      TabOrder = 3
    end
    object deNDate: TDBDateEdit
      Left = 89
      Top = 100
      Width = 93
      Height = 21
      Margins.Left = 1
      Margins.Top = 1
      DataField = 'NDATE'
      DataSource = dsrSList
      NumGlyphs = 2
      TabOrder = 4
    end
    object fledImp: TFilenameEdit
      Left = 100
      Top = 5
      Width = 321
      Height = 21
      OnAfterDialog = fledImpAfterDialog
      Filter = #1053#1072#1082#1083#1072#1076#1085#1099#1077' '#1080#1084#1087#1086#1088#1090#1072' (*.cdm)|*.dbf|'#1042#1089#1077'|*.*'
      DialogTitle = #1054#1090#1082#1088#1099#1090#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1086#1089#1090#1072#1074#1082#1080
      DirectInput = False
      NumGlyphs = 1
      TabOrder = 0
    end
    object redImp: TRichEdit
      Left = 592
      Top = 8
      Width = 185
      Height = 89
      Lines.Strings = (
        'redImp')
      TabOrder = 8
      Visible = False
    end
    object edSN: TDBEditEh
      Left = 88
      Top = 34
      Width = 93
      Height = 21
      DataField = 'SN'
      DataSource = dsrSList
      EditButtons = <>
      TabOrder = 1
      Visible = True
    end
    object deSDate: TDBDateTimeEditEh
      Left = 88
      Top = 56
      Width = 93
      Height = 21
      DataField = 'SDATE'
      DataSource = dsrSList
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 2
      Visible = True
    end
    object lcSup: TDBLookupComboboxEh
      Left = 249
      Top = 35
      Width = 173
      Height = 21
      DataField = 'SUPID'
      DataSource = dsrSList
      DropDownBox.Rows = 15
      DropDownBox.Width = -1
      EditButtons = <>
      KeyField = 'D_COMPID'
      ListField = 'FNAME'
      ListSource = dsrSup
      TabOrder = 5
      Visible = True
    end
    object lcNDS: TDBLookupComboboxEh
      Left = 249
      Top = 57
      Width = 173
      Height = 21
      DataField = 'NDSID'
      DataSource = dsrSList
      DropDownBox.Width = -1
      EditButtons = <>
      KeyField = 'NDSID'
      ListField = 'NAME'
      ListSource = dsrNDS
      TabOrder = 6
      Visible = True
    end
    object lcPayType: TDBLookupComboboxEh
      Left = 249
      Top = 80
      Width = 173
      Height = 21
      DataField = 'PAYTYPEID'
      DataSource = dsrSList
      DropDownBox.Width = -1
      EditButtons = <>
      KeyField = 'PAYTYPEID'
      ListField = 'NAME'
      ListSource = dsPayType
      TabOrder = 7
      Visible = True
    end
  end
  object plBn: TPanel
    Left = 0
    Top = 121
    Width = 424
    Height = 98
    Align = alClient
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 1
    object btnClose: TBitBtn
      Left = 346
      Top = 9
      Width = 75
      Height = 25
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = btnCloseClick
    end
    object btnImp: TBitBtn
      Left = 249
      Top = 9
      Width = 91
      Height = 25
      Caption = #1048#1084#1087#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
      TabOrder = 1
      OnClick = btnImpClick
    end
    object ListBox1: TListBox
      Left = 0
      Top = 40
      Width = 424
      Height = 58
      Align = alBottom
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object prbrImp: TProgressBar
    Left = 0
    Top = 219
    Width = 424
    Height = 17
    Align = alBottom
    Max = 500
    Step = 3
    TabOrder = 2
  end
  object db: TpFIBDatabase
    DBName = '10.1.0.252:E:\JEW.FDB'
    DBParams.Strings = (
      'lc_ctype=WIN1251'
      'user_name=SYSDBA'
      'password=masterkey')
    DefaultTransaction = tr
    DefaultUpdateTransaction = tr
    SQLDialect = 3
    Timeout = 0
    DesignDBOptions = []
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 188
    Top = 160
  end
  object tr: TpFIBTransaction
    DefaultDatabase = db
    TimeoutAction = TACommitRetaining
    TRParams.Strings = (
      'write'
      'nowait'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 188
    Top = 204
  end
  object taSList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SINV set'
      '  SUPID=:SUPID, '
      '  SDATE=:SDATE, '
      '  NDATE=:NDATE, '
      '  SSF=:SSF, '
      '  SN=:SN,  '
      '  NDSID=:NDSID, '
      '  PAYTYPEID=:PAYTYPEID  '
      'where SINVID=:SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      '(SINVID, SUPID, DEPID, SDATE, NDATE, SSF, SN,  PMARGIN, '
      
        'TR,  AKCIZ, ISCLOSED, ITYPE, NDSID, NDS, PAYTYPEID, PTR, TRNDS, ' +
        'PTRNDS, USERID,NotPR, edittr)'
      'VALUES'
      '(?SINVID, ?SUPID, ?DEPID, ?SDATE, ?NDATE, ?SSF, ?SN,  '
      
        '?PMARGIN, ?TR,  ?AKCIZ, ?ISCLOSED, ?itype, ?NDSID, ?NDS, ?PAYTYP' +
        'EID, ?PTR, ?TRNDS, ?PTRNDS, :USERID,0, 0)')
    SelectSQL.Strings = (
      'select *'
      'from SINV'
      'where SINVID=:DOCID')
    BeforeOpen = taSListBeforeOpen
    Transaction = tr
    Database = db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 228
    Top = 160
    poSQLINT64ToBCD = True
    object taSListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taSListSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taSListDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taSListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taSListNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object taSListSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taSListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object taSListPMARGIN: TFIBFloatField
      FieldName = 'PMARGIN'
    end
    object taSListTR: TFIBFloatField
      FieldName = 'TR'
    end
    object taSListAKCIZ: TFIBFloatField
      FieldName = 'AKCIZ'
    end
    object taSListISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object taSListITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taSListDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
    object taSListNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object taSListNDS: TFIBFloatField
      FieldName = 'NDS'
    end
    object taSListPAYTYPEID: TFIBIntegerField
      FieldName = 'PAYTYPEID'
    end
    object taSListCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taSListPTR: TFIBFloatField
      FieldName = 'PTR'
    end
    object taSListTRNDS: TFIBFloatField
      FieldName = 'TRNDS'
    end
    object taSListCRDATE: TFIBDateTimeField
      FieldName = 'CRDATE'
    end
    object taSListISIMPORT: TFIBSmallIntField
      FieldName = 'ISIMPORT'
    end
    object taSListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taSListMODEID: TFIBIntegerField
      FieldName = 'MODEID'
    end
    object taSListC: TFIBFloatField
      FieldName = 'C'
    end
    object taSListQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taSListSC: TFIBFloatField
      FieldName = 'SC'
    end
    object taSListW: TFIBFloatField
      FieldName = 'W'
    end
    object taSListCLTID: TFIBIntegerField
      FieldName = 'CLTID'
    end
    object taSListPTRNDS: TFIBFloatField
      FieldName = 'PTRNDS'
    end
    object taSListCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object taSListRATEURF: TFIBFloatField
      FieldName = 'RATEURF'
    end
    object taSListRSTATE: TFIBSmallIntField
      FieldName = 'RSTATE'
    end
    object taSListOPT: TFIBSmallIntField
      FieldName = 'OPT'
    end
    object taSListOPTRET: TFIBSmallIntField
      FieldName = 'OPTRET'
    end
    object taSListRETT: TFIBSmallIntField
      FieldName = 'RETT'
    end
    object taSListRETCOMPID: TFIBIntegerField
      FieldName = 'RETCOMPID'
    end
    object taSListRETDONE: TFIBSmallIntField
      FieldName = 'RETDONE'
    end
    object taSListREFRETSINVID: TFIBIntegerField
      FieldName = 'REFRETSINVID'
    end
    object taSListOPTMARGIN: TFIBFloatField
      FieldName = 'OPTMARGIN'
    end
    object taSListOPTRNORM: TFIBSmallIntField
      FieldName = 'OPTRNORM'
    end
    object taSListNOTPR: TFIBSmallIntField
      FieldName = 'NOTPR'
    end
    object taSListCOUNTCLOSED: TFIBIntegerField
      FieldName = 'COUNTCLOSED'
    end
    object taSListSEND_CDM: TFIBSmallIntField
      FieldName = 'SEND_CDM'
    end
    object taSListEDITTR: TFIBSmallIntField
      FieldName = 'EDITTR'
    end
    object taSListINVENTORYDATE: TFIBDateTimeField
      FieldName = 'INVENTORYDATE'
    end
  end
  object dsrSList: TDataSource
    DataSet = taSList
    Left = 228
    Top = 205
  end
  object taSup: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, NAME, SNAME, CODE, PAYTYPEID, NDSID'
      'FROM D_COMP'
      'WHERE SELLER>=1 and d_compid<>-1'
      'ORDER BY SNAME')
    CacheModelOptions.BufferChunks = 100
    OnCalcFields = taSupCalcFields
    Transaction = tr
    Database = db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 272
    Top = 160
    poSQLINT64ToBCD = True
    object taSupD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object taSupNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taSupSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taSupCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSupFNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'FNAME'
      Size = 80
      Calculated = True
    end
    object taSupPAYTYPEID: TIntegerField
      FieldName = 'PAYTYPEID'
      Origin = 'D_COMP.PAYTYPEID'
    end
    object taSupNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'D_COMP.NDSID'
    end
  end
  object dsrSup: TDataSource
    DataSet = taSup
    Left = 272
    Top = 206
  end
  object taNDS: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'SELECT NDSID, NAME, NDS, LONGNAME'
      'FROM D_NDS'
      'where ndsid<>-1000'
      'ORDER BY NDSID')
    Transaction = tr
    Database = db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 318
    Top = 160
    poSQLINT64ToBCD = True
    object taNDSNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taNDSNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object taNDSNDS: TFloatField
      FieldName = 'NDS'
    end
    object taNDSLONGNAME: TFIBStringField
      FieldName = 'LONGNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrNDS: TDataSource
    DataSet = taNDS
    Left = 316
    Top = 206
  end
  object taPayType: TpFIBDataSet
    SelectSQL.Strings = (
      'select PayTypeId, Name, DaysCount'
      'from D_PAYTYPE'
      'where PayTypeId<>-1000'
      'order by Name')
    CacheModelOptions.BufferChunks = 10
    Transaction = tr
    Database = db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 364
    Top = 162
    poSQLINT64ToBCD = True
    object taPayTypePAYTYPEID: TIntegerField
      FieldName = 'PAYTYPEID'
    end
    object taPayTypeNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taPayTypeDAYSCOUNT: TSmallintField
      FieldName = 'DAYSCOUNT'
      Origin = 'D_PAYTYPE.DAYSCOUNT'
    end
  end
  object dsPayType: TDataSource
    DataSet = taPayType
    Left = 364
    Top = 206
  end
  object quTmp: TpFIBQuery
    Transaction = tr
    Database = db
    Left = 396
    Top = 160
    qoStartTransaction = True
    qoTrimCharFields = True
  end
  object dsDBFimp: TDataSource
    DataSet = taDBFimp
    Left = 72
    Top = 168
  end
  object DBFconnect: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Documents and Se' +
      'ttings\'#1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088'\'#1056#1072#1073#1086#1095#1080#1081' '#1089#1090#1086#1083'\1.DBF;Mode=ReadWrite|Share Den' +
      'y None;Persist Security Info=False'
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 8
    Top = 168
  end
  object taDBFimp: TADOTable
    Connection = DBFconnect
    Left = 40
    Top = 168
  end
end
