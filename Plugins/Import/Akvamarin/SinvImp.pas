unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ToolEdit,} DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, rxToolEdit,
  ADODB, DBF, BDE, IdGlobal, regexpr;

const fsup = 9;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    dsDBFimp: TDataSource;
    DBFconnect: TADOConnection;
    taDBFimp: TADOTable;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

type
  // ��� �������
  TMounting = record
    Name : string;
    Q : integer;
    W : double;
    EdgShape : string; // ����� �������
    EdgT : string; // ��� �������
    Cleannes : string; // �������
    Chromaticity : string; // ���������
    Igroup : string; // ������
    Color : string; // ����
  end;

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������
  SSF: string;

  Kategory: integer; // ��������� ������� ������������ (�� D_DName_Sup)
  NameSup: string; // ������� ������������ � ����������
  

implementation


uses dbutil, Variants, RxStrUtils, Math, MsgDialog, ImportAKVAMARIN_Auto,
     fmUtils, UtilLib, DBTables, SNameSup;

resourcestring
  rcOrganizationNotFound = '������������� ��������� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';
  rsSelectFile='�� ������ ���� �������';

{$R *.DFM}


procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:VAriant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;

    // ��������� ���� �� ��� � ��, ��� ������ ����������� ���
    CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;

    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);
    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST(''NOW'' AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger :=SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;
  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
//ShowMessage('������ �������');    ����������������!!!
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  if ListBox1.Items.Text='' then ListBox1.Items.Text:='��������� ��������������� �������!';
  btnClose.SetFocus;
end;

procedure TfmSInvImp.Button1Click(Sender: TObject);
begin
ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
fmSNameSup.pgctrl.ActivePageIndex:=2;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.FormShow(Sender: TObject);
begin

end;

{******* ������ ��������� *******}
procedure TfmSInvImp.Import;
var
  mi: string;
  m1, m2, ii: integer;
  st1, st2, st3, st4 :string;

  ftext:textfile;
  UnitId,EndMarker, i, k, j : integer;
  Art2Id: integer;
  Tmp, Tmp2,
  Art, Art2, tmpArt, tmpArt2, ostArt,
  Good, Ins1,
  Mat, MIns,
  Sz ,Un, Country, SupId, UID_Sup,
  MainIns, tmpIns, Ins : string;
  Price, W : double;
  Log: TStringlist;
  {********* ������� ��� - ����� ********************************************}
  procedure AddLog(Msg : string);
  begin
     ListBox1.Items.Add(Msg);
     ListBox1.Refresh;
  end;
  {** �������� ������������ �������� ������������, ������������� ������-��.
      D_SNameSup - ������� ������������� ������� ������������, �������������
      ����������, � �������� �������������� ���������� *********************}
  function FindId (supname:string; fsup, ftable:integer):string;
  var
    r :Variant;
    table, id: string;
  begin
  case ftable of
   0: begin table:='d_good'; id:='d_goodid' end;
   1: begin table:='d_ins'; id:='d_insid' end;
   2: begin table:='d_mat'; id:='d_matid' end;
   3: begin table:='d_country'; id:='d_countryid' end;
  end;  // ������� ���� ������� ������������ � ����� �����-�
    r := ExecSelectSQL('select first 1 '+ id + ' from ' + table + ' where NAME = ''' +supname+'''', quTmp);
    if r=null then
    begin // ���� �� ����� - �� � ��������� ����������� ������������� ��� �����������
      r := ExecSelectSQL('select first 1 SNAME from D_SNAMESUP where SNAME_SUP = ''' +supname+
                         ''' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable)+' order by SNAME', quTmp);
      if r=null then Result:='-1' else Result:= trim(VarToStr(r));
    end
      else Result:= trim(VarToStr(r));
  end;

  function DosToWin(st:String):String;
  var
    Ch: PChar;
  begin
    Ch := StrAlloc(length(st)+1);
    OemToAnsi(pchar(st),ch);
    Result:=ch;
    StrDispose(ch);
  end;

  function WinToDos(st:String):String;
  var
    Ch: PChar;
  begin
    Ch := StrAlloc(length(st)+1);
    AnsiToOem(pchar(st),ch);
    Result:=ch;
    StrDispose(ch);
  end;

{******************************   IMPORT   ************************************}
var
  Mounting : TMounting;
  p,pi:integer;
  tmpEdgT:double;
  ogr:string;
  f: boolean; // ������� �������
  BrillIns: boolean; // ������� ��������� ��� ���� ����.������
  res, r_ins: variant;
  TS, TSWord, TSChars, TSDrop : TStringList;
  str, count_m, s1, s2, s3, s4,s5, reg_edit : string;
  r, r2: TRegExpr;

  tmp_s, tmp_s2, tmp_s3: string;
begin
  TS :=  TStringList.Create;
  TS.Delimiter := '.';
  TS.StrictDelimiter := true;
  TSWord := TStringList.Create;
  TSWord.Delimiter := ' ';
  TSWord.StrictDelimiter := true;
  TSChars := TStringList.Create;
  TSChars.Delimiter := '-';
  TSChars.StrictDelimiter := true;
  TSDrop := TStringList.Create;
  TSDrop.Delimiter := '/';
  TSDrop.StrictDelimiter := true;
  // �������� ���
  ListBox1.Clear; //**************
  Log:=TStringList.Create;
  // ��������� ���������� �� ���� � �������
  if not FileExists(fledImp.FileName) then
  begin
    ListBox1.Items.Add('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
  try
    //����������� � ����� dbf
    taDBFimp.Active:=false;
    DBFconnect.Connected:=false;
    DBFconnect.ConnectionString:='Data Source="'
                                 +trim(ExtractFilePath(fledImp.FileName)) +
                                 '";Extended Properties="DBASE IV;";"';
    DBFconnect.LoginPrompt:=false;
    DBFconnect.Mode:=cmReadWrite;
    DBFconnect.Connected:=true;
    taDBFimp.TableName:=SSF;
    taDBFimp.Active:=true;
    //����������� �����������
    //��������� �������
    prbrImp.Min := 0;
    prbrImp.Max := taDBFimp.RecordCount;
    prbrImp.Step := 1;
    ShowMessage('���������� ������� - '+IntToStr(taDBFimp.RecordCount));
    DecimalSeparator:='.';
    SupId:=taSupD_COMPID.AsString;
    // �������� ���� �� ��������
    try
      with taDBFimp do
      begin
        first;
        while not taDBFimp.eof do
        begin
          Uid_Sup:= trim(taDBFimp.FieldByName('SHK').AsString);  // ����. ��� �������
          Log.Add('Uid_Sup '+Uid_Sup);
          Art := trim(taDBFimp.FieldByName('ARTIK').AsString);
          tmp_s:=wintodos(art);
          tmp_s2:=DosToWin(art);
          Art2:='-';

          Sz := trim(taDBFimp.FieldByName('SIZE').AsString);      // ������
          W := taDBFimp.FieldByName('M').AsFloat;                 // ���
          Country:='��';  // ������ ������������
          Price:= taDBFimp.FieldByName('PRICE').AsFloat;        // ����. ���� � ���

          // �������� ��������
          try
            Mat := trim(taDBFimp.FieldByName('PROBA').AsString); // ���� ���� PROBA ���, �� ������� ���� GOLD
          except
            Mat := trim(taDBFimp.FieldByName('GOLD').AsString);
          end;

          if Mat = 'Gold 585 Dimonds' then // ���� ������� � �������� - �� �� ��. ����� - �� �����
             UnitId := 0   // ��
             else UnitId := 1;    // ��

          MIns:= FindId(Mat, fsup, 2);
          Log.Add('MIns '+MIns);
          if MIns='-1' then
          begin
            if MessageDialog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup+
                             '. ����������� ������ �������� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes then
            begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=2;
                NameSup:=Mat;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                MIns:= FindId(Mat,fsup, 2);
                if (MIns = '-1') then
                   AddLog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup)
                   else Mat := MIns;
                Screen.Cursor:=crDefault;
              end;
            end
              else AddLog('�� ������� ���������� �������� '+Mat+' � '+UID_Sup);
          end
            else Mat := MIns;

          // �������� �������� �������
          MainIns := '';
          try
            mi :=AnsiLowerCase(trim(taDBFimp.FieldByName('DEF').AsString));
            if UNITID=0 then // ��� ������������� ������
            // ���� ������� ����� ����-� � ������ ������, �� ���.���.- ���������,
            //����� - ������ ������ �� ������
            begin
               System.Delete(mi, length(mi), 1); // ������� ��������� ����� � ������
               // ����� �������� "." �� "," � ������,
               // ��� ��� - ����������� ������������� �����
               r := TRegExpr.Create;
               r.InputString := mi;
               r.Expression := '([0-9]+)\.([0-9])+';
               if r.Exec then
               begin
                  repeat
                  // ������� ��������� ���������, ������� �������� � Match[0]
                  s1:=r.Match[0];
                  s2 := ReplaceStr(s1, '.', ',');
                  mi := ReplaceStr(mi, s1, s2);
                  // � ���������� �����
                  until not r.ExecNext;
               end;
              r.Destroy;

              TS.DelimitedText := mi;
              TSWord.DelimitedText := trim(TS[0]);
              if StrPos(pchar(mi), '���������') <> nil then
                 MainIns := '���������'
               else MainIns := TSWord[1];
            end
          else  // ��� ���������� �������� ������� - ������ ������
            begin //(����� ������ ���-�� ������ ������� ������������, ��������� ������� ��� � ���� �����)
              TSWord.DelimitedText := mi;
              if IsNumeric(TSWord[0][1]) then
                 MainIns:= ReplaceStr(TSWord[0], TSWord[0][1], '');
              if IsNumeric(TSWord[0][2]) then
                 MainIns:= ReplaceStr(TSWord[0], TSWord[0][2], '');
            end;
          tmpIns:= FindId(MainIns, fsup, 1);
          if(tmpIns = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup+
                             '. ����������� ������ ������� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes
            then begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=1;
                NameSup:=MainIns;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                tmpIns:= trim(FindId(MainIns, fsup, 1));
                if (tmpIns = '-1') then
                   AddLog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup)
                   else MainIns := tmpIns;
                Screen.Cursor:=crDefault;
              end;
            end
            else AddLog('�� ������� ���������� ������� '+MainIns+' � '+UID_Sup);
          end
            else MainIns := tmpIns;
        //  end;
          except // ���� ���� DEF � ����� ���, ��
            MainIns:='-'; // ������� ��� �������
          end;
///////////////////////////////////////////////////////////////////////////////
          // �������� �����
          tmpArt:= AnsiLowerCase(taDBFimp.FieldByName('TOVAR').AsString);
          Good:=copy(tmpArt, 1, pos(' ',tmpArt)-1); // 1-� ����� - ������������ �������

          Ins1:= FindId(Good,fsup, 0);
          if(Ins1 = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ����� '+Good+' � '+UID_Sup+
                             '. ����������� ������ ����� ������������������ ��������?',
                              mtWarning, [mbYes, mbNo], 0)=mrYes
            then begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=0;
                NameSup:=Good;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                Ins1:= FindId(Good,fsup, 0);
                if (Ins1 = '-1') then
                   AddLog('�� ������� ���������� ����� '+Good+' � '+UID_Sup)
                   else Good := Ins1;
                Screen.Cursor:=crDefault;
              end;
            end
            else begin
              AddLog('�� ������� ���������� ����� '+Good+' � '+UID_Sup);
            end;
            end
            else Good := Ins1;

     // ���� ����� ������� ������� � ����, �� ���2 �������� ���,
     // ��� ���������� � ����, ����� �����, ����� �������� � �����
     // *������ ��� ���������� ������. ��� ����-�� ��. ���2 ������ ����� ����-��
         if (mat <> '��') then
         begin
           res := ExecSelectSQL('select r_art2, r_ins, r_good from InsertItem_Diamant('#39+Art+#39','#39+Art2+#39','
                                + VarToStr(CompId) + ','#39 + trim(Good)+ #39','#39 + trim(Mat) +
                                #39','#39 +trim(Country) + #39 + ',' + IntToStr(UnitId) +','#39 + MainIns +
                                #39', '+ FloatToStr(Price) + ',' +IntToStr(DocId) +')', quTmp);

           if not VarIsNull(res[0]) then // ������ ��� ���������� ������
              Art2 := res[0];
           if not VarIsNull(res[1]) then
              MainIns := res[1];
           if not VarIsNull(res[2]) then
              Good := res[2];
         end;
      // ������� ������� � ���������
      quTmp.Close;
      quTmp.SQL.Text := 'execute procedure InsFItem(:ART, :ART2, :GOOD, :MAT, :COUNTRY,'+
        ':UNITID, :COMPID, :SUPID, :MAININS, :W, :SZ, :PRICE, :SINVID, :DEPID, :PNDS, :NDSID,'+
        ':MARGIN, :SSF, :SDATE, :SN, :NDATE, :UID_SUP)';
      quTmp.Prepare;
      quTmp.ParamByName('ART').AsString := Art;
      quTmp.ParamByName('ART2').AsString := Art2;
      quTmp.ParamByName('GOOD').AsString := Good;
      quTmp.ParamByName('MAT').AsString := Mat;
      quTmp.ParamByName('COUNTRY').AsString := Country;
      quTmp.ParamByName('UNITID').AsInteger := integer(UnitId);
      quTmp.ParamByName('COMPID').AsInteger := StrToInt(CompId);
      quTmp.ParamByName('SUPID').AsInteger := taSListSUPID.AsInteger;
      quTmp.ParamByName('MAININS').AsString := MainIns;
      quTmp.ParamByName('W').AsDouble := W;
      quTmp.ParamByName('SZ').AsString := Sz;
      quTmp.ParamByName('PRICE').AsDouble := Price;
      quTmp.ParamByName('SINVID').AsInteger := taSListSINVID.AsInteger;
      quTmp.ParamByName('DEPID').AsInteger := taSListDepId.AsInteger;
      quTmp.ParamByName('PNDS').AsInteger := taNDSNDS.AsInteger;
      quTmp.ParamByName('NDSID').AsInteger := taSListNDSID.AsInteger;
      quTmp.ParamByName('MARGIN').AsDouble := FMargin;
      quTmp.ParamByName('SSF').AsString := taSListSSF.AsString;
      quTmp.ParamByName('SDATE').AsDateTime := taSListSDATE.AsDateTime;
      quTmp.ParamByName('SN').AsInteger := taSListSN.AsInteger;
      quTmp.ParamByName('NDATE').AsDateTime := taSListNDATE.AsDateTime;

      try
        quTmp.ExecQuery;
        tr.CommitRetaining;
        Art2Id := quTmp.Fields[0].AsInteger;
        quTmp.Close;
      except
        on E:Exception do
          begin
            AddLog('������ ��� ���������� ������� � ���.'+Art+' ('+IntToStr(i)+')'+' - '+e.Message);
            log.Add('������'+e.Message);
          end
      end;
      // ����� ���������� � ���������
      // ������ �������� �������
      if unitid = 0 then // ��� ������������� ������
      begin
        for j := 0 to TS.Count - 1 do
        begin
          TSWord.DelimitedText := trim(TS[j]);
          if(j=0)then
          begin
            quTmp.Close;
            quTmp.SQL.Text := 'delete from Ins where Art2Id='+IntToStr(Art2Id);
            Log.Add('if(j=0)then '+ quTmp.SQL.Text);
            quTmp.Prepare;
            quTmp.ExecQuery;
            quTmp.Transaction.CommitRetaining;
          end;

          Ins:= TSWord[1];
          tmpIns:=findId(Ins,fsup,1); // ���� � ������� �������������
          if(tmpIns = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup+
                             '. ����������� ������ ������� ������������������ ��������?',
                             mtWarning, [mbYes, mbNo], 0)=mrYes then
            begin
              try
                Screen.Cursor:=crSQLWait;
                Kategory:=1;
                NameSup:=Ins;
                ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
                tmpIns:= trim(FindId(Ins, fsup, 1));
                if (tmpIns = '-1') then
                   AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup)
                   else Ins := tmpIns;
                Screen.Cursor:=crDefault;
              end;
            end
              else
            begin
              AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup);
            end;
          end
            else Ins := tmpIns;
          // ������ �������:
          // 1 ��������� ��-57-0.09ct-3/5�. 7 ������� ��-1.15ct-3/�3�. 1 ������ ��-0.55ct-2/�2�.
          // 6 ������ � 4*2-0.7ct-3/3�. 1 �����.
          // � ����������� ����������� ��� ��������������
          Mounting.Name:=Ins; //������������ �������
          Mounting.Q:= StrToInt(TSWord[0]); //����������
          if TSWord.Count>2 then
          begin
          if TSWord[2] = '�' then // ���� ����� ������������ ����� �, �� ��� ����� "��"
          begin
             Mounting.EdgShape := '��.';          //�����
             TSChars.DelimitedText := TSWord[3];
             Mounting.EdgT:=TSChars[0]; //�������
             s4:=ReplaceStr(TSChars[1], 'ct', '');
             s5:=ReplaceStr(s4, ',', '.');
             Mounting.W:= StrToFloatDef(s5, 2); //���
             TSDrop.DelimitedText := TSChars[2];
             Mounting.Cleannes:=TSDrop[1];  // �������
             Mounting.Chromaticity:=TSDrop[0]; // ���������
             if IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)-1]) then
             begin
               Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)];  // ������
               System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
             end;
          end
             else
          begin
             TSChars.DelimitedText := TSWord[2];
             Mounting.EdgShape:=AnsiLowerCase(TSChars[0])+'.';  //�����

             r2 := TRegExpr.Create;
             r2.InputString := TSChars[1];
             r2.Expression := 'ct';
             if r2.Exec then
                s3 := r2.Match[0]
                else s3:='';
             r2.Destroy;   
             if s3 <> '' then
             begin    // ������ ��� ���
               Mounting.EdgT:=''; //������� �� �������
               s4:=ReplaceStr(TSChars[1], 'ct', '');
               s5:=ReplaceStr(s4, ',', '.');
               Mounting.W:= StrToFloatDef(s5, 2); //���
               TSDrop.DelimitedText := TSChars[2];
               Mounting.Cleannes:=TSDrop[1];  // �������
               Mounting.Chromaticity:=TSDrop[0]; // ���������
               if Length(Mounting.Cleannes)>2 then // ���� ������� ����� "������" ������� (��: �3�)
               begin
                 if IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)-1]) then
                 begin
                   Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)]; // ������
                   System.Delete(Mounting.Cleannes, 1, 1); //������� "������" �������
                   System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
                 end;
               end
                 else
               begin
                 if IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)-1]) then
                 begin
                   Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)]; // ������
                   System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
                 end;
               end;
             end
               else
             begin
               Mounting.EdgT:=TSChars[1]; //�������
               s4:=ReplaceStr(TSChars[2], 'ct', '');
               s5:=ReplaceStr(s4, ',', '.');
               Mounting.W:= StrToFloatDef(s5, 2); //���
               TSDrop.DelimitedText := TSChars[3];
               Mounting.Chromaticity:=TSDrop[0]; // ���������
               Mounting.Cleannes:=TSDrop[1];  // �������
               if Length(Mounting.Cleannes)>2 then // ���� ������� ����� "������" ������� (��: �3�)
               begin
                 if IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)-1]) then
                 begin
                   Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)]; // ������
                   System.Delete(Mounting.Cleannes, 1, 1); //������� "������" �������
                   System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
                 end;
               end
                 else
               begin
                 if IsNumeric(Mounting.Cleannes[length(Mounting.Cleannes)-1]) then
                 begin
                   Mounting.Igroup:=Mounting.Cleannes[length(Mounting.Cleannes)]; // ������
                   System.Delete(Mounting.Cleannes, length(Mounting.Cleannes), 1); //������� ����� ������ �� �������
                 end;
               end;
             end;
          end;
          end
            else
          begin
            Mounting.W:=0;
            Mounting.EdgShape:='';
            Mounting.EdgT:='';
            Mounting.Cleannes:='';
            Mounting.Chromaticity:='';
            Mounting.Igroup:='';
          end;
          Mounting.Color:='';

          quTmp.Close;
          quTmp.SQL.Text := 'execute procedure AddIns_Pr2(:ART2ID, :INSID, :Q, :W, :COLOR,'+
                            ':CHROMATICITY, :CLEANNES, :EDGTYPE, :EDGSHAPE, :IGROUP)';
          quTmp.Prepare;
          quTmp.ParamByName('ART2ID').AsInteger := Art2Id;
          quTmp.ParamByName('INSID').AsString := Mounting.Name;
          quTmp.ParamByName('Q').AsInteger := Mounting.Q;
          if Mounting.W=0 then
             quTmp.ParamByName('W').AsVariant := Null
             else quTmp.ParamByName('W').AsDouble := Mounting.W;
          if Mounting.Color='' then
             quTmp.ParamByName('COLOR').AsVariant := Null
             else quTmp.ParamByName('COLOR').AsString := Mounting.Color;
          if (Mounting.Chromaticity='') then
              quTmp.ParamByName('CHROMATICITY').AsVariant := Null
              else quTmp.ParamByName('CHROMATICITY').AsString := Mounting.Chromaticity;
          if (Mounting.Cleannes='') then
             quTmp.ParamByName('CLEANNES').AsVariant := Null
             else quTmp.ParamByName('CLEANNES').AsString := Mounting.Cleannes;
          if (Mounting.EdgT='') then
             quTmp.ParamByName('EDGTYPE').AsVariant := Null
             else quTmp.ParamByName('EDGTYPE').AsString := Mounting.EdgT;
          if (Mounting.EdgShape='') then
              quTmp.ParamByName('EDGSHAPE').AsVariant := Null
              else quTmp.ParamByName('EDGSHAPE').AsString := Mounting.EdgShape;
          if (Mounting.Igroup='') then
             quTmp.ParamByName('IGROUP').AsVariant := Null
             else quTmp.ParamByName('IGROUP').AsString := Mounting.Igroup;
          quTmp.ExecQuery;
          tr.CommitRetaining;
        end; // ����� �����
      end
        else // ��� ����������
      begin
        for j := 0 to TSWord.Count-1 do
        begin
          TSChars.DelimitedText := trim(TSWord[j]);
          if(j=0)then
          begin
            quTmp.Close;
            quTmp.SQL.Text := 'delete from Ins where Art2Id='+IntToStr(Art2Id);
            Log.Add('if(j=0)then '+ quTmp.SQL.Text);
            quTmp.Prepare;
            quTmp.ExecQuery;
            quTmp.Transaction.CommitRetaining;
          end;
          count_m:='';
          ins:='';

          if IsNumeric(TSChars[0][1]) then
             count_m:=count_m + TSChars[0][1]
             else Ins:=ReplaceStr(TSChars[0], count_m, '');
          if IsNumeric(TSChars[0][2]) then
             count_m:=count_m + TSChars[0][2]
             else Ins:=ReplaceStr(TSChars[0], count_m, '');

          tmpIns:=findId(Ins,fsup,1); // ���� � ������� �������������
          if(tmpIns = '-1')then
          begin
            if MessageDialog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup+
                           '. ����������� ������ ������� ������������������ ��������?',
                           mtWarning, [mbYes, mbNo], 0)=mrYes then
            begin
              try
              Screen.Cursor:=crSQLWait;
              Kategory:=1;
              NameSup:=Ins;
              ShowAndFreeForm(TfmSNameSup, Self, TForm(fmSNameSup), True, False);
              finally
              tmpIns:= trim(FindId(Ins, fsup, 1));
              if (tmpIns = '-1') then
                 AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup)
                 else Ins := tmpIns;
              Screen.Cursor:=crDefault;
              end;
            end
              else AddLog('�� ������� ���������� ������� '+Ins+' � '+UID_Sup);
          end
            else Ins := tmpIns;
          // ������ ������������� ���� ����. ������
          Mounting.Name:=Ins; //������������ �������
          Mounting.Q:=0; //StrToInt(count_m); //���������� ������
          Mounting.W:=0;
          Mounting.EdgShape:='';
          Mounting.EdgT:='';
          Mounting.Cleannes:='';
          Mounting.Chromaticity:='';
          Mounting.Igroup:='';
          Mounting.Color:='';

          r_ins := ExecSelectSQL('select first 1 d_insid from Ins where Art2Id = ' +
                                  Inttostr(Art2Id) + ' and d_insid = ''' + Ins+'''', quTmp);

          if r_ins = null then // ���� ������� ��� �� �������� � ��������, �� ��������
          begin

          quTmp.Close;
          quTmp.SQL.Text := 'execute procedure AddIns_Pr2(:ART2ID, :INSID, :Q, :W, :COLOR,'+
                            ':CHROMATICITY, :CLEANNES, :EDGTYPE, :EDGSHAPE, :IGROUP)';
          quTmp.Prepare;
          quTmp.ParamByName('ART2ID').AsInteger := Art2Id;
          quTmp.ParamByName('INSID').AsString := Mounting.Name;
          quTmp.ParamByName('Q').AsInteger := Mounting.Q;
          if Mounting.w=0 then
             quTmp.ParamByName('W').AsVariant := Null
             else quTmp.ParamByName('W').AsDouble := Mounting.W;
          if Mounting.Color='' then
             quTmp.ParamByName('COLOR').AsVariant := Null
             else quTmp.ParamByName('COLOR').AsString := Mounting.Color;
          if (Mounting.Chromaticity='') then
              quTmp.ParamByName('CHROMATICITY').AsVariant := Null
              else quTmp.ParamByName('CHROMATICITY').AsString := Mounting.Chromaticity;
          if (Mounting.Cleannes='') then
             quTmp.ParamByName('CLEANNES').AsVariant := Null
             else quTmp.ParamByName('CLEANNES').AsString := Mounting.Cleannes;
          if (Mounting.EdgT='') then
             quTmp.ParamByName('EDGTYPE').AsVariant := Null
             else quTmp.ParamByName('EDGTYPE').AsString := Mounting.EdgT;
          if (Mounting.EdgShape='') then
              quTmp.ParamByName('EDGSHAPE').AsVariant := Null
              else quTmp.ParamByName('EDGSHAPE').AsString := Mounting.EdgShape;
          if (Mounting.Igroup='') then
             quTmp.ParamByName('IGROUP').AsVariant := Null
             else quTmp.ParamByName('IGROUP').AsString := Mounting.Igroup;
          quTmp.ExecQuery;
          tr.CommitRetaining;
          end;
        end;    // ����� ����� �� ��������
      end; // ����� ������� ������� ����������

      quTmp.close;
      ExecSQL('execute procedure SetMainIns('+IntToStr(Art2Id)+','''+ MainIns +''')', quTmp);
      tr.CommitRetaining;
      qutmp.Close;

      prbrImp.StepIt;
      Repaint;
      Application.ProcessMessages;

      Next;
    end;
 end;
 finally
    Screen.Cursor := crDefault;
 end;
 except
   Log.SaveToFile('C:\log_akvamarin.txt');
   log.Destroy;
 end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  DocNo, DocDate, SupINN: string;
  r: Variant;
  i:byte;
begin
  if not FileExists(Name) then eXit;
  SSF := ExtractFileName(Name);
  i := pos('.',SSF);
  delete(SSF,i,strlen(pchar(SSF)));
  try
    taSList.Edit;
    taSListSSF.AsString := SSF;
    if(SupINN <> '')then
    begin
      r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+SupINN+'"', quTmp);
      if not VarIsNull(r) then taSListSUPID.AsInteger := r;
    end;
    taSList.Post;
  except
  end;
end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

end.


