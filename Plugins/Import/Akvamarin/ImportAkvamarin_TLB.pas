unit ImportAkvamarin_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 23.05.2014 15:30:34 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\Akvamarin\ImportAkvamarin.tlb (1)
// LIBID: {2E81245F-76F6-4C6A-95C4-A157E6CF138C}
// LCID: 0
// Helpfile: 
// HelpString: ImportAkvamarin Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportAkvamarinMajorVersion = 1;
  ImportAkvamarinMinorVersion = 0;

  LIBID_ImportAkvamarin: TGUID = '{2E81245F-76F6-4C6A-95C4-A157E6CF138C}';

  IID_IAKVAMARIN: TGUID = '{63D3BC65-6C0B-4E6A-B1DE-0FABB0913F54}';
  CLASS_AKVAMARIN: TGUID = '{D8593FF3-FE4A-4500-B526-3252877BF32B}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAKVAMARIN = interface;
  IAKVAMARINDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  AKVAMARIN = IAKVAMARIN;


// *********************************************************************//
// Interface: IAKVAMARIN
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {63D3BC65-6C0B-4E6A-B1DE-0FABB0913F54}
// *********************************************************************//
  IAKVAMARIN = interface(IDispatch)
    ['{63D3BC65-6C0B-4E6A-B1DE-0FABB0913F54}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IAKVAMARINDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {63D3BC65-6C0B-4E6A-B1DE-0FABB0913F54}
// *********************************************************************//
  IAKVAMARINDisp = dispinterface
    ['{63D3BC65-6C0B-4E6A-B1DE-0FABB0913F54}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoAKVAMARIN provides a Create and CreateRemote method to          
// create instances of the default interface IAKVAMARIN exposed by              
// the CoClass AKVAMARIN. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAKVAMARIN = class
    class function Create: IAKVAMARIN;
    class function CreateRemote(const MachineName: string): IAKVAMARIN;
  end;

implementation

uses ComObj;

class function CoAKVAMARIN.Create: IAKVAMARIN;
begin
  Result := CreateComObject(CLASS_AKVAMARIN) as IAKVAMARIN;
end;

class function CoAKVAMARIN.CreateRemote(const MachineName: string): IAKVAMARIN;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AKVAMARIN) as IAKVAMARIN;
end;

end.
