unit ImportAkvamarin_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportAkvamarin_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '4415004722';

type
   TAKVAMARIN = class(TAutoObject, IAKVAMARIN)
   FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ , SInvImp, Forms;

procedure  TAKVAMARIN.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TAKVAMARIN.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TAKVAMARIN, Class_AKVAMARIN, ciMultiInstance, tmApartment);
end.


