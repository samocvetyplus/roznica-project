unit ImportSkazka_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 23.05.2014 10:22:35 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\Skazka\ImportSkazka.tlb (1)
// LIBID: {3634A6BF-DF73-4820-8B51-BDA94E0DCDB6}
// LCID: 0
// Helpfile: 
// HelpString: ImportSkazka Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportSkazkaMajorVersion = 1;
  ImportSkazkaMinorVersion = 0;

  LIBID_ImportSkazka: TGUID = '{3634A6BF-DF73-4820-8B51-BDA94E0DCDB6}';

  IID_ISKAZKA: TGUID = '{DD0A1B01-230D-427E-BC19-081E2FE7813A}';
  CLASS_SKAZKA: TGUID = '{5A9696EB-4C68-45BB-8245-CBA11888F48D}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISKAZKA = interface;
  ISKAZKADisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SKAZKA = ISKAZKA;


// *********************************************************************//
// Interface: ISKAZKA
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DD0A1B01-230D-427E-BC19-081E2FE7813A}
// *********************************************************************//
  ISKAZKA = interface(IDispatch)
    ['{DD0A1B01-230D-427E-BC19-081E2FE7813A}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  ISKAZKADisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DD0A1B01-230D-427E-BC19-081E2FE7813A}
// *********************************************************************//
  ISKAZKADisp = dispinterface
    ['{DD0A1B01-230D-427E-BC19-081E2FE7813A}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoSKAZKA provides a Create and CreateRemote method to          
// create instances of the default interface ISKAZKA exposed by              
// the CoClass SKAZKA. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSKAZKA = class
    class function Create: ISKAZKA;
    class function CreateRemote(const MachineName: string): ISKAZKA;
  end;

implementation

uses ComObj;

class function CoSKAZKA.Create: ISKAZKA;
begin
  Result := CreateComObject(CLASS_SKAZKA) as ISKAZKA;
end;

class function CoSKAZKA.CreateRemote(const MachineName: string): ISKAZKA;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SKAZKA) as ISKAZKA;
end;

end.
