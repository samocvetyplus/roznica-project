unit ImportSKAZKA_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportSKAZKA_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '7722804638';

type
  TSKAZKA = class(TAutoObject, ISKAZKA)
   FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TSKAZKA.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TSKAZKA.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TSKAZKA, Class_SKAZKA, ciMultiInstance, tmApartment);
end.
