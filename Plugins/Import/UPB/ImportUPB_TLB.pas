unit ImportUPB_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 29.05.2014 14:20:37 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\UPB\ImportUPB.tlb (1)
// LIBID: {288EDB14-EE9B-4C99-AC4B-0630A16198FB}
// LCID: 0
// Helpfile: 
// HelpString: ImportUPB Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportUPBMajorVersion = 1;
  ImportUPBMinorVersion = 0;

  LIBID_ImportUPB: TGUID = '{288EDB14-EE9B-4C99-AC4B-0630A16198FB}';

  IID_IUPB: TGUID = '{6553456B-BB1A-4287-BC69-206A6F48D142}';
  CLASS_UPB: TGUID = '{992908BB-B64B-44A1-91C2-FF166C490C7C}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IUPB = interface;
  IUPBDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  UPB = IUPB;


// *********************************************************************//
// Interface: IUPB
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6553456B-BB1A-4287-BC69-206A6F48D142}
// *********************************************************************//
  IUPB = interface(IDispatch)
    ['{6553456B-BB1A-4287-BC69-206A6F48D142}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IUPBDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {6553456B-BB1A-4287-BC69-206A6F48D142}
// *********************************************************************//
  IUPBDisp = dispinterface
    ['{6553456B-BB1A-4287-BC69-206A6F48D142}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoUPB provides a Create and CreateRemote method to          
// create instances of the default interface IUPB exposed by              
// the CoClass UPB. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoUPB = class
    class function Create: IUPB;
    class function CreateRemote(const MachineName: string): IUPB;
  end;

implementation

uses ComObj;

class function CoUPB.Create: IUPB;
begin
  Result := CreateComObject(CLASS_UPB) as IUPB;
end;

class function CoUPB.CreateRemote(const MachineName: string): IUPB;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_UPB) as IUPB;
end;

end.
