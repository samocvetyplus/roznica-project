library ImportUPB;

uses
  ComServ,
  ImportUPB_TLB in 'ImportUPB_TLB.pas',
  ImportUPB_Auto in 'ImportUPB_Auto.pas',
  SNameSup in 'SNameSup.pas' {fmSNameSup},
  SinvImp in 'SinvImp.pas' {fmSInvImp};

const
  ClassName : PChar = 'ImportUPB.UPB';
  Version : PChar = '1.1';
  PluginName : PChar = '������ ��������� ���';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ���. ��� 7744000912';

{$R *.TLB}

{$R *.RES}
function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.