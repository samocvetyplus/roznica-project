unit ImportUPB_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportUPB_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '7744000912';

type
  TUPB = class(TAutoObject, IUPB)
   FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TUPB.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TUPB.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TUPB, Class_UPB, ciMultiInstance, tmApartment);
end.
