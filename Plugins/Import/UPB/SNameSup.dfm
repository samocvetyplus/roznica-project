object fmSNameSup: TfmSNameSup
  Left = 0
  Top = 0
  Caption = #1055#1088#1080#1074#1077#1076#1077#1085#1080#1077' '#1082#1088#1072#1090#1082#1080#1093' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1081' '#1082' '#1086#1073#1097#1077#1084#1091' '#1074#1080#1076#1091
  ClientHeight = 282
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pgctrl: TPageControl
    Left = 0
    Top = 0
    Width = 379
    Height = 282
    ActivePage = tsUsrView
    Align = alClient
    TabOrder = 0
    OnChange = pgctrlChange
    object tsUsrView: TTabSheet
      Tag = 1
      Caption = #1054#1073#1099#1095#1085#1099#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
      object Label1: TLabel
        Left = 3
        Top = 18
        Width = 127
        Height = 13
        Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1102':'
      end
      object gUsrView: TDBGridEh
        Left = 0
        Top = 42
        Width = 369
        Height = 177
        DataGrouping.GroupLevels = <>
        DataSource = dsSName
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnCellMouseClick = gUsrViewCellMouseClick
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object bClose1: TButton
        Left = 291
        Top = 225
        Width = 75
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1100
        TabOrder = 1
        OnClick = bClose1Click
      end
      object bCompare1: TButton
        Left = 194
        Top = 225
        Width = 75
        Height = 25
        Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100
        TabOrder = 2
        OnClick = bCompare1Click
        OnKeyPress = bCompare1KeyPress
      end
      object eName: TEdit
        Left = 136
        Top = 15
        Width = 121
        Height = 21
        TabOrder = 3
        OnChange = eNameChange
      end
    end
    object tsFullView: TTabSheet
      Tag = 2
      Caption = #1056#1072#1089#1096#1080#1088#1077#1085#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 1
      object lcKat: TDBLookupComboBox
        Tag = 4
        Left = 3
        Top = 19
        Width = 118
        Height = 21
        DropDownRows = 10
        KeyField = 'KAT'
        ListField = 'NAME_KAT'
        ListSource = dsKat
        TabOrder = 0
        OnCloseUp = lcKatCloseUp
      end
      object eSNameSup: TM207DBEdit
        Left = 146
        Top = 19
        Width = 95
        Height = 21
        TabOrder = 1
      end
      object lcSName: TDBLookupComboBox
        Left = 273
        Top = 19
        Width = 95
        Height = 21
        ListSource = dsSNameSam
        TabOrder = 2
      end
      object bCompare2: TButton
        Left = 194
        Top = 226
        Width = 75
        Height = 25
        Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100
        TabOrder = 3
        OnClick = bCompare2Click
      end
      object gFullView: TDBGridEh
        Left = 0
        Top = 46
        Width = 369
        Height = 173
        DataGrouping.GroupLevels = <>
        DataSource = dsSNameSup
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'NAME_KAT'
            Footers = <>
            Title.Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
            Width = 131
          end
          item
            EditButtons = <>
            FieldName = 'SNAME_SUP'
            Footers = <>
            Title.Caption = #1050#1088'. '#1085#1072#1080#1084'. '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
            Width = 129
          end
          item
            EditButtons = <>
            FieldName = 'SNAME'
            Footers = <>
            Title.Caption = #1050#1088#1072#1090#1082'. '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Width = 88
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object bClose2: TButton
        Left = 291
        Top = 225
        Width = 75
        Height = 25
        Caption = #1047#1072#1082#1088#1099#1090#1100
        TabOrder = 5
        OnClick = bClose2Click
      end
    end
  end
  object dsSName: TDataSource
    DataSet = taSName
    Left = 16
    Top = 216
  end
  object taSName: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = fmSInvImp.tr
    Database = fmSInvImp.db
    Left = 48
    Top = 216
  end
  object quTmp: TpFIBQuery
    Transaction = fmSInvImp.tr
    Database = fmSInvImp.db
    Left = 276
    Top = 216
    qoStartTransaction = True
    qoTrimCharFields = True
  end
  object taSNameSup: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.sname_sup, s.sname, s.ftable, sk.name_kat'
      'from D_SNameSup s, D_SNameSup_Kat sk'
      'where s.ftable = sk.kat and s.fsup = 2')
    Transaction = fmSInvImp.tr
    Database = fmSInvImp.db
    Left = 176
    Top = 216
    object taSNameSupSNAME_SUP: TFIBStringField
      FieldName = 'SNAME_SUP'
      Size = 30
      EmptyStrToNull = True
    end
    object taSNameSupSNAME: TFIBStringField
      FieldName = 'SNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object taSNameSupNAME_KAT: TFIBStringField
      FieldName = 'NAME_KAT'
      EmptyStrToNull = True
    end
    object taSNameSupFTABLE: TFIBSmallIntField
      FieldName = 'FTABLE'
    end
  end
  object dsSNameSup: TDataSource
    DataSet = taSNameSup
    Left = 144
    Top = 216
  end
  object dsKat: TDataSource
    AutoEdit = False
    DataSet = taKat
    Left = 80
    Top = 216
  end
  object taKat: TpFIBDataSet
    SelectSQL.Strings = (
      'select kat, name_kat from D_SNameSup_Kat ')
    Transaction = fmSInvImp.tr
    Database = fmSInvImp.db
    Left = 112
    Top = 216
    object taKatNAME_KAT: TFIBStringField
      FieldName = 'NAME_KAT'
      EmptyStrToNull = True
    end
    object taKatKAT: TFIBSmallIntField
      FieldName = 'KAT'
    end
  end
  object dsSNameSam: TDataSource
    DataSet = taSNameSam
    Left = 208
    Top = 216
  end
  object taSNameSam: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = fmSInvImp.tr
    Database = fmSInvImp.db
    Left = 240
    Top = 216
  end
end
