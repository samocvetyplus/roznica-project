unit SInvImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ToolEdit,} DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons,
  ComCtrls, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, DBCtrlsEh, DBGridEh, DBLookupEh, FIBQuery, pFIBQuery, rxToolEdit,
  CheckLst;

type
  TfmSInvImp = class(TForm)
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label13: TLabel;
    laMargin: TLabel;
    Label15: TLabel;
    edSSF: TDBEdit;
    deNDate: TDBDateEdit;
    fledImp: TFilenameEdit;
    plBn: TPanel;
    btnClose: TBitBtn;
    btnImp: TBitBtn;
    Label5: TLabel;
    redImp: TRichEdit;
    prbrImp: TProgressBar;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taSList: TpFIBDataSet;
    dsrSList: TDataSource;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcSup: TDBLookupComboboxEh;
    taSup: TpFIBDataSet;
    taSupD_COMPID: TIntegerField;
    taSupNAME: TFIBStringField;
    taSupSNAME: TFIBStringField;
    taSupCODE: TFIBStringField;
    taSupFNAME: TStringField;
    taSupPAYTYPEID: TIntegerField;
    taSupNDSID: TIntegerField;
    dsrSup: TDataSource;
    lcNDS: TDBLookupComboboxEh;
    taNDS: TpFIBDataSet;
    taNDSNDSID: TIntegerField;
    taNDSNAME: TFIBStringField;
    taNDSNDS: TFloatField;
    taNDSLONGNAME: TFIBStringField;
    dsrNDS: TDataSource;
    taPayType: TpFIBDataSet;
    taPayTypePAYTYPEID: TIntegerField;
    taPayTypeNAME: TFIBStringField;
    taPayTypeDAYSCOUNT: TSmallintField;
    dsPayType: TDataSource;
    lcPayType: TDBLookupComboboxEh;
    quTmp: TpFIBQuery;
    taSListSINVID: TFIBIntegerField;
    taSListSUPID: TFIBIntegerField;
    taSListDEPID: TFIBIntegerField;
    taSListSDATE: TFIBDateTimeField;
    taSListNDATE: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListSN: TFIBIntegerField;
    taSListPMARGIN: TFIBFloatField;
    taSListTR: TFIBFloatField;
    taSListAKCIZ: TFIBFloatField;
    taSListISCLOSED: TFIBSmallIntField;
    taSListITYPE: TFIBSmallIntField;
    taSListDEPFROMID: TFIBIntegerField;
    taSListNDSID: TFIBIntegerField;
    taSListNDS: TFIBFloatField;
    taSListPAYTYPEID: TFIBIntegerField;
    taSListCOMPID: TFIBIntegerField;
    taSListPTR: TFIBFloatField;
    taSListTRNDS: TFIBFloatField;
    taSListCRDATE: TFIBDateTimeField;
    taSListISIMPORT: TFIBSmallIntField;
    taSListUSERID: TFIBIntegerField;
    taSListMODEID: TFIBIntegerField;
    taSListC: TFIBFloatField;
    taSListQ: TFIBIntegerField;
    taSListSC: TFIBFloatField;
    taSListW: TFIBFloatField;
    taSListCLTID: TFIBIntegerField;
    taSListPTRNDS: TFIBFloatField;
    taSListCRUSERID: TFIBIntegerField;
    taSListRATEURF: TFIBFloatField;
    taSListRSTATE: TFIBSmallIntField;
    taSListOPT: TFIBSmallIntField;
    taSListOPTRET: TFIBSmallIntField;
    taSListRETT: TFIBSmallIntField;
    taSListRETCOMPID: TFIBIntegerField;
    taSListRETDONE: TFIBSmallIntField;
    taSListREFRETSINVID: TFIBIntegerField;
    taSListOPTMARGIN: TFIBFloatField;
    taSListOPTRNORM: TFIBSmallIntField;
    taSListNOTPR: TFIBSmallIntField;
    taSListCOUNTCLOSED: TFIBIntegerField;
    taSListSEND_CDM: TFIBSmallIntField;
    taSListEDITTR: TFIBSmallIntField;
    taSListINVENTORYDATE: TFIBDateTimeField;
    lbDep: TLabel;
    Bevel1: TBevel;
    ListBox1: TListBox;
    procedure btnImpClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taSupCalcFields(DataSet: TDataSet);
    procedure taNDSAfterOpen(DataSet: TDataSet);
    procedure taPayTypeAfterOpen(DataSet: TDataSet);
  private
    FMargin : extended;
    procedure Import;
  end;

  procedure ShowImport(DbName : string; DepId: integer);

type
  // ��� �������
  TMounting = record
    Name : string;
    Q : integer;
    W : double;
    EdgShape : string; // ����� �������
    EdgT : string; // ��� �������
    Cleannes : string; // �������
    Chromaticity : string; // ���������
    Igroup : string; // ������
    Color : string; // ����
  end;

var
  CompId : Variant; // Id � ����������� �����������
  DepName : Variant; // ������� ������������ ������
  DocId : integer; // Id ������������ ���������

implementation

uses dbUtil, {RegExpr,} Variants, RxStrUtils, Math, MsgDialog, ImportCDM_Auto,
     fmUtils, UtilLib;

resourcestring
  rcOrganizationNotFound = '������������� ��� �� ������ � ����������� �����������. ��� %s';
  rcDepSNameIsNull = '�� ������ ������� ������������ ������. ��� ������ %s';
  rsSelectNDS = '�������� ������ ���';
  rsSelectSup = '�������� ����������';
  rsSelectPayType = '�������� ��� ������';

{$R *.DFM}

procedure ShowImport(DbName : string; DepId: integer);
var
  fmSInvImp: TfmSInvImp;
  r : Variant;
  SN:VAriant;
begin
  fmSInvImp := TfmSInvImp.Create(nil);
  try
    fmSInvImp.db.Close;
    fmSInvImp.db.DBName := DbName;
    // ������� ��
    try
      fmSInvImp.db.Open;
    except
      on E:Exception do MessageDialog('������ ��� ����������� � ��'#13+E.Message, mtError, [mbOk], 0);
    end;
    // ��������� ���� �� ��� � ��, ��� ������ ����������� ���
    CompId := ExecSelectSQL('select D_CompId from D_Comp where INN="'+StrPas(INN)+'"', fmSInvImp.quTmp);
    if VarIsNull(CompId) then
    begin
      MessageDialog(Format(rcOrganizationNotFound, [INN]), mtError, [mbOk], 0);
      eXit;
    end;
    // �������� ������������ DepId �� ��, ��� ������ ������������ DepId
    DepName := ExecSelectSQL('select SName from D_Dep where D_DepId='+IntToStr(DepId), fmSINvImp.quTmp);
    if VarIsNull(DepName) then
    begin
      DepName := '';
      MessageDialog(Format(rcDepSNameIsNull, [IntToStr(DepId)]), mtWarning, [mbOk], 0);
    end;
    fmSInvImp.lbDep.Caption := DepName;
    // �������� ������� ��� ��������� �������������
    r := ExecSelectSQL('select Margin from D_Dep where D_DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if VarIsNull(r) then
    begin
      fmSInvImp.FMargin := 0;
      fmSInvImp.laMargin.Font.Color:=clRed;
    end
    else begin
      fmSInvImp.FMargin := r;
      fmSInvImp.laMargin.Caption := FloatToStr(r)+'%';
      fmSInvImp.laMargin.Font.Color:=clBlack;
    end;
    fmSInvImp.laMargin.Caption:=FloatToStr(fmSInvImp.FMargin)+'%';
    OpenDataSets([fmSInvImp.taSup, fmSInvImp.taNDS, fmSInvImp.taPayType]);

    // �������� ������ � ������� ���������
    DocId := -1;
    fmSInvImp.taSList.Open;
    fmSInvImp.taSList.Append;
    DocId := ExecSelectSQL('select NEWID from GETID(8)', fmSInvImp.quTmp);
    fmSInvImp.taSListSInvId.AsInteger:= DocId;
    fmSInvImp.taSListDepId.AsInteger := DepId;
    fmSInvImp.taSListSDate.AsDateTime:= ExecSelectSQL('SELECT CAST(''NOW'' AS timestamp) from rdb$database', fmSInvImp.quTmp);
    fmSInvImp.taSListNDate.AsVariant := Null;
    fmSInvImp.taSListNDS.AsFloat:=0;
    fmSInvImp.taSListTr.AsFloat:=0;
    fmSInvImp.taSListPMargin.AsFloat:=0;
    fmSInvImp.taSListAkciz.AsFloat:=0;
    fmSInvImp.taSListIsClosed.AsInteger:=0;
    fmSInvImp.taSListPTr.AsFloat:=0;
    fmSInvImp.taSListNotPr.AsInteger:=0;
    fmSInvImp.taSListITYPE.AsInteger:=1;
    SN:= ExecSelectSQL('SELECT max(SN)+1 FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR("TODAY") AND DepId='+IntToStr(DepId), fmSInvImp.quTmp);
    if (SN=null)or(VarIsEmpty(SN)) then SN:=1;
    fmSInvImp.taSListSN.AsInteger :=SN;
    fmSInvImp.taSListPTrNds.AsFloat:=0;
    try
      fmSInvImp.taSList.Post;
    except
      on E:Exception do begin
         MessageDialog('��� ���������� ����� ��������� ��������� ������:'#13+E.Message, mtError, [mbOk], 0);
         eXit;
      end;
    end;
    fmSInvImp.ActiveControl:=fmSInvImp.fledImp;
    fmSInvImp.ShowModal;

  finally
    if fmSInvImp.tr.Active then fmSInvImp.tr.Commit;
    if fmSInvImp.db.Connected then fmSInvImp.db.Close;
    fmSInvImp.Free;
  end;
end;

procedure TfmSInvImp.btnImpClick(Sender: TObject);
begin
  // ��������� ������ ���
  if VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsSelectNDS);
  end;
  // ��������� ������ ���������
  if VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsSelectSup);
  end;
  // ��������� ������ ��� ������
  if VarIsNull(lcPayType.KeyValue) then
  begin
    ActiveControl := lcPayType;
    raise EWarning.Create(rsSelectPayType);
  end;
  taSList.Edit;
  taSListISIMPORT.AsInteger := 1;
  taSList.Post;
  // ������ ������
  Import;
  if ListBox1.Items.Text='' then ListBox1.Items.Text:='��������� ��������������� �������!';
  btnClose.SetFocus;
end;

procedure TfmSInvImp.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taSup, taNDS, taPayType]);
  tr.Commit;
end;

procedure TfmSInvImp.Import;
var
  ftext:textfile;
  EndMarker, UID_Sup, i,k : integer;
  Art2Id, UnitId : integer;
  Tmp, Tmp2, Art, Art2, Good, Mat, Sz ,Un,
  MainIns,MIns, tmpIns, Ins1: string;
  Price, W : double;
  
  procedure AddLog(Msg : string);
  begin
     ListBox1.Items.Add(Msg);
     ListBox1.Refresh;
  end;

  function FindId (supname:string; fsup, ftable:integer):string;
  var
    r :Variant;
  begin
    r := ExecSelectSQL('select first 1 SNAME from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
                   ' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable)+' order by SNAME', quTmp);
   if UID_Sup=495337 then
   ShowMessage('select first 1 SNAME from D_SNAMESUP where SNAME_SUP like '#39+supname+'%'#39+
    ' and fsup='+IntToStr(fsup)+' and ftable='+IntToStr(ftable));
    if r=null then Result:='-1' else Result:= VarToStr(r);
  end;



var
  Mounting : TMounting;
begin
  // �������� ���
  ListBox1.Clear; //**************
  // ��������� ���������� �� ���� � �������
  if not FileExists(fledImp.FileName) then
  begin
    ListBox1.Items.Add('���� '+fledImp.FileName+' �� ����������!');
    eXit;
  end;
  Screen.Cursor := crHourGlass;
  AssignFile(ftext, fledImp.FileName);
  try
    Reset(ftext);
    for i:=1 to 6 do ReadLn(ftext);
    // ������ ������� ������� � ����� - �� 7 ������ ������� ���-�� ������� � ���������
    ReadLn(ftext, EndMarker);
    prbrImp.Min := 0;
    prbrImp.Max := EndMarker;
    //ShowMessage('���������� ������ - '+IntToStr(EndMarker));
    prbrImp.Step := 1;

    DecimalSeparator:='.';
    i:=0;
    // �������� ���� �� ��������
    repeat
      inc(i);
      ReadLn(ftext,UID_Sup,Tmp); Tmp:=ReplaceStr(Tmp, ',', '.');
      Art := ExtractWord(1, Tmp, [' ']);
      Art2 := ExtractWord(2, Tmp, [' ']);
      Sz := ExtractWord(3, Tmp, [' ']);
      W := ConvertStrToFloat(ExtractWord(4, Tmp, [' ']));
      Un := ansilowercase(ExtractWord(5, Tmp, [' ']));
      UnitId:=integer(pos('��',Un)>0);
      Price:=ConvertStrToFloat(ExtractWord(6, Tmp, [' ']));

      // �������� �������� �������
      MainIns:=ExtractWord(7, Tmp, [' ']);
      tmpIns:= trim(FindId(MainIns, 1, 1));
      if UID_Sup = 660700 then
        ShowMessage('MainIns = #'+MainIns+'# tmpIns = #'+tmpIns+'#');
      if(tmpIns = '-1')then
      begin
        AddLog('�� ������� ���������� ������� '+MainIns+' � '+IntToStr(UID_Sup));
        prbrImp.StepIt;
        continue;
      end
      else MainIns := tmpIns;

      // �������� �����
      Good:= AnsiLowerCase(ExtractWord(8, Tmp, [' ']));
      Ins1:= FindId(Good,1, 0);
      if(Ins1 = '-1')then
      begin
        AddLog('�� ������� ���������� ����� '+Good+' � '+IntToStr(UID_Sup));
        continue;
      end
      else Good:=Ins1;
        // �������� ��������
      Mat := AnsiLowerCase(ExtractWord(9, Tmp, [' ']));
      MIns := Trim(MainIns);
      //����� ��� ���������� ��� � ��� ��
       //����
      if MIns='�' then mat:=mat+mins;
       //��������
      //if (MIns='�') or (art[3] = '7') then mat:=mat+'�';

      Ins1:= FindId(Mat,1, 2);
      if Ins1='-1' then
      begin
        AddLog('�� ������� ���������� �������� '+Mat+' � '+inttostr(UID_Sup));
        prbrImp.StepIt;
        continue;
      end
      else Mat:=Ins1;
      // ������� ������� � ���������
      quTmp.Close;
      quTmp.SQL.Text := 'execute procedure InsFItem(:ART, :ART2, :GOOD, :MAT, :COUNTRY,'+
        ':UNITID, :COMPID, :SUPID, :MAININS, :W, :SZ, :PRICE, :SINVID, :DEPID, :PNDS, :NDSID,'+
        ':MARGIN, :SSF, :SDATE, :SN, :NDATE, :UID_SUP)';
      quTmp.Prepare;
      quTmp.ParamByName('ART').AsString := Art;
      quTmp.ParamByName('ART2').AsString := Art2;
      quTmp.ParamByName('GOOD').AsString := Good;
      quTmp.ParamByName('MAT').AsString := Mat;
      quTmp.ParamByName('COUNTRY').AsString := '��';  { TODO : ���������� � ���� ��� ����� ������ }
      quTmp.ParamByName('UNITID').AsInteger := UnitId;
      quTmp.ParamByName('COMPID').AsInteger := CompId; // ���
      quTmp.ParamByName('SUPID').AsInteger := taSListSUPID.AsInteger;
      quTmp.ParamByName('MAININS').AsString := MainIns;
      quTmp.ParamByName('W').AsDouble := W;
      quTmp.ParamByName('SZ').AsString := Sz;
      quTmp.ParamByName('PRICE').AsDouble := Price;
      quTmp.ParamByName('SINVID').AsInteger := taSListSINVID.AsInteger;
      quTmp.ParamByName('DEPID').AsInteger := taSListDepId.AsInteger;
      quTmp.ParamByName('PNDS').AsInteger := 0;
      quTmp.ParamByName('NDSID').AsInteger := taSListNDSID.AsInteger;
      quTmp.ParamByName('MARGIN').AsDouble := FMargin;
      quTmp.ParamByName('SSF').AsString := taSListSSF.AsString;
      quTmp.ParamByName('SDATE').AsDateTime := taSListSDATE.AsDateTime;
      quTmp.ParamByName('SN').AsInteger := taSListSN.AsInteger;
      quTmp.ParamByName('NDATE').AsDateTime := taSListNDATE.AsDateTime;
      quTmp.ParamByName('UID_SUP').AsInteger := UID_Sup;
      try
        quTmp.ExecQuery;
        tr.CommitRetaining;
        Art2Id := quTmp.Fields[0].AsInteger;
        quTmp.Close;
      except
        on E:Exception do
          begin
            AddLog('������ ��� ���������� ������� � ���.'+Art+' ('+IntToStr(i)+')'+' - '+e.Message);
            prbrImp.StepIt;
            continue;
          end
      end;
      // ����� ���������� � ���������

      // ������ �������� �������
      k:=1;
      Tmp2:=AnsiLowerCase(ExtractWord(k+1, Tmp, [';']));
      while (Tmp2<>'') do
      begin
        if(k=1)then
        begin
          quTmp.Close;
          quTmp.SQL.Text := 'delete from Ins where Art2Id='+IntToStr(Art2Id);
          quTmp.Prepare;
          quTmp.ExecQuery;
          quTmp.Transaction.CommitRetaining;
        end;
        Mounting.Name := ExtractWord(1, Tmp2, [' ']);
        Mounting.Q := StrToIntDef( ExtractWord(2, Tmp2, [' ']), 1);
        Mounting.W := ConvertStrToFloat(ExtractWord(3, Tmp2, [' ']));
        Mounting.EdgShape := ExtractWord(4, Tmp2, [' ']);
        Mounting.EdgT := ExtractWord(5, Tmp2, [' ']);
        Mounting.Cleannes := ExtractWord(6, Tmp2, [' ']);
        Mounting.Chromaticity := ExtractWord(7, Tmp2, [' ']);
        Mounting.Igroup := ExtractWord(8, Tmp2, [' ']);
        Mounting.Color := ExtractWord(9, Tmp2, [' ']);

        Ins1 := FindId(Mounting.Name,1,1);
        if(Ins1='-1')then
        begin
          AddLog('�� ������� ���������� ������������ �������. ���='+Art+', UID='+IntToStr(UID_Sup));
          inc(k);
          Tmp2 := AnsiLowerCase(ExtractWord(k+1, Tmp, [';']));
          prbrImp.StepIt;
          continue;
        end;
        quTmp.Close;
        quTmp.SQL.Text := 'execute procedure AddIns_Pr2(:ART2ID, :INSID, :Q, :W, :COLOR,'+
              ':CHROMATICITY, :CLEANNES, :EDGTYPE, :EDGSHAPE, :IGROUP)';
        quTmp.Prepare;
        quTmp.ParamByName('ART2ID').AsInteger := Art2Id;
        quTmp.ParamByName('INSID').AsString := Ins1;
        quTmp.ParamByName('Q').AsInteger := Mounting.Q;
        quTmp.ParamByName('W').AsDouble := Mounting.W;

        if Mounting.Color='-' then quTmp.ParamByName('COLOR').AsVariant := Null
        else quTmp.ParamByName('COLOR').AsString := Mounting.Color;

        if (Mounting.Chromaticity='-') then quTmp.ParamByName('CHROMATICITY').AsVariant := Null
        else quTmp.ParamByName('CHROMATICITY').AsString := Mounting.Chromaticity;

        if (Mounting.Cleannes='-') then quTmp.ParamByName('CLEANNES').AsVariant := Null
        else quTmp.ParamByName('CLEANNES').AsString := Mounting.Cleannes;

        if (Mounting.EdgT='-') then quTmp.ParamByName('EDGTYPE').AsVariant := Null
        else quTmp.ParamByName('EDGTYPE').AsString := Mounting.EdgT;

        if (Mounting.EdgShape='-') then quTmp.ParamByName('EDGSHAPE').AsVariant := Null
        else quTmp.ParamByName('EDGSHAPE').AsString := Mounting.EdgShape;

        quTmp.ExecQuery;
        tr.CommitRetaining;
        inc(k);
        Tmp2:=AnsiLowerCase(ExtractWord(k+1, Tmp, [';']));
      end;
      if(k<>1)then ExecSQL('execute procedure SetMainIns('+IntToStr(Art2Id)+',"�")', quTmp);
      prbrImp.StepIt;
    until i>=EndMarker;
  finally
    CloseFile(fText);
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmSInvImp.fledImpAfterDialog(Sender: TObject; var Name: String; var Action: Boolean);
var
  ftext: textfile;
  DocNo, DocDate, SupINN : string;
  r : Variant;
begin
  if not FileExists(Name) then eXit;
  AssignFile(ftext, Name);
  try
    Reset(ftext);
    ReadLn(ftext, DocNo);
    ReadLn(ftext, DocDate);
    ReadLn(ftext);
    ReadLn(ftext, SupINN);
    try
      taSList.Edit;
      taSListSSF.AsString := DocNo;
      taSListNDATE.AsDateTime := StrToDateTime(DocDate);

      if(SupINN <> '')then
      begin
        r := ExecSelectSQL('select D_CompId from D_Comp where INN="'+SupINN+'"', quTmp);
        if not VarIsNull(r) then taSListSUPID.AsInteger := r;
      end;     
      taSList.Post;
       // ������������
       taNDS.first;
       taPayType.first;

        while taNDS.Fieldbyname('Name').AsString <>'0%' do
              taNDS.next;

       lcNDS.KeyValue := taNDS.Fieldbyname('Ndsid').AsString;

       while taPayType.Fieldbyname('Name').AsString <>'�� �����' do
              taPayType.next;

       lcPayType.KeyValue := taPayType.Fieldbyname('PayTypeid').AsString;
    except
    end;
  finally
    CloseFile(ftext);
  end;
end;

procedure TfmSInvImp.taNDSAfterOpen(DataSet: TDataSet);
begin
taNDS.first;
 
end;

procedure TfmSInvImp.taPayTypeAfterOpen(DataSet: TDataSet);
begin
taPayType.first;

end;

procedure TfmSInvImp.taSListBeforeOpen(DataSet: TDataSet);
begin
  taSList.ParamByName('DOCID').AsInteger := DocId;
end;

procedure TfmSInvImp.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmSInvImp.taSupCalcFields(DataSet: TDataSet);
begin
  taSupFName.AsString:=taSupSName.AsString+' - '+taSupName.AsString;
end;

end.




























