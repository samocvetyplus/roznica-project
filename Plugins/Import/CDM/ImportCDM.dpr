library ImportCDM;

uses
  {ExceptionLog,}
  ComServ,
  SInvImp in 'SInvImp.pas' {fmSInvImp},
  ImportCDM_Auto in 'ImportCDM_Auto.pas',
  ImportCDM_TLB in 'ImportCDM_TLB.pas';

const
  ClassName : PChar = 'JewImport.CDM';
  Version : PChar = '1.1';
  PluginName : PChar = '������ ��������� ���';
  PluginGroup : PChar = 'JewImport';
  Description : PChar = '������ ��������� �� ������������� ���. ��� 3123026420';

{$R *.TLB}

{$R *.RES}

function GetClassName : PChar;
begin
  Result := ClassName;
end;

function GetVersion : PChar;
begin
  Result := Version;
end;

function GetPluginGroup : PChar;
begin
  Result := PluginGroup;
end;

function GetDescription : PChar;
begin
  Result := Description;
end;

function GetName : PChar;
begin
  Result := PluginName;
end;

exports
  GetClassName,
  GetVersion,
  GetPluginGroup,
  GetDescription,
  GetName,
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

begin
end.
 