unit JewImport_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 31.01.2011 12:43:20 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Documents and Settings\�������������\��� ���������\��� �������\Work\�������\Plugins\Import\CDM\ImportCDM.tlb (1)
// LIBID: {A5B45248-61D3-4515-B964-D6F7DBCFC01D}
// LCID: 0
// Helpfile: 
// HelpString: ������ ��������� ���
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportCDMMajorVersion = 1;
  ImportCDMMinorVersion = 0;

  LIBID_ImportCDM: TGUID = '{A5B45248-61D3-4515-B964-D6F7DBCFC01D}';

  IID_ICDM: TGUID = '{0432983F-BEFB-435F-B9F2-B6E82D28815A}';
  CLASS_CDM: TGUID = '{D380F557-DEEC-4882-9D5D-CE006CE5818D}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ICDM = interface;
  ICDMDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  CDM = ICDM;


// *********************************************************************//
// Interface: ICDM
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0432983F-BEFB-435F-B9F2-B6E82D28815A}
// *********************************************************************//
  ICDM = interface(IDispatch)
    ['{0432983F-BEFB-435F-B9F2-B6E82D28815A}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  ICDMDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0432983F-BEFB-435F-B9F2-B6E82D28815A}
// *********************************************************************//
  ICDMDisp = dispinterface
    ['{0432983F-BEFB-435F-B9F2-B6E82D28815A}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 1;
    procedure SetAppHandle(AppHandle: Integer); dispid 2;
  end;

// *********************************************************************//
// The Class CoCDM provides a Create and CreateRemote method to          
// create instances of the default interface ICDM exposed by              
// the CoClass CDM. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoCDM = class
    class function Create: ICDM;
    class function CreateRemote(const MachineName: string): ICDM;
  end;

implementation

uses ComObj;

class function CoCDM.Create: ICDM;
begin
  Result := CreateComObject(CLASS_CDM) as ICDM;
end;

class function CoCDM.CreateRemote(const MachineName: string): ICDM;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_CDM) as ICDM;
end;

end.
