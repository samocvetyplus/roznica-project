unit ImportDiamant_Auto;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Controls, ComObj, ActiveX, AxCtrls, Classes, ImportDIAMANT_TLB, Contnrs, StdVcl;

 const
  INN : PChar = '4415003912/441501001';

type
  TDIAMANT = class(TAutoObject, IDIAMANT)
   FAppHandle : integer;
  private
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;

  end;

implementation

uses ComServ, SInvImp, Forms;

procedure  TDIAMANT.Import(const DbName: WideString; DepId: Integer);
begin
  Application.Handle := FAppHandle;
  ShowImport(DbName, DepId);
  Application.Handle := 0;
end;

procedure  TDIAMANT.SetAppHandle(AppHandle: Integer);
begin
 FAppHandle := AppHandle;
end;

initialization
  TAutoObjectFactory.Create(ComServer,  TDIAMANT, Class_DIAMANT, ciMultiInstance, tmApartment);
end.
