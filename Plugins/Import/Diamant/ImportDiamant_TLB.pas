unit ImportDiamant_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// $Rev: 5081 $
// File generated on 27.05.2014 14:51:21 from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\work\�������\Plugins\Import\Diamant\ImportDiamant.tlb (1)
// LIBID: {BE993487-2014-416D-94F3-5ADAA0C6722B}
// LCID: 0
// Helpfile: 
// HelpString: ImportDiamant Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ImportDiamantMajorVersion = 1;
  ImportDiamantMinorVersion = 0;

  LIBID_ImportDiamant: TGUID = '{BE993487-2014-416D-94F3-5ADAA0C6722B}';

  IID_IDIAMANT: TGUID = '{0164C00D-566E-4C8C-94D5-C31E249B6E6F}';
  CLASS_DIAMANT: TGUID = '{EED856BD-2BE4-4D0D-BA01-D424694FA5C2}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IDIAMANT = interface;
  IDIAMANTDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  DIAMANT = IDIAMANT;


// *********************************************************************//
// Interface: IDIAMANT
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0164C00D-566E-4C8C-94D5-C31E249B6E6F}
// *********************************************************************//
  IDIAMANT = interface(IDispatch)
    ['{0164C00D-566E-4C8C-94D5-C31E249B6E6F}']
    procedure Import(const DbName: WideString; DepId: Integer); safecall;
    procedure SetAppHandle(AppHandle: Integer); safecall;
  end;

// *********************************************************************//
// DispIntf:  IDIAMANTDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0164C00D-566E-4C8C-94D5-C31E249B6E6F}
// *********************************************************************//
  IDIAMANTDisp = dispinterface
    ['{0164C00D-566E-4C8C-94D5-C31E249B6E6F}']
    procedure Import(const DbName: WideString; DepId: Integer); dispid 201;
    procedure SetAppHandle(AppHandle: Integer); dispid 202;
  end;

// *********************************************************************//
// The Class CoDIAMANT provides a Create and CreateRemote method to          
// create instances of the default interface IDIAMANT exposed by              
// the CoClass DIAMANT. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoDIAMANT = class
    class function Create: IDIAMANT;
    class function CreateRemote(const MachineName: string): IDIAMANT;
  end;

implementation

uses ComObj;

class function CoDIAMANT.Create: IDIAMANT;
begin
  Result := CreateComObject(CLASS_DIAMANT) as IDIAMANT;
end;

class function CoDIAMANT.CreateRemote(const MachineName: string): IDIAMANT;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_DIAMANT) as IDIAMANT;
end;

end.
