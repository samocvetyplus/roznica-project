unit RepairList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, ExtCtrls, PrnDbgeh,
  ActnList, Printers, PrntsEh, M207Ctrls, Menus,
  TB2Item, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmRepairList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    siHelp: TSpeedItem;
    siDel: TSpeedItem;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siPeriod: TSpeedItem;
    dg1: TDBGridEh;
    prdg1: TPrintDBGridEh;
    acList: TActionList;
    acPrintGrid: TAction;
    fr1: TM207FormStorage;
    acClose: TAction;
    acHelp: TAction;
    LDepFrom: TLabel;
    siAdd: TSpeedItem;
    acAdd: TAction;
    acDel: TAction;
    siView: TSpeedItem;
    acView: TAction;
    siRepair: TSpeedItem;
    acRepair: TAction;
    acPrint: TAction;
    siMoving: TSpeedItem;
    acMoving: TAction;
    pmmoving: TTBPopupMenu;
    acEditDate: TAction;
    bieditDate: TTBItem;
    SpeedItem1: TSpeedItem;
    procedure acPrintGridExecute(Sender: TObject);
    procedure acPrintGridUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acRepairUpdate(Sender: TObject);
    procedure acRepairExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure acMovingExecute(Sender: TObject);
    procedure acEditDateExecute(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
   procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  fmRepairList: TfmRepairList;

implementation
uses comdata, data3, data, M207Proc, Period, Repair, dbUtil, reportdata, DB,
  pFIBQuery, FIBQuery, getdata, MsgDialog;

{$R *.dfm}

procedure TfmRepairList.ShowPeriod;
begin
 laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmRepairList.acPrintGridExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 prdg1.Print;
end;

procedure TfmRepairList.acPrintGridUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=not dm3.taRepairListSINVID.IsNull
end;

procedure TfmRepairList.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  dmCom.taRec.Active:=True;

  dm.SetBeginDate;
  ShowPeriod;

  dm.DDepFromId:= CenterDepId;
  dm.DDepFrom:= SelfDepName;
  laDepFrom.Caption:=dm.DDepFrom;
  OpenDataSets([dm3.taRepairList]);
end;

procedure TfmRepairList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm3.taRepairList, dmCom.taRec]);
 dmCom.tr.CommitRetaining;
end;

procedure TfmRepairList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRepairList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRepairList.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmRepairList.siPeriodClick(Sender: TObject);
begin
 if GetPeriod(dm.BeginDate, dm.EndDate) then
 begin
  ReOpenDataSets([dm3.taRepairList]);
  ShowPeriod;
 end;
end;

procedure TfmRepairList.acAddUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep;
end;

procedure TfmRepairList.acAddExecute(Sender: TObject);
begin
 dm3.taRepairList.Append;
 dm3.taRepairList.Post;
 ShowAndFreeForm(TfmRepair, Self, TForm(fmRepair), True, False);
 dm3.taRepairList.Refresh;
 dg1.SumList.RecalcAll;
end;

procedure TfmRepairList.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRepairListSINVID.IsNull) and
   (dm3.taRepairListISCLOSED.AsInteger=0)
end;

procedure TfmRepairList.acDelExecute(Sender: TObject);
begin
 dm3.taRepairList.Delete;
end;

procedure TfmRepairList.acViewUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRepairListSINVID.IsNull);
end;

procedure TfmRepairList.acViewExecute(Sender: TObject);
begin
 dm3.taRepairList.Refresh;
 if dm3.taRepairListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSet(dm3.taRepairList);
 end else begin
  if dm3.taRepairListISCLOSED.AsInteger<>0 then MessageDialog('��������� �������', mtWarning, [mbOk], 0);
  ShowAndFreeForm(TfmRepair, Self, TForm(fmRepair), True, False);
  dm3.taRepairList.Refresh;
  dg1.SumList.RecalcAll;
 end  
end;

procedure TfmRepairList.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  case dm3.taRepairListISCLOSED.AsInteger of
   0: Background:=clInfoBk;
   1: Background:=clBtnFace;
   2: Background:=$00FFFF80
  end;
 end
end;

procedure TfmRepairList.acRepairUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRepairListSINVID.IsNull) and
  (dm3.taRepairListISCLOSED.AsInteger>=1);
 if TAction(Sender).Enabled then begin
  if dm3.taRepairListISCLOSED.AsInteger=1 then TAction(Sender).Caption:='������'
  else TAction(Sender).Caption:='���. ������'
 end
end;

procedure TfmRepairList.acRepairExecute(Sender: TObject);
var i:integer;
begin
 if dm3.taRepairListISCLOSED.AsInteger=1 then
  ExecSQL('update sinv set isclosed=2 where sinvid='+dm3.taRepairListSINVID.AsString, dm.quTmp)
 else begin
  with dm, qutmp do begin
   close;
   sql.Text:='select ffind from Find_uid_repair ('+dm3.taRepairListSINVID.AsString+')';
   ExecQuery;
   i:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
  end;
  if i=0 then
   ExecSQL('update sinv set isclosed=1 where sinvid='+dm3.taRepairListSINVID.AsString, dm.quTmp)
  else MessageDialog('������� �� ��������� ��������� ��� �� �������, ��� � �������� ��������� �������', mtWarning, [mbOk], 0);
 end;
 dm3.taRepairList.Refresh;   
end;

procedure TfmRepairList.acPrintExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
 try
  arr:=gen_arr(dg1, dm3.taRepairListSINVID);
  PrintDocument(arr, repair_uid);
 finally
   Finalize(arr);
 end;
end;

procedure TfmRepairList.acPrintUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= not dm3.taRepairListSINVID.IsNull;
end;

procedure TfmRepairList.acMovingExecute(Sender: TObject);
begin
//
end;

procedure TfmRepairList.acEditDateExecute(Sender: TObject);
var
  MornthDate:TDateTime;
  yy,mm,dd,h,m,s,ms:word;
begin
 mornthDate:=dmCom.GetServerTime;
 if Getdate(mornthDate) then
 begin
  DecodeDate(mornthDate,yy,mm,dd);
  DecodeTime(dmCom.GetServerTime,h,m,s,ms);
  mornthDate:=(EncodeDate(yy,mm,dd)+EncodeTime(h,m,s,ms));
  ExecSQL('update sinv set sdate='#39+datetimetostr(mornthDate)+#39' where itype=9 and isclosed=0', dm.quTmp);
  ReOpenDataSets([dm3.taRepairList]);
 end
end;

procedure TfmRepairList.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 CASE key of
  VK_RETURN, VK_SPACE: acViewExecute(nil);
 end;
end;

procedure TfmRepairList.SpeedItem1Click(Sender: TObject);
begin
with dm3, dmCom do
  begin
  PostDataSet(taRepairList);
  tr.CommitRetaining;
  ReOpenDataSet(taRepairList);
  taRepairList.Refresh;
  end;
end;

end.
