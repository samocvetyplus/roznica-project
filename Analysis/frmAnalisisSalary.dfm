object AnalisisSalary: TAnalisisSalary
  Left = 266
  Top = 148
  Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1086#1076#1072#1078
  ClientHeight = 657
  ClientWidth = 1155
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridOffice: TcxGrid
    AlignWithMargins = True
    Left = 4
    Top = 91
    Width = 1147
    Height = 318
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = alTop
    TabOrder = 0
    object GridOfficeView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourceOffice
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0'
          Kind = skSum
          FieldName = 'B$Q'
          Column = GridOfficeViewBQ
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'B$C'
          Column = GridOfficeViewBC
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'A$Q'
          Column = GridOfficeViewAQ
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'A$C'
          Column = GridOfficeViewAC
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'C$Q'
          Column = GridOfficeViewCQ
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'C$C'
          Column = GridOfficeViewCC
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'D$Q'
          Column = GridOfficeViewDQ
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'D$C'
          Column = GridOfficeViewDC
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'E$Q'
          Column = GridOfficeViewEQ
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'E$C'
          Column = GridOfficeViewEC
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.BandHeaderLineCount = 2
      Bands = <
        item
          Caption = #1055#1077#1088#1080#1086#1076
        end
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          Position.BandIndex = 0
          Position.ColIndex = 0
        end
        item
          Caption = #1055#1088#1086#1076#1072#1078#1080'?('#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103')'
          Position.BandIndex = 0
          Position.ColIndex = 1
          Width = 150
        end
        item
          Caption = #1055#1088#1086#1076#1072#1078#1080'?('#1088#1077#1072#1083#1080#1079#1072#1094#1080#1103')'
          Position.BandIndex = 0
          Position.ColIndex = 2
          Width = 150
        end
        item
          Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
          Position.BandIndex = 0
          Position.ColIndex = 3
          Width = 150
        end
        item
          Caption = #1058#1088#1077#1073#1091#1077#1084#1099#1081' '#1087#1086#1082#1072#1079#1072#1090#1077#1083#1100'?('#1087#1086' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1091')'
          Position.BandIndex = 0
          Position.ColIndex = 4
          Width = 150
        end
        item
          Caption = #1057#1088#1072#1074#1085#1077#1085#1080#1077' '#1090#1077#1082#1091#1097#1080#1093'?'#1087#1086#1082#1072#1079#1072#1090#1077#1083#1077#1081' '#1089' '#1090#1088#1077#1073#1091#1077#1084#1099#1084#1080
          Position.BandIndex = 0
          Position.ColIndex = 5
          Width = 150
        end
        item
          Caption = #1057#1088#1077#1076#1085#1077#1077' '#1090#1088#1077#1073#1091#1077#1084#1086#1077'?'#1079#1085#1072#1095#1077#1085#1080#1077' '#1074' '#1076#1077#1085#1100
          Position.BandIndex = 0
          Position.ColIndex = 6
          Width = 150
        end>
      OnCustomDrawBandHeader = GridViewCustomDrawBandHeader
      object GridOfficeViewNAME: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OFFICE$NAME'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
        IsCaptionAssigned = True
      end
      object GridOfficeViewBQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'B$Q'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewBC: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'B$C'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridOfficeViewAQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'A$Q'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewAC: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'A$C'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridOfficeViewKQ: TcxGridDBBandedColumn
        Caption = #1050'. '#1082#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'K$Q'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewKC: TcxGridDBBandedColumn
        Caption = #1050'. '#1074#1099#1088#1091#1095#1082#1072
        DataBinding.FieldName = 'K$C'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 4
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridOfficeViewCQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'C$Q'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewCC: TcxGridDBBandedColumn
        Caption = #1042#1099#1088#1091#1095#1082#1072
        DataBinding.FieldName = 'C$C'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 5
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridOfficeViewDQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'D$Q'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Styles.OnGetContentStyle = GridOfficeViewDStylesGetContentStyle
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewDC: TcxGridDBBandedColumn
        Caption = #1042#1099#1088#1091#1095#1082#1072
        DataBinding.FieldName = 'D$C'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Styles.OnGetContentStyle = GridOfficeViewDStylesGetContentStyle
        Position.BandIndex = 6
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridOfficeViewEQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'E$Q'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewEC: TcxGridDBBandedColumn
        Caption = #1042#1099#1088#1091#1095#1082#1072
        DataBinding.FieldName = 'E$C'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 7
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
    end
    object GridOfficeLevel: TcxGridLevel
      GridView = GridOfficeView
    end
  end
  object GridPerson: TcxGrid
    Left = 0
    Top = 417
    Width = 1155
    Height = 240
    Align = alClient
    TabOrder = 1
    object GridPersonView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourcePerson
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0'
          Kind = skSum
          FieldName = 'Q$BEFORE'
          Column = GridPersonViewQBEFORE
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'C$BEFORE'
          Column = GridPersonViewCBEFORE
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'Q$MIDDLE'
          Column = GridPersonViewQMIDDLE
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'C$MIDDLE'
          Column = GridPersonViewCMIDDLE
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'Q$AFTER'
          Column = GridPersonViewQAFTER
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'C$AFTER'
          Column = GridPersonViewCAFTER
        end
        item
          Format = '# ##0.##'
          Kind = skSum
          Column = GridPersonViewTOTALPERSONQ
        end
        item
          Format = '### ### ##0.00'
          Kind = skSum
          Column = GridPersonViewTOTALPERSONC
        end>
      DataController.Summary.SummaryGroups = <
        item
          Links = <
            item
              Column = GridPersonViewOFFICENAME
            end>
          SummaryItems = <
            item
              Format = '0'
              Kind = skSum
              Position = spFooter
              FieldName = 'Q$BEFORE'
              Column = GridPersonViewQBEFORE
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'C$BEFORE'
              Column = GridPersonViewCBEFORE
            end
            item
              Format = '0'
              Kind = skSum
              Position = spFooter
              FieldName = 'Q$MIDDLE'
              Column = GridPersonViewQMIDDLE
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'C$MIDDLE'
              Column = GridPersonViewCMIDDLE
            end
            item
              Format = '0'
              Kind = skSum
              Position = spFooter
              FieldName = 'Q$AFTER'
              Column = GridPersonViewQAFTER
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'C$AFTER'
              Column = GridPersonViewCAFTER
            end
            item
              Format = '0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'TOTAL$PERSON$C'
              Column = GridPersonViewTOTALPERSONC
            end
            item
              Kind = skSum
              Position = spFooter
              FieldName = 'TOTAL$PERSON$Q'
              Column = GridPersonViewTOTALPERSONQ
            end>
        end>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.BandHeaderLineCount = 2
      Bands = <
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          Visible = False
        end
        item
          Caption = #1060'.'#1048'.'#1054'.'
        end
        item
          Caption = #1044#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1087#1083#1072#1085#1072
          Width = 173
        end
        item
          Caption = #1063#1072#1089#1090#1080#1095#1085#1086#1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1083#1072#1085#1072
          Width = 279
        end
        item
          Caption = #1055#1086#1089#1083#1077' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1087#1083#1072#1085#1072
          Width = 279
        end
        item
          Caption = #1048#1090#1086#1075#1086
          Width = 188
        end>
      OnCustomDrawBandHeader = GridViewCustomDrawBandHeader
      object GridPersonViewOFFICENAME: TcxGridDBBandedColumn
        Caption = #1052#1072#1075#1072#1079#1080#1085
        DataBinding.FieldName = 'OFFICE$NAME'
        Visible = False
        GroupIndex = 0
        HeaderAlignmentHorz = taCenter
        Options.Grouping = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridPersonViewPERSONNAME: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PERSON$NAME'
        HeaderAlignmentHorz = taCenter
        Width = 196
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
        IsCaptionAssigned = True
      end
      object GridPersonViewQBEFORE: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'Q$BEFORE'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridPersonViewCBEFORE: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'C$BEFORE'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridPersonViewFPARTIALTIMESTAMP: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'F$PARTIAL$TIMESTAMP'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Kind = ckDateTime
        HeaderAlignmentHorz = taCenter
        Width = 100
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridPersonViewQMIDDLE: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'Q$MIDDLE'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridPersonViewCMIDDLE: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'C$MIDDLE'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 3
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridPersonViewFFULLTIMESTAMP: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'F$FULL$TIMESTAMP'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Kind = ckDateTime
        HeaderAlignmentHorz = taCenter
        Width = 100
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridPersonViewQAFTER: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'Q$AFTER'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 4
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridPersonViewCAFTER: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'C$AFTER'
        HeaderAlignmentHorz = taCenter
        Width = 75
        Position.BandIndex = 4
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridPersonViewOFFICEQ: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OFFICE$Q'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridPersonViewOFFICEC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OFFICE$C'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridPersonViewSUMQ: TcxGridDBBandedColumn
        DataBinding.FieldName = 'SUM$Q'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 1
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridPersonViewSUMC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'SUM$C'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 1
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object GridPersonViewTOTALPERSONQ: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'Total$Person$Q'
        HeaderAlignmentHorz = taCenter
        Width = 88
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridPersonViewTOTALPERSONC: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        DataBinding.FieldName = 'TOTAL$PERSON$C'
        HeaderAlignmentHorz = taCenter
        Width = 100
        Position.BandIndex = 5
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
    end
    object GridPersonLevel: TcxGridLevel
      GridView = GridPersonView
    end
  end
  object BarDock: TdxBarDockControl
    AlignWithMargins = True
    Left = 4
    Top = 35
    Width = 1147
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = dalTop
    BarManager = BarManager
    ExplicitTop = 28
  end
  object Splitter: TcxSplitter
    Left = 0
    Top = 409
    Width = 1155
    Height = 8
    HotZoneClassName = 'TcxXPTaskBarStyle'
    AlignSplitter = salTop
    ResizeUpdate = True
    Control = GridOffice
  end
  object Ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 1155
    Height = 31
    ApplicationButton.Visible = False
    BarManager = BarManager
    ColorSchemeName = 'Blue'
    PopupMenuItems = []
    QuickAccessToolbar.Toolbar = BarAccess
    ShowTabGroups = False
    ShowTabHeaders = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 8
    TabStop = False
  end
  object SalaryOffice: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure salary$k$iu(:office$id, :k$q, :k$c)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      
        'select * from salary$office2(:a$office$id, :a$date$begin, :a$dat' +
        'e$end)')
    AfterPost = SalaryOfficeAfterPost
    BeforeOpen = SalaryBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    UpdateTransaction = DataBuy.TransactionWrite
    RefreshTransactionKind = tkUpdateTransaction
    Left = 16
    Top = 248
    oRefreshAfterPost = False
    oFetchAll = True
    object SalaryOfficeBQ: TFIBBCDField
      FieldName = 'B$Q'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeBC: TFIBBCDField
      FieldName = 'B$C'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeAQ: TFIBBCDField
      FieldName = 'A$Q'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeAC: TFIBBCDField
      FieldName = 'A$C'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeKQ: TFIBBCDField
      FieldName = 'K$Q'
      DisplayFormat = '# ##0.000'
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeKC: TFIBBCDField
      FieldName = 'K$C'
      DisplayFormat = '### ### ##0.000'
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeCQ: TFIBBCDField
      FieldName = 'C$Q'
      DisplayFormat = '# ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeCC: TFIBBCDField
      FieldName = 'C$C'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeDQ: TFIBBCDField
      FieldName = 'D$Q'
      DisplayFormat = '# ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeDC: TFIBBCDField
      FieldName = 'D$C'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeEQ: TFIBBCDField
      FieldName = 'E$Q'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeEC: TFIBBCDField
      FieldName = 'E$C'
      DisplayFormat = '### ### ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryOfficeOFFICEID: TFIBIntegerField
      FieldName = 'OFFICE$ID'
    end
    object SalaryOfficeOFFICENAME: TFIBStringField
      FieldName = 'OFFICE$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object SalaryOfficeDATEBEGIN: TFIBDateField
      FieldName = 'DATE$BEGIN'
    end
    object SalaryOfficeDATEEND: TFIBDateField
      FieldName = 'DATE$END'
    end
  end
  object SalaryPerson: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from salary$person2(:a$office$id, :a$date$begin, :a$date$end)')
    BeforeOpen = SalaryBeforeOpen
    OnCalcFields = SalaryPersonCalcFields
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    RefreshTransactionKind = tkUpdateTransaction
    Left = 80
    Top = 248
    oFetchAll = True
    object SalaryPersonOFFICENAME: TFIBStringField
      FieldName = 'OFFICE$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object SalaryPersonPERSONNAME: TFIBStringField
      FieldName = 'PERSON$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object SalaryPersonQBEFORE: TFIBBCDField
      FieldName = 'Q$BEFORE'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonCBEFORE: TFIBBCDField
      FieldName = 'C$BEFORE'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonFPARTIALTIMESTAMP: TFIBDateTimeField
      Alignment = taCenter
      FieldName = 'F$PARTIAL$TIMESTAMP'
      DisplayFormat = 'dd.mm.yyyy hh:nn:ss'
    end
    object SalaryPersonQMIDDLE: TFIBBCDField
      FieldName = 'Q$MIDDLE'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonCMIDDLE: TFIBBCDField
      FieldName = 'C$MIDDLE'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonFFULLTIMESTAMP: TFIBDateTimeField
      Alignment = taCenter
      FieldName = 'F$FULL$TIMESTAMP'
      DisplayFormat = 'dd.mm.yyyy hh:nn:ss'
    end
    object SalaryPersonQAFTER: TFIBBCDField
      FieldName = 'Q$AFTER'
      DisplayFormat = '# ##0.##'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonCAFTER: TFIBBCDField
      FieldName = 'C$AFTER'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonOFFICEQ: TFIBBCDField
      FieldName = 'OFFICE$Q'
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonOFFICEC: TFIBBCDField
      FieldName = 'OFFICE$C'
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonSUMQ: TFIBBCDField
      FieldName = 'SUM$Q'
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonSUMC: TFIBBCDField
      FieldName = 'SUM$C'
      Size = 4
      RoundByScale = True
    end
    object SalaryPersonTOTALPERSONQ: TBCDField
      FieldKind = fkCalculated
      FieldName = 'TOTAL$PERSON$Q'
      DisplayFormat = '# ##0.##'
      currency = True
      Calculated = True
    end
    object SalaryPersonTOTALPERSONC: TBCDField
      FieldKind = fkCalculated
      FieldName = 'TOTAL$PERSON$C'
      DisplayFormat = '### ### ##0.00'
      currency = True
      Calculated = True
    end
  end
  object SourceOffice: TDataSource
    DataSet = SalaryOffice
    Left = 48
    Top = 248
  end
  object SourcePerson: TDataSource
    DataSet = SalaryPerson
    Left = 112
    Top = 248
  end
  object Styles: TcxStyleRepository
    Left = 80
    Top = 216
    PixelsPerInch = 96
    object StyleRed: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clRed
    end
    object StyleDefault: TcxStyle
    end
  end
  object Action: TActionList
    Images = DataBuy.Image32
    Left = 16
    Top = 184
    object ActionRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = ActionRangeExecute
    end
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object ActionChart: TAction
      Caption = #1043#1088#1072#1092#1080#1082
      ImageIndex = 0
      OnExecute = ActionChartExecute
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Print'
      'Common'
      'Office'
      'Range'
      'Access')
    Categories.ItemsVisibles = (
      2
      2
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    ImageOptions.Images = DataBuy.Image16
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 216
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 64
      FloatClientHeight = 550
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOffice'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonRange'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarCoefButton'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarAccess: TdxBar
      Caption = 'Access'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1181
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 6
      ShortCut = 16464
      OnClick = ButtonPrintClick
      AutoGrayScale = False
      Width = 64
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActionChart
      Category = 0
      AutoGrayScale = False
      Width = 64
    end
    object dxBarCoefButton: TdxBarLargeButton
      Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      Category = 0
      Hint = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      Visible = ivAlways
      LargeImageIndex = 23
      OnClick = dxBarCoefButtonClick
      AutoGrayScale = False
    end
    object ButtonRange: TdxBarLargeButton
      Action = ActionRange
      Category = 1
      ShortCut = 16464
      AutoGrayScale = False
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Category = 1
      ShortCut = 16466
      AutoGrayScale = False
      Width = 64
    end
    object ButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Category = 1
      Hint = #1047#1072#1082#1088#1099#1090#1100
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = ButtonCloseClick
      AutoGrayScale = False
    end
    object ButtonOffice: TdxBarLargeButton
      Caption = #1052#1072#1075#1072#1079#1080#1085
      Category = 2
      Hint = #1052#1072#1075#1072#1079#1080#1085
      Visible = ivAlways
      DropDownMenu = MenuOffice
      LargeImageIndex = 9
      OnClick = ButtonOfficeClick
      AutoGrayScale = False
      Width = 64
      SyncImageIndex = False
      ImageIndex = -1
    end
    object ButtonRangeCustom: TdxBarButton
      Caption = 'Custom'
      Category = 3
      Hint = 'Custom'
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object ButtonRangeDay: TdxBarButton
      Caption = 'Day'
      Category = 3
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonRangeWeek: TdxBarButton
      Caption = 'Week'
      Category = 3
      Hint = 'Week'
      Visible = ivAlways
      LargeImageIndex = 13
    end
    object ButtonRangeMonth: TdxBarButton
      Caption = 'Month'
      Category = 3
      Hint = 'Month'
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object ButtonRangeYear: TdxBarButton
      Caption = 'Year'
      Category = 3
      Hint = 'Year'
      Visible = ivAlways
      LargeImageIndex = 15
    end
    object dxBarButton1: TdxBarButton
      Action = DataBuy.ActionFit
      Category = 4
    end
    object dxBarButton2: TdxBarButton
      Action = DataBuy.ActionPrint
      Category = 4
    end
    object dxBarButton3: TdxBarButton
      Action = DataBuy.ActionSave
      Category = 4
    end
  end
  object MenuOffice: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 48
    Top = 216
  end
  object SalaryChart: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      
        'from salary$office$chart(:a$office$id, :a$date$begin, :a$date$en' +
        'd)')
    BeforeOpen = SalaryChartBeforeOpen
    OnCalcFields = SalaryPersonCalcFields
    Transaction = DataBuy.TransactionWrite
    Database = dmCom.db
    RefreshTransactionKind = tkUpdateTransaction
    Left = 16
    Top = 296
    oFetchAll = True
  end
end
