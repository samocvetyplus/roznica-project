object SalaryQuorter: TSalaryQuorter
  Left = 0
  Top = 0
  AutoSize = True
  Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1099
  ClientHeight = 401
  ClientWidth = 757
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridOffice: TcxGrid
    AlignWithMargins = True
    Left = 4
    Top = 91
    Width = 749
    Height = 310
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = alClient
    TabOrder = 0
    object GridOfficeView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourceQuorter
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0'
          Kind = skSum
          FieldName = 'B$Q'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'B$C'
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'A$Q'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'A$C'
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'C$Q'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'C$C'
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'D$Q'
        end
        item
          Format = '0.00'
          Kind = skSum
          FieldName = 'D$C'
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'E$Q'
        end
        item
          Format = '0'
          Kind = skSum
          FieldName = 'E$C'
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.BandHeaderLineCount = 2
      Bands = <
        item
          Caption = #1055#1077#1088#1080#1086#1076
          Width = 746
        end
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          Position.BandIndex = 0
          Position.ColIndex = 0
        end
        item
          Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
          Position.BandIndex = 0
          Position.ColIndex = 1
          Width = 150
        end>
      object GridOfficeViewNAME: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OFFICE$NAME'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Focusing = False
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
        IsCaptionAssigned = True
      end
      object GridOfficeViewKQ: TcxGridDBBandedColumn
        Caption = #1050'. '#1082#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'K$Q$A'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridOfficeViewKC: TcxGridDBBandedColumn
        Caption = #1050'. '#1074#1099#1088#1091#1095#1082#1072
        DataBinding.FieldName = 'K$C$A'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
    end
    object GridOfficeLevel: TcxGridLevel
      GridView = GridOfficeView
    end
  end
  object BarDock: TdxBarDockControl
    AlignWithMargins = True
    Left = 4
    Top = 35
    Width = 749
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = dalTop
    BarManager = BarManager
    ExplicitTop = 28
  end
  object Ribbon: TdxRibbon
    Left = 0
    Top = 0
    Width = 757
    Height = 31
    ApplicationButton.Visible = False
    BarManager = BarManager
    ColorSchemeName = 'Blue'
    PopupMenuItems = []
    QuickAccessToolbar.Toolbar = BarAccess
    ShowTabGroups = False
    ShowTabHeaders = False
    SupportNonClientDrawing = True
    Contexts = <>
    TabOrder = 2
    TabStop = False
  end
  object SalaryQuorter1: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure salary$k$iu(:office$id, :k$q, :k$c)')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      
        'select * from salary$quorter(:a$office$id, :a$date$begin, :a$dat' +
        'e$end)')
    BeforeOpen = SalaryQuorter1BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    RefreshTransactionKind = tkUpdateTransaction
    Left = 16
    Top = 248
    oRefreshAfterPost = False
    oFetchAll = True
    object fbstrngfldSalaryQuorter1OFFICENAME: TFIBStringField
      FieldName = 'OFFICE$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object fbcdfldSalaryQuorter1KCA: TFIBBCDField
      FieldName = 'K$C$A'
      DisplayFormat = '#,##0.0000'
      EditFormat = '0.0000'
      Size = 4
      RoundByScale = True
    end
    object fbcdfldSalaryQuorter1KQA: TFIBBCDField
      FieldName = 'K$Q$A'
      DisplayFormat = '#,##0.0000'
      EditFormat = '0.0000'
      Size = 4
      RoundByScale = True
    end
  end
  object SourceQuorter: TDataSource
    AutoEdit = False
    DataSet = SalaryQuorter2
    Left = 48
    Top = 280
  end
  object Styles: TcxStyleRepository
    Left = 80
    Top = 216
    PixelsPerInch = 96
    object StyleRed: TcxStyle
      AssignedValues = [svTextColor]
      TextColor = clRed
    end
    object StyleDefault: TcxStyle
    end
  end
  object Action: TActionList
    Left = 16
    Top = 184
    object ActionRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = ActionRangeExecute
    end
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object ActionChart: TAction
      Caption = #1043#1088#1072#1092#1080#1082
      ImageIndex = 0
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Print'
      'Common'
      'Office'
      'Range'
      'Access')
    Categories.ItemsVisibles = (
      2
      2
      0
      0
      2)
    Categories.Visibles = (
      True
      True
      True
      True
      True)
    ImageOptions.Images = DataBuy.Image16
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 216
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 64
      FloatClientHeight = 550
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonOffice'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonRange'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarAccess: TdxBar
      Caption = 'Access'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1181
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 6
      ShortCut = 16464
      AutoGrayScale = False
      Width = 64
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActionChart
      Category = 0
      AutoGrayScale = False
      Width = 64
    end
    object ButtonRange: TdxBarLargeButton
      Action = ActionRange
      Category = 1
      ShortCut = 16464
      AutoGrayScale = False
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Category = 1
      ShortCut = 16466
      AutoGrayScale = False
      Width = 64
    end
    object ButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Category = 1
      Hint = #1047#1072#1082#1088#1099#1090#1100
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = ButtonCloseClick
      AutoGrayScale = False
    end
    object ButtonOffice: TdxBarLargeButton
      Caption = #1052#1072#1075#1072#1079#1080#1085
      Category = 2
      Hint = #1052#1072#1075#1072#1079#1080#1085
      Visible = ivAlways
      DropDownMenu = MenuOffice
      LargeImageIndex = 9
      OnClick = ButtonOfficeClick
      AutoGrayScale = False
      Width = 64
      SyncImageIndex = False
      ImageIndex = -1
    end
    object ButtonRangeCustom: TdxBarButton
      Caption = 'Custom'
      Category = 3
      Hint = 'Custom'
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object ButtonRangeDay: TdxBarButton
      Caption = 'Day'
      Category = 3
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonRangeWeek: TdxBarButton
      Caption = 'Week'
      Category = 3
      Hint = 'Week'
      Visible = ivAlways
      LargeImageIndex = 13
    end
    object ButtonRangeMonth: TdxBarButton
      Caption = 'Month'
      Category = 3
      Hint = 'Month'
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object ButtonRangeYear: TdxBarButton
      Caption = 'Year'
      Category = 3
      Hint = 'Year'
      Visible = ivAlways
      LargeImageIndex = 15
    end
    object dxBarButton1: TdxBarButton
      Action = DataBuy.ActionFit
      Category = 4
    end
    object dxBarButton2: TdxBarButton
      Action = DataBuy.ActionPrint
      Category = 4
    end
    object dxBarButton3: TdxBarButton
      Action = DataBuy.ActionSave
      Category = 4
    end
  end
  object MenuOffice: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 48
    Top = 216
  end
  object SalaryQuorter2: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select * from salary$quorter(:a$office$id, :a$date$begin, :a$dat' +
        'e$end)')
    BeforeOpen = SalaryQuorter2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 280
    object SalaryQuorter2OFFICENAME: TFIBStringField
      FieldName = 'OFFICE$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object SalaryQuorter2KCA: TFIBBCDField
      FieldName = 'K$C$A'
      DisplayFormat = '#,##0.0000'
      EditFormat = '0.0000'
      Size = 4
      RoundByScale = True
    end
    object SalaryQuorter2KQA: TFIBBCDField
      FieldName = 'K$Q$A'
      DisplayFormat = '#,##0.0000'
      EditFormat = '0.0000'
      Size = 4
      RoundByScale = True
    end
  end
end
