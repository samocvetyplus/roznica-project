object AnalisisDateSell: TAnalisisDateSell
  Left = 0
  Top = 0
  Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1086#1076#1072#1078' '#1087#1086' '#1076#1072#1090#1072#1084
  ClientHeight = 585
  ClientWidth = 1130
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    AlignWithMargins = True
    Left = 4
    Top = 84
    Width = 1122
    Height = 501
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = alClient
    PopupMenu = MenuGrid
    TabOrder = 0
    object GridView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourceOffice
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skAverage
          Position = spFooter
          Column = GridViewPRICE
        end
        item
          Kind = skAverage
          Position = spFooter
          Column = GridViewPRICE0
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridViewCOST
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridViewCOST0
        end
        item
          Kind = skAverage
          Position = spFooter
          Column = GridViewDISCOUNT
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridViewW
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.GroupFooters = gfAlwaysVisible
      Bands = <
        item
          Caption = #1055#1077#1088#1080#1086#1076
          Width = 2846
        end
        item
          Caption = #1055#1088#1086#1076#1072#1078#1072
          Position.BandIndex = 0
          Position.ColIndex = 0
          Width = 252
        end
        item
          Caption = #1052#1072#1075#1072#1079#1080#1085
          Position.BandIndex = 0
          Position.ColIndex = 1
          Width = 122
        end
        item
          Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072
          Position.BandIndex = 0
          Position.ColIndex = 2
          Width = 127
        end
        item
          Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1090#1086#1074#1072#1088#1072
          Position.BandIndex = 0
          Position.ColIndex = 3
          Width = 125
        end
        item
          Caption = #1057#1082#1080#1076#1082#1072
          Position.BandIndex = 0
          Position.ColIndex = 4
          Width = 65
        end
        item
          Caption = #1055#1086#1083#1085#1099#1081' '
          Position.BandIndex = 0
          Position.ColIndex = 5
          Width = 126
        end
        item
          Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
          Position.BandIndex = 0
          Position.ColIndex = 6
          Width = 525
        end
        item
          Caption = #1055#1088#1080#1093#1086#1076
          Position.BandIndex = 0
          Position.ColIndex = 8
          Width = 404
        end
        item
          Caption = #1055#1086#1076#1088#1086#1073#1085#1086
          Position.BandIndex = 0
          Position.ColIndex = 7
          Width = 675
        end
        item
          Caption = #1054#1089#1090#1072#1090#1082#1080
          Position.BandIndex = 0
          Position.ColIndex = 9
          Width = 125
        end
        item
          Caption = #1052#1072#1082#1089#1080#1084#1091#1084' '#1079#1072' '#1087#1077#1088#1080#1086#1076
          Position.BandIndex = 0
          Position.ColIndex = 10
          Width = 400
        end>
      object GridViewADATE: TcxGridDBBandedColumn
        AlternateCaption = #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        Caption = #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        DataBinding.FieldName = 'ADATE'
        Width = 86
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewCLIENTNAME: TcxGridDBBandedColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        DataBinding.FieldName = 'CLIENTNAME'
        Width = 92
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewNODCARD: TcxGridDBBandedColumn
        Caption = #8470' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
        DataBinding.FieldName = 'NODCARD'
        Width = 77
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewCODE: TcxGridDBBandedColumn
        Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'CODE'
        Width = 84
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewMATID: TcxGridDBBandedColumn
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083
        DataBinding.FieldName = 'MATID'
        Position.BandIndex = 7
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewGOODID: TcxGridDBBandedColumn
        Caption = #1053#1072#1080#1084'. '#1080#1079#1076#1077#1083#1080#1103
        DataBinding.FieldName = 'GOODID'
        Width = 93
        Position.BandIndex = 7
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewColumn7: TcxGridDBBandedColumn
        Caption = #1054#1042
        DataBinding.FieldName = 'INSID'
        Position.BandIndex = 7
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewART: TcxGridDBBandedColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ART'
        Width = 93
        Position.BandIndex = 7
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object GridViewART2: TcxGridDBBandedColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        DataBinding.FieldName = 'ART2'
        Width = 69
        Position.BandIndex = 7
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object GridViewColumn10: TcxGridDBBandedColumn
        Caption = #1045#1076'. '#1080#1079#1084'.'
        DataBinding.FieldName = 'UNITID'
        Position.BandIndex = 7
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object GridViewSDATE: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        DataBinding.FieldName = 'SDATE'
        Width = 91
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewSPRICE: TcxGridDBBandedColumn
        Caption = #1062#1077#1085#1072
        DataBinding.FieldName = 'SPRICE'
        Width = 39
        Position.BandIndex = 8
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewSUP: TcxGridDBBandedColumn
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataBinding.FieldName = 'SUP'
        Width = 98
        Position.BandIndex = 8
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewSN: TcxGridDBBandedColumn
        Caption = #8470' '#1087#1086#1089#1090'.'
        DataBinding.FieldName = 'SN'
        Position.BandIndex = 8
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object GridViewColumn15: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
        DataBinding.FieldName = 'RQ'
        Position.BandIndex = 10
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewColumn16: TcxGridDBBandedColumn
        Caption = #1042#1077#1089' '#1086#1089#1090'.'
        DataBinding.FieldName = 'RW'
        Position.BandIndex = 10
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewPRICE: TcxGridDBBandedColumn
        Caption = #1089#1086' '#1089#1082#1080#1076#1082#1086#1081
        DataBinding.FieldName = 'PRICE'
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewPRICE0: TcxGridDBBandedColumn
        Caption = #1073#1077#1079' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'PRICE0'
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewCOST: TcxGridDBBandedColumn
        Caption = #1089#1086' '#1089#1082#1080#1076#1082#1086#1081
        DataBinding.FieldName = 'COST'
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewCOST0: TcxGridDBBandedColumn
        Caption = #1073#1077#1079' '#1089#1082#1080#1076#1082#1080
        DataBinding.FieldName = 'COST0'
        Position.BandIndex = 4
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewDISCOUNT: TcxGridDBBandedColumn
        Caption = '%'
        DataBinding.FieldName = 'DISCOUNT'
        FooterAlignmentHorz = taCenter
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewColumn22: TcxGridDBBandedColumn
        Caption = #1072#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'FULLART'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewW: TcxGridDBBandedColumn
        Caption = #1042#1077#1089
        DataBinding.FieldName = 'W'
        Position.BandIndex = 9
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewSZ: TcxGridDBBandedColumn
        Caption = #1056#1072#1079#1084#1077#1088
        DataBinding.FieldName = 'SZ'
        Position.BandIndex = 9
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewUID: TcxGridDBBandedColumn
        Caption = #1048#1085#1076'. '#1085#1086#1084#1077#1088
        DataBinding.FieldName = 'UID'
        Position.BandIndex = 9
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewColumn27: TcxGridDBBandedColumn
        Caption = #1053#1086#1084#1077#1088' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHECKNO'
        Position.BandIndex = 9
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewColumn29: TcxGridDBBandedColumn
        Caption = #1055#1088#1086#1076#1072#1074#1077#1094
        DataBinding.FieldName = 'EMP'
        Position.BandIndex = 9
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object GridViewCASHIERNAME: TcxGridDBBandedColumn
        Caption = #1050#1072#1089#1089#1080#1088
        DataBinding.FieldName = 'CASHIERNAME'
        Position.BandIndex = 9
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object GridViewDEPNAME: TcxGridDBBandedColumn
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'DEPNAME'
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewCODEFULL: TcxGridDBBandedColumn
        Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100' '#1080#1079#1076'-'#1103
        DataBinding.FieldName = 'CODEFULL'
        Width = 116
        Position.BandIndex = 8
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewSUMMARY: TcxGridDBBandedColumn
        Caption = 'C'#1091#1084#1084#1072' '#1087#1086' '#1080#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1102
        DataBinding.FieldName = 'SUMMARY'
        Width = 138
        Position.BandIndex = 11
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewSummCountDate: TcxGridDBBandedColumn
        Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072' '#1087#1086#1082#1091#1087#1086#1082
        DataBinding.FieldName = 'SummCountDate'
        Width = 142
        Position.BandIndex = 11
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewAllCount: TcxGridDBBandedColumn
        Caption = #1042#1089#1077#1075#1086' '#1087#1086#1082#1091#1087#1086#1082
        DataBinding.FieldName = 'AllCount'
        Width = 106
        Position.BandIndex = 11
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object BarDock: TdxBarDockControl
    AlignWithMargins = True
    Left = 4
    Top = 4
    Width = 1122
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = dalTop
    BarManager = BarManager
  end
  object Analisis: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'SELECT '
      'sl.ADATE, sl.CLIENTNAME, sl.PRICE, sl.CLIENTID, sl.COMPID,'
      'sl.PRICE0, sl.W, sl.SZ, sl.UID, sl.FULLART, sl.ART2,'
      'sl.UNITID, sl.CHECKNO, fif(sl.discount, '
      
        'sl.discount,((sl.price0 - sl.price) * 100) / sl.price0) discount' +
        ','
      'sl.EMP, sl.SDATE, sl.SPRICE, sl.SUP, sl.SN, sl.RQ, sl.RW,'
      'sl.ART, sl.NODCARD, sl.CODE, sl.INSID, sl.GOODID, sl.MATID,'
      'sl.CASHIERNAME, sl.Cost, sl.Cost0, sl.depname, sl.CODEFULL'
      ''
      'FROM ANALYSIS_SELLITEM_S(:a$date$begin, :a$date$end) sl')
    BeforeOpen = SalaryBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    UpdateTransaction = DataBuy.TransactionWrite
    Left = 16
    Top = 248
    oFetchAll = True
    object AnalisisADATE: TFIBDateTimeField
      FieldName = 'ADATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object AnalisisCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object AnalisisNODCARD: TFIBStringField
      FieldName = 'NODCARD'
      EmptyStrToNull = True
    end
    object AnalisisPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object AnalisisPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object AnalisisW: TFIBFloatField
      FieldName = 'W'
    end
    object AnalisisSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object AnalisisUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object AnalisisFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object AnalisisART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object AnalisisUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object AnalisisCHECKNO: TFIBIntegerField
      FieldName = 'CHECKNO'
    end
    object AnalisisDISCOUNT: TFIBFloatField
      FieldName = 'DISCOUNT'
    end
    object AnalisisEMP: TFIBStringField
      FieldName = 'EMP'
      Size = 40
      EmptyStrToNull = True
    end
    object AnalisisSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object AnalisisSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
    object AnalisisSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object AnalisisSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object AnalisisRQ: TFIBIntegerField
      FieldName = 'RQ'
    end
    object AnalisisRW: TFIBFloatField
      FieldName = 'RW'
    end
    object AnalisisART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object AnalisisCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object AnalisisINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object AnalisisGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object AnalisisMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object AnalisisCASHIERNAME: TFIBStringField
      FieldName = 'CASHIERNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object AnalisisCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object AnalisisCOST0: TFIBFloatField
      FieldName = 'COST0'
    end
    object AnalisisDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      EmptyStrToNull = True
    end
    object AnalisisCODEFULL: TFIBStringField
      FieldName = 'CODEFULL'
      Size = 40
      EmptyStrToNull = True
    end
    object AnalisisCLIENTID: TFIBIntegerField
      FieldName = 'CLIENTID'
    end
    object AnalisisCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
  end
  object SourceOffice: TDataSource
    DataSet = AnalisisCL
    Left = 16
    Top = 408
  end
  object Action: TActionList
    Images = DataBuy.Image32
    Left = 16
    Top = 344
    object ActionRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = ActionRangeExecute
    end
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object BrowseForFolder: TBrowseForFolder
      Caption = #1057' '#1088#1072#1079#1073#1080#1077#1085#1080#1077#1084' '#1087#1086' '#1084#1072#1075#1072#1079#1080#1085#1072#1084
      DialogCaption = #1057' '#1088#1072#1079#1073#1080#1077#1085#1080#1077#1084' '#1087#1086' '#1084#1072#1075#1072#1079#1080#1085#1072#1084
      BrowseOptions = [bifUseNewUI]
      ImageIndex = 26
      OnAccept = BrowseForFolderAccept
    end
    object BrowseForFolderAll: TBrowseForFolder
      Caption = #1054#1076#1085#1080#1084' '#1092#1072#1081#1083#1086#1084
      DialogCaption = #1054#1076#1085#1080#1084' '#1092#1072#1081#1083#1086#1084
      BrowseOptions = [bifUseNewUI]
      ImageIndex = 26
      OnAccept = BrowseForFolderAccept
    end
    object SMSButton: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '
      ImageIndex = 42
      OnExecute = SMSButtonExecute
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Print'
      'Common'
      'Range')
    Categories.ItemsVisibles = (
      2
      2
      0)
    Categories.Visibles = (
      True
      True
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 216
    DockControlHeights = (
      0
      0
      24
      0)
    object Bar: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 64
      FloatClientHeight = 550
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonRange'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonExcel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarBottom: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1181
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 50
          Visible = True
          ItemName = 'EditorBuyCount'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 6
      ShortCut = 16464
      OnClick = ButtonPrintClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonPhone: TdxBarLargeButton
      Caption = #1058#1077#1083#1077#1092#1086#1085
      Category = 0
      Hint = #1058#1077#1083#1077#1092#1086#1085
      Visible = ivNever
      ButtonStyle = bsChecked
      LargeImageIndex = 24
      AutoGrayScale = False
    end
    object EditorBuyCount: TdxBarSpinEdit
      Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1082#1091#1087#1086#1082
      Category = 0
      Hint = #1050#1086#1083'-'#1074#1086' '#1087#1086#1082#1091#1087#1086#1082
      Visible = ivNever
      OnCurChange = EditorBuyCountCurChange
      ShowCaption = True
      Width = 100
      MaxValue = 1000000.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
    end
    object ButtonExcel: TdxBarLargeButton
      Caption = 'Excel'
      Category = 0
      Hint = 'Excel'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = MenuExcel
      LargeImageIndex = 26
      AutoGrayScale = False
      Width = 64
    end
    object ButtonAll: TdxBarLargeButton
      Action = BrowseForFolderAll
      Category = 0
    end
    object ButtonOffice: TdxBarLargeButton
      Action = BrowseForFolder
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = '&Open...'
      Category = 0
      Hint = 'Open|Opens an existing file'
      Visible = ivAlways
      ImageIndex = 7
      ShortCut = 16463
    end
    object dxBarButton2: TdxBarButton
      Caption = 'SMSButton'
      Category = 0
      Hint = #1053#1072#1078#1084#1080#1090#1077', '#1095#1090#1086#1073#1099' '#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '#1087#1086' '#1082#1086#1090#1086#1088#1099#1084' '#1085#1077' '#1076#1086#1096#1083#1072' '#1057#1052#1057'-'#1088#1072#1089#1089#1099#1083#1082#1072
      Visible = ivAlways
      ImageIndex = 40
      ShortCut = 16463
      OnClick = SMSButtonExecute
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = SMSButton
      Caption = #1053#1077#1072#1082#1090#1080#1074#1085#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1057#1052#1057
      Category = 0
      Description = #1053#1072#1078#1084#1080#1090#1077', '#1095#1090#1086#1073#1099' '#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '#1076#1086' '#1082#1086#1090#1086#1088#1099#1093' '#1085#1077' '#1076#1086#1096#1083#1072' '#1057#1052#1057'-'#1088#1072#1089#1089#1099#1083#1082#1072
      ShortCut = 16463
      AutoGrayScale = False
    end
    object maxsumm: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.NullStyle = nssUnchecked
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 2
      Properties.OnChange = maxsummChange
    end
    object ButtonRange: TdxBarLargeButton
      Action = ActionRange
      Category = 1
      ShortCut = 16464
      AutoGrayScale = False
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Category = 1
      AutoGrayScale = False
      Width = 64
    end
    object ButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Category = 1
      Hint = #1047#1072#1082#1088#1099#1090#1100
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = ButtonCloseClick
      AutoGrayScale = False
    end
    object ButtonRangeCustom: TdxBarButton
      Caption = 'Custom'
      Category = 2
      Hint = 'Custom'
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object ButtonRangeDay: TdxBarButton
      Caption = 'Day'
      Category = 2
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonRangeWeek: TdxBarButton
      Caption = 'Week'
      Category = 2
      Hint = 'Week'
      Visible = ivAlways
      LargeImageIndex = 13
    end
    object ButtonRangeMonth: TdxBarButton
      Caption = 'Month'
      Category = 2
      Hint = 'Month'
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object ButtonRangeYear: TdxBarButton
      Caption = 'Year'
      Category = 2
      Hint = 'Year'
      Visible = ivAlways
      LargeImageIndex = 15
    end
  end
  object MenuExcel: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonAll'
      end
      item
        Visible = True
        ItemName = 'ButtonOffice'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 16
    Top = 376
  end
  object TdxBarSeparator
    Category = -1
    Visible = ivAlways
  end
  object OpenDialog1: TOpenDialog
    Left = 16
    Top = 280
  end
  object pFIBQuery1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 312
  end
  object SummComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from ANALYSIS_SUMM_COMP2 (:BD,:ED);')
    BeforeOpen = SummCompBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 96
    Top = 360
    object SummCompCLIENTID: TFIBIntegerField
      FieldName = 'CLIENTID'
    end
    object SummCompCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object SummCompSUMMARY: TFIBFloatField
      FieldName = 'SUMMARY'
    end
  end
  object SummCompProvider: TDataSetProvider
    DataSet = SummComp
    Left = 136
    Top = 360
  end
  object SummCompCL: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'SUMMARY'
        DataType = ftFloat
      end
      item
        Name = 'CLIENTID'
        ChildDefs = <
          item
            Name = 'CLIENTID'
          end>
        DataType = ftInteger
        Size = 1
      end
      item
        Name = 'COMPID'
        ChildDefs = <
          item
            Name = 'COMPID'
          end>
        DataType = ftInteger
        Size = 1
      end>
    IndexDefs = <
      item
        Name = 'clientid'
        Fields = 'clientid'
        GroupingLevel = 1
      end
      item
        Name = 'compid'
        Fields = 'compid'
        GroupingLevel = 2
      end>
    IndexName = 'clientid'
    Params = <
      item
        DataType = ftDateTime
        Name = 'BD'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'ED'
        ParamType = ptInput
      end>
    ProviderName = 'SummCompProvider'
    StoreDefs = True
    Left = 184
    Top = 360
    object SummCompCLCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
    end
    object SummCompCLCOMPID: TIntegerField
      FieldName = 'COMPID'
      KeyFields = 'COMPID'
    end
    object SummCompCLSUMMARY: TFloatField
      FieldName = 'SUMMARY'
    end
  end
  object AnalisisProvider: TpFIBDataSetProvider
    DataSet = Analisis
    Options = [poDisableInserts, poDisableDeletes, poUseQuoteChar]
    Left = 56
    Top = 248
  end
  object AnalisisCL: TpFIBClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ADATE'
        DataType = ftDateTime
      end
      item
        Name = 'CLIENTNAME'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'NODCARD'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'PRICE'
        DataType = ftFloat
      end
      item
        Name = 'PRICE0'
        DataType = ftFloat
      end
      item
        Name = 'W'
        DataType = ftFloat
      end
      item
        Name = 'SZ'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'UID'
        DataType = ftInteger
      end
      item
        Name = 'FULLART'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'ART2'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'UNITID'
        DataType = ftSmallint
      end
      item
        Name = 'CHECKNO'
        DataType = ftInteger
      end
      item
        Name = 'DISCOUNT'
        DataType = ftFloat
      end
      item
        Name = 'EMP'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'SDATE'
        DataType = ftDateTime
      end
      item
        Name = 'SPRICE'
        DataType = ftFloat
      end
      item
        Name = 'SUP'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'SN'
        DataType = ftInteger
      end
      item
        Name = 'RQ'
        DataType = ftInteger
      end
      item
        Name = 'RW'
        DataType = ftFloat
      end
      item
        Name = 'ART'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CODE'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'INSID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'GOODID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'MATID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'CASHIERNAME'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'COST'
        DataType = ftFloat
      end
      item
        Name = 'COST0'
        DataType = ftFloat
      end
      item
        Name = 'DEPNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'CODEFULL'
        DataType = ftString
        Size = 40
      end
      item
        Name = 'CLIENTID'
        DataType = ftInteger
      end
      item
        Name = 'COMPID'
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'uid'
        Fields = 'uid'
        Options = [ixUnique]
      end
      item
        Name = 'art'
        Fields = 'art'
        GroupingLevel = 1
      end>
    Params = <
      item
        DataType = ftString
        Name = 'USERID'
        ParamType = ptInput
      end>
    ProviderName = 'AnalisisProvider'
    StoreDefs = True
    OnCalcFields = AnalisisCLCalcFields
    Left = 96
    Top = 248
    object AnalisisCLADATE: TDateTimeField
      FieldName = 'ADATE'
    end
    object AnalisisCLCLIENTNAME: TStringField
      FieldName = 'CLIENTNAME'
      Size = 40
    end
    object AnalisisCLNODCARD: TStringField
      FieldName = 'NODCARD'
    end
    object AnalisisCLPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object AnalisisCLPRICE0: TFloatField
      FieldName = 'PRICE0'
    end
    object AnalisisCLW: TFloatField
      FieldName = 'W'
    end
    object AnalisisCLSZ: TStringField
      FieldName = 'SZ'
      Size = 10
    end
    object AnalisisCLUID: TIntegerField
      FieldName = 'UID'
    end
    object AnalisisCLFULLART: TStringField
      FieldName = 'FULLART'
      Size = 60
    end
    object AnalisisCLART2: TStringField
      FieldName = 'ART2'
    end
    object AnalisisCLUNITID: TSmallintField
      FieldName = 'UNITID'
    end
    object AnalisisCLCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
    end
    object AnalisisCLDISCOUNT: TFloatField
      FieldName = 'DISCOUNT'
    end
    object AnalisisCLEMP: TStringField
      FieldName = 'EMP'
      Size = 40
    end
    object AnalisisCLSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object AnalisisCLSPRICE: TFloatField
      FieldName = 'SPRICE'
    end
    object AnalisisCLSUP: TStringField
      FieldName = 'SUP'
    end
    object AnalisisCLSN: TIntegerField
      FieldName = 'SN'
    end
    object AnalisisCLRQ: TIntegerField
      FieldName = 'RQ'
    end
    object AnalisisCLRW: TFloatField
      FieldName = 'RW'
    end
    object AnalisisCLART: TStringField
      FieldName = 'ART'
    end
    object AnalisisCLCODE: TStringField
      FieldName = 'CODE'
      Size = 10
    end
    object AnalisisCLINSID: TStringField
      FieldName = 'INSID'
      Size = 10
    end
    object AnalisisCLGOODID: TStringField
      FieldName = 'GOODID'
      Size = 10
    end
    object AnalisisCLMATID: TStringField
      FieldName = 'MATID'
      Size = 10
    end
    object AnalisisCLCASHIERNAME: TStringField
      FieldName = 'CASHIERNAME'
      Size = 30
    end
    object AnalisisCLCOST: TFloatField
      FieldName = 'COST'
    end
    object AnalisisCLCOST0: TFloatField
      FieldName = 'COST0'
    end
    object AnalisisCLDEPNAME: TStringField
      FieldName = 'DEPNAME'
    end
    object AnalisisCLCODEFULL: TStringField
      FieldName = 'CODEFULL'
      Size = 40
    end
    object AnalisisCLCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
    end
    object AnalisisCLSUMMARY: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'SUMMARY'
      Calculated = True
    end
    object AnalisisCLCOMPID: TIntegerField
      FieldName = 'COMPID'
    end
    object AnalisisCLSummCountDate: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'SummCountDate'
      Calculated = True
    end
    object AnalisisCLAllCount: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'AllCount'
      Calculated = True
    end
  end
  object MenuGrid: TPopupMenu
    Left = 488
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1073#1088#1086#1089' '#1075#1088#1091#1087#1087#1080#1088#1086#1074#1082#1080
      OnClick = N1Click
    end
  end
  object SummCountDate: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select buy$count as buycount, client$id as clientid, buy$summ as' +
        ' buysumm from BUY$CLIENT$SUMMCOUNT (:BD,:ED);')
    BeforeOpen = SummCountDateBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 88
    Top = 408
    object SummCountDateBUYCOUNT: TFIBIntegerField
      FieldName = 'BUYCOUNT'
    end
    object SummCountDateCLIENTID: TFIBIntegerField
      FieldName = 'CLIENTID'
    end
    object SummCountDateBUYSUMM: TFIBBCDField
      FieldName = 'BUYSUMM'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
  end
  object SummCountDateProvider: TDataSetProvider
    DataSet = SummCountDate
    Left = 136
    Top = 408
  end
  object SummCountDateCL: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'CLIENTID'
        DataType = ftInteger
      end
      item
        Name = 'BUYCOUNT'
        DataType = ftInteger
      end
      item
        Name = 'BUYSUMM'
        DataType = ftBCD
        Precision = 32
        Size = 2
      end>
    IndexDefs = <
      item
        Name = 'clientid'
        Fields = 'clientid'
        GroupingLevel = 1
      end>
    IndexName = 'clientid'
    Params = <
      item
        DataType = ftDateTime
        Name = 'BD'
        ParamType = ptInput
      end
      item
        DataType = ftDateTime
        Name = 'ED'
        ParamType = ptInput
      end>
    ProviderName = 'SummCountDateProvider'
    StoreDefs = True
    Left = 184
    Top = 400
    object SummCountDateCLCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
    end
    object SummCountDateCLBUYCOUNT: TIntegerField
      FieldName = 'BUYCOUNT'
    end
    object SummCountDateCLBUYSUMM: TBCDField
      FieldName = 'BUYSUMM'
      Precision = 32
      Size = 2
    end
  end
end
