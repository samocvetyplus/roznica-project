unit frmAnalisisSalary;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, StrUtils,
  dxSkinscxPCPainter, cxGridBandedTableView, cxGridDBBandedTableView, DateUtils,
  cxContainer, dxLayoutcxEditAdapters, FIBDatabase, pFIBDatabase,
  dxLayoutControl, cxTextEdit, cxDBEdit, dxLayoutLookAndFeels,
  dxSkinsdxBarPainter, dxBar, ActnList, frmPopup, DBClient, cxSplitter,
  cxCalendar, dxSkinsDefaultPainters, dxRibbonForm, dxRibbon, dxRibbonFormCaptionHelper,
  dxSkinsdxRibbonPainter, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, ComCtrls, TeEngine, Series, TeeProcs, Chart, M207Proc;

type

{ TAnalisisSalary }

  TAnalisisSalary = class(TdxRibbonForm)
    SalaryOffice: TpFIBDataSet;
    SalaryPerson: TpFIBDataSet;
    SalaryOfficeBQ: TFIBBCDField;
    SalaryOfficeBC: TFIBBCDField;
    SalaryOfficeAQ: TFIBBCDField;
    SalaryOfficeAC: TFIBBCDField;
    SalaryOfficeKQ: TFIBBCDField;
    SalaryOfficeKC: TFIBBCDField;
    SalaryOfficeCQ: TFIBBCDField;
    SalaryOfficeCC: TFIBBCDField;
    SalaryOfficeDQ: TFIBBCDField;
    SalaryOfficeDC: TFIBBCDField;
    SalaryOfficeEQ: TFIBBCDField;
    SalaryOfficeEC: TFIBBCDField;
    SalaryPersonOFFICENAME: TFIBStringField;
    SalaryPersonPERSONNAME: TFIBStringField;
    SalaryPersonQBEFORE: TFIBBCDField;
    SalaryPersonCBEFORE: TFIBBCDField;
    SalaryPersonFPARTIALTIMESTAMP: TFIBDateTimeField;
    SalaryPersonQMIDDLE: TFIBBCDField;
    SalaryPersonCMIDDLE: TFIBBCDField;
    SalaryPersonFFULLTIMESTAMP: TFIBDateTimeField;
    SalaryPersonQAFTER: TFIBBCDField;
    SalaryPersonCAFTER: TFIBBCDField;
    SalaryPersonOFFICEQ: TFIBBCDField;
    SalaryPersonOFFICEC: TFIBBCDField;
    SalaryPersonSUMQ: TFIBBCDField;
    SalaryPersonSUMC: TFIBBCDField;
    GridOfficeLevel: TcxGridLevel;
    GridOffice: TcxGrid;
    GridPerson: TcxGrid;
    GridPersonLevel: TcxGridLevel;
    SourceOffice: TDataSource;
    SourcePerson: TDataSource;
    GridPersonView: TcxGridDBBandedTableView;
    GridOfficeView: TcxGridDBBandedTableView;
    GridOfficeViewNAME: TcxGridDBBandedColumn;
    GridOfficeViewBQ: TcxGridDBBandedColumn;
    GridOfficeViewBC: TcxGridDBBandedColumn;
    GridOfficeViewAQ: TcxGridDBBandedColumn;
    GridOfficeViewAC: TcxGridDBBandedColumn;
    GridOfficeViewKQ: TcxGridDBBandedColumn;
    GridOfficeViewKC: TcxGridDBBandedColumn;
    GridOfficeViewCQ: TcxGridDBBandedColumn;
    GridOfficeViewCC: TcxGridDBBandedColumn;
    GridOfficeViewDQ: TcxGridDBBandedColumn;
    GridOfficeViewDC: TcxGridDBBandedColumn;
    GridOfficeViewEQ: TcxGridDBBandedColumn;
    GridOfficeViewEC: TcxGridDBBandedColumn;
    GridPersonViewOFFICENAME: TcxGridDBBandedColumn;
    GridPersonViewPERSONNAME: TcxGridDBBandedColumn;
    GridPersonViewQBEFORE: TcxGridDBBandedColumn;
    GridPersonViewCBEFORE: TcxGridDBBandedColumn;
    GridPersonViewFPARTIALTIMESTAMP: TcxGridDBBandedColumn;
    GridPersonViewQMIDDLE: TcxGridDBBandedColumn;
    GridPersonViewCMIDDLE: TcxGridDBBandedColumn;
    GridPersonViewFFULLTIMESTAMP: TcxGridDBBandedColumn;
    GridPersonViewQAFTER: TcxGridDBBandedColumn;
    GridPersonViewCAFTER: TcxGridDBBandedColumn;
    GridPersonViewOFFICEQ: TcxGridDBBandedColumn;
    GridPersonViewOFFICEC: TcxGridDBBandedColumn;
    GridPersonViewSUMQ: TcxGridDBBandedColumn;
    GridPersonViewSUMC: TcxGridDBBandedColumn;
    Styles: TcxStyleRepository;
    StyleRed: TcxStyle;
    StyleDefault: TcxStyle;
    Action: TActionList;
    ActionReload: TAction;
    ActionRange: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonPrint: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    ButtonReload: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    ButtonRangeCustom: TdxBarButton;
    ButtonRangeDay: TdxBarButton;
    ButtonRangeWeek: TdxBarButton;
    ButtonRangeMonth: TdxBarButton;
    ButtonRangeYear: TdxBarButton;
    MenuOffice: TdxBarPopupMenu;
    BarDock: TdxBarDockControl;
    ButtonClose: TdxBarLargeButton;
    Splitter: TcxSplitter;
    SalaryOfficeOFFICEID: TFIBIntegerField;
    SalaryOfficeOFFICENAME: TFIBStringField;
    SalaryOfficeDATEBEGIN: TFIBDateField;
    SalaryOfficeDATEEND: TFIBDateField;
    GridPersonViewTOTALPERSONQ: TcxGridDBBandedColumn;
    SalaryPersonTOTALPERSONQ: TBCDField;
    SalaryPersonTOTALPERSONC: TBCDField;
    GridPersonViewTOTALPERSONC: TcxGridDBBandedColumn;
    BarAccess: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    Ribbon: TdxRibbon;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionChart: TAction;
    SalaryChart: TpFIBDataSet;
    dxBarCoefButton: TdxBarLargeButton;
    procedure SalaryBeforeOpen(DataSet: TDataSet);
    procedure GridOfficeViewDStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure ActionRangeExecute(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonOfficeClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure SalaryOfficeAfterPost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalaryPersonCalcFields(DataSet: TDataSet);
    procedure ActionChartExecute(Sender: TObject);
    procedure SalaryChartBeforeOpen(DataSet: TDataSet);
    procedure dxBarCoefButtonClick(Sender: TObject);
  private
    RangeBegin: TDate;
    RangeEnd: TDate;
    procedure InternalExecute;
    procedure UpdateRange;
    procedure OnRangeOk(Popup: TPopup);
    function GetOfficeID: Integer;
    procedure SetOfficeID(const Value: Integer);
  public
    class procedure Execute;
    property OfficeID: Integer read GetOfficeID write SetOfficeID;
  end;


implementation

{$R *.dfm}

uses

   comdata, data,

   uBuy, uDialog, uGridPrinter,
   dmBuy,
   frmBuyRangePopup, frmAnalisisSalaryChart, pFIBErrorHandler,
   frmAnalisisSalaryHost, frmSalaryQuorter;

{ TAnalisisSalary }

class procedure TAnalisisSalary.Execute;
var
  AnalisisSalary: TAnalisisSalary;
begin
  AnalisisSalary := TAnalisisSalary.Create(nil);

  try

    AnalisisSalary.InternalExecute;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;
  end;

  AnalisisSalary.Free;
end;


procedure TAnalisisSalary.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if SalaryOffice.State = dsEdit then
  begin
    SalaryOffice.Post;
  end;
end;

procedure TAnalisisSalary.FormCreate(Sender: TObject);
var
  Rect: TRect;

procedure SetupGrid;
var
  i: Integer;
  Caption: string;
begin
  for i := 0 to GridOfficeView.Bands.Count - 1 do
  begin
    Caption := GridOfficeView.Bands[i].Caption;

    Caption :=  AnsiReplaceStr(Caption, '?', #13#10);

    GridOfficeView.Bands[i].Caption := Caption;
  end;

if CenterDep = True then
  begin
    if dmCom.UserId <> 1 then
    GridOfficeView.Columns[6].Options.Editing := False;
    GridOfficeView.Columns[5].Options.Editing := False;
    
  end
  else

  if dmCom.User.Adm = True then
    begin
    GridOfficeView.Columns[6].Options.Editing := True;
    GridOfficeView.Columns[5].Options.Editing := True;
    end
      else
      if dmCom.User.Manager = True then
        begin
        GridOfficeView.Columns[6].Options.Editing := True;
        GridOfficeView.Columns[5].Options.Editing := True;
        end
          else
          begin
          GridOfficeView.Columns[6].Options.Editing := False;
          GridOfficeView.Columns[5].Options.Editing := False;
    end;
end;


procedure MenuOfficePopulate;
var
  Button: TdxBarLargeButton;
  ButtonLink: TdxBarItemLink;
  DataSet: TClientDataSet;
begin
  if MenuOffice.ItemLinks.Count = 0 then
  begin
    DataSet := TClientDataSet.Create(nil);

    try

      DataSet.Data := DataBuy.Table(sqlOffice);

      DataSet.First;

      while not DataSet.Eof do
      begin

        Button := TdxBarLargeButton(BarManager.AddItem(TdxBarLargeButton));

        Button.ButtonStyle := bsChecked;

        Button.GroupIndex := 2;

        Button.LargeImageIndex := ButtonOffice.LargeImageIndex;

        Button.Tag := DataSet.FieldByName('ID').AsInteger;

        Button.Caption := DataSet.FieldByName('NAME').AsString;

        Button.OnClick := ButtonOfficeClick;

        ButtonLink := MenuOffice.ItemLinks.Add;

        ButtonLink.Item := Button;

        if Button.Tag = 1 then
        begin
          Button.Caption := '���';

          Button.Tag := 0;
        end;

        if Button.Tag = OfficeID then
        begin
          Button.Down := True;
        end;

        if MenuOffice.ItemLinks.Count = 2 then
        begin
          ButtonLink.BeginGroup := True;
        end;

        DataSet.Next;
      end;

    finally

      DataSet.Free;

    end;

  end;
end;

procedure SetupBar;
var
  CategoryIndex: Integer;
begin
  CategoryIndex := BarManager.Categories.IndexOf('Office');

  if CategoryIndex <> -1 then
  begin

    if DataBuy.IsCenterDepartment then
    begin
      BarManager.CategoryItemsVisible[CategoryIndex] := ivAlways;

      MenuOfficePopulate;
    end else

    begin
      BarManager.CategoryItemsVisible[CategoryIndex] := ivNever;
    end;

  end;
end;

begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);

  BoundsRect := Rect;

  RangeBegin := DateOf(StartOfTheMonth(Date));

  RangeEnd := DateOf(EndOfTheMonth(Date));

  DataBuy := TDataBuy.Create(Self);

  if DataBuy.IsCenterDepartment then
  begin
    OfficeID := 0;
  end else
  begin
    OfficeID := DataBuy.SelfDepartmentID;
  end;

  SetupBar;

  SetupGrid;

  MenuOfficePopulate;

end;

procedure TAnalisisSalary.InternalExecute;
begin
  ActionReload.Execute;

  ShowModal;
end;

function TAnalisisSalary.GetOfficeID: Integer;
begin
  Result := ButtonOffice.Tag;
end;

procedure TAnalisisSalary.SetOfficeID(const Value: Integer);
begin
  ButtonOffice.Tag := Value;
end;

procedure TAnalisisSalary.SalaryBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
  begin
    ParamByName('a$office$id').AsInteger := OfficeID;

    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd;
  end;

end;

procedure TAnalisisSalary.SalaryChartBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
  begin
    ParamByName('a$office$id').AsInteger := SalaryOfficeOFFICEID.AsInteger;

    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd - 1;
  end;
end;

procedure TAnalisisSalary.SalaryOfficeAfterPost(DataSet: TDataSet);
begin
  SalaryOffice.UpdateTransaction.CommitRetaining;
end;

procedure TAnalisisSalary.SalaryPersonCalcFields(DataSet: TDataSet);
begin
  SalaryPersonTOTALPERSONQ.AsCurrency := SalaryPersonQBEFORE.AsCurrency + SalaryPersonQMIDDLE.AsCurrency + SalaryPersonQAFTER.AsCurrency;

  SalaryPersonTOTALPERSONC.AsCurrency := SalaryPersonCBEFORE.AsCurrency + SalaryPersonCMIDDLE.AsCurrency + SalaryPersonCAFTER.AsCurrency;
end;


procedure TAnalisisSalary.ActionRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeBegin;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);
end;

procedure TAnalisisSalary.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeBegin := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

  ActionReload.Execute;
end;

procedure TAnalisisSalary.UpdateRange;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
  BandCaption: string;
begin
  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  ButtonArray[drCustom] := ButtonRangeCustom;

  ButtonArray[drDay] := ButtonRangeDay;

  ButtonArray[drWeek] := ButtonRangeWeek;

  ButtonArray[drMonth] := ButtonRangeMonth;

  ButtonArray[drYear] := ButtonRangeYear;

  ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;

  BandCaption := DateRangeAsString(RangeBegin, RangeEnd);

  GridOfficeView.Bands[0].Caption := BandCaption;

  GridOfficeView.Bands[2].Caption := '������� ' + IntToStr(YearOf(RangeBegin) -1) + ' �.'#13#10'(����������)';

  GridOfficeView.Bands[3].Caption := '������� ' + IntToStr(YearOf(RangeBegin)) + ' �.'#13#10'(����������)';
end;


procedure TAnalisisSalary.ActionReloadExecute(Sender: TObject);
begin
  if SalaryOffice.State = dsEdit then
  begin
    SalaryOffice.Post;
  end;

  UpdateRange;

  Screen.Cursor := crHourGlass;

  try

    if SalaryOffice.Active then
    begin
      SalaryOffice.Active := False;
    end;

    if SalaryPerson.Active then
    begin
      SalaryPerson.Active := False;
    end;

    SalaryOffice.Active := True;

    SalaryPerson.Active := True;

    if OfficeID = 0 then
    begin
      GridPersonViewOFFICENAME.GroupIndex := 0;

      GridPersonView.DataController.Groups.FullExpand;
    end else
    begin
      GridPersonViewOFFICENAME.GroupIndex := -1;
    end;

  except

   on E: Exception do
   begin
     TDialog.Error(E.Message);
   end;

  end;



  Screen.Cursor := crDefault;

end;


procedure TAnalisisSalary.ButtonOfficeClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonOffice then
    begin
      GetWindowRect(Bar.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonOffice.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      MenuOffice.Popup(X, Y);
    end else

    begin
      ButtonOffice.Tag := Button.Tag;

      ActionReload.Execute;
    end;
  end;
end;

procedure TAnalisisSalary.ButtonPrintClick(Sender: TObject);
begin
  if SalaryOffice.State = dsEdit then
  begin
    SalaryOffice.Post;
  end;

  GridOfficeLevel.Caption := DateRangeAsString(RangeBegin, RangeEnd);

  GridPersonLevel.Caption := DateRangeAsString(RangeBegin, RangeEnd);

  TGridPrinter.Print(nil);
end;

procedure TAnalisisSalary.dxBarCoefButtonClick(Sender: TObject);
begin

TSalaryQuorter.Execute;

end;

procedure TAnalisisSalary.ButtonCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TAnalisisSalary.GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TAnalisisSalary.GridOfficeViewDStylesGetContentStyle(Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Index: Integer;
begin
  Index := AItem.Index;

  AStyle := StyleDefault;

  if ARecord.Values[Index] < 0 then
  begin
    AStyle := StyleRed;
  end;
end;

procedure TAnalisisSalary.ActionChartExecute(Sender: TObject);
var
  TabSheet: TTabSheet;
begin
  DialogSalaryHost := TDialogSalaryHost.Create(Self);

  SalaryOffice.First;

  while not SalaryOffice.Eof do
  begin
   SalaryChart.Active := True;

   TabSheet := TTabSheet.Create(DialogSalaryHost.Pages);

   TabSheet.PageControl := DialogSalaryHost.Pages;

   TabSheet.Caption := SalaryOfficeOFFICENAME.AsString;

   DialogSalaryChart := TDialogSalaryChart.Create(TabSheet);

   DialogSalaryChart.Align := alClient;

   DialogSalaryChart.BorderStyle := bsNone;

   DialogSalaryChart.Parent := TabSheet;

   DialogSalaryChart.AverageQ := SalaryOfficeEQ.AsFloat;

   DialogSalaryChart.AverageC := SalaryOfficeEC.AsFloat;

   DialogSalaryChart.OfficeName := SalaryOfficeOFFICENAME.AsString;

   DialogSalaryChart.DataSet := SalaryChart;

   DialogSalaryChart.Execute;

   SalaryChart.Active := False;

   SalaryOffice.Next;
  end;

  DialogSalaryHost.cbValueClick(nil);

  DialogSalaryHost.ShowModal;

  DialogSalaryHost.Free;

  DialogSalaryHost := nil;
end;


end.

