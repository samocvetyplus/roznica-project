unit frmAnalisisSalaryChart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, DB, DateUtils, StdCtrls;

type

  TDialogSalaryChart = class(TForm)
    ChartQ: TChart;
    Series2: TFastLineSeries;
    ChartC: TChart;
    FastLineSeries1: TFastLineSeries;
    FastLineSeries2: TFastLineSeries;
    Series1: TFastLineSeries;
    procedure FormResize(Sender: TObject);
  private
    FAverageQ: Double;
    FAverageC: Double;
    FDataSet: TDataSet;
    FOfficeName: string;
  public
    procedure Execute;
    property AverageQ: Double read FAverageQ  write FAverageQ;
    property AverageC: Double read FAverageC write FAverageC;
    property OfficeName: string read FOfficeName write FOfficeName;
    property DataSet: TDataSet read FDataSet write FDataSet;
  end;

var
  DialogSalaryChart: TDialogSalaryChart;

implementation

{$R *.dfm}

{ TDialogSalaryChart }

procedure TDialogSalaryChart.Execute;
var
  AQ: Double;
  AC: Double;
  BQ: Double;
  BC: Double;
  ADate: TDate;
begin
  ChartC.Series[0].Clear;

  ChartC.Series[1].Clear;

  ChartQ.Series[0].Clear;

  ChartQ.Series[1].Clear;

  if DataSet <> nil then
  begin
    Caption := '������ - ' + OfficeName;

    AQ := 0;

    AC := 0;

    DataSet.First;

    while not DataSet.Eof do
    begin
      AQ := AQ + AverageQ;

      AC := AC + AverageC;

      ADate := DateOf(DataSet.FieldByName('Date$Chart').AsDateTime);

      BQ := DataSet.FieldByName('Q$Chart').AsFloat;

      BC := DataSet.FieldByName('C$Chart').AsFloat;

      ChartC.Series[0].AddNullXY(ADate, AC);

      ChartQ.Series[0].AddNullXY(ADate, AQ);

      if ADate <= Date then
      begin
        ChartC.Series[1].AddNullXY(ADate, BC);

        ChartQ.Series[1].AddNullXY(ADate, BQ);
      end;

      DataSet.Next;
    end;

    FormResize(nil);

    Visible := True;

    //ShowModal;
  end;
end;

procedure TDialogSalaryChart.FormResize(Sender: TObject);
var
  W, H: Integer;
begin
  W := ClientWidth - 8;
  H := (ClientHeight - (4 * 3)) div 2;

  ChartQ.Top := 4;
  ChartQ.Left := 4;
  ChartQ.Width := W;
  ChartQ.Height := H;

  ChartC.Top := ChartQ.Top + H + 4;
  ChartC.Left := 4;
  ChartC.Width := W;
  ChartC.Height := H;
end;

end.


