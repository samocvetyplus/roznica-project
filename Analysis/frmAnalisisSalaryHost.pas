unit frmAnalisisSalaryHost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, ExtCtrls, IniFiles, cxPropertiesStore;

type
  TDialogSalaryHost = class(TForm)
    Pages: TPageControl;
    PanelAttribute: TPanel;
    cbValue: TCheckBox;
    procedure cbValueClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DialogSalaryHost: TDialogSalaryHost;

implementation

{$R *.dfm}

uses
  uUtils,
  frmAnalisisSalaryChart;

procedure TDialogSalaryHost.cbValueClick(Sender: TObject);
var
  i: Integer;
  TabSheet: TTabSheet;
  Dialog: TDialogSalaryChart;
begin
  for i := 0 to Pages.PageCount - 1  do
  begin
    TabSheet := Pages.Pages[i];

    Dialog := TDialogSalaryChart(TabSheet.Controls[0]);

    Dialog.ChartQ.LeftAxis.Labels := cbValue.Checked;

    Dialog.ChartC.LeftAxis.Labels := cbValue.Checked;
  end;
end;

procedure TDialogSalaryHost.FormCreate(Sender: TObject);
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(GetIniFileName);

  cbValue.Checked := IniFile.ReadBool('Salary.Chart', 'Values', True);

  Left := IniFile.ReadInteger('Salary.Chart', 'Left', Left);

  Top := IniFile.ReadInteger('Salary.Chart', 'Top', Top);

  Width := IniFile.ReadInteger('Salary.Chart', 'Width', Width);

  Height := IniFile.ReadInteger('Salary.Chart', 'Height', Height);

  IniFile.Free;
end;

procedure TDialogSalaryHost.FormDestroy(Sender: TObject);
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(GetIniFileName);

  IniFile.WriteBool('Salary.Chart', 'Values', cbValue.Checked);

  IniFile.WriteInteger('Salary.Chart', 'Left', Left);

  IniFile.WriteInteger('Salary.Chart', 'Top', Top);

  IniFile.WriteInteger('Salary.Chart', 'Width', Width);

  IniFile.WriteInteger('Salary.Chart', 'Height', Height);

  IniFile.Free;
end;

end.
