unit frmAnalisisClientSell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, StrUtils,
  dxSkinscxPCPainter, cxGridBandedTableView, cxGridDBBandedTableView, DateUtils,
  cxContainer, dxLayoutcxEditAdapters, FIBDatabase, pFIBDatabase,
  dxLayoutControl, cxTextEdit, cxDBEdit, dxLayoutLookAndFeels,
  dxSkinsdxBarPainter, dxBar, ActnList, frmPopup, DBClient, cxSplitter,
  cxCalendar, cxGroupBox, dxBarExtItems, cxGridExportLink, ShlObj, StdActns,
  dxSkinsDefaultPainters, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, ComObj, ActiveX, FIBQuery, pFIBQuery;

type

{ TAnalisisSalary }

  TAnalisisClientSell = class(TForm)
    Analisis: TpFIBDataSet;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    SourceOffice: TDataSource;
    GridView: TcxGridDBBandedTableView;
    Action: TActionList;
    ActionReload: TAction;
    ActionRange: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonPrint: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    ButtonReload: TdxBarLargeButton;
    ButtonRangeCustom: TdxBarButton;
    ButtonRangeDay: TdxBarButton;
    ButtonRangeWeek: TdxBarButton;
    ButtonRangeMonth: TdxBarButton;
    ButtonRangeYear: TdxBarButton;
    BarDock: TdxBarDockControl;
    ButtonClose: TdxBarLargeButton;
    AnalisisPHONE: TFIBStringField;
    AnalisisPHONEFLAG: TFIBIntegerField;
    AnalisisPHONEFLAGNAME: TFIBStringField;
    AnalisisNAME: TFIBStringField;
    AnalisisBIRTHDAY: TFIBDateField;
    AnalisisBIRTHDAYDAY: TFIBIntegerField;
    AnalisisBIRTHDAYMONTH: TFIBIntegerField;
    AnalisisBIRTHDAYYEAR: TFIBIntegerField;
    AnalisisOFFICEID: TFIBIntegerField;
    AnalisisOFFICENAME: TFIBStringField;
    AnalisisBUYCOUNT: TFIBIntegerField;
    AnalisisBUYSUMM: TFIBBCDField;
    AnalisisDISCOUNT: TFIBIntegerField;
    AnalisisCARD: TFIBStringField;
    AnalisisCITY: TFIBStringField;
    GridViewPHONE: TcxGridDBBandedColumn;
    GridViewPHONEFLAGNAME: TcxGridDBBandedColumn;
    GridViewNAME: TcxGridDBBandedColumn;
    GridViewBIRTHDAY: TcxGridDBBandedColumn;
    GridViewBIRTHDAYDAY: TcxGridDBBandedColumn;
    GridViewBIRTHDAYMONTH: TcxGridDBBandedColumn;
    GridViewBIRTHDAYYEAR: TcxGridDBBandedColumn;
    GridViewOFFICENAME: TcxGridDBBandedColumn;
    GridViewBUYCOUNT: TcxGridDBBandedColumn;
    GridViewBUYSUMM: TcxGridDBBandedColumn;
    GridViewDISCOUNT: TcxGridDBBandedColumn;
    GridViewCARD: TcxGridDBBandedColumn;
    GridViewCITY: TcxGridDBBandedColumn;
    ButtonPhone: TdxBarLargeButton;
    BarBottom: TdxBar;
    EditorBuyCount: TdxBarSpinEdit;
    AnalisisNAMELAST: TFIBStringField;
    AnalisisNAMEFIRST: TFIBStringField;
    AnalisisNAMEMIDDLE: TFIBStringField;
    AnalisisCNAMELAST: TStringField;
    AnalisisCNAMEFIRST: TStringField;
    AnalisisCNAMEMIDDLE: TStringField;
    AnalisisOID: TFIBIntegerField;
    ButtonExcel: TdxBarLargeButton;
    BrowseForFolder: TBrowseForFolder;
    AnalisisCALLDATE: TFIBDateTimeField;
    AnalisisCALLSTATE: TFIBIntegerField;
    AnalisisCALLSTATENAME: TFIBStringField;
    GridViewCALLDATE: TcxGridDBBandedColumn;
    GridViewCALLSTATENAME: TcxGridDBBandedColumn;
    GridViewMAXDATE: TcxGridDBBandedColumn;
    AnalisisMAXDATE: TFIBDateTimeField;
    AnalisisCALLPERSONNAME: TFIBStringField;
    AnalisisCALLOFFICENAME: TFIBStringField;
    GridViewCALLPERSONNAME: TcxGridDBBandedColumn;
    GridViewCALLOFFICENAME: TcxGridDBBandedColumn;
    GridViewNAMELAST: TcxGridDBBandedColumn;
    GridViewNAMEFIRST: TcxGridDBBandedColumn;
    GridViewNAMEMIDDLE: TcxGridDBBandedColumn;
    BrowseForFolderAll: TBrowseForFolder;
    MenuExcel: TdxBarPopupMenu;
    ButtonAll: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    GridViewOID: TcxGridDBBandedColumn;
    AnalisisMAXEMP: TStringField;
    GridViewColumn1: TcxGridDBBandedColumn;
    GridViewColumn2: TcxGridDBBandedColumn;
    AnalisisSMSMISS: TFIBIntegerField;
    OpenDialog1: TOpenDialog;
    pFIBQuery1: TpFIBQuery;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    SMSButton: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    procedure SalaryBeforeOpen(DataSet: TDataSet);
    procedure GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure ActionRangeExecute(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure EditorBuyCountCurChange(Sender: TObject);
    procedure BrowseForFolderAccept(Sender: TObject);
    procedure GridViewDataControllerGroupingChanged(Sender: TObject);
    procedure SMSButtonExecute(Sender: TObject);
  private
    RangeBegin: TDate;
    RangeEnd: TDate;
    procedure InternalExecute;
    procedure UpdateRange;
    procedure OnRangeOk(Popup: TPopup);
  public
    class procedure Execute;
  end;


implementation

{$R *.dfm}

uses

   comdata,

   uBuy, uDialog, uGridPrinter,
   dmBuy,
   frmBuyRangePopup;

{ TAnalisisSalary }
var
Excl: Variant;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
begin
  Result := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID) = S_OK;
end;

procedure TAnalisisClientSell.EditorBuyCountCurChange(Sender: TObject);
begin
  EditorBuyCount.Value := EditorBuyCount.CurValue;
end;

class procedure TAnalisisClientSell.Execute;
var
  AnalisisClientSell: TAnalisisClientSell;
begin
  AnalisisClientSell := TAnalisisClientSell.Create(nil);

  try

    AnalisisClientSell.InternalExecute;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;
  end;

  AnalisisClientSell.Free;
end;


procedure TAnalisisClientSell.FormCreate(Sender: TObject);
var
  Rect: TRect;

procedure SetupGrid;
var
  i: Integer;
  Caption: string;
begin
  for i := 0 to GridView.Bands.Count - 1 do
  begin
    Caption := GridView.Bands[i].Caption;

    Caption :=  AnsiReplaceStr(Caption, '?', #13#10);

    GridView.Bands[i].Caption := Caption;
  end;
end;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  BoundsRect := Rect;

  RangeBegin := DateOf(StartOfTheMonth(Date));

  RangeEnd := DateOf(EndOfTheMonth(Date));

  DataBuy := TDataBuy.Create(Self);

  SetupGrid;
end;

procedure TAnalisisClientSell.InternalExecute;
begin
  ShowModal;
end;

procedure TAnalisisClientSell.SalaryBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
  begin
    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd;

    ParamByName('a$buy$count').AsInteger := Trunc(EditorBuyCount.Value);

    ParamByName('a$phone$flag').AsInteger := 0;

    if ButtonPhone.Down then
    begin
      ParamByName('a$phone$flag').AsInteger := 2;
    end;
  end;
end;

procedure TAnalisisClientSell.SMSButtonExecute(Sender: TObject);
var S, phonenumber, K, phone:String;
    WorkSheet: Variant;
    Rows, Cols, i, j, z:Integer;
    f: TextFile; // ����
    fName: String[80]; // ��� �����
    buf: string[80]; // ����� ��� ������ �� �����
begin
    ShowMessage('��� ������� ��������� �������� ������ ���������, ������� �� ����� ��������� �� ���-��������. '+#13#10
              +'������ ���������� � ������� "������������ ���", ����� ��������� �� ���������� �������������� ��������� ���������'+#13#10
              +'��� �������� ������������ txt ����.� ����� ������ ���� ������ ��������� ��������� ������ �� ����,'+#13#10
              +'��� ����� ������ �������� � ������� - 79101234567 - ����� 11 ����. ������ ����� - � ����� ������.');


    //if not IsOLEObjectInstalled('Excel.Application') then
    //begin
    z := 0;
    Opendialog1.Execute;
    fName := Opendialog1.FileName; AssignFile(f, fName);

    Reset(f); // ������� ��� ������
   if IOResult <> 0 then
    begin
      MessageDlg( '������ ������� � ����� ' + fName,
      mtError, [mbOk], 0); exit
    end;
    while not EOF(f) do
      begin
      z := z+1;
      readln(f, buf); // ��������� ������ �� �����
            for j := 1 to Length(buf)  do
            begin
            if j = 1 then
            S:= S+'+'+buf[1]+'(';
            if j = 2 then
            S:= S+ buf[2];
            if j = 3 then
            S:= S+ buf[3];
            if j = 4 then
            S:= S+ buf[4]+')';
            if j = 5 then
            S:= S+ buf[5];
            if j = 6 then
            S:= S+ buf[6];
            if j = 7 then
            S:= S+ buf[7]+ '-';
            if j = 8 then
            S:= S+ buf[8];
            if j = 9 then
            S:= S+ buf[9]+'-';
            if j = 10 then
            S:= S+ buf[10];
            if j = 11 then
            S:= S+ buf[11];
            end;
        pFIBQuery1.SQL.Text:='execute procedure SMS$MISS$ADD(:PHONE);';
        pFIBQuery1.Params.ParamByName('PHONE').AsString:= S;
        pFIBQuery1.ExecQuery;
        dmCom.tr.CommitRetaining;
        S:='';
      end;

      ShowMessage('��������� '+IntToStr(z)+' ��������');

    CloseFile(f); // ������� ����
//    end
//      else
//      Excl:= CreateOleObject('Excel.Application');
//      Opendialog1.Execute;
//
//      Excl.Workbooks.Open(Opendialog1.FileName, 0, True);
//      Worksheet:= Excl.ActiveWorkbook.ActiveSheet;
//      Rows:= WorkSheet.UsedRange.Rows.Count;
//      ShowMessage('����� ��������� '+IntToStr(Rows)+' ������������� ');
//      Cols:= 1;
//
//         for i := 1 to Rows do
//         begin
//            phone := WorkSheet.UsedRange.Cells[i+1 ,Cols];
//            for j := 1 to Length(phone)  do
//            begin
//            if j = 1 then
//            S:= S+'+'+phone[1]+'(';
//            if j = 2 then
//            S:= S+ phone[2];
//            if j = 3 then
//            S:= S+ phone[3];
//            if j = 4 then
//            S:= S+ phone[4]+')';
//            if j = 5 then
//            S:= S+ phone[5];
//            if j = 6 then
//            S:= S+ phone[6];
//            if j = 7 then
//            S:= S+ phone[7]+ '-';
//            if j = 8 then
//            S:= S+ phone[8];
//            if j = 9 then
//            S:= S+ phone[9]+'-';
//            if j = 10 then
//            S:= S+ phone[10];
//            if j = 11 then
//            S:= S+ phone[11];
//            end;
//          pFIBQuery1.SQL.Text:='execute procedure SMS$MISS$ADD(:PHONE);';
//          pFIBQuery1.Params.ParamByName('PHONE').AsString:= S;
//          pFIBQuery1.ExecQuery;
//          dmCom.tr.CommitRetaining;
//          S:='';
//         end;

end;

procedure TAnalisisClientSell.ActionRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeBegin;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);
end;

procedure TAnalisisClientSell.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeBegin := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

  UpdateRange;
end;

procedure TAnalisisClientSell.UpdateRange;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
  BandCaption: string;
begin
  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  ButtonArray[drCustom] := ButtonRangeCustom;

  ButtonArray[drDay] := ButtonRangeDay;

  ButtonArray[drWeek] := ButtonRangeWeek;

  ButtonArray[drMonth] := ButtonRangeMonth;

  ButtonArray[drYear] := ButtonRangeYear;

  ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;

  BandCaption := DateRangeAsString(RangeBegin, RangeEnd);

  GridView.Bands[0].Caption := BandCaption;
end;


procedure TAnalisisClientSell.ActionReloadExecute(Sender: TObject);
begin
  UpdateRange;

  Screen.Cursor := crHourGlass;

  try

    if Analisis.Active then
    begin
      Analisis.Active := False;
    end;

    Analisis.Active := True;

    GridView.DataController.Groups.FullExpand;

    GridView.ApplyBestFit;

    GridView.DataController.Groups.FullCollapse;

    GridView.Controller.TopRowIndex := 0;

    GridView.Controller.FocusedRowIndex := 0;

  except

   on E: Exception do
   begin
     TDialog.Error(E.Message);
   end;

  end;

  Screen.Cursor := crDefault;
end;


procedure TAnalisisClientSell.ButtonPrintClick(Sender: TObject);
begin
  GridLevel.Caption := DateRangeAsString(RangeBegin, RangeEnd);

  TGridPrinter.Print(nil);
end;


procedure TAnalisisClientSell.ButtonCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TAnalisisClientSell.BrowseForFolderAccept(Sender: TObject);
var
  id: Integer;
  Folder: string;
  OfficeArray: TClientDataSet;
begin
  if Sender = BrowseForFolder then
  begin

  if not Analisis.IsEmpty then
  begin
    Folder := IncludeTrailingBackslash(BrowseForFolder.Folder);

    GridView.DataController.Groups.ClearGrouping;

    GridView.Bands[1].Visible := False;

    OfficeArray := TClientDataSet.Create(Self);

    OfficeArray.Data := DataBuy.Table(sqlOffice);

    while not OfficeArray.Eof do
    begin
      Analisis.Filtered := False;

      Analisis.Filter := 'oid = ' + OfficeArray.FieldByName('id').AsString;

      Analisis.Filtered := True;

      Analisis.First;

      if Analisis.RecordCount <> 0 then
      begin
        GridView.Controller.TopRowIndex := 0;

        if GridView.DataController.RecordCount <> 0 then
        begin
          ExportGridToExcel(Folder + AnsiLowerCase(OfficeArray.FieldByName('name').AsString) + '.xls', grid, False, True, True, 'xls');
        end;
      end;

      OfficeArray.Next;
    end;

    OfficeArray.Free;

    Analisis.Filtered := False;

    GridView.Controller.TopRowIndex := 0;

    GridView.Bands[1].Visible := True;
  end;

  end else

  begin

  if not Analisis.IsEmpty then
  begin
    Folder := IncludeTrailingBackslash(BrowseForFolderAll.Folder);

    GridView.DataController.Groups.ClearGrouping;

    GridView.Bands[1].Visible := False;

    GridView.Controller.TopRowIndex := 0;

    ExportGridToExcel(Folder + '��� ��������' + '.xls', grid, False, True, True, 'xls');

    GridView.Controller.TopRowIndex := 0;

    GridView.Bands[1].Visible := True;
  end;

  end;
end;

procedure TAnalisisClientSell.GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TAnalisisClientSell.GridViewDataControllerGroupingChanged(Sender: TObject);
begin
  GridView.DataController.Groups.FullCollapse;

  GridView.Controller.TopRowIndex := 0;

  GridView.Controller.FocusedRowIndex := 0;
end;

end.

