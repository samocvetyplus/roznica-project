object AnalisisClientSell: TAnalisisClientSell
  Left = 266
  Top = 148
  Caption = #1040#1085#1072#1083#1080#1079' '#1082#1083#1080#1077#1085#1090#1086#1074
  ClientHeight = 548
  ClientWidth = 1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    AlignWithMargins = True
    Left = 4
    Top = 84
    Width = 1072
    Height = 464
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = alClient
    TabOrder = 0
    object GridView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourceOffice
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skSum
          Position = spFooter
          FieldName = 'BUY$COUNT'
          Column = GridViewBUYCOUNT
        end
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'BUY$SUMM'
          Column = GridViewBUYSUMM
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = GridViewBUYSUMM
        end>
      DataController.Summary.SummaryGroups = <>
      DataController.OnGroupingChanged = GridViewDataControllerGroupingChanged
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.Indicator = True
      OptionsView.BandHeaderLineCount = 2
      Bands = <
        item
          Caption = #1055#1077#1088#1080#1086#1076
          Width = 2242
        end
        item
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 0
          Width = 343
        end
        item
          Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 1
          Width = 302
        end
        item
          Caption = #1044#1077#1085#1100' '#1088#1086#1078#1076#1077#1085#1080#1103
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 2
          Width = 319
        end
        item
          Caption = #1058#1077#1083#1077#1092#1086#1085
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 3
          Width = 215
        end
        item
          Caption = #1055#1086#1082#1091#1087#1082#1080
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 4
          Width = 260
        end
        item
          Caption = #1047#1074#1086#1085#1086#1082
          Position.BandIndex = 0
          Position.ColIndex = 6
          Width = 411
        end
        item
          Caption = #1055#1088#1086#1095#1077#1077
          Options.HoldOwnColumnsOnly = True
          Position.BandIndex = 0
          Position.ColIndex = 5
          Width = 208
        end
        item
          Caption = #1053#1077#1076#1086#1089#1090#1072#1074#1083#1077#1085#1086
          Position.BandIndex = 0
          Position.ColIndex = 7
          Width = 184
        end>
      OnCustomDrawBandHeader = GridViewCustomDrawBandHeader
      object GridViewCITY: TcxGridDBBandedColumn
        Caption = #1043#1086#1088#1086#1076
        DataBinding.FieldName = 'CITY'
        Visible = False
        HeaderAlignmentHorz = taCenter
        Width = 169
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewOFFICENAME: TcxGridDBBandedColumn
        Caption = #1052#1072#1075#1072#1079#1080#1085
        DataBinding.FieldName = 'OFFICE$NAME'
        HeaderAlignmentHorz = taCenter
        Width = 189
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewNAME: TcxGridDBBandedColumn
        Caption = #1060'.'#1048'.'#1054'.'
        DataBinding.FieldName = 'NAME'
        HeaderAlignmentHorz = taCenter
        Width = 101
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewPHONEFLAGNAME: TcxGridDBBandedColumn
        Caption = '*'
        DataBinding.FieldName = 'PHONE$FLAG$NAME'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 4
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewPHONE: TcxGridDBBandedColumn
        DataBinding.FieldName = 'PHONE'
        HeaderAlignmentHorz = taCenter
        Options.Grouping = False
        Position.BandIndex = 4
        Position.ColIndex = 1
        Position.RowIndex = 0
        IsCaptionAssigned = True
      end
      object GridViewBIRTHDAY: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'BIRTHDAY'
        HeaderAlignmentHorz = taCenter
        Width = 89
        Position.BandIndex = 3
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewBIRTHDAYDAY: TcxGridDBBandedColumn
        Caption = #1044#1077#1085#1100
        DataBinding.FieldName = 'BIRTHDAY$DAY'
        HeaderAlignmentHorz = taCenter
        Width = 82
        Position.BandIndex = 3
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewBIRTHDAYMONTH: TcxGridDBBandedColumn
        Caption = #1052#1077#1089#1103#1094
        DataBinding.FieldName = 'BIRTHDAY$MONTH'
        HeaderAlignmentHorz = taCenter
        Width = 81
        Position.BandIndex = 3
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewBIRTHDAYYEAR: TcxGridDBBandedColumn
        Caption = #1043#1086#1076
        DataBinding.FieldName = 'BIRTHDAY$YEAR'
        HeaderAlignmentHorz = taCenter
        Width = 81
        Position.BandIndex = 3
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewBUYCOUNT: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'BUY$COUNT'
        HeaderAlignmentHorz = taCenter
        SortIndex = 0
        SortOrder = soAscending
        Position.BandIndex = 5
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewBUYSUMM: TcxGridDBBandedColumn
        Caption = #1057#1091#1084#1084#1072
        DataBinding.FieldName = 'BUY$SUMM'
        HeaderAlignmentHorz = taCenter
        Options.Grouping = False
        Position.BandIndex = 5
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewDISCOUNT: TcxGridDBBandedColumn
        Caption = #1057#1082#1080#1076#1082#1072
        DataBinding.FieldName = 'DISCOUNT'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 7
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewCARD: TcxGridDBBandedColumn
        Caption = #1050#1072#1088#1090#1072
        DataBinding.FieldName = 'CARD'
        HeaderAlignmentHorz = taCenter
        Options.Grouping = False
        Options.Sorting = False
        Position.BandIndex = 7
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewCALLDATE: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'CALL$DATE'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 6
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object GridViewMAXDATE: TcxGridDBBandedColumn
        Caption = #1055#1086#1089#1083#1077#1076#1085#1103#1103
        DataBinding.FieldName = 'MAX$DATE'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 5
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewCALLSTATENAME: TcxGridDBBandedColumn
        Caption = #1056#1077#1072#1082#1094#1080#1103
        DataBinding.FieldName = 'CALL$STATE$NAME'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 6
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewCALLPERSONNAME: TcxGridDBBandedColumn
        Caption = #1057#1086#1090#1088#1091#1076#1085#1080#1082
        DataBinding.FieldName = 'CALL$PERSON$NAME'
        HeaderAlignmentHorz = taCenter
        Width = 126
        Position.BandIndex = 6
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewCALLOFFICENAME: TcxGridDBBandedColumn
        Caption = #1060#1080#1083#1080#1072#1083
        DataBinding.FieldName = 'CALL$OFFICE$NAME'
        HeaderAlignmentHorz = taCenter
        Width = 110
        Position.BandIndex = 6
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewNAMELAST: TcxGridDBBandedColumn
        Caption = #1060#1072#1084#1080#1083#1080#1103
        DataBinding.FieldName = 'NAME$LAST'
        HeaderAlignmentHorz = taCenter
        Width = 67
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object GridViewNAMEFIRST: TcxGridDBBandedColumn
        Caption = #1048#1084#1103
        DataBinding.FieldName = 'NAME$FIRST'
        HeaderAlignmentHorz = taCenter
        Width = 55
        Position.BandIndex = 2
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewNAMEMIDDLE: TcxGridDBBandedColumn
        Caption = #1054#1090#1095#1077#1089#1090#1074#1086
        DataBinding.FieldName = 'NAME$MIDDLE'
        HeaderAlignmentHorz = taCenter
        Width = 54
        Position.BandIndex = 2
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewOID: TcxGridDBBandedColumn
        DataBinding.FieldName = 'OID'
        Visible = False
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object GridViewColumn1: TcxGridDBBandedColumn
        Caption = #1055#1088#1086#1076#1072#1074#1077#1094
        DataBinding.FieldName = 'MAX$EMP'
        Position.BandIndex = 5
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object GridViewColumn2: TcxGridDBBandedColumn
        Caption = 'SMS'
        DataBinding.FieldName = 'SMS$MISS'
        FooterAlignmentHorz = taCenter
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 8
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridView
    end
  end
  object BarDock: TdxBarDockControl
    AlignWithMargins = True
    Left = 4
    Top = 4
    Width = 1072
    Height = 52
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = dalTop
    BarManager = BarManager
  end
  object Analisis: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      
        'select h.*, office$id oid from client$history$all3(:a$date$begin' +
        ', :a$date$end, :a$buy$count, :a$phone$flag) h')
    BeforeOpen = SalaryBeforeOpen
    Transaction = DataBuy.TransactionRead
    Database = dmCom.db
    UpdateTransaction = DataBuy.TransactionWrite
    RefreshTransactionKind = tkUpdateTransaction
    Left = 16
    Top = 248
    oRefreshAfterPost = False
    oFetchAll = True
    object AnalisisPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisPHONEFLAG: TFIBIntegerField
      FieldName = 'PHONE$FLAG'
    end
    object AnalisisPHONEFLAGNAME: TFIBStringField
      FieldName = 'PHONE$FLAG$NAME'
      Size = 4
      EmptyStrToNull = True
    end
    object AnalisisNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object AnalisisBIRTHDAY: TFIBDateField
      Alignment = taCenter
      FieldName = 'BIRTHDAY'
    end
    object AnalisisBIRTHDAYDAY: TFIBIntegerField
      FieldName = 'BIRTHDAY$DAY'
    end
    object AnalisisBIRTHDAYMONTH: TFIBIntegerField
      FieldName = 'BIRTHDAY$MONTH'
    end
    object AnalisisBIRTHDAYYEAR: TFIBIntegerField
      FieldName = 'BIRTHDAY$YEAR'
    end
    object AnalisisOFFICEID: TFIBIntegerField
      FieldName = 'OFFICE$ID'
    end
    object AnalisisOFFICENAME: TFIBStringField
      FieldName = 'OFFICE$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object AnalisisBUYCOUNT: TFIBIntegerField
      FieldName = 'BUY$COUNT'
      DisplayFormat = '0'
    end
    object AnalisisBUYSUMM: TFIBBCDField
      FieldName = 'BUY$SUMM'
      DisplayFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
    object AnalisisDISCOUNT: TFIBIntegerField
      FieldName = 'DISCOUNT'
    end
    object AnalisisCARD: TFIBStringField
      FieldName = 'CARD'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisCITY: TFIBStringField
      FieldName = 'CITY'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisNAMELAST: TFIBStringField
      FieldName = 'NAME$LAST'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisNAMEFIRST: TFIBStringField
      FieldName = 'NAME$FIRST'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisNAMEMIDDLE: TFIBStringField
      FieldName = 'NAME$MIDDLE'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisCNAMELAST: TStringField
      FieldKind = fkCalculated
      FieldName = 'C$NAME$LAST'
      Size = 32
      Calculated = True
    end
    object AnalisisCNAMEFIRST: TStringField
      FieldKind = fkCalculated
      FieldName = 'C$NAME$FIRST'
      Size = 32
      Calculated = True
    end
    object AnalisisCNAMEMIDDLE: TStringField
      FieldKind = fkCalculated
      FieldName = 'C$NAME$MIDDLE'
      Size = 32
      Calculated = True
    end
    object AnalisisOID: TFIBIntegerField
      FieldName = 'OID'
    end
    object AnalisisCALLDATE: TFIBDateTimeField
      Alignment = taCenter
      FieldName = 'CALL$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:nn:ss'
    end
    object AnalisisCALLSTATE: TFIBIntegerField
      FieldName = 'CALL$STATE'
    end
    object AnalisisCALLSTATENAME: TFIBStringField
      FieldName = 'CALL$STATE$NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object AnalisisMAXDATE: TFIBDateTimeField
      FieldName = 'MAX$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:nn:ss'
    end
    object AnalisisCALLPERSONNAME: TFIBStringField
      FieldName = 'CALL$PERSON$NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object AnalisisCALLOFFICENAME: TFIBStringField
      FieldName = 'CALL$OFFICE$NAME'
      EmptyStrToNull = True
    end
    object AnalisisMAXEMP: TStringField
      FieldName = 'MAX$EMP'
    end
    object AnalisisSMSMISS: TFIBIntegerField
      FieldName = 'SMS$MISS'
    end
  end
  object SourceOffice: TDataSource
    DataSet = Analisis
    Left = 48
    Top = 248
  end
  object Action: TActionList
    Images = DataBuy.Image32
    Left = 16
    Top = 184
    object ActionRange: TAction
      Caption = #1055#1077#1088#1080#1086#1076
      ImageIndex = 14
      OnExecute = ActionRangeExecute
    end
    object ActionReload: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = ActionReloadExecute
    end
    object BrowseForFolder: TBrowseForFolder
      Caption = #1057' '#1088#1072#1079#1073#1080#1077#1085#1080#1077#1084' '#1087#1086' '#1084#1072#1075#1072#1079#1080#1085#1072#1084
      DialogCaption = #1057' '#1088#1072#1079#1073#1080#1077#1085#1080#1077#1084' '#1087#1086' '#1084#1072#1075#1072#1079#1080#1085#1072#1084
      BrowseOptions = [bifUseNewUI]
      ImageIndex = 26
      OnAccept = BrowseForFolderAccept
    end
    object BrowseForFolderAll: TBrowseForFolder
      Caption = #1054#1076#1085#1080#1084' '#1092#1072#1081#1083#1086#1084
      DialogCaption = #1054#1076#1085#1080#1084' '#1092#1072#1081#1083#1086#1084
      BrowseOptions = [bifUseNewUI]
      ImageIndex = 26
      OnAccept = BrowseForFolderAccept
    end
    object SMSButton: TAction
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '
      ImageIndex = 42
      OnExecute = SMSButtonExecute
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Print'
      'Common'
      'Range')
    Categories.ItemsVisibles = (
      2
      2
      0)
    Categories.Visibles = (
      True
      True
      True)
    ImageOptions.LargeImages = DataBuy.Image32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
    PopupMenuLinks = <>
    ShowShortCutInHint = True
    Style = bmsUseLookAndFeel
    UseSystemFont = True
    Left = 16
    Top = 216
    DockControlHeights = (
      0
      0
      24
      0)
    object Bar: TdxBar
      AllowCustomizing = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Bar'
      CaptionButtons = <>
      DockControl = BarDock
      DockedDockControl = BarDock
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 530
      FloatTop = 0
      FloatClientWidth = 64
      FloatClientHeight = 550
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonRange'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonPrint'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonPhone'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonExcel'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonReload'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarBottom: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 1181
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 50
          Visible = True
          ItemName = 'EditorBuyCount'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object ButtonPrint: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 6
      ShortCut = 16464
      OnClick = ButtonPrintClick
      AutoGrayScale = False
      Width = 64
    end
    object ButtonPhone: TdxBarLargeButton
      Caption = #1058#1077#1083#1077#1092#1086#1085
      Category = 0
      Hint = #1058#1077#1083#1077#1092#1086#1085
      Visible = ivAlways
      ButtonStyle = bsChecked
      LargeImageIndex = 24
      AutoGrayScale = False
    end
    object EditorBuyCount: TdxBarSpinEdit
      Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1082#1091#1087#1086#1082
      Category = 0
      Hint = #1050#1086#1083'-'#1074#1086' '#1087#1086#1082#1091#1087#1086#1082
      Visible = ivAlways
      OnCurChange = EditorBuyCountCurChange
      ShowCaption = True
      Width = 100
      MaxValue = 1000000.000000000000000000
      MinValue = 1.000000000000000000
      Value = 1.000000000000000000
    end
    object ButtonExcel: TdxBarLargeButton
      Caption = 'Excel'
      Category = 0
      Hint = 'Excel'
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = MenuExcel
      LargeImageIndex = 26
      AutoGrayScale = False
      Width = 64
    end
    object ButtonAll: TdxBarLargeButton
      Action = BrowseForFolderAll
      Category = 0
    end
    object ButtonOffice: TdxBarLargeButton
      Action = BrowseForFolder
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Caption = '&Open...'
      Category = 0
      Hint = 'Open|Opens an existing file'
      Visible = ivAlways
      ImageIndex = 7
      ShortCut = 16463
    end
    object dxBarButton2: TdxBarButton
      Caption = 'SMSButton'
      Category = 0
      Hint = #1053#1072#1078#1084#1080#1090#1077', '#1095#1090#1086#1073#1099' '#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '#1087#1086' '#1082#1086#1090#1086#1088#1099#1084' '#1085#1077' '#1076#1086#1096#1083#1072' '#1057#1052#1057'-'#1088#1072#1089#1089#1099#1083#1082#1072
      Visible = ivAlways
      ImageIndex = 40
      ShortCut = 16463
      OnClick = SMSButtonExecute
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = SMSButton
      Caption = #1053#1077#1072#1082#1090#1080#1074#1085#1099#1077' '#1085#1086#1084#1077#1088#1072' '#1057#1052#1057
      Category = 0
      Description = #1053#1072#1078#1084#1080#1090#1077', '#1095#1090#1086#1073#1099' '#1079#1072#1075#1088#1091#1079#1080#1090#1100' '#1085#1086#1084#1077#1088#1072' '#1076#1086' '#1082#1086#1090#1086#1088#1099#1093' '#1085#1077' '#1076#1086#1096#1083#1072' '#1057#1052#1057'-'#1088#1072#1089#1089#1099#1083#1082#1072
      ShortCut = 16463
      AutoGrayScale = False
    end
    object ButtonRange: TdxBarLargeButton
      Action = ActionRange
      Category = 1
      ShortCut = 16464
      AutoGrayScale = False
      Width = 64
    end
    object ButtonReload: TdxBarLargeButton
      Action = ActionReload
      Category = 1
      ShortCut = 16466
      AutoGrayScale = False
      Width = 64
    end
    object ButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Category = 1
      Hint = #1047#1072#1082#1088#1099#1090#1100
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = ButtonCloseClick
      AutoGrayScale = False
    end
    object ButtonRangeCustom: TdxBarButton
      Caption = 'Custom'
      Category = 2
      Hint = 'Custom'
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object ButtonRangeDay: TdxBarButton
      Caption = 'Day'
      Category = 2
      Hint = 'Day'
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object ButtonRangeWeek: TdxBarButton
      Caption = 'Week'
      Category = 2
      Hint = 'Week'
      Visible = ivAlways
      LargeImageIndex = 13
    end
    object ButtonRangeMonth: TdxBarButton
      Caption = 'Month'
      Category = 2
      Hint = 'Month'
      Visible = ivAlways
      LargeImageIndex = 14
    end
    object ButtonRangeYear: TdxBarButton
      Caption = 'Year'
      Category = 2
      Hint = 'Year'
      Visible = ivAlways
      LargeImageIndex = 15
    end
  end
  object MenuExcel: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'ButtonAll'
      end
      item
        Visible = True
        ItemName = 'ButtonOffice'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    Left = 48
    Top = 216
  end
  object TdxBarSeparator
    Category = -1
    Visible = ivAlways
  end
  object OpenDialog1: TOpenDialog
    Left = 16
    Top = 280
  end
  object pFIBQuery1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 312
  end
end
