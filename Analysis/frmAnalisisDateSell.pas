unit frmAnalisisDateSell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, StrUtils,
  dxSkinscxPCPainter, cxGridBandedTableView, cxGridDBBandedTableView, DateUtils,
  cxContainer, dxLayoutcxEditAdapters, FIBDatabase, pFIBDatabase,
  dxLayoutControl, cxTextEdit, cxDBEdit, dxLayoutLookAndFeels,
  dxSkinsdxBarPainter, dxBar, ActnList, frmPopup, DBClient, cxSplitter,
  cxCalendar, cxGroupBox, dxBarExtItems, cxGridExportLink, ShlObj, StdActns,
  dxSkinsDefaultPainters, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, ComObj, ActiveX, FIBQuery, pFIBQuery, cxCheckBox, cxBarEditItem,
  MidasLib, Provider, pFIBClientDataSet;

type

{ TAnalisisSalary }

  TAnalisisDateSell = class(TForm)
    Analisis: TpFIBDataSet;
    Grid: TcxGrid;
    SourceOffice: TDataSource;
    Action: TActionList;
    ActionReload: TAction;
    ActionRange: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonPrint: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    ButtonReload: TdxBarLargeButton;
    ButtonRangeCustom: TdxBarButton;
    ButtonRangeDay: TdxBarButton;
    ButtonRangeWeek: TdxBarButton;
    ButtonRangeMonth: TdxBarButton;
    ButtonRangeYear: TdxBarButton;
    BarDock: TdxBarDockControl;
    ButtonClose: TdxBarLargeButton;
    ButtonPhone: TdxBarLargeButton;
    BarBottom: TdxBar;
    EditorBuyCount: TdxBarSpinEdit;
    ButtonExcel: TdxBarLargeButton;
    BrowseForFolder: TBrowseForFolder;
    BrowseForFolderAll: TBrowseForFolder;
    MenuExcel: TdxBarPopupMenu;
    ButtonAll: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    OpenDialog1: TOpenDialog;
    pFIBQuery1: TpFIBQuery;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    SMSButton: TAction;
    dxBarLargeButton1: TdxBarLargeButton;
    GridLevel: TcxGridLevel;
    GridView: TcxGridDBBandedTableView;
    GridViewADATE: TcxGridDBBandedColumn;
    GridViewCLIENTNAME: TcxGridDBBandedColumn;
    GridViewNODCARD: TcxGridDBBandedColumn;
    GridViewCODE: TcxGridDBBandedColumn;
    GridViewMATID: TcxGridDBBandedColumn;
    GridViewGOODID: TcxGridDBBandedColumn;
    GridViewColumn7: TcxGridDBBandedColumn;
    GridViewART: TcxGridDBBandedColumn;
    GridViewART2: TcxGridDBBandedColumn;
    GridViewColumn10: TcxGridDBBandedColumn;
    GridViewSDATE: TcxGridDBBandedColumn;
    GridViewSPRICE: TcxGridDBBandedColumn;
    GridViewSUP: TcxGridDBBandedColumn;
    GridViewSN: TcxGridDBBandedColumn;
    GridViewColumn15: TcxGridDBBandedColumn;
    GridViewColumn16: TcxGridDBBandedColumn;
    GridViewPRICE: TcxGridDBBandedColumn;
    GridViewPRICE0: TcxGridDBBandedColumn;
    GridViewCOST: TcxGridDBBandedColumn;
    GridViewCOST0: TcxGridDBBandedColumn;
    GridViewDISCOUNT: TcxGridDBBandedColumn;
    GridViewColumn22: TcxGridDBBandedColumn;
    GridViewW: TcxGridDBBandedColumn;
    GridViewSZ: TcxGridDBBandedColumn;
    GridViewUID: TcxGridDBBandedColumn;
    GridViewColumn27: TcxGridDBBandedColumn;
    GridViewColumn29: TcxGridDBBandedColumn;
    GridViewCASHIERNAME: TcxGridDBBandedColumn;
    GridViewDEPNAME: TcxGridDBBandedColumn;
    AnalisisADATE: TFIBDateTimeField;
    AnalisisCLIENTNAME: TFIBStringField;
    AnalisisNODCARD: TFIBStringField;
    AnalisisPRICE: TFIBFloatField;
    AnalisisPRICE0: TFIBFloatField;
    AnalisisW: TFIBFloatField;
    AnalisisSZ: TFIBStringField;
    AnalisisUID: TFIBIntegerField;
    AnalisisFULLART: TFIBStringField;
    AnalisisART2: TFIBStringField;
    AnalisisUNITID: TFIBSmallIntField;
    AnalisisCHECKNO: TFIBIntegerField;
    AnalisisDISCOUNT: TFIBFloatField;
    AnalisisEMP: TFIBStringField;
    AnalisisSDATE: TFIBDateTimeField;
    AnalisisSPRICE: TFIBFloatField;
    AnalisisSUP: TFIBStringField;
    AnalisisSN: TFIBIntegerField;
    AnalisisRQ: TFIBIntegerField;
    AnalisisRW: TFIBFloatField;
    AnalisisART: TFIBStringField;
    AnalisisCODE: TFIBStringField;
    AnalisisINSID: TFIBStringField;
    AnalisisGOODID: TFIBStringField;
    AnalisisMATID: TFIBStringField;
    AnalisisCASHIERNAME: TFIBStringField;
    AnalisisCOST: TFIBFloatField;
    AnalisisCOST0: TFIBFloatField;
    AnalisisDEPNAME: TFIBStringField;
    GridViewCODEFULL: TcxGridDBBandedColumn;
    AnalisisCODEFULL: TFIBStringField;
    GridViewSUMMARY: TcxGridDBBandedColumn;
    maxsumm: TcxBarEditItem;
    SummComp: TpFIBDataSet;
    SummCompCLIENTID: TFIBIntegerField;
    SummCompCOMPID: TFIBIntegerField;
    AnalisisCLIENTID: TFIBIntegerField;
    SummCompProvider: TDataSetProvider;
    SummCompCL: TClientDataSet;
    SummCompCLSUMMARY: TFloatField;
    SummCompCLCLIENTID: TIntegerField;
    SummCompCLCOMPID: TIntegerField;
    AnalisisProvider: TpFIBDataSetProvider;
    AnalisisCL: TpFIBClientDataSet;
    AnalisisCLADATE: TDateTimeField;
    AnalisisCLCLIENTNAME: TStringField;
    AnalisisCLNODCARD: TStringField;
    AnalisisCLPRICE: TFloatField;
    AnalisisCLPRICE0: TFloatField;
    AnalisisCLW: TFloatField;
    AnalisisCLSZ: TStringField;
    AnalisisCLUID: TIntegerField;
    AnalisisCLFULLART: TStringField;
    AnalisisCLART2: TStringField;
    AnalisisCLUNITID: TSmallintField;
    AnalisisCLCHECKNO: TIntegerField;
    AnalisisCLDISCOUNT: TFloatField;
    AnalisisCLEMP: TStringField;
    AnalisisCLSDATE: TDateTimeField;
    AnalisisCLSPRICE: TFloatField;
    AnalisisCLSUP: TStringField;
    AnalisisCLSN: TIntegerField;
    AnalisisCLRQ: TIntegerField;
    AnalisisCLRW: TFloatField;
    AnalisisCLART: TStringField;
    AnalisisCLCODE: TStringField;
    AnalisisCLINSID: TStringField;
    AnalisisCLGOODID: TStringField;
    AnalisisCLMATID: TStringField;
    AnalisisCLCASHIERNAME: TStringField;
    AnalisisCLCOST: TFloatField;
    AnalisisCLCOST0: TFloatField;
    AnalisisCLDEPNAME: TStringField;
    AnalisisCLCODEFULL: TStringField;
    AnalisisCLCLIENTID: TIntegerField;
    AnalisisCLSUMMARY: TCurrencyField;
    AnalisisCOMPID: TFIBIntegerField;
    AnalisisCLCOMPID: TIntegerField;
    MenuGrid: TPopupMenu;
    N1: TMenuItem;
    SummCompSUMMARY: TFIBFloatField;
    SummCountDate: TpFIBDataSet;
    SummCountDateProvider: TDataSetProvider;
    AnalisisCLSummCountDate: TCurrencyField;
    GridViewSummCountDate: TcxGridDBBandedColumn;
    GridViewAllCount: TcxGridDBBandedColumn;
    SummCountDateBUYCOUNT: TFIBIntegerField;
    SummCountDateCLIENTID: TFIBIntegerField;
    SummCountDateBUYSUMM: TFIBBCDField;
    SummCountDateCL: TClientDataSet;
    SummCountDateCLBUYCOUNT: TIntegerField;
    SummCountDateCLCLIENTID: TIntegerField;
    SummCountDateCLBUYSUMM: TBCDField;
    AnalisisCLAllCount: TIntegerField;
    procedure SalaryBeforeOpen(DataSet: TDataSet);
    procedure GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure ActionRangeExecute(Sender: TObject);
    procedure ButtonPrintClick(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure EditorBuyCountCurChange(Sender: TObject);
    procedure BrowseForFolderAccept(Sender: TObject);
    procedure GridViewDataControllerGroupingChanged(Sender: TObject);
    procedure SMSButtonExecute(Sender: TObject);
    procedure maxsummChange(Sender: TObject);
    procedure SummCompBeforeOpen(DataSet: TDataSet);
    procedure AnalisisCLCalcFields(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure SummCountDateBeforeOpen(DataSet: TDataSet);
  private
    RangeBegin: TDate;
    RangeEnd: TDate;
    procedure InternalExecute;
    procedure UpdateRange;
    procedure OnRangeOk(Popup: TPopup);
  public
    class procedure Execute;
  end;


implementation

{$R *.dfm}

uses

   comdata,
   data,

   uBuy, uDialog, uGridPrinter,
   dmBuy,
   frmBuyRangePopup;

{ TAnalisisSalary }
var
Excl: Variant;
check:integer;

function IsOLEObjectInstalled(Name: String): boolean;
var
  ClassID: TCLSID;
begin
  Result := CLSIDFromProgID(PWideChar(WideString(Name)), ClassID) = S_OK;
end;

procedure TAnalisisDateSell.EditorBuyCountCurChange(Sender: TObject);
begin
  EditorBuyCount.Value := EditorBuyCount.CurValue;
end;

class procedure TAnalisisDateSell.Execute;
var
  AnalisisDateSell: TAnalisisDateSell;
begin
  AnalisisDateSell := TAnalisisDateSell.Create(nil);

  try

    AnalisisDateSell.InternalExecute;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;
  end;

  AnalisisDateSell.Free;
end;


procedure TAnalisisDateSell.FormCreate(Sender: TObject);
var
  Rect: TRect;

procedure SetupGrid;
var
  i: Integer;
  Caption: string;
begin
  for i := 0 to GridView.Bands.Count - 1 do
  begin
    Caption := GridView.Bands[i].Caption;

    Caption :=  AnsiReplaceStr(Caption, '?', #13#10);

    GridView.Bands[i].Caption := Caption;
  end;
end;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  BoundsRect := Rect;

  RangeBegin := DateOf(StartOfTheMonth(Date));

  RangeEnd := DateOf(EndOfTheMonth(Date));

  DataBuy := TDataBuy.Create(Self);

  SetupGrid;

  check:=2;
end;

procedure TAnalisisDateSell.InternalExecute;
begin
  ShowModal;
end;

procedure TAnalisisDateSell.maxsummChange(Sender: TObject);
begin
if  check=2 then  check:=1
else  check:=2;
end;

procedure TAnalisisDateSell.N1Click(Sender: TObject);
begin
GridView.Controller.ClearGrouping;
end;

procedure TAnalisisDateSell.SalaryBeforeOpen(DataSet: TDataSet);

begin

  with TpFIBDataSet(Analisis) do
  begin
    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd;

//if check = 1  then
//  begin
//     ParamByName('fiag$summ').AsInteger := 1;
//    end
//    else
//     ParamByName('fiag$summ').AsInteger := 2;



    //ParamByName('a$buy$count').AsInteger := Trunc(EditorBuyCount.Value);

    //ParamByName('a$phone$flag').AsInteger := 0;

//    if ButtonPhone.Down then
//    begin
//      ParamByName('a$phone$flag').AsInteger := 2;
//    end;
  end;
end;

procedure TAnalisisDateSell.SMSButtonExecute(Sender: TObject);
var S, phonenumber, K, phone:String;
    WorkSheet: Variant;
    Rows, Cols, i, j, z:Integer;
    f: TextFile; // ����
    fName: String[80]; // ��� �����
    buf: string[80]; // ����� ��� ������ �� �����
begin
    ShowMessage('��� ������� ��������� �������� ������ ���������, ������� �� ����� ��������� �� ���-��������. '+#13#10
              +'������ ���������� � ������� "������������ ���", ����� ��������� �� ���������� �������������� ��������� ���������'+#13#10
              +'��� �������� ������������ txt ����.� ����� ������ ���� ������ ��������� ��������� ������ �� ����,'+#13#10
              +'��� ����� ������ �������� � ������� - 79101234567 - ����� 11 ����. ������ ����� - � ����� ������.');


    //if not IsOLEObjectInstalled('Excel.Application') then
    //begin
    z := 0;
    Opendialog1.Execute;
    fName := Opendialog1.FileName; AssignFile(f, fName);

    Reset(f); // ������� ��� ������
   if IOResult <> 0 then
    begin
      MessageDlg( '������ ������� � ����� ' + fName,
      mtError, [mbOk], 0); exit
    end;
    while not EOF(f) do
      begin
      z := z+1;
      readln(f, buf); // ��������� ������ �� �����
            for j := 1 to Length(buf)  do
            begin
            if j = 1 then
            S:= S+'+'+buf[1]+'(';
            if j = 2 then
            S:= S+ buf[2];
            if j = 3 then
            S:= S+ buf[3];
            if j = 4 then
            S:= S+ buf[4]+')';
            if j = 5 then
            S:= S+ buf[5];
            if j = 6 then
            S:= S+ buf[6];
            if j = 7 then
            S:= S+ buf[7]+ '-';
            if j = 8 then
            S:= S+ buf[8];
            if j = 9 then
            S:= S+ buf[9]+'-';
            if j = 10 then
            S:= S+ buf[10];
            if j = 11 then
            S:= S+ buf[11];
            end;
        pFIBQuery1.SQL.Text:='execute procedure SMS$MISS$ADD(:PHONE);';
        pFIBQuery1.Params.ParamByName('PHONE').AsString:= S;
        pFIBQuery1.ExecQuery;
        dmCom.tr.CommitRetaining;
        S:='';
      end;

      ShowMessage('��������� '+IntToStr(z)+' ��������');

    CloseFile(f); // ������� ����
//    end
//      else
//      Excl:= CreateOleObject('Excel.Application');
//      Opendialog1.Execute;
//
//      Excl.Workbooks.Open(Opendialog1.FileName, 0, True);
//      Worksheet:= Excl.ActiveWorkbook.ActiveSheet;
//      Rows:= WorkSheet.UsedRange.Rows.Count;
//      ShowMessage('����� ��������� '+IntToStr(Rows)+' ������������� ');
//      Cols:= 1;
//
//         for i := 1 to Rows do
//         begin
//            phone := WorkSheet.UsedRange.Cells[i+1 ,Cols];
//            for j := 1 to Length(phone)  do
//            begin
//            if j = 1 then
//            S:= S+'+'+phone[1]+'(';
//            if j = 2 then
//            S:= S+ phone[2];
//            if j = 3 then
//            S:= S+ phone[3];
//            if j = 4 then
//            S:= S+ phone[4]+')';
//            if j = 5 then
//            S:= S+ phone[5];
//            if j = 6 then
//            S:= S+ phone[6];
//            if j = 7 then
//            S:= S+ phone[7]+ '-';
//            if j = 8 then
//            S:= S+ phone[8];
//            if j = 9 then
//            S:= S+ phone[9]+'-';
//            if j = 10 then
//            S:= S+ phone[10];
//            if j = 11 then
//            S:= S+ phone[11];
//            end;
//          pFIBQuery1.SQL.Text:='execute procedure SMS$MISS$ADD(:PHONE);';
//          pFIBQuery1.Params.ParamByName('PHONE').AsString:= S;
//          pFIBQuery1.ExecQuery;
//          dmCom.tr.CommitRetaining;
//          S:='';
//         end;

end;

procedure TAnalisisDateSell.SummCompBeforeOpen(DataSet: TDataSet);
begin

 with TpFIBDataSet(SummComp) do
  begin
    ParamByName('BD').AsDate := RangeBegin;

    ParamByName('ED').AsDate := RangeEnd;

  end;
end;

procedure TAnalisisDateSell.SummCountDateBeforeOpen(DataSet: TDataSet);
begin
with TpFIBDataSet(SummCountDate) do
  begin
    ParamByName('BD').AsDate := RangeBegin;

    ParamByName('ED').AsDate := RangeEnd;

  end;
end;

procedure TAnalisisDateSell.ActionRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeBegin;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);
end;

procedure TAnalisisDateSell.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeBegin := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

  UpdateRange;
end;

procedure TAnalisisDateSell.UpdateRange;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
  BandCaption: string;
begin
  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  ButtonArray[drCustom] := ButtonRangeCustom;

  ButtonArray[drDay] := ButtonRangeDay;

  ButtonArray[drWeek] := ButtonRangeWeek;

  ButtonArray[drMonth] := ButtonRangeMonth;

  ButtonArray[drYear] := ButtonRangeYear;

  ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;

  BandCaption := DateRangeAsString(RangeBegin, RangeEnd);

  GridView.Bands[0].Caption := BandCaption;
end;


procedure TAnalisisDateSell.ActionReloadExecute(Sender: TObject);
begin
  UpdateRange;

  Screen.Cursor := crHourGlass;

  SummCompCL.Active:=false;

  SummCountDateCL.Active:=false;

  AnalisisCL.active:=false;
  
  with TpFIBDataSet(SummComp) do
  begin
    ParamByName('BD').AsDate := RangeBegin;

    ParamByName('ED').AsDate := RangeEnd;

  end;


   SummComp.Open;


     with TpFIBDataSet(SummCountDate) do
  begin
    ParamByName('BD').AsDate := RangeBegin;

    ParamByName('ED').AsDate := RangeEnd;

  end;

   SummCountDate.Open;

   SummCompCL.IndexFieldNames:= 'clientid;compid';

   SummCountDateCL.IndexFieldNames:= 'clientid';
   
   SummCountDateCL.Active:=true;
   
   SummCompCL.Active:=true;
  try

    if Analisis.Active then
    begin
      Analisis.Active := False;
    end;

    //Analisis.Active := True;
    Analisis.Open;

    GridView.DataController.Groups.FullExpand;

    GridView.ApplyBestFit;

    GridView.DataController.Groups.FullCollapse;

    GridView.Controller.TopRowIndex := 0;

    GridView.Controller.FocusedRowIndex := 0;

  except

   on E: Exception do
   begin
     TDialog.Error(E.Message);
   end;

  end;
  AnalisisCL.active:=true;
  Screen.Cursor := crDefault;
end;


procedure TAnalisisDateSell.AnalisisCLCalcFields(DataSet: TDataSet);
var i,j:integer;
begin
 if SummCompCL.FindKey( [AnalisisCLCLIENTID.Asinteger, AnalisisCLCOMPID.Asinteger ]) then
  begin
    AnalisisCLSUMMARY.value := SummCompCLSUMMARY.Asfloat;
  end;

  i:=SummCountDateCLclientid.asInteger;
  j:=AnalisisCLCLIENTID.Asinteger;
   if SummCountDateCL.FindKey( [AnalisisCLCLIENTID.Asinteger]) then
  begin
    AnalisisCLSummCountDate.value := SummCountDateCLBUYSUMM.Asfloat;
     AnalisisCLallcount.value := SummCountDateCLBUYcount.AsInteger;
  end;
end;

procedure TAnalisisDateSell.ButtonPrintClick(Sender: TObject);
begin
  GridLevel.Caption := DateRangeAsString(RangeBegin, RangeEnd);

  TGridPrinter.Print(nil);
end;


procedure TAnalisisDateSell.ButtonCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TAnalisisDateSell.BrowseForFolderAccept(Sender: TObject);
var
  id: Integer;
  Folder: string;
  OfficeArray: TClientDataSet;
begin
  if Sender = BrowseForFolder then
  begin

  if not Analisis.IsEmpty then
  begin
    Folder := IncludeTrailingBackslash(BrowseForFolder.Folder);

    GridView.DataController.Groups.ClearGrouping;

    GridView.Bands[1].Visible := False;

    OfficeArray := TClientDataSet.Create(Self);

    OfficeArray.Data := DataBuy.Table(sqlOffice);

    while not OfficeArray.Eof do
    begin
      Analisis.Filtered := False;

      Analisis.Filter := 'oid = ' + OfficeArray.FieldByName('id').AsString;

      Analisis.Filtered := True;

      Analisis.First;

      if Analisis.RecordCount <> 0 then
      begin
        GridView.Controller.TopRowIndex := 0;

        if GridView.DataController.RecordCount <> 0 then
        begin
          ExportGridToExcel(Folder + AnsiLowerCase(OfficeArray.FieldByName('name').AsString) + '.xls', grid, False, True, True, 'xls');
        end;
      end;

      OfficeArray.Next;
    end;

    OfficeArray.Free;

    Analisis.Filtered := False;

    GridView.Controller.TopRowIndex := 0;

    GridView.Bands[1].Visible := True;
  end;

  end else

  begin

  if not Analisis.IsEmpty then
  begin
    Folder := IncludeTrailingBackslash(BrowseForFolderAll.Folder);

    GridView.DataController.Groups.ClearGrouping;

    GridView.Bands[1].Visible := False;

    GridView.Controller.TopRowIndex := 0;

    ExportGridToExcel(Folder + '��� ��������' + '.xls', grid, False, True, True, 'xls');

    GridView.Controller.TopRowIndex := 0;

    GridView.Bands[1].Visible := True;
  end;

  end;
end;

procedure TAnalisisDateSell.GridViewCustomDrawBandHeader(Sender: TcxGridBandedTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridBandHeaderViewInfo; var ADone: Boolean);
begin
  AViewInfo.MultiLinePainting := True;
end;

procedure TAnalisisDateSell.GridViewDataControllerGroupingChanged(Sender: TObject);
begin
  GridView.DataController.Groups.FullCollapse;

  GridView.Controller.TopRowIndex := 0;

  GridView.Controller.FocusedRowIndex := 0;
end;

end.

