object DialogSalaryChart: TDialogSalaryChart
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = #1043#1088#1072#1092#1080#1082
  ClientHeight = 629
  ClientWidth = 753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object ChartQ: TChart
    AlignWithMargins = True
    Left = 9
    Top = 9
    Width = 410
    Height = 250
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Title.Text.Strings = (
      #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086)
    BottomAxis.DateTimeFormat = 'dd'
    View3D = False
    BevelOuter = bvNone
    TabOrder = 0
    PrintMargins = (
      15
      28
      15
      28)
    object Series1: TFastLineSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Visible = False
      Title = #1055#1083#1072#1085
      LinePen.Color = clRed
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object Series2: TFastLineSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Visible = False
      Title = #1060#1072#1082#1090
      LinePen.Color = clGreen
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
  object ChartC: TChart
    AlignWithMargins = True
    Left = 9
    Top = 238
    Width = 409
    Height = 250
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Title.Text.Strings = (
      #1057#1091#1084#1084#1072)
    BottomAxis.DateTimeFormat = 'dd'
    View3D = False
    View3DOptions.Zoom = 102
    BevelOuter = bvNone
    TabOrder = 1
    object FastLineSeries1: TFastLineSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Visible = False
      Title = #1055#1083#1072#1085
      LinePen.Color = clRed
      XValues.DateTime = True
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
    object FastLineSeries2: TFastLineSeries
      Marks.Callout.Brush.Color = clBlack
      Marks.Visible = False
      Title = #1060#1072#1082#1090
      LinePen.Color = clGreen
      XValues.Name = 'X'
      XValues.Order = loAscending
      YValues.Name = 'Y'
      YValues.Order = loNone
    end
  end
end
