unit frmSalaryQuorter;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ExtCtrls, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus,
  StdCtrls, Grids, DBGridEh, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, StrUtils,
  dxSkinscxPCPainter, cxGridBandedTableView, cxGridDBBandedTableView, DateUtils,
  cxContainer, dxLayoutcxEditAdapters, FIBDatabase, pFIBDatabase,
  dxLayoutControl, cxTextEdit, cxDBEdit, dxLayoutLookAndFeels,
  dxSkinsdxBarPainter, dxBar, ActnList, frmPopup, DBClient, cxSplitter,
  cxCalendar, dxSkinsDefaultPainters, dxRibbonForm, dxRibbon, dxRibbonFormCaptionHelper,
  dxSkinsdxRibbonPainter, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, ComCtrls, TeEngine, Series, TeeProcs, Chart;

type
  TSalaryQuorter = class(TdxRibbonForm)
    GridOffice: TcxGrid;
    GridOfficeView: TcxGridDBBandedTableView;
    GridOfficeViewNAME: TcxGridDBBandedColumn;
    GridOfficeViewKQ: TcxGridDBBandedColumn;
    GridOfficeViewKC: TcxGridDBBandedColumn;
    GridOfficeLevel: TcxGridLevel;
    BarDock: TdxBarDockControl;
    Ribbon: TdxRibbon;
    SalaryQuorter1: TpFIBDataSet;
    SourceQuorter: TDataSource;
    Styles: TcxStyleRepository;
    StyleRed: TcxStyle;
    StyleDefault: TcxStyle;
    Action: TActionList;
    ActionRange: TAction;
    ActionReload: TAction;
    ActionChart: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    BarAccess: TdxBar;
    ButtonPrint: TdxBarLargeButton;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ButtonRange: TdxBarLargeButton;
    ButtonReload: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    ButtonOffice: TdxBarLargeButton;
    ButtonRangeCustom: TdxBarButton;
    ButtonRangeDay: TdxBarButton;
    ButtonRangeWeek: TdxBarButton;
    ButtonRangeMonth: TdxBarButton;
    ButtonRangeYear: TdxBarButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    MenuOffice: TdxBarPopupMenu;
    fbstrngfldSalaryQuorter1OFFICENAME: TFIBStringField;
    fbcdfldSalaryQuorter1KCA: TFIBBCDField;
    fbcdfldSalaryQuorter1KQA: TFIBBCDField;
    SalaryQuorter2: TpFIBDataSet;
    SalaryQuorter2OFFICENAME: TFIBStringField;
    SalaryQuorter2KCA: TFIBBCDField;
    SalaryQuorter2KQA: TFIBBCDField;
    procedure ButtonOfficeClick(Sender: TObject);
    procedure ActionRangeExecute(Sender: TObject);
    procedure ActionReloadExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SalaryQuorter1BeforeOpen(DataSet: TDataSet);
    procedure ButtonCloseClick(Sender: TObject);
    procedure SalaryQuorter2BeforeOpen(DataSet: TDataSet);
  private
    RangeBegin: TDate;
    RangeEnd: TDate;
    procedure InternalExecute;
    procedure UpdateRange;
    procedure OnRangeOk(Popup: TPopup);
    function GetOfficeID: Integer;
    procedure SetOfficeID(const Value: Integer);
  public
    class procedure Execute;
    property OfficeID: Integer read GetOfficeID write SetOfficeID;
  end;

var
  SalaryQuorter: TSalaryQuorter;

implementation


uses

   comdata, data,

   uBuy, uDialog, uGridPrinter,
   dmBuy,
   frmBuyRangePopup, frmAnalisisSalary,
   frmAnalisisSalaryHost;

{$R *.dfm}

class procedure TSalaryQuorter.Execute;
var
  SalaryQuorter: TSalaryQuorter;
begin

  SalaryQuorter := TSalaryQuorter.Create(nil);

  try

    SalaryQuorter.InternalExecute;

  except

    on E: Exception do
    begin
      TDialog.Error(E.Message);
    end;
  end;

  SalaryQuorter.Free;
end;

procedure TSalaryQuorter.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);

  Rect.Bottom := Rect.Bottom + (GetSystemMetrics(SM_CYCAPTION) + GetDefaultWindowBordersWidth(Handle).Top);

  BoundsRect := Rect;

  RangeBegin := DateOf(StartOfTheMonth(Date));

  RangeEnd := DateOf(EndOfTheMonth(Date));

  if DataBuy.IsCenterDepartment then
  begin
    OfficeID := 0;
  end else
  begin
    OfficeID := DataBuy.SelfDepartmentID;
  end;
end;

procedure TSalaryQuorter.InternalExecute;
begin

ShowModal;

end;

function TSalaryQuorter.GetOfficeID: Integer;
begin
  Result := ButtonOffice.Tag;
end;

procedure TSalaryQuorter.SalaryQuorter1BeforeOpen(DataSet: TDataSet);
begin

  with TpFIBDataSet(DataSet) do
  begin
    ParamByName('a$office$id').AsInteger := OfficeID;

    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd;
  end;

end;


procedure TSalaryQuorter.SalaryQuorter2BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
  begin
    ParamByName('a$office$id').AsInteger := OfficeID;

    ParamByName('a$date$begin').AsDate := RangeBegin;

    ParamByName('a$date$end').AsDate := RangeEnd;
  end;
end;

procedure TSalaryQuorter.SetOfficeID(const Value: Integer);
begin
  ButtonOffice.Tag := Value;
end;


procedure TSalaryQuorter.ActionRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(Bar.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeBegin;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);
end;


procedure TSalaryQuorter.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeBegin := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

  ActionReload.Execute;
end;

procedure TSalaryQuorter.UpdateRange;
var
  DateRange: TDateRange;
  ButtonArray: array[TDateRange] of TdxBarButton;
  BandCaption: string;
begin
  DateRange := DateRangeAsRange(RangeBegin, RangeEnd);

  ButtonArray[drCustom] := ButtonRangeCustom;

  ButtonArray[drDay] := ButtonRangeDay;

  ButtonArray[drWeek] := ButtonRangeWeek;

  ButtonArray[drMonth] := ButtonRangeMonth;

  ButtonArray[drYear] := ButtonRangeYear;

  ButtonRange.LargeImageIndex := ButtonArray[DateRange].LargeImageIndex;

  BandCaption := DateRangeAsString(RangeBegin, RangeEnd);
end;



procedure TSalaryQuorter.ActionReloadExecute(Sender: TObject);
begin

  UpdateRange;

  Screen.Cursor := crHourGlass;

  try

    if SalaryQuorter2.Active then
    begin
      SalaryQuorter2.Active := False;
    end;

    SalaryQuorter2.Active := True;

  finally;

  Screen.Cursor := crDefault;
  end;

end;

procedure TSalaryQuorter.ButtonCloseClick(Sender: TObject);
begin
Close;
end;

procedure TSalaryQuorter.ButtonOfficeClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonOffice then
    begin
      GetWindowRect(Bar.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonOffice.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      MenuOffice.Popup(X, Y);
    end else

    begin
      ButtonOffice.Tag := Button.Tag;

      ActionReload.Execute;
    end;
  end;
end;

end.
