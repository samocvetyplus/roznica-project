unit SuspItemList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGridEh, Menus, TB2Item,
  M207Ctrls, ActnList, jpeg, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmSuspItemList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siAdd: TSpeedItem;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siEdit: TSpeedItem;
    siHelp: TSpeedItem;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    dg1: TDBGridEh;
    pmdep: TTBPopupMenu;
    fr1: TM207FormStorage;
    acList: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acView: TAction;
    acClose: TAction;
    acHelp: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure pmDepClick (Sender: TObject);
  end;

var
  fmSuspItemList: TfmSuspItemList;

implementation

uses data, comdata, data3, M207Proc, SuspItem, MsgDialog;

{$R *.dfm}

procedure TfmSuspItemList.pmDepClick (Sender: TObject);
begin
 if TTBItem(Sender).Tag=0 then begin
  dm3.SuspItemDepid1:=-MaxInt;
  dm3.SuspItemDepid2:=MaxInt;
 end
 else begin
  dm3.SuspItemDepid1:=TTBItem(Sender).Tag;
  dm3.SuspItemDepid2:=TTBItem(Sender).Tag;
 end;
 laDepFrom.Caption:=TTBItem(Sender).Caption;
 ReOpenDataSets([dm3.taSuspItemList]);
end;

procedure TfmSuspItemList.FormCreate(Sender: TObject);
var pm:TTBItem;
begin
 tb1.WallPaper:=wp;
 tb2.WallPaper:=wp;

 {���������� ���� ���������}
 if CenterDep then
 begin
  dm3.SuspItemDepid1:=-MaxInt;
  dm3.SuspItemDepid2:=MaxInt;

  pm:=TTBItem.Create(pmdep);
  pm.Caption:='��� ������';
  pm.Tag:=0;
  pm.OnClick:=pmDepClick;

  pmdep.Items.Add(pm);

  {��������� �������}
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid, Centerdep from d_dep where d_depid<>-1000 and ISDelete <>1 ';
  dm.quTmp.ExecQuery;

  while not (dm.qutmp.Eof) do
  begin
   pm:=TTBItem.Create(pmdep);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;

   pmdep.Items.Add(pm);
   dm.qutmp.Next;
  end;
  dm.qutmp.Transaction.CommitRetaining;
  dm.qutmp.Close;
  laDepFrom.Caption:='��� ������';
 end
 else
 begin
  dm3.SuspItemDepid1:=SelfDepId;
  dm3.SuspItemDepid2:=SelfDepId;

  pm:=TTBItem.Create(pmdep);
  pm.Caption:=SelfDepName;
  pm.Tag:=SelfDepId;
  pm.OnClick:=pmDepClick;

  pmdep.Items.Add(pm);
  laDepFrom.Caption:=SelfDepName;
 end;
{*************************************************************}
 ReOpenDataSets([dm3.taSuspItemList]);
end;

procedure TfmSuspItemList.acAddExecute(Sender: TObject);
begin
 dm3.taSuspItemList.Append;
 dm3.taSuspItemList.Post;
 ShowAndFreeForm(TfmSuspItem, Self, TForm(fmSuspItem), True, False);
 dm3.taSuspItemList.Refresh;
 dg1.SumList.RecalcAll;
end;

procedure TfmSuspItemList.acAddUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (dm3.SuspItemDepid1=SelfDepId)
end;

procedure TfmSuspItemList.acDelExecute(Sender: TObject);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then  SysUtils.Abort;
  dm3.taSuspItemList.Delete;
end;

procedure TfmSuspItemList.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (dm3.taSuspItemListDEPID.AsInteger=SelfDepId) and (not dm3.taSuspItemListSINVID.IsNull)
end;

procedure TfmSuspItemList.acViewExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmSuspItem, Self, TForm(fmSuspItem), True, False);
 dm3.taSuspItemList.Refresh;
 dg1.SumList.RecalcAll;
end;

procedure TfmSuspItemList.acViewUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taSuspItemListSINVID.IsNull)
end;

procedure TfmSuspItemList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSuspItemList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSuspItemList.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmSuspItemList.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if column.Field<>nil then
 begin
  if column.FieldName='SNAME' then Background:=dm3.taSuspItemListDEPCOLOR.AsInteger; 
 end
end;

procedure TfmSuspItemList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 PostDataSets([dm3.taSuspItemList]);
 CloseDataSets([dm3.taSuspItemList]);
end;

end.
