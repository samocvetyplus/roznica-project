unit ArtNav;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ComCtrls, ExtCtrls;

type
  TfrArtNav = class(TFrame)
    Panel5: TPanel;
    sbUp: TSpeedButton;
    sbDown: TSpeedButton;
    lb1: TListBox;
    tv1: TTreeView;
    procedure sbUpClick(Sender: TObject);
    procedure tv1Change(Sender: TObject; Node: TTreeNode);
    procedure tv1Expanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses comdata, DBTree;

{$R *.DFM}

procedure TfrArtNav.sbUpClick(Sender: TObject);
begin
  ArtBtnClick(TComponent(Sender).Tag, lb1, tv1);
end;

procedure TfrArtNav.tv1Change(Sender: TObject; Node: TTreeNode);
begin
  dmCom.ArtTvChange(Node);
end;

procedure TfrArtNav.tv1Expanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  dmCom.ArtTreeExpanding(tv1, Node);
end;

procedure TfrArtNav.tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
begin
  GetNodeImages(Node);
end;

procedure TfrArtNav.tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
begin
  GetSelNodeImages(Node);
end;

end.
