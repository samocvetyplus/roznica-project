unit DSZ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls,  Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, rxPlacemnt, rxSpeedbar;

type
  TfmDSZ = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    M207IBGrid1: TM207IBGrid;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;  public
  public
  end;

var
  fmDSZ: TfmDSZ;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmDSZ.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  Caption:=dm.quD_WHArt.AsString+' '+dm.quD_WHArt2.AsString;
  with dm do
    OpenDataSets([quD_SZ]);

end;

procedure TfmDSZ.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDSZ.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDSZ.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmDSZ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    CloseDataSets([quD_SZ]);
end;

end.
