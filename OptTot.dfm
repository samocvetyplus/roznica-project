object frOptTotal: TfrOptTotal
  Left = 0
  Top = 0
  Width = 141
  Height = 48
  TabOrder = 0
  object pa2: TPanel
    Tag = 1
    Left = 0
    Top = 0
    Width = 141
    Height = 48
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object laQC: TLabel
      Left = 4
      Top = 18
      Width = 37
      Height = 13
      Caption = '���-��:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laWC: TLabel
      Left = 4
      Top = 32
      Width = 22
      Height = 13
      Caption = '���:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtDQ: TDBText
      Left = 50
      Top = 18
      Width = 25
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DQ'
      DataSource = dm.dsD_WHA2SZ
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtDW: TDBText
      Left = 51
      Top = 32
      Width = 28
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DW'
      DataSource = dm.dsD_WHA2SZ
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 6
      Top = 2
      Width = 42
      Height = 13
      Caption = '������:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtSZ: TDBText
      Left = 52
      Top = 2
      Width = 23
      Height = 13
      AutoSize = True
      DataField = 'SZ'
      DataSource = dm.dsD_WHA2SZ
    end
    object Bevel1: TBevel
      Left = 90
      Top = 0
      Width = 3
      Height = 49
      Shape = bsLeftLine
    end
    object Bevel2: TBevel
      Left = 0
      Top = 16
      Width = 207
      Height = 5
      Shape = bsTopLine
    end
    object Bevel3: TBevel
      Left = 46
      Top = 16
      Width = 5
      Height = 33
      Shape = bsLeftLine
    end
    object Label2: TLabel
      Left = 96
      Top = 2
      Width = 35
      Height = 13
      Caption = '�����'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtQDT: TDBText
      Left = 97
      Top = 18
      Width = 32
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DQ'
      DataSource = dm.dsD_A2Total
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtWDT: TDBText
      Left = 96
      Top = 32
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DW'
      DataSource = dm.dsD_A2Total
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
end
