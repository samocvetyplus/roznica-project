object SALE_ART_FORM: TSALE_ART_FORM
  Left = 0
  Top = 0
  Caption = #1059#1090#1086#1095#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
  ClientHeight = 323
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 541
    Height = 13
    Align = alTop
    Alignment = taCenter
    BiDiMode = bdLeftToRight
    Caption = #1059#1090#1086#1095#1085#1080#1090#1077' '#1082#1072#1082#1086#1077' '#1080#1079' '#1080#1079#1076#1077#1083#1080#1081' '#1073#1099#1083#1086' '#1087#1088#1086#1076#1072#1085#1086' '#1087#1086' '#1080#1085#1076#1080#1074#1080#1076#1091#1072#1083#1100#1085#1086#1084#1091' '#1079#1072#1082#1072#1079#1091
    ParentBiDiMode = False
    ExplicitWidth = 363
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 13
    Width = 541
    Height = 308
    Align = alTop
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = cxGrid1DBTableView1CellClick
      DataController.DataSource = dmIndividualOrder.DataSource_ART_SELL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1086' '#1087#1088#1086#1076#1072#1078#1072#1093
      object ART: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ART'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object UID: TcxGridDBColumn
        Caption = #1048#1085#1076'. '#1085#1086#1084#1077#1088' (UID)'
        DataBinding.FieldName = 'UID'
        HeaderAlignmentHorz = taCenter
        Width = 120
      end
      object EMP: TcxGridDBColumn
        Caption = #1055#1088#1086#1076#1072#1074#1077#1094
        DataBinding.FieldName = 'EMP'
        HeaderAlignmentHorz = taCenter
        Width = 150
      end
      object CLIENTNAME: TcxGridDBColumn
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        DataBinding.FieldName = 'CLIENTNAME'
        Width = 165
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
end
