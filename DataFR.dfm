object dmFR: TdmFR
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 277
  Top = 108
  Height = 329
  Width = 512
  object com: TCommPortDriver
    ComPort = pnCOM1
    OnReceiveData = comReceiveData
    Left = 16
    Top = 8
  end
  object tm: TTimer
    Enabled = False
    Interval = 3500
    OnTimer = tmTimer
    Left = 64
    Top = 8
  end
  object taCheck: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID, ATYPE, NAME, LINE, POS, FLAG '
      'FROM ACHECK'
      'ORDER BY LINE, POS')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 112
    Top = 8
    object taCheckID: TIntegerField
      FieldName = 'ID'
    end
    object taCheckNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taCheckLINE: TSmallintField
      FieldName = 'LINE'
    end
    object taCheckPOS: TSmallintField
      FieldName = 'POS'
    end
    object taCheckFLAG: TFIBStringField
      FieldName = 'FLAG'
      Size = 1
      EmptyStrToNull = True
    end
    object taCheckNAMED: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAMED'
      Calculated = True
    end
    object taCheckATYPE: TSmallintField
      FieldName = 'ATYPE'
      Origin = 'ACHECK.ATYPE'
    end
  end
  object dsCheck: TDataSource
    DataSet = taCheck
    Left = 112
    Top = 56
  end
  object taIns: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select D.Name, I.Quantity, I.Weight, I.Color, I.CHROMATICITY, I.' +
        'CLEANNES, E.Name EdgType, E1.Name EdgShape, I.Gr'
      
        'from Ins I left outer join D_EDGETION E on I.EDGTYPEID = E.EDGET' +
        'IONID'
      
        '           left outer join D_EDGETION E1 on I.EDGSHAPEID = E1.ED' +
        'GETIONID,'
      '     D_Ins D'
      'where I.ART2ID = ?ART2ID and'
      '      I.D_INSID = D.D_INSID')
    DataSource = dm.dsSellItem
    OnCalcFields = taInsCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 160
    Top = 8
    object taInsNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object taInsQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
    end
    object taInsWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object taInsCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object taInsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object taInsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object taInsEDGTYPE: TFIBStringField
      FieldName = 'EDGTYPE'
      EmptyStrToNull = True
    end
    object taInsEDGSHAPE: TFIBStringField
      FieldName = 'EDGSHAPE'
      EmptyStrToNull = True
    end
    object taInsGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object taInsN: TStringField
      FieldKind = fkCalculated
      FieldName = 'N'
      Size = 100
      Calculated = True
    end
  end
end
