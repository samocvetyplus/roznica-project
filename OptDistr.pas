unit OptDistr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, SpeedBar, StdCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, Buttons, pFIBDataSet, Placemnt, Menus,
  DBCtrls, DTotal;

type
  TfmOptDistr = class(TForm)
    sb2: TStatusBar;
    tb1: TSpeedBar;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    dgWH: TM207IBGrid;
    Panel4: TPanel;
    Label13: TLabel;
    edArt: TEdit;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    Splitter2: TSplitter;
    Panel1: TPanel;
    pa1: TPanel;
    paDepFrom: TPanel;
    dg1: TM207IBGrid;
    sb1: TScrollBox;
    FormStorage1: TFormStorage;
    Splitter6: TSplitter;
    pm1: TPopupMenu;
    N1: TMenuItem;
    pm2: TPopupMenu;
    MenuItem1: TMenuItem;
    miSupFilter: TMenuItem;
    miPriceFilter: TMenuItem;
    paUID: TPanel;
    dg2: TM207IBGrid;
    Label4: TLabel;
    laSup: TLabel;
    Label5: TLabel;
    laPrice: TLabel;
    frDTotal1: TfrDTotal;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure N1Click(Sender: TObject);
    procedure dg2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetFilter(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    lWH: TList;
    lUID: TList;
    lTot: TList;
    Closing: boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure BeforeOpenWH(DataSet: TDataSet);
    procedure BeforeOpenUID(DataSet: TDataSet);
    procedure AftOpen(DataSet: TDataSet);
    procedure ShowUID(Sender: TObject);
    procedure AddClick(Sender: TObject);
    procedure DelClick(Sender: TObject);
    procedure ViewClick(Sender: TObject);
    function  GetSZGrid(DepId: integer): TM207IBGrid;
    function  GetUIDGrid(DepId: integer): TM207IBGrid;
    procedure PostUIDBeforeClose(DataSet: TDataSet);
    procedure ReopenAfterPost(DataSet: TDataSet);
    procedure UIDBeforeEdit(DataSet: TDataSet);
  public

    { Public declarations }
    Canceled: boolean;
    procedure UpdateWh;

  end;

var
  fmOptDistr: TfmOptDistr;

implementation

uses comdata, Data, DBTree, Rxstrutils, Distred, Data2, OptTot, OptBSel, M207Proc;

{$R *.DFM}

procedure TfmOptDistr.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmOptDistr.FormCreate(Sender: TObject);
var p, p1, p2: TPanel;
    g, g1: TM207IBGrid;
    ds: TDataSource;
    q: TpFIBDataSet;
    pm: TPopUpMenu;
    mi: TMenuItem;
    sbAdd, sbDel, sbView: TSpeedButton;
    fr: TfrOptTotal;
    i, w, k: integer;
    t1, t2: TDateTime;
    ed: TDBEdit;

procedure CreatePanel;
var i: integer;
begin
  with dm, dm2.quBuyer do
    begin
      p:=TPanel.Create(sb1);
      p.Width:=pa1.Width;
      p.Align:=alLeft;
      p.Parent:=sb1;

      p2:=TPanel.Create(p);
      p2.Height:=paDepFrom.Height;// div 2;
      p2.Align:=alTop;
      with  p2.Font do
        begin
          Style:=Style+[fsBold];
          Color:=clNavy;
        end;
      p2.Caption:=Fields[2].AsString;
      p2.BevelOuter:=bvLowered;
      p2.Parent:=p;


      p1:=TPanel.Create(p);
      p1.Height:=26;//paDepFrom.Height
      p1.Align:=alTop;
      with  p1.Font do
        begin
          Style:=Style+[fsBold];
          Color:=clNavy;
        end;
      p1.Caption:='';//Fields[1].AsString;
      p1.BevelOuter:=bvLowered;
      p1.Parent:=p;

      sbAdd:=TSpeedButton.Create(p1);
      sbAdd.Top:=2;
      sbAdd.Left:=2;
      sbAdd.Glyph:=dm.bmpAdd;
      sbAdd.OnClick:=AddClick;
      sbAdd.ShowHint:=True;
      sbAdd.Hint:='��������';
      sbAdd.Tag:=Fields[0].AsInteger;
      sbAdd.Flat:=True;
      sbAdd.Parent:=p1;

      sbDel:=TSpeedButton.Create(p1);
      sbDel.Top:=2;
      sbDel.Left:=4+sbAdd.Width;
      sbDel.Glyph:=dm.bmpDel;
      sbDel.OnClick:=DelClick;
      sbDel.ShowHint:=True;
      sbDel.Hint:='�������';
      sbDel.Tag:=Fields[0].AsInteger;
      sbDel.Flat:=True;
      sbDel.Parent:=p1;

      sbView:=TSpeedButton.Create(p1);
      sbView.Top:=2;
      sbView.Left:=6+sbDel.Width+sbAdd.Width;
      sbView.Glyph:=dm.bmpZoomIn;
      sbView.OnClick:=ViewClick;
      sbView.ShowHint:=True;
      sbView.Hint:='��������';
      sbView.Tag:=Fields[0].AsInteger;
      sbView.Flat:=True;
      sbView.Parent:=p1;

      ed:=TDBEdit.Create(p1);
      ed.Top:=2;
      ed.Left:=8+sbDel.Width+sbAdd.Width+sbView.Width;
      ed.Width:=60;
      ed.ShowHint:=True;
      ed.Hint:='����';
      ed.Color:=clInfoBk;
      ed.Tag:=Fields[0].AsInteger;
      ed.Parent:=p1;
      ed.DataField:='PRICE';
      ed.ParentFont:=False;
      ed.Font.Color:=clBlack;
      ed.Font.Style:=ed.Font.Style-[fsBold];

      fr:=TfrOptTotal.Create(p);
      fr.Align:=alBottom;
      fr.Parent:=p;

      g:=TM207IBGrid.Create(p);
      p1.Tag:=Integer(g);
      g.Align:=alClient;
      g.Columns.Assign(dg1.Columns);
      g.OnGetCellParams:=dg1GetCellParams;
      g.OnDblClick:=ShowUID;
      g.FixedCols:=1;
      g.Name:='A';
      g.Parent:=p;

      ds:=TDataSource.Create(Self);
      g.DataSource:=ds;

      i:=0;
      w:=0;
      with g do
        while i< Columns.Count-1 do
          if (Columns[i].FieldName='QUANTITY') or
             (Columns[i].FieldName='WEIGHT')  then
               begin
                 Inc(w, Columns[i].Width);
                 Columns.Delete(i)
               end
          else Inc(i);
      p.Width:=p.Width-w;
      q:=TpFIBDataSet.Create(Self);
      q.Tag:=Fields[0].AsInteger;
      q.Database:=dmCom.db;
      q.Transaction:=dmCom.tr;
      ds.DataSet:=q;

      q.SelectSQL.Text:=' SELECT SZ, DQ, DW '+
                        ' FROM OPTA2SZ_DISTR( ?ART2ID,  ?ACOMPID, ?USERID) '+
                        ' ORDER BY SZ ';

      q.RefreshSQL.Text:=' SELECT SZ, DQ, DW '+
                         ' FROM OPTA2SZ_DISTRR(?SZ, ?ART2ID,  ?ACOMPID, ?USERID) ';

      q.BeforeOpen:=BeforeOpenWH;
      q.AfterOpen:=AftOpen;
      q.AfterRefresh:=AftOpen;
      q.Active:=True;
      lWH.Add(g);


      fr.dtDQ.DataSource:=ds;
      fr.dtDW.DataSource:=ds;
      fr.dtSZ.DataSource:=ds;

      g1:=TM207IBGrid.Create(p);
      g1.Align:=alClient;
      g1.Columns.Assign(dg2.Columns);
      g1.OnGetCellParams:=dg2GetCellParams;
      g1.OnDblClick:=ShowUID;
      g1.FixedCols:=1;
      g1.Visible:=False;
      g1.Name:='B';
      g1.Parent:=p;
      g1.ReadOnly:=True;

      with g1 do
        for i:= 0 to Columns.Count-1 do
          if (Columns[i].FieldName='SUP') or
             (Columns[i].FieldName='PRICE')  then
               begin
                 Inc(w, Columns[i].Width);
                 Columns[i].Visible:=False;
               end;

      ds:=TDataSource.Create(Self);
      g1.DataSource:=ds;

      q:=TpFIBDataSet.Create(Self);
      q.Tag:=Fields[0].AsInteger;
      q.Database:=dmCom.db;
      q.Transaction:=dmCom.tr;
      ds.DataSet:=q;

      q.SelectSQL.Text:=' SELECT SITEMID, UID, W, TAG, DISTRID, PRICE  '+
                        ' FROM OPTA2UI_DISTR( ?ART2ID, ?ACOMPID, ?SZ)';

      q.RefreshSQL.Text:=' SELECT SITEMID, UID, W, TAG, DISTRID, PRICE  '+
                        ' FROM OPTA2UI_DISTR_R(?DISTRID)';

      q.DeleteSQL.Text:=' DELETE FROM DISTR WHERE DISTRID=?OLD_DISTRID';
      q.InsertSQL.Text:=' INSERT INTO DISTR(DistrID, UserId, SItemId, DepId, T) '+
                        ' VALUES (?DistrId, '+IntToStr(dmCom.UserId)+', ?SItemId, '+Fields[0].AsString+', 3)';
      q.ModifySQL.Text:='UPDATE DISTR SET PRICE=?PRICE WHERE DISTRID=?DISTRID';
      q.BeforeOpen:=BeforeOpenUID;
      q.AfterOpen:=AftOpen;
      q.BeforeClose:=PostUIDBeforeClose;
      q.AfterPost:=ReopenAfterPost;
      q.BeforeEdit:=UIDBeforeEdit;
      ed.DataSource:=ds;

      lUID.Add(g1);
//              q.Active:=True;

      pm:=TPopUpMenu.Create(Self);
      pm.Images:=dmCom.ilButtons;
      pm.Tag:=Integer(g);

      g1.PopupMenu:=pm;
      g.PopupMenu:=pm;

      mi:=TMenuItem.Create(pm);
      mi.Caption:='�������/��.������';
      mi.Tag:=Fields[0].AsInteger;
      mi.ShortCut:=ShortCut(VK_F3, []);
      mi.OnClick:=ShowUID;
      mi.ImageIndex:=27;
      pm.Items.Add(mi);

      mi:=TMenuItem.Create(pm);
      mi.Caption:='��������';
      mi.Tag:=Fields[0].AsInteger;
      mi.ShortCut:=ShortCut(VK_INSERT, []);
      mi.OnClick:=AddClick;
      mi.ImageIndex:=1;
      pm.Items.Add(mi);

      mi:=TMenuItem.Create(pm);
      mi.Caption:='�������';
      mi.Tag:=Fields[0].AsInteger;
      mi.ShortCut:=ShortCut(VK_DELETE, [ssCtrl]);
      mi.OnClick:=DelClick;
      mi.ImageIndex:=2;
      pm.Items.Add(mi);

      g.Tag:=Fields[0].AsInteger;
      g1.Tag:=Fields[0].AsInteger;
      ds:=TDataSource.Create(Self);
      q:=TpFIBDataSet.Create(Self);
      q.Tag:=Fields[0].AsInteger;
      q.Database:=dmCom.db;
      q.Transaction:=dmCom.tr;
      ds.DataSet:=q;
      q.SelectSQL.Text:=' SELECT DQ, DW'+
                        ' FROM OPTA2SZ_DISTRT( ?ART2ID,  ?ACOMPID, ?USERID) ';

      q.BeforeOpen:=BeforeOpenWH;
      lTot.Add(q);

      fr.dtQDT.DataSource:=ds;
      fr.dtWDT.DataSource:=ds;

    end;
end;

begin
  tb1.WallPaper:=wp;
  lWH:=TList.Create;
  lUID:=TList.Create;
  lTot:=TList.Create;
  dg2.Align:=alClient;
  paUID.Align:=alTop;
  paUID.Visible:=False;
  Canceled:=False;


  with dm, dm2 do
    begin
      quBuyer.Active:=True;
      fmOptBSel:=TfmOptBSel.Create(NIL);
      if fmOptBSel.ShowModal=mrCancel then
        begin
          fmOptBSel.Free;
          Canceled:=True;
          exit;
        end;
      Distr:=True;
    end;
  t1:=Now;
  with fmOptBSel.dg1 do
    if SelectedRows.Count>0 then
      begin
        for k:=0 to SelectedRows.Count-1 do
          begin
            dm2.quBuyer.Bookmark:=SelectedRows[k];
            CreatePanel;
          end
      end    
    else CreatePanel;
  fmOptBSel.Free;

  with dm, dm2 do
    begin
      FillListBoxes(lbComp, lbMat, lbGood, lbIns);
      PriceFilter:=False;
      SupFilter:=False;
      SetFilter(NIL);
    end;

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;

  lbCompClick(NIL);
  edArt.Text:='';

  paDepFrom.Caption:=dm.DDepFrom;
  sb2.SimpleText:=TimeToStr(Now-t1);
  Closing:=False;
end;

procedure TfmOptDistr.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmOptDistr.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmOptDistr.FormClose(Sender: TObject; var Action: TCloseAction);
var i: integer;
begin
  Closing:=True;
  with dm do
    begin
      CloseDataSets([taSEl, quD_WH, quD_SZ, quD_UID, quD_T]);
//      SupFilter:=
    end;

  with lWH do
    for i:=0 to Count-1 do
       TM207IBGrid(Items[i]).DataSource.DataSet.Active:=False;

  with lUID do
    for i:=0 to Count-1 do
       TM207IBGrid(Items[i]).DataSource.DataSet.Active:=False;

  with lTot do
    for i:=0 to Count-1 do
       TpFIBDataSet(Items[i]).Active:=False;

  with dmCom do
    begin
      tr.CommitRetaining;
    end;
  lWH.Free;
  lUID.Free;
  lTot.Free;
  fmOptDistr:=NIL;
end;

procedure TfmOptDistr.lbCompClick(Sender: TObject);
var i: integer;
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  with dm do
    begin
    with quD_WH do
      begin
        Active:=False;
        Open;
      end;
    with quD_SZ do
      begin
        Active:=False;
        Open;
      end;
    end;

  UpdateWH;
end;

procedure TfmOptDistr.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmOptDistr.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='SZ') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmOptDistr.BeforeOpenWH(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
    begin
      Params.ByName('ART2ID').AsInteger:=dm.quD_WHART2ID.AsInteger;
      Params.ByName('ACOMPID').AsInteger:=Tag;
      Params.ByName('USERID').AsInteger:=dmCom.UserId;
    end;
end;

procedure TfmOptDistr.UpdateWh;
var i: integer;
begin
  with lWH do
    for i:=0 to Count-1 do
      with TM207IBGrid(Items[i]).DataSource.DataSet do
        begin
          Active:=False;
          Open;
        end;

  with lUID do
    for i:=0 to Count-1 do
      with TM207IBGrid(Items[i]).DataSource.DataSet do
        if Active then
          begin
            Active:=False;
            Open;
          end

end;

procedure TfmOptDistr.N1Click(Sender: TObject);
begin
  with dg2 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=dg2;
          with dm.quD_UID do
            begin
              Active:=False;
              Open;
            end;
        end
      else dm.quD_UID.Active:=False;
    end;

  with dg1 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=dg1;
          dg1.DataSource.DataSet.Refresh;
        end
    end;

  paUID.Visible:=dg2.Visible;  
end;

procedure TfmOptDistr.dg2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='UID') then
    begin
      if TM207IBGrid(Sender).DataSource.DataSet.FieldByName('Tag').AsInteger=0 then Background:=dmCom.clMoneyGreen
      else  Background:=clAqua;
    end;
end;

procedure TfmOptDistr.ShowUID(Sender: TObject);
var g, g1: TM207IBGrid;
begin
  g:=GetSZGrid(TComponent(Sender).Tag);
  g1:=GetUIDGrid(TComponent(Sender).Tag);

  with g1 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=g1;
          with g1.DataSource.DataSet do
            begin
              Active:=False;
              Open;
            end;
        end
      end;

  with g do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=g;
//          g.DataSource.DataSet.Refresh;
          with g.DataSource.DataSet do
            begin
              Active:=False;
              Open
            end;
        end;
    end;
end;

procedure TfmOptDistr.BeforeOpenUID(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
    begin
      Params.ByName('ACOMPID').AsInteger:=Tag;
      Params.ByName('ART2ID').AsInteger:=dm.quD_WHArt2Id.AsInteger;
      Params.ByName('SZ').AsString:=TpFIBDataSet(GetSZGrid(Tag).DataSource.DataSet).FieldByName('SZ').AsString;
    end;
end;

procedure TfmOptDistr.AddClick(Sender: TObject);
var q, q1: TpFIBDataSet;
    ds: TDataSet;
    sz: string[20];
begin
  q:=TpFIBDataSet(GetSZGrid(TComponent(Sender).Tag).DataSource.DataSet);
  q1:=TpFIBDataSet(GetUIDGrid(TComponent(Sender).Tag).DataSource.DataSet);

  with dm do
  if dg1.Visible then // N->...
    begin
      ds:=dg1.DataSource.DataSet;
      if ds.Fields[0].IsNull or
        (ds.FieldByName('Quantity').AsFloat=ds.FieldByName('DQ').AsFloat) then  Exit;

      sz:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;

{      with quInsGrDistr do
        begin
          Params.ByName('ART2ID').AsInteger:=  quD_WHArt2Id.AsInteger;
          Params.ByName('DEPFROMID').AsInteger:= DDepFromId;
          Params.ByName('DEPTOID').AsInteger:=TComponent(Sender).Tag;
          Params.ByName('SZ').AsString:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;
          Params.ByName('USERID').AsInteger:=dmCom.UserId;
          Params.ByName('T').AsInteger:=3;
          Params.ByName('INVID').IsNull:=True;
          ExecQuery;
        end;}

      dmCom.tr.CommitRetaining;

      quD_SZ.Refresh;

      with quD_T do
        begin
          Active:=False;
          Open;
        end;
      ReOpenSZQ(q, sz);
      with q1 do
          begin
            Active:=False;
            Open;
          end;
    end
  else
    begin
//      if quD_UIDTag.AsInteger=1 then Exit;
      sz:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;
      if q1.Active then // 1->1
        begin
          q1.Append;
          q1.FieldByName('SITEMID').AsInteger:=quD_UIDSItemId.AsInteger;
          q1.FieldByName('UID').AsInteger:=quD_UIDUID.AsInteger;
          q1.FieldByName('W').AsFloat:=quD_UIDW.AsFloat;
          q1.FieldByName('TAG').AsInteger:=3;
          q1.FieldByName('DISTRID').AsInteger:=dmCom.GetId(15);
          q1.Post;

          q1.AfterOpen(q1);
        end
      else  // 1->N
        begin
          with quInsDistr do
            begin
              Params.ByName('UserId').AsInteger:=dmCom.UserId;
              Params.ByName('DepId').AsInteger:=TComponent(Sender).Tag;
              Params.ByName('SItemId').AsInteger:=quD_UIDSItemId.AsInteger;
              Params.ByName('T').AsInteger:=3;
              ExecQuery;
            end;
          ReOpenSZQ(q, sz);
          q1.Active:=False;
          q1.Open;
        end;

      dmCom.tr.CommitRetaining;
      ReOpenSZQ(q, sz);


      with quD_UID do
        begin
          Refresh;
//          while NOT EOF and (quD_UIDTag.AsInteger=1) do Next;
        end;

      quD_SZ.Refresh;

      with quD_T do
        begin
          Active:=False;
          Open;
        end;
    end;

end;

procedure TfmOptDistr.DelClick(Sender: TObject);
var i: integer;
    q, q1: TpFIBDataSet;
    sz: string[20];
begin
  q:=TpFIBDataSet(GetSZGrid(TComponent(Sender).Tag).DataSource.DataSet);
  q1:=TpFIBDataSet(GetUIDGrid(TComponent(Sender).Tag).DataSource.DataSet);

  if q1.Active then //1->...
    begin
      if q1.FieldByName('Tag').AsInteger=0 then Exit;
      i:=q1.FieldByName('SITEMID').AsInteger;
      sz:=q.FieldByName('SZ').AsString;
      q1.Delete;
      dmCom.tr.CommitRetaining;
      q1.AfterOpen(q1);


      with dm do
        begin
          ReOpenSZQ(q, sz);
          with quD_UID do
            if Active then
              if quD_UID.Locate('SITEMID', i, []) then quD_UID.Refresh;
          with quTmp do
            begin
              SQL.Text:='SELECT SZ FROM SITEM WHERE SITEMID='+IntToStr(i);
              ExecQuery;
              sz:=DelRSpace(Fields[0].AsString);
              Close;
            end;
          with dm.quD_SZ do
            begin
              if Locate('SZ', sz, []) then Refresh;
            end;
          with quD_T do
            begin
              Active:=False;
              Open;
            end;
        end;
    end
  else if q.Active then
         begin
           if q.Fields[0].IsNull then Exit;
           sz:=q.FieldByName('SZ').AsString;
{           with dm.quDelDistr do
             begin
               Params.ByName('DEPID').AsInteger:=TComponent(Sender).Tag;
               Params.ByName('USERID').AsInteger:=dmCom.UserId;
               Params.ByName('SZ').AsString:=q.FieldByName('SZ').AsString;
               Params.ByName('T').AsInteger:=3;
               ExecQuery;
               Close;
             end;}
           dmCom.tr.CommitRetaining;

           with dm, quD_UID do
             begin
               ReOpenSZQ(q, sz);
               Active:=False;
               Open;
             end;

           with dm do
             begin
               with quD_SZ do
                 if Locate('SZ', sz , []) then Refresh;
               with quD_T do
                 begin
                   Active:=False;
                   Open;
                 end;
             end
         end
       else raise Exception.Create('Error!!!');
end;

function  TfmOptDistr.GetSZGrid(DepId: integer): TM207IBGrid;
var j: integer;
begin
  Result:=NIL;
  with lWH do
    for j:=0 to Count-1 do
      if TM207IBGrid(Items[j]).Tag=DepId then
        begin
          Result:=TM207IBGrid(Items[j]);
          exit;
        end;
  if Result=NIL then raise Exception.Create('NOT FOUND!!!');
end;

function  TfmOptDistr.GetUIDGrid(DepId: integer): TM207IBGrid;
var j: integer;
begin
  Result:=NIL;
  with lUID do
    for j:=0 to Count-1 do
      if TM207IBGrid(Items[j]).Tag=DepId then
        begin
          Result:=TM207IBGrid(Items[j]);
          exit;
        end;
  if Result=NIL then raise Exception.Create('NOT FOUND!!!');
end;

procedure TfmOptDistr.AftOpen(DataSet: TDataSet);
var i: integer;
    f: TField;
begin
  with lTot do
    for i:=0 to Count-1 do
      if TpFIBDataSet(Items[i]).Tag=DataSet.Tag then
        begin
          with TpFIBDataSet(Items[i]) do
            begin
              Active:=False;
              Open;
            end;
          break;
        end;
  f:=DataSet.FindField('PRICE');
  if f<>NIL then
    TFloatField(f).Currency:=True;
end;

procedure TfmOptDistr.edArtChange(Sender: TObject);
begin
  dm.quD_WH.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmOptDistr.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with dm do
    if Key=VK_RETURN then
      with quD_WH do
      begin
        try
          DisableControls;
          Next;
          if NOT LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
        finally
          EnableControls;
        end
    end;
end;

procedure TfmOptDistr.ViewClick(Sender: TObject);
begin
{  with dm2, quDistred do
    begin
      Active:=False;
      Params.ByName('USERID').AsInteger:=dmCom.UserId;
      Params.ByName('DEPID').AsInteger:=TComponent(Sender).Tag;
      Params.ByName('T').AsInteger:=1;
      Active:=True;
    end;
  ShowAndFreeForm(TfmDistred, Self, TForm(fmDistred), True, False);}
end;

procedure TfmOptDistr.SetFilter(Sender: TObject);
begin
  if Sender<>NIL then
    with TMenuItem(Sender) do
      Checked:=NOT Checked;
  with dm do
    begin
      PriceFilter:=miPriceFilter.Checked;
      SupFilter:=miSupFilter.Checked;
      PriceFilterValue:=quD_UIDPrice.AsFloat;
      SupFilterId:=quD_UIDSupId.AsInteger;
      with quD_UID do
        begin
          Active:=False;
          Open;
        end;

      with quD_T do
        begin
          Active:=False;
          Open;
        end;


{      with quD_A2T2 do
        begin
          Active:=False;
          Open;
        end;}

      if PriceFilter then laPrice.Caption:=quD_UIDPrice.DisplayText
      else laPrice.Caption:='��� ����';
      if SupFilter then laSup.Caption:=quD_UIDSup.DisplayText
      else laSup.Caption:='��� ����������';
    end;
end;

procedure TfmOptDistr.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
{  if Field<>NIL then
    if Field.FieldName='DEP' then BAckground:=dmCom.clMoneyGreen
    else if Field.FieldName='PRICE2' then
        if Highlight then
          begin
            if dm.quDistrPriceT.AsInteger=1 then Background:=clGreen
            else Background:=clNavy;
          end
        else
          begin
            if dm.quDistrPriceT.AsInteger=1 then Background:=clRed
            else Background:=clInfoBk;
          end
}
end;


procedure TfmOptDistr.PostUIDBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;


procedure TfmOptDistr.ReopenAfterPost(DataSet: TDataSet);
var i:integer;
begin
  if not Closing then
    with DataSet do
      try
        DisableControls;
        i:=Fields[0].AsInteger;
        Active:=False;
        Open;
        Locate(Fields[0].FieldName, i, []);
      finally
        EnableControls;
      end;
end;

procedure TfmOptDistr.UIDBeforeEdit(DataSet: TDataSet);
begin
  if DataSet.Fields[0].IsNull then SysUtils.Abort;
end;

end.
