unit UIDStore_T;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, RXSplit, RxStrUtils, Buttons,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, Menus, DB,
  DBGridEh, FR_DSet, FR_DBSet, ActnList, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, jpeg, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmUIDStore_T = class(TForm)
    plSum: TPanel;
    stbrStatus: TStatusBar;
    grbxDep: TGroupBox;
    grbxMain: TGroupBox;
    RxSplitter1: TRxSplitter;
    plSellOut: TPanel;
    RxSplitter2: TRxSplitter;
    fmstrTSum: TFormStorage;
    lbInCap: TLabel;
    lbIn: TLabel;
    lbInvCap: TLabel;
    lbInv: TLabel;
    RxSplitter3: TRxSplitter;
    grbxCurr: TGroupBox;
    Label1: TLabel;
    lbOptIn: TLabel;
    Label2: TLabel;
    lbOptOut: TLabel;
    lbRetSeller: TLabel;
    Label3: TLabel;
    lbCurr: TLabel;
    lbCurrSum: TLabel;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    taIn: TpFIBDataSet;
    gridIn: TDBGridEh;
    dsrIn: TDataSource;
    taInQ: TIntegerField;
    taInW: TFloatField;
    taInCOST: TFloatField;
    taInGROUPNAME: TFIBStringField;
    taSInvDetail: TpFIBDataSet;
    taSInvDetailQ: TIntegerField;
    taSInvDetailW: TFloatField;
    taSInvDetailCOST: TFloatField;
    taSInv: TpFIBDataSet;
    taSInvDetailNAME: TFIBStringField;
    gridSInv: TDBGridEh;
    dsrSInvDetail: TDataSource;
    dsrSInv: TDataSource;
    taSInvQ: TIntegerField;
    taSInvW: TFloatField;
    taSInvCOST: TFloatField;
    taSInvGROUPNAME: TFIBStringField;
    gridOut: TDBGridEh;
    taOut: TpFIBDataSet;
    dsrOut: TDataSource;
    taOutQ: TIntegerField;
    taOutW: TFloatField;
    taOutCOST: TFloatField;
    taOutGROUPNAME: TFIBStringField;
    plSellIn: TPanel;
    taSell: TpFIBDataSet;
    taSell2: TpFIBDataSet;
    dsrSell: TDataSource;
    taSellW: TFloatField;
    taSellCOST: TFloatField;
    taSellGROUPNAME: TFIBStringField;
    gridSell: TDBGridEh;
    taSellDep: TpFIBDataSet;
    taDep: TpFIBDataSet;
    taSellDepQ: TIntegerField;
    taSellDepW: TFloatField;
    taSellDepCOST: TFloatField;
    taSellDepGROUPNAME: TFIBStringField;
    taSellDepDEPID: TIntegerField;
    taDepD_DEPID: TIntegerField;
    taDepSNAME: TFIBStringField;
    dsrSell2: TDataSource;
    taSell2Q: TIntegerField;
    taSell2W: TFloatField;
    taSell2COST: TFloatField;
    taSell2GROUPNAME: TFIBStringField;
    gridSell2: TDBGridEh;
    taSellDepCOST1: TFloatField;
    qutmp: TpFIBQuery;
    taOutDetail: TpFIBDataSet;
    gridMinus: TDBGridEh;
    dsrOutDetail: TDataSource;
    taOutDetailNAME: TFIBStringField;
    taOutDetailQ: TIntegerField;
    taOutDetailW: TFloatField;
    taOutDetailCOST: TFloatField;
    frIn: TfrDBDataSet;
    acEvent: TActionList;
    acPrint: TAction;
    frSell: TfrDBDataSet;
    frSell2: TfrDBDataSet;
    frOut: TfrDBDataSet;
    gridPlus: TDBGridEh;
    frSInv: TfrDBDataSet;
    frSInvDetail: TfrDBDataSet;
    frOutDetail: TfrDBDataSet;
    taSellQ: TFIBIntegerField;
    taF: TpFIBDataSet;
    dsrF: TDataSource;
    taFQ: TFIBIntegerField;
    taFW: TFIBFloatField;
    taFCOST: TFIBFloatField;
    taFGROUPNAME: TFIBStringField;
    DBGridEh1: TDBGridEh;
    frF: TfrDBDataSet;
    lbFSum: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    lbActAllowances: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure siExitClick(Sender: TObject);
    procedure BeforeOpen(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
  private
    LogOperIdForm : string;
  end;

var
  fmUIDStore_T: TfmUIDStore_T;

implementation

uses Data, comdata, Data2, M207Proc, ReportData, Variants, ServData, Period,
  Data3, JewConst;

{$R *.DFM}

const
  cCurrFormat = '### ### ##0.00';

procedure TfmUIDStore_T.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUIDStore_T.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    #27 : Close;
  end;
end;

procedure TfmUIDStore_T.FormCreate(Sender: TObject);
var
  f : double;
  ff : TFIBFloatField;
begin
  tb1.Wallpaper := wp;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  // �������� �����
  OpenDataSets([taIn]);
  f:=0;
  while not taIn.Eof do
  begin
    f := f+taInCOST.AsFloat;
    taIn.Next;
  end;
  lbIn.Caption := FormatFloat(cCurrFormat, f);
  // ������ �� ������
  OpenDataSets([taSInv]);
  f:=0;
  while not taSInv.Eof do
  begin
    f:=f+taSInvCOST.AsFloat;
    taSInv.Next;
  end;
  lbInv.Caption := FormatFloat(cCurrFormat, f);
  // ����������� �� ����� ������� �� ������
  OpenDataSets([taSInvDetail]);
  // ��������� �������
  taDep.Open;
  while not taDep.Eof do
  begin
    ff := TFIBFloatField.Create(taSell);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSell.Name+ff.FieldName;
    ff.DataSet := taSell;
    taSell.SelectSQL[2]:=taSell.SelectSQL[2]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSell.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      Width:=85;
    end;

    ff := TFIBFloatField.Create(taSell2);
    ff.currency := True;
    ff.FieldKind := fkData;
    ff.FieldName := 'F_'+taDep.Fields[0].AsString;
    ff.Name := taSell2.Name+ff.FieldName;
    ff.DataSet := taSell2;
    taSell2.SelectSQL[2]:=taSell2.SelectSQL[2]+', cast(0.0 as double precision) '+ff.FieldName;
    with gridSell2.Columns.Add do
    begin
      FieldName := ff.FieldName;
      Title.Caption := taDep.Fields[1].AsString;
      Footer.FieldName:= ff.FieldName;
      Footer.ValueType:=fvtSum;
      width:=85;      
    end;

    taDep.Next;
  end;
  taDep.Close;

  OpenDataSets([taSell, taSell2]);
  OpenDataSets([taSellDep]);

  taSellDep.First;
  while not taSellDep.Eof do
  begin
   taSell.Locate('GROUPNAME',taSellDepGROUPNAME.AsString,[]);
   taSell.Edit;
   taSell.FieldByName('F_'+taSellDepDEPID.AsString).AsFloat:=taSellDepCOST.AsFloat;
   taSell.Post;

   taSell2.Locate('GROUPNAME',taSellDepGROUPNAME.AsString,[]);
   taSell2.Edit;
   taSell2.FieldByName('F_'+taSellDepDEPID.AsString).AsFloat:=taSellDepCOST1.AsFloat;
   taSell2.Post;

   taSellDep.Next;
  end;

  // ��������� �����
  OpenDataSets([taOut]);
  f:=0;
  while not taOut.Eof do
  begin
    f:=f+taOutCOST.AsFloat;
    taOut.Next;
  end;
  lbCurrSum.Caption := FormatFloat(cCurrFormat, f);

  //������� �������
  qutmp.Close;
  qutmp.SQL.Text:='select sum(OPTSELLCOST) '+
                  'from uid_store '+
                  'where sinvid = '+dm3.taUIDStoreListSINVID.AsString+' and '+
                  '      OptSellNum<>0';
  qutmp.ExecQuery;
  lbOptIn.Caption:=inttostr(qutmp.Fields[0].asinteger);
  qutmp.Close;
  //������� ��������
  qutmp.SQL.Text:='select sum(OPTRETCOST) '+
                  'from uid_store '+
                  'where sinvid = '+dm3.taUIDStoreListSINVID.AsString+' and '+
                  '      OptRetNum<>0';
  qutmp.ExecQuery;
  lbOptOut.Caption:=inttostr(qutmp.Fields[0].asinteger);
  qutmp.Close;
  //�������� �����������
  qutmp.SQL.Text:='select sum(SINVRETCOST) '+
                  'from uid_store '+
                  'where sinvid = '+dm3.taUIDStoreListSINVID.AsString+' and '+
                  '      SInvRetNum<>0';
  qutmp.ExecQuery;
  lbRetSeller.Caption:=inttostr(qutmp.Fields[0].asinteger);
  qutmp.Close;
  //���� ��������
  qutmp.SQL.Text:='select sum(ACTALLOWANCESCOST) '+
                  'from uid_store '+
                  'where sinvid = '+dm3.taUIDStoreListSINVID.AsString+' and '+
                  '      ACTALLOWANCESNUM<>0';
  qutmp.ExecQuery;
  lbActAllowances.Caption:=inttostr(qutmp.Fields[0].asinteger);
  qutmp.Close;
  // ����������� �������
  OpenDataSets([taOutDetail]);
  // ����������� �������
  OpenDataSets([taF]);
  f := 0;
  while not taF.Eof do
  begin
    f := f + taFCOST.AsFloat;
    taF.Next;
  end;
  lbFSum.Caption := FormatFloat(cCurrFormat, f);
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStore_T.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taIn, taSInv, taSInvDetail, taSell, taOut, taSell2, taOutDetail]);
  dmCom.tr.CommitRetaining;
  Action := caFree;
end;

procedure TfmUIDStore_T.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDStore_T.BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := dm3.taUIDStoreListSINVID.AsInteger;
end;

procedure TfmUIDStore_T.acPrintExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStorePrint, LogOperIdForm);
  dmReport.PrintDocumentB(uidstore_t_p);
  dm3.update_operation(LogOperationID);
end;

end.
