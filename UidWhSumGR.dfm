object fmuidwhsumgr: Tfmuidwhsumgr
  Left = 246
  Top = 248
  Width = 647
  Height = 267
  Caption = #1048#1090#1086#1075#1086#1074#1099#1077' '#1089#1091#1084#1084#1099' '#1087#1086' '#1089#1082#1083#1072#1076#1091
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object dg1: TM207IBGrid
    Left = 0
    Top = 40
    Width = 639
    Height = 198
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsUidWhSumGr
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = dg1GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Expanded = False
        FieldName = 'SNAME'
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 126
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'C'
        Title.Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SAC'
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 111
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STW'
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STP'
        Title.Caption = #1055#1088#1080#1093#1086#1076'. '#1094#1077#1085#1099
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 96
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STP2'
        Title.Caption = #1056#1072#1089#1093#1086#1076'. '#1094#1077#1085#1099
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 106
        Visible = True
      end>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 639
    Height = 40
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 55
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 575
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1042#1089#1077#1075#1086
      Caption = #1042#1089#1077#1075#1086
      Hint = #1042#1089#1077#1075#1086' '#1087#1086' '#1089#1082#1083#1072#1076#1091
      ImageIndex = 63
      Spacing = 1
      Left = 3
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 58
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siCalc: TSpeedItem
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      Hint = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1086#1089#1090#1086#1103#1085#1080#1103' '#1089#1082#1083#1072#1076#1072'|'
      ImageIndex = 8
      Spacing = 1
      Left = 520
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1042#1089#1090#1072#1074#1082#1080
      Caption = #1042#1089#1090#1072#1074#1082#1080
      Hint = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      Spacing = 1
      Left = 113
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siFind: TSpeedItem
      BtnCaption = #1055#1086#1080#1089#1082
      Caption = #1055#1086#1080#1089#1082
      Hint = #1055#1086#1080#1089#1082
      ImageIndex = 48
      Spacing = 1
      Left = 179
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spitHistory: TSpeedItem
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103
      Hint = #1048#1089#1090#1086#1088#1080#1103'|'
      ImageIndex = 13
      Spacing = 1
      Left = 234
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      BtnCaption = #1056#1077#1076'.'
      Caption = 'siEdit'
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103
      ImageIndex = 72
      Spacing = 1
      Left = 289
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      BtnCaption = #1054#1088#1075#1072#1085#1080#1079'.'
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
      ImageIndex = 7
      Spacing = 1
      Left = 344
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      BtnCaption = #1055#1086#1074#1090#1086#1088#1099
      Caption = #1055#1086#1074#1090#1086#1088#1099
      Hint = #1054#1073#1085#1072#1088#1091#1078#1077#1085#1080#1077' '#13#10#1085#1077#1091#1085#1080#1082#1072#1083#1100#1085#1099#1093' '#13#10#1085#1086#1084#1077#1088#1086#1074
      ImageIndex = 56
      Spacing = 1
      Left = 399
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spItog: TSpeedItem
      BtnCaption = #1048#1090#1086#1075#1080
      Caption = #1048#1090#1086#1075#1080
      Hint = #1057#1091#1084#1084#1099' '#1080#1079#1076#1077#1083#1080#1081' '#1087#1086' '#1089#1082#1083#1072#1076#1072#1084
      ImageIndex = 9
      Spacing = 1
      Left = 454
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siText: TSpeedItem
      BtnCaption = #1058#1077#1082#1089#1090
      Caption = #1058#1077#1082#1089#1090
      Hint = #1058#1077#1082#1089#1090'|'
      Spacing = 1
      Left = 575
      Top = 3
      SectionName = 'Untitled (0)'
    end
  end
end
