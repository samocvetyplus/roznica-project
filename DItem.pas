unit DItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Buttons, db, Menus, StdCtrls, Mask, DBCtrls,
  ActnList, rxPlacemnt, rxSpeedbar;

type
  TfmDItem = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    Panel1: TPanel;
    Splitter1: TSplitter;
    sbAdd: TSpeedButton;
    sbDel: TSpeedButton;
    pm1: TPopupMenu;
    N1: TMenuItem;
    pm2: TPopupMenu;
    N2: TMenuItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    Panel2: TPanel;
    sp1: TSplitter;
    Panel4: TPanel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    laSup: TLabel;
    laPrice: TLabel;
    miSupFilter: TMenuItem;
    miPriceFilter: TMenuItem;
    FormStorage1: TFormStorage;
    Panel6: TPanel;
    dgWH: TM207IBGrid;
    Label6: TLabel;
    Label7: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    paDepFrom: TPanel;
    DBText3: TDBText;
    siPrice: TSpeedItem;
    paDI: TPanel;
    paDepTo: TPanel;
    dg1: TM207IBGrid;
    Panel5: TPanel;
    Label1: TLabel;
    laQ: TLabel;
    Label3: TLabel;
    laW: TLabel;
    paRest: TPanel;
    paRest1: TPanel;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    dgSZ: TM207IBGrid;
    TabSheet2: TTabSheet;
    dgUID: TM207IBGrid;
    acEvent: TActionList;
    acShowId: TAction;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure sbAddClick(Sender: TObject);
    procedure sbDelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetFilter(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dgSZGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dgUIDGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siPriceClick(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure UpdateQW;

  public

    { Public declarations }
  end;

var
  fmDItem: TfmDItem;

implementation

uses comdata, Data, Data2, SetPrice, M207Proc, FIBDataSet;

{$R *.DFM}

procedure TfmDItem.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmDItem.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDItem.FormCreate(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  tb1.WallPaper:=wp;
  dg1.ColumnByName['BUSYTYPE'].Visible := False;
  with dm, dm2 do
    begin
      PriceFilter:=False;
      SupFilter:=False;
      SetFilter(NIL);
      OpenDataSets([taDItem, quAllDepUID, quAllDepSz]);
      laQ.Caption:=taSElQuantity.AsString;
      laW.Caption:=taSElTotalWeight.AsString;
      with pc1 do
        ActivePage:=Pages[0];
    end;
  dgWH.ColumnByName['PRICE'].Visible:=CenterDep;
  dg1.ColumnByName['SPRICE'].Visible:=CenterDep;
  Label5.Visible:=CenterDep;
  laPrice.Visible:=CenterDep;
  Screen.Cursor:=crDefault;
  siPrice.Enabled:=CenterDep;
end;

procedure TfmDItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dm2, dmCom do
    begin
      CloseDataSets([quWHA2UID, taDItem,  quAllDepUID, quAllDepSz]);
      tr.CommitRetaining;
      dm.taSEl.Refresh;
      if WorkMode='DINV' then taDList.Refresh
      else if WorkMode='OPT' then taOptList.Refresh;
      SupFilter:=False;
      PriceFilter:=False;

      if not paRest.Visible then
        begin
          paRest.Visible:=True;
//          paDIWidth:=paDI.Width;
        end;
    end;
end;

procedure TfmDItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDItem.dgWHGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='UID') then
    Background:=dmCom.clMoneyGreen;
end;

procedure TfmDItem.sbAddClick(Sender: TObject);
begin
 if dm.WorkMode='OPT' then
  if dm.taOptListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������');

  with dm, taDItem do
    begin
      if quWHA2UIDSItemId.IsNull then exit;
      Append;
      taDItemUID.AsInteger:=quWHA2UIDUID.AsInteger;
      Post;
      quWHA2UID.Delete;
      dmCom.tr.CommitRetaining;
    end;
  UpdateQW;
end;

procedure TfmDItem.sbDelClick(Sender: TObject);
var i: integer;
begin
 if dm.WorkMode='OPT' then
  if dm.taOptListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������');

  with dm do
    begin
      i:=taDItemRef.AsInteger;
      taDItem.Delete;
      dmCom.tr.CommitRetaining;
      with quWHA2UID do
        begin
          Append;
          quWHA2UIDSItemId.AsInteger:=i;
          Post;
          Transaction.CommitRetaining;
        end;
    end;
  UpdateQW;
end;

procedure TfmDItem.UpdateQW;
begin
  with dm, quElQW do
    begin
      Params[0].AsInteger:=taSElSElId.AsInteger;
      ExecQuery;
      laQ.Caption:=Fields[0].AsString;
      laW.Caption:=Fields[1].AsString;
      Close;
    end;
end;

procedure TfmDItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then Close;
end;

procedure TfmDItem.SetFilter(Sender: TObject);
begin
  if Sender<>NIL then
    with TMenuItem(Sender) do
      Checked:=NOT Checked;
  with dm, dm2 do
    begin
      PriceFilter:=miPriceFilter.Checked;
      SupFilter:=miSupFilter.Checked;
      PriceFilterValue:=quWHA2UIDPrice.AsFloat;
      SupFilterId:=quWHA2UIDSupId.AsInteger;
      with quWHA2UID do
        begin
          if not Active then Active:=True;
          Filtered:=False;
          Filtered:=True;
        end;

      with quWHA2UIDT do
        begin
          Active:=False;
          Open;
        end;

      if PriceFilter then laPrice.Caption:=quWHA2UIDPrice.DisplayText
      else laPrice.Caption:='��� ����';
      if SupFilter then laSup.Caption:=quWHA2UIDSup.DisplayText
      else laSup.Caption:='��� ����������';
    end;
end;


procedure TfmDItem.FormActivate(Sender: TObject);
begin
  siHelp.Visible:=false;
  if dm.WorkMode='DINV' then
    begin
       paDepFrom.Caption:=dm.taDListDepFrom.AsString;
       paDepTo.Caption:=dm.taDListDepTo.AsString;
       Caption:='����������������� ������ ��� ����������� �����������'
    end
  else if dm.WorkMode='OPT' then
         begin
           paDepFrom.Caption:=dm.taOptListDepFrom.AsString;
           paDepTo.Caption:=dm.taOptListComp.AsString;
           Caption:='����������������� ������ ��� ������� �������';
           HelpContext:=100253;
           sihelp.Visible:=true;
         end;
   with dm do
     Caption:=Caption+':  '+taSElFullArt.AsString+' '+taSElArt2.AsString;

//   paDIWidth:=paDI.Width;
   paRest.Visible:=dm.WorkMode<>'OPT';
   ActiveControl:=dgWH;
   siExit.Left:=tb1.Width-tb1.BtnWidth-10;
   siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDItem.dgSZGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if NOT Highlight or dgSZ.ClearHighlight then
    Background:=dm2.quAllDepSZColor.AsInteger;
end;

procedure TfmDItem.dgUIDGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Highlight or dgUID.ClearHighlight then
     Background:=dm2.quAllDepUIDColor.AsInteger;
end;

procedure TfmDItem.siPriceClick(Sender: TObject);
begin
  fmSetPrice:=TfmSetPrice.Create(NIL);
  with dm do
    try
      fmSetPrice.Caption:='��������� ��� - '+taSElFullArt.AsString+' '+taSElArt2.AsString;
      fmSetPrice.sb1.SimpleText:='��������� ����: '+dm.taSElPrice.DisplayText;
      with taSetPrice do
        begin
          Active:=False;
          Params.ByName['ART2ID'].AsInteger:=taSElArt2Id.AsInteger;
          Params.ByName['INVID'].AsInteger:=taDListSInvId.AsInteger;
          Open;
        end;
      Art2Id:=taSElArt2Id.AsInteger;
      UseMargin:=taSElUseMargin.AsInteger=1;
      fmSetPrice.ShowModal;
      RefreshAllSEl(taSElArt2Id.AsInteger);
    finally
      fmSetPrice.Free;
    end;
end;

procedure TfmDItem.acShowIdExecute(Sender: TObject);
begin
  dg1.ColumnByName['BUSYTYPE'].Visible := not dg1.ColumnByName['BUSYTYPE'].Visible; 
end;

procedure TfmDItem.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100253);
end;

end.

