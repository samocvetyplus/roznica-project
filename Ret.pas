unit Ret;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, SpeedBar, ExtCtrls, DBCtrls, ToolEdit, RXDBCtrl, StdCtrls, Mask,
  Placemnt, Grids, DBGrids, M207Grid, M207IBGrid, db, Menus;

type
  TfmRet = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    edSN: TDBEdit;
    deSDate: TDBDateEdit;
    FormStorage1: TFormStorage;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    Label13: TLabel;
    edArt: TEdit;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    Splitter6: TSplitter;
    DBText2: TDBText;
    dgEl: TM207IBGrid;
    pmEl: TPopupMenu;
    N1: TMenuItem;
    pmWH: TPopupMenu;
    N2: TMenuItem;
    siUID: TSpeedItem;
    N3: TMenuItem;
    siCloseInv: TSpeedItem;
    lcSup: TDBLookupComboBox;
    M207IBGrid1: TM207IBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siAddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure dgElGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siUIDClick(Sender: TObject);
    procedure siCloseInvClick(Sender: TObject);
    procedure dgElEditButtonClick(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    SearchEnable: boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmRet: TfmRet;

implementation

uses comdata, Data, DBTree, DItem, Data2, OptItem, SItem, RItem;

{$R *.DFM}

procedure TfmRet.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmRet.FormCreate(Sender: TObject);
var nd: TNodeData;
begin
  tb1.WallPaper:=wp;
  with dm, dm2 do
    begin
      taSEl.SelectSQL[3]:='ORDER BY ART2';
      OpenDataSets([taSEl, dm2.quAllBuyer]);
    end;

  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns);

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCompClick(NIL);
  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;
  SetCBDropDown(lcSup);
end;

procedure TfmRet.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom, dm2 do
    begin
      PostDataSets([taOptList, taSEl ]);
      CloseDataSets([taSEl, quBuyer]);
      tr.CommitRetaining;
    end;
end;

procedure TfmRet.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRet.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  ReOpenDataSets([dm.quDPrice]);
end;

procedure TfmRet.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmRet.edArtChange(Sender: TObject);
begin
  if  SearchEnable then
    dm.quDPrice.Locate('ART2', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmRet.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if SearchEnable then
    with dm do
      if Key=VK_RETURN then
        with quDPrice do
          begin
            try
              DisableControls;
              Next;
              if NOT LocateNext('ART2', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
            finally
              EnableControls;
            end
          end;
end;

procedure TfmRet.siAddClick(Sender: TObject);
begin
  with dm, taSEl do
    begin
      Append;
      taSElArt2Id.AsInteger:=quDPriceArt2Id.AsInteger;
      Post;
      siUIDClick(NIL);
    end;
end;

procedure TfmRet.siDelClick(Sender: TObject);
begin
  dm.taSEl.Delete;
end;

procedure TfmRet.dgElGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and ((Field.FieldName='FULLART') or (Field.FieldName='ART2')) then
    begin
      Background:=dmCom.clMoneyGreen;
    end;
{    case dm.taSElWHSell.AsInteger of
        0: Background:=clAqua;
        1: Background:=dmCom.clMoneyGreen;
      end;}
end;

procedure TfmRet.siUIDClick(Sender: TObject);
begin
  with dm do
    begin
      PostDataSet(taSEl);
      if NOT taSElSElId.IsNull then
          ShowAndFreeForm(TfmRItem, Self, TForm(fmRItem), True, False);
      if taSElQuantity.AsFloat=0 then
        with taSEl do
          begin
            Tag:=1;
//            Delete;
            Tag:=0;
          end;
      taRetList.Refresh;
    end;
end;

procedure TfmRet.siCloseInvClick(Sender: TObject);
var i: integer;
begin
  with dm do
    begin
      PostDataSets([taSEl, taRetList]);
      with quTmp do
        begin
          SQL.Text:='SELECT COUNT(*) FROM SINV '+
                    'WHERE ITYPE=4 AND SN='+taRetListSN.AsString+
                    ' AND DepId='+taRetListDepId.AsString;
          ExecQuery;
          i:=Fields[0].AsInteger;
          Close;
          if i>1 then raise Exception.Create('������������ ����� ���������!!!');
        end;

      if MessageDlg('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
        begin
          with quTmp do
            begin
              SQL.Text:='EXECUTE PROCEDURE CloseInv '+taRetListSInvId.AsString+', 2';
              ExecQuery;
            end;
          taRetList.Refresh;
        end;
    end;
end;

procedure TfmRet.dgElEditButtonClick(Sender: TObject);
begin
  with dgEl.SelectedField do
    if (FieldName='TOTALWEIGHT') or (FieldName='QUANTITY') then siUIDClick(NIL);
end;

procedure TfmRet.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and ((Field.FieldName='FULLART') or (Field.FieldName='ART2')) then Background:=dmCom.clMoneyGreen;
end;

end.

