unit SelectUidWh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActnList;

type
  TfmSelectUidWh = class(TForm)
    btAnyPeriod: TButton;
    btBeginMonth: TButton;
    btAnyDate: TButton;
    acList: TActionList;
    acEsc: TAction;
    acEnter: TAction;
    procedure acEscExecute(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSelectUidWh: TfmSelectUidWh;

implementation
uses data;
{$R *.dfm}

procedure TfmSelectUidWh.acEscExecute(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TfmSelectUidWh.acEnterExecute(Sender: TObject);
begin
 if ActiveControl=btAnyPeriod then dm.UIDWHType:=2
 else if ActiveControl=btBeginMonth then dm.UIDWHType:=1
 else if ActiveControl=btAnyDate then dm.UIDWHType:=4;
 ModalResult:=mrOk;
end;

end.
