object fmArt: TfmArt
  Left = 266
  Top = 62
  Width = 800
  Height = 600
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1088#1090#1080#1082#1091#1083#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter3: TSplitter
    Left = 185
    Top = 40
    Height = 514
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 554
    Width = 792
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 792
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 170
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 188
    Top = 40
    Width = 604
    Height = 514
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 0
      Top = 284
      Width = 604
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 287
      Width = 604
      Height = 227
      ActivePage = TabSheet1
      Align = alBottom
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077' '#1080' '#1086#1087#1080#1089#1072#1085#1080#1077
        object Splitter2: TSplitter
          Left = 225
          Top = 0
          Height = 199
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 225
          Height = 199
          Align = alLeft
          Alignment = taLeftJustify
          BevelOuter = bvLowered
          TabOrder = 0
          OnDblClick = im1DblClick
          object im1: TImage
            Left = 1
            Top = 1
            Width = 223
            Height = 197
            Align = alClient
            Center = True
            OnDblClick = im1DblClick
          end
        end
        object DBMemo1: TDBMemo
          Left = 228
          Top = 0
          Width = 368
          Height = 199
          Align = alClient
          Color = clInfoBk
          DataField = 'MEMO'
          DataSource = dmCom.dsArt
          TabOrder = 1
        end
      end
    end
    inline frArtGrid1: TfrArtGrid
      Left = 0
      Top = 0
      Width = 604
      Height = 284
      Align = alClient
      TabOrder = 1
      inherited Panel2: TPanel
        Width = 604
      end
      inherited dg1: TM207IBGrid
        Width = 604
        Height = 257
        Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        IniStorage = fs1
        OnGetCellParams = frArtGrid1dg1GetCellParams
        Columns = <
          item
            Expanded = False
            FieldName = 'FULLART'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 104
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'Comp'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 221
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'Mat'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090#1077#1088#1080#1072#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 61
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'Good'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 81
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            PickList.Strings = (
              #1043#1088#1072#1084#1084#1099
              #1064#1090#1091#1082#1080)
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 45
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'Ins'
            Title.Alignment = taCenter
            Title.Caption = #1054#1089#1085'. '#1074#1089#1090#1072#1074#1082#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 94
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE1'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1080#1093#1086#1076'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 69
            Visible = True
          end>
      end
    end
  end
  inline frArtNav1: TfrArtNav
    Left = 0
    Top = 40
    Width = 185
    Height = 514
    Align = alLeft
    TabOrder = 3
    inherited tv1: TTreeView
      Height = 455
    end
  end
  object opd1: TOpenPictureDialog
    Filter = 
      #1042#1089#1077' '#1092#1072#1081#1083#1099'|*.bmp;*.jpg;*.jpeg;*.wmf;*.emf|JPEG-images|*.jpg;*.jpe' +
      'g|Windows metafiles|*.wmf;*.emf|Bitmaps (*.bmp)|*.bmp'
    Title = #1042#1099#1073#1086#1088' '#1092#1072#1081#1083#1072
    Left = 113
    Top = 165
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 296
    Top = 108
  end
end
