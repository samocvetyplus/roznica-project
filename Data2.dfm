object dm2: Tdm2
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 761
  Width = 1015
  object quInsTmpP2: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'update Price'
      'set TPrice=?Price,'
      '      InvId=NULL,'
      '      Pr=1,'
      '      PDATE=ONLYDATE('#39'NOW'#39')'
      'where Art2Id=?Art2Id and'
      '           DepId=?DepId')
    Left = 32
    Top = 105
  end
  object quInsTmpP2OP: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'UPDATE ART2'
      'SET TOPTPRICE=?PRICE,'
      '         INVID=NULL,'
      '         OPR=1,'
      '         PDATE=ONLYDATE('#39'NOW'#39')'
      'WHERE ART2ID=?ART2ID'
      ' ')
    Left = 30
    Top = 153
  end
  object quInsGr2PrOrd: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE INSGR2PROPRD'
      
        '?PRORDID, ?USERID, ?D_COMPID, ?D_MATID, ?D_GOODID, ?D_INSID, ?DE' +
        'PID, ?P1, ?P2,'
      '?P1F, ?P2F, ?PRICE2, ?SPRICE, ?OPTPRICE')
    Left = 110
    Top = 157
  end
  object quGetPrice: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select Price2'
      'from Price '
      'where DepId=?DepId and Art2Id= ?Art2Id')
    Left = 102
    Top = 103
  end
  object quGetDiscount: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT DISCOUNT'
      'FROM D_DISCOUNT'
      'WHERE D_DISCOUNTID= ?DISCOUNTID')
    Left = 42
    Top = 205
  end
  object quGetUID: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT SItemId, SZ, W, UID, Art2Id, Art2, FullArt, UnitId, Price' +
        ', Res'
      'FROM GETUID(?AUID, ?ADEPID)'
      ''
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 446
    Top = 207
    object quGetUIDSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object quGetUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quGetUIDW: TFloatField
      FieldName = 'W'
    end
    object quGetUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quGetUIDART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quGetUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quGetUIDFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quGetUIDUNITID: TIntegerField
      FieldName = 'UNITID'
    end
    object quGetUIDPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object quGetUIDRES: TSmallintField
      FieldName = 'RES'
    end
  end
  object pmDiscount: TPopupMenu
    Left = 524
    Top = 365
  end
  object quMargin2Dep: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure ApplyMargin2Dep'
      
        '?DepId, ?UserId, ?CompId1, ?MatId1, ?GoodId1, ?InsId1, ?CountryI' +
        'D1, ?CompId2, ?MatId2,'
      '?GoodId2, ?InsId2, ?CountryID2'
      '')
    Left = 118
    Top = 271
  end
  object quMargin2Opt: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure ApplyMargin2Opt'
      
        '?UserId, ?CompId1, ?MatId1, ?GoodId1, ?InsId1, ?CountryID1, ?Com' +
        'pId2, ?MatId2, ?GoodId2, ?InsId2, ?CountryID2'
      '')
    Left = 116
    Top = 315
  end
  object quClearTmpP2Dep: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE CLEARTMPP2DEP'
      
        '?DEPID, ?USERID, ?CompId1, ?MatId1, ?GoodId1, ?InsId1, ?CountryI' +
        'D1, ?CompId2, ?MatId2, ?GoodId2, ?InsId2, ?CountryID2')
    Left = 106
    Top = 215
  end
  object quClearTmpP2Opt: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE CLEARTMPP2OPT'
      ' ?USERID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?countryid1,'
      ' ?COMPID2, ?MATID2, ?GOODID2, ?INSID2, ?countryid2')
    Left = 46
    Top = 257
  end
  object quClearTmpP2All: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE CLEARTMPP2ALL'
      ' ?USERID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?countryid1,'
      ' ?COMPID2, ?MATID2, ?GOODID2, ?INSID2, ?countryid2')
    Left = 46
    Top = 303
  end
  object quMargin2Gr: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure ApplyMargin2Gr'
      
        '?UserId, ?CompId1, ?MatId1, ?GoodId1, ?InsId1, ?CountryId1,?Comp' +
        'Id2, ?MatId2,'
      '?GoodId2, ?InsId2,?CountryId2'
      '')
    Left = 548
    Top = 195
  end
  object quSetTmp2Price: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE SETTMP2PRICE'
      
        '?USERID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?COMPID2, ?MATID2' +
        ', ?GOODID2,'
      '?INSID2,  ?DEPID1, ?DEPID2, ?COUNTRYID1, ?COUNTRYID2,'
      ' ?PRICE, ?OPTPRICE')
    Left = 178
    Top = 107
  end
  object quUpdateDistrPrice: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure DISTRPRICE_U'
      '?ART2ID, ?USERID, ?DEPID, ?PRICE2')
    Left = 188
    Top = 155
  end
  object quBuyer: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_COMP'
      'SET OPTMARGIN=?OPTMARGIN'
      'WHERE D_COMPID= ?D_COMPID')
    SelectSQL.Strings = (
      'SELECT D_COMPID, NAME, SNAME, OPTMARGIN'
      'FROM D_COMP'
      'WHERE BUYER=1 and d_compid<>-1000 and WORKBUYER=1'
      'ORDER BY SNAME')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 20
    Top = 9
    object quBuyerD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quBuyerNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quBuyerSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quBuyerOPTMARGIN: TFloatField
      FieldName = 'OPTMARGIN'
    end
  end
  object dsBuyer: TDataSource
    DataSet = quBuyer
    Left = 22
    Top = 53
  end
  object quWHA2UIDT: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT Q, W'
      'FROM  WHA2UIDT'
      '(?ART2ID, ?DEPID1, ?DEPID2, ?P1, ?P2, ?SUPID1, ?SUPID2)')
    BeforeOpen = quWHA2UIDTBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 86
    Top = 9
    object quWHA2UIDTQ: TIntegerField
      FieldName = 'Q'
    end
    object quWHA2UIDTW: TFloatField
      FieldName = 'W'
    end
  end
  object dsWHA2UIDT: TDataSource
    DataSet = quWHA2UIDT
    Left = 84
    Top = 53
  end
  object quAllBuyer: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, NAME'
      'FROM D_COMP'
      'WHERE BUYER=1 '
      'ORDER BY NAME')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 148
    Top = 7
    object quAllBuyerD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quAllBuyerNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsAllBuyer: TDataSource
    DataSet = quAllBuyer
    Left = 148
    Top = 51
  end
  object dsRest: TDataSource
    DataSet = taRest
    Left = 196
    Top = 51
  end
  object taRI: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE REMAINSITEM'
      'SET SZ=?SZ,'
      '    W=?W,'
      '    UID=?UID'
      'WHERE ID=?OLD_ID'
      '')
    DeleteSQL.Strings = (
      'DELETE FROM REMAINSITEM'
      'WHERE ID=?ID')
    InsertSQL.Strings = (
      'INSERT INTO REMAINSITEM(ID, SZ, W, UID, REMAINS, DONE)'
      'VALUES (?ID, ?SZ, ?W, ?UID, ?REMAINS, 0)'
      '')
    SelectSQL.Strings = (
      'SELECT ID, SZ, W, UID, REMAINS, DONE'
      'FROM REMAINSITEM'
      'WHERE REMAINS=?REMAINS'
      ' ')
    AfterDelete = taRIAfterDelete
    AfterPost = taRIAfterDelete
    BeforeDelete = taRIBeforeDelete
    BeforeOpen = taRIBeforeOpen
    BeforePost = taRIBeforePost
    OnNewRecord = taRINewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 284
    Top = 7
    object taRIID: TIntegerField
      FieldName = 'ID'
    end
    object taRISZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taRIW: TFloatField
      FieldName = 'W'
    end
    object taRIUID: TIntegerField
      FieldName = 'UID'
    end
    object taRIREMAINS: TIntegerField
      FieldName = 'REMAINS'
    end
    object taRIDONE: TSmallintField
      FieldName = 'DONE'
    end
  end
  object dsRI: TDataSource
    DataSet = taRI
    Left = 284
    Top = 51
  end
  object dsRInv: TDataSource
    DataSet = taRInv
    Left = 256
    Top = 155
  end
  object quProd: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COMPID, CODE, SNAME'
      'FROM D_COMP'
      'WHERE PRODUCER=1'
      'ORDER BY SNAME')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 252
    Top = 263
    object quProdD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object quProdCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quProdSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object quMat: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_MATID, SNAME, NAME'
      'FROM D_MAT'
      'ORDER BY SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 288
    Top = 263
    object quMatD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object quMatSNAME: TFIBStringField
      FieldName = 'SNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object quMatNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
  end
  object quGood: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_GOODID, NAME'
      'FROM D_GOOD'
      'ORDER BY SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 328
    Top = 263
    object quGoodD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quGoodNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
  end
  object quIns: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_INSID, SNAME'
      'FROM D_INS'
      'ORDER BY SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 372
    Top = 263
    object quInsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quInsSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object quRestCost: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT COST,  COST0,  SCOST, SCOST0'
      'FROM REST_COST '
      '(?COMPID1, ?COMPID2, ?MATID1, ?MATID2, '
      ' ?GOODID1, ?GOODID2, ?INSID1, ?INSID2)')
    BeforeOpen = quRestCostBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 272
    Top = 199
    object quRestCostCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quRestCostCOST0: TFloatField
      FieldName = 'COST0'
      currency = True
    end
    object quRestCostSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object quRestCostSCOST0: TFloatField
      FieldName = 'SCOST0'
      currency = True
    end
  end
  object quAllDepSz: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SZ, DEP, DEPID, QUANTITY,  WEIGHT, COLOR'
      'FROM ALLDEPSZ(?ART2ID)'
      ' ')
    BeforeOpen = quAllDepSzBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 316
    Top = 107
    object quAllDepSzSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quAllDepSzDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quAllDepSzDEPID: TFIBStringField
      FieldName = 'DEPID'
      EmptyStrToNull = True
    end
    object quAllDepSzQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object quAllDepSzWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quAllDepSzCOLOR: TIntegerField
      FieldName = 'COLOR'
    end
  end
  object dsAllDepSz: TDataSource
    DataSet = quAllDepSz
    Left = 316
    Top = 151
  end
  object quAllDepUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select si.UID, si.W, si.SZ, d.SName Dep, d.Color'
      'from SItem si, SEl e, SInv i, D_Dep d, Art2 A2'
      'where i.IsClosed=1 and'
      '      e.SInvId=i.SInvId and'
      '      si.SElid=e.SElId and'
      '      si.BusyType=0 and'
      '      a2.Art2Id=e.Art2Id and'
      '      a2.D_ArtId= ?D_ARTID and'
      '      d.D_DepId=i.DepId'
      'order by si.SZ, d.SName')
    BeforeOpen = quAllDepUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 376
    Top = 103
    object quAllDepUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quAllDepUIDW: TFloatField
      FieldName = 'W'
    end
    object quAllDepUIDSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quAllDepUIDDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quAllDepUIDCOLOR: TIntegerField
      FieldName = 'COLOR'
    end
  end
  object dsAllDepUID: TDataSource
    DataSet = quAllDepUID
    Left = 376
    Top = 151
  end
  object dsProd: TDataSource
    DataSet = quProd
    Left = 252
    Top = 311
  end
  object dsMat: TDataSource
    DataSet = quMat
    Left = 288
    Top = 311
  end
  object dsGood: TDataSource
    DataSet = quGood
    Left = 332
    Top = 311
  end
  object dsIns: TDataSource
    DataSet = quIns
    Left = 376
    Top = 311
  end
  object quInsDSItem: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      
        'select id from INSDSItem(?SITEMID, ?ART2ID, ?DEPFROMID, ?DEPID, ' +
        '?SINVID)')
    OnExecuteError = quInsDSItemExecuteError
    Left = 760
    Top = 23
  end
  object ibdsWHItem: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SItem where UID=:UID')
    SelectSQL.Strings = (
      'select si.SItemId, si.SZ, si.W, si.UID '
      'from sitem si, sinfo inf, sel s'
      'where si.Busytype=0 and'
      '          si.selid=s.selid and'
      '            s.art2id=:art2id and'
      '            si.SInfoId=inf.SInfoId and'
      '            inf.SDate=:ADate and'
      '            inf.SSF=:N and'
      '            inf.SupId=:V_SupId')
    AfterDelete = ibdsWHItemAfterDelete
    AfterPost = ibdsWHItemAfterDelete
    BeforeDelete = ibdsWHItemBeforeDelete
    BeforeOpen = ibdsWHItemBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 412
    Top = 11
    object ibdsWHItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object ibdsWHItemSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsWHItemW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
    end
    object ibdsWHItemUID: TIntegerField
      DisplayLabel = #1048#1076'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
  end
  object dsrWHItem: TDataSource
    DataSet = ibdsWHItem
    Left = 412
    Top = 55
  end
  object taRest: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure REST_U('
      
        '?ID, ?ART2ID, ?PRICE, ?PRICE2, ?D_COMPID, ?ADATE, ?TW, ?NDSID, ?' +
        'N, ?FULLART,'
      
        '?Q, ?W, ?COST, ?UNITID, ?SUP, ?NDSNAME, ?NDSVALUE, ?ART2, ?TODO,' +
        ' ?D_ARTID,'
      
        '?COMPID, ?MATID, ?GOODID, ?INSID, ?Countryid, ?ART, ?DONE, ?MTO,' +
        ' ?MFROM)')
    DeleteSQL.Strings = (
      'DELETE FROM REMAINS'
      'WHERE ID=?OLD_ID')
    InsertSQL.Strings = (
      'execute procedure REST_I(:ID,'
      '?PRICE, ?PRICE2, ?D_COMPID, ?ADATE, ?TW, ?NDSID, ?N, '
      '?Q, ?W, ?COST, ?UNITID, ?SUP, ?NDSNAME, ?NDSVALUE, ?ART2, '
      '?COMPID, ?MATID, ?GOODID, ?INSID, ?Countryid, ?ART)')
    RefreshSQL.Strings = (
      
        'SELECT ID, ART2ID, PRICE, PRICE2, D_COMPID, ADATE, TW, NDSID, N,' +
        ' FULLART, Q,'
      '       W, COST, UNITID, SUP, NDSNAME, NDSVALUE, ART2, TODO,'
      
        '       D_ARTID, COMPID,  MATID, GOODID, INSID, Countryid, ART, D' +
        'ONE,'
      '       MAT, GOOD, INS, PROD, MTO, MFROM, RQ, RW'
      'FROM REST_R(?ID)')
    SelectSQL.Strings = (
      
        'SELECT ID, ART2ID, PRICE, PRICE2, D_COMPID, ADATE, TW, NDSID, N,' +
        ' FULLART, Q,'
      '       W, COST, UNITID, SUP, NDSNAME, NDSVALUE, ART2, TODO,'
      
        '       D_ARTID, COMPID,  MATID, GOODID, INSID,Countryid,  ART, D' +
        'ONE,'
      '       MAT, GOOD, INS, PROD, MTO, MFROM, RQ, RW'
      'FROM REST_S'
      
        '(?COMPID1, ?COMPID2, ?MATID1, ?MATID2, ?GOODID1, ?GOODID2, ?INSI' +
        'D1, ?INSID2, ?Countryid1, ?Countryid2, :USERID)'
      ' ')
    AfterDelete = CommitRetaining
    AfterInsert = taRestAfterInsert
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeOpen = taRestBeforeOpen
    BeforePost = taRestBeforePost
    OnNewRecord = taRestNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 196
    Top = 7
    object taRestID: TIntegerField
      FieldName = 'ID'
    end
    object taRestART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object taRestPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object taRestPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taRestD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
    end
    object taRestADATE: TDateTimeField
      FieldName = 'ADATE'
    end
    object taRestTW: TFloatField
      FieldName = 'TW'
    end
    object taRestN: TFIBStringField
      FieldName = 'N'
      EmptyStrToNull = True
    end
    object taRestNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object taRestFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taRestQ: TFloatField
      FieldName = 'Q'
    end
    object taRestW: TFloatField
      FieldName = 'W'
    end
    object taRestCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taRestUNITID: TIntegerField
      FieldName = 'UNITID'
      OnGetText = taRestUNITIDGetText
      OnSetText = taRestUNITIDSetText
    end
    object taRestSUP: TFIBStringField
      DisplayWidth = 20
      FieldKind = fkLookup
      FieldName = 'SUP'
      LookupDataSet = dm.quSup
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'SNAME'
      KeyFields = 'D_COMPID'
      EmptyStrToNull = True
      Lookup = True
    end
    object taRestNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object taRestNDSVALUE: TFloatField
      FieldName = 'NDSVALUE'
    end
    object taRestART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRestTODO: TSmallintField
      FieldName = 'TODO'
    end
    object taRestCOMPID: TIntegerField
      FieldName = 'COMPID'
    end
    object taRestMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRestGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRestINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRestART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taRestMAT: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'MAT'
      LookupDataSet = quMat
      LookupKeyFields = 'D_MATID'
      LookupResultField = 'D_MATID'
      KeyFields = 'MATID'
      Size = 10
      EmptyStrToNull = True
      Lookup = True
    end
    object taRestDONE: TSmallintField
      FieldName = 'DONE'
    end
    object taRestGOOD: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'GOOD'
      LookupDataSet = quGood
      LookupKeyFields = 'D_GOODID'
      LookupResultField = 'D_GOODID'
      KeyFields = 'GOODID'
      Size = 10
      EmptyStrToNull = True
      Lookup = True
    end
    object taRestINS: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'INS'
      LookupDataSet = quIns
      LookupKeyFields = 'D_INSID'
      LookupResultField = 'SNAME'
      KeyFields = 'INSID'
      Size = 10
      EmptyStrToNull = True
      Lookup = True
    end
    object taRestD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object taRestPROD: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'PROD'
      LookupDataSet = quProd
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'SNAME'
      KeyFields = 'COMPID'
      EmptyStrToNull = True
      Lookup = True
    end
    object taRestMTO: TSmallintField
      FieldName = 'MTO'
    end
    object taRestMFROM: TSmallintField
      FieldName = 'MFROM'
    end
    object taRestRQ: TFloatField
      FieldName = 'RQ'
    end
    object taRestRW: TFloatField
      FieldName = 'RW'
    end
    object taRestcountryid: TStringField
      FieldName = 'countryid'
      Size = 10
    end
  end
  object taRInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SInv'
      'where SInvId=?OLD_SInvId')
    SelectSQL.Strings = (
      
        'SELECT SINVID, SUPID, DEPID, SDATE, NDATE, SSF, COST, ISCLOSED, ' +
        'SUP, DEP,'
      '       TW, TQ, CrDate'
      'FROM   RESTLIST_S'
      'order by crdate, supid  ')
    BeforeDelete = taRIBeforeDelete
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 256
    Top = 107
    object taRInvSINVID: TIntegerField
      FieldName = 'SINVID'
    end
    object taRInvSUPID: TIntegerField
      FieldName = 'SUPID'
    end
    object taRInvDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taRInvSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object taRInvNDATE: TDateTimeField
      FieldName = 'NDATE'
    end
    object taRInvSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taRInvCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object taRInvISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
    end
    object taRInvSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object taRInvDEP: TFIBStringField
      FieldName = 'DEP'
      Size = 60
      EmptyStrToNull = True
    end
    object taRInvTW: TFloatField
      FieldName = 'TW'
    end
    object taRInvTQ: TSmallintField
      FieldName = 'TQ'
    end
    object taRInvCRDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
      FieldName = 'CRDATE'
    end
  end
  object quInsDUID: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select  SELID, T, SITEMID'
      'from  InsDUID3(?UID, ?SINVID, ?DEPID, ?DEPFROMID,?OPT)')
    OnExecuteError = quInsDSItemExecuteError
    Left = 196
    Top = 311
  end
  object quGetIns: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select i.D_InsId, i.EdgShId, i.EdgTId'
      'from D_Art a, Art2 a2, D_Ins i'
      'where a2.Art2Id= ?Art2Id and'
      '      a.D_ArtId=a2.D_ArtId and'
      '      i.D_InsId=a.D_InsId')
    Left = 788
    Top = 227
  end
  object quClient: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE CLIENT'
      'SET NAME=?NAME,'
      '        ADDRESS=?ADDRESS,'
      '        NODCARD=?NODCARD,'
      '        ADDRESSID=?ADDRESSID,'
      '        Home_Flat = ?Home_Flat,'
      '        FMAIN = ?FMAIN,'
      '        FDEL = ?FDEL,'
      '        Birthday = ?Birthday,'
      '        OldNodCard = ?OldNodCard'
      'WHERE CLIENTID=?OLD_CLIENTID')
    InsertSQL.Strings = (
      'insert into Client(CLIENTID, NAME, ADDRESS, NODCARD, ADDRESSID, '
      
        '                          HOME_FLAT, FMAIN, FDEL, Birthday, OldN' +
        'odcard)'
      'values (:CLIENTID, :NAME, :ADDRESS, :NODCARD,  :ADDRESSID, '
      '             :HOME_FLAT, :FMAIN, :FDEL, :Birthday, :OldNodcard)')
    RefreshSQL.Strings = (
      'SELECT CLIENTID, NAME, ADDRESS, NODCARD, ADDRESSID, '
      
        '               Home_Flat, depid, fmain, FDEL, birthday, oldnodca' +
        'rd'
      'FROM client_r (?CLIENTID, 0, 0)')
    SelectSQL.Strings = (
      'SELECT c.CLIENTID, c.NAME, c.ADDRESS, c.NODCARD, '
      
        '              c.ADDRESSID, c.Home_Flat, c.depid, c.fmain, c.Fdel' +
        ', c.birthday, c.oldnodcard'
      'FROM CLIENT c, d_dep d'
      'WHERE c.clientid<>-1000 and'
      '               c.depid = d.d_depid'
      'ORDER BY c.NAME')
    AfterPost = CommitRetaining
    OnNewRecord = quClientNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 424
    Top = 263
    object quClientCLIENTID: TIntegerField
      FieldName = 'CLIENTID'
      Required = True
    end
    object quClientNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quClientADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'CLIENT.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object quClientNODCARD: TFIBStringField
      FieldName = 'NODCARD'
      Origin = 'CLIENT.NODCARD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quClientADDRESSID: TIntegerField
      FieldName = 'ADDRESSID'
      Origin = 'CLIENT.ADDRESSID'
    end
    object quClientHOME_FLAT: TFIBStringField
      FieldName = 'HOME_FLAT'
      Origin = 'CLIENT.HOME_FLAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quClientDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'CLIENT.DEPID'
    end
    object quClientFMAIN: TIntegerField
      FieldName = 'FMAIN'
      Origin = 'CLIENT_S.FMAIN'
    end
    object quClientFDEL: TFIBSmallIntField
      FieldName = 'FDEL'
    end
    object quClientBIRTHDAY: TFIBDateTimeField
      FieldName = 'BIRTHDAY'
    end
    object quClientOLDNODCARD: TFIBStringField
      FieldName = 'OLDNODCARD'
      EmptyStrToNull = True
    end
  end
  object dsClient: TDataSource
    DataSet = quClient
    Left = 424
    Top = 307
  end
  object quInsClRet1: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'EXECUTE PROCEDURE INSCLIENTRET1'
      
        '( ?SELLITEMID, ?SDATE, ?NDATE, ?SN, ?SSF, ?SUPID, ?SPRICE, ?NDSI' +
        'D, :SINVID, :USERID, :CLIENTID, :RETID, :ISEND) ')
    Left = 188
    Top = 207
  end
  object quDepColor: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 660
    Top = 215
  end
  object quCheckUIDOSelled: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT SITEMID,  ART2ID, UID,  W, SZ, SPRICE, SDATE,'
      '              SSF, SN, NDATE, NDSID, SUPID, OPRICE,'
      '              PRICE2, SINFOID, PRICE0'
      'FROM CHECKUIDOSELLED(?DEPID, ?AUID, ?OPT_BUYER)')
    Left = 712
    Top = 235
  end
  object quSetSP: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'UPDATE Art2'
      'SET Price= ?Price,'
      '        TSPrice= ?TSPrice'
      'WHERE ARt2Id= ?ARt2Id')
    Left = 344
    Top = 199
  end
  object quCol: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_REC'
      'SET'
      ' CU_SBD0=?CU_SBD0,'
      ' CU_SBD1=?CU_SBD1,'
      ' CU_R=?CU_R,'
      ' CU_S0=?CU_S0,'
      ' CU_S1=?CU_S1,'
      ' CU_D0=?CU_D0,'
      ' CU_D1=?CU_D1,'
      ' CU_RT0=?CU_RT0,'
      ' CU_RT1=?CU_RT1,'
      ' CU_SL=?CU_SL,'
      ' CU_SO0=?CU_SO0,'
      ' CU_SO1=?CU_SO1,'
      ' CU_SR0=?CU_SR0,'
      ' CU_SR1=?CU_SR1,'
      ' CU_RO0 =?CU_RO0,'
      ' CU_RO1=?CU_RO1,'
      ' CU_IM0=?CU_IM0,'
      ' CU_IM1=?CU_IM1,'
      ' CU_AO=?CU_AO,'
      ' CU_AC=?CU_AC,'
      ' CU_SI=?CU_SI,'
      ' CU_RO=?CU_RO,'
      ' CU_RC=?CU_RC')
    SelectSQL.Strings = (
      'SELECT'
      '    CU_SBD0,'
      '    CU_SBD1,'
      '    CU_R,'
      '    CU_S0,'
      '    CU_S1,'
      '    CU_D0,'
      '    CU_D1,'
      '    CU_RT0,'
      '    CU_RT1,'
      '    CU_SL,'
      '    CU_SO0,'
      '    CU_SO1,'
      '    CU_SR0,'
      '    CU_SR1,'
      '    CU_RO0 ,'
      '    CU_RO1, '
      '    CU_IM0,'
      '    CU_IM1,'
      '    CU_AO,'
      '    CU_AC,'
      '    CU_SI,'
      '    CU_RO,'
      '    CU_RC'
      'FROM D_REC')
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 348
    Top = 7
    object quColCU_SBD0: TIntegerField
      FieldName = 'CU_SBD0'
    end
    object quColCU_SBD1: TIntegerField
      FieldName = 'CU_SBD1'
    end
    object quColCU_R: TIntegerField
      FieldName = 'CU_R'
    end
    object quColCU_S0: TIntegerField
      FieldName = 'CU_S0'
    end
    object quColCU_S1: TIntegerField
      FieldName = 'CU_S1'
    end
    object quColCU_D0: TIntegerField
      FieldName = 'CU_D0'
    end
    object quColCU_D1: TIntegerField
      FieldName = 'CU_D1'
    end
    object quColCU_RT0: TIntegerField
      FieldName = 'CU_RT0'
    end
    object quColCU_RT1: TIntegerField
      FieldName = 'CU_RT1'
    end
    object quColCU_SL: TIntegerField
      FieldName = 'CU_SL'
    end
    object quColCU_SO0: TIntegerField
      FieldName = 'CU_SO0'
    end
    object quColCU_SO1: TIntegerField
      FieldName = 'CU_SO1'
    end
    object quColCU_SR0: TIntegerField
      FieldName = 'CU_SR0'
    end
    object quColCU_SR1: TIntegerField
      FieldName = 'CU_SR1'
    end
    object quColCU_RO0: TIntegerField
      FieldName = 'CU_RO0'
    end
    object quColCU_RO1: TIntegerField
      FieldName = 'CU_RO1'
    end
    object quColCU_IM0: TIntegerField
      FieldName = 'CU_IM0'
      Origin = 'D_REC.CU_IM0'
    end
    object quColCU_IM1: TIntegerField
      FieldName = 'CU_IM1'
      Origin = 'D_REC.CU_IM1'
    end
    object quColCU_AO: TFIBIntegerField
      FieldName = 'CU_AO'
    end
    object quColCU_AC: TFIBIntegerField
      FieldName = 'CU_AC'
    end
    object quColCU_SI: TFIBIntegerField
      FieldName = 'CU_SI'
    end
    object quColCU_RO: TFIBIntegerField
      FieldName = 'CU_RO'
    end
    object quColCU_RC: TFIBIntegerField
      FieldName = 'CU_RC'
    end
  end
  object dsCol: TDataSource
    DataSet = quCol
    Left = 348
    Top = 51
  end
  object quMaxSDate: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT MAX(ADATE)'
      'FROM SELLITEM'
      'WHERE SELLID=?SELLID')
    Left = 516
    Top = 107
  end
  object quPHist: TpFIBDataSet
    SelectSQL.Strings = (
      'select PRORDID, PRORD, SETDATE, ITYPE, CLOSEEMPID,'
      '          SETEMPID, CLOSEEMP, SETEMP, PRICE, PRICE2,'
      '          OPTPRICE,  OLDP, OLDP2, OLDOP, SN, SDATE, SSF'
      'from PHIST_S (?Art2Id, ?DepId)'
      'order by SETDATE DESC')
    BeforeOpen = quPHistBeforeOpen
    OnCalcFields = quPHistCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 484
    Top = 11
    object quPHistPRORDID: TIntegerField
      FieldName = 'PRORDID'
    end
    object quPHistPRORD: TIntegerField
      FieldName = 'PRORD'
    end
    object quPHistSETDATE: TDateTimeField
      FieldName = 'SETDATE'
    end
    object quPHistITYPE: TIntegerField
      FieldName = 'ITYPE'
    end
    object quPHistCLOSEEMPID: TIntegerField
      FieldName = 'CLOSEEMPID'
    end
    object quPHistSETEMPID: TIntegerField
      FieldName = 'SETEMPID'
    end
    object quPHistCLOSEEMP: TFIBStringField
      FieldName = 'CLOSEEMP'
      Size = 60
      EmptyStrToNull = True
    end
    object quPHistSETEMP: TFIBStringField
      FieldName = 'SETEMP'
      Size = 60
      EmptyStrToNull = True
    end
    object quPHistPRICE: TFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quPHistPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quPHistOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      currency = True
    end
    object quPHistOLDP: TFloatField
      FieldName = 'OLDP'
      currency = True
    end
    object quPHistOLDP2: TFloatField
      FieldName = 'OLDP2'
      currency = True
    end
    object quPHistOLDOP: TFloatField
      FieldName = 'OLDOP'
      currency = True
    end
    object quPHistSN: TIntegerField
      FieldName = 'SN'
    end
    object quPHistSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object quPHistSNStr: TStringField
      FieldKind = fkCalculated
      FieldName = 'SNStr'
      Size = 40
      Calculated = True
    end
    object quPHistSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
  end
  object dsPHist: TDataSource
    DataSet = quPHist
    Left = 484
    Top = 55
  end
  object quUIDHist: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  ADATE, OP, SRC, DST, DOC, ITYPE, RN, CHECKNO,'
      '        PRORDID,SPrice, Price, SSF, NDate, inscounter, contract,'
      '        isclosed'
      'FROM UIDHist(?UID)'
      'order by inscounter, adate')
    OnCalcFields = quUIDHistCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 536
    Top = 11
    object quUIDHistADATE: TDateTimeField
      FieldName = 'ADATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDHistOP: TSmallintField
      FieldName = 'OP'
    end
    object quUIDHistSRC: TFIBStringField
      FieldName = 'SRC'
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDHistDST: TFIBStringField
      FieldName = 'DST'
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDHistDOC: TFIBStringField
      FieldName = 'DOC'
      EmptyStrToNull = True
    end
    object quUIDHistITYPE: TSmallintField
      FieldName = 'ITYPE'
    end
    object quUIDHistSOp: TStringField
      FieldKind = fkCalculated
      FieldName = 'SOp'
      Size = 40
      Calculated = True
    end
    object quUIDHistCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
    end
    object quUIDHistRN: TIntegerField
      FieldName = 'RN'
    end
    object quUIDHistSDoc: TStringField
      FieldKind = fkCalculated
      FieldName = 'SDoc'
      Size = 30
      Calculated = True
    end
    object quUIDHistPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'UIDHIST.PRORDID'
    end
    object quUIDHistSPRICE: TFloatField
      FieldName = 'SPRICE'
      Origin = 'UIDHIST.SPRICE'
    end
    object quUIDHistPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'UIDHIST.PRICE'
    end
    object quUIDHistSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'UIDHIST.SSF'
      EmptyStrToNull = True
    end
    object quUIDHistINSCOUNTER: TIntegerField
      FieldName = 'INSCOUNTER'
      Origin = 'UIDHIST.INSCOUNTER'
    end
    object quUIDHistNDATE: TDateTimeField
      FieldName = 'NDATE'
      Origin = 'UIDHIST.NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object quUIDHistISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quUIDHistStateInv: TStringField
      FieldKind = fkCalculated
      FieldName = 'StateInv'
      Size = 30
      Calculated = True
    end
    object quUIDHistCONTRACT: TFIBStringField
      FieldName = 'CONTRACT'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object dsUIDHist: TDataSource
    DataSet = quUIDHist
    Left = 536
    Top = 59
  end
  object quCheckSupUID: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'SELECT      SITEMID,  T, CONTRACT'
      'FROM  CHECKSUPUID2(?UID, ?DEPID, ?SUPID, ?CONTRACT$ID)')
    Left = 192
    Top = 263
  end
  object quCheckA2Exist: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select count(*)'
      'from Art2'
      'where Art2= ?Art2 and D_ArtId=?D_ArtId')
    Left = 440
    Top = 111
  end
  object quCheckAExist: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select count(*)'
      'from D_Art'
      'where Art=?Art')
    Left = 440
    Top = 155
  end
  object quUIDPHist: TpFIBDataSet
    SelectSQL.Strings = (
      'select CLOSEDate, PrOrd, Price2, OldP2, PrOrdId,'
      '    CLOSEEMP,    SETEMP,    PRICE,    OPTPRICE,    OLDP,'
      '    OLDOP,IType,    SN,    SINVID,    SSF,    DEPName,   color'
      'from UIDPHIST_S(?UID)')
    OnCalcFields = quPHistCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 616
    Top = 15
    object quUIDPHistCLOSEDATE: TDateTimeField
      FieldName = 'CLOSEDATE'
      Origin = 'UIDPHIST_S.CLOSEDATE'
    end
    object quUIDPHistPRORD: TFIBStringField
      FieldName = 'PRORD'
      Origin = 'UIDPHIST_S.PRORD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDPHistSNstr: TStringField
      FieldKind = fkCalculated
      FieldName = 'SNstr'
      Calculated = True
    end
    object quUIDPHistPRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'UIDPHIST_S.PRICE2'
      currency = True
    end
    object quUIDPHistOLDP2: TFloatField
      FieldName = 'OLDP2'
      Origin = 'UIDPHIST_S.OLDP2'
      currency = True
    end
    object quUIDPHistPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'UIDPHIST_S.PRORDID'
    end
    object quUIDPHistCLOSEEMP: TFIBStringField
      FieldName = 'CLOSEEMP'
      Origin = 'UIDPHIST_S.CLOSEEMP'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDPHistSETEMP: TFIBStringField
      FieldName = 'SETEMP'
      Origin = 'UIDPHIST_S.SETEMP'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDPHistPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'UIDPHIST_S.PRICE'
      currency = True
    end
    object quUIDPHistOPTPRICE: TFloatField
      FieldName = 'OPTPRICE'
      Origin = 'UIDPHIST_S.OPTPRICE'
      currency = True
    end
    object quUIDPHistOLDP: TFloatField
      FieldName = 'OLDP'
      Origin = 'UIDPHIST_S.OLDP'
      currency = True
    end
    object quUIDPHistOLDOP: TFloatField
      FieldName = 'OLDOP'
      Origin = 'UIDPHIST_S.OLDOP'
      currency = True
    end
    object quUIDPHistITYPE: TIntegerField
      FieldName = 'ITYPE'
      Origin = 'UIDPHIST_S.ITYPE'
    end
    object quUIDPHistSN: TIntegerField
      FieldName = 'SN'
      Origin = 'UIDPHIST_S.SN'
    end
    object quUIDPHistSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'UIDPHIST_S.SINVID'
    end
    object quUIDPHistSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'UIDPHIST_S.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDPHistDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'UIDPHIST_S.DEPNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quUIDPHistCOLOR: TIntegerField
      FieldName = 'COLOR'
      Origin = 'UIDPHIST_S.COLOR'
    end
  end
  object dsUIDPHist: TDataSource
    DataSet = quUIDPHist
    Left = 616
    Top = 59
  end
  object quGetA: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT FullArt, D_ArtId, D_CompId, D_InsId, D_MATID,   D_GOODID,' +
        ' D_COUNTRYID, ATT1, ATT2'
      'FROM D_Art'
      'WHERE Art=?Art')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 500
    Top = 207
    object quGetAFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_ART.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quGetAD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_ART.D_ARTID'
      Required = True
    end
    object quGetAD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_ART.D_COMPID'
      Required = True
    end
    object quGetAD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetAD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetAD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetAD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Origin = 'D_ART.D_COUNTRYID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quGetAATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quGetAATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
  end
  object quSameUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select UID, count(*) CNT'
      'from UIDWH_T'
      'where  depid between ?depid1 and ?depid2 and'
      '            userid = ?userid'
      'group by UID'
      'having count(*)>1')
    BeforeOpen = quSameUIDBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 620
    Top = 263
    object quSameUIDUID: TIntegerField
      FieldName = 'UID'
    end
    object quSameUIDCNT: TIntegerField
      FieldName = 'CNT'
      Required = True
    end
  end
  object dsSameUID: TDataSource
    DataSet = quSameUID
    Left = 624
    Top = 307
  end
  object quD_WH2: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      
        'SELECT D_ARTID, FULLART, QUANTITY, WEIGHT, UNITID, ART, D_COMPID' +
        ', D_MATID,'
      
        '       D_GOODID, D_INSID, D_COUNTRYID,PRODCODE, DQ, DW, DEPID, R' +
        'ESTQ, RESTW, ATT1, ATT2'
      'FROM D_WH2_R(?DEPID, ?D_ARTID, ?USERID)')
    SelectSQL.Strings = (
      
        'SELECT D_ARTID, FULLART, QUANTITY, WEIGHT,  UNITID,  ART, D_COMP' +
        'ID, D_MATID,'
      
        '              D_GOODID, D_INSID, D_COUNTRYID,PRODCODE, DQ, DW,  ' +
        'DEPID, RESTQ, RESTW, ATT1, ATT2'
      'FROM  '
      'D_WH2'
      
        '(?ADEPID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?COUNTRYID1,?COM' +
        'PID2, '
      
        '                           ?MATID2, ?GOODID2, ?INSID2, ?COUNTRYI' +
        'D2,?USERID, ?ART_1, ?ART_2, ?ATT1_1, ?ATT1_2, ?ATT2_1, ?ATT2_2)'
      'ORDER BY ART')
    BeforeOpen = quD_WH2BeforeOpen
    OnCalcFields = quD_WH2CalcFields
    AfterRefresh = quD_WH2AfterRefresh
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 28
    Top = 359
    object quD_WH2D_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_WH2.D_ARTID'
    end
    object quD_WH2FULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_WH2.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quD_WH2QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'D_WH2.QUANTITY'
    end
    object quD_WH2WEIGHT: TFloatField
      FieldName = 'WEIGHT'
      Origin = 'D_WH2.WEIGHT'
      DisplayFormat = '0.##'
    end
    object quD_WH2UNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'D_WH2.UNITID'
    end
    object quD_WH2ART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_WH2.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_WH2D_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_WH2.D_COMPID'
    end
    object quD_WH2D_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_WH2.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH2D_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_WH2.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH2D_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_WH2.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH2PRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'D_WH2.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH2DQ: TIntegerField
      FieldName = 'DQ'
      Origin = 'D_WH2.DQ'
    end
    object quD_WH2DW: TFloatField
      FieldName = 'DW'
      Origin = 'D_WH2.DW'
      DisplayFormat = '0.##'
    end
    object quD_WH2DEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_WH2.DEPID'
    end
    object quD_WH2RESTQ: TFIBStringField
      FieldName = 'RESTQ'
      Origin = 'D_WH2.RESTQ'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WH2RESTW: TFIBStringField
      FieldName = 'RESTW'
      Origin = 'D_WH2.RESTW'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WH2D_COUNTRYID: TFIBStringField
      DisplayLabel = #1057#1090#1088'.'
      DisplayWidth = 3
      FieldName = 'D_COUNTRYID'
      Origin = 'D_WH2.D_COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH2ATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quD_WH2ATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
  end
  object dsD_WH2: TDataSource
    DataSet = quD_WH2
    Left = 32
    Top = 403
  end
  object quD_UID2: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub 0')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'SELECT SITEMID, UID, W, SZ,'
      '       Price, Sup, SupId, NDS, IsClosed,'
      '       Art2, Art2Id, frepair'
      'from D_UID2_R (:SITEMID)')
    SelectSQL.Strings = (
      'SELECT SITEMID, UID, W, SZ,  PRICE, SUP, SUPID, NDS, ISCLOSED,'
      '    ART2, ART2ID, FREPAIR'
      'FROM D_UID2_S(?DEPID, ?D_ARTID, ?ISCLOSED)'
      'ORDER BY UID')
    AfterOpen = quD_UID2AfterOpen
    BeforeOpen = quD_UID2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    Filtered = True
    OnFilterRecord = quD_UID2FilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 96
    Top = 359
    object quD_UID2SITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'D_UID2_S.SITEMID'
    end
    object quD_UID2UID: TIntegerField
      FieldName = 'UID'
      Origin = 'D_UID2_S.UID'
    end
    object quD_UID2W: TFloatField
      FieldName = 'W'
      Origin = 'D_UID2_S.W'
    end
    object quD_UID2SZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'D_UID2_S.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_UID2PRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'D_UID2_S.PRICE'
      currency = True
    end
    object quD_UID2SUP: TFIBStringField
      FieldName = 'SUP'
      Origin = 'D_UID2_S.SUP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_UID2SUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'D_UID2_S.SUPID'
    end
    object quD_UID2NDS: TFIBStringField
      FieldName = 'NDS'
      Origin = 'D_UID2_S.NDS'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_UID2ISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
      Origin = 'D_UID2_S.ISCLOSED'
    end
    object quD_UID2ART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'D_UID2_S.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_UID2ART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'D_UID2_S.ART2ID'
    end
    object quD_UID2FREPAIR: TFIBSmallIntField
      FieldName = 'FREPAIR'
    end
  end
  object dsD_UID2: TDataSource
    DataSet = quD_UID2
    Left = 100
    Top = 403
  end
  object quD_SZ2: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  SZ, QUANTITY, WEIGHT, DQ, DW, T'
      'FROM  WHASZ_D(?D_ARTID, ?DEPID, ?INVID)'
      'ORDER BY SZ')
    BeforeOpen = quD_SZ2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 156
    Top = 359
    object quD_SZ2SZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'WHASZ_D.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_SZ2QUANTITY: TFloatField
      FieldName = 'QUANTITY'
      Origin = 'WHASZ_D.QUANTITY'
    end
    object quD_SZ2WEIGHT: TFloatField
      FieldName = 'WEIGHT'
      Origin = 'WHASZ_D.WEIGHT'
    end
    object quD_SZ2DQ: TFloatField
      FieldName = 'DQ'
      Origin = 'WHASZ_D.DQ'
    end
    object quD_SZ2DW: TFloatField
      FieldName = 'DW'
      Origin = 'WHASZ_D.DW'
    end
    object quD_SZ2T: TSmallintField
      FieldName = 'T'
      Origin = 'WHASZ_D.T'
    end
  end
  object dsD_SZ2: TDataSource
    DataSet = quD_SZ2
    Left = 160
    Top = 403
  end
  object quD_T2: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT QUANTITY, WEIGHT, DQ, DW'
      'FROM  WHASZ_DTOTAL(?D_ARTID, ?DEPID, ?SUPID1, ?SUPID2, '
      
        '                                            ?P1, ?P2, ?SZ1, ?SZ2' +
        ')'
      '')
    BeforeOpen = quD_UID2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 212
    Top = 359
    object quD_T2QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'WHASZ_DTOTAL.QUANTITY'
    end
    object quD_T2WEIGHT: TFloatField
      FieldName = 'WEIGHT'
      Origin = 'WHASZ_DTOTAL.WEIGHT'
    end
    object quD_T2DQ: TIntegerField
      FieldName = 'DQ'
      Origin = 'WHASZ_DTOTAL.DQ'
    end
    object quD_T2DW: TFloatField
      FieldName = 'DW'
      Origin = 'WHASZ_DTOTAL.DW'
    end
  end
  object dsD_T2: TDataSource
    DataSet = quD_T2
    Left = 216
    Top = 403
  end
  object quD_Q2: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_ARTID, DEPID, DEP, Q1, W1, Q2, W2'
      'FROM  DST2_S(?D_ARTID,  ?DEPFROMID)')
    BeforeOpen = quD_Q2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 264
    Top = 359
    object quD_Q2D_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'DST2_S.D_ARTID'
    end
    object quD_Q2DEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'DST2_S.DEPID'
    end
    object quD_Q2DEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'DST2_S.DEP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_Q2Q1: TIntegerField
      FieldName = 'Q1'
      Origin = 'DST2_S.Q1'
    end
    object quD_Q2W1: TFloatField
      FieldName = 'W1'
      Origin = 'DST2_S.W1'
    end
    object quD_Q2Q2: TIntegerField
      FieldName = 'Q2'
      Origin = 'DST2_S.Q2'
    end
    object quD_Q2W2: TFloatField
      FieldName = 'W2'
      Origin = 'DST2_S.W2'
    end
  end
  object dsD_Q2: TDataSource
    DataSet = quD_Q2
    Left = 264
    Top = 403
  end
  object quDst2: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SItem'
      'where SItemId= ?OLD_SItemId')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'select Dep, IsClosed, UID, SItemID,  W,  SZ,  DepId,  Ref, '
      '          Color,  Art2, Price2, NDS'
      'from Dst_ar(?SItemId)')
    SelectSQL.Strings = (
      'select Dep, IsClosed, UID, SItemID,  W,  SZ,  DepId,  Ref, '
      '          Color,  Art2, Price2, NDS'
      'from Dst_a(?DepFromId,  ?DepId1, ?DepId2, ?D_ArtId)'
      'order by Dep,  IsClosed,  UID')
    AfterPost = CommitRetaining
    BeforeOpen = quDst2BeforeOpen
    BeforePost = quDst2BeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 320
    Top = 359
    object quDst2DEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDst2ISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
      Origin = 'SINV.ISCLOSED'
    end
    object quDst2UID: TIntegerField
      FieldName = 'UID'
      Origin = 'SITEM.UID'
    end
    object quDst2SITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SITEM.SITEMID'
      Required = True
    end
    object quDst2W: TFloatField
      FieldName = 'W'
      Origin = 'SITEM.W'
    end
    object quDst2SZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'SITEM.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDst2DEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SINV.DEPID'
    end
    object quDst2REF: TIntegerField
      FieldName = 'REF'
      Origin = 'SITEM.REF'
    end
    object quDst2COLOR: TIntegerField
      FieldName = 'COLOR'
      Origin = 'D_DEP.COLOR'
    end
    object quDst2ART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDst2PRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'DST_A.PRICE2'
      currency = True
    end
    object quDst2NDS: TFIBStringField
      FieldName = 'NDS'
      Origin = 'DST_A.NDS'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsDst2: TDataSource
    DataSet = quDst2
    Left = 320
    Top = 403
  end
  object quGetUIDA2: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.Art2Id, a2.D_ArtId, a2.FullArt, a2.Art2'
      'from SItem si, Art2 a2'
      'where si.UID= ?UID and'
      '       a2.Art2Id= si.Art2Id')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 376
    Top = 359
    object quGetUIDA2ART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quGetUIDA2D_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quGetUIDA2FULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quGetUIDA2ART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
  end
  object dsGetUIDA2: TDataSource
    DataSet = quGetUIDA2
    Left = 376
    Top = 403
  end
  object quInsDUID2: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select  SELID, T'
      
        'from  InsDUID2(?UID, ?SINVID, ?DEPID, ?DEPFROMID,?OPT, ?CrUserId' +
        ')')
    OnExecuteError = quInsDSItemExecuteError
    Left = 508
    Top = 155
  end
  object quDepPrice: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select Price2'
      'from Price'
      'where Art2Id=?Art2Id and DepId=?DepId')
    Left = 448
    Top = 363
  end
  object quDepById: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select D_DepId, SSName'
      'from D_Dep'
      'where D_DepId>0 and ISDELETE<>1'
      'order by D_DepId')
    Left = 452
    Top = 411
  end
  object quInvRestCost: TpFIBDataSet
    SelectSQL.Strings = (
      'select RestSum'
      'from invrestcost (?SInvId) ')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 524
    Top = 411
    object quInvRestCostRESTSUM: TFloatField
      FieldName = 'RESTSUM'
      currency = True
    end
  end
  object quRestQ: TpFIBDataSet
    SelectSQL.Strings = (
      'select DepId, Dep, Q'
      'from TotalRestQ'
      '(?UserId, ?CompId1, ?CompId2, ?MatId1, ?MatId2,'
      '  ?GoodId1, ?GoodId2, ?InsId1, ?InsId2)'
      '')
    BeforeOpen = quRestQBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 704
    Top = 115
    object quRestQDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'TOTALRESTQ.DEPID'
    end
    object quRestQDEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'TOTALRESTQ.DEP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quRestQQ: TIntegerField
      FieldName = 'Q'
      Origin = 'TOTALRESTQ.Q'
    end
  end
  object dsRestQ: TDataSource
    DataSet = quRestQ
    Left = 704
    Top = 163
  end
  object quUIDPrice: TpFIBDataSet
    SelectSQL.Strings = (
      'select Price2'
      'from Price'
      'where Art2Id=?Art2Id and DepId=?DepId')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 680
    Top = 15
    object quUIDPricePRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'PRICE.PRICE2'
      Required = True
    end
  end
  object quDistredT: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q,'
      '       sum(si.w) W,'
      '       sum(si.Q0*e.Price2) Cost,'
      '       sum(si.Q0*sin.Price) SCost'
      'from SInv i, SEl e, SItem si, SInfo sin'
      'where i.Sinvid= ?SInvid and'
      '      e.SInvId=i.SInvId and'
      '      si.SElId=e.SElId and'
      '      sin.SInfoId=si.SInfoId')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 28
    Top = 459
    object quDistredTQ: TIntegerField
      FieldName = 'Q'
      Required = True
    end
    object quDistredTW: TFloatField
      FieldName = 'W'
    end
    object quDistredTCOST: TFloatField
      FieldName = 'COST'
      currency = True
    end
    object quDistredTSCOST: TFloatField
      FieldName = 'SCOST'
      currency = True
    end
  end
  object dsDistredT: TDataSource
    DataSet = quDistredT
    Left = 28
    Top = 507
  end
  object taItBuf: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from ItBuf'
      'where ItBufId=?OLD_ItBufId')
    InsertSQL.Strings = (
      'INSERT INTO ITBUF '
      '(ITBUFID, USERID, UID, W, SZ, SPRICE, PRICE, ART2ID)'
      'VALUES '
      '(?ITBUFID, ?USERID, ?UID, ?W, ?SZ, ?SPRICE, ?PRICE, ?ART2ID)')
    RefreshSQL.Strings = (
      'SELECT'
      '       b.ITBUFID, b.USERID, b.UID, b.W, b.SZ, b.SPRICE, b.PRICE,'
      '       b.Art2Id,'
      
        '       a2.Art, a2.Art2, a2.prodcode, a2.D_MatId, a2.D_GoodId, a2' +
        '.D_InsId'
      'FROM ITBUF b, Art2 a2'
      'WHERE b.ItBufId= ?ItBufId and'
      '      a2.Art2Id=b.Art2Id'
      'ORDER BY b.UID')
    SelectSQL.Strings = (
      'SELECT '
      
        '        b.ITBUFID, b.USERID, b.UID, b.W, b.SZ, b.SPRICE, b.PRICE' +
        ','
      '        b.Art2Id,'
      
        '       a2.Art, a2.Art2, a2.prodcode, a2.D_MatId, a2.D_GoodId, a2' +
        '.D_InsId'
      'FROM ITBUF b, Art2 a2'
      'WHERE b.USERID= ?USERID and'
      '      a2.Art2Id=b.Art2Id'
      'ORDER BY b.UID')
    AfterPost = CommitRetaining
    BeforeInsert = taItBufBeforeInsert
    BeforeOpen = taItBufBeforeOpen
    OnNewRecord = taItBufNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 76
    Top = 459
    object taItBufITBUFID: TIntegerField
      FieldName = 'ITBUFID'
      Origin = 'ITBUF.ITBUFID'
      Required = True
    end
    object taItBufUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ITBUF.USERID'
    end
    object taItBufUID: TIntegerField
      FieldName = 'UID'
      Origin = 'ITBUF.UID'
    end
    object taItBufW: TFloatField
      FieldName = 'W'
      Origin = 'ITBUF.W'
    end
    object taItBufSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'ITBUF.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taItBufART: TFIBStringField
      FieldName = 'ART'
      Origin = 'ART2.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taItBufART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taItBufPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'ART2.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taItBufD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'ART2.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taItBufD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'ART2.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taItBufD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'ART2.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taItBufART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'ITBUF.ART2ID'
    end
    object taItBufSPRICE: TFloatField
      FieldName = 'SPRICE'
      Origin = 'ITBUF.SPRICE'
      currency = True
    end
    object taItBufPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'ITBUF.PRICE'
      currency = True
    end
  end
  object dsItBuf: TDataSource
    DataSet = taItBuf
    Left = 76
    Top = 507
  end
  object quD_WH_T: TpFIBDataSet
    SelectSQL.Strings = (
      'select  TQ, TA, TW'
      'from  '
      'WH_T'
      '(:MATID1, :MATID2, :GOODID1, :GOODID2, :COMPID1,'
      
        '   :COMPID2, :COUNTRYID1, :COUNTRYID2,:ART1, :ART2, :INSID1,:INS' +
        'ID2)')
    BeforeOpen = quD_WH_TBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 132
    Top = 463
    object quD_WH_TTQ: TFIBStringField
      FieldName = 'TQ'
      Origin = 'WH_T.TQ'
      OnGetText = quD_WH_TTQGetText
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quD_WH_TTA: TFIBStringField
      FieldName = 'TA'
      Origin = 'WH_T.TA'
      OnGetText = quD_WH_TTQGetText
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object quD_WH_TTW: TFIBStringField
      FieldName = 'TW'
      Origin = 'WH_T.TW'
      OnGetText = quD_WH_TTQGetText
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
  end
  object dsD_WH_T: TDataSource
    DataSet = quD_WH_T
    Left = 132
    Top = 507
  end
  object taInvMarg: TpFIBDataSet
    UpdateSQL.Strings = (
      'update InvMarg'
      'set Margin=?Margin,'
      '     RoundNorm=?RoundNorm'
      'where InvMargId=?OLD_InvMargId')
    SelectSQL.Strings = (
      
        'SELECT  i.InvMargId, i.DepId, i.InvId, i.Margin, i.RoundNorm, d.' +
        'SName'
      'FROM D_DEP d, InvMarg i'
      'WHERE i.InvId= ?InvId and'
      '      d.D_DepId=i.DepId and '
      '      d.d_depid>0'
      'ORDER BY d.SNAME')
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taInvMargBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 196
    Top = 463
    object taInvMargINVMARGID: TIntegerField
      FieldName = 'INVMARGID'
      Origin = 'INVMARG.INVMARGID'
      Required = True
    end
    object taInvMargDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INVMARG.DEPID'
    end
    object taInvMargINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INVMARG.INVID'
    end
    object taInvMargMARGIN: TFloatField
      FieldName = 'MARGIN'
      Origin = 'INVMARG.MARGIN'
    end
    object taInvMargROUNDNORM: TSmallintField
      FieldName = 'ROUNDNORM'
      Origin = 'INVMARG.ROUNDNORM'
      OnGetText = taInvMargROUNDNORMGetText
      OnSetText = taInvMargROUNDNORMSetText
    end
    object taInvMargSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'D_DEP.SNAME'
      ReadOnly = True
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsInvMarg: TDataSource
    DataSet = taInvMarg
    Left = 192
    Top = 507
  end
  object quCheckOpenPr: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*)'
      'from Sel e, PrOrdItem pi, PrOrd p'
      'where e.SInvID= ?InvId and'
      '      pi.Art2Id= e.Art2Id and'
      '      p.PrOrdId=pi.PrOrdId and'
      '      p.IsSet=0')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 576
    Top = 123
  end
  object quNewCl: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'INSERT INTO CLIENT(ClientId, Name, Address)'
      'VALUES (?ClientId, ?Name, ?Address)')
    Left = 604
    Top = 179
  end
  object quSetInvRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETINVRSTATE'
      '(?INVID, ?RSTATE)')
    Left = 700
    Top = 315
  end
  object quSetPrOrdRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETPRORDRSTATE'
      '(?PRORDID, ?RSTATE)')
    Left = 700
    Top = 367
  end
  object quSetPActRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETPACTRSTATE'
      '(?PRORDID, ?RSTATE)')
    Left = 700
    Top = 415
  end
  object quSetSellRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETSELLRSTATE'
      '(?SELLID, ?RSTATE)')
    Left = 700
    Top = 463
  end
  object quFRest: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select D_ArtId, Art, FullArt, D_MatId, D_GoodId, D_InsId, ProdCo' +
        'de,'
      '          RQ, RW, SQ SQ1, SW SW1, '#39' '#39' SZ'
      'from D_Art'
      'where RQ>0 and'
      '      d_artid>0 and'
      '      art between :art1 and :art2'
      'order by art'
      ''
      '')
    BeforeOpen = quFRestBeforeOpen
    OnCalcFields = quFRestCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 460
    Top = 471
    object quFRestD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_ART.D_ARTID'
      Required = True
    end
    object quFRestART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quFRestFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_ART.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quFRestRQ: TIntegerField
      FieldName = 'RQ'
      Origin = 'FREST.RQ'
    end
    object quFRestRW: TFloatField
      FieldName = 'RW'
      Origin = 'FREST.RW'
      DisplayFormat = '0.##'
    end
    object quFRestD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFRestD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFRestD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFRestPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'D_ART.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFRestSQ1: TFIBStringField
      FieldName = 'SQ1'
      Origin = 'FREST.SQ1'
      Size = 200
      EmptyStrToNull = True
    end
    object quFRestSW1: TFIBStringField
      FieldName = 'SW1'
      Origin = 'FREST.SW1'
      Size = 200
      EmptyStrToNull = True
    end
    object quFRestSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsFRest: TDataSource
    DataSet = quFRest
    Left = 460
    Top = 519
  end
  object quCountry: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_COUNTRYID, NAME'
      'FROM D_COUNTRY'
      'ORDER BY SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 476
    Top = 263
    object quCountryD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Origin = 'D_COUNTRY.D_COUNTRYID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quCountryNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_COUNTRY.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsCountry: TDataSource
    DataSet = quCountry
    Left = 476
    Top = 307
  end
  object quRDInv: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT s.SINVID, s.DEPID, s.SN'
      'FROM sinv s'
      'WHERE s.DEPFROMID=?DEPFROMID and'
      '      s.SDATE BETWEEN  ?BD and  ?ED AND'
      '      s.ITYPE=2'
      ''
      '')
    SelectSQL.Strings = (
      'SELECT SINVID, DEPID, SN'
      'FROM r_dinv (:depfromid, :bd, :ed) '
      'WHERE '
      ''
      ''
      ''
      ''
      ''
      'ORDER BY SINVID')
    BeforeOpen = quRDInvBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 564
    Top = 567
    object quRDInvSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'SINV.SINVID'
      Required = True
    end
    object quRDInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SINV.DEPID'
    end
    object quRDInvSN: TIntegerField
      FieldName = 'SN'
      Origin = 'SINV.SN'
    end
  end
  object quRPrOrd: TpFIBDataSet
    SelectSQL.Strings = (
      'select PrOrdId, DepId, PrOrd'
      'from rep_prord (:bd, :ed)'
      'where'
      ''
      ''
      ''
      ''
      ''
      'order by PrOrdId')
    BeforeOpen = quRPrOrdBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 616
    Top = 471
    object quRPrOrdPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'PRORD.PRORDID'
      Required = True
    end
    object quRPrOrdDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'PRORD.DEPID'
    end
    object quRPrOrdPRORD: TIntegerField
      FieldName = 'PRORD'
      Origin = 'PRORD.PRORD'
    end
  end
  object quRPAct: TpFIBDataSet
    SelectSQL.Strings = (
      'select PrOrdId, DepId, PrOrd, NAct, FACT'
      'from ret_PAct (:BD, :ED)'
      'order by PrOrdId')
    BeforeOpen = quRPActBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 616
    Top = 518
    object quRPActPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'PRORD.PRORDID'
      Required = True
    end
    object quRPActDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'PRORD.DEPID'
    end
    object quRPActPRORD: TIntegerField
      FieldName = 'PRORD'
      Origin = 'PRORD.PRORD'
    end
    object quRPActNACT: TIntegerField
      FieldName = 'NACT'
      Origin = 'PRORD.NACT'
    end
    object quRPActFACT: TSmallintField
      FieldName = 'FACT'
      Origin = 'RET_PACT.FACT'
    end
  end
  object quRSell: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.SELLID, s.RN'
      'from SELL s'
      'where  s.DepId= ?DepId and'
      '            ONLYDATE(s.BD) between ?BD and ?ED and'
      '            s.ED is not null and'
      '            (s.RState<=2 or s.RState is null)'
      'order by s.SellId')
    BeforeOpen = quRSellBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 560
    Top = 471
    object quRSellSELLID: TIntegerField
      FieldName = 'SELLID'
      Origin = 'SELL.SELLID'
      Required = True
    end
    object quRSellRN: TIntegerField
      FieldName = 'RN'
      Origin = 'SELL.RN'
    end
  end
  object quRRet: TpFIBDataSet
    SelectSQL.Strings = (
      'select SINVID,SN'
      'from rret (:DepId, :BD, :ED)'
      'order by SInvId')
    BeforeOpen = quRRetBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 564
    Top = 519
    object quRRetSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'SINV.SINVID'
      Required = True
    end
    object quRRetSN: TIntegerField
      FieldName = 'SN'
      Origin = 'SINV.SN'
    end
  end
  object quD_WH3: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'SELECT D_ARTID, FULLART, QUANTITY, WEIGHT, UNITID, ART,SZ, D_COM' +
        'PID, D_MATID,'
      
        '       D_GOODID, D_INSID, D_COUNTRYID,PRODCODE, DQ, DW, DEPID,RE' +
        'STQ, RESTW, '
      '       ATT1, ATT2, flag, RESTA'
      'FROM D_WH3_R(?DEPID, ?D_ARTID, ?SZ,?FLAG)')
    SelectSQL.Strings = (
      
        'SELECT D_ARTID, FULLART, QUANTITY, WEIGHT,  UNITID,  ART, SZ , D' +
        '_COMPID, D_MATID,'
      
        '       D_GOODID, D_INSID, D_COUNTRYID,PRODCODE, DQ, DW,  DEPID, ' +
        'RESTQ, RESTW, ATT1, ATT2, FLAG, RESTA'
      'FROM  '
      'D_WH3'
      
        '(?ADEPID, ?COMPID1, ?MATID1, ?GOODID1, ?INSID1, ?COUNTRYID1,?COM' +
        'PID2, '
      
        ' ?MATID2, ?GOODID2, ?INSID2, ?COUNTRYID2,?ART_1, ?ART_2, ?ATT1_1' +
        ', ?ATT1_2, ?ATT2_1, ?ATT2_2)'
      'ORDER BY ART,SZ')
    BeforeOpen = quD_WH3BeforeOpen
    OnCalcFields = quD_WH3CalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 300
    Top = 463
    object quD_WH3D_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_WH3.D_ARTID'
    end
    object quD_WH3FULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_WH3.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quD_WH3QUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'D_WH3.QUANTITY'
    end
    object quD_WH3WEIGHT: TFloatField
      FieldName = 'WEIGHT'
      Origin = 'D_WH3.WEIGHT'
    end
    object quD_WH3UNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'D_WH3.UNITID'
    end
    object quD_WH3ART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_WH3.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_WH3SZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'D_WH3.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3D_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_WH3.D_COMPID'
    end
    object quD_WH3D_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_WH3.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3D_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_WH3.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3D_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_WH3.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3D_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Origin = 'D_WH3.D_COUNTRYID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3PRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'D_WH3.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quD_WH3DQ: TIntegerField
      FieldName = 'DQ'
      Origin = 'D_WH3.DQ'
    end
    object quD_WH3DW: TFloatField
      FieldName = 'DW'
      Origin = 'D_WH3.DW'
    end
    object quD_WH3DEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_WH3.DEPID'
    end
    object quD_WH3RESTQ: TFIBStringField
      FieldName = 'RESTQ'
      Origin = 'D_WH3.RESTQ'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WH3RESTW: TFIBStringField
      FieldName = 'RESTW'
      Origin = 'D_WH3.RESTW'
      Size = 200
      EmptyStrToNull = True
    end
    object quD_WH3ATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quD_WH3ATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quD_WH3FLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object quD_WH3RESTA: TFIBStringField
      FieldName = 'RESTA'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsD_WH3: TDataSource
    DataSet = quD_WH3
    Left = 300
    Top = 507
  end
  object quD_Q3: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT D_ARTID, DEPID, DEP, Q1, W1, Q2, W2'
      'FROM  DST3_S(?D_ARTID, ?ASZ, ?DEPFROMID)')
    BeforeOpen = quD_Q3BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 352
    Top = 463
    object quD_Q3D_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'DST3_S.D_ARTID'
    end
    object quD_Q3DEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'DST3_S.DEPID'
    end
    object quD_Q3DEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'DST3_S.DEP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quD_Q3Q1: TIntegerField
      FieldName = 'Q1'
      Origin = 'DST3_S.Q1'
    end
    object quD_Q3W1: TFloatField
      FieldName = 'W1'
      Origin = 'DST3_S.W1'
    end
    object quD_Q3Q2: TIntegerField
      FieldName = 'Q2'
      Origin = 'DST3_S.Q2'
    end
    object quD_Q3W2: TFloatField
      FieldName = 'W2'
      Origin = 'DST3_S.W2'
    end
  end
  object dsD_Q3: TDataSource
    DataSet = quD_Q3
    Left = 352
    Top = 515
  end
  object quDst3: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SItem'
      'where SItemId= ?OLD_SItemId')
    InsertSQL.Strings = (
      'execute procedure stub 0')
    RefreshSQL.Strings = (
      'select Dep, IsClosed, UID, SItemID,  W,  SZ,  DepId,  Ref, '
      '          Color,  Art2, Price2, NDS'
      'from Dst_ar(?SItemId)')
    SelectSQL.Strings = (
      'select Dep, IsClosed, UID, SItemID,  W,  SZ,  DepId,  Ref, '
      '          Color,  Art2, Price2, NDS'
      'from Dst_asz(?DepFromId,  ?DepId1, ?DepId2, ?D_ArtId, ?ASZ)'
      'order by Dep,  IsClosed,  UID')
    AfterPost = CommitRetaining
    BeforeOpen = quDst2BeforeOpen
    BeforePost = quDst3BeforePost
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 400
    Top = 463
    object quDst3DEP: TFIBStringField
      FieldName = 'DEP'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDst3ISCLOSED: TSmallintField
      FieldName = 'ISCLOSED'
      Origin = 'SINV.ISCLOSED'
    end
    object quDst3UID: TIntegerField
      FieldName = 'UID'
      Origin = 'SITEM.UID'
    end
    object quDst3SITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SITEM.SITEMID'
      Required = True
    end
    object quDst3W: TFloatField
      FieldName = 'W'
      Origin = 'SITEM.W'
    end
    object quDst3SZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'SITEM.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quDst3DepID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SINV.DEPID'
    end
    object quDst3REF: TIntegerField
      FieldName = 'REF'
      Origin = 'SITEM.REF'
    end
    object quDst3Color: TIntegerField
      FieldName = 'COLOR'
      Origin = 'D_DEP.COLOR'
    end
    object quDst3ART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDst3PRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'DST_ASZ.PRICE2'
    end
    object quDst3NDS: TFIBStringField
      FieldName = 'NDS'
      Origin = 'DST_ASZ.NDS'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsDst3: TDataSource
    DataSet = quDst3
    Left = 400
    Top = 507
  end
  object quACount: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*)'
      'from Art2 a2, SEl e'
      'where a2.D_ArtId= :D_ArtId and'
      '      e.Art2Id= a2.Art2Id')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 608
    Top = 411
  end
  object quSItemIType: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.IType'
      'from SItem si, SInfo sin, SInv i'
      'where si.SItemId= :SItemId and'
      '      sin.SInfoId=si.SInfoId and'
      '      i.SInvId=sin.SInvId')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 636
    Top = 103
    object quSItemITypeITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SINV.ITYPE'
    end
  end
  object quCheckUnclosedInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select ic, pc, ad'
      'from  CheckUnclosedInv(?DepId)'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 784
    Top = 279
  end
  object quCheckArtOnFil: TpFIBDataSet
    SelectSQL.Strings = (
      'select ex from CheckArtOnFil(?D_ArtId)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 780
    Top = 119
    object quCheckArtOnFilEX: TSmallintField
      FieldName = 'EX'
      Origin = 'CHECKARTONFIL.EX'
    end
  end
  object quCheckARt2OnFil: TpFIBDataSet
    SelectSQL.Strings = (
      'select ex from CheckArt2OnFil(?Art2Id)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm '
    Left = 780
    Top = 175
    object SmallintField1: TSmallintField
      FieldName = 'EX'
      Origin = 'CHECKARTONFIL.EX'
    end
  end
  object quFSZRest: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select a.D_ArtId, a.Art, a.FullArt, a.D_MatId, a.D_GoodId, a.D_I' +
        'nsId,'
      '       a.ProdCode, r.RQ, r.RW, r.SQ SQ1, r.SW SW1, r.SZ'
      'from D_Art a, ArtSZ r'
      'where a.D_ArtId=r.D_ArtId and'
      '      r.RQ>0 and'
      '      a.d_artid>0 and'
      '      a.art between :art1 and :art2'
      'order by a.Art'
      ''
      ''
      ''
      '')
    BeforeOpen = quFRestBeforeOpen
    OnCalcFields = quFSZRestCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 512
    Top = 471
    object quFSZRestD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'D_ART.D_ARTID'
      Required = True
    end
    object quFSZRestART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object quFSZRestFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'D_ART.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quFSZRestD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'D_ART.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFSZRestD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'D_ART.D_GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFSZRestD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'D_ART.D_INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFSZRestPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Origin = 'D_ART.PRODCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object quFSZRestRQ: TIntegerField
      FieldName = 'RQ'
      Origin = 'FASZREST.RQ'
    end
    object quFSZRestRW: TFloatField
      FieldName = 'RW'
      Origin = 'FASZREST.RW'
    end
    object quFSZRestSQ1: TFIBStringField
      FieldName = 'SQ1'
      Origin = 'FASZREST.SQ1'
      Size = 200
      EmptyStrToNull = True
    end
    object quFSZRestSW1: TFIBStringField
      FieldName = 'SW1'
      Origin = 'FASZREST.SW1'
      Size = 200
      EmptyStrToNull = True
    end
    object quFSZRestSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'FASZREST.SZ'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object quRecalcPrOrdQ: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure RecalcPrOrdQ ?PrOrdID')
    Left = 792
    Top = 327
  end
  object quRSINFO: TpFIBDataSet
    RefreshSQL.Strings = (
      'SELECT s.SINVID, s.DEPID, s.SN'
      'FROM sinv s'
      'WHERE s.DEPFROMID=?DEPFROMID and'
      '      s.SDATE BETWEEN  ?BD and  ?ED AND'
      '      s.ITYPE=2'
      '')
    SelectSQL.Strings = (
      'SELECT SinfoID, DEPID, SN, rep_sinfoid'
      'FROM rep_sinfo'
      'WHERE  '
      ''
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 680
    Top = 567
    object quRSINFODEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SINV.DEPID'
    end
    object quRSINFOSN: TIntegerField
      FieldName = 'SN'
      Origin = 'SINV.SN'
    end
    object quRSINFOSINFOID: TIntegerField
      FieldName = 'SINFOID'
      Origin = 'SINFO.SINFOID'
      Required = True
    end
    object quRSINFOREP_SINFOID: TIntegerField
      FieldName = 'REP_SINFOID'
      Origin = 'REP_SINFO.REP_SINFOID'
      Required = True
    end
  end
  object quSetSInfoRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETSInfoRSTATE'
      '(?SINFOID, ?RSTATE)')
    Left = 808
    Top = 467
  end
  object quRPConfirm: TpFIBDataSet
    SelectSQL.Strings = (
      'select PrOrdId, DepId, PrOrd, NAct'
      'from ret_confirm (:BD, :ED)'
      'order by PrOrdId')
    BeforeOpen = quRPConfirmBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 742
    Top = 517
    object quRPConfirmPRORDID: TIntegerField
      FieldName = 'PRORDID'
      Origin = 'RET_CONFIRM.PRORDID'
    end
    object quRPConfirmDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'RET_CONFIRM.DEPID'
    end
    object quRPConfirmPRORD: TIntegerField
      FieldName = 'PRORD'
      Origin = 'RET_CONFIRM.PRORD'
    end
    object quRPConfirmNACT: TIntegerField
      FieldName = 'NACT'
      Origin = 'RET_CONFIRM.NACT'
    end
  end
  object dsaddress: TDataSource
    DataSet = quaddress
    Left = 88
    Top = 561
  end
  object quaddress: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into D_Address (d_Address_ID, Address)'
      'values (?d_Address_ID, ?Address)'
      '')
    SelectSQL.Strings = (
      'Select d_Address_ID, Address'
      'From D_Address'
      'where d_Address_ID<>-1000')
    OnNewRecord = quaddressNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 24
    Top = 563
    object quaddressD_ADDRESS_ID: TIntegerField
      FieldName = 'D_ADDRESS_ID'
      Origin = 'D_ADDRESS.D_ADDRESS_ID'
      Required = True
    end
    object quaddressADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'D_ADDRESS.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quRUid_info: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT u.UID'
      'FROM sinv s, sel sl, sitem si, uid_info u'
      'WHERE s.DEPFROMID=?DEPFROMID and'
      '      s.SDATE BETWEEN  :BD and :ED AND'
      '      s.ITYPE=2 and'
      '      s.ISCLOSED=1 and'
      '      (s.RState<=2 or s.RState is null) and'
      ''
      '      and s.sinvid = sl.sinvid and'
      '      sl.selid = si.selid and'
      '      u.uid = si.uid '
      'ORDER BY u.uid')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 618
    Top = 567
  end
  object quRDelRec: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT s.delrecordsID, s.DEPID'
      'FROM delrecords s'
      'WHERE (s.RState<=2 or s.RState is null)'
      ' '
      'ORDER BY s.delrecordsid')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 744
    Top = 567
    object quRDelRecDELRECORDSID: TIntegerField
      FieldName = 'DELRECORDSID'
      Origin = 'DELRECORDS.DELRECORDSID'
      Required = True
    end
    object quRDelRecDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'DELRECORDS.DEPID'
    end
  end
  object quSetDelRec: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure setrstatedel'
      '(?DELRECORDSID, ?RSTATE)')
    Left = 460
    Top = 567
  end
  object quGrById: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select ID, Name'
      'from D_GR'
      'where id > -1000'
      'order by ID')
    Left = 256
    Top = 571
  end
  object quTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 788
    Top = 407
  end
  object taAtt1: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    SNAME,'
      '    SORTIND'
      'FROM'
      '    D_ATT1 '
      'order by SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    Description = 'B: '#1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103' '#1087#1088#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080' '#1087#1077#1088#1074#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072
    Left = 520
    Top = 263
    poSQLINT64ToBCD = True
    object taAtt1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taAtt1NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taAtt1SNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taAtt1SORTIND: TFIBIntegerField
      FieldName = 'SORTIND'
    end
  end
  object taAtt2: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    SNAME,'
      '    SORTIND'
      'FROM'
      '    D_ATT2 '
      'order by SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    Description = 'B: '#1048#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103' '#1087#1088#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1080' '#1087#1077#1088#1074#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072
    Left = 566
    Top = 263
    poSQLINT64ToBCD = True
    object taAtt2ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taAtt2NAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taAtt2SNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taAtt2SORTIND: TFIBIntegerField
      FieldName = 'SORTIND'
    end
  end
  object dsAtt1: TDataSource
    DataSet = taAtt1
    Left = 524
    Top = 307
  end
  object dsAtt2: TDataSource
    DataSet = taAtt2
    Left = 568
    Top = 307
  end
  object quRprOrd2: TpFIBDataSet
    SelectSQL.Strings = (
      'select PrOrdId, DepId, PrOrd'
      'from Rep_PrOrd2(:BD, :ED)'
      'order by PrOrdId')
    BeforeOpen = quRprOrd2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 678
    Top = 518
    poSQLINT64ToBCD = True
    object quRprOrd2PRORDID: TFIBIntegerField
      FieldName = 'PRORDID'
    end
    object quRprOrd2DEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRprOrd2PRORD: TFIBIntegerField
      FieldName = 'PRORD'
    end
  end
  object quCheckDeleteAct: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select tm.id1, dd.SName, dd.d_depid'
      'from tmpdata tm, d_dep dd'
      'where tm.datatype=3 and dd.d_depid=tm.id2'
      'order by tm.id2')
    Left = 344
    Top = 571
  end
  object quBirthday: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select Fyes'
      'from Bithday_Now (:clientid)')
    Left = 200
    Top = 571
  end
  object quRBeforePrord: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SNVP, STPRORD'
      'FROM BEFORE_REP_PRORD(:BD, :ED, :DEPN)')
    BeforeOpen = quRBeforePrordBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 824
    Top = 523
    poSQLINT64ToBCD = True
    object quRBeforePrordSNVP: TFIBIntegerField
      FieldName = 'SNVP'
    end
    object quRBeforePrordSTPRORD: TFIBStringField
      FieldName = 'STPRORD'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quRBeforePAct: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SNVP, STPRORD'
      'FROM RET_BEFORE_PACT(:BD, :ED)')
    BeforeOpen = quRBeforePActBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 824
    Top = 571
    poSQLINT64ToBCD = True
    object quRBeforePActSNVP: TFIBIntegerField
      FieldName = 'SNVP'
    end
    object quRBeforePActSTPRORD: TFIBStringField
      FieldName = 'STPRORD'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object quRActAllowances: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.SINVID, s.DEPFROMID, s.SN'
      'FROM sinv s'
      'WHERE s.SDATE BETWEEN  :BD and :ED AND'
      '      s.ITYPE=19 and'
      '      s.ISCLOSED=1 and'
      '     (s.RState<=2 or s.RState is null)'
      ''
      ''
      ''
      ''
      ''
      ''
      'ORDER BY SINVID')
    BeforeOpen = quRActAllowancesBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 912
    Top = 523
    poSQLINT64ToBCD = True
    object quRActAllowancesSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quRActAllowancesSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quRActAllowancesDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
  end
  object quSetActAllowances: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETActAllowancesRSTATE'
      '(?SINVID, ?RSTATE)')
    Left = 904
    Top = 467
  end
  object quRApplDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.SINVID, s.DEPID, s.SN, s.depFromId'
      'FROM sinv s'
      'WHERE s.SDATE BETWEEN  :BD and :ED AND'
      '      s.ITYPE=17 and'
      '      s.ISCLOSED=1 and'
      '     (s.RState<=2 or s.RState is null or s.Rstate=4)'
      ''
      ''
      ''
      ''
      ''
      ''
      'ORDER BY s.SINVID')
    BeforeOpen = quRApplDepBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 912
    Top = 571
    poSQLINT64ToBCD = True
    object quRApplDepSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quRApplDepDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRApplDepSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quRApplDepDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
  end
  object quSetApplDEpRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETApplDepRSTATE'
      '(?SINVID, ?RSTATE)')
    Left = 888
    Top = 411
  end
  object quRApplDepArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.SINVID, s.DEPID, s.SN'
      'FROM sinv s'
      'WHERE s.SDATE BETWEEN  :BD and :ED AND'
      '      s.ITYPE=20 and'
      '      s.ISCLOSED=1 and'
      '     (s.RState<=2 or s.RState is null or s.Rstate=4)'
      ''
      ''
      ''
      ''
      ''
      ''
      'ORDER BY s.SINVID')
    BeforeOpen = quRApplDepArtBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 896
    Top = 347
    poSQLINT64ToBCD = True
    object quRApplDepArtSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quRApplDepArtDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRApplDepArtSN: TFIBIntegerField
      FieldName = 'SN'
    end
  end
  object quRAppl: TpFIBDataSet
    SelectSQL.Strings = (
      'select applid, noappl'
      'from appl'
      'where rstate<=2 and isclosed=1')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 896
    Top = 251
    poSQLINT64ToBCD = True
    object quRApplAPPLID: TFIBIntegerField
      FieldName = 'APPLID'
    end
    object quRApplNOAPPL: TFIBIntegerField
      FieldName = 'NOAPPL'
    end
  end
  object quSetAppl: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure SETRStateAppl'
      '(?APPLID, ?RSTATE)')
    Left = 896
    Top = 299
  end
  object quSameUidP: TpFIBDataSet
    SelectSQL.Strings = (
      'select uid, price, priceinv'
      'from uidwh_t'
      'where userid=:userid and price<>priceinv')
    BeforeOpen = quSameUidPBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 888
    Top = 19
    poSQLINT64ToBCD = True
    object quSameUidPUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quSameUidPPRICE: TFIBFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quSameUidPPRICEINV: TFIBFloatField
      FieldName = 'PRICEINV'
      currency = True
    end
  end
  object dsSameUidP: TDataSource
    DataSet = quSameUidP
    Left = 888
    Top = 75
  end
  object quRSuspItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.SINVID, s.DEPID, s.SN'
      'FROM sinv s'
      'WHERE s.DEPID=:DEPFROMID and'
      '      s.SDATE BETWEEN  :BD and :ED AND'
      '      s.ITYPE=10 and'
      '     (s.RState<=2 or s.RState is null) and'
      
        '      s.sinvid not in (select userid from tmpdata where datatype' +
        '=9)'
      'order by s.sinvid')
    BeforeOpen = quRSuspItemBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 568
    Top = 619
    poSQLINT64ToBCD = True
    object quRSuspItemSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quRSuspItemDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRSuspItemSN: TFIBIntegerField
      FieldName = 'SN'
    end
  end
  object quRPactC: TpFIBDataSet
    SelectSQL.Strings = (
      'select PrOrdId, DepId, PrOrd, NAct'
      'from prord '
      
        'where setdate between :BD and :ED and itype in (9, 7) and rstate' +
        '_a<3'
      '      '
      'order by PrOrdId')
    BeforeOpen = quRPactCBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 624
    Top = 366
    object quRPactCPRORDID: TFIBIntegerField
      FieldName = 'PRORDID'
    end
    object quRPactCDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRPactCPRORD: TFIBIntegerField
      FieldName = 'PRORD'
    end
    object quRPactCNACT: TFIBIntegerField
      FieldName = 'NACT'
    end
  end
  object quRBuyInvoice: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, class$code, n, source$department$id'
      'from r$buy$invoice(:target$department$id)       ')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 840
    Top = 672
    oFetchAll = True
    object quRBuyInvoiceID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quRBuyInvoiceCLASSCODE: TFIBIntegerField
      FieldName = 'CLASS$CODE'
    end
    object quRBuyInvoiceN: TFIBIntegerField
      FieldName = 'N'
    end
    object quRBuyInvoiceSOURCEDEPARTMENTID: TFIBIntegerField
      FieldName = 'SOURCE$DEPARTMENT$ID'
    end
  end
  object quRBuyOrder: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  ID, N, Cr_Date, ADate, UserID, '
      '  MDate, DepID, State'
      'from '
      '  R_Buy_Orders(:Source$ID, :Target$ID)'
      '          ')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 760
    Top = 672
    oFetchAll = True
    object quRBuyOrderID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quRBuyOrderN: TFIBIntegerField
      FieldName = 'N'
    end
    object quRBuyOrderADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quRBuyOrderUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quRBuyOrderMDATE: TFIBDateTimeField
      FieldName = 'MDATE'
    end
    object quRBuyOrderDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quRBuyOrderSTATE: TFIBIntegerField
      FieldName = 'STATE'
    end
    object quRBuyOrderCR_DATE: TFIBDateTimeField
      FieldName = 'CR_DATE'
    end
  end
end
