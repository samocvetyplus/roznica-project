unit AddToPr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, RXSpin, Buttons, Mask, Menus;

type
  TfmAddToPr = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    seP1: TRxSpinEdit;
    seP2: TRxSpinEdit;
    seP3: TRxSpinEdit;
    cb1: TCheckBox;
    cb2: TCheckBox;
    cb3: TCheckBox;
    bDep: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bDepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddToPr: TfmAddToPr;

implementation
uses Data;

{$R *.DFM}

procedure TfmAddToPr.FormActivate(Sender: TObject);
begin
  cb1.Checked:=True;
  cb2.Checked:=True;
  cb3.Checked:=True;
end;

procedure TfmAddToPr.FormCreate(Sender: TObject);
begin
// dm.ClickUserDepMenu(dm.pmAddTopr);
end;

procedure TfmAddToPr.bDepClick(Sender: TObject);
begin
  dm.pmAddTopr.Popup(Mouse.CursorPos.X,Mouse.CursorPos.Y);
end;

end.
