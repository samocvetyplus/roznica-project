{********************************
 ����� ������ ������ ��� ��������

 Tanay
 ********************************}
unit DepSel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, CheckLst, DBTree, Buttons, comdata, ActnList,
  cxLookAndFeelPainters, cxButtons, cxControls, cxContainer, cxCheckListBox,
  cxCheckBox, ExtCtrls, Menus, dxSkinsCore, dxSkinsDefaultPainters, cxGraphics,
  cxLookAndFeels, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type

  Tfmdepsel = class(TForm)
    Actions: TActionList;
    ButtonOK: TcxButton;
    ButtonCancel: TcxButton;
    ActionCancel: TAction;
    EditorDepartments: TcxCheckListBox;
    Panel: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionCancelUpdate(Sender: TObject);
    procedure EditorDepartmentsClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
  private
    AllItems: TcxCheckListBoxItem;
    procedure LoadDepartments;
  public
    depcode : t_array_int; {������� ���� �������������}
  end;

var
  fmdepsel: Tfmdepsel;

implementation
uses data, pFIBQuery;
{$R *.dfm}


{���������� ch}
procedure Tfmdepsel.LoadDepartments;
var
  Rect: TRect;
  Item: TcxCheckListBoxItem;
  ItemHeight: Integer;
begin
  with dmcom, dm do
  begin
    with qutmp do
    begin
      if not Transaction.Active then Transaction.StartTransaction;
      if flcall_delsel=0 then
      sql.Text := 'select name, d_depid from d_dep where r_usedep = 1 and d_depid > -1000 and IsDelete <>1 and d_depid <>'+inttostr(dmcom.DefaultDep.DepId);
      if flcall_delsel=1 then
      sql.Text := 'select name, d_depid from d_dep where d_depid > -1000 and IsDelete <>1 ';
      if flcall_delsel=2 then
      sql.Text := 'select name, d_depid, enabled from GET$R$DEPARTMENTS';
      ExecQuery;
      AllItems := EditorDepartments.Items.Add;
      AllItems.Text := '*���';
      while not eof do
      begin
        Item := EditorDepartments.Items.Add;
        Item.Text := Fields[0].AsString;

        if flcall_delsel=2 then
        begin
          if Fields[2].AsInteger = 1 then Item.Enabled := True
          else
          begin
            AllItems.Enabled := False;
            Item.Enabled := False;
          end;
        end;
        setlength(depcode, length(depcode)+1);
        depcode[length(depcode)-1]:= fields[1].AsInteger;
        Next;
     end;
     Rect := EditorDepartments .ItemRect(EditorDepartments.Items.Count - 1);
     EditorDepartments.Height := EditorDepartments.GetHeight(EditorDepartments.Items.Count);
     Panel.Top := EditorDepartments.Top + EditorDepartments.Height;
     AutoSize := True;

     Transaction.CommitRetaining;
     Close;
    end;
  end;

end;

procedure Tfmdepsel.FormCreate(Sender: TObject);
begin
  LoadDepartments;
end;

procedure Tfmdepsel.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i:integer;
begin
  with dmcom do
   if ModalResult=mrOK then
   begin
     if dmcom.flcall_delsel in [0, 2] then
     begin
       SetLength(mas_dep,0);
       if not AllItems.Checked then
       begin
         for i:= 0 to EditorDepartments.Items.Count - 2 do
         begin
           if not EditorDepartments.Items[i+1].Checked then
           begin
             setlength(mas_dep, length(mas_dep)+1);
             mas_dep[length(mas_dep)-1]:=depcode[i];
           end;
         end;
       end;
    end;
    if dmcom.flcall_delsel = 1 then
    begin
      setlength(mas_dep,0);
      if not AllItems.Checked then
      begin
        for i := 0 to EditorDepartments.Items.Count-2 do
        begin
          if EditorDepartments.Items[i+1].Checked then
          begin
            setlength(mas_dep, length(mas_dep)+1);
            mas_dep[length(mas_dep)-1]:=depcode[i];
          end;
        end;
      end
      else
      begin
        for i := 0 to EditorDepartments.Items.Count-2 do
        begin
         setlength(mas_dep, length(mas_dep)+1);
         mas_dep[length(mas_dep)-1]:=depcode[i];
        end;
      end;
    end;
  end;
end;

procedure Tfmdepsel.ActionCancelUpdate(Sender: TObject);
var
  i: Integer;
  Enabled: Boolean;
begin
  Enabled := False;
  for i := 1 to EditorDepartments.Items.Count - 1 do
  if EditorDepartments.Items[i].Checked then
  begin
    Enabled := True;
    break;
  end;
  ButtonOK.Enabled := Enabled;
  ActionCancel.Enabled := True;
end;

procedure Tfmdepsel.EditorDepartmentsClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
var
  i:integer;
begin
  if AIndex = 0 then
  begin
    for i := 1 to EditorDepartments.Items.Count -1 do
    if EditorDepartments.Items[i].Enabled then
    EditorDepartments.Items[i].Checked := AllItems.Checked;
  end
  else
  begin
    i := 1;
    while (i <= EditorDepartments.Items.Count - 1) and (EditorDepartments.Items[i].Checked) do inc(i);
    if i = EditorDepartments.Items.Count then AllItems.Checked := True
    else Allitems.Checked := False;
  end;
end;

end.
