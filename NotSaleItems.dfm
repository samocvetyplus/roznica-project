object fmNotSaleItems: TfmNotSaleItems
  Left = 47
  Top = 166
  Caption = #1040#1085#1072#1083#1080#1079
  ClientHeight = 556
  ClientWidth = 1070
  Color = clBtnFace
  UseDockManager = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1070
    Height = 42
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 55
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siAppl: TSpeedItem
      BtnCaption = #1055#1086#1083#1103
      Caption = #1042#1099#1073#1086#1088' '#1087#1086#1083#1077#1081
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1079#1072#1103#1074#1082#1080
      ImageIndex = 32
      Spacing = 1
      Left = 3
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 58
      Top = 3
      Visible = True
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1042#1089#1090#1072#1074#1082#1080
      Caption = #1042#1089#1090#1072#1074#1082#1080
      Hint = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      Spacing = 1
      Left = 113
      Top = 3
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object siFind: TSpeedItem
      BtnCaption = #1055#1086#1080#1089#1082
      Caption = #1055#1086#1080#1089#1082
      Hint = #1055#1086#1080#1089#1082
      ImageIndex = 48
      Spacing = 1
      Left = 124
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spitHistory: TSpeedItem
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103
      Hint = #1048#1089#1090#1086#1088#1080#1103'|'
      ImageIndex = 13
      Spacing = 1
      Left = 179
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      BtnCaption = #1056#1077#1076'.'
      Caption = 'siEdit'
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103
      ImageIndex = 72
      Spacing = 1
      Left = 234
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      BtnCaption = #1054#1088#1075#1072#1085#1080#1079'.'
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
      ImageIndex = 7
      Spacing = 1
      Left = 289
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spItog: TSpeedItem
      BtnCaption = #1048#1090#1086#1075#1080
      Caption = #1048#1090#1086#1075#1080
      Hint = #1057#1091#1084#1084#1099' '#1080#1079#1076#1077#1083#1080#1081' '#1087#1086' '#1089#1082#1083#1072#1076#1072#1084
      ImageIndex = 9
      Spacing = 1
      Left = 344
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siCalc: TSpeedItem
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      Hint = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1086#1089#1090#1086#1103#1085#1080#1103' '#1089#1082#1083#1072#1076#1072'|'
      ImageIndex = 8
      Spacing = 1
      Left = 399
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siText: TSpeedItem
      BtnCaption = #1069#1082#1089#1087'. '#13#10#1074' '#1090#1077#1082#1089#1090
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090'|'
      Spacing = 1
      Left = 454
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 949
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 784
      Top = 3
      SectionName = 'Untitled (0)'
    end
  end
  object SpeedBar3: TSpeedBar
    Left = 0
    Top = 42
    Width = 1070
    Height = 31
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 1
    InternalVer = 1
    object Label3: TLabel
      Left = 5
      Top = 1
      Width = 154
      Height = 13
      Caption = #1059#1082#1072#1078#1080#1090#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1084#1077#1089#1103#1094#1077#1074' '
      Transparent = True
    end
    object Label6: TLabel
      Left = 4
      Top = 14
      Width = 265
      Height = 13
      Caption = ' '#1074' '#1090#1077#1095#1077#1085#1080#1080' '#1082#1086#1090#1086#1088#1099#1093' '#1080#1079#1076#1077#1083#1080#1077' '#1086#1089#1090#1072#1074#1072#1083#1086#1089#1100' '#1085#1072' '#1089#1082#1083#1072#1076#1077':'
      Transparent = True
    end
    object edMonth: TComboEdit
      Left = 288
      Top = 4
      Width = 101
      Height = 21
      Color = 16776176
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      NumGlyphs = 1
      TabOrder = 0
      OnButtonClick = edMonthButtonClick
      OnKeyDown = edMonthKeyDown
      OnKeyUp = edMonthKeyUp
    end
    object SpeedbarSection5: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object cxGrid2: TcxGrid
    Left = 0
    Top = 73
    Width = 1070
    Height = 483
    Align = alClient
    BorderStyle = cxcbsNone
    TabOrder = 2
    object cxGrid2DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsNew
      DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skCount
          Position = spFooter
          Column = cxGrid2DBTableView1Column1
        end
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid2DBTableView1Column4
        end
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid2DBTableView1Column5
        end
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid2DBTableView1Column6
        end
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid2DBTableView1Column7
        end
        item
          Kind = skSum
          Position = spFooter
          Column = cxGrid2DBTableView1Column11
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          Column = cxGrid2DBTableView1Column1
        end
        item
          Kind = skSum
          FieldName = 'SPRICE0'
          Column = cxGrid2DBTableView1Column4
        end
        item
          Kind = skSum
          FieldName = 'PRICE'
          Column = cxGrid2DBTableView1Column11
        end
        item
          Kind = skSum
          FieldName = 'COST1'
          Column = cxGrid2DBTableView1Column5
        end
        item
          Kind = skSum
          FieldName = 'COST'
          Column = cxGrid2DBTableView1Column6
        end
        item
          Kind = skSum
          FieldName = 'W'
          Column = cxGrid2DBTableView1Column7
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.GroupBySorting = True
      OptionsCustomize.GroupRowSizing = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      object cxGrid2DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'UID'
      end
      object cxGrid2DBTableView1Column2: TcxGridDBColumn
        DataBinding.FieldName = 'ART'
      end
      object cxGrid2DBTableView1Column3: TcxGridDBColumn
        DataBinding.FieldName = 'ART2'
      end
      object cxGrid2DBTableView1Column4: TcxGridDBColumn
        DataBinding.FieldName = 'SPRICE0'
      end
      object cxGrid2DBTableView1Column5: TcxGridDBColumn
        DataBinding.FieldName = 'PRICE'
      end
      object cxGrid2DBTableView1Column6: TcxGridDBColumn
        Caption = #1055#1088#1080#1093'.'#1089#1090#1086#1080#1084'.'
        DataBinding.FieldName = 'COST1'
        Visible = False
      end
      object cxGrid2DBTableView1Column7: TcxGridDBColumn
        DataBinding.FieldName = 'COST'
      end
      object cxGrid2DBTableView1Column8: TcxGridDBColumn
        DataBinding.FieldName = 'MATID'
      end
      object cxGrid2DBTableView1Column9: TcxGridDBColumn
        DataBinding.FieldName = 'GOODNAME'
      end
      object cxGrid2DBTableView1Column10: TcxGridDBColumn
        DataBinding.FieldName = 'INSID'
      end
      object cxGrid2DBTableView1Column11: TcxGridDBColumn
        DataBinding.FieldName = 'W'
      end
      object cxGrid2DBTableView1Column12: TcxGridDBColumn
        DataBinding.FieldName = 'SZ'
      end
      object cxGrid2DBTableView1Column13: TcxGridDBColumn
        DataBinding.FieldName = 'UNIT_'
      end
      object cxGrid2DBTableView1Column14: TcxGridDBColumn
        DataBinding.FieldName = 'DEP'
      end
      object cxGrid2DBTableView1Column15: TcxGridDBColumn
        DataBinding.FieldName = 'PRODCODE'
      end
      object cxGrid2DBTableView1Column16: TcxGridDBColumn
        DataBinding.FieldName = 'SUPNAME'
      end
      object cxGrid2DBTableView1Column17: TcxGridDBColumn
        DataBinding.FieldName = 'SN'
      end
      object cxGrid2DBTableView1Column18: TcxGridDBColumn
        DataBinding.FieldName = 'SN0'
      end
      object cxGrid2DBTableView1Column19: TcxGridDBColumn
        DataBinding.FieldName = 'SSF0'
      end
      object cxGrid2DBTableView1Column20: TcxGridDBColumn
        DataBinding.FieldName = 'SDATE'
      end
      object cxGrid2DBTableView1Column21: TcxGridDBColumn
        DataBinding.FieldName = 'SDATE0'
      end
      object cxGrid2DBTableView1Column22: TcxGridDBColumn
        DataBinding.FieldName = 'SELLDATE'
      end
      object cxGrid2DBTableView1Column23: TcxGridDBColumn
        DataBinding.FieldName = 'NDATE'
      end
      object cxGrid2DBTableView1Column24: TcxGridDBColumn
        DataBinding.FieldName = 'NAMEDEPFROM'
      end
      object cxGrid2DBTableView1Column25: TcxGridDBColumn
        DataBinding.FieldName = 'COUNT_MONTH'
      end
      object cxGrid2DBTableView1Column26: TcxGridDBColumn
        Caption = #1042#1089#1090#1072#1074#1082#1072
        DataBinding.FieldName = 'INS_STR'
        Width = 649
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object quNew: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT UID, ART, ART2ID, ART2, SPRICE0, PRICE, COST, COST1, mati' +
        'd, goodname, insid, W, UNIT_,'
      
        '       SZ, dep, prodcode, sn, sn0, ssf0, SDATE0, sdate, SELLDATE' +
        ', ndate,'
      '       namedepfrom, supname, count_month, ins_str'
      'FROM New_NotSell (:count_m);'
      ''
      ''
      ''
      '')
    BeforeOpen = quNewBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 288
    oAutoFormatFields = False
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1048#1076'.'#8470
      DisplayWidth = 10
      FieldName = 'UID'
    end
    object FIBFloatField1: TFIBFloatField
      DisplayLabel = #1055#1088#1080#1093'.'#1094#1077#1085#1072
      DisplayWidth = 10
      FieldName = 'SPRICE0'
    end
    object FIBFloatField2: TFIBFloatField
      DisplayLabel = #1056#1072#1089#1093'.'#1094#1077#1085#1072
      DisplayWidth = 10
      FieldName = 'PRICE'
    end
    object FIBFloatField3: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      DisplayWidth = 10
      FieldName = 'COST'
    end
    object FIBStringField1: TFIBStringField
      DisplayLabel = #1052#1072#1090'.'
      DisplayWidth = 10
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084'.'
      DisplayWidth = 20
      FieldName = 'GOODNAME'
      EmptyStrToNull = True
    end
    object FIBStringField3: TFIBStringField
      DisplayLabel = #1054#1042
      DisplayWidth = 10
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      DisplayLabel = #1040#1088#1090
      DisplayWidth = 20
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object FIBStringField5: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      DisplayWidth = 20
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object FIBStringField6: TFIBStringField
      DisplayLabel = #1045#1048
      FieldName = 'UNIT_'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBFloatField4: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      DisplayWidth = 10
      FieldName = 'W'
    end
    object FIBStringField7: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      DisplayWidth = 10
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBStringField8: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      DisplayWidth = 20
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object FIBStringField9: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      DisplayWidth = 10
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBIntegerField2: TFIBIntegerField
      DisplayLabel = #1042#1055
      DisplayWidth = 10
      FieldName = 'SN'
    end
    object FIBIntegerField3: TFIBIntegerField
      DisplayLabel = #1042#1085#1091#1090#1088'.'#8470
      DisplayWidth = 10
      FieldName = 'SN0'
    end
    object FIBStringField10: TFIBStringField
      DisplayLabel = #1042#1085#1077#1096'.'#8470
      DisplayWidth = 20
      FieldName = 'SSF0'
      EmptyStrToNull = True
    end
    object FIBDateField1: TFIBDateField
      DisplayLabel = #1042#1085#1091#1090#1088'.'#1076#1072#1090#1072
      DisplayWidth = 10
      FieldName = 'SDATE0'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object FIBDateField2: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' '#1042#1055
      DisplayWidth = 10
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object FIBDateField3: TFIBDateField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
      DisplayWidth = 10
      FieldName = 'SELLDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object FIBDateField4: TFIBDateField
      DisplayLabel = #1042#1085#1077#1096'.'#1076#1072#1090#1072
      DisplayWidth = 10
      FieldName = 'NDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object FIBStringField11: TFIBStringField
      DisplayLabel = #1050#1086#1085'.'#1089#1082#1083#1072#1076
      DisplayWidth = 20
      FieldName = 'NAMEDEPFROM'
      EmptyStrToNull = True
    end
    object FIBStringField12: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090'.'
      DisplayWidth = 51
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object FIBIntegerField4: TFIBIntegerField
      DisplayLabel = #1050'-'#1074#1086' '#1084#1077#1089'.'
      DisplayWidth = 15
      FieldName = 'COUNT_MONTH'
    end
    object quNewCOST1: TFIBFloatField
      DisplayLabel = #1057#1090'.'#1087#1088#1080#1093'.'
      FieldName = 'COST1'
    end
    object quNewART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quNewINS_STR: TFIBStringField
      FieldName = 'INS_STR'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsNew: TDataSource
    DataSet = quNew
    Left = 16
    Top = 320
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    ExplorerStubLink = dxComponentPrinter1Link1
    Version = 0
    Left = 72
    Top = 304
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42699.452766076390000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsPreview.MaxLineCount = 3
      OptionsPreview.Visible = False
      OptionsSize.AutoWidth = True
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
end
