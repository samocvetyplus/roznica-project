object ColorShema: TColorShema
  Left = 340
  Top = 161
  Caption = #1076#1077#1088#1077#1074#1086' '#1062#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072
  ClientHeight = 506
  ClientWidth = 775
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cxVerticalGrid1: TcxVerticalGrid
    Left = 8
    Top = 8
    Width = 449
    Height = 329
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    OptionsView.CategoryExplorerStyle = True
    TabOrder = 0
    Version = 1
    object cxVerticalGrid1CategoryRow1: TcxCategoryRow
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow2: TcxCategoryRow
      Properties.Caption = #1057#1082#1083#1072#1076' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object cxVerticalGrid1EditorRow1: TcxEditorRow
      Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.EditProperties.ColorValueFormat = cxcvInteger
      Properties.EditProperties.CustomColors = <>
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 2
      ParentID = 1
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow2: TcxEditorRow
      Properties.EditPropertiesClassName = 'TcxColorComboBoxProperties'
      Properties.EditProperties.CustomColors = <>
      Properties.EditProperties.DefaultColor = clMenuHighlight
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 3
      ParentID = 2
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1CategoryRow3: TcxCategoryRow
      Properties.Caption = #1055#1086#1089#1090#1072#1074#1082#1080
      ID = 4
      ParentID = -1
      Index = 2
      Version = 1
    end
    object cxVerticalGrid1EditorRow3: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 5
      ParentID = 4
      Index = 0
      Version = 1
    end
    object cxVerticalGrid1EditorRow4: TcxEditorRow
      Properties.DataBinding.ValueType = 'String'
      Properties.Value = Null
      ID = 6
      ParentID = 4
      Index = 1
      Version = 1
    end
  end
end
