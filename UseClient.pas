unit UseClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ActnList;

type
  TfmUseClient = class(TForm)
    dg1: TDBGridEh;
    acList: TActionList;
    acClose: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUseClient: TfmUseClient;

implementation
uses comdata, M207Proc;
{$R *.dfm}

procedure TfmUseClient.FormCreate(Sender: TObject);
begin
 ReOpenDataSets([dmcom.quUseClient]);
end;

procedure TfmUseClient.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 PostDataSets([dmcom.quUseClient]);
 CloseDataSets([dmcom.quUseClient]);
end;

procedure TfmUseClient.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmUseClient.FormShow(Sender: TObject);
var st:string;
begin
 if dmCom.taAddressADDRESS.IsNull then st:=''
 else st:=dmCom.taAddressADDRESS.AsString;
 fmUseClient.Caption:='Покупатели - '+trim(st)+'('+
 trim(dmCom.taAddressD_ADDRESS_ID.AsString)+')';
end;

end.
