unit CurrStArt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Ancestor, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, ComCtrls, db, pFIBDataSet, ImgList, jpeg,
  rxPlacemnt, rxSpeedbar;

type
  TfmCurrStArt = class(TfmAncestor)
    fmstrStArt: TFormStorage;
    pgctrl: TPageControl;
    tbshSz: TTabSheet;
    tbshArt2: TTabSheet;
    grSz: TM207IBGrid;
    tbshInv: TTabSheet;
    ibgrA2: TM207IBGrid;
    dbgrWHInv: TM207IBGrid;
    tbshUID: TTabSheet;
    ibgrWHUID: TM207IBGrid;
    spitAddAppl: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure grSzGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure ibgrA2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dbgrWHInvGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure ibgrWHUIDGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure pgctrlChange(Sender: TObject);
  private
    procedure OpenCurrDS;
  end;

var
  fmCurrStArt: TfmCurrStArt;

implementation

uses ServData, comdata, Data, Data2 , M207Proc;

{$R *.DFM}

procedure TfmCurrStArt.FormCreate(Sender: TObject);
begin
  if not dmcom.tr.Active then dmcom.tr.StartTransaction
  else dmcom.tr.CommitRetaining;
  //with dmServ do OpenDataSets([quWHSZ, quWHArt2, qrWHInv, quWHUID]);
  Caption := '�����. ������� ��������� �������� '+dmServ.taWHART.AsString;
  pgctrl.ActivePage := tbshUID;
  OpenCurrDS;
  //--------- ��������� ����� ��� ��������� �� �������� ----------------//
  dbgrWHInv.ColumnByName['SPRICE'].Visible := CenterDep;
  ibgrWHUID.ColumnByName['SPRICE'].Visible := CenterDep;
  //---------------------------------------------------------------------//
end;

procedure TfmCurrStArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dmcom.tr.Active then dmcom.tr.CommitRetaining;
  with dmServ do
    CloseDatasets([quWHSZ, quWHArt2, qrWHInv, quWHUID]);
  Action := caFree;
end;

procedure TfmCurrStArt.grSzGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight or grSz.ClearHighlight
  then Background := dm2.GetDepColor(dmServ.quWHSZDEPID.AsInteger)
  else Background:=clHighlight;
  if Assigned(Field)and(Field.FieldName='SZ') then Background := dmCom.clMoneyGreen;
  if Assigned(Field)and(Field.FieldName='APPL')then Background := clInfoBk;
end;

procedure TfmCurrStArt.ibgrA2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight or ibgrA2.ClearHighlight
  then Background := dm2.GetDepColor(dmServ.quWHArt2D_DEPID.AsInteger)
  else Background := clHighlight;
  if (Field<>NIL) and (Field.FieldName='ART2') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmCurrStArt.dbgrWHInvGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Assigned(Field) and (Field.FieldName='ART2')
  then Background:=dmCom.clMoneyGreen
end;

procedure TfmCurrStArt.ibgrWHUIDGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight or ibgrWHUID.ClearHighlight
  then Background := dm2.GetDepColor(dmServ.quWHUIDDEPID.AsInteger)
  else Background := clHighlight;
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmCurrStArt.OpenCurrDS;

  procedure Open(ds : TpFIbDataSet);
  begin
    if ds.Active then eXit;
    Screen.Cursor := crSQLWait;
    try
      ds.Open;
    finally
      Screen.Cursor := crDefault;
    end;
  end;

begin
  case pgctrl.ActivePage.Tag of
    1 : Open(dmServ.quWHSZ);
    2 : Open(dmServ.quWHArt2);
    3 : Open(dmServ.qrWHInv);
    4 : Open(dmServ.quWHUID);
  end;
end;

procedure TfmCurrStArt.pgctrlChange(Sender: TObject);
begin
  OpenCurrDS;
end;

end.
