unit AddUidAct;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, Mask, ActnList;

type
  TfmAddUidAct = class(TForm)
    plUid: TPanel;
    LUid: TLabel;
    edUid: TEdit;
    plInfo: TPanel;
    plButton: TPanel;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    edNewPrice: TEdit;
    LNewPrice: TLabel;
    edOldPrice: TEdit;
    LOldPrice: TLabel;
    LCrData: TLabel;
    edDateAct: TEdit;
    btDate: TBitBtn;
    cbunitid: TComboBox;
    LUnitid: TLabel;
    acList: TActionList;
    acEnter: TAction;
    LFullArt: TLabel;
    LSn: TLabel;
    edSn: TEdit;
    procedure edUidKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btDateClick(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
    procedure edSnKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddUidAct: TfmAddUidAct;

implementation
uses SetSDate, comdata, data, pFIBQuery, FIBQuery, M207Proc;
{$R *.dfm}

procedure TfmAddUidAct.edUidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else SysUtils.Abort
 end;
end;

procedure TfmAddUidAct.FormCreate(Sender: TObject);
begin
 edNewPrice.Enabled:=false;
 edOldPrice.Enabled:=false;
 btDate.Enabled:=false;
 cbunitid.Enabled:=false;
 edSn.Enabled:=false;
 cbunitid.ItemIndex:=0;
 LFullArt.Caption:=''
end;

procedure TfmAddUidAct.FormClose(Sender: TObject; var Action: TCloseAction);
var i:integer;
begin
 if ModalResult= mrok then begin
 try
  try
   strtoint(eduid.text);
  except
   Application.MessageBox('�� ����� ������ ����� �������','������!!',0);
  end;

  try
   StrToFloat(edNewPrice.text);
  except
   Application.MessageBox('�� ����� ������� ����� ���� �������','������!!',0);
  end;

  try
   StrToFloat(edOldPrice.text);
  except
   Application.MessageBox('�� ����� ������� ������ ���� �������','������!!',0);
  end;

  try
   StrTodatetime(edDateAct.text);
  except
   Application.MessageBox('�� ������� ���� �������� ����','������!!',0);
  end;

  if edSn.Text='' then i:=-1 else i:=strtoint(edSn.Text);

  with dm, qutmp do begin
   close;
   sql.Text:='select prordid from Create_UIDACT ('+edUid.Text+', '+edNewPrice.Text+
    ', '+edOldPrice.Text+', '#39+edDateAct.Text+#39', '+inttostr(cbunitid.ItemIndex-1)+
    ', '+inttostr(dmcom.UserId)+', '+inttostr(dm.SDepId)+', '+inttostr(i)+')';
   ExecQuery;
   i:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
  end;
  if i=0 then Application.MessageBox('�� ���� ������ �� ����� ���� ������ ���','������!!',0)
  else begin
   ReOpenDataSets([dm.taPrOrd]);
   dm.taPrOrd.Locate('PRORDID', i, []);
  end
 except
  Application.MessageBox('�� ��� ��������� �������','������!!',0)
 end
end;
end;

procedure TfmAddUidAct.btDateClick(Sender: TObject);
var d:tdatetime;
begin
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dmcom.GetServerTime;
   fmSetSDate.tp1.Time:=dmcom.GetServerTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    edDateAct.Text:= datetimetostr(d);
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmAddUidAct.acEnterExecute(Sender: TObject);
var st:string;
    fgo:boolean;
begin
 if ActiveControl=edUid then begin
  fgo:=true;
  if edUid.Text='' then begin
   fgo:=false;
   Application.MessageBox('������� ����� �������','��������!!',0);
  end;
  if fgo then begin
   with dm, qutmp do begin
    close;
    sql.Text:='select first 1 a2.fullart from sitem si, art2 a2 '+
     'where si.art2id=a2.art2id and si.uid='+eduid.Text;
    ExecQuery;
    if Fields[0].IsNull then st:='' else st:=trim(Fields[0].asstring);
    close;
   end;
   if st='' then Application.MessageBox('������ ������ �� ����������','��������!!',0)
   else begin
    edNewPrice.Enabled:=true;
    edOldPrice.Enabled:=true;
    btDate.Enabled:=true;
    cbunitid.Enabled:=true;
    edSn.Enabled:=true;    
    LFullArt.Caption:='������ ������� - '+st;
    ActiveControl:=edNewPrice;
   end
  end
 end else if ActiveControl=edNewPrice then  ActiveControl:=edOldPrice
 else if ActiveControl=edOldPrice then begin
  btDateClick(nil);
  ActiveControl:=btOk
 end  else if ActiveControl=edSn then  ActiveControl:=btOk
 else if ActiveControl=btOk then  ModalResult:=mrOk
 else if ActiveControl=btCancel then ModalResult:=mrCancel
end;

procedure TfmAddUidAct.edSnKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else SysUtils.Abort
 end;
end;

end.
