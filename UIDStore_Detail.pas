unit UIDStore_Detail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls, jpeg, DBGridEhGrouping,
  rxSpeedbar, GridsEh;

type
  TfmUIDStore_Detail = class(TForm)
    gridUIDStore_Detail: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siExit: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmUIDStore_Detail: TfmUIDStore_Detail;

implementation

uses Data3, comdata, m207Proc, dbUtil;

{$R *.dfm}

procedure TfmUIDStore_Detail.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  OpenDataSet(dm3.taUIDStore_Detail);
end;

procedure TfmUIDStore_Detail.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUIDStore_Detail.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmUIDStore_Detail.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDStore_Detail.FormClose(Sender: TObject; var Action: TCloseAction);
var
  f : integer;
begin
  PostDataSet(dm3.taUIDStore_Detail);
  dm3.taUIDStore_Detail.DisableControls;
  try
    dm3.taUIDStore_Detail.First;
    f := 0;
    while not dm3.taUIDStore_Detail.Eof do
    begin
      f := f + dm3.taUIDStore_DetailF.AsInteger;
      dm3.taUIDStore_Detail.Next;
    end;
    dm3.taUIDStore.Edit;
    dm3.taUIDStoreF.AsInteger := f;
    dm3.taUIDStore.Post;
  finally
    dm3.taUIDStore_Detail.EnableControls;
  end;
  CloseDataSet(dm3.taUIDStore_Detail);
  Action := caFree;
end;

end.
