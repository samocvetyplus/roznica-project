unit RetSInf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Mask, rxCurrEdit,
  rxToolEdit;

type
  TfmRetSInf = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    deSDate: TDateEdit;
    edSSF: TEdit;
    edSN: TEdit;
    lcSup: TDBLookupComboBox;
    cePrice: TCurrencyEdit;
    lcNDS: TDBLookupComboBox;
    deNDate: TDateEdit;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRetSInf: TfmRetSInf;

implementation

uses Data, comdata, M207Proc;

{$R *.DFM}

procedure TfmRetSInf.FormCreate(Sender: TObject);
begin
  OpenDataSets([dm.quSup, dmCom.taNDS]);
  SetCBDropDown(lcNDS);
  SetCBDropDown(lcSup);  
end;

procedure TfmRetSInf.FormDestroy(Sender: TObject);
begin
  CloseDataSets([dm.quSup, dmCom.taNDS]);
end;

end.
