unit UIDStoreDep_T_C;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, RXSplit, RxStrUtils, Buttons,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, Menus, DB,
  DBGridEh, FR_DSet, FR_DBSet, ActnList, FIBQuery,
  pFIBQuery, FIBDataSet, pFIBDataSet, jpeg, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmUIDStoreDep_T_C = class(TForm)
    plSum: TPanel;
    stbrStatus: TStatusBar;
    grbxDep: TGroupBox;
    grbxMain: TGroupBox;
    RxSplitter1: TRxSplitter;
    RxSplitter2: TRxSplitter;
    fmstrTSum: TFormStorage;
    lbInCap: TLabel;
    lbIn: TLabel;
    lbInvCap: TLabel;
    lbInv: TLabel;
    RxSplitter3: TRxSplitter;
    grbxCurr: TGroupBox;
    lbCurr: TLabel;
    lbCurrSum: TLabel;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    taIn: TpFIBDataSet;
    gridIn: TDBGridEh;
    dsrIn: TDataSource;
    taInW: TFloatField;
    taInCOST: TFloatField;
    taInGROUPNAME: TFIBStringField;
    taSInvDetail: TpFIBDataSet;
    taSInvDetailW: TFloatField;
    taSInvDetailCOST: TFloatField;
    taSInv: TpFIBDataSet;
    taSInvDetailNAME: TFIBStringField;
    gridSInv: TDBGridEh;
    dsrSInvDetail: TDataSource;
    dsrSInv: TDataSource;
    taSInvW: TFloatField;
    taSInvCOST: TFloatField;
    taSInvGROUPNAME: TFIBStringField;
    gridOut: TDBGridEh;
    taOut: TpFIBDataSet;
    dsrOut: TDataSource;
    taOutW: TFloatField;
    taOutCOST: TFloatField;
    taOutGROUPNAME: TFIBStringField;
    plSellIn: TPanel;
    taSell: TpFIBDataSet;
    taSell2: TpFIBDataSet;
    dsrSell: TDataSource;
    taSellW: TFloatField;
    taSellCOST: TFloatField;
    taSellGROUPNAME: TFIBStringField;
    gridSell: TDBGridEh;
    dsrSell2: TDataSource;
    taSell2W: TFloatField;
    taSell2COST: TFloatField;
    taSell2GROUPNAME: TFIBStringField;
    qutmp: TpFIBQuery;
    taOutDetail: TpFIBDataSet;
    gridMinus: TDBGridEh;
    dsrOutDetail: TDataSource;
    taOutDetailNAME: TFIBStringField;
    taOutDetailW: TFloatField;
    taOutDetailCOST: TFloatField;
    frIn: TfrDBDataSet;
    acEvent: TActionList;
    acPrint: TAction;
    frSell: TfrDBDataSet;
    frSell2: TfrDBDataSet;
    frOut: TfrDBDataSet;
    gridPlus: TDBGridEh;
    frSInv: TfrDBDataSet;
    frSInvDetail: TfrDBDataSet;
    frOutDetail: TfrDBDataSet;
    Label10: TLabel;
    lbInCom: TLabel;
    taInCOSTCOM: TFIBFloatField;
    Label11: TLabel;
    lbInvCom: TLabel;
    taSInvCOSTCOM: TFIBFloatField;
    taOutCOSTCOM: TFIBFloatField;
    Label1: TLabel;
    lbCurrSumCom: TLabel;
    taSInvDetailCOSTCOM: TFIBFloatField;
    taOutDetailCOSTCOM: TFIBFloatField;
    taSellCOSTCOM: TFIBFloatField;
    taSell2COSTCOM: TFIBFloatField;
    gridSell2: TDBGridEh;
    plActSell: TPanel;
    taSellACTCOST: TFIBFloatField;
    taSellACTCOSTCOM: TFIBFloatField;
    lbActSellCap: TLabel;
    lbActSell: TLabel;
    lbActSellComCap: TLabel;
    lbActSellCom: TLabel;
    lbREt: TLabel;
    taInQ: TFIBSmallIntField;
    taSInvQ: TFIBBCDField;
    taSellQ: TFIBSmallIntField;
    taOutQ: TFIBSmallIntField;
    taSInvDetailQ: TFIBBCDField;
    taOutDetailQ: TFIBBCDField;
    taSell2Q: TFIBSmallIntField;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure siExitClick(Sender: TObject);
    procedure BeforeOpen(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    LogOperIdForm : string;
  end;

var
  fmUIDStoreDep_T_C: TfmUIDStoreDep_T_C;

implementation

uses Data, comdata, Data2, M207Proc, ReportData, Variants, ServData, Period,
  Data3, JewConst, UidStoreDepList;

{$R *.DFM}

const
  cCurrFormat = '### ### ##0.00';

procedure TfmUIDStoreDep_T_C.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUIDStoreDep_T_C.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    #27 : Close;
  end;
end;

procedure TfmUIDStoreDep_T_C.FormCreate(Sender: TObject);
var
  f, fCom : double;
begin
  tb1.Wallpaper := wp;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  // �������� �����
  OpenDataSets([taIn]);
  f:=0;
  fcom:=0;
  while not taIn.Eof do
  begin
    f := f+taInCOST.AsFloat;
    fcom := fcom+taInCOSTCOM.AsFloat;
    taIn.Next;
  end;
  lbIn.Caption := FormatFloat(cCurrFormat, f);
  lbInCom.Caption := FormatFloat(cCurrFormat, fcom);  
  // ������ �� ������
  OpenDataSets([taSInv]);
  f:=0;
  fcom:=0;  
  while not taSInv.Eof do
  begin
    f:=f+taSInvCOST.AsFloat;
    fcom := fcom+taSInvCOSTCOM.AsFloat;
    taSInv.Next;
  end;
  lbInv.Caption := FormatFloat(cCurrFormat, f);
  lbInvCom.Caption := FormatFloat(cCurrFormat, fcom);  
  // ����������� �� ����� ������� �� ������
  OpenDataSets([taSInvDetail]);
  // ��������� �������

  OpenDataSets([taSell, taSell2]);
  f:=0;
  fcom:=0;
  while not taSell.Eof do
  begin
    f:=f+taSellACTCOST.AsFloat;
    fcom := fcom+taSellACTCOSTCOM.AsFloat;
    taSell.Next;
  end;
  lbActSell.Caption := FormatFloat(cCurrFormat, f);
  lbActSellCom.Caption := FormatFloat(cCurrFormat, fcom);

  // ��������� �����
  OpenDataSets([taOut]);
  f:=0;
  fcom:=0;
  while not taOut.Eof do
  begin
    f:=f+taOutCOST.AsFloat;
    fcom:=fcom+taOutCOSTCom.AsFloat;
    taOut.Next;
  end;
  lbCurrSum.Caption := FormatFloat(cCurrFormat, f);
  lbCurrSumCom.Caption := FormatFloat(cCurrFormat, fcom);

  qutmp.Close;
  // ����������� �������
  OpenDataSets([taOutDetail]);


  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStoreDep_T_C.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taIn, taSInv, taSInvDetail, taSell, taOut, taSell2, taOutDetail]);
  dmCom.tr.CommitRetaining;
  Action := caFree;
end;

procedure TfmUIDStoreDep_T_C.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDStoreDep_T_C.BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['INVID'].AsInteger := fmUidStoreDepList.taUIDStoreDepListSINVID.AsInteger;
  end
end;

procedure TfmUIDStoreDep_T_C.acPrintExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreDepPrint, LogOperIdForm);
  dmReport.PrintDocumentB(uidstoredep_t_p_c);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreDep_T_C.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100442)
end;

procedure TfmUIDStoreDep_T_C.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
