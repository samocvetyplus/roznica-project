object fmExpFileClient: TfmExpFileClient
  Left = 389
  Top = 303
  Width = 384
  Height = 92
  Caption = #1048#1084#1087#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1092#1072#1080#1083
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 29
    Height = 13
    Caption = #1060#1072#1080#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object btImp: TButton
    Left = 112
    Top = 32
    Width = 121
    Height = 25
    Caption = #1048#1084#1087#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnClick = btImpClick
    OnKeyDown = btImpKeyDown
  end
  object fledImp: TFilenameEdit
    Left = 60
    Top = 5
    Width = 309
    Height = 21
    Filter = 
      #1053#1072#1082#1083#1072#1076#1085#1099#1077' '#1080#1084#1087#1086#1088#1090#1072' (*.*)|*.*|'#1069#1089#1090#1077#1090' |*.dbf|'#1040#1076#1072#1084#1072#1089'|*.*|'#1062#1044#1052'(*.xls)|*' +
      '.xls'
    DialogTitle = #1054#1090#1082#1088#1099#1090#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1086#1089#1090#1072#1074#1082#1080
    NumGlyphs = 1
    TabOrder = 1
    OnKeyDown = fledImpKeyDown
  end
end
