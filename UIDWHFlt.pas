unit UIDWHFlt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, StdCtrls,
  Buttons, ExtCtrls;

type
  TfmFlt = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    M207IBGrid1: TM207IBGrid;
    cbSup: TCheckBox;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmFlt: TfmFlt;

function ShowFilter(var SupFlt: boolean; var SupId: integer): boolean;


implementation

uses Data2, Data, M207Proc;

{$R *.dfm}


function ShowFilter(var SupFlt: boolean; var SupId: integer): boolean;
begin
  fmFlt:=TfmFlt.Create(nil);
  with dm, dm2, fmFlt do
    begin
      OpenDataSets([quSup]);
      if SupId<>-1 then quSup.Locate('D_COMPID', SupId, []);
      cbSup.Checked:=SupFlt;
      Result:=ShowModal=mrOK;
      if Result then
        begin
          SupFlt:=cbSup.Checked;
          SupId:=quSupD_CompId.AsInteger;
        end;
      CloseDataSets([quSup]);
    end;
end;


end.
