unit frmCertificateHistory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter;

type

  TDialogCertificateHistory = class(TForm)
    Data: TpFIBDataSet;
    GridView: TcxGridDBTableView;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    DataROUTE: TFIBStringField;
    DataROUTEDATE: TFIBDateTimeField;
    DataSource: TDataSource;
    GridViewROUTE: TcxGridDBColumn;
    GridViewROUTEDATE: TcxGridDBColumn;
    Styles: TcxStyleRepository;
    StyleEven: TcxStyle;
    StyleOdd: TcxStyle;
    DataOFFICE: TFIBStringField;
    GridViewOFFICE: TcxGridDBColumn;
    DataCOLOR: TFIBIntegerField;
    GridViewCOLOR: TcxGridDBColumn;
    procedure DataBeforeOpen(DataSet: TDataSet);
    procedure GridViewOFFICECustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    ID: Integer;
    procedure InternalExecute;
  public
    class procedure Execute(ID: Integer);
  end;

implementation

{$R *.dfm}


uses

  ComData;

{ TDialogCertificateHistory }


class procedure TDialogCertificateHistory.Execute(ID: Integer);
var
  DialogCertificateHistory: TDialogCertificateHistory;
begin
  DialogCertificateHistory := TDialogCertificateHistory.Create(Application);

  DialogCertificateHistory.ID := ID;

  DialogCertificateHistory.InternalExecute;

  DialogCertificateHistory.Free;
end;

procedure TDialogCertificateHistory.GridViewOFFICECustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  Color: Variant;
begin
  Color := AViewInfo.GridRecord.Values[0];

  if not VarIsNull(Color) then
  begin
    ACanvas.Brush.Color := Color;
  end;
end;
procedure TDialogCertificateHistory.InternalExecute;
begin
  Data.Active := True;

  Data.Last;
  
  ShowModal;
end;

procedure TDialogCertificateHistory.DataBeforeOpen(DataSet: TDataSet);
begin
  Data.ParamByName('certificate$id').AsInteger := ID;
end;


end.
