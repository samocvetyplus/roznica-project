unit RestFind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Buttons, db, Variants, rxToolEdit;

type
  TfmRestFind = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edArt: TEdit;
    deADate: TDateEdit;
    edPrice: TEdit;
    edN: TEdit;
    lcSup: TDBLookupComboBox;
    bbFind: TBitBtn;
    bbFindNext: TBitBtn;
    BitBtn3: TBitBtn;
    edArt2: TEdit;
    edPrice2: TEdit;
    Label8: TLabel;
    Label9: TLabel;
    cb1: TCheckBox;
    Label4: TLabel;
    edTW: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcSupKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbFindClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRestFind: TfmRestFind;

implementation

uses comdata, Data, Data2, MsgDialog;

{$R *.DFM}

procedure TfmRestFind.FormCreate(Sender: TObject);
begin
  with dm, dmCom do
    begin
      quDep.Active:=True;
      quSup.Active:=True;
    end;
  edN.Text:='';
  edArt.Text:='';
  edArt2.Text:='';
  edPrice.Text:='';
  edPrice2.Text:='';
  edTW.Text:='';
  lcSup.KeyValue:=-1;
end;

procedure TfmRestFind.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
    begin
      quDep.Active:=False;
      quSup.Active:=False;
    end;
end;

procedure TfmRestFind.lcSupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=8 then
    TDBLookupComboBox(Sender).KeyValue:=-1;
end;

procedure TfmRestFind.bbFindClick(Sender: TObject);
var s: string;
    i: integer;
    FldValues: Variant;
    V: Variant;
    b: boolean;
    opt: TLocateOptions;
begin
  b := False;
  if cb1.Checked then opt:=[loCaseInsensitive, loPartialKey]
  else opt:=[loCaseInsensitive];


  s:='';
  i:=-1;
  FldValues:=VarArrayCreate([0, 6], varVariant);
  if edArt.Text<>'' then
    begin
      Inc(i);
      s:='ART;';
      FldValues[i]:=edArt.Text;
      v:=edArt.Text;
    end;

  if edArt2.Text<>'' then
    begin
      Inc(i);
      s:=s+'ART2;';
      FldValues[i]:=edArt2.Text;
      v:=edArt2.Text;
    end;

  if deADate.Date>1 then
    begin
      Inc(i);
      s:=s+'ADATE;';
      FldValues[i]:=deADate.Date;
      v:=deADate.Date;
    end;

  if edN.Text<>'' then
    begin
      Inc(i);
      s:=s+'N;';
      FldValues[i]:=edN.Text;
      v:=edN.Text;
    end;


  if lcSup.KeyValue<>-1 then
    begin
      Inc(i);
      s:=s+'D_COMPID;';
      FldValues[i]:=lcSup.KeyValue;
      v:=lcSup.KeyValue;
    end;

  if edPrice.Text<>'' then
    begin
      Inc(i);
      s:=s+'PRICE;';
      FldValues[i]:={edPrice.Text;//}StrToFloat(edPrice.Text);
      v:={edPrice.Text;//}StrToFloat(edPrice.Text);
    end;

  if edPrice2.Text<>'' then
    begin
      Inc(i);
      s:=s+'PRICE2;';
      FldValues[i]:={edPrice2.Text;//}StrToFloat(edPrice2.Text);
      v:={edPrice2.Text;//}StrToFloat(edPrice2.Text);
    end;

  if edTW.Text<>'' then
    begin
      Inc(i);
      s:=s+'TW;';
      FldValues[i]:={ed.Text;//}StrToFloat(edTW.Text);
      v:={edPrice2.Text;//}StrToFloat(edTW.Text);
    end;

  if i>-1  then
    with dm2.taRest do
      try
        Screen.Cursor:=crSQLWait;
        DisableControls;
        SetLength(s, Length(s)-1);
          case TComponent(Sender).Tag of
              1:
                if i=0 then b:=Locate(s, V, opt)
                else  b:=Locate(s, FldValues, opt);
              2:
                 begin
                   Next;
                   if i=0 then   b:=LocateNext(s, V, opt)
                   else   b:=LocateNext(s, FldValues, opt);
                   if not b then Prior;
                 end
            end;
      finally
        EnableControls;
        Screen.Cursor:=crDefault;
        if NOT b then MessageDialog('������ �� �������', mtInformation, [mbOK], 0);
      end;
end;

procedure TfmRestFind.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_F12, VK_ESCAPE] then Close;
end;

end.
