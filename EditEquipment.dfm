object fmEditEquipment: TfmEditEquipment
  Left = 266
  Top = 176
  Width = 636
  Height = 202
  Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plInfo: TPanel
    Left = 0
    Top = 0
    Width = 511
    Height = 175
    Align = alClient
    TabOrder = 0
    object lbsup: TLabel
      Left = 16
      Top = 72
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    end
    object Lbsdate: TLabel
      Left = 16
      Top = 120
      Width = 148
      Height = 13
      Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
    end
    object lbndate: TLabel
      Left = 16
      Top = 40
      Width = 83
      Height = 13
      Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object lbssf: TLabel
      Left = 16
      Top = 96
      Width = 156
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
    end
    object lbsn: TLabel
      Left = 16
      Top = 16
      Width = 91
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object lbcost: TLabel
      Left = 16
      Top = 152
      Width = 34
      Height = 13
      Caption = #1057#1091#1084#1084#1072
    end
    object dbsn: TDBEditEh
      Left = 200
      Top = 8
      Width = 121
      Height = 21
      DataField = 'SN'
      DataSource = dm3.dsEquipment
      EditButtons = <>
      TabOrder = 0
      Visible = True
    end
    object dbndate: TDBDateTimeEditEh
      Left = 200
      Top = 32
      Width = 121
      Height = 21
      DataField = 'NDATE'
      DataSource = dm3.dsEquipment
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 1
      Visible = True
    end
    object dbSup: TDBLookupComboboxEh
      Left = 200
      Top = 64
      Width = 281
      Height = 21
      DataField = 'SUPID'
      DataSource = dm3.dsEquipment
      DropDownBox.Rows = 15
      EditButtons = <>
      KeyField = 'D_COMPID'
      ListField = 'NAME'
      ListSource = dm.dsSup
      TabOrder = 2
      Visible = True
    end
    object dbsdate: TDBDateTimeEditEh
      Left = 200
      Top = 112
      Width = 121
      Height = 21
      DataField = 'SDATE'
      DataSource = dm3.dsEquipment
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
    end
    object dbssf: TDBEditEh
      Left = 200
      Top = 88
      Width = 121
      Height = 21
      DataField = 'SSF'
      DataSource = dm3.dsEquipment
      EditButtons = <>
      TabOrder = 4
      Visible = True
    end
    object dbcost: TDBEditEh
      Left = 200
      Top = 144
      Width = 121
      Height = 21
      DataField = 'COST'
      DataSource = dm3.dsEquipment
      EditButtons = <>
      TabOrder = 5
      Visible = True
    end
  end
  object plButton: TPanel
    Left = 511
    Top = 0
    Width = 117
    Height = 175
    Align = alRight
    TabOrder = 1
    object btOk: TBitBtn
      Left = 16
      Top = 16
      Width = 89
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      Kind = bkOK
    end
    object btCancel: TBitBtn
      Left = 16
      Top = 56
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      Kind = bkCancel
    end
  end
end
