object fmUChHist: TfmUChHist
  Left = 221
  Top = 315
  Width = 818
  Height = 410
  HelpContext = 100550
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082' '#1080#1079#1076#1077#1083#1080#1103' '#1080#1079' '#1089#1082#1083#1072#1076#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object spltWH: TSplitter
    Left = 0
    Top = 215
    Width = 810
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 218
    Width = 810
    Height = 163
    Align = alBottom
    AllowedOperations = []
    Color = clScrollBar
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsUIDDetalChg
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = DBGridEh1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'TABLENAME'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'NEWID'
        Footers = <>
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'OLDID'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'OLDPRICE'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'NEWPRICE'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 810
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 72
    BtnHeight = 24
    Images = dmCom.ilButtons
    BevelOuter = bvNone
    TabOrder = 1
    InternalVer = 1
    object laPeriod: TLabel
      Left = 80
      Top = 8
      Width = 46
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbFind: TLabel
      Left = 516
      Top = 8
      Width = 61
      Height = 13
      Caption = #1055#1086#1080#1089#1082' ('#1072#1088#1090'.)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 324
      Top = 8
      Width = 76
      Height = 13
      Caption = #1055#1086#1080#1089#1082' ('#8470' '#1080#1079#1076'.)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edArt: TComboEdit
      Left = 580
      Top = 4
      Width = 101
      Height = 21
      Color = 16776176
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      NumGlyphs = 1
      TabOrder = 0
      Text = 'edArt'
      OnButtonClick = edArtButtonClick
      OnKeyDown = edArtKeyDown
    end
    object edUID: TComboEdit
      Left = 404
      Top = 4
      Width = 101
      Height = 21
      Color = 16776176
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      NumGlyphs = 1
      TabOrder = 1
      Text = 'edUID'
      OnButtonClick = edUIDButtonClick
      OnKeyDown = edUIDKeyDown
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siOpen: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = 'siOpen'
      Hint = 'siOpen|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siOpenClick
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 739
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Layout = blGlyphLeft
      Spacing = 1
      Left = 667
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object DBGridEh2: TDBGridEh
    Left = 0
    Top = 29
    Width = 810
    Height = 186
    Align = alClient
    AllowedOperations = []
    Color = cl3DLight
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsUIDChg
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = DBGridEh2GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'CH_DATE'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'DISCRIP'
        Footers = <>
        Width = 121
      end
      item
        EditButtons = <>
        FieldName = 'OLDVAL'
        Footers = <>
        Width = 137
      end
      item
        EditButtons = <>
        FieldName = 'NEWVAL'
        Footers = <>
        Width = 155
      end
      item
        EditButtons = <>
        FieldName = 'ISNEW'
        Footers = <>
        Width = 157
      end
      item
        EditButtons = <>
        FieldName = 'NEWVALID'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'OLDVALID'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'NEWPRICE'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'OLDPRICE'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 368
    Top = 112
  end
end
