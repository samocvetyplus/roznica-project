unit PrHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, pFIBDataSet, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  ComCtrls, FIBDataSet, rxPlacemnt;

type
  TfmPrHist = class(TForm)
    quPrHist: TpFIBDataSet;
    StatusBar1: TStatusBar;
    dsPrHist: TDataSource;
    FormStorage1: TFormStorage;
    M207IBGrid1: TM207IBGrid;
    quPrHistTPRICE: TFloatField;
    quPrHistPRICEHSTID: TIntegerField;
    quPrHistPRICEID: TIntegerField;
    quPrHistART2ID: TIntegerField;
    quPrHistDEPID: TIntegerField;
    quPrHistPRICE2: TFloatField;
    quPrHistLASTCHANGE: TDateTimeField;
    quPrHistPEQ: TIntegerField;
    quPrHistUSEMARGIN: TSmallintField;
    quPrHistNDSID: TIntegerField;
    quPrHistINVID: TIntegerField;
    quPrHistPR: TSmallintField;
    quPrHistPRORDITEMID: TIntegerField;
    quPrHistRECDATE: TDateTimeField;
    quPrHistSN: TIntegerField;
    quPrHistITYPE: TIntegerField;
    quPrHistNDS: TFIBStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure quPrHistBeforeOpen(DataSet: TDataSet);
    function M207IBGrid1GetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;  
  end;

var
  fmPrHist: TfmPrHist;

implementation

uses comdata, Data, Data2;

{$R *.DFM}

procedure TfmPrHist.FormCreate(Sender: TObject);
begin
  quPrHist.Active:=True;
end;

procedure TfmPrHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quPrHist.Active:=False;
end;

procedure TfmPrHist.quPrHistBeforeOpen(DataSet: TDataSet);
begin
  with quPrHist, Params do
    begin
      ByName['ART2Id'].AsInteger:=dm.PrHistArt2Id;
      ByName['DEPID'].AsInteger:=dm.PrHistDepId;
    end;
end;

function TfmPrHist.M207IBGrid1GetCellCheckBox(Sender: TObject;
  Field: TField; var StateCheckBox: Integer): Boolean;
begin
  Result:=Field.FieldName='USEMARGIN';
end;

end.
