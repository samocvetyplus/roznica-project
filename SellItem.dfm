object fmSellItem: TfmSellItem
  Left = 215
  Top = 169
  Caption = #1057#1084#1077#1085#1072
  ClientHeight = 557
  ClientWidth = 1060
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyUp = FormKeyUp
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1060
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Tag = 1
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00000000FF0000FF00000000FFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        000000000000FF0000FF000000000000000000007B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFF00000000FF0000FF0000FF0000FF0000FF0000FF
        0000FF007B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        FF0000FF0000FF0000FF0000FF0000FF0000FF007B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00000000000000000000FF0000FF000000000000
        000000007B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF00000000FF0000FF00000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000000000FF
        FFFFFFFF7B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF000000000000000000000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF7B7B7BFF00FFFF00FFFF00FF
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFF7B
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siView: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FF00FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00000000000000FFFFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B
        FFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFF7B7B7B000000FF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B000000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 4
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object siSell: TSpeedItem
      Action = acSellEnd
      BtnCaption = #1055#1088#1086#1076#1072#1078#1072
      Caption = #1055#1088#1086#1076#1072#1078#1072
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000084
        8484000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000848484000000848484848484848484000000000000FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF000000848484000000FF00FFFF00FF84
        8484848484848484848484848484848484000000FF00FFFF00FF848484FF00FF
        000000FF00FFFF00FFFF00FFFFFFFFFFFFFF8484848484848484848484848484
        84848484848484FF00FF848484FF00FFFF00FFFF00FFFFFFFFFF00FFFFFFFFFF
        00FF848484848484848484848484848484848484848484FF00FF848484FF00FF
        FFFFFFFF00FFFFFFFF000000FF00FFFF00FFFF00FF8484848484848484848484
        84848484848484FF00FF848484FF00FFFFFFFF000000FF00FFFF00FF000000FF
        00FFFF00FFFF00FF848484848484848484848484848484FF00FF848484FF00FF
        FF00FFFF00FF000000FF00FFFF00FF000000FF00FFFFFFFFFF00FF8484848484
        84848484848484FF00FFFF00FF848484FF00FFFF00FFFF00FF000000FF00FFFF
        FFFF848484848484848484FFFFFF848484848484848484FF00FFFF00FFFF00FF
        848484FF00FFFF00FFFFFFFF848484848484FF00FF848484000000FFFFFF8484
        84848484848484FF00FFFF00FFFF00FFFF00FF848484000000848484FF00FF84
        8484000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF848484FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF848484000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF848484FF00FFFF00FFFF00FFFF00FFFF00FF848484FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF848484FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF848484FFFFFFFFFFFFFFFFFFFFFFFF848484FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF84
        8484848484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 41
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = acSellEndExecute
      SectionName = 'Untitled (0)'
    end
    object siOpenSell: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100'/'#1047#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FF00FFFFFF00FF00FFFF00FFFFFF00FF7B7B7B7B7B7B7B7B7B00
        FFFF00FFFF7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFF00FFFF00FFFF00FF
        00FFFF0000000000000000000000000000000000000000000000000000000000
        0000FFFFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        007B7B7BFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF0000007B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        007B7B7BFF00FFFF00FF00FFFF00FFFF00FFFF000000FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        0000FFFF00FFFF00FFFFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFF
        FFFF000000000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFFFF00000000FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000FF00FF00FFFF00FF
        FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00000000000000000000000000
        0000000000FF00FFFF00FFFF00FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF
        00FFFFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFFFF00FFFF00FFFF00FFFF00
        FF00FFFF00FFFFFF00FF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00
        FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF}
      ImageIndex = 60
      Spacing = 1
      Left = 259
      Top = 3
      OnClick = acOpenSellExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseSell: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 5
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = siCloseSellClick
      SectionName = 'Untitled (0)'
    end
    object siRet: TSpeedItem
      Tag = 2
      Action = acRet
      BtnCaption = #1042#1086#1079#1074#1088#1072#1090
      Caption = #1042#1086#1079#1074#1088#1072#1090
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00000000
        0000000000000000000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000FFFFFF000000000000FFFFFF0000000000
        00000000FFFFFF000000FF00FFFF00FFFF00FF000000FF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FF00FFFF00FF
        FF00FF0000FF000000FF00FF000000FFFFFF000000BDBDBD000000000000FFFF
        FF000000FFFFFF000000FF00FFFF00FFFF00FF0000FF0000FF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000FF
        0000FF0000FF0000FF0000FF000000FFFFFF000000000000FFFFFF0000000000
        000000000000000000000000000000FF0000FF0000FF0000FF0000FF0000FF00
        0000FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF000000FF00FF0000000000FF
        0000FF0000FF0000FF0000FF0000FF0000FF000000BDBDBDFFFFFF000000FFFF
        FF000000FF00FFFF00FF0000000000FF0000FF0000FF0000FF0000FF0000FF00
        0000FFFFFFFFFFFFFFFFFF000000000000FF00FFFF00FFFF00FF0000000000FF
        0000FF0000FF0000FF0000FF000000000000000000000000000000000000FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000FF0000FF000000FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 64
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siRetToWH: TSpeedItem
      Action = acRetToWh
      BtnCaption = #1042#1077#1088#1085#1091#1090#1100
      Caption = #1042#1077#1088#1085#1091#1090#1100
      Spacing = 1
      Left = 387
      Top = 3
      Visible = True
      OnClick = acRetToWhExecute
      SectionName = 'Untitled (0)'
    end
    object siOpenAll: TSpeedItem
      Action = acOpenAll
      BtnCaption = #1042#1089#1103' '#1089#1084#1077#1085#1072
      Caption = #1042#1089#1103' '#1089#1084#1077#1085#1072
      DropDownMenu = pmcheck
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF00000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFF000000000000FFFFFF000000000000FF
        FFFF000000000000FFFFFF000000000000000000FFFFFF000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFF000000000000FFFFFF000000000000FF
        FFFF000000000000FFFFFF000000000000000000FFFFFF000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFF000000000000FFFFFF000000000000FF
        FFFF000000000000FFFFFF000000000000000000FFFFFF000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FFFFFF000000000000FFFFFF000000000000FF
        FFFF000000000000FFFFFF000000000000000000FFFFFF000000000000FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000000000FF0000FF0000FF0000FF0000FF0000FF0000FF
        0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000000000000000BDBDBD
        BDBDBDFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF00
        00BDBDBDBDBDBD00000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 22
      Spacing = 1
      Left = 515
      Top = 3
      Visible = True
      OnClick = acOpenAllExecute
      SectionName = 'Untitled (0)'
    end
    object siXRep: TSpeedItem
      Action = acXRep
      BtnCaption = 'X-'#1086#1090#1095#1077#1090
      Caption = 'X-'#1086#1090#1095#1077#1090
      Spacing = 1
      Left = 579
      Top = 3
      Visible = True
      OnClick = acXRepExecute
      SectionName = 'Untitled (0)'
    end
    object siCheck: TSpeedItem
      Action = acCheck
      BtnCaption = #1058#1086#1074'. '#1095#1077#1082
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1095#1077#1082
      Spacing = 1
      Left = 643
      Top = 3
      Visible = True
      OnClick = acCheckExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 451
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 787
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 851
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 42
    Width = 1060
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object laDep: TLabel
      Left = 86
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 474
      Top = 8
      Width = 55
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object laSell: TLabel
      Left = 280
      Top = 8
      Width = 51
      Height = 13
      Caption = #8470' '#1089#1084#1077#1085#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 659
      Top = 8
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label16: TLabel
      Left = 200
      Top = 8
      Width = 14
      Height = 13
      Caption = #8470':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edUID: TEdit
      Left = 532
      Top = 4
      Width = 81
      Height = 21
      Color = clInfoBk
      TabOrder = 0
      OnKeyDown = ceUIDKeyDown
      OnKeyPress = edUIDKeyPress
      OnKeyUp = edUIDKeyUp
    end
    object BitBtn1: TBitBtn
      Left = 616
      Top = 4
      Width = 25
      Height = 21
      TabOrder = 1
      OnClick = ceUIDButtonClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
    end
    object edArt: TEdit
      Left = 704
      Top = 4
      Width = 77
      Height = 21
      Color = clInfoBk
      TabOrder = 2
      OnChange = edArtChange
      OnKeyDown = edArtKeyDown
    end
    object edRN: TDBEdit
      Left = 220
      Top = 4
      Width = 49
      Height = 21
      Color = clInfoBk
      DataField = 'RN'
      DataSource = dm.dsSellList
      TabOrder = 3
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmSellItem
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object pa1: TPanel
    Left = 0
    Top = 488
    Width = 1060
    Height = 69
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 201
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 0
      object Label3: TLabel
        Left = 4
        Top = 4
        Width = 34
        Height = 13
        Caption = #1057#1082#1083#1072#1076':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtDep: TDBText
        Left = 40
        Top = 4
        Width = 35
        Height = 13
        AutoSize = True
        DataField = 'DEP'
        DataSource = dm.dsSellList
      end
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 14
        Height = 13
        Caption = #8470':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtN: TDBText
        Left = 168
        Top = 4
        Width = 23
        Height = 13
        AutoSize = True
        DataField = 'RN'
        DataSource = dm.dsSellList
      end
      object Label4: TLabel
        Left = 4
        Top = 20
        Width = 77
        Height = 13
        Caption = #1053#1072#1095#1072#1083#1086' '#1089#1084#1077#1085#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtBD: TDBText
        Left = 100
        Top = 20
        Width = 30
        Height = 13
        AutoSize = True
        DataField = 'BD'
        DataSource = dm.dsSellList
      end
      object Label5: TLabel
        Left = 4
        Top = 36
        Width = 95
        Height = 13
        Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1089#1084#1077#1085#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtED: TDBText
        Left = 100
        Top = 36
        Width = 30
        Height = 13
        AutoSize = True
        DataField = 'ED'
        DataSource = dm.dsSellList
      end
    end
    object Panel3: TPanel
      Left = 287
      Top = 0
      Width = 136
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 1
      object Label8: TLabel
        Left = 6
        Top = 6
        Width = 44
        Height = 13
        Caption = #1057#1091#1084#1084#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 4
        Top = 20
        Width = 37
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 4
        Top = 36
        Width = 22
        Height = 13
        Caption = #1042#1077#1089':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtCNT: TDBText
        Left = 48
        Top = 20
        Width = 37
        Height = 13
        AutoSize = True
        DataField = 'CNT'
        DataSource = dm.dsSellList
      end
      object dbtCost: TDBText
        Left = 54
        Top = 6
        Width = 44
        Height = 13
        AutoSize = True
        DataField = 'COST'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtW: TDBText
        Left = 48
        Top = 36
        Width = 26
        Height = 13
        AutoSize = True
        DataField = 'W'
        DataSource = dm.dsSellList
      end
    end
    object Panel5: TPanel
      Left = 201
      Top = 0
      Width = 86
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel6: TPanel
      Left = 479
      Top = 0
      Width = 132
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 3
      object Label9: TLabel
        Left = 4
        Top = 4
        Width = 44
        Height = 13
        Caption = #1057#1091#1084#1084#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 4
        Top = 20
        Width = 37
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 4
        Top = 36
        Width = 22
        Height = 13
        Caption = #1042#1077#1089':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object dbtRCnt: TDBText
        Left = 47
        Top = 17
        Width = 39
        Height = 13
        AutoSize = True
        DataField = 'RCNT'
        DataSource = dm.dsSellList
      end
      object dbtRCost: TDBText
        Left = 47
        Top = 6
        Width = 53
        Height = 13
        AutoSize = True
        DataField = 'RCOST'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbtRW: TDBText
        Left = 48
        Top = 36
        Width = 34
        Height = 13
        AutoSize = True
        DataField = 'RW'
        DataSource = dm.dsSellList
      end
    end
    object Panel7: TPanel
      Left = 423
      Top = 0
      Width = 56
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1042#1086#1079#1074#1088#1072#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object Panel8: TPanel
      Left = 723
      Top = 0
      Width = 102
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 5
      object dbtCass: TDBText
        Left = 25
        Top = 20
        Width = 46
        Height = 13
        AutoSize = True
        DataField = 'TOTAL'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 27
        Top = 1
        Width = 45
        Height = 13
        Caption = 'Z-'#1086#1090#1095#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object plCostCard: TPanel
      Left = 611
      Top = 0
      Width = 112
      Height = 69
      Align = alLeft
      BevelOuter = bvLowered
      TabOrder = 6
      object LCostCard: TLabel
        Left = 10
        Top = 4
        Width = 94
        Height = 26
        Alignment = taCenter
        Caption = #1057#1091#1084#1084#1072', '#1087#1088#1086#1074#1077#1076'.'#13#10#1087#1086' '#1082#1072#1088#1090#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object dbCostCard: TDBText
        Left = 21
        Top = 36
        Width = 66
        Height = 13
        AutoSize = True
        DataField = 'COSTCARD'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object Panel4: TPanel
      Left = 825
      Top = 0
      Width = 203
      Height = 69
      Align = alLeft
      AutoSize = True
      BevelInner = bvLowered
      BevelOuter = bvNone
      TabOrder = 7
      object Label19: TLabel
        Left = 55
        Top = 1
        Width = 81
        Height = 13
        BiDiMode = bdLeftToRight
        Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentBiDiMode = False
        ParentFont = False
      end
      object Label21: TLabel
        Left = 2
        Top = 36
        Width = 108
        Height = 13
        Caption = #1054#1090#1086#1074#1072#1088#1077#1085#1086'('#1085#1086#1084#1080#1085#1072#1083'):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label22: TLabel
        Left = 2
        Top = 20
        Width = 47
        Height = 13
        Caption = #1055#1088#1086#1076#1072#1085#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object csert: TDBText
        AlignWithMargins = True
        Left = 55
        Top = 20
        Width = 29
        Height = 13
        AutoSize = True
        DataField = 'SERTSELL'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object crsert: TDBText
        AlignWithMargins = True
        Left = 116
        Top = 36
        Width = 33
        Height = 13
        AutoSize = True
        DataField = 'CSERT'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label23: TLabel
        Left = 116
        Top = 20
        Width = 49
        Height = 13
        Caption = #1044#1086#1087#1083#1072#1090#1099':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object sadd: TDBText
        AlignWithMargins = True
        Left = 171
        Top = 20
        Width = 28
        Height = 13
        AutoSize = True
        DataField = 'SERT_ADD'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 1
        Top = 55
        Width = 67
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1097#1077#1085#1086':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object rcsert: TDBText
        AlignWithMargins = True
        Left = 74
        Top = 55
        Width = 33
        Height = 13
        AutoSize = True
        DataField = 'RCSERT'
        DataSource = dm.dsSellList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object tb3: TSpeedBar
    Left = 0
    Top = 71
    Width = 1060
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 140
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 3
    InternalVer = 1
    object Label14: TLabel
      Left = 8
      Top = 8
      Width = 77
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086' '#1089#1084#1077#1085#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label15: TLabel
      Left = 222
      Top = 8
      Width = 95
      Height = 13
      Caption = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1089#1084#1077#1085#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object DBEdit1: TDBEdit
      Left = 88
      Top = 4
      Width = 125
      Height = 21
      Color = clInfoBk
      DataField = 'BD'
      DataSource = dm.dsSellList
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 320
      Top = 4
      Width = 125
      Height = 21
      Color = clInfoBk
      DataField = 'ED'
      DataSource = dm.dsSellList
      TabOrder = 1
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siSetSDate: TSpeedItem
      BtnCaption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1076#1072#1090#1099
      Caption = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1076#1072#1090#1099
      DropDownMenu = pmEditDate
      ImageIndex = 69
      Layout = blGlyphLeft
      Spacing = 1
      Left = 563
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siApplDep: TSpeedItem
      BtnCaption = #1047#1072#1103#1074#1082#1080
      Caption = 'siApplDep'
      DropDownMenu = pmApplDep
      Hint = 'siApplDep|'
      Spacing = 1
      Left = 753
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object dg2: TDBGridEh
    Left = 0
    Top = 100
    Width = 1060
    Height = 292
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsSellItem
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = ppSellItem
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = acViewExecute
    OnGetCellParams = dg2GetCellParams
    OnKeyDown = dg2KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'APPL_Q'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ADATE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        Title.TitleButton = True
        Width = 143
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'CLIENTNAME'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Title.TitleButton = True
        Width = 139
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'CLIENT$CARD'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1078#1072'|'#8470' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1099
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Title.Caption = #1054#1090#1076#1077#1083
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'|'#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'PRICE0'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'|'#1073#1077#1079' '#1089#1082#1080#1076#1082#1080
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1090#1086#1074#1072#1088#1072'|'#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COST0'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1090#1086#1074#1072#1088#1072'|'#1073#1077#1079' '#1089#1082#1080#1076#1082#1080
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'DISCOUNT'
        Footers = <>
        Title.Caption = #1057#1082#1080#1076#1082#1072
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'FULLART'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Title.TitleButton = True
        Width = 160
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'CODE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1048#1079#1075#1086#1090'.'
        Title.TitleButton = True
        Width = 43
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'MATID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1052#1072#1090'.'
        Title.TitleButton = True
        Width = 31
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'GOODID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1053#1072#1080#1084'. '#1080#1079#1076'.'
        Title.TitleButton = True
        Width = 61
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'INSID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1054#1042
        Width = 40
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1040#1088#1090#1080#1082#1091#1083
        Title.TitleButton = True
        Width = 100
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1040#1088#1090#1080#1082#1091#1083' 2'
        Title.TitleButton = True
        Width = 87
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'|'#1045#1076'. '#1080#1079#1084'.'
        Title.TitleButton = True
        Width = 55
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        Title.TitleButton = True
        Width = 83
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SPRICE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1062#1077#1085#1072
        Title.TitleButton = True
        Width = 53
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SUP'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1055#1086#1089#1090#1072#1074#1097#1080#1082
        Title.TitleButton = True
        Width = 103
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#8470' '#1087#1086#1089#1090'.'
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RETCAUSE'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1055#1088#1080#1095#1080#1085#1072
        Title.TitleButton = True
        Width = 99
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RETPRICE'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1062#1077#1085#1072
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RETCLIENT'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Title.TitleButton = True
        Width = 123
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RETDATE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RETADDRESS'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1040#1076#1088#1077#1089' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
        Title.TitleButton = True
        Width = 182
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1077#1089
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'CHECKNO'
        Footers = <>
        Title.Caption = #8470' '#1095#1077#1082#1072
        Title.TitleButton = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'Op'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        Title.TitleButton = True
        Width = 143
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'EMP'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1074#1077#1094
        Title.TitleButton = True
        Width = 196
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'CASHIERNAME'
        Footers = <>
        Title.Caption = #1050#1072#1089#1089#1080#1088
        Width = 179
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'RW'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1077#1089' '#1086#1089#1090'.'
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNT'
        Footers = <>
        Title.Caption = #1040#1082#1090' '#1089#1082#1080#1076#1082#1080
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DIFQ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1079#1076#1077#1083#1080#1103' '#1074' '#1086#1090#1082'. '#1085#1072#1082#1083'.'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 392
    Width = 1060
    Height = 67
    Align = alBottom
    Color = clMenuBar
    DataGrouping.GroupLevels = <>
    DataSource = dsrSert
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    RowDetailPanel.Color = clBtnFace
    RowSizingAllowed = True
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'ID'
        Footers = <>
        Title.Caption = #1048#1076'. '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        Title.Caption = #1060#1080#1083#1080#1072#1083
      end
      item
        EditButtons = <>
        FieldName = 'NOMINAL'
        Footers = <>
        Title.Caption = #1053#1086#1084#1080#1085#1072#1083
        Width = 76
      end
      item
        DisplayFormat = 'dd.mm.yyyy hh:mm'
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072
        Width = 149
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 459
    Width = 1060
    Height = 29
    Align = alBottom
    BevelInner = bvLowered
    TabOrder = 6
    object Label18: TLabel
      Left = 7
      Top = 6
      Width = 152
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072#1084':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 299
      Top = 6
      Width = 98
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1076#1086#1087#1083#1072#1090#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LSert: TLabel
      Left = 165
      Top = 6
      Width = 5
      Height = 16
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LSum: TLabel
      Left = 403
      Top = 6
      Width = 62
      Height = 16
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object ppPrint: TPopupMenu
    OnPopup = ppPrintPopup
    Left = 844
    Top = 326
    object mnitTicket: TMenuItem
      Tag = 4
      Action = acPrintCheck
    end
    object mnitTag: TMenuItem
      Tag = 1
      Caption = #1041#1080#1088#1082#1080
    end
    object mnitAppl: TMenuItem
      Tag = 2
      Action = acPrintAppl
    end
    object mnitDisv: TMenuItem
      Tag = 3
      Action = acPrintDisv
    end
    object mnitSep2: TMenuItem
      Caption = '-'
    end
    object mnitCmena: TMenuItem
      Tag = 5
      Action = acPrintSellInPrice
    end
    object mnitCmenaOutPrice: TMenuItem
      Tag = 6
      Action = acPrintSellOutPrice
    end
    object NSellArt: TMenuItem
      Tag = 7
      Action = acPrintSellArt
    end
    object N3: TMenuItem
      Tag = 8
      Action = acPrintSellN
    end
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 844
    Top = 272
    object acCreateApplDepSZ: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1080' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099' '#1087#1086' '#1088#1072#1079#1084#1077#1088#1091
      OnExecute = acCreateApplDepSZExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acAdd: TAction
      Tag = 1
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 119
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acSellEnd: TAction
      Caption = #1055#1088#1086#1076#1072#1078#1072
      ImageIndex = 41
      OnExecute = acSellEndExecute
      OnUpdate = acSellEndUpdate
    end
    object acOpenSell: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100'/'#1047#1072#1082#1088#1099#1090#1100' '#1089#1084#1077#1085#1091
      ImageIndex = 60
      OnExecute = acOpenSellExecute
    end
    object acCloseSell: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 5
      OnExecute = acCloseSellExecute
    end
    object acSellNew: TAction
      Caption = 'acSellNew'
      ShortCut = 16463
      OnExecute = acSellNewExecute
    end
    object acRet: TAction
      Tag = 2
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ImageIndex = 64
      ShortCut = 36
      OnExecute = acAddExecute
      OnUpdate = acRetUpdate
    end
    object acRetToWh: TAction
      Caption = #1042#1077#1088#1085#1091#1090#1100
      ShortCut = 16420
      OnExecute = acRetToWhExecute
      OnUpdate = acRetToWhUpdate
    end
    object acOpenAll: TAction
      Caption = #1042#1089#1103' '#1089#1084#1077#1085#1072
      ImageIndex = 22
      ShortCut = 121
      OnExecute = acOpenAllExecute
    end
    object acNewCheck: TAction
      Caption = #1053#1086#1074#1099#1081' '#1095#1077#1082
      ShortCut = 120
      OnExecute = acNewCheckExecute
    end
    object acCurCheck: TAction
      Caption = #1058#1077#1082#1091#1097#1080#1081' '#1095#1077#1082
      ShortCut = 122
      OnExecute = acCurCheckExecute
    end
    object acXRep: TAction
      Caption = 'X-'#1086#1090#1095#1077#1090
      OnExecute = acXRepExecute
    end
    object acCheck: TAction
      Caption = #1058#1086#1074'. '#1095#1077#1082
      OnExecute = acCheckExecute
    end
    object acPrintCheck: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1058#1086#1074#1072#1088#1085#1099#1081' '#1095#1077#1082
      ShortCut = 116
      OnExecute = acPrintCheckExecute
      OnUpdate = acPrintCheckUpdate
    end
    object acPrintAppl: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1047#1072#1103#1074#1083#1077#1085#1080#1077
      ShortCut = 117
      OnExecute = acPrintApplExecute
      OnUpdate = acPrintApplUpdate
    end
    object acPrintDisv: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1056#1072#1089#1093#1086#1076#1085#1099#1081' '#1086#1088#1076#1077#1088
      ShortCut = 118
      OnExecute = acPrintDisvExecute
      OnUpdate = acPrintDisvUpdate
    end
    object acClient: TAction
      Caption = 'acClient'
      ShortCut = 16451
      OnExecute = acClientExecute
    end
    object acLastCheck: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = 'acLastCheck'
      ShortCut = 123
      OnExecute = acLastCheckExecute
    end
    object acFRDateTime: TAction
      Caption = #1042#1088#1077#1084#1103' '#1060#1056
      ShortCut = 24658
      OnExecute = acFRDateTimeExecute
    end
    object acScanConnect: TAction
      Caption = 'acScanConnect'
      ShortCut = 24657
    end
    object acActiveEdUid: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveEdUid'
      ShortCut = 8241
      OnExecute = acActiveEdUidExecute
      OnUpdate = acActiveEdUidUpdate
    end
    object acDataWithSell: TAction
      Tag = 1
      Category = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1076#1072#1090#1099
      Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103' '#1089' '#1085#1086#1084#1077#1088#1086#1084' '#1095#1077#1082#1072
      ShortCut = 113
      OnExecute = acDataWithSellExecute
      OnUpdate = acDataWithSellUpdate
    end
    object acDataWithoutSell: TAction
      Category = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1076#1072#1090#1099
      Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103' '#1073#1077#1079' '#1085#1086#1084#1077#1088#1072' '#1095#1077#1082#1072
      ShortCut = 114
      OnExecute = acDataWithSellExecute
      OnUpdate = acDataWithSellUpdate
    end
    object acDataWithRetSell: TAction
      Tag = 2
      Category = #1048#1079#1084#1077#1085#1077#1085#1080#1077' '#1076#1072#1090#1099
      Caption = #1042#1086#1079#1074#1088#1072#1097#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
      ShortCut = 115
      OnExecute = acDataWithSellExecute
      OnUpdate = acDataWithSellUpdate
    end
    object acPrintGrid: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = 'acPrintGrid'
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
    object acPrintTag: TAction
      Tag = 1
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1041#1080#1088#1082#1080
      ShortCut = 32818
      OnExecute = acPrintTagExecute
    end
    object acPrintSellInPrice: TAction
      Tag = 5
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1084#1077#1085#1072' ('#1087#1088#1080#1093'.'#1094#1077#1085#1099')'
      ShortCut = 16433
      OnExecute = acPrintTagExecute
    end
    object acPrintSellOutPrice: TAction
      Tag = 6
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1084#1077#1085#1072' ('#1088#1072#1089#1093'.'#1094#1077#1085#1099')'
      ShortCut = 16434
      OnExecute = acPrintTagExecute
    end
    object acPrintSellArt: TAction
      Tag = 7
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1084#1077#1085#1072' ('#1089#1086#1088#1090'. '#1087#1086' '#1072#1088#1090'.)'
      ShortCut = 16435
      OnExecute = acPrintTagExecute
    end
    object acPrintSellN: TAction
      Tag = 8
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1084#1077#1085#1072' ('#1085#1086#1084#1077#1088' '#1087#1088#1080#1093'.)'
      ShortCut = 16436
      OnExecute = acPrintTagExecute
    end
    object acActiveedArt: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveedArt'
      ShortCut = 8242
      OnExecute = acActiveedArtExecute
      OnUpdate = acActiveedArtUpdate
    end
    object acActiveedRN: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveedRN'
      ShortCut = 8243
      OnExecute = acActiveedRNExecute
      OnUpdate = acActiveedRNUpdate
    end
    object acActiveBd: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveBd'
      ShortCut = 8244
      OnExecute = acActiveBdExecute
      OnUpdate = acActiveBdUpdate
    end
    object acActiveEd: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveEd'
      ShortCut = 8245
      OnExecute = acActiveEdExecute
      OnUpdate = acActiveEdUpdate
    end
    object acActiveGrid: TAction
      Category = #1055#1077#1088#1077#1093#1086#1076' '#1087#1086' '#1101#1083'-'#1090#1072#1084' '#1092#1086#1088#1084#1099
      Caption = 'acActiveGrid'
      ShortCut = 8246
      OnExecute = acActiveGridExecute
    end
    object acCreateAplldep: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1080' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099
      OnExecute = acCreateAplldepExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acClearApplDep: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' '#1079#1072#1103#1074#1086#1082' '#1085#1072' '#1092#1080#1083#1080#1083#1072#1083#1099
      Hint = 
        #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' '#1079#1072#1103#1074#1086#1082' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099' '#1080' '#1091#1076#1072#1083#1080#1090#1100' '#1086#1090#1082#1088#1099#1090#1099#1077' '#1079#1072#1103#1074#1082#1080' '#1076#1083#1103' '#1090#1077 +
        #1082#1091#1097#1077#1081' '#1089#1084#1077#1085#1099
      OnExecute = acClearApplDepExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acCreateAppl: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      OnExecute = acCreateApplExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acClearAppl: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' '#1079#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      OnExecute = acClearApplExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acRefreshAppl: TAction
      Category = #1047#1072#1103#1074#1082#1080
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1079#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      OnExecute = acRefreshApplExecute
      OnUpdate = acCreateAplldepUpdate
    end
    object acCard: TAction
      Caption = #1055#1088#1086#1076#1072#1078#1072' '#1087#1086' '#1090#1077#1088#1084#1080#1085#1072#1083#1091
      ShortCut = 16497
      OnExecute = acCardExecute
      OnUpdate = acCardUpdate
    end
    object acNotCard: TAction
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100' '#1087#1088#1086#1076#1072#1078#1091' '#1087#1086' '#1090#1077#1088#1084#1080#1085#1072#1083#1091
      ShortCut = 16498
      OnExecute = acNotCardExecute
      OnUpdate = acNotCardUpdate
    end
  end
  object pmcheck: TPopupMenu
    Left = 24
    Top = 230
    object NewCheck: TMenuItem
      Action = acNewCheck
    end
    object OldCheck: TMenuItem
      Action = acCurCheck
    end
  end
  object pmEditDate: TPopupMenu
    Left = 844
    Top = 224
    object NDataWithSell: TMenuItem
      Tag = 1
      Action = acDataWithSell
    end
    object NDataWithoutSell: TMenuItem
      Action = acDataWithoutSell
    end
    object NDataWithRetSell: TMenuItem
      Tag = 2
      Action = acDataWithRetSell
    end
  end
  object ppSellItem: TTBPopupMenu
    Left = 844
    Top = 380
    object TBItem3: TTBItem
      Action = acAdd
    end
    object TBItem2: TTBItem
      Action = acView
    end
    object TBItem1: TTBItem
      Action = acDel
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object mnitRet: TTBItem
      Action = acRet
    end
    object mnitRetToWh: TTBItem
      Action = acRetToWh
    end
    object TBItem4: TTBItem
      Action = acSellEnd
      Caption = #1055#1088#1086#1076#1072#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
    end
    object TBSeparatorItem3: TTBSeparatorItem
    end
    object TBItem5: TTBItem
      Action = acCard
    end
    object biCard: TTBItem
      Action = acNotCard
    end
  end
  object fs: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 840
    Top = 176
  end
  object pdg2: TPrintDBGridEh
    DBGridEh = dg2
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 88
    Top = 304
  end
  object pmApplDep: TTBPopupMenu
    Left = 848
    Top = 440
    object NApplDepSZ: TTBItem
      Action = acCreateApplDepSZ
    end
    object ibCreateApplDep: TTBItem
      Action = acCreateAplldep
    end
    object ibClearApplDep: TTBItem
      Action = acClearApplDep
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' '#1079#1072#1103#1074#1086#1082' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object biCreateAppl: TTBItem
      Action = acCreateAppl
    end
    object biRefreshAppl: TTBItem
      Action = acRefreshAppl
    end
    object biClearAppl: TTBItem
      Action = acClearAppl
    end
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   8'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 8'
      'order by HDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 720
    Top = 8
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 752
    Top = 8
  end
  object taSert: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    InsertSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select s.id, d.sname, s.nominal, si.sdate, d.color, si.ret'
      'from sertificate s, d_dep d, sert_info si'
      'where si.transact_id=:transact_id and'
      '      si.sert_id=s.id and'
      '      si.d_depid=d.d_depid'
      'order by s.id'
      ''
      '')
    BeforeOpen = taSertBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 504
    Top = 408
    object taSertID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taSertSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taSertNOMINAL: TFIBIntegerField
      FieldName = 'NOMINAL'
    end
    object taSertSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taSertCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taSertRET: TFIBIntegerField
      FieldName = 'RET'
    end
  end
  object dsrSert: TDataSource
    DataSet = taSert
    Left = 472
    Top = 408
  end
end
