object fmOptList: TfmOptList
  Left = 174
  Top = 270
  Width = 799
  Height = 470
  HelpContext = 100251
  Caption = #1054#1087#1090#1086#1074#1099#1077' '#1087#1088#1086#1076#1072#1078#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 791
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 570
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000
        000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7CE07FEF3D00001F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00000000E07F1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DFF7F007C007C007C007C007CFF7FEF3D00001F7C1F7C
        1F7C1F7C1F7C0000EF3DF75EFF7FF75E007CF75EFF7FF75EEF3D00001F7C1F7C
        1F7C1F7C1F7CEF3DEF3DFF7FF75EFF7F007CFF7FF75EFF7FEF3DEF3D1F7C1F7C
        1F7C1F7C1F7C1F7C0000EF3DFF7FF75EFF7FF75EFF7FEF3D00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C0000EF3DEF3DEF3DEF3DEF3D00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7CEF3D000000000000EF3D1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siExportXml: TSpeedItem
      Action = acExportXml
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090' '#13#10#1074' XML'
      Caption = 'siExportXml'
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = acExportXmlExecute
      SectionName = 'Untitled (0)'
    end
    object siExportText: TSpeedItem
      BtnCaption = #1069#1082#1089#1087#1086#1088#1090' '#13#10#1074' '#1090#1077#1082#1089#1090
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090
      DropDownMenu = pmtext
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090'|'
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 506
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      WordWrap = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 791
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 24
      Top = 8
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 306
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridOptInv: TDBGridEh
    Left = 0
    Top = 69
    Width = 791
    Height = 348
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsOptList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    PopupMenu = ppDoc
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = gridOptInvDblClick
    OnGetCellParams = gridOptInvGetCellParams
    OnKeyDown = gridOptInvKeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'DEPFROM'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 196
      end
      item
        EditButtons = <>
        FieldName = 'COMP'
        Footers = <>
        Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 119
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072'|'#1087#1088#1086#1076#1072#1078#1080
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.FieldName = 'SCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072'|'#1087#1088#1080#1093#1086#1076#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 67
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 417
    Width = 791
    Height = 19
    Panels = <>
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 28
    Top = 180
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 28
    Top = 132
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acPrintFacture: TAction
      Tag = 2
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072
      OnExecute = acPrint1Execute
      OnUpdate = acPrintUpdate
    end
    object acPrintInvoice: TAction
      Tag = 1
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1058#1086#1074#1072#1088#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103
      OnExecute = acPrint1Execute
      OnUpdate = acPrintUpdate
    end
    object acPrintFactureItem: TAction
      Tag = 4
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072'('#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086')'
      OnExecute = acPrint1Execute
      OnUpdate = acPrintUpdate
    end
    object acPrintInvoiceItem: TAction
      Tag = 3
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1058#1086#1074#1072#1088#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103' ('#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086')'
      OnExecute = acPrint1Execute
      OnUpdate = acPrintUpdate
    end
    object acChangePeriod: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1077#1088#1080#1086#1076
      ImageIndex = 3
      ShortCut = 32848
    end
    object acPrint: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acPrintPril: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1088#1077#1076#1072#1090#1086#1095#1085#1099#1081' '#1072#1082#1090
      OnExecute = acPrintPrilExecute
    end
    object acExportXml: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' XML'
      OnExecute = acExportXmlExecute
      OnUpdate = acExportXmlUpdate
    end
    object acExportTextIncome: TAction
      Caption = #1074' '#1087#1088#1080#1093#1086#1076#1085#1099#1093' '#1094#1077#1085#1072#1093
      OnExecute = acExportTextIncomeExecute
    end
    object acExportTextOutLine: TAction
      Caption = #1074' '#1088#1072#1089#1093#1086#1076#1085#1099#1093' '#1094#1077#1085#1072#1093
      OnExecute = acExportTextOutLineExecute
    end
  end
  object ppPrint: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 232
    Top = 28
    object TBItem8: TTBItem
      Action = acPrintFacture
    end
    object TBItem7: TTBItem
      Action = acPrintInvoice
    end
    object TBItem6: TTBItem
      Action = acPrintFactureItem
    end
    object TBItem5: TTBItem
      Action = acPrintInvoiceItem
    end
    object TBItem10: TTBItem
      Action = acPrintPril
    end
    object TBItem4: TTBItem
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' ('#1074' '#1087#1088#1080#1093'.'#1094#1077#1085#1072#1093')'
      OnClick = TBItem4Click
    end
  end
  object ppDoc: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 76
    Top = 132
    object TBItem3: TTBItem
      Action = acAdd
    end
    object TBItem2: TTBItem
      Action = acDel
    end
    object TBItem1: TTBItem
      Action = acView
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object TBItem9: TTBItem
      Action = acChangePeriod
    end
  end
  object pggridOptInv: TPrintDBGridEh
    DBGridEh = gridOptInv
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 216
    Top = 192
  end
  object svdFile: TSaveDialog
    Left = 328
    Top = 152
  end
  object pmtext: TTBPopupMenu
    Left = 256
    Top = 320
    object biExporttextIncome: TTBItem
      Action = acExportTextIncome
    end
    object biExportTextOutLine: TTBItem
      Action = acExportTextOutLine
    end
  end
end
