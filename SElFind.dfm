object fmSElFind: TfmSElFind
  Left = 209
  Top = 195
  Width = 318
  Height = 226
  Caption = #1055#1086#1080#1089#1082' '#1087#1086#1079#1080#1094#1080#1080' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 28
    Width = 50
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 50
    Width = 82
    Height = 13
    Caption = #1055#1088#1080#1093#1086#1076#1085#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 72
    Width = 59
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 94
    Width = 56
    Height = 13
    Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 116
    Width = 34
    Height = 13
    Caption = #1057#1091#1084#1084#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 138
    Width = 81
    Height = 13
    Caption = #1056#1072#1089#1093#1086#1076#1085#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edFullArt: TEdit
    Left = 98
    Top = 2
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 0
    Text = 'edFullArt'
  end
  object edArt2: TEdit
    Left = 98
    Top = 24
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 1
    Text = 'edFullArt'
  end
  object edPrice: TEdit
    Left = 98
    Top = 46
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 2
    Text = 'edFullArt'
  end
  object edQuantity: TEdit
    Left = 98
    Top = 68
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 3
    Text = 'edFullArt'
  end
  object edTotalWeight: TEdit
    Left = 98
    Top = 90
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 4
    Text = 'edFullArt'
  end
  object edCost: TEdit
    Left = 98
    Top = 112
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 5
    Text = 'edFullArt'
  end
  object edPrice2: TEdit
    Left = 98
    Top = 134
    Width = 107
    Height = 21
    Color = clInfoBk
    TabOrder = 6
    Text = 'edFullArt'
  end
  object BitBtn1: TBitBtn
    Tag = 1
    Left = 222
    Top = 6
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080
    TabOrder = 7
    OnClick = BitBtn1Click
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF6000F000050FFFFFFFFFFFF6660FF000050FFF000000FF6666FFF
      000050FF000000000666FFFF000050F00EEEEEE0066FFFFF00005500EEEEEEEE
      00FFFFFF0000500E99E99EEEE00F999F0000500E99999EE9E00F99F90000500E
      99E99E99999F999F0000500E99E99EE9E00F99F90000500EE999EEEEE00F999F
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object BitBtn2: TBitBtn
    Tag = 2
    Left = 222
    Top = 34
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 8
    OnClick = BitBtn1Click
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF2200F000050FFFFFFFFFFFF2222FF000050FFF000000FF2222FFF
      000050FF000000000222FFFF000050F00EEEEEE0022FFFFF00005500EEEEEEEE
      00FF00FF0000500E0E0E000EE00FFFFF0000500E000E000EE00FFFFF0000500E
      0E0E00EEE00FFFFF0000500EE0EE000EE00FFFFF0000500EEEEEEEEEE00FFFFF
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object BitBtn3: TBitBtn
    Left = 222
    Top = 62
    Width = 75
    Height = 25
    TabOrder = 9
    Kind = bkOK
  end
  object cb1: TCheckBox
    Left = 8
    Top = 168
    Width = 143
    Height = 17
    Caption = #1063#1072#1089#1090#1080#1095#1085#1086#1077' '#1089#1088#1072#1074#1085#1077#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
end
