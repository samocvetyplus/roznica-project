object fmTermCards: TfmTermCards
  Left = 286
  Top = 160
  Caption = #1057#1087#1080#1089#1086#1082' '#1080#1079#1076#1077#1083#1080#1081', '#1087#1088#1086#1076#1072#1085#1085#1099#1093' '#1087#1086' '#1082#1072#1088#1090#1072#1084
  ClientHeight = 446
  ClientWidth = 713
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGridCard: TcxGrid
    Left = 0
    Top = 0
    Width = 713
    Height = 446
    Align = alClient
    PopupMenu = pmgrid
    TabOrder = 0
    object cxGridCardDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCustomDrawCell = cxGridCardDBTableView1CustomDrawCell
      DataController.DataSource = dm.dsrCardList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          FieldName = 'COSTCARD'
          Column = cxGridCardDBTableView1COSTCARD
        end
        item
          Kind = skSum
          FieldName = 'COST'
          Column = cxGridCardDBTableView1COST
        end
        item
          Kind = skSum
          FieldName = 'COST0'
          Column = cxGridCardDBTableView1COST0
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGridCardDBTableView1CHECKNO: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1095#1077#1082#1072
        DataBinding.FieldName = 'CHECKNO'
        PropertiesClassName = 'TcxPopupEditProperties'
        Properties.PopupControl = fmTermCardUid.plTermCard
        Properties.OnCloseUp = cxGridCardDBTableView1CHECKNOPropertiesCloseUp
        Properties.OnPopup = cxGridCardDBTableView1CHECKNOPropertiesPopup
        Width = 88
      end
      object cxGridCardDBTableView1RN: TcxGridDBColumn
        Caption = #8470' '#1089#1084#1077#1085#1099
        DataBinding.FieldName = 'RN'
        Width = 75
      end
      object cxGridCardDBTableView1SNAME: TcxGridDBColumn
        Caption = #1060#1080#1083#1080#1072#1083
        DataBinding.FieldName = 'SNAME'
        Width = 107
      end
      object cxGridCardDBTableView1COSTCARD: TcxGridDBColumn
        Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1082#1072#1088#1090#1077
        DataBinding.FieldName = 'COSTCARD'
        Width = 108
      end
      object cxGridCardDBTableView1COST: TcxGridDBColumn
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' ('#1088#1077#1072#1083'.)'
        DataBinding.FieldName = 'COST'
        Width = 121
      end
      object cxGridCardDBTableView1COST0: TcxGridDBColumn
        Caption = #1057#1090#1086#1080#1084#1083#1089#1090#1100' ('#1088#1072#1089#1093'.)'
        DataBinding.FieldName = 'COST0'
        Width = 116
      end
    end
    object cxGridCardLevel1: TcxGridLevel
      GridView = cxGridCardDBTableView1
    end
  end
  object acList: TActionList
    Left = 128
    Top = 48
    object acPrintGrid: TAction
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1087#1086' '#1095#1077#1082#1072#1084
      ShortCut = 16464
      OnExecute = acPrintGridExecute
      OnUpdate = acPrintGridUpdate
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 136
    Top = 144
  end
  object pmgrid: TTBPopupMenu
    Left = 128
    Top = 96
    object biPrintGrid: TTBItem
      Action = acPrintGrid
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1080#1079#1076#1077#1083#1080#1081
    end
  end
end
