unit A2Rest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmA2Rest = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    M207IBGrid1: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmA2Rest: TfmA2Rest;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmA2Rest.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmA2Rest.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dm do
    begin
      Caption:='������� - '+quArtFullArt.AsString+' '+taA2Art2.AsString;
      OpenDataSets([quA2Rest]);
    end;
end;

procedure TfmA2Rest.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmA2Rest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    begin
      CloseDataSets([quA2Rest]);
    end;
end;

procedure TfmA2Rest.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmA2Rest.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='DEP') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmA2Rest.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_ESCAPE, VK_F12] then Close;
end;

end.
