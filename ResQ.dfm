object fmRestQ: TfmRestQ
  Left = 418
  Top = 146
  BorderStyle = bsDialog
  Caption = #1054#1089#1090#1072#1090#1082#1080
  ClientHeight = 240
  ClientWidth = 334
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 244
    Top = 12
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 0
    Width = 229
    Height = 240
    Align = alLeft
    Color = clBtnFace
    DataSource = dm2.dsRestQ
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = fp
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = False
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'DEP'
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Q'
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object fp: TFormPlacement
    UseRegistry = False
    Left = 240
    Top = 88
  end
end
