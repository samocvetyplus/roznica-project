object fmBalanceInv: TfmBalanceInv
  Left = 291
  Top = 108
  Width = 553
  Height = 382
  Caption = 'fmBalanceInv'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object pnlBalancInv: TPanel
    Left = 0
    Top = 0
    Width = 545
    Height = 353
    Align = alClient
    Caption = 'pnlBalancInv'
    TabOrder = 0
    object gr: TDBGridEh
      Left = 1
      Top = 1
      Width = 543
      Height = 351
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      AllowedSelections = []
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dsrSInv
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          EditButtons = <>
          FieldName = 'SN'
          Footers = <>
        end
        item
          EditButtons = <>
          FieldName = 'SDATE'
          Footers = <>
        end
        item
          DisplayFormat = '0#.##'
          EditButtons = <>
          FieldName = 'TOTCOST'
          Footer.DisplayFormat = '0#.##'
          Footer.FieldName = 'TOTCOST'
          Footer.ValueType = fvtSum
          Footers = <>
        end
        item
          EditButtons = <>
          FieldName = 'DISCRIPTION'
          Footers = <>
          Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object taPayInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(p.C) Cost, p.PD SDate '
      'from Payment p'
      'where p.pd between :BD and :ED and'
      '      p.CompId =  :CompID  '
      'group by p.PD ')
    BeforeOpen = taPayInvBeforeOpen
    OnCalcFields = taPayInvCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 316
    Top = 284
    poSQLINT64ToBCD = True
    object FIBDateTimeField2: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'SDATE'
    end
    object FIBFloatField2: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
    end
    object taPayInvTOTCOST: TFloatField
      DisplayLabel = #1054#1073#1097'. '#1089#1091#1084#1084#1072
      FieldKind = fkCalculated
      FieldName = 'TOTCOST'
      Calculated = True
    end
  end
  object dsrPayInv: TDataSource
    DataSet = taPayInv
    Left = 264
    Top = 284
  end
  object taRInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(coalesce(r.Cost, 0)) Cost, i.SDATE, i.SN,'
      '       coalesce(i.Tr, 0) Tr'
      'from SInv i, SRet r'
      'where i.IType = 7 and'
      '      i.IsClosed = 1 and'
      '      i.SDate between :BD and :ED and'
      '      i.SInvId = r.SInvId and'
      '      i.CompID = :CompID  '
      'group by  i.SDATE, i.SN, coalesce(i.Tr, 0)')
    BeforeOpen = taPayInvBeforeOpen
    OnCalcFields = taRInvCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 200
    Top = 284
    poSQLINT64ToBCD = True
    object FIBIntegerField1: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'SN'
    end
    object FIBDateTimeField1: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'SDATE'
    end
    object FIBFloatField1: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
    end
    object taRInvTR: TFIBFloatField
      DisplayLabel = #1058#1088'. '#1088#1072#1089#1093#1086#1076#1099
      FieldName = 'TR'
    end
    object taRInvTOTCOST: TFloatField
      DisplayLabel = #1054#1073#1097'. '#1089#1091#1084#1084#1072
      FieldKind = fkCalculated
      FieldName = 'TOTCOST'
      Calculated = True
    end
  end
  object dsrRInv: TDataSource
    DataSet = taRInv
    Left = 152
    Top = 284
  end
  object taSInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(si.Q0*e.Price) Cost,  i.SDATE, i.SN, '
      '       coalesce(i.Tr, 0) Tr, '#39#1087#1086#1089#1090#1072#1074#1082#1072'    '#39' discription'
      'from SInv i, SEl e, SItem si'
      'where i.SInvId = e.SInvId and'
      '      si.SElId= e.SElId and'
      '      i.SDate between :BD and :ED and'
      '      i.IsClosed = 1 and'
      '      i.IType = 1 and'
      '      i.SupId = :CompID  '
      'group by  i.SDATE, i.SN, coalesce(i.Tr, 0)'
      'union'
      'select sum(i.C) Cost,  i.SDATE, i.SN, '
      '       coalesce(i.Tr, 0) Tr, '#39#1086#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077#39' description'
      'from SInv i'
      'where i.SDate between :BD and :ED and'
      '      i.IsClosed = 1 and'
      '      i.IType = 22 and'
      '      i.SupId = :CompID  '
      'group by  i.SDATE, i.SN, coalesce(i.Tr, 0)')
    BeforeOpen = taPayInvBeforeOpen
    OnCalcFields = taSInvCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 84
    Top = 284
    poSQLINT64ToBCD = True
    object taSInvSN: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'SN'
    end
    object taSInvSDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'SDATE'
    end
    object taSInvCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
    end
    object taSInvTR: TFIBFloatField
      DisplayLabel = #1058#1088'. '#1088#1072#1089#1093#1086#1076#1099
      FieldName = 'TR'
    end
    object taSInvTotCost: TFloatField
      DisplayLabel = #1054#1073#1097'. '#1089#1091#1084#1084#1072
      FieldKind = fkCalculated
      FieldName = 'TOTCOST'
      Calculated = True
    end
    object taSInvDISCRIPTION: TFIBStringField
      FieldName = 'DISCRIPTION'
      Size = 12
      EmptyStrToNull = True
    end
  end
  object dsrSInv: TDataSource
    DataSet = taSInv
    Left = 36
    Top = 284
  end
end
