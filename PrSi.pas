unit PrSi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, M207DBCtrls, DBGridEh, jpeg, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmPrSi = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fr1: TFormStorage;
    dg1: TDBGridEh;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmPrSi: TfmPrSi;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmPrSi.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmPrSi.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPrSi.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmPrSi.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dmCOm, dm do
    begin
      OpenDataSets([quPrSI]);
    end;
end;

procedure TfmPrSi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCOm, dm do
    begin
      CloseDataSets([quPrSI]);
    end;
end;

end.
