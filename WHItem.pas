unit WHItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, ExtCtrls,
  DBCtrls, StdCtrls, db, jpeg, rxSpeedbar;

type
  TfmWHItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHItem: TfmWHItem;

implementation

uses Data2, comdata, M207Proc;

{$R *.DFM}

procedure TfmWHItem.FormCreate(Sender: TObject);
begin
  tb1.WallPaper := wp;
  with dm2 do ibdsWHItem.Active:=True;
end;

procedure TfmWHItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm2 do
  begin
    PostDataSet(ibdsWHItem);
    ibdsWHItem.Active := False;
  end;
  Action := caFree;
end;

procedure TfmWHItem.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHItem.SpeedItem1Click(Sender: TObject);
begin
  dm2.ibdsWHItem.Delete;
end;

procedure TfmWHItem.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmWHItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHItem.FormActivate(Sender: TObject);
begin
  ActiveControl:=dg1;
  dg1.SelectedIndex:=1;
end;

procedure TfmWHItem.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field<>NIL then
    if Field.FieldName='UID' then Background:=dmCom.clMoneyGreen
end;

end.
