object fmPrHist: TfmPrHist
  Left = 145
  Top = 66
  Width = 634
  Height = 327
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1094#1077#1085#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 279
    Width = 626
    Height = 19
    Panels = <>
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 0
    Width = 626
    Height = 279
    Align = alClient
    Color = clBtnFace
    DataSource = dsPrHist
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = FormStorage1
    TitleButtons = True
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    OnGetCellCheckBox = M207IBGrid1GetCellCheckBox
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1058#1077#1082#1091#1097#1072#1103' '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1042#1088#1077#1084#1077#1085#1085#1072#1103' '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'USEMARGIN'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1094#1077#1085#1082#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RECDATE'
        Title.Alignment = taCenter
        Title.Caption = #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SN'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NDS'
        Title.Alignment = taCenter
        Title.Caption = #1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICEHSTID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICEID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DEPID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LASTCHANGE'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PEQ'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NDSID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INVID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PR'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRORDITEMID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ITYPE'
        Visible = True
      end>
  end
  object quPrHist: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT PRICEHSTID, PRICEID, ART2ID,  DEPID, PRICE2,'
      '               LASTCHANGE, TPRICE, PEQ, USEMARGIN,'
      '               NDSID, INVID, PR, PRORDITEMID, RECDATE,'
      '               SN, ITYPE, NDS'
      'FROM  PRICEHST_S(?ART2ID,  ?DEPID)'
      'ORDER BY RECDATE DESC')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = quPrHistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 40
    Top = 52
    object quPrHistPRICEHSTID: TIntegerField
      FieldName = 'PRICEHSTID'
    end
    object quPrHistPRICEID: TIntegerField
      FieldName = 'PRICEID'
    end
    object quPrHistTPRICE: TFloatField
      FieldName = 'TPRICE'
      currency = True
    end
    object quPrHistART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object quPrHistDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object quPrHistPRICE2: TFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quPrHistLASTCHANGE: TDateTimeField
      FieldName = 'LASTCHANGE'
    end
    object quPrHistPEQ: TIntegerField
      FieldName = 'PEQ'
    end
    object quPrHistUSEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
    end
    object quPrHistNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object quPrHistINVID: TIntegerField
      FieldName = 'INVID'
    end
    object quPrHistPR: TSmallintField
      FieldName = 'PR'
    end
    object quPrHistPRORDITEMID: TIntegerField
      FieldName = 'PRORDITEMID'
    end
    object quPrHistRECDATE: TDateTimeField
      FieldName = 'RECDATE'
    end
    object quPrHistSN: TIntegerField
      FieldName = 'SN'
    end
    object quPrHistITYPE: TIntegerField
      FieldName = 'ITYPE'
    end
    object quPrHistNDS: TFIBStringField
      FieldName = 'NDS'
      EmptyStrToNull = True
    end
  end
  object dsPrHist: TDataSource
    DataSet = quPrHist
    Left = 36
    Top = 116
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 236
    Top = 128
  end
end
