unit UIDPHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, RXSpin, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ComCtrls, Menus, db, jpeg, rxPlacemnt, rxSpeedbar,
  Mask;

type
  TfmUIDPHist = class(TForm)
    tb1: TSpeedBar;
    Label1: TLabel;
    seUID: TRxSpinEdit;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siOpen: TSpeedItem;
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    siPrOrd: TSpeedItem;
    pm1: TPopupMenu;
    N1: TMenuItem;
    M207IBGrid2: TM207IBGrid;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure siOpenClick(Sender: TObject);
    procedure seUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure siPrOrdClick(Sender: TObject);
    procedure M207IBGrid2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;

  public
    { Public declarations }
  end;

var
  fmUIDPHist: TfmUIDPHist;

implementation

uses comdata, Data, Data2, PrOrd, M207Proc;

{$R *.DFM}

procedure TfmUIDPHist.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmUIDPHist.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUIDPHist.siOpenClick(Sender: TObject);
begin
 try
   Screen.Cursor:=crSQLWait;
   with dm2, quUIDPHist, Params do
    begin
      Active:=False;
      Params[0].AsInteger:=Round(seUID.Value);
      Open;
    end;
  finally
    Screen.Cursor:=crDefault;
  end;  
end;

procedure TfmUIDPHist.seUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then  siOpenClick(NIL);
end;

procedure TfmUIDPHist.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUIDPHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  //--------- ������� ��������� ����� ��� ��������� --------------//
  M207IBGrid2.ColumnByName['PRICE'].Visible := CenterDep;
  M207IBGrid2.ColumnByName['OPTPRICE'].Visible := CenterDep;
  //--------------------------------------------------------------//
  with dmCom, dm , dm2 do
    begin
      dsPrOrd.DataSet:=quPrOrd;
      if UID2Find<>-1 then
        begin
          seUID.Value:=UID2Find;
          siOpenClick(NIL);
        end;
    end;
end;

procedure TfmUIDPHist.siPrOrdClick(Sender: TObject);
begin
  with dm, dm2 do
    begin
      PrOrdId:=quUIDPHistPrOrdId.AsInteger;
      if PrOrdId<>-1 then
      begin
      quPrOrd.Active:=True;
      ShowAndFreeForm(TfmPrOrd, Self, TForm(fmPrOrd), True, False);
      quPrOrd.Active:=False;
      end;
    end;
end;

procedure TfmUIDPHist.M207IBGrid2GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field.FieldName='DEPNAME' then Background:=dm2.quUIDPHistColor.AsInteger;
end;

procedure TfmUIDPHist.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key=VK_F12 then Close;
end;

procedure TfmUIDPHist.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100530)
end;

procedure TfmUIDPHist.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
