object fmSInv: TfmSInv
  Left = 168
  Top = 217
  HelpContext = 100202
  Caption = #1055#1086#1089#1090#1072#1074#1082#1072
  ClientHeight = 647
  ClientWidth = 1153
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 220
    Width = 1153
    Height = 8
    Cursor = crVSplit
    Align = alTop
    ExplicitTop = 153
  end
  object Splitter6: TSplitter
    Left = 0
    Top = 390
    Width = 1153
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 301
    ExplicitWidth = 888
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 628
    Width = 1153
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1153
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 818
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = miAddClick
      SectionName = 'Untitled (0)'
    end
    object siIdNum: TSpeedItem
      Action = acidNum
      BtnCaption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acidNumExecute
      SectionName = 'Untitled (0)'
    end
    object siIns: TSpeedItem
      Tag = 1
      BtnCaption = #1042#1089#1090#1072#1074#1082#1080
      Caption = #1042#1089#1090#1072#1074#1082#1080
      Hint = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = miInsClick
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 450
      Top = 2
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object siFind: TSpeedItem
      BtnCaption = #1055#1086#1080#1089#1082
      Caption = #1055#1086#1080#1089#1082
      Hint = #1055#1086#1080#1089#1082'|'
      ImageIndex = 48
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = siFindClick
      SectionName = 'Untitled (0)'
    end
    object siPrice: TSpeedItem
      BtnCaption = #1062#1077#1085#1099
      Caption = #1062#1077#1085#1099
      Hint = #1062#1077#1085#1099'|'
      ImageIndex = 32
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      OnClick = siPriceClick
      SectionName = 'Untitled (0)'
    end
    object siRecalc: TSpeedItem
      BtnCaption = #1055#1077#1088#1077#1089#1095#1105#1090
      Caption = #1055#1077#1088#1077#1089#1095#1105#1090
      Hint = #1055#1077#1088#1077#1089#1095#1105#1090' '#1053#1044#1057' '#1080' '#1090#1088#1072#1085#1089#1087#1086#1088#1090#1085#1099#1093' '#1088#1072#1089#1093#1086#1076#1086#1074
      ImageIndex = 8
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      OnClick = siRecalcClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1041#1080#1088#1082#1080
      Caption = #1041#1080#1088#1082#1080
      DropDownMenu = ppPrint
      Hint = #1041#1080#1088#1082#1080'|'
      ImageIndex = 67
      Spacing = 1
      Left = 514
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1056#1072#1079#1085#1086#1077
      Caption = #1056#1072#1079#1085#1086#1077
      DropDownMenu = pmOpt
      Hint = #1056#1072#1079#1085#1086#1077
      ImageIndex = 77
      Spacing = 1
      Left = 578
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 754
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Caption = 'SpeedItem3'
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = 'SpeedItem3|'
      Spacing = 1
      Left = 642
      Top = 2
      Visible = True
      WordWrap = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
  end
  object paHeader: TPanel
    Left = 0
    Top = 40
    Width = 1153
    Height = 180
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 4
      Top = 28
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 4
      Top = 68
      Width = 83
      Height = 13
      Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 4
      Top = 48
      Width = 34
      Height = 13
      Caption = #8470' '#1089'.'#1092'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 4
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 180
      Top = 8
      Width = 28
      Height = 13
      Caption = #1055#1086#1089#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 180
      Top = 28
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 576
      Top = 8
      Width = 53
      Height = 13
      Caption = #1042' '#1090'.'#1095'. '#1053#1044#1057
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label10: TLabel
      Left = 400
      Top = 48
      Width = 14
      Height = 13
      Caption = #1058#1056
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 400
      Top = 28
      Width = 44
      Height = 13
      Caption = #1048#1079#1076#1077#1083#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 404
      Top = 70
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 504
      Top = 70
      Width = 135
      Height = 17
      Alignment = taRightJustify
      DataField = 'TOTALCOST'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText3: TDBText
      Left = 248
      Top = 27
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm.dsSList
    end
    object Label7: TLabel
      Left = 180
      Top = 48
      Width = 24
      Height = 13
      Caption = #1053#1044#1057
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 180
      Top = 68
      Width = 62
      Height = 13
      Caption = #1042#1080#1076' '#1086#1087#1083#1072#1090#1099':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 645
      Top = 27
      Width = 59
      Height = 13
      Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 644
      Top = 8
      Width = 62
      Height = 13
      Caption = #1054#1073#1097#1077#1077' '#1082'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 713
      Top = 27
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TW'
      DataSource = dm.dsSList
    end
    object DBText5: TDBText
      Left = 712
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'TQ'
      DataSource = dm.dsSList
    end
    object Label14: TLabel
      Left = 524
      Top = 8
      Width = 34
      Height = 13
      Caption = #1057#1091#1084#1084#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 645
      Top = 46
      Width = 61
      Height = 13
      Caption = #1053#1072#1094#1077#1085#1082#1072', %:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbSup: TDBText
      Left = 213
      Top = 8
      Width = 31
      Height = 13
      AutoSize = True
      DataField = 'SUP'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object DBText7: TDBText
      Left = 512
      Top = 84
      Width = 127
      Height = 17
      Alignment = taRightJustify
      DataField = 'Cost2'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 404
      Top = 86
      Width = 79
      Height = 13
      Caption = #1074' '#1088#1072#1089#1093#1086#1076#1085#1099#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lOpenInv: TLabel
      Left = 4
      Top = 115
      Width = 62
      Height = 16
      Caption = 'lOpenInv'
      Color = clPurple
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label33: TLabel
      Left = 3
      Top = 96
      Width = 44
      Height = 13
      Caption = #1044#1086#1075#1086#1074#1086#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edSN: TM207DBEdit
      Left = 92
      Top = 4
      Width = 81
      Height = 21
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsSList
      TabOrder = 0
      OnMouseDown = edSNMouseDown
    end
    object edSSF: TM207DBEdit
      Left = 92
      Top = 44
      Width = 81
      Height = 21
      Color = clAqua
      DataField = 'SSF'
      DataSource = dm.dsSList
      TabOrder = 2
      OnClick = edSSFClick
    end
    object deNDate: TDBDateEdit
      Left = 92
      Top = 64
      Width = 81
      Height = 21
      Margins.Left = 1
      Margins.Top = 1
      DataField = 'NDATE'
      DataSource = dm.dsSList
      Color = clAqua
      NumGlyphs = 2
      TabOrder = 3
      OnClick = deNDateClick
    end
    object edTr: TM207DBEdit
      Left = 454
      Top = 27
      Width = 101
      Height = 21
      Color = clInfoBk
      DataField = 'COST'
      DataSource = dm.dsSList
      ReadOnly = True
      TabOrder = 6
      OnClick = edTrClick
      OnKeyDown = edTrKeyDown
    end
    object lcNDS: TDBLookupComboBox
      Left = 248
      Top = 42
      Width = 145
      Height = 21
      Color = clInfoBk
      DataField = 'NDSID'
      DataSource = dm.dsSList
      KeyField = 'NDSID'
      ListField = 'NAME'
      ListSource = dmCom.dsNDS
      TabOrder = 4
      OnClick = lcNDSClick
      OnCloseUp = lcNDSCloseUp
    end
    object edNDS: TM207DBEdit
      Left = 554
      Top = 27
      Width = 85
      Height = 21
      Color = clInfoBk
      DataField = 'NDS'
      DataSource = dm.dsSList
      TabOrder = 7
      OnClick = edNDSClick
      OnKeyDown = edTrKeyDown
    end
    object lcPayType: TDBLookupComboBox
      Left = 248
      Top = 64
      Width = 145
      Height = 21
      Color = clInfoBk
      DataField = 'PAYTYPEID'
      DataSource = dm.dsSList
      KeyField = 'PAYTYPEID'
      ListField = 'NAME'
      ListSource = dmCom.dsPayType
      TabOrder = 5
      OnClick = lcPayTypeClick
      OnCloseUp = lcPayTypeCloseUp
    end
    object edPTr: TM207DBEdit
      Left = 454
      Top = 47
      Width = 25
      Height = 21
      Color = clInfoBk
      DataField = 'PTR'
      DataSource = dm.dsSList
      TabOrder = 8
      OnKeyDown = edTrKeyDown
      OnKeyPress = edPTrKeyPress
      OnMouseDown = edPTrMouseDown
    end
    object DBEdit1: TM207DBEdit
      Left = 578
      Top = 47
      Width = 61
      Height = 21
      Color = clInfoBk
      DataField = 'TRNDS'
      DataSource = dm.dsSList
      TabOrder = 11
      OnClick = DBEdit1Click
      OnKeyDown = edTrKeyDown
    end
    object edTrans: TM207DBEdit
      Left = 478
      Top = 47
      Width = 77
      Height = 21
      AutoSelect = False
      Color = clInfoBk
      DataField = 'TR'
      DataSource = dm.dsSList
      TabOrder = 9
      OnClick = edTransEnter
      OnEnter = edTransEnter
      OnKeyDown = edTrKeyDown
    end
    object edPTrNDS: TM207DBEdit
      Left = 554
      Top = 47
      Width = 25
      Height = 21
      Color = clInfoBk
      DataField = 'PTRNDS'
      DataSource = dm.dsSList
      TabOrder = 10
      OnClick = edPTrNDSClick
      OnKeyDown = edTrKeyDown
      OnKeyPress = edPTrKeyPress
    end
    object chbxNoCreatePr: TDBCheckBox
      Left = 92
      Top = 114
      Width = 145
      Height = 17
      HelpType = htKeyword
      TabStop = False
      Caption = #1053#1077' '#1089#1086#1079#1076#1072#1074#1072#1090#1100' '#1087#1088#1080#1082#1072#1079#1099
      DataField = 'NOTPR'
      DataSource = dm.dsSList
      Enabled = False
      TabOrder = 12
      ValueChecked = '1'
      ValueUnchecked = '0'
      Visible = False
    end
    object chbxNoRecalcTR: TDBCheckBox
      Left = 647
      Top = 69
      Width = 137
      Height = 17
      Caption = #1053#1077' '#1087#1077#1088#1077#1089#1095#1080#1090#1099#1074#1072#1090#1100' '#1058#1056
      DataField = 'EDITTR'
      DataSource = dm.dsSList
      TabOrder = 13
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object dbedSadte: TDBEditEh
      Left = 92
      Top = 24
      Width = 61
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsSList
      EditButtons = <>
      Enabled = False
      TabOrder = 1
      Visible = True
    end
    object btdate: TBitBtn
      Left = 152
      Top = 25
      Width = 20
      Height = 20
      TabOrder = 14
      TabStop = False
      OnClick = btdateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
    object cbOwner: TDBCheckBoxEh
      Left = 345
      Top = 114
      Width = 181
      Height = 17
      Alignment = taLeftJustify
      Caption = #1057#1084#1077#1085#1072' '#1074#1083#1072#1076#1077#1083#1100#1094#1072' '#1087#1088#1080' '#1074#1086#1079#1074#1088#1072#1090#1077
      DataField = 'comission$scheme'
      DataSource = dm.dsSList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 15
      ValueChecked = '1'
      ValueUnchecked = '2'
    end
    object lcSup: TcxDBLookupComboBox
      Left = 214
      Top = 4
      DataBinding.DataField = 'SUPID'
      DataBinding.DataSource = dm.dsSList
      Properties.KeyFieldNames = 'D_COMPID'
      Properties.ListColumns = <
        item
          FieldName = 'FNAME'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = dm.dsSup
      Properties.OnCloseUp = lcSupCloseUp
      Style.Color = clAqua
      TabOrder = 16
      OnClick = lcSup_oldClick
      Width = 277
    end
    object edMargin: TEdit
      Left = 712
      Top = 43
      Width = 33
      Height = 21
      TabOrder = 17
    end
    object btnSetMargin: TcxButton
      Left = 745
      Top = 43
      Width = 20
      Height = 20
      Action = acSetMargin
      TabOrder = 18
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        20000000000000040000120B0000120B00000000000000000000F8F8F800D6D6
        D6019595950389898906848484098081800E7E7F7F117F7C7E0D7D7D7D118182
        810F8383830C8585850A8888880694949404D5D5D501F6F6F600ACACAC013131
        31070A0A0A100000001900000026000000250000002600000040000000330000
        00350000002C000000240000001B070707122B2B2B08A0A0A001FAFAFA00F7F7
        F700DBDCDC02A6A5A505A29C9F00436150182071449354B780F41A633A5C8182
        81009192910A94959404A2A2A202D0D0D001F4F4F400F9F9F900FFFFFF00FFFF
        FF00FFFFFF03FFFFFF0070BC92464CBE7FE07DE9ACFF93F7BFFF37B770EC90C8
        A92EFFFFFF00FBFDFC02FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FDFE
        FE02F9FBFA0053AB7B662DB569FC5DDF97FE4FD88CF84AD588F953DB8FFF1CA1
        58D1B0D4C112FFFFFF00F8FAF901FDFDFD00FEFEFE00FEFEFE00FFFFFF03FFFF
        FF0052B27C5411B259FF25CD70FF06B954FE06BC56FF0CC25CFF19C666F920C4
        69FF0C994AA7C8E5D600FFFFFF02FDFEFD00FFFFFF00FFFFFF00FFFFFF00BCE2
        CC1300A347E503AE4DFB0DA350AC4BB87C7C2BAC658F07A74CE001B04EFF12BD
        5FFA00AE4DFF47B47770FEFFFE00FDFFFE03FFFFFF00FFFFFF00FFFFFF007ECC
        9D6B00A042E38DD1AC2DEEF6F100F9FCFA02FDFDFD00BFE4CF0C30B26B8901A7
        49FC13B45CFF00A140F685D0A638FFFFFF00FAFDFB03FFFFFF00FFFFFF008DD8
        AE43A4DFBE2DFFFFFF00FDFEFE03FDFEFE05FEFEFE03FFFFFF01FCFDFC0053C3
        844F00A444EB0EA753FF0CAA53C9BEEBD209FFFFFF00FCFEFD01FFFFFF00FAFD
        FB00FDFEFE01FBFDFD02FEFFFE00FEFFFE00FEFFFE00FCFEFD02FEFEFF04FDFE
        FD005CCD8E3400A74AE2009A43FF47C47D7FFEFFFF00FFFFFE03FFFFFF00FEFF
        FF02FCFEFD02FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFFFE00FCFE
        FD05FFFFFF006CD99C2B01A64AE7009C40F889E3B029FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFF
        FE00FDFEFD04FFFFFF005EDC963100A140FC24BC68A5EEFFF600FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FDFFFE00FDFEFE04FEFFFF0058DC924D00B144F28CE9B628FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FDFFFE00FDFEFE04FFFFFF0038DD81855DE49984FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FCFFFD00FFFFFF00BDF6D60F6DE9A535FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FEFFFE00FDFFFE01FFFFFF00}
      LookAndFeel.Kind = lfOffice11
    end
    object GroupBoxTolling: TGroupBox
      Left = 783
      Top = 2
      Width = 281
      Height = 170
      Caption = #1044#1072#1074#1072#1083#1100#1095#1077#1089#1082#1072#1103' '#1089#1093#1077#1084#1072
      TabOrder = 19
      Visible = False
      object Label29: TLabel
        Left = 7
        Top = 38
        Width = 57
        Height = 13
        Caption = #1042#1077#1089' '#1087#1086#1090#1077#1088#1100
      end
      object Label30: TLabel
        Left = 7
        Top = 59
        Width = 98
        Height = 13
        Caption = #1041#1091#1093'. '#1094#1077#1085#1072' '#1079#1072' '#1075#1088#1072#1084#1084
      end
      object Label31: TLabel
        Left = 7
        Top = 80
        Width = 63
        Height = 13
        Caption = #1042#1077#1089' '#1074#1089#1090#1072#1074#1086#1082
      end
      object Label32: TLabel
        Left = 7
        Top = 17
        Width = 101
        Height = 13
        Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089' '#1080#1079#1076#1077#1083#1080#1081
      end
      object Label34: TLabel
        Left = 11
        Top = 144
        Width = 32
        Height = 13
        Caption = #1055#1088#1086#1073#1072
      end
      object Label35: TLabel
        Left = 8
        Top = 101
        Width = 99
        Height = 13
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1074#1089#1090#1072#1074#1086#1082
      end
      object lbMaterial: TLabel
        Left = 184
        Top = 144
        Width = 3
        Height = 13
      end
      object edTotalLossesWeight: TDBEdit
        Left = 157
        Top = 36
        Width = 90
        Height = 21
        DataField = 'TOTAL$LOSSES$WEIGHT'
        DataSource = dm.dsTollingParams
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object edBuhPrice585: TDBEdit
        Left = 157
        Top = 57
        Width = 90
        Height = 21
        DataField = 'AVERAGE$PRICE'
        DataSource = dm.dsTollingParams
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object edTotalInsertionsWeight: TDBEdit
        Left = 157
        Top = 78
        Width = 90
        Height = 21
        DataField = 'TOTAL$INSERTIONS$WEIGHT'
        DataSource = dm.dsTollingParams
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object edTotalInvoiceWeight: TDBEdit
        Left = 157
        Top = 15
        Width = 90
        Height = 21
        DataField = 'TOTAL$INVOICE$WEIGHT'
        DataSource = dm.dsTollingParams
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object cxButton1: TcxButton
        Left = 253
        Top = 54
        Width = 25
        Height = 25
        TabOrder = 4
        OnClick = cxButton1Click
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          20000000000000040000120B0000120B00000000000000000000F8F8F800D6D6
          D6019595950389898906848484098081800E7E7F7F117F7C7E0D7D7D7D118182
          810F8383830C8585850A8888880694949404D5D5D501F6F6F600ACACAC013131
          31070A0A0A100000001900000026000000250000002600000040000000330000
          00350000002C000000240000001B070707122B2B2B08A0A0A001FAFAFA00F7F7
          F700DBDCDC02A6A5A505A29C9F00436150182071449354B780F41A633A5C8182
          81009192910A94959404A2A2A202D0D0D001F4F4F400F9F9F900FFFFFF00FFFF
          FF00FFFFFF03FFFFFF0070BC92464CBE7FE07DE9ACFF93F7BFFF37B770EC90C8
          A92EFFFFFF00FBFDFC02FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FDFE
          FE02F9FBFA0053AB7B662DB569FC5DDF97FE4FD88CF84AD588F953DB8FFF1CA1
          58D1B0D4C112FFFFFF00F8FAF901FDFDFD00FEFEFE00FEFEFE00FFFFFF03FFFF
          FF0052B27C5411B259FF25CD70FF06B954FE06BC56FF0CC25CFF19C666F920C4
          69FF0C994AA7C8E5D600FFFFFF02FDFEFD00FFFFFF00FFFFFF00FFFFFF00BCE2
          CC1300A347E503AE4DFB0DA350AC4BB87C7C2BAC658F07A74CE001B04EFF12BD
          5FFA00AE4DFF47B47770FEFFFE00FDFFFE03FFFFFF00FFFFFF00FFFFFF007ECC
          9D6B00A042E38DD1AC2DEEF6F100F9FCFA02FDFDFD00BFE4CF0C30B26B8901A7
          49FC13B45CFF00A140F685D0A638FFFFFF00FAFDFB03FFFFFF00FFFFFF008DD8
          AE43A4DFBE2DFFFFFF00FDFEFE03FDFEFE05FEFEFE03FFFFFF01FCFDFC0053C3
          844F00A444EB0EA753FF0CAA53C9BEEBD209FFFFFF00FCFEFD01FFFFFF00FAFD
          FB00FDFEFE01FBFDFD02FEFFFE00FEFFFE00FEFFFE00FCFEFD02FEFEFF04FDFE
          FD005CCD8E3400A74AE2009A43FF47C47D7FFEFFFF00FFFFFE03FFFFFF00FEFF
          FF02FCFEFD02FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFFFE00FCFE
          FD05FFFFFF006CD99C2B01A64AE7009C40F889E3B029FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFF
          FE00FDFEFD04FFFFFF005EDC963100A140FC24BC68A5EEFFF600FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FDFFFE00FDFEFE04FEFFFF0058DC924D00B144F28CE9B628FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FDFFFE00FDFEFE04FFFFFF0038DD81855DE49984FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FCFFFD00FFFFFF00BDF6D60F6DE9A535FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FEFFFE00FDFFFE01FFFFFF00}
        LookAndFeel.Kind = lfOffice11
      end
      object edPureK: TDBComboBox
        Left = 65
        Top = 144
        Width = 104
        Height = 21
        Style = csDropDownList
        DataField = 'PURE'
        DataSource = dm.dsTollingParams
        ItemHeight = 13
        Items.Strings = (
          '375'
          '585'
          '875'
          '925')
        TabOrder = 5
        OnChange = edPureKChange
      end
      object DBEdit2: TDBEdit
        Left = 156
        Top = 99
        Width = 90
        Height = 21
        DataField = 'TOTAL$INSERTIONS$COST'
        DataSource = dm.dsTollingParams
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -8
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object cxDBCheckBox1: TcxDBCheckBox
        Left = 6
        Top = 117
        Caption = #1042#1089#1090#1072#1074#1082#1080' '#1074#1082#1083#1102#1095#1077#1085#1099
        DataBinding.DataField = 'INCLUDE$INSERTIONS'
        DataBinding.DataSource = dm.dsTollingParams
        Properties.Alignment = taRightJustify
        Properties.NullStyle = nssUnchecked
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.Kind = lfOffice11
        TabOrder = 7
        Width = 206
      end
    end
    object chbxAddNDS: TDBCheckBox
      Left = 564
      Top = 114
      Width = 201
      Height = 17
      Caption = #1044#1086#1073#1072#1074#1083#1103#1090#1100' '#1053#1044#1057' '#1082' '#1087#1088#1080#1093#1086#1076#1085#1086#1081' '#1094#1077#1085#1077
      DataField = 'ADD$NDS$TO$PRICE'
      DataSource = dm.dsSList
      Enabled = False
      TabOrder = 20
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object lcContract: TcxDBExtLookupComboBox
      Left = 97
      Top = 91
      AutoSize = False
      DataBinding.DataField = 'CONTRACT$ID'
      DataBinding.DataSource = dm.dsSList
      Properties.Alignment.Horz = taCenter
      Properties.DropDownListStyle = lsFixedList
      Properties.View = GridContractView
      Properties.KeyFieldNames = 'ID'
      Properties.ListFieldItem = GridContractViewNAMEFORMATED
      Properties.PopupAlignment = taRightJustify
      Style.Color = clAqua
      TabOrder = 21
      OnClick = lcSup_oldClick
      Height = 21
      Width = 301
    end
  end
  object paDict: TPanel
    Left = 0
    Top = 393
    Width = 1153
    Height = 235
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'paDict'
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 289
      Top = 0
      Height = 240
      Align = alNone
    end
    object Splitter3: TSplitter
      Left = 373
      Top = 0
      Width = 2
      Height = 235
      MinSize = 20
      ExplicitLeft = 247
      ExplicitTop = 3
    end
    object Splitter7: TSplitter
      Left = 669
      Top = 0
      Height = 235
      MinSize = 20
    end
    object Panel3: TPanel
      Left = 375
      Top = 0
      Width = 294
      Height = 235
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object dgArt: TM207IBGrid
        Left = 0
        Top = 35
        Width = 294
        Height = 200
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsArt
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        PopupMenu = pmA
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = dgArtCellClick
        OnDblClick = dgArtDblClick
        OnEditButtonClick = dgArtEditButtonClick
        OnEnter = dgArtEnter
        OnExit = dgArtExit
        OnKeyDown = dgArtKeyDown
        OnMouseDown = dgArtMouseDown
        IniStorage = fs1
        TitleButtons = True
        OnGetCellParams = dgArtGetCellParams
        OnShowEditor = dgArtShowEditor
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        OnGetCellCheckBox = dgArtGetCellCheckBox
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FULLART'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Alignment = taLeftJustify
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            PickList.Strings = (
              #1043#1088
              #1064#1090)
            Title.Alignment = taCenter
            Title.Caption = #1045#1048
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 30
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RQ'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 27
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Expanded = False
            FieldName = 'RW'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 49
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'VALATT1'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1040#1090#1088'1'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 38
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'VALATT2'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1040#1090#1088'2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 39
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'F1'
            Title.Alignment = taCenter
            Title.Caption = '*'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'F2'
            Title.Alignment = taCenter
            Title.Caption = '**'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object tb2: TSpeedBar
        Left = 0
        Top = 0
        Width = 294
        Height = 35
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 1
        InternalVer = 1
        object cbSearch: TCheckBox
          Left = 4
          Top = 8
          Width = 53
          Height = 17
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = cbSearchClick
          OnMouseDown = cbSearchMouseDown
        end
        object ceArt: TComboEdit
          Left = 58
          Top = 5
          Width = 93
          Height = 21
          ButtonHint = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086#1080#1089#1082
          Color = clInfoBk
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            04000000000080000000120B0000120B00001000000010000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADAD7000007
            DADAADAD019999910DADDAD09999999990DAAD0999999999990DD71999999999
            9917A0999FF999FF9990D09999FF9FF99990A099999FFF999990D099999FFF99
            9990A09999FF9FF99990D7199FF999FF9917AD0999999999990DDAD099999999
            90DAADAD019999910DADDADAD7000007DADAADADADADADADADAD}
          NumGlyphs = 1
          TabOrder = 1
          OnButtonClick = ceArtButtonClick
          OnChange = ceArtChange
          OnClick = ceArtClick
          OnEnter = ceArtEnter
          OnKeyDown = ceArtKeyDown
          OnKeyUp = ceArtKeyUp
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1042#1089#1077
          Caption = 'SpeedItem1'
          Hint = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Spacing = 1
          Left = 155
          Top = 3
          Visible = True
          OnClick = SpeedItem1Click
          SectionName = 'Untitled (0)'
        end
        object siMergeComplex: TSpeedItem
          Action = acBegEndMerge
          BtnCaption = 'acBegEndMerge'
          Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103
          ImageIndex = 53
          Spacing = 1
          Left = 187
          Top = 3
          Visible = True
          OnClick = acBegEndMergeExecute
          SectionName = 'Untitled (0)'
        end
      end
    end
    object plFilter: TPanel
      Left = 0
      Top = 0
      Width = 373
      Height = 235
      Align = alLeft
      TabOrder = 0
      object Splitter5: TSplitter
        Tag = 3
        Left = 99
        Top = 1
        Height = 233
        AutoSnap = False
        MinSize = 20
        ExplicitLeft = 90
        ExplicitTop = -4
      end
      object Splitter9: TSplitter
        Left = 47
        Top = 1
        Height = 233
        ExplicitLeft = 38
        ExplicitTop = -4
      end
      object Splitter10: TSplitter
        Left = 148
        Top = 1
        Height = 233
        ExplicitLeft = 139
        ExplicitTop = -4
      end
      object Splitter11: TSplitter
        Left = 255
        Top = 1
        Height = 233
        ExplicitLeft = 249
        ExplicitTop = 6
      end
      object Splitter4: TSplitter
        Left = 200
        Top = 1
        Height = 233
        ExplicitLeft = 191
        ExplicitTop = -4
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 46
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 203
        Top = 1
        Width = 52
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 102
        Top = 1
        Width = 46
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
        OnMouseDown = lbGoodMouseDown
      end
      object lbIns: TListBox
        Left = 151
        Top = 1
        Width = 49
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
        OnMouseDown = lbInsMouseDown
      end
      object lbCountry: TListBox
        Left = 50
        Top = 1
        Width = 49
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt1: TListBox
        Left = 258
        Top = 1
        Width = 52
        Height = 233
        Align = alLeft
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 5
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt2: TListBox
        Left = 310
        Top = 1
        Width = 62
        Height = 233
        Align = alClient
        Color = clBtnFace
        Constraints.MinWidth = 24
        ItemHeight = 13
        TabOrder = 6
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object plArt2: TPanel
      Left = 672
      Top = 0
      Width = 403
      Height = 235
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 2
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 401
        Height = 233
        Align = alClient
        TabOrder = 0
        object dgA2: TM207IBGrid
          Left = 1
          Top = 33
          Width = 399
          Height = 199
          Align = alClient
          Color = clBtnFace
          DataSource = dm.dsA2
          Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
          PopupMenu = pmA2
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnColExit = dgA2ColExit
          OnDblClick = dgA2DblClick
          OnEditButtonClick = dgA2EditButtonClick
          OnEnter = dgA2Enter
          OnExit = dgA2Exit
          OnKeyDown = dgA2KeyDown
          OnKeyPress = dgA2KeyPress
          OnMouseDown = dgA2MouseDown
          IniStorage = fs1
          TitleButtons = True
          OnGetCellParams = dgA2GetCellParams
          MultiShortCut = 0
          ColorShortCut = 32835
          InfoShortCut = 0
          ClearHighlight = True
          OnGetCellCheckBox = dgA2GetCellCheckBox
          SortOnTitleClick = True
          Columns = <
            item
              Color = clInfoBk
              Expanded = False
              FieldName = 'ART2'
              Title.Alignment = taCenter
              Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 81
              Visible = True
            end
            item
              Color = clInfoBk
              Expanded = False
              FieldName = 'nds'
              Title.Alignment = taCenter
              Title.Caption = #1053#1044#1057
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 40
              Visible = True
            end
            item
              Alignment = taLeftJustify
              Color = clInfoBk
              Expanded = False
              FieldName = 'USEMARGIN'
              PickList.Strings = (
                #1044#1072
                #1053#1077#1090)
              Title.Alignment = taCenter
              Title.Caption = #1053#1072#1094'.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 29
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Color = clAqua
              Expanded = False
              FieldName = 'PRICE$HACK'
              Title.Alignment = taCenter
              Title.Caption = #1055#1088'.'#1094#1077#1085#1072
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Expanded = False
              FieldName = 'PRICE1'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1055#1088'.'#1094#1077#1085#1072' + '#1053#1044#1057
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 84
              Visible = True
            end
            item
              Alignment = taLeftJustify
              Color = clHighlight
              Expanded = False
              FieldName = 'Tolling$Price'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWhite
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Title.Caption = #1055#1077#1088#1077#1088#1072#1073#1086#1090#1082#1072' + '#1084#1077#1090#1072#1083#1083
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clBlack
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 105
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Color = clInfoBk
              Expanded = False
              FieldName = 'PRICE2'
              Title.Alignment = taCenter
              Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 53
              Visible = True
            end
            item
              Color = clInfoBk
              Expanded = False
              FieldName = 'W'
              Title.Alignment = taCenter
              Title.Caption = #1042#1077#1089
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 35
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Expanded = False
              FieldName = 'RQ'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 42
              Visible = True
            end
            item
              ButtonStyle = cbsEllipsis
              Expanded = False
              FieldName = 'RW'
              ReadOnly = True
              Title.Alignment = taCenter
              Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'F1'
              Title.Alignment = taCenter
              Title.Caption = '*'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'F2'
              Title.Alignment = taCenter
              Title.Caption = '**'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Visible = True
            end>
        end
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 399
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object Label18: TLabel
            Left = 8
            Top = 2
            Width = 47
            Height = 13
            Caption = #1055#1088'. '#1094#1077#1085#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label19: TLabel
            Left = 8
            Top = 16
            Width = 52
            Height = 13
            Caption = #1056#1072#1089'. '#1094#1077#1085#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBText8: TDBText
            Left = 212
            Top = 0
            Width = 65
            Height = 17
            DataField = 'PRICE1'
            DataSource = dm.dsInOutPrice
            Visible = False
          end
          object DBText9: TDBText
            Left = 212
            Top = 12
            Width = 65
            Height = 17
            DataField = 'PRICE2'
            DataSource = dm.dsInOutPrice
            Visible = False
          end
          object Label20: TLabel
            Left = 88
            Top = 0
            Width = 3
            Height = 13
          end
          object Label21: TLabel
            Left = 88
            Top = 16
            Width = 3
            Height = 13
          end
        end
      end
    end
    object plFilter2: TPanel
      Left = 1075
      Top = 0
      Width = 78
      Height = 235
      Align = alRight
      TabOrder = 3
      Visible = False
      object Label22: TLabel
        Left = 4
        Top = 8
        Width = 43
        Height = 13
        Caption = #1054#1088#1075#1072#1085#1080#1079
      end
      object lbFilterCountry: TLabel
        Left = 4
        Top = 28
        Width = 36
        Height = 13
        Caption = #1057#1090#1088#1072#1085#1072
      end
      object Label23: TLabel
        Left = 4
        Top = 48
        Width = 31
        Height = 13
        Caption = #1058#1086#1074#1072#1088
      end
      object Label24: TLabel
        Left = 4
        Top = 68
        Width = 3
        Height = 13
      end
      object Label25: TLabel
        Left = 4
        Top = 68
        Width = 42
        Height = 13
        Caption = #1042#1089#1090#1072#1074#1082#1072
      end
      object Label26: TLabel
        Left = 4
        Top = 88
        Width = 50
        Height = 13
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083
      end
      object Label27: TLabel
        Left = 4
        Top = 108
        Width = 54
        Height = 13
        Caption = #1040#1090#1090#1088#1080#1073#1091#1090' 1'
      end
      object Label28: TLabel
        Left = 4
        Top = 128
        Width = 54
        Height = 13
        Caption = #1040#1090#1090#1088#1080#1073#1091#1090' 2'
      end
      object cmbxComp: TDBComboBoxEh
        Left = 60
        Top = 4
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
      end
      object cmbxCountry: TDBComboBoxEh
        Left = 60
        Top = 24
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 1
        Visible = True
      end
      object cmbxIns: TDBComboBoxEh
        Left = 60
        Top = 64
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 2
        Visible = True
      end
      object cmbxGood: TDBComboBoxEh
        Left = 60
        Top = 44
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 3
        Visible = True
      end
      object cmbxAtt1: TDBComboBoxEh
        Left = 60
        Top = 104
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 4
        Visible = True
      end
      object cmbxMat: TDBComboBoxEh
        Left = 60
        Top = 84
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 5
        Visible = True
      end
      object cmbxAtt2: TDBComboBoxEh
        Left = 60
        Top = 124
        Width = 109
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 6
        Visible = True
      end
    end
  end
  object dgEl: TM207IBGrid
    Left = 0
    Top = 228
    Width = 1153
    Height = 162
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsSEl
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgMultiSelect]
    PopupMenu = pmEl
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnColEnter = dgElColEnter
    OnColExit = dgElColExit
    OnDblClick = dgElEditButtonClick
    OnEditButtonClick = dgElEditButtonClick
    OnKeyDown = dgElKeyDown
    OnMouseDown = dgArtMouseDown
    FixedCols = 7
    ClearSelection = False
    IniStorage = fs1
    MultiSelect = True
    TitleButtons = True
    OnGetCellParams = dgElGetCellParams
    MultiShortCut = 16472
    ColorShortCut = 32835
    InfoShortCut = 0
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'RecNo'
        Title.Alignment = taCenter
        Title.Caption = #8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRODCODE'
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_MATID'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 26
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_GOODID'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 20
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_INSID'
        Title.Alignment = taCenter
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 22
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'D_COUNTRYID'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ART2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UNITID'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1045#1048
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 28
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'PRICE$HACK'
        Title.Caption = #1055#1088'.'#1094#1077#1085#1072
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'.'#1094#1077#1085#1072' + '#1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'QUANTITY'
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTALWEIGHT'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cost'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1057#1091#1084#1084#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NdsName'
        Title.Alignment = taCenter
        Title.Caption = #1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 75
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'USEMARGIN'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1094'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EX_PRICE2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1062#1077#1085#1072' '#1089' '#1085#1072#1094#1077#1085#1082#1086#1081
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 102
        Visible = True
      end>
  end
  object GridContract: TcxGrid
    Left = 113
    Top = 252
    Width = 301
    Height = 135
    TabOrder = 5
    Visible = False
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    object GridContractView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsContract
      DataController.KeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsView.NoDataToDisplayInfoText = '...'
      OptionsView.ScrollBars = ssVertical
      OptionsView.GroupByBox = False
      object GridContractViewNUMBER: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'NUMBER'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewCONTRACTDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'CONTRACT$DATE'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewNAME: TcxGridDBColumn
        Caption = #1042#1080#1076
        DataBinding.FieldName = 'NAME'
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object GridContractViewNAMEFORMATED: TcxGridDBColumn
        DataBinding.FieldName = 'NAME$FORMATED'
        Visible = False
      end
    end
    object GridContractLevel: TcxGridLevel
      GridView = GridContractView
    end
  end
  object pmVisCol: TPopupMenu
    Left = 208
    Top = 200
    object miVisCol: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1083#1086#1085#1086#1082
      OnClick = miVisColClick
    end
  end
  object fs1: TM207FormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'plFilter.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width'
      'cbSearch.Checked'
      'paDict.Height'
      'lbAtt1.Width'
      'lbComp.Width'
      'lbAtt2.Width'
      'Panel3.Width'
      'plArt2.Width'
      'Splitter5.Width'
      'Splitter9.Width'
      'Splitter1.Width'
      'Splitter10.Width'
      'Splitter11.Width'
      'Splitter2.Width'
      'Splitter3.Width'
      'Splitter6.Width'
      'Splitter7.Width')
    StoredValues = <>
    Left = 168
    Top = 200
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 264
    Top = 200
    object miSElItem: TMenuItem
      Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      ShortCut = 114
    end
    object N3: TMenuItem
      Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077
      ImageIndex = 50
      ShortCut = 115
    end
    object N1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelClick
    end
    object N2: TMenuItem
      Caption = #1055#1086#1080#1089#1082
      ImageIndex = 48
      ShortCut = 118
      OnClick = siFindClick
    end
    object N5: TMenuItem
      Caption = #1062#1077#1085#1099
      ImageIndex = 32
      ShortCut = 116
      OnClick = siPriceClick
    end
    object N10: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      ShortCut = 117
      OnClick = miInsClick
    end
    object N11: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnClick = miAddClick
    end
    object N38: TMenuItem
      Caption = '-'
    end
    object SElId1: TMenuItem
      Caption = 'SElId'
      OnClick = SElId1Click
    end
  end
  object pmA2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 772
    Top = 412
    object miIns: TMenuItem
      Tag = 2
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      ShortCut = 117
      OnClick = miInsClick
    end
    object N4: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      ShortCut = 16429
      OnClick = N4Click
    end
    object N9: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ShortCut = 45
      OnClick = miAddClick
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object N14: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = N14Click
    end
    object N19: TMenuItem
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 16397
      OnClick = N19Click
    end
    object N20: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1088#1080#1093#1086#1076#1085#1086#1081' '#1094#1077#1085#1099
      OnClick = N20Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object N21: TMenuItem
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1094#1077#1085#1099
      OnClick = N21Click
    end
    object N33: TMenuItem
      Caption = '-'
    end
    object Art2Id1: TMenuItem
      Caption = 'Art2Id'
      OnClick = Art2Id1Click
    end
  end
  object pmA: TPopupMenu
    Images = dmCom.ilButtons
    Left = 412
    Top = 408
    object miAddArt: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnClick = miAddArtClick
    end
    object miInsFromSearch: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079' '#1087#1086#1080#1089#1082#1072
      ShortCut = 16429
      OnClick = miInsFromSearchClick
    end
    object miAEdit: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1072
      ShortCut = 115
      OnClick = miAEditClick
    end
    object N22: TMenuItem
      Caption = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 116
    end
    object N43: TMenuItem
      Tag = 1
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
      OnClick = nHistartClick
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object N16: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = N16Click
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 16397
      OnClick = N18Click
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object N28: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
      OnClick = N28Click
    end
    object N27: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnClick = N27Click
    end
    object N26: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1090#1086#1074#1072#1088#1086#1074
      OnClick = N26Click
    end
    object N7: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1089#1090#1088#1072#1085
      OnClick = N7Click
    end
    object N25: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1089#1090#1072#1074#1086#1082
      OnClick = N25Click
    end
    object N111: TMenuItem
      Action = acDictAtt1
    end
    object N210: TMenuItem
      Action = acDictAtt2
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object N30: TMenuItem
      Caption = #1055#1077#1088#1077#1089#1086#1088#1090#1080#1088#1086#1074#1082#1072
      ShortCut = 13
      Visible = False
      OnClick = N30Click
    end
    object N29: TMenuItem
      Caption = '-'
    end
    object DArtId1: TMenuItem
      Action = acShowArtId
    end
  end
  object ppPrint: TRxPopupMenu
    Style = msBtnLowered
    ShowCheckMarks = False
    Left = 344
    Top = 200
  end
  object pmOpt: TPopupMenu
    Left = 640
    Top = 204
    object N39: TMenuItem
      Caption = #1053#1072#1094#1077#1085#1082#1080' '#1076#1083#1103' '#1076#1072#1085#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnClick = N39Click
    end
    object N40: TMenuItem
      Caption = #1054#1073#1097#1080#1077' '#1085#1072#1094#1077#1085#1082#1080
      OnClick = N40Click
    end
    object N41: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1080' '#1076#1083#1103' '#1074#1089#1077#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnClick = N41Click
    end
    object N110: TMenuItem
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1099' '#1080#1079' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072' '#1076#1083#1103' '#1074#1089#1077#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnClick = N110Click
    end
    object N42: TMenuItem
      Caption = '-'
    end
    object N32: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1082#1072' '#1080#1079' '#1073#1091#1092#1077#1088#1072
      OnClick = N32Click
    end
    object N31: TMenuItem
      Caption = #1056#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077
      Enabled = False
      OnClick = N31Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object nHistart: TMenuItem
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
      OnClick = nHistartClick
    end
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 52
    Top = 196
    object acDictAtt1: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1090#1090#1088#1080#1073#1091#1090#1086#1074' 1'
      OnExecute = acDictAtt1Execute
    end
    object acDictAtt2: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1090#1090#1088#1080#1073#1091#1090#1086#1074' 2'
      OnExecute = acDictAtt2Execute
    end
    object acidNum: TAction
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      OnExecute = acidNumExecute
      OnUpdate = acidNumUpdate
    end
    object acShowArtId: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1082#1086#1076' '#1072#1088#1090#1080#1082#1091#1083#1072
      OnExecute = acShowArtIdExecute
      OnUpdate = acShowArtIdUpdate
    end
    object acBegEndMerge: TAction
      Caption = 'acBegEndMerge'
      OnExecute = acBegEndMergeExecute
    end
    object acClose: TAction
      Caption = 'acClose'
      OnExecute = acCloseExecute
    end
    object acOpen: TAction
      Caption = 'acOpen'
      OnExecute = acOpenExecute
    end
    object acCloseWithAct: TAction
      Caption = 'acCloseWithAct'
      OnExecute = acCloseWithActExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
    object acSetMargin: TAction
      ImageIndex = 0
      OnExecute = acSetMarginExecute
      OnUpdate = acSetMarginUpdate
    end
  end
  object dsArtUpdate: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select s.sinvid, p.price2 price_new, s.price2 price_curr, s.art2' +
        'id'
      'from sel s, price p'
      'where s.sinvid = :sinvid and'
      '        p.art2id = s.art2id and'
      '        p.price2 <> s.price2 and'
      '        p.price2 <> 0'
      'group by s.sinvid, p.price2, s.price2, s.art2id')
    BeforeOpen = dsArtUpdateBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 96
    Top = 256
    object dsArtUpdatePRICE_NEW: TFIBFloatField
      FieldName = 'PRICE_NEW'
    end
    object dsArtUpdatePRICE_CURR: TFIBFloatField
      FieldName = 'PRICE_CURR'
    end
    object dsArtUpdateART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object dsArtUpdateSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
  end
  object DataSource1: TDataSource
    DataSet = dsArtUpdate
    Left = 64
    Top = 256
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   1'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = :DTYPE'
      'order by HDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 784
    Top = 40
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 816
    Top = 40
  end
  object dsContract: TDataSource
    DataSet = taContract
    Left = 336
    Top = 272
  end
  object taContract: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.id, c.number, c.contract$date, c.contract$end$date, c.i' +
        's$unlimited, ct.id classid, ct.name'
      'from contract c, contract$types ct'
      'where c.company$id = 1 and c.agent$id = :agent$id and'
      
        '((onlydate(c.contract$end$date) >= :invoice$date) or (c.is$unlim' +
        'ited = 1)) and'
      
        'ct.id = c.contract$type and ct.good = 1 and ((:contract$class$id' +
        ' = 0) or ((ct.id = :contract$class$id))) and'
      'c.is$delete = 0'
      'order by ct.id, c.contract$date'
      ''
      '')
    AfterOpen = taContractAfterOpen
    BeforeOpen = taContractBeforeOpen
    OnCalcFields = taContractCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 368
    Top = 272
    oFetchAll = True
    object taContractID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taContractCLASSID: TFIBIntegerField
      FieldName = 'CLASSID'
    end
    object taContractNUMBER: TFIBStringField
      FieldName = 'NUMBER'
      Size = 32
      EmptyStrToNull = True
    end
    object taContractCONTRACTDATE: TFIBDateTimeField
      Alignment = taCenter
      FieldName = 'CONTRACT$DATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taContractNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 32
      EmptyStrToNull = True
    end
    object taContractNAMEFORMATED: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'NAME$FORMATED'
      Size = 64
      EmptyStrToNull = True
      Calculated = True
    end
    object taContractCONTRACTENDDATE: TFIBDateTimeField
      FieldName = 'CONTRACT$END$DATE'
    end
    object taContractISUNLIMITED: TFIBSmallIntField
      FieldName = 'IS$UNLIMITED'
    end
  end
end
