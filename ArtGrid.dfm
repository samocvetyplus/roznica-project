object frArtGrid: TfrArtGrid
  Left = 0
  Top = 0
  Width = 544
  Height = 240
  TabOrder = 0
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 4
      Top = 6
      Width = 95
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edArt: TEdit
      Left = 102
      Top = 2
      Width = 131
      Height = 21
      Color = clInfoBk
      TabOrder = 0
      Text = 'edArt'
      OnChange = edArtChange
      OnKeyDown = edArtKeyDown
    end
  end
  object dg1: TM207IBGrid
    Left = 0
    Top = 27
    Width = 544
    Height = 213
    Align = alClient
    Color = clBtnFace
    DataSource = dmCom.dsArt
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnMouseDown = dg1MouseDown
    FixedCols = 1
    TitleButtons = True
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    OnGetCellCheckBox = dg1GetCellCheckBox
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'FULLART'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 104
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Comp'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 221
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Mat'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090#1077#1088#1080#1072#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 61
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Color = clInfoBk
        Expanded = False
        FieldName = 'ASSAY'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1086#1073#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 43
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Good'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 81
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UNITID'
        PickList.Strings = (
          #1043#1088#1072#1084#1084#1099
          #1064#1090#1091#1082#1080)
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 45
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Ins'
        Title.Alignment = taCenter
        Title.Caption = #1054#1089#1085'. '#1074#1089#1090#1072#1074#1082#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 94
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE1'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1080#1093#1086#1076'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object pmVisCol: TPopupMenu
    Left = 100
    Top = 108
    object N1: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072' '#1082#1086#1083#1086#1085#1086#1082
    end
  end
  object fs1: TFormStorage
    Options = []
    UseRegistry = False
    StoredProps.Strings = (
      'dg1.Columns'
      'dg1.FixedCols')
    StoredValues = <>
    Left = 220
    Top = 104
  end
end
