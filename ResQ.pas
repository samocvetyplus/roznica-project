unit ResQ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, rxPlacemnt;

type
  TfmRestQ = class(TForm)
    BitBtn1: TBitBtn;
    M207IBGrid1: TM207IBGrid;
    fp: TFormPlacement;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRestQ: TfmRestQ;

implementation

uses Data2, M207Proc;

{$R *.dfm}

procedure TfmRestQ.FormCreate(Sender: TObject);
begin
  with dm2 do
    begin
      Screen.Cursor:=crSQLWait;
      OpenDataSets([quRestQ]);
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmRestQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm2 do
    begin
      CloseDataSets([quRestQ]);
    end;
end;

end.
