object fmErrRq: TfmErrRq
  Left = 243
  Top = 143
  Width = 696
  Height = 480
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dgErr: TM207IBGrid
    Left = 0
    Top = 0
    Width = 688
    Height = 446
    Align = alClient
    Color = clBtnFace
    DataSource = dsErr
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
  end
  object quErr: TpFIBDataSet
    CacheModelOptions.BufferChunks = 100
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 28
    Top = 12
  end
  object dsErr: TDataSource
    DataSet = quErr
    Left = 28
    Top = 64
  end
end
