unit A2PrHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Db, PFIBDataSet, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, FIBDataSet, rxPlacemnt;

type
  TfmA2prHist = class(TForm)
    StatusBar1: TStatusBar;
    FormStorage1: TFormStorage;
    quA2PrHst: TpFIBDataSet;
    quA2PrHstART2HSTID: TIntegerField;
    quA2PrHstART2ID: TIntegerField;
    quA2PrHstD_ARTID: TIntegerField;
    quA2PrHstART2: TFIBStringField;
    quA2PrHstSUPID: TIntegerField;
    quA2PrHstPRICE: TFloatField;
    quA2PrHstOPTPRICE: TFloatField;
    quA2PrHstTSPRICE: TFloatField;
    quA2PrHstTOPTPRICE: TFloatField;
    quA2PrHstSPEQ: TIntegerField;
    quA2PrHstOPEQ: TIntegerField;
    quA2PrHstINVID: TIntegerField;
    quA2PrHstOPR: TSmallintField;
    quA2PrHstD_COMPID: TIntegerField;
    quA2PrHstD_MATID: TFIBStringField;
    quA2PrHstD_GOODID: TFIBStringField;
    quA2PrHstD_INSID: TFIBStringField;
    quA2PrHstUNITID: TIntegerField;
    quA2PrHstART: TFIBStringField;
    quA2PrHstFULLART: TFIBStringField;
    quA2PrHstPRORDITEMID: TIntegerField;
    quA2PrHstPRORDITEMID2: TIntegerField;
    quA2PrHstRECDATE: TDateTimeField;
    quA2PrHstSUP: TFIBStringField;
    quA2PrHstPROD: TFIBStringField;
    quA2PrHstSN: TIntegerField;
    quA2PrHstITYPE: TIntegerField;
    dsA2PrHst: TDataSource;
    M207IBGrid1: TM207IBGrid;
    procedure quA2PrHstBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure quA2PrHstUNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmA2prHist: TfmA2prHist;

implementation

uses comdata, Data, Data2;

{$R *.DFM}

procedure TfmA2prHist.quA2PrHstBeforeOpen(DataSet: TDataSet);
begin
  quA2PrHst.Params[0].AsInteger:=dm.PrHistArt2Id;
end;

procedure TfmA2prHist.FormCreate(Sender: TObject);
begin
  quA2PrHst.Active:=True;
end;

procedure TfmA2prHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  quA2PrHst.Active:=False;
end;

procedure TfmA2prHist.quA2PrHstUNITIDGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  dm.UNITIDGetText(Sender, Text, DisplayText);
end;

end.
