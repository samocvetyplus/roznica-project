object dmIndividualOrder: TdmIndividualOrder
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 437
  Width = 737
  object Connection: TMyConnection
    Database = 'order2'
    ConnectionTimeout = 30
    Options.Charset = 'cp1251'
    Username = 'order'
    Password = '9V4x2Z8r'
    Server = '138.201.247.35'
    Connected = True
    LoginPrompt = False
    Left = 32
    Top = 8
  end
  object dtOrder: TMyQuery
    SQLInsert.Strings = (
      
        'call IND_ORDER_I (:DEP_CUSTOM, :DEP_SEND, 1 , :EMP_CREATE, :EMP_' +
        'STATE );')
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY, '
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.DEP_CUSTOM = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY, '
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.DEP_CUSTOM = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    BeforeOpen = SetDepParam
    BeforeClose = PostBeforeClose
    BeforePost = dtOrderBeforePost
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtOrderNewRecord
    Left = 32
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEPARTMENT'
      end>
    object dtOrderID_ORDER: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object dtOrderFIO: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object dtOrderPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object dtOrderDEP_SEND: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object dtOrderART: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object dtOrderCOMP: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object dtOrderGOOD: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object dtOrderMAT: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object dtOrderSZ: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
    end
    object dtOrderEMP_CREATE: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object dtOrderDATE_CREATE: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object dtOrderDEP_CUSTOM: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object dtOrderNAME: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object dtOrderID_STATE: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object dtOrderTYPE_ORDER: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object dtOrderEMP_STATE: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'IND_ORDER.EMP_STATE'
      Size = 30
    end
    object dtOrderDATE_STATE: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'IND_ORDER.DATE_STATE'
    end
    object dtOrderNUMBER_DOC: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'IND_ORDER.NUMBER_DOC'
    end
    object dtOrderDATE_DOC: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'IND_ORDER.DATE_DOC'
      Size = 60
    end
    object dtOrderCOMMENT: TStringField
      FieldName = 'COMMENT'
      Origin = 'IND_ORDER.COMMENT'
      Size = 10000
    end
    object dtOrderPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'IND_ORDER.PRICE'
    end
    object dtOrderDocumentName: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object dtOrderDEL: TIntegerField
      FieldName = 'DEL'
      Origin = 'IND_ORDER.DEL'
    end
    object dtOrderART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Origin = 'IND_ORDER.ART_FACTORY'
      Size = 30
    end
    object dtOrderUID: TIntegerField
      FieldName = 'UID'
      Origin = 'IND_ORDER.UID'
    end
    object dtOrderINS_FACTORY: TStringField
      FieldName = 'INS_FACTORY'
      Origin = 'IND_ORDER.INS_FACTORY'
      Size = 200
    end
    object dtOrderOLD_STATE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_STATE'
      Calculated = True
    end
  end
  object dsOrder: TMyDataSource
    DataSet = dtOrder
    Left = 24
    Top = 160
  end
  object dtHistory: TMyQuery
    SQLRefresh.Strings = (
      'select '
      '  ORDER_HISTORY.ID_ORDER,'
      '  IND_STATE.NAME,  '
      '  ORDER_HISTORY.ID_STATE,'
      '  ORDER_HISTORY.EMP_STATE, '
      '  ORDER_HISTORY.DATE_STATE, '
      '  ORDER_HISTORY.NUMBER_DOC, '
      '  ORDER_HISTORY.DATE_DOC, '
      '  ORDER_HISTORY.COMMENT, '
      '  ORDER_HISTORY.PRICE,'
      '  ORDER_HISTORY.UID'
      'from  '
      '  ORDER_HISTORY, IND_STATE'
      'where   '
      '   (ORDER_HISTORY.ID_STATE = IND_STATE.ID_STATE)'
      'order by '
      '  ORDER_HISTORY.ID_ORDER asc, '
      '  ORDER_HISTORY.DATE_STATE desc;')
    Connection = Connection
    SQL.Strings = (
      'select '
      '  ORDER_HISTORY.ID_ORDER,'
      '  IND_STATE.NAME,  '
      '  ORDER_HISTORY.ID_STATE,'
      '  ORDER_HISTORY.EMP_STATE, '
      '  ORDER_HISTORY.DATE_STATE, '
      '  ORDER_HISTORY.NUMBER_DOC, '
      '  ORDER_HISTORY.DATE_DOC, '
      '  ORDER_HISTORY.COMMENT, '
      '  ORDER_HISTORY.PRICE,'
      '  ORDER_HISTORY.UID'
      'from  '
      '  ORDER_HISTORY, IND_STATE'
      'where   '
      '   (ORDER_HISTORY.ID_STATE = IND_STATE.ID_STATE)'
      'order by '
      '  ORDER_HISTORY.ID_ORDER asc, '
      '  ORDER_HISTORY.DATE_STATE desc;')
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtHistoryNewRecord
    Left = 344
    Top = 80
    object dtHistoryID_ORDER: TIntegerField
      FieldName = 'ID_ORDER'
      Origin = 'ORDER_HISTORY.ID_ORDER'
    end
    object dtHistoryNAME: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 180
    end
    object dtHistoryID_STATE: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'ORDER_HISTORY.ID_STATE'
    end
    object dtHistoryEMP_STATE: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'ORDER_HISTORY.EMP_STATE'
      Size = 120
    end
    object dtHistoryDATE_STATE: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'ORDER_HISTORY.DATE_STATE'
    end
    object dtHistoryNUMBER_DOC: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'ORDER_HISTORY.NUMBER_DOC'
    end
    object dtHistoryDATE_DOC: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'ORDER_HISTORY.DATE_DOC'
      Size = 30
    end
    object dtHistoryCOMMENT: TStringField
      FieldName = 'COMMENT'
      Origin = 'ORDER_HISTORY.COMMENT'
      Size = 600
    end
    object dtHistoryPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'ORDER_HISTORY.PRICE'
    end
    object dtHistoryDocumentName: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object dtHistoryUID: TIntegerField
      FieldName = 'UID'
    end
  end
  object dsHistory: TMyDataSource
    DataSet = dtHistory
    Left = 336
    Top = 128
  end
  object Command: TMyCommand
    Connection = Connection
    Left = 88
    Top = 8
  end
  object dtOrder_in: TMyQuery
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      ''
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.ID_PREVIEW,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.ID_STATE <> 1) and'
      '      (IND_ORDER.DEP_SEND = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      ''
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.ID_PREVIEW,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) and'
      '      (IND_ORDER.ID_STATE <> 1) and'
      '      (IND_ORDER.DEP_SEND = :DEPARTMENT)'
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    BeforeOpen = SetDepParam
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtOrderNewRecord
    Left = 88
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEPARTMENT'
      end>
    object IntegerField1: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object StringField1: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object StringField2: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object StringField3: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object StringField4: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object StringField5: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object StringField6: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object StringField7: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object StringField8: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
      Size = 10
    end
    object StringField9: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object StringField10: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object StringField11: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object IntegerField2: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object IntegerField3: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object StringField12: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'ORDER_HISTORY.EMP_STATE'
      Size = 40
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'ORDER_HISTORY.DATE_STATE'
    end
    object IntegerField4: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'ORDER_HISTORY.NUMBER_DOC'
    end
    object StringField13: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'ORDER_HISTORY.DATE_DOC'
      Size = 10
    end
    object StringField14: TStringField
      FieldName = 'COMMENT'
      Origin = 'ORDER_HISTORY.COMMENT'
      Size = 200
    end
    object FloatField1: TFloatField
      FieldName = 'PRICE'
      Origin = 'ORDER_HISTORY.PRICE'
    end
    object StringField15: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object dtOrder_inID_PREVIEW: TIntegerField
      FieldName = 'ID_PREVIEW'
    end
    object dtOrder_inUID: TIntegerField
      FieldName = 'UID'
    end
    object dtOrder_inART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Size = 30
    end
    object dtOrder_inOLD_STATE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_STATE'
      Calculated = True
    end
  end
  object Sourse_COMP: TDataSource
    DataSet = COMP_NAME_S
    Left = 624
    Top = 120
  end
  object COMP_NAME_S: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      ' d_comp.sname  as name   '
      ' '
      'from '
      ' d_comp '
      ''
      'where  d_comp.name <> '#39'$$$$$'#39' and d_comp.name <> '#39#1053#1045#1058#39
      ';')
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsOrder
    Left = 624
    Top = 64
    WaitEndMasterScroll = True
    dcForceOpen = True
    oFetchAll = True
    object COMP_NAME_SNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
  end
  object dtCenter: TMyQuery
    SQLInsert.Strings = (
      
        'call IND_ORDER_I (:DEP_CUSTOM, :DEP_SEND, 1 , :EMP_CREATE, :EMP_' +
        'STATE );')
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :EMP_CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART, '
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) '
      ''
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;'
      ''
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) '
      ''
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART, '
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY,'
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  IND_ORDER.UID'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL)) and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.TYPE_ORDER = 1) and'
      '      (IND_ORDER.ID_STATE <> 11) '
      ''
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc;')
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtOrderNewRecord
    Left = 80
    Top = 160
    object IntegerField15: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object StringField46: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object StringField47: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object StringField48: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object StringField49: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object StringField50: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object StringField51: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object StringField52: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object StringField53: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
      Size = 10
    end
    object StringField54: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object DateTimeField7: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object StringField55: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object StringField56: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object IntegerField16: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object IntegerField17: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object StringField57: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'ORDER_HISTORY.EMP_STATE'
      Size = 40
    end
    object DateTimeField8: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'ORDER_HISTORY.DATE_STATE'
    end
    object IntegerField18: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'ORDER_HISTORY.NUMBER_DOC'
    end
    object StringField58: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'ORDER_HISTORY.DATE_DOC'
      Size = 10
    end
    object StringField59: TStringField
      FieldName = 'COMMENT'
      Origin = 'ORDER_HISTORY.COMMENT'
      Size = 200
    end
    object FloatField4: TFloatField
      FieldName = 'PRICE'
      Origin = 'ORDER_HISTORY.PRICE'
    end
    object StringField60: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object IntegerField19: TIntegerField
      FieldName = 'DEL'
    end
    object dtCenterART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Size = 30
    end
    object dtCenterINS_FACTORY: TStringField
      FieldName = 'INS_FACTORY'
      Size = 200
    end
    object dtCenterOLD_STATE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_STATE'
      Calculated = True
    end
  end
  object dtOrder_factory: TMyQuery
    SQLInsert.Strings = (
      
        'call IND_ORDER_I (:DEP_CUSTOM, :DEP_SEND, 3 , :EMP_CREATE, :EMP_' +
        'STATE );'
      ''
      ''
      'call ORDER_DOP_INFO_I();')
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);'
      ''
      ''
      
        'call ORDER_DOP_INFO_U ( :PREPAY, :TIME_ORDER, :INFO_ORDER, :COST' +
        ', :ID_ORDER);')
    SQLRefresh.Strings = (
      'call FACTORY (:DEPARTMENT);')
    Connection = Connection
    SQL.Strings = (
      'call FACTORY (:DEPARTMENT);')
    BeforeOpen = SetDepParam
    BeforeClose = PostBeforeClose
    BeforePost = dtOrderBeforePost
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtOrderNewRecord
    Left = 168
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEPARTMENT'
      end>
    object IntegerField10: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object StringField31: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object StringField32: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object StringField33: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object StringField34: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object StringField35: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object StringField36: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object StringField37: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object StringField38: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
    end
    object StringField39: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object DateTimeField5: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object StringField40: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object StringField41: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object IntegerField11: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_STATE.ID_STATE'
    end
    object IntegerField12: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object StringField42: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'IND_ORDER.EMP_STATE'
      Size = 30
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'IND_ORDER.DATE_STATE'
    end
    object IntegerField13: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'IND_ORDER.NUMBER_DOC'
    end
    object StringField43: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'IND_ORDER.DATE_DOC'
      Size = 60
    end
    object StringField44: TStringField
      FieldName = 'COMMENT'
      Origin = 'IND_ORDER.COMMENT'
      Size = 10000
    end
    object FloatField3: TFloatField
      FieldName = 'PRICE'
      Origin = 'IND_ORDER.PRICE'
    end
    object StringField45: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object IntegerField14: TIntegerField
      FieldName = 'DEL'
      Origin = 'IND_ORDER.DEL'
    end
    object dtOrder_factoryINFO_ORDER: TStringField
      FieldName = 'INFO_ORDER'
      Origin = 'ORDER_DOP_INFO.INFO_ORDER'
      Size = 200
    end
    object dtOrder_factoryPREPAY: TFloatField
      Alignment = taLeftJustify
      FieldName = 'PREPAY'
      Origin = 'ORDER_DOP_INFO.PREPAY'
    end
    object dtOrder_factoryTIME_ORDER: TStringField
      FieldName = 'TIME_ORDER'
      Origin = 'ORDER_DOP_INFO.TIME_ORDER'
      Size = 100
    end
    object dtOrder_factoryID_PREVIEW: TIntegerField
      FieldName = 'ID_PREVIEW'
      Origin = 'IND_ORDER.ID_PREVIEW'
    end
    object dtOrder_factoryUID: TIntegerField
      FieldName = 'UID'
      Origin = 'IND_ORDER.UID'
    end
    object dtOrder_factoryART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Origin = 'IND_ORDER.ART_FACTORY'
      Size = 30
    end
    object dtOrder_factoryINS_FACTORY: TStringField
      FieldName = 'INS_FACTORY'
      Origin = 'IND_ORDER.INS_FACTORY'
      Size = 200
    end
    object dtOrder_factoryCOST: TStringField
      FieldName = 'COST'
      Origin = 'ORDER_DOP_INFO.COST'
    end
    object dtOrder_factoryREMARK: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'REMARK'
      Calculated = True
    end
    object dtOrder_factoryOLD_STATE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_STATE'
      Calculated = True
    end
  end
  object dtOrder_off: TMyQuery
    SQLInsert.Strings = (
      'call IND_ORDER_I (:DEP_CUSTOM, :EMP_CREATE, :EMP_STATE );')
    SQLDelete.Strings = (
      'call IND_ORDER_D ( :ID_ORDER);')
    SQLUpdate.Strings = (
      
        'call IND_ORDER_U ( :FIO, :PHONE, :DEP_SEND, :ART,  :COMP, :GOOD,' +
        '  :MAT, :SZ, :ID_STATE, :EMP_STATE, :COMMENT, :ID_ORDER, :DATE_D' +
        'OC, :NUMBER_DOC, :PRICE, :UID, :ART_FACTORY, :INS_FACTORY, :EMP_' +
        'CREATE);')
    SQLRefresh.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  CAST(null AS CHAR(20)) COST,'
      '  CAST(null AS CHAR(200)) INFO_ORDER,'
      '  CAST(null AS CHAR(100)) TIME_ORDER, '
      '  CAST(0 AS DECIMAL(10,3)) PREPAY,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      
        '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL))and (IND' +
        '_ORDER.DATE_STATE>=:BD) and (IND_ORDER.DATE_STATE<=:ED)  and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.ID_STATE = 11) '
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc')
    Connection = Connection
    SQL.Strings = (
      'select  '
      '  IND_ORDER.ID_ORDER, '
      '  IND_ORDER.FIO, '
      '  IND_ORDER.PHONE,'
      '  IND_ORDER.DEP_SEND, '
      '  IND_ORDER.ART, '
      '  IND_ORDER.COMP, '
      '  IND_ORDER.GOOD, '
      '  IND_ORDER.MAT, '
      '  IND_ORDER.SZ, '
      '  IND_ORDER.EMP_CREATE, '
      '  IND_ORDER.DATE_CREATE, '
      '  IND_ORDER.DEP_CUSTOM,'
      '  IND_STATE.NAME,'
      '  IND_ORDER.ID_STATE, '
      '  IND_ORDER.TYPE_ORDER,'
      '  IND_ORDER.DEL,'
      '  IND_ORDER.EMP_STATE, '
      '  IND_ORDER.DATE_STATE, '
      '  IND_ORDER.NUMBER_DOC,'
      '  IND_ORDER.DATE_DOC, '
      '  IND_ORDER.COMMENT, '
      '  IND_ORDER.PRICE,'
      '  CAST(null AS CHAR(20)) COST,'
      '  CAST(null AS CHAR(200)) INFO_ORDER,'
      '  CAST(null AS CHAR(100)) TIME_ORDER, '
      '  CAST(0 AS DECIMAL(10,3)) PREPAY,'
      '  IND_ORDER.ART_FACTORY,'
      '  IND_ORDER.INS_FACTORY'
      'from '
      '   IND_ORDER, IND_STATE'
      'where'
      '    ( '
      
        '      ((IND_ORDER.DEL  <> 1) or (IND_ORDER.DEL is NULL))and (IND' +
        '_ORDER.DATE_STATE>=:BD) and (IND_ORDER.DATE_STATE<=:ED)  and '
      '      (IND_ORDER.ID_STATE = IND_STATE.ID_STATE) and'
      '      (IND_ORDER.ID_STATE = 11) '
      '     )'
      'order by '
      '  IND_ORDER.ID_ORDER desc')
    BeforeOpen = dtOrder_offBeforeOpen
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    Left = 168
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BD'
      end
      item
        DataType = ftUnknown
        Name = 'ED'
      end>
    object IntegerField5: TIntegerField
      AutoGenerateValue = arAutoInc
      FieldName = 'ID_ORDER'
      Origin = 'IND_ORDER.ID_ORDER'
    end
    object StringField16: TStringField
      FieldName = 'FIO'
      Origin = 'IND_ORDER.FIO'
      Size = 60
    end
    object StringField17: TStringField
      FieldName = 'PHONE'
      Origin = 'IND_ORDER.PHONE'
      Size = 30
    end
    object StringField18: TStringField
      FieldName = 'DEP_SEND'
      Origin = 'IND_ORDER.DEP_SEND'
      Size = 30
    end
    object StringField19: TStringField
      FieldName = 'ART'
      Origin = 'IND_ORDER.ART'
      Size = 60
    end
    object StringField20: TStringField
      FieldName = 'COMP'
      Origin = 'IND_ORDER.COMP'
      Size = 60
    end
    object StringField21: TStringField
      FieldName = 'GOOD'
      Origin = 'IND_ORDER.GOOD'
      Size = 60
    end
    object StringField22: TStringField
      FieldName = 'MAT'
      Origin = 'IND_ORDER.MAT'
      Size = 30
    end
    object StringField23: TStringField
      FieldName = 'SZ'
      Origin = 'IND_ORDER.sz'
    end
    object StringField24: TStringField
      FieldName = 'EMP_CREATE'
      Origin = 'IND_ORDER.EMP_CREATE'
      Size = 40
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'DATE_CREATE'
      Origin = 'IND_ORDER.DATE_CREATE'
    end
    object StringField25: TStringField
      FieldName = 'DEP_CUSTOM'
      Origin = 'IND_ORDER.DEP_CUSTOM'
      Size = 30
    end
    object StringField26: TStringField
      FieldName = 'NAME'
      Origin = 'IND_STATE.NAME'
      Size = 60
    end
    object IntegerField6: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'IND_ORDER.ID_STATE'
    end
    object IntegerField7: TIntegerField
      FieldName = 'TYPE_ORDER'
      Origin = 'IND_ORDER.TYPE_ORDER'
    end
    object StringField27: TStringField
      FieldName = 'EMP_STATE'
      Origin = 'IND_ORDER.EMP_STATE'
      Size = 30
    end
    object DateTimeField4: TDateTimeField
      FieldName = 'DATE_STATE'
      Origin = 'IND_ORDER.DATE_STATE'
    end
    object IntegerField8: TIntegerField
      FieldName = 'NUMBER_DOC'
      Origin = 'IND_ORDER.NUMBER_DOC'
    end
    object StringField28: TStringField
      FieldName = 'DATE_DOC'
      Origin = 'IND_ORDER.DATE_DOC'
      Size = 60
    end
    object StringField29: TStringField
      FieldName = 'COMMENT'
      Origin = 'IND_ORDER.COMMENT'
      Size = 10000
    end
    object FloatField2: TFloatField
      FieldName = 'PRICE'
      Origin = 'IND_ORDER.PRICE'
    end
    object StringField30: TStringField
      FieldKind = fkCalculated
      FieldName = 'DocumentName'
      Size = 256
      Calculated = True
    end
    object IntegerField9: TIntegerField
      FieldName = 'DEL'
      Origin = 'IND_ORDER.DEL'
    end
    object dtOrder_offINFO_ORDER: TStringField
      FieldName = 'INFO_ORDER'
      Origin = 'ORDER_DOP_INFO.INFO_ORDER'
      Size = 200
    end
    object dtOrder_offPREPAY: TFloatField
      FieldName = 'PREPAY'
      Origin = 'ORDER_DOP_INFO.PREPAY'
    end
    object dtOrder_offTIME_ORDER: TStringField
      FieldName = 'TIME_ORDER'
      Origin = 'ORDER_DOP_INFO.TIME_ORDER'
      Size = 100
    end
    object dtOrder_offART_FACTORY: TStringField
      FieldName = 'ART_FACTORY'
      Origin = 'IND_ORDER.ART_FACTORY'
      Size = 30
    end
    object dtOrder_offINS_FACTORY: TStringField
      FieldName = 'INS_FACTORY'
      Origin = 'IND_ORDER.INS_FACTORY'
      Size = 200
    end
    object dtOrder_offCOST: TStringField
      FieldName = 'COST'
      Origin = 'ORDER_DOP_INFO.COST'
    end
    object dtOrder_offOLD_STATE: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_STATE'
      Calculated = True
    end
  end
  object COUNT_PREWIEW: TMyQuery
    SQLRefresh.Strings = (
      'call COUNT_PREWIEW (:DEP);')
    Connection = Connection
    SQL.Strings = (
      'call COUNT_PREWIEW (:DEP);')
    Left = 128
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'DEP'
      end>
    object COUNT_PREWIEWCOUNT_PREWIEW: TLargeintField
      FieldName = 'COUNT_PREWIEW'
    end
    object COUNT_PREWIEWDEP_SEND: TStringField
      FieldName = 'DEP_SEND'
      Size = 30
    end
    object COUNT_PREWIEWDEP_CUSTOM: TStringField
      FieldName = 'DEP_CUSTOM'
      Size = 30
    end
  end
  object GOOD_NAME_S: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      '   '
      ' d_good.name as good '
      ''
      ''
      'from '
      ' D_good'
      'where  d_good.name <> '#39'$$$$$'#39
      ' ;'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsOrder
    Left = 248
    Top = 200
    WaitEndMasterScroll = True
    dcForceOpen = True
    object FIBStringField3: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
  end
  object Sourse_GOOD: TDataSource
    DataSet = GOOD_NAME_S
    Left = 248
    Top = 256
  end
  object MAT_NAME_S: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      ''
      ' d_mat.name as MAT '
      ''
      'from '
      ' '
      ' d_mat'
      'where '
      'd_mat.name <> '#39'$$$$$'#39' and d_mat.name <> '#39#39' ;'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsOrder
    Left = 336
    Top = 200
    WaitEndMasterScroll = True
    dcForceOpen = True
    object MAT_NAME_SMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
  end
  object Sourse_MAT: TDataSource
    DataSet = MAT_NAME_S
    Left = 336
    Top = 256
  end
  object SELL_DataSet: TpFIBDataSet
    SelectSQL.Strings = (
      'select sell.sellid,   sell.rn, sell.bd from sell'
      'WHERE  (sell.bd >'#39'01.01.2016'#39')  ORDER BY sell.bd DESC;')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 360
  end
  object DataSourse_List: TDataSource
    DataSet = SELL_DataSet
    Left = 432
    Top = 8
  end
  object DataSet_ART_SELL: TpFIBDataSet
    SelectSQL.Strings = (
      ''
      
        'SELECT sl.SELLITEMID, sl.SELLID, sl.ADATE, sl.DEP, sl.CLIENTID, ' +
        'sl.PRICE, sl.PRICE0, sl.ART2ID, sl.SITEMID, sl.EMPID, sl.W, sl.S' +
        'Z, sl.UID, sl.MEMO, sl.FULLART, sl.ART2, sl.UNITID, sl.CHECKNO, ' +
        'sl.NAME, sl.MAT, sl.DISCOUNT, sl.EMP, sl.DISCMEMO, sl.RET, sl.SD' +
        'ATE, sl.SPRICE, sl.SUP, sl.SINFOID, sl.RETSITEMID, sl.SN, sl.RQ,' +
        ' sl.RW, sl.USERID, sl.ART, sl.D_RETID, sl.RETDATE, sl.RETSELLITE' +
        'MID, sl.RETDONE, sl.SQ, sl.SW, sl.SQ_DART, sl.SW_DART, sl.RETPRI' +
        'CE, sl.OPT, sl.SQR, sl.SWR, sl.CLIENTNAME, sl.F, sl.NODCARD, sl.' +
        'CODE, sl.INSID, sl.GOODID, sl.MATID, sl.retclient, sl.retaddress' +
        ', sl.retcause, sl.CASHIERID, sl.CASHIERNAME, sl.Artid, sl.Appl_Q' +
        ', sl.SQAPPL, sl.actdiscount, sl.Card, sl.Cost, sl.Cost0, sl.difq' +
        ', sl.costcard, sl.TRANSACT_ID'
      ''
      'FROM SELLITEM_S(?SELLID, ?USERID) sl     where sl.art= :art;')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 448
    Top = 72
  end
  object DataSource_ART_SELL: TDataSource
    DataSet = DataSet_ART_SELL
    Left = 448
    Top = 128
  end
  object dtOld_state: TMyQuery
    SQLRefresh.Strings = (
      'select  '
      ''
      '                 IND_ORDER.ID_ORDER, '
      ''
      '        max(`ORDER_HISTORY`.`ID_STATE`) as max_state '
      ''
      ' from '
      ''
      '        IND_ORDER, IND_STATE, `ORDER_HISTORY`'
      ''
      ' where'
      '         (( (IND_ORDER.DEL  <> 1)'
      '         or (IND_ORDER.DEL is NULL))   '
      '         and (`ORDER_HISTORY`.`ID_ORDER`=IND_ORDER.`ID_ORDER`))'
      '         group by IND_ORDER.ID_ORDER ;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      ''
      '                 IND_ORDER.ID_ORDER, '
      ''
      '        max(`ORDER_HISTORY`.`ID_STATE`) as max_state '
      ''
      ' from '
      ''
      '        IND_ORDER, IND_STATE, `ORDER_HISTORY`'
      ''
      ' where'
      '         (( (IND_ORDER.DEL  <> 1)'
      '         or (IND_ORDER.DEL is NULL))   '
      '         and (`ORDER_HISTORY`.`ID_ORDER`=IND_ORDER.`ID_ORDER`))'
      
        '         group by IND_ORDER.ID_ORDER order by IND_ORDER.ID_ORDER' +
        ' desc;')
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtHistoryNewRecord
    Left = 656
    Top = 240
    object dtOld_stateID_ORDER: TIntegerField
      FieldName = 'ID_ORDER'
    end
    object dtOld_statemax_state: TIntegerField
      FieldName = 'max_state'
    end
  end
  object dsOld_state: TMyDataSource
    DataSet = dtOld_state
    Left = 656
    Top = 296
  end
  object dtState: TMyQuery
    SQLRefresh.Strings = (
      'select  '
      ''
      '                 IND_ORDER.ID_ORDER, '
      ''
      '        max(`ORDER_HISTORY`.`ID_STATE`) as max_state '
      ''
      ' from '
      ''
      '        IND_ORDER, IND_STATE, `ORDER_HISTORY`'
      ''
      ' where'
      '         (( (IND_ORDER.DEL  <> 1)'
      '         or (IND_ORDER.DEL is NULL))   '
      '         and (`ORDER_HISTORY`.`ID_ORDER`=IND_ORDER.`ID_ORDER`))'
      '         group by IND_ORDER.ID_ORDER ;')
    Connection = Connection
    SQL.Strings = (
      'select  '
      ''
      '        IND_STATE.NAME'
      ''
      ' from '
      ''
      '         IND_STATE'
      ''
      ' where'
      '         ( IND_STATE.id_state = :id_state ) ;')
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtHistoryNewRecord
    Left = 576
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'id_state'
      end>
    object dtStateNAME: TStringField
      FieldName = 'NAME'
      Size = 60
    end
  end
  object dsState: TMyDataSource
    DataSet = dtState
    Left = 576
    Top = 296
  end
  object DataSet_ART_SINV: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT d_art.Art, sitem.uid'
      ''
      'FROM d_art,SEL, SITEM'
      
        'WHERE    (SEL.d_artid = d_art.d_artid)    and    (SEL.sinvid =:S' +
        'INVID)'
      'and    (d_art.Art=:Art)  and SITEM.selid = sel.selid'
      ';')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 280
    Top = 368
    object DataSet_ART_SINVART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object DataSet_ART_SINVUID: TFIBIntegerField
      FieldName = 'UID'
    end
  end
  object DataSource_ART_SINV: TDataSource
    DataSet = DataSet_ART_SINV
    Left = 392
    Top = 368
  end
  object SINV_DataSet: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SINV.SINVID, SINV.SDATE,'
      'SINV.SN,D_DEP.SNAME'
      ''
      ''
      ''
      'FROM Sinv,D_DEP '
      
        'WHERE ((Sinv.DEPid =  D_DEP.d_DEPid) and (SINV.SDATE >'#39'01.01.201' +
        '6'#39') and (sinv.itype='#39'2'#39'))'
      'ORDER BY SINV.SDATE DESC;')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 72
    Top = 368
    object SINV_DataSetSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object SINV_DataSetSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object SINV_DataSetSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object SINV_DataSetSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object DataSourse_List1: TDataSource
    DataSet = SINV_DataSet
    Left = 168
    Top = 368
  end
  object Comment: TMyQuery
    SQLRefresh.Strings = (
      'select '
      ''
      '  ORDER_HISTORY.COMMENT '
      ''
      'from  '
      '  ORDER_HISTORY'
      'where   '
      ' ORDER_HISTORY.id_order = :param;')
    Connection = Connection
    SQL.Strings = (
      'select '
      ''
      '  ORDER_HISTORY.COMMENT '
      ''
      'from  '
      '  ORDER_HISTORY'
      'where   '
      ' ORDER_HISTORY.id_order = :param;')
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcDocNameField
    OnNewRecord = dtHistoryNewRecord
    Left = 520
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'param'
      end>
    object CommentCOMMENT: TStringField
      FieldName = 'COMMENT'
      Size = 200
    end
  end
  object ImageCalendar: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 524968
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000043433E616B66
          5D8F6A665C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A65
          5C8F69655B8F69655B8F69655B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F69645B8F6B665D8F69645B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F6B665D8F6B665D8F6A655C8F6A655C8F6A665D8F43423F5F969084C2F3F1
          EFFFEEECE8FFEDEBE5FFECE7E3FFEAE7E1FFE9E5DEFFE7E3DCFFE6E1D9FFE5E0
          D7FFE4DED5FFE3DCD4FFE2DCD1FFE1DAD0FFE0D9CEFFDFD7CCFFDFD8CDFFE0D9
          CEFFE1DAD0FFE4DDD3FFE1DACFFFE2DCD1FFE1DBD2FFE3DCD4FFE7DFD8FFE8E1
          DAFFEDE4E0FFEAE7E1FFECE9E3FFECEAE6FFEEECEAFF97938ABD8F897DBDF1F1
          F1FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF4F0F2FF928C84B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF8F8F8FF918D83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF6F6
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF918D83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF8F8
          F9FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF5F5F5FF908B83B78F8A7EBDF1F1
          F1FFF5F4F4FFF5F4F4FFF5F5F4FFF5F5F4FFF5F5F5FFF5F5F5FFF5F5F5FFF6F5
          F5FFF6F5F5FFF6F6F5FFF7F6F6FFF7F6F6FFF7F7F6FFF7F7F6FFF7F7F7FFF7F7
          F7FFF7F7F7FFF8F7F7FFF8F7F7FFF8F8F7FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF9F8F8FFF9F8F8FFF9F9F8FFFAF9F9FFF1F1F1FF908C83B7908A7EBDE4E4
          E4FFE0DFDFFFE1E0E0FFE2E1E1FFE2E1E1FFE2E2E2FFE3E3E3FFE3E3E3FFE4E4
          E4FFE5E5E4FFE5E5E5FFE6E6E6FFE6E6E6FFE7E7E7FFE8E8E8FFE8E8E8FFE8E8
          E8FFE9E9E9FFE9E9E9FFEAE9E9FFE9EAEAFFEAEBEBFFEBEBEBFFEBEBEBFFEBEB
          EAFFECEBEBFFECECECFFECECECFFEEEEEDFFE9E9E9FF908C83B7918B7FBDDEDA
          D4FFD3CDC4FFD2CCC3FFD2CBC3FFD1CAC2FFD1CBC0FFD0CABFFFD0C9BFFFCFC8
          BDFFCFC7BDFFCEC7BCFFCDC5BBFFCDC5BAFFCDC6B9FFCCC4B9FFCCC4B9FFCDC5
          BAFFCDC5BAFFCDC5BBFFCEC7BCFFCFC7BCFFCFC8BDFFD0C8BFFFD0C9BFFFD2CC
          C4FFD2CCC3FFD2CDC5FFD2CCC4FFD3CEC6FFDBD7D0FF918C83B7948F82C2F1EA
          E0FFECE3D8FFEAE0D4FFE8DED0FFE5DCCDFFE5DBCAFFE1D7C8FFE0D6C5FFDFD3
          C2FFDDD1BEFFDBCFBCFFD9CDBAFFD8CBB8FFD7CAB5FFD5C7B4FFD5C7B4FFD7CA
          B5FFD8CBB7FFD9CCB9FFDACEBBFFDCD0BDFFDDD2C1FFDFD4C3FFE0D5C5FFEFEA
          E1FFE4DDCEFFEEE8DFFFE6DCCFFFECE6DDFFE1D8C8FF959086BC4948436A726D
          629C716C619B716C609B716C609B706C609B706B609B706C5F9B706C5F9B706A
          5F9B706A5F9B706A5F9B706B5F9B706B5F9B706B5F9B706A5E9B706A5E9B706B
          5F9B706B5F9B706B5F9B706B5F9B706A5F9B706A5F9B706A5F9B706C5F9B706C
          609B706C609B716C609B716C609B716C609B726E619C49484368000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000043433E616B66
          5D8F6A665C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A65
          5C8F69655B8F69655B8F69655B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F69645B8F6B665D8F69645B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F6B665D8F6B665D8F6A655C8F6A655C8F6A665D8F43423F5F969084C2F3F1
          EFFFEEECE8FFEDEBE5FFECE7E3FFEAE7E1FFE9E5DEFFE7E3DCFFE6E1D9FFE5E0
          D7FFE4DED5FFE3DCD4FFE2DCD1FFE1DAD0FFE0D9CEFFDFD7CCFFDFD8CDFFE0D9
          CEFFE1DAD0FFE4DDD3FFE1DACFFFE2DCD1FFE1DBD2FFE3DCD4FFE7DFD8FFE8E1
          DAFFEDE4E0FFEAE7E1FFECE9E3FFECEAE6FFEEECEAFF97938ABD8F897DBDF1F1
          F1FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF4F0F2FF928C84B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF8F8F8FF918D83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF6F6
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF918D83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF8F8
          F9FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF5F5F5FF908B83B78F8A7EBDF1F1
          F1FFF5F4F4FFF5F4F4FFF5F5F4FFF5F5F4FFF5F5F5FFF5F5F5FFF5F5F5FFF6F5
          F5FFF6F5F5FFF6F6F5FFF7F6F6FFF7F6F6FFF7F7F6FFF7F7F6FFF7F7F7FFF7F7
          F7FFF7F7F7FFF8F7F7FFF8F7F7FFF8F8F7FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF9F8F8FFF9F8F8FFF9F9F8FFFAF9F9FFF1F1F1FF908C83B7908A7EBDE4E4
          E4FFE0DFDFFFE1E0E0FFE2E1E1FFE2E1E1FFE2E2E2FFE3E3E3FFE3E3E3FFE4E4
          E4FFE5E5E4FFE5E5E5FFE6E6E6FFE6E6E6FFE7E7E7FFE8E8E8FFE8E8E8FFE8E8
          E8FFE9E9E9FFE9E9E9FFEAE9E9FFE9EAEAFFEAEBEBFFEBEBEBFFEBEBEBFFEBEB
          EAFFECEBEBFFECECECFFECECECFFEEEEEDFFE9E9E9FF908C83B7918B7FBDDEDA
          D4FFD3CDC4FFD2CCC3FFD2CBC3FFD1CAC2FFD1CBC0FFD0CABFFFD0C9BFFFCFC8
          BDFFCFC7BDFFCEC7BCFFCDC5BBFFCDC5BAFFCDC6B9FFCCC4B9FFCCC4B9FFCDC5
          BAFFCDC5BAFFCDC5BBFFCEC7BCFFCFC7BCFFCFC8BDFFD0C8BFFFD0C9BFFFD2CC
          C4FFD2CCC3FFD2CDC5FFD2CCC4FFD3CEC6FFDBD7D0FF918C83B7948F82C2F1EA
          E0FFECE3D8FFEAE0D4FFE8DED0FFE5DCCDFFE5DBCAFFE1D7C8FFE0D6C5FFDFD3
          C2FFDDD1BEFFDBCFBCFFD9CDBAFFD8CBB8FFD7CAB5FFD5C7B4FFD5C7B4FFD7CA
          B5FFD8CBB7FFD9CCB9FFDACEBBFFDCD0BDFFDDD2C1FFDFD4C3FFE0D5C5FFEFEA
          E1FFE4DDCEFFEEE8DFFFE6DCCFFFECE6DDFFE1D8C8FF959086BC4948436A726D
          629C716C619B716C609B716C609B706C609B706B609B706C5F9B706C5F9B706A
          5F9B706A5F9B706A5F9B706B5F9B706B5F9B706B5F9B706A5E9B706A5E9B706B
          5F9B706B5F9B706B5F9B706B5F9B706A5F9B706A5F9B706A5F9B706C5F9B706C
          609B706C609B716C609B716C609B716C609B726E619C49484368000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000043433E616B66
          5D8F6A665C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A65
          5C8F69655B8F69655B8F69655B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F69645B8F6B665D8F69645B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F6B665D8F6B665D8F6A655C8F6A655C8F6A665D8F43423F5F969084C2F3F1
          EFFFEEECE8FFEDEBE5FFECE7E3FFEAE7E1FFE9E5DEFFE7E3DCFFE6E1D9FFE5E0
          D7FFE4DED5FFE3DCD4FFE2DCD1FFE1DAD0FFE0D9CEFFDFD7CCFFDFD8CDFFE0D9
          CEFFE1DAD0FFE4DDD3FFE1DACFFFE2DCD1FFE1DBD2FFE3DCD4FFE7DFD8FFE8E1
          DAFFEDE4E0FFEAE7E1FFECE9E3FFECEAE6FFEEECEAFF97938ABD8F897DBDF1F1
          F1FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF4F0F2FF928C84B78F897DBDF5F5
          F6FFF9F9F8FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFFF9F9F8FFF8F8F8FF918D83B78F897DBDF5F5
          F6FFF9F9F8FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCC
          F6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCC
          F6FF5BCCF6FF5BCCF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3
          FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3
          FAFF64D3FAFF64D3FAFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF6F6
          F7FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DD
          F6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DD
          F6FF76DDF6FF76DDF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFFF9F9F8FFF4F4F4FF918D83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCC
          F6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCC
          F6FF5BCCF6FF5BCCF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3
          FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3
          FAFF64D3FAFF64D3FAFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DD
          F6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DD
          F6FF76DDF6FF76DDF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCC
          F6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCC
          F6FF5DAFFBFF5BCCF6FF5BCCF6FF5BCCF6FF5BCCF6FF5DAFFBFF5BCCF6FF5BCC
          F6FF5BCCF6FF5BCCF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3
          FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3
          FAFF5DAFFBFF64D3FAFF64D3FAFF64D3FAFF64D3FAFF5DAFFBFF64D3FAFF64D3
          FAFF64D3FAFF64D3FAFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DF
          FEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6
          FCFF5DAFFBFF68DFFEFF63D6FCFF63D6FCFF63D6FCFF5DAFFBFF68DFFEFF63D6
          FCFF63D6FCFF63D6FCFF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DD
          F6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DD
          F6FF5DAFFBFF76DDF6FF76DDF6FF76DDF6FF76DDF6FF5DAFFBFF76DDF6FF76DD
          F6FF76DDF6FF76DDF6FF5DAFFBFFF9F9F8FFF4F4F4FF908B83B78F897DBDF8F8
          F9FFF9F9F8FF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAFFBFF5DAF
          FBFF5DAFFBFF5DAFFBFF5DAFFBFFF9F9F8FFF5F5F5FF908B83B78F8A7EBDF1F1
          F1FFF5F4F4FFF5F4F4FFF5F5F4FFF5F5F4FFF5F5F5FFF5F5F5FFF5F5F5FFF6F5
          F5FFF6F5F5FFF6F6F5FFF7F6F6FFF7F6F6FFF7F7F6FFF7F7F6FFF7F7F7FFF7F7
          F7FFF7F7F7FFF8F7F7FFF8F7F7FFF8F8F7FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF9F8F8FFF9F8F8FFF9F9F8FFFAF9F9FFF1F1F1FF908C83B7908A7EBDE4E4
          E4FFE0DFDFFFE1E0E0FFE2E1E1FFE2E1E1FFE2E2E2FFE3E3E3FFE3E3E3FFE4E4
          E4FFE5E5E4FFE5E5E5FFE6E6E6FFE6E6E6FFE7E7E7FFE8E8E8FFE8E8E8FFE8E8
          E8FFE9E9E9FFE9E9E9FFEAE9E9FFE9EAEAFFEAEBEBFFEBEBEBFFEBEBEBFFEBEB
          EAFFECEBEBFFECECECFFECECECFFEEEEEDFFE9E9E9FF908C83B7918B7FBDDEDA
          D4FFD3CDC4FFD2CCC3FFD2CBC3FFD1CAC2FFD1CBC0FFD0CABFFFD0C9BFFFCFC8
          BDFFCFC7BDFFCEC7BCFFCDC5BBFFCDC5BAFFCDC6B9FFCCC4B9FFCCC4B9FFCDC5
          BAFFCDC5BAFFCDC5BBFFCEC7BCFFCFC7BCFFCFC8BDFFD0C8BFFFD0C9BFFFD2CC
          C4FFD2CCC3FFD2CDC5FFD2CCC4FFD3CEC6FFDBD7D0FF918C83B7948F82C2F1EA
          E0FFECE3D8FFEAE0D4FFE8DED0FFE5DCCDFFE5DBCAFFE1D7C8FFE0D6C5FFDFD3
          C2FFDDD1BEFFDBCFBCFFD9CDBAFFD8CBB8FFD7CAB5FFD5C7B4FFD5C7B4FFD7CA
          B5FFD8CBB7FFD9CCB9FFDACEBBFFDCD0BDFFDDD2C1FFDFD4C3FFE0D5C5FFEFEA
          E1FFE4DDCEFFEEE8DFFFE6DCCFFFECE6DDFFE1D8C8FF959086BC4948436A726D
          629C716C619B716C609B716C609B706C609B706B609B706C5F9B706C5F9B706A
          5F9B706A5F9B706A5F9B706B5F9B706B5F9B706B5F9B706A5E9B706A5E9B706B
          5F9B706B5F9B706B5F9B706B5F9B706A5F9B706A5F9B706A5F9B706C5F9B706C
          609B706C609B716C609B716C609B716C609B726E619C49484368000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000043433E616B66
          5D8F6A665C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A655C8F6A65
          5C8F69655B8F69655B8F69655B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F69645B8F6B665D8F69645B8F69645B8F69645B8F69645B8F69645B8F6964
          5B8F6B665D8F6B665D8F6A655C8F6A655C8F6A665D8F43423F5F969084C2F3F1
          EFFFEEECE8FFEDEBE5FFECE7E3FFEAE7E1FFE9E5DEFFE7E3DCFFE6E1D9FFE5E0
          D7FFE4DED5FFE3DCD4FFE2DCD1FFE1DAD0FFE0D9CEFFDFD7CCFFDFD8CDFFE0D9
          CEFFE1DAD0FFE4DDD3FFE1DACFFFE2DCD1FFE1DBD2FFE3DCD4FFE7DFD8FFE8E1
          DAFFEDE4E0FFEAE7E1FFECE9E3FFECEAE6FFEEECEAFF97938ABD8F897DBDF1F1
          F1FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF8F8F8FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7F7FFF7F7
          F7FFF7F7F7FFF7F7F7FFF7F7F7FFF8F8F8FFF4F0F2FF928C84B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF8F8F8FF918D83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF5F5
          F6FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF6F6
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F7FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF918D83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF7F7
          F8FFF9F9F8FFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFD6D4D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6D4D4FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD6D4D4FFF9F9F8FFF4F4F4FF908B83B78F897DBDF8F8
          F9FFF9F9F8FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4D4FFD6D4
          D4FFD6D4D4FFD6D4D4FFD6D4D4FFF9F9F8FFF5F5F5FF908B83B78F8A7EBDF1F1
          F1FFF5F4F4FFF5F4F4FFF5F5F4FFF5F5F4FFF5F5F5FFF5F5F5FFF5F5F5FFF6F5
          F5FFF6F5F5FFF6F6F5FFF7F6F6FFF7F6F6FFF7F7F6FFF7F7F6FFF7F7F7FFF7F7
          F7FFF7F7F7FFF8F7F7FFF8F7F7FFF8F8F7FFF8F8F8FFF8F8F8FFF8F8F8FFF8F8
          F8FFF9F8F8FFF9F8F8FFF9F9F8FFFAF9F9FFF1F1F1FF908C83B7908A7EBDE4E4
          E4FFE0DFDFFFE1E0E0FFE2E1E1FFE2E1E1FFE2E2E2FFE3E3E3FFE3E3E3FFE4E4
          E4FFE5E5E4FFE5E5E5FFE6E6E6FFE6E6E6FFE7E7E7FFE8E8E8FFE8E8E8FFE8E8
          E8FFE9E9E9FFE9E9E9FFEAE9E9FFE9EAEAFFEAEBEBFFEBEBEBFFEBEBEBFFEBEB
          EAFFECEBEBFFECECECFFECECECFFEEEEEDFFE9E9E9FF908C83B7918B7FBDDEDA
          D4FFD3CDC4FFD2CCC3FFD2CBC3FFD1CAC2FFD1CBC0FFD0CABFFFD0C9BFFFCFC8
          BDFFCFC7BDFFCEC7BCFFCDC5BBFFCDC5BAFFCDC6B9FFCCC4B9FFCCC4B9FFCDC5
          BAFFCDC5BAFFCDC5BBFFCEC7BCFFCFC7BCFFCFC8BDFFD0C8BFFFD0C9BFFFD2CC
          C4FFD2CCC3FFD2CDC5FFD2CCC4FFD3CEC6FFDBD7D0FF918C83B7948F82C2F1EA
          E0FFECE3D8FFEAE0D4FFE8DED0FFE5DCCDFFE5DBCAFFE1D7C8FFE0D6C5FFDFD3
          C2FFDDD1BEFFDBCFBCFFD9CDBAFFD8CBB8FFD7CAB5FFD5C7B4FFD5C7B4FFD7CA
          B5FFD8CBB7FFD9CCB9FFDACEBBFFDCD0BDFFDDD2C1FFDFD4C3FFE0D5C5FFEFEA
          E1FFE4DDCEFFEEE8DFFFE6DCCFFFECE6DDFFE1D8C8FF959086BC4948436A726D
          629C716C619B716C609B716C609B706C609B706B609B706C5F9B706C5F9B706A
          5F9B706A5F9B706A5F9B706B5F9B706B5F9B706B5F9B706A5E9B706A5E9B706B
          5F9B706B5F9B706B5F9B706B5F9B706A5F9B706A5F9B706A5F9B706C5F9B706C
          609B706C609B716C609B716C609B716C609B726E619C49484368000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000001100000011000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00060000001C00000037000000370000001C0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          001C0000003D008B4BFF008B4BFF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000060000001C0000
          003D008848FF00C382FF00C382FF008848FF0000003D0000001C000000060000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000060000001C0000003D0088
          48FF00BF80FF00DFA0FF00DFA0FF00BF80FF008848FF0000003D0000001C0000
          0006000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000060000001C0000003D008848FF00BD
          7FFF00DC9FFF00D89BFF00D89BFF00DC9FFF00BD7FFF008848FF0000003D0000
          001C000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000060000001C0000003D008948FF00BD81FF00D9
          A0FF00D69CFF00D49AFF00D49AFF00D69CFF00D9A0FF00BD81FF008948FF0000
          003D0000001C0000000600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000060000001C0000003D008948FF00BC81FF00D7A0FF00D4
          9CFF00D29AFF00D29AFF00D29AFF00D29AFF00D49CFF00D7A0FF00BC81FF0089
          48FF0000003D0000001C00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000001C0000003D008948FF00B981FF00D5A0FF00D29CFF00D0
          9AFF00D09AFF00D09AFF00D09AFF00D09AFF00D09AFF00D29CFF00D5A0FF00B9
          81FF008948FF0000003D0000001C000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00060000001C0000003D008948FF00B881FF00D3A0FF00D09CFF00CE9AFF00CE
          9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00D09CFF00D3
          A0FF00B881FF008948FF0000003D0000001C0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          001C0000003D008948FF00B780FF00D1A0FF00CE9CFF00CC9AFF00CC9AFF00CC
          9AFF00CC9AFF00CB9AFF00CB9AFF00CC9AFF00CC9AFF00CC9AFF00CC9AFF00CE
          9CFF00D1A0FF00B780FF008948FF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000110000
          0037008948FF00B680FF00CFA0FF00CB9CFF00CA9AFF00CA9AFF00CA9AFF00CA
          9AFF00CA9BFF21E6B0FF21E6B0FF00CA9BFF00CA9AFF00CA9AFF00CA9AFF00CA
          9AFF00CB9CFF00CFA0FF00B680FF008948FF0000003D0000001C000000060000
          000000000000000000000000000000000000000000000000000000000011008B
          4AFF00B581FF00CDA1FF00CA9DFF00C99BFF00C99BFF00C99BFF00C99BFF00CA
          9DFF2BEBB7FF008341FF008341FF2BEBB7FF00CA9DFF00C99BFF00C99BFF00C9
          9BFF00C99BFF00CB9DFF00CEA1FF00B580FF008948FF0000003D0000001C0000
          000600000000000000000000000000000000000000000000000000000006008A
          49FF34F0C1FF00C99EFF00C79BFF00C79BFF00C79BFF00C79BFF00C79DFF34ED
          BDFF008644FF0000000600000006008644FF34EDBDFF00C79DFF00C79BFF00C7
          9BFF00C79BFF00C79BFF00C89DFF00CCA1FF00B482FF008A47FF0000003D0000
          001C000000060000000000000000000000000000000000000000000000000000
          0006008644FF3DEDC1FF00C59CFF00C49BFF00C49BFF00C59CFF3DEDC1FF0086
          43FF00000006000000000000000000000006008643FF3DEDC1FF00C59CFF00C4
          9BFF00C59BFF00C59BFF00C59BFF00C69DFF00C9A1FF00B282FF008A47FF0000
          003D0000001C0000000600000000000000000000000000000000000000000000
          000000000006008643FF45EEC6FF00C29CFF00C29CFF45EEC6FF008643FF0000
          00060000000000000000000000000000000000000006008643FF45EEC6FF00C3
          9CFF00C29BFF00C39BFF00C39BFF00C39BFF00C49DFF00C7A1FF00B182FF008A
          47FF0000003D0000001C00000006000000000000000000000000000000000000
          00000000000000000006008643FF4DF1CBFF4DF1CBFF008643FF000000060000
          0000000000000000000000000000000000000000000000000006008642FF4EF0
          C9FF00C09BFF00C09BFF00C19BFF00C19BFF00C19BFF00C29DFF00C5A1FF00B0
          82FF008A47FF0000003D0000001C000000060000000000000000000000000000
          0000000000000000000000000006008947FF008947FF00000006000000000000
          0000000000000000000000000000000000000000000000000000000000060086
          42FF58F0CFFF00BE9BFF00BE9BFF00BF9BFF00BF9BFF00BF9BFF00C09DFF00C3
          A1FF00AF82FF008A47FF0000003D0000001C0000000600000000000000000000
          0000000000000000000000000000000100010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0006008642FF61F2D4FF00BB9BFF00BC9AFF00BD9BFF00BD9BFF00BD9BFF00BE
          9DFF00C1A1FF00AE82FF008B47FF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000006008642FF6AF3D8FF00B99CFF00BA9BFF00BB9CFF00BB9CFF00BB
          9CFF00BC9EFF00BEA2FF00AC81FF008B47FF0000003700000011000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000006008641FF73F3DCFF00B79CFF00B89BFF00B99CFF00B9
          9CFF00B99CFF00BA9DFF00BBA1FF00A981FF008C49FF00000011000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000006008641FF7BF5E0FF00B59BFF00B59BFF00B7
          9CFF00B79CFF00B69BFF00B59DFF7DF8E5FF008A48FF00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000006008641FF84F6E5FF00B29BFF00B3
          9AFF00B39AFF00B29BFF84F6E5FF008641FF0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000006008641FF8EF6E9FF00B2
          9AFF00B29AFF8EF6E9FF008641FF000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000006008640FF95F8
          EFFF95F8EFFF008640FF00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060089
          46FF008946FF0000000600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000020000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000005000000030000000200000000000000050000
          00080000000B0000000E00000010000000140000001700000028000000310001
          02350211357203236DAD05319BD2063BBBFF053DB9FF063CBAFF063ABBFF063B
          BBFF053098EC042067B70210338900000051000000460000003E0000003B0000
          0033000000310000002800000019000000140000001000000000000000040000
          000400000006000000090000000D00000010000000140001041D0219527F0536
          AEEF043CBEFF0346CAFF0250D8FF0252DBFF0256E0FF0257E1FF0255DFFF0252
          DBFF024ED6FF0342C6FF053ABBFF0634A8EB03184C820001022A000000240000
          00200000001C0000001800000014000000100000000D00000000000000000000
          000000000000000000000000000000000000011035480431A2DE043EC2FF0251
          DAFF0257E2FF0257E2FF0154DCFF0252D7FF0150D4FF014FD1FF0150D4FF0252
          D7FF0154DBFF0257E2FF0257E2FF024ED7FF053ABDFF04309FDB010E2E3F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000021A55750537B9FC024FD6FF0258E3FF0254
          DBFF024ED2FF024ECEFF024ECEFF024DCCFF024DCDFF014ECEFF014DCDFF014D
          CCFF014DCCFF014DCCFF014FD0FF0254DBFF0257E2FF0349D1FF0535B3F90217
          4E6C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000003206B93063EC1FF0355DFFF0255DDFF0250D3FF0350
          D0FF0652D0FF0A54D3FF0B57D5FF0955D7FF0451D1FF034DCBFF024ECEFF0251
          D5FF0252D8FF0251D5FF024FD0FF014ECEFF014FD1FF0255DDFF0353DCFF0538
          BAFF041E63880406090900000000000000000000000000000000000000000000
          000000000000021853720742C3FF0357E0FF0153D9FF0351D3FF0552D3FF0C58
          D4FF155CD1FF1253C1FF084ABBFF0C4DBBFF1C57BEFF3368C2FF245DC0FF1150
          BCFF0749BAFF084CC1FF0251D5FF0252D7FF024FD2FF024FD1FF0153D9FF0354
          DEFF053BBDFF04194C67070B1010000000000000000000000000000000000000
          0000010F3245083DBDFC0456DEFF0252D7FF0351D3FF0553D5FF105AD6FF205E
          C9FF0548BBFF416DB9FFA1A6AEFFC6C6C6FFD7D7D7FFDFDFDFFFE3E3E3FFE4E4
          E4FFD7DBE2FF6D91CEFF094BBCFF084EC3FF0354DAFF0252D6FF0250D3FF0252
          D8FF0351DAFF0537B5F908163645060A0E0E0000000000000000000000000001
          04060530A0DB0856DBFF0254DAFF0352D5FF0555D8FF0855D6FF0855D6FF0855
          D6FF0551D2FF0F41AFFFA7A7A7FFAAAAAAFFC4C4C4FFD6D6D6FFDFDFDFFFE3E3
          E3FFE4E4E4FFE5E5E5FFE0E1E4FF4072C7FF084BBEFF0354D8FF0353D8FF0351
          D3FF0253D9FF044BD2FF1340A4D6010204050000000000000000000000000217
          4D690A4CCDFF0557DDFF0353D8FF0354DAFF0A57D7FF0855D6FF0855D6FF0755
          D5FF0655D5FF0551D3FF0E41B0FFA5A5A5FFA9A9A9FFC3C3C3FFD6D6D6FFDFDF
          DFFFE3E3E3FFE4E4E4FFE5E5E5FFE5E5E5FF6D90CFFF074BBEFF0455DBFF0354
          D9FF0352D6FF0353DAFF0A46C7FF0C23536A0000000000000000000102030534
          AAE80B5ADDFF0354DAFF0455DBFF7EB2F6FF98C5FAFF1764DBFF0F59D6FF0855
          D6FF0855D6FF0755D6FF0551D3FF0E41B1FFA5A5A5FFA9A9A9FFC3C3C3FFD6D6
          D6FFDEDEDEFFE3E3E3FFE4E4E4FFE5E5E5FFE5E5E5FF4877C9FF094EC3FF0456
          DDFF0455D9FF0353D8FF0C55D8FF0432A3DF00000000000000000212394E0849
          CCFF0B5DDFFF0456DDFF0D5EDFFF5291EBFF0449BFFF3787E8FF1564DBFF0856
          D6FF0856D6FF0856D6FF0756D6FF0551D3FF0E42B2FFA5A5A5FFA9A9A9FFC4C4
          C4FFD7D7D7FFE0E0E0FFE5E5E5FFE6E6E6FFE7E7E7FFE5E6E7FF0E4FC0FF0453
          D5FF0557DDFF0455DBFF0755D9FF0540C3FF010E2C3C0000000003236E960B52
          D5FF0658DDFF0658DEFF0759DEFF0D52C7FF648CD1FF0E57D1FF3989EAFF1765
          DCFF0957D7FF0957D7FF0957D7FF0857D7FF0551D3FF0D42B4FFA5A5A5FFA9A9
          A9FFC5C5C5FFD9D9D9FFE2E2E2FFE7E7E7FFE8E8E8FFE9E9E9FF8EA9D8FF094E
          C3FF0558DEFF0557DEFF0457DDFF0447CCFF031F6387000000000537A3D80855
          D8FF0558DEFF0859DDFF0B5BDEFF074CC0FFE3E5E9FFE5E7E9FF0E57D1FF3B8B
          EAFF1766DDFF0958D8FF0958D8FF0958D8FF0858D8FF0551D3FF0D42B5FFA5A5
          A5FFA9A9A9FFC5C5C5FFDADADAFFE4E4E4FFE9E9E9FFEAEAEAFFE8E9EAFF074B
          C0FF0658DEFF0658DEFF0558DEFF054BD1FF053197C9000000000643C0FA0655
          DCFF075ADFFF0B5CDEFF0E5AD8FF1555C3FFEAEAEAFFEBEBEBFFE8EAECFF0E57
          D1FF3E8DEAFF1867DDFF0958D8FF0958D8FF0958D8FF0858D8FF0551D3FF0C43
          B6FFA5A5A5FFA9A9A9FFC6C6C6FFDCDCDCFFE5E5E5FFEAEAEAFFECECECFF2661
          C7FF0655D7FF075ADFFF0659DFFF054FD3FF063DB7EF000000000746C6FF0658
          DEFF095BDFFF0F5EDEFF0E58D1FF2D64C3FFEAEAEAFFECECECFFEFEFEFFFEAEC
          EEFF0E57D1FF4290EBFF1968DEFF0A59D9FF0A59D9FF0A59D9FF0959D9FF0551
          D3FF0C43B8FFA5A5A5FFA9A9A9FFC7C7C7FFDDDDDDFFE6E6E6FFEAEAEAFF497A
          CEFF0755D4FF085BE0FF075ADFFF0651D6FF0643C4FF000000000848C6FF075A
          E0FF0A5CDFFF1160DFFF0E55CCFF4774C1FFE8E8E8FFECECECFFF0F0F0FFF1F1
          F1FFECEEF0FF0E57D1FF4692ECFF1A69DFFF0A5ADAFF0A5ADAFF0A5ADAFF095A
          DAFF0551D3FF0B42B9FFA5A5A5FFA9A9A9FFC6C6C6FFDBDBDBFFE4E4E4FF648C
          D3FF0854D1FF0A5CE0FF085BE0FF0754D8FF0745C4FF00000000094BC7FF085A
          DFFF0B5DE0FF1262DFFF1259CEFF386AC0FFE0E0E0FFEAEAEAFFF0F0F0FFF2F2
          F2FFF2F2F2FFEDEFF1FF0E57D1FF4995EDFF1C6BE0FF0B5BDBFF0B5BDBFF0B5B
          DBFF0A5BDBFF0551D3FF0B43BAFFA5A5A5FFA8A8A8FFBBBBBBFFC7C7C7FF4274
          CBFF125BD2FF0E60E0FF0A5DE1FF0854D7FF0847C5FF00000000094AC2F90859
          DDFF0B5EE2FF1363E0FF175FD2FF225DC2FFC9C9C9FFE8E8E8FFEFEFEFFFF3F3
          F3FFF3F3F3FFF4F4F4FFF0F1F3FF0E57D1FF4C97EEFF1C6BE0FF0B5BDBFF0B5B
          DBFF0B5BDBFF0A5BDBFF0551D3FF0A43BCFFA3A4A4FFA8A8A8FFB1B1B1FF205C
          C4FF1D64D4FF1363E0FF0B5DE1FF0955D6FF0745B8ED000000000843A6D20959
          DAFF0B5FE2FF1262E1FF2069DDFF094EC5FFB0B1B1FFE0E0E0FFECECECFFF2F2
          F2FFF5F5F5FFF5F5F5FFF6F6F6FFF2F3F5FF0E57D1FF509AF0FF1E6EE1FF0C5D
          DCFF0C5DDCFF0C5DDCFF0B5DDCFF0551D3FF0943BDFFA6A7A7FFABACACFF0B4E
          C3FF286BD6FF1564E0FF1363E2FF0D56D2FF073B97C000000000063177960A57
          D4FF0F62E3FF1062E3FF1D68E0FF0E52C8FF7990B8FFBEBEBEFFE7E7E7FFEFEF
          EFFFF3F3F3FFF7F7F7FFF8F8F8FFF8F8F8FFF4F5F7FF0E57D1FF549CF0FF206F
          E2FF0D5EDDFF0D5EDDFF0D5EDDFF0C5EDDFF0551D3FF0943BDFF6A86B6FF1856
          C2FF3573D6FF1664E1FF2776E7FF0B54CEFF052B6B8700000000031838450B57
          CEFF1E6FE8FF1265E5FF1868E3FF1C63D5FF1456C7FFB1B1B1FFC7C7C7FFE6E6
          E6FFEFEFEFFFF5F5F5FFF8F8F8FFF8F8F8FFF9F9F9FFF6F7F9FF0E57D1FF579F
          F1FF206FE3FF0D5EDEFF0D5EDEFF0D5EDEFF0C5EDEFF0551D3FF0348C4FF2E6B
          CEFF236CDDFF1D6CE5FF2D7AE6FF0A54CCFF02122B3600000000000102030B4F
          B7E11667E1FF2F7EEBFF1365E5FF206DE1FF0F54CAFF567DC1FFB1B1B1FFC0C0
          C0FFE3E3E3FFECECECFFF2F2F2FFF5F5F5FFF6F6F6FFF7F7F7FFF3F4F6FF0E57
          D1FF5AA0F2FF2170E4FF0D5FDFFF0D5FDFFF0D5FDFFF0C5FDFFF0D55CDFF2B72
          DCFF1A68E1FF3D88ECFF1463D9FF0A49AED80000000000000000000000000628
          5B6F0C5DD4FF3985EEFF1D6FE8FF186AE5FF256EDFFF094FC8FF738DBAFFB2B2
          B2FFB2B2B2FFCDCDCDFFE4E4E4FFEAEAEAFFEEEEEEFFEFEFEFFFEFEFEFFFEBEC
          EEFF0E57D1FF5CA2F3FF2272E4FF0E60DFFF0E60DFFF0E60DFFF256DDBFF216D
          E1FF2776E8FF3984EBFF0D5CD0FF05224E600000000000000000000000000002
          05060B52B4DB1466DFFF4992F0FF196CE9FF1D6DE5FF246CDCFF0950C9FF567E
          C1FFB1B1B1FFAFAFAFFFB1B1B1FFBDBDBDFFC7C7C7FFCCCCCCFFCDCDCDFFC8C8
          C8FFBDBDBEFF0E57D1FF5FA4F3FF2372E5FF0E61E0FF2E72DAFF2972E0FF2072
          E8FF5099F1FF1362D7FF0B4BAACF000102030000000000000000000000000000
          00000317323C0D60D1FC2172E6FF4E97F2FF176CE9FF1E6FE7FF2872E1FF1056
          CCFF1759CAFF8094B8FFAFAFAFFFB3B3B3FFB4B4B4FFB2B2B2FFB2B2B2FFB4B4
          B4FFB4B5B5FF6F8ABAFF044ECBFFA2CDFDFF327BE3FF2873E3FF1D6EE7FF569E
          F4FF1D6EE0FF0E5ECEF903142C36000000000000000000000000000000000000
          000000000000072D62750E62D5FF307FEBFF559CF4FF1C71EBFF1D70E9FF2976
          E7FF236AD9FF0E54CDFF0B51CAFF2C65C8FF4573C4FF557CC0FF3C6EC4FF245F
          C7FF0D52CAFF145ACEFF66A4F3FF3382F1FF2272E7FF2273EAFF59A1F4FF2A7A
          E7FF0E61D4FF0628576900000000000000000000000000000000000000000000
          0000000000000000000008336E840F65D5FF2677E8FF5CA2F5FF287AEEFF1B71
          EDFF2174EBFF2977E8FF2E78E4FF2167D8FF1A61D4FF195ED1FF1D63D5FF266D
          DAFF3077E2FF2D79E7FF2373E9FF1669E6FF2E7FEFFF5CA3F4FF2072E3FF0E64
          D5FF0730677B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000062651600F63D0F91A6DE1FF519BF3FF4F99
          F4FF2478EFFF1C72EEFF2074EDFF2476EDFF2677ECFF2778EBFF2778ECFF2576
          ECFF2275EDFF1D72EEFF277AEEFF4F99F3FF4F98F2FF166BDDFF0E61CEF60521
          4654000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000418323C0C53AFCF0E65D8FF2477
          E7FF569EF4FF579FF6FF3E8CF3FF257BF1FF1D74F1FF1E74F1FF1D73F0FF277C
          F1FF408EF3FF5AA1F6FF549CF3FF2073E3FF0F66D7FF0C50A9C903142B330000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000005264E5D0D59
          BBDE0F67D7FF176CDCFF3584EAFF5BA3F4FF60A7F5FF66ACF6FF63A9F5FF57A0
          F3FF3281E9FF156ADBFF0F67D7FF0D56B6D80523495700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000003152B3308346D810C50A7C60D5BBEE10F67D7FF0F67D7FF0F67D7FF0D5A
          BBDE0B4EA4C307336A7E0312262D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end>
  end
end
