unit AddApplItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, Buttons, StdCtrls, DBCtrls,
  M207Ctrls, jpeg, DBGridEhGrouping, rxPlacemnt, rxSpeedbar,
  GridsEh;

type
  TfmAddApplDepItem = class(TForm)
    dg1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    Splitter1: TSplitter;
    pButton: TPanel;
    Panel1: TPanel;
    dg2: TDBGridEh;
    sbAdd: TSpeedButton;
    sbDel: TSpeedButton;
    LArt: TLabel;
    dbart: TDBText;
    fr: TM207FormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure sbAddClick(Sender: TObject);
    procedure sbDelClick(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddApplDepItem: TfmAddApplDepItem;

implementation

uses data, comdata, data3, M207Proc, DB, data2, MsgDialog;

{$R *.dfm}

procedure TfmAddApplDepItem.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 OpenDataSets([dm3.quSelectUid, dm3.quApplDepArt]);
end;

procedure TfmAddApplDepItem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm3.quSelectUid, dm3.quApplDepArt]);
end;

procedure TfmAddApplDepItem.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmAddApplDepItem.sbAddClick(Sender: TObject);
begin
 if dm3.quApplDepListISCLOSED.AsInteger=1 then MessageDialog('������ �������', mtWarning, [mbOk], 0)
 else if dm3.quApplDepListMODEID.AsInteger<>SelfDepId then MessageDialog('������ ������� �� ������ �������', mtWarning, [mbOk], 0)
 else if (not dm3.quSelectUidUID.IsNull) and (dm3.quSelectUidFEXISTS.AsInteger=1) then
 begin
  dm3.quApplDepArt.Insert;
  dm3.quApplDepArtART2ID.AsInteger:=dm3.quSelectUidART2ID.AsInteger;
  dm3.quApplDepArtUID.AsInteger:=dm3.quSelectUidUID.AsInteger;
  dm3.quApplDepArtSZ.AsString:=dm3.quSelectUidSZ.AsString;
  dm3.quApplDepArtW.AsFloat:=dm3.quSelectUidW.AsFloat;
  dm3.quApplDepArtCOST.AsFloat:=dm3.quSelectUidCOST.AsFloat;
  dm3.quApplDepArtCOSTP.AsFloat:=dm3.quSelectUidCOSTP.AsFloat;  
  dm3.quApplDepArt.Post;
  dm3.quSelectUid.Delete;
  ReOpenDataSets([dm3.quSelectUid]);
 end;
end;

procedure TfmAddApplDepItem.sbDelClick(Sender: TObject);
begin
 if dm3.quApplDepListISCLOSED.AsInteger=1 then MessageDialog('������ �������', mtWarning, [mbOk], 0)
 else if dm3.quApplDepListMODEID.AsInteger<>SelfDepId then MessageDialog('������ ������� �� ������ �������', mtWarning, [mbOk], 0)
 else if not dm3.quApplDepArtUID.IsNull then
 begin
  dm3.quSelectUid.Insert;
  dm3.quSelectUidART2ID.AsInteger:=dm3.quApplDepArtART2ID.AsInteger;
  dm3.quSelectUidUID.AsInteger:=dm3.quApplDepArtUID.AsInteger;
  dm3.quSelectUidSZ.AsString:=dm3.quApplDepArtSZ.AsString;
  dm3.quSelectUidW.AsFloat:=dm3.quApplDepArtW.AsFloat;
  dm3.quSelectUidCOST.AsFloat:=dm3.quApplDepArtCOST.AsFloat;
  dm3.quSelectUidCOSTP.AsFloat:=dm3.quApplDepArtCOSTP.AsFloat;    
  dm3.quSelectUidART2.AsString:=dm3.quApplDepArtART2.AsString;
  dm3.quSelectUid.Post;
  dm3.quApplDepArt.Delete;
  ReOpenDataSets([dm3.quSelectUid]);    
 end
end;

procedure TfmAddApplDepItem.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmAddApplDepItem.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if dm3.quSelectUidFEXISTS.AsInteger=0 then Background:=clYellow;
end;

end.
