object fmCLSINV_CHECK: TfmCLSINV_CHECK
  Left = 0
  Top = 0
  Caption = #1042#1085#1080#1084#1072#1085#1080#1077'! '#1048#1084#1077#1102#1090#1089#1103' '#1085#1077#1079#1072#1082#1088#1099#1090#1099#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077'!'
  ClientHeight = 157
  ClientWidth = 518
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 0
    Width = 518
    Height = 120
    Align = alTop
    DataGrouping.GroupLevels = <>
    DataSource = dsClsinv
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.Caption = #8470
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'RESNAME'
        Footers = <>
        Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        Width = 153
      end
      item
        EditButtons = <>
        FieldName = 'SSUM'
        Footers = <>
        Title.Caption = #1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
        Width = 86
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object btnOk: TBitBtn
    Left = 160
    Top = 124
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object BitBtn1: TBitBtn
    Left = 257
    Top = 124
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object clsinv: TpFIBDataSet
    SelectSQL.Strings = (
      'select sn,resname, ssum, sdate '
      'from clsinv_period(:bd,:ed)'
      'order by sdate')
    BeforeOpen = clsinvBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 128
    object clsinvSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object clsinvRESNAME: TFIBStringField
      FieldName = 'RESNAME'
      Size = 300
      EmptyStrToNull = True
    end
    object clsinvSSUM: TFIBFloatField
      FieldName = 'SSUM'
    end
    object clsinvSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
  end
  object dsClsinv: TDataSource
    DataSet = clsinv
    Left = 72
    Top = 128
  end
end
