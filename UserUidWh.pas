unit UserUidWh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, ActnList, MsgDialog,
  M207Ctrls, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmUserUIdWh = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    dg1: TDBGridEh;
    acList: TActionList;
    acDel: TAction;
    acClose: TAction;
    acUpdate: TAction;
    siUpdate: TSpeedItem;
    fr1: TM207FormStorage;
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acUpdateExecute(Sender: TObject);
    procedure acUpdateUpdate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUserUIdWh: TfmUserUIdWh;

implementation
uses data, comdata, data3, M207Proc, dbUtil;
{$R *.dfm}

procedure TfmUserUIdWh.acDelUpdate(Sender: TObject);
begin
 Taction(Sender).Enabled:= ((dmcom.Adm and CenterDep) or ((not dmcom.Seller) and (not CenterDep)));
end;

procedure TfmUserUIdWh.acDelExecute(Sender: TObject);
begin
 if MessageDialog('������� �����, ������������ ��� ���������� ������������', mtWarning, [mbYes, mbNo], 0)=mrYes then
 begin
  Screen.Cursor:=crSQLWait;
  ExecSQL('delete from uidwh_t where userid='+dm3.taUSerUidWhUSERID.asString, dm.quTmp);
  ReOpenDataSets([dm3.taUSerUidWh]);  
  Screen.Cursor:=crDefault;
 end;
end;

procedure TfmUserUIdWh.FormCreate(Sender: TObject);
begin
 tb1.Wallpaper:=wp;
 OpenDataSets([dm3.taUSerUidWh]);
 dg1.FieldColumns['USERID'].Visible:=AppDebug;
end;

procedure TfmUserUIdWh.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUserUIdWh.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUserUIdWh.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmUserUIdWh.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm3.taUSerUidWh]);
end;

procedure TfmUserUIdWh.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  if (dm3.taUSerUidWhCALCUIDWH.IsNull) then Background:=clWindow
  else if (dm3.taUSerUidWhCALCUIDWH.AsInteger=0) then Background:=clWindow
  else Background:=clRed
 end
end;

procedure TfmUserUIdWh.acUpdateExecute(Sender: TObject);
begin
 if MessageDialog('����� ���� ������������� ������? ', mtWarning, [mbYes, mbNo], 0)=mrYes then
 begin
  Screen.Cursor:=crSQLWait;
  ExecSQL('update d_emp set calcuidwh=0 where d_empid='+dm3.taUSerUidWhUSERID.AsString, dm.quTmp);
  ReOpenDataSet(dm3.taUSerUidWh);
  Screen.Cursor:=crDefault;  
 end
end;

procedure TfmUserUIdWh.acUpdateUpdate(Sender: TObject);
begin
 Taction(Sender).Enabled:= (((dmcom.Adm and CenterDep) or ((not dmcom.Seller) and (not CenterDep))))
   and (not dm3.taUSerUidWhUSERID.isnull) and (dm3.taUSerUidWhCALCUIDWH.AsInteger<>0);
end;

end.
