unit SElFind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, db, Variants;

type
  TfmSElFind = class(TForm)
    Label1: TLabel;
    edFullArt: TEdit;
    edArt2: TEdit;
    edPrice: TEdit;
    edQuantity: TEdit;
    edTotalWeight: TEdit;
    edCost: TEdit;
    edPrice2: TEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    cb1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSElFind: TfmSElFind;

implementation

uses Data, MsgDialog;

{$R *.DFM}

procedure TfmSElFind.FormCreate(Sender: TObject);
begin
  edFullArt.Text:='';
  edArt2.Text:='';
  edPrice.Text:='';
  edQuantity.Text:='';
  edTotalWeight.Text:='';
  edCost.Text:='';
  edPrice2.Text:= '';
end;

procedure TfmSElFind.BitBtn1Click(Sender: TObject);
var s: string;
    i: integer;
    FldValues: Variant;
    V: Variant;
    b: boolean;
    opt: TLocateOptions;
begin
  b := False;
  
  if cb1.Checked then opt:=[loCaseInsensitive, loPartialKey]
  else opt:=[loCaseInsensitive];

  s:='';
  i:=-1;
  FldValues:=VarArrayCreate([0, 6], varVariant);

  if edFullArt.Text<>'' then
    begin
      Inc(i);
      s:='ART;';
      FldValues[i]:=edFullArt.Text;
      v:=edFullArt.Text;
    end;

  if edArt2.Text<>'' then
    begin
      Inc(i);
      s:=s+'ART2;';
      FldValues[i]:=edArt2.Text;
      v:=edArt2.Text;
    end;

  if edPrice.Text<>'' then
    begin
      Inc(i);
      s:=s+'PRICE;';
      FldValues[i]:=edPrice.Text;
      v:=edPrice.Text;
    end;

  if edQuantity.Text<>'' then
    begin
      Inc(i);
      s:=s+'QUANTITY;';
      FldValues[i]:=edQuantity.Text;
      v:=edQuantity.Text;
    end;

  if edTotalWeight.Text<>'' then
    begin
      Inc(i);
      s:=s+'TOTALWEIGHT;';
      FldValues[i]:=edTotalWeight.Text;
      v:=edTotalWeight.Text;
    end;

  if edCost.Text<>'' then
    begin
      Inc(i);
      s:=s+'COST;';
      FldValues[i]:=edCOST.Text;
      v:=edCost.Text;
    end;

  if edPrice2.Text<>'' then
    begin
      Inc(i);
      s:=s+'PRICE2;';
      FldValues[i]:=edPrice2.Text;
      v:=edPrice2.Text;
    end;


  if i>-1  then
    with dm.taSel do
      try
        DisableControls;
        SetLength(s, Length(s)-1);
        case TComponent(Sender).Tag of
          1:
            if i=0 then b:=Locate(s, V, opt)
            else b:=Locate(s, FldValues, opt);
          2:
            begin
              Next;
              if i=0 then b:=LocateNext(s, V, opt)
              else b:=LocateNext(s, FldValues, opt);
              if not b then Prior
            end
          end;
         if NOT b then MessageDialog('������ �� �������', mtInformation, [mbOK], 0);
      finally
        EnableControls;
      end
end;

procedure TfmSElFind.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_F12, VK_ESCAPE] then Close;
end;

end.
