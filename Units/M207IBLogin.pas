unit M207IBLogin;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, Db, RxLogin, RxDBSecur,
     RxGrdCpt,  pFIBDataSet, pFIBDatabase, rxPlacemnt, jpeg;


{$ifdef IBX}
Type TLoginFunction=function(UsersTable: TIBDataSet; const Password: String): Boolean of object;
{$else}
Type TLoginFunction=function(UsersTable: TpFIBDataSet; const Password: String): Boolean of object;
{$endif}

  TfmDBLogin = class(TForm)
    KeyImage: TImage;
    HintLabel: TLabel;
    UserNameLabel: TLabel;
    PasswordLabel: TLabel;
    edPassword: TEdit;
    DatabaseLabel: TLabel;
    cbDB: TComboBox;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    cbUser: TComboBox;
    fs1: TFormStorage;
    SpeedButton1: TSpeedButton;
    od1: TOpenDialog;
    Version: TLabel;
    FTimer: TTimer; // ��������� ��� ���������� ������� ���������� (������ � decCompileTime)
    Image1: TImage;

    procedure FormCreate(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure KeyImageDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cbUserKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton1Click(Sender: TObject);
    procedure cbDBKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    Attempt: integer;
   public
    { Public declarations }
    AttemptCount: Integer;
    UserField: string;

    LoginFunction: TLoginFunction;
    SelectDB: boolean;
{$ifdef IBX}
    taUser: TIBDataSet;
    Database: TIBDatabase;
{$else}
    taUser: TpFIBDataSet;
    Database: TpFIBDatabase;
{$endif}

  end;


const IDH_LOGON: integer=0;
const IDH_ERR_USER: integer=0;
const IDH_ERR_DB: integer=0;

{$ifdef IBX}
function LoginDB(ADatabase: TIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean):boolean;
{$else}
function LoginDB(ADatabase: TpFIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean):boolean;
{$endif}


var DBaseName: string;

implementation

uses RxAppUtils, RxDConst, Consts, RxVclUtils, RxConst, RxStrUtils,
     M207LoginEdit, M207Proc, MsgDialog, comdata, uUtils, Splash;

{$R *.DFM}

procedure TfmDBLogin.FormCreate(Sender: TObject);
var
  FileDate: Integer;
  FileName: string;
begin
  FileName := Application.ExeName;
  FileDate := FileAge(FileName);
  Version.Caption := '������ �� '+ DateToStr(FileDateToDateTime(FileDate))+ '  ';
end;

{$ifdef IBX}
function LoginDB(ADatabase: TIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean):boolean;
{$else}
function LoginDB(ADatabase: TpFIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean):boolean;
{$endif}
var fmDBLogin: TfmDbLogin;
begin
  fmDBLogin:=TfmDBLogin.Create(Application);
  try
    fmDBLogin.HelpContext:=IDH_LOGON;
    with fmDBLogin, cbDB do
      begin
        AttemptCount:= Attempts;
        Database:= ADatabase;
{$ifdef IBX}
        taUser:=TIBDataSet.Create(NIL);
{$else}
        taUser:=TpFIBDataSet.Create(NIL);
{$endif}
        taUser.Database:=ADatabase;
        taUser.SelectSQL.Text:='SELECT * FROM '+AUserTable;
        UserField:=AUserField;
        LoginFunction:=ALoginFunction;
        SelectDB:= NOT ASelectDB;
        Result:=ShowModal=mrOK;
     end;
  finally
    fmDBLogin.Free;
  end

end;


procedure TfmDBLogin.OkBtnClick(Sender: TObject);
var i: integer;
begin
  if cbDB.Text = '' then
  begin
     MessageDialog('�� ������� ���� ������ ��� �����������!',  mtError, [mbOK], IDH_ERR_USER);
     exit;
  end;
  Application.ProcessMessages;
  fmSplash.Show; // ��������������� �������� (splash)

  Hide; // ������ ����� ����������� � ���
  Application.ProcessMessages;
  fmSplash.SplashBar.StepIt; // ����� � ����� ����������� ������ ProgressBar'a
  Inc(Attempt);
  with  Database do
  begin
  fmSplash.SplashBar.Stepit;
     if Connected then
     begin
         with DefaultTransaction do
         if Active then Active:=False;
         Connected:=False;
     end;
     fmSplash.SplashBar.Stepit;
     if cbDB.Text<>'' then
        DatabaseName:=ExtractWord(1, cbDB.Text, [';']);
     try
        Connected:=True;
        DefaultTransaction.Active:=True;
        taUser.Active:=True;
        fmSplash.SplashBar.Stepit;
        if taUser.Locate(UserField, cbUser.Text, [loCaseInsensitive]) then
          if LoginFunction(taUser, edPassword.Text) then
          begin
              fmSplash.SplashBar.Stepit;
              ModalResult:=mrOK;
              with cbUser, Items do
              begin
                 i:=IndexOf(cbUser.Text);
                 if i=-1 then  Insert(0, cbUser.Text)
                 else Move(i, 0);
              end;
              fmSplash.SplashBar.Stepit;
              with cbDB, Items do
              begin
                 i:=IndexOf(LowerCase(cbDB.Text));
                 if i=-1 then  Insert(0, cbDB.Text)
                 else Move(i, 0);
              end;
              fmSplash.SplashBar.Stepit;
              DBaseName:=cbDB.Text;
              taUser.Active:=False;
              Database.Connected:=False;
              fmSplash.SplashBar.Stepit;
              exit;
            end;
         taUser.Active:=False;
         Database.Connected:=False;
         MessageDialog('�������� ��� ������������ ��� ������',  mtError, [mbOK], IDH_ERR_USER);

      //   dmCom:=TdmCom.Create(Forms.Application);
         fmSplash.Hide;
         Show;
         fmSplash.SplashBar.Position:=0;
         edPassword.Text:='';
         ActiveControl:=edPassword;
      except
        on E: Exception do
        begin
           MessageDialog('������ ��� ����������� � ��������� ���� �����'+#13#10+E.Message,
                           mtError, [mbOK], IDH_ERR_DB);
           fmSplash.Hide;
           Show;
           fmSplash.SplashBar.Position:=0;
        end;
      end;
      if DefaultTransaction.Active then DefaultTransaction.Commit;
    end;
  if Attempt=AttemptCount then
     ModalResult:=mrCancel;
end;

procedure TfmDBLogin.FormActivate(Sender: TObject);
begin
  with cbUser do
    Text:=Items[0];
  with cbDB do
    Text:=Items[0];
  KeyImageDblClick(Self);
end;

procedure TfmDBLogin.KeyImageDblClick(Sender: TObject);
begin
  SelectDB:=NOT SelectDB;
  if SelectDB then   Height:=cbDB.Top+cbDB.Height+ cbDB.Height div 2 +Height-ClientHeight
     else Height:=cbDB.Top-1+Height-ClientHeight;
end;

procedure TfmDBLogin.FormDestroy(Sender: TObject);
begin
  try
    with taUser.Database.DefaultTransaction do
         if Active then Commit;
    taUser.Free;
  except
  end;
end;

procedure TfmDBLogin.cbUserKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
     fmLoginEdit:=TfmLoginEdit.Create(NIL);
     fmLoginEdit.Caption:='������������';
     try
       fmLoginEdit.lb.Items.Text:=cbUser.Items.Text;
       if fmLoginEdit.ShowModal=mrOK then
          cbUser.Items.Text:=fmLoginEdit.lb.Items.Text;
     finally
       fmLoginEdit.Free;
     end;
  end;
end;

procedure TfmDBLogin.SpeedButton1Click(Sender: TObject);
begin
  if od1.Execute then
     cbDB.Text:=od1.FileName;
end;

procedure TfmDBLogin.cbDBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
  begin
     fmLoginEdit:=TfmLoginEdit.Create(NIL);
     fmLoginEdit.Caption:='���� ������';
     try
       fmLoginEdit.lb.Items.Text:=cbDB.Items.Text;
       if fmLoginEdit.ShowModal=mrOK then
          cbDB.Items.Text:=fmLoginEdit.lb.Items.Text;
     finally
       fmLoginEdit.Free;
     end;
  end;
end;

end.
