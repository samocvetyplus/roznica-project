unit M207LoginEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TfmLoginEdit = class(TForm)
    lb: TListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLoginEdit: TfmLoginEdit;

implementation

{$R *.DFM}

procedure TfmLoginEdit.BitBtn3Click(Sender: TObject);
begin
  with lb, Items do
  if (Count>0) and (ItemIndex<>-1) then Delete(ItemIndex);
end;

procedure TfmLoginEdit.FormActivate(Sender: TObject);
begin
  ActiveControl:=lb;
  with lb do
    if lb.Items.Count>0 then lb.ItemIndex:=0;
end;

end.
