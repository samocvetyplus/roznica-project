{**************************************************}
{  ����� ������� � �����                           }
{  Copyright (c) SZinkov, bAsile for ���������     }
{  ����� ������ ������ dm.SellRet                  }
{    True - ����� ���������� ������� ��� ��������  }
{    False - ����� ������� ��� ������              }
{**************************************************}
unit SellEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Mask, ExtCtrls, ComCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, DB, Menus, pFIBDataSet,
  Variants, DBCtrlsEh, DBLookupEh, RxStrUtils, ComDrv32,
  M207DBCtrls, DBGridEh, rxPlacemnt, rxToolEdit;

type
  TfmSellEdit = class(TForm)
    pm1: TPopupMenu;
    siSelect: TMenuItem;
    DBEdit7: TM207DBEdit;
    FormStorage1: TFormStorage;
    pc2: TPageControl;
    tshWH: TTabSheet;
    tshRet: TTabSheet;
    Panel3: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    deBD: TDateEdit;
    deED: TDateEdit;
    bbOpenSelled: TBitBtn;
    lcRetClient: TDBLookupComboBox;
    dgRet: TM207IBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Label18: TLabel;
    edSearch: TEdit;
    Button1: TButton;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    dgWH: TM207IBGrid;
    tsPrice: TTabSheet;
    dgPrice: TM207IBGrid;
    lbComp: TListBox;
    Splitter1: TSplitter;
    lbMat: TListBox;
    Splitter2: TSplitter;
    lbGood: TListBox;
    Splitter3: TSplitter;
    lbIns: TListBox;
    Splitter4: TSplitter;
    Label22: TLabel;
    edSArt: TEdit;
    Label23: TLabel;
    edUID: TEdit;
    lbCountry: TListBox;
    Splitter5: TSplitter;
    Panel4: TPanel;
    Panel5: TPanel;
    bbOk: TBitBtn;
    bbCnl: TBitBtn;
    bbAdd: TBitBtn;
    DBMemo1: TDBMemo;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBText7: TDBText;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    laRetDate: TLabel;
    laRetCause: TLabel;
    lbRetPrice: TLabel;
    DBEdit1: TM207DBEdit;
    DBEdit3: TM207DBEdit;
    DBEdit4: TM207DBEdit;
    lcEmp: TDBLookupComboBox;
    ceDiscount: TRxDBComboEdit;
    DBEdit2: TM207DBEdit;
    DBEdit5: TM207DBEdit;
    DBEdit6: TM207DBEdit;
    DBEdit8: TM207DBEdit;
    DBEdit9: TM207DBEdit;
    edPrice: TM207DBEdit;
    DBEdit11: TM207DBEdit;
    DBEdit10: TM207DBEdit;
    deRetDate: TDBDateEdit;
    M207DBEdit1: TM207DBEdit;
    edRetPrice: TDBEdit;
    Label25: TLabel;
    lcbxRetCause: TDBLookupComboboxEh;
    dbehretclient: TDBEditEh;
    Label24: TLabel;
    ednodcard: TEdit;
    edclient: TEdit;
    dbehRetaddress: TDBEditEh;
    Label26: TLabel;
    btHelp: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure siSelectClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure ceDiscountButtonClick(Sender: TObject);
    procedure edSearchChange(Sender: TObject);
    procedure edSearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure bbOpenSelledClick(Sender: TObject);
    procedure lcRetClientKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure edSArtChange(Sender: TObject);
    procedure edSArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edUIDChange(Sender: TObject);
    procedure sbRetDictClick(Sender: TObject);
    procedure dgWHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dgRetGetCellParams(Sender: TObject; Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure bbAddClick(Sender: TObject);
    procedure deEDAcceptDate(Sender: TObject; var ADate: TDateTime; var Action: Boolean);
    procedure deEDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lcbxRetCauseNotInList(Sender: TObject; NewText: String;
      var RecheckInList: Boolean);
    procedure lcbxRetCauseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbehretclientKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ednodcardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ednodcardKeyPress(Sender: TObject; var Key: Char);
    procedure ednodcardKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dbehRetaddressKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btHelpClick(Sender: TObject);
  private
    { Private declarations }
    SearchEnable: boolean;
    LogOprIdForm:string;    
  public
   {����� �������}
    oldclientid, newidclient:integer;
    retaddress, RetClient, oldclientname, newclientname, oldnodcard, newnodcard:string;
    CanselFl, FlRetClient:boolean;
  end;

var
  fmSellEdit: TfmSellEdit;

implementation

uses SellItem, comdata, Data, Data2, Client, DBTree, SellUid, StrUtils,
  RetDict, M207Proc, eClient, ServData, Data3, JewConst, pFIBQuery,
  FIBQuery, MsgDialog;

{$R *.DFM}

procedure TfmSellEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
  begin
    ednodcard.Text:='';
    edclient.Text:='';

    if (ModalResult=mrOK) and taSellItemArt2Id.IsNull then raise Exception.Create('���������� ������� �������');


    if ModalResult=mrOK then
    begin
      PostDataSet(taSellItem);


      if dm.WorkMode = 'SELLCH' then
      begin
       if taSellItemRET.AsInteger=1 then
        begin
         RetClient:=taSellItemRETCLIENT.AsString;
         retaddress:=taSellItemRETADDRESS.AsString;
        end;
       taSellItem.First;
       while not taSellItem.Eof do
       begin
        taSellItem.Edit;
        taSellItemCLIENTID.AsInteger:=newidclient;
        taSellItemNODCARD.AsString:= newnodcard;
        if taSellItemRET.AsInteger=1 then
        begin
         taSellItemRETCLIENT.AsString:=RetClient;
         taSellItemRETADDRESS.AsString:=retaddress;         
        end;

        taSellItem.Post;
        taSellItem.Next;
       end;
      end
      else
      begin
        taSellItem.Edit;
        taSellItemCLIENTID.AsInteger:=newidclient;
        taSellItemNODCARD.AsString:= newnodcard;
        taSellItem.Post;
      end;


      if dm.SellRet then
      begin
          with dm, quSellElDel do
          begin
            SQL.Text := 'update SellElem set IsNotProve = 1 where Uid =' +  quSelledUID.AsString;
            ExecQuery;
            Transaction.CommitRetaining;
            close;
          end;
          dm.quSelled.Delete;
      end;

    end
    else begin
       CancelDataSet(taSellItem);
    end;
    if(SellRet)then begin CloseDataSets([dm.quSelled]); dm.SellRet:=false end;
  end;

end;

procedure TfmSellEdit.FormActivate(Sender: TObject);
begin
 ActiveControl := bbCnl; //�������� �����
  with dm, dmCom do
  begin
    Old_D_MatId:='.';
    //��������� ���������� ����������� � ����������� �� ��������� �����
     If (dm.taSellListCLOSED.AsInteger=1){ or (dm.taSellListCLOSED.IsNull) }then
  begin
   fmSellEdit.VertScrollBar.Visible:=false;
   M207DBEdit1.Enabled:=false;
   DBEdit1.Enabled:=false;
   DBEdit2.Enabled:=false;
   DBEdit3.Enabled:=false;
   DBEdit4.Enabled:=false;
   DBEdit5.Enabled:=false;
   DBEdit8.Enabled:=false;
   DBEdit9.Enabled:=false;
   DBEdit10.Enabled:=false;
   DBEdit11.Enabled:=false;
   edPrice.Enabled:=false;
   bbOk.Enabled:=false;
   ceDiscount.Enabled:=false;
   DBMemo1.visible:=false;
   Label25.visible:=false;
   pc2.visible:=false;
   ednodcard.Enabled:=false;
   dbehretclient.Enabled:=false;
   dbehRetaddress.Enabled:=false;
   deRetDate.Enabled:=false;
   lcbxRetCause.Enabled:=false;
   edRetPrice.Enabled:=false;
   fmSellEdit.Width:=596;
   fmSellEdit.Height:=318;
  end
  else
  begin
  fmSellEdit.VertScrollBar.Visible:=true;
   M207DBEdit1.Enabled:=true;
   DBEdit1.Enabled:=true;
   DBEdit2.Enabled:=true;
   DBEdit3.Enabled:=true;
   DBEdit4.Enabled:=true;
   DBEdit5.Enabled:=true;
   DBEdit8.Enabled:=true;
   DBEdit9.Enabled:=true;
   DBEdit10.Enabled:=true;
   DBEdit11.Enabled:=true;
   edPrice.Enabled:=true;
   bbOk.Enabled:=true;
   bbAdd.Enabled:=true;
   ceDiscount.Enabled:=true;
   DBMemo1.visible:=true;
   Label25.visible:=true;
   pc2.visible:=true;
   dbehretclient.Enabled:=true;
   dbehRetaddress.Enabled:=true;
   deRetDate.Enabled:=true;
   lcbxRetCause.Enabled:=true;
   edRetPrice.Enabled:=true;
   fmSellEdit.Width:=777;
   fmSellEdit.Height:=581;
  end;
  //======================================================
    if SellRet then
    begin
      ActiveControl:=bbCnl;
      Caption:='����� ������� ��� ��������';
      tshWH.TabVisible := False;
      tshRet.TabVisible := True;
      pc2.ActivePage := tshRet;
      laRetCause.Visible:=True;
      laRetDate.Visible:=True;
      deRetDate.Visible:=True;
      lcbxRetCause.Visible:=True;
      lbRetPrice.Visible := True;
      edRetPrice.Visible := True;
      Label24.Visible:=true;
      dbehretclient.Visible:=true;

      Label26.Visible:=true;
      dbehRetaddress.Visible:=true;

      laRetDate.Visible:=true;
      deRetDate.Visible:=true;


      if not quSelled.Active then
      begin
        deED.Date:= strtodatetime(datetostr(dmCom.GetServerTime));
        deBD.Date:=IncMonth(dmCom.GetServerTime, -3);
        RetDate1:=deBD.Date;
        RetDate2:=deED.Date+1;
        RetClientId:=lcRetClient.KeyValue;
        quSelled.Active:=True;
      end;
      edUID.Text := '';
      fmSellEdit.HelpContext:=100241;
    end
    else
    begin
      Caption:='����� ������� ��� �������';
      tshWH.TabVisible := True;
      tshRet.TabVisible := False;
      pc2.ActivePage:= tshWH;
      pc1.ActivePage:=TabSheet1;
      tsPrice.TabVisible:=false;

      Label24.Visible:=false;
      dbehretclient.Visible:=false;

      Label26.Visible:=false;
      dbehRetaddress.Visible:=false;

      laRetDate.Visible:=False;
      deRetDate.Visible:=False;

      laRetCause.Visible:=False;
      lcbxRetCause.Visible:=False;

      lbRetPrice.Visible := False;
      edRetPrice.Visible := False;


      if not quD_WH.Active then
      begin
        D_MatId:='*';
        D_GoodId:='*';
        D_InsId:='*';
        D_CompId:=-1;
        quD_WH.Active:=True;
      end;

      fmSellEdit.HelpContext:=100232;
    end;
    dm.DPriceDepId:=SDepId;
    lbComp.ItemIndex:=0;
    lbMat.ItemIndex:=0;
    lbGood.ItemIndex:=0;
    lbIns.ItemIndex:=0;
    lbCountry.ItemIndex:=0;
    lbCompClick(NIL);
  end;

  SearchEnable:=False;
  edSearch.Text:='';
  SearchEnable:=True;
  SetCBDropDown(lcEmp);

  RetClient:='';
  retaddress:='';
  FlRetClient:=false;

 {************************}
  if (dm.taSellItem.State<>dsInsert) then
   begin
    CanselFl:=false;
    oldclientid:=dm.taSellItemCLIENTID.AsInteger;
    oldclientname:= dm.taSellItemCLIENTNAME.AsString;
    oldnodcard:= dm.taSellItemNODCARD.AsString;
    newidclient:=oldclientid;
    newnodcard:=oldnodcard;
    newclientname:=oldclientname;
    edNodCard.Text:= oldnodcard;
    edClient.Text := oldclientname;
   end
 {************************}
end;

procedure TfmSellEdit.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  if dm.SellRet then pc1.ActivePage:= tshRet
  else pc1.ActivePage:= tshWH;
end;

procedure TfmSellEdit.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;


procedure TfmSellEdit.FormCreate(Sender: TObject);
begin
  dm.D_WHArt:='';
  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry,nil,nil);
  dmCom.D_Att1Id := ATT1_DICT_ROOT;
  dmCom.D_Att2Id := ATT2_DICT_ROOT;
  // �������� ������� ��������� �������
  Finalize(dmServ.FSellItemIds);
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
 
end;

procedure TfmSellEdit.siSelectClick(Sender: TObject);
var DepId: integer;
    clientid:integer;
    clientname, nodcard:string;
    LogOperationID:string;
    fl:boolean;
begin
  DepId := 0;
  with dm, taSellItem do
    if(pc2.ActivePage = tshWH) then
    begin
      case pc1.ActivePage.TabIndex of // ������� �������
        1: begin
             if quDPriceArt2Id.IsNull then exit;

             LogOperationID:=dm3.insert_operation(sLog_AddSell,LogOprIdForm);

             if NOT (State in [dsInsert, dsEdit]) then Edit;
             taSellItemArt2Id.AsInteger:=quDPriceArt2Id.AsInteger;
             taSellItemArt2.AsString:=quDPriceArt2.AsString;
             taSellItemPrice.AsFloat:=quDPricePrice.AsFloat;
             taSellItemPrice0.AsFloat:=quDPricePrice.AsFloat;
             taSellItemUnitId.AsInteger:=quDPriceUnitId.AsInteger;
             taSellItemFullArt.AsString:=quDPriceFullArt.AsString;
             {************************}
             CanselFl:=false;
             oldclientid:=dm.taSellItemCLIENTID.AsInteger;

             dm.qutmp.close;
             dm.qutmp.sql.Text:='select nodcard, name from client where clientid = '+inttostr(oldclientid);
             dm.qutmp.ExecQuery;
             dm.taSellItemNODCARD.AsString:=dm.qutmp.Fields[0].AsString;
             dm.taSellItemCLIENTNAME.AsString:=dm.qutmp.Fields[1].AsString;
             dm.qutmp.Close;

             oldclientname:= dm.taSellItemCLIENTNAME.AsString;
             oldnodcard:= dm.taSellItemNODCARD.AsString;
             newidclient:=oldclientid;
             newnodcard:=oldnodcard;
             newclientname:=oldclientname;
             edNodCard.Text:= oldnodcard;
             edClient.Text := oldclientname;

             dm3.update_operation(LogOperationID);
             
             {************************}
             if dm.WorkMode<>'SELLCH' then
             begin
              ActiveControl := ednodcard;
              ednodcard.SelectAll;
             end
             else
             ActiveControl := bbOK;
        end;

        0: begin
             if quD_WHArt2Id.IsNull then exit;
             quWHA2UID.Active:=True;
             try
               if fmSellUID.ShowModal=mrOK then
               begin
                 {������� �� ����� ���� ������� ��� ���������� � ����� � ������� sellid ��� ����
                  ������� ��� ���������� �����}
                 dm3.quGetSmallestSell.Close;
                 dm3.quGetSmallestSell.ParamByName('AUID').AsInteger:=quWHA2UIDUID.AsInteger;
                 dm3.quGetSmallestSell.ParamByName('ASELLID').AsInteger:=taSellItemSELLID.AsInteger;
                 dm3.quGetSmallestSell.ParamByName('ARet').AsInteger:=0;
                 dm3.quGetSmallestSell.ExecQuery;
                 if dm3.quGetSmallestSell.Fields[0].AsInteger=1 then
                 begin
                  dm3.quGetSmallestSell.Transaction.CommitRetaining;
                  dm3.quGetSmallestSell.close;
                  raise Exception.Create(trim(dm3.quGetSmallestSell.Fields[1].AsString));
                 end;
                 dm3.quGetSmallestSell.Transaction.CommitRetaining;
                 dm3.quGetSmallestSell.close;

                 LogOperationID:=dm3.insert_operation(sLog_AddSell,LogOprIdForm);

                 if NOT (State in [dsInsert, dsEdit]) then Edit;
                 taSellItemArt2Id.AsInteger:=quD_WHArt2Id.AsInteger;
                 taSellItemFullArt.AsString:=quD_WHFullArt.AsString;
                 taSellItemArt2.AsString:=quD_WHArt2.AsString;
                 taSellItemPrice.AsFloat:=quD_WHPrice2.AsFloat;
                 taSellItemPrice0.AsFloat:=quD_WHPrice2.AsFloat;

                 taSellItemSZ.AsString:=quWHA2UIDSZ.AsString;
                 taSellItemW.AsFloat:=quWHA2UIDW.AsFloat;
                 taSellItemUID.AsString:=quWHA2UIDUID.AsString;
                 taSellItemSItemId.AsInteger:=quWHA2UIDSItemId.AsInteger;
                 taSellItemUnitId.AsInteger:=quD_WHUnitId.AsInteger;

                 {************************}
                  CanselFl:=false;
                  oldclientid:=dm.taSellItemCLIENTID.AsInteger;

                  dm.qutmp.close;
                  dm.qutmp.sql.Text:='select nodcard, name from client where clientid = '+inttostr(oldclientid);
                  dm.qutmp.ExecQuery;
                  dm.taSellItemNODCARD.AsString:=dm.qutmp.Fields[0].AsString;
                  dm.taSellItemCLIENTNAME.AsString:=dm.qutmp.Fields[1].AsString;
                  dm.qutmp.Close;

                  oldclientname:= dm.taSellItemCLIENTNAME.AsString;
                  oldnodcard:= dm.taSellItemNODCARD.AsString;
                  newidclient:=oldclientid;
                  newnodcard:=oldnodcard;
                  newclientname:=oldclientname;
                  edNodCard.Text:= oldnodcard;
                  edClient.Text := oldclientname;
                 {************************}

                 dm3.update_operation(LogOperationID);
                 
                 if dm.WorkMode<>'SELLCH' then
                 begin
                   ActiveControl := ednodcard;
                   ednodcard.SelectAll;
                 end
                 else
                  ActiveControl := bbOk;
               end
             finally
               quWHA2UID.Active:=False;
             end;
        end;
      end;
    end
    else begin // ������� �������
      if quSelledSellItemId.IsNull then exit;

      with dm, qutmp do begin
       close;
       sql.text:='select first 1 respstoringid from respstoring where stateuid<4 and uid='+quSelledUID.AsString;
       ExecQuery;
       if Fields[0].IsNull then fl:=true else fl:=false;
       close;
       Transaction.CommitRetaining;
       if not fl then begin
        edUID.Text:='';
        raise Exception.Create('������� �� ������������� ��������');
       end
      end;

      LogOperationID:=dm3.insert_operation(sLog_AddRet,LogOprIdForm);

      if NOT (State in [dsInsert, dsEdit]) then Edit;
      taSellItemRetSellItemId.AsInteger:=quSelledSellItemId.AsInteger;
      taSellItemArt2Id.AsInteger:=quSelledArt2Id.AsInteger;
      taSellItemArt2.AsString:=quSelledArt2.AsString;

      taSellItemPrice.AsFloat:=quSelledPrice.AsFloat;
      taSellItemPrice0.AsFloat:=quSelledPrice0.AsFloat;
      taSellItemSZ.AsString:=quSelledSZ.AsString;
      taSellItemW.AsFloat:=quSelledW.AsFloat;
      taSellItemUID.AsString:=quSelledUID.AsString;
      dm.taSellItemCLIENTID.AsInteger:=quSelledCLIENTID.AsInteger;
      dm.taSellItemCLIENTNAME.AsString:=quSelledCLIENT.AsString;

      if WorkMode='SELLCH' then
      begin
       if FlRetClient then
       begin
         taSellItemRETCLIENT.AsString:=RetClient;
         taSellItemRETADDRESS.AsString:=retaddress;
       end
       else FlRetClient:=true;
      end;

       dm.qutmp.close;
       dm.qutmp.sql.Text:='select nodcard from client where clientid = '+quSelledCLIENTID.AsString;
       dm.qutmp.ExecQuery;
       dm.taSellItemNODCARD.AsString:=dm.qutmp.Fields[0].AsString;
       dm.qutmp.Close;


      {************************}
       CanselFl:=false;
       oldclientid:=dm.taSellItemCLIENTID.AsInteger;
       oldclientname:= dm.taSellItemCLIENTNAME.AsString;
       oldnodcard:= dm.taSellItemNODCARD.AsString;
       newidclient:=oldclientid;
       newnodcard:=oldnodcard;
       newclientname:=oldclientname;
       edNodCard.Text:= oldnodcard;
       edClient.Text := oldclientname;
       {************************}

      if NOT quSelledSItemId.IsNull then
        taSellItemSItemId.AsInteger:=quSelledSItemId.AsInteger;
      if WorkMode<>'SELLCH' then
      begin
       if NOT quSelledClientId.IsNull then
         taSellItemClientId.AsInteger:=quSelledClientId.AsInteger;
       taSellItemCheckNo.AsVariant := quSelledCheckNo.AsVariant;
      end
      else
      begin
       with qutmp do
       begin
        close;
        sql.Text:='select clientid from sellitem where sellitemid = '+#13#10+
                  '(select max(sellitemid) from sellitem where uid = '+quSelledUID.AsString+
                  ')';
        ExecQuery;
        clientid:=Fields[0].AsInteger;
        close;
        if clientid = 0 then clientid:=-1;
        sql.Text:='select name, nodcard from client where clientid = '+inttostr(clientid);
        ExecQuery;
        clientname:=Fields[0].AsString;
        nodcard:=Fields[1].AsString;
        close;
       end;
       taSellItemCLIENTID.AsInteger:=clientid;
       taSellItemCLIENTNAME.AsString:=clientname;
       taSellItemNODCARD.AsString:=nodcard;
      end;

      taSellItemUnitId.AsInteger:=quSelledUnitId.AsInteger;
      taSellItemADate.AsDateTime:=quSelledSellDate.AsDateTime;
      taSellItemFullArt.AsString:=quSelledFullArt.AsString;

      if (WorkMode='SELL')or(WorkMode='SELLCH') then DepId:=taCurSellDepId.AsInteger
      else if WorkMode='ALLSELL' then DepId:=taSellListDepId.AsInteger;
      taSellItemRetPrice.AsFloat:=dm2.GetPrice(DepId, taSellItemArt2Id.AsInteger);

      dmCom.tr.CommitRetaining;

      dm3.update_operation(LogOperationID);
      
      if (taSellItemRET.AsInteger = 0) then
      begin
       ActiveControl := ednodcard;
       ednodcard.SelectAll;
      end
      else begin ActiveControl := dbehretclient; dbehretclient.SelectAll; end;
    end;
end;

procedure TfmSellEdit.pc1Change(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    case pc1.ActivePage.TabIndex of
        0: ReopenDataSets([dm.quD_WH]);
        1: ReopenDataSets([dm.quDPrice]);
       end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TfmSellEdit.ceDiscountButtonClick(Sender: TObject);
var p: TPoint;
begin
  with ceDiscount do
    begin
      p:=ClientToScreen(Point(-2, Height-4));
      dm2.pmDiscount.PopUp(p.x, p.y);
     end;
end;

procedure TfmSellEdit.edSearchChange(Sender: TObject);
begin
  if SearchEnable then
    if pc1.ActivePage.TabIndex=0 then
      begin
        if dm.quD_WH.Active then
          dm.quD_WH.Locate('ART', edSearch.Text, [loCaseInsensitive, loPartialKey])
      end
    else
      begin
        if dm.quDPrice.Active then
          dm.quDPrice.Locate('ART', edSearch.Text, [loCaseInsensitive, loPartialKey]);
      end;
end;

procedure TfmSellEdit.edSearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var d: TpFIBDataSet;
begin
  if SearchEnable then
    with dm do
      case Key of
        VK_RETURN:
          begin
            if pc1.ActivePage.TabIndex=0 then d:= quD_WH
            else d:= quDPrice;
            with d do
              begin
                try
                  DisableControls;
                  Next;
                  if NOT LocateNext('ART', edSearch.Text, [loCaseInsensitive, loPartialKey]) then Prior;
                finally
                  EnableControls;
                end
              end;
          end;
        VK_DOWN:
            if pc1.ActivePage.TabIndex=0 then ActiveControl:= dgWH
            else ActiveControl:= dgPrice;
        end;

end;

procedure TfmSellEdit.bbOpenSelledClick(Sender: TObject);
begin

   dm.SellElUser := dmCom.Userid;
   with dm,quToCheckShiftTime do
    try
      Screen.Cursor:=crSQLWait;
      quSelled.Active:=False;
      with quToCheckShiftTime do
      begin
        SQL.Text := 'execute procedure  Sell_Ret_Fill(?BD, ?ED, ?CLIENTID1, ?CLIENTID2,?UserID)';
        Prepare;
        ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(deED.Date+1);
        ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(deBD.Date);
        ParamByName('UserId').AsInteger:= SellElUser;

        if lcRetClient.KeyValue = null then
        begin
          ParamByName('CLIENTID1').AsInteger:=-MAXINT;
          ParamByName('CLIENTID2').AsInteger:=MAXINT;
        end
        else
        begin
          ParamByName('CLIENTID1').AsInteger:=lcRetClient.KeyValue;
          ParamByName('CLIENTID2').AsInteger:=lcRetClient.KeyValue;
        end;
        if not transaction.Active then transaction.StartTransaction;
        ExecQuery;
        close;
        Transaction.CommitRetaining;
      end;
      quSelled.Open;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmSellEdit.lcRetClientKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
      #8: lcRetClient.KeyValue:=NULL;
    end;
end;

procedure TfmSellEdit.Button1Click(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Screen.Cursor:=crSQLWait;
         pc1Change(NIL);
         ReopenDataSets([quArt]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmSellEdit.edSArtChange(Sender: TObject);
begin
  with dm.quSelled do
    if Active then Locate('ART', edSArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmSellEdit.edSArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
   UID : integer;
begin
  case Key of
    VK_DOWN   : ActiveControl:= dgRet;
    VK_RETURN : begin
      UID := StrToIntDef(edUID.Text, -1);
      if(UID = -1)then eXit;
      dm.quSelled.Locate('UID', UID, []);
      if(UID = dm.quSelledUID.AsInteger)then
      begin
      {������� �� ����� ���� ������� ��� ���������� � ����� � ������� sellid ��� ����
       ������� ��� ���������� �����}
       dm3.quGetSmallestSell.Close;
       dm3.quGetSmallestSell.ParamByName('AUID').AsInteger:=UID;
       dm3.quGetSmallestSell.ParamByName('ASELLID').AsInteger:=dm.taSellItemSELLID.AsInteger;
       dm3.quGetSmallestSell.ParamByName('ARet').AsInteger:=1;
       dm3.quGetSmallestSell.ExecQuery;
       if dm3.quGetSmallestSell.Fields[0].AsInteger=1 then
       begin
        dm3.quGetSmallestSell.Transaction.CommitRetaining;
        dm3.quGetSmallestSell.close;
        raise Exception.Create(trim(dm3.quGetSmallestSell.Fields[1].AsString));
       end;
       dm3.quGetSmallestSell.Transaction.CommitRetaining;
       dm3.quGetSmallestSell.close;

       siSelectClick(nil);
      end
      else MessageDialog('������� �� ������� � �������� �������', mtWarning, [mbOk], 0);
    end;
  end;
end;

procedure TfmSellEdit.edUIDChange(Sender: TObject);
begin
  with dm.quSelled do
    if Active then Locate('UID', edUID.Text, []);
end;

procedure TfmSellEdit.sbRetDictClick(Sender: TObject);
var b:boolean;
begin
//  ShowAndFreeForm(TfmRetDict, Self, TForm(fmRetDict), True, False);
 b:=GetBit(dmCom.EditRefBook, 10);
 ShowAndFreeFormEnabled(TfmRetDict, Self, TForm(fmRetDict), True, False, b,'spitPrint;siExit;tb1;', 'dg1;');
  with dm, dmCom, dm2 do
    begin
      if taRet.Tag<>-1 then
        begin
          with taSellItem do
            if NOT (State in [dsInsert, dsEdit]) then Edit;
          taSellItemD_RetId.AsString:=taRetD_RetId.AsString;
          ReopenDataSets([taRet]);
        end
    end;

end;

procedure TfmSellEdit.dgWHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    siSelectClick(Sender);
end;

procedure TfmSellEdit.dgRetGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  case dm.quSelledRetDone.AsInteger of
      0: Background:=clInfoBk;
      1: Background:=clBtnFace;
  end;
end;

procedure TfmSellEdit.bbAddClick(Sender: TObject);
var LogOperationID:string;
begin
  if dm.SellRet then LogOperationID:=dm3.insert_operation(sLog_Ret,LogOprIdForm)
  else LogOperationID:=dm3.insert_operation(sLog_SelectSell,LogOprIdForm);
 with dm do
    begin
      if WorkMode='SELLCH' then taSellItemCHECKNO.AsInteger:=D_CHECK0;
      PostDataSet(taSellItem);
      if dm.WorkMode = 'SELLCH' then
      begin
       if taSellItemRET.AsInteger=1 then
       begin
         RetClient:=taSellItemRETCLIENT.AsString;
         retaddress:=taSellItemRETADDRESS.AsString;
       end;
       taSellItem.First;
       while not taSellItem.Eof do
       begin
        taSellItem.Edit;
        taSellItemCLIENTID.AsInteger:=newidclient;
        taSellItemNODCARD.AsString:= newnodcard;
        if taSellItemRET.AsInteger=1 then
        begin
         taSellItemRETCLIENT.AsString:=RetClient;
         taSellItemRETADDRESS.AsString:=retaddress;
        end;
        taSellItem.Post;
        taSellItem.Next;
       end;
      end
      else
      begin
        taSellItem.Edit;
        taSellItemCLIENTID.AsInteger:=newidclient;
        taSellItemNODCARD.AsString:= newnodcard;
        taSellItem.Post;
      end;

      ednodcard.Text:='';
      edclient.Text:='';

      taSellItem.Append;
      if pc2.ActivePage=tshRet then dgRet.SetFocus
      else  edSearch.SetFocus;

      if dm.SellRet then
      begin
          with dm, quSellElDel do
          begin
            SQL.Text := 'update SellElem set IsNotProve = 1 where Uid =' +  quSelledUID.AsString;
            ExecQuery;
            close;
          end;
          quSelled.Delete;
         ActiveControl:=edUid;
       end;
    end;
  dm3.update_operation(LogOperationID)  
end;

procedure TfmSellEdit.deEDAcceptDate(Sender: TObject; var ADate: TDateTime;
  var Action: Boolean);
begin
  with dm, quSelled, Params do
    try
      Screen.Cursor:=crSQLWait;
      Active:=False;
      RetDate1:=deBD.Date;
      RetDate2:=ADate+1;
      RetClientId:=lcRetClient.KeyValue;
      Open;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmSellEdit.deEDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    bbOpenSelledClick(nil);
end;

procedure TfmSellEdit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  IF (Key=VK_F12) or (Key=VK_ESCAPE) then
  begin
    bbCnl.Click;
  end;
end;

procedure TfmSellEdit.lcbxRetCauseNotInList(Sender: TObject;
  NewText: String; var RecheckInList: Boolean);
begin
  if MessageDialog('�������� ������� �������� '+NewText+' � ����������?', mtConfirmation,
    [mbYes, mbNo], 0) <> mrYes then
  begin
    ActiveControl := lcbxRetCause;
    eXit;
  end;
  dmCom.taRet.Insert;
  dmCom.taRetD_RETID.AsString := IntToStr(dmCom.GetId(35));
  dmCom.taRetRET.AsString := NewText;
  dmCom.taRet.Post;
  lcbxRetCause.Text := NewText;
end;

procedure TfmSellEdit.lcbxRetCauseKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN :
      if dm.SellRet then // ����� ������ �������
      begin
        lcbxRetCause.CloseUp(True);
        ActiveControl := bbOk;
      end;
  end;
end;

procedure TfmSellEdit.dbehretclientKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
 case Key of
    VK_RETURN :
      if dm.SellRet then // ����� �������� �������
      begin
        ActiveControl := dbehRetaddress;
        dbehRetaddress.SelectAll;
      end;
  end;
end;

procedure TfmSellEdit.ednodcardKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
with dm do
case key of
VK_RETURN:
 begin
   if (not dm.checkInv_RUseDep(dm.taSellListDEPID.AsInteger)) and (WorkMode = 'ALLSELL' ) then
       raise Exception.Create('������ ������������� ������� � ������� '#39 +
                  dmCom.Dep[dm.taSellListDEPID.AsInteger].SName + #39' �� ������');

  {����� ���������� �� ������ �����}
   quTmp.Close;
   if CenterDep then // ���� �� ���� ��������������
    begin
     quTmp.SQL.Text:='select clientid, name, nodcard from client where '+
                     ' fmain=1 and clientid<>-1000 and fdel=0 and nodcard='#39+DelRSpace(edNodCard.Text)+#39;
     quTmp.ExecQuery;
     if qutmp.Fields[0].AsInteger=0 then
      begin
       quTmp.Close;
       quTmp.SQL.Text:='select first 1 clientid, name, nodcard from client where '+
                       ' clientid<>-1000 and fdel=0 and nodcard='#39+DelRSpace(edNodCard.Text)+#39;
       quTmp.ExecQuery;
      end;
    end
   else // ���� ������ ��� �������� �������������
    begin
     quTmp.SQL.Text:='select clientid, name, nodcard from client where clientid<>-1000 and fdel=0 and depid=gen_id(selfdepid, 0) and nodcard='#39+DelRSpace(edNodCard.Text)+#39;
     quTmp.ExecQuery;
    end;
   if qutmp.Fields[0].AsInteger=0 then
   begin
    dm2.quClient.Insert;
    dm2.quClientNODCARD.AsString:= edNodCard.Text;

    if ShowAndFreeForm(TfmeClient, Self, TForm(fmeClient), True, False)<> mrOk then
     begin
       edNodCard.Text:= oldnodcard;
       edClient.Text:= oldclientname;
       ActiveControl:= edNodCard;
       edNodCard.SelectAll;
       dm2.quClient.Cancel;
     end
     else
      begin
       dm2.quClient.Post;
       newidclient := dm2.quClientCLIENTID.AsInteger;
       newclientname := dm2.quClientNAME.AsString;
       newnodcard := dm2.quClientNODCARD.AsString;
       edNodCard.Text:= newnodcard;
       edClient.Text:= newclientname;
       ActiveControl:= bbOk;
      end
    end
     else
     begin
      newidclient := quTmp.Fields[0].AsInteger;
      newclientname := quTmp.Fields[1].AsString;
      newnodcard := quTmp.Fields[2].AsString;
      edNodCard.Text := quTmp.Fields[2].AsString;
      edClient.Text := quTmp.Fields[1].AsString;
      ActiveControl:= bbOk;
     end;
    qutmp.Close;

 end;
end;

end;

procedure TfmSellEdit.ednodcardKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '0'..'9','/',#8 : ;
 '�', '�': CanselFl:=true;
 else sysUtils.Abort;
 end;
end;

procedure TfmSellEdit.ednodcardKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if CanselFl
  then begin edNodCard.Text:='��� ����. �����'; CanselFl:=false; end;
end;

procedure TfmSellEdit.dbehRetaddressKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case Key of
    VK_RETURN :
      if dm.SellRet then // ����� �������� �������
      begin
        ActiveControl := lcbxRetCause;
      end;
  end;
end;

procedure TfmSellEdit.btHelpClick(Sender: TObject);
begin
 if dm.SellRet then Application.HelpContext(100241)
 else Application.HelpContext(100232)
end;

end.

