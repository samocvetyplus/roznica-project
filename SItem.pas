unit SItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Grids, DBGrids, RXDBCtrl, ExtCtrls,
  DBCtrls, StdCtrls, Buttons, Menus, DB, M207Grid, M207IBGrid, RxMenus,
  DBGridEh, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmSItem = class(TForm)
    fs1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siExit: TSpeedItem;
    sb1: TStatusBar;
    pmSItem: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    siAdd: TSpeedItem;
    SpeedItem2: TSpeedItem;
    N3: TMenuItem;
    N4: TMenuItem;
    spitTag: TSpeedItem;
    N5: TMenuItem;
    paItBuf: TPanel;
    tb2: TSpeedBar;
    dgItBuf: TM207IBGrid;
    N6: TMenuItem;
    miBuf: TMenuItem;
    sp1: TSplitter;
    SpeedbarSection1: TSpeedbarSection;
    siInBuf: TSpeedItem;
    siFromBuf: TSpeedItem;
    SpeedItem1: TSpeedItem;
    ppPrint: TRxPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    dg1: TDBGridEh;
    siHelp: TSpeedItem;
    procedure ToolButton3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure sbAddSItemClick(Sender: TObject);
    procedure siDelSItemClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spitTagClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure miBufClick(Sender: TObject);
    procedure siInBufClick(Sender: TObject);
    procedure siFromBufClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1KeyPress(Sender: TObject; var Key: Char);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siHelpClick(Sender: TObject);
  private
    { Private declarations }
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
    procedure UpdateQW;
  end;

var
  fmSItem: TfmSItem;

implementation

uses Data, comdata, CopySIt, ReportData, M207Proc, Data2, goods_sam, Data3;

{$R *.DFM}

procedure TfmSItem.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmSItem.ToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfmSItem.FormActivate(Sender: TObject);
begin
  with dm, taSItem do
    begin
      Active:=True;
      dg1.SelectedIndex:=2;
      if WChar<>#0 then
        begin
          Insert;
          taSItemW.AsString:=WChar;
        end;
    end;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taSItem, taItBuf]);
      CloseDataSets([taSItem, taItBuf]);
      taSEl.Refresh;
      WChar:=#0;
      taGoodsSam.Close;
    end;
end;

procedure TfmSItem.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  UpdateQW;
  dmCom.taGoodsSam.Open;
  ActiveControl:= dg1;
  dg1.SelectedIndex:=3;
  dmcom.FillTagMenu(ppPrint, spitTagClick, 0);
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  dg1.FindFieldColumn('WEIGHT$INSERTION').Visible := dm.taSListISTOLLING.AsInteger = 1;
  
  dg1.FindFieldColumn('COST$INSERTION').Visible := dm.taSListISTOLLING.AsInteger = 1;
end;

procedure TfmSItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSItem.sbAddSItemClick(Sender: TObject);
var i: integer;
    LogOperationID: string;
begin
 {log}
 LogOperationID:=dm3.insert_operation('�������� �������',LogOprIdForm);
 {***}

  with dm, taSItem do
    begin
      Append;
      taSItemSS.AsString:='-';
      with dg1 do
        begin
          i:=0;
          while Columns[i].Field.FieldName<>'W' do Inc(i);
          SelectedIndex:=i;
        end
    end;

 {log}
 dm3.update_operation(LogOperationID);
 {***}
end;

procedure TfmSItem.siDelSItemClick(Sender: TObject);
var LogOperationID:string;
begin
 {log}
 LogOperationID:=dm3.insert_operation('������� �������',LogOprIdForm);
 {***}
  with dm do
    begin
      taSItem.Delete;
    end;
 {log}
 dm3.update_operation(LogOperationID);
 {***}
end;

procedure TfmSItem.UpdateQW;
begin
  with dm, quElQW do
    begin
      Params[0].AsInteger:=taSElSElId.AsInteger;
      ExecQuery;
      sb1.Panels[0].Text:='���-��: '+Fields[0].AsString;
      sb1.Panels[1].Text:='���: '+Fields[1].AsString;
      Close;
    end;
end;


procedure TfmSItem.N3Click(Sender: TObject);
var w: extended;
    sz: string[20];
    pr1, pr2:integer;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('����������� ���� �������',LogOprIdForm);
  with dm do
    begin
      PostDataSets([taSItem]);
      if NOT taSItem.Fields[0].IsNull then
        begin
          w:=taSItemW.AsFloat;
          sz:=taSItemSS.AsString;
          pr1:=taSItemD_GOODS_SAM1.AsInteger;
          pr2:=taSItemD_GOODS_SAM2.AsInteger;
          taSItem.Append;
          taSItemW.AsFloat:=w;
          taSItemSS.AsString:=sz;
          taSItemD_GOODS_SAM1.AsInteger:=pr1;
          taSItemD_GOODS_SAM2.AsInteger:=pr2;
          taSItem.Post;
        end;
    end;
   dm3.update_operation(LogOperationID); 
end;

procedure TfmSItem.N4Click(Sender: TObject);
var SItemId: integer;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('����������� ��������� �������',LogOprIdForm);
  with dm do
    begin
      PostDataSets([taSItem]);
      if NOT taSItem.Fields[0].IsNull then
        begin
          fmCopySItem:=TfmCopySItem.Create(Application);
          if fmCopySItem.ShowModal=mrOK then
            begin
              SItemId:=taSItemSItemId.AsInteger;
              taSItem.Active:=False;
              with quTmp do
                begin
                  SQL.Text:='execute procedure CopySItem '+IntToStr(SItemId)+', '+IntToStr(Round(fmCopySItem.se1.Value));
                  ExecQuery;
                end;
              taSItem.Active:=True;
              UpdateQW;
            end;
          fmCopySItem.Free;
        end;
    end;
  dm3.update_operation(LogOperationID);  
end;

procedure TfmSItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;  
end;

procedure TfmSItem.spitTagClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:= gen_arr(dg1, dm.taSItemSItemId);
  try
    PrintTag(arr, it_tag,TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSItem.N5Click(Sender: TObject);
begin
  with dm.taSItem do
    begin
      Active:=False;
      SelectSQL[SelectSQL.Count-1]:='ORDER BY SZ';
      Active:=True;
    end;
end;

procedure TfmSItem.miBufClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�����',LogOprIdForm);
  with miBuf, dm2 do
    begin
      Checked:=NOT Checked;
      paItBuf.Visible:=Checked;
      sp1.Visible:=Checked;
      if Checked then OpenDataSets([taItBuf])
      else
        begin
          PostDataSets([taItBuf]);
          CloseDataSets([taItBuf]);
        end;
    end;
  dm3.update_operation(LogOperationID);  
end;

procedure TfmSItem.siInBufClick(Sender: TObject);
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taSItem]);
      taItBuf.Append;
      taItBuf.Post;
      with taSItem do
        begin
          Tag:=1;
          Delete;
          Tag:=0;
        end;
    end;
end;

procedure TfmSItem.siFromBufClick(Sender: TObject);
begin
  with dmCom, dm, dm2  do
    if NOT taItBufItBufId.IsNull and (taItBufArt2Id.AsInteger=taSElArt2Id.AsInteger) then
      begin
        NotCalcUID:=True;
        taSItem.Insert;
        NotCalcUID:=False;

        taSItemUID.AsInteger:=taItBufUID.AsInteger;
        taSItemSS.AsString:=taItBufSZ.AsString;
        taSItemW.AsFloat:=taItBufW.AsFloat;

        taItBuf.Delete;
      end;
end;

procedure TfmSItem.SpeedItem1Click(Sender: TObject);
begin
  dm.ClearItBuf;
end;

procedure TfmSItem.N8Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� ���. ����������',LogOprIdForm);
//  ShowAndFreeForm(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False);
  b:=GetBit(dmCom.EditRefBook, 2) and CenterDep;
  ShowAndFreeFormEnabled(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False, b,'siExit;tb1;spitPrint;siSort;','dg1;');
  dmCom.taGoodsSam.Open;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSItem.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i, si: integer;
begin
  case Key of
      VK_RETURN: if Shift=[ssCtrl] then Close
               else if Shift=[] then
                 with dm do
                   begin
                     PostDataSets([taSItem]);
                     with taSItem do
                       try
                         DisableControls;
                         si:=dg1.SelectedIndex;
                         i:=taSItemSItemId.AsInteger;
                         Active:=False;
                         SelectSQL[SelectSQL.Count-1]:='ORDER BY SZ';
                         Active:=True;
                         Locate('SITEMID', i, []);
                         dg1.SelectedIndex:=si;
                       finally
                         EnableControls;
                       end;
                   end;
  VK_RIGHT: if (dg1.SelectedIndex <5) and (dg1.InplaceEditor.EditText='-')  then
  begin
   dg1.SelectedIndex:=dg1.SelectedIndex+1;
   dg1.InplaceEditor.SelectAll;
  end;
 end;

end;

procedure TfmSItem.dg1KeyPress(Sender: TObject; var Key: Char);
begin
  if dg1.SelectedField.FieldName='SZ' then
    if NOT (Key in ['0'..'9', '.' ,'-', #8]) then SysUtils.Abort;

end;

procedure TfmSItem.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if (Column.Field.FieldName='RecNo') or (Column.Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmSItem.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100203);
end;

end.
