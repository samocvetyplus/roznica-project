unit VibCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ActnList;

type
  TfmVibcard = class(TForm)
    LOldnodcard: TLabel;
    Lnewnodcard: TLabel;
    ValOldNodcard: TLabel;
    edNewNodCard: TEdit;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    acList: TActionList;
    acEnter: TAction;
    acEsc: TAction;
    procedure acEscExecute(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmVibcard: TfmVibcard;

implementation

uses data, comdata, DBUtil, DB, data2, MsgDialog;

{$R *.dfm}

procedure TfmVibcard.acEscExecute(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TfmVibcard.acEnterExecute(Sender: TObject);
var res:boolean;
begin
 if ActiveControl = btCancel then ModalResult:=mrCancel
 else if ActiveControl = edNewNodCard then ActiveControl:=btOk
 else if ActiveControl = btOk then
 begin
  if edNewNodCard.Text='' then
  begin
   MessageDialog('������� ����� ����� ���������� �����', mtWarning, [mbOk], 0);
   ActiveControl:=edNewNodCard;
   SysUtils.Abort;
  end;
   dm.quTmp.Close;
   if dm.IsClient=0 then
    dm.quTmp.SQL.Text:='select clientid from client where nodcard='#39+edNewNodCard.Text+#39' and '+
                       ' clientid<>'+dmcom.taClientCLIENTID.AsString+' and fdel=0 '
   else if dm.IsClient=1 then
    dm.quTmp.SQL.Text:='select clientid from client where nodcard='#39+edNewNodCard.Text+#39' and '+
                       ' clientid<>'+dm2.quClientCLIENTID.AsString+' and fdel=0 ';
   dm.quTmp.ExecQuery;
   if dm.quTmp.Fields[0].IsNull then res:=true else res:=false;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
   if res then ModalResult:=mrOk
   else begin
    MessageDialog('����. ����� � ��������� ������� ��� ����������!', mtWarning, [mbOk], 0);
    ActiveControl:=edNewNodCard;
   end

 end
end;

procedure TfmVibcard.FormClose(Sender: TObject; var Action: TCloseAction);
var  oldnodcard, address, flat, name:string;
     birthday:TDateTime;
     addressid, oldclientid, max_fdel:integer;
begin
 if ModalResult=mrok then
 begin
   dm.quTmp.Close;
   if dm.isClient=0 then
    dm.quTmp.SQL.Text:='select max(fdel) from client where nodcard='#39+dmcom.taClientNODCARD.AsString+#39
   else if dm.isClient=1 then
    dm.quTmp.SQL.Text:='select max(fdel) from client where nodcard='#39+dm2.quClientNODCARD.AsString+#39;
   dm.quTmp.ExecQuery;
   if dm.quTmp.Fields[0].IsNull then max_fdel:=1 else max_fdel:=dm.quTmp.Fields[0].AsInteger+1;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;

  if dm.IsClient=0 then
  begin
   ExecSQL('update client set fdel='+inttostr(max_fdel)+' where clientid='+dmcom.taClientCLIENTID.AsString, dm.quTmp);
   oldclientid:=dmcom.taClientCLIENTID.AsInteger;
   oldnodcard:=dmcom.taClientNODCARD.AsString;
   address:=dmcom.taClientADDRESS.AsString;
   flat:=dmcom.taClientHOME_FLAT.AsString;
   birthday:=dmcom.taClientBIRTHDAY.AsDateTime;
   addressid:=dmcom.taClientADDRESSID.AsInteger;
   name:=dmcom.taClientNAME.AsString;
   dmcom.taClient.Insert;
   dmcom.taClientNODCARD.AsString:=edNewNodCard.Text;
   dmcom.taClientOLDNODCARD.AsString:=oldnodcard;
   dmcom.taClientADDRESS.AsString:=address;
   dmcom.taClientHOME_FLAT.AsString:=flat;
   dmcom.taClientBIRTHDAY.AsDateTime:=birthday;
   dmcom.taClientADDRESSID.AsInteger:=addressid;
   dmcom.taClientNAME.AsString:=name;
   dmcom.taClientF2.AsInteger:=0;
   dmcom.taClientF3.AsInteger:=0;
   dmcom.taClient.Post;
   ExecSQL('update client set newclientid='+dmcom.taClientCLIENTID.AsString+' where clientid='+inttostr(oldclientid), dm.quTmp);
   ExecSQL('update sellitem set clientid='+dmcom.taClientCLIENTID.AsString+' where clientid='+inttostr(oldclientid), dm.quTmp);
   ExecSQL('execute procedure Calc_sum_sell ('+dmcom.taClientCLIENTID.AsString+', 0)', dm.quTmp);
   ExecSQL('execute procedure Calc_sum_sell ('+inttostr(oldclientid)+', 0)', dm.quTmp);
   dmcom.taClient.Refresh;
   oldnodcard:=dmcom.taClientNODCARD.AsString;
   ReOpenDataSet(dmcom.taClient);
   dmcom.taClient.Locate('NODCARD', oldnodcard, []);
  end
  else if dm.IsClient=1 then
  begin
   ExecSQL('update client set fdel='+inttostr(max_fdel)+' where clientid='+dm2.quClientCLIENTID.AsString, dm.quTmp);
   oldclientid:= dm2.quClientCLIENTID.AsInteger;
   oldnodcard:=dm2.quClientNODCARD.AsString;
   address:=dm2.quClientADDRESS.AsString;
   flat:=dm2.quClientHOME_FLAT.AsString;
   birthday:=dm2.quClientBIRTHDAY.AsDateTime;
   addressid:=dm2.quClientADDRESSID.AsInteger;
   name:=dm2.quClientNAME.AsString;
   if dm2.quClient.State in [dsEdit, dsInsert] then dm2.quClient.Post;
   dm2.quClient.Insert;
   dm2.quClientNODCARD.AsString:=edNewNodCard.Text;
   dm2.quClientOLDNODCARD.AsString:=oldnodcard;
   dm2.quClientADDRESS.AsString:=address;
   dm2.quClientHOME_FLAT.AsString:=flat;
   dm2.quClientBIRTHDAY.AsDateTime:=birthday;
   dm2.quClientADDRESSID.AsInteger:=addressid;
   dm2.quClientNAME.AsString:=name;
   dm2.quClient.Post;
   ExecSQL('update client set newclientid='+dm2.quClientCLIENTID.AsString+' where clientid='+inttostr(oldclientid), dm.quTmp);
   ExecSQL('update sellitem set clientid='+dm2.quClientCLIENTID.AsString+' where clientid='+inttostr(oldclientid), dm.quTmp);
   ExecSQL('execute procedure Calc_sum_sell ('+dm2.quClientCLIENTID.AsString+', 0)', dm.quTmp);
   ExecSQL('execute procedure Calc_sum_sell ('+inttostr(oldclientid)+', 0)', dm.quTmp);
   dm2.quClient.Refresh;
  end
 end
end;

procedure TfmVibcard.FormCreate(Sender: TObject);
begin
 if dm.IsClient=0 then ValOldNodcard.Caption:=dmcom.taClientNODCARD.AsString
 else if dm.IsClient=1then ValOldNodcard.Caption:=dm2.quClientNODCARD.AsString
end;

procedure TfmVibcard.btOkClick(Sender: TObject);
var res:boolean;
begin
  if edNewNodCard.Text='' then begin
   MessageDialog('������� ����� ����� ���������� �����', mtWarning, [mbOk], 0);
   ActiveControl:=edNewNodCard;
   SysUtils.Abort;
  end;
   dm.quTmp.Close;
   if dm.IsClient=0 then
    dm.quTmp.SQL.Text:='select clientid from client where nodcard='#39+edNewNodCard.Text+#39' and '+
                       ' clientid<>'+dmcom.taClientCLIENTID.AsString+' and fdel=0 '
   else if dm.IsClient=1 then
    dm.quTmp.SQL.Text:='select clientid from client where nodcard='#39+edNewNodCard.Text+#39' and '+
                       ' clientid<>'+dm2.quClientCLIENTID.AsString+' and fdel=0 ';
   dm.quTmp.ExecQuery;
   if dm.quTmp.Fields[0].IsNull then res:=true else res:=false;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
   if not res then begin
    MessageDialog('����. ����� � ��������� ������� ��� ����������!', mtWarning, [mbOk], 0);
    ActiveControl:=edNewNodCard;
   end
   else ModalResult:=mrOk;
end;

end.
