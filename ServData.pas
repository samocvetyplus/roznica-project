unit ServData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, pFIBDataSet, pFIBStoredProc, FR_DSet, FR_DBSet, FR_Class,
  Menus, IdBaseComponent, IdComponent,
  IdTCPServer, ScktComp, xmldom, XMLIntf, msxmldom, XMLDoc, Math, FIBQuery,
  pFIBQuery, FIBDataSet, Winsock, ComDrv32, dbUtil, Variants, Consts,
  AppEvnts;

type
  TFilter = record
    isActivate : boolean;
    Prod1 : integer;
    Prod2 : integer;
    Mat1 : string;
    Mat2 : string;
    Good1 : string;
    Good2 : string;
  end;

  TSellItemInfo = record
    Id : integer;
    Action : string;
  end;

const
  cCurrFormat = '### ### ##0.00';

type
  TdmServ = class(TDataModule)
    quWHA2UID: TpFIBDataSet;
    quWHA2UIDSITEMID: TIntegerField;
    quWHA2UIDUID: TIntegerField;
    quWHA2UIDW: TFloatField;
    quWHA2UIDPRICE2: TFloatField;
    quWHA2UIDSZ: TFIBStringField;
    quWHA2UIDSUP: TFIBStringField;
    quWHA2UIDSUPID: TIntegerField;
    quWHA2UIDNDSID: TIntegerField;
    quWHA2UIDNDSNAME: TFIBStringField;
    quWHA2UIDDEPID: TIntegerField;
    quWHA2UIDSINFOID: TIntegerField;
    quWHA2UIDPRICE: TFloatField;
    quWHA2UIDSSF: TFIBStringField;
    quWHA2UIDSDATE: TDateTimeField;
    quWHA2UIDSN: TIntegerField;
    quWHSZ: TpFIBDataSet;
    dsrWHA2UID: TDataSource;
    dsrWHSZ: TDataSource;
    taWH: TpFIBDataSet;
    dsrWH: TDataSource;
    quWHArt2: TpFIBDataSet;
    quWHArt2ART2ID: TIntegerField;
    quWHArt2ART2: TFIBStringField;
    quWHArt2WEIGHT: TFloatField;
    quWHArt2QUANTITY: TIntegerField;
    quWHArt2PRICE2: TFloatField;
    dsrWHArt2: TDataSource;
    quWHUID: TpFIBDataSet;
    dsWHUID: TDataSource;
    qrSupInf: TpFIBDataSet;
    dsrSupInf: TDataSource;
    qrSupInfR_SUP: TFIBStringField;
    qrSupInfR_SUM: TFloatField;
    qrSupInfR_SUPCODE: TFIBStringField;
    quWHArt2D_DEPID: TIntegerField;
    qrWHInv: TpFIBDataSet;
    dsrWHInv: TDataSource;
    frReport: TfrReport;
    qrAnalitica: TpFIBDataSet;
    dsrAnalitica: TDataSource;
    qrAnaliticaD_ARTID: TIntegerField;
    qrAnaliticaD_COMPID: TIntegerField;
    qrAnaliticaD_MATID: TFIBStringField;
    qrAnaliticaD_GOODID: TFIBStringField;
    qrAnaliticaART: TFIBStringField;
    qrAnaliticaUNITID: TIntegerField;
    qrAnaliticaMEMO: TMemoField;
    qrAnaliticaPICT: TBlobField;
    qrAnaliticaD_INSID: TFIBStringField;
    qrAnaliticaFULLART: TFIBStringField;
    quProducer: TpFIBDataSet;
    quProducerD_COMPID: TIntegerField;
    quProducerCODE: TFIBStringField;
    quProducerSNAME: TFIBStringField;
    dsrProducer: TDataSource;
    quHistItem: TpFIBDataSet;
    dsrHistItem: TDataSource;
    quHistArt: TpFIBDataSet;
    dsrHistArt: TDataSource;
    quHistArtDescr: TStringField;
    quHistArtR_SN: TIntegerField;
    quHistArtR_SDATE: TDateTimeField;
    quHistArtR_W: TFloatField;
    quHistArtR_ART2: TFIBStringField;
    quHistArtR_C: TIntegerField;
    quHistArtR_I: TIntegerField;
    quHistArtR_SUPID: TIntegerField;
    quHistArtR_SUPNAME: TFIBStringField;
    quHistArtR_PRICE: TFloatField;
    quHistArtR_PRICE2: TFloatField;
    quHistArtR_DEPID: TIntegerField;
    quHistArtR_DEPNAME: TFIBStringField;
    quHistItemR_UID: TIntegerField;
    quHistItemR_W: TFloatField;
    quHistItemR_SZ: TFIBStringField;
    quHistArtLINK: TIntegerField;
    quHistArtART2ID: TIntegerField;
    quHistItemR_ART2: TFIBStringField;
    quHistItemR_ART: TFIBStringField;
    quHistItemR_UNITID: TIntegerField;
    quHistItemR_COMPID: TIntegerField;
    quHistItemR_PRICE: TFloatField;
    quHistItemR_PRICE2: TFloatField;
    quHistItemR_CURRPRICE: TFloatField;
    quHistItemR_COMPNAME: TFIBStringField;
    quHistItemR_PRICE2OLD: TFloatField;
    taRetCltList: TpFIBDataSet;
    dsRetCltList: TDataSource;
    pmRetClt: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    taRetCltListSINVID: TIntegerField;
    taRetCltListSITEMID: TIntegerField;
    taRetCltListART2ID: TIntegerField;
    taRetCltListDEPID: TIntegerField;
    taRetCltListDEP: TFIBStringField;
    taRetCltListSDATE: TDateTimeField;
    taRetCltListSN: TIntegerField;
    taRetCltListPNDS: TFloatField;
    taRetCltListNDSNAME: TFIBStringField;
    taRetCltListISCLOSED: TSmallintField;
    taRetCltListCLIENTID: TIntegerField;
    taRetCltListCLIENTNAME: TFIBStringField;
    taRetCltListUSERID: TIntegerField;
    taRetCltListFIO: TFIBStringField;
    taRetCltListUID: TIntegerField;
    taRetCltListW: TFloatField;
    taRetCltListSZ: TFIBStringField;
    taRetCltListART: TFIBStringField;
    taRetCltListART2: TFIBStringField;
    taRetCltListFULLART: TFIBStringField;
    taRetCltListD_RETID: TFIBStringField;
    taRetCltListRET: TFIBStringField;
    taRetCltListPRICE2: TFloatField;
    taRetCltListPRICE2OLD: TFloatField;
    taRetCltListSPRICE: TFloatField;
    taRetCltListCURRPRICE: TFloatField;
    taRetCltListCOST2: TFloatField;
    taRetCltListSELLN: TIntegerField;
    taTSumD: TpFIBDataSet;
    dsrTSumD: TDataSource;
    taTSumDID: TIntegerField;
    taTSumDIDATE: TDateTimeField;
    taTSumDINC: TFloatField;
    taTSumDINW: TFloatField;
    taTSumDINQ: TIntegerField;
    taTSumDINVINC: TFloatField;
    taTSumDINVW: TFloatField;
    taTSumDINVQ: TIntegerField;
    taTSumDINVOSTC: TFloatField;
    taTSumDINVOSTW: TFloatField;
    taTSumDINVOSTQ: TIntegerField;
    taTSumDSLINC: TFloatField;
    taTSumDSLOUTC: TFloatField;
    taTSumDSLW: TFloatField;
    taTSumDSLQ: TIntegerField;
    taTSumDOPTINC: TFloatField;
    taTSumDOPTOUTC: TFloatField;
    taTSumDOPTW: TFloatField;
    taTSumDOPTQ: TIntegerField;
    taTSumDRETOPTINC: TFloatField;
    taTSumDRETOPTRW: TFloatField;
    taTSumDRETOPTRQ: TIntegerField;
    taTSumDRETINC: TFloatField;
    taTSumDRETW: TFloatField;
    taTSumDRETQ: TIntegerField;
    taTSumDRETSINC: TFloatField;
    taTSumDRETSW: TFloatField;
    taTSumDRETSQ: TIntegerField;
    taTSumDOUTC: TFloatField;
    taTSumDOUTW: TFloatField;
    taTSumDOUTQ: TIntegerField;
    quAct: TpFIBDataSet;
    quTmp: TpFIBQuery;
    taAppl: TpFIBDataSet;
    dsrAppl: TDataSource;
    taApplUNITID: TIntegerField;
    taApplD_INSID: TFIBStringField;
    taApplD_MATID: TFIBStringField;
    taApplD_GOODID: TFIBStringField;
    taApplD_COMPID: TIntegerField;
    quArtByUId: TpFIBQuery;
    quHistArt1: TpFIBDataSet;
    dsrHistArt1: TDataSource;
    quHistArt1R_SN: TIntegerField;
    quHistArt1R_SDATE: TDateTimeField;
    quHistArt1R_W: TFloatField;
    quHistArt1R_ART2: TFIBStringField;
    quHistArt1R_C: TIntegerField;
    quHistArt1R_I: TIntegerField;
    quHistArt1R_SUPID: TIntegerField;
    quHistArt1R_SUPNAME: TFIBStringField;
    quHistArt1R_PRICE: TFloatField;
    quHistArt1R_PRICE2: TFloatField;
    quHistArt1R_DEPID: TIntegerField;
    quHistArt1R_DEPNAME: TFIBStringField;
    quHistArt1LINK: TIntegerField;
    quHistArt1ART2ID: TIntegerField;
    quHistArt1Descr: TStringField;
    dsrList3: TDataSource;
    taList3: TpFIBDataSet;
    taList3ARTID: TIntegerField;
    taList3ART: TFIBStringField;
    taList3FULLART: TFIBStringField;
    taList3D_COMPID: TIntegerField;
    taList3D_MATID: TFIBStringField;
    taList3D_GOODID: TFIBStringField;
    taList3UNITID: TIntegerField;
    taList3D_INSID: TFIBStringField;
    taList3INS: TFloatField;
    taList3INW: TFloatField;
    taList3INQ: TIntegerField;
    taList3OUTS: TFloatField;
    taList3OUTW: TFloatField;
    taList3DEBTORS: TFloatField;
    taList3DEBTORW: TFloatField;
    taList3DEBTORQ: TIntegerField;
    taList3SALES: TFloatField;
    taList3SALEW: TFloatField;
    taList3SALEQ: TIntegerField;
    taList3RETS: TFloatField;
    taList3RETW: TFloatField;
    taList3RETQ: TIntegerField;
    taList3RETOPTS: TFloatField;
    taList3RETOPTW: TFloatField;
    taList3RETOPTQ: TIntegerField;
    taList3RETVENDORS: TFloatField;
    taList3RETVENDORW: TFloatField;
    taList3RETVENDORQ: TIntegerField;
    taList3OPTS: TFloatField;
    taList3OPTW: TFloatField;
    quJTmp: TpFIBQuery;
    taList3OUTQ: TIntegerField;
    taList3OPTQ: TIntegerField;
    taJSz: TpFIBDataSet;
    dsrJSz: TDataSource;
    taList3SHINS: TFIBStringField;
    taList3SHINW: TFIBStringField;
    taList3SHINQ: TFIBStringField;
    taList3SHOUTW: TFIBStringField;
    taList3SHOUTS: TFIBStringField;
    taList3SHOUTQ: TFIBStringField;
    taList3SHRETS: TFIBStringField;
    taList3SHRETW: TFIBStringField;
    taList3SHRETQ: TFIBStringField;
    taList3SHSALES: TFIBStringField;
    taList3SHSALEQ: TFIBStringField;
    taList3SHSALEW: TFIBStringField;
    taList3SHMOVETOS: TFIBStringField;
    taList3SHMOVETOW: TFIBStringField;
    taList3SHMOVETOQ: TFIBStringField;
    taList3SHMOVEFROMS: TFIBStringField;
    taList3SHMOVEFROMW: TFIBStringField;
    taList3SHMOVEFROMQ: TFIBStringField;
    quWH_S1: TpFIBQuery;
    quWH_S2: TpFIBQuery;
    pmList3: TPopupMenu;
    mnitAllDep: TMenuItem;
    N4: TMenuItem;
    taList3ID: TIntegerField;
    taList3DEPID1: TIntegerField;
    taList3DEPID2: TIntegerField;
    taList3SNAME: TFIBStringField;
    quHistArt1R_ART: TFIBStringField;
    quHistArt1R_MATID: TFIBStringField;
    quHistArt1R_GOODID: TFIBStringField;
    quWH_T: TpFIBDataSet;
    quWH_TTQ: TFIBStringField;
    quWH_TTA: TFIBStringField;
    dsrWH_T: TDataSource;
    frdsrList3: TfrDBDataSet;
    taWHD_ARTID: TIntegerField;
    taWHART: TFIBStringField;
    taWHUNITID: TIntegerField;
    taWHD_COMPID: TIntegerField;
    taWHCOMPCODE: TFIBStringField;
    taWHMAT: TFIBStringField;
    taWHGOOD: TFIBStringField;
    taWHINS: TFIBStringField;
    taWHQ: TFloatField;
    taWHW: TFloatField;
    taWHTW: TFIBStringField;
    taWHTQ: TFIBStringField;
    frdsrJSz: TfrDBDataSet;
    quWH_TTW: TFIBStringField;
    quUIDWH_T: TpFIBDataSet;
    dsrUIDWH_T: TDataSource;
    quHistArtSum: TpFIBDataSet;
    dsrHistArtSum: TDataSource;
    quHistArtSumTD: TFIBStringField;
    quHistArtSumTS: TFIBStringField;
    quHistArtSumTR: TFIBStringField;
    quHistArtSumTRET: TFIBStringField;
    quHistArtSumTC: TStringField;
    quHistArt1Sum: TpFIBDataSet;
    IBStringField1: TFIBStringField;
    IBStringField2: TFIBStringField;
    IBStringField3: TFIBStringField;
    IBStringField4: TFIBStringField;
    StringField1: TStringField;
    dsrHistArt1Sum: TDataSource;
    quUIDWH_TSNAME: TFIBStringField;
    quUIDWH_TCOLOR: TIntegerField;
    quUIDWH_TC: TIntegerField;
    quUIDWH_TSAC: TIntegerField;
    quUIDWH_TSTW: TFloatField;
    quUIDWH_TSTP: TFloatField;
    quUIDWH_TSTP2: TFloatField;
    taWHNDSID: TIntegerField;
    taWHNDSNAME: TStringField;
    taRetCltListRSTATE: TSmallintField;
    qrWHInvSW: TFloatField;
    qrWHInvSC: TIntegerField;
    qrWHInvART2: TFIBStringField;
    qrWHInvPRICE2: TFloatField;
    qrWHInvSPRICE: TFloatField;
    qrWHInvSDATE0: TDateTimeField;
    qrWHInvSSF0: TFIBStringField;
    qrWHInvSN0: TIntegerField;
    taRetCltListRETPRICE: TFloatField;
    taWHCOUNTRYID: TFIBStringField;
    taJUID: TpFIBDataSet;
    dsrJUID: TDataSource;
    frdsrJUID: TfrDBDataSet;
    taJUIDARTID: TIntegerField;
    taJUIDART: TFIBStringField;
    taJUIDFULLART: TFIBStringField;
    taJUIDD_COMPID: TIntegerField;
    taJUIDD_MATID: TFIBStringField;
    taJUIDD_GOODID: TFIBStringField;
    taJUIDUNITID: TSmallintField;
    taJUIDD_INSID: TFIBStringField;
    taJUIDINS: TFloatField;
    taJUIDINW: TFloatField;
    taJUIDINQ: TIntegerField;
    taJUIDOUTS: TFloatField;
    taJUIDOUTW: TFloatField;
    taJUIDOUTQ: TIntegerField;
    taJUIDDEBTORS: TFloatField;
    taJUIDDEBTORW: TFloatField;
    taJUIDDEBTORQ: TIntegerField;
    taJUIDSALES: TFloatField;
    taJUIDSALEW: TFloatField;
    taJUIDSALEQ: TIntegerField;
    taJUIDRETS: TFloatField;
    taJUIDRETW: TFloatField;
    taJUIDRETQ: TIntegerField;
    taJUIDRETOPTS: TFloatField;
    taJUIDRETOPTW: TFloatField;
    taJUIDRETOPTQ: TIntegerField;
    taJUIDRETVENDORS: TFloatField;
    taJUIDRETVENDORW: TFloatField;
    taJUIDRETVENDORQ: TIntegerField;
    taJUIDOPTS: TFloatField;
    taJUIDOPTW: TFloatField;
    taJUIDOPTQ: TIntegerField;
    taJUIDSZ: TFIBStringField;
    taJUIDCOMP: TFIBStringField;
    taJUIDBD: TDateTimeField;
    taJUIDED: TDateTimeField;
    taJUIDSALESTR: TFIBStringField;
    taJUIDUID: TIntegerField;
    taJUIDUID_SUP: TIntegerField;
    taJUIDCURRQ: TIntegerField;
    taJSzSum: TpFIBDataSet;
    taApplList: TpFIBDataSet;
    dsrApplList: TDataSource;
    taApplQ: TIntegerField;
    taApplPR: TFloatField;
    taApplAITEMID: TIntegerField;
    taApplAPPLID: TIntegerField;
    taApplARTID: TIntegerField;
    taApplART: TFIBStringField;
    taWHZQ: TIntegerField;
    taApplSZ: TFIBStringField;
    quSupTmp: TpFIBQuery;
    taJSzSumARTID: TIntegerField;
    taJSzSumART: TFIBStringField;
    taJSzSumFULLART: TFIBStringField;
    taJSzSumD_COMPID: TIntegerField;
    taJSzSumD_MATID: TFIBStringField;
    taJSzSumD_GOODID: TFIBStringField;
    taJSzSumUNITID: TSmallintField;
    taJSzSumD_INSID: TFIBStringField;
    taJSzSumINS: TFloatField;
    taJSzSumINW: TFloatField;
    taJSzSumINQ: TIntegerField;
    taJSzSumOUTS: TFloatField;
    taJSzSumOUTW: TFloatField;
    taJSzSumOUTQ: TIntegerField;
    taJSzSumDEBTORS: TFloatField;
    taJSzSumDEBTORW: TFloatField;
    taJSzSumDEBTORQ: TIntegerField;
    taJSzSumSALES: TFloatField;
    taJSzSumSALEW: TFloatField;
    taJSzSumSALEQ: TIntegerField;
    taJSzSumRETS: TFloatField;
    taJSzSumRETW: TFloatField;
    taJSzSumRETQ: TIntegerField;
    taJSzSumRETOPTS: TFloatField;
    taJSzSumRETOPTW: TFloatField;
    taJSzSumRETOPTQ: TIntegerField;
    taJSzSumRETVENDORS: TFloatField;
    taJSzSumRETVENDORW: TFloatField;
    taJSzSumRETVENDORQ: TIntegerField;
    taJSzSumOPTS: TFloatField;
    taJSzSumOPTW: TFloatField;
    taJSzSumOPTQ: TIntegerField;
    taJSzSumSZ: TFIBStringField;
    taJSzSumCOMP: TFIBStringField;
    taJSzSumBD: TDateTimeField;
    taJSzSumED: TDateTimeField;
    taJSzSumSALESTR: TFIBStringField;
    taJSzSumCURRS: TFloatField;
    taJSzSumCURRW: TFloatField;
    taJSzSumCURRQ: TIntegerField;
    taJSzSumID: TIntegerField;
    taJSzSumZQ: TIntegerField;
    dsrJSzSum: TDataSource;
    quErr_Sell: TpFIBDataSet;
    dserr_sell: TDataSource;
    quErr_SellUID: TIntegerField;
    quErr_SellRN: TIntegerField;
    quErr_SellBD: TDateTimeField;
    quErr_SellED: TDateTimeField;
    quErr_SellBUSYTYPE: TIntegerField;
    quErr_SellSELLITEMID: TIntegerField;
    quErr_SellNAME: TFIBStringField;
    dsrTotJSz: TDataSource;
    taTotJSz: TpFIBDataSet;
    taTotJSzDEPNAME: TFIBStringField;
    taTotJSzSELLRANGE: TIntegerField;
    taTotJSzSELLQ: TIntegerField;
    taTotJSzSELLW: TFloatField;
    taTotJSzDEPID: TIntegerField;
    taTotJSzColor: TIntegerField;
    quWHArt2SNAME: TFIBStringField;
    quWHSZSZ: TFIBStringField;
    quWHSZQUANTITY: TIntegerField;
    quWHSZWEIGHT: TFloatField;
    quWHSZDEPID: TIntegerField;
    quWHSZSNAME: TFIBStringField;
    quPriceCheck: TpFIBQuery;
    quNewAppl: TpFIBQuery;
    XMLSell: TXMLDocument;
    quSellArt: TpFIBDataSet;
    quDiscount: TpFIBDataSet;
    quWHAnlz1: TpFIBQuery;
    taWH_SellDate: TpFIBDataSet;
    taGrIns: TpFIBDataSet;
    quComp: TpFIBQuery;
    quUidWH5: TpFIBQuery;
    dsrJDep: TDataSource;
    taJDep: TpFIBDataSet;
    taJDepNAME: TFIBStringField;
    taJDepD_DEPID: TIntegerField;
    taJDepSell_In: TFloatField;
    taJDepSell_InRet: TFloatField;
    taJDepSUM: TFloatField;
    taJDepSUM1: TFloatField;
    taJDepSUM2: TFloatField;
    taJDepSUM3: TFloatField;
    taJDepSUM4: TFloatField;
    taJDepColour: TIntegerField;
    taJDepSUM5: TFloatField;
    taJDepSellDep: TFloatField;
    taRetCltListSCOST: TFIBFloatField;
    taRetCltListRETCOST: TFIBFloatField;
    quInventory: TpFIBDataSet;
    dsInventory: TDataSource;
    quInventoryUID: TFIBIntegerField;
    quInventoryART2ID: TFIBIntegerField;
    quInventorySZ: TFIBStringField;
    quInventoryW: TFIBFloatField;
    quInventoryISIN: TFIBSmallIntField;
    quInventoryDEPID: TFIBIntegerField;
    quInventoryPRODCODE: TFIBStringField;
    quInventoryART: TFIBStringField;
    quInventoryART2: TFIBStringField;
    quInventoryD_MATID: TFIBStringField;
    quInventoryD_GOODID: TFIBStringField;
    quInventoryD_INSID: TFIBStringField;
    quInventorySNAME: TFIBStringField;
    quInventoryCOLOR: TFIBIntegerField;
    quInventoryNAME: TFIBStringField;
    quInventoryPRICE: TFIBFloatField;
    quInventoryDescription: TStringField;
    ComScan: TCommPortDriver;
    quInventoryINVENTORYID: TFIBIntegerField;
    quInventoryORDER_: TFIBIntegerField;
    ComScan2: TCommPortDriver;
    taMol: TpFIBDataSet;
    dsMol: TDataSource;
    taMolD_MOLID: TFIBIntegerField;
    taMolD_COMPID: TFIBIntegerField;
    taMolFIO: TFIBStringField;
    taMolD_DEPID: TFIBIntegerField;
    taMolCOLORSHOP: TFIBIntegerField;
    taMolPOST: TFIBStringField;
    taMolPRESEDENT: TFIBIntegerField;
    taMolMEMBER: TFIBIntegerField;
    quInventoryPRICE0: TFIBFloatField;
    ap1: TApplicationEvents;
    quListInventory: TpFIBDataSet;
    quListInventorySINVID: TFIBIntegerField;
    quListInventoryDEPID: TFIBIntegerField;
    quListInventorySDATE: TFIBDateTimeField;
    quListInventoryNDATE: TFIBDateTimeField;
    quListInventoryCRDATE: TFIBDateTimeField;
    quListInventorySN: TFIBIntegerField;
    quListInventoryISCLOSED: TFIBSmallIntField;
    quListInventoryITYPE: TFIBSmallIntField;
    quListInventoryUSERID: TFIBIntegerField;
    quListInventoryFIO: TFIBStringField;
    dsListInventory: TDataSource;
    quInvSinv: TpFIBDataSet;
    quInvSinvSINVID: TFIBIntegerField;
    quInvSinvDEPID: TFIBIntegerField;
    quInvSinvSDATE: TFIBDateTimeField;
    quInvSinvNDATE: TFIBDateTimeField;
    quInvSinvCRDATE: TFIBDateTimeField;
    quInvSinvSN: TFIBIntegerField;
    quInvSinvISCLOSED: TFIBSmallIntField;
    quInvSinvITYPE: TFIBSmallIntField;
    quInvSinvUSERID: TFIBIntegerField;
    quInvSinvQ: TFIBIntegerField;
    quInvSinvCRUSERID: TFIBIntegerField;
    dsInvSinv: TDataSource;
    quInvSinvCOMPID: TFIBIntegerField;
    quListInventoryQ: TFIBIntegerField;
    quListInventoryCRUSERID: TFIBIntegerField;
    quListInventoryCRFIO: TFIBStringField;
    taWHATT1: TFIBStringField;
    taWHATT2: TFIBStringField;
    taTotJSzOUTQ: TFIBIntegerField;
    taTotJSzOUTW: TFIBFloatField;
    taTotJSzOUTRANGE: TFIBIntegerField;
    quListInventoryINVENTORYDATE: TFIBDateTimeField;
    quInventoryD_SUPID: TFIBIntegerField;
    quInventoryD_COMPID: TFIBIntegerField;
    quInventoryD_NOTE1: TFIBIntegerField;
    quInventoryD_NOTE2: TFIBIntegerField;
    quInventoryD_ATT1ID: TFIBIntegerField;
    quInventoryD_ATT2ID: TFIBIntegerField;
    quInventoryD_COUNTRYID: TFIBStringField;
    quInventoryNAMESUP: TFIBStringField;
    quInventoryNAMEATT1: TFIBStringField;
    quInventoryNAMEATT2: TFIBStringField;
    quInventoryGOODS1: TFIBStringField;
    quInventoryGOODS2: TFIBStringField;
    quInventoryCOST: TFIBFloatField;
    quInventoryCOSTP: TFIBFloatField;
    quListInventoryDEPNAME: TFIBStringField;
    quInventorySINVID: TFIBIntegerField;
    quInventoryUNITID: TFIBSmallIntField;
    quTextInventory: TpFIBDataSet;
    quTextInventoryW: TFIBFloatField;
    quTextInventoryART: TFIBStringField;
    quTextInventoryART2: TFIBStringField;
    quTextInventoryPRICE: TFIBFloatField;
    quTextInventoryUNITID: TFIBSmallIntField;
    quTextInventoryQ: TFIBIntegerField;
    quInventoryFL: TFIBSmallIntField;
    quHistArt1SumTACT: TFIBStringField;
    taApplListAPPLID: TFIBIntegerField;
    taApplListNOAPPL: TFIBIntegerField;
    taApplListZDATE: TFIBDateTimeField;
    taApplListUSERID: TFIBIntegerField;
    taApplListISUIDWH: TFIBIntegerField;
    taApplListISSELL: TFIBIntegerField;
    taApplListSNAME: TFIBStringField;
    taApplListSUPID: TFIBIntegerField;
    taApplListFIO: TFIBStringField;
    taApplListALLQ: TFIBIntegerField;
    taApplListPR: TFIBFloatField;
    taApplListF1: TFIBSmallIntField;
    taApplListF2: TFIBSmallIntField;
    taApplListISSEND: TFIBSmallIntField;
    taApplListISCLOSED: TFIBSmallIntField;
    taApplListOP: TFIBStringField;
    taApplListDEP: TFIBStringField;
    taApplListCOLOR: TFIBIntegerField;
    quUIDWH_TSTP3: TFIBFloatField;
    taRetCltListACTDISCOUNT: TFIBFloatField;
    taRetCltListCHECKNO: TFIBIntegerField;
    taApplListSDATE: TFIBDateTimeField;
    taApplRQ: TFIBIntegerField;
    taJSzARTID: TFIBIntegerField;
    taJSzART: TFIBStringField;
    taJSzD_COMPID: TFIBIntegerField;
    taJSzD_MATID: TFIBStringField;
    taJSzD_GOODID: TFIBStringField;
    taJSzUNITID: TFIBSmallIntField;
    taJSzD_INSID: TFIBStringField;
    taJSzINS: TFIBFloatField;
    taJSzINW: TFIBFloatField;
    taJSzINQ: TFIBIntegerField;
    taJSzOUTS: TFIBFloatField;
    taJSzOUTW: TFIBFloatField;
    taJSzOUTQ: TFIBIntegerField;
    taJSzDEBTORS: TFIBFloatField;
    taJSzDEBTORW: TFIBFloatField;
    taJSzDEBTORQ: TFIBIntegerField;
    taJSzSALES: TFIBFloatField;
    taJSzSALEW: TFIBFloatField;
    taJSzSALEQ: TFIBIntegerField;
    taJSzRETS: TFIBFloatField;
    taJSzRETW: TFIBFloatField;
    taJSzRETQ: TFIBIntegerField;
    taJSzRETOPTS: TFIBFloatField;
    taJSzRETOPTW: TFIBFloatField;
    taJSzRETOPTQ: TFIBIntegerField;
    taJSzRETVENDORS: TFIBFloatField;
    taJSzRETVENDORW: TFIBFloatField;
    taJSzRETVENDORQ: TFIBIntegerField;
    taJSzOPTS: TFIBFloatField;
    taJSzOPTW: TFIBFloatField;
    taJSzOPTQ: TFIBIntegerField;
    taJSzSZ: TFIBStringField;
    taJSzCOMP: TFIBStringField;
    taJSzCURRS: TFIBFloatField;
    taJSzCURRW: TFIBFloatField;
    taJSzBD: TFIBDateTimeField;
    taJSzED: TFIBDateTimeField;
    taJSzFULLART: TFIBStringField;
    taJSzCURRQ: TFIBIntegerField;
    taJSzZQ: TFIBIntegerField;
    taJSzISCALC: TFIBSmallIntField;
    taJSzISSZOPT: TFIBSmallIntField;
    taJSzPERCENTSELL: TFIBFloatField;
    taJSzPERCENTRET: TFIBFloatField;
    taJSzPERCENTSRET: TFIBFloatField;
    taJSzID: TFIBIntegerField;
    taJSzWRITE_OFFS: TFIBFloatField;
    taJSzWRITE_OFFW: TFIBFloatField;
    taJSzWRITE_OFFQ: TFIBIntegerField;
    taJSzSURPLUSS: TFIBFloatField;
    taJSzSURPLUSQ: TFIBIntegerField;
    taJSzSURPLUSW: TFIBFloatField;
    DepSort: TpFIBDataSet;
    DepSortR_NO: TFIBIntegerField;
    DepSortD_DEPID: TFIBIntegerField;
    DepSortSNAME: TFIBStringField;
    taApplART2: TFIBStringField;
    taRetCltListSUPNAME: TFIBStringField;
    taApplListW: TFIBFloatField;
    taApplAVG_W: TFIBFloatField;
    quWHUIDART2: TFIBStringField;
    quWHUIDUID: TFIBIntegerField;
    quWHUIDW: TFIBFloatField;
    quWHUIDSZ: TFIBStringField;
    quWHUIDDEPID: TFIBIntegerField;
    quWHUIDART2ID: TFIBIntegerField;
    quWHUIDPRICE2: TFIBFloatField;
    quWHUIDSPRICE: TFIBFloatField;
    quWHUIDSDATE: TFIBDateTimeField;
    quWHUIDSN: TFIBIntegerField;
    quWHUIDSSF: TFIBStringField;
    quWHUIDSUP: TFIBStringField;
    quWHUIDSNAME: TFIBStringField;
    quWHUIDD_GOODS_SAM1: TFIBStringField;
    quWHUIDD_GOODS_SAM2: TFIBStringField;
    quWHUIDAPPLDEPQ: TFIBSmallIntField;
    quWHUIDAPPLDEP_USER: TFIBIntegerField;
    quWHUIDFUIDRESTID: TFIBIntegerField;
    taMolISMOL: TFIBIntegerField;
    taMolISCHAIRMAN: TFIBIntegerField;
    taMolISMEMBER: TFIBIntegerField;
    procedure quWHA2UIDBeforeOpen(DataSet: TDataSet);
    procedure quWHSZBeforeOpen(DataSet: TDataSet);
    procedure quWHArt2BeforeOpen(DataSet: TDataSet);
    procedure taWHUNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure quWHUIDBeforeOpen(DataSet: TDataSet);
    procedure qrWHInvBeforeOpen(DataSet: TDataSet);
    procedure taWHBeforeOpen(DataSet: TDataSet);
    procedure taRetCltBeforeOpen(DataSet: TDataSet);
    procedure quHistArtCalcFields(DataSet: TDataSet);
    procedure quHistArtBeforeOpen(DataSet: TDataSet);
    procedure quHistItemBeforeOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure taRetCltListAfterDelete(DataSet: TDataSet);
    procedure taRetCltListBeforeOpen(DataSet: TDataSet);
    procedure RetCltClick(Sender: TObject);
    procedure taTSumDBeforeOpen(DataSet: TDataSet);
    procedure taApplBeforeOpen(DataSet: TDataSet);
    procedure UNITIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taApplBeforePost(DataSet: TDataSet);
    procedure quHistArt1BeforeOpen(DataSet: TDataSet);
    procedure quHistArt1CalcFields(DataSet: TDataSet);
    procedure taList3UNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taList3BeforeOpen(DataSet: TDataSet);
    procedure taJSzBeforeOpen(DataSet: TDataSet);
    procedure mnitList3Click(Sender: TObject);
    procedure taWHCalcFields(DataSet: TDataSet);
    procedure taWHAfterOpen(DataSet: TDataSet);
    procedure TGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
   procedure TGetTextWithDel(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taWHAfterScroll(DataSet: TDataSet);
    procedure quUIDWH_TBeforeOpen(DataSet: TDataSet);
    procedure TFGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taRetCltListRSTATEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taApplListBeforeOpen(DataSet: TDataSet);
    procedure taApplListBeforeInsert(DataSet: TDataSet);
    procedure taApplAfterDelete(DataSet: TDataSet);
    procedure taApplBeforeDelete(DataSet: TDataSet);
    procedure taApplListNewRecord(DataSet: TDataSet);
    procedure taApplNewRecord(DataSet: TDataSet);
    procedure taApplAfterEdit(DataSet: TDataSet);
    procedure taApplAfterPost(DataSet: TDataSet);
//    procedure udpSellDataReceived(Sender: TComponent; NumberBytes: Integer;
//      FromIP: String; Port: Integer);
//    procedure TCPServerExecute(AThread: TIdPeerThread);
//    procedure TCPServerConnect(AThread: TIdPeerThread);
//    procedure ServerSocket1ClientConnect(Sender: TObject;
//      Socket: TCustomWinSocket);
//    procedure ServerSocket1ClientWrite(Sender: TObject;
//      Socket: TCustomWinSocket);
    procedure taJSzSumBeforeOpen(DataSet: TDataSet);
    procedure taJSzAfterPost(DataSet: TDataSet);
    procedure taJSzCalcFields(DataSet: TDataSet);
    procedure taTotJSzBeforeOpen(DataSet: TDataSet);
    procedure taJDepCalcFields(DataSet: TDataSet);
    procedure quInventoryBeforeOpen(DataSet: TDataSet);
    procedure quInventoryCalcFields(DataSet: TDataSet);
    procedure quInventoryAfterPost(DataSet: TDataSet);
//    procedure ComScanReceiveData(Sender: TObject; DataPtr: Pointer;
//      DataSize: Integer);                       *************
    procedure quInventoryAfterDelete(DataSet: TDataSet);
    procedure taJSzNewRecord(DataSet: TDataSet);
    procedure taJSzBeforeDelete(DataSet: TDataSet);
    procedure taJSzARTIDChange(Sender: TField);
    procedure taJSzSZChange(Sender: TField);
//    procedure ComScan2ReceiveData(Sender: TObject; DataPtr: Pointer;
//      DataSize: Integer);                       *************
    procedure taMolAfterPost(DataSet: TDataSet);
    procedure taMolAfterDelete(DataSet: TDataSet);
    procedure taMolNewRecord(DataSet: TDataSet);
    procedure taMolBeforeDelete(DataSet: TDataSet);
    procedure taApplListAfterDelete(DataSet: TDataSet);
//    procedure ap1Deactivate(Sender: TObject);   *************
//    procedure ap1Activate(Sender: TObject);     *************
    procedure CommitRetaining(DataSet: TDataSet);
    procedure quInvSinvBeforeOpen(DataSet: TDataSet);
    procedure quInvSinvAfterPost(DataSet: TDataSet);
    procedure taJSzzqChange(Sender: TField);
    procedure quWHUIDAPPLDEPQChange(Sender: TField);
    procedure quWHUIDBeforeClose(DataSet: TDataSet);
    procedure taMolBeforePost(DataSet: TDataSet);
  private
    DiscReEnter:boolean;  
    Form:TForm;
    FHistED: TDatetime;
    FHistBD: TDateTime;
    FWHDepId : integer;
    FHistDepId: integer;
    FHistArtid: integer;
    FHistArt: string;
    FHistStrict : byte;
    FList3DepId: integer;
    FFilter : TFilter;
    FL3BD, FL3ED : TDateTime;
    FWholeArt: byte;
    FIsIns: integer;
    FIsEdtAppl: boolean;
    procedure SetList3DepId(const Value: integer);
    procedure Insert_Record (ValScan:string);
    function MessageDlgCtr(const Msg: string; DlgType: TMsgDlgType;
      Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
  public
   {***********}
    CurInventory:integer;
    Inventory_Precent:integer;
    RepeatClick : boolean;
    FInvIn :string;
    FSellEdit : boolean;
    FSellItemIds : array of TSellItemInfo; // ������� ��������� �������
   {***********}
    KindTSum:integer; //0-����� �� ����, 1-��������
    FArt : string;
    ApplyFilter : boolean;
    Mode : string;
    // ���������� ����� ������� ������������ � ������� �������
    UID : integer;
    // ������������ ������ � ����� ������� �������
    Art : string;
    //��� ������ �� ������� ��������� ��� ������/�������
    GrMat:string;
    Mat: string;
    Goods: string;
    Supplier: string;
    ApplSupid:integer; //����� ������������
    NoAppl : integer; // ����� ������
    SzMode: integer; // ��������� �� ��� ������ ���������� �������(�� ��������/
                     //    ��� ��������)

    TQ, TW, TA: string; // ����� ��� ������ �� ����������
    SearchArt: string;  // ����� �������� ��� �������������� �� ������ ���
                        // ���������� � ������ �� �������
    DicSupID: integer;  // ���������, ������������ ��� ���������� ��� ��������
                       // �� ������ ��� � ������ �� �������

    property WHDepId : integer read FWHDepId write FWHDepId;
    (* ������� *)
      (* ������ ��� �������, ������ ��� *)
    property HistBD : TDateTime read FHistBD write FHistBD;
    property HistED : TDatetime read FHistED write FHistED;
      (* ������������� *)
    property HistDepId : integer read FHistDepId write FHistDepId;
      (* Id ������� *)
    property HistArtId : integer read FHistArtid write FHistArtId;
      (* ������� *)
    property HistArt : string read FHistArt write FHistArt;
    property HistStrict : byte read FHistStrict write FHistStrict;

    (* Id ������ ��� �������� ������������ ����.���. *)
    property List3DepId : integer read FList3DepId write SetList3DepId;
    (* ������� ������ �� ���������� ��� ������� ���������� *)
    property WholeArt : byte read FWholeArt write FWholeArt;
    (* ��������� ������� *)
    property Filter : TFilter read FFilter write FFilter;
    (* ��� ��������� ���������� *)
    property L3BD : TDateTime read FL3BD write FL3BD;
    property L3ED : TDateTime read FL3ED write FL3ED;

    property IsIns : integer read FIsIns write FIsIns; //��� ������� �� �������� + �������
    property IsEdtAppl : boolean read FIsEdtAppl write FIsEdtAppl; //��� ������� �� �������� + �������
    // ���� ������ ���������������, �� ��� ������ �� ������� �������� ��������� � ���������� ����

    function SumField(Field : TField) : double;
    procedure CheckAnaliz(IsUpdate: integer; Old_Sz: string);
  published
    procedure OnScanCode(ScanCode: string);
  end;

  TSocketThread = class(TThread)
  private
    fsocketAddr, Fread: string;
    fsocketport: integer;
  published
    property  socketAddr: string read fsocketAddr write fsocketAddr;
    property  socketport: integer read fsocketport write fsocketport;
  protected
    procedure Execute; override;
  end;

var
  dmServ: TdmServ;
  thr1:TSocketThread;

implementation

uses comdata, WH, Data, Data2, RetCltList, Splash, RxDateUtil, M207Proc,
  {ImportData, }rxStrUtils, List3, bsStrUtils, DInv, SInv, selledit, dst2, dst, dst3,
  dinvitem, Progress, inventory, sret, sellitem, Data3, jewconst, MsgDialog, uUtils,
  uScanCode, Main, Sertificate, frmCertificateCollation;

{$R *.DFM}

(******************************************************************************)
procedure TSocketThread.Execute;
var s:integer;
    Addr: sockaddr_in;

  procedure ReadSocket;
  var i:integer;
      buf: array [0..254] of char;
      Stword, sn:string;
      SDep, Sinvid:integer;
      nbytes : integer;
  begin
    FillChar(buf,254,#0);
    nbytes := recv(s,buf,sizeof(buf),0);
    if (nbytes < 0) then
    begin
      MessageDialog('������ ������ recv', mtInformation, [mbOk], 0);
      Self.Terminate;
    end
    else if (nbytes = 0) then
    begin
      MessageDialog('������ ��������� ����:(', mtInformation, [mbOk], 0);
      Self.Terminate;
    end;

    i:=0;
    Fread:='';
    while (i<=254)and(buf[i]<>#0) do
    begin
      Fread:=Fread+buf[i]; inc(i)
    end;
    stWord:= dm.WorkMode;
    sDep:=strtoint(ExtractWord(1,Fread,[':']));
    sn:=ExtractWord(2,Fread,[':']);
    sinvid:=strtoint(ExtractWord(3,Fread,[':']));
    if not dm.SetSocketClose then
    begin
      if (stWord='DINV')and(dm.taDList.Active)and(sdep=SelfDepId) then
      begin
        if (pos('*'+inttostr(sinvid)+';',dm.sWorkInvID)>0) and (dm.ClosedInvId=-1) then
          if dm.taDListISCLOSED.AsInteger=0 then
          begin
            dmCom.tr.CommitRetaining;
            dm.taDList.Refresh;
            MessageDialog('������� ��������� �'+sn+' ������ �������������', mtInformation, [mbOk], 0);
            CancelDataSets([dm.taSEl]);
            ReOpenDataSets([dm.taSEl]);
            dm.SetCloseInvBtn(fmDInv.siCloseInv, dm.taDListIsClosed.AsInteger);
          end;
      end;

      if (stWord='SINV')and(dm.taSList.Active)and(sdep=SelfDepId) then
      begin
        if (dm.taSListSInvId.AsInteger=sInvId) and (dm.ClosedInvId=-1) then
          if dm.taSListISCLOSED.AsInteger=0 then
          begin
            dmCom.tr.CommitRetaining;
            dm.taSList.Refresh;
            MessageDialog('������� ��������� �'+sn+' ������ �������������', mtInformation, [mbOk], 0);
            CancelDataSets([dm.taSEl]);
            ReOpenDataSets([dm.taSEl]);
            dm.SetCloseInvBtn(fmSInv.siCloseInv, dm.taSListIsClosed.AsInteger);
          end;
      end;
    end
    else dm.SetSocketClose:=False;
  end;

const WSVer = $101;
var
   wsaData: TWSAData;
begin
  if(WSAStartup(WSVer, wsaData) <> 0)then
  begin
     MessageDialog('������ ������������� ������ winsock', mtError, [mbOk], 0);
     eXit;
  end;
  s:=socket(AF_INET, SOCK_STREAM, 0);
  if(s = -1)then
  begin
    MessageDialog('���������� ������� �����', mtError, [mbOk], 0);
    eXit;
  end;
  try
    FillChar(Addr, SizeOf(Addr), #0);
    Addr.sin_family := AF_INET;
    Addr.sin_port := htons(fsocketport);
    Addr.sin_addr.S_addr:=inet_addr(pchar(fsocketAddr));
    if(connect(s, Addr, sizeof(Addr))= -1) then
    begin
      MessageDialog('���������� �������������� � ������� ���������!'#13+'���������� �� ����� �������� ���������', mtWarning, [mbOk], 0);
      eXit;
    end;
    while not Terminated  do
    begin
      ReadSocket;
    end;
  finally
    closesocket(s);
    WSACleanup;
  end;
end;
(******************************************************************************)

function RetClt(q : TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function List3(q : TpFIBDataSet) : boolean;
begin
  Result := True;
end;

procedure TdmServ.quWHA2UIDBeforeOpen(DataSet: TDataSet);
begin
  with quWHA2UID.Params do
  begin
    ByName['D_ArtId'].AsInteger := taWHD_ARTID.AsInteger;
    if WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := WHDepId;
      ByName['DEPID2'].AsInteger := WHDepId;
    end;
  end;

end;

procedure TdmServ.quWHSZBeforeOpen(DataSet: TDataSet);
begin
  with quWHSZ.Params do
  begin
    ByName['USERID'].AsInteger := dmCom.UserId;
    ByName['ARTID'].AsInteger := taWHD_ARTID.AsInteger;
    if WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := WHDepId;
      ByName['DEPID2'].AsInteger := WHDepId;
    end;
  end;
end;

procedure TdmServ.quWHArt2BeforeOpen(DataSet: TDataSet);
begin
  with quWHArt2.Params do
  begin
    ByName['USERID'].AsInteger := dmCom.UserId;
    ByName['ARTID'].AsInteger := taWHD_ARTID.AsInteger;
    if WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := WHDepId;
      ByName['DEPID2'].AsInteger := WHDepId;
    end;
  end;
end;

procedure TdmServ.taWHUNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure TdmServ.quWHUIDBeforeOpen(DataSet: TDataSet);
var lf: TStringlist;
begin
  with TpFIBDataSet(DAtaSet).Params do
  begin
    lf:=TStringlist.Create;
    ByName['ARTID'].AsInteger := taWHD_ARTID.AsInteger;
    if WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else
    begin
      ByName['DEPID1'].AsInteger := WHDepId;
      ByName['DEPID2'].AsInteger := WHDepId;
    end;
    lf.AddStrings(quWHUID.SelectSQL);
    lf.SaveToFile('c:\art.txt');
  end;
end;

procedure TdmServ.qrWHInvBeforeOpen(DataSet: TDataSet);
begin
  qrWHInv.Params.ByName['ArtId'].AsInteger := taWHD_ARTID.AsInteger;
end;

procedure TdmServ.taWHBeforeOpen(DataSet: TDataSet);
{����� ��������� � �������}
 Function find_z (st:string):string;
 var i:integer;
     fl:boolean;
 begin
  result:='';
  fl:=false;
  if length(st)>1 then
  for i:=1 to length(st) do
   if st[i]='*' then begin
    result:=result+'%'; fl:=true end
   else result:=result+st[i];

  if fl then result:='and art like '''+result+'''';

 end;

begin
  with TpFIBDataSet(DataSet) do
  begin

(*****************************************************************)
    if (DataSet.Name ='taWH') then
      begin
      if dm.lMultiSelect and (dm.SD_MatID<>'')then
          SelectSQL[6]:=' and Mat in ('+dm.SD_MatID+') '
      else
          SelectSQL[6]:=' ';

      if dm.lMultiSelect and (dm.SD_GoodID<>'')then
          SelectSQL[7]:='and Good in ('+dm.SD_GoodID+') '
      else
          SelectSQL[7]:=' ';

      if dm.lMultiSelect and (dm.SD_CompID<>'')then
          SelectSQL[8]:='and D_CompId in ('+dm.SD_CompID+') '
      else
          SelectSQL[8]:=' ';

     end;
(*****************************************************************)
    dmCom.D_INSID:='*';
    dmCom.SetArtFilter(Params);
    if Params.FindParam('USERID') <> nil then
      try
       if CenterDep then
            ParamByName('USERID').AsInteger:=UserDefault;
      except
      end;

    if Params.FindParam('ART1') <> nil then
      if dmCom.FilterArt = '' then
      begin
        ParamByName('ART1').AsString := '!!';
        ParamByName('ART2').AsString := '��';
      end
      else
      begin
         ParamByName('ART1').AsString := dmCom.FilterArt;
         ParamByName('ART2').AsString := dmCom.FilterArt+'��';
      end;
    Prepare;
  end;
end;

procedure TdmServ.taRetCltBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    ByName['P_Inv'].AsInteger := dmServ.taRetCltListSINVID.AsInteger;
end;

procedure TdmServ.quHistArtCalcFields(DataSet: TDataSet);
var
  s : string;
begin
  case quHistArtR_I.AsInteger of
    1 : s := '������ (��������)';
    2 : s := '�����. �����������';
    3 : s := '������� �������';
    5 : s := '������ (�������)';
    6 : s := '������ (������� �� ����������)';
    7 : s := '������� �����������';
    8 : s := '������� �� ���. �����������';
    10 : s := '��������� �������';
  end;
  quHistArtDescr.AsString := s;
end;

procedure TdmServ.quHistArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['ArtId'].AsInteger := HistArtId;// ibdsWHTmpD_ARTID.AsInteger;
    if HistDepId = -1 then
    begin
      ByName['DepId1'].AsInteger := -MaxInt;
      ByName['DepId2'].AsInteger := MaxInt;
    end
    else begin
      ByName['DepId1'].AsInteger := HistDepId;
      ByName['DepId2'].AsInteger := HistDepId;
    end;
    ByName['BD'].AsDate := HistBD;
    ByName['ED'].AsDate := HistED;
  end;
end;

procedure TdmServ.quHistItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    if DataSource = dsrHistArt1 then
    begin
      ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(HistBD);
      ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(HistED);
    end
    else begin
      ByName['BD'].AsDate := dm.BeginDate;
      ByName['ED'].AsDate := dm.EndDate;
    end;
  end;
end;

procedure TdmServ.DataModuleCreate(Sender: TObject);
var
   i : integer;
   ff : TFloatField;
   ff_ : TFloatField;
   sqlstr: string;
begin
  if not CenterDep then
    begin
      pmRetClt.Items.Clear;
    end;

  dmCom.FillDepMenu(dmCom.quDep, pmRetClt, RetClt, RetCltClick, True);
  dmCom.FillDepMenu(dmCom.taShop, pmList3, List3, mnitList3Click, True);
  // ������ ������ ��� ������ �� ����������
  if not dmCom.tr.Active then dmCom.tr.StartTransaction;
  OpenDataSets([dmCom.taRec]);

  FIsIns := 1;
  with taJSz do
  begin
    SelectSQL.Clear;
    SelectSQL.Add('select ID, ARTID, ART, D_COMPID, D_MATID, ');
    SelectSQL.Add(' D_GOODID, UNITID, D_INSID, INS, INW, INQ, OUTS, ');
    SelectSQL.Add(' OUTW, OUTQ, DEBTORS, DEBTORW, DEBTORQ, SALES, SALEW, ');
    SelectSQL.Add(' SALEQ, RETS, RETW, RETQ, RETOPTS, RETOPTW, RETOPTQ, ');
    SelectSQL.Add(' RETVENDORS, RETVENDORW, RETVENDORQ, OPTS, OPTW, OPTQ,') ;
    SelectSQL.Add(' SZ, COMP, CURRS, CURRW, CURRQ, BD, ED, FULLART, ') ;
    SelectSQL.Add(' ZQ, IsCalc, isszopt, percentsell, percentret, percentsret, Write_offS, Write_offW, Write_offQ, ');
    SelectSQL.Add(' SurPlusS, SurPlusW, SurPlusQ, ');
    if  not DepSort.Transaction.Active then DepSort.StartTransaction;
    DepSort.Open;
    while not DepSort.Eof do
    begin
      SelectSQL.Add(' cast(get_word(SaleStrQ, '';'',' + DepSort.FieldByName('R_No').AsString +') as double precision) RQ_' + DepSort.FieldByName('D_DepID').AsString +
                   ', cast(get_word(SaleStrW, '';'',' + DepSort.FieldByName('R_No').AsString + ')as double precision) RW_'+ DepSort.FieldByName('D_DepID').AsString  + ' ,');
      SelectSQL.Add(' cast(get_word(OutStrQ, '';'',' + DepSort.FieldByName('R_No').AsString +') as double precision) RQ1_' + DepSort.FieldByName('D_DepID').AsString +
                   ', cast(get_word(OutStrW, '';'',' + DepSort.FieldByName('R_No').AsString + ')as double precision) RW1_'+ DepSort.FieldByName('D_DepID').AsString  + ' ,');
      DepSort.Next;
    end;

    DepSort.Close;
    DepSort.Transaction.CommitRetaining;
    sqlstr := SelectSQL[SelectSQL.Count-1];
  end;
  delete(sqlStr, length(sqlStr), 1);
  taJSz.SelectSQL[taJSz.SelectSQL.Count-1] := sqlstr;
  taJSz.SelectSQL.Add(' from GROUPANLZ(:IsIns,:SzMode,:USERID) ');
  taJSz.Prepare;
    // �������� �����
  for i :=0 to Pred(dmCom.DepCount) do
    begin
      ff_ := TFloatField.Create(taWH);
      ff_.FieldKind := fkCalculated;
      ff_.FieldName := 'RW_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff_.DisplayFormat := '0.##';
      ff_.DisplayLabel := dmcom.DepInfo[i].SName;
      ff_.DataSet:=taWH;
      ff_.Tag := dmcom.DepInfo[i].DepId;

      ff_ := TFloatField.Create(taWH);
      ff_.FieldKind := fkCalculated;
      ff_.FieldName := 'RQ_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff_.DisplayLabel := dmcom.DepInfo[i].SName;
      ff_.DataSet:= taWH;
      ff_.Tag := dmcom.DepInfo[i].DepId;

      ff := TFloatField.Create(taJSz);
      ff.FieldKind := fkData;
      ff.FieldName := 'RW_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff.SetFieldType(ftFloat);
      ff.DisplayFormat := '0.##';
      ff.DisplayLabel := '�������|' + dmcom.DepInfo[i].SName  + '|���';
      ff.DataSet:=taJSz;
      ff.Tag := dmcom.DepInfo[i].DepId;

      ff := TFloatField.Create(taJSz);
      ff.FieldKind := fkData;
      ff.FieldName := 'RQ_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff.SetFieldType(ftInteger);

      ff.DisplayLabel :=  '�������|' + dmcom.DepInfo[i].SName  + '|���-��';
      ff.DataSet:= taJSz;
      ff.Tag := dmcom.DepInfo[i].DepId;

      ff := TFloatField.Create(taJSz);
      ff.FieldKind := fkData;
      ff.FieldName := 'RW1_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff.SetFieldType(ftFloat);
      ff.DisplayFormat := '0.##';
      ff.DisplayLabel := '��������� �������|' + dmcom.DepInfo[i].SName  + '|���';
      ff.DataSet:=taJSz;
      ff.Tag := dmcom.DepInfo[i].DepId;

      ff := TFloatField.Create(taJSz);
      ff.FieldKind := fkData;
      ff.FieldName := 'RQ1_'+IntToStr(dmcom.DepInfo[i].DepId);
      ff.SetFieldType(ftInteger);

      ff.DisplayLabel :=  '��������� �������|' + dmcom.DepInfo[i].SName  + '|���-��';
      ff.DataSet:= taJSz;
      ff.Tag := dmcom.DepInfo[i].DepId;
    end;
    if not CenterDep then
    begin
        quWHUID.SelectSQL.Text:=ReplaceStr(quWHUID.SelectSQL.Text, 'UIDWH_T', 'FUIDREST');
        //quWHUID.SelectSQL[7] := ' ';
        //quWHUID.SelectSQL[11] := ' ';
        quWHSZ.SelectSQL.Text:=ReplaceStr(quWHSZ.SelectSQL.Text, 'UIDWH_T', 'FUIDREST');
        quWHSZ.SelectSQL[4] := ' ';
        quWHSZ.SelectSQL[7] := ' ';
        quWHArt2.SelectSQL.Text:=ReplaceStr(quWHArt2.SelectSQL.Text, 'UIDWH_T', 'FUIDREST');
        quWHArt2.SelectSQL[4] := ' ';
        quWHArt2.SelectSQL[7] := ' ';
        qrWHInv.SelectSQL.Text:=ReplaceStr(qrWHInv.SelectSQL.Text, 'UIDWH_T', 'FUIDREST');
    end;

(******************************************************************************)
  dm.SetSocketClose:=false;
  if dmCom.NotifServerParam.Notif_Use then
  begin
    thr1 := TSocketThread.Create(true);
    thr1.socketAddr:=dmCom.NotifServerParam.Notif_Ip;
    thr1.socketport:=dmCom.NotifServerParam.Notif_Port;
    thr1.Resume;
  end;
(******************************************************************************)
end;

procedure TdmServ.RetCltClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taRetCltList.Close;
    TMenuItem(Sender).Checked:=True;
    dm.DDepFromId:= TMenuItem(Sender).Tag;
    dm.DDepFrom:= TMenuItem(Sender).Caption;
    dm.SDepId := TMenuItem(Sender).Tag;
    fmRetCltList.laDepFrom.Caption:= dm.DDepFrom;
    taRetCltList.Open;
  finally
    Screen.Cursor:=crDefault;
  end;
end;


procedure TdmServ.taRetCltListAfterDelete(DataSet: TDataSet);
begin
   dmcom.tr.CommitRetaining;
end;

procedure TdmServ.taRetCltListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    if dm.SDepId=-1 then
    begin
      ByName['DEPID1'].AsInteger:= -MAXINT;
      ByName['DEPID2'].AsInteger:= MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger:= dm.SDepId;
      ByName['DEPID2'].AsInteger:= dm.SDepId;
    end;
    ByName['BD'].AsTimeStamp:= DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp:= DateTimeToTimeStamp(dm.EndDate);
  end;

end;


procedure TdmServ.taTSumDBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['BD'].asDate := dm.BeginDate;
    ByName['ED'].AsDate := dm.EndDate;
  end;
end;

procedure TdmServ.taApplBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
   begin
     ByName['NAp'].AsInteger := NoAppl;
     if (GrMat='')or (GrMat[1]='*') then
       begin
         ByName['Gr1'].AsString:='!!';
         ByName['Gr2'].AsString:='��';
       end
     else
       begin
         ByName['Gr1'].AsString:=GrMat;
         ByName['Gr2'].AsString:=GrMat;
       end;
   end;
end;

procedure TdmServ.UNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure TdmServ.taApplBeforePost(DataSet: TDataSet);
begin
    if VarIsNull(taApplSZ.OldValue)  then exit;
    if (taApplSZ.OldValue <> taApplSZ.AsString) then
      CheckAnaliz(1,taApplSZ.OldValue)
    else
      CheckAnaliz(-1,'');
end;

procedure TdmServ.quHistArt1BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['ART'].AsString := HistArt;
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(HistBD);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(HistED);
    ByName['DEPID1'].AsInteger := -MaxInt;
    ByName['DEPID2'].AsInteger := MaxInt;
    ByName['KIND'].AsInteger := WholeArt;
  end;
end;

procedure TdmServ.quHistArt1CalcFields(DataSet: TDataSet);
var
  s : string;
begin
  case quHistArt1R_I.AsInteger of
    1 : s := '������ (��������)';
    2 : s := '�����. �����������';
    3 : s := '������� �������';
    5 : s := '������ (�������)';
    6 : s := '������ (������� �� ����������)';
    7 : s := '������� �����������';
    8 : s := '������� �� ���. �����������';
    10 : s := '��������� �������';
    19 : s := '��� ��������';
    21 : s := '������ (�������)';        
  end;
  quHistArt1Descr.AsString := s;
end;

procedure TdmServ.taList3UNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure TdmServ.taList3BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['USERID'].AsInteger := dmCom.User.UserId;
    case List3Kind of
      l3kOther : begin
        ByName['GROUPID1'].AsInteger := 3;
        ByName['GROUPId2'].AsInteger := 3;
      end;
      l3kMain : begin
        ByName['GROUPID1'].AsInteger := -MAXINT;
        ByName['GROUPID2'].AsInteger := MAXINT;
      end;
    end;
    dmCom.SetArtFilter(Params);
  end;
end;

procedure TdmServ.taJSzBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
   try
    ByName['USERID'].AsInteger := dmCom.UserId;
    ByName['SzMode'].AsInteger := SzMode;
    ByName['IsIns'].AsInteger :=  FIsIns;
   except
   end;
end;

procedure TdmServ.mnitList3Click(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    TMenuItem(Sender).Checked:=True;
    fmList3.laDep.Caption:= TMenuItem(Sender).Caption;
    List3DepId := TMenuItem(Sender).Tag;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TdmServ.SetList3DepId(const Value: integer);
var
  i : integer;
  Shop : string;
begin
  FList3DepId := Value;
  if Value = -1 then
  begin
    fmList3.grlist3.Columns.BeginUpdate;
    for i:=0 to Pred(fmList3.grlist3.Columns.Count) do
      if not Assigned(fmList3.grlist3.Columns[i].Field)
      then fmList3.grlist3.Columns[i].Visible := False
      else fmList3.grlist3.Columns[i].Visible := True;
    fmList3.grlist3.Columns.EndUpdate;
  end
  else begin
    for i:=0 to dmCom.DepCount do if dmCom.DepInfo[i].Shop = 1 then
      if dmCom.DepInfo[i].DepId = Value then
      begin
        Shop := dmCom.DepInfo[i].SName;
        break;
      end;
    fmList3.grlist3.Columns.BeginUpdate;
    for i:=0 to Pred(fmList3.grlist3.Columns.Count) do
      if ScanF(fmList3.grlist3.Columns[i].Title.Caption, Shop, 1) <> 0
      then fmList3.grlist3.Columns[i].Visible := True
      else if Assigned(fmList3.grlist3.Columns[i].Field)and
              (fmList3.grlist3.Columns[i].Field.Tag = 1)
           then fmList3.grlist3.Columns[i].Visible := True
           else fmList3.grlist3.Columns[i].Visible := False;
    fmList3.grlist3.Columns.EndUpdate;
  end;
end;

procedure TdmServ.taWHCalcFields(DataSet: TDataSet);
var
  i : integer;
  DefaultSeparator : char;
begin
  DefaultSeparator := DecimalSeparator;
  try
    DecimalSeparator := '.';
    for i := 0 to Pred(dmcom.DepCount) do
    begin
{������� � ���� ������}    
      DataSet.FieldByName('RW_'+IntToStr(dmcom.DepInfo[i].DepId)).AsFloat :=
                         StrToFloat(ExtractWord(i+1, taWHTW.AsString , [';']));
      DataSet.FieldByName('RQ_'+IntToStr(dmcom.DepInfo[i].DepId)).AsFloat :=
                         StrToFloat(ExtractWord(i+1, taWHTQ.AsString, [';']));
    end;
  finally
    DecimalSeparator := DefaultSeparator;
  end;
end;

function TdmServ.SumField(Field: TField): double;
var
  DataSet : TDataSet;
  BookMark : TBookMark;
begin
  Result := 0;
  DataSet := Field.DataSet;
  DataSet.DisableControls;
  BookMark := DataSet.GetBookmark;
  DataSet.First;
  while not DataSet.Eof do
  begin
    Result := Result + Field.AsFloat;
    DataSet.Next;
  end;
  DataSet.GotoBookmark(BookMark);
  DataSet.EnableControls;
end;

procedure TdmServ.taWHAfterOpen(DataSet: TDataSet);
var
    i: integer;
    TSql: string;
    tstr: string;

begin
  TQ := '';
  TW := '';
  TA := '';
  TSql := '';


  if dm.lMultiSelect and (dm.SD_MatID<>'')then
      TSql:= TSql +  ' and Mat in ('+dm.SD_MatID+') ';

  if dm.lMultiSelect and (dm.SD_GoodID<>'')then
      TSql := TSql + ' and Good in ('+dm.SD_GoodID+') ';

  if dm.lMultiSelect and (dm.SD_CompID<>'')then
      TSql := TSql + ' and D_CompId in ('+dm.SD_CompID+') ';

  if dmCom.FilterArt <> '' then
      TSql := TSql +  ' and  Art between '#39 + dmCom.FilterArt + #39'  and '#39 +
                                              dmCom.FilterArt + '��'#39;


  if not (quTmp.Transaction.Active) then quTmp.Transaction.StartTransaction;

  for i:=0 to Pred(dmCom.DepCount) do
   with quTmp do
   begin
     Close;
{��������� ��� � ������ ����������}
     SQL.Text := 'select sum(get_word(tw,'#39+';'+#39',' + IntToStr(i+1) +  ')) ' +
                 'from  ArtWH  ' +
                 'where get_word(tq,'#39+';'+#39',' + IntToStr(i+1) +  ') <> 0  ' + TSql;
     ExecQuery;
     tstr := ReplaceStr(trim(Fields[0].AsString),'.', ',');
     if tstr = '' then tstr := '0';
     TW :=  TW +  dmCom.DepInfo[i].SName + ': ' + FloatToStr(RoundTo(StrToFloat(tstr),-2)) + '  ';

     Close;
{��������� ���������� � ������ ����������}
     Sql.Text := 'select sum(get_word(tq,'#39+';'+#39',' + IntToStr(i+1) +  ')) ' +
                 'from  ArtWH  ' +
                 'where get_word(tq,'#39+';'+#39',' + IntToStr(i+1) +  ') <> 0  ' + TSql;

     ExecQuery;
     tstr := ReplaceStr(trim(Fields[0].AsString),'.', ',');
     if tstr = '' then tstr := '0';
     TQ :=  TQ +  dmCom.DepInfo[i].SName + ': ' +  FloatToStr(RoundTo(StrToFloat(tstr),-2)) + '  ';

     Close;
{����������� � ������ ����������}
     Sql.Text := 'select count(distinct D_ArtID) ' +
                 'from  ArtWH  ' +
                 'where get_word(tq,'#39+';'+#39',' + IntToStr(i+1) +  ') <> 0  ' + TSql;
     ExecQuery;
     tstr := trim(Fields[0].AsString);
     if tstr = '' then tstr := '0';
     TA :=  TA + dmCom.DepInfo[i].SName + ': ' + tstr + '   ';
     quTmp.Transaction.CommitRetaining;
     Close;
  end;
  ReOpenDataSets([quWh_T]);
end;

procedure TdmServ.TGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  i : integer;
begin
  Text := '';
  i:=1;
  while ExtractWord(i, Sender.AsString, [';'])<>'' do
  begin
    Text := Text + dmcom.DepInfo[i-1].SName + ': '+ExtractWord(i, Sender.AsString, [';'])+' ';
    inc(i);
  end;
end;
procedure TdmServ.TGetTextWithDel(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  i : integer;
begin
  Text := '';
  i:=1;
  with dmCom do
  begin
    if not DepSortWithDel.Active then  OpenDataSet(DepSortWithDel);
    DepSortWithDel.First;
    while not DepSortWithDel.Eof do
    begin
      Text := Text + DepSortWithDelSName.AsString + ': '+ExtractWord(i, Sender.AsString, [';'])+' ';
      inc(i);
      DepSortWithDel.Next;
    end
  end
end;

procedure TdmServ.taWHAfterScroll(DataSet: TDataSet);

  procedure Open(ds : TpFIBDataSet);
  begin
    Screen.Cursor := crSQLWait;
    try
      ds.Close;
      ds.Open;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  
begin
  if not Assigned(fmWH) then eXit;
  if fmWH.chbxInfo.Checked then
    case fmWH.pgctrl.ActivePage.Tag of
      1 : Open(quWHSZ);
      2 : Open(quWHArt2);
      3 : Open(qrWHInv);
      4 : Open(quWHUID);
    end
  else  begin
    quWHSZ.Close;
    quWHArt2.Close;
    qrWHInv.Close;
    quWHUID.Close;
  end;
end;

procedure TdmServ.quUIDWH_TBeforeOpen(DataSet: TDataSet);
begin
  dm.quUIDWHBeforeOpen(DataSet);
end;

procedure TdmServ.TFGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  i : integer;
  s : string;
begin
  Text := '';
  i:=1;
  while ExtractWord(i, Sender.AsString, [';'])<>'' do
  begin
    s := StringReplace(ExtractWord(i, Sender.AsString, [';']), '.', ',', [rfReplaceAll]);
    Text := Text + dmcom.DepInfo[i-1].SName + ':'+
       FormatFloat(cCurrFormat, StrToFloat(s))+' ';
    inc(i);
  end;
end;

procedure TdmServ.taRetCltListRSTATEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           1: Text:='��������� � �������';
           2: Text:='�������';
           3: Text:='������';
           4: Text:='����������';
         end;
end;

procedure TdmServ.taApplListBeforeOpen(DataSet: TDataSet);
begin
 with dm, TpFIBDataSet(DataSet).Params do
  begin
     ByName['BD'].AsTimeStamp:= DateTimeToTimeStamp(BeginDate);
     ByName['ED'].AsTimeStamp:= DateTimeToTimeStamp(EndDate);
     if ApplSupId>0 then
       begin
         ByName['Sup1'].asInteger:=ApplSupId;
         ByName['Sup2'].asInteger:=ApplSupId;
       end
     else
       begin
         ByName['Sup1'].asInteger:=-Maxint;
         ByName['Sup2'].asInteger:=Maxint;
       end;
  end;
end;

procedure TdmServ.taApplListBeforeInsert(DataSet: TDataSet);
begin
  if ApplSupid<0 then raise Exception.Create('���������� ������� ������������!');
end;

procedure TdmServ.taApplAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure TdmServ.taApplBeforeDelete(DataSet: TDataSet);
var sqlstr: string;
begin
  if MessageDialog('������� ������� �� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  sqlstr := ' execute procedure CheckAnaliz(' + taApplAPPLID.AsString  + ', ' +
            taApplARTID.AsString + ', ''' +  taApplSZ.AsString + ''', ' +
            IntToStr(0) +',' + IntToStr(dmCom.UserID)  + ', -1, '''')';

  ExecSQL(sqlstr,quTmp);
end;

procedure TdmServ.taApplListNewRecord(DataSet: TDataSet);
begin
  taApplListApplId.AsInteger:=dmCom.GetId(31);
  taApplListUserId.AsInteger:=dmCom.UserId;
  taApplListSupid.AsInteger :=ApplSupId;
end;

procedure TdmServ.taApplNewRecord(DataSet: TDataSet);
begin
  taApplAItemId.AsInteger:=dmCom.GetId(34);
  taApplApplId.AsInteger:=taApplListApplId.AsInteger;
  taApplArtid.AsInteger :=dm.quArtD_ARTID.AsInteger;
  taApplUnitId.AsInteger:=dm.quArtUnitId.AsInteger;
  taApplD_Compid.AsInteger :=dm.quArtD_CompID.AsInteger;
  taApplD_InsId.AsString :=dm.quArtD_InsID.AsString;
  taApplD_MatId.AsString :=dm.quArtD_MatID.AsString;
  taApplD_GoodId.AsString :=dm.quArtD_GoodID.AsString;
  taApplArt.AsString :=dm.quArtART.AsString;
  taApplSZ.AsString := '-';
end;

procedure TdmServ.taApplAfterEdit(DataSet: TDataSet);
begin
// Dataset.Refresh;
// dmCom.tr.CommitRetaining;
end;

procedure TdmServ.taApplAfterPost(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

//procedure TdmServ.udpSellDataReceived(Sender: TComponent;
//  NumberBytes: Integer; FromIP: String; Port: Integer);
//var ms: TMemoryStream;
//    rSellItemid: integer;
//    rUserID: integer;
//    s: string;
//    itypeoperator:string;
{begin
  ms:=TMemoryStream.Create;
  try
   udpSell.ReadStream(ms);
   SetLength(s, NumberBytes);
   ms.Read(s[1], NumberBytes);

   if ExtractWord(1, s, [':'])='SELL' then
   begin
    itypeoperator:= EXtractWord(2, s, [':']);
    rSellItemid := strtoint(EXtractWord(3, s, [':']));
    strtoint(EXtractWord(4, s, [':']));
    rUserID := strtoint(EXtractWord(5, s, [':']));
    if (dm.taSellItem.Active)and(dmcom.User.UserId = rUserID) then
    begin
      if(dm.taSellItem.State in [dsInsert, dsEdit]) then
      begin
        SetLength(FSellItemIds, Length(FSellItemIds)+1);
        FSellItemIds[Length(FSellItemIds)-1].Id := rSellItemId;
        FSellItemIds[Length(FSellItemIds)-1].Action := itypeoperator;
      end
      else begin
       if (itypeoperator = 'INS') and (not dm.taSellItem.Locate('SELLITEMID', rSellItemid,[])) then
       begin
        sleep(1000);
        dm.taSellItem.Insert;
        dm.taSellItemF.AsInteger := -1;
        with dm, dmcom, quTmp do
        begin
         SQL.Text:='Select W, SZ, ADATE, EmpId from SELLITEM where SELLITEMID = '+inttostr(rSellItemid);
         ExecQuery;
         taSellItemW.AsFloat:= Fields[0].AsFloat;
         taSellItemSz.AsString:= Fields[1].AsString;
         taSellItemAdate.AsDateTime:= Fields[2].AsDateTime;
         taSellItemSELLITEMID.AsInteger := rSellItemid;
         taSellItemUSERID.AsInteger := Fields[3].AsInteger;
         quTmp.Close;
        end;
        dm.taSellItem.Post;
        dm.taSellItem.Edit;
        dm.taSellItemF.AsInteger := 1;
        dm.taSellItem.Post;
        dm.taSellItem.Transaction.CommitRetaining;
       end;

       if (itypeoperator = 'DEL') and (dm.taSellItem.Active) and
          (dm.taSellItem.Locate('SELLITEMID',rSellItemid,[]))
       then begin
         sleep(1000);
         dm.taSellItem.Transaction.CommitRetaining;
         dm.taSellItem.Edit;
         dm.taSellItemF.AsInteger := -1;
         dm.taSellItem.Post;
         dm.taSellItem.Delete;
         dm.taSellItem.Transaction.CommitRetaining;
       end;
       if (itypeoperator = 'UP') and (dm.taSellItem.Locate('SELLITEMID',rSellItemid,[])) then
       begin
         sleep(1000);
         dm.taSellItem.Refresh;
       end;
      end;
      //FInserSellItem:=false;
     end;
    end;
   finally
    ms.Free;
  end;

end;}

procedure TdmServ.taJSzSumBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('USERID').AsInteger := dmCom.UserId;
  TpFIBDataSet(DataSet).ParamByName('CompSz').AsInteger := SzMode;
end;

procedure TdmServ.taJSzAfterPost(DataSet: TDataSet);
begin
  if taJSzzq.OldValue <> taJSzzq.Value then
    dmServ.IsEdtAppl := true;
  taJSz.Transaction.CommitRetaining;
end;

procedure TdmServ.taJSzCalcFields(DataSet: TDataSet);
var
  DefaultSeparator : char;
begin
  DefaultSeparator := DecimalSeparator;
  try
    DecimalSeparator := '.';
  finally
    DecimalSeparator := DefaultSeparator;
  end;

end;

procedure TdmServ.taTotJSzBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('userid').AsInteger := dmCom.UserId;
end;

procedure TdmServ.taJDepCalcFields(DataSet: TDataSet);
var totSum: real;
begin
  with dataset do
  begin
    if (FieldByName('SUM').AsFloat = 0) or ((FieldByName('SUM').AsFloat - FieldByName('SUM3').AsFloat) =0 ) then
    begin
      FieldByName('Sell_In').AsFloat := 0;
      FieldByName('Sell_InRet').AsFloat := 0;
    end
    else
    begin
      with quTmp do
      begin
        Close;
        Sql.Text := 'select Sum(s.inpricesell) ' + taJDep.SelectSQL[1]+ ' ' + taJDep.SelectSQL[2]
        + ' '+taJDep.SelectSQL[3]+ '  '+ taJDep.SelectSQL[4]+ '  ' + taJDep.SelectSQL[5] + '  '+
        taJDep.SelectSQL[6]+'  ' + taJDep.SelectSQL[7]+ '  ' + taJDep.SelectSQL[8] ;
        ExecQuery;
        TotSum := Fields[0].AsFloat;
        Transaction.CommitRetaining;
        Close;
      end;
      FieldByName('Sell_In').AsFloat := FieldByName('SUM3').AsFloat *100 /
                                        FieldByName('SUM').AsFloat;
      FieldByName('Sell_InRet').AsFloat := FieldByName('SUM3').AsFloat *100 /
                                        (FieldByName('SUM').AsFloat - FieldByName('SUM1').AsFloat);
      FieldByName('SellDep').AsFloat := FieldByName('SUM3').AsFloat *100 /TotSum;
    end;
  end;
end;

procedure TdmServ.quInventoryBeforeOpen(DataSet: TDataSet);
begin
 if FInvIn<>''
 then quInventory.SelectSQL[15]:=' and ISIN in '+FInvIn+' '
 else quInventory.SelectSQL[15]:=' ';

 with dm, quInventory do
 begin
  if lMultiSelect_I and (SD_MatId_I<>'')then
     SelectSQL[16]:=' and D_MatId in ('+SD_MatID_I+') '
  else
     SelectSQL[16]:=' ';

  if lMultiSelect_I and (SD_GoodID_I<>'')then
     SelectSQL[17]:=' and D_GoodId in ('+SD_GoodID_I+') '
  else
     SelectSQL[17]:=' ';

  if lMultiSelect_I and (SD_CompID_I<>'')then
     SelectSQL[18]:=' and D_CompId in ('+SD_CompID_I+') '
  else
     SelectSQL[18]:=' ';

  if lMultiSelect_I and (SD_SupID_I<>'')then
     SelectSQL[19]:=' and D_SupId in ('+SD_SupID_I+') '
  else
     SelectSQL[19]:=' ';

  if lMultiSelect_I and (SD_Note1_I<>'')then
     SelectSQL[20]:=' and d_note1 in ('+SD_Note1_I+') '
  else
     SelectSQL[20]:=' ';

  if lMultiSelect_I and (SD_Note2_I<>'')then
     SelectSQL[21]:=' and d_note2 in ('+SD_Note2_I+') '
  else
     SelectSQL[21]:=' ';

  if lMultiSelect_I and (SD_Att1_I<>'')then
     SelectSQL[22]:=' and d_att1id in ('+SD_Att1_I+') '
  else
     SelectSQL[22]:=' ';

  if lMultiSelect_I and (SD_Att2_I<>'')then
     SelectSQL[23]:=' and d_att2id in ('+SD_Att2_I+') '
  else
     SelectSQL[23]:=' ';
 end;

 quInventory.Prepare;
 dmCom.SetArtFilter(quInventory.Params);
// quInventory.ParamByName('ISINVID').AsInteger:=-1;
 quInventory.ParamByName('ISINVID').AsInteger:=CurInventory;
 if dmcom.FilterDepID=0 then
 begin
  quInventory.ParamByName('DEPID1').AsInteger:=-MaxInt;
  quInventory.ParamByName('DEPID2').AsInteger:=MaxInt;
 end
 else
 begin
  quInventory.ParamByName('DEPID2').AsInteger:=dmcom.FilterDepID;
  quInventory.ParamByName('DEPID1').AsInteger:=dmcom.FilterDepID;
 end
end;

procedure TdmServ.quInventoryCalcFields(DataSet: TDataSet);
begin
 if quInventoryISIN.AsInteger=0 then quInventoryDescription.AsString:='������� �� ������� �� ������';
 if quInventoryISIN.AsInteger=1 then quInventoryDescription.AsString:='������� ������� �� ������';
 if quInventoryISIN.AsInteger=2 then quInventoryDescription.AsString:='������� �� �������� �� ������';
   end;

procedure TdmServ.quInventoryAfterPost(DataSet: TDataSet);
begin
  dmcom.tr.CommitRetaining;
end;

{ ������� MessageDlg ����������� ������ ��� ������� ��������� ���� }

{ TODO : MessageDlgCtr }
function TdmServ.MessageDlgCtr(const Msg: string; DlgType: TMsgDlgType;
  Buttons: TMsgDlgButtons; HelpCtx: Longint): Integer;
begin
  with CreateMessageDialog(Msg, DlgType, Buttons) do
  try
    HelpContext := HelpCtx;
    If (Form <> nil) and (Form.Name<>'') then
    begin
     Left := Form.Left + (Form.Width div 2)-(Width div 2);
     Top := Form.Top + (Form.Height div 2) - Height;
    end;
    Result := ShowModal;
  finally
    Free;
  end;
end;

{************ ������ �� �������� ***********************************************
 * ����� ������ ���������:
 * 1. ���������� �� ������� ��� ����������� � ������������:
 *    - ����� ����.���� (��� ������� Article ��� ����.����� ���������� DiscountCard)
 *    - ��������������� ��� ��������� ���
 * 2. ���� ������ ��� ������� � �����, �� ���������� �������������� ��������,
 *    ����� �� ��������� ���. ��� ����� ������������� �������� �����������
 *    �����, ����������� EAN13ControlDigits �� ��c������������� ������ �
 *    �������� 13�� ������� ����.����. (��� ����� ������� �� 13 ��������:
 *    12 �������� � 13� ������ - ����������� �����.)
 * 3. ���� �������� ������ ������� (��� ��������� �����), �� ��������� ��������
 *    ���������� �� ���� ��������� Insert_Record, ������� �������� ���
 *    �������� � �������� ���� ����������
 ******************************************************************************}


procedure TdmServ.OnScanCode(ScanCode: string);
var
  Code: string;
  Article: Integer;
  CodeClass: TScanCodeClass;
  ean13: char;
  Handled: boolean;
  //------- ������ ����������� ����� -----------//
  function EAN13ControlDigits(Code: string): Char;
  var
     i, s1, s2, s3: Integer;
  begin
     s1 := 0;
     s2 := 0;
     for i := 1 to 12 do
     begin
        if Odd(i) then s1 := s1 + Ord(Code[i]) - Ord('0')
        else s2 := s2 + Ord(Code[i]) - Ord('0');
     end;
     s3:=10 - ((s1 + s2*3) mod 10);
     if s3=10 then s3:=0;
     Result := IntToStr(s3)[1];
  end;
 //---------------------------------------------//
begin
  if Form = nil then Form:=Screen.ActiveForm
     else if (Screen.ActiveForm.Name<>'') then Form:=Screen.ActiveForm;

  ParseScanCode(ScanCode, CodeClass, Code); // ������ ����. ���� (������������ ����� ���� � ��������� ��������)
  if (CodeClass = scArticle) or (CodeClass = scSertificate) then  // ������ ��������� ������ ��� �������! ������ � ����.������� ����� �� ������������!
  begin
    ean13:=EAN13ControlDigits(ScanCode); // 13 ������ - ����������� ����� (������� �� ������������������ ����)

    if ean13 <> Code[Length(Code)] then
    begin
       MessageDlgCtr('������������ �����-���.', mtInformation, [mbOK], 0);
       exit;
    end;

    SetLength(Code, Length(Code) - 1); // �������� ��������� ������ ���� (����.�����)

    if (fmMain.ActivFm) and (not fmMain.Show_msg) then
       Handled := True else
       Handled := False;

    if Handled then
    begin
       handled:=false;
       fmMain.Show_msg:=true;
       Beep;
       ShowMessage('�������� ����������! ��� �������� ��� �������� ���������.');
       Handled:=true;
       fmMain.Show_msg:=false;
    end
    else
       Insert_Record(Code); // �������� ��.������ ���������������� ������� � �������� �����
  end
  else
  begin
    MessageDlgCtr('������������ �����-���.', mtInformation, [mbOK], 0);
    exit;
  end;
end;

{* ��������������� ��� ���������� � �������� �����.
   ����������� ������ ������ � ��������.
   ������ � ��������� ����������� � ����� ��� ���� ���������� dll *}
procedure TdmServ.Insert_Record (ValScan:string);
var
  i,j, clientid, DepId: integer;
  clientname, nodcard, sqlstr, Contract: string;
  T, selid, SItemId, uid: integer;
  FieldsRes:t_array_var;
  fl, IsEnd:boolean;
  sn:integer;
  dinvid:integer;
  ds:TpFIBDataSet;
  FExists, order_, isin:integer;
  LogOperationID,LogOprIdForm: string;
  r :variant;
  inventoryid, fres:integer;
  fexsits:boolean;
  EditMode: TEditMode;
  SelectedDepartmentID: Integer;
begin
{ TODO : Insert_Record }
  sitemid := 0;
  dinvid := 0;
  sn := 0;
  ds := nil;
   LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
  {������ ������ ��� ��������� ����������}
  {*****************�����������*****************}

  if dm.WorkMode = 'Certificate Collation' then
  begin
    if DialogCertificateCollation <> nil then
    begin
      DialogCertificateCollation.OnCertificate(ValScan);
    end;

  end else

  if dm.WorkMode='Sert' then
  begin

     EditMode := fmSertificate.EditMode;

     if EditMode = emReadOnly then
     begin

       Exit;

     end;

     if EditMode = emInsert then
     begin

       if fmsertificate.EditorNominal.text = '' then
       begin

         ShowMessage('������� ������� �����������!')

       end else
       begin

         if fmSertificate.EditorOffice.Text='' then
         begin

           ShowMessage('�������� ������!');

         end else
         begin

           if (not fmSertificate.taSert.Locate('Sert_id', strToInt(ValScan),[])) then
           begin

             fmSertificate.taSert.Append;

             fmSertificate.taSertSERT_ID.Value:=strToInt(ValScan);

             fmSertificate.taSertNOMINAL.Value:=StrToInt(fmsertificate.EditorNominal.text);

             fmSertificate.taSertD_Depid.Value:= Integer(fmSertificate.EditorOffice.Items.Objects[fmSertificate.EditorOffice.ItemIndex]);

             fmSertificate.taSert.Post;

             fmSertificate.taSert.Transaction.CommitRetaining;

             fmSertificate.taSert.ReopenLocate('DEPNAME');

             fmSertificate.taSert.Last;

           end else
           begin

             ShowMessage('���������� � ����� ������� ��� ����������!');

           end;

         end;

       end;

     end;

     if EditMode = emDelete then
     begin

       SelectedDepartmentID := Integer(fmSertificate.EditorOffice.ItemIndex);

       if SelectedDepartmentID <= 0 then
       begin

         MessageDlg('�� ������� ������������� ��� �������� ������������!', mtError, [mbOk], 0);

         Exit;

       end;

       if fmSertificate.taSert.Locate('SERT_ID;D_DEPID', VarArrayOf([StrToInt(ValScan), SelectedDepartmentID]), []) then
       begin

         fmSertificate.acDel.Execute;

       end else
       begin

         MessageDlg('���������� � ������� ' + ValScan + ' �� ��������� �� ��������� �������!', mtWarning, [mbOk], 0);

       end;

     end;

  end;

  {*****************�������*********************}
   if dm.WorkMode = 'SURPLUS' then
   begin
    if dm.taSListISCLOSED.AsInteger = 1 then MessageDlgCtr('��������� �������', mtInformation, [mbOK], 0)
    else if ValScan='' then MessageDlgCtr('�� ����� ������ ����� �������', mtInformation, [mbOK], 0)
    else begin
     with dm, qutmp do
     begin
      close;
      sql.Text:='select fres, sitemid from SurPlus_I ('+ValScan+', '+dm.taSListSINVID.AsString+')';
      ExecQuery;
      if (not Fields[1].IsNull) then sitemid:=Fields[1].AsInteger;
      i:=Fields[0].AsInteger;
      close;
      Transaction.CommitRetaining;
     end;

     fexsits:=false;
     case i of
      -1: MessageDlgCtr('������� � ����� ������� �� ������������ �� ������', mtInformation, [mbOK], 0);
      0: MessageDlgCtr('������� �� �����', mtInformation, [mbOK], 0);
      2: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
      4: MessageDlgCtr('������� ������� �����', mtInformation, [mbOK], 0);
      5: MessageDlgCtr('������� ���������� ����������', mtInformation, [mbOK], 0);
      6: fexsits:=true
      else MessageDlgCtr('������ � ���� ���������� � �������������!!!', mtInformation, [mbOK], 0);
     end;

     if fexsits then
     begin
      ReOpenDataSets([dm3.quSurPlusItem]);
      dm3.quSurPlusItem.Locate('SITEMID', sitemid, []);
      dm.taSList.Refresh;
     end;
    end;
   end;
  (****************��� �������� ������������ ���-�� ������� � �������, ���������� �� ���������*)
  if dm.WorkMode = 'CHECKDINV' then
  begin
    with dm3, dm3.quDInvCheck, qutmp do
    begin
      if Locate('O_UID', ValScan, []) then
      begin
        if quDInvCheckO_Status.AsInteger <> 2 then
        begin
          if State <> dsEdit then Edit;
          quDInvCheckO_Status.AsInteger := 1;
          Post;
        end
      end
      else
      begin
        if CHECKMode = 'DINV' then
          sqlstr := 'select O_STATUS from DInvCheck_S('+ dm.taDListSINVID.AsString +') where O_UID = ' + ValScan
           else if CHECKMode = 'SRET' then
                   sqlstr := 'select O_STATUS from DInvCheck_S('+ dm.taSRetListSINVID.AsString +') where O_UID = ' + ValScan;

        r := ExecSelectSQL(sqlstr, quTmp);

        if VarIsNull(r) then
        begin
          if State <> dsInsert then Insert;
          quDInvCheckO_UID.AsString := ValScan;
          quDInvCheckO_STATUS.AsInteger := 2;
          Post;
        end
        else
        if Integer(r) <> 2 then
        begin
          if CHECKMode = 'DINV' then
             ExecSQL('update DInvCheck Set Status = 1 where uid = ' + ValScan +
                     ' and SInvId = ' + dm.taDListSINVID.AsString, quTmp)
          else
            if CHECKMode = 'SRET' then
               ExecSQL('update DInvCheck Set Status = 1 where uid = ' + ValScan +
                       ' and SInvId = ' + dm.taSRetListSINVID.AsString, quTmp);
        end
      end;
      Refresh;
    end;
  end;
  {***************��� ��������������********************}
  if (dm.WorkMode='INVENTORY'){and(fmInventory.Enabled)} then
   begin
    with dmcom, quTmp do
    begin
     close;
     sql.Text:='select FExists, order_, isin from EDIT_INVENTORY('+ValScan+', 0, '+inttostr(CurInventory)+')';
     ExecQuery;
     FExists:=Fields[0].AsInteger;
     order_:=Fields[1].AsInteger;
     isin:=Fields[2].AsInteger;
     close;
     Transaction.CommitRetaining;
    end;
    if FExists=1 then
    begin
     if quInventory.Locate('UID',ValScan,[]) then
     begin
      quInventory.Edit;
      quInventoryORDER_.AsInteger:=order_;
      quInventoryISIN.AsInteger:=isin;
      quInventory.Post;
      quInventory.Refresh;
      inventoryid:=quInventoryINVENTORYID.AsInteger;
      quInventory.Delete;
      quInventory.Append;
      quInventoryINVENTORYID.AsInteger:=inventoryid;
      quInventoryFL.AsInteger:=-1;
      quInventory.Post;
      quInventory.Refresh;
     end
     else
      with dmcom, quTmp do
      begin
       close;
       sql.Text:='select FExists, order_, isin from EDIT_INVENTORY('+ValScan+', 1, '+inttostr(CurInventory)+')';
       ExecQuery;
       Transaction.CommitRetaining;
       close;
      end;

    end
    else
    begin
     if centerdep then
     begin
      r:=ExecSelectSQL('select sinvid from find_sinvid ('+IntToStr(dmServ.CurInventory )+')', dmCom.quTmp);
      if VarIsNull(r) then
       raise Exception.Create('�� ������� ������ � sinv');
     end;

     quInventory.Append;
     quInventoryINVENTORYID.AsInteger:=dmcom.GetID(45);
     quInventoryORDER_.AsInteger:=order_;
     quInventoryUID.AsInteger:=strtoint(ValScan);
     quInventoryFL.AsInteger:=0;
     if CenterDep then quInventorySINVID.AsInteger:=r
     else quInventorySINVID.AsInteger:=quListInventorySINVID.AsInteger;
     quInventory.Post;
     quInventory.Refresh;
     quInventory.Edit;
     quInventoryFL.AsInteger:=-1;
     quInventory.Post;
     quInventory.Refresh;
    end;

   {����������� ������������ ����������� ��� ����������� ������ ������}
    with dmcom, quTmp do
    begin
     close;
     sql.Text:='select precent from Precent_Execution_Inventory ('+IntToStr(CurInventory)+')';
     ExecQuery;
     Inventory_Precent:=round(Fields[0].AsFloat);
     Transaction.CommitRetaining;
     close;
     fmInventory.prbar1.Position:=Inventory_Precent;
    end;
   end;
     {***************��������� �����. �����������**********}
   If (Dm.WorkMode='DINV') and ((Assigned(fmDinv)) or (Assigned(fmDinvitem))) then
    begin
     PostDataSets([dm.taDList]);
     dmCom.tr.CommitRetaining;
     if not dm.checkInv_RUseDep(dm.taDListDEPFROMID.AsInteger) then
     raise Exception.Create('������ ������������� ��������� � ������� '#39 +
     dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������');
     try
      dmcom.ExecuteQutmp(dm.quTmp,'  select uidwhcalc  from d_rec ',1, FieldsRes);
      if FieldsRes[0]=null then t:=0 else t:=FieldsRes[0];
     finally
      Finalize(FieldsRes);
     end;
     If t = 1 then
      MessageDlgCtr('��������� ������ �����������', mtInformation, [mbOK], 0)
     else
     begin
      if dm.taDListISCLOSED.AsInteger=1 then
       MessageDlgCtr('��������� �������!!!', mtInformation, [mbOK], 0)
      else
       begin
        with dm2, quInsDUID, Params do
         begin
          uid := StrToInt(ValScan);
          ByName['UID'].AsInteger:=StrToInt(ValScan);
          ByName['SINVID'].AsInteger:=dm.taDListSInvId.AsInteger;
          ByName['DEPID'].AsInteger:=dm.taDListDepId.AsInteger;
          ByName['DEPFROMID'].AsInteger:=dm.taDListDepFromId.AsInteger;
          ByName['OPT'].AsInteger:=0;
          ExecQuery;

          SElId:=FieldByName('SELID').AsInteger;
          SItemID := FieldByName('SITEMID').AsInteger;

          SItemID := FieldByName('SItemID').AsInteger;

          T:=FieldByName('T').AsInteger;

          Close;
         end;

         if dm.quDInvItem.Active then {��������� ������������ ��-��������}
          case T of
           0,1: begin
                 dm.quDInvItem.Append;
                 dm.quDInvItemSItemID.AsInteger := SItemID;
                 dm.quDInvItem.Post;
                 //dm.quDInvItem.Refresh;
                 //dm.quDInvItem.Locate('uid',uid,[]);
                end;
           2: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
           3: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
           4: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
          end;

         if dm.taSEl.Active  and (not dm.quDInvItem.Active)  then {��������� ������������� ��-����������}
          case T of
              0: begin
                   if dm.taSEl.Locate('SELID', SElID, []) then dm.taSEl.Refresh;
                   dm.taDList.Refresh;
                 end;
              1: begin
                  ReOpenDataSets([dm.taSEl]);
                  dm.taSEl.Locate('SELID', SElId, []);
                 end;
              2: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
              3: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
              4: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
          end;

         dmCom.tr.CommitRetaining;
         dm.taDList.Refresh;
       end;
     end;
    end;
    {***************������ � ����**********}
    if ((dm.WorkMode='ALLSELL')or(dm.WorkMode='SELL')or(dm.WorkMode='SELLCH')) and(dm.taSellItem.Active) then
    begin
      if (not dm.SellRet)then
      begin
       fl:=true;
       if ((dm.WorkMode<>'SELLCH')and(dmcom.User.ALLSeLL=0)) then
       begin
         MessageDlgCtr('��� ���� �� ����������', mtInformation, [mbOK], 0);
         fl:=false;
       end;
       {� ���� ��������� ������� � ������� ������}
       RepeatClick:=false;

       dm.SellRet:=False;
       UID := strtoint(ValScan);
       if fl then
       begin
        with dm2, quGetUID do
         begin
           Active:=False;
           ParamByName('AUID').AsInteger:=uid;
           if dm.WorkMode='ALLSELL' then  ParamByName('ADEPID').AsInteger:=dm.taSellListDepId.AsInteger
            else   ParamByName('ADEPID').AsInteger:=dm.taCurSellDepId.AsInteger;
           Active:=True;
         end;

        case dm2.quGetUIDRes.AsInteger of
         1: begin
             MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
             fl:=false;
            end;
         2: begin
             MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
             fl:=false;
           end;
         3: begin
             MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
             fl:=false;
           end;
        end;

        if fl then
         begin
          if dm2.quGetUIDSItemId.IsNull then
          begin
           MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
          end
          else
          begin
           {������� �� ����� ���� ������� ��� ���������� � ����� � ������� sellid ��� ����
            ������� ��� ���������� �����}
           dm3.quGetSmallestSell.Close;
           dm3.quGetSmallestSell.ParamByName('AUID').AsInteger:=dm2.quGetUIDUID.AsInteger;
           dm3.quGetSmallestSell.ParamByName('ASELLID').AsInteger:=dm.taSellItemSELLID.AsInteger;
           dm3.quGetSmallestSell.ParamByName('ARet').AsInteger:=0;
           dm3.quGetSmallestSell.ExecQuery;
           if dm3.quGetSmallestSell.Fields[0].AsInteger=1 then
            begin
             MessageDlgCtr(trim(dm3.quGetSmallestSell.Fields[1].AsString), mtInformation, [mbOK], 0);
             fl:=false;
            end;
           dm3.quGetSmallestSell.Transaction.CommitRetaining;
           dm3.quGetSmallestSell.close;

           if fl then
           begin
            LogOperationID:=dm3.insert_operation(sLog_AddSellScan,LogOprIdForm);
            dm.taSellItem.Append;
            dm.taSellItemArt2Id.AsInteger:=dm2.quGetUIDArt2Id.AsInteger;
            dm.taSellItemFullArt.AsString:=dm2.quGetUIDFullArt.AsString;
            dm.taSellItemArt2.AsString:=dm2.quGetUIDArt2.AsString;
            dm.taSellItemPrice.AsFloat:=dm2.quGetUIDPrice.AsFloat;
            dm.taSellItemPrice0.AsFloat:=dm2.quGetUIDPrice.AsFloat;
            dm.taSellItemSZ.AsString:=dm2.quGetUIDSZ.AsString;
            dm.taSellItemW.AsFloat:=dm2.quGetUIDW.AsFloat;
            dm.taSellItemUID.AsString:=dm2.quGetUIDUID.AsString;
            dm.taSellItemSItemId.AsInteger:=dm2.quGetUIDSItemId.AsInteger;
            dm.taSellItemUnitId.AsInteger:=dm2.quGetUIDUnitId.AsInteger;
            dm.taSellItem.Post;

            dmCom.tr.CommitRetaining;

            ExecSQL('update sitem set busytype=2 where sitemid='+inttostr(dm.taSellItemSItemId.AsInteger), dm.quTmp);
            dm3.update_operation(LogOperationID);
            if (dm.WorkMode = 'SELLCH')and(
              not( (dm.taSellItemCHECKNO.AsInteger=0)or(dm.taSellItemCHECKNO.IsNull)))
            then raise Exception.Create(Format('������ [003]. ������ ���������� � �������������!'#13'UID=%s, WorkMode=%s', [dm2.quGetUIDUID.AsString, dm.WorkMode]));
           end
          end;
         end;
         dm2.quGetUID.Active:=False;
       end;
     end
    else
     begin // ������� �������
      if not dm.quSelled.Locate('UID',ValScan,[]) then
        MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0)
      else
      begin
      {������� �� ����� ���� ������� ��� ���������� � ����� � ������� sellid ��� ����
       ������� ��� ���������� �����}
       fl:=true;
       dm3.quGetSmallestSell.Close;
       dm3.quGetSmallestSell.ParamByName('AUID').AsInteger:=strtoint(ValScan);
       dm3.quGetSmallestSell.ParamByName('ASELLID').AsInteger:=dm.taSellItemSELLID.AsInteger;
       dm3.quGetSmallestSell.ParamByName('ARet').AsInteger:=1;
       dm3.quGetSmallestSell.ExecQuery;
       if dm3.quGetSmallestSell.Fields[0].AsInteger=1 then
       begin
        MessageDlgCtr(trim(dm3.quGetSmallestSell.Fields[1].AsString), mtInformation, [mbOK], 0);
        fl:=false;
       end;
       dm3.quGetSmallestSell.Transaction.CommitRetaining;
       dm3.quGetSmallestSell.close;

       if fl then
       begin
        with dm, qutmp do begin
         close;
         sql.text:='select first 1 respstoringid from respstoring where stateuid<4 and uid='+quSelledUID.AsString;
         ExecQuery;
         if Fields[0].IsNull then fl:=true else fl:=false;
         close;
         Transaction.CommitRetaining;
         if not fl then MessageDlgCtr('������� �� ������������� ��������', mtInformation, [mbOK], 0);
        end;
        if fl then begin
         if NOT (dm.taSellItem.State in [dsInsert, dsEdit]) then dm.taSellItem.Edit;
         dm.taSellItemRetSellItemId.AsInteger:=dm.quSelledSellItemId.AsInteger;
         dm.taSellItemArt2Id.AsInteger:=dm.quSelledArt2Id.AsInteger;
         dm.taSellItemArt2.AsString:=dm.quSelledArt2.AsString;

         dm.taSellItemPrice.AsFloat:=dm.quSelledPrice.AsFloat;
         dm.taSellItemPrice0.AsFloat:=dm.quSelledPrice0.AsFloat;
         dm.taSellItemSZ.AsString:=dm.quSelledSZ.AsString;
         dm.taSellItemW.AsFloat:=dm.quSelledW.AsFloat;
         dm.taSellItemUID.AsString:=dm.quSelledUID.AsString;
         dm.taSellItemCLIENTID.AsInteger:=dm.quSelledCLIENTID.AsInteger;
         dm.taSellItemCLIENTNAME.AsString:=dm.quSelledCLIENT.AsString;

         if dm.WorkMode='SELLCH' then
         begin
          if fmSellEdit.FlRetClient then
          begin
            dm.taSellItemRETCLIENT.AsString:=fmSellEdit.RetClient;
            dm.taSellItemRETADDRESS.AsString:=fmSellEdit.retaddress;
          end
          else fmSellEdit.FlRetClient:=true;
         end;


         dm.qutmp.close;
         dm.qutmp.sql.Text:='select nodcard from client where clientid = '+dm.quSelledCLIENTID.AsString;
         dm.qutmp.ExecQuery;
         dm.taSellItemNODCARD.AsString:=dm.qutmp.Fields[0].AsString;
         dm.qutmp.Close;

         {************************}
         fmSellEdit.CanselFl:=false;
         fmSellEdit.oldclientid:=dm.taSellItemCLIENTID.AsInteger;
         fmSellEdit.oldclientname:= dm.taSellItemCLIENTNAME.AsString;
         fmSellEdit.oldnodcard:= dm.taSellItemNODCARD.AsString;
         fmSellEdit.newidclient:=fmSellEdit.oldclientid;
         fmSellEdit.newnodcard:=fmSellEdit.oldnodcard;
         fmSellEdit.newclientname:=fmSellEdit.oldclientname;
         fmSellEdit.edNodCard.Text:= fmSellEdit.oldnodcard;
         fmSellEdit.edClient.Text := fmSellEdit.oldclientname;
         {************************}


         if NOT dm.quSelledSItemId.IsNull then
          dm.taSellItemSItemId.AsInteger:=dm.quSelledSItemId.AsInteger;
         if dm.WorkMode<>'SELLCH' then
         begin
          if NOT dm.quSelledClientId.IsNull then
           dm.taSellItemClientId.AsInteger:=dm.quSelledClientId.AsInteger;
          dm.taSellItemCheckNo.AsVariant := dm.quSelledCheckNo.AsVariant;
         end
         else
         begin
          with dm.qutmp do
          begin
           dm.qutmp.close;
           sql.Text:='select clientid from sellitem where sellitemid = '+#13#10+
                     '(select max(sellitemid) from sellitem where uid = '+dm.quSelledUID.AsString+
                     ')';
           ExecQuery;
           clientid:=Fields[0].AsInteger;
           dm.qutmp.close;
           if clientid = 0 then clientid:=-1;
           sql.Text:='select name, nodcard from client where clientid = '+inttostr(clientid);
           ExecQuery;
           clientname:=Fields[0].AsString;
           nodcard:=Fields[1].AsString;
           dm.qutmp.Close;
          end;
          dm.taSellItemCLIENTID.AsInteger:=clientid;
          dm.taSellItemCLIENTNAME.AsString:=clientname;
          dm.taSellItemNODCARD.AsString:=nodcard;
         end;

         dm.taSellItemUnitId.AsInteger:=dm.quSelledUnitId.AsInteger;
         dm.taSellItemADate.AsDateTime:=dm.quSelledSellDate.AsDateTime;
         dm.taSellItemFullArt.AsString:=dm.quSelledFullArt.AsString;

         if (dm.WorkMode='SELL')or(dm.WorkMode='SELLCH') then DepId:=dm.taCurSellDepId.AsInteger
         else if dm.WorkMode='ALLSELL' then DepId:=dm.taSellListDepId.AsInteger;
         dm.taSellItemRetPrice.AsFloat:=dm2.GetPrice(DepId, dm.taSellItemArt2Id.AsInteger);

         dmCom.tr.CommitRetaining;

         if (dm.taSellItemRET.AsInteger = 0) then
         begin
          fmSellEdit.ActiveControl:=fmSellEdit.ednodcard;
          fmSellEdit.ednodcard.SetFocus;
          fmSellEdit.ednodcard.SelectAll;
         end
         else
         begin
          fmSellEdit.ActiveControl := fmSellEdit.dbehretclient;
          fmSellEdit.dbehretclient.SetFocus;
          fmSellEdit.dbehretclient.SelectAll;
         end;
        end
       end;
      end;
     end;
    end;
{*******************�������� �����������*******************}
   if  (dm.WorkMode='SRET') and (dm.taSRet.Active){ and (fmSRet.Active)} then
   begin
//   ShowMessage('begin');
    with dm, dm2, dmCom do
      begin
        fl:=true;
        if taSRetListIsClosed.AsInteger=1 then
        begin MessageDlgCtr('��������� �������!!!', mtInformation, [mbOK], 0);
              fl:=false end;
        if fl then
        begin
         with quCheckSupUID, Params do
           begin
             uid := StrToInt(ValScan);
             ByName['UID'].AsInteger:=StrToInt(ValScan);
             ByName['DEPID'].AsInteger:=taSRetListDepFromId.AsInteger;
             ByName['SUPID'].AsInteger:=taSRetListCompId.AsInteger;
             ByName['CONTRACT$ID'].AsInteger:=taSRetListCONTRACTID.AsInteger;             
             ExecQuery;

             SItemId:=FieldByName('SItemID').AsInteger;
             T:=FieldByName('T').AsInteger;
             Contract := FieldByName('contract').AsString;
             Close;
           end;

//         if T > 100 then
//         begin
//           T := 1;
//         end;

         if T > 99 then
         begin
           if T = 100 then
           begin
             MessageDialog('�� ������ �������.', mtError, [mbOk], 0);
           end else
           if T = 101 then
           begin
             MessageDialog('������� �� ����� �������� ��������, �� ���� ��������� � ���������. ', mtError, [mbOk], 0);

             with taSRet do
                  begin
                    Append;
                    taSRetSItemId.AsInteger:=SItemId;
                    Post;
                    Last;
                    refresh;  // dm.taSRet.Locate('UID', edUID.text, []);
                  end;
           end else
           if T = 102 then
           begin
              MessageDialog('������� ������� �� ��������� � ��������� ��������� ��  �������� : "��� ��������".', mtError, [mbOK], 0);
           end
           else
           if T = 103 then
           begin
             MessageDialog('������ ��������� ����������� � �� ������������ �������.'#13#10 + '������� �������: ' + Contract, mtError, [mbOK], 0);
           end;
         end;


         case T of
             1: with taSRet do
                   begin
                //     ShowMessage('append');
                     Append;    // ������
              //       ShowMessage('SItemId');
                     taSRetSItemId.AsInteger:=SItemId;
            //         ShowMessage('Post');
                     Post;
          //           ShowMessage('last');
        //             last;   //Locate('UID', UID, []);
      //               ShowMessage('end');
                   end;
             2: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
             3: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
             4: MessageDlgCtr('������� ���������� ������ �����������', mtInformation, [mbOK], 0);
             5: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
           end;
        end;
      end;
    //  ShowMessage('end');
   end;
{*****************������������� �� ������ ���������********************}
{*****************������������� �� ���������  � ��������***************}
{*****************������������� �� ������ ���������********************}
  if (Assigned(fmDst2)or Assigned(fmDst3)or Assigned(fmDst))and(dm.WorkMode='DINV'){and
     (fmDst3.Active and fmDst.Active or fmDst2.Active)} then
  begin
   if Assigned(fmDst2) then begin
    dinvid:=fmDst2.DToId;
    sn:=fmDst2.Select_Sinvid(fmDst2.DToId);
    ds:=dm.quD_WH
   end
   else
    if Assigned(fmDst3) then begin
     dinvid:=fmDst3.DToId;
     sn:=fmDst3.Select_Sinvid(fmDst3.DToId);
     ds:=dm2.quD_WH3
    end
    else
    if Assigned(fmDst) then begin
     dinvid:=fmDst.DToId;
     sn:=fmDst.Select_Sinvid(fmDst.DToId);
     ds:=dm2.quD_WH2
    end;

    with dm, dm2, dmCom do
      begin
       if dinvid=-1 then
        MessageDlgCtr('����� �� ������!!!', mtInformation, [mbOK], 0)
       else
       begin
        with qutmp do
         begin
          Sql.Text := '  select uidwhcalc  from d_rec ';
          ExecQuery;
          i:= Fields[0].AsInteger;
          Transaction.CommitRetaining;
          close;
         end;
        If i = 1 then
         MessageDlgCtr('��������� ������ �����������', mtInformation, [mbOK], 0)
        else
        begin
         {����������� ������������� ���� ��������� � �� ����������}
         with qutmp do
         begin
          sql.Text:='select isclosed, sinvid, userid from sinv where sinvid='+inttostr(sn);
          ExecQuery;
          if Fields[1].IsNull then
           begin
             IsEnd:=true;
             MessageDlgCtr('��������� �������', mtInformation, [mbOK], 0)
           end
          else if Fields[0].AsInteger=1 then
           begin
             IsEnd:=true;
             MessageDlgCtr('��������� �������', mtInformation, [mbOK], 0)
           end
          else if (not Fields[2].IsNull) and (Fields[2].asinteger<>dmcom.UserId) then
           begin
             IsEnd:=true;
             MessageDlgCtr('��������� ������� ��. �������������', mtInformation, [mbOK], 0)
           end
          else IsEnd:=false;
          Transaction.CommitRetaining;
          close;
         end;
         {********************************************************}
{         if sn=0 then
          MessageDlgCtr('��������� �������', mtInformation, [mbOK], 0)
         else}
         if not IsEnd then
         begin
          with quInsDUID2, Params do
           begin
            ByName['UID'].AsInteger:=StrToInt(ValScan);
            ByName['SINVID'].asinteger:=sn;
            ByName['DEPID'].AsInteger:=dinvid;
            ByName['DEPFROMID'].AsInteger:=DstDepId;
            ByName['OPT'].AsInteger:=0;
            ByName['CRUSERID'].AsInteger:=dmCom.UserId;
            ExecQuery;

            T:=FieldByName('T').AsInteger;

            tr.CommitRetaining;
            Close;
           end;
          case T of
           2: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
           3: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
           4: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);           
          end;
          if ds.Active then  ds.Refresh;
         end
        end;
       end;
      end;
   end;
   {************������� �������****************}
   if  (dm.WorkMode='OPT')and (dm.taSEl.Active) then
   begin
    with dm, dm2, dmCom do
      begin
        dmCom.tr.CommitRetaining;
        PostDataSets([taOptList]);
        if taOptListIsClosed.AsInteger=1 then MessageDlgCtr('��������� �������!', mtInformation, [mbOK], 0)
        else
        begin
         with quInsDUID, Params do
           begin
            ByName['UID'].AsInteger:=StrToInt(ValScan);
            ByName['SINVID'].AsInteger:=taOptListSInvId.AsInteger;
            ByName['DEPFROMID'].AsInteger:=taOptListDepFromId.AsInteger;
            ByName['OPT'].AsInteger:=1;
            ExecQuery;

            SElId:=FieldByName('SELID').AsInteger;
            T:=FieldByName('T').AsInteger;
            Close;
           end;
         case T of
            0: begin
                 if taSEl.Locate('SELID', SElID, []) then taSEl.Refresh;
                 taOptList.Refresh;
               end;
            1: begin
                 ReOpenDataSets([taSEl]);
                 taSEl.Locate('SELID', SElId, []);
                 taOptList.Refresh;
               end;
            2: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
            3: MessageDlgCtr('������� �������', mtInformation, [mbOK], 0);
            4: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);            
         end;
       end;
      end;
   end;
  {***********************���� ����������******************************}
  if (dm3.quActAllowances.Active) and (dm.WorkMode='ACTALLOW') then
  begin
   if dm3.quActAllowancesListISCLOSED.AsInteger=1 then
    MessageDlgCtr('��������� �������', mtInformation, [mbOK], 0)
   else begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select fres, sitemid from ActAllowances_I ('+ValScan+', '+
                       dm3.quActAllowancesListSINVID.AsString+')';
    dm.quTmp.ExecQuery;
    fres:=dm.quTmp.Fields[0].AsInteger;
    sitemid:=dm.quTmp.Fields[1].AsInteger;
    dm.quTmp.Close;
    dm.quTmp.Transaction.CommitRetaining;
    case fres of
     0: begin ReOpenDataSets([dm3.quActAllowances]);
              dm3.quActAllowances.Locate('SITEMID', sitemid, []) end;
     1: MessageDlgCtr('������� ������� � �������', mtInformation, [mbOK], 0);
     2: MessageDlgCtr('������� ������� �����', mtInformation, [mbOK], 0);
     3: MessageDlgCtr('������� ���������� ����������', mtInformation, [mbOK], 0);
     4: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);
     5: MessageDlgCtr('������� �� �������', mtInformation, [mbOK], 0);     
    end;
   end;
  end
end;
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
{procedure TdmServ.ComScanReceiveData(Sender: TObject; DataPtr: Pointer;
  DataSize: Integer);
var
  p :pchar;
  i,j: longint;
  ValScan: string;
begin    }
{ TODO : COM1 }
{  p := dataptr;
  for i:=1 to DataSize do
  begin
   if chr(ord(p^))<>#13  then dmcom.SScanZ:=dmcom.SScanZ+chr(ord(p^))
   else
    begin
     if Form = nil then Form:=Screen.ActiveForm
     else if (Screen.ActiveForm.Name<>'') then Form:=Screen.ActiveForm;
     if (dmCom.SScanZ[1]<>'2') or (length(dmCom.SScanZ)>13) then
     begin
      MessageDlgCtr('�� ����� ������ �����', mtInformation, [mbOK], 0);
      dmcom.SScanZ:=''
     end
     else
     try
      SetProgress('�������������� ���������� �� �������');

      j:=2; ValScan:='';
      while (dmcom.SScanZ[j]='0') and (j<=length(dmcom.SScanZ)) do inc(j);
      while (j<length(dmcom.SScanZ)) do begin ValScan:=ValScan+dmcom.SScanZ[j]; inc(j) end;
      dmcom.SScanZ:='';
      Insert_Record (ValScan);
      beep;
     finally
      UnProgress;
      dmcom.FBan1:=false;
     end;
   end;
   inc(p);
  end;
end; }

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////


procedure TdmServ.quInventoryAfterDelete(DataSet: TDataSet);
begin
 dmcom.tr.CommitRetaining;
end;


procedure TdmServ.CheckAnaliz(IsUpdate: integer; Old_Sz: string);
var sqlstr: string;
begin
  if (taApplSZ.AsString <> '') and (taApplARTID.AsString<> '') and
     (taApplQ.AsInteger <> 0)  then
  begin
    sqlstr := ' execute procedure CheckAnaliz(' + taApplAPPLID.AsString  + ', ' +
            taApplARTID.AsString + ', ''' +  taApplSZ.AsString + ''', ' +
            taApplQ.AsString +',' + taApplListUSERID.AsString  + ', ' + IntToStr(IsUpdate) +
            ', ''' + Old_Sz + ''')';
    ExecSQL(sqlstr,quTmp);
  end;
end;

procedure TdmServ.taJSzNewRecord(DataSet: TDataSet);
var i: integer;
begin
    with dm do
    begin
      quGetArt.ACtive:=False;
      quGetArt.Params[0].AsInteger:=Art2Id;
      quGetArt.ACtive:=True;

      taJSzARTID.AsInteger := quGetArtD_ARTID.AsInteger;
      taJSzSZ.AsString  := '-';
      taJSzIsSzOpt.AsInteger := SzMode;
      taJSzISCALC.AsInteger := 0;
      taJSzART.AsString := quGetArtArt.AsString;
      taJSzD_MATID.AsString := quGetArtD_MatId.AsString;
      taJSzD_GOODID.AsString := quGetArtD_GoodId.AsString;
      taJSzD_INSID.AsString := quGetArtD_InsId.AsString;
      taJSzCOMP.AsString := quGetArtProdCode.AsString;
      taJSzUNITID.AsInteger := quGetArtUnitId.AsInteger;

      for i :=0 to Pred(dmCom.DepCount) do
      begin
        taJSz.FieldByName('RW_' + IntToStr(dmcom.DepInfo[i].DepId)).AsFloat := 0;
        taJSz.FieldByName('RQ_' + IntToStr(dmcom.DepInfo[i].DepId)).AsInteger := 0;
      end;
    end;

end;

procedure TdmServ.taJSzBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  if taJSzISCALC.AsInteger = 1 then
  begin
    MessageDialog('������� �������� ��� ������� � �� ����� ���� ������', mtWarning, [mbOk], 0);
    SysUtils.Abort;
  end
end;

procedure TdmServ.taJSzARTIDChange(Sender: TField);
begin
//   if dmServ.taJSz.State<>dsInsert then
//      raise Exception.Create('�������������� ��������� ������ ������� � ���-�� ������ ');
end;

procedure TdmServ.taJSzSZChange(Sender: TField);
begin
  if taJSz.State<>dsInsert then
    if (taJSzISCALC.AsInteger = 1)then
    begin
      MessageDialog('�������������� ��������, ������������ ��� �������, �� ��������', mtWarning, [mbOk], 0);
      dmServ.taJSz.Cancel;
    end
end;

{procedure TdmServ.ComScan2ReceiveData(Sender: TObject; DataPtr: Pointer;
  DataSize: Integer);
var
  p :pchar;
  i,j: longint;
  ValScan: string;
begin      }
{ TODO : COM2 }
{  p := dataptr;
  for i:=1 to DataSize do
  begin
   if chr(ord(p^))<>#13  then dmcom.SScanM:=dmcom.SScanM+chr(ord(p^))
   else
    begin
     if Form = nil then Form:=Screen.ActiveForm
     else if (Screen.ActiveForm.Name='') then Form:=Screen.ActiveForm;
    
     if (dmCom.SScanM[1]<>'2')or (length(dmCom.SScanM)>13) then
     begin
      MessageDlgCtr('�� ����� ������ �����', mtInformation, [mbOK], 0);
      dmcom.SScanM:='';
     end
     else
     try
      SetProgress('�������������� ���������� �� �������');

      j:=2; ValScan:='';
      while (dmcom.SScanM[j]='0') and (j<=length(dmcom.SScanM)) do inc(j);
      while (j<length(dmcom.SScanM)) do begin ValScan:=ValScan+dmcom.SScanM[j]; inc(j) end;
      dmcom.SScanM:='';
      Insert_Record (ValScan);


      beep;
     finally
      UnProgress;
      dmcom.FBan2:=false;      
     end;
   end;
   inc(p);
  end;
end; }

procedure TdmServ.taMolAfterPost(DataSet: TDataSet);
begin
 dmcom.tr.CommitRetaining;
end;

procedure TdmServ.taMolAfterDelete(DataSet: TDataSet);
begin
 dmcom.tr.CommitRetaining;
end;

procedure TdmServ.taMolNewRecord(DataSet: TDataSet);
begin
 taMolPRESEDENT.AsInteger:=0;
 taMolMEMBER.AsInteger:=0;
 taMolD_DEPID.AsInteger:=SelfDepId;
 taMolCOLORSHOP.AsInteger:=dmCom.Dep[SelfDepId].Color;
end;

procedure TdmServ.taMolBeforeDelete(DataSet: TDataSet);
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmServ.taApplListAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

{procedure TdmServ.ap1Deactivate(Sender: TObject);
begin
 if not dmcom.IsActive then
 begin
  if ComScan.Connected then ComScan.Disconnect;
  if ComScan2.Connected then ComScan2.Disconnect;
 end
end; }

{procedure TdmServ.ap1Activate(Sender: TObject);
begin
 if not dmcom.IsActive then
 begin
  if ComScan.Connected then ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then ComScan.ComPort:=pnCOM3
     else  ComScan.ComPort:=pnCOM4;

  ComScan.Connect;
  dmcom.SScanZ:='';

  if ComScan2.Connected then ComScan2.Disconnect;
  if dmcom.ScanComM='COM1' then ComScan2.ComPort:=pnCOM1
   else  if dmcom.ScanComM='COM2' then ComScan2.ComPort:=pnCOM2
    else  if dmcom.ScanComM='COM3' then ComScan2.ComPort:=pnCOM3
     else  ComScan2.ComPort:=pnCOM4;

  ComScan2.Connect;
  dmcom.SScanM:='';
 end;
end;  }

procedure TdmServ.CommitRetaining(DataSet: TDataSet);
begin
  (DataSet as TpFIBDataSet).Transaction.CommitRetaining;
end;

procedure TdmServ.quInvSinvBeforeOpen(DataSet: TDataSet);
begin
 quInvSinv.ParamByName('SINVID').AsInteger:=CurInventory;
end;

procedure TdmServ.quInvSinvAfterPost(DataSet: TDataSet);
begin
 dmcom.tr.CommitRetaining;
end;

procedure TdmServ.taJSzzqChange(Sender: TField);
begin
  if (taJSz.State = dsEdit) and ( not FIsEdtAppl) then
        FIsEdtAppl := True;
end;

procedure TdmServ.quWHUIDAPPLDEPQChange(Sender: TField);
begin
  if DiscReEnter then Exit;
  DiscReEnter:=True;
  quWHUID.Edit;
  quWHUIDAPPLDEP_USER.AsInteger:=dmcom.UserId;
  quWHUID.Post;
  DiscReEnter:=False;
end;

procedure TdmServ.quWHUIDBeforeClose(DataSet: TDataSet);
begin
 PostDataSet(quWHUID);
end;

procedure TdmServ.taMolBeforePost(DataSet: TDataSet);
var
  Flag: Byte;
begin
  Flag := 0;

  if taMolISMOL.AsInteger = 1 then
  begin
    Flag := Flag or 1;
  end;

  if taMolISCHAIRMAN.AsInteger = 1 then
  begin
    Flag := Flag or 2;
  end;

  if taMolISMEMBER.AsInteger = 1 then
  begin
    Flag := Flag or 4;
  end;

  taMolMEMBER.AsInteger := Flag;
end;

initialization


end.

