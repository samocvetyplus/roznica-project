unit Err;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, rxPlacemnt;

type TString255=string[255];

type
  TfmErr = class(TForm)
    Image1: TImage;
    me1: TMemo;
    BitBtn1: TBitBtn;
    bbPrior: TBitBtn;
    bbNext: TBitBtn;
    FormPlacement1: TFormPlacement;
    procedure FormActivate(Sender: TObject);
    procedure bbPriorClick(Sender: TObject);
    procedure bbNextClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    e: array[0..10] of TString255;
    c, i: integer;
  end;

procedure ShowError(err: array of TString255);

var
  fmErr: TfmErr;

implementation

{$R *.DFM}

procedure ShowError(err: array of TString255);
var j: integer;
begin
  with fmErr do
    begin
      c:=High(err);
      for j:=0 to c do
        e[j]:=err[j];
      ShowModal;
    end;
end;


procedure TfmErr.FormActivate(Sender: TObject);
begin
 i:=0;
 me1.Text:=e[i];
 bbPrior.Enabled:=False;
 bbNext.Enabled:=c>0;
end;

procedure TfmErr.bbPriorClick(Sender: TObject);
begin
  if i=0 then exit;
  Dec(i);
  bbPrior.Enabled:=i>0;
  bbNext.Enabled:=c>0;
  me1.Text:=e[i];
end;

procedure TfmErr.bbNextClick(Sender: TObject);
begin
  if i=c then exit;
  Inc(i);
  bbNext.Enabled:=i<c;
  bbPrior.Enabled:=c>0;
  me1.Text:=e[i];
end;

procedure TfmErr.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_RETURN) or (Key=VK_ESCAPE) then Close;
end;

initialization



finalization



end.
