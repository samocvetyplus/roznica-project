object fmUseClient: TfmUseClient
  Left = 405
  Top = 165
  BorderStyle = bsDialog
  Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1080
  ClientHeight = 499
  ClientWidth = 468
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dg1: TDBGridEh
    Left = 0
    Top = 0
    Width = 468
    Height = 499
    Align = alClient
    DataSource = dmCom.dsUseClient
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Color = clBtnFace
        EditButtons = <>
        FieldName = 'CLIENTID'
        Footers = <>
        ReadOnly = True
        Title.Alignment = taCenter
      end
      item
        Color = clBtnFace
        EditButtons = <>
        FieldName = 'ADDRESSID'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        EditButtons = <>
        FieldName = 'NODCARD'
        Footers = <>
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1044#1080#1089#1082#1086#1085#1090#1085#1072#1103' '#1082#1072#1088#1090#1072
        Width = 118
      end
      item
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Width = 177
      end>
  end
  object acList: TActionList
    Left = 280
    Top = 184
    object acClose: TAction
      Caption = 'acClose'
      ShortCut = 27
      OnExecute = acCloseExecute
    end
  end
end
