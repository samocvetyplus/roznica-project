object fmAnlzClient_SellDep: TfmAnlzClient_SellDep
  Left = 218
  Top = 129
  Width = 913
  Height = 549
  Caption = #1040#1085#1072#1083#1080#1079' '#1087#1086' '#1087#1088#1086#1076#1072#1078#1072#1084' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 42
    Width = 905
    Height = 478
    Align = alClient
    BorderStyle = cxcbsNone
    TabOrder = 0
    object cxGridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsAnlzClient
      DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoFocusTopRowAfterSorting]
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skCount
          Position = spFooter
        end
        item
          Kind = skSum
          Position = spFooter
        end
        item
          Kind = skSum
          Position = spFooter
        end
        item
          Kind = skSum
          Position = spFooter
        end
        item
          Kind = skSum
          Position = spFooter
        end
        item
          Kind = skSum
          Position = spFooter
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
          Position = spFooter
          Column = cxGridDBTableView1Column1
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
          Column = cxGridDBTableView1Column1
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
        end
        item
          Kind = skSum
          FieldName = 'SPRICE0'
        end
        item
          Kind = skSum
          FieldName = 'PRICE'
        end
        item
          Kind = skSum
          FieldName = 'COST1'
        end
        item
          Kind = skSum
          FieldName = 'COST'
        end
        item
          Kind = skSum
          FieldName = 'W'
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
          Column = cxGridDBTableView1Column1
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsCustomize.DataRowSizing = True
      OptionsCustomize.GroupRowSizing = True
      OptionsView.Footer = True
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      object cxGridDBTableView1Column1: TcxGridDBColumn
        Caption = #8470' '#1082#1072#1088#1090#1099
        DataBinding.FieldName = 'CARD'
        Options.ShowEditButtons = isebAlways
        Options.CellMerging = True
        Width = 95
      end
      object cxGridDBTableView1Column2: TcxGridDBColumn
        Caption = #1057#1082#1083#1072#1076
        DataBinding.FieldName = 'DEP'
        Width = 117
      end
      object cxGridDBTableView1Column3: TcxGridDBColumn
        Caption = #1054#1073#1097'. '#1089#1090'. '#1087#1086#1082#1091#1087#1086#1082' ('#1088#1091#1073'.)'
        DataBinding.FieldName = 'SUM_CLIENT'
        Width = 138
      end
      object cxGridDBTableView1Column4: TcxGridDBColumn
        Caption = #1057#1082#1080#1076#1082#1072' (%)'
        DataBinding.FieldName = 'DISCOUNT'
        Width = 80
      end
      object cxGridDBTableView1Column5: TcxGridDBColumn
        Caption = #1060#1048#1054' '#1082#1083#1080#1077#1085#1090#1072
        DataBinding.FieldName = 'CLIENT_NAME'
      end
      object cxGridDBTableView1Column6: TcxGridDBColumn
        Caption = #1043#1086#1088#1086#1076
        DataBinding.FieldName = 'CITY'
      end
      object cxGridDBTableView1Column7: TcxGridDBColumn
        Caption = #1040#1076#1088#1077#1089
        DataBinding.FieldName = 'ADDRESS'
      end
      object cxGridDBTableView1Column8: TcxGridDBColumn
        Caption = #1044#1086#1084' / '#1050#1074'.'
        DataBinding.FieldName = 'NO_HOME'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 905
    Height = 42
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 55
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 344
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 289
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object quAnlzClient: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT clientid, card, depid, dep, max_balance, sum_client, disc' +
        'ount, client_name, address, no_home, city'
      'FROM COUNT_CLIENT_DISCOUNT'
      ''
      ''
      ''
      ''
      ''
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 288
    oAutoFormatFields = False
    object quAnlzClientCLIENTID: TFIBIntegerField
      FieldName = 'CLIENTID'
    end
    object quAnlzClientCARD: TFIBStringField
      FieldName = 'CARD'
      Size = 32
      EmptyStrToNull = True
    end
    object quAnlzClientDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quAnlzClientDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object quAnlzClientMAX_BALANCE: TFIBFloatField
      FieldName = 'MAX_BALANCE'
    end
    object quAnlzClientSUM_CLIENT: TFIBFloatField
      FieldName = 'SUM_CLIENT'
    end
    object quAnlzClientDISCOUNT: TFIBIntegerField
      FieldName = 'DISCOUNT'
    end
    object quAnlzClientCLIENT_NAME: TFIBStringField
      FieldName = 'CLIENT_NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object quAnlzClientADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 256
      EmptyStrToNull = True
    end
    object quAnlzClientNO_HOME: TFIBStringField
      FieldName = 'NO_HOME'
      Size = 32
      EmptyStrToNull = True
    end
    object quAnlzClientCITY: TFIBStringField
      FieldName = 'CITY'
      EmptyStrToNull = True
    end
  end
  object dsAnlzClient: TDataSource
    DataSet = quAnlzClient
    Left = 16
    Top = 320
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    ExplorerStubLink = dxComponentPrinter1Link1
    Version = 0
    Left = 72
    Top = 304
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40345.621762581020000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      OptionsPreview.MaxLineCount = 3
      OptionsPreview.Visible = False
      OptionsSize.AutoWidth = True
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 128
    Top = 8
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Left = 80
    Top = 8
    PixelsPerInch = 96
    object cxStyle2: TcxStyle
    end
  end
end
