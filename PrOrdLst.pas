unit PrOrdLst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, db, Menus, pFIBQuery, RxMenus, M207DBCtrls,
  DBGridEh, PrnDbgeh, ActnList, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmPrOrdList = class(TForm)
    SpeedbarSection1: TSpeedbarSection;
    tb1: TSpeedBar;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    laDep: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    pm2: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    Panel1: TPanel;
    Splitter1: TSplitter;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    Panel2: TPanel;
    dg1: TM207IBGrid;
    tb3: TSpeedBar;
    SpeedbarSection3: TSpeedbarSection;
    siClearAll: TSpeedItem;
    siCreate: TSpeedItem;
    pm1: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    siCopy: TSpeedItem;
    miCopy: TMenuItem;
    siMargin: TSpeedItem;
    N12: TMenuItem;
    N14: TMenuItem;
    siApplyMargin2Art2: TSpeedItem;
    N15: TMenuItem;
    pmClear: TPopupMenu;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    pmMargin: TPopupMenu;
    miMargin2Art: TMenuItem;
    miMargin2Dep: TMenuItem;
    miMargin2Gr: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N13: TMenuItem;
    N25: TMenuItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    spitPrint: TSpeedItem;
    pmPrint: TRxPopupMenu;
    mnitOrder: TMenuItem;
    NTagPrint: TMenuItem;
    siOpen: TSpeedItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    tb4: TSpeedBar;
    Label1: TLabel;
    edArt: TEdit;
    cbCh: TCheckBox;
    mnitActOV: TMenuItem;
    cbAct: TCheckBox;
    siAct: TSpeedItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    cbDif: TCheckBox;
    cbAll: TCheckBox;
    siFind: TSpeedItem;
    N29: TMenuItem;
    lbCountry: TListBox;
    Splitter6: TSplitter;
    SpeedbarSection4: TSpeedbarSection;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    cbNonReVal: TCheckBox;
    dg2: TDBGridEh;
    pdg2: TPrintDBGridEh;
    acList: TActionList;
    acPribtGrid: TAction;
    siHelp: TSpeedItem;
    acDel: TAction;
    acView: TAction;
    acAddUiAct: TAction;
    NAddUidAct: TMenuItem;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    VisiblePrord_FullArt: TCheckBox;
    Label7: TLabel;
    CheckClose: TAction;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure siClearAllClick(Sender: TObject);
    procedure siClearDepClick(Sender: TObject);
    procedure siClearArtClick(Sender: TObject);
    procedure siClearClick(Sender: TObject);
    procedure siCreateClick(Sender: TObject);
    procedure miCopyClick(Sender: TObject);
    procedure siMarginClick(Sender: TObject);
    procedure siApplyMargin2Art2Click(Sender: TObject);
    procedure miMargin2DepClick(Sender: TObject);
    procedure miMargin2GrClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mnitOrderClick(Sender: TObject);
    procedure siOpenClick(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbChClick(Sender: TObject);
    procedure mnitActOVClick(Sender: TObject);
    procedure cbActClick(Sender: TObject);
    procedure siActClick(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure siFindClick(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure cbNonReValClick(Sender: TObject);
    procedure dg2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPribtGridExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acAddUiActUpdate(Sender: TObject);
    procedure acAddUiActExecute(Sender: TObject);
    procedure VisiblePrord_FullArtClick(Sender: TObject);
    procedure CheckCloseExecute(Sender: TObject);
  private
    { Private declarations }
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ExecQ(q: TpFIBQuery; Reopen: boolean);
    procedure ShowPeriod;
    procedure PrintTag_(Sender: TObject);    
  public
    { Public declarations }
  end;

var
  fmPrOrdList: TfmPrOrdList;

implementation

uses comdata, Data, SInv, PrOrd, Data2, DBTree, PriceMarg, AddToPr,RxStrUtils,
     ReportData, Period, PAct, PrHist, A2PrHist, PrFind, InsRepl, M207Proc, Data3,
     DbUtil, MsgDialog, AddUidAct, FIBQuery, ClipBrd;

{$R *.DFM}

procedure TfmPrOrdList.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmPrOrdList.PrintTag_(Sender: TObject);
var arr : TarrDoc;
begin
 arr := gen_arr(dg2, dm.taPrOrdPRORDID);
 try
   PrintTag(arr, act_tag, TMenuItem(Sender).Tag);
 finally
   Finalize(arr);
 end;
end;

procedure TfmPrOrdList.FormCreate(Sender: TObject);
var c: TColumn;
    i: integer;
    //  enClose: boolean;
begin
 Clipboard.AsText := dm.quPrice.SelectSQL.Text;

  with dm do
    begin
      dsPrOrd.DataSet:=taPrOrd;
      WorkMode:='PRICE';
      taPrOrd.SelectSQL[5]:=' ';
      taPrOrd.SelectSQL[6]:=' ';
      SetBeginDate;
      taPrOrd.SelectSQL[5]:='where iType <> 14';
      if CenterDep then taPrOrd.SelectSQL[6]:='Order by PRORD desc'
         else taPrOrd.SelectSQL[6]:='Order by SetDate desc';

    end;
  ShowPeriod;
  with dg1, dm.quPrice do
    for i:=0 to Fields.Count-1 do
      if Fields[i].FieldName[1] in ['P', 'O', 'S'] then
        begin
          c:=Columns.Add;
          c.Field:=Fields[i];
          c.Title.Caption:=Fields[i].DisplayLabel;
          c.Expanded:=false;
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=50;
          case Fields[i].FieldName[1] of
              'S':  begin
                      c.ReadOnly:=True;
                      c.Color:=clAqua;
                      if not CenterDep  then c.Visible := false;
                    end;
              'O': begin
                     c.Color:=clLime;
                     if not CenterDep  then c.Visible := false;
                   end;  
             end;
        end;
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  tb3.WallPaper:=wp;
  tb4.WallPaper:=wp;

 Clipboard.AsText := dm.quPrice.SelectSQL.Text;

  with dmCom, dm, dm2 do
    begin
      Old_D_MatId:='.';
      if not tr.Active then tr.StartTransaction;
      taComp.SelectSQL[taComp.SelectSQL.Count-2]:='WHERE PRODUCER=1';
      FillListBoxes(lbComp, lbMat, lbGood, lbIns,lbCountry,nil,nil);
      dmCom.D_Att1Id := ATT1_DICT_ROOT;
      dmCom.D_Att2Id := ATT2_DICT_ROOT;
    end;

  VisiblePrord_FullArt.Checked:=false;
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbCompClick(NIL);
  dm.R_Item:='PRORD';
  if (not CenterDep)  then
    cbDif.Visible := False;

//  enclose:=false;
{  with dm.taPrOrd do
  begin
    Active:=True;
    refresh;
    first;
    while not EOF do
    begin
      if dm.taPrOrdSETDATE.AsString='' then begin// ���� ������ ������
         enClose:=true; next;
      end; 
    end;   
  end;      }

{  if enClose then
     SpeedItem2.Enabled:=True else
     SpeedItem2.Enabled:=False;  }

  dmcom.FillTagMenu(NTagPrint, PrintTag_, 0);
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

 Clipboard.AsText := dm.quPrice.SelectSQL.Text;

end;

procedure TfmPrOrdList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSets([quPrice]);
      CloseDataSets([taPrOrd, taRec, quPrice]);
      taPrOrd.SelectSQL[5]:=' ';

      tr.CommitRetaining;
      WorkMode:='';
      R_Item:='';
    end;
end;

procedure TfmPrOrdList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPrOrdList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmPrOrdList.siAddClick(Sender: TObject);
var LogOperationID:string;
begin
  if dm.SDepId<>-1 then
    begin
      LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
      dm.taPrOrd.Append;
      ShowAndFreeForm(TfmPrOrd, Self, TForm(fmPrOrd), True, False);
      dm3.update_operation(LogOperationID);
    end
  else MessageDialog('���������� ������� �����', mtInformation, [mbOK], 0);
end;

procedure TfmPrOrdList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
  dm.ClickUserDepMenu(dm.pmPrOrd);
end;

procedure TfmPrOrdList.N5Click(Sender: TObject);
var p: TPoint;
begin
  p:=tb1.ClientToScreen(Point(tb2.Left, tb2.Top+tb2.Height));
  siDep.DropDownMenu.PopUp(p.x, p.y);
end;

procedure TfmPrOrdList.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
var fn: string[32];
    b: boolean;
    e: extended;
    i: integer;
begin

  if (Field<>NIL) then
    begin
      if ((Field.FieldName='FULLART')
                        or (Field.FieldName='ART2')
                        or (Field.FieldName='ART')) then
      with dm, quPrice do
        begin
          i:=0;
          Background:=dmCom.clMoneyGreen;
          while (Fields[i].FieldName[1]<>'P') and (i<FieldCount) do Inc(i);
          if (i<FieldCount){ and (Fields[i].FieldName='P') } then
            begin
              e:=Fields[i].AsFloat;
              while (i<FieldCount) do
                begin
                  if Fields[i].FieldName[1]='P' then
                   if Abs(e-Fields[i].AsFloat)>1e-2 then
                    begin
                      Background:=clRed;
                      break;
                    end;
                  Inc(i);
                end
            end
        end
      else if Field.FieldName[1] in ['P', 'O', 'S'] then
           begin
             fn:=Field.FieldName;
             case Field.FieldName[1] of
                'P': fn[1]:='T';
                'O': fn:='X';
                'S': fn:='Y';
               end;
             b:=dm.quPrice.FieldByName(fn).AsInteger=0;
             if  Highlight then
               begin
                 if b then Background:=clGreen
                 else Background:=clNavy;
               end
             else
               begin
                 if b then
                   begin
                      case Field.FieldName[1] of
                          'P':
                              begin
                                fn[1]:='R';
                                if dm.quPrice.FieldByName(fn).AsInteger=1 then Background:=clRed
                                else Background:=dmCom.clCream;
                              end;
                          'O':
                              begin
                                fn:='V';
                                if dm.quPrice.FieldByName(fn).AsInteger=1 then Background:=clRed
                                else Background:=dmCom.clCream;
                              end ;
                          'S':
                              begin
                                Background:=clRed
                              end;
                         end
                   end
                 else
                   case Field.FieldName[1] of
                       'P': Background:=clInfoBk;
                       'O': Background:=clYellow;
                       'S': Background:=clAqua;
                     end;
               end
           end;
    end

end;

procedure TfmPrOrdList.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
end;

procedure TfmPrOrdList.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmPrOrdList.siClearAllClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������������ ���� ��������� ������',LogOprIdForm);
  with dmCom, dm2, quClearTmpP2All do
    begin
      PostDataSet(dm.quPrice);
      Params.ByName['USERID'].AsInteger:=UserId;
      SetArtFilter(Params);
      ExecQ(quClearTmpP2All, True);
    end;
   dm3.update_operation(LogOperationID) 
end;

procedure TfmPrOrdList.ExecQ(q: TpFIBQuery; Reopen: boolean);
begin
  with dm do
    try
      Screen.Cursor:=crSQLWait;
      q.ExecQuery;
      q.Transaction.CommitRetaining;
      with quPrice do
        if Reopen then
          begin
            Active:=False;
            Open;
          end
        else Refresh;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmPrOrdList.siClearDepClick(Sender: TObject);
var q: TpFIBQuery;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������������ ���� ���������� ������',LogOprIdForm);
  PostDataSet(dm.quPrice);
  q:=NIL;
  case dg1.SelectedField.FieldName[1] of
      'P': begin
             q:=dm2.quClearTmpP2Dep;
             q.Params.ByName['DEPID'].AsInteger:=dg1.SelectedField.Tag;
           end;
      'O': begin
             q:=dm2.quClearTmpP2Opt;
           end;
     end;
  if q<>NIL then
    with q do
      begin
        Params.ByName['USERID'].AsInteger:=dmCom.UserId;
        dmCom.SetArtFilter(Params);
        ExecQ(q, True);
      end;
  dm3.update_operation(LogOperationID)    
end;

procedure TfmPrOrdList.siClearArtClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������������ ���� ���������� ��������',LogOprIdForm);
  PostDataSet(dm.quPrice);
  dm.quTmp.SQL.Text:='UPDATE PRICE SET TPRICE=PRICE2, PDate=NULL WHERE ART2ID='+dm.quPriceArt2Id.AsString;
  ExecQ(dm.quTmp, false);  
  dm.quTmp.SQL.Text:='UPDATE ART2 SET TSPRICE=PRICE, PDate=NULL, OPTPRICE=TOPTPRICE WHERE ART2ID='+dm.quPriceArt2Id.AsString;
  ExecQ(dm.quTmp, false);
  dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrdList.siClearClick(Sender: TObject);
var fn: string[32];
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������������ ����',LogOprIdForm);
  PostDataSet(dm.quPrice);
  with dm, dg1 do
    if NOT quPriceArt2Id.IsNull and (SelectedField.FieldName[1] in ['P', 'O', 'S']) then
      begin
        fn:=SelectedField.FieldName;

        case SelectedField.FieldName[1] of
            'P': fn[1]:='T';
            'O': fn:='X';
            'S': fn:='Y'
           end;

        if quPrice.FieldByName(fn).AsInteger=0 then
          begin
            case SelectedField.FieldName[1] of
                'P': dm.quTmp.SQL.Text:='UPDATE PRICE SET TPRICE=PRICE2, PDATE=NULL WHERE ART2ID='+
                               dm.quPriceArt2Id.AsString+
                               ' AND DEPID='+IntToStr(SelectedField.Tag);
                'O': dm.quTmp.SQL.Text:='UPDATE ART2 SET TOPTPRICE=OPTPRICE, PDATE=NULL WHERE ART2ID='+
                               dm.quPriceArt2Id.AsString;
                'S': dm.quTmp.SQL.Text:='UPDATE ART2 SET TSPRICE=PRICE, PDATE=NULL WHERE ART2ID='+
                               dm.quPriceArt2Id.AsString;

               end;

            ExecQ(dm.quTmp, False);
          end
      end;
  dm3.update_operation(LogOperationID)    
end;

procedure TfmPrOrdList.siCreateClick(Sender: TObject);
var LogOperationID:string;
begin
  if MessageDialog('������������ ������� �� ����?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
     begin
       LogOperationID:=dm3.insert_operation('����������� ������ �� ����',LogOprIdForm);
       PostDataSet(dm.quPrice);
       dm.quTmp.SQL.Text:='EXECUTE PROCEDURE CREATEPRORD '+IntToStr(dmCom.UserId);
       ExecQ(dm.quTmp, True);
       dm.quTmp.Transaction.CommitRetaining;
       ReOpenDataSet(dm.taPrOrd);
       dm3.update_operation(LogOperationID)  
     end;
end;

procedure TfmPrOrdList.miCopyClick(Sender: TObject);
var e: extended;
    i: integer;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('����������� ����',LogOprIdForm);
  with dm, quPrice do
    if NOT quPriceArt2Id.IsNull and (dg1.SelectedField.FieldName[1]='P') then
      begin
        PostDataSet(dm.quPrice);
        e:=dg1.SelectedField.AsFloat;
        if State<>dsEdit then Edit;
        for i:=0 to Fields.Count-1 do
          if Fields[i].FieldName[1]='P' then Fields[i].AsFloat:=e;
        Post;
      end;
  dm3.update_operation(LogOperationID)
end;

procedure TfmPrOrdList.siMarginClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�������',LogOprIdForm);
  PostDataSet(dm.quPrice);
  ShowAndFreeForm(TfmPrMargin, Self, TForm(fmPrMargin), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrdList.siApplyMargin2Art2Click(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('��������� ������� ��� ��������',LogOprIdForm);
  with dm, quTmp do
    if NOT quPriceArt2Id.IsNull then
      begin
        PostDataSet(quPrice);
        SQL.Text:='execute procedure ApplyMargin2Art2 '+quPriceArt2Id.AsString+', '+IntToStr(dmCom.UserId);
        ExecQuery;
        quPrice.Refresh;
      end;
   dm3.update_operation(LogOperationID)
end;

procedure TfmPrOrdList.miMargin2DepClick(Sender: TObject);
var q: TpFIBQuery;
    LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('��������� ������� ��� ������',LogOprIdForm);
  PostDataSet(dm.quPrice);
  with dm, dm2 do
    begin
      if dg1.SelectedField.FieldName[1]='P' then
        begin
          q:=quMargin2Dep;
          q.Params.ByName['DEPID'].AsInteger:=dg1.SelectedField.Tag;
        end
      else if dg1.SelectedField.FieldName[1]='O' then q:=quMargin2Opt
           else q:=NIL;
      if q<>NIL then
        try
          Screen.Cursor:=crSQLWait;
          q.Params.ByName['USERID'].AsInteger:=dmCom.UserId;
          dmCom.SetArtFilter(q.Params);
          q.ExecQuery;

          with quPrice do
            begin
              Active:=False;
              Open;
            end;
        finally
          Screen.Cursor:=crDefault;
        end;
    end;
  dm3.update_operation(LogOperationID)
end;

procedure TfmPrOrdList.miMargin2GrClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('��������� ������� ��� ������',LogOprIdForm);
  with dm, dm2, quMargin2Gr do
    try
      Screen.Cursor:=crSQLWait;
      PostDataSet(dm.quPrice);
      Params.ByName['USERID'].AsInteger:=dmCom.UserId;
      dmCom.SetArtFilter(Params);
      ExecQuery;
      with quPrice do
        begin
          Active:=False;
          dmCom.tr.CommitRetaining;
          Open;
        end;
    finally
      Screen.Cursor:=crDefault;
    end;
  dm3.update_operation(LogOperationID)  
end;

procedure TfmPrOrdList.SpeedItem1Click(Sender: TObject);
var iDepid:integer;
    LogOperationID:string;
begin
  try
    LogOperationID:=dm3.insert_operation('����',LogOprIdForm);
    fmAddToPr:=TfmAddToPr.Create(NIL);
    if fmAddToPr.ShowModal=mrOK then
      with dm, dmCom, dm2 do
        begin
          Screen.Cursor:=crSQLWait;
          iDepid:=SDepid;
          with quSetTmp2Price do
            begin
              Params.ByName['USERID'].AsInteger:=UserId;
              if SDepid>0 then
                begin
                  ParamByName('DEPID1').AsInteger:=SDepid;
                  ParamByName('DEPID2').AsInteger:=SDepid;
                end
               else
                begin
                  ParamByName('DEPID1').AsInteger:=-MAXINT;
                  ParamByName('DEPID2').AsInteger:=MAXINT;
                end;
              SetArtFilter(Params);
              with fmAddToPr do
                begin
                  if cb2.Checked then ParamByName('PRICE').AsFloat:=fmAddToPr.seP2.Value
                  else ParamByName('PRICE').IsNull:=True;

                  if cb3.Checked then ParamByName('OPTPRICE').AsFloat:=fmAddToPr.seP3.Value
                  else  ParamByName('OPTPRICE').IsNull:=True;
                end;

              ExecQuery;
            end;
          SDepid:=iDepid;
          with quPrice do
            begin
              Active:=False;
              Open;
            end;
      end;
  finally
    fmAddToPr.Free;
    Screen.Cursor:=crDefault;
    dm3.update_operation(LogOperationID);
  end;

end;



procedure TfmPrOrdList.SpeedItem3Click(Sender: TObject);
var i: integer;
    LogOperationID:string;
begin
  i := 0;
  with dm, taPrOrd do
    begin
      PostDataSets([taPrOrd, taPrOrdItem]);
      if MessageDialog('���������� ����?', mtConfirmation, [mbOK, mbCancel], 0)=mrOK then
        try
          LogOperationID:=dm3.insert_operation('���������� ����',LogOprIdForm);
          DisableControls;
          i:=taPrOrdPrOrdId.AsInteger;
          First;
          while NOT EOF do
            begin

              if (taPrOrdIsSet.AsInteger<>1) and (taPrOrdIsClosed.AsInteger=1) and (taPrOrdDepId.AsInteger>0) and not dmCOm.Dep[taPrOrdDepId.AsInteger].NotSetPr then
                begin
                  with quTmp do
                    begin
                      SQL.Text:='execute procedure CLOSEPRORD '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId);
                      ExecQuery;
                    end;
                  taPrOrd.Refresh;
                end;
              Next;
            end;
          with quPrice do
            begin
              Active:=False;
              Open;
            end
        finally
          Locate('PRORDID', i, []);
          EnableControls;
          dm3.update_operation(LogOperationID);
        end;
    end;
  dmCom.tr.CommitRetaining;
end;

procedure TfmPrOrdList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;
end;

procedure TfmPrOrdList.mnitOrderClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr :=  gen_arr(dg2, dm.taPrOrdPRORDID);
  try
    PrintDocument(arr, order);
  finally
    Finalize(arr);
  end;
end;

procedure TfmPrOrdList.siOpenClick(Sender: TObject);
begin
dmcom.db.Connected:=true;
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId)or (Old_D_CountryID<>D_CountryID) then
       begin
         Old_D_CompId := D_CompId;
         Old_D_MatId := D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryId:=D_CountryId;
         Screen.Cursor:=crSQLWait;
//         ShowMessage(quPrice.SelectSQL.Text);
         ReopenDataSets([quPrice]);
//         ShowMessage(quPrice.SelectSQL.Text);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmPrOrdList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.calc_BeginDate, dm.EndDate) then
    begin
      Screen.Cursor:=crSQLWait;
      ReOpenDataSets([dm.taPrOrd]);
      Screen.Cursor:=crDefault;
      ShowPeriod;
    end;
end;

procedure TfmPrOrdList.ShowPeriod;
begin
   laPeriod.Caption:='� '+DateToStr(dm.calc_beginDate) + ' �� ' +DateToStr(dm.EndDate);
end;


procedure TfmPrOrdList.edArtChange(Sender: TObject);
begin
  with dm.quPrice do
    begin
      if not Active then Active:=True;
        Locate('ART', trim(edArt.Text), [loCaseInsensitive, loPartialKey]);

    end;  
end;

procedure TfmPrOrdList.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then
  begin
    ACtiveControl:=dg1;
  end;

//  if Key = VK_RETURN then
//  begin
//    with dm.quPrice do
//    begin
//      Filtered := false;
//      Filter := 'art like ' + QuotedStr(trim(edArt.text) + '%');
//      FilterOptions := FilterOptions + [foCaseInsensitive] - [foNoPartialCompare];
//      Filtered := true;
//    end;
//  end;

end;

procedure TfmPrOrdList.cbChClick(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dm do
    begin
       PriceChangedFilter:=cbCh.Checked;
       PriceDifFilter:=cbDif.Checked;
       quPrice.Filtered:=cbCh.Checked or cbDif.Checked;
       if not quPrice.Active then quPrice.Active:=true;
    end;
  Screen.Cursor:=crDefault;
end;

procedure TfmPrOrdList.mnitActOVClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr := gen_arr(dg2, dm.taPrOrdPRORDID);
  try
    dmReport.ActType := 0;
    PrintDocument(arr, act_uid); // act_uid
  finally
    Finalize(arr);
  end;
end;

procedure TfmPrOrdList.cbActClick(Sender: TObject);
var
  tmp : string;
begin
  with dm, taPrOrd do
    try
      Screen.Cursor:=crSQLWait;
      Active:=False;
      tmp := trim(SelectSQL[5]);
      if cbAct.Checked then
      begin
        SelectSQL[5]:=tmp + ' and Q>0';
        if trim(SelectSQL[6]) <> '' then
          SelectSQL[6] := ReplaceStr(SelectSQL[6], 'Where', 'And');
      end
      else
        SelectSQL[5]:=ReplaceStr(SelectSQL[5], 'and Q>0', '');
      Active:=True;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmPrOrdList.siActClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmPAct, Self, TForm(fmPAct), True, False);
end;

procedure TfmPrOrdList.N28Click(Sender: TObject);
begin
  with dm do
    begin
      PrHistArt2Id:=quPriceArt2Id.AsInteger;
      if dg1.SelectedField.FieldName[1]='P' then
        begin
          PrHistDepId:=dg1.SelectedField.Tag;
          ShowAndFreeForm(TfmPrHist, Self, TForm(fmPrHist), True, False);
        end
      else
      if dg1.SelectedField.FieldName[1] in ['O', 'S'] then
        ShowAndFreeForm(TfmA2PrHist, Self, TForm(fmA2PrHist), True, False);
    end;
end;

procedure TfmPrOrdList.cbAllClick(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dm do
    begin
      quPrice.Active:=False;
      if cbAll.Checked then quPrice.SelectSQL.Text:=PSQL1
      else quPrice.SelectSQL.Text:=PSQL2;
      quPrice.Active:=True;
    end;
  Screen.Cursor:=crDefault;
end;

procedure TfmPrOrdList.siFindClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmPrFind, Self, TForm(fmPrFind), True, False);
end;

procedure TfmPrOrdList.N34Click(Sender: TObject);
begin
  if dg1.SelectedField.FieldName[1] in ['P','O'] then
    with dmCom, dm, quTemp do
      begin
        SelectSQL.Text:='SELECT PDATE FROM ';
        case dg1.SelectedField.FieldName[1] of
           'P': SelectSQL.Text:=SelectSQL.Text+'PRICE WHERE ART2ID='+
                                quPriceArt2Id.AsString+' AND DEPID='+
                                IntToStr(dg1.SelectedField.Tag);
           'O': SelectSQL.Text:=SelectSQL.Text+'ART2 WHERE ART2ID='+quPriceArt2Id.AsString;
          end;
        Active:=True;
        MessageDialog(Fields[0].DisplayText, mtInformation, [mbOk], 0);
        Active:=False;
      end
end;

procedure TfmPrOrdList.N37Click(Sender: TObject);
var LogOperationID:string;
begin
  with dmCom, dm do
    begin
      case TMenuItem(Sender).Tag of
          1: begin
              LogOperationID:=dm3.insert_operation('����� ��������� �������',LogOprIdForm);
              quTmp.SQL.Text:='UPDATE PRORD SET RSTATE_P=0 WHERE PRORDID='+taPrOrdPrOrdId.AsString;
             end;
          2: begin
              LogOperationID:=dm3.insert_operation('����� ��������� ����',LogOprIdForm);
              quTmp.SQL.Text:='UPDATE PRORD SET RSTATE_A=0 WHERE PRORDID='+taPrOrdPrOrdId.AsString;
             end
        end;
      quTmp.ExecQuery;
      tr.CommitRetaining;
      taPrOrd.Refresh;
    end;
  dm3.update_operation(LogOperationID)  
end;

procedure TfmPrOrdList.cbNonReValClick(Sender: TObject);
var tmp: string;
begin
  with dm, taPrOrd do
    try
      Screen.Cursor:=crSQLWait;
      Active:=False;
      if cbNonReVal.Checked then
      begin
        tmp := trim(SelectSQL[5]);
        if tmp <> '' then
          SelectSQL[5]:=tmp+' and IsReVal = 1'
        else
          SelectSQL[5]:='Where IsReVal = 1';
        end
      else
        SelectSQL[5]:=ReplaceStr(SelectSQL[5], 'and IsReVal=1', '');
      Active:=True;
    finally
      Screen.Cursor:=crDefault;
    end;

end;

procedure TfmPrOrdList.CheckCloseExecute(Sender: TObject);
var i, j, fl1: integer;
    prord, LogOperationID:string;
    prordl: TStringList;
    Condition: Boolean;
    IsSet: Boolean;
    DepValid: Boolean;
    DontSetPrord: Boolean;
begin

  i := 0;

  prordl := TStringList.Create;

  if MessageDialog('������� ��� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
    with dm, taPrOrd do
      begin
        try

          Screen.Cursor:=crSQLWait;

          LogOperationID:=dm3.insert_operation('������� ��� �������',LogOprIdForm);

          DisableControls;

          i := taPrOrdPrOrdId.AsInteger;

          Last;

          fl1:=0;

          prord := '';

          while not BOF do
          begin

            IsSet := taPrordISSET.AsInteger = 1;

            DepValid := taPrOrdDepId.AsInteger > 0;

            DontSetPrord  := dmCOm.Dep[taPrOrdDepId.AsInteger].NotSetPr;

            Condition := (not IsSet) and (DepValid) and (not DontSetPrord);

            if (Condition)  then
            begin

              with quTmp do
              begin

                Close;

                SQL.Text:='select WARN from CL_FROM_PRORD('+taPrOrdPRORDID.AsString+')';

                ExecQuery;

                prord := trim(Fields[0].AsString);

              end;

              if (prord = '') and (fl1 = 0) then
              begin

                Condition := (taPrOrdITYPE.AsInteger = 12) and (taPrOrdONLYACT.AsInteger = 1);

                with quTmp do
                begin

                  Close;

                  if Condition then
                  begin
                    SQL.Text := 'execute procedure CLOSEPRORD1 '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId) + ', ' + taPrOrdUID.AsString;
                  end else
                  begin
                    SQL.Text:='execute procedure CLOSEPRORD '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId);
                  end;

                  ExecQuery;

                  Transaction.CommitRetaining;

                end;

                RefreshDataSet(taPrord);

              end else
              if (prord <> '') then
              begin

                Condition := (taPrordONLYACT.AsInteger = 0) or ((taPrordONLYACT.AsInteger = 1) and
                             ((taPrordITYPE.AsInteger = 11) or (taPrordITYPE.AsInteger = 14)));

                if (prordl.IndexOF(prord) = -1) and Condition then
                begin
                  prordl.Add(prord);
                end;

                Application.MessageBox(pchar(prord),'��������!!!', 0);

                SysUtils.Abort;

                fl1 := 1;

              end;

            end;

          Prior;

        end;

        if (not CenterDep) then ReopenDataSet(taPrOrd);

        if prordl.Count <> 0 then
        begin

          prordl.Sort;

          fl1 := 1;

        end;

        quPrice.CloseOpen(false);

      finally

        Locate('PRORDID', i, []);

        EnableControls;

        dm3.update_operation(LogOperationID);

        Screen.Cursor:=crDefault;

      end;

      PostDataSets([taPrOrd, taPrOrdItem]);

//        try
//          Screen.Cursor := crSQLWait;
//          DisableControls;
//
//          i:=taPrOrdPrOrdId.AsInteger;
//          Last;
//          fl1:=0;
//
//          prord := '';
//
//          while NOT BOF do
//            begin
//              if (taPrOrdIsSet.AsInteger <> 1) and (taPrOrdIsClosed.AsInteger = 1) and
//                 (taPrOrdDepId.AsInteger > 0) and not dmCOm.Dep[taPrOrdDepId.AsInteger].NotSetPr then
//                begin
//                  If ((taPrordOnlyAct.AsInteger = 0) or ((taPrordOnlyAct.AsInteger = 1) and
//                     ((taPrordItype.AsInteger = 11) or (taPrordItype.AsInteger = 14)))) then
//                    with quTmp do
//                      begin
//                        close;
//                        SQL.Text:='select WARN from CL_FROM_PRORD('+dm.taPrOrdPRORDID.AsString+')';
//                        ExecQuery;
//                        prord := trim(Fields[0].AsString);
//
//                        if (prord <> '') and (prordl.IndexOf(prord) = - 1) then
//                          prordl.Add(prord);
//                      end;
//
//                  if (prord <> '') then fl1 := 1;
//
//                  if (prord = '') and (fl1=0) then
//                    with quTmp do
//                      begin
//                        close;
//                        if (taPrOrdITYPE.AsInteger = 12) and (taPrOrdONLYACT.AsInteger = 1) then
//                          begin
//                            SQL.Text := 'execute procedure CLOSEPRORD1 '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId) + ', ' +
//                                       taPrOrdUID.AsString;
//                          end
//                        else
//                          SQL.Text:='execute procedure CLOSEPRORD '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId);
//                        ExecQuery;
//                        Transaction.CommitRetaining;
//                      end;
//                    RefreshDataSet(taPrOrd);
//                  end
//                  else
//                    if prord <> '' then
//                      Application.MessageBox(pchar(prord),'��������!!!', 0);
//              Prior;
//            end;
//          //*****
//          if prordl.Count <> 0 then
//            begin
//              prordl.Sort;
//              //   ShowMessage(prordl.Text);
//              fl1:=1;
//            end;
//          //*****
//          if (not CenterDep) then
//            ReopenDataSet(taPrOrd);
//          with quPrice do
//            begin
//              Active:=False;
//              Open;
//            end
//        finally
//          Locate('PRORDID', i, []);
//          EnableControls;
//          dm3.update_operation(LogOperationID);
//          Screen.Cursor:=crDefault;
//        end;
    end;

  dmCom.tr.CommitRetaining;

  prordl.Free;

end;

procedure TfmPrOrdList.dg2GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  with dm do
    begin  // ����������� �������� ����� �����
      if taPrOrdIsSet.AsInteger=1 then Background:=clAqua  //�������
      else
       if dm.taPrOrdIsClosed.AsInteger=0 then Background:=clInfoBk  //������
       else Background:=clBtnFace;  //�����

      if Column.Field.FieldName='Q' then
        if Column.Field.AsInteger>0 then
          if taPrOrdPActClosed.AsInteger=0 then Background := clRed  //�������
          else Background:=dmCom.clCream;
      if (dm.taPrOrdISREVAL.AsInteger = 1) then Background := clBlue; //�����
    end;

end;

procedure TfmPrOrdList.dg2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_SPACE, VK_RETURN:
       if not dm.taPrOrdPRORDID.IsNull then acViewExecute(Sender);
    end;
end;

procedure TfmPrOrdList.acPribtGridExecute(Sender: TObject);
begin
 pdg2.Print;
end;

procedure TfmPrOrdList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100310)
end;

procedure TfmPrOrdList.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taPrOrdPRORDID.IsNull) and
  (dm.taPrOrdISCLOSED.AsInteger=0);
end;

procedure TfmPrOrdList.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������� ������',LogOprIdForm);
  dm.taPrOrd.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrdList.acViewUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taPrOrdPRORDID.IsNull);
end;

procedure TfmPrOrdList.acViewExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
  ShowAndFreeForm(TfmPrOrd, Self, TForm(fmPrOrd), True, False);
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCompClick(NIL);
  dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrdList.acAddUiActUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (dm.SDepId<>-1) and (dmcom.Adm);
 TAction(Sender).Visible:= dmcom.Adm;
end;

procedure TfmPrOrdList.acAddUiActExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('������� ��� ��� �������',LogOprIdForm);
 ShowAndFreeForm(TfmAddUidAct, Self, TForm(fmAddUidAct), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrdList.VisiblePrord_FullArtClick(Sender: TObject);
var
  tmp : string;
begin
  with dm, taPrOrd do
    try
      Screen.Cursor:=crSQLWait;
      Active:=False;
      tmp := trim(SelectSQL[5]);
      if VisiblePrord_FullArt.Checked then
         SelectSQL[5] := ReplaceStr(SelectSQL[5], 'iType <> 14','iType = 14')
      else
         SelectSQL[5] := ReplaceStr(SelectSQL[5], 'iType = 14','iType <> 14');
      Active:=True;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

end.


