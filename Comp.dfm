object fmComp: TfmComp
  Left = 65
  Top = 134
  HelpContext = 100102
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1080' '#1092#1080#1079#1080#1095#1077#1089#1082#1080#1093' '#1083#1080#1094
  ClientHeight = 652
  ClientWidth = 1175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 587
    Top = 71
    Width = 0
    Height = 562
    ExplicitLeft = 484
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 633
    Width = 1175
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 587
    Top = 71
    Width = 588
    Height = 562
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 588
      Height = 278
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 4
        Top = 48
        Width = 30
        Height = 26
        Caption = #1070#1088#1080#1076'.'#13#10#1072#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 80
        Width = 45
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 4
        Top = 104
        Width = 29
        Height = 13
        Caption = #1060#1072#1082#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 168
        Top = 80
        Width = 29
        Height = 13
        Caption = 'E-Mail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 168
        Top = 104
        Width = 48
        Height = 13
        Caption = #1048#1085#1090#1077#1088#1085#1077#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label22: TLabel
        Left = 4
        Top = 32
        Width = 30
        Height = 13
        Caption = #1043#1086#1088#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 200
        Top = 32
        Width = 38
        Height = 13
        Caption = #1048#1085#1076#1077#1082#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label25: TLabel
        Left = 4
        Top = 128
        Width = 36
        Height = 13
        Caption = #1057#1090#1088#1072#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label35: TLabel
        Left = 4
        Top = 144
        Width = 30
        Height = 26
        Caption = #1055#1086#1095#1090'.'#13#10#1072#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label27: TLabel
        Left = 6
        Top = 176
        Width = 42
        Height = 26
        Caption = #1040#1076#1088#1077#1089#13#10#1076#1083#1103' '#1076#1086#1082'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cbPhis: TDBCheckBox
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
        DataField = 'PHIS'
        DataSource = dmCom.dsComp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object edAddress: TDBEdit
        Left = 44
        Top = 52
        Width = 333
        Height = 21
        Color = clInfoBk
        DataField = 'ADDRESS'
        DataSource = dmCom.dsComp
        TabOrder = 3
      end
      object edPhone: TDBEdit
        Left = 64
        Top = 76
        Width = 97
        Height = 21
        Color = clInfoBk
        DataField = 'PHONE'
        DataSource = dmCom.dsComp
        TabOrder = 4
      end
      object edEMail: TDBEdit
        Left = 220
        Top = 76
        Width = 157
        Height = 21
        Color = clInfoBk
        DataField = 'EMAIL'
        DataSource = dmCom.dsComp
        TabOrder = 6
      end
      object edFax: TDBEdit
        Left = 64
        Top = 100
        Width = 97
        Height = 21
        Color = clInfoBk
        DataField = 'FAX'
        DataSource = dmCom.dsComp
        TabOrder = 5
      end
      object edWWW: TDBEdit
        Left = 220
        Top = 100
        Width = 157
        Height = 21
        Color = clInfoBk
        DataField = 'WWW'
        DataSource = dmCom.dsComp
        TabOrder = 7
      end
      object edCity: TDBEdit
        Left = 44
        Top = 28
        Width = 145
        Height = 21
        Color = clInfoBk
        DataField = 'CITY'
        DataSource = dmCom.dsComp
        TabOrder = 1
      end
      object edPostIndex: TDBEdit
        Left = 240
        Top = 28
        Width = 137
        Height = 21
        Color = clInfoBk
        DataField = 'POSTINDEX'
        DataSource = dmCom.dsComp
        TabOrder = 2
      end
      object edPaddress: TDBEdit
        Left = 46
        Top = 148
        Width = 515
        Height = 21
        Color = clInfoBk
        DataField = 'PADDRESS'
        DataSource = dmCom.dsComp
        TabOrder = 8
      end
      object lcCountry: TDBLookupComboBox
        Left = 46
        Top = 127
        Width = 515
        Height = 21
        Color = clInfoBk
        DataField = 'D_COUNTRYID'
        DataSource = dmCom.dsComp
        KeyField = 'D_COUNTRYID'
        ListField = 'NAME'
        ListSource = dmCom.dsCountry
        TabOrder = 9
      end
      object edADDRESSPOST: TDBEdit
        Left = 46
        Top = 175
        Width = 515
        Height = 21
        Color = clInfoBk
        DataField = 'ADDRESSPOST'
        DataSource = dmCom.dsComp
        TabOrder = 10
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 278
      Width = 588
      Height = 284
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 1
      object TabSheet7: TTabSheet
        Caption = #1054#1073#1097#1080#1077
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 298
        object Label32: TLabel
          Left = 164
          Top = 12
          Width = 59
          Height = 13
          Caption = #1042#1080#1076' '#1086#1087#1083#1072#1090#1099
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label33: TLabel
          Left = 164
          Top = 36
          Width = 24
          Height = 13
          Caption = #1053#1044#1057
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label26: TLabel
          Left = 399
          Top = 12
          Width = 51
          Height = 13
          Caption = #1044#1072#1090#1072' '#1056#1069#1059
        end
        object cbSeller: TDBCheckBox
          Left = 8
          Top = 8
          Width = 81
          Height = 17
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataField = 'SELLER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox5: TDBCheckBox
          Left = 8
          Top = 24
          Width = 97
          Height = 17
          Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
          DataField = 'PRODUCER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object cbBuyer: TDBCheckBox
          Left = 8
          Top = 40
          Width = 133
          Height = 17
          Caption = #1054#1087#1090#1086#1074#1099#1081' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1100' '
          DataField = 'BUYER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object lcPayType: TDBLookupComboBox
          Left = 7964
          Top = 4064
          Width = 145
          Height = 21
          Color = clInfoBk
          DataField = 'PAYTYPEID'
          DataSource = dmCom.dsComp
          KeyField = 'PAYTYPEID'
          ListField = 'NAME'
          ListSource = dmCom.dsPayType
          TabOrder = 3
        end
        object lcNDS: TDBLookupComboBox
          Left = 228
          Top = 32
          Width = 145
          Height = 21
          Color = clInfoBk
          DataField = 'NDSID'
          DataSource = dmCom.dsComp
          KeyField = 'NDSID'
          ListField = 'NAME'
          ListSource = dmCom.dsNDS
          TabOrder = 4
        end
        object dbcommission: TDBCheckBox
          Left = 8
          Top = 82
          Width = 129
          Height = 17
          Caption = #1050#1086#1084#1080#1089#1089#1080#1103
          DataField = 'COMMISSION'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
          Visible = False
        end
        object chworkorg: TDBCheckBox
          Left = 8
          Top = 105
          Width = 217
          Height = 17
          Caption = #1057' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1086#1084' '#1074#1077#1076#1077#1090#1089#1103' '#1088#1072#1073#1086#1090#1072
          DataField = 'WORKORG'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object chretorg: TDBCheckBox
          Left = 8
          Top = 162
          Width = 361
          Height = 17
          Caption = #1056#1072#1079#1088#1077#1096#1077#1085' '#1074#1086#1079#1074#1088#1072#1090' '#1080#1079#1076#1077#1083#1080#1081' '#1083#1102#1073#1099#1093' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1086#1074
          DataField = 'RETORG'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object cbWorkProd: TDBCheckBox
          Left = 8
          Top = 120
          Width = 241
          Height = 17
          Caption = #1057' '#1080#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1077#1084' '#1074#1077#1076#1077#1090#1089#1103' '#1088#1072#1073#1086#1090#1072
          DataField = 'WORKPROD'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object chWorkBuyer: TDBCheckBox
          Left = 8
          Top = 136
          Width = 241
          Height = 17
          Caption = #1057' '#1086#1087#1090#1086#1074#1099#1084' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1084' '#1074#1077#1076#1077#1090#1089#1103' '#1088#1072#1073#1086#1090#1072
          DataField = 'WORKBUYER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 9
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox6: TDBCheckBox
          Left = 8
          Top = 56
          Width = 133
          Height = 17
          Caption = #1041#1072#1085#1082
          DataField = 'Is$Bank'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object REUDateEdit: TcxDBDateEdit
          Left = 456
          Top = 8
          DataBinding.DataField = 'REUED'
          DataBinding.DataSource = dmCom.dsComp
          Properties.ImmediatePost = True
          Properties.SaveTime = False
          Properties.ShowTime = False
          TabOrder = 11
          Width = 89
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 298
        object pc1: TPageControl
          Left = 0
          Top = 0
          Width = 580
          Height = 256
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          ExplicitHeight = 298
          object TabSheet1: TTabSheet
            Caption = #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 270
            object Label7: TLabel
              Left = 4
              Top = 4
              Width = 24
              Height = 13
              Caption = #1048#1053#1053
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label10: TLabel
              Left = 232
              Top = 4
              Width = 22
              Height = 13
              Caption = #1041#1048#1050
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label11: TLabel
              Left = 4
              Top = 28
              Width = 31
              Height = 13
              Caption = #1054#1050#1055#1054
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label12: TLabel
              Left = 136
              Top = 28
              Width = 38
              Height = 13
              Caption = #1054#1050#1042#1069#1044
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label13: TLabel
              Left = 4
              Top = 52
              Width = 25
              Height = 13
              Caption = #1041#1072#1085#1082
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label14: TLabel
              Left = 4
              Top = 76
              Width = 23
              Height = 13
              Caption = #1057#1095#1077#1090
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label15: TLabel
              Left = 4
              Top = 100
              Width = 46
              Height = 13
              Caption = #1050#1086#1088'.'#1073#1072#1085#1082
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label16: TLabel
              Left = 4
              Top = 124
              Width = 44
              Height = 13
              Caption = #1050#1086#1088'.'#1089#1095#1077#1090
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label17: TLabel
              Left = 4
              Top = 148
              Width = 50
              Height = 13
              Caption = #1040#1076#1088'.'#1089#1095#1077#1090#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label18: TLabel
              Left = 104
              Top = 168
              Width = 211
              Height = 13
              Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100'/'#1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label21: TLabel
              Left = 128
              Top = 204
              Width = 183
              Height = 13
              Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1075#1083#1072#1074#1085#1086#1075#1086' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label24: TLabel
              Left = 128
              Top = 242
              Width = 185
              Height = 13
              Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1075#1086' '#1083#1080#1094#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label37: TLabel
              Left = 260
              Top = 28
              Width = 23
              Height = 13
              Caption = #1050#1055#1055
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edINN: TDBEdit
              Left = 60
              Top = 0
              Width = 157
              Height = 21
              Color = clInfoBk
              DataField = 'INN'
              DataSource = dmCom.dsComp
              TabOrder = 0
            end
            object edBIK: TDBEdit
              Left = 260
              Top = 0
              Width = 101
              Height = 21
              Color = clInfoBk
              DataField = 'BIK'
              DataSource = dmCom.dsComp
              TabOrder = 1
            end
            object edOKPO: TDBEdit
              Left = 60
              Top = 24
              Width = 73
              Height = 21
              Color = clInfoBk
              DataField = 'OKPO'
              DataSource = dmCom.dsComp
              TabOrder = 2
            end
            object edOKONH: TDBEdit
              Left = 176
              Top = 24
              Width = 81
              Height = 21
              Color = clInfoBk
              DataField = 'OKONH'
              DataSource = dmCom.dsComp
              TabOrder = 3
            end
            object edBank: TDBEdit
              Left = 60
              Top = 48
              Width = 301
              Height = 21
              Color = clInfoBk
              DataField = 'BANK'
              DataSource = dmCom.dsComp
              TabOrder = 4
            end
            object edBill: TDBEdit
              Left = 60
              Top = 72
              Width = 301
              Height = 21
              Color = clInfoBk
              DataField = 'BILL'
              DataSource = dmCom.dsComp
              TabOrder = 5
            end
            object edKBank: TDBEdit
              Left = 60
              Top = 96
              Width = 301
              Height = 21
              Color = clInfoBk
              DataField = 'KBANK'
              DataSource = dmCom.dsComp
              TabOrder = 6
            end
            object edKBill: TDBEdit
              Left = 60
              Top = 120
              Width = 301
              Height = 21
              Color = clInfoBk
              DataField = 'KBILL'
              DataSource = dmCom.dsComp
              TabOrder = 7
            end
            object edAdrBill: TDBEdit
              Left = 59
              Top = 144
              Width = 302
              Height = 21
              Color = clInfoBk
              DataField = 'ADRBILL'
              DataSource = dmCom.dsComp
              TabOrder = 8
            end
            object edBoss: TDBEdit
              Left = 112
              Top = 180
              Width = 185
              Height = 21
              Color = clInfoBk
              DataField = 'BOSS'
              DataSource = dmCom.dsComp
              TabOrder = 9
            end
            object edBossPhone: TDBEdit
              Left = 296
              Top = 180
              Width = 65
              Height = 21
              Color = clInfoBk
              DataField = 'BOSSPHONE'
              DataSource = dmCom.dsComp
              TabOrder = 10
            end
            object edBuh: TDBEdit
              Left = 3
              Top = 218
              Width = 294
              Height = 21
              Color = clInfoBk
              DataField = 'BUH'
              DataSource = dmCom.dsComp
              TabOrder = 11
            end
            object edBuhPhone: TDBEdit
              Left = 296
              Top = 218
              Width = 65
              Height = 21
              Color = clInfoBk
              DataField = 'BUHPHONE'
              DataSource = dmCom.dsComp
              TabOrder = 12
            end
            object edOFIO: TDBEdit
              Left = 3
              Top = 256
              Width = 294
              Height = 21
              Color = clInfoBk
              DataField = 'OFIO'
              DataSource = dmCom.dsComp
              TabOrder = 13
            end
            object edOPhone: TDBEdit
              Left = 296
              Top = 256
              Width = 65
              Height = 21
              Color = clInfoBk
              DataField = 'PHONE'
              DataSource = dmCom.dsComp
              TabOrder = 14
            end
            object edBossPost: TDBEdit
              Left = 4
              Top = 180
              Width = 105
              Height = 21
              Color = clInfoBk
              DataField = 'BOSSPOST'
              DataSource = dmCom.dsComp
              TabOrder = 15
            end
            object edKPP: TDBEdit
              Left = 284
              Top = 24
              Width = 77
              Height = 21
              Color = clInfoBk
              DataField = 'KPP'
              DataSource = dmCom.dsComp
              TabOrder = 16
            end
          end
          object TabSheet2: TTabSheet
            Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 270
            object Label6: TLabel
              Left = 4
              Top = 4
              Width = 128
              Height = 13
              Caption = #1057#1077#1088#1080#1103'  '#1080' '#1085#1086#1084#1077#1088' '#1087#1072#1089#1087#1086#1088#1090#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label8: TLabel
              Left = 4
              Top = 52
              Width = 66
              Height = 13
              Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label9: TLabel
              Left = 4
              Top = 28
              Width = 56
              Height = 13
              Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label30: TLabel
              Left = 4
              Top = 76
              Width = 90
              Height = 13
              Caption = #8470' '#1089#1074#1080#1076#1077#1090#1077#1083#1100#1089#1090#1074#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label31: TLabel
              Left = 4
              Top = 100
              Width = 66
              Height = 13
              Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label34: TLabel
              Left = 5
              Top = 126
              Width = 24
              Height = 13
              Caption = #1048#1053#1053
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object edPaspSer: TDBEdit
              Left = 136
              Top = 0
              Width = 37
              Height = 21
              Color = clInfoBk
              DataField = 'PASPSER'
              DataSource = dmCom.dsComp
              TabOrder = 0
            end
            object edPaspNum: TDBEdit
              Left = 176
              Top = 0
              Width = 85
              Height = 21
              Color = clInfoBk
              DataField = 'PASPNUM'
              DataSource = dmCom.dsComp
              TabOrder = 1
            end
            object deDistrDate: TDBDateEdit
              Left = 76
              Top = 48
              Width = 121
              Height = 21
              Margins.Left = 1
              Margins.Top = 1
              DataField = 'DISTRDATE'
              DataSource = dmCom.dsComp
              Color = clInfoBk
              NumGlyphs = 2
              TabOrder = 3
            end
            object edDistrPlace: TDBEdit
              Left = 76
              Top = 24
              Width = 281
              Height = 21
              Color = clInfoBk
              DataField = 'DISTRPLACE'
              DataSource = dmCom.dsComp
              TabOrder = 2
            end
            object DBEdit5: TDBEdit
              Left = 100
              Top = 72
              Width = 97
              Height = 21
              Color = clInfoBk
              DataField = 'CERTN'
              DataSource = dmCom.dsComp
              TabOrder = 4
            end
            object DBDateEdit4: TDBDateEdit
              Left = 76
              Top = 96
              Width = 121
              Height = 21
              Margins.Left = 1
              Margins.Top = 1
              DataField = 'CERTDATE'
              DataSource = dmCom.dsComp
              Color = clInfoBk
              NumGlyphs = 2
              TabOrder = 5
            end
            object edINN2: TDBEdit
              Left = 76
              Top = 120
              Width = 197
              Height = 21
              Color = clInfoBk
              DataField = 'INN'
              DataSource = dmCom.dsComp
              TabOrder = 6
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = #1044#1086#1075#1086#1074#1086#1088
        ImageIndex = 4
        object BarDockControl: TdxBarDockControl
          Left = 0
          Top = 0
          Width = 580
          Height = 22
          Align = dalTop
          BarManager = BarManager
        end
        object Grid: TcxGrid
          Left = 0
          Top = 22
          Width = 580
          Height = 234
          Align = alClient
          TabOrder = 1
          LevelTabs.Style = 8
          LookAndFeel.Kind = lfOffice11
          object View: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = SourceContract
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsData.Appending = True
            OptionsView.NoDataToDisplayInfoText = '<>'
            OptionsView.CellTextMaxLineCount = 3
            OptionsView.DataRowHeight = 36
            OptionsView.GroupByBox = False
            object ViewCOMPANYID: TcxGridDBColumn
              Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
              DataBinding.FieldName = 'COMPANY$ID'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownListStyle = lsFixedList
              Properties.KeyFieldNames = 'D_COMPID'
              Properties.ListColumns = <
                item
                  FieldName = 'NAME'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = SelfCompanysDataSource
              Width = 111
            end
            object ViewCONTRACTTYPE: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
              DataBinding.FieldName = 'CONTRACT$TYPE'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'NAME'
                end>
              Properties.ListOptions.ShowHeader = False
              Properties.ListSource = dmCom.dsContractType
              Width = 128
            end
            object ViewNUMBER: TcxGridDBColumn
              Caption = #1053#1086#1084#1077#1088
              DataBinding.FieldName = 'NUMBER'
              Width = 91
            end
            object ViewCONTRACTDATE: TcxGridDBColumn
              Caption = #1044#1072#1090#1072
              DataBinding.FieldName = 'CONTRACT$DATE'
              Width = 98
            end
            object ViewCONTRACTENDDATE: TcxGridDBColumn
              Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103
              DataBinding.FieldName = 'CONTRACT$END$DATE'
              Width = 124
            end
            object ViewISUNLIMITED: TcxGridDBColumn
              Caption = #1041#1077#1089#1089#1088#1086#1095#1085#1099#1081
              DataBinding.FieldName = 'IS$UNLIMITED'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = 1
              Properties.ValueUnchecked = 0
              Width = 73
            end
            object ViewTAG: TcxGridDBColumn
              Caption = #1044#1086#1087'. '#1082#1083#1102#1095
              DataBinding.FieldName = 'TAG'
              Width = 100
            end
            object ViewCOMMENT: TcxGridDBColumn
              Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
              DataBinding.FieldName = 'COMMENT'
              Width = 300
            end
          end
          object Level: TcxGridLevel
            GridView = View
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = #1055#1088#1072#1074#1072' '#1076#1086#1089#1090#1091#1087#1072
        TabVisible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 298
        object Label19: TLabel
          Left = 4
          Top = 8
          Width = 38
          Height = 13
          Caption = #1055#1072#1088#1086#1083#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 4
          Top = 120
          Width = 87
          Height = 13
          Caption = #1057#1090#1077#1087#1077#1085#1100' '#1076#1086#1074#1077#1088#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 48
          Top = 4
          Width = 121
          Height = 21
          Color = clInfoBk
          TabOrder = 0
        end
        object DBCheckBox1: TDBCheckBox
          Left = 4
          Top = 32
          Width = 221
          Height = 17
          Caption = #1042#1082#1083#1102#1095#1077#1085#1080#1077' '#1074' '#1089#1087#1080#1089#1086#1082' '#1088#1072#1089#1089#1099#1083#1082#1080' ('#1089#1082#1083#1072#1076')'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 4
          Top = 52
          Width = 229
          Height = 17
          Caption = #1042#1082#1083#1102#1095#1077#1085#1080#1077' '#1074' '#1089#1087#1080#1089#1086#1082' '#1088#1072#1089#1089#1099#1083#1082#1080' ('#1072#1085#1086#1085#1089#1099')'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox3: TDBCheckBox
          Left = 4
          Top = 72
          Width = 213
          Height = 17
          Caption = #1056#1072#1073#1086#1090#1072' '#1089' "'#1085#1072#1082#1086#1087#1080#1090#1077#1083#1100#1085#1086#1081' '#1082#1086#1088#1079#1080#1085#1086#1081'"'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 4
          Top = 92
          Width = 221
          Height = 17
          Caption = #1053#1072#1083#1080#1095#1080#1077' '#1089#1082#1080#1076#1086#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit2: TDBEdit
          Left = 96
          Top = 116
          Width = 45
          Height = 21
          Color = clInfoBk
          TabOrder = 5
        end
      end
      object TabSheet5: TTabSheet
        Caption = #1051#1086#1075#1086#1090#1080#1087
        ImageIndex = 2
        TabVisible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 298
        object DBImage1: TDBImage
          Left = 8
          Top = 8
          Width = 385
          Height = 309
          DataField = 'LOGO'
          TabOrder = 0
          OnDblClick = DBImage1DblClick
        end
      end
      object tsMol: TTabSheet
        Caption = #1052#1054#1051
        ImageIndex = 3
        TabVisible = False
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 298
        object tb3: TSpeedBar
          Left = 0
          Top = 0
          Width = 580
          Height = 41
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          BoundLines = [blTop, blLeft, blRight]
          Position = bpCustom
          Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
          BtnOffsetHorz = 2
          BtnOffsetVert = 2
          BtnWidth = 64
          BtnHeight = 36
          Images = dmCom.ilButtons
          BevelOuter = bvSpace
          TabOrder = 0
          InternalVer = 1
          object SpeedbarSection3: TSpeedbarSection
            Caption = 'Untitled (0)'
          end
          object SpeedItem2: TSpeedItem
            BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
            Caption = #1044#1086#1073#1072#1074#1080#1090#1100
            Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
            ImageIndex = 1
            Margin = 0
            Spacing = 1
            Left = 2
            Top = 2
            Visible = True
            OnClick = SpeedItem2Click
            SectionName = 'Untitled (0)'
          end
          object SpeedItem3: TSpeedItem
            BtnCaption = #1059#1076#1072#1083#1080#1090#1100
            Caption = #1059#1076#1072#1083#1080#1090#1100
            Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
            ImageIndex = 2
            Margin = 0
            Spacing = 1
            Left = 66
            Top = 2
            Visible = True
            OnClick = SpeedItem3Click
            SectionName = 'Untitled (0)'
          end
        end
        object M207IBGrid1: TM207IBGrid
          Left = 0
          Top = 41
          Width = 580
          Height = 215
          Align = alClient
          Color = clBtnFace
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Visible = False
          MultiShortCut = 0
          ColorShortCut = 0
          InfoShortCut = 0
          ClearHighlight = False
          SortOnTitleClick = False
          Columns = <
            item
              Color = clInfoBk
              Expanded = False
              FieldName = 'FIO'
              Title.Alignment = taCenter
              Title.Caption = #1060#1048#1054' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086' '#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1093' '#1083#1080#1094
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clNavy
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = []
              Width = 299
              Visible = True
            end>
        end
      end
    end
    object DBRadioGroup1: TDBRadioGroup
      Left = 0
      Top = 207
      Width = 561
      Height = 55
      Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100' '#1040#1044#1056#1045#1057' '#1076#1083#1103' '#1087#1077#1095#1072#1090#1080' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074' '#1087#1086' '#1074#1086#1079#1074#1088#1072#1090#1091
      DataField = 'ADDRESS$PRINT'
      DataSource = dmCom.dsComp
      Items.Strings = (
        #1070#1088#1080#1076#1080#1095#1077#1089#1082#1080#1081
        #1055#1086#1095#1090#1086#1074#1099#1081)
      ParentBackground = True
      TabOrder = 2
      Values.Strings = (
        '1'
        '2')
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1175
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Margin = 0
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Margin = 0
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = SpeedItem5Click
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Margin = 0
      Spacing = 1
      Left = 402
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siSort: TSpeedItem
      BtnCaption = #1057#1086#1088#1090'.'
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      Hint = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      ImageIndex = 3
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = siSortClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 298
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 1175
    Height = 30
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 1
    BtnOffsetVert = 1
    BtnWidth = 96
    BtnHeight = 24
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 3
    InternalVer = 1
    object laCompCat: TLabel
      Left = 98
      Top = 4
      Width = 215
      Height = 19
      AutoSize = False
      Caption = #1042#1057#1045
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
      Layout = tlCenter
    end
    object Label36: TLabel
      Left = 415
      Top = 9
      Width = 24
      Height = 13
      Caption = #1050#1054#1044
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LMeneger: TLabel
      Left = 880
      Top = 8
      Width = 21
      Height = 13
      Caption = #1042#1057#1045
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label38: TLabel
      Left = 590
      Top = 6
      Width = 163
      Height = 13
      Caption = #1057' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1077#1081' '#1074#1077#1076#1077#1090#1089#1103' '#1088#1072#1073#1086#1090#1072
      Transparent = True
    end
    object Label39: TLabel
      Left = 211
      Top = 8
      Width = 71
      Height = 13
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
      Transparent = True
    end
    object edCode: TEdit
      Left = 448
      Top = 3
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edCodeKeyDown
    end
    object chAllWorkOrg: TCheckBox
      Left = 575
      Top = 7
      Width = 12
      Height = 12
      Hint = 
        #1045#1089#1083#1080' '#1085#1077' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085#1072' '#1075#1072#1083#1086#1095#1082#1072', '#1090#1086' '#1087#1086#1082#1072#1079#1074#1072#1102#1090#1089#1103' '#1090#1086#1083#1100#1082#1086' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080', ' +
        #13#10#1091' '#1082#1086#1090#1086#1088#1099#1093' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085' '#1092#1083#1072#1075' "'#1057' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1077#1081' '#1074#1077#1076#1077#1090#1089#1103' '#1088#1072#1073#1086#1090#1072'"'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      State = cbChecked
      TabOrder = 1
      OnClick = chAllWorkOrgClick
    end
    object Edit1: TEdit
      Left = 288
      Top = 3
      Width = 121
      Height = 21
      TabOrder = 2
      OnChange = Edit1Change
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siCategory: TSpeedItem
      BtnCaption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
      Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
      DropDownMenu = pmCat
      Hint = #1050#1072#1090#1077#1075#1086#1088#1080#1103
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 1
      Top = 1
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object simeneger: TSpeedItem
      BtnCaption = #1052#1077#1085#1077#1076#1078#1077#1088
      Caption = #1052#1077#1085#1077#1076#1078#1077#1088
      DropDownMenu = pmMeneger
      Hint = #1052#1077#1085#1077#1076#1078#1077#1088'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 777
      Top = 1
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 71
    Width = 587
    Height = 562
    Hint = 
      #1062#1074#1077#1090' '#1086' '#1079#1072#1082#1086#1085#1095#1077#1085#1085#1086#1084' '#1089#1088#1086#1082#1077':'#13#10#1078#1077#1083#1090#1099#1081' - '#1056#1069#1059','#13#10#1075#1086#1083#1091#1073#1086#1081' - '#1076#1086#1075#1086#1074#1086#1088','#13#10#1092#1080 +
      #1086#1083#1077#1090#1086#1074#1099#1081'- '#1086#1073#1072
    Align = alLeft
    AllowedSelections = [gstRecordBookmarks, gstAll]
    Color = clBtnFace
    ColumnDefValues.Title.Alignment = taCenter
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmCom.dsComp
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentShowHint = False
    PopupMenu = pmdg1
    RowDetailPanel.Color = clBtnFace
    ShowHint = True
    SortLocal = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellMouseClick = dg1CellMouseClick
    OnGetCellParams = dg1GetCellParams
    OnKeyDown = dg1KeyDown
    OnKeyUp = dg1KeyUp
    OnMouseUp = dg1MouseUp
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        ReadOnly = True
        Title.Caption = #8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 34
      end
      item
        Color = clInfoBk
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 136
      end
      item
        Color = clInfoBk
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        Title.Caption = #1050#1088'. '#1085#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 93
      end
      item
        Color = clInfoBk
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'CODE'
        Footers = <>
        Title.Caption = #1050#1086#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 46
      end
      item
        Color = clInfoBk
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Prod'
        Footers = <>
        Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 129
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'FORMATED$NAME'
        Footers = <>
        Title.Caption = #1058#1086#1088#1075#1086#1074#1072#1103' '#1084#1072#1088#1082#1072
        Width = 126
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pmCat: TPopupMenu
    Left = 212
    Top = 132
    object N1: TMenuItem
      Tag = 1
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080
      OnClick = N3Click
    end
    object N2: TMenuItem
      Tag = 2
      Caption = #1054#1087#1090#1086#1074#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080
      OnClick = N3Click
    end
    object N4: TMenuItem
      Tag = 3
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      OnClick = N3Click
    end
    object N5: TMenuItem
      Tag = 4
      Caption = #1041#1072#1085#1082#1080
      OnClick = N3Click
    end
    object N3: TMenuItem
      Tag = 100
      Caption = #1042#1089#1077
      OnClick = N3Click
    end
  end
  object od1: TOpenDialog
    DefaultExt = 'BMP'
    Filter = 'BMP-'#1092#1072#1081#1083#1099'|*.BMP'
    Left = 592
    Top = 329
  end
  object ppPrint: TRxPopupMenu
    Left = 256
    Top = 136
    object mnitAll: TMenuItem
      Caption = #1042#1089#1077
      OnClick = mnitAllClick
    end
    object st1: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' ('#1082#1088#1072#1090#1082#1086')'
      OnClick = st1Click
    end
    object mnitCur: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' ('#1087#1086#1083#1085#1086')'
      OnClick = mnitCurClick
    end
    object N6: TMenuItem
      Caption = #1042#1080#1076#1099' '#1086#1087#1083#1072#1090' '
      OnClick = N6Click
    end
    object NManager: TMenuItem
      Caption = #1054#1075#1088#1072#1085#1080#1079#1072#1094#1080#1080' '#1080' '#1084#1077#1085#1077#1076#1078#1077#1088#1099
      OnClick = fmMainNManagerClick
    end
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'dg1.Width')
    StoredValues = <>
    Left = 172
    Top = 176
  end
  object ActionList1: TActionList
    Left = 168
    Top = 136
    object acNewRecords: TAction
      Caption = 'acNewRecords'
      ShortCut = 45
      OnExecute = acNewRecordsExecute
    end
    object acUseComp: TAction
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082' '#1080#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103' '#1074'...'
      OnExecute = acUseCompExecute
      OnUpdate = acUseCompUpdate
    end
    object Action1: TAction
      Caption = 'Action1'
      ShortCut = 16466
      OnExecute = Action1Execute
    end
    object actionAddContract: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1076#1086#1075#1086#1074#1086#1088
      ImageIndex = 0
      OnExecute = actionAddContractExecute
    end
    object actionDeleteContract: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1076#1086#1075#1086#1074#1086#1088
      ImageIndex = 1
      OnExecute = actionDeleteContractExecute
      OnUpdate = actionDeleteContractUpdate
    end
    object actionSaveChanges: TAction
      OnExecute = actionSaveChangesExecute
      OnUpdate = actionSaveChangesUpdate
    end
  end
  object pmdg1: TPopupMenu
    Left = 256
    Top = 176
    object NUseComp: TMenuItem
      Action = acUseComp
    end
  end
  object pmMeneger: TPopupMenu
    Left = 208
    Top = 176
    object NAll: TMenuItem
      Tag = -1000
      Caption = #1042#1057#1045
      OnClick = NAllClick
    end
    object NNotMeneger: TMenuItem
      Tag = -999
      Caption = #1053#1045' '#1059#1057#1058#1040#1053#1054#1042#1051#1045#1053
      OnClick = NAllClick
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = Images
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 1144
    Top = 408
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowClose = False
      AllowQuickCustomizing = False
      BorderStyle = bbsNone
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockControl = BarDockControl
      DockedDockControl = BarDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 924
      FloatTop = 473
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarButton3'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarButton1: TdxBarButton
      Action = actionAddContract
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = actionDeleteContract
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = actionSaveChanges
      Category = 0
      ImageIndex = 2
    end
  end
  object Contract: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure contract$u(:id, :company$id, :contract$type, :' +
        'number, :contract$date, '
      
        '                             :contract$end$date, :is$unlimited, ' +
        ':tag, :comment, '
      '                             :is$delete)')
    DeleteSQL.Strings = (
      'execute procedure contract$d')
    InsertSQL.Strings = (
      
        'execute procedure contract$i(:company$id, :agent$id, :contract$t' +
        'ype, :number,'
      
        '                             :contract$date, :contract$end$date,' +
        ' :is$unlimited,'
      '                             :tag, :comment, :is$delete)')
    RefreshSQL.Strings = (
      'select'
      '  company$id,'
      '  agent$id,'
      '  contract$type,'
      '  number,'
      '  contract$date,'
      '  contract$end$date,'
      '  is$unlimited,'
      '  tag,'
      '  comment,'
      '  is$delete'
      'from '
      '  Contract$R(:ID)')
    SelectSQL.Strings = (
      'select'
      '  id,'
      '  company$id,'
      '  agent$id,'
      '  contract$type,'
      '  number,'
      '  contract$date,'
      '  contract$end$date,'
      '  is$unlimited,'
      '  tag,'
      '  comment,'
      '  is$delete'
      'from '
      '  Contract$S(:Company$ID, ?D_CompID)')
    AfterPost = ContractAfterPost
    BeforeClose = ContractBeforeClose
    BeforeOpen = ContractBeforeOpen
    OnNewRecord = ContractNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dmCom.dsComp
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 1144
    Top = 472
    poEmptyStrToNull = False
    dcForceOpen = True
    oRefreshAfterPost = False
    object ContractID: TFIBIntegerField
      FieldName = 'ID'
    end
    object ContractCONTRACTTYPE: TFIBIntegerField
      FieldName = 'CONTRACT$TYPE'
    end
    object ContractCONTRACTDATE: TFIBDateTimeField
      FieldName = 'CONTRACT$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object ContractCONTRACTENDDATE: TFIBDateTimeField
      FieldName = 'CONTRACT$END$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object ContractISUNLIMITED: TFIBSmallIntField
      FieldName = 'IS$UNLIMITED'
    end
    object ContractTAG: TFIBStringField
      FieldName = 'TAG'
      Size = 32
      EmptyStrToNull = False
    end
    object ContractCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 256
      EmptyStrToNull = False
    end
    object ContractISDELETE: TFIBIntegerField
      FieldName = 'IS$DELETE'
    end
    object ContractNUMBER: TFIBStringField
      FieldName = 'NUMBER'
      Size = 32
      EmptyStrToNull = False
    end
    object ContractCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object ContractAGENTID: TFIBIntegerField
      FieldName = 'AGENT$ID'
    end
  end
  object SourceContract: TDataSource
    DataSet = Contract
    Left = 1144
    Top = 504
  end
  object Images: TImageList
    Left = 1144
    Top = 440
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000242D7EFFD4D5
      E5FF000000000000000000000000000000000000000000000000000000000000
      0000242D7EFFD4D5E5FF0000000000000000EFEFEFFFD5D5D5FFCCCCCCFFBFBF
      BFFFB7B7B7FFB3B3B3FFB3B3B3FFB3B3B3FFB3B3B3FFB3B3B3FFB3B3B3FFB7B7
      B7FFBFBFBFFFCCCCCCFFD5D5D5FFEFEFEFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002B9948FF03BB2FFF04B82FFF06AC2FFF1C8F3BFF000000000000
      00000000000000000000000000000000000000000000283084FF303ACCFF2C36
      BAFFD4D5E5FF000000000000000000000000000000000000000000000000242D
      7EFF3B42BDFF343CAEFFD4D5E5FF00000000F7F7F7FFA3A3A2FF8A8B8EFF888C
      93FF848484FF828181FF828282FF828282FF828281FF828281FF818181FF8383
      83FF888B92FF87898BFFA3A3A3FFF7F7F7FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A972EFF00CB30FF00CC30FF00CC30FF0C8D2EFF000000000000
      000000000000000000000000000000000000293185FF646CDDFF6061E3FF3739
      DEFF2B35BBFFD4D5E5FF00000000000000000000000000000000242D7EFF3A41
      C0FF383DE9FF383EE0FF343CAEFFD4D5E5FFFFFFFFFFCDCED0FFFAEED4FFE6CD
      94FFF5F3F1FFF8F8F8FFF6F5F5FFF5F5F4FFF5F3F3FFF4F3F2FFF4F3F3FFEFED
      EBFFDFC491FFF1E5CEFFCDCECFFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF01C630FF00CB30FF00CC30FF0C902EFF000000000000
      0000000000000000000000000000000000004E57BAFF7171E2FF6A6AE2FF4D4E
      E0FF3739DEFF2B35BCFFD4D5E5FF0000000000000000242D7EFF373FC4FF383D
      E9FF383DE9FF383DE9FF3B42C1FF242D7EFFFFFFFFFFCDD0D6FFDFC282FFC787
      06FFE8E6E0FFF0F0F2FFEDECEBFFECEBEBFFEBEAE9FFE9E8E7FFE9EAEBFFE0DD
      D7FFBB7906FFD5B67EFFCDD0D5FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF02C130FF01C630FF00CB30FF0B932EFF000000000000
      000000000000000000000000000000000000D4D5E5FF4D56BAFF7070E1FF6868
      E2FF3C3DDDFF3739DEFF2A34BCFFD4D5E5FF242D7EFF333CC9FF383DE9FF383D
      E9FF383DE9FF3840C7FF2B338BFF00000000FFFFFFFFCDD0D5FFE6CB90FFCF94
      0EFFF0EDE8FFF7F8F9FFF4F3F2FFF3F2F2FFF2F1F0FFF0EFEEFFF1F1F2FFE8E4
      DFFFC48507FFDCBF87FFCDD0D5FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF2EC753FF02C130FF01C630FF0B962EFF000000000000
      00000000000000000000000000000000000000000000D4D5E5FF4C54BBFF6E6E
      E1FF6767E2FF3738DCFF3739DEFF2A34BDFF303ACDFF383DE8FF383DE9FF383D
      E9FF353ECBFF262E83FF0000000000000000FFFFFFFFCDD0D5FFE7CD96FFCF96
      18FFF0EFE9FFF9F9FBFFF5F5F3FFF4F4F3FFF3F2F2FFF1F0EFFFF2F2F3FFE9E6
      E0FFC68704FFDDC186FFCDD0D5FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D8C2EFF0A992EFF0A992EFF0A99
      2EFF0A992EFF0CA632FF3EC65EFF32C856FF02C130FF07A52EFF0B962EFF0B93
      2EFF0C902EFF0C8D2EFF0D882EFF000000000000000000000000D4D5E5FF5861
      C3FF6D6DE1FF6363E1FF3738DCFF3739DEFF383AE2FF383BE6FF383DE8FF333C
      CFFF262E83FF000000000000000000000000FFFFFFFFCDD0D5FFE8CF9BFFD19A
      25FFF4F3F1FFFDFEFFFFF9FAFBFFF8F9FBFFF7F8FAFFF6F6F7FFF6F7FBFFECEA
      E8FFC88A05FFDFC286FFCED0D6FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000077D48EFF73C187FF6BC181FF63C2
      7BFF5AC375FF53C46EFF4AC568FF42C762FF2FC754FF02C130FF01C630FF00CB
      30FF00CC30FF00CC30FF06AC2FFF00000000000000000000000000000000D4D5
      E5FF5962D0FF6B6BE0FF5353DEFF3738DCFF3839DFFF383AE2FF3039D2FF252D
      83FF00000000000000000000000000000000FFFFFFFFCED0D5FFE9D1A2FFD29B
      2DFFEEE8DBFFF7F4EFFFF4EFE7FFF3EFE7FFF2EDE5FFF0EBE1FFF1EEE8FFE7E0
      D1FFC98B04FFE0C588FFCED0D6FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000083D497FF7FC390FF77C38AFF6FC3
      84FF67C37EFF5FC478FF57C572FF4EC76BFF46C965FF33C857FF02C130FF01C6
      30FF00CB30FF00CC30FF04B82FFF000000000000000000000000000000000000
      0000293185FF6A70E4FF6B6AE0FF3F3FDAFF3738DCFF3539DCFF232EAFFFD4D5
      E5FF00000000000000000000000000000000FFFFFFFFCFD0D4FFEAD4AAFFD59B
      2EFFD7A545FFD4A850FFD4A649FFD3A543FFD3A53FFFD2A43BFFD19F2DFFD299
      13FFCC8B04FFE1C78CFFCFD1D6FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008EDAA1FF8BC69AFF83C594FF7BC5
      8DFF73C487FF6AC581FF63C67BFF5BC775FF52C86FFF4ACA68FF36C959FF02C1
      30FF01C630FF00CB30FF03BB2FFF000000000000000000000000000000002A32
      85FF6D78E7FF6D75E0FF6E6FDFFF6968E0FF3737D8FF3738DCFF3539DDFF232E
      B0FFD4D5E5FF000000000000000000000000FFFFFFFFCFD0D4FFEBD7B1FFD599
      2FFFDEC18BFFECEDEDFFEBE7DEFFEAE7DFFFE5DFD4FFE2DDD2FFDFD7C4FFD49A
      17FFCF8D04FFE3C98EFFCFD1D6FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001C933BFF0A992EFF0A992EFF0A99
      2EFF0A992EFF07A52EFF6EC685FF67C77EFF5FC878FF07A52EFF0A992EFF0A99
      2EFF0A992EFF0A972EFF0D8B2EFF0000000000000000000000002B3386FF7381
      EAFF6378DFFF6B79E0FF6B73DFFF646CE5FF6767E0FF3737D8FF3738DCFF3538
      DDFF232DB0FFD4D5E5FF0000000000000000FFFFFFFFCFD1D4FFECD8B6FFD59B
      39FFE3CBA2FFF6FCFFFFF4F2F0FFF4F8FFFFE0C898FFD5A646FFE4E1DCFFD6A1
      2FFFD09104FFE4CB94FFCFD1D6FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF7AC88EFF72C888FF6BC882FF0A992EFF000000000000
      000000000000000000000000000000000000000000002C3486FF818EEEFF6F84
      E1FF667CE0FF6A7EE1FF6E79EAFF333B8FFF515BD2FF5757DCFF3737D8FF3738
      DCFF3538DDFF222DB1FFD4D5E5FF00000000FFFFFFFFD0D1D4FFEEDABBFFD79F
      44FFE4CDA6FFF8FCFFFFF6F3EFFFF6F9FFFFDEC28BFFD29A2DFFE5E0D8FFD8A5
      3CFFD2940FFFE5CB95FFD0D2D7FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF86CA97FF7EC992FF76CA8CFF0A992EFF000000000000
      0000000000000000000000000000000000002C3486FF909CF2FF8093E5FF778B
      E3FF6E83E1FF7D87F1FF2A3286FF00000000D4D5E5FF505AD2FF3F3ED7FF3737
      D8FF3738DCFF3237DCFF2531C4FFD4D5E5FFFFFFFFFFD0D1D4FFEBD7B8FFD399
      42FFE2CAA5FFF8FDFFFFF6F4F2FFF5F7FAFFE6DBC9FFDECBA5FFE6E3DDFFD39E
      37FFD19621FFE9D9B5FFCCCED1FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF92CCA1FF8ACB9BFF82CB95FF0A992EFF000000000000
      000000000000000000000000000000000000888FE1FF92A2E9FF8899E6FF7F91
      E5FF818CF4FF2A3286FF000000000000000000000000D4D5E5FF4E58D3FF3535
      D6FF3737D8FF3738DCFF2B35DAFF242D7EFFFFFFFFFFD2D3D4FFFBF3E7FFEAD3
      B3FFF1E8DAFFFAFCFEFFF8F8F7FFF7F7F6FFF8F9FBFFF9FCFFFFF2F1F0FFE7D1
      A8FFEBDAB9FFE9EAECFFE1E1E1FFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000A992EFF9ECFABFF96CEA4FF8ECD9EFF0A992EFF000000000000
      000000000000000000000000000000000000D4D5E5FF878EE2FF91A0E9FF9099
      F7FF2B3386FF0000000000000000000000000000000000000000D4D5E5FF4C57
      D3FF3535D6FF2A33DCFF252D84FF00000000FFFFFFFFDDDDDDFFD2D3D4FFD1D3
      D6FFD1D2D3FFD0D0CFFFD0D0D0FFD0D0D0FFD0CFCFFFCFCFCDFFCFCFCFFFD1D3
      D7FFCDCED1FFE1E1E1FFFFFFFFFFFDFDFDFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001C933BFFA5E6B5FF9DE4AEFF96E3A8FF1C933BFF000000000000
      00000000000000000000000000000000000000000000D4D5E5FF858CD9FF242D
      7EFF00000000000000000000000000000000000000000000000000000000D4D5
      E5FF2B37C9FF242D85FF0000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFEFEFEFFFFFFFFFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFCFF300000000F83F87E100000000
      F83F03C000000000F83F018000000000F83F000100000000F83F800300000000
      0001C007000000000001E00F000000000001F00F000000000001E00700000000
      0001C00300000000F83F800100000000F83F010000000000F83F038000000000
      F83F07C100000000F83F8FE300000000}
  end
  object SelfCompanys: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  D_COMPID, Name'
      'from '
      '  d_Comp'
      'where '
      '  bit(basket, 0) = 1')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 1112
    Top = 472
  end
  object SelfCompanysDataSource: TDataSource
    DataSet = SelfCompanys
    Left = 1112
    Top = 504
  end
end
