unit WHUID;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmWHUID = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fmstrWHArt2: TFormStorage;
    ibgrWHUID: TM207IBGrid;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ibgrWHUIDGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHUID: TfmWHUID;

implementation

uses comdata, Data, ServData, Data2, M207Proc;

{$R *.DFM}

procedure TfmWHUID.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWHUID.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHUID.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if dm.WorkMode = 'WH' then
  begin
    ibgrWHUID.DataSource.DataSet := dmServ.quWHUID;
    KeyPreview := True;
    OnKeyPress := dm.CloseFormByEsc;
  end;
  ibgrWHUID.DataSource.DataSet.Active := True;
end;

procedure TfmWHUID.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHUID.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ibgrWHUID.DataSource.DataSet.Close;
end;

procedure TfmWHUID.ibgrWHUIDGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if dm.WorkMode = 'WH' then
  begin
    if Highlight or ibgrWHUID.ClearHighlight then Background := dm2.GetDepColor(dmServ.quWHUIDDEPID.AsInteger)
    else Background:=clHighlight;
  end;
  if (Field<>NIL) and (Field.FieldName='UID') then Background:=dmCom.clMoneyGreen;
end;

end.
