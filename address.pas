unit address;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, DB, ActnList, jpeg, rxSpeedbar;

type
  TfmAddress = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    siPrint: TSpeedItem;
    acList: TActionList;
    acClient: TAction;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siPrintClick(Sender: TObject);
    procedure acClientExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmAddress: TfmAddress;

implementation

uses comdata, DBTree, Data, M207Proc, Sort, UseClient;

{$R *.dfm}

procedure TfmAddress.WMSysCommand(var Message: TWMSysCommand); 
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmAddress.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmAddress.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taAddress.Delete;
end;

procedure TfmAddress.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taAddress.Append;
end;

procedure TfmAddress.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      OpenDataSets([taAddress]);
    end;
end;

procedure TfmAddress.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      if taAddressD_ADDRESS_ID.IsNull then taAddress.Tag:=-1
      else taAddress.Tag:=taAddressD_ADDRESS_ID.AsInteger;
      PostDataSets([taAddress]);
    end;
end;

procedure TfmAddress.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmAddress.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
 if (Field.FieldName='SNAME')  then Background:=dmcom.taAddressCOLOR.asinteger;
end;

procedure TfmAddress.siPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsAddress, 65); //'gd'
end;

procedure TfmAddress.acClientExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmUseClient, Self, TForm(fmUseClient), True, False)
end;

procedure TfmAddress.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100112)
end;

procedure TfmAddress.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
