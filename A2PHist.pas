unit A2PHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmA2PHist = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    M207IBGrid2: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmA2PHist: TfmA2PHist;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmA2PHist.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmA2PHist.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmA2PHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  dm2.quPHist.Open;
end;

procedure TfmA2PHist.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmA2PHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm2.quPHist.Close;
end;

end.
