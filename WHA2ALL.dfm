object fmWHA2All: TfmWHA2All
  Left = 272
  Top = 173
  Width = 483
  Height = 375
  Caption = #1057#1082#1083#1072#1076' - '#1040#1088#1090#1080#1082#1091#1083#1099
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 329
    Width = 475
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 475
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 371
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      BtnCaption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Hint = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siUIDClick
      SectionName = 'Untitled (0)'
    end
    object siSZ: TSpeedItem
      BtnCaption = #1056#1072#1079#1084#1077#1088#1099
      Caption = #1056#1072#1079#1084#1077#1088#1099
      Hint = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 47
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = siSZClick
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 41
    Width = 475
    Height = 288
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsWHA2All
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = PopupMenu1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 1
    TitleButtons = True
    OnGetCellParams = M207IBGrid1GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    PrintDataSet = 0
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 180
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'QUANTITY'
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 90
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'WEIGHT'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 85
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 156
    Top = 132
  end
  object PopupMenu1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 260
    Top = 164
    object N1: TMenuItem
      Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      ShortCut = 114
      OnClick = siUIDClick
    end
    object N2: TMenuItem
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 47
      RadioItem = True
      ShortCut = 115
      OnClick = siSZClick
    end
  end
end
