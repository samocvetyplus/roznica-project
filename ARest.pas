unit ARest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmARest = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    M207IBGrid1: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmARest: TfmARest;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmARest.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmARest.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dm do
    begin
      Caption:='������� - '+quArtFullArt.AsString;
      OpenDataSets([quARest]);
    end;
end;

procedure TfmARest.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmARest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    begin
      CloseDataSets([quARest]);
    end;
end;

procedure TfmARest.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmARest.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='DEP') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmARest.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_ESCAPE, VK_F12] then Close;
end;

end.
