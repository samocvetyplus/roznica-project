unit UIDWH;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DateUtils,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, db, Mask, Menus, RxMenus,
  DBCtrls, Buttons, M207Proc, CheckLst, ImgList, RXSplit,
  DBGridEh, ActnList, PrnDbgeh, Printers, PrntsEh, dbUtil, StrUtils,
  FIBDataSet, pFIBDataSet, RXSpin, jpeg, DBGridEhGrouping, rxPlacemnt,
  rxToolEdit, rxSpeedbar, GridsEh, XLSDbRead4, CellFormats4, ComObj, XLSFormat,
  FormattedObj4, Cell4, XLSExportComp, FIBQuery, pFIBQuery;

type
  TfmUIDWH = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    laDep: TLabel;
    FormStorage1: TFormStorage;
    spitPrint: TSpeedItem;
    pm1: TPopupMenu;
    miAnyPeriod: TMenuItem;
    SpeedItem4: TSpeedItem;
    laPeriod: TLabel;
    ppPrint: TRxPopupMenu;
    mnitTag: TMenuItem;
    mnitList: TMenuItem;
    siCalc: TSpeedItem;
    SpeedItem3: TSpeedItem;
    siFind: TSpeedItem;
    pm3: TPopupMenu;
    N2: TMenuItem;
    spitHistory: TSpeedItem;
    pm2: TPopupMenu;
    N3: TMenuItem;
    miUIDHist: TMenuItem;
    Panel1: TPanel;
    laTime: TLabel;
    B1: TMenuItem;
    miUIDPHist: TMenuItem;
    N5: TMenuItem;
    siEdit: TSpeedItem;
    N4: TMenuItem;
    N6: TMenuItem;
    tb3: TSpeedBar;
    lbFind: TLabel;
    lbFindUID: TLabel;
    edUID: TComboEdit;
    edArt: TComboEdit;
    spbr4: TSpeedBar;
    lbMat: TLabel;
    lbGood: TLabel;
    lbComp: TLabel;
    lbSup: TLabel;
    SpeedbarSection3: TSpeedbarSection;
    plTotal: TPanel;
    lbArt: TLabel;
    lbW: TLabel;
    lbQ: TLabel;
    txtArt: TDBText;
    txtQ: TDBText;
    txtW: TDBText;
    lbPrice: TLabel;
    lbPrice2: TLabel;
    txtPrice: TDBText;
    txtPrice2: TDBText;
    N7: TMenuItem;
    SpeedItem7: TSpeedItem;
    SpeedItem8: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    spItog: TSpeedItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    chbMaxPrice: TCheckBox;
    nHArt: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    grUIDWH: TDBGridEh;
    actL: TActionList;
    acPrint: TAction;
    prdbe1: TPrintDBGridEh;
    grTotal: TDBGridEh;
    chlbMat: TCheckListBox;
    chlbGood: TCheckListBox;
    chlbProd: TCheckListBox;
    N13: TMenuItem;
    SpeedItem6: TSpeedItem;
    chlbnote1: TCheckListBox;
    chlbnote2: TCheckListBox;
    ScrollBar1: TScrollBar;
    btnDo: TSpeedButton;
    spbr5: TSpeedBar;
    lbnote1: TLabel;
    lbnote2: TLabel;
    ScrollBar2: TScrollBar;
    SpeedbarSection5: TSpeedbarSection;
    SpeedItem14: TSpeedItem;
    SpeedItem13: TSpeedItem;
    siText: TSpeedItem;
    svdfile: TSaveDialog;
    acIns: TAction;
    acSumGr: TAction;
    acVHelpFields: TAction;
    PrinterSetupDialog1: TPrinterSetupDialog;
    SpeedItem11: TSpeedItem;
    SpeedItem12: TSpeedItem;
    Label1: TLabel;
    Label2: TLabel;
    chlbatt1: TCheckListBox;
    chlbatt2: TCheckListBox;
    N16: TMenuItem;
    mnitList1: TMenuItem;
    siinv: TSpeedItem;
    NTagAllSelect: TMenuItem;
    pminventory: TPopupMenu;
    Nnew: TMenuItem;
    Nold: TMenuItem;
    acNewTextIncome: TAction;
    siAppl: TSpeedItem;
    acCreateAppl: TAction;
    acClearAppl: TAction;
    acRecalclAppl: TAction;
    pmAppl: TPopupMenu;
    NCreateAppl: TMenuItem;
    NRecalcAppl: TMenuItem;
    NClearAppl: TMenuItem;
    NCreateInv: TMenuItem;
    N19: TMenuItem;
    NApplDep: TMenuItem;
    NMClearApplDep: TMenuItem;
    acCreateApplDep: TAction;
    acClearApplDep: TAction;
    acNewInventory: TAction;
    acNewInventory1: TAction;
    acOldInventory: TAction;
    acEditUidwh: TAction;
    acRecalcUiDWH: TAction;
    acRepeat: TAction;
    acStartGlobalInventory: TAction;
    acStopGlobalInventory: TAction;
    N20: TMenuItem;
    N21: TMenuItem;
    pmtext: TPopupMenu;
    acTextOld: TAction;
    acTextOld1: TMenuItem;
    N22: TMenuItem;
    acNewTextOutLine: TAction;
    N23: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    plSummApplDep: TPanel;
    N26: TMenuItem;
    NRealCost: TMenuItem;
    acRealCost: TAction;
    acBadPrice: TAction;
    acTest: TAction;
    N27: TMenuItem;
    NDinv: TMenuItem;
    NPrordWH: TMenuItem;
    plDepAppl: TPanel;
    LCountApplDep: TLabel;
    DBText1: TDBText;
    sp1: TSplitter;
    plDepFromAppl: TPanel;
    Label3: TLabel;
    DBText2: TDBText;
    lbSz: TLabel;
    siSz: TSpeedItem;
    chlbsz: TCheckListBox;
    siHelp: TSpeedItem;
    SpeedbarSection4: TSpeedbarSection;
    siType: TSpeedItem;
    pmtype: TPopupMenu;
    NAll: TMenuItem;
    NAllWh: TMenuItem;
    NSell: TMenuItem;
    NSret: TMenuItem;
    plCost: TPanel;
    edCost1: TRxSpinEdit;
    edCost2: TRxSpinEdit;
    LCost: TLabel;
    Label4: TLabel;
    chCost: TCheckBox;
    NOpenSinv: TMenuItem;
    tb6: TSpeedBar;
    SpeedbarSection6: TSpeedbarSection;
    LArt2: TLabel;
    plFilterW: TPanel;
    Lw: TLabel;
    rxw1: TRxSpinEdit;
    rxw2: TRxSpinEdit;
    Label5: TLabel;
    chW: TCheckBox;
    edArt2: TComboEdit;
    NCreateAppl1: TMenuItem;
    acCreateAppl1: TAction;
    lbIns: TLabel;
    SpeedItem15: TSpeedItem;
    chlbIns: TCheckListBox;
    SpeedItem16: TSpeedItem;
    Label6: TLabel;
    Label7: TLabel;
    siComission: TSpeedItem;
    pmComission: TPopupMenu;
    N1: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    Color: TSpeedItem;
    Label8: TLabel;
    g1: TProgressBar;
    siPayType: TSpeedItem;
    pmPay: TPopupMenu;
    N18: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N32: TMenuItem;
    Excel1: TMenuItem;
    XLSExportFile1: TXLSExportFile;
    chlbSup: TCheckListBox;
    SD1: TSaveDialog;
    N17: TMenuItem;
    acSales: TAction;
    pFBQNika: TpFIBQuery;
    NClRet: TMenuItem;
    siTolling: TSpeedItem;
    pmTolling: TPopupMenu;
    N_All: TMenuItem;
    N_Tolling: TMenuItem;
    N_not_Tolling: TMenuItem;
    acTollingFilter: TAction;
    nComissionAct: TMenuItem;
    quUIDWHdelete: TpFIBDataSet;
    quUIDWHdeleteUIDWHID: TFIBIntegerField;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure grUIDWHGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure mnitListClick(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure siFindClick(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure miUIDHistClick(Sender: TObject);
    procedure miUIDPHistClick(Sender: TObject);
    procedure SpeedItem6Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure btnDoClick(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edUIDButtonClick(Sender: TObject);
    procedure edUIDKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtButtonClick(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
    procedure chlbProdExit(Sender: TObject);
    procedure chlbProdClickCheck(Sender: TObject);
    procedure chlbSupClickCheck(Sender: TObject);
    procedure chlbMatClickCheck(Sender: TObject);
    procedure chlbGoodClickCheck(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure spItogClick(Sender: TObject);
    procedure chbMaxPriceClick(Sender: TObject);
    procedure nHArtClick(Sender: TObject);
    procedure udpDataReceived(Sender: TComponent; NumberBytes: Integer;
      FromIP: String; Port: Integer);
    procedure dgeNGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgeuuGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure SpeedItem6Click0(Sender: TObject);
    procedure chlbnote1ClickCheck(Sender: TObject);
    procedure chlbnote2ClickCheck(Sender: TObject);
    procedure SpeedItem13Click(Sender: TObject);
    procedure SpeedItem8Click(Sender: TObject);
    procedure SpeedItem14Click(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acSumGrExecute(Sender: TObject);
    procedure acVHelpFieldsExecute(Sender: TObject);
    procedure SpeedItem11Click(Sender: TObject);
    procedure SpeedItem12Click(Sender: TObject);
    procedure chlbatt2ClickCheck(Sender: TObject);
    procedure chlbatt1ClickCheck(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure acNewTextIncomeExecute(Sender: TObject);
    procedure acCreateApplExecute(Sender: TObject);
    procedure acClearApplExecute(Sender: TObject);
    procedure acRecalclApplExecute(Sender: TObject);
    procedure grUIDWHTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure acCreateApplDepExecute(Sender: TObject);
    procedure acClearApplDepExecute(Sender: TObject);
    procedure acNewTextIncomeUpdate(Sender: TObject);
    procedure acNewInventoryExecute(Sender: TObject);
    procedure acNewInventory1Execute(Sender: TObject);
    procedure acOldInventoryExecute(Sender: TObject);
    procedure acEditUidwhExecute(Sender: TObject);
    procedure acRecalcUiDWHExecute(Sender: TObject);
//    procedure acRepeatExecute(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure acStartGlobalInventoryUpdate(Sender: TObject);
    procedure acStopGlobalInventoryUpdate(Sender: TObject);
    procedure acStartGlobalInventoryExecute(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure acTextOldExecute(Sender: TObject);
    procedure acNewTextOldExecute(Sender: TObject);
    procedure acNewTextOutLineExecute(Sender: TObject);
    procedure acNewTextOutLineUpdate(Sender: TObject);
    procedure acRecalcUiDWHUpdate(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure acCreateApplDepUpdate(Sender: TObject);
    procedure acRealCostExecute(Sender: TObject);
//    procedure acTestExecute(Sender: TObject);
//    procedure acBadPriceExecute(Sender: TObject);
    procedure acBadPriceUpdate(Sender: TObject);
    procedure NDinvClick(Sender: TObject);
    procedure NPrordWHClick(Sender: TObject);
    procedure siSzClick(Sender: TObject);
    procedure chlbszClickCheck(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure NAllClick(Sender: TObject);
    procedure chCostClick(Sender: TObject);
    procedure chWClick(Sender: TObject);
    procedure edArt2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArt2ButtonClick(Sender: TObject);
    procedure edArt2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acRepeatUpdate(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure acCreateAppl1Execute(Sender: TObject);
    procedure cbDifferenceClick(Sender: TObject);
    procedure ButtonUIDClick(Sender: TObject);
    procedure SpeedItem15Click(Sender: TObject);
    procedure chlbInsExit(Sender: TObject);
    procedure chlbInsClickCheck(Sender: TObject);
    procedure cbComissionClick(Sender: TObject);
    procedure OnComission(Sender: TObject);
    procedure OnPaytype(Sender: TObject);
    procedure ColorClick(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure acSalesExecute(Sender: TObject);
    procedure acTollingFilterExecute(Sender: TObject);
    procedure nComissionActClick(Sender: TObject);
    procedure UIDWHClean;
  private
    FNotRecalc {������ ��������� ������},
    FThisCalcWH {����� �� ������ ��� ��� ������ �� �����}:boolean;
    FGenTUIDWH:integer;
    FSearchEnable : boolean;
    FApplyFilter : boolean;
    FStopUIDSearchFunc : TLocateCallBack;
    FStopUIDSearch : boolean;
    FSearchingUID : boolean;
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure PrepareData;
    procedure ShowPeriod;
    function StopSearch : boolean;
    function GetIdxByCode(sl : TStringList; Code : Variant) : integer;
    procedure CheckCalcing;
    procedure GetSell;

    procedure TagPrintWH (Sender: TObject);
    procedure TagPrintWHAll (Sender: TObject);
//    procedure MouseWheelHandler(var Message: TMessage);override;
//    function  Stop: boolean;
  public
    function CheckMultiContr(chlb:TCheckListBox;var st:string; lCanModif:boolean=false):boolean;
    function ChlbClickCheck(chlb:TCheckListBox;var st:string):variant;
    function UIDWHFilter:string;
    function StrMultiContr(chlb:TCheckListBox):string;
  end;

var
  fmUIDWH: TfmUIDWH;
  flagstatesort:byte;

implementation

uses comdata, Data2, DataFR, Data, ReportData, OptList, Period, Ins,
  UIDFind, HistItem, UIDHist, UIDPHist, UIDEdit, SameUID,
  UIDWHFlt, dbTree, ServData, Variants, RXStrUtils, Comp,
  uidwhsumgr, getdata, goods_sam, Inventory, PredInventory, data3,
  MsgDialog, ListInventory, SameUIDP, MsgDialogC, fmUtils,
  SelectUidWh, Col, JewConst, Lexems, bsUtils;

{$R *.DFM}
procedure TfmUIDWH.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

{procedure TfmUIDWH.MouseWheelHandler(var Message: TMessage);
var
  p:TPoint;
begin
 GetCursorPos(p);
 If WindowFromPoint(p)=grUIDWH.Handle then
 begin
  If Message.WParam<0 then
  grUIDWH.Perform(WM_VSCROLL, SB_LINEDOWN, 0)
  else
  grUIDWH.Perform(WM_VSCROLL, SB_LINEUP, 0);
 end;
end;   }

procedure TfmUIDWH.TagPrintWH (Sender: TObject);
var arr : TarrDoc;
begin
 if  dm.dsUIDWH.DataSet=dm.quUIDWH then arr:=gen_arr(grUIDWH, dm.quUIDWHSITEMID)
 else arr:=gen_arr(grUIDWH, dm.quMaxUIDWHSITEMID);
 try
   PrintTag(arr, it_tag,TMenuItem(Sender).Tag);
 finally
   Finalize(arr);
 end;
end;

procedure TfmUIDWH.TagPrintWHAll (Sender: TObject);
var arr : TarrDoc;
begin
  grUIDWH.SelectedRows.SelectAll;
  if dm.dsUIDWH.DataSet=dm.quUIDWH then arr:=gen_arr(grUIDWH, dm.quUIDWHSITEMID)
  else arr:=gen_arr(grUIDWH, dm.quMaxUIDWHSITEMID);
  try
    PrintTag(arr, it_tag,TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
  grUIDWH.SelectedRows.Clear;
end;

procedure TfmUIDWH.siExitClick(Sender: TObject);
begin
  CloseDataSets([dm.quMaxUIDWH]);
  dm.PayType:=-1000; 
  dm.dsUIDWH.DataSet:=dm.quUIDWH;
  Close;
end;

procedure TfmUIDWH.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
end;

procedure TfmUIDWH.FormCreate(Sender: TObject);
var j: integer;
   mi: TMenuItem;
   selfdep: integer;
   found: boolean;
begin
dm.IsTolling := -1;
grUIDWH.ScrollBy(1, 1000);

//---------------------------
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
  flagstatesort:=0;
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  tb3.WallPaper:=wp;
  tb6.WallPaper:=wp;
  spbr4.Wallpaper := wp;
  spbr5.Wallpaper := wp;
  plSummApplDep.Visible := CenterDep;
  with dmCom, dm, dm2 do
    begin
      WorkMode:='UIDWH';
      edArt.Text := FilterArt;
      if tr.Active then tr.CommitRetaining;
      if not tr.Active then tr.StartTransaction;
      if NOT tr1.Active then tr1.StartTransaction;
      UIDWHBD:=FirstMonthDate;   // ������ �������� ��� ������ - ������ ���� ������
      UIDWHED:= strtodatetime(datetostr(dmCom.GetServerTime))+0.9999; // ����� - ������� ����
      ShowPeriod;  // ������� ������� ��� ������� ������
      OpenDataSets([taNDS, quDep, taAtt1, taAtt2]);
      with dm.quTmp do
    begin
      Close;
      //SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=1 AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') and depid = '+inttostr(SDepId)+')';
      SQL.Text:='SELECT d_depid from d_dep where isprog=1';
      ExecQuery;
      selfdep:=Fields[0].AsInteger;
      Close;
    end;
if (selfdep in [1,20,21])  then
  begin
   acSales.Enabled:=true;
   N17.Visible:=true;
  end else
  begin
   acSales.Enabled:=false;
   N17.Visible:=false;
  end;

      dm.LoadArtSL(ALL_DICT);
      D_MatId:='*';
      D_GoodId:='*';
      D_CompId:=-1;
      D_SupId := -1;
      D_Att1ID:=ATT1_DICT_ROOT;
      D_Att2ID:=ATT2_DICT_ROOT;
      D_Note1:=-1;
      D_Note2:=-1;
      UidwhSZ_:='*';
      D_Insid := '*';

      SD_MatID:='';
      SD_GoodID:='';
      SD_SupID:='';
      SD_CompID:='';
      SD_Note1:='';
      SD_Note2:='';
      SD_Att1:='';
      SD_Att2:='';
      SD_InsID := '';
      UidwhSZ:='';


      dm.FilterArt2:='';
      IFilterCost:=false;
      IFilterW:=false;      
      siSz.BtnCaption:='*���';

      lMultiSelect:=false;
      lSellSelect:=false;
      AllUIDWH:=true;

      chlbProd.Items.Assign(dm.slProd);
      chlbProd.ItemIndex := GetIdxByCode(dm.slProd, D_CompId);
      chlbProd.Checked[chlbProd.ItemIndex]:=true;
      chlbProdClickCheck(chlbProd);

      chlbSup.Items.Assign(dm.slSup);
      chlbSup.ItemIndex := GetIdxByCode(dm.slSup, D_SupId);
      chlbSup.Checked[chlbSup.ItemIndex]:=true;
      chlbSupClickCheck(chlbSup);

      chlbMat.Items.Assign(dm.slMat);
      chlbMat.ItemIndex := GetIdxByCode(dm.slMat, D_MatId);
      chlbMat.Checked[chlbMat.ItemIndex]:=true;
      chlbMatClickCheck(chlbMat);

      chlbGood.Items.Assign(dm.slGood);
      chlbGood.ItemIndex := GetIdxByCode(dm.slGood, D_GoodId);
      chlbGood.Checked[chlbGood.ItemIndex]:=true;
      chlbGoodClickCheck(chlbGood);

      chlbnote1.Items.Assign(dm.slNote1);
      chlbnote1.ItemIndex := GetIdxByCode(dm.slNote1, D_Note1);
      chlbnote1.Checked[chlbnote1.ItemIndex]:=true;
      chlbnote1ClickCheck(chlbnote1);

      chlbnote2.Items.Assign(dm.slNote2);
      chlbnote2.ItemIndex := GetIdxByCode(dm.slNote2, D_Note2);
      chlbnote2.Checked[chlbnote2.ItemIndex]:=true;
      chlbnote2ClickCheck(chlbnote2);

      chlbatt1.Items.Assign(dm.slAtt1);
      chlbatt1.ItemIndex := GetIdxByCode(dm.slAtt1, D_Att1ID);
      chlbatt1.Checked[chlbatt1.ItemIndex]:=true;
      chlbatt1ClickCheck(chlbatt1);

      chlbatt2.Items.Assign(dm.slAtt2);
      chlbatt2.ItemIndex := GetIdxByCode(dm.slAtt2, D_Att2ID);
      chlbatt2.Checked[chlbatt2.ItemIndex]:=true;
      chlbatt2ClickCheck(chlbatt2);

      chlbSz.Items.Assign(dm.slSz);
      chlbSZ.ItemIndex := GetIdxByCode(dm.slSZ, UidwhSZ_);
      chlbSZ.Checked[chlbSZ.ItemIndex]:=true;
      chlbSZClickCheck(chlbSZ);

      chlbIns.Items.Assign(dm.slIns);
      chlbIns.ItemIndex := GetIdxByCode(dm.slIns, D_InsID);
      chlbIns.Checked[chlbIns.ItemIndex]:=true;
      chlbInsClickCheck(chlbIns);

      FSearchEnable := False;
      FApplyFilter := True;
      edUID.Text := '';
      FSearchEnable := True;
      btnDo.Glyph.Handle := LoadBitMap(hInstance, 'REDO');
      FStopUIDSearchFunc := StopSearch;
      FSearchingUID := False;
      FGenTUIDWH := dmCom.GetID(35);

      {���������� ���� ��� ������ � �������������}
      for j:=0 to grUIDWH.Columns.Count -1 do
        if  grUIDWH.Columns[j].FieldName = 'SPRICE0' then
            begin
              if Not CenterDep then  grUIDWH.Columns[j].Visible := false
              else grUIDWH.Columns[j].Visible := true;
              break;
            end;

     siEdit.Enabled:=CenterDep;
     N4.Enabled:=CenterDep;
     NCreateAppl1.Visible:=CenterDep;
      with quPrepUIDWH do
        begin
          ExecQuery;
          if (Fields[0].asInteger=0)and(Fields[1].asDate<strtodatetime(DateToStr(dmCom.GetServerTime))) then
          begin
            ExecSQL('update d_rec set calcwhdate=0', quTmp);
            PrepareData;
          end;
          Close;
        end;
    end;
  dmcom.FillTagMenu(mnitTag, TagPrintWH, 0);
  dmcom.FillTagMenu(NTagAllSelect, TagPrintWHAll, 0);

  dm.IsChangeApplDep:=false;
  dm.IsChangeAppl:=false;
  fmUIDWH.Caption:='����� ��-��������';
  FThisCalcWH:=false;
  FNotRecalc:=false;
  if CenterDep then
  begin
   grUIDWH.FieldColumns['PRICEINV'].Visible:=false;
   grUIDWH.FieldColumns['COSTINV'].Visible:=false;
   grUIDWH.FieldColumns['EUID'].Visible:=true;
   acRealCost.Enabled:=true;
   acRealCost.Visible:=true;
  end
  else
  begin
   grUIDWH.FieldColumns['PRICEINV'].Visible:=true;
   grUIDWH.FieldColumns['COSTINV'].Visible:=true;
   grUIDWH.FieldColumns['EUID'].Visible:=false;
   acRealCost.Enabled:=false;
   acRealCost.Visible:=false;
  end;
  dm.Comission := -1;
 if Centerdep then
 
  //��������� ���� ����� �����
With dmcom.taPayType do
begin
dmcom.taPayType.Open;
first;
while not eof do
if (dmcom.taPayTypePAYTYPEID.AsInteger<>7) then
 begin
   mi:=TmenuItem.Create(Self);
   mi.Caption:='������: '+dmCom.taPayTypeNAME.AsString;
   mi.OnClick:=OnPaytype;
   mi.Tag:=dmCom.taPayTypePAYTYPEID.AsInteger;
   pmPay.Items.Add(mi);
   next;
 end
 else
 next;
end;
dmcom.taPayType.Close;
  //--------- ������� ��������� ����� ��� ��������� --------------//
  sipaytype.Visible:=CenterDep;
  grTotal.FieldColumns['STP'].Visible := CenterDep;
  mnitList1.Enabled:=CenterDep;
  //--------------------------------------------------------------//
  //��������� ���� ������
  ppPrint.Items[16].Visible:=CenterDep;

  if not CenterDep then begin
    found := false;

    for j := 0 to grUIDWH.Columns.Count - 1 do begin
      if grUIDWH.Columns[j].FieldName = 'Tolling$Price' then begin
        found := true;
        break;
      end;
    end;

    if found then begin
      grUIDWH.Columns[j].FieldName := '';
      grUIDWH.Columns[j].Visible := false;
    end;
  end;
  

end;

procedure TfmUIDWH.FormActivate(Sender: TObject);
begin
  with dm do
    begin
      dm.ReopenUidWH:=false;
      UIDWHTime:=dmCom.GetServerTime;
      ShowPeriod;  // ������� ������� ��� ������� ������
      if NOT dsUIDWH.DataSet{quUIDWH}.Active then
        if UIDWHType=3 then
          begin
            ClickUserDepMenu(dm.pmWH);
          end;

    if Not CenterDep then
    begin

      with NoCloseRetAct do
         begin
           Params.ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(UIDWHBD);
           Params.ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(UIDWHED);
           SetDepFilter(NoCloseRetAct.Params, SDepId);

         end;

      with NoCloseRetAct do
        begin
          ExecQuery;
          if (Fields[0].asInteger<>0) and (not dmcom.Seller) then
            MessageDialog('�������  ' + IntToStr(Fields[0].asInteger) + '  �� �������� ���� �������� �� �����������', mtWarning, [mbOk], 0);
          Close;
        end;

    end;
    end ;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;

  acRepeat.Visible:= AppDebug;
  acBadPrice.Visible:= AppDebug or ((not AppDebug) and dmcom.Adm);
  acTest.Visible:= acRepeat.Visible or acBadPrice.Visible;
  acEditUidwh.Enabled:=CenterDep;
end;

procedure TfmUIDWH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
    begin
      {��� ������ ��������� ��������� � ������� �� ������, ���� ��� ���������������}
      if dm.IsChangeApplDep then
      begin
       if MessageDialog('������ �� ������� ���� ���������������. ������������ ������?', mtWarning, [mbYes, mbNo], 0)=mrYes then
        acCreateApplDepExecute(nil);
      end;

      if dm.IsChangeAppl then
      begin
        if MessageDialog('������ ����������� ���� ���������������. ������� �����?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel
            then SysUtils.Abort;
      end;
      CloseDataSets([taNDS, quDep, dsUIDWH.DataSet{quUIDWH}]);
      if not tr.Active then tr.StartTransaction;
      tr.CommitRetaining;
      if not tr1.Active then tr1.StartTransaction;
      tr1.CommitRetaining;
      FilterArt:='';
      UIDWHType:=3;
     if FThisCalcWH or FNotRecalc then
      begin
       FThisCalcWH:=false;
       FNotRecalc:=false;
       ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.qutmp);
      end
    end;
    Deactivate;

end;


procedure TfmUIDWH.grUIDWHGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
//  if Highlight then Background:=clHighlight
//  else
  AFont.Color:=clWindowText;
  with dm.dsUIDWH.DataSet do
  if Field<>NIL then
          if Field.FieldName='DEP' then Background:=FieldByName('Color').AsInteger//dm.quUIDWHColor.AsInteger
          else if (Field.FieldName='ART2') or
                  (Field.FieldName='SZ') then Background:=clBtnFace
          else if (Field.FieldName='PRICE') or
                  (Field.FieldName='COST') or
                  (Field.FieldName='SPRICE0') then Background:=clAqua
          else
            with dm do
              case FieldByName('T').AsInteger of
//              case dm.quUIDWHT.AsInteger of
                   0: case {quUIDWH}FieldByName('IType').AsInteger of
                          2: if  {quUIDWH}FieldByName('Sdate0').AsDateTime<=dm.BeginDate then   Background:=CU_SBD1
                          else  Background:=CU_S1;
                          5: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                               0: Background:=CU_SBD0;
                               1: Background:=CU_SBD1;
                             end
                          else  Background:=CU_R;
                        end;

                   -1,1: case {quUIDWH}FieldByName('IType').AsInteger of
                          1: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_S0;
                                 1: Background:=CU_S1;
                               end;
                          2: begin
                           {�����.�����������}
                              if {quUIDWH}FieldByName('IsClosed').AsInteger=0 then    Background:=CU_IM0 //�� ��������
                              else if  {quUIDWH}FieldByName('Sdate0').AsDateTime<=dm.BeginDate then   Background:=CU_SBD1 //������ �� ������ �������
                                   else Background:=CU_S1; //������ � ���� �������
                           {
                              if (quUIDWHSdate.IsNull or (quUIDWHSdate.AsDateTime<=dm.BeginDate)) then background:=CU_SBD1
                              else
                                if quUIDWHIsClosed.AsInteger=0 then    Background:=CU_IM0
                                else Background:=CU_IM1;
}
                             end;
{                               case quUIDWHIsClosed.AsInteger of
                                   0: Background:=CU_D0;
                                   1: Background:=CU_D1;
                                 end;}
                          6: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_RT0;
                                 1: Background:=CU_RT1;
                               end;
                          8: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_RO0;
                                 1: Background:=CU_RO1;
                               end;
                        end;

                 2: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                        0,1: Background:=dm2.GetDepColor({quUIDWH}FieldByName('DepId').AsInteger);
                        2: Background:=CU_SL;
                      end;

                 3: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                        0: Background:=CU_SO0;
                        1: Background:=CU_SO1;
                      end;

                 4: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                        0: Background:=CU_SR0;
                        1: Background:=CU_SR1;
                      end;
                 6: Background:=CU_AC;
                 7: Background:=CU_AO;
               end;
end;

procedure TfmUIDWH.mnitListClick(Sender: TObject);
begin
  dmServ.WHDepId := dm.SDepId;

  if (TMenuItem(Sender).Tag = -2) or (TMenuItem(Sender).Tag = -3) then
     dmReport.UIDWHSelectModif(dmReport.taUIDWHB,TWinControl(Sender).Tag>1)
  else
     dmReport.UIDWHSelectModif(dmReport.taUIDWHA,TWinControl(Sender).Tag>1);

  dmReport.FilterText:=UIDWHFilter;

  if TMenuItem(Sender).Tag<0 then
  begin
     case TMenuItem(Sender).Tag of
     -1:  dmReport.PrintLayout(art_all3);
     -2:  dmReport.PrintLayout(art_all4);
     -3:  dmReport.PrintLayout(art_all5);
     end
  end
  else
  begin
  if dm.SDepId = -1 then
     if TMenuItem(Sender).Tag=0 then dmReport.PrintLayout(art_all)
        else dmReport.PrintLayout(art_all2)
     else
     if TMenuItem(Sender).Tag=0 then dmReport.PrintLayout(art_dep)
        else dmReport.PrintLayout(art_dep2);
  end;
end;

procedure TfmUIDWH.GetSell();
var  BD, ED: TDateTime;
     d,m,y : word;
begin
  with dmCom, dm do
  begin

    BD := UIDWHBD; // ��������� ���� ������� �� ���������
    DecodeDate(BD, y, m, d); // ���������� ������ ������������� ����

    if m = 12 then
    begin
     y := y + 1;
     m := 1;
    end;
    ED := EncodeDate(y, m+1, 1)-0.00001; // �������� ���� ��������������� �� ������ ���������� ������ �� ���������


    while (ED < UIDWHED) do // ���� ED ������ ������������� �������� ����
    begin
      with quTmp do
        begin
          Close;
          if CenterDep then
             SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month ?BD, ?ED,?Fin_ED, ?USERID'
          else
             SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month_dep ?BD, ?ED,?Fin_ED, ?USERID';

          Params[0].AsTimeStamp:=DateTimeToTimeStamp(BD);
          Params[1].AsTimeStamp:=DateTimeToTimeStamp(ED);
          Params[2].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
          if UIDWHType=3 then Params[3].AsInteger:=UserDefault
          else Params[3].AsInteger:=UserId;
          ExecQuery;
          tr.CommitRetaining;
          tr1.CommitRetaining;
        end;

      BD := ED+0.00001;
      DecodeDate(BD, y, m, d);
      if m = 12 then
       ED := EncodeDate(y+1, 1, 1)-0.00001
      else
        ED := EncodeDate(y, m+1, 1)-0.00001;
    end;

    ED:= UIDWHED;

    with quTmp do
    begin
      Close;
      if CenterDep then
       SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month ?BD, ?ED,?Fin_ED, ?USERID'
      else
       SQL.Text:='EXECUTE PROCEDURE UIDWH_S_5_Month_dep ?BD, ?ED,?Fin_ED, ?USERID';
      Params[0].AsTimeStamp:=DateTimeToTimeStamp(BD);
      Params[1].AsTimeStamp:=DateTimeToTimeStamp(ED);
      Params[2].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
      if UIDWHType=3 then Params[3].AsInteger:=UserDefault
      else Params[3].AsInteger:=UserId;
      ExecQuery;
      tr.CommitRetaining;
      tr1.CommitRetaining;
    end
  end;

end;

procedure TfmUIDWH.UIDWHClean;
var a1, a, b, c, s, z  :integer;
  I: Integer;
begin

  if (quUIDWHdelete.Active) then
  quUIDWHdelete.Close;
quUIDWHdelete.Open;
quUIDWHdelete.First;
a1:=quUIDWHdelete.FieldByName('uidwhid').AsInteger; //a1 - ������ ������
quUIDWHdelete.Last;
z :=quUIDWHdelete.FieldByName('uidwhid').AsInteger;   //z - ��������� ������
c:= z-a1+1;// c - count �������
s:= c div 10; //s - ���������� ������� ��� ��������

  for I := 0 to 9  do
  begin
  if (quUIDWHdelete.Active) then
  quUIDWHdelete.Close;
  quUIDWHdelete.Open;
  quUIDWHdelete.First;
  a:=quUIDWHdelete.FieldByName('uidwhid').AsInteger;
  b:= a+s;
  quUIDWHdelete.DeleteSQL.Text:= 'delete from uidwh_t where userid = -100 and uidwhid >= '+IntToStr(a)+' and uidwhid <= '+IntToStr(b);
  quUIDWHdelete.Delete;

  end;
   dmCom.tr.CommitRetaining;
end;

procedure TfmUIDWH.PrepareData;  // ���������� ������
var i: integer;
 s:integer;
begin
  Application.Minimize;
  with dmCom, dm do
    begin
      Application.ProcessMessages;
      with quTmp do
      begin
        if UIDWHType=3 then
          begin
            SQL.Text:='EXECUTE PROCEDURE StartUIDWHCalc';
            ExecQuery;
            tr.CommitRetaining;
            ExecSQL('EXECUTE PROCEDURE CreateApplDep_Recalc ('+IntToStr(UserDefault)+')', qutmp); //�������� ������ �� ������� � �����������
            UIDWHClean;
            SQL.Text:='DELETE FROM UIDWH_T WHERE USERID='+IntToStr(UserDefault);
          end
        else
          begin
            ExecSQL('update d_rec set calcwhdate=calcwhdate+1',qutmp);
            ExecSQL('EXECUTE PROCEDURE CreateApplDep_Recalc ('+IntToStr(UserId)+')', qutmp);
            SQL.Text:='DELETE FROM UIDWH_T WHERE USERID='+IntToStr(UserId);
          end;
      end;
      UIDWHTime:=dmCom.GetServerTime;
      g1.Position:=0;
      quTmp.ExecQuery;
      tr1.CommitRetaining;
      tr.CommitRetaining;
      g1.Position:=1;

      Application.ProcessMessages;
  {      for i:=1 to 9 do
        begin
          if (i = 5) and (UIDWHType=2)  then
             GetSell  // ����� ��������� �������
          else
            with quTmp do
            begin             }
            {------------------------------------------------------------------}
            {-���� ������������� ����� �� �� ������������ ������  -}
         {      if (UIDWHType <> 3) then
               begin
                  if CenterDep then
                     SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+' ?BD, ?ED, ?USERID'
                     else
                     SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+'_dep ?BD, ?ED, ?USERID';
                  Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
                  Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
                  if UIDWHType=3 then
                     Params[2].AsInteger:=UserDefault
                     else Params[2].AsInteger:=UserId;
                  ExecQuery;
                  tr.CommitRetaining;
                  tr1.CommitRetaining;
               end        }
             {------------------------------------------------------------------}
           {    else
               begin
                  if (i <> 8) then
                  begin
                     if CenterDep then
                        SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+' ?BD, ?ED, ?USERID'
                        else
                        SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+'_dep ?BD, ?ED, ?USERID';
                     Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
                     Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
                     if UIDWHType=3 then
                        Params[2].AsInteger:=UserDefault
                        else Params[2].AsInteger:=UserId;
                     ExecQuery;
                     tr.CommitRetaining;
                     tr1.CommitRetaining;
                  end;
                end;
             end;
             g1.Progress:=i+1;
             Application.ProcessMessages;
        end;       }
     for i:=1 to 9 do
        begin
          if (i = 5) and (UIDWHType=2)  then
             GetSell  // ����� ��������� �������
          else
            with quTmp do
            begin
                  if CenterDep then
                  SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+' ?BD, ?ED, ?USERID'
                  else
                  SQL.Text:='EXECUTE PROCEDURE UIDWH_S_'+IntToStr(i)+'_dep ?BD, ?ED, ?USERID';
                  Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
                  Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
     //             showmessage(Params[0].AsString + ' , ' + Params[1].AsString);
                  if UIDWHType=3 then
                     Params[2].AsInteger:=UserDefault
                     else Params[2].AsInteger:=UserId;
                     ExecQuery;
                  tr.CommitRetaining;
             end;
             g1.Position:=i+1;
             Application.ProcessMessages;
        end;

        if CenterDep then
        begin
          with quTmp do
          begin
            SQL.Text:='EXECUTE PROCEDURE UIDWH_S_END ?BD, ?ED, ?USERID';
            Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
            Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);

            if UIDWHType=3 then Params[2].AsInteger:=UserDefault
            else Params[2].AsInteger:=UserId;
            ExecQuery;
            tr.CommitRetaining;
            tr1.CommitRetaining;
          end;
        end;
        g1.Position:=i+1;

      with quTmp do
        begin
          if UIDWHType=3 then
            begin
              SQL.Text:='EXECUTE PROCEDURE EndUIDWHCalc '+IntToStr(UserId);
            end
          else
            begin
              SQL.Text:='UPDATE D_EMP SET UIDWHBD=?BD, UIDWHED=?ED, UIDWHDATE=''TODAY''  WHERE D_EMPID=?USERID';
              Params[0].AsTimeStamp:=DateTimeToTimeStamp(UIDWHBD);
              Params[1].AsTimeStamp:=DateTimeToTimeStamp(UIDWHED);
              Params[2].AsInteger:=UserId;
            end;

          ExecQuery;
          tr.CommitRetaining;
          tr1.CommitRetaining;

          g1.Position:=11;
        end;
        if UIDWHType<>3 then ExecSQL('update d_rec set calcwhdate=calcwhdate-1',qutmp);
       with dm.NoCloseRetAct do
       begin
         begin
           ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(UIDWHBD);
           ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(UIDWHED);
           SetDepFilter(NoCloseRetAct.Params, SDepId);
         end;
       end;
      if (g1.Position <11) then g1.Position:=11;
    end;
 Application.Restore;
end;

procedure TfmUIDWH.SpeedItem3Click(Sender: TObject);
begin
  with dmCom, dm do
    begin
       Art2Id:=dsUIDWH.DataSet.FieldValues['Art2Id'];
       Art2:=dsUIDWH.DataSet.FieldValues['Art2'];
       dsUIDWH.DataSet.Tag:=dsUIDWH.DataSet.FieldValues['UID'];
       ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);
    end;
end;


procedure TfmUIDWH.siFindClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmUIDFind, Self, TForm(fmUIDFind), True, False);
end;

function TfmUIDWH.UIDWHFilter:string;
begin
 Result:='�������: ';
 if (dmCom.FilterArt<>'') then result:=result+dmCom.FilterArt+'; '
  else result:=result+'���; ';
 result:=result+'��������: '+StrMultiContr(chlbMat)+'; ';
 result:=result+'�����:    '+StrMultiContr(chlbGood)+'; ';
 result:=result+'�������.: '+StrMultiContr(chlbProd)+'; ';
 result:=result+'�������.: '+StrMultiContr(chlbSup)+'; ';
 result:=result+'����.1: '+StrMultiContr(chlbnote1)+'; ';
 result:=result+'����.2: '+StrMultiContr(chlbnote2)+'; ';
 result:=result+'���.1: '+StrMultiContr(chlbatt1)+'; ';
 result:=result+'���.2: '+StrMultiContr(chlbatt2)+'; ';
 result:=result+'������: '+StrMultiContr(chlbsz)+'; ';
end;

function TfmUIDWH.StrMultiContr(chlb:TCheckListBox):string;
var i:integer; st:string;
begin
  st:='';
  for i:=0 to chlb.Items.Count-1 do
   if chlb.Checked[i] then
       st:=st+TNodeData(chlb.Items.Objects[I]).Name+',';
  delete(st,length(st),1);
  result:=st;
end;
{******************************************************************************}
{************* ������� ������� ��� ������� ������ *****************************}
procedure TfmUIDWH.ShowPeriod;
begin
  case dm.UIDWHType of
      1: laPeriod.Caption:='������ ������';
      2: laPeriod.Caption:='� '+DateToStr(dm.UIDWHBD) + ' �� ' +DateToStr(dm.UIDWHED);
      3: with dm, quTmp do
           begin
             SQL.Text:='SELECT UIDWHDATE FROM D_REC';
             ExecQuery;
             laPeriod.Caption:='����� �� '+DateTimeToStr(quTmp.Fields[0].AsDateTime);
             Close;
           end;
      4: laPeriod.Caption:='����� �� ���� '+DateToStr(dm.UIDWHED);
    end;
end;
{******************************************************************************}

procedure TfmUIDWH.N3Click(Sender: TObject);
begin
  dm.FindArt:='';
  ShowAndFreeForm(TfmHistItem, Self, TForm(fmHistItem), True, False);
end;

procedure TfmUIDWH.miUIDHistClick(Sender: TObject);
begin
  with dm do
    UID2Find:=dsUIDWH.DataSet.FieldValues['UID'];// quUIDWHUID.AsInteger;
  ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
end;

procedure TfmUIDWH.miUIDPHistClick(Sender: TObject);
begin
  with dm do
    UID2Find:=dsUIDWH.DataSet.FieldValues['UID']; // quUIDWHUID.AsInteger;
  ShowAndFreeForm(TfmUIDPHist, Self, TForm(fmUIDPHist), True, False);
end;

procedure TfmUIDWH.SpeedItem6Click(Sender: TObject);
begin
  with dm do
    if ShowFilter(UIDWHSupFlt, UIDWHSupId) then
      try
        Screen.Cursor:=crSQLWait;
        dsUIDWH.DataSet{quUIDWH}.Filtered:=UIDWHSupFlt;
      finally
        Screen.Cursor:=crDefault;
      end;
end;

procedure TfmUIDWH.N6Click(Sender: TObject);
begin
  dmServ.WHDepId := dm.SDepId;
//  dm.AllUIDWH:=TWinControl(Sender).Tag=3;
 if (TWinControl(Sender).Tag=2){and(dm.SDepId>0)} then
  begin
    dmReport.UIDWHSelectModif(dmReport.taUIDWHAu,TWinControl(Sender).Tag>1);
    dmReport.PrintLayout(art_dep3);
  end
 else
  begin
  dmReport.UIDWHSelectModif(dmReport.taUIDWH,TWinControl(Sender).Tag=1);
  if dm.SDepId = -1 then
     dmReport.PrintLayout(uid_all)
  else
    dmReport.PrintLayout(uid_dep);
  end;

  (*
      if TMenuItem(Sender).Tag=0 then dmReport.PrintLayout(uid_dep)
                                 else dmReport.PrintLayout(uid_dep2);
  *)
end;

procedure TfmUIDWH.btnDoClick(Sender: TObject);
var l1,l2,l3,l4,l5,l6,l7,l8, l9, l10:boolean;
begin
  Screen.Cursor:=crSQLWait;
  Application.Minimize;
  dmCom.FilterArt := edArt.Text;
  dm.FilterArt2 := edArt2.Text;  
  l1:=CheckMultiContr(chlbProd,dm.SD_CompId);
  if l1 then dmCom.D_CompId:=-1;
  l2:=CheckMultiContr(chlbSup,dm.SD_supId);
  if l2 then dmCom.D_SupId:=-1;
  l3:=CheckMultiContr(chlbMat,dm.SD_MatId,true);
  if l3 then dmCom.D_MatId:='*';
  l4:=CheckMultiContr (chlbGood,dm.SD_GoodId,true);
  if l4 then dmCom.D_GoodId:='*';
  l5:=CheckMultiContr(chlbnote1,dm.SD_Note1);
  if l5 then dmCom.D_Note1:=-1;
  l6:=CheckMultiContr(chlbnote2,dm.SD_Note2);
  if l6 then dmCom.D_Note2:=-1;
  l7:=CheckMultiContr(chlbatt1,dm.SD_Att1);
  if l7 then dmCom.D_Att1ID:=ATT1_DICT_ROOT;
  l8:=CheckMultiContr(chlbatt2,dm.SD_Att2);
  if l8 then dmCom.D_Att2ID:=ATT2_DICT_ROOT;
  l9:=CheckMultiContr(chlbsz,dm.uidwhsz, true);
  if l9 then dmcom.UIDWHSZ_:='*';

  l10 := CheckMultiContr(chlbIns, dm.SD_InsID, True);
  if l10 then dmCom.D_InsId := '*';

  dm.lMultiSelect:=l1 or l2 or l3 or l4 or l5 or l6 or l7 or l8 or l9 or l10;
{  CheckCalcing;}

  ReOpenDataSets([dm.dsUIDWH.DataSet{dm.quUIDWH}]);

//  ShowPeriod;
  dm.SetVisEnabled(plTotal,False);
  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbSup,False);
  dm.SetVisEnabled(chlbProd,False);
  dm.SetVisEnabled(chlbnote1,False);
  dm.SetVisEnabled(chlbnote2,False);
  dm.SetVisEnabled(chlbatt1,False);
  dm.SetVisEnabled(chlbatt2,False);
  dm.SetVisEnabled(chlbsz,False);
  dm.SetVisEnabled(chlbIns, False);
  Application.Restore;
  Screen.Cursor:=crDefault;
end;

function TfmUIDWH.CheckMultiContr(chlb:TCheckListBox;var st:string; lCanModif:boolean=false):boolean;
var i:integer; fb:boolean; s:string;
begin
  Result:=False;
  st:='';s:='';
  if chlb.Checked[0] then
   begin
    for i:=1 to chlb.Items.Count-1 do
      chlb.Checked[i]:=False;
    st:='';
   end
  else
   begin
    fb:=false;
    for i:=0 to chlb.Items.Count-1 do
     if chlb.Checked[i] then
     begin
       if fb then Result:=true;
       fb:=true;
       s:=TNodeData(chlb.Items.Objects[I]).Code;
       if lCanModif then s:=#39+s+#39; //s:='"'+s+'"';
       st:=st+s+',';
     //  st:=st+TNodeData(chlb.Items.Objects[I]).Code+',';
     end;
   delete(st,length(st),1);
   if pos(',',st)<=0 then st:='';
   end;
end;

procedure TfmUIDWH.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  UID : integer;
begin
  case key of
    VK_RETURN :
      try
        Screen.Cursor := crSQLWait;
        UID := StrToIntDef(edUID.Text, -1);
        if UID <> -1 then
        begin
          edUID.Glyph.Handle := LoadBitmap(hInstance, 'STOP');
          FStopUIDSearch := False;
          FSearchingUID := True;
          if not LocateF(dm.dsUIDWH.DataSet{dm.quUIDWH}, 'UID', UID, [], False, FStopUIDSearchFunc)
          then if not FStopUIDSearch then MessageDialog('������� �� �������', mtInformation, [mbOk], 0);
        end;
      finally
        edUID.Glyph.Handle := LoadBitmap(hInstance, 'FIND');
        FSearchingUID := False;
        Screen.Cursor := crDefault;
      end;
    VK_ESCAPE : FStopUIDSearch := True;
  end;
end;

function TfmUIDWH.StopSearch: boolean;
begin
  Result := FStopUIDSearch;
end;

procedure TfmUIDWH.edUIDButtonClick(Sender: TObject);
var
  Key : word;
begin
  if FSearchingUID then FStopUIDSearch := True
  else begin
    FStopUIDSearch := False;
    Key := VK_RETURN;
    edUIDKeyDown(Sender, Key, []);
  end;
end;

procedure TfmUIDWH.edUIDKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN : begin
       ActiveControl := grUIDWH;
       Activecontrol.SetFocus;
    end;
  end;
end;

procedure TfmUIDWH.Excel1Click(Sender: TObject);
var
  i, j, k:integer;
  sellcost,sinvcost,allcost:real;
  PayTypeName: String;
  AgentName: String;
  AgentType: String;
  SelfType: String;
  Columns: Smallint;
  ContractNumber: Variant;
  ContractType: Smallint;
  SummSInvCost: real;
begin

XLSExportFile1.Workbook.Create(1);

dmReport.UIDWHSelectModif(dmReport.taUIDWHB, false);

if (sd1.Execute) then
begin
//������ �� ������ � �� ��������� �������
  dmServ.WHDepId := dm.SDepId;
  With dm, dmreport.taUIDWHB do
  begin
   if lSellSelect then  SelectSQL[16]:=' and u.T=2 '
       else if (not AllUIDWH) and (not lSRetSelect) and (not lOSinvSelect) then  SelectSQL[16]:=' and ((u.T between -1 and 1 ) or (u.T=5) or (u.T=7))'
        else if lSRetSelect then  SelectSQL[16]:=' and (u.T=4 or u.T=5) '
         else if lOSinvSelect then   SelectSQL[16]:=' and (u.T=-2) '
           else SelectSQL[16]:=' ';
   end;
//------------------------------
//������������ �������� �����
  allcost:=0;
  SummSInvCost := 0;

  PayTypeName := '';

  AgentType := '���������';
  SelfType := '����������';
  Columns := 11;
  ContractType := 1;

  case dm.PayType of

             3:
               begin
                PayTypeName := '����������';
                ContractType := 3;
               end;

    1000000005: PayTypeName := '������������';

  end;

  if dm.Comission = 1 then
  begin
    PayTypeName := '��������';
    AgentType := '��������';
    Selftype := '�����������';
    Columns := 12;
    ContractType := 2;
  end;

  AgentName := dmCom.db.QueryValue('select name from d_comp where d_compid = :CompID', 0, [dmCom.D_SupID]);

  ContractNumber := dmCom.db.QueryValue('select number ' +
                                        'from contract ' +
                                        'where Company$ID = :Company$ID and ' +
                                        'Agent$ID = :Agent$ID and '+
                                        'Contract$Type = :Contract$Type and ' +
                                        'Is$Delete = 0 and ' +
                                        '((Is$Unlimited = 1) or (Contract$End$Date >= current_date))', 0, [1, dmCom.D_SupID, ContractType]);

 //------------------------------------------
  XLSExportFile1.Workbook.Sheets[0].Cells[1,0].Value:='�����������';
  XLSExportFile1.Workbook.Sheets[0].Cells[1,3].Value:='��� "��������� ����" ';
  XLSExportFile1.Workbook.Sheets[0].Cells[2,0].Value:='����� �� ������ c '+DateToStr(dm.UIDWHBD)+ ' �� ' + DateToStr(dm.UIDWHED);
  XLSExportFile1.Workbook.Sheets[0].Cells[3,0].Value:='�� �������� ' + PayTypeName + ' �'+ VarToStr(ContractNumber);
  XLSExportFile1.Workbook.Sheets[0].Cells[4,0].Value:= AgentType + ' '+ AgentName;
  XLSExportFile1.Workbook.Sheets[0].Cells[5,0].Value:='�';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,1].Value:='������������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,2].Value:='�������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,3].Value:='�������2';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,4].Value:='�������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,5].Value:='������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,6].Value:='���-��';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,7].Value:='���';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,8].Value:='���� ��������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,9].Value:='���� �������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,10].Value:='����� ��������';
  XLSExportFile1.Workbook.Sheets[0].Cells[5,11].Value:='����� ����������';

  PayTypeName := '���������';

  if dm.PayType = 3 then
  begin
    PayTypeName := '��������� ����������';
  end;

  if dm.Comission = 1 then
  begin
    PayTypeName := '������������ ��������������';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,12].Value := PayTypeName;
  end;

for i := 0 to 4 do
  begin
   XLSExportFile1.Workbook.Sheets[0].Rows[i].FontBold:=true;
   XLSExportFile1.Workbook.Sheets[0].Rows[i].HAlign:=xlHAlignLeft;
  end;

  XLSExportFile1.Workbook.Sheets[0].Rows[5].FontBold:=true;
  XLSExportFile1.Workbook.Sheets[0].Rows[5].HAlign:=xlHAlignCenter;
  XLSExportFile1.Workbook.Sheets[0].Rows[5].Wrap:=true;
  XLSExportFile1.Workbook.Sheets[0].Columns[0].Width:=5;
  XLSExportFile1.Workbook.Sheets[0].Columns[0].Width:=5;
  XLSExportFile1.Workbook.Sheets[0].Columns[1].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[3].Width:=20;
  XLSExportFile1.Workbook.Sheets[0].Columns[4].Width:=10;
  XLSExportFile1.Workbook.Sheets[0].Columns[5].Width:=8;
  XLSExportFile1.Workbook.Sheets[0].Columns[6].Width:=8;
  XLSExportFile1.Workbook.Sheets[0].Columns[8].Width:=10;
  XLSExportFile1.Workbook.Sheets[0].Columns[9].Width:=10;
  XLSExportFile1.Workbook.Sheets[0].Columns[10].Width:=18;
  XLSExportFile1.Workbook.Sheets[0].Columns[11].Width:=18;

  if dm.Comission = 1 then
  begin
    XLSExportFile1.Workbook.Sheets[0].Columns[12].Width:=20;
  end;

  i:=6;

  dmreport.taUIDWHB.Active:=true;

  dmreport.taUIDWHB.Open;

  dmreport.taUIDWHB.First;

While not dmreport.taUIDWHB.Eof do
      begin
          XLSExportFile1.Workbook.Sheets[0].Cells[i,0].Value:=dmreport.taUIDWHB.RecNo;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,1].Value:=dmreport.taUIDWHBBODYGOOD.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,2].Value:=dmreport.taUIDWHBART.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,3].Value:=dmreport.taUIDWHBART2.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,4].Value:=dmreport.taUIDWHBBODYDEPARTMENT.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,5].Value:=dmreport.taUIDWHBBODYSIZE.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,6].Value:=dmreport.taUIDWHBQ.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,7].Value:=dmreport.taUIDWHBW.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,8].Value:=dmreport.taUIDWHBSPRICE.Value;
          XLSExportFile1.Workbook.Sheets[0].Cells[i,9].Value:=dmreport.taUIDWHBPRICE.Value;
          if dmreport.taUIDWHBUNITID.AsInteger = 0 then
             sellcost:= dmreport.taUIDWHBPRICE.Value*dmreport.taUIDWHBQ.Value//���������
          else                                                               //����������
             sellcost:= dmreport.taUIDWHBPRICE.Value*dmreport.taUIDWHBW.Value;
            XLSExportFile1.Workbook.Sheets[0].Cells[i,11].value:=sellcost;
         if dmreport.taUIDWHBUNITID.AsInteger = 0 then
            sinvcost := dmreport.taUIDWHBSPRICE.Value*dmreport.taUIDWHBQ.Value//���������
          else                                                                //���������
            sinvcost := dmreport.taUIDWHBSPRICE.Value*dmreport.taUIDWHBW.Value;
           XLSExportFile1.Workbook.Sheets[0].Cells[i,10].value:=sinvcost;

          if dm.Comission = 1 then
          begin
            XLSExportFile1.Workbook.Sheets[0].Cells[i,12].value:=sellcost-sinvcost;
          end;

          SummSinvCost := SummSInvCost + SInvCost;

          allcost:=allcost+(sellcost-sinvcost);
       dmreport.taUIDWHB.Next;
       i:=i+1;
      end;
   for i := 5 to (5+dmreport.taUIDWHB.RecordCount) do
      begin
       for j := 0 to Columns do
        begin
         XLSExportFile1.Workbook.Sheets[0].Cells[i,j].BorderColorRGB[xlBorderAll]:=xlcolorblack;
         XLSExportFile1.Workbook.Sheets[0].Cells[i,j].BorderStyle[xlBorderAll]:=bsThin;
         if (j>=7) then XLSExportFile1.Workbook.Sheets[0].Cells[i,j].FormatStringIndex:=29;
        end;

       end;
      k := 5+dmreport.taUIDWHB.RecordCount+1;

      XLSExportFile1.Workbook.Sheets[0].Cells[k,1].Value:='�����';
      XLSExportFile1.Workbook.Sheets[0].Cells[k,10].Formula:='SUM(K7:K'+intToStr(k)+')';
      XLSExportFile1.Workbook.Sheets[0].Cells[k,11].Formula:='SUM(L7:L'+intToStr(k)+')';

      if dm.Comission = 1 then
      begin
        XLSExportFile1.Workbook.Sheets[0].Cells[k,12].Formula:='SUM(M7:M'+intToStr(k)+')';
      end;

       XLSExportFile1.Workbook.Sheets[0].Rows[k].FormatStringIndex:=29;
   for j := 10 to Columns do
        begin
         XLSExportFile1.Workbook.Sheets[0].Cells[k,j].BorderColorRGB[xlBorderAll]:=xlcolorblack;
         XLSExportFile1.Workbook.Sheets[0].Cells[k,j].BorderStyle[xlBorderAll]:=bsthin;
        end;
  XLSExportFile1.Workbook.Sheets[0].Cells[i,1].BorderColorRGB[xlBorderAll]:=xlcolorblack;
  XLSExportFile1.Workbook.Sheets[0].Cells[i,1].BorderStyle[xlBorderAll]:=bsThin;

  if (dm.PayType = 3) then
  begin
    AllCost := SummSinvCost;
  end;

  XLSExportFile1.Workbook.Sheets[0].Rows[i].FontBold:=true;
  XLSExportFile1.Workbook.Sheets[0].Cells[i+2,0].Value:= PayTypeName + ' �� ������ c '+DateToStr(dm.UIDWHBD)+ ' �� ' + DateToStr(dm.UIDWHED)+' '+ GetCapitalsMoney(allcost)+'��� ���';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+4,0].Value:='������ ��������� ��������� � � ����. ' + AgentType + ' ��������� �� ������, �������� � ������ �������� ����� �� �����.';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+5,0].Value:= SelfType + '______________________        ' + AgentType + '_____________________';
  XLSExportFile1.SaveToFile(sd1.FileName);
  XLSExportFile1.Workbook.Clear;
  MessageDlg('������� �������� ������� � ���� ' + sd1.FileName, mtInformation, [mbOk], 0);
  dmreport.taUIDWHB.Active:=false;
  dmreport.taUIDWHB.Close;
 end;
end;



procedure TfmUIDWH.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//var
//l: integer;
  begin
  case key of
    VK_RETURN :
       try
         if (edArt.GetTextLen<19) then
         begin
         Screen.Cursor := crSQLWait;
         dm.ReopenUidWH:=true;
         dmCom.FilterArt := edArt.Text;
         dm.FilterArt2 := edArt2.Text;
         CheckCalcing;
         ReOpenDataSets([dm.dsUIDWH.DataSet{dm.quUIDWH}]);
         ShowPeriod;
         dm.SetVisEnabled(plTotal,False);
         end
         else ShowMessage('����� ���� �� ������ ��������� 18 ��������! ���������� ������ ������� ���������� ��������.');
       finally
         Screen.Cursor := crDefault;
       end;
  end;
end;

procedure TfmUIDWH.edArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_DOWN : begin
      ActiveControl := grUIDWH;
      ActiveControl.SetFocus;
    end;
  end;
end;

procedure TfmUIDWH.edArtButtonClick(Sender: TObject);
var
  Key : word;
begin
 Key := VK_RETURN;
   edArtKeyDown(Sender, Key, []);
end;

function TfmUIDWH.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      eXit;
    end;
end;

procedure TfmUIDWH.CheckCalcing;
var i: integer;
begin
  with dm, quTmp do
  if (UIDWHType=3) then
    begin
      SQL.Text:='SELECT UIDWHCALC FROM D_REC';
      ExecQuery;
      i:=Fields[0].AsInteger;
      Close;
      if i>0 then
       begin

        raise Exception.Create('UIDWHCALC');
       end;
    end;
end;

procedure TfmUIDWH.SpeedItem7Click(Sender: TObject);
begin
//
  dm.SetVisEnabled(chlbSup,True);
  chlbSup.SetFocus;
end;

procedure TfmUIDWH.chlbProdExit(Sender: TObject);
begin
   dm.SetVisEnabled(TWinControl(sender),False);
end;

function TfmUIDWH.ChlbClickCheck(chlb:TCheckListBox;var st:string):variant;
var
  i:integer;
begin
  i:=chlb.ItemIndex;

  WHILE (i>0) and (not chlb.Checked[i]) do dec(i);

  if chlb.Checked[i] then
    begin
     st:=chlb.Items[i];
     if (i<>0) then     chlb.Checked[0]:=false;
    end
  else
    begin
     i:=chlb.Items.Count-1;
     WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
     st:=chlb.Items[i];
     chlb.Checked[i]:=true;
    end;

   Result:=TNodeData(chlb.Items.Objects[i]).Code;
end;

procedure TfmUIDWH.chlbProdClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_CompId:=ChlbClickCheck(chlbProd,s);
 SpeedItem8.BtnCaption:=s;
 //ChlbClickCheck(chlbProd,dmCom.D_CompId);
end;

procedure TfmUIDWH.chlbSupClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_SupId:=ChlbClickCheck(chlbSup,s);
 SpeedItem7.BtnCaption:=s;
{
begin
 if chlbSup.Checked[chlbSup.ItemIndex] then
  begin
   SpeedItem7.BtnCaption:=chlbSup.Items[chlbSup.ItemIndex];
   dmCom.D_SupId :=TNodeData(chlbSup.Items.Objects[chlbSup.ItemIndex]).Code;
  end;}
end;

procedure TfmUIDWH.chlbMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_MatId:=ChlbClickCheck(chlbMat,s);
 SpeedItem10.BtnCaption:=s;
end;

procedure TfmUIDWH.chlbGoodClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GoodId:=ChlbClickCheck(chlbGood,s);
 SpeedItem9.BtnCaption:=s;
end;

procedure TfmUIDWH.SpeedItem9Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGood,True);
  chlbGood.SetFocus;
end;

procedure TfmUIDWH.SpeedItem10Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbMat,True);
  chlbMat.SetFocus;
end;

procedure TfmUIDWH.spItogClick(Sender: TObject);
var b: TBookMark;
    st:string;
    DefaultSeparator:char;
begin
  b := nil;
  DefaultSeparator := ' ';
  Application.ProcessMessages;
  if (not dm.lMultiSelect) and (not dm.FilterArtb) and (not dm.IFilterCost) and (not dm.IFilterW) then
  begin
     ReOpenDataSets([dmServ.quUIDWH_T]);

{     showmessage('comission  ' + dmServ.quUIDWH_T.Params[32].AsString);
     showmessage('paytype  ' + dmServ.quUIDWH_T.Params[33].AsString);
     showmessage('t1  ' + dmServ.quUIDWH_T.Params[13].AsString);
     showmessage('t2  ' + dmServ.quUIDWH_T.Params[14].AsString);
     showmessage('t3  ' + dmServ.quUIDWH_T.Params[15].AsString);
     showmessage('t4  ' + dmServ.quUIDWH_T.Params[16].AsString);
     showmessage('t5  ' + dmServ.quUIDWH_T.Params[17].AsString);
     showmessage('t6  ' + dmServ.quUIDWH_T.Params[18].AsString);}

     grTotal.datasource:=dmServ.dsrUIDWH_T;
  end
  else
    begin
      with dm.quTmp do
      begin
        SQL.Text:='DELETE FROM T_UIDWH WHERE GEN_USER='+IntToStr(FGenTUIDWH);
        ExecQuery;
        dmCom.tr.CommitRetaining;
      end;
      with dm.dsUIDWH.DataSet do
        try
          Screen.Cursor:=crSQLWait;
          b:= GetBookmark;
          DisableControls;
          First;
          DefaultSeparator := DecimalSeparator;
          DecimalSeparator:='.';
          while NOT EOF do
            begin
             if (dm.AllUIDWH or (dm.lSellSelect and (FieldByName('T').AsInteger=2))
                 or((FieldByName('T').AsInteger>-2) and(FieldByName('T').AsInteger<2) or (FieldByName('T').AsInteger=5)
                 or (FieldByName('T').AsInteger=7) or (FieldByName('T').AsInteger=8))
                 or (dm.lSRetSelect and (FieldByName('T').AsInteger>=4) and (FieldByName('T').AsInteger<=5))
                 or (dm.lOSinvSelect and (FieldByName('T').AsInteger=-2))) then
              begin
                dm.quTmp.Close;
                st:='';
                if Fieldbyname('W').isNull then st:=st+', 0' else st:=st+', '+Fieldbyname('W').asstring;
                if Fieldbyname('COST1').isNull then st:=st+', 0' else st:=st+', '+Fieldbyname('COST1').asstring;
                if Fieldbyname('COST').isNull then st:=st+', 0' else st:=st+', '+Fieldbyname('COST').asstring;
                if Fieldbyname('COSTINV').isNull then st:=st+', 0' else st:=st+', '+Fieldbyname('COSTINV').asstring;
                dm.quTmp.SQL.Text:='EXECUTE PROCEDURE ADD_TUIDWH '+inttostr(FGenTUIDWH)+', '+
                Fieldbyname('DEPID').asstring+', '+Fieldbyname('D_ARTID').asstring+st;
{               dm.quTmp.Params[0].AsBcd:=FGenTUIDWH;
                dm.quTmp.Params[1].AsInteger:=Fieldbyname('DEPID').aSiNTEGER;
                dm.quTmp.Params[2].AsInteger:=Fieldbyname('D_ARTID').aSiNTEGER;
                dm.quTmp.Params[3].AsFloat:=Fieldbyname('W').asFloat;
                dm.quTmp.Params[4].AsFloat:=Fieldbyname('COST1').asFloat;
                dm.quTmp.Params[5].AsFloat:=Fieldbyname('COST').asFloat;
                dm.quTmp.Params[6].AsFloat:=Fieldbyname('COSTINV').asFloat;
 }              dm.quTmp.ExecQuery;
                dmCom.tr.CommitRetaining;
              end;
              Next;
            end;
        finally
          DecimalSeparator := DefaultSeparator;
          GotoBookmark(b);
          FreeBookmark(b);
          EnableControls;
          Screen.Cursor:=crDefault;
          Application.ProcessMessages;
        end;
      dm.quTempUIDWH.tag:=FGenTUIDWH;
      ReOpenDataSets([dm.quTempUIDWH,dmCom.quDep]);
      grTotal.datasource:=dm.dsTempUIDWH;
    end;
    dm.SetVisEnabled(plTotal,True);
    if CenterDep then
    begin
     if NRealCost.Checked then grTotal.FieldColumns['STP3'].Visible:=true
     else grTotal.FieldColumns['STP3'].Visible:=false;
    end
    else grTotal.FieldColumns['STP3'].Visible:=true;
    
    Screen.Cursor:=crDefault;
end;

procedure TfmUIDWH.chbMaxPriceClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    Application.Minimize;
    if chbMaxPrice.checked then
      begin
        CloseDataSets([dm.quUIDWH]);      
        ReOpenDataSets([dm.quMaxUIDWH]);
        dm.dsUIDWH.DataSet:=dm.quMaxUIDWH;
      end
    else
      begin
        CloseDataSets([dm.quMaxUIDWH]);
        ReOpenDataSets([dm.quUIDWH]);
        dm.dsUIDWH.DataSet:=dm.quUIDWH;
      end
  finally
    Application.Restore;
    Screen.Cursor:=crDefault;    
  end;
end;

procedure TfmUIDWH.nHArtClick(Sender: TObject);
begin
 with dm do
    FindArt:=dsUIDWH.DataSet.FieldValues['Art']; //quUIDWHArt.AsString;
 ShowAndFreeForm(TfmHistItem, Self, TForm(fmHistItem), True, False);
end;

procedure TfmUIDWH.udpDataReceived(Sender: TComponent;
  NumberBytes: Integer; FromIP: String; Port: Integer);
var ms: TMemoryStream;
    s: String;
begin {
  ms:=TMemoryStream.Create;
  try
    udp.ReadStream(ms);
    SetLength(s, NumberBytes);
    ms.Read(s[1], NumberBytes);
    if (EXtractWord(1, s, [':'])=UpperCase(dmCom.db.DatabaseName)) and (EXtractWord(2, s, [':'])=IntToSTr(SelfDepId)) and (EXtractWord(4, s, [':'])='UIDWHCALC') then
      if EXtractWord(3, s, [':'])<>IntToStr(dmCom.UserId) then
      MessageDialog('���������� ��������� ������ ������ ������������� ���������',
         mtInformation, [mbOK], 0);
  finally
    ms.Free;
  end; }
end;

procedure TfmUIDWH.dgeNGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
var i:integer;
begin
  AFont.Color:=clWindowText;
  with dm.dsUIDWH.DataSet , Column do
  if Field<>NIL then
          if Field.FieldName='DEP' then Background:=FieldByName('Color').AsInteger
          else if (Field.FieldName='APPL_Q') then exit
          else if (Field.FieldName='APPLDEP_Q') then
          begin
           if dm.dsUIDWH.DataSet=dm.quUIDWH then
           begin
            if not dm.quUIDWHUID.IsNull then i:=dm.quUIDWHUID.AsInteger else i:=-1;
           end
           else
           begin
            if not dm.quMaxUIDWHUID.IsNull then i:=dm.quMaxUIDWHUID.AsInteger else i:=-1;
           end;
           if i<>-1 then
           with dm, qutmp do
           begin
            close;
            sql.Text:='select uid from appldep_item where uid='+string(dm.dsUIDWH.DataSet.FieldValues['UID']);
            ExecQuery;
            if not Fields[0].IsNull then Background:=clYellow else Background:=clInfoBk;
            Transaction.CommitRetaining;
            close;
           end;
           exit
          end
          else if (Field.FieldName='NAMEDEPFROM') and
                  (not FieldByName('NAMEDEPFROM').IsNull) then
                    Background:=FieldbyName('colordf').Asinteger//dm.quUIDWHColor.AsInteger
          else if (Field.FieldName='ART2') or
                  (Field.FieldName='SZ') then Background:=clBtnFace
          else if (Field.FieldName='PRICE') or
                  (Field.FieldName='COST') or
                  (Field.FieldName='SPRICE0') or
                  (Field.FieldName='PRICEINV') or
                  (Field.FieldName='Tolling$Price') or
                  (Field.FieldName='COSTINV') then Background:=clAqua
          else
            with dm do
              case FieldByName('T').AsInteger of
//              case dm.quUIDWHT.AsInteger of
                   0: case {quUIDWH}FieldByName('IType').AsInteger of
                          2: if {quUIDWH}FieldByName('IsClosed').AsInteger=0 then Background:=CU_IM0 //�� ��������
                             else begin
                              if  {quUIDWH}FieldByName('Sdate0').AsDateTime<=dm.BeginDate then   Background:=CU_SBD1
                              else  Background:=CU_S1;
                             end;
                          5: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                               0: Background:=CU_SBD0;
                               1: Background:=CU_SBD1;
                             end;
                          9: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                              0: Background:=CU_RO;
                              1: Background:=CU_RC;
                             end;
                          10: Background:=CU_SI;
                          else  Background:=CU_R;
                        end;

                   -2,-1,1: case {quUIDWH}FieldByName('IType').AsInteger of
                          1: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_S0;
                                 1: Background:=CU_S1;
                               end;
                          2: begin
                           {�����.�����������}
                              if {quUIDWH}FieldByName('IsClosed').AsInteger=0 then    Background:=CU_IM0 //�� ��������
                              else if  {quUIDWH}FieldByName('Sdate0').AsDateTime<=dm.BeginDate then   Background:=CU_SBD1 //������ �� ������ �������
                                   else Background:=CU_S1; //������ � ���� �������
                             end;
                          6: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_RT0;
                                 1: Background:=CU_RT1;
                               end;
                          8: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                                 0: Background:=CU_RO0;
                                 1: Background:=CU_RO1;
                               end;
                          9: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                              0: Background:=CU_RO;
                              1: Background:=CU_RC;
                             end;
                          10: Background:=CU_SI;
                        end;

                 2: case {quUIDWH}FieldByName('IsClosed').AsInteger of
                        0,1: Background:=dm2.GetDepColor({quUIDWH}FieldByName('DepId').AsInteger);
                        2: Background:=CU_SL;
                      end;

                 3: Background:=CU_SO1;
                 4: Background:=CU_SR1;
                 5: Background:=CU_SR0;
                 6: Background:=CU_AC;
                 7: Background:=CU_AO;
                 8: Background:=CU_SO0;
               end;
end;

procedure TfmUIDWH.dgeuuGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
if Column.Field<>NIL then
  Background:=grTotal.DataSource.DataSet.FieldByName('COLOR').AsInteger;
end;

procedure TfmUIDWH.acPrintExecute(Sender: TObject);
begin
 if grUIDWH.Focused then
   prdbe1.DBGridEh:=grUIDWH
  else
 prdbe1.DBGridEh:=grTotal;

 VirtualPrinter.Orientation := poLandscape;

 prdbe1.Print;
end;


procedure TfmUIDWH.SpeedItem6Click0(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      LogOperationID:=dm3.insert_operation('���������� �����������',LogOprIdForm);
      b:=GetBit(dmCom.EditRefBook, 4) and CenterDep;
      ShowAndFreeFormEnabled(TfmComp, Self, TForm(fmComp), True, False, b,
        'siHelp;siExit;tb1;spitPrint;siSort;edCode;siCategory;tb2;PageControl1;'+
        'pc1;TabSheet1;TabSheet2;TabSheet3;TabSheet4;TabSheet5;TabSheet6;TabSheet7;'+
        'Panel1;tsMol;mnitAll;st1;mnitCur;N6;N1;N2;N3;N4;N5;edCode;chAllWorkOrg;'+
        'laCompCat;Label36;Splitter1;',
        'dg1;M207IBGrid1;');
      dm3.update_operation(LogOperationID);
      tr.CommitRetaining ;
    end;
end;

procedure TfmUIDWH.chlbnote1ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Note1:=ChlbClickCheck(chlbnote1,s);
 SpeedItem13.BtnCaption:=s;
end;

procedure TfmUIDWH.chlbnote2ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Note2:=ChlbClickCheck(chlbnote2,s);
 SpeedItem14.BtnCaption:=s;
end;

procedure TfmUIDWH.SpeedItem13Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbnote1,True);
  chlbnote1.SetFocus;
end;

procedure TfmUIDWH.SpeedItem8Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbProd,True);
  chlbProd.SetFocus;
end;

procedure TfmUIDWH.SpeedItem14Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbnote2,True);
  chlbnote2.SetFocus;
end;

procedure TfmUIDWH.acInsExecute(Sender: TObject);
begin
  with dmCom, dm do
    begin
       Art2Id:=dsUIDWH.DataSet.FieldValues['Art2Id'];
       Art2:=dsUIDWH.DataSet.FieldValues['Art2'];
       dsUIDWH.DataSet{quUIDWH}.Tag:=dsUIDWH.DataSet.FieldValues['UID'];
       ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);
    end;
end;

procedure TfmUIDWH.acSumGrExecute(Sender: TObject);
begin
  ShowAndFreeForm(Tfmuidwhsumgr, Self, TForm(fmuidwhsumgr), True, False);
end;

procedure TfmUIDWH.acVHelpFieldsExecute(Sender: TObject);
begin
 grUIDWH.FieldColumns['T'].Visible:= not grUIDWH.FieldColumns['T'].Visible;
 grUIDWH.FieldColumns['ITYPE'].Visible:= not grUIDWH.FieldColumns['ITYPE'].Visible;
 grUIDWH.FieldColumns['ITYPE'].Visible:= not grUIDWH.FieldColumns['ITYPE'].Visible;
 grUIDWH.FieldColumns['ITYPE'].Visible:= not grUIDWH.FieldColumns['ITYPE'].Visible;
 grUIDWH.FieldColumns['ITYPE'].Visible:= not grUIDWH.FieldColumns['ITYPE'].Visible;  
end;

procedure TfmUIDWH.SpeedItem11Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbatt1,True);
  chlbatt1.SetFocus;
end;

procedure TfmUIDWH.SpeedItem12Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbatt2,True);
  chlbatt2.SetFocus;
end;

procedure TfmUIDWH.chlbatt2ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Att2ID:=ChlbClickCheck(chlbatt2,s);
 SpeedItem12.BtnCaption:=s;
end;

procedure TfmUIDWH.chlbatt1ClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_Att1ID:=ChlbClickCheck(chlbatt1,s);
 SpeedItem11.BtnCaption:=s;
end;

procedure TfmUIDWH.N16Click(Sender: TObject);
var LogOperationID:string;
    b:boolean;
begin
  LogOperationID:=dm3.insert_operation('���������� ���. �����',LogOprIdForm);
//  ShowAndFreeForm(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False);
  b:=GetBit(dmCom.EditRefBook, 2) and CenterDep;
  ShowAndFreeFormEnabled(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False, b,'siExit;tb1;spitPrint;siSort;','dg1;');
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDWH.acNewTextIncomeExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='�����_����';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    AssignFile(ftext, Path);
    try
     Rewrite(ftext);

     dmServ.WHDepId := dm.SDepId;
     dmReport.FilterText:=UIDWHFilter;
     dmReport.UIDWHSelectModif(dmReport.taUIDWHA,TWinControl(Sender).Tag>1);
     ReOpenDataSet(dmReport.taUIDWHA);
     with dmreport, taUidWHA do
      begin
       First;
       while not EOf do
       begin
        writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
         [taUIDWHAArt.AsString,'|', taUIDWHAArt2.AsString, '|', taUIDWHASPrice.AsFloat, '|',
          GetUNIT(taUIDWHAUNITID.AsInteger), '|', GetUNITQ(taUIDWHAUNITID.AsInteger,
          taUIDWHAQ.AsInteger,taUIDWHAW.AsFloat), '|']));
          writeln(ftext, format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',

         [taUIDWHAArt.AsString,'|', taUIDWHAArt2.AsString, '|', taUIDWHASPrice.AsFloat, '|',
          GetUNIT(taUIDWHAUNITID.AsInteger), '|', GetUNITQ(taUIDWHAUNITID.AsInteger,
          taUIDWHAQ.AsInteger,taUIDWHAW.AsFloat), '|']));


         next;
        end;
       MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
     end;
    finally
     Screen.Cursor:=crDefault;
     CloseFile(fText);
     CloseDataSet(dmReport.taUIDWHA);
    end;
   end;
end;

// ������������ ������� ����������
procedure TfmUIDWH.acCreateApplExecute(Sender: TObject);
var LogOperationID:string;
begin
 if MessageDialog('��������� ������ ������� ��� ������������ ������ ������������?', mtWarning, [mbYes, mbNo], 0)=mrNo then
 begin // ������ ������������ �� ������� ��������
    LogOperationID:=dm3.insert_operation('������������ ������',LogOprIdForm);
    PostDataSet(dm.dsUIDWH.DataSet);
    Screen.Cursor := crSQLWait;
    ExecSQL('execute procedure Create_Appl_FromUidwh ('+inttostr(dmcom.User.UserId)+',0, '+string(dm.dsUIDWH.DataSet.FieldValues['USERID_WH'])+')', dmCom.quTmp);
    dm.IsChangeAppl:=false;
    ReOpenDataSet(dm.dsUIDWH.DataSet);
    Screen.Cursor := crDefault;
    MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
    dm3.update_operation(LogOperationID);
 end
 else
 begin  // ������ ����������� � ������ ������ ��������
    LogOperationID:=dm3.insert_operation('������������ ������',LogOprIdForm);
    PostDataSet(dm.dsUIDWH.DataSet);
    Screen.Cursor := crSQLWait;
    ExecSQL('execute procedure Create_Appl_FromUidwh_Art2 ('+inttostr(dmcom.User.UserId)+',0, '+string(dm.dsUIDWH.DataSet.FieldValues['USERID_WH'])+')', dmCom.quTmp);
    dm.IsChangeAppl:=false;
    ReOpenDataSet(dm.dsUIDWH.DataSet);
    Screen.Cursor := crDefault;
    MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
    dm3.update_operation(LogOperationID);
 end;
end;

procedure TfmUIDWH.acClearApplExecute(Sender: TObject);
var LogOperationID:string;
begin
if MessageDialog('�� ������������� ������ �������� ���� ������ �����������?', mtWarning, [mbYes,mbcancel],0)=mrYes then
begin
 LogOperationID:=dm3.insert_operation('�������� ���� ������',LogOprIdForm);
 PostDataSet(dm.dsUIDWH.DataSet);
 Screen.Cursor := crSQLWait;
 if dm.dsUIDWH.DataSet=dm.quUIDWH then
 begin
  ExecSQL('update uidwh_t set appl_q=null where userid='+dm.quUIDWHUSERID_WH.AsString+
          ' and appl_q<>0 and appl_q is not null and appl_user='+inttostr(dmcom.User.UserId), dmCom.quTmp);
  ReOpenDataSet(dm.quUIDWH);
 end
 else
 begin
  ExecSQL('update uidwh_t set appl_q=null where userid='+dm.quMaxUIDWHUSERID_WH.AsString+
          ' and appl_q<>0 and appl_q is not null and appl_user='+inttostr(dmcom.User.UserId), dmCom.quTmp);
  ReOpenDataSet(dm.quMaxUIDWH);
 end;
 dm.IsChangeAppl:=false;
 ReOpenDataSet(dm.dsUIDWH.DataSet);
 Screen.Cursor := crDefault;
 MessageDialog('���� "������ �����������" �������!', mtInformation, [mbOk], 0); 
 dm3.update_operation(LogOperationID);
end;
end;

procedure TfmUIDWH.acRecalclApplExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation('�������� ������',LogOprIdForm);
 PostDataSet(dm.dsUIDWH.DataSet);
 Screen.Cursor := crSQLWait;
 ExecSQL('execute procedure Create_Appl_FromUidwh ('+inttostr(dmcom.User.UserId)+',1, '+string(dm.dsUIDWH.DataSet.FieldValues['USERID_WH']){quUIDWHUSERID_WH.AsString}+')', dmCom.quTmp);
 dm.IsChangeAppl:=false;
 ReOpenDataSet(dm.dsUIDWH.DataSet);
 Screen.Cursor := crDefault;
 MessageDialog('���������� ������ ���������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;

procedure TfmUIDWH.grUIDWHTitleBtnClick(Sender: TObject; ACol: Integer;
  Column: TColumnEh);
begin
 if (Column.Field.FieldKind = fkData) then grUIDWH.SortLocal:=false
 else grUIDWH.SortLocal:=true;
end;

procedure TfmUIDWH.acCreateApplDepExecute(Sender: TObject);
var
  LogOperationID,
  s:string;
  k:integer;
begin
s:='';
 LogOperationID:=dm3.insert_operation('������������ ������ �� �������',LogOprIdForm);
 PostDataSet(dm.dsUIDWH.DataSet);
 dm.quUIDWH.Transaction.CommitRetaining;
 Screen.Cursor := crSQLWait;
 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select nouid, k from CHECK_APPLDEPITEM ('+inttostr(dmcom.UserId)+', '+
          dm.quUIDWHUSERID_WH.asString+')';
 dm.quTmp.ExecQuery;
 k:= dm.quTmp.Fields[1].AsInteger;
 Screen.Cursor := crDefault;
 if  (trim(dm.quTmp.Fields[0].AsString))<>'' then
 begin
   if (k<=15) then s:='' else s:='...';
   If MessageDialog('�� ��������� ������� �� ����� ���� ������� ������ �� �������: '
   +trim(dm.quTmp.Fields[0].AsString)+s+
   ' ������������ ������ �� �������, ������� ��� �� ���� ��������?', mtWarning, [mbYes, mbNo], 0)=mrYes
   then
   begin
   ExecSQL('execute procedure CREATE_APPLDEP ('+inttostr(dmcom.UserId)+', '+
          dm.quUIDWHUSERID_WH.asString+')', dmCom.quTmp);
    dm.quTmp.Transaction.CommitRetaining;
 dm.quTmp.Close;
 dm.IsChangeApplDep:=false;
 Screen.Cursor := crSQLWait;
 ReOpenDataSet(dm.quUIDWH);
 Screen.Cursor := crDefault;
 MessageDialog('������ �� ������� �����������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
   end;
 end
 else
 begin
  ExecSQL('execute procedure CREATE_APPLDEP ('+inttostr(dmcom.UserId)+', '+
          dm.quUIDWHUSERID_WH.asString+')', dmCom.quTmp);
 dm.quTmp.Transaction.CommitRetaining;
 dm.quTmp.Close;
 dm.IsChangeApplDep:=false;
 Screen.Cursor := crSQLWait;
 ReOpenDataSet(dm.quUIDWH);
 Screen.Cursor := crDefault;
 MessageDialog('������ �� ������� �����������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
 end;
end;

procedure TfmUIDWH.acClearApplDepExecute(Sender: TObject);
var LogOperationID:string;
begin
if MessageDialog('�� ������������� ������ �������� ���� ������ �� �������?', mtWarning, [mbYes,mbcancel],0)=mrYes then
begin
 LogOperationID:=dm3.insert_operation('�������� ���� ������ �� �������',LogOprIdForm);
 PostDataSet(dm.dsUIDWH.DataSet);
 Screen.Cursor := crSQLWait;
 if dm.dsUIDWH.DataSet=dm.quUIDWH then
 begin
  ExecSQL('update uidwh_t set appldep_q=null where userid='+dm.quUIDWHUSERID_WH.AsString+' and '+
   'appldep_user='+inttostr(dmcom.User.UserId)+' and appldep_q is not null', dmCom.quTmp);
  ReOpenDataSet(dm.quUIDWH);
  dm.IsChangeApplDep:=false;
 end
 else
 begin
  ExecSQL('update uidwh_t set appldep_q=null where userid='+dm.quMaxUIDWHUSERID_WH.AsString+' and '+
   'appldep_user='+inttostr(dmcom.User.UserId)+' and appldep_q is not null', dmCom.quTmp);
  ReOpenDataSet(dm.quMaxUIDWH);
  dm.IsChangeApplDep:=false;
 end;
 Screen.Cursor := crDefault;
 MessageDialog('���� "������" �������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;
end;

procedure TfmUIDWH.acNewTextIncomeUpdate(Sender: TObject);
begin
 if (dm.dsUIDWH.DataSet=dm.quUIDWH) and CenterDep then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false;
end;

procedure TfmUIDWH.acNewInventoryExecute(Sender: TObject);
var LogOperationID:string;
    HID : Variant;
begin
  if(SelfDepId <> dm.SDepId)then
  begin
    MessageDialog('�������� ����� '+dmCom.Dep[SelfDepId].SName, mtWarning, [mbOk], 0);
    eXit;
  end;
  if MessageDialog('������� ����� ��������������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  dmServ.CurInventory := dmCom.GetId(8);
  try
    ExecSQL('insert into SInv(SINVID, ITYPE, DEPID, USERID, ISCLOSED) values('+IntToStr(dmServ.CurInventory)+',16,'+IntToStr(dm.SDepId)+','+
      IntToStr(dmCom.UserId)+', 0)', dmServ.quTmp);
    if CenterDep then
    begin
      HID := ExecSelectSQL('select HID from Inventory_Head where SINVID='+IntToStr(dmServ.CurInventory ), dmCom.quTmp);
      if VarIsNull(HID) then                                                                                                                                  
        raise Exception.Create('��� ������ � ������� Inventory_Head SINVID='+IntToStr(dmServ.CurInventory));
    end;
  except
    on E:Exception do begin
      MessageDialog('��� �������� �������������� �������� ������.'#13+
        E.Message+#13+'���������� � �������������', mtError, [mbOk], 0);
      eXit;
    end;
  end;
  if ShowAndFreeForm(TfmPredInverty, Self, TForm(fmPredInverty), True, False)=mrOk then
  begin
    LogOperationID:=dm3.insert_operation('������� ����� ��������������',LogOprIdForm);
    ExecSQL('execute procedure Fill_inventory('+IntToStr(dmServ.CurInventory)+')', dmCom.quTmp);
    if CenterDep then dmServ.CurInventory := HID;
    ReOpenDataSet(dmServ.quListInventory);
    if not dmServ.quListInventory.Locate('SINVID', dmServ.CurInventory, []) then
      raise Exception.Create('�� ������� ��������� ��������������. ���������� � �������������');
    ShowAndFreeForm(TfmInventory, Self, TForm(fmInventory), True, False);
    PostDataSet(dmServ.quListInventory);
    CloseDataSet(dmServ.quListInventory);
    dm3.update_operation(LogOperationID);
  end
  else begin
    ExecSQL('delete from SINV where SINVID='+IntToStr(dmServ.CurInventory)+' and ITYPE=16', dmServ.quTmp);
  end;
end;

procedure TfmUIDWH.acNewInventory1Execute(Sender: TObject);
var
  HID : Variant;

  function CreateNewInventory:boolean;

  begin
    Result := False;
    dmServ.CurInventory := dmCom.GetId(8);
    try
      ExecSQL('insert into SInv(SINVID, ITYPE, DEPID, USERID, ISCLOSED) values('+IntToStr(dmServ.CurInventory)+',16,'+IntToStr(dm.SDepId)+','+
      IntToStr(dmCom.UserId)+', 0)', dmCom.quTmp);
      if ShowAndFreeForm(TfmPredInverty, Self, TForm(fmPredInverty), True, False)<>mrOk then
      begin
        ExecSQL('delete from SINV where SINVID='+IntToStr(dmServ.CurInventory)+' and ITYPE=16', dmCom.quTmp);
        eXit;
      end;
      if CenterDep then
      begin
        HID := ExecSelectSQL('select HID from Inventory_Head where SINVID='+IntToStr(dmServ.CurInventory ), dmCom.quTmp);
        if VarIsNull(HID) then
          raise Exception.Create('��� ������ � ������� Inventory_Head SINVID='+IntToStr(dmServ.CurInventory));
      end;
      Result := True;
    except
      on E:Exception do MessageDialog('��� �������� �������������� �������� ������.'#13+
             E.Message+#13+'���������� � �������������', mtError, [mbOk], 0);
    end;
  end;

var
  LogOperationID:string;
  r : Variant;
  msg : string;
  ShowDialog : boolean;
  ResDialog : word;
  Bookmark : Pointer;
begin
  ResDialog := 0;

  if (dm.SDepId = -1) then raise EWarning.Create('�������� �����');
  r := ExecSelectSQL('select max(SInvId) from SINV where ITYPE=16 and isclosed=0 and DepId='+inttostr(SelfDepId), dmCom.quTmp);
  if VarIsNull(r)or(r=0) then
  begin
    // ��� ������ ��������������
    if MessageDialog('������� ����� ��������������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
    if not CreateNewInventory then eXit;
  end
  else begin
    dmServ.CurInventory := r;
    msg := '�������� ������ � �������������� ��� ������� ����� ��������������?'#13+
           '�� - �������� ������ � ������� ��������������'#13+
           '��� - ������� ����� ��������������'#13+
           '������ - �������� ��������';
    case MessageDialog(msg, mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes : ;
      mrNo : if not CreateNewInventory then eXit;
      mrCancel : SysUtils.Abort;
      else SysUtils.Abort;
    end;
  end;
  LogOperationID:=dm3.insert_operation('������� �������������� �� ���������� �� ������',LogOprIdForm);
  Bookmark := dm.quUIDWH.GetBookmark;
  try
    ShowDialog := True;
    dm.quUIDWH.First;
    while not dm.quUIDWH.Eof do
    begin
      if (dm.quUIDWHT.AsInteger in [2, // ������� �������
                                   3, // ������� ���.
                                   4, // ������� ����������� ������� ���������
                                   6 // ��� �������� ������ ���
                                   ])
      then begin
        if ShowDialog then
        begin
          msg := '������� '+dm.quUIDWHUID.AsString+' �� �������� �� ������. ���������:'+dm.quUIDWHMemo.AsString+#13+
                 '�������� � ��������������?';
          ResDialog := MessageDialogC(msg, mtConfirmation, [mbYes, mbNo, mbCancel], 0);
          ShowDialog := MsgDialogC._msgShow;
        end;
        case ResDialog of
          mrYes : begin
                    // �������� � ��������������
                    ExecSQL('insert into inventory (inventoryid, sinvid, uid, isin) '+
                            'values ('+IntToStr(dmCom.GetID(45))+','+IntToStr(dmServ.CurInventory)+','+dm.quUIDWHUID.AsString+', 0) ', dmCom.quTmp);
                  end;
          mrNo : ;// ��������� � ���� �������
          mrCancel : eXit;
        end;
      end
      else begin
        // �������� � ��������������
        ExecSQL('insert into inventory (inventoryid, sinvid, uid, isin) '+
                'values ('+IntToStr(dmCom.GetID(45))+','+IntToStr(dmServ.CurInventory)+','+dm.quUIDWHUID.AsString+', 0) ', dmCom.quTmp);
      end;
      dm.quUIDWH.Next;
    end;
  finally
    dm.quUIDWH.GotoBookmark(Bookmark);
  end;
  if CenterDep then dmServ.CurInventory := HID;
  ReOpenDataSet(dmServ.quListInventory);
  if not dmServ.quListInventory.Locate('SINVID', dmServ.CurInventory, []) then
    raise Exception.Create('�� ������� ��������� ��������������. ���������� � �������������');
  ShowAndFreeForm(TfmInventory, Self, TForm(fmInventory), True, False);
  PostDataSet(dmServ.quListInventory);
  CloseDataSet(dmServ.quListInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDWH.acOldInventoryExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID:= dm3.insert_operation('������ ��������������','-1000');
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      ShowAndFreeForm(TfmListInventory, Self, TForm(fmListInventory), True, False);
      tr.CommitRetaining;
    end;
 dm3.update_operation(LogOperationID);
end;

procedure TfmUIDWH.acEditUidwhExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('������������� �������',LogOprIdForm);
  with dm do
  begin
      if ShowAndFreeForm(TfmUIDEdit, Self, TForm(fmUIDEdit), True, False)=mrOK then  PostDataSets([quUIDWH])
      else CancelDataSets([quUIDWH]);
  end;
  dmcom.tr.CommitRetaining;
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDWH.acRecalcUiDWHExecute(Sender: TObject);
var LogOperationID:string;
    FManyUses:boolean;
begin
  { TODO : �������� ��������� ������ }
  try
   if not dm.countmaxpross then // ���� �� �������� ����������� ���������� ���-�� ��������
   begin
    {�������� ��� ��������� �� ������, ��� �� ����� ������������ ����� ������ ����� �� ������}
    if FThisCalcWH then // ���� ����� �� ������ ��� ��� ������ �� �����
    begin
     dm.quTmp.Close;
     dm.quTmp.sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
     dm.quTmp.ExecQuery;
     if dm.quTmp.fields[0].IsNull then FManyUses:=false
     else if dm.quTmp.fields[0].AsInteger<=1 then FManyUses:=false else FManyUses:=true;
     dm.quTmp.close;
     dm.quTmp.Transaction.CommitRetaining;
     if FManyUses then
      MessageDialog('����� ��� ������ ������������� ��� ���-�� ������.'+
                 #13#10+'�������� ��������.', mtInformation, [mbOk], 0);
    end else FManyUses:=false;
    {****************************************************************************************}
    if (not FManyUses) then
    begin
     Application.Minimize;
     Screen.Cursor:=crSQLWait;
     Application.ProcessMessages;
     dm.SetVisEnabled(plTotal,false);
     with dm do
     begin
        dm.quUIDWH.Active:=False;
        LogOperationID:=dm3.insert_operation('�������� ������',LogOprIdForm);
        PrepareData; // ���������� ������
        acRealCostExecute(nil);
        {*�������������� ������� �������*
         UIDWHSZ:='';
         dmcom.UIDWHSZ_:='*';
         chlbSz.Items.Assign(dm.slSz);
         chlbSZ.ItemIndex := GetIdxByCode(dm.slSZ, dmcom.UidwhSZ_);
         chlbSZ.Checked[chlbSZ.ItemIndex]:=true;
         chlbSZClickCheck(chlbSZ);
         ReOpenDataSet(dm.dsUIDWH.DataSet);
        ********************************}
        dm3.update_operation(LogOperationID);
      if Not CenterDep then
        with NoCloseRetAct do
          begin
              ExecQuery;
              if (Fields[0].asInteger<>0) then
                MessageDialog('������� ' + IntToStr(Fields[0].asInteger) + ' �� �������� ���� �������� �� �����������', mtWarning, [mbOk], 0);
              Close;
          end;

     end;
     with dmCom do
      begin
        if tr1.Active then tr1.COmmit;
        if not tr1.Active then tr1.StartTransaction;
      end;
     dm.quUIDWH.Active:=True;
     dm.quUIDWH.Transaction.CommitRetaining;
     dmCom.tr.CommitRetaining;
     ShowPeriod; // ������� ������� ��� ������� ������
     g1.Position:=10;
     if (g1.Position <11) then g1.Position:=11;  ///////////******************
    end;
   end
  finally
    Screen.Cursor:=crDefault;
    Application.Restore;
  end;
end;

// �������� --> �������
{procedure TfmUIDWH.acRepeatExecute(Sender: TObject);
begin
  if ShowAndFreeForm(TfmSameUID, Self, TForm(fmSameUID), True, False)=mrOK then
    with dm, quUIDWH do
      try
        Screen.Cursor:=crSQLWait;
        DisableControls;
        Active:=False;
        SelectSQL[SelectSQL.Count-1]:='ORDER BY 2';
        Active:=True;
        Locate('UID', UID, []);
      finally
        EnableControls;
        Screen.Cursor:=crDefault;
      end;
end;  }

procedure TfmUIDWH.N7Click(Sender: TObject);
var st:string;
    FCalcUser {������ �� ������� ������������ ����������},
    FUseCalc {����� � ���� ������������� ��� ������ ��� ���},
    FCalcMaxWh {��������� ������������ ���������� �������},
    FManyUses {������ ����������� �����, ��� ��� � ����� ��� ���-�� �����},
    FShowLastWh {�������� ��� ������������ ����� ��� ���}:boolean;
    CountCalcWh:integer;

begin
  FUseCalc := False;
  FCalcMaxWh := False;

{ TODO : �������� ������ �� ��������� ������ }
 if dm.dsUIDWH.DataSet<>dm.quUIDWH then raise Exception.Create('��� ��������� ������ ����� ����� ����� "�� ����. �����"');

 if dm.countmaxpross then sysUtils.Abort; // ���� ��������� ������������ ���������� �������

 dm.UIDWHType:=TMenuItem(Sender).Tag;
  case TComponent(Sender).Tag of
      2: begin // ����� �� ������������ ����
           FManyUses:=false; // ������ ������ ��������
           with dm, qutmp do
           begin
            //�������� ������� ��� ������� �������������
            Screen.Cursor:=crSQLWait;
            close;
            sql.Text:='select first 1 uid from uidwh_t where userid='+inttostr(dmcom.UserId);
            ExecQuery;
            Screen.Cursor:=crDefault;
            if not fields[0].IsNull then FCalcUser:=true else FCalcUser:=false;
            close;
            Transaction.CommitRetaining;
            //���� ������� �� ������������ ���, �� ����������� ���������� �������� ������� �� ������ � ��������� � ���� ���-���
            if not FCalcUser then
            begin
             //������������ ���������� �������� �� ������ ������
             Screen.Cursor:=crSQLWait;
             dm.quTmp.Close;
             dm.quTmp.SQL.Text:='select distinct u.userid from uidwh_t u, d_emp de '+
                       'where u.userid=de.d_empid';
             dm.quTmp.ExecQuery;
             while not dm.quTmp.Eof do
             begin
               dm.quTmp.Next;
             end;
             CountCalcWh:=dm.quTmp.RecordCount;
             dm.quTmp.Close;
             dm.quTmp.Transaction.CommitRetaining;
             Screen.Cursor:=crDefault;

         //    close;
         //    Transaction.CommitRetaining;
             if CountCalcWh<dm.MaxCountUIDWH then  begin
              FCalcMaxWh:=false;
              FThisCalcWH:=true;

              ExecSQL('update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

             end else begin
              FCalcMaxWh:=true;
              MessageDialog('���������� ������������ ���������� ������� �� ������.'+
               #13#10+'��� ������� ���������� � ��������������.', mtInformation, [mbOk], 0);
             end
            end
            else if not FThisCalcWH then begin
             Screen.Cursor:=crSQLWait;

             sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
             ExecQuery;

             Screen.Cursor:=crDefault;
             if fields[0].IsNull then FUseCalc:=false
             else if fields[0].AsInteger=0 then FUseCalc:=false else FUseCalc:=true;
             close;
             Transaction.CommitRetaining;
             if FUseCalc then
             begin
              if not FNotRecalc then
              if MessageDialog('����� �� ������ ������������. �������� ��������. ������� ������������?', mtInformation, [mbYes, mbNo], 0)=mrYes then
              begin
               FNotRecalc:=true;
               Screen.Cursor:=crSQLWait;

               sql.Text:='select uidwhbd, uidwhed from d_emp where d_empid='+inttostr(dmcom.UserId);
               ExecQuery;

               Screen.Cursor:=crDefault;
               if not fields[0].IsNull then dm.UIDWHBD:=fields[0].AsDateTime;
               if not fields[1].IsNull then dm.UIDWHED:=fields[1].AsDateTime;
               close;
               Transaction.CommitRetaining;
               ReOpenDataSet(dm.quUIDWH);
               fmUIDWH.Caption:='����� - '+trim(dmcom.UserName);

               ShowPeriod;

               ExecSQL('update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

              end
             end
             else ExecSQL('update d_emp set calcuidwh=calcuidwh+1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

            end else begin
              Screen.Cursor:=crSQLWait;

              sql.Text:='select calcuidwh from d_emp where d_empid='+inttostr(dmcom.UserId);
              ExecQuery;

              Screen.Cursor:=crDefault;
              if fields[0].IsNull then FManyUses:=false
              else if fields[0].AsInteger<=1 then FManyUses:=false else FManyUses:=true;
              close;
              Transaction.CommitRetaining;
              if FManyUses then
               MessageDialog('����� ��� ������ ������������� ��� ���-�� ������.'+
                #13#10+'�������� ��������.', mtInformation, [mbOk], 0);
            end;

            FShowLastWh:=false;
            if (FCalcUser) and (not FThisCalcWH) and (not FUseCalc) and (not FManyUses) then
            begin
             Screen.Cursor:=crSQLWait;

             sql.Text:='select uidwhbd, uidwhed from d_emp where d_empid='+inttostr(dmcom.UserId);
             ExecQuery;

             Screen.Cursor:=crDefault;
             if not fields[0].IsNull then begin
               st:=st+'� '+fields[0].AsString;
               dm.UIDWHBD:=fields[0].AsDateTime;
             end;
             if not fields[1].IsNull then begin
              st:=st+' �� '+fields[1].AsString;
              dm.UIDWHED:=fields[1].AsDateTime;
             end;
             close;
             Transaction.CommitRetaining;
             if MessageDialog('����� ��������� ��� �������� ������������ '+st+'. ����������� �����?', mtWarning, [mbYes, mbNo], 0)=mrNo then
             begin
              FThisCalcWH:=true;
              FShowLastWh:=true;
              ReOpenDataSet(dm.quUIDWH);
              fmUIDWH.Caption:='����� - '+trim(dmcom.UserName);

               ShowPeriod;

             end
             else FShowLastWh:=false;
            end;


            if (((not FCalcUser) and (not FCalcMaxWh)) or
                (FCalcUser and (not FThisCalcWH) and (not FUseCalc) and (not FShowLastWh)) or
                (FCalcUser and FThisCalcWH and (not FShowLastWh) and (not FManyUses))) then
             if ShowAndFreeForm(TfmSelectUidWh, Self, TForm(fmSelectUidWh), True, False)=mrOk then
             begin
             case dm.UIDWHType of
             1: begin  {**** ����� �� ������������ ������ ***}
                  FThisCalcWH:=true;
                  dm.UIDWHBD:=dmCOm.FirstMonthDate-1+0.9999;
                  dm.UIDWHED:=dm.UIDWHBD;

                  acRecalcUiDWHExecute(nil); //�������� ������

                  fmUIDWH.Caption:='����� - '+trim(dmcom.UserName);
                end;
             2: begin {*** ����� �� ������ ������ ***}
                  if GetPeriod(dm.UIDWHBD, dm.UIDWHED) then
                  begin
                    FThisCalcWH:=true;

                    acRecalcUiDWHExecute(nil);

                    fmUIDWH.Caption:='����� - '+trim(dmcom.UserName);
                  end
                  else
                    if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
                end;
             4: begin {*** ����� �� ���� ***}
                  if Getdate(dm.UIDWHED) then
                  begin
                    FThisCalcWH:=true;
                    dm.UIDWHBD:=dm.UIDWHED;

                    acRecalcUiDWHExecute(nil);

                    fmUIDWH.Caption:='����� - '+trim(dmcom.UserName);
                  end
                  else
                    if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
               end;
            end
            end
            else if not FThisCalcWH then ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);
         end;
        end;
      3: with dm.dsUIDWH.DataSet{quUIDWH} do // ������� �����
           begin
             try
               if FThisCalcWH or FNotRecalc then
               begin

                ExecSQL('update d_emp set calcuidwh=calcuidwh-1 where d_empid='+inttostr(dmcom.UserId), dm.quTmp);

                FThisCalcWH:=false;
                FNotRecalc:=false;
               end;
               FNotRecalc:=false;
               dm.UIDWHBD:=dmCom.FirstMonthDate;
               dm.UIDWHED:= strtodatetime(datetostr(dmCom.GetServerTime))+0.9999;
               Screen.Cursor:=crSQLWait;
               Active:=False;
               if dmCom.tr1.Active then dmCom.tr1.Commit;
               if not dmCom.tr1.Active then dmCom.tr1.StartTransaction;
               Application.ProcessMessages;

               CheckCalcing;

               Open;

               ShowPeriod;

               fmUIDWH.Caption:='����� ��-��������';
             finally
               Screen.Cursor:=crDefault;
             end;
           end;
    end;
end;

procedure TfmUIDWH.acStartGlobalInventoryUpdate(Sender: TObject);
begin
  // ������ ��� ������
  acStartGlobalInventory.Visible := CenterDep;
  if acStartGlobalInventory.Visible then
  begin
    acStartGlobalInventory.Visible := dmCom.GlobalInventoryEnabled=0;
  end;
end;

procedure TfmUIDWH.acStopGlobalInventoryUpdate(Sender: TObject);
begin
  // ������ ��� ������
  acStopGlobalInventory.Visible := CenterDep;
  if acStopGlobalInventory.Visible then
  begin
    acStopGlobalInventory.Visible := dmCom.GlobalInventoryEnabled=1;
  end;
end;

procedure TfmUIDWH.acSalesExecute(Sender: TObject);
var
  i,j:integer;
  dS: Boolean;
begin
//dmReport.PrintDocumentB(Sales);
 if ((dmcom.d_compid=-1) and (dmcom.d_supid=-1)) then
       ShowMessage('��������! ���������� ������� ���������� ��� ������������!')
     else
     if ((CenterDep) and (dm.SDepId=-1)) then
      ShowMessage('��������! ���������� ������� �����!') else
begin
  if MessageDialog('�������� ���� �������? (��� ����������� �������������)', mtInformation, [mbYes, mbNo], 0)=mrYes
  then dS:=true
  else dS:=false;

XLSExportFile1.Workbook.Create(1);
if (sd1.Execute) then
begin
 //------------------------------------------
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].Value:='����� �� ����������� ������������';
    dm.quTmp.Close;
    dm.quTmp.SQL.Text := 'select Name from D_Dep where D_DepId='+IntToStr(dm.SDepId);
    dm.quTmp.ExecQuery;
    XLSExportFile1.Workbook.Sheets[0].Cells[0,9].Value:=dm.quTmp.Fields[0].AsString;
    dm.quTmp.Close;

  XLSExportFile1.Workbook.Sheets[0].Cells[1,9].Value:=DateToStr(dm.UIDWHBD)+' - ';
  XLSExportFile1.Workbook.Sheets[0].Cells[2,9].Value:=DateToStr(dm.UIDWHED);
for i := 0 to 3 do
  begin
   XLSExportFile1.Workbook.Sheets[0].Rows[i].FontBold:=true;
   XLSExportFile1.Workbook.Sheets[0].Rows[i].HAlign:=xlHAlignLeft;

  end;
  XLSExportFile1.Workbook.Sheets[0].Rows[4].BorderColorRGB[xlBorderAll]:=xlcolorblack;
  XLSExportFile1.Workbook.Sheets[0].Rows[4].BorderStyle[xlBorderAll]:=bsThin;
  XLSExportFile1.Workbook.Sheets[0].Rows[4].FontBold:=true;
  XLSExportFile1.Workbook.Sheets[0].Rows[4].HAlign:=xlHAlignCenter;
  XLSExportFile1.Workbook.Sheets[0].Rows[4].Wrap:=true;
  XLSExportFile1.Workbook.Sheets[0].Columns[0].Width:=3;
  XLSExportFile1.Workbook.Sheets[0].Columns[1].Width:=20;
  XLSExportFile1.Workbook.Sheets[0].Columns[2].Width:=20;
  XLSExportFile1.Workbook.Sheets[0].Columns[3].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[4].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[5].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[6].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[7].Width:=30;
  XLSExportFile1.Workbook.Sheets[0].Columns[8].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Columns[9].Width:=15;
  XLSExportFile1.Workbook.Sheets[0].Cells[4,0].Value:='�';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,1].Value:='�������� ���������';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,2].Value:='��������� �� �������� �����-�����, ���.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,3].Value:='������� �� ������ �������, ��.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,4].Value:='�������, ��.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,5].Value:='������� �� �������, ��.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,6].Value:='����� ������, %';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,7].Value:='�������� �����, �� ������� ���� ������������� ������';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,8].Value:='����� ������ �� �������, ��.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,9].Value:='������� �� ����� �������, ��.';
  XLSExportFile1.Workbook.Sheets[0].Cells[4,10].Value:='���� �������';
  dmreport.ibdsSales.Active:=true;
  dmreport.ibdsSales.Open;
  dmreport.ibdsSales.First;
  i:=5;
While not dmreport.ibdsSales.Eof do
      begin
          XLSExportFile1.Workbook.Sheets[0].Cells[i,0].Value:=dmreport.ibdsSales.RecNo; //����� ������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,1].Value:=dmreport.ibdsSalesFULLART.AsString;//�������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,2].Value:=dmreport.ibdsSalesPRICE.AsFloat;//������� ���������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,3].Value:=dmreport.ibdssalesFcount.AsInteger;//������� �� ������ �������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,4].Value:=dmreport.ibdssalesscount.AsInteger;//���������� ���������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,5].Value:=dmreport.ibdssalesdcount.AsInteger;//������� �� �������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,6].Value:=dmreport.ibdssalesdisc.AsInteger;//����� ������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,7].Value:=''; //�����
          XLSExportFile1.Workbook.Sheets[0].Cells[i,8].Value:=dmreport.ibdssalesrcount.AsInteger;//����� ������ �� �������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,9].Value:=dmreport.ibdssaleslcount.AsInteger;//������� �� ����� �������
          XLSExportFile1.Workbook.Sheets[0].Cells[i,10].Value:=dmreport.ibdssalesselldate.AsString;//������� �� ����� �������
          dmreport.ibdsSales.Next;
       i:=i+1;
      end;
   for i := 4 to (4+dmreport.ibdsSales.RecordCount) do
      begin
       for j := 0 to 9 do
        begin
         XLSExportFile1.Workbook.Sheets[0].Cells[i,j].BorderColorRGB[xlBorderAll]:=xlcolorblack;
         XLSExportFile1.Workbook.Sheets[0].Cells[i,j].BorderStyle[xlBorderAll]:=bsThin;
         if (j>=7) then XLSExportFile1.Workbook.Sheets[0].Cells[i,j].FormatStringIndex:=0;
        end;

       end;

  //XLSExportFile1.Workbook.Sheets[0].Rows[i].FontBold:=true;
  XLSExportFile1.Workbook.Sheets[0].Cells[i+2,0].Value:='���������������';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+4,0].Value:='�������� ������������ ������';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+5,0].Value:='��� "�� "����" ____________________/������� �. �./';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+6,0].Value:='������������';
  XLSExportFile1.Workbook.Sheets[0].Cells[i+7,0].Value:='__________________________________/�������� �. �./';
  XLSExportFile1.SaveToFile(sd1.FileName);
  XLSExportFile1.Workbook.Clear;
  MessageDlg('������� �������� ������� � ���� ' + sd1.FileName, mtInformation, [mbOk], 0);
   dmreport.ibdsSales.Active:=false;
   dmreport.ibdsSales.Close;
 end;
end;
end;

procedure TfmUIDWH.acStartGlobalInventoryExecute(Sender: TObject);
begin
  dmCom.GlobalInventoryEnabled := 1;
end;

procedure TfmUIDWH.N21Click(Sender: TObject);
begin
  dmCom.GlobalInventoryEnabled := 0;
end;

procedure TfmUIDWH.acTextOldExecute(Sender: TObject);
var sDir, Path:string;
    i :integer;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='A_UID_.sql';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='sql';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    AssignFile(ftext, Path);
    Path := ExtractFileName(Path);
    SetLength(Path, Length(Path) - 4);
    try
     Rewrite(ftext);{
     writeln(ftext,'');
     writeln(ftext,format('%4s %10s %5s %5s %15s %10s %5s %5s %5s',
         ['�', '��.', '�����', '���.', '�������', '���.2', '������',
          '��.', '���']));
     writeln(ftext,format('%4s %10s %5s %5s %15s %10s %5s %5s %12s',
         ['', '�����', '', '', '', '', '',
          '���.', '']));

     writeln(ftext,'');
     writeln(ftext,''); }
     writeln(ftext, 'DELETE FROM ' + Path + ';');
     with dm, quUIDWH do
      begin
       First;
       i:=1;
       while not EOf do
       begin
       {
        writeln(ftext,format('%4d% 10d %5s %5s %15s %10s %5s %5s %8.2f',
         [i, quUIDWHUID.AsInteger, quUIDWHGOODID.AsString, quUIDWHMATID.AsString,
          quUIDWHART.AsString,quUIDWHART2.AsString, quUIDWHSZ.AsString,
          GetUNIT(quUIDWHUNITID.AsInteger), quUIDWHW.AsFloat]));
       }
       writeln(ftext,
        format('INSERT INTO ' + Path + ' (UID) VALUES (%6d);',[dm.quUIDWHUID.AsInteger]));{

        Art2Id:= FieldValues['Art2Id'];
        OpenDataSet(taA2Ins);
        taA2Ins.Last;
        taA2Ins.First;



        writeln(ftext,
        format(
        '|%5d'   +
        '|%10d'  +
        '|%10s'  +
        '|%10s'  +
        '|%20s'  +
        '|%20s'  +
        '|%10s'  +
        '|%1d'   +
        '|%8.2f' +
        '|%10s'  +
        '|%2d'   +
        '|'
        ,[
        i,                                   // ����� �� �������                      %5d
        dm.quUIDWHUID.AsInteger,             // ������������� �������                 %10d
        dm.quUIDWHGOODID.AsString,           // ������������� ������                  %10s
        dm.quUIDWHMATID.AsString,            // ������������� ���������               %10s
        dm.quUIDWHART.AsString,              // �������                               %20s
        dm.quUIDWHART2.AsString,             // ������ �������                        %20s
        dm.quUIDWHSZ.AsString,               // ������                                %10s
        dm.quUIDWHUNITID.AsInteger,          // ������������� ������� ���������       %1d
        dm.quUIDWHW.AsFloat,                 // ���                                   %8.2f
        dm.quUIDWHINSID.AsString,            // ������������� �������                 %10s
        taA2Ins.RecordCount                  // ���������� �������                    %2d
        ]));
        while not taA2Ins.eof do
        begin
          writeln(ftext,
          format('|%10s|%2d|%8.3f|%20s|%20s|%20s|%10s|%10s|%10s'
          , [
          dm.taA2InsD_INSID.AsString,
          dm.taA2InsQUANTITY.AsInteger,
          dm.taA2InsWEIGHT.AsFloat,
          dm.taA2InsCOLOR.AsString,
          dm.taA2InsCHROMATICITY.AsString,
          dm.taA2InsCLEANNES.AsString,
          dm.taA2InsGR.AsString,
          dm.taA2InsEDGTYPEID.AsString,
          dm.taA2InsEDGSHAPEID.AsString,
          ])
          );
          taA2Ins.Next;
        end;
        CloseDataSet(taA2Ins);}
         next;
         inc(i);
        end;
     end;
    finally
     writeln(ftext,'');
     CloseFile(fText);
    end;
   end;
  Screen.Cursor:=crDefault;
  MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
end;

procedure TfmUIDWH.acTollingFilterExecute(Sender: TObject);
begin
  dm.IsTolling := TMenuItem(Sender).Tag;
  Screen.Cursor := crSQLWait;
  ReOpenDataSet(dm.dsUIDWH.DataSet);
  Screen.Cursor := crDefault;
  siTolling.BtnCaption := TMenuItem(Sender).Caption;
end;

procedure TfmUIDWH.acNewTextOutLineExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;

begin
  sDir := ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='�����_����';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    AssignFile(ftext, Path);
    try
     Rewrite(ftext);

     dmServ.WHDepId := dm.SDepId;
     dmReport.FilterText:=UIDWHFilter;
     dmReport.UIDWHSelectModif(dmReport.taUIDWHA,TWinControl(Sender).Tag>1);
     ReOpenDataSet(dmReport.taUIDWHA);
     with dmreport, taUidWHA do
      begin
       First;
       while not EOf do
       begin
        writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
         [taUIDWHAArt.AsString,'|', taUIDWHAArt2.AsString, '|', taUIDWHAPrice.AsFloat, '|',
          GetUNIT(taUIDWHAUNITID.AsInteger), '|', GetUNITQ(taUIDWHAUNITID.AsInteger,
          taUIDWHAQ.AsInteger,taUIDWHAW.AsFloat), '|']));
         next;
        end;
       MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
     end;
    finally
     Screen.Cursor:=crDefault;
     CloseFile(fText);
     CloseDataSet(dmReport.taUIDWHA);
    end;
   end;
end;

procedure TfmUIDWH.acNewTextOutLineUpdate(Sender: TObject);
begin
 if dm.dsUIDWH.DataSet=dm.quUIDWH then TAction(Sender).Enabled:=true
 else TAction(Sender).Enabled:=false;
end;

procedure TfmUIDWH.acRecalcUiDWHUpdate(Sender: TObject);
begin
 if (dm.dsUIDWH.DataSet=dm.quUIDWH) then TAction(Sender).Enabled:=(not FNotRecalc)
 else TAction(Sender).Enabled:=false;
end;

procedure TfmUIDWH.N25Click(Sender: TObject);
begin
  dmReport.PrintDocumentB(uidWH_Report)
end;

procedure TfmUIDWH.acCreateApplDepUpdate(Sender: TObject);
begin
 TAction(Sender).Visible:=CenterDep
end;

procedure TfmUIDWH.acRealCostExecute(Sender: TObject);
begin
 if NRealCost.Checked then
 begin
   if dm.countmaxpross then begin
    NRealCost.Checked:=false;
    sysUtils.Abort;
   end;

  if dm.UIDWHType=3 then
   ExecSQL('execute procedure CALC_REAL_COST ('+inttostr(UserDefault)+');', dm.quTmp)
  else
   ExecSQL('execute procedure CALC_REAL_COST ('+inttostr(dmcom.UserId)+');', dm.quTmp);
  if dm.dsUIDWH.DataSet=dm.quUIDWH then ReOpenDataSet(dm.quUIDWH)
  else ReOpenDataSet(dm.quMaxUIDWH);
  grUIDWH.FieldColumns['PRICEINV'].Visible:=true;
  grUIDWH.FieldColumns['COSTINV'].Visible:=true;
  plTotal.Visible:=false;
 end
 else
 begin
  grUIDWH.FieldColumns['PRICEINV'].Visible:=false;
  grUIDWH.FieldColumns['COSTINV'].Visible:=false;
  plTotal.Visible:=false;
 end
end;

procedure TfmUIDWH.acBadPriceUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=
  (NRealCost.Checked and CenterDep) or (not CenterDep) and
  ((dmcom.Adm and (not AppDebug)) or AppDebug)
end;

procedure TfmUIDWH.NDinvClick(Sender: TObject);
begin
 if not NRealCost.Checked then raise Exception.Create('������� � ������� "�������� ���� �� ������� ������"');
 if dm.SDepId=-1 then raise Exception.Create('�������� �����');
 dmReport.PrintDocumentB(UidWh_INV_Art)
end;

procedure TfmUIDWH.NPrordWHClick(Sender: TObject);
begin
 if CenterDep then
  if not NRealCost.Checked then raise Exception.Create('������� � ������� "�������� ���� �� ������� ������"');
 if dm.SDepId=-1 then raise Exception.Create('�������� �����');
 dmReport.PrintDocumentB(UidWh_Prord)
end;

procedure TfmUIDWH.siSzClick(Sender: TObject);
begin
 dm.SetVisEnabled(chlbsz,True);
 chlbsz.SetFocus;
end;

procedure TfmUIDWH.chlbszClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.UIDWHSZ_:=ChlbClickCheck(chlbSz,s);
 siSz.BtnCaption:=s;
end;

procedure TfmUIDWH.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100301)
end;

procedure TfmUIDWH.NAllClick(Sender: TObject);
begin
 SItype.BtnCaption:=TMenuItem(Sender).Caption;
 with dm do
  begin
   AllUIDWH:= TMenuItem(Sender).Tag=2;      // �� ������
   lSellSelect:=TMenuItem(Sender).Tag=3;   // ���������
   lSRetSelect:=TMenuItem(Sender).Tag=4;   // �������� �����������
   lOSinvSelect:=TMenuItem(Sender).Tag=5;  // �������� ��������
   lClRetSelect:=TMenuItem(Sender).Tag=6;  // ��������� ��������
   ReopenDataSets([dsUIDWH.DataSet{quUIDWH}]);
   dm.SetVisEnabled(plTotal,false);
  end;
end;

procedure TfmUIDWH.nComissionActClick(Sender: TObject);
begin

  dmServ.WHDepId := dm.SDepId;

  dmReport.UIDWHSelectModif(dmReport.taUIDWHB, false);

  dmreport.FilterText := UIDWHFilter;

  dmReport.PrintDocumentB(comissionAct);

end;

procedure TfmUIDWH.chCostClick(Sender: TObject);
begin
 if chCost.Checked then begin
  dm.IFilterCost:=true;
  dm.ICOst1:=edCost1.Value;
  dm.ICOst2:=edCost2.Value;
  edCost1.Enabled:=false;
  edCost2.Enabled:=false;
 end else begin
  edCost1.Enabled:=true;
  edCost2.Enabled:=true;
  dm.IFilterCost:=false;
  edCost1.Value:=0;
  edCost2.Value:=0;
 end;
 ReopenDataSets([dm.dsUIDWH.DataSet{quUIDWH}]);
end;

procedure TfmUIDWH.chWClick(Sender: TObject);
begin
 if chW.Checked then begin
  dm.IFilterW:=true;
  dm.Iw1:=rxw1.Value;
  dm.Iw2:=rxw2.Value;
  rxw1.Enabled:=false;
  rxw2.Enabled:=false;
 end else begin
  rxw1.Enabled:=true;
  rxw2.Enabled:=true;
  dm.IFilterW:=false;
  rxw1.Value:=0;
  rxw2.Value:=0;
 end;
 ReopenDataSets([dm.dsUIDWH.DataSet{quUIDWH}]);
end;

procedure TfmUIDWH.edArt2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_RETURN :
       try
         Screen.Cursor := crSQLWait;
         dm.FilterArt2 := edArt2.Text;
         dmCom.FilterArt := edArt.Text;
         ReOpenDataSets([dm.dsUIDWH.DataSet]);
         dm.SetVisEnabled(plTotal,False);
       finally
         Screen.Cursor := crDefault;
       end;
  end;
end;

procedure TfmUIDWH.edArt2ButtonClick(Sender: TObject);
var
  Key : word;
begin
 Key := VK_RETURN;
   edArt2KeyDown(Sender, Key, []);
end;

procedure TfmUIDWH.edArt2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_DOWN : begin
      ActiveControl := grUIDWH;
      ActiveControl.SetFocus;
    end;
  end;
end;

procedure TfmUIDWH.acRepeatUpdate(Sender: TObject);
begin
 if (dm.dsUIDWH.DataSet=dm.quUIDWH) then
  TAction(Sender).Enabled:=(not FNotRecalc) and AppDebug
 else TAction(Sender).Enabled:=false;
end;

procedure TfmUIDWH.SpeedItem2Click(Sender: TObject);
begin
//
end;

// -----------  ������������ ������ ������������ ------------- //
procedure TfmUIDWH.acCreateAppl1Execute(Sender: TObject);
var LogOperationID:string;
begin
 if MessageDialog('��������� ������ ������� ��� ������������ ������ ������������?', mtWarning, [mbYes, mbNo], 0)=mrNo then
 begin  // ��� ����� ������� ��������
   LogOperationID:=dm3.insert_operation('������������ ������',LogOprIdForm);
   PostDataSet(dm.dsUIDWH.DataSet);
   Screen.Cursor := crSQLWait;
   ExecSQL('execute procedure Create_Appl_FromUidwh1 ('+inttostr(dmcom.User.UserId)+',0, '+string(dm.dsUIDWH.DataSet.FieldValues['USERID_WH'])+')', dmCom.quTmp);
   dm.IsChangeAppl:=false;
   ReOpenDataSet(dm.dsUIDWH.DataSet);
   Screen.Cursor := crDefault;
   MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
   dm3.update_operation(LogOperationID);
 end
 else
 begin // � ������ ������� ��������
   LogOperationID:=dm3.insert_operation('������������ ������',LogOprIdForm);
   PostDataSet(dm.dsUIDWH.DataSet);
   Screen.Cursor := crSQLWait;
   ExecSQL('execute procedure Create_Appl_FromUidwh1_Art2 ('+inttostr(dmcom.User.UserId)+',0, '+string(dm.dsUIDWH.DataSet.FieldValues['USERID_WH'])+')', dmCom.quTmp);
   dm.IsChangeAppl:=false;
   ReOpenDataSet(dm.dsUIDWH.DataSet);
   Screen.Cursor := crDefault;
   MessageDialog('������ �����������!', mtInformation, [mbOk], 0);
   dm3.update_operation(LogOperationID);
 end;
end;

procedure TfmUIDWH.cbDifferenceClick(Sender: TObject);
begin
//  dm.cbDifference := cbDifference.Checked;
//  ReopenDataSets([dm.dsUIDWH.DataSet]);
end;

procedure TfmUIDWH.acNewTextOldExecute(Sender: TObject);
var sDir, Path:string;
    i, j :integer;
    ftext:textfile;
    PacketN: Integer;
    NewSupplierID: Integer;
    NewSupplier: string;
    NewProducerID: Integer;
    NewProducer: string;
    NewExternalDocument: string;
    NewExternalDocumentDate: TDateTime;
    NewFiscalID: Integer;
    NewCountryID: string;
    OldSupplierID: Integer;
    OldSupplier: string;
    OldProducerID: Integer;
    OldProducer: string;
    OldExternalDocument: string;
    OldExternalDocumentDate: TDateTime;
    Supplier:string;
    Directory: string;
    Art2: string;
    InsQ: Integer;
    Flag: Boolean;
    InvoiceName: string;

function NormalizeValue(Value: string): string;
begin
  Value := AnsiReplaceStr(Value, '*' , ' ');
  Value := AnsiReplaceStr(Value, '"' , ' ');
  Value := AnsiReplaceStr(Value, '''' , ' ');
  Value := AnsiReplaceStr(Value, '/' , ' ');
  Value := AnsiReplaceStr(Value, '\' , ' ');

  while AnsiContainsText(Value, '  ') do
  Value := AnsiReplaceStr(Value, '  ' , ' ');

  Value := Trim(Value);

  Result := Value;
end;

begin
  dm.quUIDWH.Active := False;
  dm.UIDWHExportMode := True;
  dm.quUIDWH.Active := True;

  PacketN := 0;
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
  svdFile.FileName:='Dummy';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='tmp';
  if svdFile.Execute then
   begin
    Path:=svdFile.FileName;
    SetLength(Path, Length(Path) - 4);
     with dm, quUIDWH do
      begin
       First;
       i:=1;
       while not EOf do

       begin
         NewSupplierID := dm.quUIDWHSupID0.AsInteger;
         NewSupplier := {Trim(}dm.quUIDWHSUPNAME.AsString{)};
         NewProducerID := dm.quUIDWHProducerID.AsInteger;
         NewProducer := Trim(dm.quUIDWHPRODNAME.AsString);
         NewExternalDocument := Trim(dm.quUIDWHSSF0.AsString);
         NewExternalDocumentDate := DateOf(dm.quUIDWHNDate.AsDateTime);
         NewCountryID := dm.quUIDWHD_COUNTRYID.AsString;
         NewFiscalID := dm.quUIDWHNDSID.AsInteger;
         Supplier := NewSupplier;
         Supplier := AnsiReplaceStr(Supplier, '*' , ' ');
         Supplier := AnsiReplaceStr(Supplier, '"' , ' ');
         Supplier := AnsiReplaceStr(Supplier, '''' , ' ');
         Supplier := AnsiReplaceStr(Supplier, '/' , ' ');
         Supplier := Trim(AnsiReplaceStr(Supplier, '\' , ' '));
         while AnsiContainsText(Supplier, '  ') do
         Supplier := AnsiReplaceStr(Supplier, '  ' , ' ');
         Supplier := Trim(Supplier);
         Directory := Trim(ExtractFilePath(Path));
         Directory := Directory + Supplier + '\';
         if not DirectoryExists(Directory) then CreateDir(Directory);
         if PacketN <> 0 then
         begin
           if (NewSupplierID <> OldSupplierID) or
           (NewProducerID <> OldProducerID) or
           (NewExternalDocument <> OldExternalDocument) or
            (NewExternalDocumentDate <> OldExternalDocumentDate) then
           begin
             Inc(PacketN);
             CloseFile(fText);
             InvoiceName := '�������� (' + FormatDateTime('yyyy.mm.dd', NewExternalDocumentDate) + ')(';
             InvoiceName := InvoiceName + NormalizeValue(NewExternalDocument) + ').' + IntToHex(PacketN, 4) + '.txt';
             AssignFile(ftext, Directory + InvoiceName);
             Rewrite(ftext);
             writeln(ftext, NewSupplier);
             writeln(ftext, IntToStr(NewSupplierID));
             writeln(ftext, NewExternalDocument);
             writeln(ftext, DateTimeToStr(NewExternalDocumentDate));
             writeln(ftext, NewProducer);
             writeln(ftext, IntToStr(NewProducerID));
             writeln(ftext, NewCountryID);
             writeln(ftext, IntToStr(NewFiscalID));
           end
         end
         else
         begin
           Inc(PacketN);
           InvoiceName := '�������� (' + FormatDateTime('yyyy.mm.dd', NewExternalDocumentDate) + ')(';
           InvoiceName := InvoiceName + NormalizeValue(NewExternalDocument) + ').' + IntToHex(PacketN, 4) + '.txt';
           AssignFile(ftext, Directory + InvoiceName);
           Rewrite(ftext);
           writeln(ftext, NewSupplier);
           writeln(ftext, IntToStr(NewSupplierID));
           writeln(ftext, NewExternalDocument);
           writeln(ftext, DateTimeToStr(NewExternalDocumentDate));
           writeln(ftext, NewProducer);
           writeln(ftext, IntToStr(NewProducerID));
           writeln(ftext, NewCountryID);
           writeln(ftext, IntToStr(NewFiscalID));
         end;

        Art2Id:= FieldValues['Art2Id'];
        Art2:= Trim(FieldByName('Art2').AsString);
        OpenDataSet(taA2Ins);
        taA2Ins.Last;
        taA2Ins.First;

        InsQ := taA2Ins.RecordCount;

        writeln(ftext,
        format(
        '%5d'   +
        '|%10d'  +
        '|%10s'  +
        '|%10s'  +
        '|%20s'  +
        '|%20s'  +
        '|%10s'  +
        '|%1d'   +
        '|%8.2f' +
        '|%10s'  +
        '|%10.2f' +
        '|%10.2f' +
        '|%2d'   +
        '|'
        ,[
        i,
        dm.quUIDWHUID.AsInteger,             // ������������� �������                 %10d
        dm.quUIDWHGOODID.AsString,           // ������������� ������                  %10s
        dm.quUIDWHMATID.AsString,            // ������������� ���������               %10s
        dm.quUIDWHART.AsString,              // �������                               %20s
        dm.quUIDWHART2.AsString,             // ������ �������                        %20s
        dm.quUIDWHSZ.AsString,               // ������                                %10s
        dm.quUIDWHUNITID.AsInteger,          // ������������� ������� ���������       %1d
        dm.quUIDWHW.AsFloat,                 // ���                                   %8.2f
        dm.quUIDWHINSID.AsString,            // ������������� �������                 %10s
        dm.quUIDWHSPRICE0.AsFloat,           // ��������� ����                        %8.2f
        dm.quUIDWHPRICE.AsFloat,             // ��������� ����                        %8.2f
        InsQ                                 // ���������� �������                    %2d
        ]));

        if (InsQ > 0) then
        begin
          j := 1;
          while not taA2Ins.eof do
          begin
            writeln(ftext,
            format('*|%2d|%10s|%2d|%8.3f|%20s|%20s|%20s|%10s|%10s|%10s|'
            , [
            j,
            Trim(dm.taA2InsD_INSID.AsString),      // �������
            dm.taA2InsQUANTITY.AsInteger,          // ����������
            dm.taA2InsWEIGHT.AsFloat,              // ���
            Trim(dm.taA2InsCOLOR.AsString),        // ����
            Trim(dm.taA2InsCHROMATICITY.AsString), // ���������
            Trim(dm.taA2InsCLEANNES.AsString),     // �������
            Trim(dm.taA2InsGR.AsString),           // ������
            Trim(dm.taA2InsEDGTYPEID.AsString),    // �������
            Trim(dm.taA2InsEDGSHAPEID.AsString)    // �����
            ])
            );
            Inc(j);
            taA2Ins.Next;
          end;
        end;
        CloseDataSet(taA2Ins);
        next;
        inc(i);
        OldSupplierID :=  NewSupplierID;
        OldSupplier :=  NewSupplier;
        OldProducerID :=  NewProducerID;
        OldProducer := NewProducer;
        OldExternalDocument := NewExternalDocument;
        OldExternalDocumentDate := NewExternalDocumentDate;
      end;
      if PacketN <> 0 then CloseFile(ftext);
    end;
   end;
  Screen.Cursor:=crDefault;
  MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
  dm.quUIDWH.Active := False;
  dm.UIDWHExportMode := False;
  dm.quUIDWH.Active := True;
end;

procedure TfmUIDWH.ButtonUIDClick(Sender: TObject);
var
  UID: Integer;
begin

  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='DELETE FROM UID_KURSK';
  dm.quTmp.ExecQuery;
  dmcom.tr.CommitRetaining;
  dm.quTmp.Close;

  dm.quUIDWH.First;
  while not dm.quUIDWH.EOf do
  begin
    UID := dm.quUIDWHUID.AsInteger;
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='INSERT INTO UID_KURSK(UID) values (' +  IntToStr(UID) + ')';
    dm.quTmp.ExecQuery;
    dmcom.tr.CommitRetaining;
    dm.quTmp.Close;
    dm.quUIDWH.Next;
  end;
end;

procedure TfmUIDWH.SpeedItem15Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbIns, True);
  chlbIns.SetFocus;
end;

procedure TfmUIDWH.chlbInsExit(Sender: TObject);
begin
  dm.SetVisEnabled(TWinControl(sender),False);
end;

procedure TfmUIDWH.chlbInsClickCheck(Sender: TObject);
var
  s:string;
begin
 dmCom.D_InsId :=ChlbClickCheck(chlbIns ,s);
 SpeedItem15.BtnCaption:=s;
end;

procedure TfmUIDWH.cbComissionClick(Sender: TObject);
begin
 SItype.BtnCaption:=TMenuItem(Sender).Caption;
 with dm do
  begin
   AllUIDWH:= TMenuItem(Sender).Tag=2;
   lSellSelect:=TMenuItem(Sender).Tag=3;
   lSRetSelect:=TMenuItem(Sender).Tag=4;
   lOSinvSelect:=TMenuItem(Sender).Tag=5;
   ReopenDataSets([dsUIDWH.DataSet{quUIDWH}]);
   dm.SetVisEnabled(plTotal,false);
  end;

end;

procedure TfmUIDWH.OnPaytype(Sender: TObject);
var
  Pay: Integer;
begin
     Pay:= TMenuItem(Sender).Tag;
     if dm.PayType <> Pay then
  begin
    dm.PayType := Pay;
     with dm do
     begin
     siPayType.BtnCaption := TMenuItem(Sender).Caption;
     ReopenDataSets([dsUIDWH.DataSet{quUIDWH}]);
     dm.SetVisEnabled(plTotal,false);
     end;
  end;
end;

procedure TfmUIDWH.OnComission(Sender: TObject);
var
  Comission: Integer;
begin
  Comission := TMenuItem(Sender).Tag;
  if dm.Comission <> Comission then
  begin
    dm.Comission := Comission;
    with dm do
    begin
      siComission.BtnCaption := TMenuItem(Sender).Caption;
      ReopenDataSets([dsUIDWH.DataSet{quUIDWH}]);
      dm.SetVisEnabled(plTotal,false);
    end;
  end;
end;

procedure TfmUIDWH.ColorClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ColorSchema, '-1000');
  ShowAndFreeForm(TfmCol, Self, TForm(fmCol), True, False);
  dm3.update_operation(LogOperationID);
end;

initialization
  fmUIDWH:=NIL;




end.








