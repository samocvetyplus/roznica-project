unit InfoEditArtHist;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Grids, DBGridEh,
  M207Ctrls, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmInfoEditArtHist = class(TForm)
    tb1: TSpeedBar;
    Label1: TLabel;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    edArt: TEdit;
    siperiod: TSpeedItem;
    Lperiod: TLabel;
    pc1: TPageControl;
    teditart: TTabSheet;
    teditart2: TTabSheet;
    teditins: TTabSheet;
    dgEditArt: TDBGridEh;
    dgEditArt2: TDBGridEh;
    dgEditIns: TDBGridEh;
    fr1: TM207FormStorage;
    chbOnlyEdit: TCheckBox;
    siHelp: TSpeedItem;
    Label2: TLabel;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siperiodClick(Sender: TObject);
    procedure chbOnlyEditClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmInfoEditArtHist: TfmInfoEditArtHist;

implementation
uses data3, M207Proc, comdata, Period;
{$R *.dfm}

procedure TfmInfoEditArtHist.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmInfoEditArtHist.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmInfoEditArtHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  dm3.FilterEdit:=false;
  dm3.FilterArt:='';
  dm3.MBD:=dmcom.FirstMonthDate;
  dm3.MED:=dmcom.GetServerTime;
  OpenDataSets([dm3.quEditArt, dm3.quEditArt2, dm3.quEditIns]);
  Lperiod.Caption:='� '+datetimetostr(dm3.MBD)+' �� '+datetimetostr(dm3.MED);
end;

procedure TfmInfoEditArtHist.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CloseDataSets([dm3.quEditArt, dm3.quEditArt2, dm3.quEditIns]);
end;

procedure TfmInfoEditArtHist.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN:
  begin
   dm3.FilterArt:=edart.Text;
   ReOpenDataSets([dm3.quEditArt, dm3.quEditArt2, dm3.quEditIns]);
  end;
 end
end;

procedure TfmInfoEditArtHist.siperiodClick(Sender: TObject);
begin
 if GetPeriod(dm3.MBD, dm3.MED) then
 begin
   ReOpenDataSets([dm3.quEditArt, dm3.quEditArt2, dm3.quEditIns]);
   Lperiod.Caption:='� '+datetimetostr(dm3.MBD)+' �� '+datetimetostr(dm3.MED);
 end
end;

procedure TfmInfoEditArtHist.chbOnlyEditClick(Sender: TObject);
begin
 dm3.FilterEdit:=chbOnlyEdit.Checked;
 ReOpenDataSets([dm3.quEditArt, dm3.quEditArt2, dm3.quEditIns]);
end;

procedure TfmInfoEditArtHist.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100560)
end;

procedure TfmInfoEditArtHist.FormActivate(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
