object fmOptDistr: TfmOptDistr
  Left = 89
  Top = 76
  Width = 812
  Height = 612
  Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1086#1077' '#1088#1072#1089#1087#1088#1077#1076#1077#1083#1077#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1081' '#1076#1083#1103' '#1086#1087#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1072#1078#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 323
    Width = 804
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object sb2: TStatusBar
    Left = 0
    Top = 566
    Width = 804
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 804
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 675
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 326
    Width = 804
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 249
      Top = 0
      Width = 3
      Height = 240
      Cursor = crHSplit
    end
    object Panel3: TPanel
      Left = 252
      Top = 0
      Width = 552
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dgWH: TM207IBGrid
        Left = 0
        Top = 27
        Width = 552
        Height = 213
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsD_WH
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        IniStorage = FormStorage1
        TitleButtons = True
        MultiShortCut = 0
        ColorShortCut = 0
        PrintDataSet = 0
        ClearHighlight = False
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRODCODE'
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 28
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'D_MATID'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 27
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'D_GOODID'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 34
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'D_INSID'
            Title.Alignment = taCenter
            Title.Caption = #1054#1042
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 23
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 82
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2 '
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 48
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 48
            Visible = True
          end
          item
            Color = clYellow
            Expanded = False
            FieldName = 'OPTPRICE'
            Title.Alignment = taCenter
            Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 55
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 58
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 552
        Height = 27
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label13: TLabel
          Left = 12
          Top = 6
          Width = 32
          Height = 13
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edArt: TEdit
          Left = 50
          Top = 2
          Width = 79
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          Text = 'edArt'
          OnChange = edArtChange
          OnKeyDown = edArtKeyDown
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 249
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object Splitter4: TSplitter
        Left = 121
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object Splitter5: TSplitter
        Left = 184
        Top = 1
        Width = 3
        Height = 238
        Cursor = crHSplit
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 124
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 187
        Top = 1
        Width = 61
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 42
    Width = 804
    Height = 281
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter6: TSplitter
      Left = 209
      Top = 0
      Width = 3
      Height = 281
      Cursor = crHSplit
    end
    object pa1: TPanel
      Left = 0
      Top = 0
      Width = 209
      Height = 281
      Align = alLeft
      BevelOuter = bvNone
      Caption = 'pa1'
      TabOrder = 0
      object paDepFrom: TPanel
        Left = 0
        Top = 0
        Width = 209
        Height = 19
        Align = alTop
        BevelOuter = bvLowered
        Caption = 'paDepFrom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object dg1: TM207IBGrid
        Left = 0
        Top = 19
        Width = 209
        Height = 214
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsD_SZ
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm1
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = N1Click
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dg1GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        PrintDataSet = 0
        ClearHighlight = False
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 32
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 32
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 34
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'DQ'
            Title.Alignment = taCenter
            Title.Caption = #1050'-'#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 29
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'DW'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 44
            Visible = True
          end>
      end
      object paUID: TPanel
        Left = 10
        Top = 186
        Width = 185
        Height = 29
        BevelOuter = bvLowered
        TabOrder = 2
        object Label4: TLabel
          Left = 3
          Top = 1
          Width = 61
          Height = 13
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object laSup: TLabel
          Left = 65
          Top = 1
          Width = 27
          Height = 13
          Caption = 'laSup'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 3
          Top = 13
          Width = 44
          Height = 13
          Caption = #1055#1088' '#1094#1077#1085#1072':'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object laPrice: TLabel
          Left = 47
          Top = 13
          Width = 27
          Height = 13
          Caption = 'laSup'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object dg2: TM207IBGrid
        Left = -4
        Top = 68
        Width = 209
        Height = 146
        Color = clBtnFace
        DataSource = dm.dsD_UID
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm2
        ReadOnly = True
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        OnDblClick = N1Click
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dg2GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        PrintDataSet = 0
        ClearHighlight = False
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 67
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 45
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SUP'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
      inline frDTotal1: TfrDTotal
        Left = 0
        Top = 233
        Width = 209
        Height = 48
        Align = alBottom
        TabOrder = 4
        inherited pa2: TPanel
          Width = 209
        end
      end
    end
    object sb1: TScrollBox
      Left = 212
      Top = 0
      Width = 592
      Height = 281
      Align = alClient
      TabOrder = 1
    end
  end
  object FormStorage1: TFormStorage
    StoredProps.Strings = (
      'pa1.Width'
      'lbComp.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 441
    Top = 58
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 40
    Top = 66
    object N1: TMenuItem
      Caption = #1056#1072#1079#1084#1077#1088#1099'/'#1048#1076'.'#1085#1086#1084#1077#1088#1072
      ImageIndex = 27
      ShortCut = 114
      OnClick = N1Click
    end
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 146
    Top = 102
    object MenuItem1: TMenuItem
      Caption = #1056#1072#1079#1084#1077#1088#1099'/'#1048#1076'.'#1085#1086#1084#1077#1088#1072
      ImageIndex = 27
      ShortCut = 114
      OnClick = N1Click
    end
    object miSupFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1087#1086#1089#1090#1072#1097#1080#1082#1091
      ShortCut = 116
      OnClick = SetFilter
    end
    object miPriceFilter: TMenuItem
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1094#1077#1085#1077
      ShortCut = 117
      OnClick = SetFilter
    end
  end
end
