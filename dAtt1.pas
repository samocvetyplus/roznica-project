unit dAtt1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, ComCtrls, ActnList, jpeg,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmdAtt1 = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    StatusBar1: TStatusBar;
    gridAtt1: TDBGridEh;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    spitSort: TSpeedItem;
    acSort: TAction;
    acClose: TAction;
    siPrint: TSpeedItem;
    siHelp: TSpeedItem;
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acSortExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acSortUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure siPrintClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  end;

var
  fmdAtt1: TfmdAtt1;

implementation

uses comdata, dbTree, Sort, Data, m207Proc;

{$R *.dfm}

procedure TfmdAtt1.acAddExecute(Sender: TObject);
begin
  dmCom.taAtt1.Insert;
end;

procedure TfmdAtt1.acDelExecute(Sender: TObject);
begin
  dmCom.taAtt1.Delete;
end;

procedure TfmdAtt1.acSortExecute(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  fmSort:=TfmSort.Create(Application);
  with fmSort do
  try
    lb1.Items.Clear;
    dmCom.quTmp.SQL.Text:='SELECT ID, NAME FROM D_ATT1 WHERE ID<>-1 ORDER BY SORTIND';
    dmCom.quTmp.ExecQuery;
    while not dmCom.quTmp.Eof do
    begin
      nd:=TNodeData.Create;
      nd.Code:=dmCom.quTmp.Fields[0].AsString;
      lb1.Items.AddObject(dmCom.quTmp.Fields[1].AsString, nd);
      dmCom.quTmp.Next;
    end;
    dmCom.quTmp.Close;
    if ShowModal=mrOK then
    begin
      dmCom.quTmp.SQL.Text:='UPDATE D_ATT1 SET SORTIND=?SORTIND where ID=?ID';
      for i:=0 to lb1.Items.Count-1 do
        with dmCom.quTmp do
        begin
          dmCom.quTmp.Params[0].AsInteger:=i;
          dmCom.quTmp.Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
          dmCom.quTmp.ExecQuery;
        end;
      dmCom.tr.CommitRetaining;
      ReOpenDataSets([dmCom.taAtt1]);      
    end;
  finally
    fmSort.Free;
  end;
end;

procedure TfmdAtt1.acAddUpdate(Sender: TObject);
begin
{  acAdd.Visible := CenterDep;
  acAdd.Enabled := dmCom.taAtt1.Active;}
end;

procedure TfmdAtt1.acDelUpdate(Sender: TObject);
begin
{  acDel.Visible := CenterDep;
  acDel.Enabled := dmCom.taAtt1.Active and (not dmCom.taAtt1.IsEmpty);}
end;

procedure TfmdAtt1.acSortUpdate(Sender: TObject);
begin
{  acSort.Visible := CenterDep;
  acSort.Enabled := dmCom.taAtt1.Active and (not dmCom.taAtt1.IsEmpty);}
end;

procedure TfmdAtt1.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining
  else dmCom.tr.StartTransaction;
  OpenDataSets([dmCom.taAtt1]);
end;

procedure TfmdAtt1.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmdAtt1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmCom.taAtt1]);
  Action := caFree;
end;

procedure TfmdAtt1.acCloseExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfmdAtt1.siPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsrAtt1, 66); //'gd'
end;

procedure TfmdAtt1.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100108)
end;

procedure TfmdAtt1.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
