unit DBTree;

interface

uses ComCtrls, Forms, Graphics, Controls, pFIBQuery;

Type TNodeText=function(q: TpFIBQuery): string;

Type TNodeData=class(TObject)
       public
         Name: string;
         Code: variant;
         Loaded: boolean;
         Value: integer;
         SaveValue: integer;
         constructor Create;
       end;

//Type TIterateProcedure=procedure(Node: TOutlineNode);

Procedure InitTree(RootText: string; tv: TTreeView; RootCode: variant);

Procedure ExpandTree(tv: TTreeView; Node: TTreeNode; NodeText: TNodeText; q: TpFIBQuery;
                      FieldInd: array of byte);



Procedure ReExpand(Node: TTreeNode);
Procedure GetNodeImages(Node: TTreeNode);
Procedure GetSelNodeImages(Node: TTreeNode);

Function NodeText(q: TpFIBQuery): string;
Function NodeText1(q: TpFIBQuery): string;
Function  GetParent(Node: TTreeNode; Level: integer): TTreeNode;

//Procedure IterateOutLine(co: TCheckOutLine; ParentItem: integer; p: TIterateProcedure);

implementation

uses RxStrUtils, Variants;

constructor TNodeData.Create;
begin
  inherited Create;
  Value:=0;
end;

procedure ExpandTree(tv: TTreeView; Node: TTreeNode; NodeText: TNodeText; q: TpFIBQuery; FieldInd: array of byte);
var pN: TTreeNode;
    ND: TNodeData;
begin
  try
  Screen.Cursor:=crHourGlass;
  ND:=TNodeData(Node.Data);
  if NOT ND.Loaded then
    begin
      ND.Loaded:=True;
      with q do
        begin
        
          if ND.Code=NULL then
            begin
              Params[0].AsString:='-2';
              Params[1].AsInteger:=1;
            end
          else
            begin
              Params[0].AsString:=ND.Code;
              Params[1].AsInteger:=0;
            end;

          ExecQuery;
          Node.HasChildren:=NOT Fields[0].IsNull;
          while NOT EOF do
            begin
              pN:=tv.Items.AddChild(Node,NodeText(q)) ;
              pN.StateIndex:=1;
              pN.HasChildren:=q.Fields[FieldInd[2]].AsInteger>0;
              ND:=TNodeData.Create;
              pN.Data:=ND;
              ND.Loaded:=False;
              ND.Code:=DelRSpace(q.Fields[FieldInd[0]].AsString);
              ND.Name:=DelRSpace(q.Fields[FieldInd[1]].AsString);
              Next;
            end;
          Close;
        end;
    end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure InitTree(RootText: string; tv: TTreeView; RootCode: variant);
var ND: TNodeData;
begin
  ND:=TNodeData.Create;
  ND.Code:=RootCode;
  ND.Loaded:=False;

  with tv.Items.AddChild(NIL, RootText) do
    begin
      Data:=ND;
      HasChildren:=True;
      StateIndex:=0;
    end;
  tv.TopItem.Expand(False);

end;


procedure ReExpand(Node: TTreeNode);
begin
  with Node do
    begin
      TNodeData(Data).Loaded:=False;
      Collapse(False);
      DeleteChildren;
      HasChildren:=True;
      Expand(False);
    end;
end;

Procedure GetNodeImages(Node: TTreeNode);
begin
  with Node do
    if Node.Parent=NIL then ImageIndex:=0
    else if HasChildren AND NOT Expanded then ImageIndex:=1
         else if HasChildren AND Expanded then ImageIndex:=2
              else ImageIndex:=3;
end;


Procedure GetSelNodeImages(Node: TTreeNode);
begin
  with Node do
    if Node.Parent=NIL then SelectedIndex:=0
    else if HasChildren AND NOT Expanded then SelectedIndex:=1
         else if HasChildren AND Expanded then SelectedIndex:=2
              else SelectedIndex:=3;
end;


Function NodeText(q: TpFIBQuery): string;
begin
  Result:=DelRSpace(q.Fields[0].AsString)+' - '+DelRSpace(q.Fields[1].AsString);
end;


Function NodeText1(q: TpFIBQuery): string;
begin
  Result:=DelRSpace(q.Fields[1].AsString);
end;


Procedure SaveNodesValue(tv: TTreeView);
var i: integer;
begin
  with tv do
    for i:=0 to Items.Count-1 do
      if Items[i].Data<>NIL then
      with TNodeData(Items[i].Data) do
        SaveValue:=Value;
end;


Procedure RestoreNodesValue(tv: TTreeView);
var i: integer;
begin
  with tv do
    for i:=0 to Items.Count-1 do
      if Items[i].Data<>NIL then
        with TNodeData(Items[i].Data) do
          Value:=SaveValue;
end;

{Procedure IterateOutLine(co: TCheckOutLine; ParentItem: integer; p: TIterateProcedure);
var i: integer;
begin
  p(TNodeData(co.Items[ParentItem].Data));
  i:=co.Items[ParentItem].GetFirstChild;
  while i<>-1 do
    begin
      IterateOutLine(co, i, p);
      i:=co.Items[ParentItem].GetNextChild(i);
    end;
end;}

Function  GetParent(Node: TTreeNode; Level: integer): TTreeNode;
var nd: TTreeNode;
begin
  nd:=Node;
  while (nd.Level<>Level) AND (nd.Parent<>NIL) do
    nd:= nd.Parent;
  Result:=nd;
end;


end.


