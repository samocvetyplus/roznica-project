unit Dst3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, db, DTotal, Menus, DBCtrls, Buttons,
  Mask, DBGridEh, Variants, ComDrv32, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  t_sinvid = record
              depid:integer;
              sinvid:Longint;
             end;
  t_massinvid = array of t_sinvid;

  TfmDst3 = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    pm2: TPopupMenu;                                                                                           
    miSupFilter: TMenuItem;
    miPriceFilter: TMenuItem;
    laDepFrom: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    laSup: TLabel;
    laPrice: TLabel;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    pa1: TPanel;
    Panel1: TPanel;
    laQC: TLabel;
    dtQT: TDBText;
    laWC: TLabel;
    dtWT: TDBText;
    Label2: TLabel;
    dtQDT: TDBText;
    Label3: TLabel;
    dtWDT: TDBText;
    Splitter2: TSplitter;
    paB: TPanel;
    dg1: TM207IBGrid;
    plFilter: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter7: TSplitter;
    Panel2: TPanel;
    dg3: TM207IBGrid;
    Splitter1: TSplitter;
    Label6: TLabel;
    DBText1: TDBText;
    Label8: TLabel;
    laInv: TLabel;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    siDTo: TSpeedItem;
    pmDTo: TPopupMenu;
    Label10: TLabel;
    edUID: TEdit;
    SpeedItem3: TSpeedItem;
    ceArt: TComboEdit;
    cbSearch: TCheckBox;
    plWh: TPanel;
    lbWHInfo: TLabel;
    txtArt: TDBText;
    txtQ: TDBText;
    Label7: TLabel;
    lbW: TLabel;
    txtW: TDBText;
    pm3: TPopupMenu;
    DArtId1: TMenuItem;
    N2: TMenuItem;
    lbCountry: TListBox;
    Splitter6: TSplitter;
    DBText2: TDBText;
    laFullArt: TLabel;
    laSZ: TLabel;
    dge2: TDBGridEh;
    lbAtt1: TListBox;
    lbAtt2: TListBox;
    Splitter8: TSplitter;
    Splitter10: TSplitter;
    dgWH: TDBGridEh;
    siHelp: TSpeedItem;
    Label9: TLabel;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure SetFilter(Sender: TObject);
    procedure paBResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure ViewClick(Sender: TObject);
    procedure AddClick(Sender: TObject);
    procedure DelClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure dg3Exit(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dge2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem3Click(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbSearchClick(Sender: TObject);
    procedure DArtId1Click(Sender: TObject);
    procedure dgWHDblClick(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgWHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
    procedure dge2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    { Private declarations }
    FirstBtn: TButton;
    StopFlag: boolean;
    NewSinv:integer;
    newsinvid:t_massinvid;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ClearFilter;
    procedure PMDToClick(Sender: TObject);
    procedure CMDialogKey(var Message: TCMDialogKey); message CM_DIALOGKEY;
    function  Add(Sender: TObject; Refrsh: boolean): integer;
    function  Stop: boolean;
    procedure visible_btn;
    procedure edtFindArt(Sender: TObject);
  public
   DToId: integer;
   function  Select_Sinvid (d:integer):integer;
  end;

var
  fmDst3: TfmDst3;

implementation

uses comdata, Data, Data2, DBTree, DSZ, Distred, M207Proc, ResQ, FIBQuery,
  ServData, MsgDialog, uUtils;

{$R *.DFM}
Function TfmDst3.Select_Sinvid (d:integer):integer;
var i:integer;
begin
 i:=0;
 while (i < length(newsinvid))and(newsinvid[i].depid<>d)do inc(i);
 if newsinvid[i].depid=d then result:=newsinvid[i].sinvid
 else result:=0;
end;

procedure TfmDst3.siExitClick(Sender: TObject);
begin
  if dm.DstOne2Many then
    begin
      with pc1 do
        if ActivePage.TabIndex=0 then Close
        else
          begin
            ActivePage:=Pages[0];
            pc1Change(NIL);
          end;
    end
  else Close;
end;

procedure TfmDst3.WMSysCommand(var Message: TWMSysCommand);
begin
 if Message.CmdType = SC_MINIMIZE THEN MinimizeApp
  else inherited;
end;


procedure TfmDst3.FormCreate(Sender: TObject);
  procedure CreateLbl(ACaption: string; var ATop: integer);
  begin
    with TPanel.Create(paB) do
      begin
        Caption:=ACaption;
        Top:=ATop;
        Height:=20;
        Left:=2;
        Width:=paB.Width-4;
        Alignment:=taCenter;
        Font.Color:=clNavy;
        with Font do
          Style:=Style+[fsBold];
        Parent:=paB;
        Inc(ATop,Height);
      end
  end;

  function CreateBtn(ACaption, AHint: string; ATag: integer;
                      var ATop: integer; oc: TNotifyEvent;
                      Bname:string): TButton;
  var b: TButton;
  begin
    b:= TButton.Create(paB);
    with b do
      begin
        Name:=BName;
        Caption:=ACaption;
        Hint:=AHint;
        ShowHint:=True;
        Top:=ATop;
        Left:=2;
        Height:=25;
        Width:=paB.Width-4;
        Parent:=paB;
        OnClick:=oc;
        Tag:=ATag;
        Inc(ATop,Height);
        Result:=b;
      end;
  end;

var t, i: integer;
    s: string;
    mi: TMenuItem;
    c: TColumnEh;
    btn: TButton;
    b: boolean;
begin
  NewSinv:=0;
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  DToId:=-1;

  with dm, dm2, dmcom do
    begin
      Distr:=True;
      Old_D_MatId:='.';
      D_WHArt:='';
      FillListBoxes(lbComp, lbMat, lbGood, lbIns,lbcountry,lbAtt1,lbAtt2);
      if DstOne2Many then
        begin
          lbComp.ItemIndex:=0;
          lbMat.ItemIndex:=0;
          lbGood.ItemIndex:=0;
          lbIns.ItemIndex:=0;
          lbCountry.ItemIndex:=0;
          lbAtt1.ItemIndex := 0;
          lbAtt2.ItemIndex := 0;
          D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
          D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
          D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
          D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
          D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
          D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
          D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
         // lbCompClick(NIL);
        end;

      PriceFilter:=False;
      SupFilter:=False;


      if WorkMode='DINV' then
        begin
          laDepFrom.Caption:=dm.DDepFrom;
          with pc1 do
            ActivePage:=Pages[0];
        end
      else
        begin
          laDepFrom.Caption:=dm.SDep;
          DstArt2Id:=taSElArt2Id.AsInteger;
          with pc1 do
            ActivePage:=Pages[1];
          SetFilter(NIL);
          ReOpenDataSets([quDst3, quD_Q3]);
        end;
    end;


  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              begin
                mi:=TMenuItem.Create(pmDTo);
                mi.Caption:=Fields[2].AsString;
                mi.Tag:=Fields[0].AsInteger;
                mi.OnClick:=PMDToClick;
                pmDTo.Items.Add(mi);
              end;
          Next;
        end;
      Active:=False;
    end;


  t:=2;
  b:=True;
  CreateLbl('��������', t);
  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              begin
                btn:=CreateBtn(Fields[2].AsString, '����������� �� '+Fields[1].AsString, Fields[0].AsInteger, t, AddClick,
                               'AB_'+Fields[0].AsString);
                if b then
                  begin
                    FirstBtn:=btn;
                    b:=False;
                  end;
              end;
          Next;
        end;
      Active:=False;
    end;
  CreateLbl('��������', t);
  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DstDepId then
            if dm.DstOne2Many or (Fields[0].AsInteger=dm.taDListDepId.AsInteger) then
              CreateBtn(Fields[2].AsString, '�������� ������������ ������� �� '+Fields[1].AsString, Fields[0].AsInteger, t, ViewClick,
                        'VB_'+Fields[0].AsString);
          Next;
        end;
      Active:=False;
    end;
  Inc(t, 10);
  CreateBtn('�������', '������� ������� �� �������� �����', -1, t, DelClick,'DelB');

  s:='';
  if dm.DstOne2Many then
    with dm do
      begin
       sWorkInvID:='';
       SetLength(newsinvid,0);

       qutmp.Close;
       quTmp.SQL.Text:='select d_depid, Sname from d_dep where d_depid<>-1000 and '+
                       ' isdelete <> 1 and  d_depid<>'+inttostr(DstDepId);
       qutmp.ExecQuery;
       while not qutmp.Eof do
        begin
         qutmp1.Close;
         qutmp1.SQL.Text:='select sinvid, sn from Seach_Sinvid( '+
                           qutmp.Fields[0].AsString+', '+inttostr(DstDepId)+', '+
                           inttostr(dmCom.UserId)+')';
         qutmp1.ExecQuery;
         if (qutmp1.Fields[0].AsInteger=0) then raise Exception.Create('��� �������� ��������� �� '+qutmp.Fields[1].asstring);

         s:=s+qutmp.Fields[1].AsString[1]+'-'+qutmp1.Fields[1].AsString+', ';
         sWorkInvID:=sWorkInvID+'*'+qutmp1.Fields[0].AsString+';';
         SetLength(newsinvid,length(newsinvid)+1);
         newsinvid[length(newsinvid)-1].depid:=qutmp.Fields[0].AsInteger;
         newsinvid[length(newsinvid)-1].sinvid:=qutmp1.Fields[0].AsInteger;

         qutmp.Next;
         qutmp1.Close;
        end;
        qutmp.Transaction.CommitRetaining;
        qutmp.Close;
        SetLength(s, Length(s)-2);
      end
   else   
   begin
    SetLength(newsinvid,1);
    newsinvid[0].sinvid:=dm.tadlistsinvid.asinteger;
    newsinvid[0].depid:=dm.tadlistdepid.asinteger;    
    s:=dm.taDListSN.AsString;
   end;

  laInv.Caption:=s;


  with dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dgWH.Columns.Add;
          c.Field:=quD_WH3.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RW_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;

          c:=dgWH.Columns.Add;
          c.Field:=quD_WH3.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:='�������|'+slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Footers.Add;
          c.Footers.Items[0].FieldName:='A_'+slDepDepId[i];
          c.Footers.Items[0].ValueType:=fvtSum;
          c.Footers.Add;
          c.Footers.Items[1].FieldName:='RQ_'+slDepDepId[i];
          c.Footers.Items[1].ValueType:=fvtSum;
          c.Width:=80;
        end;
    end;

{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;

  dmServ.ComScan.Connect;
  dmcom.SScanZ:='';   }

  dgWh.RestoreColumnsLayoutIni(GetIniFileName, Name+'_dgwh', [crpColIndexEh, crpColWidthsEh]);
  ActiveControl:=ceArt;
  ReOpenDataSets([dm2.quD_WH3]);
end;

procedure TfmDst3.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDst3.lbCompClick(Sender: TObject);
begin
  with dmCom, dm, dm2 do
    begin
      D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
      D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
      D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
      D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
      D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
      D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
      D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
//      ReOpenDataSets([quD_WH3]);
    end;
end;

procedure TfmDst3.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmDst3.SetFilter(Sender: TObject);
begin
  if Sender<>NIL then
    with TMenuItem(Sender) do
      Checked:=NOT Checked;
  with dm, dm2 do
    begin
      PriceFilter:=miPriceFilter.Checked;
      SupFilter:=miSupFilter.Checked;
      SZFilter:=true;

      PriceFilterValue:=quD_UID2Price.AsFloat;
      SupFilterId:=quD_UID2SupId.AsInteger;
      ReOpenDataSets([quD_UID2]);

      if PriceFilter then laPrice.Caption:=quD_UID2Price.DisplayText
      else laPrice.Caption:='���';
      if SupFilter then laSup.Caption:=quD_UID2Sup.DisplayText
      else laSup.Caption:='���';
    end;
end;


procedure TfmDst3.ClearFilter;
begin
  with dm, dm2 do
    begin
      PriceFilter:=False;
      SupFilter:=False;
      SZFilter:=False;

      miPriceFilter.Checked:=False;
      miSupFilter.Checked:=False;

      PriceFilterValue:=quD_UID2Price.AsFloat;
      SupFilterId:=quD_UID2SupId.AsInteger;
      SZFilterValue:=quD_UID2SZ.AsString;

      laPrice.Caption:='���';
      laSup.Caption:='���';
    end;
end;



procedure TfmDst3.paBResize(Sender: TObject);
var i: integer;
begin
  with paB do
    for i:=0 to ComponentCount-1 do
      begin
        if Components[i] is TSpeedButton then
           TSpeedButton(Components[i]).Width:=paB.Width-4;
        if Components[i] is TPanel then
           TPanel(Components[i]).Width:=paB.Width-4;

      end
end;

procedure TfmDst3.FormClose(Sender: TObject; var Action: TCloseAction);
var i:integer;
begin
  with dm, dm2 do
    begin
      CloseDataSets([quDst3, quD_UID2, quD_T2, quD_Q3, quD_WH_T]);
      if DstOne2Many then quD_WH3.Active:=False;
      Distr:=False;
    end;

    dm.ClosedInvId:=dm.taDListSInvId.AsInteger;
    for i:=0 to length(newsinvid)-1 do
    begin
     dm.taDList.Locate('SINVID', newsinvid[i].sinvid, []);
     dm.taDList.Refresh;
    end;
     dm.taDList.Locate('SINVID', dm.ClosedInvId, []);

{      ClosedInvId:=taDListSInvId.AsInteger;
      ReopenDatasets([taDList]);
      taDList.Locate('SINVID', ClosedInvId, []);}

    Finalize(newsinvid);

// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;   **********
 dgwh.SaveColumnsLayoutIni(GetIniFileName, Name+'_dgwh', true);
end;

procedure TfmDst3.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field<>NIL then
    if Field.FieldName='DEP' then Background:=dm2.quDst3Color.AsInteger
    else if NOT Highlight or dg1.ClearHighlight then
           if dm2.quDst3IsClosed.AsInteger=1 then Background:=clBtnFace
           else Background:=clAqua;
end;

procedure TfmDst3.ViewClick(Sender: TObject);
var s: string; i,sn:integer;
begin
  with dm do
    begin
      with quTmp do
        begin
         if DstOne2Many then
          begin
           sn:=Select_Sinvid(tcomponent(Sender).Tag);
           if (sn<>0) then
            sql.Text:='select sn, sinvid from sinv where sinvid='+inttostr(sn)
           else raise Exception.Create('����������� ��������� ������!');
          end
         else
           SQL.Text:='SELECT SN, SINVID FROM SINV WHERE ITYPE=2 AND DEPFROMID='+IntToStr(DDepFromId)+' AND DepId='+IntToSTr(TComponent(Sender).Tag)+' AND SN in ('+laInv.Caption+') AND FYEAR(SDATE)=FYEAR(''TODAY'') and CRUSERID='+inttostr(taDListCrUserId.AsInteger);
          ExecQuery;
          if (Fields[1].isnull)then raise Exception.Create('��������� �������!');
          s:=Fields[0].AsString;
          i:=Fields[1].AsInteger;
          Transaction.CommitRetaining;
          Close;
        end;

      try
        quDistred.Tag:=i;
        fmDistred:=TfmDistred.Create(NIL);
        fmDistred.Caption:=TButton(Sender).Caption+': ��������� �'+s;
        fmDistred.ShowModal;
      finally
        fmDistred.Free;
      end
    end;
end;


function TfmDst3.Add(Sender: TObject; Refrsh: boolean): integer;
var i,sn:integer;
begin
  Result := 0;
  with dm, dm2 do
    begin
      if quD_UID2SItemId.IsNull then Exit;
      with qutmp do
      begin
       Sql.Text := '  select uidwhcalc  from d_rec ';
       ExecQuery;
       i:= Fields[0].AsInteger;
       Transaction.CommitRetaining;
       close;
      end;
      If i = 1 then Raise Exception.Create('��������� ������ �����������');

      if quD_UID2FREPAIR.AsInteger=0 then
      begin
       with quInsDSItem, Params do
        begin
          ByName['SITEMID'].AsInteger:=quD_UID2SItemId.AsInteger;
          ByName['ART2ID'].AsInteger:=quD_UID2Art2Id.AsInteger;
          ByName['DEPFROMID'].AsInteger:=DstDepId;
          ByName['DEPID'].AsInteger:=TComponent(Sender).Tag;

          sn:=Select_Sinvid(TComponent(Sender).Tag);
          if sn<>0 then ByName['SINVID'].asinteger:=sn
          else raise Exception.Create('��������� �������');

          ExecQuery;
          Result:=Fields[0].AsInteger;
          dmcom.tr.CommitRetaining;
          Close;

          if Refrsh then
            begin
              quDst3.Append;
              quDst3SItemId.AsInteger:=Result;
              quDst3.Post;
            end;
        end;
       quD_UID2.Delete;
       quD_UID2.tAG:=quD_UID2.FieldByName('UID').AsInteger;
      end else ShowMessage('������� �� �������!');
    end;
end;

procedure TfmDst3.AddClick(Sender: TObject);
var i, j: integer;
    Refrsh: boolean;
begin
  i := 0;
  Refrsh:=dg1.GetSortField('SZ')=-1;
  with dmCom, dm , dm2 do
    begin

      if not Refrsh then
        quDst3.Active:=False;

      if dge2.SelectedRows.Count=0 then i:=Add(Sender, Refrsh)
      else
        begin
          for j:=0 to dge2.SelectedRows.Count-1 do
            begin
              try
                dge2.DataSource.DataSet.Bookmark:=dge2.SelectedRows[j];
                i:=Add(Sender, Refrsh);
              except
             end;
            end;
          dge2.SelectedRows.Clear;
        end;
//      tr.CommitRetaining;

      laSZ.Caption:=quD_UID2SZ.DisplayText;
      if DBText1.DataSource.DataSet.Active then laFullArt.Caption:=DBText1.Field.AsString;
      SetVisEnabled(TWinControl(laFullArt),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText1),pc1.ActivePage<>pc1.Pages[1]);
      SetVisEnabled(TWinControl(laSz),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText2),pc1.ActivePage<>pc1.Pages[1]);
//      dmCom.tr.Commit;
//      if not dmcom.tr.Active then  dmCom.tr.StartTransaction;
      ReOpenDataSets([quD_UID2,quD_T2, quD_Q3]);
      quD_UID2.Locate('UID',quD_UID2.TAG,[]);
      with quDst3 do
        try
          DisableControls;
          if not Active then Open;
          Locate('SITEMID', i, []);
          i:=RecNo;
          Last;
          MoveBy(i-RecordCount);
        finally
          EnableControls;
        end;
    end;
end;

procedure TfmDst3.DelClick(Sender: TObject);
begin
  with dm, dm2, quDst3 do
    begin
      if quDst3SItemId.IsNull or (quDst3IsClosed.AsInteger=1) then Exit;
      with quD_UID2 do
        begin
          Append;
          quD_UID2SItemId.AsInteger:=quDst3Ref.AsInteger;//quDst3SItemId.AsInteger;
          Post;
        end;
      Delete;
      dmCom.tr.CommitRetaining;
      ReOpenDataSets([quD_T2, quD_Q3]);
      SetVisEnabled(TWinControl(laFullArt),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText1),pc1.ActivePage<>pc1.Pages[1]);
      SetVisEnabled(TWinControl(laSz),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText2),pc1.ActivePage<>pc1.Pages[1]);
    end
end;

Procedure TfmDst3.visible_btn;
var i,sn:integer;
begin
 for i:=0 to dm2.slDepDepId.Count-1 do
 begin
  sn:=Select_Sinvid(strtoint(dm2.slDepDepId[i]));
  if sn<>0 then
   begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select isclosed, userid from sinv where sinvid='+inttostr(sn);
    dm.quTmp.ExecQuery;
    if (dm.quTmp.Fields[0].AsInteger=1)or(dm.quTmp.Fields[0].isnull) or
       ((not dm.quTmp.Fields[1].IsNull) and (dm.quTmp.Fields[1].AsInteger<>dmcom.UserId)) then
     with paB do
     tbutton(FindComponent('AB_'+dm2.slDepDepId[i])).Enabled:=false
    else
     with paB do
     tbutton(FindComponent('AB_'+dm2.slDepDepId[i])).Enabled:=true;
    dm.quTmp.Transaction.CommitRetaining; 
    dm.quTmp.Close;
   end;
 end;
end;

procedure TfmDst3.pc1Change(Sender: TObject);
begin
  with pc1, dm, dm2 do
   begin
    if ActivePage=Pages[1] then
      begin
        if (not quD_WH3.Active) then SysUtils.Abort;
        visible_btn;
        DstD_ArtId:=quD_WH3D_ArtId.AsInteger;
        SZFilterValue:=quD_WH3SZ.AsString;
        SetFilter(NIL);
        ReOpenDataSets([quDst3, quD_Q3]);
      end
    else
      begin
        CloseDataSets([quD_UID2, quD_Q3, quD_T2, quDst3]);
        ceArt.SelectAll;
        ActiveControl:=ceArt;
        ClearFilter;
        //REopenDatasets([quD_WH3]);
        if quD_WH3.Active then
        begin
         quD_WH3.Refresh;
         if quD_WH3FLAG.AsInteger=0 then
         begin
          qud_wh3.Locate('D_ARTID;FLAG',VarArrayOf([quD_WH3D_artid.AsInteger,1]),[]);
          quD_WH3.Refresh;
         end;
         dgwh.SumList.RecalcAll;
        end;
        ActiveControl:=ceArt;
      end;
      SetVisEnabled(TWinControl(laFullArt),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText1),pc1.ActivePage<>pc1.Pages[1]);
      SetVisEnabled(TWinControl(laSz),pc1.ActivePage=pc1.Pages[1]);
      SetVisEnabled(TWinControl(DBText2),pc1.ActivePage<>pc1.Pages[1]);
    end;
end;

procedure TfmDst3.dg3Exit(Sender: TObject);
begin
  PostDataSets([dm2.quD_Q3]);
end;

procedure TfmDst3.SpeedItem1Click(Sender: TObject);
begin
  dm.Old_D_CompId:=dmCom.D_CompId;
  dm.Old_D_MatId:=dmCom.D_MatId;
  dm.Old_D_GoodId:=dmCom.D_GoodId;
  dm.Old_D_InsId:=dmCom.D_InsId;
  dm.Old_D_CountryId:=dmCom.D_CountryId;
  dm.Old_D_Att1Id := dmCom.D_Att1Id;
  dm.Old_D_Att2Id := dmCom.D_Att2Id;
  dm.D_WHArt:=ceArt.Text;
  ReopenDataSets([dm2.quD_WH3]);
end;

procedure TfmDst3.PMDToClick(Sender: TObject);
begin
  DToId:=TMenuItem(Sender).Tag;
  siDTo.BtnCaption:=TMenuItem(Sender).Caption;
end;

procedure TfmDst3.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var T,i, sn: integer;
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        if DToId=-1 then raise Exception.Create('����� �� ������!!!');

        with qutmp do
        begin
         close;
         Sql.Text := '  select uidwhcalc  from d_rec ';
         ExecQuery;
         i:= Fields[0].AsInteger;
         Transaction.CommitRetaining;
         close;
        end;
        If i = 1 then Raise Exception.Create('��������� ������ �����������');

        sn:=Select_Sinvid(DToId);
       {����������� ������������� ���� ��������� � �� ����������}
        with qutmp do
        begin
         sql.Text:='select isclosed, sinvid, userid from sinv where sinvid='+inttostr(sn);
         ExecQuery;
         if Fields[1].IsNull then
         begin
          Transaction.CommitRetaining;
          close;
          raise Exception.Create('��������� �������')
         end
         else if Fields[0].AsInteger=1 then
              begin
               Transaction.CommitRetaining;
               close;
               raise Exception.Create('��������� �������')
              end
         else if (not Fields[2].IsNull) and (Fields[2].asinteger<>dmcom.UserId) then
          begin
           Transaction.CommitRetaining;
           close;
           raise Exception.Create('��������� ������� ��. �������������');
          end;
         Transaction.CommitRetaining;
         close;
        end;
        {********************************************************}


        with quInsDUID2, Params do
          begin
            ByName['UID'].AsInteger:=StrToInt(edUID.Text);

            sn:=Select_Sinvid(DToId);
            if sn<>0 then ByName['SINVID'].asinteger:=sn
            else raise Exception.Create('��������� �������');

            ByName['DEPID'].AsInteger:=DToId;
            ByName['DEPFROMID'].AsInteger:=DstDepId;

            ByName['OPT'].AsInteger:=0;
            ByName['CRUSERID'].AsInteger:=dmCom.UserId;
            ExecQuery;

            T:=FieldByName('T').AsInteger;
            tr.CommitRetaining;            
            Close;
          end;
        case T of
            2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
            3: MessageDialog('������� �������', mtInformation, [mbOK], 0);
            4: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);            
          end;
        quD_WH3.Refresh;
        edUID.Text:='';
      end;
end;

procedure TfmDst3.FormActivate(Sender: TObject);
begin
  if not dm.DstOne2Many then
    with pc1, dm, dm2 do
      begin
        ActivePage:=Pages[1];
        SetFilter(NIL);
        ReOpenDataSets([quDst3, quD_Q3]);
      end;

  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDst3.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then
    siExitClick(NIL);
end;

procedure TfmDst3.dge2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_TAB then Key:=0;
end;

procedure TfmDst3.CMDialogKey(var Message: TCMDialogKey);
begin
  if Message.CharCode=VK_TAB then
    begin
      if ActiveControl=dge2 then ActiveControl:=FirstBtn
      else if ActiveControl.Parent=paB then ActiveControl:=dge2
           else inherited;
    end
  else inherited;         
end;

procedure TfmDst3.SpeedItem3Click(Sender: TObject);
begin
  ShowAndFreeForm(TfmRestQ, Self, TForm(fmRestQ), True, False);
end;

procedure TfmDst3.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmDst3.ceArtChange(Sender: TObject);
begin
//
end;
procedure TfmDst3.edtFindArt(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    cbSearch.Tag:=0;
    if cbSearch.Checked then
    begin
      StopFlag:=False;
      with dm2.quD_WH3 do
      begin
        if not Active then Open;
        if not LocateF(dm2.quD_WH3, 'ART', ceArt.Text, [loBeginingPart], False, Stop) then
        begin
          dmCom.D_CompId:=-1;
          dmCom.D_MatId:= '*';
          dmCom.D_GoodId:= '*';
          dmCom.D_InsId:= '*';
          dmCom.D_CountryId:= '*';
          dmCom.D_Att1Id := -2;
          dmCom.D_Att2Id := -2;
          lbComp.ItemIndex:=0;
          lbMat.ItemIndex:=0;
          lbGood.ItemIndex:=0;
          lbIns.ItemIndex:=0;
          lbCountry.ItemIndex:=0;
          lbAtt1.ItemIndex := 0;
          lbAtt2.ItemIndex := 0;

          ReOpenDataSets([dm2.quD_WH3]);
          LocateF(dm2.quD_WH3, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
        end;
      end;
    end;
  finally
    Screen.Cursor:=crDefault;
  end
end;

procedure TfmDst3.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: with dm, dm2 do
                   begin
                     if cbSearch.Checked then
                          edtFindArt(ceArt)
//                       dgWhDblClick(NIL)
                     else {if cbSearch.Tag=0 then}
                            begin
                              dm.D_WHArt:=ceArt.Text;
                              ReopenDataSets([dm2.quD_WH3]);
                              cbSearch.Tag:=1;
                            end
{                          else
                            begin
                              dgWhDblClick(NIL);
                              cbSearch.Tag:=0;
                            end;              }
                   end;
      VK_ESCAPE: StopFlag:=True;                   
    end;
end;

procedure TfmDst3.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then
  begin
   ActiveControl:=dgWH;
  end
end;

function TfmDst3.Stop: boolean;
begin
  Result:=StopFlag;
end;


procedure TfmDst3.cbSearchClick(Sender: TObject);
begin
  if cbSearch.Checked then
    with dm do
      begin
        D_WHArt:='';
        ReopenDataSets([quD_WH]);
      end;
end;

procedure TfmDst3.DArtId1Click(Sender: TObject);
begin
  MessageDialog(dm2.quD_WH3D_ArtId.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmDst3.dgWHDblClick(Sender: TObject);
begin
  with pc1 do
    ActivePage:=Pages[1];
  pc1Change(NIL);
end;

procedure TfmDst3.dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  with dm2 do
   if Column.Field.Tag>0 then Background:=GetDepColor(Column.Field.Tag)
    else
    if (Column.Field.FieldName='DQ') or (Column.Field.FieldName='DW') then Background:=dmCom.clCream
     else
     if quD_WH3DQ.AsInteger=0 then Background:=clBtnFace
      else if quD_WH3Quantity.AsInteger=quD_WH3DQ.AsInteger then Background:=clInfoBk
           else Background:=clAqua;
end;

procedure TfmDst3.dgWHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then dgWHDblClick(NIL)
end;

procedure TfmDst3.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100225);
end;

procedure TfmDst3.dge2GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 with Column do
  if (Field.FieldName<>'PRICE') then
   case  dm2.quD_UID2FREPAIR.AsInteger of
    1: Background:=clBtnFace;
    else case dm2.quD_UID2ISCLOSED.AsInteger of
          0: Background:=dmCom.clMoneyGreen;
          1: Background:=clInfoBk;
        end;
  end;      
end;

end.




