unit OptList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl,
  StdCtrls, db, Menus, RxMenus, DBCtrls,
  DBGridEh, ActnList, TB2Item, PrnDbgeh, xmldom, XMLDoc, XMLIntf, RXStrUtils,
  jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmOptList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    laDepFrom: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    spitPrint: TSpeedItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    gridOptInv: TDBGridEh;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acView: TAction;
    ppPrint: TTBPopupMenu;
    ppDoc: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    acPrintFacture: TAction;
    acPrintInvoice: TAction;
    acPrintFactureItem: TAction;
    acPrintInvoiceItem: TAction;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    StatusBar1: TStatusBar;
    acChangePeriod: TAction;
    TBItem9: TTBItem;
    pggridOptInv: TPrintDBGridEh;
    acPrint: TAction;
    TBItem10: TTBItem;
    acPrintPril: TAction;
    acExportXml: TAction;
    svdFile: TSaveDialog;
    siExportXml: TSpeedItem;
    siExportText: TSpeedItem;
    acExportTextIncome: TAction;
    pmtext: TTBPopupMenu;
    acExportTextOutLine: TAction;
    biExporttextIncome: TTBItem;
    biExportTextOutLine: TTBItem;
    siHelp: TSpeedItem;
    Label1: TLabel;
    SpeedItem1: TSpeedItem;
    TBItem4: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure gridOptInvDblClick(Sender: TObject);
    procedure acPrint1Execute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure gridOptInvGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintPrilExecute(Sender: TObject);
    procedure acExportXmlUpdate(Sender: TObject);
    procedure acExportXmlExecute(Sender: TObject);
    procedure acExportTextIncomeExecute(Sender: TObject);
    procedure acExportTextOutLineExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure gridOptInvKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
    procedure TBItem4Click(Sender: TObject);
  private
    FLogOperIdForm : string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  end;

var
  fmOptList: TfmOptList;

implementation

uses comdata, Data, SInv, DInv, Opt, ReportData, Period, M207Proc, Data3,
  JewConst, {ImportData,} MsgDialog, dbUtil;

{$R *.DFM}

(* �������� ��������� ���������� � dm.OptDepClick *)

procedure TfmOptList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp else inherited;
end;

procedure TfmOptList.FormCreate(Sender: TObject);
begin
  laDepFrom.Caption:='';
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  //ppPrint.Skin := dmCom.PopupSkin;
  //ppDoc.Skin := dmCom.PopupSkin;
  TBItem4.Visible := CenterDep;
  with dmCom do
    begin
      tr.Active:=True;
      tr.CommitRetaining;
      taRec.Active:=True;
    end;
  dm.WorkMode:='OPT';
  dm.SetBeginDate;
  ShowPeriod;
  FLogOperIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmOptList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      CloseDataSets([dm.taOptList, taRec]);
      tr.CommitRetaining;
    end;
  dm.WorkMode:='';
end;

procedure TfmOptList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmOptList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmOptList.FormActivate(Sender: TObject);
begin
  dm.ClickUserDepMenu(dm.pmOptList);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmOptList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taOptList]);
      ShowPeriod;
    end;
end;

procedure TfmOptList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmOptList.acAddExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_AddOptSell, FLogOperIdForm);
  dm.taOptList.Append;
  PostDataSets([dm.taOptList]);
  ShowAndFreeForm(TfmOpt, Self, TForm(fmOpt), True, False);
  dm.taOptList.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmOptList.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dm.taOptList.Active;
end;

procedure TfmOptList.acDelExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_DelOptSell, FLogOperIdForm);
  dm.taOptList.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmOptList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dm.taOptList.Active and (not dm.taOptList.IsEmpty);
end;

procedure TfmOptList.acViewExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  dm.taOptList.Refresh;
  if dm.taOptListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSets([dm.taOptList]);
  end else begin
   LogOperationID := dm3.insert_operation(sLog_ViewOptSell, FLogOperIdForm);
   ShowAndFreeForm(TfmOpt, Self, TForm(fmOpt), True, False);
   dm3.update_operation(LogOperationID);
  end; 
end;

procedure TfmOptList.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dm.taOptList.Active and (not dm.taOptList.IsEmpty) and
    (dm.taOptListIType.AsInteger=3);
end;

procedure TfmOptList.gridOptInvDblClick(Sender: TObject);
begin
  acView.Execute;
end;

procedure TfmOptList.acPrint1Execute(Sender: TObject);
var
  LogOperationID: string;
begin
  case (Sender as TAction).Tag of
    1 : begin
      LogOperationID := dm3.insert_operation(sLog_OptPrintInvoice, FLogOperIdForm);
      dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), invoice_opt);
      dm3.update_operation(LogOperationID);
    end;
    2 : begin
      LogOperationID := dm3.insert_operation(sLog_OptPrintFacture, FLogOperIdForm);
      dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), facture_opt);
      dm3.update_operation(LogOperationID);
    end;
    3 : begin
      LogOperationID := dm3.insert_operation(sLog_OptPrintInvoiceUID, FLogOperIdForm);
      if dm.taOptListIType.AsInteger=3
      then dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), invoice_opt_u);
      dm3.update_operation(LogOperationID);
    end;
    4 : begin
      LogOperationID := dm3.insert_operation(sLog_OptPrintFactureUID, FLogOperIdForm);
      if dm.taOptListIType.AsInteger=3
      then dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), facture_opt_u);
      dm3.update_operation(LogOperationID);
    end;
  end;
end;

procedure TfmOptList.acPrintUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := dm.taOptList.Active and (not dm.taOptList.IsEmpty);
end;

procedure TfmOptList.gridOptInvGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (not dm.taOptListIType.IsNull) and (dm.taOptListIType.AsInteger<>3) then Background:=clTeal
  else if dm.taOptListIsClosed.AsInteger=0 then Background:=clInfoBk
       else Background:=clBtnFace;
end;

procedure TfmOptList.acPrintExecute(Sender: TObject);
begin
 pggridOptInv.Preview;
end;

procedure TfmOptList.acPrintPrilExecute(Sender: TObject);
begin
  if dm.taOptListIType.AsInteger=3 then
    dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), facture_opt_pril);
end;

procedure TfmOptList.acExportXmlUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep and dmcom.Adm;
end;

procedure TfmOptList.acExportXmlExecute(Sender: TObject);
var Att, FilterN, ItemFilter, ItemFilter1 : IXMLNode;
    Path, sDir:string;
    uid:integer;
begin    {
 sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
 svdFile.InitialDir:=sDir;
 svdFile.DefaultExt:='xml';

 svdFile.FileName:='���_����_'+trim(dm.taOptListSN.AsString);

 if svdFile.Execute then  Path:=svdFile.FileName
 else exit; }
 {
 with dmimp do
 begin
  quExportDep.Active:=false;
  quExportDep.ParamByName('DEPID').AsInteger:=209;
  quExportDep.ParamByName('OPTSINVID').AsInteger:=dm.taOptListSINVID.AsInteger;
  quExportDep.Active:=true;
  xmlDoc.Active:=true;
  while not quExportDep.Eof do
  begin
   FilterN:=xmlDoc.DocumentElement.AddChild('_'+trim(quExportDepUID.AsString));
   Att := XMLdoc.CreateNode('SSF', ntAttribute);
   Att.NodeValue := trim(quExportDepSSF.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SUPID', ntAttribute);
   Att.NodeValue := trim(quExportDepSUPID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('NDSID', ntAttribute);
   Att.NodeValue := trim(quExportDepNDSID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('PRICE', ntAttribute);
   Att.NodeValue := trim(quExportDepPRICE.AsString);
   FilterN.AttributeNodes.Add(Att);

   Att := XMLdoc.CreateNode('W', ntAttribute);
   Att.NodeValue := trim(quExportDepW.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SZ', ntAttribute);
   Att.NodeValue := trim(quExportDepSz.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODS_SAM1', ntAttribute);
   Att.NodeValue := trim(quExportDepD_GOODS_SAM1.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODS_SAM2', ntAttribute);
   Att.NodeValue := trim(quExportDepD_GOODS_SAM2.AsString);
   FilterN.AttributeNodes.Add(Att);

   Att := XMLdoc.CreateNode('ART2', ntAttribute);
   Att.NodeValue := trim(quExportDepART2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ART', ntAttribute);
   Att.NodeValue := trim(quExportDepART.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_COMPID', ntAttribute);
   Att.NodeValue := trim(quExportDepD_COMPID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODID', ntAttribute);
   Att.NodeValue := trim(quExportDepD_GOODID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_INSID', ntAttribute);
   Att.NodeValue := trim(quExportDepD_INSID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_MATID', ntAttribute);
   Att.NodeValue := trim(quExportDepD_MATID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('UNITID', ntAttribute);
   Att.NodeValue := trim(quExportDepUNITID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_COUNTRYID', ntAttribute);
   Att.NodeValue := trim(quExportDepD_COUNTRYID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ATT1', ntAttribute);
   Att.NodeValue := trim(quExportDepATT1.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ATT2', ntAttribute);
   Att.NodeValue := trim(quExportDepATT2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('PRICE2', ntAttribute);
   Att.NodeValue := trim(quExportDepPRICE2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('NDATE', ntAttribute);
   Att.NodeValue := trim(quExportDepNDATE.AsString);
   FilterN.AttributeNodes.Add(Att);


    ItemFilter:=FilterN.AddChild('INS');
    ItemFilter1 := XMLdoc.CreateNode('INSID', ntAttribute);
    ItemFilter1.NodeValue := trim(quExportDepINSID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('QUANTITY',ntAttribute);
    if quExportDepQUANTITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepQUANTITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('WEIGTH',ntAttribute);
    if quExportDepWEIGTH.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepWEIGTH.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('COLOR',ntAttribute);
    if quExportDepCOLOR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCOLOR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CHROMATICITY',ntAttribute);
    if quExportDepCHROMATICITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCHROMATICITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CLEANNES',ntAttribute);
    if quExportDepCLEANNES.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCLEANNES.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('GR',ntAttribute);
    if quExportDepGR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepGR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('MAIN',ntAttribute);
    if quExportDepMAIN.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepMAIN.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGTYPEID',ntAttribute);
    if quExportDepEDGTYPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepEDGTYPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGSHAPEID',ntAttribute);
    if quExportDepEDGSHAPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepEDGSHAPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);

   uid:=quExportDepUID.AsInteger;
   quExportDep.Next;

   while (quExportDepUID.AsInteger=uid) and (not quExportDep.Eof) do
   begin
    ItemFilter:=FilterN.AddChild('INS');
    ItemFilter1 := XMLdoc.CreateNode('INSID', ntAttribute);
    ItemFilter1.NodeValue := trim(quExportDepINSID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('QUANTITY',ntAttribute);
    if quExportDepQUANTITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepQUANTITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('WEIGTH',ntAttribute);
    if quExportDepWEIGTH.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepWEIGTH.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('COLOR',ntAttribute);
    if quExportDepCOLOR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCOLOR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CHROMATICITY',ntAttribute);
    if quExportDepCHROMATICITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCHROMATICITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CLEANNES',ntAttribute);
    if quExportDepCLEANNES.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepCLEANNES.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('GR',ntAttribute);
    if quExportDepGR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepGR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('MAIN',ntAttribute);
    if quExportDepMAIN.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepMAIN.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGTYPEID',ntAttribute);
    if quExportDepEDGTYPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepEDGTYPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGSHAPEID',ntAttribute);
    if quExportDepEDGSHAPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepEDGSHAPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);

    uid:=quExportDepUID.AsInteger;
    quExportDep.Next;
   end;
  end;
  XMLdoc.SaveToFile(Path);
  XMLdoc.Active := False;
  MessageDialog('������� ��������!', mtInformation, [mbOk], 0);
//  xmlDoc.Active:=false;
 end}
end;

procedure TfmOptList.acExportTextIncomeExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='���_����_����_'+trim(dm.taOptListSN.AsString);
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if not svdFile.Execute then exit;

  try
   Path:=svdFile.FileName;
   AssignFile(ftext, Path);
   Rewrite(ftext);

   with dm do
   begin
    qutmp.Close;
    qutmp.SQL.Text:='select W, ART, ART2, PRICE, UNITID, Q from TEXTOPTPRICE('+
                    taOptListSINVID.AsString+', 0)';
    quTmp.ExecQuery;
    while not qutmp.Eof do
    begin
     writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
          [qutmp.fields[1].AsString,'|', qutmp.fields[2].AsString, '|', qutmp.fields[3].AsFloat, '|',
           GetUNIT(qutmp.fields[4].AsInteger), '|', GetUNITQ(qutmp.fields[4].AsInteger,
           qutmp.fields[5].AsInteger,qutmp.fields[0].AsFloat), '|']));
     qutmp.Next;
    end;
    MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
   end;
   finally
    CloseFile(fText);
    dm.qutmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
    Screen.Cursor:=crDefault;
  end
end;

procedure TfmOptList.acExportTextOutLineExecute(Sender: TObject);
var sDir, Path:string;
    ftext:textfile;
 Function GetUNIT(UINTID:integer):string;
 begin
  if UINTID=1 then result:='��'
  else result:='��'
 end;

 Function GetUNITQ(UINTID:integer; Q:integer; w:real):string;
 begin
  if UINTID=1 then result:=floattostr(w)
  else result:=inttostr(q)
 end;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  Screen.Cursor:=crSQLWait;
 //�������� � ��������� ����
  svdFile.FileName:='���_����_����_'+trim(dm.taOptListSN.AsString);
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if not svdFile.Execute then exit;

  try
   Path:=svdFile.FileName;
   AssignFile(ftext, Path);
   Rewrite(ftext);

   with dm do
   begin
    qutmp.Close;
    qutmp.SQL.Text:='select W, ART, ART2, PRICE, UNITID, Q from TEXTOPTPRICE('+
                    taOptListSINVID.AsString+', 1)';
    quTmp.ExecQuery;
    while not qutmp.Eof do
    begin
     writeln(ftext,format('%15s %1s %10s %1s %10.2f %1s %5s %1s %10s %1s',
          [qutmp.fields[1].AsString,'|', qutmp.fields[2].AsString, '|', qutmp.fields[3].AsFloat, '|',
           GetUNIT(qutmp.fields[4].AsInteger), '|', GetUNITQ(qutmp.fields[4].AsInteger,
           qutmp.fields[5].AsInteger,qutmp.fields[0].AsFloat), '|']));
     qutmp.Next;
    end;
    MessageDialog('�������� � ��������� ���� ���������!', mtWarning, [mbOk], 0);
   end;
   finally
    CloseFile(fText);
    dm.qutmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
    Screen.Cursor:=crDefault;
  end
end;

procedure TfmOptList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100251)
end;

procedure TfmOptList.gridOptInvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN, VK_SPACE: if (dm.taOptList.Active and (not dm.taOptList.IsEmpty) and
    (dm.taOptListIType.AsInteger=3)) then acViewExecute(nil);
 end;
end;

procedure TfmOptList.SpeedItem1Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taOptList);
  tr.CommitRetaining;
  ReOpenDataSet(taOptList);
  taOptList.Refresh;
  end;
end;

procedure TfmOptList.TBItem4Click(Sender: TObject);
begin
    dmReport.PrintDocumentA(gen_arr(gridOptInv, dm.taOptListSINVID), invoice_opt_pr);
end;

end.
