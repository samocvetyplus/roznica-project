object fmRepSetting: TfmRepSetting
  Left = 250
  Top = 200
  BorderStyle = bsDialog
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080' '#1076#1083#1103' '#1087#1077#1088#1077#1089#1099#1083#1082#1080
  ClientHeight = 223
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dg1: TDBGridEh
    Left = 0
    Top = 0
    Width = 233
    Height = 223
    Align = alLeft
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsRepSetting
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1084#1072#1075#1072#1079#1080#1085#1072
        Width = 192
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pInfo: TPanel
    Left = 233
    Top = 0
    Width = 455
    Height = 223
    Align = alClient
    TabOrder = 1
    object LIsRecord: TLabel
      Left = 16
      Top = 24
      Width = 180
      Height = 13
      Caption = #1050#1086#1076' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103' '#1076#1083#1103' '#1087#1077#1088#1077#1089#1099#1083#1082#1080
    end
    object LGenOffSet: TLabel
      Left = 24
      Top = 96
      Width = 203
      Height = 26
      Caption = 
        #1047#1085#1072#1095#1077#1085#1080#1077', '#1089' '#1082#1086#1090#1086#1088#1086#1075#1086' '#1085#1072#1095#1080#1085#1072#1077#1090#1089#1103' '#1086#1090#1095#1077#1090' '#13#10#1075#1077#1085#1077#1088#1072#1090#1086#1088#1086#1074' '#1074' '#1074#1099#1073#1088#1072#1085#1085#1086#1084' ' +
        #1084#1072#1075#1072#1079#1080#1085#1077
    end
    object dbIsrecord: TDBEdit
      Left = 208
      Top = 24
      Width = 121
      Height = 21
      DataField = 'R_CODE'
      DataSource = dm3.dsRepSetting
      TabOrder = 0
    end
    object dbR_GenOffSet: TDBEdit
      Left = 248
      Top = 96
      Width = 121
      Height = 21
      DataField = 'R_GENOFFSET'
      DataSource = dm3.dsRepSetting
      TabOrder = 1
    end
    object dbR_UserDep: TDBCheckBox
      Left = 23
      Top = 144
      Width = 239
      Height = 17
      Alignment = taLeftJustify
      Caption = #1055#1077#1088#1077#1089#1099#1083#1082#1072' '#1085#1072' '#1092#1080#1083#1080#1072#1083
      DataField = 'R_USERDEP'
      DataSource = dm3.dsRepSetting
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object dbHere: TDBCheckBox
      Left = 24
      Top = 168
      Width = 239
      Height = 17
      Alignment = taLeftJustify
      Caption = #1055#1077#1088#1077#1089#1099#1083#1082#1072' '#1089' '#1101#1090#1086#1075#1086' '#1092#1080#1083#1080#1072#1083#1072
      DataField = 'HERE'
      DataSource = dm3.dsRepSetting
      TabOrder = 3
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object btAddRecord: TButton
      Left = 24
      Top = 56
      Width = 209
      Height = 17
      Action = acAddRecord
      TabOrder = 4
    end
    object btDelRecord: TButton
      Left = 232
      Top = 56
      Width = 209
      Height = 17
      Action = acDelRecord
      TabOrder = 5
    end
    object dbIsftpRep: TDBCheckBox
      Left = 24
      Top = 192
      Width = 239
      Height = 17
      Alignment = taLeftJustify
      Caption = #1055#1077#1088#1077#1076#1072#1095#1072' '#1087#1086' FTP'
      DataField = 'ISFTPREP'
      DataSource = dm3.dsRepSetting
      TabOrder = 6
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object btGenSetting: TButton
      Left = 374
      Top = 94
      Width = 75
      Height = 25
      Action = acGenStting
      TabOrder = 7
    end
  end
  object acList: TActionList
    Left = 568
    Top = 152
    object acAddRecord: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077' '#1074' '#1087#1077#1088#1077#1089#1099#1083#1082#1091
      OnExecute = acAddRecordExecute
      OnUpdate = acAddRecordUpdate
    end
    object acDelRecord: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077' '#1080#1079' '#1087#1077#1088#1077#1089#1099#1083#1082#1080
      OnExecute = acDelRecordExecute
      OnUpdate = acDelRecordUpdate
    end
    object acGenStting: TAction
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      OnExecute = acGenSttingExecute
      OnUpdate = acGenSttingUpdate
    end
  end
end
