object fmShopReport_CheckData: TfmShopReport_CheckData
  Left = 247
  Top = 160
  Width = 725
  Height = 604
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1076#1072#1085#1085#1099#1093' '#1090#1086#1074#1072#1088#1085#1086#1075#1086' '#1086#1090#1095#1077#1090#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object gridCheckData: TDBGridEh
    Left = 0
    Top = 40
    Width = 717
    Height = 426
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsrShopReport_CheckData
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghPreferIncSearch, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridCheckDataGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'Q0'
        Footers = <>
        Title.TitleButton = True
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'WHBC2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'WHEC2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'D+C2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'D-C2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'APC2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'SELLC2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'AAC2'
        Footers = <>
        Title.Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103' '#1088#1072#1089#1093'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RETC2'
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'C2'
        Footers = <>
        Title.TitleButton = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 717
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      Hint = #1042#1099#1093#1086#1076'|'
      ImageIndex = 0
      Spacing = 1
      Left = 650
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acRefresh
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C16421642164216421642164216421642164216421642
        164216421F7C1F7C1F7C1642BF6B7A577B577E5B5D535D4F3E4F3E4B3E4F3E4F
        5E4F16421F7C1F7C1F7C1642BF6F354BA81EF13686168616CF2A1A473D4B3D4B
        5E4F16421F7C1F7C1F7C163ADF737957840E6002600260026002A8163C4B3D4B
        5E4F16421F7C1F7C1F7C163ADF77795B840E600285123747F1366106F2323D4F
        5E4F16421F7C1F7C1F7C3742FF7B795F820A60026106133F7E5F1747F0365E53
        5E4F16421F7C1F7C1F7C3742FF7FBD6F7653554F354B354B7E637E5F564B5E57
        5E5316421F7C1F7C1F7C7942FF7F975BDF77BE6F564F354B354736477D5B7E5B
        7E5716421F7C1F7C1F7C7942FF7F5147985FDF77334761066002820A7E637E5F
        7F5B16421F7C1F7C1F7CBB42FF7F534F61065247785BA6166002840E9E679E63
        7E5B16421F7C1F7C1F7CBB42FF7FFE7BC8226002600260026002840EBF6F7C63
        F85616421F7C1F7C1F7CDC46FF7FFF7FDC733043C71EA71A3143A81E153A173E
        173E16421F7C1F7C1F7CDC46FF7FFF7FFF7FFF7FDD77DC73FF7F17573436DD3A
        9D2618361F7C1F7C1F7CFD4AFF7FFF7FFF7FFF7FFF7FFF7FFF7F5C6B173E1F3B
        593A1F7C1F7C1F7C1F7CFD4ADF7BDF7BDF7BDF7BDF7BDF7BDF7B5C67173E7942
        1F7C1F7C1F7C1F7C1F7CFD4ABB42BB42BB42BB42BB42BB42BB42BB42173E1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acRefreshExecute
      SectionName = 'Untitled (0)'
    end
    object siClear: TSpeedItem
      Action = acClear
      BtnCaption = #1054#1095#1080#1089#1090#1080#1090#1100
      Caption = 'siClear'
      ImageIndex = 80
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acClearExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acUIDHistory
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acUIDHistoryExecute
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 556
    Width = 717
    Height = 19
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 466
    Width = 717
    Height = 90
    Align = alBottom
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 108
      Height = 13
      Caption = #1042#1093#1086#1076#1103#1097#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
    end
    object Label2: TLabel
      Left = 8
      Top = 28
      Width = 21
      Height = 13
      Caption = #1042#1055'+'
    end
    object Label3: TLabel
      Left = 8
      Top = 44
      Width = 18
      Height = 13
      Caption = #1042#1055'-'
    end
    object Label4: TLabel
      Left = 452
      Top = 24
      Width = 81
      Height = 13
      Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
    end
    object Label5: TLabel
      Left = 172
      Top = 28
      Width = 103
      Height = 13
      Caption = #1056#1086#1079#1085#1080#1095#1085#1099#1077' '#1087#1088#1086#1076#1072#1078#1080
    end
    object Label6: TLabel
      Left = 172
      Top = 44
      Width = 108
      Height = 13
      Caption = #1056#1086#1079#1085#1080#1095#1085#1099#1077' '#1074#1086#1079#1074#1088#1072#1090#1099
    end
    object txtDP: TDBText
      Left = 40
      Top = 28
      Width = 26
      Height = 13
      AutoSize = True
      DataField = 'D+C2'
      DataSource = dsrT
    end
    object txtDM: TDBText
      Left = 40
      Top = 44
      Width = 28
      Height = 13
      AutoSize = True
      DataField = 'D-C2'
      DataSource = dsrT
    end
    object txtI: TDBText
      Left = 128
      Top = 8
      Width = 14
      Height = 13
      AutoSize = True
      DataField = 'WHBC2'
      DataSource = dsrT
    end
    object txtSell: TDBText
      Left = 292
      Top = 28
      Width = 28
      Height = 13
      AutoSize = True
      DataField = 'SELLC2'
      DataSource = dsrT
    end
    object txtSellRet: TDBText
      Left = 292
      Top = 44
      Width = 45
      Height = 13
      AutoSize = True
      DataField = 'RETC2'
      DataSource = dsrT
    end
    object Label7: TLabel
      Left = 8
      Top = 64
      Width = 115
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
    end
    object txtOut: TDBText
      Left = 128
      Top = 65
      Width = 28
      Height = 13
      AutoSize = True
      DataField = 'WHEC2'
      DataSource = dsrT
    end
    object txtAP: TDBText
      Left = 540
      Top = 24
      Width = 25
      Height = 13
      AutoSize = True
      DataField = 'APC2'
      DataSource = dsrT
    end
    object Label8: TLabel
      Left = 288
      Top = 64
      Width = 110
      Height = 13
      Caption = #1056#1072#1089#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
    end
    object txtR: TDBText
      Left = 408
      Top = 64
      Width = 19
      Height = 13
      AutoSize = True
      DataField = 'T'
      DataSource = dsrT
    end
    object Label9: TLabel
      Left = 452
      Top = 40
      Width = 69
      Height = 13
      Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
    end
    object txtAAC2: TDBText
      Left = 540
      Top = 40
      Width = 65
      Height = 17
      DataField = 'AAC2'
      DataSource = dsrT
    end
  end
  object taShopReport_CheckData: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, USERID, UID, Q0,'
      '    WHBC2, WHEC2, "D+C2", "D-C2",'
      '    APC2, SELLC2, RETC2, C2, AAC2'
      'from TMP_CHECKDATA_SHOPREPORT'
      'where USERID=:USERID'
      'order by UID')
    BeforeOpen = taShopReport_CheckDataBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 76
    Top = 156
    poSQLINT64ToBCD = True
    object taShopReport_CheckDataID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taShopReport_CheckDataUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taShopReport_CheckDataUID: TFIBIntegerField
      DisplayLabel = #1048#1076'.'#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taShopReport_CheckDataQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taShopReport_CheckDataWHBC2: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076'. '#1088#1072#1089#1093'.'#1089#1090#1086#1080#1086#1084#1089#1090#1100
      FieldName = 'WHBC2'
      currency = True
    end
    object taShopReport_CheckDataWHEC2: TFIBFloatField
      DisplayLabel = #1048#1089#1093'. '#1088#1072#1089#1093'. '#1089#1090#1086#1080#1086#1084#1089#1090#1100
      FieldName = 'WHEC2'
      currency = True
    end
    object taShopReport_CheckDataDC2: TFIBFloatField
      DisplayLabel = #1042#1055'+ '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'D+C2'
      currency = True
    end
    object taShopReport_CheckDataDC22: TFIBFloatField
      DisplayLabel = #1042#1055'- '#1088#1072#1089#1093'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'D-C2'
      currency = True
    end
    object taShopReport_CheckDataAPC2: TFIBFloatField
      DisplayLabel = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'APC2'
      currency = True
    end
    object taShopReport_CheckDataSELLC2: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072' '#1088#1072#1089#1093'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SELLC2'
      currency = True
    end
    object taShopReport_CheckDataRETC2: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
      FieldName = 'RETC2'
      currency = True
    end
    object taShopReport_CheckDataC2: TFIBFloatField
      DisplayLabel = #1056#1072#1089#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'C2'
      currency = True
    end
    object taShopReport_CheckDataAAC2: TFIBFloatField
      FieldName = 'AAC2'
    end
  end
  object dsrShopReport_CheckData: TDataSource
    DataSet = taShopReport_CheckData
    Left = 80
    Top = 208
  end
  object acEvent: TActionList
    Left = 196
    Top = 156
    object acRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      OnExecute = acRefreshExecute
    end
    object acClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      ImageIndex = 0
      OnExecute = acClearExecute
    end
    object acUIDHistory: TAction
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      OnExecute = acUIDHistoryExecute
      OnUpdate = acUIDHistoryUpdate
    end
  end
  object taT: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(WHBC2) WHBC2, sum(WHEC2) WHEC2, '
      '    sum("D+C2") "D+C2", sum("D-C2") "D-C2",'
      
        '    sum(APC2) APC2, sum(SELLC2) SELLC2, sum(RETC2) RETC2, sum(AA' +
        'C2) AAC2'
      'from TMP_CHECKDATA_SHOPREPORT'
      'where USERID=:USERID')
    BeforeOpen = taTBeforeOpen
    OnCalcFields = taTCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 32
    Top = 264
    poSQLINT64ToBCD = True
    object taTWHBC2: TFIBFloatField
      FieldName = 'WHBC2'
      currency = True
    end
    object taTWHEC2: TFIBFloatField
      FieldName = 'WHEC2'
      currency = True
    end
    object taTDC2: TFIBFloatField
      FieldName = 'D+C2'
      currency = True
    end
    object taTDC22: TFIBFloatField
      FieldName = 'D-C2'
      currency = True
    end
    object taTAPC2: TFIBFloatField
      FieldName = 'APC2'
      currency = True
    end
    object taTSELLC2: TFIBFloatField
      FieldName = 'SELLC2'
      currency = True
    end
    object taTRETC2: TFIBFloatField
      FieldName = 'RETC2'
      currency = True
    end
    object taTT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'T'
      currency = True
      Calculated = True
    end
    object taTAAC2: TFIBFloatField
      FieldName = 'AAC2'
    end
  end
  object dsrT: TDataSource
    DataSet = taT
    Left = 32
    Top = 312
  end
end
