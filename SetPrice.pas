unit SetPrice;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, Menus, StdCtrls, Mask, DBCtrls, jpeg,
  rxPlacemnt, rxSpeedbar;

type
  TfmSetPrice = class(TForm)
    sb1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    M207IBGrid1: TM207IBGrid;
    pm1: TPopupMenu;
    N1: TMenuItem;
    siSet: TSpeedItem;
    siBack: TSpeedItem;
    miApplyMargin: TMenuItem;
    siCopy: TSpeedItem;
    siMargin: TSpeedItem;
    miReturnPrice: TMenuItem;
    N4: TMenuItem;
    tb2: TSpeedBar;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siSetClick(Sender: TObject);
    procedure siBackClick(Sender: TObject);
    procedure siMarginClick(Sender: TObject);
    procedure siCopyClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ExecSQLQuery(q: string);
    procedure ShowMargin;
  public
    { Public declarations }
  end;

var
  fmSetPrice: TfmSetPrice;

implementation

uses comdata, Data, Data2, DepMarg, M207Proc;

{$R *.DFM}

procedure TfmSetPrice.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmSetPrice.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSetPrice.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmSetPrice.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
end;

procedure TfmSetPrice.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    begin
      PostDataSets([taSetPrice]);
      CloseDataSets([taSetPrice]);
    end;
end;

procedure TfmSetPrice.M207IBGrid1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='NAME') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmSetPrice.siSetClick(Sender: TObject);
begin
  with dm do
    begin
      ExecSQLQuery('execute procedure ApplyMargin '+IntToStr(Art2Id));
      UseMargin:=True;
      ShowMargin;
    end;
end;

procedure TfmSetPrice.siBackClick(Sender: TObject);
begin
  with dm do
    begin
      ExecSQLQuery('update Price set TPrice=Price2, UseMargin=0 where Art2Id='+IntToStr(Art2Id));
      ExecSQLQuery('update Art2 set TOptPrice=OptPrice where Art2Id='+IntToStr(Art2Id));      
      UseMargin:=True;
      ShowMargin;
    end;
end;

procedure TfmSetPrice.siMarginClick(Sender: TObject);
begin
  PostDataSets([dm.taSetPrice]);
  ShowAndFreeForm(TfmDepMargin, Self, TForm(fmDepMargin), True, False);
end;

procedure TfmSetPrice.siCopyClick(Sender: TObject);
begin
  with dm do
    ExecSQLQuery('execute procedure CopyPrice '+
                     taSetPriceDepId.AsString+', '+
                     IntToStr(Art2Id));
end;

procedure TfmSetPrice.ExecSQLQuery(q: string);
begin
  with dm, dmCom do
    begin
      PostDataSets([dm.taSetPrice]);
      with quTmp do
        begin
          SQL.Text:=q;
          ExecQuery;
        end;
      tr.CommitRetaining;
      with taSetPrice do
        begin
          Active:=False;
          Open;
        end
    end;
end;

procedure TfmSetPrice.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;
end;

procedure TfmSetPrice.FormActivate(Sender: TObject);
begin
{  with dm do
    begin
      siSet.Down:=UseMargin;
      siSet.Down:=UseMargin;
      miApplyMargin.Checked:=UseMargin;
      miReturnPrice.Checked:=NOT UseMargin
    end;}
end;

procedure TfmSetPrice.ShowMargin;
begin
{  with dm do
    begin
      if siSet.Down xor UseMargin then siSet.Down:=UseMargin;
      if siBack.Down xor NOT UseMargin then siSet.Down:=UseMargin;

      if miApplyMargin.Checked xor UseMargin then  miApplyMargin.Checked:=UseMargin;
      if miReturnPrice.Checked xor NOT UseMargin then miReturnPrice.Checked:=NOT UseMargin
    end;}
end;

end.

