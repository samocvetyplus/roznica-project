unit Sort;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TfmSort = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    lb1: TListBox;
    Panel1: TPanel;
    bbUp: TBitBtn;
    bbDown: TBitBtn;
    BSort: TButton;
    procedure bbUpClick(Sender: TObject);
    procedure bbDownClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure lb1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lb1StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure lb1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure BSortClick(Sender: TObject);
  private
    { Private declarations }
    DraggingIndex: integer;    
  public
  
    { Public declarations }
  end;

var
  fmSort: TfmSort;

implementation

{$R *.DFM}

procedure TfmSort.bbUpClick(Sender: TObject);
var i: integer;
begin
  with lb1, Items do
    if ItemIndex>0 then
      begin
        i:=ItemIndex-1;
        Move(ItemIndex,i);
        ActiveControl:=lb1;
        ItemIndex:=i;
      end
end;

procedure TfmSort.bbDownClick(Sender: TObject);
var i: integer;
begin
  with lb1, Items do
    if ItemIndex<Count-1 then
      begin
        i:=ItemIndex+1;
        Move(ItemIndex,i);
        ActiveControl:=lb1;
        ItemIndex:=i;
      end
end;

procedure TfmSort.FormActivate(Sender: TObject);
begin
  ActiveControl:=lb1;
  lb1.ItemIndex:=0;
end;

procedure TfmSort.lb1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var p: TPoint;
    i: integer;
begin
  p.X:=X;
  p.Y:=Y;
  i:=lb1.ItemAtPos(p, True);
  Accept:=(i<>DraggingIndex) AND (i>-1);
end;

procedure TfmSort.lb1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  DraggingIndex:=lb1.ItemIndex;
end;

procedure TfmSort.lb1DragDrop(Sender, Source: TObject; X, Y: Integer);
var i: integer;
    p: TPoint;
begin
  p.X:=X;
  p.Y:=Y;
  i:=lb1.ItemAtPos(p, True);
  if i>-1 then lb1.Items.Move(DraggingIndex, i);
  lb1.ItemIndex:=i;
end;

procedure TfmSort.BSortClick(Sender: TObject);
begin
 lb1.Sorted:=true; 
end;

end.
