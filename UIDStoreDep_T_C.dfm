object fmUIDStoreDep_T_C: TfmUIDStoreDep_T_C
  Left = 167
  Top = 81
  Width = 944
  Height = 705
  HelpContext = 100442
  Caption = #1048#1090#1086#1075#1086#1074#1099#1077' '#1089#1091#1084#1084#1099' '#1089' '#1091#1095#1077#1090#1086#1084' '#1082#1086#1084#1080#1089#1089#1080#1080'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 72
    Width = 97
    Height = 13
    Caption = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 144
    Top = 72
    Width = 48
    Height = 13
    Caption = 'lbCurrSum'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 272
    Top = 12
    Width = 87
    Height = 13
    Caption = #1042#1085#1091#1090#1088'. '#1086#1087#1090'. '#1087#1088#1080#1093'.:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 393
    Top = 14
    Width = 25
    Height = 13
    Caption = 'lbOpt'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object plSum: TPanel
    Left = 0
    Top = 41
    Width = 936
    Height = 616
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object grbxDep: TGroupBox
      Left = 2
      Top = 156
      Width = 932
      Height = 173
      Align = alTop
      Anchors = [akLeft, akRight]
      Caption = #1056#1086#1079#1085#1080#1094#1072' '
      TabOrder = 0
      object RxSplitter2: TRxSplitter
        Left = 2
        Top = 15
        Width = 928
        Height = 3
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
      end
      object plSellIn: TPanel
        Left = 2
        Top = 18
        Width = 928
        Height = 126
        Align = alClient
        BevelOuter = bvNone
        Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1099
        TabOrder = 1
        object gridSell: TDBGridEh
          Left = 0
          Top = 0
          Width = 473
          Height = 126
          Align = alLeft
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          HorzScrollBar.Visible = False
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Width = 85
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 60
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 68
            end
            item
              EditButtons = <>
              FieldName = 'COSTCOM'
              Footers = <
                item
                  FieldName = 'COSTCOM'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clOlive
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ValueType = fvtSum
                end>
              Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
        object gridSell2: TDBGridEh
          Left = 473
          Top = 0
          Width = 455
          Height = 126
          Align = alClient
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell2
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Checkboxes = False
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Title.Caption = #1043#1088#1091#1087#1087#1072
              Width = 84
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.Color = clWhite
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1057#1091#1084#1084#1072
              Width = 61
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1042#1077#1089
              Width = 69
            end
            item
              EditButtons = <>
              FieldName = 'COSTCOM'
              Footers = <
                item
                  FieldName = 'COSTCOM'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clOlive
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ValueType = fvtSum
                end>
              Title.Caption = #1050#1086#1084#1080#1089#1089#1089#1080#1103
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object plActSell: TPanel
        Left = 2
        Top = 144
        Width = 928
        Height = 27
        Align = alBottom
        TabOrder = 2
        object lbActSellCap: TLabel
          Left = 8
          Top = 8
          Width = 57
          Height = 13
          Caption = #1040#1082#1090' '#1089#1082#1080#1076#1082#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbActSell: TLabel
          Left = 88
          Top = 8
          Width = 41
          Height = 13
          Caption = 'lbActSell'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbActSellComCap: TLabel
          Left = 336
          Top = 8
          Width = 51
          Height = 13
          Caption = #1050#1086#1084#1080#1089#1089#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbActSellCom: TLabel
          Left = 400
          Top = 8
          Width = 62
          Height = 13
          Caption = 'lbActSellCom'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object grbxMain: TGroupBox
      Left = 2
      Top = 2
      Width = 932
      Height = 151
      Align = alTop
      Caption = #1054#1073#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      TabOrder = 1
      object lbInCap: TLabel
        Left = 12
        Top = 12
        Width = 185
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbIn: TLabel
        Left = 204
        Top = 12
        Width = 17
        Height = 13
        Caption = 'lbIn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInvCap: TLabel
        Left = 476
        Top = 12
        Width = 182
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1079#1072' '#1086#1090#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInv: TLabel
        Left = 664
        Top = 12
        Width = 23
        Height = 13
        Caption = 'lbInv'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 320
        Top = 12
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInCom: TLabel
        Left = 376
        Top = 12
        Width = 38
        Height = 13
        Caption = 'lbInCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 792
        Top = 12
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInvCom: TLabel
        Left = 848
        Top = 12
        Width = 44
        Height = 13
        Caption = 'lbInvCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object gridIn: TDBGridEh
        Left = 12
        Top = 28
        Width = 453
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrIn
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.Value = '999'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridSInv: TDBGridEh
        Left = 476
        Top = 28
        Width = 445
        Height = 93
        AllowedOperations = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInv
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object RxSplitter1: TRxSplitter
      Left = 2
      Top = 153
      Width = 932
      Height = 3
      ControlFirst = grbxMain
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
    end
    object RxSplitter3: TRxSplitter
      Left = 2
      Top = 329
      Width = 932
      Height = 3
      ControlFirst = grbxDep
      Align = alTop
      BevelOuter = bvLowered
    end
    object grbxCurr: TGroupBox
      Left = 2
      Top = 332
      Width = 932
      Height = 282
      Align = alClient
      Caption = #1054#1087#1090', '#1074#1086#1079#1074#1088#1072#1090', '#1080#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
      TabOrder = 4
      object lbCurr: TLabel
        Left = 12
        Top = 20
        Width = 183
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1082#1086#1085#1077#1094' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbCurrSum: TLabel
        Left = 198
        Top = 20
        Width = 48
        Height = 13
        Caption = 'lbCurrSum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 320
        Top = 20
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbCurrSumCom: TLabel
        Left = 376
        Top = 20
        Width = 69
        Height = 13
        Caption = 'lbCurrSumCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbREt: TLabel
        Left = 11
        Top = 140
        Width = 108
        Height = 13
        Caption = #1056#1086#1079#1085#1077#1095#1085#1099#1077' '#1074#1086#1079#1074#1088#1072#1090#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object gridOut: TDBGridEh
        Left = 12
        Top = 38
        Width = 461
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrOut
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridMinus: TDBGridEh
        Left = 483
        Top = 39
        Width = 454
        Height = 98
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        BorderStyle = bsNone
        DataGrouping.GroupLevels = <>
        DataSource = dsrOutDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridPlus: TDBGridEh
        Left = 11
        Top = 159
        Width = 462
        Height = 98
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        BorderStyle = bsNone
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInvDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 81
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object stbrStatus: TStatusBar
    Left = 0
    Top = 657
    Width = 936
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 936
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 859
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 795
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object fmstrTSum: TFormStorage
    IniFileName = 'jew.ini'
    IniSection = 'TfrTSum'
    UseRegistry = False
    StoredProps.Strings = (
      'plSum.Height'
      'grbxMain.Height'
      'grbxCurr.Height'
      'grbxCurr.Width'
      'grbxDep.Height'
      'grbxDep.Width')
    StoredValues = <>
    Left = 532
    Top = 9
  end
  object taIn: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select ENTRYNUM Q, ENTRYW W, ENTRYCOST COST, GR GROUPNAME, ENTRY' +
        'COSTCOM COSTCOM'
      'from UID_Store_SUM'
      'where SINVID=:INVID ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 12
    Top = 92
    object taInQ: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taInW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taInCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taInGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taInCOSTCOM: TFIBFloatField
      FieldName = 'COSTCOM'
      currency = True
    end
  end
  object dsrIn: TDataSource
    DataSet = taIn
    Left = 12
    Top = 140
  end
  object taSInvDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1074' '#1088#1072#1089#1093#1086#1076#1085#1099#1093' '#1094#1077#1085#1072#1093#39' as char(20)) NAME, sum(RETNUM) Q' +
        ', sum(RETW) W, sum(RETCOST) COST, sum(RETCOSTCOM) COSTCOM'
      'from UID_store_sum'
      'where SINVID=:INVID'
      'union all'
      
        'select cast('#39#1074' '#1094#1077#1085#1072#1093' '#1074#1086#1079#1074#1088#1072#1090#1072#39' as char(20)) NAME, sum(RETNUM) Q,' +
        ' sum(RETW) W, sum(RETCOST2) COST, sum(RETCOST2COM) COSTCOM'
      'from UID_store_sum'
      'where SINVID=:INVID'
      'union all'
      
        'select cast('#39#1072#1082#1090' '#1089#1082#1080#1076#1082#1080#39' as char(20)) NAME, sum(RETNUM) Q, sum(R' +
        'ETW) W, sum(ACTDISCOUNTRETCOST) COST, sum(ACTDISCOUNTRETCOSTCOM)' +
        ' COSTCOM'
      'from UID_store_sum'
      'where SINVID=:INVID'
      'union all'
      
        'select cast('#39#1072#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080#39' as char(20)) NAME, sum(0) Q, sum(RE' +
        'TW) W, sum(ACTCOST) COST, sum(ACTCOSTCOM) COSTCOM'
      'from UID_store_sum'
      'where SINVID=:INVID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 44
    Top = 552
    object taSInvDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSInvDetailCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
    end
    object taSInvDetailQ: TFIBBCDField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object taSInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select DINVNUM+RETNUM Q, DINVW+RETW W, '
      '       DINVCOST+RETCOST COST, '
      '       GR GROUPNAME,'
      '       DINVCOSTCOM+RETCOSTCOM COSTCOM'
      'from UID_Store_SUM'
      'where SINVID=:INVID ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 516
    Top = 72
    object taSInvW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSInvCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taSInvQ: TFIBBCDField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object dsrSInvDetail: TDataSource
    DataSet = taSInvDetail
    Left = 44
    Top = 600
  end
  object dsrSInv: TDataSource
    DataSet = taSInv
    Left = 516
    Top = 120
  end
  object taOut: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select RESNUM Q, RESW W, RESCOST COST, GR GROUPNAME, RESCOSTCOM ' +
        'COSTCOM'
      'from UID_Store_sum'
      'where SINVID=:INVID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 32
    Top = 440
    object taOutW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taOutGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taOutQ: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
  end
  object dsrOut: TDataSource
    DataSet = taOut
    Left = 76
    Top = 444
  end
  object taSell: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select SELLNUM Q, SELLW W, SELLCOST COST, '
      '       GR GROUPNAME, SELLCOSTCOM COSTCOM,'
      '       ACTDISCOUNTSELLCOST ACTCOST,'
      '       ACTDISCOUNTSELLCOSTCOM ACTCOSTCOM'
      'from UID_Store_Sum'
      'where SINVID=:INVID ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 216
    object taSellQ: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSellW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSellCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSellGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSellCOSTCOM: TFIBFloatField
      FieldName = 'COSTCOM'
    end
    object taSellACTCOST: TFIBFloatField
      FieldName = 'ACTCOST'
    end
    object taSellACTCOSTCOM: TFIBFloatField
      FieldName = 'ACTCOSTCOM'
    end
  end
  object taSell2: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select SELLNUM Q, SELLW W, SELLCOST2 COST, '
      '       GR GROUPNAME, SELLCOST2COM COSTCOM'
      'from UID_Store_Sum'
      'where SINVID=:INVID ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 584
    Top = 204
    object taSell2W: TFloatField
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSell2COST: TFloatField
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSell2GROUPNAME: TFIBStringField
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSell2COSTCOM: TFIBFloatField
      FieldName = 'COSTCOM'
    end
    object taSell2Q: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
  end
  object dsrSell: TDataSource
    DataSet = taSell
    Left = 64
    Top = 216
  end
  object dsrSell2: TDataSource
    DataSet = taSell2
    Left = 644
    Top = 204
  end
  object qutmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 680
    Top = 572
  end
  object taOutDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1042#1086#1079#1074#1088#1072#1090' '#1088#1072#1089#1093#1086#1076#39' as char(20)) NAME, sum(u.DINVNUMFRO' +
        'M) Q, '
      '       sum(u.DINVFROMW) W, sum(u.DINVFROMCOST) COST,'
      '       sum(u.DINVFROMCOSTCOM) COSTCOM'
      'from UID_store_sum u'
      'where u.SINVID=:INVID'
      'union all'
      'select cast('#39#1056#1086#1079'. '#1087#1088#1086#1076#1072#1078#1080#39' as char(20)) NAME, sum(u.SELLNUM) Q, '
      '       sum(u.SELLW) W, sum(u.SELLCOST) COST,'
      '       sum(u.SELLCOSTCOM) COSTCOM'
      'from UID_store_sum u'
      'where u.SINVID=:INVID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 516
    Top = 456
    object taOutDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taOutDetailCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taOutDetailQ: TFIBBCDField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object dsrOutDetail: TDataSource
    DataSet = taOutDetail
    Left = 584
    Top = 456
  end
  object frIn: TfrDBDataSet
    DataSet = taIn
    Left = 64
    Top = 92
  end
  object acEvent: TActionList
    Left = 128
    Top = 12
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintExecute
    end
  end
  object frSell: TfrDBDataSet
    DataSet = taSell
    Left = 108
    Top = 224
  end
  object frSell2: TfrDBDataSet
    DataSet = taSell2
    Left = 692
    Top = 204
  end
  object frOut: TfrDBDataSet
    DataSet = taOut
    Left = 116
    Top = 441
  end
  object frSInv: TfrDBDataSet
    DataSet = taSInv
    Left = 576
    Top = 72
  end
  object frSInvDetail: TfrDBDataSet
    DataSet = taSInvDetail
    Left = 124
    Top = 600
  end
  object frOutDetail: TfrDBDataSet
    DataSet = taOutDetail
    Left = 664
    Top = 456
  end
end
