object fmUIDStore_T_C: TfmUIDStore_T_C
  Left = 152
  Top = 114
  HelpContext = 100422
  Caption = #1048#1090#1086#1075#1086#1074#1099#1077' '#1089#1091#1084#1084#1099' '#1089' '#1091#1095#1077#1090#1086#1084' '#1082#1086#1084#1080#1089#1089#1080#1080'.'
  ClientHeight = 732
  ClientWidth = 936
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 72
    Width = 97
    Height = 13
    Caption = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 144
    Top = 72
    Width = 48
    Height = 13
    Caption = 'lbCurrSum'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 272
    Top = 12
    Width = 87
    Height = 13
    Caption = #1042#1085#1091#1090#1088'. '#1086#1087#1090'. '#1087#1088#1080#1093'.:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 393
    Top = 14
    Width = 25
    Height = 13
    Caption = 'lbOpt'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object plSum: TPanel
    Left = 0
    Top = 41
    Width = 936
    Height = 672
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object grbxDep: TGroupBox
      Left = 2
      Top = 132
      Width = 932
      Height = 302
      Align = alTop
      Anchors = [akLeft, akRight]
      Caption = #1056#1086#1079#1085#1080#1094#1072' '
      TabOrder = 0
      object plSellOut: TPanel
        Left = 2
        Top = 108
        Width = 928
        Height = 192
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object gridSell2: TDBGridEh
          Left = 0
          Top = 0
          Width = 928
          Height = 89
          Align = alTop
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell2
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Checkboxes = False
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Title.Caption = #1043#1088#1091#1087#1087#1072
              Width = 84
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.Color = clWhite
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1057#1091#1084#1084#1072
              Width = 61
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1050#1086#1083'-'#1074#1086
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1042#1077#1089
              Width = 69
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
        object gridSellCom: TDBGridEh
          Left = 0
          Top = 139
          Width = 928
          Height = 53
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dsrSellKeep
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FrozenCols = 1
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
          ParentFont = False
          RowDetailPanel.Color = clBtnFace
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Color = clBtnFace
              DblClickNextVal = True
              EditButtons = <>
              FieldName = 'COM'
              Footers = <>
              Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
              Width = 108
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footers = <>
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
        object GridSellKeep: TDBGridEh
          Left = 0
          Top = 89
          Width = 928
          Height = 50
          Align = alTop
          DataGrouping.GroupLevels = <>
          DataSource = dsrSellCom
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FrozenCols = 1
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
          ParentFont = False
          RowDetailPanel.Color = clBtnFace
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Color = clBtnFace
              DblClickNextVal = True
              EditButtons = <>
              FieldName = 'COM'
              Footers = <>
              Width = 108
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footers = <>
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object plSellIn: TPanel
        Left = 2
        Top = 15
        Width = 928
        Height = 90
        Align = alTop
        BevelOuter = bvNone
        Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1099
        TabOrder = 1
        object gridSell: TDBGridEh
          Left = 0
          Top = 0
          Width = 928
          Height = 90
          Align = alClient
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          HorzScrollBar.Visible = False
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Width = 85
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 60
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 68
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object RxSplitter2: TRxSplitter
        Left = 2
        Top = 105
        Width = 928
        Height = 3
        ControlFirst = plSellIn
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
      end
    end
    object grbxMain: TGroupBox
      Left = 2
      Top = 2
      Width = 932
      Height = 127
      Align = alTop
      Caption = #1054#1073#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      TabOrder = 1
      object lbInCap: TLabel
        Left = 12
        Top = 12
        Width = 185
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbIn: TLabel
        Left = 203
        Top = 9
        Width = 17
        Height = 13
        Caption = 'lbIn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInvCap: TLabel
        Left = 476
        Top = 12
        Width = 182
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1079#1072' '#1086#1090#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInv: TLabel
        Left = 664
        Top = 12
        Width = 23
        Height = 13
        Caption = 'lbInv'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 320
        Top = 12
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInCom: TLabel
        Left = 376
        Top = 12
        Width = 38
        Height = 13
        Caption = 'lbInCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 792
        Top = 12
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInvCom: TLabel
        Left = 848
        Top = 12
        Width = 44
        Height = 13
        Caption = 'lbInvCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object gridIn: TDBGridEh
        Left = 12
        Top = 28
        Width = 453
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrIn
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.Value = '999'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
            Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103
          end
          item
            EditButtons = <>
            FieldName = 'CostKeep'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridSInv: TDBGridEh
        Left = 476
        Top = 28
        Width = 445
        Height = 93
        AllowedOperations = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInv
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'CostKeep'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object RxSplitter1: TRxSplitter
      Left = 2
      Top = 129
      Width = 932
      Height = 3
      ControlFirst = grbxMain
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
    end
    object RxSplitter3: TRxSplitter
      Left = 2
      Top = 434
      Width = 932
      Height = 3
      ControlFirst = grbxDep
      Align = alTop
      BevelOuter = bvLowered
    end
    object grbxCurr: TGroupBox
      Left = 2
      Top = 437
      Width = 932
      Height = 233
      Align = alClient
      Caption = #1054#1087#1090', '#1074#1086#1079#1074#1088#1072#1090', '#1080#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
      TabOrder = 4
      object lbCurr: TLabel
        Left = 12
        Top = 20
        Width = 183
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1082#1086#1085#1077#1094' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbCurrSum: TLabel
        Left = 198
        Top = 20
        Width = 48
        Height = 13
        Caption = 'lbCurrSum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbFSum: TLabel
        Left = 592
        Top = 20
        Width = 35
        Height = 13
        Caption = 'lbFSum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 480
        Top = 20
        Width = 105
        Height = 13
        Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1089#1091#1084#1084#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 320
        Top = 20
        Width = 51
        Height = 13
        Caption = #1050#1086#1084#1080#1089#1089#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbCurrSumCom: TLabel
        Left = 376
        Top = 20
        Width = 69
        Height = 13
        Caption = 'lbCurrSumCom'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object gridOut: TDBGridEh
        Left = 3
        Top = 36
        Width = 461
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrOut
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'CostKeep'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridMinus: TDBGridEh
        Left = 3
        Top = 135
        Width = 462
        Height = 106
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrOutDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'COSTKEEP'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridPlus: TDBGridEh
        Left = 467
        Top = 135
        Width = 462
        Height = 106
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInvDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 87
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'COSTCOM'
            Footers = <
              item
                FieldName = 'COSTCOM'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clOlive
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'CostKeep'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.Caption = #1061#1088#1072#1085#1077#1085#1080#1077
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBGridEh1: TDBGridEh
        Left = 468
        Top = 38
        Width = 461
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrF
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object stbrStatus: TStatusBar
    Left = 0
    Top = 713
    Width = 936
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 936
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 859
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 795
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object fmstrTSum: TFormStorage
    IniFileName = 'jew.ini'
    IniSection = 'TfrTSum'
    UseRegistry = False
    StoredProps.Strings = (
      'plSellOut.Height'
      'plSum.Height'
      'grbxMain.Height'
      'grbxCurr.Height'
      'grbxCurr.Width'
      'grbxDep.Height'
      'grbxDep.Width')
    StoredValues = <>
    Left = 532
    Top = 9
  end
  object taIn: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  count(*) Q, '
      '  sum(W) W, '
      '  sum(ENTRYCOST) COST, '
      '  GR GROUPNAME, '
      '  sum(ENTRYCOSTCOM) COSTCOM, '
      '  sum(entrycost$keep) CostKeep'
      'from '
      '  UID_Store'
      'where '
      '  SINVID=:INVID and'
      '  (ENTRYNUM <> 0 or ENTRYNUMCOM <> 0 or ENTRYNUM$keep <> 0)     '
      'group by '
      '  GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 12
    Top = 92
    object taInQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taInW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taInCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taInGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taInCOSTCOM: TFIBFloatField
      FieldName = 'COSTCOM'
      currency = True
    end
    object taInCostKeep: TFIBFloatField
      FieldName = 'CostKeep'
    end
  end
  object dsrIn: TDataSource
    DataSet = taIn
    Left = 12
    Top = 140
  end
  object taSInvDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1055#1086#1089#1090#1072#1074#1082#1072#39' as char(20)) NAME, count(*) Q, sum(W) W, ' +
        'sum(SINVCOST) COST, sum(SINVCOSTCOM) COSTCOM, sum(SINVCOST$keep)' +
        ' CostKeep'
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SINVNUM<>0 or SINVNUMCOM<>0 or SINVNUM$Keep<>0)'
      'union all'
      
        'select cast('#39#1056#1086#1079#1085'.'#1074#1086#1079#1074#1088#1072#1090#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(RETCOST) COST, sum(RETCOSTCOM) COSTCOM, sum(RETCOST$keep' +
        ') CostKeep'
      'from UID_store'
      'where SINVID=:INVID and'
      '     (RETNUM<>0 or RETNUMCOM<>0 or RETNUM$Keep<>0)'
      'union all'
      
        'select cast('#39#1054#1087#1090'.'#1074#1086#1079#1074#1088#1072#1090#39' as char(20)) NAME, count(*) Q, sum(W) ' +
        'W, sum(OPTRETCOST) COST,  sum(OPTRETCOSTCOM) COSTCOM, sum(OPTRET' +
        'COST$keep) CostKeep'
      'from UID_store'
      'where SINVID=:INVID and'
      '     (OPTRETNUM<>0 or OPTRETNUMCOM<>0 or OPTRETNUM$Keep<>0)'
      'union all'
      
        'select cast('#39#1048#1079#1083#1080#1096#1082#1080#39' as char(20)) NAME, count(*) Q, sum(W) W, s' +
        'um(SURPLUSCOST) COST, cast(0.0 as double precision) COSTCOM, cas' +
        't(0.0 as double precision) CostKeep'
      'from UID_store'
      'where SINVID=:INVID and'
      '      SURPLUSNUM<>0'
      ' '
      ' ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 52
    Top = 568
    object taSInvDetailQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSInvDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSInvDetailCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
    end
    object taSInvDetailCostKeep: TFIBFloatField
      FieldName = 'CostKeep'
    end
  end
  object taSInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, '
      '       sum(SINVCOST)+ sum(RETCOST)+'
      '       sum(OPTRETCOST)+ sum(SURPLUSCOST) COST, '
      '       GR GROUPNAME,'
      '       sum(SINVCOSTCOM)+ sum(RETCOSTCOM)+'
      '       sum(OPTRETCOSTCOM) COSTCOM,'
      '       sum(SINVCOST$keep)+ sum(RETCOST$keep)+'
      '       sum(OPTRETCOST$keep) COSTkeep'
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SINVNUM<>0 or OPTRETNUM<>0 or '
      '      RETNUM<>0 or SURPLUSNUM<>0 or'
      '      SINVNUMCOM<>0 or OPTRETNUMCOM<>0 or '
      '      RETNUMCOM<>0 or'
      '      SINVNUM$KEEP<>0 or OPTRETNUM$KEEP<>0 or '
      '      RETNUM$KEEP<>0 )'
      ''
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 516
    Top = 72
    object taSInvQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSInvW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSInvCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taSInvCostKeep: TFIBFloatField
      FieldName = 'CostKeep'
    end
  end
  object dsrSInvDetail: TDataSource
    DataSet = taSInvDetail
    Left = 52
    Top = 608
  end
  object dsrSInv: TDataSource
    DataSet = taSInv
    Left = 516
    Top = 120
  end
  object taOut: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select count(*) Q, sum(W) W, sum(RESCOST) COST, GR GROUPNAME, su' +
        'm(RESCOSTCOM) COSTCOM, sum(RESCOST$keep) COSTkeep'
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (RESNUM<>0 or RESNUMCOM<>0)'
      '     '
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 64
    Top = 472
    object taOutW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taOutGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taOutQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taOutCostKeep: TFIBFloatField
      FieldName = 'CostKeep'
    end
  end
  object dsrOut: TDataSource
    DataSet = taOut
    Left = 28
    Top = 468
  end
  object taSell: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, sum(SELLCOST) COST, '
      '          GR GROUPNAME'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '           SELLNUM<>0'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 84
    Top = 208
    object taSellQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSellW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSellCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSellGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object taSell2: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, sum(SELLCOST2) COST, '
      '          GR GROUPNAME'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '           SELLNUM<>0'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 308
    object taSell2Q: TIntegerField
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSell2W: TFloatField
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSell2COST: TFloatField
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSell2GROUPNAME: TFIBStringField
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrSell: TDataSource
    DataSet = taSell
    Left = 152
    Top = 216
  end
  object taSellDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(u.W) W, sum(u.SELLCOST) COST, '
      '          u.GR GROUPNAME, u.DEPID, sum(u.SELLCOST2) COST1'
      'from UID_Store u,  D_Dep  d'
      'where u.SINVID=:INVID and'
      '      u.SELLNUM<>0 and'
      '      u.DepID = d.D_DepID and'
      '      d.ISDELETE <>1  '
      'group by GR, DEPID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 208
    object taSellDepQ: TIntegerField
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSellDepW: TFloatField
      FieldName = 'W'
      ProviderFlags = []
    end
    object taSellDepCOST: TFloatField
      FieldName = 'COST'
      ProviderFlags = []
    end
    object taSellDepGROUPNAME: TFIBStringField
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSellDepDEPID: TIntegerField
      FieldName = 'DEPID'
      ProviderFlags = []
    end
    object taSellDepCOST1: TFloatField
      FieldName = 'COST1'
      ProviderFlags = []
    end
  end
  object taDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select D_DEPID, SNAME'
      'from D_Dep'
      'where D_DEPID<>-1000 and ISDELETE <> 1'
      'order by SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 256
    object taDepD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Origin = 'D_DEP.D_DEPID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object taDepSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrSell2: TDataSource
    DataSet = taSell2
    Left = 76
    Top = 308
  end
  object qutmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 680
    Top = 572
  end
  object taOutDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1056#1086#1079#1085'.'#1087#1088#1086#1076#1072#1078#1080#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(SELLCOST) COST, sum(SELLCOSTCOM) COSTCOM, sum(SELLCOST$K' +
        'eep) CostKeep'
      'from UID_store'
      'where SINVID=:INVID and'
      '     (SELLNUM<>0 or SELLNUMCOM<>0 or SELLNUM$Keep<>0)'
      ''
      'union all'
      
        'select cast('#39#1054#1087#1090'.'#1087#1088#1086#1076#1072#1078#1080#39' as char(20)) NAME, count(*) Q, sum(W) ' +
        'W, sum(OPTSELLCOST) COST, sum(OPTSELLCOSTCOM) COSTCOM, sum(OPTSE' +
        'LLCOST$Keep) CostKeep'
      'from UID_store'
      'where SINVID=:INVID and'
      '     (OPTSELLNUM<>0 or OPTSELLNUMCOM<>0 or OPTSELLNUM$Keep<>0)'
      'union all'
      
        'select cast('#39#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084#39' as char(20)) NAME, count(*) Q,' +
        ' sum(W) W, sum(SINVRETCOST) COST, sum(SINVRETCOSTCOM) COSTCOM, s' +
        'um(SINVRETCOST$Keep) CostKeep'
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SINVRETNUM<>0 or SINVRETNUMCOM<>0 or SINVRETNUM$Keep<>0)'
      ''
      'union all'
      
        'select cast('#39#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(ACTALLOWANCESCOST) COST, sum(ACTALLOWANCESCOSTCOM) COSTC' +
        'OM, sum(ACTALLOWANCESCOST$Keep) CostKeep'
      'from UID_Store'
      'where SINVID=:INVID and'
      
        '     (ACTALLOWANCESNUM<>0 or ACTALLOWANCESNUMCOM<>0 or ACTALLOWA' +
        'NCESNUM$Keep<>0)'
      ''
      ' ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 500
    Top = 576
    object taOutDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutDetailQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taOutDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taOutDetailCOSTCOM: TFIBFloatField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COSTCOM'
      currency = True
    end
    object taOutDetailCOSTKEEP: TFIBFloatField
      FieldName = 'COSTKEEP'
    end
  end
  object dsrOutDetail: TDataSource
    DataSet = taOutDetail
    Left = 536
    Top = 608
  end
  object frIn: TfrDBDataSet
    DataSet = taIn
    Left = 64
    Top = 92
  end
  object acEvent: TActionList
    Left = 128
    Top = 12
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintExecute
    end
  end
  object frSell: TfrDBDataSet
    DataSet = taSell
    Left = 76
    Top = 256
  end
  object frSell2: TfrDBDataSet
    DataSet = taSell2
    Left = 116
    Top = 308
  end
  object frOut: TfrDBDataSet
    DataSet = taOut
    Left = 100
    Top = 465
  end
  object frSInv: TfrDBDataSet
    DataSet = taSInv
    Left = 576
    Top = 72
  end
  object frSInvDetail: TfrDBDataSet
    DataSet = taSInvDetail
    Left = 124
    Top = 600
  end
  object frOutDetail: TfrDBDataSet
    DataSet = taOutDetail
    Left = 608
    Top = 568
  end
  object taF: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(F) Q, sum(W) W, sum(FCOST) COST, GR GROUPNAME'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      F<>0'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Description = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1085#1072#1083#1080#1095#1080#1077
    Left = 548
    Top = 464
    poSQLINT64ToBCD = True
    object taFQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taFW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taFCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taFGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      EmptyStrToNull = True
    end
  end
  object dsrF: TDataSource
    DataSet = taF
    Left = 724
    Top = 464
  end
  object frF: TfrDBDataSet
    DataSet = taF
    Left = 644
    Top = 492
  end
  object taSellCom: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      
        'select count(*) Q, sum(W) W, sum(SELLCOSTCOM) COST, '#39#1042' '#1087#1088#1080#1093#1086#1076#1085#1099#1093 +
        ' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SELLNUMCOM<>0'
      'union all'
      
        'select count(*) Q, sum(W) W, sum(SELLCOST2COM) COST, '#39#1042' '#1088#1072#1089#1093#1086#1076#1085#1099 +
        #1093' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SELLNUMCOM<>0')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 120
    Top = 368
    poSQLINT64ToBCD = True
    object taSellComQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSellComW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taSellComCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taSellComCOM: TFIBStringField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COM'
      Size = 17
      EmptyStrToNull = True
    end
  end
  object dsrSellCom: TDataSource
    DataSet = taSellCom
    Left = 192
    Top = 368
  end
  object taSellDepCom: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(u.W) W, sum(u.SELLCOSTCOM) COST, '
      '       u.DEPID, sum(u.SELLCOST2COM) COST1'
      'from UID_Store u,  D_Dep  d'
      'where u.SINVID=:INVID and'
      '      u.SELLNUMCOM<>0 and'
      '      u.DepID = d.D_DepID and'
      '      d.ISDELETE <>1  '
      'group by DEPID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 352
    Top = 368
    poSQLINT64ToBCD = True
    object taSellDepComQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taSellDepComW: TFIBFloatField
      FieldName = 'W'
    end
    object taSellDepComCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSellDepComCOST1: TFIBFloatField
      FieldName = 'COST1'
    end
    object taSellDepComDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
  end
  object frSellCom: TfrDBDataSet
    DataSet = taSellCom
    Left = 272
    Top = 368
  end
  object taSellAll: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      
        'select count(*) Q, sum(W) W, sum(SELLCOSTCOM+SELLCOST) COST, '#39#1042' ' +
        #1087#1088#1080#1093#1086#1076#1085#1099#1093' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SELLNUMCOM<>0 or SELLNUM<>0)'
      'union all'
      
        'select count(*) Q, sum(W) W, sum(SELLCOST2COM+SELLCOST2) COST, '#39 +
        #1042' '#1088#1072#1089#1093#1086#1076#1085#1099#1093' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SELLNUMCOM<>0 or SELLNUM<>0)')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 432
    Top = 320
    poSQLINT64ToBCD = True
    object taSellAllQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taSellAllW: TFIBFloatField
      FieldName = 'W'
    end
    object taSellAllCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSellAllCOM: TFIBStringField
      FieldName = 'COM'
      Size = 17
      EmptyStrToNull = True
    end
  end
  object taSellAllDep: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select count(*) Q, sum(u.W) W, sum(u.SELLCOSTCOM+u.SELLCOST) COS' +
        'T, '
      '       u.DEPID, sum(u.SELLCOST2COM+u.SELLCOST2) COST1'
      'from UID_Store u,  D_Dep  d'
      'where u.SINVID=:INVID and'
      '      (u.SELLNUMCOM<>0 or u.SELLNUM<>0) and'
      '      u.DepID = d.D_DepID and'
      '      d.ISDELETE <>1  '
      'group by DEPID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 528
    Top = 368
    poSQLINT64ToBCD = True
    object taSellAllDepQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taSellAllDepW: TFIBFloatField
      FieldName = 'W'
    end
    object taSellAllDepCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSellAllDepDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taSellAllDepCOST1: TFIBFloatField
      FieldName = 'COST1'
    end
  end
  object frSellAll: TfrDBDataSet
    DataSet = taSellAll
    Left = 592
    Top = 368
  end
  object taSellKeep: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      
        'select count(*) Q, sum(W) W, sum(SELLCOST$KEEP) COST, '#39#1042' '#1087#1088#1080#1093#1086#1076#1085 +
        #1099#1093' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SELLNUM$KEEP<>0'
      'union all'
      
        'select count(*) Q, sum(W) W, sum(SELLCOST2$KEEP) COST, '#39#1042' '#1088#1072#1089#1093#1086#1076 +
        #1085#1099#1093' '#1094#1077#1085#1072#1093#39' com'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SELLNUM$KEEP<>0'
      ' ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 120
    Top = 400
    poSQLINT64ToBCD = True
    object taSellKeepQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSellKeepW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taSellKeepCost: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taSellKeepCom: TFIBStringField
      DisplayLabel = #1050#1086#1084#1080#1089#1089#1080#1103
      FieldName = 'COM'
      Size = 17
      EmptyStrToNull = True
    end
  end
  object dsrSellKeep: TDataSource
    DataSet = taSellKeep
    Left = 192
    Top = 408
  end
  object taSellDepKeep: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(u.W) W, sum(u.SELLCOST$Keep) COST, '
      '       u.DEPID, sum(u.SELLCOST2$Keep) COST1'
      'from UID_Store u,  D_Dep  d'
      'where u.SINVID=:INVID and'
      '      u.SELLNUM$Keep<>0 and'
      '      u.DepID = d.D_DepID and'
      '      d.ISDELETE <>1  '
      'group by DEPID'
      ' ')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 352
    Top = 416
    poSQLINT64ToBCD = True
    object taSellDepKeepQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taSellDepKeepW: TFIBFloatField
      FieldName = 'W'
    end
    object taSellDepKeepCost: TFIBFloatField
      FieldName = 'COST'
    end
    object taSellDepKeepCost1: TFIBFloatField
      FieldName = 'COST1'
    end
    object taSellDepKeepDepID: TFIBIntegerField
      FieldName = 'DEPID'
    end
  end
  object frSellKeep: TfrDBDataSet
    DataSet = taSellKeep
    Left = 272
    Top = 416
  end
  object taInAll: TpFIBDataSet
    SelectSQL.Strings = (
      'select realcost, factcost, otv, prcost,'
      '       cost5, cost30, cost45, cost60, cost90'
      'from uid_st_in(:invid)')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 344
    Top = 8
    object taInAllREALCOST: TFIBFloatField
      FieldName = 'REALCOST'
    end
    object taInAllFACTCOST: TFIBFloatField
      FieldName = 'FACTCOST'
    end
    object taInAllOTV: TFIBFloatField
      FieldName = 'OTV'
    end
    object taInAllPRCOST: TFIBFloatField
      FieldName = 'PRCOST'
    end
    object taInAllCOST5: TFIBFloatField
      FieldName = 'COST5'
    end
    object taInAllCOST30: TFIBFloatField
      FieldName = 'COST30'
    end
    object taInAllCOST45: TFIBFloatField
      FieldName = 'COST45'
    end
    object taInAllCOST60: TFIBFloatField
      FieldName = 'COST60'
    end
    object taInAllCOST90: TFIBFloatField
      FieldName = 'COST90'
    end
  end
  object taOutAll: TpFIBDataSet
    SelectSQL.Strings = (
      'select realcost, factcost, otv, prcost,'
      '       cost5, cost30, cost45, cost60, cost90'
      'from uid_st_out(:invid)')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 408
    Top = 8
    object taOutAllREALCOST: TFIBFloatField
      FieldName = 'REALCOST'
    end
    object taOutAllFACTCOST: TFIBFloatField
      FieldName = 'FACTCOST'
    end
    object taOutAllOTV: TFIBFloatField
      FieldName = 'OTV'
    end
    object taOutAllPRCOST: TFIBFloatField
      FieldName = 'PRCOST'
    end
    object taOutAllCOST5: TFIBFloatField
      FieldName = 'COST5'
    end
    object taOutAllCOST30: TFIBFloatField
      FieldName = 'COST30'
    end
    object taOutAllCOST45: TFIBFloatField
      FieldName = 'COST45'
    end
    object taOutAllCOST60: TFIBFloatField
      FieldName = 'COST60'
    end
    object taOutAllCOST90: TFIBFloatField
      FieldName = 'COST90'
    end
  end
  object frInAll: TfrDBDataSet
    DataSet = taInAll
    Left = 376
    Top = 8
  end
  object frOutAll: TfrDBDataSet
    DataSet = taOutAll
    Left = 440
    Top = 8
  end
end
