unit Access;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, CheckLst, DBCtrlsEh,
  ExtCtrls, DB, cxControls, cxPC, M207DBCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter;

type
  TfmAccess = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    edUserName: TDBEdit;
    edPswd: TDBEdit;
    rgPrmissions: TRadioGroup;
    pgrigth: TcxPageControl;
    taballrigth: TcxTabSheet;
    M207DBBits1: TM207DBBits;
    tabRefBook: TcxTabSheet;
    bitsVIEWREFBOOK: TM207DBBits;
    dbbtEDITREFBOOK: TM207DBBits;
    Label6: TLabel;
    Label7: TLabel;
    chballsell: TDBCheckBoxEh;
    DBCheckBox2: TDBCheckBox;
    btPswd: TButton;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure rgPrmissionsClick(Sender: TObject);
    procedure M207DBBits1ClickCheck(Sender: TObject);
    procedure btPswdClick(Sender: TObject);
  end;

var
  fmAccess: TfmAccess;

implementation

uses comdata, data, EditPswd, dbUtil, M207Proc;

{$R *.DFM}

procedure TfmAccess.FormCreate(Sender: TObject);
begin
  with dmCom do
    Caption:='����� ������� - '+taEmpFName.AsString+' '+taEmpLName.AsString+' '+taEmpSName.AsString;
//  edPswdTest.Text:='';

 if dmcom.taEmpPERMISSIONS_USER.IsNull then rgPrmissions.ItemIndex:=1
 else rgPrmissions.ItemIndex:=dmcom.taEmpPERMISSIONS_USER.AsInteger;

end;

procedure TfmAccess.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult=mrOK then
  begin
  //  if edPswdTest.Text<>dmCom.taEmpPswd.AsString then
    //  begin
      //  edPswdTest.Text:='';
        //raise Exception.Create('��������� ���� ������!');
      //end;
    if (dmcom.taEmp.State in [dsEdit, dsInsert]) then dmcom.taEmp.Post;
    dmCom.VIEWREFBOOK:=dmcom.taEmpVIEWREFBOOK.AsInteger;
    dmCom.EditRefBook:=dmcom.taEmpEDITREFBOOK.AsInteger;
    dmcom.Acc:=dmcom.taEmpACC.AsInteger;
  end
  else if (dmcom.taEmp.State in [dsEdit, dsInsert]) then dmcom.taEmp.Cancel
end;

procedure TfmAccess.rgPrmissionsClick(Sender: TObject);
var i:integer;
begin
 if (rgPrmissions.ItemIndex<>dmcom.taEmpPERMISSIONS_USER.AsInteger)
    or (dmcom.taEmpPERMISSIONS_USER.IsNull) then
 case rgPrmissions.ItemIndex of
 0, 1, 3: begin //�������������, ��������
     dmCom.taEmp.Edit;
     dmcom.taEmpPERMISSIONS_USER.AsInteger:=rgPrmissions.ItemIndex;
     if rgPrmissions.ItemIndex=0 then dmcom.taEmpALLWH.AsInteger:=1
     else dmcom.taEmpALLWH.AsInteger:=0;
     dmcom.taEmpALLSELL.AsInteger:=1;
     for i:=0 to M207DBBits1.Count-1 do
      M207DBBits1.Checked[i]:=true;
     M207DBBits1.UpdateData(M207DBBits1);

     for i:=0 to bitsVIEWREFBOOK.Count-1 do
      bitsVIEWREFBOOK.Checked[i]:=true;
     bitsVIEWREFBOOK.UpdateData(bitsVIEWREFBOOK);

     for i:=0 to dbbtEDITREFBOOK.Count-1 do
      dbbtEDITREFBOOK.Checked[i]:=true;
     dbbtEDITREFBOOK.UpdateData(dbbtEDITREFBOOK);
    end;
 2: begin //��������
     dmCom.taEmp.Edit;
     dmcom.taEmpPERMISSIONS_USER.AsInteger:=rgPrmissions.ItemIndex;
     dmcom.taEmpALLWH.AsInteger:=0;
     dmcom.taEmpALLSELL.AsInteger:=0;
     M207DBBits1.Checked[0]:=false;
     M207DBBits1.Checked[1]:=false;
     M207DBBits1.Checked[2]:=true;
     M207DBBits1.Checked[3]:=true;
     M207DBBits1.Checked[4]:=false;
     M207DBBits1.Checked[5]:=true;
     M207DBBits1.Checked[6]:=true;
     M207DBBits1.Checked[7]:=false;
     M207DBBits1.Checked[8]:=false;
     M207DBBits1.Checked[9]:=false;
     M207DBBits1.Checked[10]:=false;
     M207DBBits1.Checked[11]:=false;
     M207DBBits1.Checked[12]:=true;
     M207DBBits1.Checked[13]:=true;
     M207DBBits1.Checked[14]:=false;
     M207DBBits1.Checked[15]:=true;
     M207DBBits1.Checked[16]:=true;
     M207DBBits1.Checked[17]:=false;
     M207DBBits1.Checked[18]:=true;
     M207DBBits1.Checked[19]:=false;
     M207DBBits1.Checked[20]:=false;
     M207DBBits1.Checked[21]:=false;
     M207DBBits1.Checked[22]:=false;
     M207DBBits1.Checked[23]:=false;
     M207DBBits1.Checked[24]:=false;
     M207DBBits1.Checked[25]:=false;
     M207DBBits1.Checked[26]:=false;
     M207DBBits1.Checked[27]:=false;
     M207DBBits1.Checked[28]:=false;

     M207DBBits1.UpdateData(M207DBBits1);

     for i:=0 to bitsVIEWREFBOOK.Count-1 do
      bitsVIEWREFBOOK.Checked[i]:=true;
     bitsVIEWREFBOOK.Checked[16]:=false;
     bitsVIEWREFBOOK.Checked[5]:=false;
     bitsVIEWREFBOOK.Checked[6]:=false;
     bitsVIEWREFBOOK.Checked[8]:=false;
     bitsVIEWREFBOOK.Checked[14]:=false;
     bitsVIEWREFBOOK.Checked[15]:=false;
     bitsVIEWREFBOOK.UpdateData(bitsVIEWREFBOOK);

     for i:=0 to dbbtEDITREFBOOK.Count-1 do
      dbbtEDITREFBOOK.Checked[i]:=false;
     dbbtEDITREFBOOK.Checked[10]:=true;
     dbbtEDITREFBOOK.Checked[11]:=true;
     dbbtEDITREFBOOK.Checked[13]:=true;
     dbbtEDITREFBOOK.UpdateData(dbbtEDITREFBOOK);

    end;
 end;
end;

procedure TfmAccess.M207DBBits1ClickCheck(Sender: TObject);
var i:integer;
begin
 if M207DBBits1.ItemIndex<>12 then exit;
 if M207DBBits1.Checked[12] then
 begin
  tabRefBook.Visible:=true;
  dmCom.taEmp.Edit;
  if rgPrmissions.ItemIndex=2 then
  begin
   for i:=0 to dbbtEDITREFBOOK.Count-1 do
    dbbtEDITREFBOOK.Checked[i]:=false;
   dbbtEDITREFBOOK.Checked[10]:=true;
   dbbtEDITREFBOOK.Checked[11]:=true;
   dbbtEDITREFBOOK.Checked[13]:=true;
   for i:=0 to bitsVIEWREFBOOK.Count-1 do
    bitsVIEWREFBOOK.Checked[i]:=true;
   bitsVIEWREFBOOK.Checked[16]:=false;
   bitsVIEWREFBOOK.Checked[5]:=false;
   bitsVIEWREFBOOK.Checked[6]:=false;
   bitsVIEWREFBOOK.Checked[8]:=false;
   bitsVIEWREFBOOK.Checked[14]:=false;
   bitsVIEWREFBOOK.Checked[15]:=false;
  end
  else
  begin
   for i:=0 to dbbtEDITREFBOOK.Count-1 do
    dbbtEDITREFBOOK.Checked[i]:=Centerdep;
   dbbtEDITREFBOOK.Checked[10]:=true;
   dbbtEDITREFBOOK.Checked[11]:=true;
   dbbtEDITREFBOOK.Checked[13]:=true;
   for i:=0 to bitsVIEWREFBOOK.Count-1 do
    bitsVIEWREFBOOK.Checked[i]:=true;
  end;
  dbbtEDITREFBOOK.UpdateData(dbbtEDITREFBOOK);
  bitsVIEWREFBOOK.UpdateData(bitsVIEWREFBOOK);
 end
 else
 begin
  tabRefBook.Visible:=false;
  dmCom.taEmp.Edit;  
  for i:=0 to dbbtEDITREFBOOK.Count-1 do
   dbbtEDITREFBOOK.Checked[i]:=false;
  for i:=0 to bitsVIEWREFBOOK.Count-1 do
   bitsVIEWREFBOOK.Checked[i]:=false;
  dbbtEDITREFBOOK.UpdateData(dbbtEDITREFBOOK);
  bitsVIEWREFBOOK.UpdateData(bitsVIEWREFBOOK);
 end;
end;

procedure TfmAccess.btPswdClick(Sender: TObject);
begin
 ShowAndFreeForm(TfmEditPswd, Self, TForm(fmEditPswd), True, False);
end;

end.
