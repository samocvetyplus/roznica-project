unit Data;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, Db, PFIBDataSet, PFIBQuery, StdCtrls, PFIBDatabase,
  Provider, DBClient, RxStrHlder, DateUtils, SqlTimSt,
  FIBQuery, FIBDataSet, DBCtrlsEh, TB2Item, FR_DSet, FR_DBSet,
  pFIBStoredProc, FIBDatabase, rxPlacemnt, rxSpeedBar;

const COMP_DICT=1;
      MAT_DICT=2;
      GOOD_DICT=4;
      INS_DICT=8;
      SUP_DICT=16;
      COUNTRY_DICT=32;
      GOODSSAM1_DICT=64;
      GOODSSAM2_DICT=128;
      ATT1_DICT=256;
      ATT2_DICT=512;
      DEP_DICT=1024;
      SZ_DICT=2048;      
      UserDefault=-100;

      ATT1_DICT_ROOT = -2;
      ATT2_DICT_ROOT = -2;
      DEP_DICT_ROOT = '*���';

      ALL_DICT=$FFFFFF;
      NL=#13#10;

type
  Tdm = class(TDataModule)
    taSList: TpFIBDataSet;
    taSListSINVID: TIntegerField;
    taSListSUPID: TIntegerField;
    taSListDEPID: TIntegerField;
    taSListSDATE: TDateTimeField;
    taSListNDATE: TDateTimeField;
    taSListSSF: TFIBStringField;
    taSListPMARGIN: TFloatField;
    taSListNDS: TFloatField;
    taSListTR: TFloatField;
    taSListCOST: TFloatField;
    taSListTOTALCOST: TFloatField;
    taSListSUP: TFIBStringField;
    dsSList: TDataSource;
    quSup: TpFIBDataSet;
    dsSup: TDataSource;
    quTmp: TpFIBQuery;
    taSEl: TpFIBDataSet;
    taSElSELID: TIntegerField;
    taSElSINVID: TIntegerField;
    taSElART2: TFIBStringField;
    taSElPRICE: TFloatField;
    taSElQUANTITY: TSmallintField;
    taSElFULLART: TFIBStringField;
    dsSEl: TDataSource;
    taSElTOTALWEIGHT: TFloatField;
    taSElQ: TFloatField;
    taSElCost: TFloatField;
    taSItem: TpFIBDataSet;
    taSItemSITEMID: TIntegerField;
    taSItemSELID: TIntegerField;
    taSItemW: TFloatField;
    dsSItem: TDataSource;
    taSItemRecNo: TIntegerField;
    taSElPRICE2: TFloatField;
    taSListAKCIZ: TFloatField;
    taSElPNDS: TFloatField;
    taDefDist: TpFIBDataSet;
    taDefDistDEFDISTID: TIntegerField;
    taDefDistNAME: TFIBStringField;
    dsDefDist: TDataSource;
    taDefDistDISTRIBP: TSmallintField;
    quWHUID: TpFIBDataSet;
    dsWHUID: TDataSource;
    taA2Ins: TpFIBDataSet;
    dsA2Ins: TDataSource;
    taA2InsINSID: TIntegerField;
    taA2InsART2ID: TIntegerField;
    taA2InsQUANTITY: TSmallintField;
    taA2InsWEIGHT: TFloatField;
    taA2InsCOLOR: TFIBStringField;
    taA2InsCHROMATICITY: TFIBStringField;
    taA2InsCLEANNES: TFIBStringField;
    taA2InsD_INSID: TFIBStringField;
    taA2InsIns: TStringField;
    quIns: TpFIBDataSet;
    quInsINSID: TIntegerField;
    quInsART2ID: TIntegerField;
    quInsD_INSID: TFIBStringField;
    quInsQUANTITY: TSmallintField;
    quInsWEIGHT: TFloatField;
    quInsCOLOR: TFIBStringField;
    quInsSHAPE: TFIBStringField;
    quInsCHROMATICITY: TFIBStringField;
    quInsCLEANNES: TFIBStringField;
    quInsEDGETION: TFIBStringField;
    quInsINS: TFIBStringField;
    dsIns: TDataSource;
    taSElART2ID: TIntegerField;
    taSElUNITID: TIntegerField;
    pmWH: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    quWH: TpFIBDataSet;
    dsWH: TDataSource;
    taSListISCLOSED: TSmallintField;
    quWHArt2: TpFIBDataSet;
    quWHArt2ART2ID: TIntegerField;
    quWHArt2ART2: TFIBStringField;
    quWHArt2WEIGHT: TFloatField;
    quWHArt2COST: TFloatField;
    dsWHArt2: TDataSource;
    taSItemUID: TIntegerField;
    quWHUIDSITEMID: TIntegerField;
    quWHUIDUID: TIntegerField;
    quWHUIDW: TFloatField;
    quWHUIDPRICE2: TFloatField;
    quWHUIDART2ID: TIntegerField;
    quWHUIDART2: TFIBStringField;
    quWHA2UID: TpFIBDataSet;
    quWHA2UIDSITEMID: TIntegerField;
    quWHA2UIDUID: TIntegerField;
    quWHA2UIDW: TFloatField;
    quWHA2UIDPRICE2: TFloatField;
    dsWHA2UID: TDataSource;
    quWHSZ: TpFIBDataSet;
    dsWHSZ: TDataSource;
    quWHSZWEIGHT: TFloatField;
    quWHSZCOST: TFloatField;
    quWHA2SZ: TpFIBDataSet;
    dsWHA2SZ: TDataSource;
    quWHA2SZWEIGHT: TFloatField;
    quWHA2SZCOST: TFloatField;
    taDefDistDEPID: TIntegerField;
    taDefDistQUANTITY: TFloatField;
    taDefDistWEIGHT: TFloatField;
    pmDListTo: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    pmDListFrom: TPopupMenu;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    taDList: TpFIBDataSet;
    taDListSINVID: TIntegerField;
    taDListDEPFROMID: TIntegerField;
    taDListDEPID: TIntegerField;
    taDListSDATE: TDateTimeField;
    taDListPMARGIN: TFloatField;
    taDListNDS: TFloatField;
    taDListTR: TFloatField;
    taDListCOST: TFloatField;
    taDListAKCIZ: TFloatField;
    taDListISCLOSED: TSmallintField;
    taDListDEPFROM: TFIBStringField;
    taDListDEPTO: TFIBStringField;
    dsDList: TDataSource;
    quD_WH: TpFIBDataSet;
    dsD_WH: TDataSource;
    taDItem: TpFIBDataSet;
    taDItemSITEMID: TIntegerField;
    taDItemSELID: TIntegerField;
    taDItemW: TFloatField;
    taDItemUID: TIntegerField;
    taDItemBUSYTYPE: TSmallintField;
    taDItemREF: TIntegerField;
    dsDItem: TDataSource;
    quElQW: TpFIBQuery;
    taSElRecNo: TIntegerField;
    quD_UID: TpFIBDataSet;
    quD_UIDSITEMID: TIntegerField;
    quD_UIDUID: TIntegerField;
    quD_UIDW: TFloatField;
    dsD_UID: TDataSource;
    quD_SZ: TpFIBDataSet;
    dsD_SZ: TDataSource;
    quD_SZQUANTITY: TFloatField;
    quD_SZWEIGHT: TFloatField;
    quD_SZDQ: TFloatField;
    quD_SZDW: TFloatField;
    quInsDistr: TpFIBQuery;
    quD_SZSZ: TFIBStringField;
    quWHUIDSZ: TFIBStringField;
    quWHA2UIDSZ: TFIBStringField;
    quWHSZSZ: TFIBStringField;
    quWHA2SZSZ: TFIBStringField;
    taDItemSZ: TFIBStringField;
    taSItemBUSYTYPE: TSmallintField;
    quD_T: TpFIBDataSet;
    quD_TWEIGHT: TFloatField;
    quD_TDW: TFloatField;
    dsD_T: TDataSource;
    pmPrOrd: TPopupMenu;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    taPrOrd: TpFIBDataSet;
    dsPrOrd: TDataSource;
    taPrOrdPRORDID: TIntegerField;
    taPrOrdDEPID: TIntegerField;
    taPrOrdISCLOSED: TSmallintField;
    taPrOrdDEP: TFIBStringField;
    taPrOrdItem: TpFIBDataSet;
    dsPrOrdItem: TDataSource;
    taPrOrdItemPRORDITEMID: TIntegerField;
    taPrOrdItemPRORDID: TIntegerField;
    taPrOrdItemART2ID: TIntegerField;
    taPrOrdItemFULLART: TFIBStringField;
    taPrOrdItemART2: TFIBStringField;
    taSListDEP: TFIBStringField;
    taSListSN: TIntegerField;
    taDListSN: TIntegerField;
    taSListNDSID: TIntegerField;
    taSElNDSID: TIntegerField;
    taSElNdsName: TStringField;
    taA2InsEdgType: TStringField;
    taA2InsEdgShape: TStringField;
    taDepMargin: TpFIBDataSet;
    taDepMarginD_DEPID: TIntegerField;
    taDepMarginNAME: TFIBStringField;
    taDepMarginMARGIN: TFloatField;
    taDepMarginROUNDNORM: TSmallintField;
    dsDepMargin: TDataSource;
    pmPrice: TPopupMenu;
    taSElART: TFIBStringField;
    taSElDISTRED: TSmallintField;
    taSetPrice: TpFIBDataSet;
    taSetPriceNAME: TFIBStringField;
    taSetPricePRICE1: TFloatField;
    taSetPricePRICE2: TFloatField;
    dsSetPrice: TDataSource;
    taPrOrdSN: TIntegerField;
    taPrOrdItemPRICE2: TFloatField;
    taPrOrdItemRQ: TFloatField;
    taPrOrdItemRW: TFloatField;
    taSetPriceDEPID: TIntegerField;
    taSElUSEMARGIN: TSmallintField;
    taPrOrdItemWHRQ: TFloatField;
    taPrOrdItemWHRW: TFloatField;
    taPrOrdItemCURPRICE: TFloatField;
    taSellList: TpFIBDataSet;
    dsSellList: TDataSource;
    taSellListSELLID: TIntegerField;
    taSellListBD: TDateTimeField;
    taSellListED: TDateTimeField;
    taSellListDEPID: TIntegerField;
    pmSell: TPopupMenu;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    pmSellItem: TPopupMenu;
    taSellItem: TpFIBDataSet;
    taSellItemSELLITEMID: TIntegerField;
    taSellItemSELLID: TIntegerField;
    taSellItemADATE: TDateTimeField;
    dsSellItem: TDataSource;
    taSellListN: TIntegerField;
    taSellItemDEP: TSmallintField;
    taSellItemCLIENTID: TIntegerField;
    taSellItemPRICE: TFloatField;
    taSellItemFULLART: TFIBStringField;
    taSellItemART2: TFIBStringField;
    taSellItemPRICE0: TFloatField;
    taSellItemART2ID: TIntegerField;
    taSellItemSITEMID: TIntegerField;
    taSellItemW: TFloatField;
    taSellItemSZ: TFIBStringField;
    taSellItemUID: TIntegerField;
    taSellListDEP: TFIBStringField;
    taSellListCNT: TIntegerField;
    taSellListW: TFloatField;
    taSellListCOST: TFloatField;
    taA2: TpFIBDataSet;
    dsA2: TDataSource;
    taA2ART2ID: TIntegerField;
    taA2ARTID: TIntegerField;
    taA2SUPID: TIntegerField;
    taA2INVID: TIntegerField;
    taA2USEMARGIN: TSmallintField;
    taA2PRICE1: TFloatField;
    taA2PRICE2: TFloatField;
    taA2NDSID: TFloatField;
    taA2nds: TStringField;
    taSListPAYTYPEID: TIntegerField;
    quPrice: TpFIBDataSet;
    dsPrice: TDataSource;
    taA2OPTPRICE: TFloatField;
    taSetPriceOPTPRICE: TFloatField;
    taA2PDIF: TSmallintField;
    quPriceARTID: TIntegerField;
    quPriceART2ID: TIntegerField;
    quPriceART2: TFIBStringField;
    quArt: TpFIBDataSet;
    dsArt: TDataSource;
    quArtD_ARTID: TIntegerField;
    quArtD_COMPID: TIntegerField;
    quArtD_MATID: TFIBStringField;
    quArtD_GOODID: TFIBStringField;
    quArtART: TFIBStringField;
    quArtUNITID: TIntegerField;
    quArtD_INSID: TFIBStringField;
    quArtFULLART: TFIBStringField;
    quPriceART: TFIBStringField;
    taPrOrdPRORD: TIntegerField;
    taPrOrdISSET: TSmallintField;
    taPrOrdCLOSEDATE: TDateTimeField;
    taPrOrdSETDATE: TDateTimeField;
    taPrOrdCLOSEEMPID: TIntegerField;
    taPrOrdSETEMPID: TIntegerField;
    taPrOrdCLOSEFIO: TFIBStringField;
    taPrOrdSETFIO: TFIBStringField;
    taPrOrdItemCUROP: TFloatField;
    taPrOrdItemCURSP: TFloatField;
    taPrOrdItemPRICE: TFloatField;
    taPrOrdItemOPTPRICE: TFloatField;
    quDPrice: TpFIBDataSet;
    dsDPrice: TDataSource;
    quDPriceARTID: TIntegerField;
    quDPriceART2ID: TIntegerField;
    quDPriceART2: TFIBStringField;
    quDPriceART: TFIBStringField;
    quDPricePRICE: TFloatField;
    quDPriceOPTPRICE: TFloatField;
    quDPriceSPRICE: TFloatField;
    quArtRQ: TFloatField;
    quArtRW: TFloatField;
    quARest: TpFIBDataSet;
    dsARest: TDataSource;
    quARestDEPID: TIntegerField;
    quARestDEP: TFIBStringField;
    quARestRW: TFloatField;
    taA2RQ: TFloatField;
    taA2RW: TFloatField;
    quA2Rest: TpFIBDataSet;
    quA2RestDEPID: TIntegerField;
    quA2RestDEP: TFIBStringField;
    quA2RestRW: TFloatField;
    dsA2Rest: TDataSource;
    dsEmp: TDataSource;
    quEmp: TpFIBDataSet;
    quEmpD_EMPID: TIntegerField;
    quEmpFIO: TFIBStringField;
    taSellItemEMPID: TIntegerField;
    taSellItemMEMO: TMemoField;
    taSellItemUNITID: TIntegerField;
    quDPriceUNITID: TSmallintField;
    quFindUID: TpFIBDataSet;
    dsFindUID: TDataSource;
    quFindUIDSITEMID: TIntegerField;
    quFindUIDSZ: TFIBStringField;
    quFindUIDW: TFloatField;
    quFindUIDUID: TIntegerField;
    quFindUIDART2ID: TIntegerField;
    quFindUIDART2: TFIBStringField;
    quFindUIDFULLART: TFIBStringField;
    quFindUIDUNITID: TSmallintField;
    quFindUIDPRICE: TFloatField;
    taSellItemCHECKNO: TIntegerField;
    taCurSell: TpFIBDataSet;
    dsCurSell: TDataSource;
    taCurSellSELLID: TIntegerField;
    taCurSellBD: TDateTimeField;
    taCurSellED: TDateTimeField;
    taCurSellDEPID: TIntegerField;
    taCurSellN: TIntegerField;
    taCurSellEMP1ID: TIntegerField;
    taCurSellEMP2ID: TIntegerField;
    taA2InsGR: TFIBStringField;
    taA2W: TFloatField;
    taSElCost2: TFloatField;
    quWHA2UIDSUP: TFIBStringField;
    quWHA2UIDSUPID: TIntegerField;
    quD_UIDSUP: TFIBStringField;
    quD_UIDPRICE: TFloatField;
    quD_UIDSUPID: TIntegerField;
    quDistrPrice: TpFIBDataSet;
    dsDistrPrice: TDataSource;
    taSListTW: TFloatField;
    taSListTQ: TSmallintField;
    pmOptList: TPopupMenu;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    taOptList: TpFIBDataSet;
    dsOptList: TDataSource;
    taOptListSINVID: TIntegerField;
    taOptListDEPFROMID: TIntegerField;
    taOptListSDATE: TDateTimeField;
    taOptListSN: TIntegerField;
    taOptListPMARGIN: TFloatField;
    taOptListNDS: TFloatField;
    taOptListTR: TFloatField;
    taOptListCOST: TFloatField;
    taOptListAKCIZ: TFloatField;
    taOptListISCLOSED: TSmallintField;
    taOptListDEPFROM: TFIBStringField;
    taOptListCOMPID: TIntegerField;
    taOptListCOMP: TFIBStringField;
    taSElWHSELL: TSmallintField;
    quWHA2UIDNDSID: TIntegerField;
    quWHA2UIDNDSNAME: TFIBStringField;
    taSellItemNAME: TFIBStringField;
    quSupD_COMPID: TIntegerField;
    quSupNAME: TFIBStringField;
    quSupSNAME: TFIBStringField;
    taSListPTR: TFloatField;
    taSListPNDS: TFloatField;
    taPrOrdITYPE: TSmallintField;
    taPrOrdSNstr: TStringField;
    quD_SZT: TSmallintField;
    pmRetList: TPopupMenu;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    taSellItemMAT: TFIBStringField;
    taSellItemDISC: TFloatField;
    quUIDWH: TpFIBDataSet;
    quUIDWHSITEMID: TIntegerField;
    quUIDWHUID: TIntegerField;
    quUIDWHMATID: TFIBStringField;
    quUIDWHGOODID: TFIBStringField;
    quUIDWHINSID: TFIBStringField;
    quUIDWHART: TFIBStringField;
    quUIDWHART2: TFIBStringField;
    quUIDWHUNITID: TIntegerField;
    quUIDWHNDSNAME: TFIBStringField;
    quUIDWHNDSID: TIntegerField;
    quUIDWHPRICE: TFloatField;
    quUIDWHW: TFloatField;
    quUIDWHSZ: TFIBStringField;
    quUIDWHDEP: TFIBStringField;
    quUIDWHCOST: TFloatField;
    quUIDWHART2ID: TIntegerField;
    quUIDWHDEPID: TIntegerField;
    dsUIDWH: TDataSource;
    quWHD_ARTID: TIntegerField;
    quWHART: TFIBStringField;
    quWHUNITID: TIntegerField;
    quWHCOMP: TFIBStringField;
    quWHMAT: TFIBStringField;
    quWHGOOD: TFIBStringField;
    quWHINS: TFIBStringField;
    quWHWEIGHT: TFloatField;
    quWHUIDSPRICE: TFloatField;
    quWHUIDSUP: TFIBStringField;
    quSupCODE: TFIBStringField;
    taPrOrdSSF: TFIBStringField;
    quUIDWHCOLOR: TIntegerField;
    taSellItemDISCMEMO: TFIBStringField;
    taSElPRODCODE: TFIBStringField;
    taSElD_MATID: TFIBStringField;
    taSElD_GOODID: TFIBStringField;
    taSElD_INSID: TFIBStringField;
    taSElD_COMPID: TIntegerField;
    taSListTRNDS: TFloatField;
    quPriceD_COMPID: TIntegerField;
    quPricePRODCODE: TFIBStringField;
    quPriceD_MATID: TFIBStringField;
    quPriceD_GOODID: TFIBStringField;
    quPriceD_INSID: TFIBStringField;
    quWHCOST: TFloatField;
    quDPriceD_COMPID: TIntegerField;
    quDPriceD_MATID: TFIBStringField;
    quDPriceD_GOODID: TFIBStringField;
    quDPriceD_INSID: TFIBStringField;
    quDPricePRODCODE: TFIBStringField;
    quUIDWHPRODCODE: TFIBStringField;
    quUIDWHSUPCODE: TFIBStringField;
    quUIDWHSN: TIntegerField;
    taDListCOLOR: TIntegerField;
    taSellItemRET: TSmallintField;
    taSElD_ARTID: TIntegerField;
    quUIDWHSN0: TIntegerField;
    quUIDWHSSF0: TFIBStringField;
    quUIDWHSPRICE0: TFloatField;
    quUIDWHSUPID0: TIntegerField;
    quUIDWHSDATE0: TDateTimeField;
    quUIDWHSDATE: TDateTimeField;
    quUIDWHSELLDATE: TDateTimeField;
    quUIDWHT: TSmallintField;
    quD_UIDSZ: TFIBStringField;
    quDst: TpFIBDataSet;
    dsDst: TDataSource;
    quDstSITEMID: TIntegerField;
    quDstUID: TIntegerField;
    quDstW: TFloatField;
    quDstSZ: TFIBStringField;
    quDstDEPID: TIntegerField;
    quDstISCLOSED: TSmallintField;
    quDstDEP: TFIBStringField;
    quDstCOLOR: TIntegerField;
    quDistred: TpFIBDataSet;
    dsDistred: TDataSource;
    quDistredPRODCODE: TFIBStringField;
    quDistredD_COMPID: TIntegerField;
    quDistredD_MATID: TFIBStringField;
    quDistredD_GOODID: TFIBStringField;
    quDistredD_INSID: TFIBStringField;
    quDistredART: TFIBStringField;
    quDistredART2: TFIBStringField;
    quDistredSZ: TFIBStringField;
    quDistredQ: TIntegerField;
    quDistredW: TFloatField;
    quDistredPRICE: TFloatField;
    quD_Q: TpFIBDataSet;
    dsD_Q: TDataSource;
    quD_QDEPID: TIntegerField;
    quD_QDEP: TFIBStringField;
    quD_QQ1: TIntegerField;
    quD_QW1: TFloatField;
    quD_QQ2: TIntegerField;
    quD_QW2: TFloatField;
    quD_QPRICE: TFloatField;
    quWHQUANTITY: TIntegerField;
    quWHArt2QUANTITY: TIntegerField;
    quWHArt2PRICE2: TFloatField;
    quWHArt2DEP: TFIBStringField;
    quWHUIDDEPID: TIntegerField;
    quWHUIDSDATE: TDateTimeField;
    quWHUIDSN: TIntegerField;
    quWHUIDSSF: TFIBStringField;
    quWHUIDDEP: TFIBStringField;
    quWHSZQUANTITY: TIntegerField;
    quWHA2UIDDEPID: TIntegerField;
    quWHA2UIDSINFOID: TIntegerField;
    quWHA2UIDPRICE: TFloatField;
    quWHA2UIDSSF: TFIBStringField;
    quWHA2UIDSDATE: TDateTimeField;
    quWHA2UIDSN: TIntegerField;
    quWHA2SZQUANTITY: TIntegerField;
    quARestRQ: TIntegerField;
    quA2RestRQ: TIntegerField;
    quDPriceFULLART: TFIBStringField;
    taSetPricePRICEID: TIntegerField;
    quD_QART2ID: TIntegerField;
    quD_QPEQ: TSmallintField;
    taSElEP2: TFloatField;
    quDistredART2ID: TIntegerField;
    quD_UIDNDS: TFIBStringField;
    taPrOrdItemOLDP: TFloatField;
    taPrOrdItemOLDP2: TFloatField;
    taPrOrdItemOLDOP: TFloatField;
    quD_UIDISCLOSED: TSmallintField;
    quDstREF: TIntegerField;
    taSellItemSDATE: TDateTimeField;
    taSellItemSPRICE: TFloatField;
    taSellItemSUP: TFIBStringField;
    taSellItemSINFOID: TIntegerField;
    quSupFNAME: TStringField;
    quPrepUIDWH: TpFIBQuery;
    taCurSellDEP: TFIBStringField;
    taCurSellCNT: TIntegerField;
    taCurSellW: TFloatField;
    taCurSellCOST: TFloatField;
    taPrOrdItemQ: TIntegerField;
    taPrOrdItemW: TFloatField;
    dsSelled: TDataSource;
    taSellItemRETSITEMID: TIntegerField;
    quDPriceOPEQ: TFloatField;
    quArtUSERID: TIntegerField;
    taSellItemSN: TIntegerField;
    taPrOrdPRDATE: TDateTimeField;
    taSellItemRQ: TIntegerField;
    taSellItemRW: TFloatField;
    taSellItemUSERID: TIntegerField;
    quUIDWHISCLOSED: TSmallintField;
    quSListT: TpFIBDataSet;
    quSListTCOST: TFloatField;
    quSListTQ: TIntegerField;
    quSListTW: TFloatField;
    dsSListT: TDataSource;
    taDListSCOST: TFloatField;
    quDListT: TpFIBDataSet;
    quDListTCOST: TFloatField;
    quDListTSCOST: TFloatField;
    quDListTQ: TIntegerField;
    quDListTW: TFloatField;
    dsDListT: TDataSource;
    taOptListSCOST: TFloatField;
    taSListISIMPORT: TSmallintField;
    taSElEX_PRICE2: TFloatField;
    taSellListRCNT: TIntegerField;
    taSellListRW: TFloatField;
    taSellListRCOST: TFloatField;
    taCurSellRCNT: TIntegerField;
    taCurSellRW: TFloatField;
    taCurSellRCOST: TFloatField;
    taSellListCASS: TFloatField;
    taCurSellCASS: TFloatField;
    taCurSellRN: TIntegerField;
    taSellListRN: TIntegerField;
    taSListUSERID: TIntegerField;
    taSListFIO: TFIBStringField;
    taDListUSERID: TIntegerField;
    taDListFIO: TFIBStringField;
    quUIDWHNDATE: TDateTimeField;
    taSellItemART: TFIBStringField;
    taSellItemOp: TStringField;
    taPrOrdSINVID: TIntegerField;
    taPrOrdQ: TIntegerField;
    taPrOrdItemUNITID: TSmallintField;
    taPrOrdItemCost: TFloatField;
    taPrOrdItemOldCost: TFloatField;
    taPrOrdItemdPrice: TFloatField;
    taPrOrdItemdCost: TFloatField;
    taPrOrdCOST: TFloatField;
    taPrOrdOLDCOST: TFloatField;
    taPrOrdDCOST: TFloatField;
    quPrSI: TpFIBDataSet;
    quPrSIPRSIID: TIntegerField;
    quPrSISITEMID: TIntegerField;
    quPrSIUID: TIntegerField;
    quPrSIW: TFloatField;
    quPrSISZ: TFIBStringField;
    quPrSISDATE: TDateTimeField;
    quPrSISUP: TFIBStringField;
    quPrSISN: TIntegerField;
    quPrSISUPID: TIntegerField;
    quPrSISPRICE: TFloatField;
    dsPrSI: TDataSource;
    pmSRetList: TPopupMenu;
    taSRetList: TpFIBDataSet;
    dsSRetList: TDataSource;
    taSRet: TpFIBDataSet;
    taSRetSRETID: TIntegerField;
    taSRetSINVID: TIntegerField;
    taSRetART2ID: TIntegerField;
    taSRetSITEMID: TIntegerField;
    taSRetUID: TIntegerField;
    taSRetW: TFloatField;
    taSRetSZ: TFIBStringField;
    taSRetQ0: TFloatField;
    taSRetSPRICE: TFloatField;
    taSRetCOST: TFloatField;
    taSRetSDATE: TDateTimeField;
    taSRetSUPID: TIntegerField;
    taSRetSSF: TFIBStringField;
    taSRetSN: TIntegerField;
    taSRetNDATE: TDateTimeField;
    taSRetNDSID: TIntegerField;
    taSRetPRODID: TIntegerField;
    taSRetPROD: TFIBStringField;
    taSRetGOODID: TFIBStringField;
    taSRetINSID: TFIBStringField;
    taSRetMATID: TFIBStringField;
    taSRetART: TFIBStringField;
    taSRetART2: TFIBStringField;
    taSRetSUP: TFIBStringField;
    taSRetUNITID: TSmallintField;
    dsSRet: TDataSource;
    taSRetRecNo: TIntegerField;
    taSRetD_RETID: TFIBStringField;
    taORetList: TpFIBDataSet;
    dsORetList: TDataSource;
    taORetListSINVID: TIntegerField;
    taORetListDEPID: TIntegerField;
    taORetListSDATE: TDateTimeField;
    taORetListSN: TIntegerField;
    taORetListPMARGIN: TFloatField;
    taORetListNDS: TFloatField;
    taORetListTR: TFloatField;
    taORetListCOST: TFloatField;
    taORetListAKCIZ: TFloatField;
    taORetListISCLOSED: TSmallintField;
    taORetListDEP: TFIBStringField;
    taORetListCOMPID: TIntegerField;
    taORetListCOMP: TFIBStringField;
    taORetListSCOST: TFloatField;
    pmORetList: TPopupMenu;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    quOSelled: TpFIBDataSet;
    quOSelledSITEMID: TIntegerField;
    quOSelledUID: TIntegerField;
    quOSelledPRODCODE: TFIBStringField;
    quOSelledMATID: TFIBStringField;
    quOSelledGOODID: TFIBStringField;
    quOSelledINSID: TFIBStringField;
    quOSelledART: TFIBStringField;
    quOSelledART2: TFIBStringField;
    quOSelledUNITID: TIntegerField;
    quOSelledNDSNAME: TFIBStringField;
    quOSelledNDSVALUE: TFloatField;
    quOSelledNDSID: TIntegerField;
    quOSelledSPRICE: TFloatField;
    quOSelledW: TFloatField;
    quOSelledSZ: TFIBStringField;
    quOSelledCOST: TFloatField;
    quOSelledART2ID: TIntegerField;
    quOSelledSN0: TIntegerField;
    quOSelledSSF0: TFIBStringField;
    quOSelledSDATE0: TDateTimeField;
    quOSelledSN: TIntegerField;
    quOSelledSDATE: TDateTimeField;
    quOSelledNDATE0: TDateTimeField;
    quOSelledFULLART: TFIBStringField;
    quOSelledOPTPRICE: TFloatField;
    quOSelledSUPID: TIntegerField;
    quOSelledSINFOID: TIntegerField;
    quOSelledSUP: TFIBStringField;
    quOSelledPRODID: TIntegerField;
    quOSelledBUYERID: TIntegerField;
    quOSelledBUYER: TFIBStringField;
    dsOSelled: TDataSource;
    taORet: TpFIBDataSet;
    taORetSITEMID: TIntegerField;
    taORetSINVID: TIntegerField;
    taORetART2ID: TIntegerField;
    taORetUID: TIntegerField;
    taORetW: TFloatField;
    taORetSZ: TFIBStringField;
    taORetQ0: TFloatField;
    taORetSPRICE: TFloatField;
    taORetSDATE: TDateTimeField;
    taORetSSF: TFIBStringField;
    taORetSN: TIntegerField;
    taORetNDATE: TDateTimeField;
    taORetNDSID: TIntegerField;
    taORetD_RETID: TFIBStringField;
    taORetPRODID: TIntegerField;
    taORetPROD: TFIBStringField;
    taORetGOODID: TFIBStringField;
    taORetINSID: TFIBStringField;
    taORetMATID: TFIBStringField;
    taORetART: TFIBStringField;
    taORetART2: TFIBStringField;
    taORetUNITID: TSmallintField;
    taORetD_ARTID: TIntegerField;
    taORetSINFOID: TIntegerField;
    taORetSUPID: TIntegerField;
    taORetOPRICE: TFloatField;
    taORetPRICE2: TFloatField;
    dsORet: TDataSource;
    taORetRecNo: TIntegerField;
    taORetNDS: TStringField;
    taORetRET: TStringField;
    taORetSup: TStringField;
    taORetPRICE0: TFloatField;
    taORetCost: TFloatField;
    quOSelledOPRICE: TFloatField;
    quOSelledPRICE2: TFloatField;
    taSellItemD_RETID: TFIBStringField;
    taOptListSUPID: TIntegerField;
    quUIDWHMemo: TStringField;
    quUIDWHITYPE: TSmallintField;
    taPrOrdPACTCLOSED: TSmallintField;
    taA2USERID: TIntegerField;
    taA2F1: TSmallintField;
    taA2F2: TSmallintField;
    quArtF1: TSmallintField;
    quArtF2: TSmallintField;
    quFCReate: TpFIBQuery;
    quFRemove: TpFIBQuery;
    quCreateExtA_1: TpFIBQuery;
    quDropExtA_1: TpFIBQuery;
    quAToExt: TpFIBQuery;
    quSListTTR: TFloatField;
    quSListTCNOTR: TFloatField;
    quD_TQUANTITY: TIntegerField;
    quD_TDQ: TIntegerField;
    quUIDWHPRODID: TIntegerField;
    taCurSellCLOSED: TSmallintField;
    taSellListCLOSED: TSmallintField;
    quSRetUID: TpFIBDataSet;
    dsSRetUID: TDataSource;
    quSRetUIDSITEMID: TIntegerField;
    quSRetUIDUID: TIntegerField;
    quSRetUIDW: TFloatField;
    quSRetUIDSZ: TFIBStringField;
    quSRetUIDDEPID: TIntegerField;
    quSRetUIDPRICE2: TFloatField;
    quSRetUIDSINFOID: TIntegerField;
    quSRetUIDPRICE: TFloatField;
    quSRetUIDSUPID: TIntegerField;
    quSRetUIDNDSID: TIntegerField;
    quSRetUIDSN: TIntegerField;
    quSRetUIDSSF: TFIBStringField;
    quSRetUIDSDATE: TDateTimeField;
    quSRetUIDSUP: TFIBStringField;
    quSRetUIDNDSNAME: TFIBStringField;
    taSItemSS: TFIBStringField;
    pmPHist: TPopupMenu;
    quMaxArtPrice: TpFIBDataSet;
    dsMaxArtPrice: TDataSource;
    quInsTmpUID: TpFIBQuery;
    quTmp2: TpFIBQuery;
    taSListPTRNDS: TFloatField;
    pmRepl: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    taSellListUSESDATE: TSmallintField;
    taSellItemRETSELLITEMID: TIntegerField;
    taSellItemRETDONE: TSmallintField;
    taA2InsEDGTYPEID: TFIBStringField;
    taA2InsEDGSHAPEID: TFIBStringField;
    quPrOrd: TpFIBDataSet;
    quPrOrdPRORDID: TIntegerField;
    quPrOrdDEPID: TIntegerField;
    quPrOrdCLOSEDATE: TDateTimeField;
    quPrOrdSETDATE: TDateTimeField;
    quPrOrdISCLOSED: TSmallintField;
    quPrOrdPRORD: TIntegerField;
    quPrOrdISSET: TSmallintField;
    quPrOrdPRDATE: TDateTimeField;
    quPrOrdCLOSEEMPID: TIntegerField;
    quPrOrdSETEMPID: TIntegerField;
    quPrOrdITYPE: TSmallintField;
    quPrOrdDEP: TFIBStringField;
    quPrOrdSN: TIntegerField;
    quPrOrdSSF: TFIBStringField;
    quPrOrdCLOSEFIO: TFIBStringField;
    quPrOrdSETFIO: TFIBStringField;
    quPrOrdSINVID: TIntegerField;
    quPrOrdQ: TIntegerField;
    quPrOrdCOST: TFloatField;
    quPrOrdOLDCOST: TFloatField;
    quPrOrdDCOST: TFloatField;
    quPrOrdPACTCLOSED: TIntegerField;
    quPrOrdSNStr: TStringField;
    quTemp: TpFIBDataSet;
    fs: TFormStorage;
    quADict: TpFIBDataSet;
    dsADict: TDataSource;
    quADictD_ARTID: TIntegerField;
    quADictD_COMPID: TIntegerField;
    quADictD_MATID: TFIBStringField;
    quADictD_GOODID: TFIBStringField;
    quADictART: TFIBStringField;
    quADictUNITID: TIntegerField;
    quADictD_INSID: TFIBStringField;
    quADictFULLART: TFIBStringField;
    quA2Dict: TpFIBDataSet;
    quA2DictART2ID: TIntegerField;
    quA2DictD_ARTID: TIntegerField;
    quA2DictART2: TFIBStringField;
    dsA2Dict: TDataSource;
    quA2DictSUPID: TIntegerField;
    quGetArt: TpFIBDataSet;
    quGetArtD_ARTID: TIntegerField;
    quGetArtD_MATID: TFIBStringField;
    quGetArtD_GOODID: TFIBStringField;
    quGetArtD_INSID: TFIBStringField;
    quGetArtART: TFIBStringField;
    quGetArtUNITID: TIntegerField;
    quGetArtART2: TFIBStringField;
    quGetArtPRODCODE: TFIBStringField;
    quUIDWHUSERID: TIntegerField;
    quPrOrdNACT: TIntegerField;
    taPrOrdNACT: TIntegerField;
    quNewA2Ins: TpFIBQuery;
    taDListCRUSERID: TIntegerField;
    quUIDWHRETDATE: TDateTimeField;
    taDListCRFIO: TFIBStringField;
    taDListURF_FROM: TSmallintField;
    taDListURF_TO: TSmallintField;
    taReturn_URF: TpFIBDataSet;
    dsReturn_Urf: TDataSource;
    taA2ART2: TStringField;
    taSElSSUM: TFloatField;
    taSElSSUMD: TFloatField;
    taDListSCOSTD: TFloatField;
    taDListOPT: TSmallintField;
    taOptListOPT: TSmallintField;
    taOptListITYPE: TSmallintField;
    taORetListOPTRET: TSmallintField;
    taORetListITYPE: TSmallintField;
    taDListOPTRET: TSmallintField;
    quDstNDS: TFIBStringField;
    taDItemSPRICE: TFloatField;
    quDistredNDS: TFIBStringField;
    taSellItemSQ: TFIBStringField;
    taSellItemSW: TFIBStringField;
    taDListRSTATE: TSmallintField;
    quSupPAYTYPEID: TIntegerField;
    quSupNDSID: TIntegerField;
    taSListOPTMARGIN: TFloatField;
    taSListOPTRNORM: TSmallintField;
    taSElSUPCODE: TFIBStringField;
    quUIDSelled: TpFIBDataSet;
    dsUIDSelled: TDataSource;
    quUIDSelledSITEMID: TIntegerField;
    quUIDSelledUID: TIntegerField;
    quUIDSelledPRODCODE: TFIBStringField;
    quUIDSelledMATID: TFIBStringField;
    quUIDSelledGOODID: TFIBStringField;
    quUIDSelledINSID: TFIBStringField;
    quUIDSelledART: TFIBStringField;
    quUIDSelledART2: TFIBStringField;
    quUIDSelledUNITID: TIntegerField;
    quUIDSelledNDSNAME: TFIBStringField;
    quUIDSelledNDSVALUE: TFloatField;
    quUIDSelledNDSID: TIntegerField;
    quUIDSelledPRICE: TFloatField;
    quUIDSelledW: TFloatField;
    quUIDSelledSZ: TFIBStringField;
    quUIDSelledSUPCODE: TFIBStringField;
    quUIDSelledCOST: TFloatField;
    quUIDSelledART2ID: TIntegerField;
    quUIDSelledSN0: TIntegerField;
    quUIDSelledSSF0: TFIBStringField;
    quUIDSelledSPRICE0: TFloatField;
    quUIDSelledSUPID0: TIntegerField;
    quUIDSelledSDATE0: TDateTimeField;
    quUIDSelledSN: TIntegerField;
    quUIDSelledSDATE: TDateTimeField;
    quUIDSelledSELLDATE: TDateTimeField;
    quUIDSelledT: TSmallintField;
    quUIDSelledMAT: TFIBStringField;
    quUIDSelledSELLITEMID: TIntegerField;
    quUIDSelledCLIENTID: TIntegerField;
    quUIDSelledCLIENT: TFIBStringField;
    quUIDSelledCHECKNO: TIntegerField;
    quUIDSelledFULLART: TFIBStringField;
    quUIDSelledPRICE0: TFloatField;
    quUIDSelledRETDONE: TSmallintField;
    quTempUIDWH: TpFIBDataSet;
    dsTempUIDWH: TDataSource;
    quUIDWHD_ARTID: TIntegerField;
    quUIDWHCOST1: TFloatField;
    taPrOrdRSTATE_P: TSmallintField;
    taPrOrdRSTATE_A: TSmallintField;
    taSellListRSTATE: TSmallintField;
    taPrMargin: TpFIBDataSet;
    taPrMarginD_DEPID: TIntegerField;
    taPrMarginNAME: TFIBStringField;
    dsPrMargin: TDataSource;
    taPrMarginTMARGIN: TFloatField;
    taPrMarginTROUNDNORM: TSmallintField;
    quUIDWHS_INS: TFIBStringField;
    taSellItemRETPRICE: TFloatField;
    quArtD_COUNTRYID: TFIBStringField;
    taSElD_COUNTRYID: TFIBStringField;
    quUIDWHD_COUNTRYID: TFIBStringField;
    quDPriceD_COUNTRYID: TFIBStringField;
    quADictD_COUNTRYID: TFIBStringField;
    taA2THISINV: TSmallintField;
    taDListNOTPR: TSmallintField;
    taSellItemopt: TSmallintField;
    quTempUIDWHD_DEPID: TIntegerField;
    quTempUIDWHC: TIntegerField;
    quTempUIDWHSAC: TIntegerField;
    quTempUIDWHSTW: TFloatField;
    quTempUIDWHSTP: TFloatField;
    quTempUIDWHSTP2: TFloatField;
    quUIDWHCOMPID: TIntegerField;
    taPrOrdONLYACT: TSmallintField;
    quPrSINEWPRICE: TFloatField;
    quPrSIOLDPRICE: TFloatField;
    taPrOrdItemQ0: TFloatField;
    taSellItemSQR: TFIBStringField;
    taSellItemSWR: TFIBStringField;
    taDListCOST0: TFloatField;
    taDListCost1: TCurrencyField;
    quArtAVG_W: TFloatField;
    quArtAVG_SPR: TFloatField;
    pmAddTopr: TPopupMenu;
    N5: TMenuItem;
    N6: TMenuItem;
    taSListCost2: TCurrencyField;
    taSListNOTPR: TSmallintField;
    taSListNAMEPAYTYPE: TFIBStringField;
    translTmp: TpFIBQuery;
    taCurSellFREG: TSmallintField;
    taSellListEMP1ID: TIntegerField;
    taSellItemCLIENTNAME: TFIBStringField;
    taSellItemF: TIntegerField;
    taSellItemNODCARD: TFIBStringField;
    quUIDWHUIDWHID: TIntegerField;
    dsInOutPrice: TDataSource;
    NoCloseRetAct: TpFIBQuery;
    quInOutPrice: TpFIBDataSet;
    quInOutPricePRICE1: TFloatField;
    quInOutPricePRICE2: TFloatField;
    quInOutPriceSN: TIntegerField;
    quInOutPriceART: TFIBStringField;
    quUIDWHNAMEDEPFROM: TFIBStringField;
    quUIDWHCOLORDF: TIntegerField;
    pmJSz: TPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    quToCheckShiftTime: TpFIBQuery;
    quSelled: TpFIBDataSet;
    quSellElDel: TpFIBQuery;
    quSelledSITEMID: TIntegerField;
    quSelledUID: TIntegerField;
    quSelledPRODCODE: TFIBStringField;
    quSelledMATID: TFIBStringField;
    quSelledGOODID: TFIBStringField;
    quSelledINSID: TFIBStringField;
    quSelledART: TFIBStringField;
    quSelledART2: TFIBStringField;
    quSelledUNITID: TIntegerField;
    quSelledPRICE: TFloatField;
    quSelledW: TFloatField;
    quSelledSZ: TFIBStringField;
    quSelledSUPCODE: TFIBStringField;
    quSelledCOST: TFloatField;
    quSelledART2ID: TIntegerField;
    quSelledSN0: TIntegerField;
    quSelledSSF0: TFIBStringField;
    quSelledSPRICE0: TFloatField;
    quSelledSUPID0: TIntegerField;
    quSelledSDATE0: TDateTimeField;
    quSelledSN: TIntegerField;
    quSelledSDATE: TDateTimeField;
    quSelledSELLDATE: TDateTimeField;
    quSelledSELLITEMID: TIntegerField;
    quSelledCLIENTID: TIntegerField;
    quSelledCLIENT: TFIBStringField;
    quSelledCHECKNO: TIntegerField;
    quSelledNDATE: TDateTimeField;
    quSelledFULLART: TFIBStringField;
    quSelledPRICE0: TFloatField;
    quSelledRETDONE: TSmallintField;
    quTmp1: TpFIBQuery;
    quUIDWHSUPNAME: TFIBStringField;
    taSItemD_GOODS_SAM1: TIntegerField;
    taSItemD_GOODS_SAM2: TIntegerField;
    taSItemgoods1: TStringField;
    taSItemgoods2: TStringField;
    quUIDWHGOODS1: TFIBStringField;
    quUIDWHGOODS2: TFIBStringField;
    quUIDWHPRODNAME: TStringField;
    quUIDWHGOODSID1: TIntegerField;
    quUIDWHGOODSID2: TIntegerField;
    quCurSellCheck: TpFIBDataSet;
    dsCurSellCheck: TDataSource;
    quCurSellCheckSELLID: TIntegerField;
    quCurSellCheckBD: TDateTimeField;
    quCurSellCheckED: TDateTimeField;
    quCurSellCheckDEPID: TIntegerField;
    quCurSellCheckN: TIntegerField;
    quCurSellCheckEMP1ID: TIntegerField;
    quCurSellCheckEMP2ID: TIntegerField;
    quCurSellCheckDEP: TFIBStringField;
    quCurSellCheckCNT: TIntegerField;
    quCurSellCheckW: TFloatField;
    quCurSellCheckCOST: TFloatField;
    quCurSellCheckRCNT: TIntegerField;
    quCurSellCheckRW: TFloatField;
    quCurSellCheckRCOST: TFloatField;
    quCurSellCheckRN: TIntegerField;
    quCurSellCheckCLOSED: TSmallintField;
    quCurSellCheckFREG: TSmallintField;
    quCurSellCheckCASS: TFloatField;
    taSellListFREG: TIntegerField;
    quCurSellCheckCHECKNO: TIntegerField;
    taPrOrdItemISREVAL: TIntegerField;
    quPrOrdISREVAL: TIntegerField;
    taPrOrdISREVAL: TIntegerField;
    taSellItemEMP: TFIBStringField;
    quDInvItem: TpFIBDataSet;
    dsDInvItem: TDataSource;
    quDInvItemPRODCODE: TFIBStringField;
    quDInvItemD_MATID: TFIBStringField;
    quDInvItemD_GOODID: TFIBStringField;
    quDInvItemD_INSID: TFIBStringField;
    quDInvItemART: TFIBStringField;
    quDInvItemART2: TFIBStringField;
    quDInvItemUID: TIntegerField;
    quDInvItemQ0: TFloatField;
    quDInvItemSZ: TFIBStringField;
    quDInvItemPPRICE: TFloatField;
    quDInvItemDEPFROMPRICE: TFloatField;
    quDInvItemDEPPRICE: TFloatField;
    quDInvItemOPTPRICE: TFloatField;
    quDInvItemSITEMID: TIntegerField;
    quDInvItemRQ: TIntegerField;
    quDInvItemRW: TFloatField;
    quDInvItemSQ: TFIBStringField;
    quDInvItemSELID: TIntegerField;
    taSellItemCODE: TFIBStringField;
    taSellItemINSID: TFIBStringField;
    taSellItemGOODID: TFIBStringField;
    taSellItemMATID: TFIBStringField;
    quDInvItemCOST: TFloatField;
    quUidWhSumGr: TpFIBDataSet;
    dsUidWhSumGr: TDataSource;
    quUidWhSumGrSNAME: TFIBStringField;
    quUidWhSumGrCOLOR: TIntegerField;
    quUidWhSumGrC: TIntegerField;
    quUidWhSumGrSAC: TIntegerField;
    quUidWhSumGrSTW: TFloatField;
    quUidWhSumGrSTP: TFloatField;
    quUidWhSumGrSTP2: TFloatField;
    quUidWhSumGrGR_SAC: TFIBStringField;
    quUidWhSumGrGR_SAW: TFIBStringField;
    quUidWhSumGrGR_SAP: TFIBStringField;
    quUidWhSumGrGR_SAP2: TFIBStringField;
    taSellItemRETCLIENT: TFIBStringField;
    taSellListCOST0: TFloatField;
    taSellListRCOST0: TFloatField;
    taOptListQ: TIntegerField;
    taOptListW: TFloatField;
    taSellListSCOST: TFloatField;
    taSellListRSCOST: TFloatField;
    taPrOrdUID: TIntegerField;
    taSellItemSQ_DART: TFIBStringField;
    taSellItemSW_DART: TFIBStringField;
    taSellItemRETDATE: TDateTimeField;
    taSellItemRETADDRESS: TFIBStringField;
    taDListQUID: TFIBIntegerField;
    taDListWUID: TFIBFloatField;
    taDListInCost1: TFIBFloatField;
    taDListOutCost1: TFIBFloatField;
    taDListInQuid: TFIBIntegerField;
    taDListOutQuid: TFIBIntegerField;
    taDListInSn: TFIBIntegerField;
    taDListOutSn: TFIBIntegerField;
    taDListInSCost: TFIBFloatField;
    taDListOutSCost: TFIBFloatField;
    taDListInWUid: TFIBFloatField;
    taDListOutWuid: TFIBFloatField;
    quD_WHD_ARTID: TFIBIntegerField;
    quD_WHART2ID: TFIBIntegerField;
    quD_WHFULLART: TFIBStringField;
    quD_WHART2: TFIBStringField;
    quD_WHQUANTITY: TFIBIntegerField;
    quD_WHWEIGHT: TFIBFloatField;
    quD_WHPRICE2: TFIBFloatField;
    quD_WHUNITID: TFIBIntegerField;
    quD_WHOPTPRICE: TFIBFloatField;
    quD_WHPRICE: TFIBFloatField;
    quD_WHART: TFIBStringField;
    quD_WHD_COMPID: TFIBIntegerField;
    quD_WHD_MATID: TFIBStringField;
    quD_WHD_GOODID: TFIBStringField;
    quD_WHD_INSID: TFIBStringField;
    quD_WHD_COUNTRYID: TFIBStringField;
    quD_WHPRODCODE: TFIBStringField;
    quD_WHPEQ: TFIBSmallIntField;
    quD_WHDQ: TFIBIntegerField;
    quD_WHDW: TFIBFloatField;
    quD_WHDEPID: TFIBIntegerField;
    quD_WHOPEQ: TFIBSmallIntField;
    quD_WHRESTQ: TFIBStringField;
    quD_WHRESTW: TFIBStringField;
    quD_WHS_INS: TFIBStringField;
    quArtATT1: TFIBIntegerField;
    quArtATT2: TFIBIntegerField;
    quD_WHATT1: TFIBIntegerField;
    quD_WHATT2: TFIBIntegerField;
    quDPriceDEPID: TFIBIntegerField;
    quDPriceATT1: TFIBIntegerField;
    quDPriceATT2: TFIBIntegerField;
    quD_WHFLAG: TFIBIntegerField;
    quD_WHRESTA: TFIBStringField;
    taSListSnOpen: TFIBIntegerField;
    taSListSnClose: TFIBIntegerField;
    taSListTotalCostOpen: TFloatField;
    taSListTotalCostClose: TFloatField;
    taSListCostOpen: TFIBFloatField;
    taSListCostClose: TFIBFloatField;
    taSListTrOpen: TFIBFloatField;
    taSListTrClose: TFIBFloatField;
    taSListTqOpen: TFIBSmallIntField;
    taSListTQClose: TFIBSmallIntField;
    taSListTWOpen: TFIBFloatField;
    taSListTWClose: TFIBFloatField;
    quUIDWHATT1: TFIBIntegerField;
    quUIDWHATT2: TFIBIntegerField;
    quUIDWHNAMEATT1: TFIBStringField;
    quUIDWHNAMEATT2: TFIBStringField;
    taSListNDSOpen: TFIBFloatField;
    taSListNDSClose: TFIBFloatField;
    taSListTrNDSOpen: TFIBFloatField;
    taSListTrNDSClose: TFIBFloatField;
    taSListEDITTR: TFIBSmallIntField;
    quUIDWHNAMEGOOD: TFIBStringField;
    taORetListQ: TFIBIntegerField;
    taORetListW: TFIBFloatField;
    taORetListCostO: TFIBFloatField;
    taORetListCostC: TFIBFloatField;
    taORetListQO: TFIBIntegerField;
    taORetListQC: TFIBIntegerField;
    taORetListWO: TFIBFloatField;
    taORetListWC: TFIBFloatField;
    taORetListSNO: TFIBIntegerField;
    taORetListSNC: TFIBIntegerField;
    quSupR: TpFIBDataSet;
    quSupRD_COMPID: TFIBIntegerField;
    quSupRNAME: TFIBStringField;
    quSupRSNAME: TFIBStringField;
    quSupRCODE: TFIBStringField;
    quSupRPAYTYPEID: TFIBIntegerField;
    quSupRNDSID: TFIBIntegerField;
    quSupRFNAME: TStringField;
    dsSupR: TDataSource;
    quUIDWHAPPL_Q: TFIBIntegerField;
    taSellItemRETCAUSE: TFIBStringField;
    quA2DictD_INSID: TFIBStringField;
    quArtVALATT1: TFIBStringField;
    quArtVALATT2: TFIBStringField;
    quUIDWHUSERID_WH: TFIBIntegerField;
    quUIDWHAPPLDEP_Q: TFIBIntegerField;
    taA2InsMAIN: TFIBSmallIntField;
    quMaxUIDWH: TpFIBDataSet;
    quMaxUIDWHSITEMID: TFIBIntegerField;
    quMaxUIDWHUID: TFIBIntegerField;
    quMaxUIDWHUNITID: TFIBIntegerField;
    quMaxUIDWHNDSID: TFIBIntegerField;
    quMaxUIDWHPRICE: TFIBFloatField;
    quMaxUIDWHW: TFIBFloatField;
    quMaxUIDWHSZ: TFIBStringField;
    quMaxUIDWHCOST: TFIBFloatField;
    quMaxUIDWHART2ID: TFIBIntegerField;
    quMaxUIDWHDEPID: TFIBIntegerField;
    quMaxUIDWHSN0: TFIBIntegerField;
    quMaxUIDWHSSF0: TFIBStringField;
    quMaxUIDWHSPRICE0: TFIBFloatField;
    quMaxUIDWHSUPID0: TFIBIntegerField;
    quMaxUIDWHSDATE0: TFIBDateTimeField;
    quMaxUIDWHSN: TFIBIntegerField;
    quMaxUIDWHSDATE: TFIBDateTimeField;
    quMaxUIDWHSELLDATE: TFIBDateTimeField;
    quMaxUIDWHT: TFIBSmallIntField;
    quMaxUIDWHISCLOSED: TFIBSmallIntField;
    quMaxUIDWHNDATE: TFIBDateTimeField;
    quMaxUIDWHITYPE: TFIBSmallIntField;
    quMaxUIDWHPRODID: TFIBIntegerField;
    quMaxUIDWHD_ARTID: TFIBIntegerField;
    quMaxUIDWHRETDATE: TFIBDateTimeField;
    quMaxUIDWHCOMPID: TFIBIntegerField;
    quMaxUIDWHMATID: TFIBStringField;
    quMaxUIDWHGOODID: TFIBStringField;
    quMaxUIDWHINSID: TFIBStringField;
    quMaxUIDWHART: TFIBStringField;
    quMaxUIDWHART2: TFIBStringField;
    quMaxUIDWHS_INS: TFIBStringField;
    quMaxUIDWHPRODCODE: TFIBStringField;
    quMaxUIDWHSUPCODE: TFIBStringField;
    quMaxUIDWHSUPNAME: TFIBStringField;
    quMaxUIDWHDEP: TFIBStringField;
    quMaxUIDWHCOLOR: TFIBIntegerField;
    quMaxUIDWHCOST1: TFIBFloatField;
    quMaxUIDWHD_COUNTRYID: TFIBStringField;
    quMaxUIDWHUSERID: TFIBIntegerField;
    quMaxUIDWHUIDWHID: TFIBIntegerField;
    quMaxUIDWHNAMEDEPFROM: TFIBStringField;
    quMaxUIDWHCOLORDF: TFIBIntegerField;
    quMaxUIDWHGOODS1: TFIBStringField;
    quMaxUIDWHGOODS2: TFIBStringField;
    quMaxUIDWHGOODSID1: TFIBIntegerField;
    quMaxUIDWHGOODSID2: TFIBIntegerField;
    quMaxUIDWHATT1: TFIBIntegerField;
    quMaxUIDWHATT2: TFIBIntegerField;
    quMaxUIDWHNAMEATT1: TFIBStringField;
    quMaxUIDWHNAMEATT2: TFIBStringField;
    quMaxUIDWHNAMEGOOD: TFIBStringField;
    quMaxUIDWHAPPL_Q: TFIBIntegerField;
    quMaxUIDWHUSERID_WH: TFIBIntegerField;
    quMaxUIDWHAPPLDEP_Q: TFIBIntegerField;
    quMaxUIDWHmemo: TStringField;
    quMaxUIDWHNDSNAME: TStringField;
    quMaxUIDWHPRORDNAME: TStringField;
    quADictATT1: TFIBIntegerField;
    quADictATT2: TFIBIntegerField;
    quUIDWHAPPLDEP_USER: TFIBIntegerField;
    quMaxUIDWHAPPLDEP_USER: TFIBIntegerField;
    taSellItemCASHIERID: TFIBIntegerField;
    taSellItemCASHIERNAME: TFIBStringField;
    taSellListOPENEMP: TFIBStringField;
    taSellItemARTID: TFIBIntegerField;
    quTempUIDWHsname: TStringField;
    quTempUIDWHCOLOR: TStringField;
    taSellItemAPPL_Q: TFIBIntegerField;
    taSListITYPE: TFIBIntegerField;
    taSListCLTID: TFIBIntegerField;
    taSRetListSINVID: TFIBIntegerField;
    taSRetListDEPFROMID: TFIBIntegerField;
    taSRetListSDATE: TFIBDateTimeField;
    taSRetListSN: TFIBIntegerField;
    taSRetListPMARGIN: TFIBFloatField;
    taSRetListTR: TFIBFloatField;
    taSRetListCOST: TFIBFloatField;
    taSRetListR_COUNT: TFIBIntegerField;
    taSRetListAKCIZ: TFIBFloatField;
    taSRetListISCLOSED: TFIBSmallIntField;
    taSRetListDEPFROM: TFIBStringField;
    taSRetListCOMPID: TFIBIntegerField;
    taSRetListCOMP: TFIBStringField;
    taSRetListUSERID: TFIBIntegerField;
    taSRetListFIO: TFIBStringField;
    taSRetListDEF_CDM: TFIBSmallIntField;
    taSRetListW: TFIBFloatField;
    quCountAppl: TpFIBDataSet;
    dsCountAppl: TDataSource;
    taSellItemSQAPPL: TFIBStringField;
    quCountApplSQ: TFIBStringField;
    quCountApplStSQ: TStringField;
    quUIDWHPRICEINV: TFIBFloatField;
    quUIDWHCOSTINV: TFIBFloatField;
    quMaxUIDWHPRICEINV: TFIBFloatField;
    quMaxUIDWHCOSTINV: TFIBFloatField;
    quTempUIDWHSTP3: TFIBFloatField;
    taSellItemACTDISCOUNT: TFIBFloatField;
    taSellListACTDISCOUNT: TFIBFloatField;
    taSellItemCARD: TFIBSmallIntField;
    taSRetListPTR: TFIBFloatField;
    taSRetListPTRNDS: TFIBFloatField;
    taSRetListTRNDS: TFIBFloatField;
    quPrSINEWCOST: TFIBFloatField;
    quPrSIOLDCOST: TFIBFloatField;
    taSellListACTDISCOUNTRET: TFIBFloatField;
    taSRetListRET_NONDS: TFIBSmallIntField;
    quCountApplSQ1: TFIBStringField;
    quCountApplStSQ1: TStringField;
    taDListCLTID: TFIBIntegerField;
    taSRetListCountClose: TIntegerField;
    taSRetListCountOpen: TIntegerField;
    taSRetListCostClose: TFIBFloatField;
    taSRetListCostOpen: TFIBFloatField;
    taSRetListQClose: TIntegerField;
    taSRetListQOpen: TIntegerField;
    taSRetListWClose: TFIBFloatField;
    taSRetListWOpen: TFIBFloatField;
    taCardList: TpFIBDataSet;
    dsrCardList: TDataSource;
    quD_UIDFREPAIR: TFIBSmallIntField;
    taSellItemCOST: TFIBFloatField;
    taSellItemCOST0: TFIBFloatField;
    taSellItemDIFQ: TFIBIntegerField;
    taSellItemCOSTCARD: TFIBFloatField;
    taSellListCOSTCARD: TFIBFloatField;
    taCurSellCOSTCARD: TFIBFloatField;
    quCurSellCheckCOSTCARD: TFIBFloatField;
    taCardListCHECKNO: TFIBIntegerField;
    taCardListRN: TFIBIntegerField;
    taCardListSNAME: TFIBStringField;
    taCardListCOST: TFIBFloatField;
    taCardListCOST0: TFIBFloatField;
    taCardListCOSTCARD: TFIBFloatField;
    taCardListSELLID: TFIBIntegerField;
    frDList: TfrDBDataSet;
    frDinvitem: TfrDBDataSet;
    taSRetListGruzootprav_ID: TIntegerField;
    taSRetListProizvoditel_ID: TIntegerField;
    taSRetListGruzootprav: TStringField;
    taSRetListProizvoditel: TStringField;
    trCommission: TpFIBTransaction;
    spSetCommission: TpFIBStoredProc;
    quUIDWHProducerID: TIntegerField;
    taSListColor: TFIBIntegerField;
    taSListcomissionscheme: TFIBIntegerField;
    taDListInCost0: TFIBFloatField;
    taDListOutCost0: TFIBFloatField;
    quUIDWHDepartmentName: TStringField;
    taSListCURR_COLOR: TFIBIntegerField;
    taSElCOLOR: TFIBIntegerField;
    quDInvItemUNIT: TFIBStringField;
    taPrOrdItemCLOSEDATE: TFIBDateTimeField;
    quDInvItemrecNo: TIntegerField;
    dsUnitId: TDataSource;
    quUnitId: TpFIBDataSet;
    quUnitIdUNITID: TFIBIntegerField;
    quUnitIdUNIT: TFIBStringField;
    pmAnlzSelDed: TPopupMenu;
    MenuItem2: TMenuItem;
    taDepMarginSNAME: TFIBStringField;
    taSellItemTRANSACT_ID: TFIBIntegerField;
    taSellListSERTSELL: TFIBFloatField;
    taSellListCSERT: TFIBFloatField;
    taSellListSERT_ADD: TFIBFloatField;
    taSellListTOTAL: TFIBFloatField;
    taSellListreal_vych: TCurrencyField;
    taSellListreal_razn: TCurrencyField;
    taSellListRCSERT: TFIBIntegerField;
    taSellListRETURN_COST: TFIBFloatField;
    taAnlzSell_days: TpFIBDataSet;
    dsrAnzSell_days: TDataSource;
    taAnlzSell_daysD_DAY: TFIBDateField;
    taAnlzSell_daysW_D: TFIBStringField;
    taAnlzSell_daysH10_CL: TFIBIntegerField;
    taAnlzSell_daysH11_CL: TFIBIntegerField;
    taAnlzSell_daysH12_CL: TFIBIntegerField;
    taAnlzSell_daysH13_CL: TFIBIntegerField;
    taAnlzSell_daysH14_CL: TFIBIntegerField;
    taAnlzSell_daysH15_CL: TFIBIntegerField;
    taAnlzSell_daysH16_CL: TFIBIntegerField;
    taAnlzSell_daysH17_CL: TFIBIntegerField;
    taAnlzSell_daysH18_CL: TFIBIntegerField;
    taAnlzSell_daysH19_CL: TFIBIntegerField;
    taAnlzSell_daysH20_CL: TFIBIntegerField;
    taAnlzSell_daysH21_CL: TFIBIntegerField;
    taAnlzSell_daysH10_S: TFIBIntegerField;
    taAnlzSell_daysH11_S: TFIBIntegerField;
    taAnlzSell_daysH12_S: TFIBIntegerField;
    taAnlzSell_daysH13_S: TFIBIntegerField;
    taAnlzSell_daysH14_S: TFIBIntegerField;
    taAnlzSell_daysH15_S: TFIBIntegerField;
    taAnlzSell_daysH16_S: TFIBIntegerField;
    taAnlzSell_daysH17_S: TFIBIntegerField;
    taAnlzSell_daysH18_S: TFIBIntegerField;
    taAnlzSell_daysH19_S: TFIBIntegerField;
    taAnlzSell_daysH20_S: TFIBIntegerField;
    taAnlzSell_daysH21_S: TFIBIntegerField;
    taAnlzSell_daysITOGO_CL: TFIBIntegerField;
    taAnlzSell_daysITOGO_SELL: TFIBIntegerField;
    quUIDWHNDOG: TFIBStringField;
    dsCassa: TpFIBDataSet;
    dsCassaDEPARTMENTID: TFIBIntegerField;
    dsCassaDEVICE: TFIBBCDField;
    dsCassaSUMMA: TFIBFloatField;
    dsCassaNAME: TFIBStringField;
    dsCassaCHECKSCOUNT: TFIBIntegerField;
    taSListADDNDSTOPRICE: TFIBSmallIntField;
    taSListISTOLLING: TFIBSmallIntField;
    taTollingParams: TpFIBDataSet;
    dsTollingParams: TDataSource;
    taTollingParamsAVERAGEPRICE: TFIBFloatField;
    taTollingParamsTOTALLOSSESWEIGHT: TFIBBCDField;
    taTollingParamsTOTALINSERTIONSWEIGHT: TFIBBCDField;
    taTollingParamsTOTALINVOICEWEIGHT: TFIBBCDField;
    taA2TollingPrice: TCurrencyField;
    taTollingParamsINVOICEID: TFIBIntegerField;
    quUIDWHTollingPrice: TCurrencyField;
    taSElPRICEHACK: TFIBFloatField;
    taSElPRICENDS: TFIBFloatField;
    taA2PRICEHACK: TFIBFloatField;
    taA2PRICENDS: TFIBFloatField;
    quUIDWHFLAG: TIntegerField;
    quUIDWHFLAGNAME: TStringField;
    quSRetUIDISSAFE: TFIBIntegerField;
    taSListCONTRACTCLASSID: TFIBIntegerField;
    taSRetListCONTRACTCLASSID: TFIBIntegerField;
    taSRetListCONTRACTID: TFIBIntegerField;
    taSListCONTRACTID: TFIBIntegerField;
    taSListCONTRACTCLASSNAME: TFIBStringField;
    taTollingParamsPUREK: TFIBFloatField;
    taTollingParamsPURE: TFIBIntegerField;
    taSRetListCONTRACTCLASSNAME: TFIBStringField;
    taSItemWEIGHTINSERTION: TFIBBCDField;
    taSItemCOSTINSERTION: TFIBFloatField;
    taTollingParamsTOTALINSERTIONSCOST: TFIBBCDField;
    taSItemEUID: TFIBStringField;
    quUIDWHEUID: TFIBStringField;
    taTollingParamsINCLUDEINSERTIONS: TFIBIntegerField;
    taSListcontractnumber: TStringField;
    taSListcontractdate: TDateTimeField;
    taSellItemDISCOUNT: TFIBFloatField;
    taSellItemCLIENTCARD: TStringField;
    PmRET: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem19: TMenuItem;
    procedure PriceClick(Sender: TObject);
    procedure PHistClick(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DelConf(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure PostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taSListNewRecord(DataSet: TDataSet);
    procedure taSListCalcFields(DataSet: TDataSet);
    procedure taSListPNDSChange(Sender: TField);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure taSElNewRecord(DataSet: TDataSet);
    procedure taSElBeforeOpen(DataSet: TDataSet);
    procedure taSElBeforeInsert(DataSet: TDataSet);
    procedure taSElCalcFields(DataSet: TDataSet);
    procedure taSItemBeforeOpen(DataSet: TDataSet);
    procedure taSItemCalcFields(DataSet: TDataSet);
    procedure SElAfterPost(DataSet: TDataSet);
    procedure taSListAfterDelete(DataSet: TDataSet);
    procedure taDefDistBeforeOpen(DataSet: TDataSet);
    procedure taSListDEPIDChange(Sender: TField);
    procedure taA2InsBeforeOpen(DataSet: TDataSet);
    procedure taA2InsNewRecord(DataSet: TDataSet);

    procedure taSListBeforePost(DataSet: TDataSet);
    procedure taSElQUANTITYChange(Sender: TField);
    procedure taSElTOTALWEIGHTChange(Sender: TField);
    procedure taSElBeforePost(DataSet: TDataSet);
    procedure WHClick(Sender: TObject);
    procedure quWHBeforeOpen(DataSet: TDataSet);
    procedure quWHArt2BeforeOpen(DataSet: TDataSet);
    procedure quWHUIDBeforeOpen(DataSet: TDataSet);
    procedure quWHA2UIDBeforeOpen(DataSet: TDataSet);
    procedure quWHSZBeforeOpen(DataSet: TDataSet);
    procedure quWHA2SZBeforeOpen(DataSet: TDataSet);
    procedure DListToClick(Sender: TObject);
    procedure DListFromClick(Sender: TObject);
    procedure taDListAfterDelete(DataSet: TDataSet);
    procedure taDListBeforePost(DataSet: TDataSet);
    procedure taDListNewRecord(DataSet: TDataSet);
    procedure taDListBeforeOpen(DataSet: TDataSet);
    procedure taDListDEPFROMIDChange(Sender: TField);
    procedure taDListDEPIDChange(Sender: TField);
    procedure quD_WHBeforeOpen(DataSet: TDataSet);
    procedure taDItemNewRecord(DataSet: TDataSet);
    procedure taDItemBeforeOpen(DataSet: TDataSet);
    procedure quD_UIDBeforeOpen(DataSet: TDataSet);
    procedure quD_SZBeforeOpen(DataSet: TDataSet);
    procedure taSItemNewRecord(DataSet: TDataSet);
    procedure PrOrdClick(Sender: TObject);
    procedure AddtoPrClick(Sender: TObject);
    procedure taPrOrdNewRecord(DataSet: TDataSet);
    procedure taPrOrdItemBeforeOpen(DataSet: TDataSet);
    procedure taPrOrdItemBeforeInsert(DataSet: TDataSet);
    procedure taPrOrdItemNewRecord(DataSet: TDataSet);
    procedure taSItemAfterDelete(DataSet: TDataSet);
    procedure taDepMarginROUNDNORMGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taDepMarginROUNDNORMSetText(Sender: TField; const Text: String);
    procedure BeforeDelete(DataSet: TDataSet);
    procedure taSetPriceAfterPost(DataSet: TDataSet);
    procedure SellClick(Sender: TObject);
    procedure RETClick(Sender: TObject);
    procedure taSellListBeforeOpen(DataSet: TDataSet);
    procedure SellItemClick(Sender: TObject);
    procedure taSellItemBeforeOpen(DataSet: TDataSet);
    procedure taSellItemNewRecord(DataSet: TDataSet);
    procedure taSellItemBeforeInsert(DataSet: TDataSet);
    procedure taA2BeforeOpen(DataSet: TDataSet);
    procedure taA2BeforeInsert(DataSet: TDataSet);
    procedure taA2NewRecord(DataSet: TDataSet);
    procedure taA2BeforeEdit(DataSet: TDataSet);
    procedure taA2PRICE1Change(Sender: TField);
    procedure taA2AfterPost(DataSet: TDataSet);
    procedure quPriceBeforeOpen(DataSet: TDataSet);
    procedure quArtNewRecord(DataSet: TDataSet);
    procedure quArtBeforePost(DataSet: TDataSet);
    procedure quArtBeforeInsert(DataSet: TDataSet);
    procedure quPriceBeforePost(DataSet: TDataSet);
    procedure quDPriceBeforeOpen(DataSet: TDataSet);
    procedure taA2USEMARGINChange(Sender: TField);
    procedure taSElUSEMARGINChange(Sender: TField);
    procedure quArtBeforeOpen(DataSet: TDataSet);
    procedure quARestBeforeOpen(DataSet: TDataSet);
    procedure quA2RestBeforeOpen(DataSet: TDataSet);
    procedure quEmpBeforeOpen(DataSet: TDataSet);
    procedure taSellItemCalcFields(DataSet: TDataSet);
    procedure taCurSellBeforeOpen(DataSet: TDataSet);
    procedure taCurSellNewRecord(DataSet: TDataSet);
    procedure taA2BeforePost(DataSet: TDataSet);
    procedure quDistrPriceBeforeOpen(DataSet: TDataSet);
    procedure quDistrPriceBeforePost(DataSet: TDataSet);
    procedure quDistrPriceBeforeEdit(DataSet: TDataSet);
    procedure UNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure UNITIDSetText(Sender: TField; const Text: String);
    procedure YesNoGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure YesNoSetText(Sender: TField; const Text: String);
    procedure taSetPriceBeforePost(DataSet: TDataSet);
    procedure taPrOrdBeforeDelete(DataSet: TDataSet);
    procedure taPrOrdBeforeEdit(DataSet: TDataSet);
    procedure taPrOrdItemBeforeEdit(DataSet: TDataSet);
    procedure taPrOrdItemBeforeDelete(DataSet: TDataSet);
    procedure taPrOrdItemBeforePost(DataSet: TDataSet);
    procedure OptDepClick(Sender: TObject);
    procedure taOptListNewRecord(DataSet: TDataSet);
    procedure taOptListBeforeOpen(DataSet: TDataSet);
    procedure taDItemBeforePost(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taPrOrdCalcFields(DataSet: TDataSet);
    procedure taSItemBeforePost(DataSet: TDataSet);
    procedure taSellItemDISCOUNTChange(Sender: TField);
    procedure taSellItemPRICEChange(Sender: TField);
    procedure quDstBeforeOpen(DataSet: TDataSet);
    procedure quDistredBeforeOpen(DataSet: TDataSet);
    procedure quD_QBeforeOpen(DataSet: TDataSet);
    procedure quD_UIDAfterOpen(DataSet: TDataSet);
    procedure taSListBeforeEdit(DataSet: TDataSet);
    procedure taSElBeforeEdit(DataSet: TDataSet);
    procedure taSItemBeforeDelete(DataSet: TDataSet);
    procedure taSItemBeforeEdit(DataSet: TDataSet);
    procedure taSItemBeforeInsert(DataSet: TDataSet);
    procedure taDListBeforeEdit(DataSet: TDataSet);
    procedure taDItemBeforeEdit(DataSet: TDataSet);
    procedure taDItemBeforeInsert(DataSet: TDataSet);
    procedure taDItemBeforeDelete(DataSet: TDataSet);
    procedure quD_UIDFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure quWHA2UIDFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure quSupCalcFields(DataSet: TDataSet);
    procedure quUIDWHAfterOpen(DataSet: TDataSet);
    procedure quUIDWHBeforeOpen(DataSet: TDataSet);
    procedure taSellItemAfterPost(DataSet: TDataSet);
    procedure taSellListNewRecord(DataSet: TDataSet);
    procedure taSellListBeforeInsert(DataSet: TDataSet);
    procedure quPriceFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taDListAfterOpen(DataSet: TDataSet);
    procedure taSellListCalcFields(DataSet: TDataSet);
    procedure taCurSellCalcFields(DataSet: TDataSet);
    procedure taSellItemBeforeEdit(DataSet: TDataSet);
    procedure taSellItemBeforeDelete(DataSet: TDataSet);
    procedure taPrOrdItemCalcFields(DataSet: TDataSet);
    procedure quPrSIBeforeOpen(DataSet: TDataSet);
    procedure taSRetListNewRecord(DataSet: TDataSet);
    procedure taOptListBeforeInsert(DataSet: TDataSet);
    procedure taSRetBeforeInsert(DataSet: TDataSet);
    procedure taSRetBeforeOpen(DataSet: TDataSet);
    procedure taSRetNewRecord(DataSet: TDataSet);
    procedure taSRetCalcFields(DataSet: TDataSet);
    procedure taSRetAfterDelete(DataSet: TDataSet);
    procedure taORetListBeforeOpen(DataSet: TDataSet);
    procedure taORetListNewRecord(DataSet: TDataSet);
    procedure ORetClick(Sender: TObject);
    procedure quOSelledBeforeOpen(DataSet: TDataSet);
    procedure taORetAfterDelete(DataSet: TDataSet);
    procedure taORetBeforeOpen(DataSet: TDataSet);
    procedure taORetNewRecord(DataSet: TDataSet);
    procedure taORetCalcFields(DataSet: TDataSet);
    procedure taSellItemBeforePost(DataSet: TDataSet);
    procedure quUIDWHCalcFields(DataSet: TDataSet);
    procedure quSelledBeforeOpen(DataSet: TDataSet);
    procedure quArtAfterPost(DataSet: TDataSet);
    procedure quArtAfterInsert(DataSet: TDataSet);
    procedure quArtAfterCancel(DataSet: TDataSet);
    procedure quSListTCalcFields(DataSet: TDataSet);
    procedure quSRetUIDBeforeOpen(DataSet: TDataSet);
    procedure taSItemAfterInsert(DataSet: TDataSet);
    procedure quMaxArtPriceBeforeOpen(DataSet: TDataSet);
    procedure ReplClick(Sender: TObject);
    procedure taSellListBeforePost(DataSet: TDataSet);
    procedure taA2InsBeforePost(DataSet: TDataSet);
    procedure quPrOrdCalcFields(DataSet: TDataSet);
    procedure quPrOrdBeforeOpen(DataSet: TDataSet);
    procedure taCurSellBeforePost(DataSet: TDataSet);
    procedure quADictBeforeOpen(DataSet: TDataSet);
    procedure quA2DictBeforeInsert(DataSet: TDataSet);
    procedure quA2DictBeforePost(DataSet: TDataSet);
    procedure quA2DictNewRecord(DataSet: TDataSet);
    procedure quADictBeforeInsert(DataSet: TDataSet);
    procedure quADictBeforePost(DataSet: TDataSet);
    procedure quADictNewRecord(DataSet: TDataSet);
    procedure quUIDWHBeforePost(DataSet: TDataSet);
    procedure quA2DictAfterPost(DataSet: TDataSet);
    procedure quD_WHCalcFields(DataSet: TDataSet);
    procedure quMaxArtPriceAfterOpen(DataSet: TDataSet);
    procedure quUIDWHFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure quUIDWHAfterPost(DataSet: TDataSet);
    procedure quD_WHAfterOpen(DataSet: TDataSet);
    procedure quUIDSelledBeforeOpen(DataSet: TDataSet);
    procedure quArtAfterScroll(DataSet: TDataSet);
    procedure quUIDWHSELLDATEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure quTempUIDWHBeforeOpen(DataSet: TDataSet);
    procedure RSTATEGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure udpInvDataReceived(Sender: TComponent; NumberBytes: Integer;
      FromIP: String; Port: Integer);
    procedure dsArtDataChange(Sender: TObject; Field: TField);
    procedure quArtAfterOpen(DataSet: TDataSet);
    procedure taSRetListBeforeEdit(DataSet: TDataSet);
    procedure taSellListBeforeDelete(DataSet: TDataSet);
    procedure taSellListAfterDelete(DataSet: TDataSet);
    procedure taSellItemAfterDelete(DataSet: TDataSet);
    procedure quInOutPriceBeforeOpen(DataSet: TDataSet);
    procedure quInOutPriceAfterOpen(DataSet: TDataSet);
    procedure taSRetBeforeEdit(DataSet: TDataSet);
    procedure quCurSellCheckBeforeOpen(DataSet: TDataSet);
    procedure quCurSellCheckCalcFields(DataSet: TDataSet);
    procedure quDInvItemBeforeOpen(DataSet: TDataSet);
    procedure quDInvItemCalcFields(DataSet: TDataSet);
    procedure quDInvItemBeforeDelete(DataSet: TDataSet);
    procedure quDInvItemAfterDelete(DataSet: TDataSet);
    procedure quDInvItemAfterPost(DataSet: TDataSet);
    procedure quDInvItemBeforeEdit(DataSet: TDataSet);
    procedure quUidWhSumGrBeforeOpen(DataSet: TDataSet);
    procedure quUidWhSumGrCalcFields(DataSet: TDataSet);
    procedure taSellListBeforeEdit(DataSet: TDataSet);
    procedure taDListCalcFields(DataSet: TDataSet);
    procedure quD_WHAfterRefresh(DataSet: TDataSet);
    procedure taORetListCalcFields(DataSet: TDataSet);
    procedure quSupRCalcFields(DataSet: TDataSet);
    procedure taA2AfterRefresh(DataSet: TDataSet);
    procedure taSRetListBeforeOpen(DataSet: TDataSet);
    procedure quADictAfterPost(DataSet: TDataSet);
    procedure taA2InsBeforeDelete(DataSet: TDataSet);
    procedure taA2InsAfterRefresh(DataSet: TDataSet);
    procedure taA2InsAfterPost(DataSet: TDataSet);
    procedure quMaxUIDWHBeforeOpen(DataSet: TDataSet);
    procedure quMaxUIDWHCalcFields(DataSet: TDataSet);
    procedure quMaxUIDWHAfterPost(DataSet: TDataSet);
    procedure quMaxUIDWHAfterOpen(DataSet: TDataSet);
    procedure quMaxUIDWHBeforePost(DataSet: TDataSet);
    procedure quUIDWHAPPLDEP_QChange(Sender: TField);
    procedure quUIDWHBeforeClose(DataSet: TDataSet);
    procedure quMaxUIDWHBeforeClose(DataSet: TDataSet);
    procedure taOptListBeforeEdit(DataSet: TDataSet);
    procedure quCountApplBeforeOpen(DataSet: TDataSet);
    procedure quCountApplCalcFields(DataSet: TDataSet);
    procedure quUIDWHAfterScroll(DataSet: TDataSet);
    procedure quUIDWHAPPL_QChange(Sender: TField);
    procedure taSRetListCalcFields(DataSet: TDataSet);
    procedure quInOutPriceAfterRefresh(DataSet: TDataSet);
    procedure taA2AfterCancel(DataSet: TDataSet);
    procedure taOptListBeforePost(DataSet: TDataSet);
    procedure taSRetListBeforePost(DataSet: TDataSet);
    procedure taPrOrdBeforeOpen(DataSet: TDataSet);
    procedure quDstBeforePost(DataSet: TDataSet);
    procedure quDstBeforeDelete(DataSet: TDataSet);
    procedure taSRetBeforePost(DataSet: TDataSet);
    procedure quDInvItemBeforePost(DataSet: TDataSet);
    procedure taSListTWChange(Sender: TField);
    procedure AnlzSelDepClick(Sender: TObject);
    procedure taPrOrdDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taSRetAfterOpen(DataSet: TDataSet);
    procedure taSRetAfterPost(DataSet: TDataSet);
    procedure taAnlzSell_daysBeforeOpen(DataSet: TDataSet);
    procedure dsCassaBeforeOpen(DataSet: TDataSet);
    procedure taA2CalcFields(DataSet: TDataSet);
    procedure taSListADDNDSTOPRICEChange(Sender: TField);
    procedure taTollingParamsBeforeOpen(DataSet: TDataSet);
    procedure taTollingParamsBeforePost(DataSet: TDataSet);
    procedure taTollingParamsBeforeClose(DataSet: TDataSet);
    procedure taA2PRICEHACKChange(Sender: TField);
    procedure taSElPRICEHACKChange(Sender: TField);
    procedure taSElBeforeClose(DataSet: TDataSet);

    {  procedure taSRetListAfterEdit(DataSet: TDataSet);
    procedure taDListAfterEdit(DataSet: TDataSet);
    procedure taSListAfterEdit(DataSet: TDataSet);
}  private
    { Private declarations }
    ErrSick:boolean;
    SellItemSZ:string;
    ArtMode:integer;
    SELid:integer;
    SITEMid:integer;
    procedure CheckSellClosed;
    procedure SetAFilter2(Params: TFIBXSQLDA);
  public
    FilterArt2:string; //���������� � ������ �� �������� �� ������� ��������
    A2Err:boolean;
    UidwhSZ:string;
    SellitemCard:integer;
    SItype:integer;
    IsChangeApplDep:boolean;
    IsChangeAppl:boolean;
    IsClient : integer;
    IsTolling : Integer;
    SetSocketClose:boolean;
    D_CHECK0 : integer;
    DstDepId: integer;
    DstArt2Id: integer;
    SDepId: integer;
    SDep: string;
    DDepFromId: integer;
    DDepFrom: string;
    WHSZMode: integer;
    WorkMode: string;
    Distr: boolean;
    //bmpAdd, bmpDel, bmpZoomIn: TBitmap;
    SellId: integer;
    Sells: boolean;
    DPriceFilter: boolean;
    DPriceP1Filter: boolean;
    DPriceP2Filter: boolean;
    DPriceP1: extended;
    DPriceP2: extended;
    D_ARTID: integer;
    ART2ID: integer;
    DPriceDepId: integer;
    EmpDepId: integer;
    UseFReg: boolean;
    WChar: char;
    PriceFilter: boolean;
    SupFilter: boolean;
    SZFilter: boolean;
    PriceFilterValue: extended;
    SupFilterId: integer;
    SZFilterValue: string[10];
    UseMargin: boolean;
    SellMode: boolean;
    DistrW: integer;
    DistrT: integer;

    slProd, slMat, slGood, slIns, slSup, slCOUNTRY, slGrMat, slNote1, slNote2, slAtt1, slAtt2, slDep, slSZ: TStringList;
    SellRet: boolean;

    UIDWHTime: TDateTime;
    BeginDate: TDateTime;
    EndDate: TDateTime;
    calc_beginDate: TDateTime; // ������ ������� ������� ��� ������������ ������ 
                               // �������� � ������ ���

    Old_D_CompId: integer;
    Old_D_MatId: string[40];
    Old_D_GoodId: string[40];
    Old_D_InsId: string[40];
    Old_D_CountryId: string[40];
    Old_D_Att1Id: integer;
    Old_D_Att2Id: integer;

    lMultiSelect,lSellSelect,AllUIDWH, lSRetSelect, lOSinvSelect, lClRetSelect:boolean;
    SD_CompId: string;
    SD_SupId: string;
    SD_MatId: string;
    SD_GrMatId: string;
    SD_GoodId: string;
    SD_Note1: string;
    SD_Note2: string;
    SD_Att1: string;
    SD_Att2: string;
    SD_InsID: string;

    {��� �������������� ������ � ��������������}
    lMultiSelect_I:boolean;
    SD_CompId_I, SD_SupId_I, SD_MatId_I, SD_GrMatId_I, SD_GoodId_I,
    SD_Note1_I, SD_Note2_I, SD_Att1_I, SD_Att2_I: string;

    sWorkInvID:string;

    NewUnitId: integer;

    NewArt: string;
    FindArt:string;
    ReopenUidWH:boolean;

    OBeginDate, OEndDate: TDateTime;

    OptBuyerId: variant;

    RetDate1, RetDate2: TDateTime;
    RetClientId: variant;

    PActAfterSInv: boolean;
    PActAfterDInv: boolean;
    PActAfterSell: boolean;
    CU_SBD0, CU_SBD1, CU_R, CU_S0, CU_S1, CU_D0, CU_D1,
    CU_RT0, CU_RT1, CU_SL, CU_SO0, CU_SO1, CU_SR0, CU_SR1,
    CU_RO0, CU_RO1, CU_IM0, CU_IM1, CU_AO, CU_AC, CU_SI, CU_RO, CU_RC: TColor;
    TmpDir, TmpDB: string;
    MaxCountWH :integer;
    MaxCountUIDWH :integer;

    AllA2: boolean;

    DstOne2Many: boolean;

    PrHistArt2Id: integer;
    PrHistDepId: integer;

    UID2Find: integer;

    A2Inserted: boolean;

    PTrNds: extended;

    R_Item: string;

    PriceChangedFilter: boolean;
    PriceDifFilter: boolean;

    ClosedInvId: integer;

    PSQL1: string;
    PSQL2: string;

    PrOrdId: integer;
    InsertedArtId: integer;
    UIDWHBD, UIDWHED: TDateTime;
    UID: integer;

    DstD_ArtId: integer;

    UIDWHSupId: integer;
    UIDWHSupFlt: boolean;

    DstEl: boolean;
    D_WHArt: string;

    NotCalcUID: boolean;

    Art: string;
    InsertedArt: string;

    UIDWHType: integer;

    ArtId: integer;

    NewClientId: integer;

    RBD, RED: TDateTime;
    SupCaseId:integer;
    SellElUser: integer;
    IsSellEmpty: boolean;
//    cbDifference: Boolean;

    FilterArtb :boolean;
    //���������� �� ������ �� ���������
    IFilterCost, IFilterW:boolean;
    ICOst1, ICost2, Iw1, Iw2:real;
    Comission: Integer;
    PayType: integer;//������ �� ���� �����
    UIDWHExportMode: Boolean;
    procedure LoadArtSL(Dict: integer);
    function IsInArr(Arr : array of Variant; Value : Variant) : boolean;
    function getCommaBlob(blob : string; index : integer) : string;
    function getPosInArr(Arr : array of Variant; Value : Variant) : integer;

    procedure ReopenArtDict;
    procedure CloseFormByEsc(Sender: TObject; var Key: Char);

    procedure SetBeginDate; //��������� �������������� ���.

    procedure FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, lbAtt1, lbAtt2: TListBox);
    procedure FillComboBoxes(cmbxComp, cmbxMat,cmbxGood, cmbxIns, cmbxCountry, cmbxAtt1, cmbxAtt2: TDBComboboxEh);
    procedure ListBoxKeyPress(Sender: TObject; var Key: Char);
    procedure ReOpenSZQ(q: TpFIBDataSet; sz: string);
    procedure RefreshAllSEl(Art2Id: integer);
    function  GetMargin(DepId: integer; var Margin: extended; var RoundNorm: integer; InvId: integer): boolean;
    procedure OpenSell;
    procedure CloseSell;
    procedure CheckClosedPr;
    procedure SetAFilter(Params: TFIBXSQLDA);
    procedure SetDepFilter(Params: TFIBXSQLDA; DepId: integer);
    procedure ClickUserDepMenu(pm: TPopupMenu);
    procedure SetCloseInvBtn(si: TSpeedItem; IsClosed: integer);
    procedure CreateTmpFile(FileName: string);
    procedure RemoveTmpFile(FileName: string);
    procedure FindLBItem(lb: TListBox; Value: variant);
    procedure CheckDInvN;
    procedure ClearItBuf;
    function  GetArt2ArtId(Art2Id: integer): integer;
    function EmptyStrToZero(const s:string):string;
    procedure SetVisEnabled(WC:TWincontrol;b:boolean);
    function AcceptUIDWH(DataSet:TDataSet):boolean;
    function EmptyInv (invid, itype:integer):boolean;
    function closeinv (invid, itype:integer):boolean;
    function countmaxpross:boolean;
    function checkInv_RUseDep(DepID: integer): boolean; { �������� �� ��, ����� �� ����������� ��������� �� ������
    � �������, ��� �������� ������� ��������� ����}
    function CheckForSpecSymb(instr: string):boolean; {�������� �� ������� ������ ������ ����. ��������}
    function CheckForUIDWHCalc: boolean; {�������� ����� �� �������� ������}
    function CheckForWOrder(SITEMID:Integer): boolean; {��������, ������� �� ������� �� ���. �� ������}
    function pCreatePrord(invid:string):boolean;
    {$IFDEF TRIALPERIOD}
    procedure CheckTrialPeriod(Date: TDateTime);
    {$ENDIF}
  end;

var
  dm: Tdm;
  CenterDep: boolean;
  SelfDepId, CenterDepId: integer;
  SelfDepName:string;
  goods1, goods2:integer;
  p:integer;
function AccFunction: boolean;


type TdcPreview = (pvDesign, pvPreview, pvPrint);
     TarrDoc = array of LongInt;

var DocPreview : TdcPreview;
    NOWH: boolean;
    IsReCalc : boolean; // True - ������ ��� ��� �������� ����� �����, ����������
                        // ���������������, ����� ������ ��� ������ ��������
    SafeLoad: boolean;

    // ��������� ��������� ������
    AppDebug, AppUpdate : boolean;

implementation

uses
  ComData,
  
  SList, WH, DList, DBTree, SItem,
  PrOrdLst, RxStrUtils, Data2, math, SellList, SellItem, OpenSell,
  OptList, OptItem, DataFR, SplashFR,
  ServData, SRetLst, ORetList, RetCltList, SInv,DInv, PHist, InsRepl, Ins, Variants, M207Proc,
  StrUtils, Client, ReportData, ArtDict, AddToPr, dbUtil, Appl, MsgDialog, DInvItem,
  AnlzSelEmp, seldep_anlz, UIDWH, UIDWH2;

{$R *.DFM}

function tdm.countmaxpross:boolean;
var i:integer;
begin
 result:=false;
 with qutmp do
 begin
   close;
   sql.Text:='select calcwhdate from d_rec';
   ExecQuery;
   if Fields[0].IsNull then i:=0 else i:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
   if i>= MaxCountWH then
   begin
     MessageDialog('�������� ����������� ���������� ���-�� ��������.'#13#10+
               '��������� ����� ������ ������!', mtWarning, [mbOk], 0);
     result:=true;
   end;
 end;
end;

function AccFunction: boolean;
begin
  Result:=True;
end;

function RetClt(q : TpFIBDataSet): boolean;
begin
  Result := True;
end;

function Art2Dep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;

function WHDep(q: TpFIBDataSet): boolean;{ TODO : ����� ���������� ������ }
begin
//�������� ��� ������ �� ������������ ����
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function TSumDep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;

function DListToDep(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=(q.FieldByName('D_DEPID').AsInteger=SelfDepId) or (q.FieldByName('CenterDep').AsInteger=1);
end;


function DListFromDep(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=(q.FieldByName('D_DEPID').AsInteger=SelfDepId) or (q.FieldByName('CenterDep').AsInteger=1);
end;

function PrOrdDep(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function PriceDep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;

function SellDep(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function OptDep(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function SelDep_Anlz(q: TpFIBDataSet): boolean;
begin
  if CenterDep then Result:=True
  else Result:=q.FieldByName('D_DEPID').AsInteger=SelfDepId;
end;

function PHistDep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;

function ReplDep(q: TpFIBDataSet): boolean;
begin
  Result:=True;
end;


function Tdm.EmptyInv (invid, itype:integer):boolean;
begin
 result:=false;
 if itype=0 then
  with qutmp1 do
  begin
   close;
   sql.Text:='select count(*) from sel sl where sl.sinvid='+inttostr(invid);
   ExecQuery;
   if Fields[0].AsInteger=0 then result:=true;
   close;
   Transaction.CommitRetaining;
  end
 else if itype=1 then
  with qutmp1 do
  begin
   close;
   sql.Text:='select count(*) from prorditem where prordid='+inttostr(invid);
   ExecQuery;
   if Fields[0].AsInteger=0 then result:=true;
   close;
   Transaction.CommitRetaining;
  end
 else if itype=2 then
  with qutmp1 do
  begin
   close;
   sql.Text:='select count(*) from sret where sinvid='+inttostr(invid);
   ExecQuery;
   if Fields[0].AsInteger=0 then result:=true;
   close;
   Transaction.CommitRetaining;
  end
 else if itype=3 then
  with qutmp1 do
  begin
   close;
   sql.Text:='select count(*) from repair where sinvid='+inttostr(invid);
   ExecQuery;
   if Fields[0].AsInteger=0 then result:=true;
   close;
   Transaction.CommitRetaining;
  end
end;

function Tdm.closeinv (invid, itype:integer):boolean;
begin
 result:=false;
 if itype=0 then
  with qutmp1 do
   begin
    close;
    sql.Text:='select isclosed from sinv where sinvid='+inttostr(invid);
    ExecQuery;
    if (Fields[0].AsInteger=1) or (Fields[0].AsInteger=2) then result:=true;
    close;
    Transaction.CommitRetaining;
   end;
 if itype=1 then
  with qutmp1 do
   begin
    close;
    sql.Text:='select isset from prord where prordid='+inttostr(invid);
    ExecQuery;
    if Fields[0].AsInteger=1 then result:=true;
    close;
    Transaction.CommitRetaining;
   end
end;

procedure Tdm.PHistClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    quDPrice.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmPHist.laDep.Caption:=SDep;
    quDPrice.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;



procedure Tdm.PriceClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
  finally
    Screen.Cursor:=crDefault;
  end;
end;


procedure Tdm.ReplClick(Sender: TObject);
var i,j, PacketId: integer;
begin
 j:=0;
 if TMenuItem(Sender).Tag<>-1 then  InsRPacket(dmCom.db, R_Item, slDep.Values[IntToStr(TComponent(Sender).Tag)], 1, '', '', 1,j, PacketId)
  else
    for i:=2 to pmRepl.Items.Count-1 do
       InsRPacket(dmCom.db, R_Item, slDep.Values[IntToStr(TComponent(Sender).Tag)], 1, '', '', 1,j, PacketId);
end;

procedure Tdm.DataModuleCreate(Sender: TObject);
var Select: string;
    SWhere, RWhere, SFrom, RFrom: string;
    ff: TFloatField;
    sf: TSmallIntField;
    s: string[200];
    DepName: string;
    y, m, dd:word;
begin
  dmCom.InitDepInfo;
  IsSellEmpty := false;
  DistrW:=dmCom.fsCom.ReadInteger('DISTRW', -1);
  ArtMode:=0;
  PayType:=-1000;
  DecodeDate(dmcom.GetServerTime, y, m, dd);
  dmcom.FirstMonthDate:=EncodeDate(y, m, 1);
  dmcom.IsLoseCard:=false;
  with dmcom, quTmp do
  begin
   qutmp.close;
   qutmp.SQL.Text:='select rdb$procedure_name from rdb$procedures where rdb$procedure_name='#39+'LOG$GETID'+#39;
   qutmp.ExecQuery;
   if qutmp.Fields[0].IsNull then
    begin IsExistsLog:=false; IsMakeLog:=false end
   else begin
    IsExistsLog:=true;
    if LogGetID(47)=1 then IsMakeLog:=true else IsMakeLog:=false;
   end
  end;

  with dmcom, qutmp do
  begin
   close;
   sql.Text:='execute procedure Get_CurrCon ('+inttostr(dmcom.UserId)+', 0)';
   ExecQuery;
   close;
  end;

  with dmCom, quTmp do
   begin
     if tr.Active then tr.CommitRetaining;
     if not tr.Active then tr.StartTransaction;

     SQL.Text:='select CenterDep, D_DepId, SName from D_Dep where IsProg=1';
     ExecQuery;
     CenterDep:=Fields[0].AsInteger=1;
     SelfDepId:=Fields[1].AsInteger;
     SelfDepName:=Fields[2].AsString;     
     Close;
     SQL.Text:='select D_DepId from D_Dep where CenterDep=1';
     ExecQuery;
     if Fields[0].IsNull then raise Exception.Create('�� ������ ����������� �����');
     CenterDepId:=Fields[0].AsInteger;
     Close;
     {
     if CenterDep then
     begin
       Close;
       SQL.Text:='execute procedure upd_comp_prod';
       ExecQuery;
     end;
     }
     tr.CommitRetaining;
   end;


  if (not CenterDep)  then
    begin
      pmPrOrd.Items.Clear;
      pmAddTopr.Items.Clear;
      pmSell.Items.Clear;
      pmRET.Items.Clear;
      pmOptList.Items.Clear;
      pmORetList.Items.Clear;
      pmRetList.Items.Clear;
      pmAnlzSelDed.Items.Clear;
    end;
   { TODO : ���  ��������� ����������� ��� ������ }
  if (not CenterDep) then
      pmWH.Items.Clear;


  dmCom.FillDepMenu(dmCom.quDep, pmWH, WHDep, WHClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmDListTo, DListToDep, DListToClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmDListFrom, DListFromDep, DListFromClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmPrOrd, PrOrdDep, PrOrdClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmAddToPr, PrOrdDep, AddtoPrClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmPrice, PriceDep, PriceClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmSell, SellDep, SellClick, True );

  dmCom.FillDepMenu(dmCom.quDep, pmRET, SellDep, retClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmSellItem, SellDep, SellItemClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmOptList, OptDep, OptDepClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmORetList, OptDep, ORetClick, True );
  dmCom.FillDepMenu(dmCom.quDep, pmPHist, PHistDep, PHistClick, True);
  dmCom.FillDepMenu(dmCom.quDep, pmAnlzSelDed, SelDep_Anlz, AnlzSelDepClick, True);

  SafeLoad:=False;
  Distr:=False;

  slProd:=TStringList.Create;
  slMat:=TStringList.Create;
  slGood:=TStringList.Create;
  slIns:=TStringList.Create;
  slSup:=TStringList.Create;
  slCountry:=TStringList.Create;
  slGrMat:=TStringList.Create;
  slNote1:=TStringList.Create;
  slNote2:=TStringList.Create;
  slAtt1 := TStringlist.Create;
  slAtt2 := TStringList.Create;
  slDep := TStringList.Create;
  slSZ := TStringList.Create;
  if not dmCom.tr.Active then dmCom.tr.StartTransaction;
  LoadArtSL(ALL_DICT);
  dmCom.tr.CommitRetaining;

  Select:='select A2.D_ArtId ARTID, A2.Art2Id, A2.Art, A2.Art2, '+NL+
            ' A2.D_COMPID, A2.D_MATID, A2.D_GOODID, A2.D_INSID, A2.D_COUNTRYID, '+NL+
            ' A2.TSPrice SPrice, A2.TOptPrice OptPrice, A2.SPEQ Y, A2.OPEQ X, A2.OPR V,'+NL+
            ' A2.ProdCode AProdCode ';

  SFrom:=' FROM Art2 A2';

  RFrom:=' FROM Art2 A2';

  SWhere:=' WHERE A2.D_COMPID between ?COMPID1 and ?COMPID2 AND'+NL+
          ' A2.D_MATID between  ?MATID1 and  ?MATID2 AND'+NL+
          ' A2.D_GOODID between ?GOODID1 and  ?GOODID2 AND'+NL+
          ' A2.D_INSID between  ?INSID1 and ?INSID2 and'+NL+
          ' A2.D_COUNTRYID between  ?COUNTRYID1 and ?COUNTRYID2 and'+NL+
          ' A2.RQ>0'+NL;

  RWhere:=' WHERE A2.ART2ID=?ART2ID'+NL;

  if (not CenterDep)  then
{ TODO :
�� �������� �� ���������� ���������� � ����� ������
� ������ �������� }
  begin
   with dmCom, taDep do
    begin
      if not tr.Active then tr.StartTransaction;
      D_DepId := SelfDepId;
      Active:=True;
      DepName := taDepSNAME.AsString ;
      Active:=False;
      tr.CommitRetaining;
    end;
    Select:=Select+', (select TPrice from Price where Art2Id=a2.Art2Id and DepId='+IntToStr(SelfDepId)+') P'+IntToStr(SelfDepId);
    Select:=Select+', (select PEQ from Price where Art2Id=a2.Art2Id and DepId='+IntToStr(SelfDepId)+') T'+IntToStr(SelfDepId);
    Select:=Select+', (select PR from Price where Art2Id=a2.Art2Id and DepId='+IntToStr(SelfDepId)+') R'+IntToStr(SelfDepId);

    ff:=TFloatField.Create(quPrice);
    ff.FieldName:='P'+IntToStr(SelfDepId);
    ff.DisplayLabel:=DepNAME;
    ff.Currency:=True;
    ff.DataSet:=quPrice;
    ff.Tag:=SelfDepId;

    sf:=TSmallintField.Create(quPrice);
    sf.FieldName:='T'+IntToStr(SelfDepId);
    sf.DataSet:=quPrice;
    sf.Tag:=SelfDepId;

    sf:=TSmallintField.Create(quPrice);
    sf.FieldName:='R'+IntToStr(SelfDepId);
    sf.DataSet:=quPrice;
    sf.Tag:= SelfDepId;

    

  end
  else
  begin
    with dmCom, quDep do
      begin
        if not tr.Active then tr.StartTransaction;
        Active:=True;
        First;
        while NOT EOF do
          begin

            Select:=Select+', (select TPrice from Price where Art2Id=a2.Art2Id and DepId='+quDepD_DepId.AsString+') P'+quDepD_DepId.AsString;
            Select:=Select+', (select PEQ from Price where Art2Id=a2.Art2Id and DepId='+quDepD_DepId.AsString+') T'+quDepD_DepId.AsString;
            Select:=Select+', (select PR from Price where Art2Id=a2.Art2Id and DepId='+quDepD_DepId.AsString+') R'+quDepD_DepId.AsString;

            ff:=TFloatField.Create(quPrice);
            ff.FieldName:='P'+quDepD_DepId.AsString;
            ff.DisplayLabel:=quDepSName.AsString;
            ff.Currency:=True;
            ff.DataSet:=quPrice;
            ff.Tag:=quDepD_DepId.AsInteger;

            sf:=TSmallintField.Create(quPrice);
            sf.FieldName:='T'+quDepD_DepId.AsString;
            sf.DataSet:=quPrice;
            sf.Tag:=quDepD_DepId.AsInteger;

            sf:=TSmallintField.Create(quPrice);
            sf.FieldName:='R'+quDepD_DepId.AsString;
            sf.DataSet:=quPrice;
            sf.Tag:=quDepD_DepId.AsInteger;

            Next;
          end;
        Active:=False;
        tr.CommitRetaining;
      end;
  end;


  ff:=TFloatField.Create(quPrice);
  ff.FieldName:='OPTPRICE';
  ff.DisplayLabel:='���.����';
  ff.Currency:=True;
  ff.DataSet:=quPrice;
  ff.Tag:=-1;

  sf:=TSmallintField.Create(quPrice);
  sf.FieldName:='X';
  sf.DisplayLabel:='���';
  sf.DataSet:=quPrice;
  sf.Tag:=-1;

  ff:=TFloatField.Create(quPrice);
  ff.FieldName:='SPRICE';
  ff.DisplayLabel:='��.����';
  ff.Currency:=True;
  ff.DataSet:=quPrice;
  ff.Tag:=-1;

  sf:=TSmallintField.Create(quPrice);
  sf.FieldName:='Y';
  sf.DisplayLabel:='������';
  sf.DataSet:=quPrice;
  sf.Tag:=-1;

  sf:=TSmallintField.Create(quPrice);
  sf.FieldName:='V';
  sf.DisplayLabel:='���';
  sf.DataSet:=quPrice;
  sf.Tag:=-1;

  with quPrice do
    begin
      SelectSQL.Text:=Select+NL+SFrom+NL+SWhere+NL+'ORDER BY A2.ART';
      PSQL1:=SelectSQL.Text;
      PSQL2:=Select+NL+SFrom+NL+SWhere+NL+'ORDER BY A2.ART';
      SelectSQL.Text:=PSQL2;
      RefreshSQL.Text:=Select+NL+RFrom+NL+RWhere;
    end;

  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      taRec.Active:=True;
      TmpDir:=taRecTmpDir.AsString;
      TmpDB:=taRecTmpDB.AsString;
      PTrNds:=taRecPTrNds.AsFloat;
      MaxCountWH:=taRecMAXCOUNTWH.AsInteger;
      MaxCountUIDWH:=taRecMAXCOUNTUIDWH.AsInteger;
      taRec.Active:=False;
      tr.CommitRetaining;
    end;

  s := ' ';

  SetBeginDate;

  UIDWHSupId:=-1;
  UIDWHSupFlt:= False;
  NotCalcUID:= False;
  UIDWHType:=3;

  try
    GetHereId(dmCom.db);
    FillReplMenu(dmCom.db, pmRepl, ReplClick);
  except
  end;


  RBD:=dmCom.FirstMonthDate;
  RED:=dmCom.GetServerTime;
//  RBD:=RED-100;

  if not dmCom.DepSortWithDel.Active then  OpenDataSet(dmCom.DepSortWithDel);
  ErrSick:=false;
  A2Err:=false;

  if CenterDep then
  begin
    taSellList.SQLs.SelectSQL.Text := AnsiReplaceText(taSellList.SQLs.SelectSQL.Text, 'FROM SELL_S', 'FROM SELL_SN1');
  end;
end;

procedure Tdm.DelConf(DataSet: TDataSet);
begin
  if not CheckForUIDWHCalc then
    raise Exception.Create('�������������� �� ��������. ���� �������� ������. ��� ������ ������� Esc');

  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then
    SysUtils.Abort
  else
    if (WorkMode = 'ALLSELL') or (WorkMode = 'SELL') or (WorkMode = 'SELLCH') then
     if (DataSet.Name='tasellitem')and(taSellItemRET.AsInteger=1) then
      with quSellElDel do
      begin
        SQL.Text := 'update SellElem set IsNotProve = 0 where Uid =' +  taSellItemUID.AsString;
        ExecQuery;
        Transaction.CommitRetaining;
        close;
      end
end;

procedure Tdm.SetBeginDate;
begin
  calc_beginDate:=dmCom.GetServerTime-30;
  BeginDate:=dmCom.FirstMonthDate;
  EndDate:= strtodatetime(datetostr(dmCom.GetServerTime))+0.99999;
end;

procedure Tdm.CommitRetaining(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure Tdm.PostBeforeClose(DataSet: TDataSet);
begin
 PostDataSet(DataSet);
end;

procedure Tdm.PostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if (Pos('update conflicts with concurrent update', E.Message)<>0) or
     (Pos('deadlock', E.Message)<>0) then
     DataSet.Cancel
     else
       if (Pos('in unique index "D_ART_IDX1"', E.Message)<>0) then
           raise Exception.Create('������� � ������ ���������������� ��� ����������');

   DataSet.cancel;
end;

procedure Tdm.taSListNewRecord(DataSet: TDataSet);
begin
  taSListSInvId.AsInteger:=dmCom.GetId(8);
  if SDepId<>-1 then taSListDepId.AsInteger:=SDepId;
  taSListSDate.AsDateTime:=dmCom.GetServerTime;
  taSListNDate.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime));
  taSListNDS.AsFloat:=0;
  taSListTr.AsFloat:=0;

  taSListPMargin.AsFloat:=0;
  taSListCost.AsFloat:=0;
  taSListAkciz.AsFloat:=dmCom.taRecAkciz.AsFloat;

  taSListIsClosed.AsInteger:=0;
  taSListDep.AsString:=SDep;
  taSListPTr.AsFloat:=0;
  taSListNotPr.AsInteger:=0;
  taSListITYPE.AsInteger:=SItype;

  taSListISTOLLING.AsInteger := IsTolling;

  with quTmp do
    begin
      //SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=1 AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') and depid = '+inttostr(SDepId)+')';
      SQL.Text:='SELECT max(SN) FROM SINV WHERE ITYPE in (1, 21) AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') AND depid = '+inttostr(SDepId);
      ExecQuery;
      if Fields[0].IsNull then taSListSN.AsInteger:=1
      else taSListSN.AsInteger:=Fields[0].AsInteger+1;
      Close;
    end;
  taSListPTrNds.AsFloat:=PTrNds;

end;

procedure Tdm.taSListCalcFields(DataSet: TDataSet);
begin
  taSListTotalCost.AsFloat:=taSListCost.AsFloat+taSListTr.AsFloat;
  if taSListISCLOSED.AsInteger=1 then
  begin
   taSListSnOpen.AsInteger:=1;
   taSListSnClose.AsInteger:=0;
   taSListTotalCostOpen.Asfloat:=taSListTOTALCOST.AsFloat;
   taSListTotalCostClose.AsFloat:=0;
   taSListCostOpen.AsFloat:=taSListCOST.AsFloat;
   taSListCostClose.AsFloat:=0;
   taSListTrOpen.AsFloat:=taSListTR.AsFloat;
   taSListTrClose.AsFloat:=0;
   taSListTqOpen.AsInteger:=taSListTQ.AsInteger;
   taSListTQClose.AsInteger:=0;
   taSListTWOpen.AsFloat:=taSListTW.AsFloat;
   taSListTWClose.AsFloat:=0;
   taSListNDSOpen.AsFloat:=taSListNDS.AsFloat;
   taSListNDSClose.AsFloat:=0;
   taSListTrNDSOpen.AsFloat:=taSListTRNDS.AsFloat;
   taSListTrNDSClose.AsFloat:=0;
  end
  else
  begin
   taSListSnOpen.AsInteger:=0;
   taSListSnClose.AsInteger:=1;
   taSListTotalCostOpen.Asfloat:=0;
   taSListTotalCostClose.AsFloat:=taSListTOTALCOST.AsFloat;
   taSListCostOpen.AsFloat:=0;
   taSListCostClose.AsFloat:=taSListCOST.AsFloat;
   taSListTrOpen.AsFloat:=0;
   taSListTrClose.AsFloat:=taSListTR.AsFloat;
   taSListTqOpen.AsInteger:=0;
   taSListTQClose.AsInteger:=taSListTQ.AsInteger;
   taSListTWOpen.AsFloat:=0;
   taSListTWClose.AsFloat:=taSListTW.AsFloat;
   taSListNDSOpen.AsFloat:=0;
   taSListNDSClose.AsFloat:=taSListNDS.AsFloat;
   taSListTrNDSOpen.AsFloat:=0;
   taSListTrNDSClose.AsFloat:=taSListTRNDS.AsFloat;
  end
end;

procedure Tdm.taSListPNDSChange(Sender: TField);
begin
  taSListNDS.AsFloat:=taSListCost.AsFloat*Sender.AsFloat/100;
end;

procedure Tdm.taSListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;

      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
end;

procedure Tdm.taSElNewRecord(DataSet: TDataSet);
begin
  with dmCom do
    begin
      taSElSElId.AsInteger:=GetId(10);
      if WorkMode='SINV' then
        begin
          taSElArt2Id.AsInteger:=taA2Art2Id.AsInteger;
          taSElSInvId.AsInteger:=taSListSInvId.AsInteger;
          taSElFullArt.AsString:=quArtFullArt.AsString;

          //taSElPrice.AsFloat:=taA2Price1.AsFloat;

          taSElPriceHack.AsFloat:=taA2PriceHack.AsFloat;

          taSElUseMargin.AsInteger:=taA2UseMargin.AsInteger;

          {if taA2UseMargin.AsInteger=0 then }taSElPrice2.AsFloat:=taA2Price2.AsFloat;
{          else
            begin
              GetMargin(taSListDepId.AsInteger, Margin, RoundNorm);
              P:=Power(10, RoundNorm);
              taSElPrice2.AsFloat:=Ceil(taSElPrice.AsFloat*P*(1+Margin/100))/P;
            end;}
//          taSElNDSID.AsInteger:=taA2NDSID.AsInteger;
          taSElArt2.AsString:=taA2Art2.AsString;
          taSElUnitId.AsInteger:=quArtUnitId.AsInteger;
//          if NOT taA2NDSID.IsNull then taSElNDSID.AsInteger:=taA2NDSID.AsInteger
          if NOT taSListNDSID.IsNull then taSElNDSId.AsInteger:=taSListNDSID.AsInteger
               else raise Exception.Create('���������� ������ �������� � ���� ���');
        end
      else
      if WorkMode='DINV' then
         begin
           taSElArt2Id.AsInteger:=quD_WHArt2Id.AsInteger;
           taSElUseMargin.AsInteger:=0;
           taSElSInvId.AsInteger:=taDListSInvId.AsInteger;
           taSElFullArt.AsString:=quD_WHFullArt.AsString;
           taSElPrice.AsFloat:=quD_WHPrice2.AsFloat;

           taSElPrice2.AsFloat:=dm2.GetPrice(taDListDepId.AsInteger, taSElArt2Id.AsInteger);

           taSElPNDS.AsFloat:=0;
           taSElArt2.AsString:=quD_WHArt2.AsString;
           taSElUnitId.AsInteger:=quD_WHUnitId.AsInteger;

         end
       else
       if WorkMode='OPT' then
         begin
           taSElUseMargin.AsInteger:=0;
           taSElSInvId.AsInteger:=taOptListSInvId.AsInteger;
           taSElPNDS.AsFloat:=0;
           case SellMode of
               False:
                  begin
                    taSElFullArt.AsString:=quDPriceFullArt.AsString;
                    taSElPrice.AsFloat:=quDPricePrice.AsFloat;
                    taSElPrice2.AsFloat:=quDPriceOptPrice.AsFloat;
                    taSElArt2.AsString:=quDPriceArt2.AsString;
                    taSElUnitId.AsInteger:=quDPriceUnitId.AsInteger;
                    taSElArt2Id.AsInteger:=quDPriceArt2Id.AsInteger;
                    taSElWHSell.AsInteger:=0;
                  end;
               True:
                  begin
                    taSElFullArt.AsString:=quD_WHFullArt.AsString;
                    taSElPrice.AsFloat:=quD_WHPrice.AsFloat;
                    taSElPrice2.AsFloat:=quD_WHOptPrice.AsFloat;
                    taSElArt2.AsString:=quD_WHArt2.AsString;
                    taSElUnitId.AsInteger:=quD_WHUnitId.AsInteger;
                    taSElArt2Id.AsInteger:=quD_WHArt2Id.AsInteger;
                    taSElWHSell.AsInteger:=1;
                  end;
             end;
         end;
   end;
end;


procedure Tdm.taSElBeforeOpen(DataSet: TDataSet);
begin
  if WorkMode='SINV' then taSEl.Params[0].AsInteger:=taSListSInvId.AsInteger
  else if WorkMode='DINV' then taSEl.Params[0].AsInteger:=taDListSInvId.AsInteger
       else if WorkMode='OPT' then taSEl.Params[0].AsInteger:=taOptListSInvId.AsInteger
            else if WorkMode='REST' then  taSEl.Params[0].AsInteger:=dm2.taRInvSInvId.AsInteger
                 else if WorkMode='SRET' then  taSEl.Params[0].AsInteger:=taSRetListSInvId.AsInteger;

end;

procedure Tdm.taSElBeforeInsert(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taSListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;

//    if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
      PostDataSets([taSList, taSEl, {dmCom.taArt,} taA2]);
      if taA2ArtId.IsNull then SysUtils.Abort;
      if taSListSupId.IsNull then raise Exception.Create('���������� ������ ����������');
      if taSListNdsId.IsNull then raise Exception.Create('���������� ������ ��� � ���������');
    end
  else
  if WorkMode='DINV' then
    begin
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taDListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;

      if taDListDepFromId.AsInteger<>SelfDepId then raise Exception.Create('���������� ������������� ������ ���������');
  if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
      PostDataSets([taDList, taSEl]);
      if quD_WHArt2Id.IsNull then SysUtils.Abort;
    end
  else if WorkMode='OPT' then
         begin
           PostDataSets([taOptList, taSEl]);
           case SellMode of
               False: if quDPriceArt2Id.IsNull then SysUtils.Abort;
               True:  if quD_WHArt2Id.IsNull then SysUtils.Abort;
             end;
         end
       else if WorkMode='RET' then
              begin
                if quDPriceArt2Id.IsNull then SysUtils.Abort;
              end;
end;

procedure Tdm.taSElCalcFields(DataSet: TDataSet);
begin
  case  taSElUnitId.AsInteger of
      0: taSElCost.AsFloat:=taSElQuantity.AsFloat*taSElPrice.AsFloat;
      1: taSElCost.AsFloat:=taSElTotalWeight.AsFloat*taSElPrice.AsFloat;
    end;
  if WorkMode='OPT' then
    case  taSElUnitId.AsInteger of
        0: taSElCost2.AsFloat:=taSElQuantity.AsFloat*taSElEP2.AsFloat;
        1: taSElCost2.AsFloat:=taSElTotalWeight.AsFloat*taSElEP2.AsFloat;
      end
  else
    case  taSElUnitId.AsInteger of
        0: taSElCost2.AsFloat:=taSElQuantity.AsFloat*taSElPrice2.AsFloat;
        1: taSElCost2.AsFloat:=taSElTotalWeight.AsFloat*taSElPrice2.AsFloat;
      end;


  taSElRecNo.AsInteger:=taSEl.RecNo;
end;

procedure Tdm.taSItemBeforeOpen(DataSet: TDataSet);
begin
  taSItem.Params[0].AsInteger:=taSElSElId.AsInteger;
end;

procedure Tdm.taSItemCalcFields(DataSet: TDataSet);
begin
  taSItemRecNo.AsInteger:=taSItem.RecNo;
end;

procedure Tdm.SElAfterPost(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
  if WorkMode='SINV' then taSList.Refresh
  else  if WorkMode='DINV' then taDList.Refresh
        else if WorkMode='OPT' then taOptList.Refresh;

  DataSet.Tag:=0;
  {
  if WorkMode='SINV' then
    if (taSElPrice.AsFloat<>taSElPrice.OldValue) or
       (taSElPrice2.AsFloat<>taSElPrice2.OldValue) or
       (taSElUseMargin.AsInteger<>taSElUseMargin.OldValue)
       or (taSElNDSid.AsInteger<>taSElNDSid.OldValue)then
      with taA2 do
        begin
          if Locate('ART2ID', taSElArt2Id.AsInteger, []) then
            begin
              taA2.Refresh;
            end;
          RefreshAllSEl(taSElArt2Id.AsInteger);
        end;}

  if WorkMode='SINV' then
      with taA2 do
        begin
          if Locate('ART2ID', taSElArt2Id.AsInteger, []) then
            begin
              taA2.Refresh;
            end;
          //RefreshAllSEl(taSElArt2Id.AsInteger);
        end;

end;

procedure Tdm.taSListADDNDSTOPRICEChange(Sender: TField);
begin
 // taSList.Post;
  if dm.taA2.State in [dsEdit, dsInsert] then dm.taA2.Post;
  Screen.Cursor := crSQLWait;
  ReopenDataSet(dm.taA2);
  fmSInv.dgA2.Columns[4].Visible := Sender.AsInteger = 1;
  Screen.Cursor := crDefault;
end;

procedure Tdm.taSListAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure Tdm.ReopenArtDict;
var s: string;
begin
  with dmCom, dm, quArt do
    begin
      Active:=False;
      s:='';
      if D_CompId<>-1 then s:=s+' AND A.D_COMPID='+IntToStr(D_COMPID);
      if D_MatId<>'*' then s:=s+' AND A.D_MATID='''+D_MATID+'''';
      if D_GoodId<>'*' then s:=s+' AND A.D_GOODID='''+D_GOODID+'''';
      if D_COUNTRYId<>'*' then s:=s+' AND A.D_COUNTRYID='''+D_COUNTRYID+'''';
      if D_InsId<>'*' then s:=s+' AND A.D_INSID='''+D_INSID+'''';
      if s<>'' then
        begin
          s[2]:=' ';
          s[3]:=' ';
          s[4]:=' ';
          s:='WHERE '+s;
        end;
      SelectSQL[3]:=s;
      Open;
    end;
end;

procedure Tdm.taDefDistBeforeOpen(DataSet: TDataSet);
begin
  with taDefDist do
    begin
      ParamByName('UserId').AsInteger:=dmCom.UserId;
      ParamByName('D_ArtId').AsInteger:=dmCom.taArtD_ArtId.AsInteger;
    end;
end;

procedure Tdm.taSListDEPIDChange(Sender: TField);
begin
  if taSList.State<>dsInsert then SysUtils.Abort;
end;


procedure Tdm.taA2InsBeforeOpen(DataSet: TDataSet);
begin
  taA2Ins.Params[0].AsInteger:=Art2Id;
end;

procedure Tdm.taA2InsNewRecord(DataSet: TDataSet);
begin

  taA2InsInsId.AsInteger:=dmCom.GetId(11);
  taA2InsArt2Id.AsInteger:=Art2Id;
  with dm2.quGetIns do
    begin
      Params[0].AsInteger:=Art2Id;
      ExecQuery;
      if not Fields[0].IsNull then taA2InsD_InsId.AsString:=DelRSpace(Fields[0].AsString);
      if not Fields[1].IsNull then taA2InsEdgShapeId.AsString:=DelRSpace(Fields[1].AsString);
      if not Fields[2].IsNull then taA2InsEdgTypeId.AsString:=DelRSpace(Fields[2].AsString);
      Close;
    end
//  taA2InsD_InsId.AsString:=dmCom.taArtD_InsId.AsString;
end;

procedure Tdm.taSListBeforePost(DataSet: TDataSet);
begin
  if taSListNDS.IsNull then  raise Exception.Create('���������� ������ �������� � ���� ���');
  if taSListNDS.AsFloat<0 then  raise Exception.Create('��� ������ ���� ������ ��� ����� ���� ');
  if taSListTr.IsNull then  raise Exception.Create('���������� ������ �������� � ���� ��.�������');
  if taSListTr.AsFloat<0 then  raise Exception.Create('��.������� ������ ���� ������ ��� ����� ���� ');
  if taSListSDATE.IsNull then raise Exception.Create('���������� ������ ���� ��������� ��������');
  {$IFDEF TRIALPERIOD}
  CheckTrialPeriod(taSListSDATE.AsDateTime); 
  {$ENDIF}
end;

procedure Tdm.taSElQUANTITYChange(Sender: TField);
begin
  if taSElUnitId.AsInteger=0 then taSElQ.AsFloat:=taSElQuantity.AsFloat;
end;

procedure Tdm.taSElTOTALWEIGHTChange(Sender: TField);
begin
  if taSElUnitId.AsInteger=0 then taSElQ.AsFloat:=taSElQuantity.AsFloat;
end;

procedure Tdm.taSElBeforePost(DataSet: TDataSet);
begin
  if taSElPrice.IsNull then raise Exception.Create('���������� ������ �������� � ���� ��.����');
  if taSElPrice2.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���.����');
  if taSElPrice.AsFloat<0 then raise Exception.Create('��.���� ������ ���� ������ ��� ����� ����');
  if taSElPrice2.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
  PostDataSets([taA2]);
end;

procedure Tdm.WHClick(Sender: TObject);
begin
  try
   Screen.Cursor:=crSQLWait;
    if WorkMode='ARTWH' then
      begin
        quWH.Active:=False;
      end
    else
      begin
       if fmUIDWH2 = nil then
       begin
         dsUIDWH.DataSet.Active:=false;
       end;
//        quUIDWH.Active:=False;
      end;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;

   if WorkMode='ARTWH' then
      begin
//        fmWH.laDep.Caption:=SDep;
        quWH.Active:=True;
      end
    else
      begin
        if fmUIDWH <> nil then
        begin
          fmUIDWH.laDep.Caption:=SDep;
        end;
        if fmUIDWH2<> nil then
        begin
          fmUIDWH2.laDep.Caption:=SDep;
        end;

      //  fmseldep_anlz.laDep.Caption:=dm.SDep;
//        quUIDWH.Active:=True;
        if fmUIDWH2 = nil then
        begin
          dsUIDWH.DataSet.Active:=true;
        end else
        begin
          fmUIDWH2.Warehouse.OfficeChanged;
        end;
      end;
  except
    on E: Exception do
    begin
      ShowMessage(E.Message);
      Screen.Cursor:=crDefault;
    end;
  end;
  Screen.Cursor:=crDefault;
end;

procedure Tdm.quWHBeforeOpen(DataSet: TDataSet);
begin
  SetDepFilter(quWH.Params, SDepId);
end;

procedure Tdm.quWHArt2BeforeOpen(DataSet: TDataSet);
begin
  with quWHArt2 do
    begin
      ParamByName('D_ARTID').AsInteger:=quWHD_ArtId.AsInteger;
      SetDepFilter(Params, SDepId);
    end;
end;

procedure Tdm.quWHUIDBeforeOpen(DataSet: TDataSet);
begin
  with quWHUID, Params do
    begin
{      ByName('D_ARTID').AsInteger := dm2.ibdsWHTmpD_ARTID.AsInteger;
      ByName('DepId1').AsInteger := Low(Integer);
      ByName('DepId2').AsInteger := High(Integer);}

      ParamByName('D_ARTID').AsInteger:=quWHD_ARTID.AsInteger;
      SetDepFilter(Params, SDepId);
    end;

end;

procedure Tdm.quWHA2UIDBeforeOpen(DataSet: TDataSet);
var A2ID: integer;
    DID: integer;
    ddate:TDAteTime;
begin
  DID := 0;
  A2ID := 0;
  Ddate := Date;
  if Sells then
      begin
        if WorkMode='ALLSELL' then
         begin
         DID:=taSellList.FieldByName('DepId').AsInteger;
         DDate:=taSellList.FieldByName('ED').AsDateTime;
         end
        else
         begin
          DID:=taCurSell.FieldByName('DepId').AsInteger;
          DDate:=dmCom.GetServerTime;// taCurSell.FieldByName('ED').AsDateTime;
         end;
        A2ID:=quD_WHArt2Id.AsInteger;
      end
    else
    if WorkMode='ARTWH' then
      begin
        DID:=SDepId;
        A2ID:=quWHArt2Art2Id.AsInteger;
      end
    else if WorkMode='DINV' then
           begin
             DID:=taDListDepFromId.AsInteger;
             A2ID:=taSElArt2Id.AsInteger;
           end
         else if WorkMode='OPT' then
                begin
                  DID:=taOptListDepFromId.AsInteger;
                  A2ID:=taSElArt2Id.AsInteger;
                end
               else if WorkMode='SRET' then
                   begin
                     DID:=taSRetListDepFromId.AsInteger;
                     A2ID:=quD_WHArt2Id.AsInteger;
                   end;
  with quWHA2UID.Params do
    begin
      ByName['ART2ID'].AsInteger:=A2ID;
      ByName['DDATE'].AsDAte:=Ddate;
      if DID=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=DID;
          ByName['DEPID2'].AsInteger:=DID;
        end;
    end;
end;

procedure Tdm.quWHSZBeforeOpen(DataSet: TDataSet);
var DAId: integer;
    DID: integer;
begin
  DAId := 0;
  DID := 0;
  if WorkMode='ARTWH' then
    begin
      if WHSZMode=1 then
        begin
          DAID:=quWHD_ARTID.AsInteger;
          DID:=SDepId;
        end
      else
        begin
          DAID:=dmCom.taArtD_ArtId.AsInteger;
          DID:=taDefDistDepID.AsInteger;
        end;
    end
  else if WorkMode='DINV' then
         begin
           with quTmp do
             begin
               SQL.Text:='SELECT D_ARTID FROM ART2 WHERE ART2ID='+taSElArt2Id.AsString;
               ExecQuery;
               DAId:=Fields[0].AsInteger;
               Close;
             end;
           DID:=taDListDepFromId.AsInteger;
         end
      else if WorkMode='OPT' then
             begin
               with quTmp do
                 begin
                   SQL.Text:='SELECT D_ARTID FROM ART2 WHERE ART2ID='+taSElArt2Id.AsString;
                   ExecQuery;
                   DAId:=Fields[0].AsInteger;
                   Close;
                 end;
               DID:=taOptListDepFromId.AsInteger;
             end;

  with quWHSZ, Params do
    begin
      ByName['D_ARTID'].AsInteger:=DAId;
      SetDepFilter(Params, DID);
    end;
end;

procedure Tdm.quWHA2SZBeforeOpen(DataSet: TDataSet);
var DID: integer;
    A2ID: integer;
begin
  DID := 0;
  A2ID := 0;
  with quWHA2SZ do
  if WorkMode='ARTWH' then
    begin
      DID:=SDepId;
      A2ID:=quWHArt2Art2Id.AsInteger;
    end
  else if WorkMode='DINV' then
         begin
           DID:=taDListDepFromID.AsInteger;
           if Distr then A2ID:=DstArt2Id
           else A2ID:=taSElArt2ID.AsInteger;
         end
       else if WorkMode='OPT' then
         begin
           DID:=taOptListDepFromID.AsInteger;
           A2ID:=taSElArt2ID.AsInteger;
         end;


  with quWHA2SZ.Params do
    begin
      ByName['ART2ID'].AsInteger:=A2ID;
      if DID=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=DID;
          ByName['DEPID2'].AsInteger:=DID;
        end
    end;
end;

procedure Tdm.DListToClick(Sender: TObject);
var
j:integer;
begin
  try
    Screen.Cursor:=crSQLWait;
    taDList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmDList.laDepTo.Caption:=SDep;
    if not centerdep then
    begin
    if SDepId=centerdepid then
    begin
    pmDListFrom.items.tag:=SelfDepId;
    DDepFromId:=pmDListFrom.Items.tag;
    for j := 0 to pmDlistFrom.Items.Count - 1 do
    begin
    if pmDlistFrom.items.Items[j].tag=DDepFromId then
    DDepFrom:= pmDListFrom.items[j].Caption;
    end;
    //DDepFrom:= pmDListFrom.items[DDepFromId-13].Caption;
    end
    else
    begin
    if SDepId=SelfDepId then
    begin
    pmDListFrom.Items.tag:=CenterDepId;
    DDepFromId:=pmDListFrom.Items.tag;
    for j := 0 to pmDlistFrom.Items.Count - 1 do
     begin
    if pmDlistFrom.items.Items[j].tag=DDepFromId then
    DDepFrom:=pmDListFrom.items[j].Caption;
    end;
    end;
    end;
    end;
     taDList.Active:=True;
    fmDList.laDepFrom.Caption:=DDepFrom;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.DListFromClick(Sender: TObject);
var
i:integer;
begin
  try
    Screen.Cursor:=crSQLWait;
    taDList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    DDepFromId:= TMenuItem(Sender).Tag;
    DDepFrom:= TMenuItem(Sender).Caption;
    fmDList.laDepFrom.Caption:=DDepFrom;
    if not centerdep then
    begin
      if DDepFromId=selfdepid then
      begin
        pmDListTo.Items.tag:=CenterDepId;
        SDepId:=pmDListTo.Items.tag;
        for I := 0 to pmDlistTo.Items.Count - 1 do
        begin
          if pmDlistTo.items.Items[i].tag=SDepid then

          SDep := pmDListTo.items[i].Caption;
        end;
      end
       else
      begin
        if DDepFromId=CenterDepId then
        begin
          pmDListTo.Items.tag:=SelfDepId;
          SDepId:=pmDListTo.Items.tag;
          for I := 0 to pmDlistTo.Items.Count - 1 do
          begin
            if pmDlistTo.items.Items[i].tag=SDepid then
            SDep:= pmDListTo.Items.Items[i].Caption;
          end;
        end;
      end;
    end;

    taDList.Active:=True;
    fmDList.laDepTo.Caption:=SDep;

  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.taDListAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure Tdm.taDListBeforePost(DataSet: TDataSet);
begin
  if taDListDepFromId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������� �����');
  if taDListDepId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������� �����');
  if taDListSN.AsString='' then raise Exception.Create('���������� ������ ����� ���������');
  if taDListSDATE.IsNull then raise Exception.Create('���������� ������ ���� ���������');
  {$IFDEF TRIALPERIOD}
  CheckTrialPeriod(taDListSDATE.AsDateTime);
  {$ENDIF}
end;

procedure Tdm.taDListNewRecord(DataSet: TDataSet);
begin
  taDListSInvId.AsInteger:=dmCom.GetId(8);
  if SDepId<>-1 then taDListDepId.AsInteger:=SDepId;
  if DDepFromId<>-1 then taDListDepFromId.AsInteger:=DDepFromId;
  taDListSDate.AsDateTime:=dmCom.GetServerTime;//Date;
{  quSMode.Active:=True;
  quSMode.First;
  taSListModeID.AsInteger:=quSModeD_SModeId.AsInteger;}
  taDListNDS.AsFloat:=0;
  taDListTr.AsFloat:=0;
//  taSListPTr.AsFloat:=0;
  taDListPMargin.AsFloat:=0;
  taDListCost.AsFloat:=0;
  taDListAkciz.AsFloat:=dmCom.taRecAkciz.AsFloat;
//  taDListSN.AsInteger:=dmCom.GetId(16);
  taDListIsClosed.AsInteger:=0;
  taDListDepFrom.AsString:=DDepFrom;
  taDListDepTo.AsString:=SDep;
  taDListCrUserId.AsInteger:=dmCom.UserId;
  with quTmp do
    begin
      Close;
      SQL.Text:='SELECT MAX(SN) FROM SINV WHERE ITYPE=2 AND DEPFROMID='+IntToStr(DDepFromId)+' AND FYEAR(SDATE)=FYEAR(''TODAY'') AND DEPID='+IntToStr(SDepId);
{      SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=2 AND DEPFROMID='+IntToStr(DDepFromId)+')';}

{      SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=2 AND DEPFROMID='+IntToStr(DDepFromId)+')';}
      ExecQuery;
      if Fields[0].IsNull then taDListSN.AsInteger:=1
      else
         taDListSN.AsInteger:=Fields[0].AsInteger+1;
      Close;
    end;
  with dm do
    begin
      if (dmCom[SDepId].Urf=1) and (dmCom[DDepFromId].Urf=0) then
        begin
          taDListOpt.AsInteger:=1;
          taDListOptRet.AsInteger:=0;
        end;
      if (dmCom[SDepId].Urf=0) and (dmCom[DDepFromId].Urf=1) then
        begin
          taDListOpt.AsInteger:=0;
          taDListOptRet.AsInteger:=1;
        end;
    end;
 //*************/
  taDListNotPr.asInteger:=1;
 //***********/
end;

procedure Tdm.taDListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPTOID1'].AsInteger:=-MAXINT;
          ByName['DEPTOID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPTOID1'].AsInteger:=SDepId;
          ByName['DEPTOID2'].AsInteger:=SDepId;
        end;

      if DDepFromId=-1 then
        begin
          ByName['DEPFROMID1'].AsInteger:=-MAXINT;
          ByName['DEPFROMID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPFROMID1'].AsInteger:=DDepFromId;
          ByName['DEPFROMID2'].AsInteger:=DDepFromId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
end;

procedure Tdm.taDListDEPFROMIDChange(Sender: TField);
begin
  if taDList.State<>dsInsert then SysUtils.Abort;
end;

procedure Tdm.taDListDEPIDChange(Sender: TField);
begin
  if taDList.State<>dsInsert then SysUtils.Abort;
end;

procedure Tdm.quD_WHBeforeOpen(DataSet: TDataSet);
var DepId, nPos_: integer;
    s: string;
    fl_seach:boolean;
    s_seach :string;
    len_seach, i: integer;
    num_z:integer;
begin
  DepId := 0;
  fl_seach := False;
      nPos_:=NPos('*',D_WHArt,1);
      if (Length(D_WHArt)>0) and (nPos_>0) then
        begin
          if ((NPos('*',D_WHArt,2)>0)or(NPos_<>1)) then
            begin
             quD_WH.SelectSQL[3]:='D_WHL';
             {������� ��������� � ������}
             num_z:=0;
             for i:=1 to length(D_WHArt) do if D_WHArt[i]='*' then inc(num_z);
             {����������� ����� ������ ����������� %}
             len_seach:=(20-length(D_WHArt))div num_z;
             s_seach:='';
             for i:=1 to len_seach do s_seach:=s_seach+'%';
             fl_seach:=true;
             s:=ReplaceStr(D_WHArt,'*',s_seach);
            end
          else
            begin
              quD_WH.SelectSQL[3]:='D_WHR';
              s:=ReverseString(Copy(D_WHArt, 2, MAXINT));
            end;
        end
      else
        begin
          quD_WH.SelectSQL[3]:='D_WH';
          s:=D_WHArt;
        end;

      quD_WH.Prepare;

      if Sells then
        begin
          if (WorkMode='SELL') or (WorkMode = 'SELLCH') then DEPID:=taCurSellDepId.AsInteger
          else if WorkMode='ALLSELL' then DEPID:=taSellListDepId.AsInteger;
        end
        else if Distr then
               begin
                 if DstOne2Many then DepId:=DstDepId
                 else DepId:=taDListDepFromId.AsInteger;
               end
             else if WorkMode='DINV' then DepId:=taDListDepFromId.AsInteger
                  else if WorkMode='OPT' then DepId:=taOptListDepFromId.AsInteger
                       else if WorkMode='SRET' then DepId:=taSRetListDepFromId.AsInteger;
      quD_WH.ParamByName('ADEPID').AsInteger:=DepId;
      quD_WH.ParamByName('SHOWALL').AsInteger:=1;

      dmCom.SetArtFilter(quD_WH.Params);
      quD_WH.ParamByName('USERID').AsInteger:=dmCom.UserId;
      quD_WH.ParamByName('ART_1').AsString:=s;
      if fl_seach then quD_WH.ParamByName('ART_2').AsString:=s
      else quD_WH.ParamByName('ART_2').AsString:=s+'��';
end;

procedure Tdm.taDItemNewRecord(DataSet: TDataSet);
begin
  taDItemSItemId.AsInteger:=dmCom.GetId(14);
  taDItemW.AsFloat:=0;
  taDItemSElId.AsInteger:=taSElSElId.AsInteger;
  if (WorkMode='DINV') or (WorkMode='OPT') and (taSElWHSell.AsInteger=1) then
    begin
      taDItemW.AsFloat:=quWHA2UIDW.AsFloat;
      taDItemSZ.AsString:=quWHA2UIDSZ.AsString;
      taDItemRef.AsInteger:=quWHA2UIDSItemId.AsInteger;
      taDItemSPrice.AsFloat:=quWHA2UIDPrice.AsFloat;
  //    taDItemFSElId.AsInteger:=quWHA2UIDFSELID.AsInteger;
    end;
  if WorkMode='DINV' then taDItemBusyType.AsInteger:=0
  else if WorkMode='OPT' then
         begin
           taDItemBusyType.AsInteger:=4;
           if taSElWHSell.AsInteger=0 then
             begin
               taDItemSZ.AsString:='-';
             end
         end;
end;

procedure Tdm.taDItemBeforeOpen(DataSet: TDataSet);
begin
  taDItem.Params[0].AsInteger:=taSElSElId.AsInteger;
end;

procedure Tdm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, lbAtt1, lbAtt2: TListBox);

procedure Fill(lb: TListBox; sl: TStringList);
var s: string;
    i: integer;
begin
  if lb<>NIL then 
    with lb do
      begin
        if ItemIndex<>-1 then s:=lb.Items[ItemIndex];
        Items.Clear;
        Items.Assign(sl);
        i:=Items.IndexOf(s);
        if i<>-1 then ItemIndex:=i;
      end;
end;

begin
  LoadArtSL(ALL_DICT);
  Fill(lbComp, slProd);
  Fill(lbMat, slMat);
  Fill(lbIns, slIns);
  Fill(lbGood, slGood);
  Fill(lbCountry, slCountry);
  Fill(lbAtt1, slAtt1);
  Fill(lbAtt2, slAtt2);
end;

procedure Tdm.ListBoxKeyPress(Sender: TObject; var Key: Char);
var c, i, j: integer;
    k: byte;
begin
  if Key in ['*', '0'..'9', 'A'..'Z', 'a'..'z', '�'..'�', '�'..'�'] then
    with TListBox(Sender) do
      begin
        k:=byte(Key) and not byte(32);
        c:=Items.Count;
        i:=ItemIndex;
        j:=i;
        if (byte(Items[i][1]) and not byte(32) = k) then Inc(i);
        while (i<c) and (byte(Items[i][1]) and not byte(32) <>k) do Inc(i);
        if i<c then ItemIndex:=i
        else
          begin
            i:=0;
            while (i<ItemIndex) and (byte(Items[i][1]) and not byte(32)<>k) do Inc(i);
            if i<ItemIndex then ItemIndex:=i;
          end;
        if j<>ItemIndex then TListBox(Sender).OnClick(NIL);
      end;
end;

procedure Tdm.quD_UIDBeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with TpFIBDataSet(DataSet), Params do
    begin

      if DstOne2Many then DepId:= DstDepId
      else DepId:= taDListDepFromId.AsInteger;

      ByName['ADEPID'].AsInteger:=DepID;
      ByName['ART2ID'].AsInteger:=DstArt2Id;

      if DataSet=quD_T then
        begin
          if SupFilter then
            begin
              ByName['SUPID1'].AsInteger:=SupFilterId;
              ByName['SUPID2'].AsInteger:=SupFilterId;
            end
          else
            begin
              ByName['SUPID1'].AsInteger:=-MAXINT;
              ByName['SUPID2'].AsInteger:=MAXINT;
            end;

          if PriceFilter then
            begin
              ByName['P1'].AsFloat:=PriceFilterValue-0.0001;
              ByName['P2'].AsFloat:=PriceFilterValue+0.0001;
            end
          else
            begin
              ByName['P1'].AsFloat:=-MAXINT;
              ByName['P2'].AsFloat:=MAXINT;
            end;
          if SZFilter then
            begin
              ByName['SZ1'].AsString:=SZFilterValue;
              ByName['SZ2'].AsString:=SZFilterValue;
            end
          else
            begin
              ByName['SZ1'].AsString:='!';
              ByName['SZ2'].AsString:='��';
            end;
        end
      else
        begin
          if WorkMode='SINV' then ByName['IsClosed'].AsInteger:=0
          else ByName['IsClosed'].AsInteger:=1;
        end;
    end;
end;

procedure Tdm.quD_SZBeforeOpen(DataSet: TDataSet);
var DID: integer;
begin
  if WorkMode='SINV' then DID:=DstDepId
  else
    if Distr then DID:=DstDepId
    else DID:=taDListDepFromID.AsInteger;

  with quD_SZ, Params do
    begin
      ByName['DEPID'].AsInteger:=DID;
      ByName['ART2ID'].AsInteger:=DstArt2ID;
      if WorkMode='SINV' then ByName['INVID'].AsInteger:=taSListSInvID.AsInteger
      else ByName['INVID'].IsNull:=True;
    end;
end;

procedure Tdm.taSItemNewRecord(DataSet: TDataSet);
var selid:integer;
begin
  taSItemSItemId.AsInteger:=dmCom.GetId(14);
  taSItemSElId.AsInteger:=taSElSElId.AsInteger;
  if ((WorkMode='SINV') or (WorkMode='REST')) and NOT NotCalcUID then
    taSItemUID.AsInteger:=dmCom.GetId(17);
  taSItemBusyType.AsInteger:=0;
  taSItemW.AsFloat:=0;
  taSItemSS.AsString:='-';

  with qutmp do
  begin
   close;
   sql.Text:='select count(*) from sitem where selid = '+inttostr(taSItemSELID.AsInteger);
   ExecQuery;
   selid:=Fields[0].AsInteger;
   close;
  end;
  if selid>0 then
  begin
   taSItemD_GOODS_SAM1.AsInteger:=goods1;
   taSItemD_GOODS_SAM2.AsInteger:=goods2;
  end
  else
  begin
   taSItemD_GOODS_SAM1.AsInteger:=12;
   taSItemD_GOODS_SAM2.AsInteger:=12
  end
end;

procedure Tdm.PrOrdClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taPrOrd.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmPrOrdList.laDep.Caption:=SDep;
    taPrOrd.Active:=True;
//  CalcSListCost;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.AddtoPrClick(Sender: TObject);
begin
  try
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    fmAddToPr.bDep.Caption:='����� '+TMenuItem(Sender).Caption;
  finally
  end;
end;

procedure Tdm.taPrOrdNewRecord(DataSet: TDataSet);
begin
  taPrOrdPrOrdId.AsInteger:=dmCom.GetId(18);
  if SDepId<>-1 then taPrOrdDepId.AsInteger:=SDepId;
  taPrOrdDep.AsString:=SDep;
  taPrOrdPrDate.AsDateTime:=dmCom.GetServerTime;
end;

procedure Tdm.taPrOrdItemBeforeOpen(DataSet: TDataSet);
begin
  taPrOrdItem.Params[0].AsInteger := dsPrOrd.DataSet.Fields[0].AsInteger;
end;

procedure Tdm.taPrOrdItemBeforeInsert(DataSet: TDataSet);
begin
  CheckClosedPr;
  PostDataSet(taPrOrd);
  if taPrOrd.Fields[0].IsNull then SysUtils.Abort;
  if quDPriceArt2Id.IsNull then SysUtils.Abort;
end;

procedure Tdm.taPrOrdItemNewRecord(DataSet: TDataSet);
begin
  taPrOrdItemPrOrdItemId.AsInteger:=dmCom.GetId(19);
  taPrOrdItemPrOrdId.AsInteger:=taPrOrdPrOrdId.AsInteger;
  taPrOrdItemArt2Id.AsInteger:=quDPriceArt2Id.AsInteger;
  taPrOrdItemFullArt.AsString:=quDPriceFullArt.AsString;
  taPrOrdItemArt2.AsString:=quDPriceArt2.AsString;
  taPrOrdItemCurPrice.AsFloat:=quDPricePrice.AsFloat;
end;

procedure Tdm.taSItemAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
  taSEl.Refresh;
  if (WorkMode='SINV') or (WorkMode='REST') then
    begin
      if fmSItem<>NIL then fmSItem.UpdateQW;
    end;
end;

procedure Tdm.ReOpenSZQ(q: TpFIBDataSet; sz: string);
begin
  with q do
    if Active then
       begin
         if Locate('SZ', sz, []) then Refresh
         else
           begin
             Active:=False;
             Open;
             Locate('SZ', sz, []);
           end;
       end;
end;


procedure Tdm.taDepMarginROUNDNORMGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  case Sender.AsInteger of
      2: Text:='�������';
      1: Text:='������� ������';
      0: Text:='�����';
      -1: Text:='������� ������';
      -2: Text:='����� ������';
      -3: Text:='������ ������';
     else Text:='';
   end;
end;

procedure Tdm.taDepMarginROUNDNORMSetText(Sender: TField;
  const Text: String);
begin
  if Text='�������'  then Sender.AsInteger:=2
  else
    if Text='������� ������'  then Sender.AsInteger:=1
    else
      if Text='�����'  then Sender.AsInteger:=0
      else
        if Text='������� ������'  then Sender.AsInteger:=-1
        else
          if Text='����� ������'  then Sender.AsInteger:=-2
          else
            if Text='������ ������'  then Sender.AsInteger:=-3
            else Sender.Clear;

end;

procedure Tdm.BeforeDelete(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taSListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;
    end
  else
  if WorkMode='DINV' then
    begin
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taDListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;
      if not AppDebug then
       if taDListDepFromId.AsInteger<>SelfDepId then raise Exception.Create('���������� ������������� ������ ���������');
    end;
  if (DataSet.Tag=0)and(WorkMode<>'DINVDEL') then DelConf(DataSet); 
end;

procedure Tdm.taSetPriceAfterPost(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

function Tdm.GetMargin(DepId: integer; var Margin: extended; var RoundNorm: integer; InvId: integer): boolean;
begin
  with quTmp do
    begin
      if InvId<>-1 then
        SQL.Text:='SELECT Margin, RoundNorm FROM InvMarg WHERE DepId='+IntToStr(DepId)+' AND InvId='+IntToStr(InvId)
      else
        SQL.Text:='SELECT Margin, RoundNorm FROM D_Dep WHERE D_DepId='+IntToStr(DepId);
      ExecQuery;
      Margin:=Fields[0].AsFloat;
      RoundNorm:=Fields[1].AsInteger;
      Close;
      if InvId<>-1 then
        SQL.Text:='SELECT COUNT(DISTINCT MARGIN) FROM InvMarg WHERE InvId='+IntToStr(InvId)
      else
        SQL.Text:='SELECT COUNT(DISTINCT MARGIN) FROM D_Dep';
      ExecQuery;
      Result := Fields[0].AsInteger = 1;
      Close
    end;
end;

procedure Tdm.retClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
//    taSellList.Active:=False;
       TMenuItem(Sender).Checked:=True;
     SDepId:= TMenuItem(Sender).Tag;
     SDep:= TMenuItem(Sender).Caption;
       //fmCashret.laDep.Caption:=SDep;
//    taSellList.Active:=True;
  finally
   Screen.Cursor:=crDefault;
  end;
end;



procedure Tdm.SellClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taSellList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmSellList.laDep.Caption:=SDep;
    taSellList.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.taSellListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
  dmserv.FSellEdit:=false;
end;


procedure Tdm.SellItemClick(Sender: TObject);
begin
  try
    if ((not dmcom.User.AllWh)and(TMenuItem(Sender).Tag<>dmcom.User.DepId)) then
     raise Exception.Create('������������ �� ����� ������� ����� �� '+TMenuItem(Sender).Caption);
    Screen.Cursor:=crSQLWait;
    taSellItem.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmSellItem.laDep.Caption:=SDep;
    if SDepId=-1 then SellId:=-1
    else
      begin

{ TODO : ���������� �������� ������������ � ���������� ������� }

     EmpDepId:=SDepId;
{     with quEmp do
      begin
       Active:=False;
       Open;
      end;}

     with taCurSell do
      begin
       Active:=False;
       Open;
       ReOpenDataSets([dm2.quClient]);
       D_CHECK0 := 0;

       quCurSellCheck.Active:=false;
       quCurSellCheck.Open;
       {�������� ������� �� �����}
       if (not taCurSellSellId.IsNull) and (taCurSellEMP1ID.AsInteger = dmcom.User.UserId)
        then  SellId:=taCurSellSellId.AsInteger
        else SellId:=-1;
        fmSellItem.ShowSell;
       end;

       UseFReg:=false;
       if taCurSellFREG.AsInteger=1 then
       if (dmcom.FReg_) and (not dmfr.FrReady)and(SEllID<>-1) then
       begin
        while (MessageDialog('������ � ���������� �������������', mtInformation, [mbOK,mbCancel],0) = mrOk) and (not UseFReg) do
         if dmfr.FrReady  then UseFReg := true
       end
       else if (dmcom.FReg_)and(SDepId = dmcom.User.DepId) then UseFReg:=true;

      taSellItem.Active:=True;

   end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.taSellItemBeforeOpen(DataSet: TDataSet);
begin
  if WorkMode='SELLCH' then
   begin
{    if D_CHECK0 =0 then     taSellItem.SelectSQL[6] := 'where checkno is null'
    else}
    taSellItem.SelectSQL[6] := 'where checkno = '+inttostr(D_CHECK0);
    taSellItem.Params[0].AsInteger:=taCurSellSellId.AsInteger;
   end
  else  taSellItem.SelectSQL[6] := '';
  if WorkMode='SELL' then taSellItem.Params[0].AsInteger:=taCurSellSellId.AsInteger
  else if WorkMode='ALLSELL' then taSellItem.Params[0].AsInteger:=taSellListSellId.AsInteger;


  taSellItem.Params[1].AsInteger:=dmCom.UserId;
  //dmserv.FInserSellItem:=false;
  dmserv.FSellEdit:=false;
end;

procedure Tdm.taSellItemNewRecord(DataSet: TDataSet);
var DepId, i, j:integer;
    st:string;
begin
 if IsSellEmpty then
 begin
   IsSellEmpty := false;
   raise Exception.Create('��� �������');
 end;
  {*****���������� ����� � ��������������� ��������***}
  with qutmp  do
  begin
   close;
   sql.Text:='select count(*) from d_dep where d_depid<>-1000 and isdelete<>1';
   ExecQuery;
   i:=Fields[0].AsInteger;
   close;
   st:='';
   for j:=1 to i do st:=st+'0;';
   taSellItemSQAPPL.AsString:=st;
  end;
  {***************************************************}

  taSellItemSellItemId.AsInteger:=dmCom.GetId(22);
  taSellItemF.AsInteger := 1;
  taSellItemCARD.AsInteger := SellitemCard;

  // basile
  if (WorkMode='SELL')or(WorkMode='SELLCH')  then
  begin
    taSellItemSellId.AsInteger:=taCurSellSellId.AsInteger;
    taSellItemEmpId.AsInteger:= taCurSellEMP1id.AsInteger;
    taSellItemCASHIERID.AsInteger:= taCurSellEMP1id.AsInteger;
    if (WorkMode = 'SELLCH') then
    begin
      taSellItemCHECKNO.AsInteger:=0;
      D_CHECK0:=0;
    end;
  end
  else if (WorkMode = 'ALLSELL')then
  begin
    taSellItemSellId.AsInteger:=taSellListSellId.AsInteger;
    taSellItemEmpId.AsInteger:= dmcom.UserId; // taSellListEMP1ID.AsInteger;
    taSellItemCASHIERID.AsInteger:= dmcom.UserId;
  end
  else raise Exception.Create('������ [002]. ���������� � �������������');

  taSellItemDep.AsInteger:=1;
  taSellItemDiscount.AsFloat:=0;
  taSellItemClientId.AsInteger:=-1;
  if SellRet then taSellItemRet.AsInteger:=1
  else taSellItemRet.AsInteger:=0;

/////////////////////////////////�����/////////////////////////////////
    {��������� ���� ������� � ��������
    - � ����� - ����� ������� ����
    - � �������� (ed is not null) ���� ���� �������� ������ ������� ����,
      �� ����� ���� ������� - ���� �������� �����}
  if SellRet then{��������}
  begin
   if (WorkMode='SELL')or(WorkMode='SELLCH')  then
{   begin
    if taCurSellBD.AsDateTime > dmCom.GetServerTime then taSellItemRETDATE.AsDateTime:=Now
      else }taSellItemRETDATE.AsDateTime:=dmCom.GetServerTime{;
   end}
   else
   begin
    if taSellListUseSDate.AsInteger=1 then taSellItemRETDATE.AsDateTime:=taSellListBD.AsDateTime
    else
    begin
     if ((not taSellListED.IsNull)and(taSellListED.AsDateTime<dmCom.GetServerTime)) then
       taSellItemRETDATE.AsDateTime:=taSellListED.AsDateTime-0.001
     else
{     begin
       if taSellListBD.AsDateTime > dmCom.GetServerTime then taSellItemRETDATE.AsDateTime:=Now
        else} taSellItemRETDATE.AsDateTime:=dmCom.GetServerTime;
//     end;
    end;
   end;
//    taSellItemRetDate.AsDateTime:=Now;
  end
  else {�������}
  begin
   if (WorkMode='SELL')or(WorkMode='SELLCH')  then
   begin
{    if taCurSellBD.AsDateTime > dmCom.GetServerTime then taSellItemADATE.AsDateTime:=Now
      else} taSellItemADATE.AsDateTime:=dmCom.GetServerTime;
   end
   else
   begin
    if taSellListUseSDate.AsInteger=1 then taSellItemADATE.AsDateTime:=taSellListBD.AsDateTime
    else
    begin
     if ((not taSellListED.IsNull)and(taSellListED.AsDateTime<dmCom.GetServerTime)) then
       taSellItemADATE.AsDateTime:=taSellListED.AsDateTime-0.001
     else
     begin
 {      if taSellListBD.AsDateTime > dmCom.GetServerTime then taSellItemADATE.AsDateTime:=Now
        else }taSellItemADATE.AsDateTime:=dmCom.GetServerTime;
     end;
    end;
   end;
  end;

/////////////////////////////////�����/////////////////////////////////

  taSellItemRetDone.AsInteger:=0;
  if WorkMode='ALLSELL' then DepId:=taSellListDepId.asInteger
  else DepId:=taCurSellDepId.asInteger;
  taSellItemOpt.AsInteger:=dmCom.Dep[DepID].Urf;
end;

procedure Tdm.taSellItemBeforeInsert(DataSet: TDataSet);
var i:integer;
begin

  if ((dmcom.User.ALLSeLL=0)and(WorkMode<>'SELLCH')) then
   raise Exception.Create('��� ���� �� ����������');

  with qutmp do
  begin
   Sql.Text := '  select uidwhcalc  from d_rec ';
   ExecQuery;
   i:= Fields[0].AsInteger;
   close;
  end;
   If i = 1 then Raise Exception.Create('��������� ������ �����������');
   //dmserv.FInserSellItem := True;
   CheckSellClosed;
   PostDataSets([taCurSell, taSellList]);
   if (WorkMode='SELL')or(WorkMode='SELLCH') then
     if taCurSellSellId.IsNull then OpenSell;
end;

procedure Tdm.taA2BeforeOpen(DataSet: TDataSet);
begin
  taA2.Params[0].AsInteger:=dm.quArtD_ArtId.AsInteger;
  if WorkMode='SINV' then taA2.Params[1].AsInteger:=taSListSInvId.AsInteger
  else
  if WorkMode='APPL' then taA2.Params[1].AsInteger:=dmserv.taApplListAPPLID.AsInteger;
  taA2.Params[2].AsInteger:=dmCOm.UserId;
//  taA2.Params[3].AsInteger:=Integer(AllA2);
end;

procedure Tdm.taA2BeforeInsert(DataSet: TDataSet);
begin
  with dmCom do
    begin
      PostDataSets([quArt, taSList]);
      if WorkMode='SINV' then
        if taSListSupId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���������');
      if quArtD_ArtId.IsNull then SysUtils.Abort;
      taA2.First;
    end;
  
end;

procedure Tdm.taA2NewRecord(DataSet: TDataSet);
begin
  taA2Art2Id.AsInteger:=dmCom.GetId(24);
  taA2ArtId.AsInteger:=dm.quArtD_ArtId.AsInteger;
  if(WorkMode = 'SINV')then
  begin
   taA2SupId.AsInteger:=taSListSupId.AsInteger;
   taA2InvId.AsInteger:=taSListSInvId.AsInteger;
  end;
//  if taSListISTOLLING.AsInteger = 1 then
//    begin
//       try
//         taA2Price1.AsFloat := taTollingParamsAVERAGEPRICE.AsFloat * (1 +
//         + taTollingParamsTOTALLOSSESWEIGHT.AsFloat/(taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat));
//       except
//         On E:Exception do ShowMessage('������ ��� ���������� ��������� ������������ ����:' + E.Message);
//       end;
//    end
//  else
    taA2Price1.AsFloat:=0;

  taA2Price2.AsFloat:=0;
  taA2Art2.AsString:='-';
  taA2UseMargin.AsInteger:=1;
end;

procedure Tdm.taA2BeforeEdit(DataSet: TDataSet);
begin
  if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  PostDataSets([taSList, taSEl]);
end;

procedure Tdm.taA2PRICE1Change(Sender: TField);
var Margin: extended;
    RoundNorm: integer;
    p: extended;
begin

  if taSListISTOLLING.AsInteger = 1 then
    begin
      if taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat > 0 then
        try
          taA2TollingPrice.AsFloat := taA2Price1.AsFloat + taTollingParamsAVERAGEPRICE.AsFloat * (1 +
          + taTollingParamsTOTALLOSSESWEIGHT.AsFloat/(taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat));
        except
          On E:Exception do ShowMessage('������ ��� ���������� ��������� ������������ ����:' + E.Message);
        end
      else
        taA2TollingPrice.AsFloat := 0;
    end
  else
    taA2TollingPrice.AsCurrency := 0.00;


  if taA2UseMargin.AsInteger = 1 then
    begin
      GetMargin(taSListDepId.AsInteger, Margin, RoundNorm, taSListSInvId.AsInteger);
      p:=Power(10, RoundNorm);
//        taA2Price2.AsFloat:=FF(Ceil(taA2Price1.AsFloat * (1+Margin/100)*p)/p);
        taA2Price2.AsFloat:= Ceil(taA2Price1.AsFloat * (1+Margin/100)*p)/p;

      if taSListISTOLLING.AsInteger = 1 then
        taA2Price2.AsFloat:=FF(Ceil(taA2TollingPrice.AsFloat * (1+Margin/100)*p)/p);

    end;
//  else taA2Price2.AsFloat:=taA2Price1.AsFloat;
end;


procedure Tdm.taA2AfterPost(DataSet: TDataSet);
var Art2Id: integer;
begin
 dmCom.tr.CommitRetaining;

 A2Err:=false;
 {
  if (taA2Price1.AsFloat<>taA2Price1.OldValue) or
     (taA2Price2.AsFloat<>taA2Price2.OldValue) or
     (taA2Art2.AsString<>taA2Art2.OldValue) then
    begin
      RefreshAllSEl(taA2Art2Id.AsInteger);
    end;
  if A2Inserted then
   begin
     Art2Id:=taA2Art2Id.AsInteger;
     ReopenDataSets([taA2]);
     taA2.Locate('ART2ID', Art2Id, []);
   end;
 }

  if A2Inserted then
  begin
     Art2Id:=taA2Art2Id.AsInteger;
     ReopenDataSets([taA2]);
     taA2.Locate('ART2ID', Art2Id, []);
  end else

  begin
    RefreshAllSEl(taA2Art2Id.AsInteger);
  end;

   Screen.Cursor := crDefault;
end;

procedure Tdm.RefreshAllSEl(Art2Id: integer);
var i: integer;
begin
  i := 0;
  with taSEl do
    try
      DisableControls;
      i:=taSElSElId.AsInteger;
      First;
      while NOT EOF do
        begin
          if taSElArt2Id.AsInteger=Art2Id then taSEl.Refresh;
          Next;
        end;
    finally
      Locate('SELID', i, []);
      EnableControls;
    end;
end;

procedure Tdm.quPriceBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), dmCom do
    begin
//  if NOT Prepared then Prepare;
      try
        Params.ByName['USERID'].AsInteger:=UserId;
      except
      end;
      SetArtFilter(Params);

    end;
end;


procedure Tdm.quArtNewRecord(DataSet: TDataSet);
var UnitId: integer;
begin
  with dmCom do
    begin
      quArtD_ArtID.AsInteger:=GetID(5);
      if D_CompID<>-1 then quArtD_CompId.AsInteger:=D_CompId;
      if D_MatId<>'' then quArtD_MatId.AsString:=D_MatId;
      if D_GoodId<>'' then quArtD_GoodId.AsString:=D_GoodId;
      if D_CountryId<>'' then quArtD_CountryId.AsString:=D_CountryId;
      if D_InsId<>'' then quArtD_InsId.AsString:=D_InsId;
      if D_Att1Id=ATT1_DICT_ROOT then quArtATT1.AsInteger := -1
      else quArtATT1.AsInteger := D_Att1Id;
      if D_Att2Id=ATT2_DICT_ROOT then quArtATT2.AsInteger := -1
      else quArtATT2.AsInteger := D_Att2Id;
      with quTmp do
        begin
          SQL.Text:='SELECT UNITID FROM D_INS WHERE D_INSID='''+D_InsId+'''';
          ExecQuery;
          if NOT Fields[0].IsNull then UnitId:=Fields[0].AsInteger
          else
            begin
              Close;
              SQL.Text:='SELECT UNITID FROM D_MAT WHERE D_MATID='''+D_MATId+'''';
              ExecQuery;
              if NOT Fields[0].IsNull then UnitId:=Fields[0].AsInteger
              else UnitId:=1;
            end;
          Close;
        end;
      quArtUnitId.AsInteger:=UnitId;
    end;
end;

procedure Tdm.quArtBeforePost(DataSet: TDataSet);
var i: integer;
    s,s2: string;
begin
  if not CheckForSpecSymb(quArtArt.AsString) then  begin
     dm.quArt.Delete;
     raise Exception.Create('������� �� ����� ���������� �� ����. ��������');
     end;

  if quArt.State=dsInsert then
    begin
      with dm2.quCheckAExist do
        begin
          s2:=quArtArt.AsString;
          Params[0].AsString:=s2;
          ExecQuery;
          i:=Fields[0].AsInteger;
          Close;
          if i>0 then
            with dm2.quGetA do
              begin
                Active:=False;
                Params[0].AsString:=quArtArt.AsString;
                Active:=True;
                s:= '';
                while not EOF do
                begin
                  s:=s + Fields[0].AsString + ' ; ';
                  Next;
                end;
                First;
                //i:=Fields[1].AsInteger;
                case MessageDialog('�������  ��� ����������.'+#13#10+s+#13#10+'����������?', mtConfirmation, [mbYes, mbNo, mbIgnore], 0) of
                mrNo:
                  begin
                    DataSet.Cancel;
                    SysUtils.Abort;
                  end;
                mrIgnore: begin
                            DataSet.Cancel;
//                            quArt.Edit;
                            ArtMode:=1;
                            Art:=s2;
                            ReOpenDataSets([quArt]);
//                            quArt.Locate('D_ARTID', I, []);
                            IF WorkMode='SINV' then fmSInv.miAEditClick(DataSet)
                            else if WorkMode = 'APPL' then fmAppl.miAEdit
                            else  fmArtDict.miAEditClick(DataSet);
                            ArtMode:=0;
                            Art:='';
                            i:=quArt.FieldByName('D_Artid').AsInteger;
                            ReOpenDataSets([quArt]);
                            quArt.Locate('D_ARTID', I, []);
                            SysUtils.Abort;
                          end;
                else end;
                Active:=False;
            end;
        end;
      InsertedArt:=quArtArt.AsString;
      InsertedArtId:=quArtD_ArtId.AsInteger;
    end
  else InsertedArtId:=-1;

  if quArtD_COMPId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������������');
  if quArtD_MATId.IsNull  then raise Exception.Create('���������� ������ �������� � ���� ��������');
  if quArtD_GOODId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������������');
  if quArtD_InsId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������� �������');
  if quArtArt.AsString='' then raise Exception.Create('���������� ������ �������� � ���� �������');
end;

procedure Tdm.quArtBeforeInsert(DataSet: TDataSet);
begin
  with dmCom do
    begin
      if D_CompId=-1 then raise Exception.Create('���������� ������� �������������');
      if D_MatId='*' then raise Exception.Create('���������� ������� ��������');
      if D_GoodId='*' then raise Exception.Create('���������� ������� ��� ������');
      if D_InsId='*' then raise Exception.Create('���������� ������� �������� �������');
      if D_CountryId='*' then raise Exception.Create('���������� ������� ������');
    end;
  NewArt:=quArtArt.AsString;
  NewUnitId:=quArtUnitId.AsInteger;
end;

procedure Tdm.quPriceBeforePost(DataSet: TDataSet);
var
    i: integer;
    q: TpFIBQuery;
begin
  q := nil;
  with quPrice, dm2 do
    for i:=0 to Fields.Count-1 do
      if (Fields[i].FieldName[1] in ['P', 'O']) and (Fields[i].AsFloat<>Fields[i].OldValue) then
        begin
          if Fields[i].AsFloat < 0 then raise Exception.Create('���� ������ ���� ������ ��� ����� ����');
          case Fields[i].FieldName[1] of
              'P':  begin
                      q:=quInsTmpP2;
                      q.Params.ByName['DEPID'].AsInteger:=quPrice.Fields[i].Tag;
                    end;
              'O':  q:=quInsTmpP2OP;
            end;
          with q do
            begin
              ParamByName('ART2ID').AsInteger:=quPriceArt2Id.AsInteger;
              ParamByName('PRICE').AsFloat:=quPrice.Fields[i].AsFloat;
            ExecQuery;
            end;
        end
end;

procedure Tdm.quDPriceBeforeOpen(DataSet: TDataSet);
var DID: integer;
begin
  DID := 0;
  with TpFIBDataSet(DataSet), dmCom do
    begin
      SetArtFilter(Params);
      if WorkMode='OPT' then DID:=taOptListDepFromId.AsInteger
      else if WorkMode='PRICE' then DID:=DPriceDepId
           else if (WorkMode='SELL') or (WorkMode = 'SELLCH') then DID:=taCurSellDepId.AsInteger
                else if WorkMode='ALLSELL' then DID:=taSellListDepId.AsInteger
                     else if WorkMode='SRET' then DID:=taSRetListDepFromId.AsInteger
                          else if WorkMode='PHIST' then DID:=SDepId;
      ParamByName('DEPID').AsInteger:=DID;
    end;
end;

procedure Tdm.taA2USEMARGINChange(Sender: TField);
var Margin: extended;
    RoundNorm: integer;
    p: extended;
begin
  if taA2UseMargin.AsInteger=1 then
    begin
      GetMargin(taSListDepId.AsInteger, Margin, RoundNorm, taSListSInvId.AsInteger);
      p:=Power(10, RoundNorm);
      taA2Price2.AsFloat:=FF(Ceil(taA2Price1.AsFloat*(1+Margin/100)*p)/p);
    end
  else taA2Price2.AsFloat:=FF(dm2.GetPrice(taSListDepId.AsInteger, taA2Art2Id.AsInteger));
end;

procedure Tdm.taAnlzSell_daysBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
{      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;  }
      ByName['ADATE'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['BDATE'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
// taAnlzSell_days.ParamByName('ADATE').AsDate:=dm.BeginDate;
 //taAnlzSell_days.ParamByName('BDATE').AsDate:=dm.EndDate;
 taAnlzSell_days.ParamByName('DEP').Asinteger:=dm.SDepId;
end;

procedure Tdm.taSElUSEMARGINChange(Sender: TField);
var Margin: extended;
    RoundNorm: integer;
    p: extended;
begin
  if WorkMode='SINV' then
    if taSElUseMargin.AsInteger=1 then
      begin
        GetMargin(taSListDepId.AsInteger, Margin, RoundNorm, taSListSInvId.AsInteger);
        p:=Power(10, RoundNorm);

        taSElPrice2.AsFloat:=Ceil(taSElPrice.AsFloat*(1+Margin/100)*p)/p;
      end
    else taSElPrice2.AsFloat:=dm2.GetPrice(taSListDepId.AsInteger, taSElArt2Id.AsInteger);;
end;

procedure Tdm.quArtBeforeOpen(DataSet: TDataSet);
var s: string;
begin
  if (Length(Art)>0) and (Art[1]='*') then
    begin
      quArt.SelectSQL[3]:='AR_S';
      s:=ReverseString(Copy(Art, 2, MAXINT));
    end
    else
    begin
      quArt.SelectSQL[3]:='A_S';
      s:=Art;
    end;


    quArt.Prepare;

    quArt.ParamByName('USERID').AsInteger:=dmCom.UserId;
    quArt.ParamByName('DEPID').Asinteger:= taSListDepId.Asinteger;
    if ArtMode=1 then SetAFilter2(quArt.Params)
    else SetAFilter(quArt.Params);
    quArt.ParamByName('ART_1').AsString:=s;
    quArt.ParamByName('ART_2').AsString:=s+'��';
end;

procedure Tdm.SetAFilter(Params: TFIBXSQLDA);
begin

  with dmCom,Params do
    begin
      if D_COMPID=-1 then
        begin
          ByName['COMPID1'].AsInteger:=-MAXINT;
          ByName['COMPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['COMPID1'].AsInteger:=D_COMPID;
          ByName['COMPID2'].AsInteger:=D_COMPID;
        end;

      if D_MATID='*' then
        begin
          ByName['MATID1'].AsString:='!';
          ByName['MATID2'].AsString:='��';
        end
      else
        begin
          ByName['MATID1'].AsString:=D_MATID;
          ByName['MATID2'].AsString:=D_MATID;
        end;

      if D_GOODID='*' then
        begin
          ByName['GOODID1'].AsString:='!';
          ByName['GOODID2'].AsString:='��';
        end
      else
        begin
          ByName['GOODID1'].AsString:=D_GOODID;
          ByName['GOODID2'].AsString:=D_GOODID;
        end;

      if D_INSID='*' then
        begin
          ByName['INSID1'].AsString:='!';
          ByName['INSID2'].AsString:='��';
        end
      else
        begin
          ByName['INSID1'].AsString:=D_INSID;
          ByName['INSID2'].AsString:=D_INSID;
        end;
      if D_COUNTRYID='*' then
        try
          ByName['COUNTRYID1'].AsString:='!';
          ByName['COUNTRYID2'].AsString:='��';
        except
        end
      else
        try
          ByName['COUNTRYID1'].AsString:=D_CountryID;
          ByName['COUNTRYID2'].AsString:=D_COUNTRYID;
        except
        end;
        
      if D_Att1Id = ATT1_DICT_ROOT then
        try
          ByName['ATT1_1'].AsInteger := -MAXINT;
          ByName['ATT1_2'].AsInteger := MAXINT;
        except
        end
      else
        try
          ByName['ATT1_1'].AsInteger := D_Att1Id;
          ByName['ATT1_2'].AsInteger := D_Att1Id;
        except
        end;
      if D_Att2Id = ATT1_DICT_ROOT then
        try
          ByName['ATT2_1'].AsInteger := -MAXINT;
          ByName['ATT2_2'].AsInteger := MAXINT;
        except
        end
      else
        try
          ByName['ATT2_1'].AsInteger := D_Att2Id;
          ByName['ATT2_2'].AsInteger := D_Att2Id;
        except
        end;
    end
end;

procedure Tdm.SetAFilter2(Params: TFIBXSQLDA);
begin
  with Params do
    begin
       try
          ByName['COMPID1'].AsInteger:=dm2.quGetA.FieldByName('D_COMPID').AsInteger;
          ByName['COMPID2'].AsInteger:=dm2.quGetA.FieldByName('D_COMPID').AsInteger;
        except
        end;
        try
          ByName['MATID1'].AsString:=dm2.quGetA.FieldByName('D_MatID').AsString ;
          ByName['MATID2'].AsString:=dm2.quGetA.FieldByName('D_MatID').AsString ;
        except
        end;
        try
          ByName['GOODID1'].AsString:=dm2.quGetA.FieldByName('D_GOODID').AsString ;
          ByName['GOODID2'].AsString:=dm2.quGetA.FieldByName('D_GOODID').AsString ;
        except
        end;
        try
          ByName['INSID1'].AsString:=dm2.quGetA.FieldByName('D_INSID').AsString ;
          ByName['INSID2'].AsString:=dm2.quGetA.FieldByName('D_INSID').AsString ;
        except
        end;
        try
          ByName['COUNTRYID1'].AsString:=dm2.quGetA.FieldByName('D_COUNTRYID').AsString ;
          ByName['COUNTRYID2'].AsString:=dm2.quGetA.FieldByName('D_COUNTRYID').AsString;
        except
        end;
        try
          ByName['ATT1_1'].AsInteger := dm2.quGetAATT1.AsInteger;
          ByName['ATT1_2'].AsInteger := dm2.quGetAATT1.AsInteger;
        except
        end;
        try
          ByName['ATT2_1'].AsInteger := dm2.quGetAATT2.AsInteger;
          ByName['ATT2_2'].AsInteger := dm2.quGetAATT2.AsInteger;
        except
        end;
    end
end;

procedure Tdm.quARestBeforeOpen(DataSet: TDataSet);
begin
  with quARest do
    begin
      Params[0].AsInteger := D_ARTID;
    end;
end;

procedure Tdm.quA2RestBeforeOpen(DataSet: TDataSet);
begin
  with quA2Rest do
    begin
      Params[0].AsInteger:=ART2ID;
    end;
end;

procedure Tdm.quEmpBeforeOpen(DataSet: TDataSet);
begin
  with quEmp do
    begin
      Params[0].AsInteger:=EmpDepId;
    end;
end;

procedure Tdm.taSellItemCalcFields(DataSet: TDataSet);
var i: integer;
    Card: string;
    s, sr,sDA, sAp, qs, ws, qsr, wsr, qsD_Art, wsD_Art, qappl: string;
begin
  case taSellItemUnitId.AsInteger of
      0: begin
//          taSellItemCost.AsFloat:=taSellItemPrice.AsFloat;
//          taSellItemCost0.AsFloat:=taSellItemPrice0.AsFloat;
          taSellItemDisc.AsFloat:=taSellItemPrice0.AsFloat - taSellItemPrice.AsFloat;
         end;
      1: begin
//          taSellItemCost.AsFloat:=taSellItemPrice.AsFloat*taSellItemW.AsFloat;
//          taSellItemCost0.AsFloat:=taSellItemPrice0.AsFloat*taSellItemW.AsFloat;
          taSellItemDisc.AsFloat:=taSellItemPrice0.AsFloat*taSellItemW.AsFloat - taSellItemCost.AsFloat;
         end;
    end;
  case taSellItemRet.AsInteger of
      0: taSellItemOp.AsString:='�������';
      1: taSellItemOp.AsString:='�������';
    end;

  qs:=taSellItemSQ.AsString;
  ws:=ReplaceStr(taSellItemSW.AsString, '.', DecimalSeparator);

  qsr:=taSellItemSQR.AsString;
  wsr:=ReplaceStr(taSellItemSWR.AsString, '.', DecimalSeparator);

  qsD_Art:=taSellItemSQ_DART.AsString;
  wsD_Art:=ReplaceStr(taSellItemSW_DART.AsString, '.', DecimalSeparator);

  qappl:=taSellItemSQAPPL.AsString;
  // ???
  with dm2 do
    for i:=0 to slDepDepId.Count-1 do
      begin
        s:=ExtractWord(i+1, ws, [';']);
        sr:=ExtractWord(i+1, wsr, [';']);
        sDA :=ExtractWord(i+1, wsD_Art, [';']);
        sAp:=ExtractWord(i+1, qAppl, [';']);

        if Centerdep then
         DataSet.FieldByName('APPLQ_'+slDepDepId[i]).AsFloat := StrToFloat(sAp);

        if  StrToInt(slDepDepId[i])<>SelfDepId then
          begin
            if s='' then DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=0
            else DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(s);
          end
        else
          begin
            if sr='' then DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=0
            else DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(sr);
          end;

        if sDA='' then DataSet.FieldByName('RWA_'+slDepDepId[i]).AsFloat:=0
        else DataSet.FieldByName('RWA_'+slDepDepId[i]).AsFloat:=StrToFloat(sDA);



        s:=ExtractWord(i+1, qs, [';']);
        sr:=ExtractWord(i+1, qsr, [';']);
        sDA:=ExtractWord(i+1, qsD_Art, [';']);

        if  StrToInt(slDepDepId[i])<>SelfDepId then
          begin
            if s='' then DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=0
            else DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(s);
          end
        else
          begin
            if sr='' then DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=0
            else DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(sr);
          end;

        if sDA='' then DataSet.FieldByName('RQA_'+slDepDepId[i]).AsInteger:=0
        else DataSet.FieldByName('RQA_'+slDepDepId[i]).AsInteger:=StrToInt(sDA);
      end;
  // end ???

  Card := Trim(taSellItemNODCARD.AsString);

  if (Card <> '') and (Card[1] = '-') then
  begin
    Card := '';
  end;

  taSellItemCLIENTCARD.AsString := Card;
  
end;

procedure Tdm.taCurSellBeforeOpen(DataSet: TDataSet);
begin
  taCurSell.Params[0].AsInteger:=SDepId;
  taCurSell.Params[1].AsInteger:=dmcom.User.UserId;
end;

procedure Tdm.taCurSellNewRecord(DataSet: TDataSet);
begin
  taCurSellSellId.AsInteger:=dmCom.GetID(21);
  taCurSellDepId.AsInteger:=SDepId;
  taCurSellClosed.AsInteger:=0;
end;

procedure Tdm.taA2BeforePost(DataSet: TDataSet);
var i: integer;
begin
  if A2Err then
  begin
    fmSInv.ActiveControl:= fmSInv.dgA2;
    taA2.Cancel;
    A2Err:=false;
    sysUtils.Abort
  end;

  if not CheckForSpecSymb(taA2Art2.AsString) then
    begin
//      dm.taA2.Delete;
      raise Exception.Create('������� 2 �� ����� ���������� �� ����. ��������');
    end;

  A2Err:=true;
  A2Inserted:=taA2.State=dsInsert;
  if A2Inserted then
  with dm2.quCheckA2Exist, Params do
  begin
    ByName['ART2'].AsString:=taA2Art2.AsString;
    ByName['D_ARTID'].AsInteger:=taA2ArtID.AsInteger;
    ExecQuery;
    i:=Fields[0].AsInteger;
    Close;
    if i<>0 then if MessageDialog('������� 2 ��� ����������. ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  end;

  PostDataSets([taSEl]);
  if taA2Art2.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������� 2');
  if taA2Price1.IsNull then raise Exception.Create('���������� ������ �������� � ���� ��.����');
  if taA2Price2.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���.����');
  if taA2Price1.AsFloat<0 then raise Exception.Create('��.���� ������ ���� ������ ��� ����� ����');
  if taA2Price2.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
    if not (taA2.FieldByName('Art2').OldValue=null) and (taA2.FieldByName('Art2').OldValue <> taA2.FieldByName('Art2').NewValue ) then
  begin
  if MessageDialog('������� 2 ��� �������. ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then
    taA2.FieldByName('Art2').newValue:= taA2.FieldByName('Art2').OldValue;
  end;

  if not A2Inserted then
  begin
    if (VarIsNull(taA2Price1.OldValue) and not VarIsNull(taA2Price1.NewValue)) or
       (not VarIsNull(taA2Price1.OldValue) and VarIsNull(taA2Price1.NewValue)) or (taA2Price1.OldValue <> taA2Price1.NewValue) then
    begin
      if (taA2W.AsFloat = 0) then
      begin
        if MessageDialog('��������� ���� ��������. ����������?', mtConfirmation, [mbYes, mbNo], 0) <> mrYes then
        begin
          SysUtils.Abort;
        end;
      end;
    end;
  end;

  Screen.Cursor := crSQLWait;
end;

procedure Tdm.taA2CalcFields(DataSet: TDataSet);
var Price1: Double;
begin

  if taSListISTOLLING.AsInteger = 1 then
    begin

      if taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat > 0 then
        try
          taA2TollingPrice.AsFloat := taA2Price1.AsFloat + taTollingParamsAVERAGEPRICE.AsFloat * (1 +
          + taTollingParamsTOTALLOSSESWEIGHT.AsFloat/(taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat));
        except
          On E:Exception do ShowMessage('������ ��� ���������� ��������� ������������ ����:' + E.Message);
        end
      else
        taA2TollingPrice.AsFloat := 0;
    end
  else
    taA2TollingPrice.AsCurrency := 0.00;
end;

procedure Tdm.quDistrPriceBeforeOpen(DataSet: TDataSet);
var i: integer;
begin
  with quDistrPrice, Params do
    for i:=0 to Count-1 do
      if Odd(i) then Params[i].AsInteger:=dmCom.UserId
      else Params[i].AsInteger:=quD_WHArt2Id.AsInteger;
end;

procedure Tdm.quDistrPriceBeforePost(DataSet: TDataSet);
var i: integer;
begin
  with quDistrPrice do
    for i:=0 to Fields.Count-1 do
      if Fields[i].FieldName[1]='P' then
         if Fields[i].AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');

  with quDistrPrice do
    for i:=0 to Fields.Count-1 do
      if Fields[i].FieldName[1]='P' then
        if Fields[i].AsFloat<>Fields[i].OldValue then
          with dm2, quUpdateDistrPrice, Params do
            begin
              ByName['Art2Id'].AsInteger:=quDistrPrice.Params[0].AsInteger;
              ByName['USERID'].AsInteger:=dmCom.UserId;
              ByName['DEPID'].AsInteger:=quDistrPrice.Fields[i].Tag;
              ByName['PRICE2'].AsFloat:=quDistrPrice.Fields[i].AsFloat;
              ExecQuery;
            end;
end;

procedure Tdm.quDistrPriceBeforeEdit(DataSet: TDataSet);
begin
{  if quDistrPriceDepId.AsInteger=DDepFromId then SysUtils.Abort;}
end;

procedure Tdm.UNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure Tdm.UNITIDSetText(Sender: TField; const Text: String);
begin
  if Text='��' then Sender.AsInteger:=0
  else  Sender.AsInteger:=1;
end;

procedure Tdm.YesNoGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='���';
           1: Text:='��';
         end;
end;

procedure Tdm.YesNoSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.AsInteger:=0
  else  Sender.AsInteger:=1;
end;

procedure Tdm.taSetPriceBeforePost(DataSet: TDataSet);
begin
  if taSetPriceOptPrice.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���.����');
  if taSetPricePrice2.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���.����');
  if taSetPriceOptPrice.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
  if taSetPricePrice2.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
end;

procedure Tdm.taPrOrdBeforeDelete(DataSet: TDataSet);
begin
  if dm.taPrOrdIsSet.AsInteger=1 then raise Exception.Create('���������� ������� ������������� ������');
  BeforeDelete(DataSet);
end;

procedure Tdm.taPrOrdBeforeEdit(DataSet: TDataSet);
begin
   CheckClosedPr;
end;

procedure Tdm.CheckClosedPr;
begin
  if dm.taPrOrdIsClosed.AsInteger=1 then raise Exception.Create('���������� ������������� �������� ������');
end;

procedure Tdm.taPrOrdItemBeforeEdit(DataSet: TDataSet);
begin
  CheckClosedPr;
end;

procedure Tdm.taPrOrdItemBeforeDelete(DataSet: TDataSet);
begin
  CheckClosedPr;
  DelConf(DataSet);
end;

procedure Tdm.taPrOrdItemBeforePost(DataSet: TDataSet);
begin
  if taPrOrdItemPrice.AsFloat<0 then raise Exception.Create('��.���� ������ ���� ������ ��� ����� ����');
  if taPrOrdItemPrice2.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
  if taPrOrdItemOptPrice.AsFloat<0 then raise Exception.Create('���.���� ������ ���� ������ ��� ����� ����');
end;

procedure Tdm.OptDepClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taOptList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    DDepFromId:= CenterDepId;
    DDepFrom:= dmCom.Dep[CenterDepId].SName;
    fmOptList.laDepFrom.Caption:=DDepFrom;
    taOptList.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure Tdm.taOptListNewRecord(DataSet: TDataSet);
begin
  taOptListSInvId.AsInteger:=dmCom.GetId(8);
  if DDepFromId<>-1 then taOptListDepFromId.AsInteger:=DDepFromId;
  taOptListSDate.AsDateTime:= dmCom.GetServerTime;
  taOptListNDS.AsFloat:=0;
  taOptListTr.AsFloat:=0;
  taOptListPMargin.AsFloat:=0;
  taOptListCost.AsFloat:=0;
  taOptListAkciz.AsFloat:=dmCom.taRecAkciz.AsFloat;
  taOptListIsClosed.AsInteger:=0;
  taOptListDepFrom.AsString:=DDepFrom;
  taOptListSupId.AsInteger:=dmCom.taRecD_CompId.AsInteger;

  with quTmp do
    begin
      SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=3 AND DEPFROMID='+IntToStr(DDepFromId)+' AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39'))';
      ExecQuery;
      if Fields[0].IsNull then       taOptListSN.AsInteger:=1
      else      taOptListSN.AsInteger:=Fields[0].AsInteger+1;
      Close;
    end;
  taOptListOpt.AsInteger:=1;
  taOptListIType.AsInteger:=3 ;
end;

procedure Tdm.taOptListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if DDepFromId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=DDepFromId;
          ByName['DEPID2'].AsInteger:=DDepFromId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
end;

procedure Tdm.taDItemBeforePost(DataSet: TDataSet);
begin
  if taSElUnitId.AsInteger=1 then
    if taDItemW.AsFloat=0 then raise Exception.Create('���������� ������ ���');
  if (WorkMode='OPT') and not SellMode then
    if fmOptItem<>NIL then fmOptItem.UpdateQW;
end;

procedure Tdm.DataModuleDestroy(Sender: TObject);
begin
  dmCom.fsCom.WriteInteger('DISTRW', DistrW);
//  dmCom.fsCom.WriteString('DSTSORT', quDst.SelectSQL[8]);
  taSEl.UnPrepare;
  quD_WH.UnPrepare;
  slProd.Free;
  slMat.Free;
  slGrMat.Free;
  slGood.Free;
  slIns.Free;
  slSup.Free;
  slCountry.Free;
  slNote1.Free;
  slNote2.Free;
  slAtt1.Free;
  slAtt2.Free;
  slDep.Free;
  slSZ.Free;

  ExecSelectSQL('execute procedure Get_CurrCon ('+inttostr(dmcom.UserId)+', 1)',dmcom.qutmp);
end;

procedure Tdm.taPrOrdCalcFields(DataSet: TDataSet);
var s: string;
begin
  s:=taPrOrdSN.AsString;
  case taPrOrdIType.AsInteger of
      1: s:=s+' ��';
      2: s:=s+' ��';
      5: s:=taPrOrdSSF.AsString+' ���';
      6: s:=s+' �.����� ';
      7: s:=s+' �� (���. ���)';      
      9: s:='���. ���';
     10: s:='�����������';
     11: s:='��������� ��.';
     12: s:='��������� ���.';
     13: s:='����. ���. ���. �� ������';
     14: s:='��������� ���. ���.';
     16: s:=s+' ���.'
    end;
  taPrOrdSNStr.AsString:=s;
end;

procedure Tdm.taPrOrdDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if Pos('FK_PRORDITEM_2', E.Message)<>0 then
  begin
    Action := daAbort;
    MessageDialog('������ ������� �� ������ ������.', mtError, [mbOK], 0);
  end;
end;

procedure Tdm.taSItemBeforePost(DataSet: TDataSet);
var i: integer;
    s: string[16];
begin
  if not CheckForUIDWHCalc then
    raise Exception.Create('�������������� �� ��������. ���� �������� ������. ��� ������ ������� Esc');
  if taSItemW.NewValue <> taSItemW.OldValue  then
  begin
    if not CheckForWOrder(taSItemSITEMID.AsInteger) then
      raise Exception.Create('�������������� �� ��������, �.�. ���. ������ �� ��������� ���� �� ������. ������� ������� � ����� �������� ��� � ���������� �����');
  end;
  if taSItemUID.IsNull then raise Exception.Create('���������� ������ �������� � ���� ��. �����');
  if taSItemW.IsNull then raise exception.create('���������� ������ �������� � ���� ���');
  if taSElUnitId.AsInteger=1 then
    if taSItemW.AsFloat=0  then raise exception.create('���������� ������ �������� � ���� ���');
  s:=taSItemSS.AsString;
  for i:=1 to Length(s) do
    if s[i]='.' then s[i]:=',';
  taSItemSS.AsString:=s;
end;

procedure Tdm.OpenSell;
var b: boolean;
    n,i: integer;
    D: TDateTime;
begin
  b := False;
//����
//if dm.quEmpD_EmpId.IsNull then raise Exception.Create('���������� ������ ��������� � ���������� �����������');
  try
    fmOpenSell := TfmOpenSell.Create(NIL);
    with fmOpenSell do
      begin
        b:=ShowModal=mrOK;
        if b then
          begin
            // basile
            //D := Now;
            { TODO : ���������������� }
            D:=dmCom.GetServerTime;
//Tanay
            UseFReg:=false;
            IF dmcom.FReg_ and (not dmfr.FrReady) then raise Exception.Create('����� �� ����� ���� ������� � ���������� �������������. ���������� ����������� �� �����.')
            else if dmcom.FReg_ then UseFReg:=true;
//////
            if UseFReg then
             with dmFR do
              begin
               fmSplashFR.Show;
               if not com.Connected then com.Connect;
               //������������� ����-�����
               FillChar(OutBuf, SizeOf(OutBuf), 0);
               OutBuf[8]:=Ord(DateToStr(D)[1]);

               OutBuf[9]:=Ord(DateToStr(D)[2]);
               OutBuf[10]:=Ord(DateToStr(D)[4]);
               OutBuf[11]:=Ord(DateToStr(D)[5]);
               OutBuf[12]:=Ord(DateToStr(D)[7]);
               OutBuf[13]:=Ord(DateToStr(D)[8]);
               OutBuf[14]:=Ord(DateToStr(D)[9]);
               OutBuf[15]:=Ord(DateToStr(D)[10]);

               OutBuf[17]:=Ord(TimeToStr(D)[1]);
               OutBuf[18]:=Ord(TimeToStr(D)[2]);
               OutBuf[19]:=Ord(TimeToStr(D)[4]);
               OutBuf[20]:=Ord(TimeToStr(D)[5]);
               SendPacket($47, 24, OutBuf);
               if (Status > 0) then
                begin
                 MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
                 fmSplashFR.Close;
                 exit;
                end;
               //�������� ����� �����
               FillChar(OutBuf, SizeOf(OutBuf), 0);
               OutBuf[8]:=$30;
               SendPacket($44, 12, OutBuf);
               if (Status > 0) then
                begin
                 MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
                 fmSplashFR.Close;
                 exit;
                end;
               n:=StrToInt(chr(Buf[32])+chr(Buf[33])+chr(Buf[34])+chr(Buf[35])+chr(Buf[36])) + 1;
               //��������� �����
               FillChar(OutBuf, SizeOf(OutBuf), 0);
               OutBuf[8]:=Ord(IntToStr(dmcom.User.UserId)[1]);
               OutBuf[9]:=Ord(IntToStr(dmcom.User.UserId)[2]);
               for i:=1 to Length(dmcom.User.FIO) do
                OutBuf[10+i]:=Ord(StrToOem(dmcom.User.FIO)[i]);
               SendPacket($31, 53, OutBuf);
               fmSplashFR.Close;
               if (Status > 0) then
                begin
                 MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
                 exit;
                end;
              end
            else
              begin
                n:=Round(se1.Value);
              end;

              {�������������}
            with taCurSell do
              begin
                Insert;
                taCurSellBD.AsDateTime:=D;
                taCurSellN.AsInteger:=n;
                taCurSellEmp1Id.AsInteger:=dmcom.User.UserId; // lc1.KeyValue;
                if UseFReg then taCurSellFREG.AsInteger:=1
                           else taCurSellFREG.AsInteger:=0;
                Post;
              end;
            dmCom.tr.CommitRetaining;
            {����������� ���������� ��������}
            ReOpenDataSets([dm2.quClient]);

            with qutmp do
            begin
             sql.Text := 'execute procedure opensell_ ('+inttostr(taCurSellSELLID.AsInteger)+')';
             ExecQuery;
             dmcom.tr.CommitRetaining;
             close;
            end;
            with taSellItem do
              begin
                Active:=False;
                Open;
                D_CHECK0:=0;
                ReOpenDataSets([quCurSellCheck]);
              end;
          end;
      end
  finally
    fmOpenSell.Free;
    fmSellItem.ShowSell;
    if not b then SysUtils.Abort;
  end;
end;


procedure Tdm.CloseSell;
begin

  if taCurSellSellId.IsNull then exit;
  try
   if dm.UseFReg  then
     with dmFR do
      begin
       fmSplashFR.Show;
       if not com.Connected then com.Connect;
     //��������� �����
       FillChar(OutBuf, SizeOf(OutBuf), 0);
       OutBuf[8]:=$32;
       SendPacket($35, 12, OutBuf);
       fmSplashFR.Close;
       if (Status > 0) and (Status <> 12) then
        begin
        MessageDialog(ErrStr[Status], mtError, [mbOk], 0);
       exit;
      end;
     com.Disconnect;
    end;

    with qutmp do
    begin
     sql.Text := 'execute procedure closesell_ ('+inttostr(taCurSellSELLID.AsInteger)+')';
     ExecQuery;
     dmcom.tr.CommitRetaining;
     close;
    end;

   with taCurSell do
    begin
//     if not (State in [dsInsert, dsEdit]) then Edit;
//     taCurSellED.AsDateTime:=dmCom.GetServerTime;
//     taCurSellClosed.AsInteger:=1;
//     Post;
//     Active:=False;
//     Open;
    end;
//   with taSellItem do
//    begin
//     Active:=False;
//     Open;
//    end;
  finally
   fmSellItem.ShowSell;
//   if UseFreg  then raise Exception.Create('����� �� ����� ���� �������. ���������� �� ����� � ������.');
  end;
end;

procedure Tdm.SetDepFilter(Params: TFIBXSQLDA; DepId: integer);
begin
  with Params do
   try
    if DepId=-1 then
      begin
        ByName['DEPID1'].AsInteger:=-MAXINT;
        ByName['DEPID2'].AsInteger:=MAXINT;
      end
    else
      begin
        ByName['DEPID1'].AsInteger:=DepId;
        ByName['DEPID2'].AsInteger:=DepId;
      end
   except
   end;
end;



var DiscReEnter: boolean;

procedure Tdm.taSellItemDISCOUNTChange(Sender: TField);
begin
  if DiscReEnter then Exit;
  DiscReEnter:=True;
  taSellItemPrice.AsFloat:=taSellItemPrice0.AsFloat*(1-Sender.AsFloat/100);
  DiscReEnter:=False;
end;

procedure Tdm.taSellItemPRICEChange(Sender: TField);
begin
{  if DiscReEnter then Exit;
  DiscReEnter:=True;
  if taSellItemPrice0.AsFloat=0 then taSellItemDiscount.AsFloat:=0
  else taSellItemDiscount.AsFloat:=(taSellItemPrice0.AsFloat-taSellItemPrice.AsFloat)/taSellItemPrice0.AsFloat*100;
  DiscReEnter:=False;}
end;

procedure Tdm.ClickUserDepMenu(pm: TPopupMenu);
var i: integer;
begin
{� ������� �������� ���� UserAllWh ���� ��� ����������, �� ���������� �� �����, ���������
 ��� ������� ������������}
  if CenterDep then pm.Items[0].Click
  else
  begin
    with pm do
     for i:=0 to Items.Count-1 do
      if Items[i].Tag=SelfDepId then
       begin
        Items[i].Click;
        exit;
       end
  end
end;

procedure Tdm.LoadArtSL(Dict: integer);
var nd: TNodeData;
    s: string[30];

  procedure LoadSL(sl: TStringList; SQLText: string; AllCode: variant);
  begin
    with quTmp, sl do
      begin
        sl.Clear;
        nd:=TNodeData.Create;
        nd.Name:='*���';
        nd.Code:=AllCode;
        AddObject('*���', nd);
        SQL.Text:=SQLText;
          ExecQuery;
          while NOT EOF do
            begin
              nd:=TNodeData.Create;
              s:=DelRSpace(Fields[1].AsString);
{              if IsThird then
                 if not Fields[2].IsNull then
                    s := s + '---' + trim(Fields[2].AsString) ;}
              nd.Name:=s;
              nd.Code:=DelRSpace(Fields[0].AsString);
              sl.AddObject(s, nd);
              Next;
            end;
          Close;
        end;
  end;
begin
//  dmCom.tr.Active:=True;
  if Dict AND COMP_DICT <>0 then
    begin
      slProd.Clear;
 //     LoadSL(slProd, 'SELECT D_COMPID, SNAME FROM D_COMP WHERE PRODUCER=1 ORDER BY SORTIND', -1);
      LoadSL(slProd, 'SELECT D_COMPID, SNAME FROM D_COMP WHERE PRODUCER>=1 and WORKPROD=1 ORDER BY SNAME', -1);
    end;
  if Dict AND MAT_DICT <>0 then
    begin
      slMat.Clear;
      LoadSL(slMat,  'SELECT D_MATID, NAME FROM D_MAT WHERE D_MATID <> '#39+'-1000'+#39' ORDER BY SORTIND', '*');
      slGrMat.Clear;
      LoadSL(slGrMat,  'SELECT distinct gr, gr as name FROM D_MAT WHERE D_MATID <> '#39+'-1000'+#39, '*');
    end;

  if Dict AND GOOD_DICT <>0 then
    begin
      slGood.Clear;
      LoadSL(slGood, 'SELECT D_GOODID, NAME FROM D_GOOD where D_GOODID<>'#39+'-1000'+#39' ORDER BY SORTIND', '*');
    end;

  if Dict AND INS_DICT <> 0then
    begin
      slIns.Clear;
      LoadSL(slIns,  'SELECT D_INSID, NAME FROM D_INS WHERE D_INSID<>'#39+'-1000'+#39' ORDER BY SORTIND', '*');
      //LoadSL(slIns,  'SELECT D_INSID, NAME FROM D_INS ORDER BY SORTIND', '*');
    end;

  if Dict and COMP_DICT <> 0 then
  begin
    slSup.Clear;
//    LoadSL(slSup, 'SELECT  D_COMPID, SNAME FROM D_COMP WHERE SELLER=1 ORDER BY SORTIND', -1);
    LoadSL(slSup, 'SELECT  D_COMPID, SNAME FROM D_COMP WHERE SELLER>=1 and WORKORG=1 ORDER BY SNAME', -1);
  end;
//  dmCom.tr.Commit;
  if Dict AND COUNTRY_DICT <>0 then
    begin
      slCountry.Clear;
      //LoadSL(slCountry, 'SELECT D_COUNTRYID, NAME FROM D_COUNTRY ORDER BY SORTIND', '*');
      LoadSL(slCountry, 'SELECT D_COUNTRYID, NAME FROM D_COUNTRY WHERE D_COUNTRYID<>'#39+'-1000'+#39' ORDER BY SORTIND', '*');
    end;

  if Dict and GOODSSAM1_DICT <> 0 then
  begin
    slNote1.Clear;
//    LoadSL(slSup, 'SELECT  D_COMPID, SNAME FROM D_COMP WHERE SELLER=1 ORDER BY SORTIND', -1);
    LoadSL(slNote1, 'SELECT  D_GOODSID_SAM, NAME_SAM FROM D_GOODS_SAM WHERE D_GOODSID_SAM<>-1000 ORDER BY NAME_SAM', -1);
  end;

  if Dict and GOODSSAM2_DICT <> 0 then
  begin
    slNote2.Clear;
//    LoadSL(slSup, 'SELECT  D_COMPID, SNAME FROM D_COMP WHERE SELLER=1 ORDER BY SORTIND', -1);
    LoadSL(slNote2, 'SELECT  D_GOODSID_SAM, NAME_SAM FROM D_GOODS_SAM WHERE D_GOODSID_SAM<>-1000 ORDER BY NAME_SAM', -1);
  end;

  if Dict and ATT1_DICT <> 0 then
  begin
    slAtt1.Clear;
    LoadSL(slAtt1, 'SELECT ID, NAME FROM D_ATT1 WHERE ID<>-1000 order by SORTIND', ATT1_DICT_ROOT);
  end;

  if Dict and ATT2_DICT <> 0 then
  begin
    slAtt2.Clear;
    LoadSL(slAtt2, 'SELECT ID, NAME FROM D_ATT2 WHERE ID<>-1000 order by SORTIND', ATT2_DICT_ROOT);
  end;

  if Dict and DEP_DICT <> 0 then
  begin
    slDep.Clear;
    LoadSL(slDep, 'SELECT D_DEPID, SNAME FROM D_DEP where D_DEPID<>-1000 order by SORTIND', DEP_DICT_ROOT);
   end;

  if Dict AND SZ_DICT <>0 then
    begin
      slSz.Clear;
      if not Assigned(fmUIDWH) then
       LoadSL(slSZ,  'SELECT distinct SZ, SZ FROM UIDWH_T WHERE userid=-100  ORDER BY SZ', '*')
      else begin
       if dm.UIDWHType=3 then
       LoadSL(slSZ,  'SELECT distinct SZ, SZ FROM UIDWH_T WHERE userid=-100  ORDER BY SZ', '*')
      else
       LoadSL(slSZ,  'SELECT distinct SZ, SZ FROM UIDWH_T WHERE userid='+inttostr(dmcom.UserId)+'  ORDER BY SZ', '*')
     end
    end;

end;

procedure Tdm.quDstBeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with quDst.Params do
    begin
      if DstOne2Many then
        begin
          DepId:=DstDepId;
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          DepId:=taDListDepFromId.AsInteger;
          ByName['DEPID1'].AsInteger:=taDListDepId.AsInteger;
          ByName['DEPID2'].AsInteger:=taDListDepId.AsInteger;
        end;
      ByName['DepFromId'].AsInteger:=DepId;
      ByName['Art2Id'].AsInteger:=DstArt2Id;
    end;
end;

procedure Tdm.quDistredBeforeOpen(DataSet: TDataSet);
begin
  with  quDistred, Params do
    begin
      ByName['SINVID'].AsInteger:=quDistred.Tag;
    end
end;

procedure Tdm.quD_QBeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with quD_Q, Params do
    begin
      ByName['ART2ID'].AsInteger:=DstArt2Id;
      if DstOne2Many then DepId:=DstDepId
      else DepId:= taDListDepFromId.AsInteger;
      ByName['DEPFROMID'].AsInteger:=DepId;
    end;
end;


procedure Tdm.quD_UIDAfterOpen(DataSet: TDataSet);
begin
  ReopenDataSets([quD_T]);
end;

procedure Tdm.taSListBeforeEdit(DataSet: TDataSet);
begin
  if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.taSElBeforeClose(DataSet: TDataSet);
begin
 p:=DataSet.FieldByName('RecNo').AsInteger;
end;

procedure Tdm.taSElBeforeEdit(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      PostDataSets([taA2]);
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taSListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;
     if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
    end
  else
  if WorkMode='DINV' then
    begin
      qutmp.Close;
      qutmp.SQL.Text:='select isclosed from sinv where sinvid='+taDListSINVID.AsString;
      qutmp.ExecQuery;
      if qutmp.Fields[0].AsInteger=1 then
       begin
        qutmp.Close;
        raise Exception.Create('��������� �������!!!');
       end
      else qutmp.Close;
     if taDListDepFromId.AsInteger<>SelfDepId then raise Exception.Create('���������� ������������� ������ ���������');
     if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
    end;
end;

procedure Tdm.SetCloseInvBtn(si: TSpeedItem; IsClosed: integer);
begin
  with si do
    if IsClosed=1 then
      begin
        BtnCaption:='�������...';
        Hint:='������� ���������';
        ImageIndex:=6;
      end
    else
      begin
        BtnCaption:='�������...';
        Hint:='������� ���������';
        ImageIndex:=5
      end;
end;

function Tdm.getCommaBlob(blob: string; index: integer): string;
var
  i, c : integer;
begin
  for i := 0 to Pred(Index)-1 do
  begin
    c := Pos(';', blob);
    delete(blob, 1, c);
  end;
  Result := copy(blob, 1, Pos( ';', blob)-1);
end;

function Tdm.IsInArr(arr: array of Variant; Value: Variant): boolean;
var
  i : integer;
begin
  Result := False;
  for i:= Low(arr) to High(arr) do
    if arr[i]=Value then Result := True;
end;

function Tdm.getPosInArr(Arr: array of Variant; Value: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i:=Low(Arr) to High(arr) do
    if arr[i] = Value then
    begin
      Result := i;
      break;
    end;
end;


procedure Tdm.taSItemBeforeDelete(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
    end;
  BeforeDelete(DataSet);
end;

procedure Tdm.taSItemBeforeEdit(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
    end;
end;

procedure Tdm.taSItemBeforeInsert(DataSet: TDataSet);
begin
  if WorkMode='SINV' then
    begin
      if taSListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!')
      else
      begin
       goods1:=taSItemD_GOODS_SAM1.AsInteger;
       goods2:=taSItemD_GOODS_SAM2.AsInteger;
      end
    end;
end;

procedure Tdm.taDListBeforeEdit(DataSet: TDataSet);
begin
  if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
    raise Exception.Create('������ ������������� ��������� � ������� '#39 +
                  dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������')
  else if taDListIsClosed.AsInteger=1 then
  begin
   raise Exception.Create('��������� �������!!!');
  end
end;

procedure Tdm.taDItemBeforeEdit(DataSet: TDataSet);
begin
  if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
    raise Exception.Create('������ ������������� ��������� � ������� '#39 +
                 dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������')
  else if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.taDItemBeforeInsert(DataSet: TDataSet);
begin
  if (dm.WorkMode<>'OPT')and (not checkInv_RUseDep(taDListDEPFROMID.AsInteger)) then
    raise Exception.Create('������ ������������� ��������� � ������� '#39 +
                 dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������')
  else if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.taDItemBeforeDelete(DataSet: TDataSet);
begin
  if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
    raise Exception.Create('������ ������������� ��������� � ������� '#39 +
                  dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������')
  else if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.quD_UIDFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin

  Accept:=True;
  if SupFilter then Accept:= Accept and (quD_UIDSupId.AsInteger=SupFilterId);
  if PriceFilter then Accept:= Accept and (Abs(quD_UIDPrice.AsFloat-PriceFilterValue)<0.0001);
  if SZFilter then Accept:=Accept and (quD_UIDSZ.AsString=SZFilterValue);

end;

procedure Tdm.quWHA2UIDFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept:=True;
  if SupFilter then Accept:=Accept and (quWHA2UIDSupId.AsInteger=SupFilterId);
  if PriceFilter then Accept:=Accept and (Abs(quWHA2UIDPrice.AsFloat-PriceFilterValue)<0.001);
end;

procedure Tdm.quSupCalcFields(DataSet: TDataSet);
begin
  quSupFName.AsString:=quSupSName.AsString+' - '+quSupName.AsString;
end;

procedure Tdm.quUIDWHAfterOpen(DataSet: TDataSet);
begin
  if Assigned(fmUIDWH) then
     with fmUIDWH do
       begin
        laTime.Caption:='����� ����������: '+TimeToStr(dmCom.GetServerTime - UIDWHTime);
       end;
  UIDWHTime:=-1;
end;

{��������� �������� �������� �� ���������� ��� ���}
function art_z (st:string; var st1:string):boolean;
var i:integer;
    fl:boolean;
begin

 fl:=false;
 st1:='';
 for i:=1 to length(st) do
  if st[i] in ['*'] then
   begin
    st1:=st1+'%';
    fl:=true;
   end
 else   st1:=st1+st[i];
 result:=fl;
end;

procedure Tdm.quUIDWHBeforeOpen(DataSet: TDataSet);
var s,st: string;
    ch:char;

begin
  if UIDWHTime<0 then UIDWHTime:=dmCom.GetServerTime;
  with TpFIBDataSet(DataSet) do
    begin
      if (Length(dmCom.FilterArt)>0) and (dmCom.FilterArt[1]='*') and (Length(dmCom.FilterArt)=1) then
        begin
          if DataSet=quUIDWH then SelectSQL[17]:='REVERSESTR(a2.Art) between :ART1 and :ART2 and'
          else SelectSQL[1]:='UIDWH_ST_R';
          s:=ReverseString(Copy(dmCom.FilterArt, 2, MAXINT));
        end
      else
        begin
          if DataSet=quUIDWH then
           if art_z(dmCom.FilterArt,st) then
            SelectSQL[17]:='a2.Art like :ART1 and'
                    else
            SelectSQL[17]:='a2.Art between :ART1 and :ART2 and'
          else SelectSQL[1]:='UIDWH_ST3';
          s:=dmCom.FilterArt;
        end;

      //12.08.02
     if (DataSet=quUIDWH) then
      begin
      if lMultiSelect and (SD_MatID<>'')then
          SelectSQL[9]:=' a2.d_MatId in ('+SD_MatID+') and '
      else
        if dmCom.D_MatId <> '*' then SelectSQL[9]:=' a2.d_MatId = ' + #39 + dmCom.D_MatId + #39 + ' and '
        else SelectSQL[9]:=' ';

      if lMultiSelect and (SD_GoodID<>'')then
          SelectSQL[11]:=' a2.d_GoodId in ('+SD_GoodID+') and '
      else
      if dmCom.D_GoodId <> '*' then SelectSQL[11]:=' a2.d_GoodId = ' + #39 +  dmCom.D_GoodId + #39 + ' and '
      else SelectSQL[11]:=' ';//'a2.D_GoodID between :GoodID1 and :GoodID2 and ';

      if lMultiSelect and (SD_CompID<>'')then
          SelectSQL[13]:=' a2.d_CompId in ('+SD_CompID+') and '
      else
      if dmCom.D_CompId <> -1 then SelectSQL[13] := ' a2.d_COMPID = ' + IntToStr(dmCom.D_CompId) + ' and '
      else SelectSQL[13]:=' '; //'a2.D_COMPID between :COMPID1 and :COMPID2 and ';

      if lMultiSelect and (SD_InsID <> '') then
         SelectSQL[18]:=' a2.d_InsId in ('+SD_InsID+') and '
      else
      if dmCom.D_InsId <> '*' then SelectSQL[18] := ' a2.d_INSID = ' + #39 + dmCom.D_InsId + #39 + ' and '
      else SelectSQL[18]:=' ';


      if lMultiSelect and (SD_SupID<>'')then
          SelectSQL[19]:='u.SupId0 in ('+SD_SupID+')  and '
      else
      if dmCom.D_SupId <> -1 then SelectSQL[19] := ' u.SupId0 = ' + IntToStr(dmCom.D_SupId) + ' and '
      else SelectSQL[19]:=' '; //'u.SupId0 between :SUPID1 and :SUPID2 ';

      if lMultiSelect and (SD_Note1<>'')then
          SelectSQL[20]:=' u.Goodsid1 in ('+SD_Note1+') and '
      else
      if dmCom.D_Note1 <> -1 then SelectSQL[20]:=' u.Goodsid1 = ' + IntToStr(dmCom.D_Note1) + ' and '
      else SelectSQL[20]:=' '; //'a2.D_COMPID between :COMPID1 and :COMPID2 and ';


      if lMultiSelect and (SD_Note2<>'')then
          SelectSQL[21]:=' u.Goodsid2 in ('+SD_Note2+') and '
      else
      if dmCom.D_Note2 <> -1 then SelectSQL[21]:=' u.Goodsid2 = ' + IntToStr(dmCom.D_Note2) + ' and '
      else SelectSQL[21]:=' '; //'a2.D_COMPID between :COMPID1 and :COMPID2 and ';

      if lMultiSelect and (SD_Att1<>'')then
          SelectSQL[23]:=' a2.Att1 in ('+SD_Att1+') and'
      else
      if dmCom.D_Att1Id <> -2 then SelectSQL[23]:=' a2.Att1 = ' + IntToStr(dmCom.D_Att1Id) + ' and '
      else SelectSQL[23]:=' '; //'a2.D_COMPID between :COMPID1 and :COMPID2 and ';

      if lMultiSelect and (SD_Att2<>'')then SelectSQL[24]:=' a2.Att2 in ('+SD_Att2+') and'
      else
      if dmCom.D_Att2Id <> -2 then SelectSQL[24]:=' a2.Att2 = ' + IntToStr(dmCom.D_Att2Id) + ' and '
      else SelectSQL[24]:=' '; //'a2.D_COMPID between :COMPID1 and :COMPID2 and ';

      if lSellSelect then  SelectSQL[22]:=' u.T=2 and'
      else if (not AllUIDWH) and (not lSRetSelect) and (not lOSinvSelect) and (not lClRetSelect) then  SelectSQL[22]:=' ((u.T between -1 and 1 ) or (u.T=5) or (u.T=7) or (u.T=8)) and'
        else if lSRetSelect then  SelectSQL[22]:=' (u.T=4 or u.T=5) and'
          else if lOSinvSelect then SelectSQL[22]:=' (u.T=-2) and'
           else if lClRetSelect then SelectSQL[22]:=' (u.T in (-2,-1,1)) and (u.Itype=6) and'
            else SelectSQL[22]:=' ';

      if dm.Comission <> -1 then
      begin
        if Trim(SelectSQL[22]) = '' then SelectSQL[22] := ' u.Comission = ' + IntToStr(dm.Comission) + ' and'
        else SelectSQL[22] := SelectSQL[22] + ' u.Comission = ' + IntToStr(dm.Comission) + ' and';
     end;

      //������ ���� ������//
      if dm.PayType<>-1000 then
       begin
        SelectSQL[5]:='FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 at2, d_good dg, sitem sit, sinfo inf, sinv s ';
        SelectSQL[37]:= 'and u.uid = sit.uid and sit.itype = 1 and ';
         if dm.PayType=-1 then
          selectSqL[38]:='sit.sinfoid = inf.sinfoid and inf.sinvid = s.sinvid and (s.paytypeid is null)'
                else
          selectSqL[38]:='sit.sinfoid = inf.sinfoid and inf.sinvid = s.sinvid and s.paytypeid='+IntToStr(dm.PayType);
       end
       else
         if dm.IsTolling <> - 1 then
           begin
//             SelectSQL[5]:='FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 at2, d_good dg, sitem sit, sinfo inf, sinv s';
//             SelectSQL[37]:= 'and u.uid = sit.uid and sit.itype=1 and ';
//             if dm.IsTolling = 1 then
//               selectSqL[38]:='sit.sinfoid = inf.sinfoid and inf.sinvid = s.sinvid and s.is$tolling = 1'
//             else
//               selectSqL[38]:='sit.sinfoid = inf.sinfoid and inf.sinvid = s.sinvid and s.is$tolling <> 1';
           end
         else
           begin
             SelectSQL[5]:='FROM UIDWH_T u, Art2 a2, D_Comp c2, D_Dep d, d_att1 at1, d_att2 at2, d_good dg ';
             SelectSQL[37]:='';
             SelectSQL[38]:='';
//        selectSqL[39]:='';
       end;
      //------------------//

      if lMultiSelect and (UidwhSZ<>'') then
          SelectSQL[31]:=' u.sz in ('+UidwhSZ+') and'
      else
      if dmCom.UIDWHSZ_ <> '*' then SelectSQL[31]:=' u.sz = ' + #39 + dmCom.UIDWHSZ_ + #39 + ' and '
      else SelectSQL[31]:=' ';

      ch:=DecimalSeparator;
      DecimalSeparator:='.';

      if IFilterCost then
          SelectSQL[33]:=' FIF(a2.UnitId, u.Price*u.W, u.Price) between '+FloatToStr(ICost1)+' and '+FloatToStr(ICost2) + ' and'
      else
          SelectSQL[33]:=' ';

      if IFilterW then
          SelectSQL[34]:=' u.w between '+FloatToStr(Iw1)+' and '+FloatToStr(Iw2) + ' and '
      else
          SelectSQL[34]:=' ';

      DecimalSeparator:=ch;
      end;

      Prepare;

      SetDepFilter(Params, SDepId);

      if UIDWHType=3 then
       begin
        Params.ByName['USERID'].AsInteger:=UserDefault;
        dm2.quSameUid.ParamByName('USERID').AsInteger:=UserDefault;
       end
      else
       begin
        ParamByName('USERID').AsInteger:=dmCom.UserId;
        dm2.quSameUid.ParamByName('USERID').AsInteger:=dmCom.UserId;
       end;
      FilterArtb:=false;
      if dmCom.FilterArt = '' then
      begin
        ParamByName('ART1').AsString :='!';
        ParamByName('ART2').AsString := '��';
      end
      else begin
        if not art_z (s,st) then
         begin
           ParamByName('ART1').AsString := s;
           ParamByName('ART2').AsString := s+'��';
         end
else
         begin
           ParamByName('ART1').AsString := st;
           FilterArtb:=true;
         end;
      end;

      if FilterArt2 = '' then
      begin
        ParamByName('ART21').AsString :='';
        ParamByName('ART22').AsString := '��';
      end
      else
      begin
       ParamByName('ART21').AsString := FilterArt2;
       ParamByName('ART22').AsString := FilterArt2+'��';
      end;

      dmCom.SetArtFilter(Params);
     if (DataSet.Name <>'quUIDWH') then
      begin
        ParamByName('COMISSION').AsInteger := dm.Comission;
        ParamByName('PAYTYPE').AsInteger := dm.PayType;
        if lSellSelect then
          begin
            ParamByName('T1').AsInteger := 2;
            ParamByName('T2').AsInteger := 2;
            ParamByName('T3').AsInteger := 2;
            ParamByName('T4').AsInteger := 2;
            ParamByName('T5').AsInteger := 2;
          end
        else if lOSinvSelect then
          begin
            ParamByName('T1').AsInteger := -2;
            ParamByName('T2').AsInteger := -2;
            ParamByName('T3').AsInteger := -2;
            ParamByName('T4').AsInteger := -2;
            ParamByName('T5').AsInteger := -2;
          end
        else if AllUIDWH then
          begin
            ParamByName('T1').AsInteger := -3;
            ParamByName('T2').AsInteger := 100;
            ParamByName('T3').AsInteger := 0;
            ParamByName('T4').AsInteger := 0;
            ParamByName('T5').AsInteger := 0;
          end
         else if lSRETSelect then
          begin
            ParamByName('T1').AsInteger := 4;
            ParamByName('T2').AsInteger := 4;
            ParamByName('T3').AsInteger := 5;
            ParamByName('T4').AsInteger := 5;
            ParamByName('T5').AsInteger := 5;
          end
           else
          begin
            ParamByName('T1').AsInteger := -1;
            ParamByName('T2').AsInteger := 1;
            ParamByName('T3').AsInteger := 5;
            ParamByName('T4').AsInteger := 7;
            ParamByName('T5').AsInteger := 8;            
          end
      end;
    end;
{if UIDWHExportMode then
  begin
    TpFIBDataSet(DataSet).SelectSQL[38] := 'Order by u.SupId0, a2.d_compid, u.NDATE, u.SSF0';
  end
  else
  begin
    TpFIBDataSet(DataSet).SelectSQL[38] := 'Order by a2.art, u.SZ, a2.art2';
  end; }

  //quUIDWH.SelectSQL.Text := quUIDWH.SelectSQL.Text;


  
end;

procedure Tdm.taSellItemAfterPost(DataSet: TDataSet);
begin
  with dm, dmCom do
    begin
      tr.CommitRetaining;
      if WorkMode='ALLSELL' then taSellList.Refresh;
       if WorkMode='SELL' then taCurSell.Refresh;
       if WorkMode='SELLCH' then quCurSellCheck.Refresh;
    end;
end;

procedure Tdm.taSellListNewRecord(DataSet: TDataSet);
begin
  taSellListSellId.AsInteger:=dmCom.GetId(21);
  taSellListBD.AsDateTime:=dmCom.GetServerTime;
  taSellListDepId.AsInteger:=SDepId;
  taSellListDep.AsString:=SDep;
  taSellListClosed.AsInteger:=0;
  taSellListUseSDate.AsInteger:=0;
  taSellListEMP1ID.AsInteger:= dmcom.User.UserId;
end;

procedure Tdm.taSellListBeforeInsert(DataSet: TDataSet);
begin
 if (dmcom.User.ALLSeLL=0) then
   raise Exception.Create('��� ���� �� ���������� �������');

  if SDepId=-1 then raise Exception.Create('���������� ������� �����');
end;

procedure Tdm.quPriceFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var i: integer;
    s: string;
    e: extended;
begin
  Accept:=False;
  with quPrice do
    begin
      i:=0;
      if PriceChangedFilter then {���������� ����}
        while (i<FieldCount-1) and not Accept do
          begin
            if Fields[i].FieldName[1]='X' then
              begin
                if Fields[i].AsInteger=0 then Accept:=True;
              end
            else
              if Fields[i].FieldName[1]='T' then
                begin
                  s:=Fields[i].FieldName;
                  s[1]:='R';
                  if (Fields[i].AsInteger=0) and (FieldByName(s).AsInteger=1) then Accept:=True;
                end;
            Inc(i);
          end
      else Accept:=True;
      if not Accept then exit;
      if PriceDifFilter then       {������ ����}
        begin
          i:=0;
          while (Fields[i].FieldName[1]<>'P') and (i<FieldCount) do Inc(i);
          if (i<FieldCount){ and (Fields[i].FieldName='P') } then
            begin
              e:=Fields[i].AsFloat;
              while (i<FieldCount) do
                begin
                  if Fields[i].FieldName[1]='P' then
                  if Abs(e-Fields[i].AsFloat)>1e-2 then
                    begin
                      Accept:=True;
                      exit;
                    end;
                  Inc(i);
                end;
              Accept:=False;
            end
        end;
   end;
end;

procedure Tdm.taDListAfterOpen(DataSet: TDataSet);
begin
//  ReopenDataSets([quDListT]);
end;

procedure Tdm.taSellListCalcFields(DataSet: TDataSet);
begin
  taSellListCass.AsFloat:=taSellListCost.AsFloat-taSellListRCost.AsFloat;
  taSellListreal_vych.AsCurrency:=taSellListTotal.AsCurrency-taSellListSertSell.AsCurrency+taSellListCSert.AsCurrency;
  taSellListreal_razn.AsCurrency:=taSellListCost.AsCurrency-taSellListreal_vych.AsCurrency;
end;

procedure Tdm.taCurSellCalcFields(DataSet: TDataSet);
begin
  taCurSellCass.AsFloat:=taCurSellCost.AsFloat-taCurSellRCost.AsFloat;
end;

procedure Tdm.taSellItemBeforeEdit(DataSet: TDataSet);
begin
  {if (dmcom.User.ALLSeLL=0) then
   begin
    if ((dm.WorkMode<>'SELLCH')or
       ((dm.WorkMode='SELLCH') and (NOT taSellItemCheckNo.IsNull) and (taSellItemCheckNo.AsInteger <> 0)))
    then raise Exception.Create('��� ���� �� ��������������')
   end
  else
  begin
   // ���� ���� �������� ��� � ��� ������� ����� �������������� ����������
{   if (NOT taSellItemCheckNo.IsNull) and (taSellItemRet.AsInteger=0) and
      (taSellItemCheckNo.AsInteger <> 0) then
   begin
    if (WorkMode<>'ALLSELL') then
     if taCurSellFREG.AsInteger=1 then SysUtils.Abort;
    if (WorkMode='ALLSELL') then
     if (taSellListFREG.AsInteger=1) then SysUtils.Abort;
   end;
  end;
  CheckSellClosed;   }
end;

procedure Tdm.CheckSellClosed;
begin
  if WorkMode='ALLSELL' then
    begin
      if taSellListClosed.AsInteger=1 then SysUtils.Abort;
    end
  else
    begin
      if taCurSellSellId.IsNull or NOT taCurSellED.IsNull then SysUtils.Abort;
    end;
end;

procedure Tdm.taSellItemBeforeDelete(DataSet: TDataSet);
begin
  if (dmcom.User.ALLSeLL=0) then
   begin
    if ((dm.WorkMode<>'SELLCH')or
       ((dm.WorkMode='SELLCH') and (NOT taSellItemCheckNo.IsNull) and (taSellItemCheckNo.AsInteger <> 0)))
    then raise Exception.Create('��� ���� �� ��������')
   end
  else
  begin
   // ���� ���� �������� ��� � ��� ������� ����� �������������� ����������
   if (NOT taSellItemCheckNo.IsNull) and (taSellItemRet.AsInteger=0) and
      (taSellItemCheckNo.AsInteger <> 0) then
   begin
    if (WorkMode<>'ALLSELL') then
     if taCurSellFREG.AsInteger=1 then SysUtils.Abort;
    if (WorkMode='ALLSELL') then
     if taSellListFREG.AsInteger=1 then SysUtils.Abort;
   end;
  end;

  SellItemSZ:= taSellItemART.AsString;
  CheckSellClosed;
  if(dm.taSellItemF.AsInteger <> -1) then DelConf(DataSet);

end;

procedure Tdm.CloseFormByEsc(Sender: TObject; var Key: Char);
begin
  case Key of
    #27 : TForm(Sender).Close;
  end;
end;

procedure Tdm.taPrOrdItemCalcFields(DataSet: TDataSet);
begin
{  if taPrOrdIType.AsInteger<>11 then
    begin
      case taPrOrdItemUnitId.AsInteger of
        0: begin
              taPrOrdItemOldCost.AsFloat:=taPrOrdItemOldP2.AsFloat*taPrOrdItemRQ.AsFloat;
              if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemCost.AsFloat:=taPrOrdItemPrice2.AsFloat*taPrOrdItemRQ.AsFloat
              else taPrOrdItemCost.AsFloat:=taPrOrdItemOldCost.AsFloat;
            end;
        1: begin
             taPrOrdItemOldCost.AsFloat:=taPrOrdItemOldP2.AsFloat*taPrOrdItemRW.AsFloat;
             if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemCost.AsFloat:=taPrOrdItemPrice2.AsFloat*taPrOrdItemRW.AsFloat
             else taPrOrdItemCost.AsFloat:=taPrOrdItemOldCost.AsFloat;
           end;
       end;
     if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemdPrice.AsFloat:=taPrOrdItemPrice2.AsFloat-taPrOrdItemOldp2.AsFloat
     else taPrOrdItemdPrice.AsFloat:=0;
     taPrOrdItemdCost.AsFloat:=taPrOrdItemCost.AsFloat-taPrOrdItemOldCost.AsFloat;
   end
 else
    begin
      case taPrOrdItemUnitId.AsInteger of
        0: begin
              taPrOrdItemOldCost.AsFloat:=taPrOrdItemOldP2.AsFloat*taPrOrdItemRW.AsFloat;
              if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemCost.AsFloat:=taPrOrdItemPrice2.AsFloat*taPrOrdItemRQ.AsFloat
              else taPrOrdItemCost.AsFloat:=taPrOrdItemOldCost.AsFloat;
            end;
        1: begin
             taPrOrdItemOldCost.AsFloat:=taPrOrdItemOldP2.AsFloat*taPrOrdItemRQ.AsFloat;
             if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemCost.AsFloat:=taPrOrdItemPrice2.AsFloat*taPrOrdItemRW.AsFloat
             else taPrOrdItemCost.AsFloat:=taPrOrdItemOldCost.AsFloat;
           end;
       end;
     if NOT taPrOrdItemPrice2.IsNull then taPrOrdItemdPrice.AsFloat:=taPrOrdItemPrice2.AsFloat-taPrOrdItemOldp2.AsFloat
     else taPrOrdItemdPrice.AsFloat:=0;
     taPrOrdItemdCost.AsFloat:=taPrOrdItemCost.AsFloat-taPrOrdItemOldCost.AsFloat;
   end}
end;

procedure Tdm.quPrSIBeforeOpen(DataSet: TDataSet);
begin
  quPrSI.Params[0].AsInteger:=taPrOrdItemPrOrdItemId.AsInteger;
end;

procedure Tdm.taSRetListNewRecord(DataSet: TDataSet);
var
 r : Variant;
begin
  taSRetListSInvId.AsInteger:=dmCom.GetId(8);
  if DDepFromId<>-1 then taSRetListDepFromId.AsInteger:=DDepFromId;
  taSRetListSDate.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime))+Time;
  taSRetListTr.AsFloat:=0;
  taSRetListPMargin.AsFloat:=0;
  taSRetListCost.AsFloat:=0;
  taSRetListAkciz.AsFloat:=dmCom.taRecAkciz.AsFloat;
  taSRetListIsClosed.AsInteger:=0;
  taSRetListDepFrom.AsString:=DDepFrom;
  taSRetListUSERID.AsInteger := dmCom.UserId;
  r := ExecSelectSQL('select RET_NONDS from D_REC', dm.quTmp);
  if not VarIsNull(r)then taSRetListRET_NONDS.AsInteger := r
  else taSRetListRET_NONDS.AsInteger := 0;
end;

procedure Tdm.taOptListBeforeInsert(DataSet: TDataSet);
begin
  if DDepFromId=-1 then raise Exception.Create('���������� ������� �����');
end;

procedure Tdm.taSRetBeforeInsert(DataSet: TDataSet);
var i:integer;
begin
 if WorkMode='SRET' then
 begin
   with qutmp do
   begin
    Sql.Text := '  select uidwhcalc  from d_rec ';
    ExecQuery;
    i:= Fields[0].AsInteger;
    close;
   end;
   If i = 1 then Raise Exception.Create('��������� ������ �����������');
   PostDataSet(taSRetList);
 end
 else PostDataSet(taORetList);
end;

procedure Tdm.taSRetBeforeOpen(DataSet: TDataSet);
begin
//showmessage('taSRetBeforeOpen begin');
  if WorkMode='SRET' then taSRet.Params[0].AsInteger:=taSRetListSInvId.AsInteger
  else taSRet.Params[0].AsInteger:=taORetListSInvId.AsInteger;
//  showmessage('taSRetBeforeOpen end');
end;

procedure Tdm.taSRetNewRecord(DataSet: TDataSet);
begin
  taSRetSRetId.AsInteger:=dmCom.GetId(30);
  if WorkMode='SRET' then taSRetSInvId.AsInteger:=taSRetListSInvId.AsInteger
  else taSRetSInvId.AsInteger:=taORetListSInvId.AsInteger;
end;

procedure Tdm.taTollingParamsBeforeClose(DataSet: TDataSet);
begin
  if dataSet.State in [dsEdit, dsInsert] then dataSet.Post;
end;

procedure Tdm.taTollingParamsBeforeOpen(DataSet: TDataSet);
begin
  (DataSet as tpFIBDataSet).ParamByName('Invoice$ID').AsInteger := taSListSINVID.AsInteger;
end;

procedure Tdm.taTollingParamsBeforePost(DataSet: TDataSet);
begin

  if taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat <= 0 then
    taTollingParamsINCLUDEINSERTIONS.AsFloat := 0
    else
    taTollingParamsINCLUDEINSERTIONS.AsFloat := 1;

  if (taTollingParamsTOTALINVOICEWEIGHT.AsFloat - taTollingParamsTOTALINSERTIONSWEIGHT.AsFloat) < 0 then
    begin
      MessageDialog('����� ��� ��������� �� ��������� ���� ����������� ������ ��� ����� ����!', mtWarning, [mbOK], 0);
      taTollingParams.Transaction.RollbackRetaining;
      fmSInv.edTotalInvoiceWeight.SetFocus;
      SysUtils.Abort;
    end;

  if (taTollingParamsAVERAGEPRICE.AsFloat < 800) then
    if MessageDialog('������ ���� �������. ������� ����������?', mtInformation, [mbYes, mbNo], 0) = mrNo then
      begin
        taTollingParams.Transaction.RollbackRetaining;
        fmSInv.edBuhPrice585.SetFocus;
      end;

  if (taTollingParamsTOTALLOSSESWEIGHT.AsFloat = 0) then
    begin
      if MessageDialog('������ ����� ����! ����������?', mtWarning, [mbYes, mbNo], 0) = mrNo then
        begin
          taTollingParams.Transaction.RollbackRetaining;
          fmSInv.edTotalLossesWeight.SetFocus;
          SysUtils.Abort;
        end;
    end;

  taTollingParamsPUREK.AsCurrency := taTollingParamsPURE.AsInteger / 1000;
   
end;

procedure Tdm.taSRetCalcFields(DataSet: TDataSet);
begin
{ TODO : �������� ��������������� ����� ��������� ��������}
 if not taSRetListSINVID.IsNull then
  with dm, qutmp do
  begin
   sql.Text:= 'execute procedure SRETLIST_SN ('+inttostr(taSRetListSINVID.asinteger)+')';
   ExecQuery;
   dmcom.tr.CommitRetaining;
   taSRetList.Refresh;
   close;
  end;
{***************************************************}
  taSRetRecNo.AsInteger:=DataSet.RecNo;
  case taSRetUnitId.AsInteger of
      0: taSRetCost.AsFloat:=taSRetSPrice.AsFloat;
      1: taSRetCost.AsFloat:=taSRetSPrice.AsFloat*taSRetW.AsFloat;
    end;
end;

procedure Tdm.taSRetAfterDelete(DataSet: TDataSet);
begin
//showmessage('taSRetAfterDelete begin');
  dmCom.tr.CommitRetaining;
  if WorkMode='SRET' then taSRetList.Refresh
  else taORetList.Refresh;
 // reopendataset(tasret);
// showmessage('taSRetAfterDelete end');
end;

procedure Tdm.taSRetAfterOpen(DataSet: TDataSet);
begin
//showmessage('taSRetAfterOpen');
end;

procedure Tdm.taSRetAfterPost(DataSet: TDataSet);
begin
//showmessage('taSRetAfterDelete begin');
  dmCom.tr.CommitRetaining;
  if WorkMode='SRET' then taSRetList.Refresh
  else taORetList.Refresh;
// showmessage('taSRetAfterDelete end');
end;

procedure Tdm.taORetListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
end;

procedure Tdm.taORetListNewRecord(DataSet: TDataSet);
begin
  taORetListSInvId.AsInteger:=dmCom.GetId(8);
  if SDepId<>-1 then taORetListDepId.AsInteger:=SDepId;
  taORetListSDate.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime));
  taORetListNDS.AsFloat:=0;
  taORetListTr.AsFloat:=0;
//  taSListPTr.AsFloat:=0;
  taORetListPMargin.AsFloat:=0;
  taORetListCost.AsFloat:=0;
  taORetListAkciz.AsFloat:=dmCom.taRecAkciz.AsFloat;
//  taDListSN.AsInteger:=dmCom.GetId(16);
  taORetListIsClosed.AsInteger:=0;
  taORetListDep.AsString:=SDep;

  with quTmp do
    begin
      SQL.Text:='SELECT SN FROM SINV WHERE SINVID=(SELECT MAX(SINVID) FROM SINV WHERE ITYPE=8 AND DEPID='+IntToStr(SDepId)+' AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39'))';
      ExecQuery;
      if Fields[0].IsNull then       taORetListSN.AsInteger:=1
      else       taORetListSN.AsInteger:=Fields[0].AsInteger+1;
      Close;
    end;

  taORetListIType.AsInteger:=8;
  taORetListOptRet.AsInteger:=1;  
end;

procedure Tdm.ORetClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taORetList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= CenterDepId ;
    SDep := dmCom.Dep[CenterDepId].SName;
    fmORetList.laDepFrom.Caption:=SDep;
    taORetList.Active:=True;
//    CalcSListCost;
  finally
    Screen.Cursor:=crDefault;
  end;
end;


procedure Tdm.quOSelledBeforeOpen(DataSet: TDataSet);
begin
  with quOSelled, Params do
    begin
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(OBeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(OEndDate);
      ByName['DEPID'].AsInteger:=taORetListDepId.AsInteger;
      if OptBuyerId=NULL then
        begin
          ByName['BUYERID1'].AsInteger:=-MAXINT;
          ByName['BUYERID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['BUYERID1'].AsInteger:=OptBuyerId;
          ByName['BUYERID2'].AsInteger:=OptBuyerId;
        end;
      dmCom.SetArtFilter(Params);
    end;
end;

procedure Tdm.taORetAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
  taORetList.Refresh;
end;
                  
procedure Tdm.taORetBeforeOpen(DataSet: TDataSet);
begin
  taORet.Params[0].AsInteger:=taORetListSInvId.AsInteger;
end;

procedure Tdm.taORetNewRecord(DataSet: TDataSet);
begin
  taORetSItemId.AsInteger:=dmCom.GetId(14);
  taORetSInvId.AsInteger:=taORetListSInvId.AsInteger;
end;

procedure Tdm.taORetCalcFields(DataSet: TDataSet);
begin
  taORetRecNo.AsInteger:=DataSet.RecNo;
  case taORetUnitId.AsInteger of
      0: taORetCost.AsFloat:=taORetPrice0.AsFloat;
      1: taORetCost.AsFloat:=taORetPrice0.AsFloat*taORetW.AsFloat;
    end;
end;

procedure Tdm.taSellItemBeforePost(DataSet: TDataSet);
var IsEditApp: boolean;
begin
{ if WorkMode='SELLCH' then
   if taSellItemUID.AsInteger=0 then
   begin
    taSellItem.Refresh;
    raise Exception.Create('�� ������ ��. �����');
   end;}
  if taSellItemW.IsNull then raise Exception.Create('���������� ������ �������� � ���� ���');
  if taSellItemSZ.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������');
  if taSellItemRet.AsInteger=0 then
    if (WorkMode='SELL')or(WorkMode='SELLCH') then
      begin
        if taSellItemADate.AsDateTime<taCurSellBD.AsDateTime then
          raise Exception.Create('������� ����� ���� ������� �� ������ '+DateTimeToStr(taCurSellBD.AsDateTime));
      end
    else
      begin
        if taSellItemADate.AsDateTime<taSellListBD.AsDateTime then
          raise Exception.Create('������� ����� ���� ������� �� ������ '+DateTimeToStr(taSellListBD.AsDateTime));
      end;

    if (WorkMode='ALLSELL') and ( Centerdep) then
    begin
     IsEditApp:= false;
     if dmServ.quTmp.Open then dmServ.quTmp.Close;
     dmServ.quTmp.SQL.Text:='select d_depid, sname from d_dep where d_depid<>-1000 and d_depid<>'+
                       taSellListDEPID.AsString+' and isdelete<>1 ';
     dmServ.quTmp.ExecQuery;
     while not dmServ.quTmp.Eof do
     begin
//       if not taSellItem.FieldExist('QART_'+quTmp.Fields[0].AsString,i)then break;
        if (taSellItem.FieldByName('QART_'+dmServ.quTmp.Fields[0].AsString).NewValue  <>
          taSellItem.FieldByName('QART_'+dmServ.quTmp.Fields[0].AsString).OldValue)
{         or
         ((taSellItem.FieldByName('QART_'+dmServ.quTmp.Fields[0].AsString).NewValue =
         taSellItem.FieldByName('QART_'+dmServ.quTmp.Fields[0].AsString).OldValue) and
           (taSellItem.FieldByName('QART_'+dmServ.quTmp.Fields[0].AsString).NewValue = 0))} then
       begin
         IsEditApp:= True;
         break;
       end;
       dmServ.quTmp.Next;
     end;
     dmServ.quTmp.Close;
  {   if taSellItemAPPL_Q.NewValue <> taSellItemAPPL_Q.OldValue then
       IsEditApp:= True;
     if (not IsEditApp) and (not checkInv_RUseDep(taSellListDEPID.AsInteger)) then
       raise Exception.Create('������ ������������� ������� � ������� '#39 +
                  dmCom.Dep[taSellListDEPID.AsInteger].SName + #39' �� ������');    }
    end;
end;

procedure Tdm.quUIDWHCalcFields(DataSet: TDataSet);
var s: string;
begin
  s:='';
  with dataset do
  case {quUIDWH}FieldByName('T').AsInteger of
      0: case FieldByName('IType').AsInteger of
             2: if  FieldByName('Sdate0').AsDateTime<=dm.BeginDate then s:='������ �� ������ �������'
                else  s:='������ �� �����������';
             5: s:='�������';
             9: case FieldByName('IsClosed').AsInteger of
                 0: s:='������, ��������� �������';
                 1: s:='������, ��������� �������';
                end;
             10: s:='���������� �������';
             21: s:='�������';
             else  case FieldByName('IsClosed').AsInteger of
                       0: s:='������ �� ������ �������, ��������� �� �������';
                       1: s:='������ �� ������ �������';
                     end
           end;

      1,-1: begin
           case FieldByName('IType').AsInteger of
               1: s:='������ �� �����������';
                {���� ���� �����.����������� � ��� ���� ��������� � ����� �������, �� ��� ��� - ����� ��� ������}
               2: if (FieldByName('Sdate').IsNull or (FieldByName('Sdate').AsDateTime<=dm.BeginDate))
                  then s:='������ �� ������ �������'
                  else s:='���������� �����������';

               6:
                  begin
                    s:='������� �� �����������';
                  end;
               8: s:='������� �� ������� �����������';
               9: case FieldByName('IsClosed').AsInteger of
                   0: s:='������, ��������� �������';
                   1: s:='������, ��������� �������';
                  end;
               10: s:='���������� �������';
               21: s:='�������';
             end;

           case FieldByName('IsClosed').AsInteger of
               0: s:=s+', ��������� �� �������';
             end;
         end;
      2: case FieldByName('IsCLosed').AsInteger of
      {��� ������ ��������� ����� � ������� �� �������� � ����������� ������}
             0: s:='�������, ����� �� �������';
             1,2: s:='�������';
           end;
      3: case FieldByName('IsClosed').AsInteger of
            0: s:='������� �������, ��������� �� �������';
            1: s:='������� �������';
          end;
      4: case FieldByName('IsClosed').AsInteger of
            1: s:='������� �����������';
          end;
      5: case FieldByName('IsClosed').AsInteger of
            0: s:='������� �����������, ��������� �� �������';
          end;
      6: s:='��� ��������';
      7: s:='��� ��������, �������� ���������';
    end;

  Dataset.FieldByName('Memo').AsString:=s;

  if CenterDep then
  begin

    with quTmp2 do
      begin
        Close;
        SQL.Text := 'select in$price from get$tolling$price(:UID)';
        Params[0].AsInteger := DataSet.FieldByName('UID').AsInteger;
        ExecQuery;
        DataSet.FieldByName('Tolling$Price').AsCurrency := Fields[0].AsCurrency;
      end;

  {end else
  begin
    DataSet.FieldByName('Tolling$Price').AsCurrency := 0.00;}
  end;

   case quUIDWHFLAG.AsInteger of
      0: begin
        quUIDWHFLAGNAME.AsString := '';
      end;

      1: begin
        quUIDWHFLAGNAME.AsString := '�';
      end;

      2: begin
        quUIDWHFLAGNAME.AsString := '�';
      end;

      3: begin
        quUIDWHFLAGNAME.AsString := '�';
      end;
   end;


end;

procedure Tdm.quSelledBeforeOpen(DataSet: TDataSet);
begin
  with quSelled.Params do
    begin
      if (WorkMode='SELL') or (WorkMode='SELLCH') then ByName['DEPID'].AsInteger:=taCurSellDepId.AsInteger
      else if WorkMode='ALLSELL' then ByName['DEPID'].AsInteger:=taSellListDepId.AsInteger;
      ByName['UserID'].AsInteger := SellElUser;
    end;

end;

procedure  Tdm.CreateTmpFile(FileName: string);
begin
  with quFCreate do
    begin
      Params[0].AsString:=FileName;
      ExecQuery;
    end;
end;

procedure  Tdm.RemoveTmpFile(FileName: string);
begin
  with quFRemove do
    begin
      Params[0].AsString:=FileName;
      ExecQuery;
    end;
end;

procedure Tdm.quArtAfterPost(DataSet: TDataSet);
begin
  CommitRetaining(DataSet);
  if InsertedArtId<>-1 then
    begin
      if Assigned(fmSINv) then
        fmSInv.ceArt.Text:=InsertedArt;
      if Art<>'' then Art:=InsertedArt;
      ReOpenDataSets([quArt]);
      quArt.Locate('D_ARTID', InsertedArtId, []);
    end;
//  fmSInv.dgArt.ReadOnly:=True;
end;

procedure Tdm.quArtAfterInsert(DataSet: TDataSet);
begin
//  fmSInv.dgArt.ReadOnly:=False;
//  Application.ProcessMessages;
  if Assigned(fmSInv) then
  begin
    fmSInv.Label21.Caption:='0,00�';
    fmSInv.Label20.Caption:='0,00�'
  end
end;

procedure Tdm.quArtAfterCancel(DataSet: TDataSet);
begin
//  fmSInv.dgArt.ReadOnly:=False;
//  Application.ProcessMessages;
end;

procedure Tdm.quSListTCalcFields(DataSet: TDataSet);
begin
  quSListTCNoTr.AsFloat:=quSListTCost.AsFloat-quSListTTr.AsFloat;
end;

procedure Tdm.quSRetUIDBeforeOpen(DataSet: TDataSet);
var A2ID: integer;
    DID: integer;
begin
  DID:=taSRetListDepFromId.AsInteger;
  A2ID:=quD_WHArt2Id.AsInteger;
  with quSRetUID.Params do
    begin
      ByName['contract$id'].AsInteger := taSRetListCONTRACTID.AsInteger;
      ByName['ART2ID'].AsInteger:=A2ID;
      ByName['SUPID'].AsInteger:=taSRetListCompId.AsInteger;
      if DID=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=DID;
          ByName['DEPID2'].AsInteger:=DID;
        end;
    end;
end;

procedure Tdm.taSItemAfterInsert(DataSet: TDataSet);
var i: integer;
begin
  if fmSItem<>NIL then
    with fmSItem, dg1 do
      begin
        i:=0;
        while Columns[i].Field.FieldName<>'W' do Inc(i);
        SelectedIndex:=i;
      end;
end;

procedure Tdm.FindLBItem(lb: TListBox; Value: variant);
var i: integer;
begin
  with lb do
    for i:=0 to Items.Count-1 do
      if TNodeData(Items.Objects[i]).Code=Value then
        begin
          ItemIndex:=i;
          exit;
        end;
end;

procedure Tdm.quMaxArtPriceBeforeOpen(DataSet: TDataSet);
begin
  with quMaxArtPrice, Params do
    begin
      ByName['D_ARTID'].AsInteger:=quArtD_ArtId.AsInteger;
      ByName['DEPID'].AsInteger:=taSListDepId.AsInteger;
    end;
end;

procedure Tdm.taSellListBeforePost(DataSet: TDataSet);
begin
  if taSellListBD.AsFloat<1 then raise Exception.Create('���������� ������ ���� � ����� ������ �����');
  {$IFDEF TRIALPERIOD}
  CheckTrialPeriod(taSellListBD.AsDateTime); 
  {$ENDIF}
end;

procedure Tdm.taA2InsBeforePost(DataSet: TDataSet);
var s: string;
begin
  s:=taA2InsEdgTypeId.AsString;
  if s<>'' then
    if not dmCom.quEdgT.Locate('EDGETIONID', s, []) then
      begin
        quTmp.SQL.Text:='INSERT INTO D_EDGETION (EDGETIONID, NAME, T) VALUES ('''+s+''', '''+s+''', 2)';
        quTmp.ExecQuery;
        dmCom.tr.CommitRetaining;
        if fmIns<>NIL then
          fmIns.dg1['EDGTYPEID'].PickList.Add(s);
      end;

end;

procedure Tdm.quPrOrdCalcFields(DataSet: TDataSet);
var s: string;
begin
  s:=quPrOrdSN.AsString;
  case quPrOrdIType.AsInteger of
      1: s:=s+' ��';
      2: s:=s+' ��';
      5: s:=quPrOrdSSF.AsString+' ���';
      6: s:=s+' �.����� ';
     10: s:='�����������';
     11: s:='��������� ��.';
     12: s:='��������� ���.';
     13: s:='����. ���. ���. �� ������';
    end;
  quPrOrdSNStr.AsString:=s;
end;

procedure Tdm.quPrOrdBeforeOpen(DataSet: TDataSet);
begin
  quPrOrd.Params[0].AsInteger:=PrOrdId;
end;

procedure Tdm.taCurSellBeforePost(DataSet: TDataSet);
begin
  if taCurSellBD.AsFloat<1 then raise Exception.Create('���������� ������ ���� � ����� ������ �����');
end;

procedure Tdm.quADictBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
    SetAFilter(Params);
end;

procedure Tdm.quA2DictBeforeInsert(DataSet: TDataSet);
begin
  PostDataSets([quADict]);
  if quADictD_ArtId.IsNull then SysUtils.Abort;
  quA2Dict.First;

end;

procedure Tdm.quA2DictBeforePost(DataSet: TDataSet);
var i: integer;
begin
  A2Inserted:=quA2Dict.State=dsInsert;
  if A2Inserted then
    with dm2.quCheckA2Exist, Params do
      begin
        ByName['ART2'].AsString:=quA2DictArt2.AsString;
        ByName['D_ARTID'].AsInteger:=quA2DictD_ArtID.AsInteger;
        ExecQuery;
        i:=Fields[0].AsInteger;
        Close;
        if i<>0 then if MessageDialog('������� 2 ��� ����������. ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
      end;

  if quA2DictArt2.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������� 2');
end;

procedure Tdm.quA2DictNewRecord(DataSet: TDataSet);
begin
  quA2DictArt2Id.AsInteger:=dmCom.GetId(24);
  quA2DictD_ArtId.AsInteger:=quADictD_ArtId.AsInteger;
  quA2DictArt2.AsString:='-';
  quA2DictSupId.AsInteger := dmServ.DicSupID;
end;

procedure Tdm.quADictBeforeInsert(DataSet: TDataSet);
begin
  with dmCom do
    begin
      if D_CompId=-1 then raise Exception.Create('���������� ������� �������������');
      if D_MatId='*' then raise Exception.Create('���������� ������� ��������');
      if D_GoodId='*' then raise Exception.Create('���������� ������� ��� ������');
      if D_InsId='*' then raise Exception.Create('���������� ������� �������� �������');
      if D_CountryId='*' then raise Exception.Create('���������� ������� ������');      
    end;
  NewArt:=quADictArt.AsString;
  NewUnitId:=quADictUnitId.AsInteger;
end;

procedure Tdm.quADictBeforePost(DataSet: TDataSet);
var i: integer;
    s: string;
begin
  if not CheckForSpecSymb(quADictArt.AsString) then begin
     dm.quADict.Delete;
     raise Exception.Create('�������  �� ����� ���������� �� ����. ��������');
     end;

  if quADict.State=dsInsert then
    begin
      with dm2.quCheckAExist do
        begin
          Params[0].AsString:=quADictArt.AsString;
          ExecQuery;
          i:=Fields[0].AsInteger;
          Close;
          if i>0 then
            with dm2.quGetA do
              begin
                Active:=False;
                Params[0].AsString:=quADictArt.AsString;
                Active:=True;
                s:=Fields[0].AsString;
                Active:=False;
                if MessageDialog('�������  ��� ����������.'+#13#10+s+#13#10+'����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
            end;
        end;
      InsertedArtId:=quADictD_ArtId.AsInteger;
    end
  else InsertedArtId:=-1;

  if quADictD_COMPId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������������');
  if quADictD_MATId.IsNull  then raise Exception.Create('���������� ������ �������� � ���� ��������');
  if quADictD_GOODId.IsNull then raise Exception.Create('���������� ������ �������� � ���� ������������');
  if quADictD_InsId.IsNull then raise Exception.Create('���������� ������ �������� � ���� �������� �������');
  if quADictArt.AsString='' then raise Exception.Create('���������� ������ �������� � ���� �������');

end;
procedure Tdm.quADictNewRecord(DataSet: TDataSet);
var UnitId: integer;
begin
  with dmCom do
    begin
      quADictD_ArtID.AsInteger:=GetID(5);
      if D_CompID<>-1 then quADictD_CompId.AsInteger:=D_CompId;
      if D_MatId<>'' then quADictD_MatId.AsString:=D_MatId;
      if D_GoodId<>'' then quADictD_GoodId.AsString:=D_GoodId;
      if D_countryId<>'' then quADictD_countryId.AsString:=D_countryId;
      if D_InsId<>'' then quADictD_InsId.AsString:=D_InsId;
      if D_Att1Id=ATT1_DICT_ROOT then quADictATT1.AsInteger := -1
      else quADictATT1.AsInteger := D_Att1Id;
      if D_Att2Id=ATT2_DICT_ROOT then quADictATT2.AsInteger := -1
      else quADictATT2.AsInteger := D_Att2Id;
      with quTmp do
        begin
          SQL.Text:='SELECT UNITID FROM D_INS WHERE D_INSID='''+D_InsId+'''';
          ExecQuery;
          if NOT Fields[0].IsNull then UnitId:=Fields[0].AsInteger
          else
            begin
              Close;
              SQL.Text:='SELECT UNITID FROM D_MAT WHERE D_MATID='''+D_MATId+'''';
              ExecQuery;
              if NOT Fields[0].IsNull then UnitId:=Fields[0].AsInteger
              else UnitId:=1;
            end;
          Close;
        end;
      quADictUnitId.AsInteger:=UnitId;
    end;
end;

procedure Tdm.quUIDWHBeforePost(DataSet: TDataSet);
begin
  quUIDWHUserId.AsInteger:=dmCom.UserId;
end;

procedure Tdm.quA2DictAfterPost(DataSet: TDataSet);
begin
 if (A2Inserted) and Assigned(fmUIDWH) then
    if (quUIDWHINSID.AsString  = dmCom.D_InsId) then
    with quNewA2Ins, Params do
      begin
        ByName['OLDART2ID'].AsInteger:=quUIDWHArt2Id.AsInteger;
        ByName['NEWART2ID'].AsInteger:=quA2DictArt2Id.AsInteger;
        ExecQuery;
      end;

  CommitRetaining(DataSet);
end;

function Tdm.EmptyStrToZero(const s:string):string;
begin
 if s='' then
   result:='0'
 else result:=s;
end;

procedure Tdm.quD_WHCalcFields(DataSet: TDataSet);
var i: integer;
    qs, ws, as_: string;
begin
  qs:=quD_WHRestQ.AsString;
  as_:=ReplaceStr(quD_WHRestA.AsString, '.', ',');
  ws:=ReplaceStr(quD_WHRestW.AsString, '.', ',');
  for i:=0 to dm2.slDepDepId.Count-1 do
    begin
      DataSet.FieldByName('RW_'+dm2.slDepDepId[i]).AsFloat:=StrToFloat(dm.EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
      DataSet.FieldByName('RQ_'+dm2.slDepDepId[i]).AsInteger:=StrToInt(dm.EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      DataSet.FieldByName('A_'+dm2.slDepDepId[i]).AsInteger:=
        round(StrToFloat(dm.EmptyStrToZero(ExtractWord(i+1, as_, [';']))));
    end;
end;

procedure Tdm.quMaxArtPriceAfterOpen(DataSet: TDataSet);
begin
  with DataSet do
    begin
      TFloatField(Fields[0]).Currency:=True;
      TFloatField(Fields[1]).Currency:=True;
    end;
end;


procedure Tdm.CheckDInvN;
var i: integer;
begin
  with quTmp do
    begin
      SQL.Text:='SELECT count(*) SINVID FROM SINV '+
                'WHERE ITYPE=2 AND SN='+taDListSN.AsString+
                ' AND DepFromId='+taDListDepFromId.AsString+' AND FYEAR(SDATE)='+GetSYear(taDListSDate.AsDateTime)+
                ' AND DepId='+taDListDepId.AsString;
      ExecQuery;
      i:=Fields[0].AsInteger;
      Close;
      if i>1 then raise Exception.Create('������������ ����� ���������!!!');
    end;
end;

function Tdm.AcceptUIDWH(DataSet:TDataSet):boolean;
var
    Accept:boolean;
begin
  Accept:=true;
  with TpFIBDataSet(DataSet) do
    begin
    {
      if (Length(dmCom.FilterArt)>0) then
       begin
        if (dmCom.FilterArt[1]='*') then
          begin  s:=ReverseString(Copy(dmCom.FilterArt, 2, MAXINT));
                 s1:=ReverseString(FieldByName('ART').asString);
          end
        else
         begin
            s:=dmCom.FilterArt;
            s1:=FieldByName('ART').asString;
         end;
        accept:=Accept and (pos(s,s1)=1);
      end;
      }       
      if (SD_MatID<>'')then
         Accept:=Accept and (pos('"'+FieldByName('MATID').asString+'"',SD_MatID)>0);
      if (SD_GoodID<>'')then
         Accept:=Accept and (pos('"'+FieldByName('GOODID').asString+'"',SD_GoodID)>0);
      if (SD_CompID<>'')then
         Accept:=Accept and (pos(FieldByName('CompID').asString+',',SD_CompID)>0);
      try
      if (SD_SupID<>'')then
         Accept:=Accept and (pos(FieldByName('SupID0').asString+',',SD_SupID)>0);
      except
      end;
    end;
  Result:=Accept;
end;

procedure Tdm.quUIDWHFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
 // if UIDWHSupFlt then Accept:=quUIDWHSupId0.AsInteger=UIDWHSupId;
 //Accept:=AcceptUIDWH(DataSet);

{ if lSellSelect then  Accept:=Accept and (TpFIBDataSet(DataSet).FieldByName('T').asInteger=2)
 else if not AllUIDWH then  Accept:=Accept and (TpFIBDataSet(DataSet).FieldByName('T').asInteger >=-1) and
                                               (TpFIBDataSet(DataSet).FieldByName('T').asInteger <=1);
 }

end;

procedure Tdm.quUIDWHAfterPost(DataSet: TDataSet);
begin
  with dmCom do
    begin
      tr.CommitRetaining;
      tr1.CommitRetaining;
    end;
end;

procedure Tdm.ClearItBuf;
begin
  with dmCom, dm2 do
    begin
      CloseDataSets([taItBuf]);
      quTmp.SQL.Text:='DELETE FROM ITBUF WHERE USERID='+IntToStr(UserId);
      quTmp.ExecQuery;
      tr.CommitRetaining;
      OpenDataSets([taItBuf]);
    end;
end;


procedure Tdm.quD_WHAfterOpen(DataSet: TDataSet);
begin
//  ReopenDataSets([dm2.quD_WH_T]);
end;

procedure Tdm.quUIDSelledBeforeOpen(DataSet: TDataSet);
begin
  with quUIDSelled, Params do
    begin
      if (WorkMode='SELL')and(WorkMode='SELLCH') then ByName['DEPID'].AsInteger:=taCurSellDepId.AsInteger
      else if WorkMode='ALLSELL' then ByName['DEPID'].AsInteger:=taSellListDepId.AsInteger;
    end;
end;

function Tdm.GetArt2ArtId(Art2Id: integer): integer;
begin
  with quTmp do
    begin
      SQL.Text:='SELECT D_ArtId FROM ART2 WHERE  ART2Id='+IntToSTr(Art2Id);
      ExecQuery;
      Result:=Fields[0].AsInteger;
      Close;
    end;
end;

procedure Tdm.quArtAfterScroll(DataSet: TDataSet);
begin
//  if WorkMode='SINV'  then    ReopenDataSets([dm.quMaxArtPrice]);
end;

procedure Tdm.quUIDWHSELLDATEGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
 Text:=DateToStr(Sender.AsDateTime);
 if (Text='17.11.1858') or (Text='17.11.58') then Text:='';
end;

procedure Tdm.quTempUIDWHBeforeOpen(DataSet: TDataSet);
begin
 with quTempUIDWH do
  ParamByName('USERID').AsInteger:=tag;
end;

procedure Tdm.RSTATEGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           1: Text:='��������� � �������';
           2: Text:='�������';
           3: Text:='������';
           4: Text:='����������';
         end;
end;

procedure Tdm.SetVisEnabled(WC:TWincontrol;b:boolean);
begin
   with WC do
    begin
     enabled:=b;
     visible:=b;
    end;
end;

procedure Tdm.udpInvDataReceived(Sender: TComponent; NumberBytes: Integer;
  FromIP: String; Port: Integer);
var ms: TMemoryStream;
    rInvId: integer;
    rUserId: integer;
    s: string;
begin{                
  ms:=TMemoryStream.Create;
  try
    udpInv.ReadStream(ms);
    SetLength(s, NumberBytes);
    ms.Read(s[1], NumberBytes);

    //����
    //(EXtractWord(1, s, [':'])=UpperCase(dmCom.db.DatabaseName)) and
    if (EXtractWord(1, s, [':'])=IntToStr(SelfDepId)) and (EXtractWord(3, s, [':'])='INV') then
      begin
        rUserId:=StrToIntDef(EXtractWord(2, s, [':']), -1000);
        if (rUserId<>-1000) and (rUserId<>dmCom.UserId)
        then
          begin
            rInvId:=StrToIntDef(EXtractWord(4, s, [':']), -1000);
            if WorkMode='SINV' then
              if (taSListSInvId.AsInteger=rInvId) and (ClosedInvId=-1) then
                begin
                  MessageDialog('��������� ������� ������ �������������', mtInformation, [mbOK], 0);
                  dmCom.tr.CommitRetaining;
                  taSList.Refresh;
                  CancelDataSets([taSEl]);
                  ReOpenDataSets([taSEl]);
                  SetCloseInvBtn(fmSInv.siCloseInv, taSListIsClosed.AsInteger);
                end;
            if WorkMode='DINV' then
              if (pos('*'+inttostr(rInvId)+';',sWorkInvID)>0) and (ClosedInvId=-1) then
                begin
                  MessageDialog('��������� ������� ������ �������������', mtInformation, [mbOK], 0);
                  dmCom.tr.CommitRetaining;
                  taDList.Refresh;
                  CancelDataSets([taSEl]);
                  ReOpenDataSets([taSEl]);
                  SetCloseInvBtn(fmDInv.siCloseInv, taDListIsClosed.AsInteger);
               end;
          end;
      end;
  finally
    ms.Free;
  end;
  }
end;

procedure Tdm.dsArtDataChange(Sender: TObject; Field: TField);
begin
  if WorkMode='SINV' then ReopenDataSets([dm.quMaxArtPrice]);

end;

procedure Tdm.dsCassaBeforeOpen(DataSet: TDataSet);
begin
 with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
  dmserv.FSellEdit:=false;
end;

procedure Tdm.quArtAfterOpen(DataSet: TDataSet);
begin
  // by basile
  if(WorkMode = 'APPL')then
    ReopenDatasets([dmServ.taAppl]);
end;

procedure Tdm.taSRetListBeforeEdit(DataSet: TDataSet);
begin
 if WorkMode='SRET' then
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.taSellListBeforeDelete(DataSet: TDataSet);
begin
 if (dmcom.User.ALLSeLL=0) then
   raise Exception.Create('��� ���� �� �������� �������');

 DelConf(DataSet);
 dmserv.FSellEdit:=true;
end;


procedure Tdm.taSellListAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure Tdm.taSellItemAfterDelete(DataSet: TDataSet);
begin
  with dm, dmCom do
    begin
      tr.CommitRetaining;
      if WorkMode='ALLSELL' then taSellList.Refresh;
      if WorkMode='SELL' then taCurSell.Refresh;
      if WorkMode='SELLCH' then quCurSellCheck.Refresh;

      if (WorkMode='SELL')or(WorkMode='ALLSELL') then
       begin
         taSellItem.DisableControls;
         taSellItem.DisableScrollEvents;
         taSellItem.First;
         while not taSellItem.Eof do
         begin
         if taSellItemART.AsString=SellItemSZ then taSellItem.Refresh;
         taSellItem.Next;
         end;
         taSellItem.EnableControls;
         taSellItem.EnableScrollEvents;
       end;

      if WorkMode='SELLCH' then  ReOpenDataSet(taSellItem);
    end;
end;

procedure Tdm.quInOutPriceBeforeOpen(DataSet: TDataSet);
begin
{  ErrSick:=true;
  if taA2.State in [dsEdit, dsInsert] then
   taA2.Post;}

  quInOutPrice.Params[0].AsInteger  := taSElSINVID.AsInteger;
  quInOutPrice.Params[1].AsInteger  := taSListDEPID.AsInteger;
  quInOutPrice.Params[2].AsInteger:= taA2Art2Id.AsInteger;
//  ErrSick:=false;
end;

procedure Tdm.quInOutPriceAfterOpen(DataSet: TDataSet);
var st1,st2:string;
    i:integer;
begin
 if WorkMode = 'SINV' then
   if not taA2ART2ID.IsNull then
   begin
    if quInOutPricePRICE1.AsFloat<0.001 then st1:='00'
    else st1:=quInOutPricePRICE1.AsString;
    if quInOutPricePRICE2.AsFloat<0.001 then st2:='00'
    else st2:=quInOutPricePRICE2.AsString;
    i:=1;
    while ((st1[i]<>',')and(st1[i]<>'.'))and (i<=length(st1)) do inc(i);
    if i>length(st1) then st1:=st1+',00�'
                     else
    if  i=length(st1)-1 then st1:=st1+'0�'
     else st1:=st1+'�';
    fmSInv.Label20.Caption:= st1;
    i:=1;
    while ((st2[i]<>',')and(st2[i]<>'.'))and (i<=length(st2)) do inc(i);
    if i>length(st2) then st2:=st2+',00�'
                     else
    if  i=length(st2)-1 then st2:=st2+'0�'
     else st2:=st2+'�';
    fmSInv.Label21.Caption:= st2;
   end;                                               
end;

procedure Tdm.taSRetBeforeEdit(DataSet: TDataSet);
begin
 if WorkMode='SRET' then
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.quCurSellCheckBeforeOpen(DataSet: TDataSet);
begin
  quCurSellCheck.ParamByName('DepId').AsInteger:=SDepId;
  quCurSellCheck.ParamByName('EmpID').AsInteger:=dmcom.User.UserId;
  quCurSellCheck.ParamByName('CHECKNO').AsInteger:=D_CHECK0;
end;

procedure Tdm.quCurSellCheckCalcFields(DataSet: TDataSet);
begin
  quCurSellCheckCASS.AsFloat:=quCurSellCheckCost.AsFloat-quCurSellCheckRCost.AsFloat;
end;

procedure Tdm.quDInvItemBeforeOpen(DataSet: TDataSet);
begin
 quDInvItem.Params[0].AsInteger:=taDListSInvId.AsInteger
end;

procedure Tdm.quDInvItemCalcFields(DataSet: TDataSet);
var i: integer;
    qs, ws: string;
begin
  quDInvItemRecNo.AsInteger:=DataSet.RecNo;
  qs:=quDInvItemSQ.AsString;
//  ws:=ReplaceStr(quDInvItemSW.AsString, '.', ',');
  with dm2 do
    for i:=0 to slDepDepId.Count-1 do
      begin
   //     DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
        DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      end;
end;

procedure Tdm.quDInvItemBeforeDelete(DataSet: TDataSet);
begin
 if not CheckForUIDWHCalc then raise Exception.Create('�������������� �� ��������. ���� �������� ������. ��� ������ ������� Esc');
 if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
 if DataSet.Tag=0 then
 begin
  DelConf(DataSet);
  SellItemSZ:= taSellItemSZ.AsString;
 end
end;

procedure Tdm.quDInvItemAfterDelete(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
  taDList.Refresh;
//  ReOpenDataSets([quDInvItem]);
  DataSet.Tag:=0;
end;

procedure Tdm.quDInvItemAfterPost(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
  taDList.Refresh;
  //ReOpenDataSets([quDInvItem]);
  //quDInvItem.Locate('SITEMID',SITEMid,[]);
  //quDInvItem.Refresh;
end;

procedure Tdm.quDInvItemBeforeEdit(DataSet: TDataSet);
begin
  if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  SitemID := quDInvItemSITEMID.AsInteger;
  SELID := quDInvItemSELID.AsInteger;  
end;

procedure Tdm.quUidWhSumGrBeforeOpen(DataSet: TDataSet);
var s,st: string;
begin
  with TpFIBDataSet(DataSet) do
    begin
      if (Length(dmCom.FilterArt)>0) and (dmCom.FilterArt[1]='*') and (Length(dmCom.FilterArt)=1) then
        begin
          s:=ReverseString(Copy(dmCom.FilterArt, 2, MAXINT));
        end
      else
        begin
          s:=dmCom.FilterArt;
        end;
      //12.08.02
      Prepare;

      SetDepFilter(Params, SDepId);

      if UIDWHType=3 then
       begin
        ParamByName('USERID').AsInteger:=UserDefault;
        dm2.quSameUid.ParamByName('USERID').AsInteger:=UserDefault;
       end
      else
       begin
        ParamByName('USERID').AsInteger:=dmCom.UserId;
        dm2.quSameUid.ParamByName('USERID').AsInteger:=dmCom.UserId;
       end;
      FilterArtb:=false;
      if dmCom.FilterArt = '' then
      begin
        ParamByName('ART1').AsString := '!!';
        ParamByName('ART2').AsString := '��';
      end
      else begin
        if not art_z (s,st) then
         begin
           ParamByName('ART1').AsString := s;
           ParamByName('ART2').AsString := s+'��';
         end
                           else
          begin
           ParamByName('ART1').AsString := st;
           FilterArtb:=true;
          end;
      end;
      dmCom.SetArtFilter(Params);
     if (DataSet.Name <>'quUIDWH') then
      begin
        if lSellSelect then
          begin
            ParamByName('T1').AsInteger := 2;
            ParamByName('T2').AsInteger := 2;
            ParamByName('T3').AsInteger := 2;
            ParamByName('T4').AsInteger := 2;
            ParamByName('T5').AsInteger := 2;
          end
        else if lOSinvSelect then
          begin
            ParamByName('T1').AsInteger := -2;
            ParamByName('T2').AsInteger := -2;
            ParamByName('T3').AsInteger := -2;
            ParamByName('T4').AsInteger := -2;
            ParamByName('T5').AsInteger := -2;
          end
        else if AllUIDWH then
          begin
            ParamByName('T1').AsInteger := -3;
            ParamByName('T2').AsInteger := 100;
            ParamByName('T3').AsInteger := 0;
            ParamByName('T4').AsInteger := 0;
            ParamByName('T5').AsInteger := 0;
          end
         else if lSRETSelect then
          begin
            ParamByName('T1').AsInteger := 4;
            ParamByName('T2').AsInteger := 4;
            ParamByName('T3').AsInteger := 5;
            ParamByName('T4').AsInteger := 5;
            ParamByName('T5').AsInteger := 5;
          end
           else
          begin
            ParamByName('T1').AsInteger := -1;
            ParamByName('T2').AsInteger := 1;
            ParamByName('T3').AsInteger := 5;
            ParamByName('T4').AsInteger := 7;
            ParamByName('T5').AsInteger := 8;
          end
      end;
    end;
end;
procedure Tdm.quUidWhSumGrCalcFields(DataSet: TDataSet);
var i: integer;
    qs, ws, ps, p2s: string;
begin
  qs:=quUidWhSumGrGR_SAC.AsString;
  ws:=ReplaceStr(quUidWhSumGrGR_SAw.AsString, '.', ',');
  ps:=ReplaceStr(quUidWhSumGrGR_SAp.AsString, '.', ',');
  p2s:=ReplaceStr(quUidWhSumGrGR_SAp2.AsString, '.', ',');

  with dm2 do
    for i:=0 to slGrId.Count-1 do
      begin
        DataSet.FieldByName('RW_'+slGrId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
        DataSet.FieldByName('RQ_'+slGrId[i]).AsInteger:=StrToInt(EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
        DataSet.FieldByName('RP_'+slGrId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, ps, [';'])));
        DataSet.FieldByName('RP2_'+slGrId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, p2s, [';'])));
      end;
end;

procedure Tdm.taSellListBeforeEdit(DataSet: TDataSet);
begin
 if (dmcom.User.ALLSeLL=0) then
   raise Exception.Create('��� ���� �� �������������� �������');

end;

procedure Tdm.taDListCalcFields(DataSet: TDataSet);
begin
 if (taDListISCLOSED.AsInteger=0) then
 begin
  taDListOutCost1.AsFloat:=taDListCost1.AsFloat;
  taDListInCost1.AsFloat:=0;
  taDListOutCost0.AsFloat:=taDListCost0.AsFloat;
  taDListInCost0.AsFloat:=0;
  taDListOutQuid.AsInteger:=taDListQUID.AsInteger;
  taDListInQuid.AsInteger:=0;
  taDListOutSCost.AsFloat:=taDListSCost.AsFloat;
  taDListInSCost.AsFloat:=0;
  taDListOutWuid.AsFloat:=taDListWuid.AsFloat;
  taDListInWuid.AsFloat:=0;
  taDListOutSn.AsInteger:=1;
  taDListInSn.AsInteger:=0;
 end
 else
 begin
  taDListOutCost1.AsFloat:=0;
  taDListINCost1.AsFloat:=taDListCost1.AsFloat;
  taDListOutCost0.AsFloat:=0;
  taDListINCost0.AsFloat:=taDListCost0.AsFloat;
  taDListOutQuid.AsInteger:=0;
  taDListInQuid.AsInteger:=taDListQUID.AsInteger;
  taDListOutSCost.AsFloat:=0;
  taDListInSCost.AsFloat:=taDListSCost.AsFloat;
  taDListOutWuid.AsFloat:=0;
  taDListInWuid.AsFloat:=taDListWuid.AsFloat;
  taDListOutSn.AsInteger:=0;
  taDListInSn.AsInteger:=1;
 end
end;

procedure Tdm.quD_WHAfterRefresh(DataSet: TDataSet);
begin
//  ReopenDataSets([dm2.quD_WH_T]);
end;

procedure Tdm.FillComboBoxes(cmbxComp, cmbxMat, cmbxGood, cmbxIns, cmbxCountry, cmbxAtt1, cmbxAtt2 : TDBComboBoxEh);

  procedure Fill(cmbx: TDBComboboxEh; sl: TStringList; Dict : integer);
  var s: string;
      i: integer;
  begin
    if cmbx=NIL then eXit;
    LoadArtSL(Dict);
    with cmbx do
    begin
      if (ItemIndex<>-1)and(Items.Count<>0) then s:=cmbx.Items[ItemIndex];
      Items.Clear;
      Items.Assign(sl);
      i:=Items.IndexOf(s);
      if i<>-1 then ItemIndex:=i;
    end;
  end;
  
begin
  Fill(cmbxComp, slProd, COMP_DICT);
  Fill(cmbxMat, slMat, MAT_DICT);
  Fill(cmbxIns, slIns, INS_DICT);
  Fill(cmbxGood, slGood, GOOD_DICT);
  Fill(cmbxCountry, slCountry, COUNTRY_DICT);
  Fill(cmbxAtt1, slAtt1, ATT1_DICT);
  Fill(cmbxAtt2, slAtt2, ATT2_DICT);
end;

procedure Tdm.taORetListCalcFields(DataSet: TDataSet);
begin
 if taORetListISCLOSED.AsInteger=0 then
 begin
  taORetListSNC.AsInteger:=0;
  taORetListSNO.AsInteger:=1;
  taORetListCostC.AsFloat:=0;
  taORetListCostO.AsFloat:=taORetListCOST.AsFloat;
  taORetListQC.AsInteger:=0;
  taORetListQO.AsInteger:=taORetListQ.AsInteger;
  taORetListWC.AsFloat:=0;
  taORetListWO.AsFloat:=taORetListW.AsFloat;
 end
 else
 begin
  taORetListSNC.AsInteger:=1;
  taORetListSNO.AsInteger:=0;
  taORetListCostC.AsFloat:=taORetListCOST.AsFloat;
  taORetListCostO.AsFloat:=0;
  taORetListQC.AsInteger:=taORetListQ.AsInteger;
  taORetListQO.AsInteger:=0;
  taORetListWC.AsFloat:=taORetListW.AsFloat;
  taORetListWO.AsFloat:=0;
 end
end;

procedure Tdm.quSupRCalcFields(DataSet: TDataSet);
begin
  quSupRFName.AsString:=quSupRSName.AsString+' - '+quSupRName.AsString;
end;

procedure Tdm.taA2AfterRefresh(DataSet: TDataSet);
begin
 if (WorkMode='SINV') and (quArt.Active) then quArt.Refresh;
end;

procedure Tdm.taSRetListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    if DDepFromId=-1 then
    begin
      ByName['DEPID1'].AsInteger:=-MAXINT;
      ByName['DEPID2'].AsInteger:=MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger:=DDepFromId;
      ByName['DEPID2'].AsInteger:=DDepFromId;
    end;
    ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(BeginDate);
    ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
  end;
end;

procedure Tdm.quADictAfterPost(DataSet: TDataSet);
var sqlstr: string;
    FieldsRes:Variant;

begin
  sqlstr := 'select Art2ID from Art2 where D_ArtID =  ' + quADictD_ARTID.AsString;
  try
  FieldsRes := ExecSelectSQL(sqlstr, quTmp2);
  if FieldsRes=null then
  begin
    if quA2Dict.State<>dsInsert then
       quA2Dict.Insert;
     quA2Dict.Post;
  end
  finally
     Finalize(FieldsRes);
  end;
  dmCom.tr.CommitRetaining;
end;

procedure Tdm.taA2InsBeforeDelete(DataSet: TDataSet);
begin
 if taA2InsMAIN.AsInteger=1 then
 begin
   MessageDialog('������� �������� ������� ������', mtInformation, [mbOk], 0);
   SysUtils.Abort;
 end
 else if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then
    SysUtils.Abort;
end;

procedure Tdm.taA2InsAfterRefresh(DataSet: TDataSet);
begin
 if taA2.Active then taA2.Refresh;
//  if quUIDWH.Active then quUIDWH.Refresh;
end;

procedure Tdm.taA2InsAfterPost(DataSet: TDataSet);
var i:integer;
begin
 dmCom.tr.CommitRetaining;
 if quUIDWH.Active then
 begin
//  quUIDWH.Refresh;
  i:=quUIDWHSITEMID.AsInteger;
  ReOpenDataSet(quUIDWH);
  quUIDWH.Locate('SITEMID', i, []);
 end;
end;

procedure Tdm.quMaxUIDWHBeforeOpen(DataSet: TDataSet);
var st: string;
    ch:char;
begin
  with TpFIBDataSet(DataSet) do
   begin
    if UIDWHType=3 then ParamByName('USERID').AsInteger:=UserDefault
       else  ParamByName('USERID').AsInteger:=dmCom.UserId;
    SetDepFilter(Params, SDepId);

    if lSellSelect then ParamByName('I_T').AsString:=' and u.T=2 '
      else if (not AllUIDWH) and (not lSRetSelect) and (not lOSinvSelect) then  ParamByName('I_T').AsString:=' and ((u.T between -1 and 1 ) or (u.T=5) or (u.T=7) or (u.t=8))'
        else if lSRetSelect then  ParamByName('I_T').AsString:=' and (u.T=4 or u.T=5) '
          else if lOSinvSelect then  ParamByName('I_T').AsString:=' and (u.T=-2) '
           else ParamByName('I_T').AsString:=' ';

     if lMultiSelect and (SD_MatID<>'')then ParamByName('I_MAT').AsString:=' and a2.d_MatId in ('+SD_MatID+')'
      else if dmcom.D_MATID='*' then ParamByName('I_MAT').AsString:=' '
           else ParamByName('I_MAT').AsString:=' and a2.d_MatId= '#39+dmcom.D_MATID+#39;


     if (dmCom.FilterArt<>'') then
      begin
        if art_z(dmCom.FilterArt,st) then  ParamByName('I_ART').AsString:=' and a2.Art like '#39+st+#39
        else ParamByName('I_ART').AsString:=' and a2.Art like '#39+dmCom.FilterArt+'%'#39
      end
       else  ParamByName('I_ART').AsString:=' ';

     if (FilterArt2<>'') then ParamByName('I_ART2').AsString:=' and a2.Art2 like '#39+FilterArt2+'%'#39
     else  ParamByName('I_ART2').AsString:=' ';

      if lMultiSelect and (SD_GoodID<>'')then ParamByName('I_GOOD').AsString:=' and a2.d_GoodId in ('+SD_GoodID+')'
      else if dmcom.D_GOODID='*' then ParamByName('I_GOOD').AsString:=' '
           else ParamByName('I_GOOD').AsString:=' and a2.d_GoodId ='#39+dmcom.D_GOODID+#39;


      if lMultiSelect and (SD_CompID<>'')then ParamByName('I_COMP').AsString:=' and a2.d_CompId in ('+SD_CompID+')'
      else if dmcom.D_COMPID=-1 then ParamByName('I_COMP').AsString:=' '
           else ParamByName('I_COMP').AsString:=' and a2.d_CompId = '+inttostr(dmcom.D_compid);

      if lMultiSelect and (SD_SupID<>'')then ParamByName('I_SUPID0').AsString:=' and u.SupId0 in ('+SD_SupID+') '
      else if dmcom.D_SUPID=-1 then ParamByName('I_SUPID0').AsString:=' '
           else ParamByName('I_SUPID0').AsString:=' and u.SupId0 ='+inttostr(dmcom.D_SUPID);

      if lMultiSelect and (SD_Note1<>'')then ParamByName('I_GOODSID1').AsString:=' and u.Goodsid1 in ('+SD_Note1+') '
      else if dmcom.D_Note1=-1 then ParamByName('I_GOODSID1').AsString:=' '
           else ParamByName('I_GOODSID1').AsString:=' and u.Goodsid1 ='+inttostr(dmcom.D_Note1);

      if lMultiSelect and (SD_Note2<>'')then ParamByName('I_GOODSID2').AsString:=' and u.Goodsid2 in ('+SD_Note2+') '
      else if dmcom.D_Note2=-1 then ParamByName('I_GOODSID2').AsString:=' '
           else ParamByName('I_GOODSID2').AsString:=' and u.Goodsid2 ='+inttostr(dmcom.D_Note2);


      if lMultiSelect and (SD_Att1<>'')then ParamByName('I_ATT1').AsString:=' and a2.Att1 in ('+SD_Att1+') '
      else if dmcom.D_Att1Id=ATT1_DICT_ROOT then ParamByName('I_ATT1').AsString:=' '
           else ParamByName('I_ATT1').AsString:=' and a2.Att1 = '+inttostr(dmcom.D_Att1Id);

      if lMultiSelect and (SD_Att2<>'')then ParamByName('I_ATT2').AsString:=' and a2.Att2 in ('+SD_Att2+') '
      else if dmcom.D_Att2Id=ATT1_DICT_ROOT then ParamByName('I_ATT2').AsString:=' '
           else ParamByName('I_ATT2').AsString:=' and a2.Att2 = '+inttostr(dmcom.D_Att2Id);

      if lMultiSelect and (UidwhSZ<>'')then ParamByName('I_SZ').AsString:=' and u.sz in ('+UidwhSZ+') '
      else if dmcom.UIDWHSZ_='*' then ParamByName('I_SZ').AsString:=' '
           else ParamByName('I_SZ').AsString:=' and u.sz = '#39+dmcom.UIDWHSZ_+#39;

      ch:=DecimalSeparator;
      DecimalSeparator:='.';

      if dm.IFilterCost then ParamByName('I_COST').AsString:=' and FIF(a2.UnitId, u.Price*u.W, u.Price) between '+FloatToStr(dm.ICOst1)+' and '+FloatToStr(dm.ICost2)
      else ParamByName('I_COST').AsString:=' ';

      if dm.IFilterW then ParamByName('I_W').AsString:=' and u.W between '+FloatToStr(dm.Iw1)+' and '+FloatToStr(dm.Iw2)
      else ParamByName('I_W').AsString:=' ';

      DecimalSeparator:=ch;      
  end;
end;

procedure Tdm.quMaxUIDWHCalcFields(DataSet: TDataSet);
var s: string;
begin
  s:='';
  with dataset do
  case FieldByName('T').AsInteger of
      0: case FieldByName('IType').AsInteger of
             2: if  FieldByName('Sdate0').AsDateTime<=dm.BeginDate then s:='������ �� ������ �������'
                else  s:='������ �� �����������';
             5: s:='�������';
             9: case FieldByName('IsClosed').AsInteger of
                 0: s:='������, ��������� �������';
                 1: s:='������, ��������� �������';
                end;
             10: s:='���������� �������';
             21: s:='�������';
             else  case FieldByName('IsClosed').AsInteger of
                       0: s:='������ �� ������ �������, ��������� �� �������';
                       1: s:='������ �� ������ �������';
                     end
           end;

      1,-1: begin
           case FieldByName('IType').AsInteger of
               1: s:='������ �� �����������';
                {���� ���� �����.����������� � ��� ���� ��������� � ����� �������, �� ��� ��� - ����� ��� ������}
               2: if (FieldByName('Sdate').IsNull or (FieldByName('Sdate').AsDateTime<=dm.BeginDate))
                  then s:='������ �� ������ �������'
                  else s:='���������� �����������';

               6:
                  begin
                    s:='������� �� �����������';
                  end;
               8: s:='������� �� ������� �����������';
               9: case FieldByName('IsClosed').AsInteger of
                   0: s:='������, ��������� �������';
                   1: s:='������, ��������� �������';
                  end;
               10: s:='���������� �������';
               21: s:='�������';
             end;

           case FieldByName('IsClosed').AsInteger of
               0: s:=s+', ��������� �� �������';
             end;
         end;
      2: case FieldByName('IsCLosed').AsInteger of
      {��� ������ ��������� ����� � ������� �� �������� � ����������� ������}
             0: s:='�������, ����� �� �������';
             1,2: s:='�������';
           end;
      3: case FieldByName('IsClosed').AsInteger of
            0: s:='������� �������, ��������� �� �������';
            1: s:='������� �������';
          end;
      4: case FieldByName('IsClosed').AsInteger of
            1: s:='������� �����������';
          end;
      5: case FieldByName('IsClosed').AsInteger of
            0: s:='������� �����������, ��������� �� �������';
          end;
    end;

  Dataset.FieldByName('Memo').AsString:=s;
end;

procedure Tdm.quMaxUIDWHAfterPost(DataSet: TDataSet);
begin
  with dmCom do
    begin
      tr.CommitRetaining;
      tr1.CommitRetaining;
    end;
end;

procedure Tdm.quMaxUIDWHAfterOpen(DataSet: TDataSet);
begin
  if Assigned(fmUIDWH) then
     with fmUIDWH do
       begin
        laTime.Caption:='����� ����������: '+TimeToStr(dmCom.GetServerTime -UIDWHTime);
       end;
  UIDWHTime:=-1;
end;

procedure Tdm.quMaxUIDWHBeforePost(DataSet: TDataSet);
begin
  quMaxUIDWHUserId.AsInteger:=dmCom.UserId;
end;

procedure Tdm.quUIDWHAPPLDEP_QChange(Sender: TField);
begin
 if quUIDWH.State in [dsEdit] then IsChangeApplDep:=true;
end;

procedure Tdm.quUIDWHBeforeClose(DataSet: TDataSet);
begin
 PostDataSet(quUIDWH);
end;

procedure Tdm.quMaxUIDWHBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(quMaxUIDWH);
end;

procedure Tdm.taOptListBeforeEdit(DataSet: TDataSet);
begin
 if taOptListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm.quCountApplBeforeOpen(DataSet: TDataSet);
begin
 if dsUIDWH.DataSet=quUIDWH then
 begin
  if not quUIDWHD_ARTID.IsNull then begin
   quCountAppl.ParamByName('ARTID').AsInteger:=quUIDWHD_ARTID.AsInteger;
   quCountAppl.ParamByName('SZ').AsString:=quUIDWHSZ.AsString;
  end else begin
   quCountAppl.ParamByName('ARTID').AsInteger:=-1000;
   quCountAppl.ParamByName('SZ').AsString:='';
  end
 end
 else
 begin
  if not quMaxUIDWHD_ARTID.IsNull then begin
   quCountAppl.ParamByName('ARTID').AsInteger:=quMaxUIDWHD_ARTID.AsInteger;
   quCountAppl.ParamByName('SZ').AsString:=quMaxUIDWHSZ.AsString;
  end else begin
   quCountAppl.ParamByName('ARTID').AsInteger:=-1000;
   quCountAppl.ParamByName('SZ').AsString:='';   
  end
 end
end;

procedure Tdm.quCountApplCalcFields(DataSet: TDataSet);
var i:Integer;
begin
 quCountApplStSQ.AsString:='';
 if not quCountApplSQ.IsNull then
 with qutmp do
 begin
  close;
  sql.Text:='select ssname from d_dep where d_depid<>-1000 and isdelete<>1';
  ExecQuery;
  i:=1;
  while not eof do
  begin
   quCountApplStSQ.AsString:=trim(quCountApplStSQ.AsString)+' '+
    trim(Fields[0].AsString)+'-'+ExtractWord(i,quCountApplSQ.AsString,[';'])+'; ';
   quCountApplStSQ1.AsString:=trim(quCountApplStSQ1.AsString)+' '+
    trim(Fields[0].AsString)+'-'+ExtractWord(i,quCountApplSQ1.AsString,[';'])+'; ';
   Next;
   inc(i);
  end;
  close;
  Transaction.CommitRetaining;
 end
end;

procedure Tdm.quUIDWHAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(quCountAppl);
end;

procedure Tdm.quUIDWHAPPL_QChange(Sender: TField);
begin
 if quUIDWH.State in [dsEdit] then IsChangeAppl:=true;
end;

procedure Tdm.taSRetListCalcFields(DataSet: TDataSet);
begin
 if taSRetListISCLOSED.AsInteger=0 then begin
  taSRetListCountOpen.AsInteger:=1;
  taSRetListCountClose.AsInteger:=0;
  taSRetListQOpen.AsInteger:=taSRetListR_COUNT.AsInteger;
  taSRetListQClose.AsInteger:=0;
  taSRetListWOpen.AsFloat:=taSRetListW.AsFloat;
  taSRetListWClose.AsFloat:=0;
  taSRetListCostOpen.AsFloat:=taSRetListCOST.AsFloat;
  taSRetListCostClose.AsFloat:=0;
 end
 else begin
  taSRetListCountOpen.AsInteger:=0;
  taSRetListCountClose.AsInteger:=1;
  taSRetListQOpen.AsInteger:=0;
  taSRetListQClose.AsInteger:=taSRetListR_COUNT.AsInteger;
  taSRetListWOpen.AsFloat:=0;
  taSRetListWClose.AsFloat:=taSRetListW.AsFloat;
  taSRetListCostOpen.AsFloat:=0;
  taSRetListCostClose.AsFloat:=taSRetListCOST.AsFloat;
 end;
end;

procedure Tdm.quInOutPriceAfterRefresh(DataSet: TDataSet);
var st1,st2:string;
    i:integer;
begin
 if not taA2ART2ID.IsNull then
 begin
  if quInOutPricePRICE1.AsFloat<0.001 then st1:='00'
  else st1:=quInOutPricePRICE1.AsString;
  if quInOutPricePRICE2.AsFloat<0.001 then st2:='00'
  else st2:=quInOutPricePRICE2.AsString;
  i:=1;
  while ((st1[i]<>',')and(st1[i]<>'.'))and (i<=length(st1)) do inc(i);
  if i>length(st1) then st1:=st1+',00�'
                   else
  if  i=length(st1)-1 then st1:=st1+'0�'
   else st1:=st1+'�';
  fmSInv.Label20.Caption:= st1;
  i:=1;
  while ((st2[i]<>',')and(st2[i]<>'.'))and (i<=length(st2)) do inc(i);
  if i>length(st2) then st2:=st2+',00�'
                   else
  if  i=length(st2)-1 then st2:=st2+'0�'
   else st2:=st2+'�';
  fmSInv.Label21.Caption:= st2;
 end;
end;

procedure Tdm.taA2AfterCancel(DataSet: TDataSet);
begin
  if (taA2Price1.AsFloat<>taA2Price1.OldValue) or
     (taA2Price2.AsFloat<>taA2Price2.OldValue) or
     (taA2Art2.AsString<>taA2Art2.OldValue) then
    begin
      RefreshAllSEl(taA2Art2Id.AsInteger);
    end;
  if A2Inserted then
   begin
     Art2Id:=taA2Art2Id.AsInteger;
     ReopenDataSets([taA2]);
     taA2.Locate('ART2ID', Art2Id, []);
   end;
end;
function Tdm.checkInv_RUseDep(DepID: integer): boolean;
begin
// true - ���� ����� ���� ��� ���� ��������
//false - ���� ��� ������� ������� ��� ���� ����
  if (dmCom.Dep[DepID].R_UseDep = 1) and (DepID <> SelfDepId) then Result := false
  else Result := true;
end;

{$IFDEF TRIALPERIOD}

const
{$IFDEF MOSCOW}
  cTrialEnd = #164#165#186#165#165#186#166#164#164#161; // 01.11.2005
{$ENDIF}
{$IFDEF CLIENT_TEST}
  cTrialEnd = #164#165#186#164#165#186#166#164#164#161; // 01.01.2005
{$ENDIF}
{$IFDEF CLIENT_KEMEROVO}
  cTrialEnd = #165#161#186#165#165#186#166#164#164#161; // 15.11.2005
{$ENDIF}
//  cTrialEnd = #164#165#186#164#165#186#166#164#164#162; // 01.01.2006

cTrialEnd = #164#165#186#164#161#186#166#164#164#162; // 01.05.2006

procedure Tdm.CheckTrialPeriod(Date: TDateTime);
var
  TrialEnd : TDateTime;
  s : string;
  i: integer;
begin
  try
    s := '';
    for i:=1 to Length(cTrialEnd) do
      s := s+Chr(Ord(cTrialEnd[i]) xor $94); //148
    TrialEnd := StrToDateTime(s);
  except

  end;
  if Date > TrialEnd then
  begin
    SysUtils.Abort;
  end;
end;
{$ENDIF}

procedure Tdm.taOptListBeforePost(DataSet: TDataSet);
begin
  if taOptListSDATE.IsNull then raise Exception.Create('���������� ������ ���� ���������');
  {$IFDEF TRIALPERIOD}
  CheckTrialPeriod(taOptListSDATE.AsDateTime);
  {$ENDIF}
end;

procedure Tdm.taSRetListBeforePost(DataSet: TDataSet);
var
  i: integer;
begin
  if taSRetListSDATE.IsNull then raise Exception.Create('���������� ������ ���� ���������');
  if taSRetListSDATE.OldValue <> Null then
     if (GetSYear(taSRetListSDATE.OldValue) <> GetSYear(taSRetListSDATE.NewValue)) then
     with dm, quTmp do
     begin
       close;
       SQL.Text:='SELECT max(sn) FROM SINV '+
                 'WHERE ITYPE=7 '+
                 ' AND DepFromId='+taSRetListDepFromId.AsString+' AND FYEAR(SDATE)='+GetSYear(taSRetListSDATE.NewValue);
       ExecQuery;
       if Fields[0].IsNull then i:=1
       else i:=Fields[0].AsInteger+1;
       Close;
       Transaction.CommitRetaining;
       taSRetListSN.AsInteger:=i;
       MessageDialog('��������! �� �������� ��� �������� ���������! ����� ��������� ��� ������� � '+
                      VarToStr(taSRetListSN.OldValue) + ' ��  '+ VarToStr(taSRetListSN.NewValue)+
                      ' � ������������ � ���������� ���������� ���������� ��������� �� ������� ���.', mtInformation, [mbOk], 0);
     end;
  {$IFDEF TRIALPERIOD}
  CheckTrialPeriod(taSRetListSDATE.AsDateTime);
  {$ENDIF}
end;

procedure Tdm.taPrOrdBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=SDepId;
          ByName['DEPID2'].AsInteger:=SDepId;
        end;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(calc_beginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(EndDate);
    end;
end;

function Tdm.CheckForSpecSymb(instr: string):boolean;
begin
  if (Length(instr) > 0) then
  begin
    if (instr[1] in ['�' .. '�']) or
       (instr[1] in ['�' .. '�']) or
       (instr[1] in ['0' .. '9']) or
       (instr[1] in ['A' .. 'Z']) or
       (instr[1] in ['a' .. 'z']) or
       ((Ord(instr[1]) = 045) and (Length(instr) = 1)) then
      Result := True
    else
      Result := False;
  end
  else
    Result := False;
end;

function Tdm.CheckForUIDWHCalc: boolean;
var Res: Variant;
begin
  Res := ExecSelectSQL('SELECT UIDWHCALC FROM D_REC', dmServ.quTmp);
  if (not VarIsNull(Res)) and (Integer(Res) > 0) then
    RESULT := FALSE
  else RESULT := TRUE;
end;
procedure Tdm.quDstBeforePost(DataSet: TDataSet);
begin
  if not CheckForUIDWHCalc then raise Exception.Create('�������������� �� �� ��������. ���� �������� ������. ��� ������ ������� Esc');
end;

procedure Tdm.quDInvItemBeforePost(DataSet: TDataSet);
begin
  if not CheckForUIDWHCalc then raise Exception.Create('�������������� �� �� ��������. ���� �������� ������. ��� ������ ������� Esc');
end;

procedure Tdm.quDstBeforeDelete(DataSet: TDataSet);
begin
 if not CheckForUIDWHCalc then raise Exception.Create('�������������� �� �� ��������. ���� �������� ������. ��� ������ ������� Esc');

end;

procedure Tdm.taSRetBeforePost(DataSet: TDataSet);
begin
//showmessage('taSRetBeforePost begin');
if not CheckForUIDWHCalc then raise Exception.Create('�������������� �������� �� ��������. ���� �������� ������. ��� ������ ������� Esc');
//showmessage('taSRetBeforePost end');
end;

function Tdm.CheckForWOrder(SITEMID:Integer): boolean;
var sqlstr: string;
    Res: Variant;
begin
  sqlstr := ' select p.PRORD ' +
            ' from prsi ps, PRORDITEM pi, prord p ' +
            ' where ps.SITEMID = ' + IntToStr(SitemId) + ' and ' +
            '       pi.PRORDITEMID= ps.PRORDITEMID and ' +
            '       p.PRORDID = pi.PRORDID and   ' +
            '       p.ITYPE = 12  ';

  Res := ExecSelectSQL(sqlstr, quTmp);
  if  VarIsNull(Res) then
    RESULT := True
  else
    RESULT := false;
end;


procedure Tdm.taSListTWChange(Sender: TField);
begin
  if not (taSListPAYTYPEID.AsInteger in [7, 8]) then
  begin
    taSListcomissionscheme.AsInteger := 0;
  end;
end;

procedure Tdm.AnlzSelDepClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    fmAnlzSelEmp.quAnlzSelEmp.Active:=False;
    TMenuItem(Sender).Checked:=True;
    SDepId:= TMenuItem(Sender).Tag;
    SDep:= TMenuItem(Sender).Caption;
    fmAnlzSelEmp.laDep.Caption:=dm.SDep;
    fmAnlzSelEmp.quAnlzSelEmp.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

// ������� ��������� ������� ������� � ���������, � ������� �������� ����.���� ������ � �������
// � ���������� �������� ��������� ������:
// 1. ������� �� � ������� ������,
// 2. ������� ��, �� �������� �������
// 3. �� ��������� �� � �� ��������� ������, ������ � ����� �������������� ���������


function Tdm.pCreatePrord(invid:string):boolean;
var
  prsi_dep:integer;
  buttonSelected : Integer;
  prord:integer;
begin
  prsi_dep:=0;
  result:=true;
  if CenterDep then
  begin
    with dm.quTmp do
    begin // �������� �-�� ������� � ���������, � ������� �������� ����.���� ������ � �������
      close;
      SQL.Text := 'select result from COUNT_ITEM_PRORD_DEP('+dm.taDListSINVID.AsString+')';
      ExecQuery;
      prsi_dep := Fields[0].AsInteger;
      close;
    end;
  end;

  if (prsi_dep>0) then
  begin
    buttonSelected := MessageDialog('��������! ��������� ���� �� ������ � ������� ��������! ������������ ������� �� ����������?', mtWarning, [mbYes, mbNo, mbCancel], 0);
    if buttonSelected =mrYes then
    begin
      with dm, dm.quTmp do
      begin
        close;
        SQL.Text := 'execute procedure CREATEPRORD_DEP('+dm.taDListSINVID.AsString+')';
        ExecQuery;
        Transaction.CommitRetaining;
        Close;

        if taDListDEPID.AsInteger =1 then   // ���� ������ ������ ��� ������
           PActAfterDInv:=true;  // ���������� ������� �������� �������
        result:=true;
      end;
    end;
    if buttonSelected =mrCancel then
    begin
      result:=false;
    end;
    if buttonSelected =mrNo then
    begin
      result:=true;
    end;
  end;
end;

procedure Tdm.taSElPRICEHACKChange(Sender: TField);
begin
  if WorkMode = 'SINV' then
  begin
    if taSListADDNDSTOPRICE.AsInteger = 1 then
    begin
      taSElPRICENDS.AsFloat := SimpleRoundTo(taSElPRICEHACK.AsFloat * taSListPNDS.AsFloat * 0.01, -2);

      taSElPRICE.AsFloat := taSElPRICEHACK.AsFloat +  taSElPRICENDS.AsFloat;

    end else

    begin
      taSElPRICENDS.AsFloat := 0;

      taSElPRICE.AsFloat := taSElPRICEHACK.AsFloat;
    end;
  end;
end;

procedure Tdm.taA2PRICEHACKChange(Sender: TField);
begin
  if WorkMode = 'SINV' then
  begin
    if taSListADDNDSTOPRICE.AsInteger = 1 then
    begin
      taa2PRICENDS.AsFloat := SimpleRoundTo(taa2PRICEHACK.AsFloat * taSListPNDS.AsFloat * 0.01, -2);

      taa2PRICE1.AsFloat := taa2PRICEHACK.AsFloat +  taa2PRICENDS.AsFloat;

    end else

    begin
      taa2PRICENDS.AsFloat := 0;

      taa2PRICE1.AsFloat := taa2PRICEHACK.AsFloat;
    end;
  end;
end;

initialization
  AppDebug:=False;
  AppUpdate:=False;

end.
