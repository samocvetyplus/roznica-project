object fmUIDStore: TfmUIDStore
  Left = 64
  Top = 89
  Width = 1032
  Height = 748
  Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object gridUIDStore: TDBGridEh
    Left = 0
    Top = 69
    Width = 1024
    Height = 631
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsrUIDStore
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridUIDStoreGetCellParams
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 86
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 40
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 46
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'GR'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 90
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1093#1086#1076'.|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOST'
        Footers = <>
        Title.Caption = #1042#1093#1086#1076'.|'#1089#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'SINVNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1086#1089#1090#1072#1074#1082#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSNUM'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1048#1079#1083#1080#1096#1082#1080
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'RETNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'SINVCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1086#1089#1090#1072#1074#1082#1072
        Title.EndEllipsis = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'SURPLUSCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1048#1079#1083#1080#1096#1082#1080
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DINVCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1055
        Title.EndEllipsis = True
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Width = 38
      end
      item
        EditButtons = <>
        FieldName = 'OPTRETCOST'
        Footers = <>
        Title.Caption = #1055#1088#1080#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMFROM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1055
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'SELLNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1088#1086#1076#1072#1078#1072'|'#1056#1086#1079#1085#1080#1094#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1055#1088#1086#1076#1072#1078#1072'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESNUM'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1050#1086#1083'-'#1074#1086'|'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'DINVFROMCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1055
        Title.EndEllipsis = True
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'OPTSELLCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1086#1076#1072#1078#1080'|'#1054#1087#1090'.'
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SINVRETCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1091
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESCOST'
        Footers = <>
        Title.Caption = #1056#1072#1089#1093#1086#1076'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'RESNUM'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 51
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'RESCOST'
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|c'#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Width = 48
      end
      item
        ButtonStyle = cbsEllipsis
        Color = 16776176
        EditButtons = <>
        FieldName = 'F'
        Footers = <>
        Title.Caption = #1060#1072#1082#1090'.|'#1085#1072#1083#1080#1095#1080#1077
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 48
        OnEditButtonClick = gridUIDStoreColumns15EditButtonClick
      end
      item
        Color = 16776176
        EditButtons = <>
        FieldName = 'FCOST'
        Footers = <>
        Title.Caption = #1060#1072#1082#1090'.|'#1089#1090#1086#1080#1084#1086#1089#1090#1100
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'ARTID'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'DEPID'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1024
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      Spacing = 1
      Left = 906
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 1024
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object Label2: TLabel
      Left = 172
      Top = 8
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
      Transparent = True
    end
    object Label3: TLabel
      Left = 362
      Top = 8
      Width = 86
      Height = 13
      Caption = #1053#1077' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
      Transparent = True
    end
    object cmbxDep: TDBComboBoxEh
      Left = 44
      Top = 4
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxDepChange
    end
    object cmbxGrMat: TDBComboBoxEh
      Left = 212
      Top = 4
      Width = 105
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 1
      Visible = True
      OnChange = cmbxGrMatChange
    end
    object chbxBadRecord: TDBCheckBoxEh
      Left = 347
      Top = 8
      Width = 12
      Height = 12
      Action = acShowBadArt
      Flat = True
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 700
    Width = 1024
    Height = 19
    Panels = <>
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 24
    Top = 128
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acShowId: TAction
      Caption = 'acShowId'
      ShortCut = 8260
      OnExecute = acShowIdExecute
    end
    object acShowBadArt: TAction
      Caption = #1053#1077' '#1089#1086#1086#1090#1074#1077#1090#1089#1090#1074#1080#1103
      OnExecute = acShowBadArtExecute
    end
  end
end
