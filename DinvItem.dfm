object fmDInvItem: TfmDInvItem
  Left = 278
  Top = 300
  HelpContext = 100222
  Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1074#1085#1091#1090#1088#1077#1085#1085#1077#1075#1086' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
  ClientHeight = 588
  ClientWidth = 1081
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1081
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 195
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      BtnCaption = #1041#1080#1088#1082#1080
      Caption = #1041#1080#1088#1082#1080
      DropDownMenu = pmprint
      Hint = #1054#1090#1084#1077#1095#1077#1085#1085#1099#1077
      ImageIndex = 67
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siPrintClick
      SectionName = 'Untitled (0)'
    end
    object siUID2: TSpeedItem
      BtnCaption = #1048#1079#1076#1077#1083#1080#1103
      Caption = #1048#1079#1076#1077#1083#1080#1103
      Hint = #1048#1079#1076#1077#1083#1080#1103'|'
      ImageIndex = 42
      Spacing = 1
      Left = 259
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1058#1077#1082#1091#1097#1080#1077' '#13#10#1094#1077#1085#1099
      Caption = #1058#1077#1082#1091#1097#1080#1077' '#1094#1077#1085#1099
      Hint = #1058#1077#1082#1091#1097#1080#1077' '#1094#1077#1085#1099'|'
      Spacing = 1
      Left = 323
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      BtnCaption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Hint = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      Spacing = 1
      Left = 387
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 299
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 1081
    Height = 68
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 100
      Height = 13
      Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 200
      Top = 8
      Width = 87
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 200
      Top = 28
      Width = 86
      Height = 13
      Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laCost: TLabel
      Left = 340
      Top = 28
      Width = 74
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBTextTo: TDBText
      Left = 491
      Top = 28
      Width = 58
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'COST'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 290
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPFROM'
      DataSource = dm.dsDList
    end
    object DBText3: TDBText
      Left = 290
      Top = 28
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPTO'
      DataSource = dm.dsDList
    end
    object Label3: TLabel
      Left = 340
      Top = 48
      Width = 96
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1087#1088'. '#1094#1077#1085#1072#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText4: TDBText
      Left = 436
      Top = 48
      Width = 113
      Height = 13
      Alignment = taRightJustify
      DataField = 'SCOST'
      DataSource = dm.dsDList
    end
    object laCostD: TLabel
      Left = 560
      Top = 28
      Width = 101
      Height = 13
      Caption = #1057#1091#1084#1084#1072' '#1074' '#1086#1087#1090'. '#1094#1077#1085#1072#1093':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbtCostD: TDBText
      Left = 665
      Top = 28
      Width = 44
      Height = 13
      AutoSize = True
      DataField = 'SCOSTD'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laCost0: TLabel
      Left = 340
      Top = 8
      Width = 74
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBTextFr: TDBText
      Left = 495
      Top = 8
      Width = 54
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'COST0'
      DataSource = dm.dsDList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 81
      Height = 21
      Cursor = crDrag
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsDList
      TabOrder = 0
    end
    object DBCheckBox1: TDBCheckBox
      Left = 676
      Top = 8
      Width = 117
      Height = 17
      Caption = #1053#1077' '#1089#1086#1079#1076'. '#1087#1088#1080#1082#1072#1079
      DataField = 'NOTPR'
      DataSource = dm.dsDList
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object dbSdate: TDBEditEh
      Left = 117
      Top = 26
      Width = 60
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsDList
      EditButtons = <>
      Enabled = False
      ReadOnly = True
      TabOrder = 2
      Visible = True
    end
    object btSdate: TBitBtn
      Left = 176
      Top = 26
      Width = 20
      Height = 22
      TabOrder = 3
      OnClick = btSdateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 569
    Width = 1081
    Height = 19
    Panels = <
      item
        Width = 300
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 109
    Width = 1081
    Height = 32
    Align = alTop
    TabOrder = 3
    object Label2: TLabel
      Left = 96
      Top = 8
      Width = 52
      Height = 13
      Caption = #1048#1085'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 376
      Top = 8
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edUid: TEdit
      Left = 156
      Top = 5
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edUidKeyDown
    end
    object edart: TEdit
      Left = 445
      Top = 5
      Width = 121
      Height = 21
      TabOrder = 1
      OnKeyDown = edartKeyDown
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 141
    Width = 1081
    Height = 428
    Align = alClient
    Color = clInfoBk
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsDInvItem
    Flat = True
    FooterColor = clMoneyGreen
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 4
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    OnKeyDown = dg1KeyDown
    OnKeyUp = dg1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'recNo'
        Footer.FieldName = 'recNo'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #8470
        Width = 27
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1048#1076'.'#1085#1086#1084#1077#1088
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1048#1079#1076#1077#1083#1080#1077'|'#1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1053#1072#1080#1084'.'
        Title.EndEllipsis = True
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'Q0'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1042#1077#1089
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1056#1072#1079#1084'.'
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1054#1042
        Width = 31
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1052#1072#1090'.'
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'UNIT'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1045#1048
        Width = 29
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1080#1079#1076#1077#1083#1080#1103'|'#1048#1079#1075'.'
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
        Title.Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077'|'#1057#1091#1084#1084#1072
      end
      item
        EditButtons = <>
        FieldName = 'DEPFROMPRICE'
        Footers = <>
        Title.Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077'|DepFromPrice'
        Title.EndEllipsis = True
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'DEPPRICE'
        Footers = <>
        Title.Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077'|DepPrice'
        Title.EndEllipsis = True
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'PPRICE'
        Footers = <>
        Title.Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077'|'#1042' '#1087#1088#1080#1093'.'#1094#1077#1085#1072#1093
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'OPTPRICE'
        Footers = <>
        Title.Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077'|'#1042' '#1086#1087#1090'.'#1094#1077#1085#1072#1093
        Title.EndEllipsis = True
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
        Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1050#1086#1083'-'#1074#1086
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'RW'
        Footers = <>
        Title.Caption = #1054#1089#1090#1072#1090#1082#1080'|'#1042#1077#1089
        Width = 46
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pmprint: TPopupMenu
    Left = 328
    Top = 224
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    OnSavePlacement = siExitClick
    OnRestorePlacement = FormActivate
    StoredValues = <>
    Left = 112
    Top = 272
  end
  object AcList: TActionList
    Left = 784
    Top = 16
    object acOpen: TAction
      Caption = 'acOpen'
      OnExecute = acOpenExecute
    end
    object acClose: TAction
      Caption = 'acClose'
      OnExecute = acCloseExecute
    end
  end
end
