unit uUtils;

interface

uses Windows, Forms, Dialogs, SysUtils, Controls, Graphics, GraphUtil, ShlObj;

type

  TScanCodeClass = (scUnknown, scDiscountCard, scArticle);

  TCharSet = set of Char;

  TNotifyDialog = class(TForm)
  end;





function GetIniFileName: string;
function GetIniPathName: string;
function GetCurrentDirectory: string;

function ParseScanCode(ScanCode: string; var CodeClass: TScanCodeClass; var Code: string): Boolean;

function GetTempDir:string;

function RemoveNotChars(Value:string;Chars:TCharSet):string;
function RemoveChars(Value:string; Chars:TCharSet):string;
function ReplaceChars(Value:string; Chars:TCharSet; S:string):string;
function ReplaceNotChars(Value:string; Chars:TCharSet; S:string):string;
function GetSubString(Value: string; StartIndex: Integer; EndIndex: Integer): string;

function ErrorOkMessage(Msg: string): TModalResult;
function InformationMessage(Msg: string): TModalResult;
function QuestionYesNoMessage(Msg: string): Boolean;
function QuestionYesNoCancelMessage(Msg: string): TModalResult;

procedure GrayScaleImageList(Source, Target: TImageList);
procedure ShadowImageList(Source, Target: TImageList);

function CreateNotifyDialog(Msg: string): TNotifyDialog;

implementation

function CreateNotifyDialog(Msg: string): TNotifyDialog;
var
  Form: TForm;
begin
  Form := CreateMessageDialog(Msg, mtInformation, []);
  Form.Caption := '����������';
  Form.FormStyle := fsStayOnTop;
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Result := TNotifyDialog(Form);
end;

function QuestionYesNoCancelMessage(Msg: string): TModalResult;
var
  Form: TForm;
begin
  Form := CreateMessageDialog(Msg, mtConfirmation,	[mbYes, mbNo, mbCancel]);
  Form.Caption := '������';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Result := Form.ShowModal;
end;

function QuestionYesNoMessage(Msg: string): Boolean;
var
  Form: TForm;
begin
  Form := CreateMessageDialog(Msg, mtConfirmation,	[mbYes, mbNo]);
  Form.Caption := '������';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Result := Form.ShowModal = mrYes;
end;


function ErrorOkMessage(Msg: string): TModalResult;
var
  Form: TForm;
begin
  Form := CreateMessageDialog(Msg, mtError,	[mbOK]);
  Form.Caption := '������';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Form.ShowModal;
  Result := mrOK;
end;

function InformationMessage(Msg: string): TModalResult;
var
  Form: TForm;
begin
  Form := CreateMessageDialog(Msg, mtInformation, [mbOK]);
  Form.Caption := '����������';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Form.ShowModal;
  Result := mrOK;
end;

{
procedure GrayScaleBitmap(Bitmap: TBitmap);
var
  X, Y: Integer;
  Color: TColor;
  BitmapWidth: Integer;
  BitmapHeight: Integer;
  Hue, Luminance, Saturation: WORD;
begin
  if Bitmap <> nil then
  begin
    BitmapWidth := Bitmap.Width;
    BitmapHeight := Bitmap.Height;
    for X := 0 to BitmapWidth do
    for Y := 0 to BitmapHeight do
    begin
      Color := Bitmap.Canvas.Pixels[X, Y];
      if Color <> clWhite then
      begin
        ColorRGBToHLS(Color, Hue, Luminance, Saturation);
        Color := ColorHLSToRGB(0, Luminance, 0);
        Bitmap.Canvas.Pixels[X, Y] := Color;
      end
      else Bitmap.Canvas.Pixels[X, Y] := cl3DLight;
    end;
  end;
end;}

procedure GrayScaleBitmap(Bitmap: TBitmap);
var
  X, Y: Integer;
  Color: TColor;
  BitmapWidth: Integer;
  BitmapHeight: Integer;
  R, G, B, M: Integer;
begin
  if Bitmap <> nil then
  begin
    BitmapWidth := Bitmap.Width;
    BitmapHeight := Bitmap.Height;
    for X := 0 to BitmapWidth do
    for Y := 0 to BitmapHeight do
    begin
      Color := Bitmap.Canvas.Pixels[X, Y];
      R := GetRValue(Color);
      G := GetGValue(Color);
      B := GetBValue(Color);
      M := (R + G + B) div 3;
      Color := RGB(M, M, M);
      Bitmap.Canvas.Pixels[X, Y] := Color;
    end;
  end;
end;





procedure ShadowBitmap(Bitmap: TBitmap);
var
  X, Y: Integer;
  Color: TColor;
  BitmapWidth: Integer;
  BitmapHeight: Integer;
begin
  if Bitmap <> nil then
  begin
    BitmapWidth := Bitmap.Width;
    BitmapHeight := Bitmap.Height;
    for X := 0 to BitmapWidth do
    for Y := 0 to BitmapHeight do
    begin
      Color := Bitmap.Canvas.Pixels[X, Y];
      if Color <> clWhite then
      begin
        Color := GetShadowColor(Color);
        Bitmap.Canvas.Pixels[X, Y] := Color;
      end
      else Bitmap.Canvas.Pixels[X, Y] := cl3DLight;
    end;
  end;
end;

procedure GrayScaleImageList(Source, Target: TImageList);
var
  i: Integer;
  Bitmap: TBitmap;
begin
  Target.Clear;
  for i := 0 to Source.Count - 1 do
  begin
    Bitmap := TBitmap.Create;
    Source.GetBitmap(i, Bitmap);
    GrayScaleBitmap(Bitmap);
    Target.AddMasked(Bitmap, clWhite);
    Bitmap.Free;
  end;
end;

procedure ShadowImageList(Source, Target: TImageList);
var
  i: Integer;
  Bitmap: TBitmap;
begin
  Target.Clear;
  for i := 0 to Source.Count - 1 do
  begin
    Bitmap := TBitmap.Create;
    Source.GetBitmap(i, Bitmap);
    ShadowBitmap(Bitmap);
    Target.AddMasked(Bitmap, clWhite);
    Bitmap.Free;
  end;
end;


function GetTempDir:string;
var
  TempDir:Pointer;
  TempDirLength:DWORD;
begin
  TempDirLength := GetTempPath(0,Nil);
  GetMem(TempDir,TempDirLength);
  GetTempPath(TempDirLength,TempDir);
  Result := StrPas(TempDir);
  FreeMem(TempDir);
  if Result[Length(Result)] <> '\' then Result := Result + '\';
end;

function ReplaceChars(Value:string; Chars:TCharSet; S:string):string;
var
  i:Integer;
begin                 
  Result := '';
  for i := 1 to Length(Value) do
  if Value[i] in Chars then Result := Result + S
  else Result := Result + Value[i];
end;

function ReplaceNotChars(Value:string; Chars:TCharSet; S:string):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
  if not (Value[i] in Chars) then Result := Result + S
  else Result := Result + Value[i];
end;


function RemoveChars(Value:string; Chars:TCharSet):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
  if not (Value[i] in Chars) then Result := Result + Value[i];
end;

function RemoveNotChars(Value:string;Chars:TCharSet):string;
var
  i:Integer;
begin
  Result := '';
  for i := 1 to Length(Value) do
  if Value[i] in Chars then Result := Result + Value[i];
end;

function GetSubString(Value: string; StartIndex: Integer; EndIndex: Integer): string;
var
  i: Integer;
begin
  Result := '';
  if StartIndex < 1 then StartIndex := 1;
  if EndIndex > Length(Value) then EndIndex := Length(Value);
  for i := StartIndex to EndIndex do
  begin
    Result := Result + Value[i];
  end;
end;


function ParseScanCode(ScanCode: string; var CodeClass: TScanCodeClass; var Code: string): Boolean;
var
  i: Integer;
  Symbol: Char;
  ByteSymbol: Byte;
  AScanCode: string;
begin

  Code := '';
  CodeClass := scUnknown;

  AScanCode := RemoveNotChars(ScanCode, ['0'..'9','-']);

  if AScanCode = ScanCode then
  begin
    i := 1;
    Code := '';

    while (i <= Length(ScanCode)) and (ScanCode[i] = '0') do Inc(i);

    while (i <= Length(ScanCode)) do
    begin
      Code := Code + ScanCode[i];
      Inc(i);
    end;

    ScanCode := Code;

    Code := '';

    if ScanCode <> '' then
    begin
      Symbol := ScanCode[1];
      if Symbol in ['1'..'9'] then
      begin
        ByteSymbol := Ord(Symbol) - Ord('0');
        if ByteSymbol <= Byte(Ord(High(TScanCodeClass))) then
        begin
          CodeClass := TScanCodeClass(ByteSymbol);
          i := 2;
          while (i <= Length(ScanCode)) and (ScanCode[i] = '0') do Inc(i);

          while (i <= Length(ScanCode)) do
          begin
            Code := Code + ScanCode[i];
            Inc(i);
          end;

          if Code = '' then CodeClass := scUnknown;
        end;
      end;
    end;
  end;
  Result := CodeClass <> scUnknown;
end;

function GetSpecialFolderLocation(nFolder : integer) : string;
var
  aPidl: PItemIDList;
  fLinkDir : string;
begin
  if SUCCEEDED(SHGetSpecialFolderLocation(0, nFolder, aPidl)) then
  begin
    SetLength(fLinkDir, MAX_PATH);
    SHGetPathFromIDList(aPidl, PChar(fLinkDir));
    SetLength(fLinkDir, StrLen(PChar(fLinkDir)));
    result := fLinkDir;
  end;
end;

function GetIniFileName: string;
var
  FilePath: string;
  ApplicationName: string;
begin
  ApplicationName := ExtractFileName(Application.ExeName);
  SetLength(ApplicationName, Length(ApplicationName) - 4);
  FilePath := GetSpecialFolderLocation(CSIDL_APPDATA);
  FilePath := FilePath + '\' + ApplicationName;
  if not DirectoryExists(FilePath) then CreateDir(FilePath);
  Result := FilePath + '\' + ApplicationName + '.ini';
end;

function GetIniPathName: string;
var
  FilePath: string;
  ApplicationName: string;
begin
  ApplicationName := ExtractFileName(Application.ExeName);
  SetLength(ApplicationName, Length(ApplicationName) - 4);
  FilePath := GetSpecialFolderLocation(CSIDL_APPDATA);
  FilePath := FilePath + '\' + ApplicationName;
  if not DirectoryExists(FilePath) then CreateDir(FilePath);
  Result := FilePath + '\';
end;


function GetCurrentDirectory: string;
var
  Buffer: array[0..1024] of Char;
begin
  Windows.GetCurrentDirectory(1024, @Buffer);
  Result := StrPas(@Buffer);
  Result := IncludeTrailingBackslash(Result);
end;



end.
