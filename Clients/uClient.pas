unit uClient;

interface

uses

  uView, uInterFace;


type

  TClient = class(TObject)
  private
    FView: TView;
    FClient: IClient;
    function GetID: Integer;
    procedure SetID(const Value: Integer);
  public
    constructor Create(Client: IClient);
    destructor Destroy; override;
    property View: TView read FView;
    property ID: Integer read GetID write SetID;
  end;

implementation

{ TClient }

constructor TClient.Create(Client: IClient);
var
  AView: IView;
begin
  FClient := Client;
  FClient.QueryInterface(IID_View, AView);
  FView := TView.Create(AView);
end;

destructor TClient.Destroy;
begin
  FView.Free;
  FClient := nil;
  inherited;
end;

function TClient.GetID: Integer;
begin
  Result := FClient.ID;
end;

procedure TClient.SetID(const Value: Integer);
begin
  FClient.ID := Value;
end;

end.
