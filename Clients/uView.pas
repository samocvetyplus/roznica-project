unit uView;

interface

uses

  SysUtils,
  uInterface;



type

  TView = class(TObject)
  private
    FView: IView;
  public
    constructor Create(View: IView);
    destructor Destroy; override;
    function Execute: Boolean;
    procedure Show;
  end;


implementation

{ TView }

constructor TView.Create(View: IView);
begin
  FView := View;
end;

destructor TView.Destroy;
begin
  FView := nil;
  inherited Destroy;
end;

function TView.Execute: Boolean;
begin
  Result := FView.Execute;
end;

procedure TView.Show;
begin
  FView.Show;
end;

end.
