unit uClientBalance;

interface

uses

  uView, uInterFace;

type

  TClientBalance = class(TObject)
  private
    FView: TView;
    FClientBalance: IClientBalance;
    function GetID: Integer;
    procedure SetID(const Value: Integer);
  public
    constructor Create(ClientBalance: IClientBalance);
    destructor Destroy; override;
    property View: TView read FView;
    property ID: Integer read GetID write SetID;
  end;

implementation

{ TClientBalance }

constructor TClientBalance.Create(ClientBalance: IClientBalance);
var
  AView: IView;
begin
  FClientBalance := ClientBalance;
  FClientBalance.QueryInterface(IID_View, AView);
  FView := TView.Create(AView);
end;

destructor TClientBalance.Destroy;
begin
  FView.Free;
  FClientBalance := nil;
  inherited;
end;

function TClientBalance.GetID: Integer;
begin
  Result := FClientBalance.ID;
end;

procedure TClientBalance.SetID(const Value: Integer);
begin
  FClientBalance.ID := Value;
end;

end.
