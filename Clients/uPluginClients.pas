unit uPluginClients;

interface

uses

  Windows, Forms, SysUtils,
  uInterface, uClient, uClientBalance, uClients, pFIBDatabase, messages, dialogs;

type

  TPluginClients = class(TObject)
  private
    Handle: THandle;
    Plugin: IPlugin;
    FActive: Boolean;
    FClient: TClient;
    FClients: TClients;
    FClientBalance: TClientBalance;
  public
    constructor Create(DataBase: TpFIBDataBase);
    destructor Destroy; override;
    property Active: Boolean read FActive;
    property Client: TClient read FClient;
    property Clients: TClients read FClients;
    property ClientBalance: TClientBalance read FClientBalance;
  end;


  TGetPlugin = function: IPlugin;

var
  PluginClients: TPluginClients;

implementation


{ TPluginClients }

constructor TPluginClients.Create(DataBase: TpFIBDataBase);
var
  AClients: IClients;
  AClient: IClient;
  AClientBalance: IClientBalance;
  GetPlugin: TGetPlugin;
begin
  if DataBase <> nil then
  begin
    Handle := SafeLoadLibrary('Clients.dll');
    if Handle <> 0 then
    begin
      GetPlugin := GetProcAddress(Handle, 'GetPlugin');
      if Assigned(GetPlugin) then
      begin
        Plugin := GetPlugin;
        if Plugin <> nil then
        begin
          try
            Plugin.Initialize(Application.Handle, PChar(DataBase.DatabaseName));
            Plugin.QueryInterface(IID_Client, AClient);
            FClient := TClient.Create(AClient);
            Plugin.QueryInterface(IID_ClientBalance, AClientBalance);
            FClientBalance := TClientBalance.Create(AClientBalance);
            Plugin.QueryInterface(IID_Clients, AClients);
            FClients := TClients.Create(AClients);
            FActive := True;
          except
          end;
        end;
      end;
    end;
  end;
  
  if not Active then
  begin
    if Client <> nil then Client.Free;
    if Clients <> nil then Clients.Free;
    if ClientBalance <> nil then FClientBalance.Free;
    if Plugin <> nil then Plugin := nil;
    if Handle <> 0 then
    begin
      FreeLibrary(Handle);
      Handle := 0;
    end;
  end;
end;

destructor TPluginClients.Destroy;
begin
  if Handle <> 0 then
  begin
    if Client <> nil then FClient.Free;
    if Clients <> nil then FClients.Free;
    if ClientBalance <> nil then FClientBalance.Free;
    if Plugin <> nil then Plugin := nil;
    FreeLibrary(Handle);
  end;
  inherited Destroy;
end;


end.
