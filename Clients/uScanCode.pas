unit uScanCode;

interface

uses Windows, Messages, Forms, SysUtils, TypInfo;

type

  TApplicationHook = class(TObject)
  protected
    function Hook(var Message: TMessage): Boolean;
  public
    constructor Create;
    destructor Destroy; override;
  end;


implementation

uses Controls;

type

  TOnScanCode = procedure (ScanCode: string) of object;

var
  WM_SCANCODE: UINT;

constructor TApplicationHook.Create;
begin
  Application.HookMainWindow(Hook);
end;

destructor TApplicationHook.Destroy;
begin
  Application.UnhookMainWindow(Hook);
  inherited Destroy;
end;

function TApplicationHook.Hook(var Message: TMessage): Boolean;
var
  Data: PCopyDataStruct;
  Form: TForm;
  ScanCode: string;
  MethodAddress: Pointer;
  Method: TMethod;
  Handle: THandle;
  Control: TWinControl;
begin
  Result := False;
  if Message.Msg = WM_COPYDATA then
  begin
    Data := TWMCopyData(Message).CopyDataStruct;
    if Data^.dwData = WM_SCANCODE then
    begin
      Handle := Message.WParam;
      Control := FindControl(Handle);
      if (Control <> nil) and (Control is TForm) then
      begin
        Form := TForm(Control);
        ScanCode := StrPas(Data^.lpData);
        MethodAddress := Form.MethodAddress('OnScanCode');

        if MethodAddress <> nil then
        begin
          Method.Code := MethodAddress;
          Method.Data := Form;
          TOnScanCode(Method)(ScanCode);
          Result := True;
        end;
      end;
    end;
  end;
end;

initialization

  WM_SCANCODE := RegisterWindowMessage('WM_SCANCODE');

end.


