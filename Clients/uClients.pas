unit uClients;

interface

uses

  uView, uInterFace;


type

  TClients = class(TObject)
  private
    FView: TView;
    FClients: IClients;
    function GetID: Integer;
  public
    constructor Create(Clients: IClients);
    destructor Destroy; override;
    property View: TView read FView;
    property ID: Integer read GetID;
  end;

implementation

{ TClients }

constructor TClients.Create(Clients: IClients);
var
  AView: IView;
begin
  FClients := Clients;
  FClients.QueryInterface(IID_View, AView);
  FView := TView.Create(AView);
end;

destructor TClients.Destroy;
begin
  FView.Free;
  FClients := nil;
  inherited;
end;

function TClients.GetID: Integer;
begin
  Result := FClients.ID;
end;

end.
