unit uInterface;

interface

const

IID_Plugin: TGUID = '{D0CE4A58-6389-45EB-B983-0E3233A6E185}';
IID_Client: TGUID = '{E6846A0C-2EE0-4A7B-BBDC-A6985EA15372}';
IID_Clients: TGUID = '{BE7F15EA-2002-4D04-969C-19A3A5090BBE}';
IID_View: TGUID = '{B73ED33C-6085-4F6A-B9E5-983C472E9D49}';
IID_ClientBalance: TGUID = '{B6335705-F59F-43DF-B34F-E01357DF1919}';

type

IPlugin = interface
['{D0CE4A58-6389-45EB-B983-0E3233A6E185}']
  procedure Initialize(ApplicationHandle: Integer; DataBaseName: PChar);
end;

IView = interface
['{B73ED33C-6085-4F6A-B9E5-983C472E9D49}']
   function GetParent: Integer;
   procedure SetParent(Parent: Integer);
   procedure Show;
   procedure Hide;
   function Execute: Boolean;
   function GetCaption: PChar;
   property Parent: Integer read GetParent write SetParent;
   property Caption: PChar read GetCaption;
end;

IClient = interface
['{E6846A0C-2EE0-4A7B-BBDC-A6985EA15372}']
  function GetID: Integer;
  procedure SetID(Value: Integer);
  property ID: Integer read GetID write SetID;
end;

IClientBalance = interface
['{B6335705-F59F-43DF-B34F-E01357DF1919}']
  function GetID: Integer;
  procedure SetID(Value: Integer);
  property ID: Integer read GetID write SetID;
end;

IClients = interface
['{BE7F15EA-2002-4D04-969C-19A3A5090BBE}']
  function GetID: Integer;
  property ID: Integer read GetID;  
end;

implementation

end.
