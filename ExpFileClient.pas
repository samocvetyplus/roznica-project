unit ExpFileClient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, rxToolEdit;

type
  TfmExpFileClient = class(TForm)
    Label1: TLabel;
    btImp: TButton;
    fledImp: TFilenameEdit;
    procedure btImpClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btImpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure fledImpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmExpFileClient: TfmExpFileClient;
  ImpError : TStringList;

implementation

uses rxStrUtils, data, comdata;

{$R *.dfm}

procedure TfmExpFileClient.btImpClick(Sender: TObject);
var Path:string;
    ftext:textfile;
    Tmp: string;
    EndWord:string;
    nodcard, nameclient, strit, flat:string;
    key:char;
    stritall:string;
    oldpos_:integer;
    //������� �����

 {������� ������� � ����� � ������ � � �����}
 Function DelFromWord32 (st :string):string;
 var i:integer;
 begin
  i:=1;
  while (i<=length(st))and(st[i]=' ') do inc(i);
  delete(st,1,i-1);
  i:=length(st);
  while (i>0)and(st[i]=' ')do dec(i);
  delete(st,i+1,length(st)-i+2);
  result:=st;
 end;

 {������� ����� ���������� �������}
 Function UpCaseWord (st:string):string;
 var i:integer;
 begin
  result:='';
  for i:=1 to length(st) do
   result:=result+UPCASE(st[i]);
 end;

 Function ExtractWordCur(Pos:integer; st:string; ch:char):string;
 var i,k:integer;
     newst:string;
 begin
  i:=1;
  k:=0;
  while (k<>Pos)and(i<=length(st)) do
  begin
   newst:='';
   while (st[i]<>ch)and(i<=length(st)) do
    begin
     newst:=newst+st[i];
     inc(i);
    end;
   inc(i);
   inc(k);
  end;
  if (k=Pos) then result:=newst else result:='';
 end;

 Function ExtractWordLast(pos:integer; st:string; ch:char; var oldpos:integer):string;
 var i,k:integer;
     newst:string;
 begin
  i:=length(st);
  k:=0;
  while (i>0)and (k<>pos) do
  begin
   newst:='';
   while (st[i]<>ch)and(i>0) do
    begin
     newst:=st[i]+newst;
     dec(i);
    end;
   dec(i);
   inc(k);
  end;
  oldpos:=i+1;
  result:=newst;
 end;

begin
  Path:=fledImp.FileName;
//  ImpError.Clear;
  if not FileExists(Path) then
  begin
    ImpError.Add('���� '+Path+' �� ����������!');
    eXit;
  end;
  AssignFile(ftext, Path);
  Reset(ftext);
//  DefaultSeparator := DecimalSeparator;
  Screen.Cursor := crSQLWait;
  key:=chr(9);
  EndWord:='end';
  Readln(ftext,tmp);
  while ExtractWord(1,tmp,[key])<>'end' do
  begin
   nodcard:=ExtractWordCur(*ExtractWord*)(1,tmp,key);
   nameclient:=UpCaseWord(ExtractWordCur(*ExtractWord*)(2,tmp,key));
   stritall:=ExtractWordCur(*ExtractWord*)(3,tmp,key);
   strit:='';
   flat:='';

   if (stritall<>'') and (stritall[1]<>'�') and (stritall[2]<>'.') then
   begin
    flat:=ExtractWordLast(*ExtractWord*)(1,stritall,',',oldpos_);
    if length(flat)>=2 then
    if (flat[1]='�') and (flat[2]='.') then
     flat:=ExtractWordLast(*ExtractWord*)(2,stritall,',',oldpos_);

    strit:=stritall;
    if oldpos_=0 then inc(oldpos_);
    delete(strit,oldpos_,length(strit)-oldpos_+1);
    if (strit='') then
     begin
      strit:=stritall;
      flat:='';
     end;
   end;

   {�������� ������ ��������}
   nodcard:=DelFromWord32(nodcard);
   nameclient:=DelFromWord32(AnsiUpperCase(nameclient));
   strit:=DelFromWord32(AnsiUpperCase(strit));
   flat:=DelFromWord32(flat);

   with dm, qutmp do
   begin
    if not dmcom.tr.Active then dmcom.tr.StartTransaction else dmcom.tr.CommitRetaining;
    close;
    sql.text:='execute procedure InsClient ('''+nodcard+''', '''+nameclient+''','''+
               strit+''','''+flat+''')';
    ExecQuery;
    dmcom.tr.CommitRetaining;
    close;
   end;
   Readln(ftext,tmp);
  end;
  CloseFile(fText);
  Screen.Cursor := crDefault;
end;

procedure TfmExpFileClient.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_ESCAPE:close;
 VK_RETURN: if ActiveControl=fledImp then ActiveControl:=btImp
   else if ActiveControl=btImp then btImpClick(nil);
 end;
end;

procedure TfmExpFileClient.btImpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_ESCAPE:close;
 VK_RETURN: if ActiveControl=fledImp then ActiveControl:=btImp
   else if ActiveControl=btImp then btImpClick(nil);
 end;
end;

procedure TfmExpFileClient.fledImpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_ESCAPE:close;
 VK_RETURN: if ActiveControl=fledImp then ActiveControl:=btImp
   else if ActiveControl=btImp then btImpClick(nil);
 end;
end;

end.
