unit WHArt2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, Menus, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmWHArt2 = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fmstrWHArt2: TFormStorage;
    ibgrA2: TM207IBGrid;
    siUID: TSpeedItem;
    ppWHArt2: TPopupMenu;
    N1: TMenuItem;
    siSZ: TSpeedItem;
    N2: TMenuItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ibgrA2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siUIDClick(Sender: TObject);
    procedure siSZClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHArt2: TfmWHArt2;

implementation

uses comdata, Data, WHA2UID, WHSZ, ServData, Data2, WHUID, M207Proc;

{$R *.DFM}

procedure TfmWHArt2.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmWHArt2.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHArt2.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if dm.WorkMode = 'WH' then
  begin
    ibgrA2.DataSource := dmServ.dsrWHArt2;
    KeyPreview := True;
    OnKeyPress := dm.CloseFormByEsc;
  end;
  ibgrA2.DataSource.DataSet.Active := True;
end;

procedure TfmWHArt2.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHArt2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ibgrA2.DataSource.DataSet.Active :=False;
end;

procedure TfmWHArt2.ibgrA2GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if dm.WorkMode = 'WH' then
  begin
    if Highlight or ibgrA2.ClearHighlight
    then Background := dm2.GetDepColor(dmServ.quWHArt2D_DEPID.AsInteger)
    else Background:=clHighlight;
  end;
  if (Field<>NIL) and (Field.FieldName='ART2') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmWHArt2.siUIDClick(Sender: TObject);
begin
  if dm.WorkMode = 'WH'
  then ShowAndFreeForm(TfmWHUID, Self, TForm(fmWHUID), True, False)
  else ShowAndFreeForm(TfmWHA2UID, Self, TForm(fmWHA2UID), True, False);
end;

procedure TfmWHArt2.siSZClick(Sender: TObject);
begin
  dm.WHSZMode:=2;
  ShowAndFreeForm(TfmWHSZ, Self, TForm(fmWHSZ), True, False)
end;

end.
