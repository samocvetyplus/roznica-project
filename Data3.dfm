object dm3: Tdm3
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 645
  Width = 938
  object quAM: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT AMID, ARTID1, ARTID2, FULLART1, FULLART2, '
      '              MDATE, R_DATE, R_STATE'
      'FROM AM'
      'WHERE ONLYDATE(MDATE) between :MBD and :MED'
      'ORDER BY MDATE')
    BeforeOpen = quAMBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 16
    Top = 8
    object quAMAMID: TIntegerField
      FieldName = 'AMID'
      Origin = 'AM.AMID'
      Required = True
    end
    object quAMARTID1: TIntegerField
      FieldName = 'ARTID1'
      Origin = 'AM.ARTID1'
    end
    object quAMARTID2: TIntegerField
      FieldName = 'ARTID2'
      Origin = 'AM.ARTID2'
    end
    object quAMFULLART1: TFIBStringField
      FieldName = 'FULLART1'
      Origin = 'AM.FULLART1'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quAMFULLART2: TFIBStringField
      FieldName = 'FULLART2'
      Origin = 'AM.FULLART2'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quAMMDATE: TDateTimeField
      FieldName = 'MDATE'
      Origin = 'AM.MDATE'
    end
    object quAMR_DATE: TDateTimeField
      FieldName = 'R_DATE'
      Origin = 'AM.R_DATE'
    end
    object quAMR_STATE: TSmallintField
      FieldName = 'R_STATE'
      Origin = 'AM.R_STATE'
      OnGetText = quAMR_STATEGetText
    end
  end
  object dsAM: TDataSource
    DataSet = quAM
    Left = 16
    Top = 56
  end
  object quA2M: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT A2MID, ART2ID1, ART2ID2, FULLART, '
      '              ART21, ART22,'
      '       MDATE, R_DATE, R_STATE'
      'FROM A2M'
      'WHERE ONLYDATE(MDATE) BETWEEN :MBD AND :MED'
      'ORDER BY MDATE')
    BeforeOpen = quAMBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 60
    Top = 8
    object quA2MA2MID: TIntegerField
      FieldName = 'A2MID'
      Origin = 'A2M.A2MID'
      Required = True
    end
    object quA2MART2ID1: TIntegerField
      FieldName = 'ART2ID1'
      Origin = 'A2M.ART2ID1'
    end
    object quA2MART2ID2: TIntegerField
      FieldName = 'ART2ID2'
      Origin = 'A2M.ART2ID2'
    end
    object quA2MART21: TFIBStringField
      FieldName = 'ART21'
      Origin = 'A2M.ART21'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quA2MART22: TFIBStringField
      FieldName = 'ART22'
      Origin = 'A2M.ART22'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quA2MMDATE: TDateTimeField
      FieldName = 'MDATE'
      Origin = 'A2M.MDATE'
    end
    object quA2MR_DATE: TDateTimeField
      FieldName = 'R_DATE'
      Origin = 'A2M.R_DATE'
    end
    object quA2MR_STATE: TSmallintField
      FieldName = 'R_STATE'
      Origin = 'A2M.R_STATE'
    end
    object quA2MFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'A2M.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsA2M: TDataSource
    DataSet = quA2M
    Left = 60
    Top = 56
  end
  object quRAM: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT AMID'
      'FROM AM'
      'WHERE (R_State<=2 or R_State is null) and'
      '      MDATE between :BD and :ED'
      'ORDER BY AMID')
    BeforeOpen = quRAMBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 16
    Top = 124
    object quRAMAMID: TIntegerField
      FieldName = 'AMID'
      Origin = 'AM.AMID'
      Required = True
    end
  end
  object quRA2M: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT A2MID'
      'FROM A2M'
      'WHERE (R_State<=2 or R_State is null) and'
      '      MDATE between :BD and :ED'
      'ORDER BY A2MID')
    BeforeOpen = quRAMBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 172
    object quRA2MA2MID: TIntegerField
      FieldName = 'A2MID'
      Origin = 'A2M.A2MID'
      Required = True
    end
  end
  object taUIDStoreList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SInv set'
      '  SN=:SN'
      'where SINVID=:SINVID')
    DeleteSQL.Strings = (
      'delete from SInv where SINVID=:SINVID')
    InsertSQL.Strings = (
      'insert into SInv(SINVID, SDATE, NDATE, USERID, SN, itype)'
      'values (:SINVID, :SDATE, :NDATE, :USERID, :SN, 15)')
    RefreshSQL.Strings = (
      
        'select i.SINVID, i.SDATE, i.NDATE, i.USERID, i.SN, e.FIO, i.CRDA' +
        'TE'
      'from SInv i left join D_Emp e on (i.USERID=e.D_EMPID)'
      'where i.sinvid = ?sinvid')
    SelectSQL.Strings = (
      
        'select i.SINVID, i.SDATE, i.NDATE, i.USERID, i.SN, e.FIO, i.CRDA' +
        'TE'
      'from SInv i left join D_Emp e on (i.USERID=e.D_EMPID)'
      'where FYear(i.SDate)=:EYEAR and'
      '      i.ITYPE= 15'
      'order by i.SDate')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = taUIDStoreListBeforeDelete
    BeforeOpen = taUIDStoreListBeforeOpen
    OnNewRecord = taUIDStoreListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1086#1073#1086#1088#1086#1090#1085#1099#1093' '#1074#1077#1076#1086#1084#1086#1089#1090#1077#1081
    Left = 124
    Top = 8
    object taUIDStoreListSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'SINV.SINVID'
      Required = True
    end
    object taUIDStoreListSDATE: TDateTimeField
      DisplayLabel = 'C'
      FieldName = 'SDATE'
      Origin = 'SINV.SDATE'
    end
    object taUIDStoreListNDATE: TDateTimeField
      DisplayLabel = #1087#1086
      FieldName = 'NDATE'
      Origin = 'SINV.NDATE'
    end
    object taUIDStoreListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SINV.USERID'
    end
    object taUIDStoreListSN: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '
      FieldName = 'SN'
      Origin = 'SINV.SN'
    end
    object taUIDStoreListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Origin = 'D_EMP.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taUIDStoreListCRDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103'/'#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
      FieldName = 'CRDATE'
    end
  end
  object dsrUIDStoreList: TDataSource
    DataSet = taUIDStoreList
    Left = 124
    Top = 53
  end
  object taCheckUIDStoreData: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.ID, t.UID, t.DEPID, t.FLAG, d.SName DEPNAME,'
      '       t.WHCOST, t.UIDSTORECOST'
      'from Tmp_CheckData_UIDStore t, D_Dep d'
      'where t.DEPID=d.D_DEPID'
      'order by UID')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 224
    Top = 8
    object taCheckUIDStoreDataID: TIntegerField
      FieldName = 'ID'
      Origin = 'TMP_CHECKDATA_UIDSTORE.ID'
      Required = True
    end
    object taCheckUIDStoreDataUID: TIntegerField
      DisplayLabel = #1048#1076'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'TMP_CHECKDATA_UIDSTORE.UID'
      Required = True
    end
    object taCheckUIDStoreDataDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'TMP_CHECKDATA_UIDSTORE.DEPID'
      Required = True
    end
    object taCheckUIDStoreDataFLAG: TSmallintField
      DisplayLabel = #1060#1083#1072#1075
      FieldName = 'FLAG'
      Origin = 'TMP_CHECKDATA_UIDSTORE.FLAG'
      OnGetText = taCheckUIDStoreDataFLAGGetText
    end
    object taCheckUIDStoreDataDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taCheckUIDStoreDataWHCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      FieldName = 'WHCOST'
      currency = True
    end
    object taCheckUIDStoreDataUIDSTORECOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1074' '#1086#1073#1086#1088#1086#1090#1085#1086#1081' '#1074#1077#1076#1086#1084#1086#1089#1090#1080
      FieldName = 'UIDSTORECOST'
      currency = True
    end
  end
  object dsrCheckUIDStoreData: TDataSource
    DataSet = taCheckUIDStoreData
    Left = 224
    Top = 53
  end
  object quTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 312
    Top = 8
  end
  object taUIDStore: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure stub(0)')
    SelectSQL.Strings = (
      'select s.SINVID, s.ARTID, s.SZ, s.GR, s.DEPID, s.UNITID, '
      
        '  sum(s.ENTRYNUM) ENTRYNUM, sum(s.SINVNUM) SINVNUM,   sum(s.DINV' +
        'NUMFROM) DINVNUMFROM, sum(s.DINVNUM) DINVNUM, sum(s.SELLNUM) SEL' +
        'LNUM, sum(s.RETNUM) RETNUM,'
      
        '  sum(s.OPTSELLNUM) OPTSELLNUM, sum(s.OPTRETNUM) OPTRETNUM, sum(' +
        's.SINVRETNUM) SINVRETNUM,   '
      '  sum(s.RESNUM) RESNUM, sum(s.F) F, a.ART, d.SName DEPNAME,'
      
        '  sum(s.ENTRYCOST) ENTRYCOST, sum(s.SINVCOST) SINVCOST, sum(s.DI' +
        'NVCOST) DINVCOST,'
      '  sum(s.DINVFROMCOST) DINVFROMCOST, sum(s.SELLCOST) SELLCOST,'
      '  sum(s.RETCOST) RETCOST, sum(s.OPTSELLCOST) OPTSELLCOST,'
      '  sum(s.OPTRETCOST) OPTRETCOST, sum(s.SINVRETCOST) SINVRETCOST,'
      
        '  sum(s.RESCOST) RESCOST, sum(s.FCOST) FCOST, sum(s.ACTALLOWANCE' +
        'SNUM) ACTALLOWANCESNUM, sum(s.ACTALLOWANCESCOST) ACTALLOWANCESCO' +
        'ST,'
      '  sum(s.SURPLUSNUM) SURPLUSNUM, sum(s.SURPLUSCOST) SURPLUSCOST'
      'from UID_Store s, D_Art a, D_Dep d'
      'where s.SINVID=:SINVID and'
      '           s.DEPID between :DEPID1 and :DEPID2 and'
      '           s.GR between :GR1 and :GR2 and'
      '           s.ARTID=a.D_ARTID and'
      '           s.DEPID=d.D_DEPID and d.ISDELETE <>1'
      
        'group by s.SINVID, s.ARTID, s.SZ, s.GR, s.DEPID, s.UNITID, a.ART' +
        ', d.SName')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = taUIDStoreBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100'. '#1055#1086' '#1072#1088#1090#1080#1082#1091#1083#1072#1084' '#1080' '#1088#1072#1079#1084#1077#1088#1072#1084
    Left = 92
    Top = 100
    object taUIDStoreSINVID: TIntegerField
      FieldName = 'SINVID'
      Required = True
    end
    object taUIDStoreARTID: TIntegerField
      FieldName = 'ARTID'
      Required = True
    end
    object taUIDStoreSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDStoreGR: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GR'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUIDStoreDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taUIDStoreUNITID: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'UNITID'
      OnGetText = UNITIDGetText
    end
    object taUIDStoreART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUIDStoreDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUIDStoreENTRYNUM: TFIBBCDField
      DisplayLabel = #1042#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'ENTRYNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreSINVNUM: TFIBBCDField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072
      FieldName = 'SINVNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreDINVNUMFROM: TFIBBCDField
      DisplayLabel = #1042#1055' '#1089
      FieldName = 'DINVNUMFROM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreDINVNUM: TFIBBCDField
      DisplayLabel = #1042#1055' '#1085#1072
      FieldName = 'DINVNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreSELLNUM: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072' '#1088#1086#1079#1085#1080#1094#1072
      FieldName = 'SELLNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreOPTSELLNUM: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072' '#1086#1087#1090'.'
      FieldName = 'OPTSELLNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreOPTRETNUM: TFIBBCDField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1086#1087#1090'.'
      FieldName = 'OPTRETNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreSINVRETNUM: TFIBBCDField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      FieldName = 'SINVRETNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreRETNUM: TFIBBCDField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1088#1086#1079#1085#1080#1094#1072
      FieldName = 'RETNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreRESNUM: TFIBBCDField
      DisplayLabel = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      FieldName = 'RESNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreF: TFIBBCDField
      DisplayLabel = #1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077
      FieldName = 'F'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreENTRYCOST: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'ENTRYCOST'
      currency = True
    end
    object taUIDStoreSINVCOST: TFIBFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SINVCOST'
      currency = True
    end
    object taUIDStoreDINVCOST: TFIBFloatField
      DisplayLabel = #1042#1055'+ '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'DINVCOST'
      currency = True
    end
    object taUIDStoreDINVFROMCOST: TFIBFloatField
      DisplayLabel = #1042#1055'- '#1089#1090#1086#1080#1086#1084#1089#1090#1100
      FieldName = 'DINVFROMCOST'
      currency = True
    end
    object taUIDStoreSELLCOST: TFIBFloatField
      DisplayLabel = #1057#1086#1080#1084#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078#1080
      FieldName = 'SELLCOST'
      currency = True
    end
    object taUIDStoreRETCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1088#1086#1079#1085'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'RETCOST'
      currency = True
    end
    object taUIDStoreOPTSELLCOST: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1080' '#1086#1087#1090' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'OPTSELLCOST'
      currency = True
    end
    object taUIDStoreOPTRETCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1086#1087#1090'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'OPTRETCOST'
      currency = True
    end
    object taUIDStoreSINVRETCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SINVRETCOST'
      currency = True
    end
    object taUIDStoreRESCOST: TFIBFloatField
      DisplayLabel = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'RESCOST'
      currency = True
    end
    object taUIDStoreFCOST: TFIBFloatField
      DisplayLabel = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'FCOST'
      currency = True
    end
    object taUIDStoreACTALLOWANCESNUM: TFIBBCDField
      FieldName = 'ACTALLOWANCESNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreACTALLOWANCESCOST: TFIBFloatField
      FieldName = 'ACTALLOWANCESCOST'
    end
    object taUIDStoreSURPLUSNUM: TFIBBCDField
      FieldName = 'SURPLUSNUM'
      Size = 0
      RoundByScale = True
    end
    object taUIDStoreSURPLUSCOST: TFIBFloatField
      FieldName = 'SURPLUSCOST'
      currency = True
    end
  end
  object dsrUIDStore: TDataSource
    DataSet = taUIDStore
    Left = 92
    Top = 144
  end
  object taUidItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update UID_Store set'
      '  F=:F,'
      '  FCOST=:FCOST  '
      'where UIDSTOREID=:UIDSTOREID')
    SelectSQL.Strings = (
      
        'select s.UIDSTOREID, s.SINVID, s.ARTID, s.SZ, s.GR, s.DEPID, s.U' +
        'NITID, '
      '          s.ENTRYNUM, s.SINVNUM,  s.DINVNUMFROM,  s.DINVNUM,'
      '          s.SELLNUM,  s.RETNUM, s.OPTSELLNUM,  s.OPTRETNUM,'
      '          s.SINVRETNUM, s.RESNUM, s.F, a.ART, d.SName DEPNAME,'
      '          s.uid, s.W, s.Q0, s.ENTRYCOST, s.SINVCOST, s.DINVCOST,'
      '          s.DINVFROMCOST, s.SELLCOST, s.RETCOST, s.OPTSELLCOST,'
      '          s.OPTRETCOST, s.SINVRETCOST, s.RESCOST, s.FCOST, '
      '          s.ACTALLOWANCESNUM, s.ACTALLOWANCESCOST, s.SURPLUSNUM,'
      
        '          s.SURPLUSCOST, s.ENTRYNUMCOM, s.SINVNUMCOM, s.DINVNUMF' +
        'ROMCOM,'
      
        '          s.DINVNUMCOM, s.SELLNUMCOM, s.RETNUMCOM, s.OPTSELLNUMC' +
        'OM,'
      
        '          s.OPTRETNUMCOM, s.SINVRETNUMCOM, s.RESNUMCOM, s.ENTRYC' +
        'OSTCOM,'
      
        '          s.SINVCOSTCOM, s.DINVCOSTCOM, s.SELLCOSTCOM, s.RETCOST' +
        'COM,'
      
        '          s.OPTSELLCOSTCOM, s.OPTRETCOSTCOM, s.SINVRETCOSTCOM, s' +
        '.RESCOSTCOM,'
      '          s.SELLCOST2COM, s.RETCOST2COM, s.ACTALLOWANCESNUMCOM,'
      '          s.ACTALLOWANCESCOSTCOM'
      'from UID_Store s, D_Art a, D_Dep d'
      'where s.SINVID=:SINVID and'
      '           s.DEPID between :DEPID1 and :DEPID2 and'
      '           s.GR between :GR1 and :GR2 and'
      '           s.ARTID=a.D_ARTID and'
      '           s.DEPID=d.D_DEPID and d.ISDELETE <>1 and'
      '           s.UID between :UID1 and :UID2 '
      ''
      '')
    AfterClose = taUidItemAfterClose
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = taUidItemBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Description = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
    Left = 256
    Top = 96
    object taUidItemSINVID: TIntegerField
      FieldName = 'SINVID'
      Origin = 'UID_STORE.SINVID'
      Required = True
    end
    object taUidItemARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'UID_STORE.ARTID'
      Required = True
    end
    object taUidItemSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'UID_STORE.SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUidItemGR: TFIBStringField
      FieldName = 'GR'
      Origin = 'UID_STORE.GR'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUidItemDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'UID_STORE.DEPID'
    end
    object taUidItemUNITID: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'UNITID'
      Origin = 'UID_STORE.UNITID'
      OnGetText = UNITIDGetText
    end
    object taUidItemENTRYNUM: TSmallintField
      FieldName = 'ENTRYNUM'
      Origin = 'UID_STORE.ENTRYNUM'
    end
    object taUidItemSINVNUM: TSmallintField
      FieldName = 'SINVNUM'
      Origin = 'UID_STORE.SINVNUM'
    end
    object taUidItemDINVNUMFROM: TSmallintField
      FieldName = 'DINVNUMFROM'
      Origin = 'UID_STORE.DINVNUMFROM'
    end
    object taUidItemDINVNUM: TSmallintField
      FieldName = 'DINVNUM'
      Origin = 'UID_STORE.DINVNUM'
    end
    object taUidItemSELLNUM: TSmallintField
      FieldName = 'SELLNUM'
      Origin = 'UID_STORE.SELLNUM'
    end
    object taUidItemRETNUM: TSmallintField
      FieldName = 'RETNUM'
      Origin = 'UID_STORE.RETNUM'
    end
    object taUidItemOPTSELLNUM: TSmallintField
      FieldName = 'OPTSELLNUM'
      Origin = 'UID_STORE.OPTSELLNUM'
    end
    object taUidItemOPTRETNUM: TSmallintField
      FieldName = 'OPTRETNUM'
      Origin = 'UID_STORE.OPTRETNUM'
    end
    object taUidItemSINVRETNUM: TSmallintField
      FieldName = 'SINVRETNUM'
      Origin = 'UID_STORE.SINVRETNUM'
    end
    object taUidItemRESNUM: TSmallintField
      FieldName = 'RESNUM'
      Origin = 'UID_STORE.RESNUM'
    end
    object taUidItemF: TIntegerField
      FieldName = 'F'
      Origin = 'UID_STORE.F'
    end
    object taUidItemART: TFIBStringField
      FieldName = 'ART'
      Origin = 'D_ART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUidItemDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUidItemUID: TIntegerField
      FieldName = 'UID'
      Origin = 'UID_STORE.UID'
      Required = True
    end
    object taUidItemW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'UID_STORE.W'
      DisplayFormat = '0.##'
    end
    object taUidItemQ0: TFloatField
      FieldName = 'Q0'
      Origin = 'UID_STORE.Q0'
    end
    object taUidItemUIDSTOREID: TFIBIntegerField
      FieldName = 'UIDSTOREID'
    end
    object taUidItemENTRYCOST: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076'. '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'ENTRYCOST'
      currency = True
    end
    object taUidItemSINVCOST: TFIBFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SINVCOST'
      currency = True
    end
    object taUidItemDINVCOST: TFIBFloatField
      DisplayLabel = #1042#1055'+ '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'DINVCOST'
      currency = True
    end
    object taUidItemDINVFROMCOST: TFIBFloatField
      DisplayLabel = #1042#1055'- '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'DINVFROMCOST'
      currency = True
    end
    object taUidItemSELLCOST: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SELLCOST'
      currency = True
    end
    object taUidItemRETCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'RETCOST'
      currency = True
    end
    object taUidItemOPTSELLCOST: TFIBFloatField
      DisplayLabel = #1054#1087#1090' '#1087#1088#1086#1076#1072#1078#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'OPTSELLCOST'
      currency = True
    end
    object taUidItemOPTRETCOST: TFIBFloatField
      DisplayLabel = #1054#1087#1090' '#1074#1086#1079#1074#1088#1072#1090' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'OPTRETCOST'
      currency = True
    end
    object taUidItemSINVRETCOST: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SINVRETCOST'
      currency = True
    end
    object taUidItemRESCOST: TFIBFloatField
      DisplayLabel = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'RESCOST'
      currency = True
    end
    object taUidItemFCOST: TFIBFloatField
      DisplayLabel = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'FCOST'
      currency = True
    end
    object taUidItemACTALLOWANCESNUM: TFIBIntegerField
      FieldName = 'ACTALLOWANCESNUM'
    end
    object taUidItemACTALLOWANCESCOST: TFIBFloatField
      FieldName = 'ACTALLOWANCESCOST'
      currency = True
    end
    object taUidItemSURPLUSNUM: TFIBIntegerField
      FieldName = 'SURPLUSNUM'
    end
    object taUidItemSURPLUSCOST: TFIBFloatField
      FieldName = 'SURPLUSCOST'
      currency = True
    end
    object taUidItemENTRYNUMCOM: TFIBSmallIntField
      FieldName = 'ENTRYNUMCOM'
    end
    object taUidItemSINVNUMCOM: TFIBSmallIntField
      FieldName = 'SINVNUMCOM'
    end
    object taUidItemDINVNUMFROMCOM: TFIBSmallIntField
      FieldName = 'DINVNUMFROMCOM'
    end
    object taUidItemDINVNUMCOM: TFIBSmallIntField
      FieldName = 'DINVNUMCOM'
    end
    object taUidItemSELLNUMCOM: TFIBSmallIntField
      FieldName = 'SELLNUMCOM'
    end
    object taUidItemRETNUMCOM: TFIBSmallIntField
      FieldName = 'RETNUMCOM'
    end
    object taUidItemOPTSELLNUMCOM: TFIBSmallIntField
      FieldName = 'OPTSELLNUMCOM'
    end
    object taUidItemOPTRETNUMCOM: TFIBSmallIntField
      FieldName = 'OPTRETNUMCOM'
    end
    object taUidItemSINVRETNUMCOM: TFIBSmallIntField
      FieldName = 'SINVRETNUMCOM'
    end
    object taUidItemRESNUMCOM: TFIBSmallIntField
      FieldName = 'RESNUMCOM'
    end
    object taUidItemENTRYCOSTCOM: TFIBFloatField
      FieldName = 'ENTRYCOSTCOM'
      currency = True
    end
    object taUidItemSINVCOSTCOM: TFIBFloatField
      FieldName = 'SINVCOSTCOM'
      currency = True
    end
    object taUidItemDINVCOSTCOM: TFIBFloatField
      FieldName = 'DINVCOSTCOM'
      currency = True
    end
    object taUidItemSELLCOSTCOM: TFIBFloatField
      FieldName = 'SELLCOSTCOM'
      currency = True
    end
    object taUidItemRETCOSTCOM: TFIBFloatField
      FieldName = 'RETCOSTCOM'
      currency = True
    end
    object taUidItemOPTSELLCOSTCOM: TFIBFloatField
      FieldName = 'OPTSELLCOSTCOM'
      currency = True
    end
    object taUidItemOPTRETCOSTCOM: TFIBFloatField
      FieldName = 'OPTRETCOSTCOM'
      currency = True
    end
    object taUidItemSINVRETCOSTCOM: TFIBFloatField
      FieldName = 'SINVRETCOSTCOM'
      currency = True
    end
    object taUidItemRESCOSTCOM: TFIBFloatField
      FieldName = 'RESCOSTCOM'
      currency = True
    end
    object taUidItemSELLCOST2COM: TFIBFloatField
      FieldName = 'SELLCOST2COM'
      currency = True
    end
    object taUidItemRETCOST2COM: TFIBFloatField
      FieldName = 'RETCOST2COM'
      currency = True
    end
    object taUidItemACTALLOWANCESNUMCOM: TFIBSmallIntField
      FieldName = 'ACTALLOWANCESNUMCOM'
    end
    object taUidItemACTALLOWANCESCOSTCOM: TFIBFloatField
      FieldName = 'ACTALLOWANCESCOSTCOM'
      currency = True
    end
  end
  object dsUidItem: TDataSource
    DataSet = taUidItem
    Left = 256
    Top = 144
  end
  object quUidRepeat: TpFIBDataSet
    SelectSQL.Strings = (
      'select uid, count(*)'
      'from uidwh_t'
      'where userid = -1'
      'group by uid'
      'having count(*)>1')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 320
    Top = 96
    object quUidRepeatUID: TIntegerField
      FieldName = 'UID'
    end
    object quUidRepeatCOUNT: TIntegerField
      FieldName = 'COUNT'
      Required = True
    end
  end
  object dsUidRepeat: TDataSource
    DataSet = quUidRepeat
    Left = 320
    Top = 144
  end
  object quUIDChg: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select UID,  CH_Date , DISCRIP,  NEWVAL, OLDVAL, ISNEW, NEWVALID' +
        ', OLDVALID, NEWPRICE, OLDPRICE'
      'from UIDCNG_HIST(:BD,:ED)'
      ''
      ''
      ''
      ''
      ''
      'ORDER BY CH_Date')
    AfterOpen = quUIDChgAfterOpen
    AfterScroll = quUIDChgAfterScroll
    BeforeOpen = quUIDChgBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 388
    Top = 8
    object quUIDChgUID: TIntegerField
      DisplayLabel = #1048#1076'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = '"EDTSEL"."UID"'
    end
    object quUIDChgDISCRIP: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077' '#1076#1077#1081#1089#1090#1074#1080#1103
      FieldName = 'DISCRIP'
      Origin = '"EDTSEL"."DISCRIP"'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object quUIDChgCH_DATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      FieldName = 'CH_DATE'
      Origin = '"EDTSEL"."CH_DATE"'
    end
    object quUIDChgOLDVAL: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
      FieldName = 'OLDVAL'
      Origin = 'UIDCNG_HIST.OLDVAL'
      FixedChar = True
      Size = 100
      EmptyStrToNull = True
    end
    object quUIDChgNEWVAL: TFIBStringField
      DisplayLabel = #1053#1086#1074'. '#1079#1085#1072#1095#1077#1085#1080#1077
      FieldName = 'NEWVAL'
      Origin = 'UIDCNG_HIST.NEWVAL'
      FixedChar = True
      Size = 100
      EmptyStrToNull = True
    end
    object quUIDChgISNEW: TFIBStringField
      DisplayLabel = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1080
      FieldName = 'ISNEW'
      Origin = 'UIDCNG_HIST.ISNEW'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quUIDChgNEWVALID: TIntegerField
      FieldName = 'NEWVALID'
      Origin = 'UIDCNG_HIST.NEWVALID'
    end
    object quUIDChgOLDVALID: TIntegerField
      FieldName = 'OLDVALID'
      Origin = 'UIDCNG_HIST.OLDVALID'
    end
    object quUIDChgNEWPRICE: TFIBFloatField
      DisplayLabel = #1053#1086#1074'. '#1094#1077#1085#1072
      FieldName = 'NEWPRICE'
    end
    object quUIDChgOLDPRICE: TFIBFloatField
      DisplayLabel = #1057#1090'. '#1094#1077#1085#1072
      FieldName = 'OLDPRICE'
    end
  end
  object dsUIDChg: TDataSource
    DataSet = quUIDChg
    Left = 388
    Top = 56
  end
  object quDetalChg: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT e.tablename, e.oldid,e.newid,e.OLDPRICE,e.NEWPRICE,e.DepI' +
        'D, d.Sname, d.Color'
      'FROM edtsel e , D_dep d'
      'WHERE e.depID = d.D_depid and'
      '      e.uid = :UID and'
      '      e.CH_DATE between (:CDate-0.001) and (:CDate+0.001) and'
      '      e.discrip = :discrip'
      'ORDER BY DepID'
      '')
    BeforeOpen = quDetalChgBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 456
    Top = 8
    object quDetalChgNEWID: TIntegerField
      DisplayLabel = #1053#1086#1074#1099#1081' ID '
      FieldName = 'NEWID'
      Origin = 'UIDCHANGE.NEWID'
    end
    object quDetalChgOLDID: TIntegerField
      DisplayLabel = #1057#1090#1072#1088#1099#1081' ID'
      FieldName = 'OLDID'
      Origin = 'UIDCHANGE.OLDID'
    end
    object quDetalChgTABLENAME: TFIBStringField
      DisplayLabel = #1053#1072#1079#1074#1072#1085#1080#1077' '#1090#1072#1073#1083#1080#1094#1099
      FieldName = 'TABLENAME'
      Origin = 'EDTSEL.TABLENAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object quDetalChgDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'UIDCHANGE.DEPID'
    end
    object quDetalChgOLDPRICE: TFloatField
      DisplayLabel = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
      FieldName = 'OLDPRICE'
      Origin = 'EDTSEL.OLDPRICE'
    end
    object quDetalChgNEWPRICE: TFloatField
      DisplayLabel = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
      FieldName = 'NEWPRICE'
      Origin = 'EDTSEL.NEWPRICE'
    end
    object quDetalChgSNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object quDetalChgCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
  end
  object dsUIDDetalChg: TDataSource
    DataSet = quDetalChg
    Left = 456
    Top = 56
  end
  object quTmpLog: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 420
    Top = 140
  end
  object quGetSmallestSell: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select RES, NSELL'
      'from GETSMALLESTSELL(:AUID, :ASELLID, :ARet)')
    Left = 128
    Top = 200
  end
  object taUIDStore_Detail: TpFIBDataSet
    UpdateSQL.Strings = (
      'update UID_STORE set'
      '  F=:F'
      'where UIDSTOREID=:UIDSTOREID'
      '  ')
    SelectSQL.Strings = (
      'select s.UIDSTOREID, s.UID, s.RESNUM, s.F,'
      '       s.ENTRYNUM, s.SINVNUM, s.DINVNUMFROM, s.DINVNUM,'
      '       s.SELLNUM,  s.RETNUM, s.OPTSELLNUM,  s.OPTRETNUM,'
      '       s.SINVRETNUM'
      'from UID_Store s '
      'where s.SINVID=:SINVID and'
      '      s.ARTID=:ARTID and'
      '      s.SZ=:SZ and'
      '      s.DEPID=:DEPID'
      'order by s.UID')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taUIDStore_DetailBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Description = 
      #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100'. '#1044#1077#1090#1072#1083#1080#1079#1072#1094#1080#1080' '#1087#1086' '#1080#1079#1076#1077#1083#1080#1103#1084' '#1076#1083#1103' '#1072#1088#1090#1080#1082#1091#1083#1072' '#1080' '#1089#1082#1083#1072 +
      #1076#1072
    Left = 176
    Top = 100
    poSQLINT64ToBCD = True
    object taUIDStore_DetailUIDSTOREID: TFIBIntegerField
      FieldName = 'UIDSTOREID'
    end
    object taUIDStore_DetailUID: TFIBIntegerField
      DisplayLabel = #1048#1076'.'#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taUIDStore_DetailRESNUM: TFIBSmallIntField
      DisplayLabel = #1048#1089#1093'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'RESNUM'
    end
    object taUIDStore_DetailF: TFIBIntegerField
      DisplayLabel = #1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077
      FieldName = 'F'
    end
    object taUIDStore_DetailENTRYNUM: TFIBSmallIntField
      DisplayLabel = #1042#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'ENTRYNUM'
    end
    object taUIDStore_DetailSINVNUM: TFIBSmallIntField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072
      FieldName = 'SINVNUM'
    end
    object taUIDStore_DetailDINVNUMFROM: TFIBSmallIntField
      DisplayLabel = #1042#1055' '#1088#1072#1089#1093#1086#1076
      FieldName = 'DINVNUMFROM'
    end
    object taUIDStore_DetailDINVNUM: TFIBSmallIntField
      DisplayLabel = #1042#1055' '#1087#1088#1080#1093#1086#1076
      FieldName = 'DINVNUM'
    end
    object taUIDStore_DetailSELLNUM: TFIBSmallIntField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1072
      FieldName = 'SELLNUM'
    end
    object taUIDStore_DetailRETNUM: TFIBSmallIntField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090
      FieldName = 'RETNUM'
    end
    object taUIDStore_DetailOPTSELLNUM: TFIBSmallIntField
      DisplayLabel = #1054#1087#1090' '#1087#1088#1086#1076#1072#1078#1072
      FieldName = 'OPTSELLNUM'
    end
    object taUIDStore_DetailOPTRETNUM: TFIBSmallIntField
      DisplayLabel = #1054#1087#1090' '#1074#1086#1079#1074#1088#1072#1090
      FieldName = 'OPTRETNUM'
    end
    object taUIDStore_DetailSINVRETNUM: TFIBSmallIntField
      DisplayLabel = #1042#1086#1079#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
      FieldName = 'SINVRETNUM'
    end
  end
  object dsrUIDStore_Detail: TDataSource
    DataSet = taUIDStore_Detail
    Left = 176
    Top = 144
  end
  object quSelectLog: TpFIBDataSet
    SelectSQL.Strings = (
      'select l_userid'
      'from r_log (:bd, :ed)')
    BeforeOpen = quSelectLogBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 424
    Top = 200
    poSQLINT64ToBCD = True
    object quSelectLogL_USERID: TFIBIntegerField
      FieldName = 'L_USERID'
    end
  end
  object dsSelectLog: TDataSource
    DataSet = quSelectLog
    Left = 424
    Top = 256
  end
  object quSetLogRState: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure setlogrstate (:l_userid, :rstate)')
    Left = 352
    Top = 208
  end
  object quDest: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select r_code'
      'from d_dep'
      'where centerdep = 1')
    Left = 352
    Top = 264
  end
  object quEditArt: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT  AHSTID, D_ARTID, D_COMPID, D_MATID, D_GOODID, ART,'
      '              UNITID, D_INSID, ADATE, OLD_D_COMPID, OLD_D_MATID,'
      '              OLD_D_GOODID, OLD_ART, OLD_UNITID, OLD_D_INSID,'
      '              D_COUNTRYID, OLD_D_COUNTRYID, ATT1ID, ATT2ID,'
      
        '              OLD_ATT1ID, OLD_ATT2ID, fullart, comp, old_comp, a' +
        'tt1,'
      '              old_att1, att2, old_att2'
      'FROM AHST_S (:BD, :ED)'
      ''
      ''
      '')
    BeforeOpen = quEditArtBeforeOpen
    OnCalcFields = quEditArtCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 24
    Top = 272
    poSQLINT64ToBCD = True
    object quEditArtAHSTID: TFIBIntegerField
      FieldName = 'AHSTID'
    end
    object quEditArtD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quEditArtD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quEditArtD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quEditArtUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quEditArtD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quEditArtOLD_D_COMPID: TFIBIntegerField
      FieldName = 'OLD_D_COMPID'
    end
    object quEditArtOLD_D_MATID: TFIBStringField
      FieldName = 'OLD_D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtOLD_D_GOODID: TFIBStringField
      FieldName = 'OLD_D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtOLD_ART: TFIBStringField
      FieldName = 'OLD_ART'
      EmptyStrToNull = True
    end
    object quEditArtOLD_UNITID: TFIBSmallIntField
      FieldName = 'OLD_UNITID'
    end
    object quEditArtOLD_D_INSID: TFIBStringField
      FieldName = 'OLD_D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtOLD_D_COUNTRYID: TFIBStringField
      FieldName = 'OLD_D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditArtATT1ID: TFIBIntegerField
      FieldName = 'ATT1ID'
    end
    object quEditArtATT2ID: TFIBIntegerField
      FieldName = 'ATT2ID'
    end
    object quEditArtOLD_ATT1ID: TFIBIntegerField
      FieldName = 'OLD_ATT1ID'
    end
    object quEditArtOLD_ATT2ID: TFIBIntegerField
      FieldName = 'OLD_ATT2ID'
    end
    object quEditArtFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quEditArtCOMP: TFIBStringField
      FieldName = 'COMP'
      EmptyStrToNull = True
    end
    object quEditArtOLD_COMP: TFIBStringField
      FieldName = 'OLD_COMP'
      EmptyStrToNull = True
    end
    object quEditArtATT1: TFIBStringField
      FieldName = 'ATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object quEditArtOLD_ATT1: TFIBStringField
      FieldName = 'OLD_ATT1'
      Size = 80
      EmptyStrToNull = True
    end
    object quEditArtATT2: TFIBStringField
      FieldName = 'ATT2'
      Size = 80
      EmptyStrToNull = True
    end
    object quEditArtOLD_ATT2: TFIBStringField
      FieldName = 'OLD_ATT2'
      Size = 80
      EmptyStrToNull = True
    end
    object quEditArtUNIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'UNIT'
      Size = 10
      Calculated = True
    end
    object quEditArtOLD_UINT: TStringField
      FieldKind = fkCalculated
      FieldName = 'OLD_UINT'
      Size = 10
      Calculated = True
    end
  end
  object quEditArt2: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT A2HIST_ID,  ART2ID,  ART2, ADATE, OLD_ART2, FULLART, ART'
      'FROM A2HIST_S  (:BD, :ED)'
      ''
      ''
      ''
      '')
    BeforeOpen = quEditArt2BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 96
    Top = 272
    poSQLINT64ToBCD = True
    object quEditArt2A2HIST_ID: TFIBIntegerField
      FieldName = 'A2HIST_ID'
    end
    object quEditArt2ART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quEditArt2ART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quEditArt2ADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quEditArt2OLD_ART2: TFIBStringField
      FieldName = 'OLD_ART2'
      EmptyStrToNull = True
    end
    object quEditArt2FULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quEditArt2ART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
  end
  object quEditIns: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT INSHIST_ID, INSID, ART2ID, D_INSID, QUANTITY, WEIGHT,'
      '             COLOR, CHROMATICITY, CLEANNES, GR, SHAPE, EDGETION,'
      '             MAIN, EDGTYPEID, EDGSHAPEID, ADATE, OLD_ART2ID,'
      '             OLD_D_INSID, OLD_QUANTITY, OLD_WEIGHT, OLD_COLOR,'
      '             OLD_CHROMATICITY, OLD_CLEANNES, OLD_GR, OLD_SHAPE,'
      
        '             OLD_EDGETION, OLD_MAIN, OLD_EDGTYPEID, OLD_EDGSHAPE' +
        'ID,'
      '             STATE, ISPRORD, ART, FULLART, ART2, OLD_ART2'
      'FROM INSHIST_S (:BD, :ED)'
      ''
      ''
      ''
      '')
    BeforeOpen = quEditInsBeforeOpen
    OnCalcFields = quEditInsCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 160
    Top = 272
    poSQLINT64ToBCD = True
    object quEditInsINSHIST_ID: TFIBIntegerField
      FieldName = 'INSHIST_ID'
    end
    object quEditInsINSID: TFIBIntegerField
      FieldName = 'INSID'
    end
    object quEditInsART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quEditInsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsQUANTITY: TFIBSmallIntField
      FieldName = 'QUANTITY'
    end
    object quEditInsWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
    object quEditInsCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object quEditInsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object quEditInsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object quEditInsGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsSHAPE: TFIBStringField
      FieldName = 'SHAPE'
      EmptyStrToNull = True
    end
    object quEditInsEDGETION: TFIBStringField
      FieldName = 'EDGETION'
      EmptyStrToNull = True
    end
    object quEditInsMAIN: TFIBSmallIntField
      FieldName = 'MAIN'
    end
    object quEditInsEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quEditInsOLD_ART2ID: TFIBIntegerField
      FieldName = 'OLD_ART2ID'
    end
    object quEditInsOLD_D_INSID: TFIBStringField
      FieldName = 'OLD_D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsOLD_QUANTITY: TFIBSmallIntField
      FieldName = 'OLD_QUANTITY'
    end
    object quEditInsOLD_WEIGHT: TFIBFloatField
      FieldName = 'OLD_WEIGHT'
    end
    object quEditInsOLD_COLOR: TFIBStringField
      FieldName = 'OLD_COLOR'
      EmptyStrToNull = True
    end
    object quEditInsOLD_CHROMATICITY: TFIBStringField
      FieldName = 'OLD_CHROMATICITY'
      EmptyStrToNull = True
    end
    object quEditInsOLD_CLEANNES: TFIBStringField
      FieldName = 'OLD_CLEANNES'
      EmptyStrToNull = True
    end
    object quEditInsOLD_GR: TFIBStringField
      FieldName = 'OLD_GR'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsOLD_SHAPE: TFIBStringField
      FieldName = 'OLD_SHAPE'
      EmptyStrToNull = True
    end
    object quEditInsOLD_EDGETION: TFIBStringField
      FieldName = 'OLD_EDGETION'
      EmptyStrToNull = True
    end
    object quEditInsOLD_MAIN: TFIBSmallIntField
      FieldName = 'OLD_MAIN'
    end
    object quEditInsOLD_EDGTYPEID: TFIBStringField
      FieldName = 'OLD_EDGTYPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsOLD_EDGSHAPEID: TFIBStringField
      FieldName = 'OLD_EDGSHAPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quEditInsSTATE: TFIBSmallIntField
      FieldName = 'STATE'
    end
    object quEditInsISPRORD: TFIBSmallIntField
      FieldName = 'ISPRORD'
    end
    object quEditInsART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quEditInsFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quEditInsART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quEditInsOperation: TStringField
      FieldKind = fkCalculated
      FieldName = 'Operation'
      Calculated = True
    end
    object quEditInsWhere_: TStringField
      FieldKind = fkCalculated
      FieldName = 'Where_'
      Size = 30
      Calculated = True
    end
    object quEditInsOLD_ART2: TFIBStringField
      FieldName = 'OLD_ART2'
      EmptyStrToNull = True
    end
  end
  object dsEditArt: TDataSource
    DataSet = quEditArt
    Left = 24
    Top = 320
  end
  object dsEditArt2: TDataSource
    DataSet = quEditArt2
    Left = 96
    Top = 320
  end
  object dsEditIns: TDataSource
    DataSet = quEditIns
    Left = 160
    Top = 320
  end
  object quCompD_art: TpFIBDataSet
    SelectSQL.Strings = (
      
        'Select d_artid, art, d_matid, d_goodid, d_insid, unitid, d_compi' +
        'd'
      'From D_art'
      'where d_compid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 376
    poSQLINT64ToBCD = True
    object quCompD_artD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quCompD_artART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quCompD_artD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quCompD_artD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quCompD_artD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quCompD_artUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quCompD_artD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
  end
  object quCompSinfo: TpFIBDataSet
    SelectSQL.Strings = (
      'select sinfoid, sn, sinvid, selid, ssf, sdate, ndate'
      'from sinfo'
      'where supid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 104
    Top = 376
    poSQLINT64ToBCD = True
    object quCompSinfoSINFOID: TFIBIntegerField
      FieldName = 'SINFOID'
    end
    object quCompSinfoSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quCompSinfoSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCompSinfoSELID: TFIBIntegerField
      FieldName = 'SELID'
    end
    object quCompSinfoSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quCompSinfoSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quCompSinfoNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
  end
  object quCompSinv: TpFIBDataSet
    SelectSQL.Strings = (
      'select sinvid, sn, sdate, ndate, ssf, itype'
      'from sinv'
      'where supid=:d_compid or'
      '      compid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    OnCalcFields = quCompSinvCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 168
    Top = 376
    poSQLINT64ToBCD = True
    object quCompSinvSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCompSinvSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quCompSinvSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quCompSinvNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quCompSinvSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quCompSinvITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quCompSinvTypeSinv: TStringField
      FieldKind = fkCalculated
      FieldName = 'TypeSinv'
      Size = 30
      Calculated = True
    end
  end
  object dsCompD_art: TDataSource
    DataSet = quCompD_art
    Left = 32
    Top = 424
  end
  object dsCompSinfo: TDataSource
    DataSet = quCompSinfo
    Left = 104
    Top = 424
  end
  object dsCompSinv: TDataSource
    DataSet = quCompSinv
    Left = 168
    Top = 424
  end
  object dsCompArt2: TDataSource
    DataSet = quCompArt2
    Left = 232
    Top = 424
  end
  object quCompArt2: TpFIBDataSet
    SelectSQL.Strings = (
      'select art2id, art, art2, d_compid, supid'
      'from art2'
      'where supid = :D_compid or d_compid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 232
    Top = 376
    poSQLINT64ToBCD = True
    object quCompArt2ART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quCompArt2ART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quCompArt2ART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quCompArt2D_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quCompArt2SUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
  end
  object quApplDepList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update sinv'
      'set depfromid=:depfromid'
      'where sinvid=:sinvid')
    DeleteSQL.Strings = (
      'delete from sinv where sinvid=:sinvid')
    InsertSQL.Strings = (
      
        'insert into sinv (SINVID, DEPID, SDATE, ISCLOSED, ITYPE, DEPFROM' +
        'ID, '
      '                  CRUSERID, MODEID, SN)'
      'values (:sinvid, :depid, :sdate, 0, :itype, :depfromid,  '
      '                  :cruserid, :modeid, :SN);')
    RefreshSQL.Strings = (
      'SELECT sinvid, depid, sdate, userid, sname, color, '
      '       fio, c, w, cost, costp, DEPFROMID, snamefrom,'
      '       colordepfrom, cruserid, crusername, isclosed, '
      '       sn, modeid, itype, sellid, COMMENT, edittr, RSTATE_S'
      'FROM ApplDepList_R (:Sinvid)'
      '')
    SelectSQL.Strings = (
      'SELECT sinvid, depid, sdate, userid, sname, color,'
      '       fio, c, w, cost, costp, DEPFROMID, snamefrom,'
      '       colordepfrom, cruserid, crusername, isclosed,'
      '       sn, modeid, itype, sellid, COMMENT, edittr, RSTATE_S'
      'FROM ApplDepList_S (:I_DEPID1, :I_DEPID2, '
      ' :I_DEPFROMID1, :I_DEPFROMID2, :I_BD, :I_ED)'
      'where edittr between :edittr1 and :edittr2')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = quApplDepListBeforeDelete
    BeforeOpen = quApplDepListBeforeOpen
    OnNewRecord = quApplDepListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 440
    Top = 352
    poSQLINT64ToBCD = True
    object quApplDepListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quApplDepListDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quApplDepListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quApplDepListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quApplDepListSNAME: TFIBStringField
      FieldName = 'SNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object quApplDepListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object quApplDepListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 40
      EmptyStrToNull = True
    end
    object quApplDepListC: TFIBIntegerField
      FieldName = 'C'
    end
    object quApplDepListW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quApplDepListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quApplDepListCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
    object quApplDepListDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
    object quApplDepListSNAMEFROM: TFIBStringField
      FieldName = 'SNAMEFROM'
      EmptyStrToNull = True
    end
    object quApplDepListCOLORDEPFROM: TFIBIntegerField
      FieldName = 'COLORDEPFROM'
    end
    object quApplDepListCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object quApplDepListCRUSERNAME: TFIBStringField
      FieldName = 'CRUSERNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object quApplDepListISCLOSED: TFIBIntegerField
      FieldName = 'ISCLOSED'
    end
    object quApplDepListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quApplDepListMODEID: TFIBIntegerField
      FieldName = 'MODEID'
    end
    object quApplDepListITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quApplDepListSELLID: TFIBIntegerField
      FieldName = 'SELLID'
    end
    object quApplDepListCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 50
      EmptyStrToNull = True
    end
    object quApplDepListEDITTR: TFIBSmallIntField
      FieldName = 'EDITTR'
    end
    object quApplDepListRSTATE_S: TFIBStringField
      FieldName = 'RSTATE_S'
      EmptyStrToNull = True
    end
  end
  object dsApplDepList: TDataSource
    DataSet = quApplDepList
    Left = 440
    Top = 400
  end
  object quApplDepItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update appldep'
      'set refusal=:refusal'
      'where appldepid=:appldepid')
    DeleteSQL.Strings = (
      'delete from appldep where appldepid=:appldepid')
    InsertSQL.Strings = (
      
        'insert into appldep (appldepid, uid, sz, w, art2id, sinvid, FUID' +
        'WH, cost, costp)'
      
        'values (:appldepid, :uid, :sz, :w, :art2id, :sinvid, 0, :cost, :' +
        'costp)'
      '')
    RefreshSQL.Strings = (
      'select ap.appldepid, ap.uid, ap.w, ap.sz, ap.sinvid, ap.art2id,'
      
        '          a2.art, a2.prodcode, a2.d_goodid, a2.d_insid, a2.d_mat' +
        'id, a2.art2, ap.cost, ap.costp, a2.d_artid, ap.refusal'
      'from appldep ap, art2 a2'
      'where ap.art2id = a2.art2id and'
      '           ap.appldepid = :appldepid'
      '')
    SelectSQL.Strings = (
      'select ap.appldepid, ap.uid, ap.w, ap.sz, ap.sinvid, ap.art2id,'
      
        '       a2.art, a2.prodcode, a2.d_goodid, a2.d_insid, a2.d_matid,' +
        ' a2.art2, ap.cost, ap.costp, a2.d_artid, ap.refusal'
      'from appldep ap, art2 a2'
      'where ap.art2id = a2.art2id and'
      '           ap.sinvid = :sinvid')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = quApplDepItemBeforeOpen
    OnNewRecord = quApplDepItemNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 528
    Top = 352
    poSQLINT64ToBCD = True
    object quApplDepItemAPPLDEPID: TFIBIntegerField
      FieldName = 'APPLDEPID'
    end
    object quApplDepItemUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quApplDepItemW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quApplDepItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepItemSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quApplDepItemART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quApplDepItemART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quApplDepItemPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepItemD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepItemD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepItemD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepItemART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quApplDepItemCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quApplDepItemCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
    object quApplDepItemD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object quApplDepItemREFUSAL: TFIBSmallIntField
      FieldName = 'REFUSAL'
    end
  end
  object dsApplDepItem: TDataSource
    DataSet = quApplDepItem
    Left = 528
    Top = 400
  end
  object quApplDepArt: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from appldep where appldepid=:appldepid')
    InsertSQL.Strings = (
      
        'insert into appldep (appldepid, uid, sz, w, art2id, sinvid, Fuid' +
        'wh, cost, costp)'
      
        'values (:appldepid, :uid, :sz, :w, :art2id, :sinvid, 0, :cost, :' +
        'costp)')
    RefreshSQL.Strings = (
      
        'select ap.appldepid, ap.uid, ap.w, ap.sz, a2.art2, ap.sinvid, ap' +
        '.art2id, ap.cost,'
      '           ap.costp'
      'from appldep ap, art2 a2'
      'where ap.appldepid = :appldepid and'
      '           a2.art2id = ap.art2id'
      '')
    SelectSQL.Strings = (
      
        'select ap.appldepid, ap.uid, ap.w, ap.sz, a2.art2, ap.sinvid, ap' +
        '.art2id, ap.cost,'
      '          ap.costp'
      'from appldep ap, art2 a2'
      'where ap.art2id = a2.art2id and'
      '           ap.sinvid = :sinvid and'
      '           a2.d_artid= :d_artid ')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = quApplDepArtBeforeOpen
    OnNewRecord = quApplDepArtNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 608
    Top = 352
    poSQLINT64ToBCD = True
    object quApplDepArtUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quApplDepArtW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quApplDepArtSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quApplDepArtAPPLDEPID: TFIBIntegerField
      FieldName = 'APPLDEPID'
    end
    object quApplDepArtART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quApplDepArtSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quApplDepArtART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quApplDepArtCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quApplDepArtCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
  end
  object dsApplDepArt: TDataSource
    DataSet = quApplDepArt
    Left = 608
    Top = 400
  end
  object quSelectUid: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure stub (0)')
    InsertSQL.Strings = (
      'execute procedure stub (0)')
    RefreshSQL.Strings = (
      
        'select sinvid, sitemid, uid, sz, w, art2, art2id, cost, costp, F' +
        'exists'
      'from selectUid_r (:uid, :sinvid, :Fexists)'
      ''
      '')
    SelectSQL.Strings = (
      
        'select sinvid, sitemid, uid, sz, w, art2, art2id, cost, costp, F' +
        'exists'
      'from selectUid_s (:sinvid, :Depid,  :d_artid, :UID)'
      ''
      '')
    BeforeOpen = quSelectUidBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 688
    Top = 352
    poSQLINT64ToBCD = True
    object quSelectUidSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object quSelectUidUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quSelectUidSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quSelectUidW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quSelectUidART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quSelectUidART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quSelectUidSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quSelectUidCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quSelectUidCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
    object quSelectUidFEXISTS: TFIBSmallIntField
      FieldName = 'FEXISTS'
    end
  end
  object dsSelectUid: TDataSource
    DataSet = quSelectUid
    Left = 688
    Top = 400
  end
  object quRepSetting: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure REPSETTING_U (:d_depid, :r_code, :OLD_R_CODE, ' +
        ':r_genoffset,'
      '                                :r_userdep, :isftprep, :here)')
    DeleteSQL.Strings = (
      '')
    InsertSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      'select d_depid, name, color, r_code, r_genoffset,'
      '       r_userdep,  isftprep, here, usedep, isrecord'
      'from REPSETTING_R (:D_DEPID)')
    SelectSQL.Strings = (
      'select d_depid, name, color, r_code, r_genoffset,'
      '       r_userdep,  isftprep, here, usedep, isrecord'
      'from REPSETTING_S')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 504
    Top = 128
    poSQLINT64ToBCD = True
    object quRepSettingD_DEPID: TFIBIntegerField
      FieldName = 'D_DEPID'
    end
    object quRepSettingNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
    object quRepSettingCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object quRepSettingR_CODE: TFIBStringField
      FieldName = 'R_CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quRepSettingR_GENOFFSET: TFIBIntegerField
      FieldName = 'R_GENOFFSET'
    end
    object quRepSettingR_USERDEP: TFIBSmallIntField
      FieldName = 'R_USERDEP'
    end
    object quRepSettingISFTPREP: TFIBSmallIntField
      FieldName = 'ISFTPREP'
    end
    object quRepSettingHERE: TFIBSmallIntField
      FieldName = 'HERE'
    end
    object quRepSettingUSEDEP: TFIBSmallIntField
      FieldName = 'USEDEP'
    end
    object quRepSettingISRECORD: TFIBSmallIntField
      FieldName = 'ISRECORD'
    end
  end
  object dsRepSetting: TDataSource
    DataSet = quRepSetting
    Left = 504
    Top = 176
  end
  object quGenSetting: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'select rdb$generator_name'
      'from rdb$generators'
      'where rdb$system_flag is null and'
      '     (rdb$generator_name<>'#39'CENTERDEPID'#39' and'
      '      rdb$generator_name<>'#39'SELFDEPID'#39' and'
      '      rdb$generator_name<>'#39'GEN_UID'#39' and'
      '      rdb$generator_name<>'#39'ISMAKELOG'#39')')
    Left = 568
    Top = 144
  end
  object quActAllowancesList: TpFIBDataSet
    UpdateSQL.Strings = (
      'Update Sinv'
      'Set Sdate=:Sdate,'
      '    sn=:SN '
      'where Sinvid=:Sinvid')
    DeleteSQL.Strings = (
      'delete from sinv where sinvid=:sinvid')
    InsertSQL.Strings = (
      
        'INSERT INTO SINV (SINVID, DEPFROMID, NDATE, SDATE, SN, ISCLOSED,' +
        ' ITYPE, CRUSERID)'
      
        'VALUES (:SINVID, :DEPFROMID, :NDATE, :SDATE, :SN, 0, 19, :CRUSER' +
        'ID)')
    RefreshSQL.Strings = (
      'SELECT sinvid, depfromid, ndate, sdate, sn, isclosed,'
      '       itype, userid, cruserid, username, crusername,'
      
        '       depname, color, costp, cost, q, w, Descrip,Ref_SInvID, sd' +
        'ate1'
      'FROM ActAllowancesList_R (:SINVID)')
    SelectSQL.Strings = (
      'SELECT sinvid, depfromid, ndate, sdate, sn, isclosed,'
      '       itype, userid, cruserid, username, crusername,'
      
        '       depname, color, costp, cost, q, w, Descrip, Ref_SInvID, s' +
        'date1'
      'FROM ActAllowancesList_S (:BD, :ED, :DepID1, :depid2)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeEdit = quActAllowancesListBeforeEdit
    BeforeOpen = quActAllowancesListBeforeOpen
    OnCalcFields = quActAllowancesListCalcFields
    OnNewRecord = quActAllowancesListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 496
    Top = 236
    poSQLINT64ToBCD = True
    object quActAllowancesListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quActAllowancesListDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
    object quActAllowancesListNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quActAllowancesListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quActAllowancesListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quActAllowancesListISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quActAllowancesListITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object quActAllowancesListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quActAllowancesListCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object quActAllowancesListUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quActAllowancesListCRUSERNAME: TFIBStringField
      FieldName = 'CRUSERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quActAllowancesListDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      EmptyStrToNull = True
    end
    object quActAllowancesListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object quActAllowancesListCOSTP: TFIBFloatField
      FieldName = 'COSTP'
      currency = True
    end
    object quActAllowancesListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quActAllowancesListQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object quActAllowancesListW: TFIBFloatField
      FieldName = 'W'
      EditFormat = '0.##'
    end
    object quActAllowancesListOpenCostP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OpenCostP'
      currency = True
      Calculated = True
    end
    object quActAllowancesListCloseCostP: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CloseCostP'
      currency = True
      Calculated = True
    end
    object quActAllowancesListOpenCost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OpenCost'
      currency = True
      Calculated = True
    end
    object quActAllowancesListCloseCost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CloseCost'
      currency = True
      Calculated = True
    end
    object quActAllowancesListOpenQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OpenQ'
      Calculated = True
    end
    object quActAllowancesListCloseQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CloseQ'
      Calculated = True
    end
    object quActAllowancesListOpenW: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OpenW'
      EditFormat = '0.##'
      Calculated = True
    end
    object quActAllowancesListCloseW: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CloseW'
      EditFormat = '0.##'
      Calculated = True
    end
    object quActAllowancesListOpenSn: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'OpenSn'
      Calculated = True
    end
    object quActAllowancesListCloseSn: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CloseSn'
      Calculated = True
    end
    object quActAllowancesListDESCRIP: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'DESCRIP'
      Size = 200
      EmptyStrToNull = True
    end
    object quActAllowancesListREF_SINVID: TFIBIntegerField
      FieldName = 'REF_SINVID'
    end
    object quActAllowancesListSDATE1: TFIBDateTimeField
      FieldName = 'SDATE1'
    end
  end
  object dsActAllowancesList: TDataSource
    DataSet = quActAllowancesList
    Left = 496
    Top = 284
  end
  object quActAllowances: TpFIBDataSet
    UpdateSQL.Strings = (
      'update d_mat'
      'set probe$id = :probe$id '
      'where d_matid = :d_matid')
    DeleteSQL.Strings = (
      'execute procedure ActAllowances_D(:OLD_SITEMID)')
    RefreshSQL.Strings = (
      'Select SITEMID, SELID, PRODCODE, D_MATID, D_GOODID,'
      '       D_INSID, ART, ART2, UID, W, SZ, sPRICE,'
      '       price2, COST, scost, SDate'
      'From ActAllowances_R(:SITEMID)')
    SelectSQL.Strings = (
      'Select'
      '   a.SITEMID, a.SELID, a.PRODCODE, a.D_MATID, a.D_GOODID,'
      '   a.D_INSID, a.ART, a.ART2, a.UID, a.W, a.SZ, a.sPRICE,'
      
        '   a.price2, a.COST, a.scost, a.SDate, m.id material$id, p.id pr' +
        'obe$id'
      
        'From ActAllowances_S(:SINVID) a, d_mat dm, d_probe p, d_material' +
        ' m'
      
        'where dm.d_matid = a.d_matid and p.id = dm.probe$id and m.id = p' +
        '.matid')
    AfterDelete = CommitRetaining
    BeforeDelete = quActAllowancesBeforeDelete
    BeforeEdit = quActAllowancesBeforeEdit
    BeforeOpen = quActAllowancesBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 576
    Top = 228
    poSQLINT64ToBCD = True
    object quActAllowancesSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object quActAllowancesSELID: TFIBIntegerField
      FieldName = 'SELID'
    end
    object quActAllowancesPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quActAllowancesD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quActAllowancesD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quActAllowancesD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quActAllowancesART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quActAllowancesART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quActAllowancesUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quActAllowancesW: TFIBFloatField
      FieldName = 'W'
      EditFormat = '0.##'
    end
    object quActAllowancesSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quActAllowancesSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object quActAllowancesPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quActAllowancesCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quActAllowancesSCOST: TFIBFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object quActAllowancesSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quActAllowancesMATERIALNAME: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'MATERIAL$NAME'
      LookupDataSet = DBProbe
      LookupKeyFields = 'PROBE$ID'
      LookupResultField = 'MATERIAL$NAME'
      KeyFields = 'PROBE$ID'
      Size = 30
      EmptyStrToNull = True
      Lookup = True
    end
    object quActAllowancesPROBENAME: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'PROBE$NAME'
      LookupDataSet = DBProbe
      LookupKeyFields = 'PROBE$ID'
      LookupResultField = 'PROBE$NAME'
      KeyFields = 'PROBE$ID'
      Size = 8
      EmptyStrToNull = True
      Lookup = True
    end
    object quActAllowancesMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object quActAllowancesPROBEID: TFIBIntegerField
      FieldName = 'PROBE$ID'
    end
  end
  object dsActAllowances: TDataSource
    DataSet = quActAllowances
    Left = 592
    Top = 284
  end
  object dsDInvCheck: TDataSource
    DataSet = quDInvCheck
    Left = 552
    Top = 56
  end
  object quDInvCheck: TpFIBDataSet
    UpdateSQL.Strings = (
      'update  DInvCheck'
      '  Set Status = :O_Status'
      'where ID = :O_ID ')
    DeleteSQL.Strings = (
      'delete from DInvCheck'
      'where SInvID = :SInvID and'
      '      uid = :O_UID ')
    InsertSQL.Strings = (
      'Insert into DInvCheck(ID, SInvID, UID, Status)'
      'Values (:O_ID, :sinvid, :O_UID, :O_Status);'
      '')
    RefreshSQL.Strings = (
      'select O_UID, O_Comp, O_Goods, O_Mat, O_Ins, O_Unit, O_Art, O_W,'
      '        O_Sz, O_Price, O_Status'
      'from DInvCheck_R(:O_ID) ')
    SelectSQL.Strings = (
      'select O_ID, O_UID, O_Comp, O_Goods, O_Mat, O_Ins, O_Unit,'
      '       O_Art, O_W, O_Sz, O_Price, O_Status'
      'from DInvCheck_S(:SInvID)'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    AfterPost = quDInvCheckAfterPost
    BeforeOpen = quDInvCheckBeforeOpen
    BeforePost = quDInvCheckBeforePost
    OnNewRecord = quDInvCheckNewRecord
    Transaction = dmCom.tr1
    Database = dmCom.db
    Left = 552
    Top = 8
    poSQLINT64ToBCD = True
    object quDInvCheckO_ID: TFIBIntegerField
      FieldName = 'O_ID'
    end
    object quDInvCheckO_UID: TFIBIntegerField
      DisplayLabel = #1048#1076'. '#1085#1086#1084#1077#1088
      FieldName = 'O_UID'
    end
    object quDInvCheckO_COMP: TFIBStringField
      DisplayLabel = #1048#1079#1075'.'
      FieldName = 'O_COMP'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_GOODS: TFIBStringField
      DisplayLabel = #1058#1086#1074#1072#1088
      FieldName = 'O_GOODS'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_MAT: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'O_MAT'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_INS: TFIBStringField
      DisplayLabel = #1042#1089#1090#1072#1074#1082#1072
      FieldName = 'O_INS'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_UNIT: TFIBStringField
      DisplayLabel = #1045#1076'.'
      FieldName = 'O_UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_ART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'O_ART'
      EmptyStrToNull = True
    end
    object quDInvCheckO_W: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'O_W'
    end
    object quDInvCheckO_SZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'O_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quDInvCheckO_PRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1099
      FieldName = 'O_PRICE'
    end
    object quDInvCheckO_STATUS: TFIBIntegerField
      FieldName = 'O_STATUS'
    end
  end
  object quActAllowParamPrn: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Writeoff_sinv'
      ' Set  ActName_ID = :ActName_ID,'
      '      WriteOffReason_ID = :WriteOffReason_ID,'
      '      DefectDiscript_ID = :DefectDiscript_ID,'
      '      spoilingreason_ID = :Solution_ID  '
      'where SinvID = :SInvID'
      '')
    InsertSQL.Strings = (
      
        'insert into Writeoff_sinv(SinvID, ActName_ID, WriteOffReason_ID,' +
        ' DefectDiscript_ID, spoilingreason_ID, Solution_ID)'
      
        'values(:SInvID,:ActName_ID, :WriteOffReason_ID, :DefectDiscript_' +
        'ID, :spoilingreason_ID, :Solution_ID);')
    SelectSQL.Strings = (
      
        'select SInvID, ActName_ID, WriteOffReason_ID, DefectDiscript_ID,' +
        ' spoilingreason_ID, Solution_ID'
      'from Writeoff_sinv '
      'where SinvID = :SInvID')
    AfterPost = CommitRetaining
    BeforeOpen = quActAllowParamPrnBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 680
    Top = 236
    poSQLINT64ToBCD = True
    object quActAllowParamPrnActName: TStringField
      DisplayWidth = 200
      FieldKind = fkLookup
      FieldName = 'ActName'
      LookupDataSet = quWriteoff_Param
      LookupKeyFields = 'ID'
      LookupResultField = 'DISCRIP'
      KeyFields = 'ACTNAME_ID'
      Size = 200
      Lookup = True
    end
    object quActAllowParamPrnACTNAME_ID: TFIBIntegerField
      FieldName = 'ACTNAME_ID'
    end
    object quActAllowParamPrnWRITEOFFREASON_ID: TFIBIntegerField
      FieldName = 'WRITEOFFREASON_ID'
    end
    object quActAllowParamPrnDEFECTDISCRIPT_ID: TFIBIntegerField
      FieldName = 'DEFECTDISCRIPT_ID'
    end
    object quActAllowParamPrnSPOILINGREASON_ID: TFIBIntegerField
      FieldName = 'SPOILINGREASON_ID'
    end
    object quActAllowParamPrnSOLUTION_ID: TFIBIntegerField
      FieldName = 'SOLUTION_ID'
    end
    object quActAllowParamPrnWriteOffReason: TStringField
      DisplayWidth = 250
      FieldKind = fkLookup
      FieldName = 'WriteOffReason'
      LookupDataSet = quWriteoff_Param
      LookupKeyFields = 'ID'
      LookupResultField = 'DISCRIP'
      KeyFields = 'WRITEOFFREASON_ID'
      Size = 250
      Lookup = True
    end
    object quActAllowParamPrnDefectDiscript: TStringField
      DisplayWidth = 250
      FieldKind = fkLookup
      FieldName = 'DefectDiscript'
      LookupDataSet = quWriteoff_Param
      LookupKeyFields = 'ID'
      LookupResultField = 'DISCRIP'
      KeyFields = 'DEFECTDISCRIPT_ID'
      Size = 250
      Lookup = True
    end
    object quActAllowParamPrnspoilingreason: TStringField
      DisplayWidth = 250
      FieldKind = fkLookup
      FieldName = 'spoilingreason'
      LookupDataSet = quWriteoff_Param
      LookupKeyFields = 'ID'
      LookupResultField = 'DISCRIP'
      KeyFields = 'SPOILINGREASON_ID'
      Size = 250
      Lookup = True
    end
    object quActAllowParamPrnSolution: TStringField
      DisplayWidth = 250
      FieldKind = fkLookup
      FieldName = 'Solution'
      LookupDataSet = quWriteoff_Param
      LookupKeyFields = 'ID'
      LookupResultField = 'DISCRIP'
      KeyFields = 'SOLUTION_ID'
      Size = 250
      Lookup = True
    end
    object quActAllowParamPrnSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
  end
  object dsActAllowParamPrn: TDataSource
    DataSet = quActAllowParamPrn
    Left = 684
    Top = 284
  end
  object dsWriteoff_Param: TDataSource
    DataSet = quWriteoff_Param
    Left = 712
    Top = 184
  end
  object quWriteoff_Param: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    InsertSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select ID, stretrim(Discrip) as Discrip'
      'from Write_off_Param'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 708
    Top = 128
    poSQLINT64ToBCD = True
    object quWriteoff_ParamID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quWriteoff_ParamDISCRIP: TFIBStringField
      FieldName = 'DISCRIP'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object quD_writeoff: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Write_off_Param'
      'where ID = :ID')
    InsertSQL.Strings = (
      'insert into Write_off_Param(ID, Discrip,IType)'
      'values(:ID,:Discrip, :IType)')
    SelectSQL.Strings = (
      'select ID, Discrip'
      'from Write_off_Param'
      'where Itype = :IType'
      '')
    OnNewRecord = quD_writeoffNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 628
    Top = 128
    poSQLINT64ToBCD = True
    object quD_writeoffID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quD_writeoffDISCRIP: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'DISCRIP'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsD_writeoff: TDataSource
    DataSet = quD_writeoff
    Left = 632
    Top = 184
  end
  object dsRecipGift: TDataSource
    DataSet = quReciepGift
    Left = 776
    Top = 284
  end
  object quReciepGift: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Writeoff_sinv'
      ' Set  ActName_ID = 8,'
      '      Company = :ReciepCompany, '
      '      Company_Address = :ReciepAddress  '
      'where SinvID = :SInvID')
    InsertSQL.Strings = (
      
        'insert into Writeoff_sinv(SInvID, ActName_ID, WriteOffReason_ID,' +
        ' DefectDiscript_ID, spoilingreason_ID,'
      '         Solution_ID, Company, Company_Address)'
      'values(:SInvID,8,-2,-3,-4,-5,:ReciepCompany,:ReciepAddress)'
      '')
    SelectSQL.Strings = (
      
        'select Company as ReciepCompany, Company_Address as ReciepAddres' +
        's'
      'from Writeoff_sinv '
      'where SinvID = :SInvID')
    AfterPost = CommitRetaining
    BeforeOpen = quReciepGiftBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 768
    Top = 236
    poSQLINT64ToBCD = True
    object quReciepGiftRECIEPCOMPANY: TFIBStringField
      FieldName = 'RECIEPCOMPANY'
      Size = 40
      EmptyStrToNull = True
    end
    object quReciepGiftRECIEPADDRESS: TFIBStringField
      FieldName = 'RECIEPADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object quSurPlusItem: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure DINVITEM_D(:SITEMID)'
      '')
    RefreshSQL.Strings = (
      'select si.uid, si.sitemid, si.selid, si.art2id,'
      '       sl.price, sl.price2, sl.unitid,'
      '       a2.art, a2.art2, a2.prodcode, a2.d_matid,'
      '       a2.d_goodid, a2.d_insid, si.sz,'
      '       si.w, si.q0, a2.d_countryid'
      'from sitem si, sel sl, art2 a2'
      'where si.selid=sl.selid and'
      '      si.sitemid=:sitemid and'
      '      si.art2id=a2.art2id')
    SelectSQL.Strings = (
      'select si.uid, si.sitemid, si.selid, si.art2id,'
      '       sl.price, sl.price2, sl.unitid,'
      '       a2.art, a2.art2, a2.prodcode, a2.d_matid,'
      '       a2.d_goodid, a2.d_insid, si.sz,'
      '       si.w, si.q0, a2.d_countryid'
      'from sitem si, sel sl, art2 a2'
      'where si.selid=sl.selid and'
      '      sl.sinvid=:sinvid and'
      '      si.art2id=a2.art2id'
      'order by si.sitemid')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = quSurPlusItemBeforeOpen
    OnCalcFields = quSurPlusItemCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 728
    Top = 8
    poSQLINT64ToBCD = True
    object quSurPlusItemUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quSurPlusItemSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object quSurPlusItemSELID: TFIBIntegerField
      FieldName = 'SELID'
    end
    object quSurPlusItemART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quSurPlusItemPRICE: TFIBFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object quSurPlusItemPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object quSurPlusItemUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quSurPlusItemART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quSurPlusItemART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quSurPlusItemPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quSurPlusItemQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object quSurPlusItemD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quSurPlusItemSCost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCost'
      currency = True
      Calculated = True
    end
    object quSurPlusItemCost2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'Cost2'
      currency = True
      Calculated = True
    end
    object quSurPlusItemUnit: TStringField
      FieldKind = fkCalculated
      FieldName = 'Unit'
      Size = 5
      Calculated = True
    end
    object quSurPlusItemRno: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Rno'
      Calculated = True
    end
  end
  object dsSurPlusItem: TDataSource
    DataSet = quSurPlusItem
    Left = 728
    Top = 56
  end
  object quEquipment: TpFIBDataSet
    UpdateSQL.Strings = (
      'update sinv '
      'set supid=:supid,'
      '    depid=:depid,'
      '    sdate=:sdate,'
      '    ndate=:ndate,'
      '    ssf=:ssf,'
      '    sn=:sn,'
      '    isclosed=:isclosed,'
      '    itype=:itype,'
      '    userid=:userid,'
      '    cruserid=:cruserid,'
      '    c=:cost'
      'where sinvid=:sinvid')
    DeleteSQL.Strings = (
      'delete from sinv where sinvid=:sinvid')
    InsertSQL.Strings = (
      
        'insert into sinv (sinvid, supid, depid, sdate, ndate, ssf, sn, i' +
        'sclosed, itype,'
      '                  userid, cruserid, c)'
      
        'values (?sinvid, ?supid, ?depid, ?sdate, ?ndate, ?ssf, ?sn, ?isc' +
        'losed, ?itype,'
      '        ?userid, ?cruserid, ?cost)')
    RefreshSQL.Strings = (
      'select  sinvid, supid, sup, depid, depname, sdate, '
      '        ndate, ssf, sn, isclosed, itype, userid,'
      '        closeuser, cruserid, createuser, cost, color'
      'from Equipment_R (:sinvid)')
    SelectSQL.Strings = (
      'select  sinvid, supid, sup, depid, depname, sdate, '
      '        ndate, ssf, sn, isclosed, itype, userid,'
      '        closeuser, cruserid, createuser, cost, color'
      'from Equipment_S (:bd, :ed)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = quEquipmentBeforeOpen
    OnNewRecord = quEquipmentNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 776
    Top = 352
    poSQLINT64ToBCD = True
    object quEquipmentSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quEquipmentSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object quEquipmentSUP: TFIBStringField
      FieldName = 'SUP'
      Size = 60
      EmptyStrToNull = True
    end
    object quEquipmentDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object quEquipmentDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object quEquipmentSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object quEquipmentNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quEquipmentSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quEquipmentSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object quEquipmentISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object quEquipmentITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object quEquipmentUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object quEquipmentCLOSEUSER: TFIBStringField
      FieldName = 'CLOSEUSER'
      Size = 30
      EmptyStrToNull = True
    end
    object quEquipmentCRUSERID: TFIBIntegerField
      FieldName = 'CRUSERID'
    end
    object quEquipmentCREATEUSER: TFIBStringField
      FieldName = 'CREATEUSER'
      Size = 30
      EmptyStrToNull = True
    end
    object quEquipmentCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quEquipmentCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
  end
  object dsEquipment: TDataSource
    DataSet = quEquipment
    Left = 776
    Top = 400
  end
  object taUidStoreDepItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select u.UIDSTOREDEPID, u.SINVID, u.UID, u.ARTID,'
      '       u.ART2ID, u.SZ, u.GR, u.DEPID, u.UNITID,'
      '       u.Q0, u.W, u.ENTRYNUM, u.DINVNUMFROM, '
      '       u.DINVNUM, u.SELLNUM, u.RETNUM,'
      '       u.RESNUM, u.ENTRYCOST, u.DINVCOST,'
      '       u.DINVFROMCOST, u.SELLCOST, u.RETCOST,'
      '       u.RESCOST, u.SELLCOST2, u.RETCOST2,'
      '       u.ACTALLOWANCESNUM, u.ACTALLOWANCESCOST,'
      '       u.ENTRYNUMCOM, u.DINVNUMFROMCOM,'
      '       u.DINVNUMCOM, u.SELLNUMCOM, u.RETNUMCOM,'
      '       u.RESNUMCOM, u.ENTRYCOSTCOM, u.DINVCOSTCOM,'
      '       u.DINVFROMCOSTCOM, u.SELLCOSTCOM,'
      '       u.RETCOSTCOM, u.RESCOSTCOM, u.SELLCOST2COM,'
      '       u.RETCOST2COM, u.ACTALLOWANCESNUMCOM,'
      '       u.ACTALLOWANCESCOSTCOM, u.ACTDISCOUNTSELLNUM,'
      '       u.ACTDISCOUNTRETNUM, u.ACTDISCOUNTSELLCOST,'
      '       u.ACTDISCOUNTRETCOST, u.ACTNUM, u.ACTCOST,'
      '       u.ACTDISCOUNTSELLNUMCOM, u.ACTDISCOUNTRETNUMCOM,'
      '       u.ACTDISCOUNTSELLCOSTCOM, u.ACTDISCOUNTRETCOSTCOM,'
      '       u.ACTNUMCOM, u.ACTCOSTCOM, a.Art'
      'From UID_STORE_DEP u, D_Art a'
      'where u.sinvid=:sinvid and'
      '      u.GR between :GR1 and :GR2 and'
      '      u.Artid = a.d_artid and'
      '      u.uid between :uid1 and :uid2'
      ''
      ''
      '')
    BeforeOpen = taUidStoreDepItemBeforeOpen
    OnCalcFields = taUidStoreDepItemCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 832
    Top = 8
    poSQLINT64ToBCD = True
    object taUidStoreDepItemUIDSTOREDEPID: TFIBIntegerField
      FieldName = 'UIDSTOREDEPID'
    end
    object taUidStoreDepItemSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taUidStoreDepItemUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taUidStoreDepItemARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taUidStoreDepItemART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taUidStoreDepItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taUidStoreDepItemGR: TFIBStringField
      FieldName = 'GR'
      EmptyStrToNull = True
    end
    object taUidStoreDepItemDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taUidStoreDepItemUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object taUidStoreDepItemQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taUidStoreDepItemW: TFIBFloatField
      FieldName = 'W'
    end
    object taUidStoreDepItemENTRYNUM: TFIBSmallIntField
      FieldName = 'ENTRYNUM'
    end
    object taUidStoreDepItemDINVNUMFROM: TFIBSmallIntField
      FieldName = 'DINVNUMFROM'
    end
    object taUidStoreDepItemDINVNUM: TFIBSmallIntField
      FieldName = 'DINVNUM'
    end
    object taUidStoreDepItemSELLNUM: TFIBSmallIntField
      FieldName = 'SELLNUM'
    end
    object taUidStoreDepItemRETNUM: TFIBSmallIntField
      FieldName = 'RETNUM'
    end
    object taUidStoreDepItemRESNUM: TFIBSmallIntField
      FieldName = 'RESNUM'
    end
    object taUidStoreDepItemENTRYCOST: TFIBFloatField
      FieldName = 'ENTRYCOST'
      currency = True
    end
    object taUidStoreDepItemDINVCOST: TFIBFloatField
      FieldName = 'DINVCOST'
      currency = True
    end
    object taUidStoreDepItemDINVFROMCOST: TFIBFloatField
      FieldName = 'DINVFROMCOST'
      currency = True
    end
    object taUidStoreDepItemSELLCOST: TFIBFloatField
      FieldName = 'SELLCOST'
      currency = True
    end
    object taUidStoreDepItemRETCOST: TFIBFloatField
      FieldName = 'RETCOST'
      currency = True
    end
    object taUidStoreDepItemRESCOST: TFIBFloatField
      FieldName = 'RESCOST'
      currency = True
    end
    object taUidStoreDepItemSELLCOST2: TFIBFloatField
      FieldName = 'SELLCOST2'
      currency = True
    end
    object taUidStoreDepItemRETCOST2: TFIBFloatField
      FieldName = 'RETCOST2'
      currency = True
    end
    object taUidStoreDepItemACTALLOWANCESNUM: TFIBIntegerField
      FieldName = 'ACTALLOWANCESNUM'
    end
    object taUidStoreDepItemACTALLOWANCESCOST: TFIBFloatField
      FieldName = 'ACTALLOWANCESCOST'
      currency = True
    end
    object taUidStoreDepItemENTRYNUMCOM: TFIBSmallIntField
      FieldName = 'ENTRYNUMCOM'
    end
    object taUidStoreDepItemDINVNUMFROMCOM: TFIBSmallIntField
      FieldName = 'DINVNUMFROMCOM'
    end
    object taUidStoreDepItemDINVNUMCOM: TFIBSmallIntField
      FieldName = 'DINVNUMCOM'
    end
    object taUidStoreDepItemSELLNUMCOM: TFIBSmallIntField
      FieldName = 'SELLNUMCOM'
    end
    object taUidStoreDepItemRETNUMCOM: TFIBSmallIntField
      FieldName = 'RETNUMCOM'
    end
    object taUidStoreDepItemRESNUMCOM: TFIBSmallIntField
      FieldName = 'RESNUMCOM'
    end
    object taUidStoreDepItemENTRYCOSTCOM: TFIBFloatField
      FieldName = 'ENTRYCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemDINVCOSTCOM: TFIBFloatField
      FieldName = 'DINVCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemDINVFROMCOSTCOM: TFIBFloatField
      FieldName = 'DINVFROMCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemSELLCOSTCOM: TFIBFloatField
      FieldName = 'SELLCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemRETCOSTCOM: TFIBFloatField
      FieldName = 'RETCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemRESCOSTCOM: TFIBFloatField
      FieldName = 'RESCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemSELLCOST2COM: TFIBFloatField
      FieldName = 'SELLCOST2COM'
      currency = True
    end
    object taUidStoreDepItemRETCOST2COM: TFIBFloatField
      FieldName = 'RETCOST2COM'
      currency = True
    end
    object taUidStoreDepItemACTALLOWANCESNUMCOM: TFIBSmallIntField
      FieldName = 'ACTALLOWANCESNUMCOM'
    end
    object taUidStoreDepItemACTALLOWANCESCOSTCOM: TFIBFloatField
      FieldName = 'ACTALLOWANCESCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemACTDISCOUNTSELLNUM: TFIBSmallIntField
      FieldName = 'ACTDISCOUNTSELLNUM'
    end
    object taUidStoreDepItemACTDISCOUNTRETNUM: TFIBSmallIntField
      FieldName = 'ACTDISCOUNTRETNUM'
    end
    object taUidStoreDepItemACTDISCOUNTSELLCOST: TFIBFloatField
      FieldName = 'ACTDISCOUNTSELLCOST'
      currency = True
    end
    object taUidStoreDepItemACTDISCOUNTRETCOST: TFIBFloatField
      FieldName = 'ACTDISCOUNTRETCOST'
      currency = True
    end
    object taUidStoreDepItemACTNUM: TFIBSmallIntField
      FieldName = 'ACTNUM'
    end
    object taUidStoreDepItemACTCOST: TFIBFloatField
      FieldName = 'ACTCOST'
      currency = True
    end
    object taUidStoreDepItemACTDISCOUNTSELLNUMCOM: TFIBSmallIntField
      FieldName = 'ACTDISCOUNTSELLNUMCOM'
    end
    object taUidStoreDepItemACTDISCOUNTRETNUMCOM: TFIBSmallIntField
      FieldName = 'ACTDISCOUNTRETNUMCOM'
    end
    object taUidStoreDepItemACTDISCOUNTSELLCOSTCOM: TFIBFloatField
      FieldName = 'ACTDISCOUNTSELLCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemACTDISCOUNTRETCOSTCOM: TFIBFloatField
      FieldName = 'ACTDISCOUNTRETCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemACTNUMCOM: TFIBSmallIntField
      FieldName = 'ACTNUMCOM'
    end
    object taUidStoreDepItemACTCOSTCOM: TFIBFloatField
      FieldName = 'ACTCOSTCOM'
      currency = True
    end
    object taUidStoreDepItemART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taUidStoreDepItemUNIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'UNIT'
      Size = 5
      Calculated = True
    end
  end
  object dsUidStoreDepItem: TDataSource
    DataSet = taUidStoreDepItem
    Left = 832
    Top = 56
  end
  object quCompBalans: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select b.id, b.c PAY, b.compid, dr.bb PDATE, cast ('#39#1085#1072#1095#1072#1083#1100#1085#1099#1081' '#1073#1072 +
        #1083#1072#1085#1089#39' as char(20)) DESCRIPTON'
      'from balanceb b, d_rec dr'
      'where b.compid=:d_compid'
      'union'
      
        'select p.id, p.c PAY, p.compid, p.pd PDATE, cast('#39#1086#1087#1083#1072#1090#1099#39' as cha' +
        'r(20)) DESCRIPTON'
      'from payment p'
      'where p.compid=:d_compid'
      'union'
      
        'select tm.id, tm.balance_e PAY, tm.compid, cast('#39'NOW'#39' as timesta' +
        'mp) PDATE, cast('#39#1073#1072#1083#1072#1085#1089#39' as char(20)) DESCRIPTON'
      'from tmp_balance tm'
      'where tm.compid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 304
    Top = 376
    poSQLINT64ToBCD = True
    object quCompBalansID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCompBalansPAY: TFIBFloatField
      FieldName = 'PAY'
    end
    object quCompBalansCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object quCompBalansPDATE: TFIBDateTimeField
      FieldName = 'PDATE'
    end
    object quCompBalansDESCRIPTON: TFIBStringField
      FieldName = 'DESCRIPTON'
      EmptyStrToNull = True
    end
  end
  object dsCompBalans: TDataSource
    DataSet = quCompBalans
    Left = 304
    Top = 424
  end
  object quCompRemains: TpFIBDataSet
    SelectSQL.Strings = (
      'select r.id, r.art2id, a2.fullart'
      'from remains r, art2 a2'
      'where a2.art2id=r.art2id'
      ' and r.d_compid=:d_compid')
    BeforeOpen = quCompD_artBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 376
    Top = 376
    poSQLINT64ToBCD = True
    object quCompRemainsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCompRemainsART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quCompRemainsFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsCompRemains: TDataSource
    DataSet = quCompRemains
    Left = 376
    Top = 424
  end
  object taRespStoring: TpFIBDataSet
    UpdateSQL.Strings = (
      'update respstoring'
      'set datenresp=:datenresp'
      'where respstoringid=:respstoringid')
    DeleteSQL.Strings = (
      'delete from respstoring where respstoringid=:respstoringid')
    RefreshSQL.Strings = (
      'select sl.rn, si.adate, a2.prodcode, a2.art, a2.art2,'
      '       a2.d_goodid, a2.d_insid, a2.d_matid, a2.d_countryid,'
      '       rs.uid, rs.stateuid, rs.userid, de.fio, rs.confirm,'
      '       rs.retclient, rs.retaddress, dr.ret, rs.dateresp,'
      '       rs.respstoringid, sl.depid, dd.sname, dd.color,'
      '       si.price, si.price0, si.q0, si.sz, si.w, rs.datenresp'
      'from respstoring rs, sell sl, sellitem si, art2 a2, d_emp de,'
      '     d_ret dr, d_dep dd'
      'where sl.sellid = rs.sellid and'
      '      rs.sellitemid = si.sellitemid and'
      '      a2.art2id = si.art2id and'
      '      de.d_empid = rs.userid and'
      '      dr.d_retid = rs.d_retid and'
      '      dd.d_depid = sl.depid and'
      '      rs.respstoringid = :respstoringid')
    SelectSQL.Strings = (
      'select sl.rn, si.adate, a2.prodcode, a2.art, a2.art2,'
      '       a2.d_goodid, a2.d_insid, a2.d_matid, a2.d_countryid,'
      '       rs.uid, rs.stateuid, rs.userid, de.fio, rs.confirm,'
      '       rs.retclient, rs.retaddress, dr.ret, rs.dateresp,'
      '       rs.respstoringid, sl.depid, dd.sname, dd.color,'
      '       si.price, si.price0, si.q0, si.sz, si.w, rs.datenresp'
      'from respstoring rs, sell sl, sellitem si, art2 a2, d_emp de,'
      '     d_ret dr, d_dep dd'
      'where sl.sellid = rs.sellid and'
      '      rs.sellitemid = si.sellitemid and'
      '      a2.art2id = si.art2id and'
      '      de.d_empid = rs.userid and'
      '      dr.d_retid = rs.d_retid and'
      '      dd.d_depid = sl.depid and'
      
        '    (((rs.dateresp between :bd and :ed) and (rs.stateuid=4)) or ' +
        '(rs.stateuid<>4) ) and'
      '      sl.depid between :depid1 and :depid2'
      ''
      '')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taRespStoringBeforeOpen
    OnCalcFields = taRespStoringCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 832
    Top = 120
    poSQLINT64ToBCD = True
    object taRespStoringRN: TFIBIntegerField
      FieldName = 'RN'
    end
    object taRespStoringADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object taRespStoringPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taRespStoringART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRespStoringD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taRespStoringSTATEUID: TFIBIntegerField
      FieldName = 'STATEUID'
    end
    object taRespStoringUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taRespStoringFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRespStoringCONFIRM: TFIBSmallIntField
      FieldName = 'CONFIRM'
    end
    object taRespStoringRETCLIENT: TFIBStringField
      FieldName = 'RETCLIENT'
      Size = 60
      EmptyStrToNull = True
    end
    object taRespStoringRETADDRESS: TFIBStringField
      FieldName = 'RETADDRESS'
      Size = 110
      EmptyStrToNull = True
    end
    object taRespStoringRET: TFIBStringField
      FieldName = 'RET'
      Size = 40
      EmptyStrToNull = True
    end
    object taRespStoringDATERESP: TFIBDateTimeField
      FieldName = 'DATERESP'
    end
    object taRespStoringRESPSTORINGID: TFIBIntegerField
      FieldName = 'RESPSTORINGID'
    end
    object taRespStoringDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taRespStoringSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taRespStoringCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taRespStoringPRICE: TFIBFloatField
      FieldName = 'PRICE'
      currency = True
    end
    object taRespStoringPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
      currency = True
    end
    object taRespStoringCOST: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COST'
      currency = True
      Calculated = True
    end
    object taRespStoringCOST0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COST0'
      currency = True
      Calculated = True
    end
    object taRespStoringUNIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'UNIT'
      Size = 10
      Calculated = True
    end
    object taRespStoringSate: TStringField
      FieldKind = fkCalculated
      FieldName = 'Sate'
      Size = 50
      Calculated = True
    end
    object taRespStoringQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taRespStoringSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taRespStoringW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taRespStoringSCOST0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST0'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST1'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST2'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST3: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST3'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST00: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST00'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST01: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST01'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST02: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST02'
      currency = True
      Calculated = True
    end
    object taRespStoringSCOST03: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST03'
      currency = True
      Calculated = True
    end
    object taRespStoringSQ0: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQ0'
      Calculated = True
    end
    object taRespStoringSQ1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQ1'
      Calculated = True
    end
    object taRespStoringSQ2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQ2'
      Calculated = True
    end
    object taRespStoringSQ3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQ3'
      Calculated = True
    end
    object taRespStoringSW0: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SW0'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRespStoringSW1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SW1'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRespStoringSW2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SW2'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRespStoringSW3: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SW3'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRespStoringDATENRESP: TFIBDateTimeField
      FieldName = 'DATENRESP'
    end
  end
  object dsRespStoring: TDataSource
    DataSet = taRespStoring
    Left = 832
    Top = 176
  end
  object taRepairList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE SINV'
      'SET COMPID=?COMPID,'
      '    SDATE=?SDATE,'
      '    SN=?SN,'
      '    PMARGIN=?PMARGIN,'
      '    TR=?TR,'
      '    AKCIZ=?AKCIZ,'
      '    ISCLOSED=?ISCLOSED,'
      '    PTR=:PTR,'
      '    PTRNDS=:PTRNDS,'
      '    TRNDS=:TRNDS,'
      '    RET_NONDS=:RET_NONDS'
      'WHERE SINVID=?OLD_SINVID ')
    DeleteSQL.Strings = (
      'DELETE FROM SInv'
      'WHERE SINVID=?OLD_SINVID')
    InsertSQL.Strings = (
      'INSERT INTO SINV'
      '(SINVID, DEPFROMID, SDATE, SN, PMARGIN, TR, AKCIZ,'
      
        ' ISCLOSED, COMPID, ITYPE, USERID, TR, PTR, PTRNDS, TRNDS, RET_NO' +
        'NDS)'
      'VALUES'
      '(?SINVID, ?DEPFROMID, ?SDATE, ?SN, ?PMARGIN, ?TR, ?AKCIZ,'
      
        ' ?ISCLOSED, ?COMPID, 9, ?USERID, ?TR, ?PTR, ?PTRNDS, ?TRNDS, ?RE' +
        'T_NONDS)')
    RefreshSQL.Strings = (
      'SELECT SINVID, DEPFROMID, SDATE, SN, PMARGIN, TR,'
      '       COST, R_COUNT, AKCIZ, ISCLOSED, DEPFROM,'
      '       COMPID, COMP, USERID, FIO, W, PTR,'
      '       PTRNDS, TRNDS, RET_NONDS'
      'FROM REPIARLIST_R(:SINVID)')
    SelectSQL.Strings = (
      'SELECT SINVID, DEPFROMID, SDATE, SN, PMARGIN, TR,'
      '       COST, R_COUNT, AKCIZ, ISCLOSED, DEPFROM,'
      '       COMPID, COMP, USERID, FIO, W, PTR,'
      '       PTRNDS, TRNDS, RET_NONDS'
      'FROM REPIARLIST_S(:BD, :ED)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taRepairListBeforeDelete
    BeforeEdit = taRepairListBeforeEdit
    BeforeOpen = taRepairListBeforeOpen
    OnCalcFields = taRepairListCalcFields
    OnNewRecord = taRepairListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 840
    Top = 232
    poSQLINT64ToBCD = True
    object taRepairListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taRepairListDEPFROMID: TFIBIntegerField
      FieldName = 'DEPFROMID'
    end
    object taRepairListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taRepairListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object taRepairListPMARGIN: TFIBFloatField
      FieldName = 'PMARGIN'
    end
    object taRepairListTR: TFIBFloatField
      FieldName = 'TR'
      currency = True
    end
    object taRepairListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taRepairListR_COUNT: TFIBIntegerField
      FieldName = 'R_COUNT'
    end
    object taRepairListAKCIZ: TFIBFloatField
      FieldName = 'AKCIZ'
    end
    object taRepairListISCLOSED: TFIBSmallIntField
      FieldName = 'ISCLOSED'
    end
    object taRepairListDEPFROM: TFIBStringField
      FieldName = 'DEPFROM'
      Size = 60
      EmptyStrToNull = True
    end
    object taRepairListCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taRepairListCOMP: TFIBStringField
      FieldName = 'COMP'
      Size = 60
      EmptyStrToNull = True
    end
    object taRepairListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taRepairListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRepairListW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taRepairListPTR: TFIBFloatField
      FieldName = 'PTR'
    end
    object taRepairListPTRNDS: TFIBFloatField
      FieldName = 'PTRNDS'
    end
    object taRepairListTRNDS: TFIBFloatField
      FieldName = 'TRNDS'
    end
    object taRepairListRET_NONDS: TFIBSmallIntField
      FieldName = 'RET_NONDS'
    end
    object taRepairListSCOSTOPEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOSTOPEN'
      currency = True
      Calculated = True
    end
    object taRepairListSCOSTCLOSED: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOSTCLOSED'
      currency = True
      Calculated = True
    end
    object taRepairListSTROPEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'STROPEN'
      currency = True
      Calculated = True
    end
    object taRepairListSTRCLOSED: TFloatField
      FieldKind = fkCalculated
      FieldName = 'STRCLOSED'
      currency = True
      Calculated = True
    end
    object taRepairListCOUNTOPEN: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'COUNTOPEN'
      Calculated = True
    end
    object taRepairListCOUNTCLOSED: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'COUNTCLOSED'
      Calculated = True
    end
    object taRepairListSWOPEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SWOPEN'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRepairListSWCLOSED: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SWCLOSED'
      DisplayFormat = '0.##'
      Calculated = True
    end
    object taRepairListSCOST1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SCOST1'
      Calculated = True
    end
    object taRepairListSTR1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'STR1'
      Calculated = True
    end
    object taRepairListCOUNT1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'COUNT1'
      Calculated = True
    end
    object taRepairListSW1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SW1'
      Calculated = True
    end
    object taRepairListSQOPEN: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQOPEN'
      Calculated = True
    end
    object taRepairListSQCLOSED: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQCLOSED'
      Calculated = True
    end
    object taRepairListSQ1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SQ1'
      Calculated = True
    end
  end
  object dsRepairList: TDataSource
    DataSet = taRepairList
    Left = 840
    Top = 288
  end
  object taRepair: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE REPAIR'
      'SET D_RETID=:D_RETID'
      'WHERE REPAIRID=:REPAIRID')
    DeleteSQL.Strings = (
      'delete from repair where repairid=:repairid')
    InsertSQL.Strings = (
      
        'INSERT INTO REPAIR (REPAIRID, UID, SITEMID, RESPSTORINGID, SINVI' +
        'D, D_RETID)'
      
        'VALUES (:REPAIRID, :UID, :SITEMID, :RESPSTORINGID, :SINVID, :D_R' +
        'ETID)')
    RefreshSQL.Strings = (
      'SELECT REPAIRID, SINVID, ART2ID, SITEMID, UID, W,'
      '       SZ, Q0, SPRICE, COST, SDATE, SUPID, SSF,'
      '       SN, NDATE, NDSID, PRODID, PROD, GOODID,'
      '       INSID, MATID, ART, ART2, SUP, UNITID,'
      '       D_RETID, respstoringid'
      'FROM REPAIR_R(:REPAIRID)')
    SelectSQL.Strings = (
      'SELECT REPAIRID, SINVID, ART2ID, SITEMID, UID, W,'
      '       SZ, Q0, SPRICE, COST, SDATE, SUPID, SSF,'
      '       SN, NDATE, NDSID, PRODID, PROD, GOODID,'
      '       INSID, MATID, ART, ART2, SUP, UNITID,'
      '       D_RETID, respstoringid'
      'FROM REPAIR_S(:SINVID)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taRepairBeforeDelete
    BeforeEdit = taRepairBeforeEdit
    BeforeOpen = taRepairBeforeOpen
    OnNewRecord = taRepairNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 840
    Top = 344
    poSQLINT64ToBCD = True
    object taRepairREPAIRID: TFIBIntegerField
      FieldName = 'REPAIRID'
    end
    object taRepairSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taRepairART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taRepairSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object taRepairUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taRepairW: TFIBFloatField
      FieldName = 'W'
    end
    object taRepairSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taRepairQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taRepairSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
    object taRepairCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taRepairSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object taRepairSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taRepairSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taRepairSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object taRepairNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object taRepairNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object taRepairPRODID: TFIBIntegerField
      FieldName = 'PRODID'
    end
    object taRepairPROD: TFIBStringField
      FieldName = 'PROD'
      EmptyStrToNull = True
    end
    object taRepairGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRepairINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRepairMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRepairART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taRepairART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRepairSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object taRepairUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object taRepairD_RETID: TFIBStringField
      FieldName = 'D_RETID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRepairUNIT: TStringField
      FieldKind = fkCalculated
      FieldName = 'UNIT'
      Size = 10
      Calculated = True
    end
    object taRepairRET: TStringField
      FieldKind = fkLookup
      FieldName = 'RET'
      LookupDataSet = dmCom.taRet
      LookupKeyFields = 'D_RETID'
      LookupResultField = 'RET'
      KeyFields = 'D_RETID'
      Size = 30
      Lookup = True
    end
    object taRepairRESPSTORINGID: TFIBIntegerField
      FieldName = 'RESPSTORINGID'
    end
  end
  object dsRepair: TDataSource
    DataSet = taRepair
    Left = 840
    Top = 392
  end
  object taSuspItemList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update sinv'
      'set sn = :sn,'
      '    userid = :userid'
      'where sinvid=:sinvid')
    DeleteSQL.Strings = (
      'delete from sinv where sinvid=:sinvid')
    InsertSQL.Strings = (
      
        'insert into sinv (sinvid, sn, depid, sdate, itype, userid, rstat' +
        'e)'
      'values (:sinvid, :sn, :depid, :sdate, 10, :userid, 0)')
    RefreshSQL.Strings = (
      'select SINVID, SN, DEPID, SDATE, ITYPE, USERID,'
      '  RSTATE, FIO, SNAME, COST, W, Q, depcolor'
      'FROM SUSPITEMLIST_R(:SINVID)')
    SelectSQL.Strings = (
      'select SINVID, SN, DEPID, SDATE, ITYPE, USERID,'
      '  RSTATE, FIO, SNAME, COST, W, Q, depcolor'
      'FROM SUSPITEMLIST_S(:DEPID1, :DEPID2)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeEdit = taSuspItemListBeforeEdit
    BeforeOpen = taSuspItemListBeforeOpen
    OnCalcFields = taSuspItemListCalcFields
    OnNewRecord = taSuspItemListNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 240
    Top = 256
    poSQLINT64ToBCD = True
    object taSuspItemListSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taSuspItemListSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object taSuspItemListDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taSuspItemListITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object taSuspItemListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taSuspItemListRSTATE: TFIBIntegerField
      FieldName = 'RSTATE'
    end
    object taSuspItemListFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSuspItemListSNAME: TFIBStringField
      FieldName = 'SNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taSuspItemListCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taSuspItemListW: TFIBFloatField
      FieldName = 'W'
    end
    object taSuspItemListQ: TFIBIntegerField
      FieldName = 'Q'
      DisplayFormat = '0.##'
    end
    object taSuspItemListDEPCOLOR: TFIBIntegerField
      FieldName = 'DEPCOLOR'
    end
    object taSuspItemListState: TStringField
      FieldKind = fkCalculated
      FieldName = 'State'
      Calculated = True
    end
    object taSuspItemListSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
  end
  object dsSuspItemList: TDataSource
    DataSet = taSuspItemList
    Left = 240
    Top = 304
  end
  object taSuspItem: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure DINVITEM_D (?OLD_SITEMID)')
    InsertSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select sitemid, uid, sz, w, unit, sprice, price2,'
      '       scost, cost2, art2id, prordcode, d_matid,'
      '       d_goodid, d_insid, artid, d_countryid,'
      '       art, art2'
      'from SuspItem_S (:sinvid)')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taSuspItemBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 328
    Top = 320
    poSQLINT64ToBCD = True
    object taSuspItemSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object taSuspItemUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taSuspItemSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taSuspItemUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
      currency = True
    end
    object taSuspItemPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
      currency = True
    end
    object taSuspItemSCOST: TFIBFloatField
      FieldName = 'SCOST'
      currency = True
    end
    object taSuspItemCOST2: TFIBFloatField
      FieldName = 'COST2'
      currency = True
    end
    object taSuspItemART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taSuspItemPRORDCODE: TFIBStringField
      FieldName = 'PRORDCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taSuspItemD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSuspItemART: TFIBStringField
      FieldName = 'ART'
      Size = 30
      EmptyStrToNull = True
    end
    object taSuspItemART2: TFIBStringField
      FieldName = 'ART2'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dsSuspItem: TDataSource
    DataSet = taSuspItem
    Left = 392
    Top = 320
  end
  object taUSerUidWh: TpFIBDataSet
    SelectSQL.Strings = (
      'select u.userid, de.fio, de.uidwhbd, de.uidwhed, de.calcuidwh'
      'from uidwh_t u, d_emp de'
      'where u.userid<>-100 and'
      '      u.userid = de.d_empid'
      'group by u.userid, de.fio, de.uidwhbd, de.uidwhed, de.calcuidwh'
      'PLAN SORT (JOIN (DE NATURAL,U INDEX (UIDWH_T_IDX6)))'
      'order by de.fio')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 448
    Top = 448
    object taUSerUidWhUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taUSerUidWhFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taUSerUidWhUIDWHBD: TFIBDateTimeField
      FieldName = 'UIDWHBD'
    end
    object taUSerUidWhUIDWHED: TFIBDateTimeField
      FieldName = 'UIDWHED'
    end
    object taUSerUidWhCALCUIDWH: TFIBSmallIntField
      FieldName = 'CALCUIDWH'
    end
  end
  object dsUserUidWh: TDataSource
    DataSet = taUSerUidWh
    Left = 544
    Top = 448
  end
  object DBProbe: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.id probe$id, p.name probe$name, p.probe, m.id material$' +
        'id, m.name material$name'
      'from d_probe p, d_material m'
      'where p.id >0 and m.id = p.matid'
      'order by m.id, p.probe')
    OnCalcFields = DBProbeCalcFields
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 568
    Top = 528
    oFetchAll = True
    object DBProbePROBEID: TFIBIntegerField
      FieldName = 'PROBE$ID'
    end
    object DBProbePROBENAME: TFIBStringField
      FieldName = 'PROBE$NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object DBProbePROBE: TFIBFloatField
      FieldName = 'PROBE'
    end
    object DBProbeMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
    object DBProbeMATERIALNAME: TFIBStringField
      FieldName = 'MATERIAL$NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object DBProbeNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'NAME'
      Size = 32
      Calculated = True
    end
  end
  object DataSourceProbe: TDataSource
    DataSet = DBProbe
    Left = 496
    Top = 528
  end
end
