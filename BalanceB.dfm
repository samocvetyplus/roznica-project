object fmBalanceB: TfmBalanceB
  Left = 256
  Top = 156
  Width = 531
  Height = 366
  Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1073#1072#1083#1072#1085#1089
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 523
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      DockPos = 0
      TabOrder = 0
      object TBItem2: TTBItem
        Action = acAdd
        DisplayMode = nbdmImageAndText
      end
      object TBItem1: TTBItem
        Action = acDel
        DisplayMode = nbdmImageAndText
      end
      object TBControlItem1: TTBControlItem
        Control = Label1
      end
      object TBControlItem2: TTBControlItem
        Control = edBB
      end
      object Label1: TLabel
        Left = 118
        Top = 3
        Width = 91
        Height = 13
        Caption = ' '#1041#1072#1083#1072#1085#1089' '#1085#1072' '#1082#1086#1085#1077#1094' '
      end
      object edBB: TDBDateTimeEditEh
        Left = 209
        Top = 0
        Width = 77
        Height = 19
        DataField = 'BB'
        DataSource = dsrRec
        EditButton.Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
          262FA31D20B5FF00FF4B4B4B5B5B5B87817B87817BFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FF82776F5F61BC2126B2666059A3A3A4FAFAFAE3
          E0DCD1CEC99A979582807E7874715D5A58FF00FFFF00FFFF00FFFF00FFA89381
          7B75C42024B2BA9775A3A3A4FFFFFFFFFFFFFFFFFDD6D0CAC8C0B8F6EBDDEDE0
          D1A59B916B6864FF00FFFF00FFA893817873C32024B2B99B7EA3A3A4FEFFFFFF
          FFFF9F9FA07371713535377E7B76FFF5E7FFF2E1878685FF00FFFF00FFA89483
          7873C52024B3BAA18AA3A3A4FEFFFFFFFFFFCACACCF5F3F2FFFDF9353537F3EA
          E0FDF0E2878685FF00FFFF00FFA8978B7876CA2024B2BBA896A3A3A4FEFFFFFF
          FFFFB3B3B4828283A2A1A2353537F8F2EBFDF2E9878685FF00FFFF00FF918984
          7779CF2024B2BCADA0A3A3A4FEFFFFFFFFFF9A9A9B3535377C7C7ECCCCCBFFFE
          FBFCF6EF878685FF00FFFF00FF8E8888787BD42024B1BDB2A9A3A3A4FEFEFFFF
          FFFFBCBCBD353537353537353537FFFEFDFCF9F5878685FF00FFFF00FF8E8C8E
          787EDA2024B1B4B3B9A3A3A4FEFEFEE9E9E9E2E2E2BCBCBCA2A2A3DFDFE0FFFF
          FFFCFBFB878685FF00FFFF00FF8E8F947880DE2023B1B4B3B9A3A3A4FFFFFFA5
          A5A5BBBBBB9F9FA0C6C6C7C5C5C6E2E2E3FEFEFE878685FF00FFFF00FF8E8F95
          7880DF2023B1B4B3B9A3A3A4FFFFFFB9B9B9B3B3B4ACACADA8A8A9B9B9BAE0E0
          E1FFFFFF878685FF00FFFF00FF9091977B83E22023B0B6B5B6A3A3A4FFFFFF79
          7979E1E2E2FBFBFCEDEDEDB9B9B9DBDBDCFFFFFF878685FF00FFFF00FF83848A
          646AD21D23BA6F71919E9DAFBBB9BC7F7D738D8C88EDEAE0E7E7E36465629090
          90FFFFFF878685FF00FFFF00FFFF00FF4141632E317D3034833F43933A3D9440
          42964444605655996D6893736E885E5B61918D8A616161FF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF4B4B4BFF00FF3B3D624B4B4B373765FF00FF3333833838
          65141477FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4B4B4B4B
          4B4BFF00FFFF00FF4B4B4E4C4C4CFF00FFFF00FFFF00FFFF00FF}
        EditButton.ShortCut = 16397
        EditButton.Style = ebsGlyphEh
        EditButton.Width = 16
        EditButtons = <>
        Flat = True
        Kind = dtkDateEh
        TabOrder = 0
        Visible = True
      end
    end
  end
  object gridBalanceB: TDBGridEh
    Left = 0
    Top = 23
    Width = 523
    Height = 295
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dsrBalanceB
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        AutoDropDown = True
        DropDownBox.Columns = <
          item
            FieldName = 'NAME'
          end
          item
            FieldName = 'SNAME'
          end>
        EditButtons = <>
        FieldName = 'COMPNAME'
        Footers = <>
        Width = 352
      end
      item
        EditButtons = <>
        FieldName = 'C'
        Footers = <>
        Width = 71
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 318
    Width = 523
    Height = 19
    Panels = <>
  end
  object acEvent: TActionList
    Left = 20
    Top = 48
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 2
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 3
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acClose: TAction
      Caption = 'acClose'
      ShortCut = 27
      OnExecute = acCloseExecute
    end
  end
  object taBalanceB: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BALANCEB'
      'SET '
      '    COMPID = ?COMPID,'
      '    C = ?C'
      'WHERE'
      '    ID = ?OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    BALANCEB'
      'WHERE'
      '        ID = ?OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO BALANCEB('
      '    ID,'
      '    COMPID,'
      '    C'
      ')'
      'VALUES('
      '    ?ID,'
      '    ?COMPID,'
      '    ?C'
      ')')
    RefreshSQL.Strings = (
      'select ID, COMPID, C'
      'from BalanceB'
      ' WHERE '
      '        BALANCEB.ID = ?OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select ID, COMPID, C'
      'from BalanceB')
    AfterClose = CommitRetaining
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PastBeforeClose
    OnNewRecord = taBalanceBNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 20
    Top = 100
    poSQLINT64ToBCD = True
    object taBalanceBID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taBalanceBCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taBalanceBC: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'C'
      currency = True
    end
    object taBalanceBCOMPNAME: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      FieldKind = fkLookup
      FieldName = 'COMPNAME'
      LookupDataSet = taComp
      LookupKeyFields = 'D_COMPID'
      LookupResultField = 'SNAME'
      KeyFields = 'COMPID'
      Size = 60
      EmptyStrToNull = True
      Lookup = True
    end
  end
  object dsrBalanceB: TDataSource
    DataSet = taBalanceB
    Left = 20
    Top = 148
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.D_CompId, c.Name, c.sname'
      'from D_Comp c '
      'where c.d_compid<>-1000 and c.seller>=1 and workorg=1'
      '      and c.d_compid<>-1'
      'order by c.SortInd')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 84
    Top = 100
    poSQLINT64ToBCD = True
    object taCompD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taCompNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taCompSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object taRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'update D_Rec set'
      '  BB=:BB')
    SelectSQL.Strings = (
      'select BB'
      'from D_REC')
    AfterClose = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PastBeforeClose
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 144
    Top = 100
    poSQLINT64ToBCD = True
    object taRecBB: TFIBDateTimeField
      FieldName = 'BB'
    end
  end
  object dsrRec: TDataSource
    DataSet = taRec
    Left = 144
    Top = 148
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 248
    Top = 64
  end
end
