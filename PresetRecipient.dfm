object frmPresetRecipient: TfrmPresetRecipient
  Left = 495
  Top = 365
  Width = 290
  Height = 116
  Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 4
    Width = 65
    Height = 13
    Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100': '
  end
  object Label2: TLabel
    Left = 0
    Top = 32
    Width = 34
    Height = 13
    Caption = #1040#1076#1088#1077#1089':'
  end
  object DBEditEh1: TDBEditEh
    Left = 64
    Top = 4
    Width = 205
    Height = 21
    DataField = 'RECIEPCOMPANY'
    DataSource = dm3.dsRecipGift
    EditButtons = <>
    TabOrder = 0
    Visible = True
  end
  object DBEditEh2: TDBEditEh
    Left = 64
    Top = 32
    Width = 205
    Height = 21
    DataField = 'RECIEPADDRESS'
    DataSource = dm3.dsRecipGift
    EditButtons = <>
    TabOrder = 1
    Visible = True
  end
  object BitBtn1: TBitBtn
    Left = 52
    Top = 60
    Width = 75
    Height = 25
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 180
    Top = 60
    Width = 75
    Height = 25
    TabOrder = 3
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
end
