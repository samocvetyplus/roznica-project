unit JewConst;

interface

resourcestring
  // ��� ���� ����������� 50 ��������
  sLog_OptSell = '������� �������';
  sLog_Rest = '��������� �������� � ���������';
  sLog_SRet = '������� �����������';
  sLog_ORet = '������� �� �������� ����������';
  sLog_SellRet = '������� �� ��������� �����������';
  sLog_ChangePrice = '����������. ��������� ����';
  sLog_ArtHist = '������� ��������';
  sLog_AMHist  = '������� ����������� 1-� ���������';
  sLog_A2MHist = '������� ����������� 2-� ���������';
  sLog_UIDHist = '������� �������';
  sLog_UIDPriceHist = '������� ���� �������';
  sLog_UIDInsHist = '������� ��������� ���-� �������';
  sLog_Inventory = '��������������';
  sLog_UIDStore = '��������� ���������';
  sLog_UIDStoreDep = '��������� ��������� �� ��������';  
  sLog_ShopReport = '�������� ����� �� ��������';
  sLog_FRest = '�������';
  sLog_Setting = '�������� �� ���������';
  sLog_Setting1 = '���������';
  sLog_ColorSchema = '�������� �����';
  sLog_AnalizSz = '������ (�������)';
  sLog_AnalizDep = '������ �� �������';
  sLog_PriceAnalysis = 'Price analysis';
  sLog_WarehouseAnalysis = 'Warehouse analysis';
  sLog_ApplList = '������ �����������';
  sLog_ApplDepList = '������ �� �������';  
  sLog_ExportWithRest = '������� � ���������';
  sLog_ExportNoRest = '������� ��� ��������';
  sLog_ExportWithRestNoReCalc = '������� � ��������� ��� ���������';
  sLog_ExportSetting = '��������� ���������� ��� ��������';

  sLog_AddOptSell = '���������� ������� �������';
  sLog_DelOptSell = '�������� ��������� ������� �������';
  sLog_ViewOptSell = '�������� ��������� ������� �������';

  sLog_AddClient = '���������� ����������';
  sLog_EditClient = '�������������� ������ � ����������';

  sLog_Ret = '������� �������';
  sLog_AddRet = '�������� ������� �������';
  sLog_SelectSell = '����� �������� �� �������';
  sLog_AddSell = '�������� ������� � �������';
  sLog_AddSellScan = '�������� ������� � ������� ����� ������';  

  sLog_CreateRest = '������� ��������� ��������';
  sLog_ViewRest = '�������� ��������� ��������';
  sLog_DelRest = '������� ��������� ��������';
  sLog_CloseRest = '������� ��������� ��������';
  sLog_CloseAllRest = '������� ��� ��������� ��������';
  sLog_PrintTagRest = '������ ����� (�������)';
  sLog_PrintNewTagRest = '������ ����� ����� (�������)';
  sLog_PrintBlackTagRest = '������ ������ ����� (�������)';
  sLog_PrintBarCodeTagRest = '������ ����� �� �����-����� (�������)';
  sLog_PrintPrordRest = '������ ������� (�������)';
  sLog_DelArtRest = '������� ������� �� ��������';
  sLog_AddArtRest = '�������� ������� �� ��������';
  sLog_Ins = '�������';
  sLog_FindRest = '����� � ��������';
  sLog_MergeRest = '����������� � ��������';
  sLog_CompareRest = '����������� (�������)';
  aLog_ViewRinv = '�������� �������� �� ��������';

  sLog_InventoryExport = '������� ��������������';
  sLog_InventoryMarkFind = '��������������. �������� ������� ��� ���������';
  sLog_InventoryMarkNotFind = '��������������. �������� ������� ��� �� ���������';
  sLog_InventoryMarkExcess = '��������������. �������� ������� ��� ������';
  sLog_InventoryDelUID = '��������������. ������� �������';
  sLog_InventorySetting = '��������� ��������������';
  sLog_InventoryMOL = '�������� ��� ��������������';
  sLog_InventoryPrintBefore = '������������������ �����-��� ������� � ����������';
  sLog_InventoryPrintAfter = '����������� ���������� �������� ���������';
  sLog_InventoryPrintSubsB = '�������� �� ������ ��������������';
  sLog_InventoryPrintSubsE = '�������� �� ����� ��������������';
  sLog_InventoryPrintOrder = '������';
  sLog_InventoryPrintAct = '���';

  sLog_OptPrintInvoice = '������ �������� ��������� ������� �������';
  sLog_OptPrintFacture = '������ ����-������� ������� �������';
  sLog_OptPrintInvoiceUID = '������ �������� �������� (��-��������) ������� �������';
  sLog_OptPrintFactureUID = '������ ����-������� (��-��������) ������� �������';
  sLog_OptPrintInvoicePrPrice = '������ �������� ��������� ������� ������� (� ����.�����)';

  sLog_SRetLstDel = '������� ��������� �������� ����������';
  sLog_SretLstView = '�������� ���������';
  sLog_SretLstAddJew = '�������� ��������� �������� �� ����������';
  sLog_SretLstAddCDM = '�������� ��������� �������� ���';
  sLog_acCreateSInvBuSRet = '������� �������� �� ��������';
  sLog_ExportToTextCdm = '������� � ��������� ���� �������� ���';
  sLog_ChangeSDate = '�������� ����  ���. ����. � ���� ���. ���������';
  sLog_PrintFactureSRetLst = '������ ����-������� (������� �����������)';
  sLog_PrintInvoiceUIDSRetLst = '������ ��������� ��-�������� (������� �����������)';
  sLog_PrintInvoiceArtSRetLst2 = '������ ��������� ����12(� ����.�����)';
  sLog_PrintInvoiceArtSRetLst = '������ ��������� ��-���������� (������� �����������)';
  sLog_PrintInvoiceUID2SRetLst = '������ ��������� ��-�������� (+0.01%)(������� �����������)';
  sLog_PrintInvoice12supSRetLst = '������ ��������� ����12 � ���. � �������� (������� �����������)';
  sLog_PrintActRetSRetLst = '������ ���� �������� (������� �����������)';
  sLog_AddSRet = '���������� ������� � ��������� �������� ����������';
  sLog_DelSRet = '�������� ������� �� ��������� �������� ����������';
  sLog_CloseSRet = '�������� ��������� �������� ����������';
  sLog_OpenSRet = '�������� ��������� �������� ����������';

  sLog_AddORetLst = '���������� ��������� �������� ��������';
  sLog_DelORetLst = '�������� ��������� �������� ��������';
  sLog_ViewORetLst = '�������� ��������� �������� ��������';
  sLog_PrintORetLst = '������ ��������� �������� ��������';
  sLog_AddORet = '���������� ������� � ��������� �������� ��������';
  sLog_DelORet = '�������� ������� � ��������� �������� ��������';
  sLog_OpenORet = '�������� ��������� �������� ��������';
  sLog_CloseORet = '�������� ��������� �������� ��������';

  sLog_ApplAdd = '�������� ������';
  sLog_ApplDel = '������� ������';
  sLog_ApplView = '�������� ������';
  sLog_ApplPrint = '������ ������';
  sLog_ApplExportTxt = '������� ������ � ��������� ����';
  sLog_ApplAddRecord = '�������� ������� � ������';
  sLog_ApplDelRecord = '������� ������� �� ������';

  sLog_UIDStoreAdd = '�������� ��������� ���������';
  sLog_UIDStoreDel = '������� ��������� ���������';
  sLog_UIDStoreRefresh = '�������� ��������� ���������';
  sLog_UIDStoreTotal = '����� �� ��������� ��������� ��� ����� ��������';
  sLog_UIDStoreTotalC = '����� �� ��������� ��������� � ������ ��������';
  sLog_UIDStoreViewByArt = '�������� ��-����������';
  sLog_UIDStoreViewByUID = '�������� ��-��������';
  sLog_UIDStoreFillEntryData = '��������� �������� �� ������';
  sLog_UIDStoreFillOutData = '��������� ����������� ������� �� ������';
  sLog_UIDStorePrint = '������ ���� �� ��������� ���������';
  sLog_UIDStoreArtDetail = '����������� �������� �� ��������';
  sLog_UIDStoreRepeat = '������� �� ������';
  sLog_UIDStoreCheckData = '�������� ������';

  sLog_CalcJSz = '������ ������� �� ��������';
  sLog_AddArtJSz = '���������� ��������';
  sLog_DelArtJSz = '�������� ��������';
  sLog_CreateApplJSz = '������������ ������';
  sLog_ExportListJSz = '������� ��������� � ��������� ����';
  sLog_ExportApplJSz = '������� ������ � ��������� ����';
  sLog_PrintListJSz = '������ ���������';
  sLog_PrintApplJSz = '������ ������';
  sLog_ApplSumJSz = '��������������� ����� ������';
  sLog_DoubllingArtJSz = '������������ ��������';

  sLog_CalcJDep = '������ ������� �� �������������';

  sLog_Depart = '��������� �����������';
  sLog_DepartAdd = '�������� �������������';
  sLog_DepartDel = '������� �������������';
  sLog_DepartSort = '���������� �������������';
  sLog_EmpAdd = '�������� ������������';
  sLog_EmpDel = '������� ������������';
  sLog_EmpKey = '����� ������� ������������';

  sLog_ShopReportAdd = '���������� ��������� ������';
  sLog_ShopReportDel = '�������� ��������� ������';

  sLog_ActAllowancesClose = '�������� ���� ��������';
  sLog_ActAllowancesOpen = '�������� ���� ��������';

  sLog_ApplDepExport = '������� ������ �� �������';

  sLog_ActAllowances = '������ ����� ��������';
  sLog_Equipment = '������ ��������� ������������';

  rsInvClose = '��������� �������';

  sLog_UidStoreDepAdd = '���������� �������� �� ���������';
  sLog_UIDStoreDepPrint = '������ ���� �� ��������� ��������� �� ���������';
  sLog_UIDStoreDepRefresh = '�������� ��������� ��������� �� ���������';
  sLog_UIDStoreDepDel = '������� ��������� ��������� �� ���������';

  sLog_RespStoring = '������ ������� �� ������������ ��������';
  sLog_RepairList = '������ ��������� �� ������';
  sLog_SuspItemList = '������ ���������� �������';

  sLog_UserUidWh = '������ ������������ �������';
  sLog_NotSaleItems = '������ �������, �� ��������� �� ��������� ������';
implementation

end.
