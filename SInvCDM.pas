unit SinvCDM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBCtrls, Menus, Placemnt, ExtCtrls, StdCtrls,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, SpeedBar ,db, M207DBCtrls;
type
  TfmSInvCDM = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siEdit: TSpeedItem;
    siCloseInv: TSpeedItem;
    spitPrint: TSpeedItem;
    dg1: TM207IBGrid;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siPeriod: TSpeedItem;
    Panel1: TPanel;
    Panel3: TPanel;
    Panel6: TPanel;
    M207DBPanel1: TM207DBPanel;
    fs1: TFormStorage;
    pmPrint: TPopupMenu;
    mnitPrList: TMenuItem;
    spError: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siDelClick(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure siEditClick(Sender: TObject);
    procedure mnitPrListClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure siCloseInvClick(Sender: TObject);
    procedure spErrorClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  end;

var
  fmSInvCDM: TfmSInvCDM;

implementation

uses Data, ServData, comdata, M207Proc, Period,DBTree, Appl, ReportData,
  SItemCDM;

{$R *.dfm}

procedure TfmSInvCDM.WMSysCommand(var Message: TWMSysCommand);
begin
 {if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else} inherited;
end;


procedure TfmSInvCDM.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  with dmCom, dm do
    begin
      tr.Active:=True;
      SetBeginDate;
      ShowPeriod;
      WorkMode:='CDM';
      OpenDatasets([dmServ.taSInvCDM]);
    end;
end;

procedure TfmSInvCDM.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dmServ.taSInvCDM]);
      ShowPeriod;
    end;
end;

procedure TfmSInvCDM.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmSInvCDM.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmSInvCDM.siExitClick(Sender: TObject);
begin
 Close;
end;

procedure TfmSInvCDM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dmServ do
    begin
      CloseDataSets([taSInvCDM]);
      tr.Commit;
      dm.WorkMode:='';
    end;
end;

procedure TfmSInvCDM.siDelClick(Sender: TObject);
begin
  dmServ.taSInvCDM.Delete;
end;

procedure TfmSInvCDM.siAddClick(Sender: TObject);
begin
 with dmServ.taSInvCDM do
    try
       DisableControls;
//     if not Locate('ISCLOSED;CRUSERID', VarArrayOf([0, dmCom.UserId]), []) then
       Append;
       Post;
       dmCom.tr.CommitRetaining;
    finally
       EnableControls;
    end;
  siEditClick(Sender);
end;

procedure TfmSInvCDM.siEditClick(Sender: TObject);
begin
 with dmServ do
  begin
  PostDataSets([taSInvCDM]);
//  taSInvCDM.Refresh;
  ShowAndFreeForm(TfmSItemCDM, Self, TForm(fmSItemCDM), True, False);
  Reopendatasets([taSInvCDM]);
  end;
end;

procedure TfmSInvCDM.mnitPrListClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  dmReport.PrintDocumentA(gen_arr(dg1, dmServ.taSInvCDMSINVID), invoice_u_cdm);
end;

procedure TfmSInvCDM.N1Click(Sender: TObject);
begin
// dmReport.AppleText(dmServ.taSInvCDMAPPLID.asInteger,dmServ.taSInvCDMSNAME.asString);
end;

procedure TfmSInvCDM.siCloseInvClick(Sender: TObject);
begin
  with dm, dmCom, quTmp do
    begin
      Close;
      SQL.Text:='execute procedure  Close_INV_CDM '+dmServ.taSInvCDMSINVID.AsString;
      ExecQuery;
      tr.CommitRetaining;
      dmServ.taSInvCDM.Refresh;
    end;
end;

procedure TfmSInvCDM.spErrorClick(Sender: TObject);
var s:string;
begin
  with  dmCom, quTmp do
    begin
      Close;
//      SQL.Text:='select uid from ERR_CDM_INV('+dmServ.taSInvCDMREFSINVID.AsString+')';
//      ExecQuery;
      s:='';
      while not eof do
        begin
          s:=s+Fields[0].AsString+'; ';
          next;
        end;
      tr.CommitRetaining;
      ShowMessage(s);
    end;
end;

procedure TfmSInvCDM.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
var c: TColor;
begin
  if NOT Highlight or dg1.ClearHighlight then
    begin
      if dmServ.taSInvCDMISCLOSED.AsInteger=0 then Background:=dm.CU_IM0 //clInfoBk
      else
       if dmServ.taSInvCDMITYPE.AsInteger=10 then Background:=dm.CU_IM1
       else Background:=clBtnFace;
      if (Field<>NIL) then
        begin
          if Field.FieldName='SN' then Background:=clAqua
          else if Field.FieldName='DEP' then Background:=dmServ.taSInvCDMColor.AsInteger;
        end;
    end;
end;

end.
