unit SInvImport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls;

type
  TfmSInvImport = class(TForm)
    lsvPlugins: TListView;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    Label3: TLabel;
    mmDescr: TMemo;
    lbClassName: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure lsvPluginsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
  end;

var
  fmSInvImport: TfmSInvImport;

type
  TLibStrFunction = function : PChar;stdcall;

implementation

uses Data, comdata, ComObj, MsgDialog;

{$R *.dfm}

type
  TPluginInfo = record
    Name : string;
    ClassName : string;
    Version : string;
    Description : string;
  end;


procedure TfmSInvImport.FormCreate(Sender: TObject);
var
  Lib : Variant;
  PluginGroup : string;
  Func : TLibStrFunction;
  PluginInfo : ^TPluginInfo;

  procedure SearchFile(Dir : string);
  var
    sr : TSearchRec;
    r : integer; //��������� ������
  begin
    r := FindFirst(Dir+'*.*',faAnyFile, sr);
    while r=0 do
    begin
      if ((sr.Attr=faDirectory)or (sr.Attr=8208)) and ((sr.Name='.')or(sr.Name='..')) then
      begin
        r := FindNext(sr);
        continue;
      end;

      if ((sr.Attr=faDirectory) or (sr.Attr=8208)) then //���� ����� ����������, �� ���� ����� � ���
      begin
        SearchFile(Dir+sr.Name+'\'); //���������� �������� ���� ���������
        r := FindNext(sr); //���� ����. ����
        continue;
      end;

      if ExtractFileExt(sr.Name)='.dll' then
      begin
        // ���������
        Lib:=null;
        try
          Lib:=LoadLibrary(PChar(Dir+sr.Name));
          @Func := GetProcAddress(Lib,'GetPluginGroup');
          if @Func <> nil then
          begin
            PluginGroup := Func;
            if(PluginGroup = 'JewImport')then
            begin
              PluginInfo := AllocMem(SizeOf(TPluginInfo));
              @Func := GetProcAddress(Lib,'GetVersion');
              if @Func <> nil then PluginInfo^.Version := Func else PluginInfo^.Version := '����������';
              @Func := GetProcAddress(Lib,'GetDescription');
              if @Func <> nil then PluginInfo^.Description := Func else PluginInfo^.Description := '����������';
              @Func := GetProcAddress(Lib,'GetClassName');
              if @Func <> nil then PluginInfo^.ClassName := Func else PluginInfo^.ClassName := 'IUnknown';
              @Func := GetProcAddress(Lib,'GetName');
              if @Func <> nil then PluginInfo^.Name := Func else PluginInfo^.Name := sr.Name;               

              with lsvPlugins.Items.Add do
              begin
                Caption := PluginInfo^.Name;
                SubItems.Add(PluginInfo^.Version);
                Data := PluginInfo;
              end;
            end;
          end;
        except
        end;
        if (Lib<>null) then FreeLibrary(Lib);
      end;
      r := FindNext(sr);
    end;
    FindClose(sr);
  end;

var
  StartDir : string;
begin
  lsvPlugins.Items.Clear;
  // ���� � ����������
  StartDir := ExtractFileDir(Application.ExeName)+'\Plugins\';
  lsvPlugins.OnChange := nil;
  if DirectoryExists(StartDir) then SearchFile(StartDir);
  if (lsvPlugins.Items.Count = 0) then
  begin
    MessageDialog('������ ������� �� ����������.', mtWarning, [mbOk], 0);
    //SysUtils.Abort;
    PostMessage(Handle, WM_CLOSE, 0, 0);
  end
  else begin
    lsvPlugins.OnChange := lsvPluginsChange;
    lsvPlugins.Items[0].Selected := True;
    lsvPlugins.Refresh;
  end;
end;

procedure TfmSInvImport.Button2Click(Sender: TObject);
begin
  Close;
end;

procedure TfmSInvImport.Button1Click(Sender: TObject);
var
  V : OleVariant;
begin
//ShowMessage(TPluginInfo(lsvPlugins.Selected.Data^).ClassName + ' - I');


  if not Assigned(lsvPlugins.Selected) then eXit;
  try

    V := CreateOleObject(TPluginInfo(lsvPlugins.Selected.Data^).ClassName);
    V.SetAppHandle(Application.Handle);
    V.Import(dmCom.db.DBName, dm.SDepId);
    V:=null;
  except
    on E:Exception do MessageDialog('������ '+E.Message, mtError, [mbOk], 0);
  end;
  Close;
end;

procedure TfmSInvImport.lsvPluginsChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  with TPluginInfo(Item.Data^) do
  begin
    mmDescr.Text := Description;
    lbClassName.Caption := ClassName;
  end
end;

end.
