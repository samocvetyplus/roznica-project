unit Good;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, DBGridEh, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmGood = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    siSort: TSpeedItem;
    gridGood: TDBGridEh;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure siSortClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmGood: TfmGood;

implementation

uses comdata, DBTree, Sort, Data, M207Proc;

{$R *.DFM}

procedure TfmGood.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmGood.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom do
    begin
      gridGood.ReadOnly:=not Centerdep;
      SpeedItem2.Enabled:=Centerdep;
      SpeedItem1.Enabled:=Centerdep;
      taGood.Active:=True;
    end;
end;

procedure TfmGood.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSet(taGood);
      taGood.Active:=False;
      dm.LoadArtSL(GOOD_DICT);
      tr.CommitRetaining;
    end;
end;

procedure TfmGood.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmGood.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmGood.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taGood.Append;
end;

procedure TfmGood.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taGood.Delete;
end;

procedure TfmGood.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsGood, 34); //'gd'
end;

procedure TfmGood.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_GOODID, NAME '+
                       'FROM D_GOOD '+
                       'where d_goodid<>'#39+'-1000'+#39' '+
                       ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_GOOD set SortInd=?SortInd where D_GOODId=?D_GOODId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taGood]);
            end
        end;
    finally
      fmSort.Free;
    end;


end;

procedure TfmGood.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100104)
end;

procedure TfmGood.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
