unit RetDict;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ActnList, jpeg, rxSpeedbar;

type
  TfmRetDict = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    spitPrint: TSpeedItem;
    aclist: TActionList;
    acD_RETID: TAction;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure acD_RETIDExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmRetDict: TfmRetDict;

implementation

uses comdata, DBTree, Sort, Data, M207Proc;

{$R *.DFM}

procedure TfmRetDict.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRetDict.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
//      dg1.ReadOnly:=not Centerdep;
//      SpeedItem1.Visible := Centerdep;
      taRet.Active:=True;
    end;
//  dg1.ColumnByName['D_RETID'].Visible:=false;
end;

procedure TfmRetDict.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSet(taRet);
      tr.CommitRetaining;
    end;
end;

procedure TfmRetDict.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRetDict.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRetDict.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taRet.Append;
end;

procedure TfmRetDict.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taRet.Delete;
end;

procedure TfmRetDict.spitPrintClick(Sender: TObject);
begin
 PrintDict(dmCom.dsRet, 63); //'gd'
end;

procedure TfmRetDict.SpeedItem3Click(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_RETID, RET '+
                       'FROM D_RET '+
                       'where D_RETID<>'#39+'-1000'+#39' '+
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_GOOD set SortInd=?SortInd where D_GOODId=?D_GOODId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taGood]);
            end
        end;
    finally
      fmSort.Free;
    end;


end;

procedure TfmRetDict.acD_RETIDExecute(Sender: TObject);
begin
//  dg1.ColumnByName['D_RETID'].Visible:=not dg1.ColumnByName['D_RETID'].Visible;
end;

procedure TfmRetDict.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100123)
end;

procedure TfmRetDict.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
