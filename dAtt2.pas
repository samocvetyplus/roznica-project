unit dAtt2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, ComCtrls, Grids, DBGridEh, ExtCtrls, jpeg,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmdAtt2 = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    spitSort: TSpeedItem;
    gridAtt2: TDBGridEh;
    StatusBar1: TStatusBar;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acSort: TAction;
    acClose: TAction;
    siprint: TSpeedItem;
    siHelp: TSpeedItem;
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acSortExecute(Sender: TObject);
    procedure acSortUpdate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure siprintClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  end;

var
  fmdAtt2: TfmdAtt2;

implementation

uses comdata, Sort, dbTree, m207Proc, Data;

{$R *.dfm}

procedure TfmdAtt2.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmdAtt2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmCom.taAtt2]);
  Action := caFree;
end;

procedure TfmdAtt2.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  gridAtt2.ReadOnly := not CenterDep;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining
  else dmCom.tr.StartTransaction;
  OpenDataSets([dmCom.taAtt2]);
end;

procedure TfmdAtt2.acAddExecute(Sender: TObject);
begin
  dmCom.taAtt2.Insert;
end;

procedure TfmdAtt2.acAddUpdate(Sender: TObject);
begin
{  acAdd.Visible := CenterDep;
  acAdd.Enabled := dmCom.taAtt2.Active;}
end;

procedure TfmdAtt2.acDelExecute(Sender: TObject);
begin
  dmCom.taAtt2.Delete;
end;

procedure TfmdAtt2.acDelUpdate(Sender: TObject);
begin
{  acDel.Visible := CenterDep;
  acDel.Enabled := dmCom.taAtt2.Active and (not dmCom.taAtt2.IsEmpty);}
end;

procedure TfmdAtt2.acSortExecute(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  fmSort:=TfmSort.Create(Application);
  with fmSort do
  try
    lb1.Items.Clear;
    dmCom.quTmp.SQL.Text:='SELECT ID, NAME FROM D_ATT2 WHERE ID<>-1 ORDER BY SORTIND';
    dmCom.quTmp.ExecQuery;
    while not dmCom.quTmp.Eof do
    begin
      nd:=TNodeData.Create;
      nd.Code:=dmCom.quTmp.Fields[0].AsString;
      lb1.Items.AddObject(dmCom.quTmp.Fields[1].AsString, nd);
      dmCom.quTmp.Next;
    end;
    dmCom.quTmp.Close;
    if ShowModal=mrOK then
    begin
      dmCom.quTmp.SQL.Text:='UPDATE D_ATT2 SET SORTIND=?SORTIND where ID=?ID';
      for i:=0 to lb1.Items.Count-1 do
        with dmCom.quTmp do
        begin
          dmCom.quTmp.Params[0].AsInteger:=i;
          dmCom.quTmp.Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
          dmCom.quTmp.ExecQuery;
        end;
      dmCom.tr.CommitRetaining;
    end;
  finally
    fmSort.Free;
  end;
end;

procedure TfmdAtt2.acSortUpdate(Sender: TObject);
begin
{  acSort.Visible := CenterDep;
  acSort.Enabled := dmCom.taAtt2.Active and (not dmCom.taAtt2.IsEmpty);}
end;

procedure TfmdAtt2.acCloseExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfmdAtt2.siprintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsrAtt2, 67); //'gd'
end;

procedure TfmdAtt2.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100108)
end;

procedure TfmdAtt2.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
