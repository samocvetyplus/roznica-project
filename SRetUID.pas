unit SRetUid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, StdCtrls, Buttons, db,
  rxPlacemnt;

type
  TfmSRetUID = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dg1: TM207IBGrid;
    FormStorage1: TFormStorage;
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
     FContractID: Integer;
     FContractClassID: Integer;
  public
    property ContractID: Integer read FContractID write FContractID;  
    property ContractClassID: Integer read FContractClassID write FContractClassID;
  end;

var
  fmSRetUID: TfmSRetUID;

implementation

uses comdata, Data;

{$R *.DFM}

procedure TfmSRetUID.dg1GetCellParams(Sender: TObject; Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
var
  IsSafe: Boolean;
begin
  if (Field<>NIL) then
  begin
   if (Field.FieldName='UID') then
   begin
     IsSafe := dm.quSRetUIDISSAFE.AsInteger = 1;

     if IsSafe then
     begin
       Background := dmCom.clMoneyGreen;
     end else
     begin
       Background := clRed;
     end;
   end;
  end;
end;

end.
