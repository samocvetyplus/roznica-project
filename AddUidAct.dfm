object fmAddUidAct: TfmAddUidAct
  Left = 290
  Top = 141
  Width = 352
  Height = 305
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1082#1090' '#1076#1083#1103' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plUid: TPanel
    Left = 0
    Top = 0
    Width = 344
    Height = 65
    Align = alTop
    TabOrder = 0
    object LUid: TLabel
      Left = 8
      Top = 16
      Width = 62
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LFullArt: TLabel
      Left = 8
      Top = 40
      Width = 44
      Height = 13
      Caption = 'LFullArt'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edUid: TEdit
      Left = 79
      Top = 10
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyPress = edUidKeyPress
    end
  end
  object plInfo: TPanel
    Left = 0
    Top = 65
    Width = 344
    Height = 170
    Align = alClient
    TabOrder = 1
    object LNewPrice: TLabel
      Left = 9
      Top = 16
      Width = 59
      Height = 13
      Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
    end
    object LOldPrice: TLabel
      Left = 9
      Top = 46
      Width = 63
      Height = 13
      Caption = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
    end
    object LCrData: TLabel
      Left = 9
      Top = 72
      Width = 52
      Height = 13
      Caption = #1044#1072#1090#1072' '#1072#1082#1090#1072
    end
    object LUnitid: TLabel
      Left = 9
      Top = 112
      Width = 104
      Height = 13
      Caption = #1045#1076#1077#1085#1080#1094#1099' '#1080#1079#1084#1077#1088#1077#1085#1080#1103
    end
    object LSn: TLabel
      Left = 9
      Top = 144
      Width = 109
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1042#1055
    end
    object edNewPrice: TEdit
      Left = 80
      Top = 8
      Width = 240
      Height = 21
      TabOrder = 0
    end
    object edOldPrice: TEdit
      Left = 80
      Top = 40
      Width = 241
      Height = 21
      TabOrder = 1
    end
    object edDateAct: TEdit
      Left = 80
      Top = 72
      Width = 161
      Height = 21
      Enabled = False
      TabOrder = 2
    end
    object btDate: TBitBtn
      Left = 232
      Top = 72
      Width = 25
      Height = 21
      TabOrder = 3
      OnClick = btDateClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
        262FA31D20B5FF00FF4B4B4B5B5B5B87817B87817BFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF82776F5F61BC2126B2666059A3A3A4FAFAFAE3
        E0DCD1CEC99A979582807E7874715D5A58FF00FFFF00FFFF00FFFF00FFA89381
        7B75C42024B2BA9775A3A3A4FFFFFFFFFFFFFFFFFDD6D0CAC8C0B8F6EBDDEDE0
        D1A59B916B6864FF00FFFF00FFA893817873C32024B2B99B7EA3A3A4FEFFFFFF
        FFFF9F9FA07371713535377E7B76FFF5E7FFF2E1878685FF00FFFF00FFA89483
        7873C52024B3BAA18AA3A3A4FEFFFFFFFFFFCACACCF5F3F2FFFDF9353537F3EA
        E0FDF0E2878685FF00FFFF00FFA8978B7876CA2024B2BBA896A3A3A4FEFFFFFF
        FFFFB3B3B4828283A2A1A2353537F8F2EBFDF2E9878685FF00FFFF00FF918984
        7779CF2024B2BCADA0A3A3A4FEFFFFFFFFFF9A9A9B3535377C7C7ECCCCCBFFFE
        FBFCF6EF878685FF00FFFF00FF8E8888787BD42024B1BDB2A9A3A3A4FEFEFFFF
        FFFFBCBCBD353537353537353537FFFEFDFCF9F5878685FF00FFFF00FF8E8C8E
        787EDA2024B1B4B3B9A3A3A4FEFEFEE9E9E9E2E2E2BCBCBCA2A2A3DFDFE0FFFF
        FFFCFBFB878685FF00FFFF00FF8E8F947880DE2023B1B4B3B9A3A3A4FFFFFFA5
        A5A5BBBBBB9F9FA0C6C6C7C5C5C6E2E2E3FEFEFE878685FF00FFFF00FF8E8F95
        7880DF2023B1B4B3B9A3A3A4FFFFFFB9B9B9B3B3B4ACACADA8A8A9B9B9BAE0E0
        E1FFFFFF878685FF00FFFF00FF9091977B83E22023B0B6B5B6A3A3A4FFFFFF79
        7979E1E2E2FBFBFCEDEDEDB9B9B9DBDBDCFFFFFF878685FF00FFFF00FF83848A
        646AD21D23BA6F71919E9DAFBBB9BC7F7D738D8C88EDEAE0E7E7E36465629090
        90FFFFFF878685FF00FFFF00FFFF00FF4141632E317D3034833F43933A3D9440
        42964444605655996D6893736E885E5B61918D8A616161FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF4B4B4BFF00FF3B3D624B4B4B373765FF00FF3333833838
        65141477FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4B4B4B4B
        4B4BFF00FFFF00FF4B4B4E4C4C4CFF00FFFF00FFFF00FFFF00FF}
    end
    object cbunitid: TComboBox
      Left = 128
      Top = 104
      Width = 201
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 4
      Items.Strings = (
        '--'#1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102'--'
        #1064#1090'.'
        #1043#1088'.')
    end
    object edSn: TEdit
      Left = 128
      Top = 136
      Width = 121
      Height = 21
      TabOrder = 5
      OnKeyPress = edSnKeyPress
    end
  end
  object plButton: TPanel
    Left = 0
    Top = 235
    Width = 344
    Height = 41
    Align = alBottom
    TabOrder = 2
    object btOk: TBitBtn
      Left = 32
      Top = 8
      Width = 105
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      Kind = bkOK
    end
    object btCancel: TBitBtn
      Left = 200
      Top = 8
      Width = 105
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object acList: TActionList
    Left = 160
    Top = 160
    object acEnter: TAction
      Caption = 'acEnter'
      ShortCut = 13
      OnExecute = acEnterExecute
    end
  end
end
