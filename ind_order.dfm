object DialogOrder: TDialogOrder
  Left = 0
  Top = 0
  Caption = #1048#1085#1076#1080#1074#1080#1076#1091#1072#1083#1100#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
  ClientHeight = 602
  ClientWidth = 922
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TcxPageControl
    Left = 0
    Top = 0
    Width = 922
    Height = 30
    ActivePage = Page_out
    Align = alTop
    TabOrder = 0
    OnClick = PageControlClick
    ClientRectBottom = 30
    ClientRectRight = 922
    ClientRectTop = 24
    object Page_out: TcxTabSheet
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      ImageIndex = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object Page_in: TcxTabSheet
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      ImageIndex = 1
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object Page_Factory: TcxTabSheet
      Caption = #1047#1072#1082#1072#1079#1099' '#1085#1072' '#1079#1072#1074#1086#1076
      ImageIndex = 2
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object Page_off: TcxTabSheet
      Caption = #1047#1072#1074#1077#1088#1096#1077#1085#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
      ImageIndex = 3
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object Page_Center: TcxTabSheet
      Caption = #1047#1072#1082#1072#1079#1099' "'#1060#1080#1083#1080#1072#1083'-'#1060#1080#1083#1080#1072#1083'"'
      ImageIndex = 4
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 171
    Width = 922
    Height = 431
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 0
    Align = alClient
    PopupMenu = MenuGrid
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    ExplicitTop = 185
    ExplicitHeight = 417
    object ViewOrders: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewOrdersCellClick
      OnCustomDrawCell = ViewOrdersCustomDrawCell
      OnFocusedRecordChanged = ViewOrdersFocusedRecordChanged
      DataController.DataSource = dmIndividualOrder.dsOrder
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'PRICE'
          Column = ViewOrdersPRICE
        end
        item
          Kind = skCount
          Position = spFooter
          FieldName = 'ID_ORDER'
          Column = ViewOrdersID_ORDER
        end
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.NoDataToDisplayInfoText = #1047#1072#1082#1072#1079#1099' '#1086#1090#1089#1091#1090#1089#1090#1074#1091#1102#1090
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.DataRowHeight = 36
      OptionsView.GroupFooters = gfAlwaysVisible
      Bands = <
        item
          Caption = #1047#1072#1082#1072#1079
        end
        item
          Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1072#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
          Visible = False
        end
        item
          Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1089#1086#1089#1090#1086#1103#1085#1080#1080' '#1079#1072#1082#1072#1079#1072
        end>
      object ViewOrdersID_ORDER: TcxGridDBBandedColumn
        Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'ID_ORDER'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewOrdersFIO: TcxGridDBBandedColumn
        Caption = #1060#1048#1054' '#1082#1083#1080#1077#1085#1090#1072
        DataBinding.FieldName = 'FIO'
        HeaderAlignmentHorz = taCenter
        Width = 251
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewOrdersPHONE: TcxGridDBBandedColumn
        Caption = #1058#1077#1083#1077#1092#1086#1085
        DataBinding.FieldName = 'PHONE'
        HeaderAlignmentHorz = taCenter
        Width = 67
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewOrdersDEP_SEND: TcxGridDBBandedColumn
        Caption = #1050#1086#1084#1091
        DataBinding.FieldName = 'DEP_SEND'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.CaseSensitiveSearch = True
        Properties.IncrementalFiltering = False
        Properties.KeyFieldNames = 'NAME'
        Properties.ListColumns = <
          item
            FieldName = 'NAME'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = Sourse_DEP
        HeaderAlignmentHorz = taCenter
        Width = 84
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewOrdersART: TcxGridDBBandedColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ART'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'ART'
        Properties.ListColumns = <
          item
            FieldName = 'ART'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = Sourse_ART
        Properties.OnCloseUp = ViewOrdersARTPropertiesCloseUp
        HeaderAlignmentHorz = taCenter
        Width = 92
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object ViewOrdersCOMP: TcxGridDBBandedColumn
        Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
        DataBinding.FieldName = 'COMP'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.CaseSensitiveSearch = True
        Properties.IncrementalFiltering = False
        Properties.KeyFieldNames = 'NAME'
        Properties.ListColumns = <
          item
            FieldName = 'NAME'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dmIndividualOrder.Sourse_COMP
        HeaderAlignmentHorz = taCenter
        Width = 179
        Position.BandIndex = 0
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object ViewOrdersGOOD: TcxGridDBBandedColumn
        Caption = #1042#1080#1076' '#1080#1079#1076#1077#1083#1080#1103
        DataBinding.FieldName = 'GOOD'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'GOOD'
        Properties.ListColumns = <
          item
            FieldName = 'GOOD'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dmIndividualOrder.Sourse_GOOD
        HeaderAlignmentHorz = taCenter
        Width = 112
        Position.BandIndex = 0
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object ViewOrdersMAT: TcxGridDBBandedColumn
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083
        DataBinding.FieldName = 'MAT'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'MAT'
        Properties.ListColumns = <
          item
            FieldName = 'MAT'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dmIndividualOrder.Sourse_MAT
        HeaderAlignmentHorz = taCenter
        Width = 87
        Position.BandIndex = 0
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object ViewOrdersSZ: TcxGridDBBandedColumn
        Caption = #1056#1072#1079#1084#1077#1088
        DataBinding.FieldName = 'SZ'
        HeaderAlignmentHorz = taCenter
        Width = 48
        Position.BandIndex = 0
        Position.ColIndex = 10
        Position.RowIndex = 0
      end
      object ViewOrdersEMP_CREATE: TcxGridDBBandedColumn
        Caption = #1055#1088#1080#1085#1103#1083' '#1079#1072#1082#1072#1079' '#1086#1090' '#1082#1083#1080#1077#1085#1090#1072' '
        DataBinding.FieldName = 'EMP_CREATE'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'FIO'
        Properties.ListColumns = <
          item
            FieldName = 'FIO'
          end>
        Properties.ListSource = Source_EMP
        HeaderAlignmentHorz = taCenter
        Width = 152
        Position.BandIndex = 0
        Position.ColIndex = 12
        Position.RowIndex = 0
      end
      object ViewOrdersDATE_CREATE: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072' '#1057#1086#1079#1076#1072#1085#1080#1103
        DataBinding.FieldName = 'DATE_CREATE'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 98
        Position.BandIndex = 0
        Position.ColIndex = 13
        Position.RowIndex = 0
      end
      object ViewOrdersDEP_CUSTOM: TcxGridDBBandedColumn
        Caption = #1047#1072#1082#1072#1079#1095#1080#1082
        DataBinding.FieldName = 'DEP_CUSTOM'
        HeaderAlignmentHorz = taCenter
        Width = 73
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ViewOrdersNAME: TcxGridDBBandedColumn
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 91
        Position.BandIndex = 2
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewOrdersID_STATE: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ID_STATE'
        Visible = False
        Position.BandIndex = 2
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewOrdersTYPE_ORDER: TcxGridDBBandedColumn
        Caption = #1058#1080#1087' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'TYPE_ORDER'
        Visible = False
        Width = 59
        Position.BandIndex = 0
        Position.ColIndex = 14
        Position.RowIndex = 0
      end
      object ViewOrdersEMP_STATE: TcxGridDBBandedColumn
        Caption = #1048#1079#1084#1077#1085#1080#1083' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'EMP_STATE'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 115
        Position.BandIndex = 2
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewOrdersDATE_STATE: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1072' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
        DataBinding.FieldName = 'DATE_STATE'
        GroupSummaryAlignment = taCenter
        Options.Editing = False
        Width = 95
        Position.BandIndex = 2
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object ViewOrdersNUMBER_DOC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'NUMBER_DOC'
        Visible = False
        Position.BandIndex = 2
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object ViewOrdersDATE_DOC: TcxGridDBBandedColumn
        DataBinding.FieldName = 'DATE_DOC'
        Visible = False
        Position.BandIndex = 2
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object ViewOrdersCOMMENT: TcxGridDBBandedColumn
        Caption = #1050#1086#1084#1077#1085#1090#1072#1088#1080#1080' '#1082' '#1079#1072#1082#1072#1079#1091
        DataBinding.FieldName = 'COMMENT'
        HeaderAlignmentHorz = taCenter
        Width = 314
        Position.BandIndex = 2
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewOrdersPRICE: TcxGridDBBandedColumn
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'PRICE'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 106
        Position.BandIndex = 2
        Position.ColIndex = 8
        Position.RowIndex = 0
      end
      object ViewOrdersDocumentName: TcxGridDBBandedColumn
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090
        DataBinding.FieldName = 'DocumentName'
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Width = 81
        Position.BandIndex = 2
        Position.ColIndex = 7
        Position.RowIndex = 0
      end
      object ViewOrdersCOST: TcxGridDBBandedColumn
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
        DataBinding.FieldName = 'COST'
        HeaderAlignmentHorz = taCenter
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewOrdersINFO_ORDER: TcxGridDBBandedColumn
        Caption = #1054#1087#1090#1086#1074#1072#1103' '#1094#1077#1085#1072' ('#1074#1080#1076#1085#1072' '#1090#1086#1083#1100#1082#1086' '#1085#1072' '#1094#1077#1085#1090#1088#1077')'
        DataBinding.FieldName = 'INFO_ORDER'
        HeaderAlignmentHorz = taCenter
        Width = 250
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewOrdersTIME_ORDER: TcxGridDBBandedColumn
        Caption = #1057#1088#1086#1082' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '
        DataBinding.FieldName = 'TIME_ORDER'
        HeaderAlignmentHorz = taCenter
        Width = 150
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewOrdersPREPAY: TcxGridDBBandedColumn
        Caption = #1055#1088#1077#1076#1086#1087#1083#1072#1090#1072
        DataBinding.FieldName = 'PREPAY'
        HeaderAlignmentHorz = taCenter
        Width = 80
        Position.BandIndex = 1
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewOrdersART_FACTORY: TcxGridDBBandedColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083' ('#1076#1083#1103' '#1079#1072#1074#1086#1076#1072')'
        DataBinding.FieldName = 'ART_FACTORY'
        HeaderAlignmentHorz = taCenter
        Width = 164
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
      object ViewOrdersID_PREVIEW: TcxGridDBBandedColumn
        DataBinding.FieldName = 'ID_PREVIEW'
        Visible = False
        Position.BandIndex = 0
        Position.ColIndex = 15
        Position.RowIndex = 0
      end
      object ViewOrdersINS_FACTORY: TcxGridDBBandedColumn
        Caption = #1042#1089#1090#1072#1074#1082#1080
        DataBinding.FieldName = 'INS_FACTORY'
        HeaderAlignmentHorz = taCenter
        Width = 164
        Position.BandIndex = 0
        Position.ColIndex = 11
        Position.RowIndex = 0
      end
      object ViewOrdersOLD_STATE: TcxGridDBBandedColumn
        Caption = #1055#1088#1077#1076#1080#1076#1091#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'OLD_STATE'
        Position.BandIndex = 2
        Position.ColIndex = 9
        Position.RowIndex = 0
      end
      object ViewOrdersColumn1: TcxGridDBBandedColumn
        DataBinding.FieldName = 'COMMplus'
        Position.BandIndex = 0
        Position.ColIndex = 16
        Position.RowIndex = 0
      end
    end
    object ViewHistory: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = ViewHistoryCellClick
      OnFocusedRecordChanged = ViewHistoryFocusedRecordChanged
      DataController.DataSource = dmIndividualOrder.dsHistory
      DataController.DetailKeyFieldNames = 'ID_ORDER'
      DataController.MasterKeyFieldNames = 'ID_ORDER'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.NoDataToDisplayInfoText = #1048#1089#1090#1086#1088#1080#1103' '#1079#1072#1082#1072#1079#1072' '#1086#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
      OptionsView.CellTextMaxLineCount = 3
      OptionsView.DataRowHeight = 36
      OptionsView.GroupByBox = False
      object ViewHistoryID_ORDER: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1079#1072#1082#1072#1079#1072
        DataBinding.FieldName = 'ID_ORDER'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object ViewHistoryNAME: TcxGridDBColumn
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        HeaderAlignmentHorz = taCenter
        Width = 76
      end
      object ViewHistoryID_STATE: TcxGridDBColumn
        DataBinding.FieldName = 'ID_STATE'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object ViewHistoryEMP_STATE: TcxGridDBColumn
        Caption = #1048#1079#1084#1077#1085#1080#1083' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'EMP_STATE'
        HeaderAlignmentHorz = taCenter
        Width = 86
      end
      object ViewHistoryDATE_STATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
        DataBinding.FieldName = 'DATE_STATE'
        HeaderAlignmentHorz = taCenter
        Width = 120
      end
      object ViewHistoryNUMBER_DOC: TcxGridDBColumn
        DataBinding.FieldName = 'NUMBER_DOC'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object ViewHistoryDATE_DOC: TcxGridDBColumn
        DataBinding.FieldName = 'DATE_DOC'
        Visible = False
        HeaderAlignmentHorz = taCenter
      end
      object ViewHistoryCOMMENT: TcxGridDBColumn
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081' '#1082' '#1079#1072#1082#1072#1079#1091
        DataBinding.FieldName = 'COMMENT'
        HeaderAlignmentHorz = taCenter
        Width = 500
      end
      object ViewHistoryPRICE: TcxGridDBColumn
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1087#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'PRICE'
        HeaderAlignmentHorz = taCenter
        Width = 58
      end
      object ViewHistoryDocumentName: TcxGridDBColumn
        Caption = #1044#1086#1082#1091#1084#1077#1085#1090
        DataBinding.FieldName = 'DocumentName'
        HeaderAlignmentHorz = taCenter
        Width = 150
      end
      object ViewHistoryUID: TcxGridDBColumn
        Caption = #1048#1085#1076#1080#1074#1080#1076#1091#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' UID'
        DataBinding.FieldName = 'UID'
      end
    end
    object LevelOrders: TcxGridLevel
      GridView = ViewOrders
      object LevelHistory: TcxGridLevel
        GridView = ViewHistory
      end
    end
  end
  object EditorNote: TcxDBMemo
    Left = 0
    Top = 128
    Align = alTop
    DataBinding.DataField = 'COMMENT'
    DataBinding.DataSource = dmIndividualOrder.dsHistory
    TabOrder = 6
    Visible = False
    ExplicitTop = 142
    Height = 43
    Width = 922
  end
  object Actions: TActionList
    Images = Images32
    Left = 528
    object ActionIncludeFactory: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = ActionIncludeFactoryExecute
    end
    object ActionIncludeIN: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
    end
    object ActionExcludeFactory: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = ActionExcludeFactoryExecute
    end
    object ActionRefreshOUT: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionRefreshOUTExecute
    end
    object ActionRefreshOFF: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionRefreshOFFExecute
    end
    object ActionRefreshCenter: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionRefreshCenterExecute
    end
    object ActionRefreshFactory: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionRefreshFactoryExecute
    end
    object ActionRefreshIN: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 2
      OnExecute = ActionRefreshINExecute
    end
    object ActionExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 3
      OnExecute = ActionExitExecute
    end
    object ActionIncludeOUT: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = ActionIncludeOUTExecute
    end
    object ActionExcludOUT: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = ActionExcludOUTExecute
    end
    object ActionExcludIn: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
    end
    object ActionEditStateOUT: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      ImageIndex = 5
      OnExecute = ActionEditStateOUTExecute
    end
    object ActionEditStateIN: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      ImageIndex = 5
      OnExecute = ActionEditStateINExecute
    end
    object ActionEditStateFactory: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      ImageIndex = 5
      OnExecute = ActionEditStateFactoryExecute
    end
    object ActionPreview: TAction
      Caption = #1047#1072#1082#1072#1079' '#1087#1088#1086#1089#1084#1086#1090#1088#1077#1085
      Hint = #1045#1089#1083#1080' '#1079#1072#1082#1072#1079' '#1087#1088#1086#1089#1084#1086#1090#1088#1077#1085' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1077#1084' '
      ImageIndex = 9
      OnExecute = ActionPreviewExecute
    end
    object ActionCOMMENT: TAction
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      ImageIndex = 10
      OnExecute = ActionCOMMENTExecute
    end
    object ActionEditOffOUT: TAction
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100' '#1079#1072#1082#1072#1079
      ImageIndex = 6
      OnExecute = ActionEditOffOUTExecute
    end
    object TypeOrder: TAction
      Caption = #1058#1080#1087' '#1079#1072#1082#1072#1079#1072
      ImageIndex = 7
    end
    object ActionPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 12
      OnExecute = ActionPrintExecute
    end
    object Office: TAction
      Caption = #1060#1080#1083#1080#1072#1083
      ImageIndex = 8
      OnExecute = OfficeExecute
    end
    object ActionCancel: TAction
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100' '#1079#1072#1082#1072#1079
      ImageIndex = 7
      OnExecute = ActionCancelExecute
    end
    object ActionOtkaz: TAction
      Caption = #1054#1090#1082#1072#1079
      ImageIndex = 4
      OnExecute = ActionOtkazExecute
    end
    object ActionlaPeriod: TAction
      Caption = 'ActionlaPeriod'
      ImageIndex = 13
      OnExecute = ActionlaPeriodExecute
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default'
      'TypeOrder')
    Categories.ItemsVisibles = (
      2
      2)
    Categories.Visibles = (
      True
      True)
    ImageOptions.LargeImages = Images32
    ImageOptions.LargeIcons = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfOffice11
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 608
    DockControlHeights = (
      0
      0
      98
      0)
    object Bar: TdxBar
      AllowClose = False
      AllowQuickCustomizing = False
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 494
      FloatTop = 175
      FloatClientWidth = 119
      FloatClientHeight = 312
      ItemLinks = <
        item
          Visible = True
          ItemName = 'add'
        end
        item
          Visible = True
          ItemName = 'del'
        end
        item
          Visible = True
          ItemName = 'refresh'
        end
        item
          Visible = True
          ItemName = 'Preview'
        end
        item
          Visible = True
          ItemName = 'editState'
        end
        item
          Visible = True
          ItemName = 'editotkaz'
        end
        item
          Visible = True
          ItemName = 'Cancel'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          Visible = True
          ItemName = 'Print'
        end
        item
          Visible = True
          ItemName = 'ExitForm'
        end>
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarFilter: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 56
      DockingStyle = dsTop
      FloatLeft = 1037
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'BtnOrderOut'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'BtnOffice'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'laPeriod'
        end>
      OneOnRow = True
      Row = 1
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object add: TdxBarLargeButton
      Action = ActionIncludeOUT
      Category = 0
      AutoGrayScale = False
    end
    object del: TdxBarLargeButton
      Action = ActionExcludOUT
      Category = 0
      AutoGrayScale = False
    end
    object refresh: TdxBarLargeButton
      Action = ActionRefreshOUT
      Category = 0
      AutoGrayScale = False
    end
    object editState: TdxBarLargeButton
      Action = ActionEditStateOUT
      Category = 0
      AutoGrayScale = False
    end
    object EditOff: TdxBarLargeButton
      Action = ActionEditOffOUT
      Category = 0
      Hint = #1042#1099#1087#1086#1083#1085#1077#1085#1085#1099#1077' '#1080#1083#1080' '#1086#1090#1084#1077#1085#1077#1085#1085#1099#1077' '#1079#1072#1082#1072#1079#1099
      AutoGrayScale = False
    end
    object ExitForm: TdxBarLargeButton
      Action = ActionExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
    object BtnOrderOut: TdxBarLargeButton
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      Category = 0
      Hint = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      Visible = ivAlways
      DropDownMenu = TypeOrderPopupMenu
      LargeImageIndex = 7
      PaintStyle = psCaptionInMenu
      OnClick = BtnOrderOutClick
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object Type_Out: TdxBarLargeButton
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      Category = 0
      Hint = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      Visible = ivAlways
      LargeImageIndex = 7
      OnClick = Type_OutClick
      SyncImageIndex = False
      ImageIndex = 6
    end
    object Type_In: TdxBarLargeButton
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      Category = 0
      Hint = #1042#1093#1086#1076#1103#1097#1080#1077
      Visible = ivAlways
      LargeImageIndex = 7
      OnClick = Type_InClick
    end
    object Type_Factory: TdxBarLargeButton
      Caption = #1053#1072' '#1079#1072#1074#1086#1076
      Category = 0
      Hint = #1053#1072' '#1079#1072#1074#1086#1076
      Visible = ivAlways
      LargeImageIndex = 7
      OnClick = Type_FactoryClick
    end
    object BtnOffice: TdxBarLargeButton
      Action = Office
      Category = 0
      DropDownMenu = OfficePopupMenu
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
    object Center: TdxBarLargeButton
      Caption = #1062#1077#1085#1090#1088
      Category = 0
      Hint = #1062#1077#1085#1090#1088
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Kursk: TdxBarLargeButton
      Caption = #1050#1091#1088#1089#1082
      Category = 0
      Hint = #1050#1091#1088#1089#1082
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Olimp: TdxBarLargeButton
      Caption = #1054#1083#1080#1084#1087
      Category = 0
      Hint = #1054#1083#1080#1084#1087
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Belgorod: TdxBarLargeButton
      Caption = #1041#1077#1083#1075#1086#1088#1086#1076
      Category = 0
      Hint = #1041#1077#1083#1075#1086#1088#1086#1076
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Vester: TdxBarLargeButton
      Caption = #1042#1077#1089#1090#1077#1088
      Category = 0
      Hint = #1042#1077#1089#1090#1077#1088
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Oskol: TdxBarLargeButton
      Caption = #1054#1089#1082#1086#1083
      Category = 0
      Hint = #1054#1089#1082#1086#1083
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Moll: TdxBarLargeButton
      Caption = #1057#1080#1090#1080' '#1052#1086#1083#1083
      Category = 0
      Hint = #1057#1080#1090#1080' '#1052#1086#1083#1083
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Rio: TdxBarLargeButton
      Caption = #1056#1080#1086
      Category = 0
      Hint = #1056#1080#1086
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Orel: TdxBarLargeButton
      Caption = #1054#1088#1105#1083
      Category = 0
      Hint = #1054#1088#1105#1083
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Europe: TdxBarLargeButton
      Caption = #1045#1074#1088#1086#1087#1072
      Category = 0
      Hint = #1045#1074#1088#1086#1087#1072
      Visible = ivAlways
      LargeImageIndex = 8
      OnClick = OfficeClick
    end
    object Preview: TdxBarLargeButton
      Action = ActionPreview
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 10
      ShowCaption = False
      SyncImageIndex = False
      ImageIndex = 5
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 5
      AutoGrayScale = False
      HotImageIndex = 11
      ShowCaption = False
      SyncImageIndex = False
      ImageIndex = 11
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxMemoProperties'
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxMemoProperties'
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = ActionCOMMENT
      Category = 0
      AllowAllUp = True
      AutoGrayScale = False
    end
    object Print: TdxBarLargeButton
      Action = ActionPrint
      Category = 0
      AutoGrayScale = False
    end
    object Cancel: TdxBarLargeButton
      Action = ActionCancel
      Category = 0
      AutoGrayScale = False
    end
    object editotkaz: TdxBarLargeButton
      Action = ActionOtkaz
      Category = 0
      AutoGrayScale = False
    end
    object laPeriod: TdxBarLargeButton
      Action = ActionlaPeriod
      Align = iaClient
      Caption = #1055#1077#1088#1080#1086#1076
      Category = 0
      ButtonStyle = bsChecked
      Down = True
      AutoGrayScale = False
      GlyphLayout = glLeft
    end
  end
  object Images32: TcxImageList
    Height = 32
    Width = 32
    FormatVersion = 1
    DesignInfo = 568
    ImageInfo = <
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000101
          010101010102010101040101010200000000000000000101010B010101180101
          01270101012A0101012901010129010101290101012901010129010101290101
          012901010128010101260101011A0101010E0101010900000000000000000000
          0000000000000000000000000000000000000000000000000000000000000101
          01030101010401010106010101090101010A0101010C01010110010101250101
          0138369F71FF369F71FF369F71FF369F71FF369F71FF369F71FF369F71FF369F
          71FF369F71FF369F71FF369F71FF02140F440101012401010121010101150101
          010E0101010B0101010901010109010101050101010301010102000000000000
          00000101010201010104010101060101010A0101010D01010111010101140101
          011835A071FF3ED7A8FF3ED7A8FF3ED7A8FF3ED7A8FF3ED7A8FF3ED7A8FF3ED7
          A8FF3ED7A8FF3ED7A8FF359F72FF010101200101011C01010118010101140101
          010F0101010B0101010801010105010101030000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000035A172FF3ED7A8FF15BA8DFF15BA8DFF15BA8DFF15BA8DFF15BA8DFF15BA
          8DFF15BA8DFF3ED7A8FF34A072FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000033A273FF3BD7A7FF0DC790FF0DC790FF0DC790FF0DC790FF0DC790FF0DC7
          90FF0DC790FF3BD7A7FF34A273FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000032A374FF36D5A5FF0DC791FF0DC791FF0DC791FF0DC791FF0DC791FF0DC7
          91FF0DC791FF36D6A6FF33A374FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000031A475FF31D5A3FF0DC892FF0DC892FF0DC892FF0DC892FF0DC892FF0DC8
          92FF0DC892FF31D4A4FF32A474FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002FA576FF2BD3A2FF0ECA93FF0ECA93FF0ECA93FF0ECA93FF0ECA93FF0ECA
          93FF0ECA93FF2BD3A2FF30A575FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002EA777FF25D2A0FF0ECB94FF0ECB94FF0ECB94FF0ECB94FF0ECB94FF0ECB
          94FF0ECB94FF25D1A0FF2FA676FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002DA778FF1FD09EFF0ECC95FF0ECC95FF0ECC95FF0ECC95FF0ECC95FF0ECC
          95FF0ECC95FF1FD19DFF2DA778FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00002BA97AFF19D09BFF0ECD97FF0ECD97FF0ECD97FF0ECD97FF0ECD97FF0ECD
          97FF0ECD97FF19D09CFF2CA979FF000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002AAB7CFF29AB
          7CFF2AAB7CFF29AB7BFF2AAB7BFF2AAB7BFF2AAB7BFF2AAB7BFF2AAB7BFF29AB
          7BFF29AB7BFF16CE9AFF0ECF99FF0ECF99FF0ECF99FF0ECF99FF0ECF99FF0ECF
          99FF0ECF99FF15CF9AFF2AAA7BFF2BAA7BFF2BAA7AFF2AAA7BFF2AAA7AFF2BAA
          7AFF2BAA7AFF2AAA7AFF2BAA7AFF2BAA7AFF2BAA7AFF0000000028AD7DFF1FD4
          A1FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DC
          B1FF40DCB1FF12CE99FF0ED09AFF0ED09AFF0ED09AFF0ED09AFF0ED09AFF0ED0
          9AFF0ED09AFF12CE99FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DCB1FF40DC
          B1FF40DCB1FF40DCB1FF40DCB1FF42DDB2FF29AB7BFF0000000026AF7EFF22D7
          A4FF12C997FF12C997FF12C997FF12C997FF12C997FF12C997FF12C997FF12C9
          97FF12C997FF12C997FF0ED29CFF0ED29CFF0ED29CFF0ED29CFF0ED29CFF0ED2
          9CFF0ED29CFF0ED29CFF12C997FF12C997FF12C997FF12C997FF12C997FF12C9
          97FF12C997FF12C997FF12C997FF17D49FFF28AD7DFF0000000024B07FFF27D8
          A6FF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD3
          9DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD3
          9DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD39DFF0FD3
          9DFF0FD39DFF0FD39DFF0FD39DFF1BD6A1FF26AE7EFF0000000023B281FF2ADA
          A8FF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD5
          9FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD5
          9FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD59FFF0FD5
          9FFF0FD59FFF0FD59FFF0FD59FFF1ED8A4FF24B07FFF0000000021B381FF2DDB
          ABFF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6
          A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6
          A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6A1FF0FD6
          A1FF0FD6A1FF0FD6A1FF0FD6A1FF21D9A7FF22B281FF0000000020B583FF31DD
          ACFF11D8A2FF11D8A2FF11D8A2FF11D8A2FF11D8A2FF10D8A2FF10D8A2FF10D8
          A2FF10D8A2FF10D8A2FF10D8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8
          A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8A2FF0FD8
          A2FF0FD8A2FF0FD8A2FF0FD8A2FF25DBA9FF22B381FF000000001DB784FF36DE
          AFFF14D9A5FF14D9A5FF14D9A5FF14D9A5FF14D9A5FF14D9A5FF14D9A5FF13D9
          A5FF13D9A5FF13D9A5FF13D9A5FF13D9A5FF12D9A4FF12D9A4FF12D9A4FF12D9
          A4FF12D9A4FF12D9A4FF11D9A4FF11D9A4FF11D9A4FF11D9A4FF11D9A4FF11D9
          A4FF10D9A4FF10D9A4FF10D9A4FF29DCABFF1FB582FF000000001CB885FF3BDF
          B0FF18DBA7FF18DBA7FF18DBA7FF18DBA7FF18DBA7FF17DBA7FF17DBA7FF17DB
          A7FF17DBA7FF16DBA7FF16DBA7FF16DBA7FF16DBA7FF16DBA7FF15DBA7FF15DB
          A7FF15DBA7FF15DBA7FF15DBA7FF14DBA7FF14DBA7FF14DBA7FF14DBA7FF14DB
          A6FF14DBA6FF14DBA6FF13DBA6FF2EDEADFF1EB784FF000000001AB986FFCFF4
          E8FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9
          D9FFF5F9F7FF21DDAAFF1BDCA8FF1BDCA8FF1BDCA8FF1BDCA8FF1ADCA8FF1ADC
          A8FF1ADCA8FF1FDDAAFFF5F9F7FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9
          D9FF91F9D9FF91F9D9FF91F9D9FFF5F9F7FF1CB885FF0000000019BB88FF19BB
          87FF19BC88FF19BB87FF19BB87FF19BB88FF1ABB88FF19BA87FF19BB88FF1ABB
          87FF19BB87FF2CE0AEFF1FDDAAFF1FDDAAFF1FDDAAFF1FDDAAFF1EDDAAFF1EDD
          AAFF1EDDAAFF2AE0AEFF1ABA87FF1BBB87FF1ABA87FF1ABA87FF1ABA87FF1ABA
          87FF1ABA87FF1ABA87FF1BBA86FF1BBA86FF1BBA86FF00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000018BC88FF37E2B0FF24DDAAFF23DDAAFF23DDAAFF23DDAAFF23DDAAFF22DD
          AAFF22DEAAFF35E2B0FF18BC88FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000016BD89FF41E4B4FF28DEACFF28DEACFF27DEACFF27DEACFF27DEACFF27DE
          ACFF26DEACFF3EE3B3FF17BD89FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000015BF8BFF48E5B6FF2CDEACFF2CDEACFF2BDEACFF2BDEACFF2BDEACFF2BDE
          ACFF2BDEACFF47E5B6FF16BF8AFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000014C08CFF4EE6B8FF30DEACFF30DEACFF30DEACFF2FDEACFF2FDEACFF2FDE
          ACFF2FDEADFF4DE6B8FF14C08CFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000013C18DFF52E7B9FF33DEACFF33DEACFF33DEACFF33DEACFF33DEACFF32DE
          ACFF32DEADFF51E7B9FF13C18DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000012C38DFF55E8BAFF37DDACFF37DDACFF36DDACFF36DDACFF36DEACFF36DE
          ACFF36DEACFF55E8BAFF12C28DFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000010C48EFF56E9BBFF39DDACFF39DDACFF39DDACFF39DDACFF39DDACFF39DD
          ACFF39DDACFF56E9BBFF11C38EFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FC48FFF7BEDC8FF3CDCACFF3CDCACFF3BDCACFF3BDCACFF3BDCACFF3BDC
          ACFF3BDDACFF7BEDC8FF10C48FFF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FC590FFF5F9F7FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9D9FF91F9
          D9FF91F9D9FFF5F9F7FF10C590FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000FC590FF0FC590FF0FC590FF0FC590FF0FC590FF0FC590FF0FC590FF0FC5
          90FF0FC590FF0FC590FF0FC590FF000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00860001FF80000000C0000003FFC007FFFFC007FFFFC007FFFFC007FFFFC0
          07FFFFC007FFFFC007FFFFC007FF000000010000000100000001000000010000
          0001000000010000000100000001000000010000000100000001FFC007FFFFC0
          07FFFFC007FFFFC007FFFFC007FFFFC007FFFFC007FFFFC007FFFFC007FFFFC0
          07FF}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000007080F12090B14180000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000B0F1E27000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000007080F124855A4CC4958AAD50809
          1215000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001118303F4059B9F6273670960000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000008090F124A56A5CC5B6BCDFF5A6ACCFF4958
          AAD5080912150000000000000000000000000000000000000000000000000000
          000000000000000000000000000011172F3D425ABAF6445DBFFF435DBFFF2635
          6E93000000000000000000000000000000000000000000000000000000000000
          0000000000000000000007070D0F4D58A6CC5E6DCFFF6F7CD7FF707ED8FF596A
          CDFF4957A9D30809121500000000000000000000000000000000000000000000
          0000000000000000000012182F3D445BBBF6475FC3FF6879D5FF445DC1FF425D
          C0FF25346C900000000000000000000000000000000000000000000000000000
          00000000000007080D0F4F59A8CD616FD1FF717EDBFF9FA6F4FF9FA6F3FF6E7C
          D9FF596ACDFF4756A6D008091115000000000000000000000000000000000000
          0000000000001318303D465CBBF34960C5FF6877D8FF9397F3FF858DEAFF445D
          C3FF425DBFFF25346C9000000000000000000000000000000000000000000000
          000007080D0F525BAACD6470D3FF747FDCFF98A0F2FF9099F4FF8F97F4FF959E
          F2FF6C7ADAFF596ACDFF4756A6D007080F120000000000000000000000000000
          00001318303D485DBCF34C61C6FF6777D9FF868DF1FF7D82EEFF8186EFFF8188
          EAFF455CC3FF425CBFFF24336A8D000000000000000000000000000000000708
          0D0F535BA7C96772D4FF7681DDFF99A2F2FF8B95F3FF7C86F1FF7B85F1FF868F
          F3FF939CF2FF6C7ADAFF5869CDFF4653A4CD07080F1200000000000000001318
          2E3A4B5FBEF34E63C8FF6A78DBFF868CF1FF7279EDFF676EEBFF6C72EBFF7D81
          EEFF8087E9FF445CC3FF425CBFFF24336A8D000000000000000007080E0F555B
          A9C96A74D6FF7982DEFF9FA8F4FF8F9AF4FF7C88F1FF707DF0FF6F7BEFFF7681
          F0FF858FF2FF939CF2FF6B79D9FF5869CDFF4553A4CD06070D0F14182F3A4E60
          BFF35165C9FF6C7ADBFF888EF1FF737BEEFF636AEAFF5B63E9FF5D65EAFF6B71
          EBFF7F84EFFF838BE9FF435DC1FF415CBEFF2332678A000000003E437A906D75
          D7FF767FDCFFAAB1F4FFA2ABF7FF8A95F4FF7784F1FF6E7CEFFF6B79EFFF6D7A
          EFFF7681F0FF858FF2FF939BF2FF6A79D9FF5869CDFF4756A8D34F61BEF05466
          CBFF6E7CDCFF8A91F2FF767EEEFF646DEBFF5B64EAFF5860E8FF5A62E9FF646C
          EBFF797FEEFF8F94F2FF818CE5FF425DC0FF415CBEFF11183242050509094F55
          9BB76D75D7FF767FDCFFA1A9F3FF949FF5FF808CF2FF7281F1FF6C7BEFFF6B78
          EFFF6D7AEFFF7681F0FF858FF2FF929AF1FF6D7ADAFF5869CEFF5768CDFF707D
          DEFF8C93F2FF7880EEFF676FECFF5E67EAFF5962E9FF5A63E9FF6068EAFF6E75
          ECFF8288F0FF7C86E5FF455EC2FF435DBFFF1F2B587500000000000000000404
          06064F559CB86C75D7FF747EDCFF9EA7F3FF929EF5FF808CF2FF7281F1FF6C7B
          EFFF6B78EFFF6D7AEFFF7681F0FF848DF2FF939BF3FF8E97EFFF929AF2FF8A92
          F2FF7981EFFF6972EDFF606AEBFF5C65EAFF5D66E9FF626AEBFF6F77EDFF8187
          EFFF7C85E5FF485FC5FF455EC1FF1F2B57730000000000000000000000000000
          0000040406064C5296B26C76D7FF717CDBFF9DA7F3FF929EF5FF808CF2FF7280
          F1FF6C7BEFFF6B78EFFF6D7AEFFF747FF0FF7C85F1FF8088F1FF7D85F1FF757E
          EFFF6A75EEFF626CECFF5E68EBFF5F69EAFF656DECFF727AEDFF838AF0FF7C86
          E5FF4A60C6FF4860C3FF1E29536D000000000000000000000000000000000000
          000000000000040406064A4F91AC6C75D7FF707BDBFF9DA7F3FF929EF5FF808C
          F2FF7280F1FF6C7BEFFF6B78EFFF6B78EEFF6D79EFFF6E79EFFF6C77EEFF6873
          EEFF646EECFF616CECFF616BEBFF6770ECFF747DEEFF868DF1FF7F88E6FF4C62
          C7FF4A61C4FF2029546D00000000000000000000000000000000000000000000
          0000000000000000000002020403484E8EA96B75D7FF6F79DAFF9DA6F3FF929E
          F5FF7F8BF2FF717FF1FF6B7AEFFF6A78EFFF6976EEFF6774EEFF6672EEFF6571
          EDFF636FEDFF636EECFF6872EDFF757EEFFF888FF1FF7F89E5FF4F63C8FF4D63
          C5FF1F2850670000000000000000000000000000000000000000000000000000
          000000000000000000000000000002020403464C8CA66B75D6FF6D77D9FF9DA6
          F4FF8E9AF5FF7986F1FF6E7DF0FF6A79EFFF6977EFFF6875EEFF6673EEFF6571
          EEFF6571EDFF6672EEFF727BEEFF868EF2FF808AE7FF5265CAFF4F64C7FF2028
          5067000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000002020403444A87A06A75D6FF8690
          E6FF9AA4F6FF808DF3FF7180F0FF6C7BF0FF6A79EFFF6977EFFF6875EEFF6673
          EEFF6672EEFF6B76EEFF7A83F0FF929AF3FF6D7BD9FF5266C8FF1F274C610000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000017192D346A74D5FC6B76
          DAFF9DA8F6FF8290F3FF7381F2FF6E7DF0FF6B7AF0FF6A79EFFF6977EFFF6875
          EEFF6774EEFF6C78EFFF7E87F0FF8E97F0FF5668CDFF4859B0DF070810130000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000001A1C323A686FCBF06E76D9FF8791
          E7FF99A4F6FF818EF4FF7381F1FF6E7EF1FF6D7CF0FF6B7AF0FF6A79EFFF6977
          EFFF6976EEFF6C79EFFF7B84F1FF9199F3FF6E7CDBFF5667CBFF4453A4D00809
          1115000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000001B1C323A6A71CCF07078DAFF8790E7FFA1AC
          F7FF8B99F5FF7B8AF3FF7382F2FF6F7FF1FF6E7EF1FF6D7CF0FF6B7AF0FF6A79
          EFFF6977EFFF6B78EEFF737FF0FF838CF2FF9199F1FF6A78D8FF5567CBFF4554
          A7D3080911150000000000000000000000000000000000000000000000000000
          000000000000000000001D1E353D6E73D0F37279DCFF8993E8FFA4AFF8FF8F9D
          F6FF808FF4FF7686F3FF7383F2FF7381F2FF7281F1FF7181F1FF707FF0FF6D7C
          F0FF6B7AEFFF6A78EFFF6D7AEFFF7581F0FF858EF2FF939BF2FF6A79D8FF5567
          CAFF4455A8D50809111500000000000000000000000000000000000000000000
          00000000000020213B437176D5F6757BDDFF8D96E9FFA6B2F9FF92A0F6FF8292
          F5FF7989F4FF7586F3FF7685F3FF7988F3FF7E8BF3FF7F8CF3FF7D8AF3FF7884
          F1FF707FF1FF6C7BEFFF6B78EFFF6D7AEFFF7681F0FF858EF2FF929AF2FF6A79
          D9FF5467CAFF4455A7D5080A1418000000000000000000000000000000000000
          000021213B437477D5F6777CDEFF9098EBFFA8B4F8FF94A2F7FF8395F6FF7C8C
          F4FF7889F4FF7888F4FF7D8DF4FF8795F5FF939FF5FF98A3F6FF949FF5FF8A97
          F4FF7E8AF2FF7280F1FF6C7BEFFF6B78EFFF6D7AEFFF7581F0FF848EF2FF929A
          F2FF6A79D9FF5466CAFF4555AAD8080A14180000000000000000000000002424
          40487678D6F67A7EDFFF929AEBFFAAB7FAFF95A5F8FF8597F6FF7E8FF5FF7A8C
          F5FF7B8BF4FF8090F5FF8B99F6FF9CA8F7FF949DEFFF8590E6FF949EEDFF9DA7
          F4FF909CF5FF7F8BF2FF7280F1FF6C7BEFFF6B78EFFF6D7AEFFF7581F0FF858E
          F2FF949CF2FF6C7AD8FF5366C9FF4555A9D8090B161B000000002727454E7A7C
          DBF97C7EDFFF979CEAFFB5BFFBFF9DABF8FF899AF7FF7F91F6FF7C8EF5FF7D8E
          F5FF8193F6FF8E9CF6FFA0ABF8FF98A1EFFF6C76D8FF6A74D7FF6973D6FF6E79
          DAFF9CA5F3FF919CF5FF7F8BF2FF7280F1FF6C7BEFFF6B78EFFF6E7BEFFF7983
          F0FF8C95F3FF9BA4F2FF6C7BD7FF5266C8FF4557ABDB090B161B2D2E505A7C7D
          DEFC7D7EDFFF999FECFFB5BFFAFF9DABF9FF899BF7FF8092F6FF7E91F6FF8395
          F6FF909FF7FFA2AEF8FF9AA3EFFF6F78DAFF6C75D7FF2C2F5767474E8FAC6773
          D4FF6E79DAFF9CA6F3FF919CF5FF7F8BF2FF7280F1FF6D7CEFFF6F7CF0FF7A84
          F0FF8C95F3FFA0A7F5FF7180D8FF5467C8FF495BB2E40C0F1D24000000002F2F
          535D7C7DDEFC7D7FE1FF969DEDFFAAB7FAFF95A5F9FF889AF7FF8799F7FF92A1
          F7FFA4B0F8FF9CA5F0FF7279DCFF6F76D9FF2B2F55640000000003040606474E
          8FAC6773D4FF6D79D9FF9CA5F3FF919CF5FF7F8BF2FF7582F1FF7885F1FF8691
          F3FF99A0F4FF7481DCFF5868CBFF4C5CB4E40D111F2700000000000000000000
          00002F2F535E7B7DDEFC7C80E1FF959DEEFFA9B6FAFF9BAAF9FF9AA9F8FFA7B3
          F9FF9FA8F1FF747BDCFF7278DAFF2C2F55640000000000000000000000000304
          0606464E8FAC6773D4FF6E7ADAFF9CA5F3FF929EF5FF8793F3FF8B96F4FF9AA3
          F5FF7682DFFF5B6BCEFF505FB8E70E1120270000000000000000000000000000
          0000000000002F2F535E7B7DDEFC7C80E1FF969DEDFFB2BEFAFFB1BDFAFFA3AB
          F2FF777CDEFF747ADCFF2F315967000000000000000000000000000000000000
          000003040606474E92AF6672D4FF6E7ADAFF9EA7F3FF9FA9F6FFA2ABF6FF7985
          DFFF5D6DCFFF5261BAE70F112128000000000000000000000000000000000000
          00000000000000000000303156617B7DDEFC7C7FE1FF999FEBFFA9B1F2FF787D
          DFFF777CDDFF31335C6A00000000000000000000000000000000000000000000
          00000000000003040606474E92AF6672D4FF6D79D9FFA6AFF4FF7D88E0FF606E
          D1FF5562BCE71013242B00000000000000000000000000000000000000000000
          0000000000000000000000000000303156617B7CDDFC7C7FE0FF7A7EDFFF797C
          DDFF32345D6A0000000000000000000000000000000000000000000000000000
          0000000000000000000003040606464E90AE6671D3FF6D78D7FF626FD2FF5965
          BFEA1013232A0000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000303055607C7EDFFF7B7EDFFF3435
          5F6C000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000003040606464E90AE6571D2FF5B66C0EA1113
          232A000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000303055603637626F0000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000030406063F46829C1315262D0000
          0000000000000000000000000000000000000000000000000000}
        Mask.Data = {
          BE000000424DBE000000000000003E0000002800000020000000200000000100
          010000000000800000000000000000000000020000000000000000000000FFFF
          FF00FE7FFEFFFC3FFC7FF81FF83FF00FF01FE007E00FC003C007800180030000
          0001000000000000000180000003C0000007E000000FF000001FF800003FFC00
          007FFE00007FFC00003FF800001FF000000FE0000007C0000003800000010000
          00000000000080010001C0038003E007C007F00FE00FF81FF01FFC3FF83FFE7F
          FC7F}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00F2F7F200F3F6F300FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0096B496002981290082A08200FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00C0CDC0004295430013981E001CAD2A0080A08000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00F7F7F7008CB4
          8C00238F2A0020AF32002AC5400020B5320081A18100FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00F9F9F900BDD1BD002C8E30001FA5
          2F0035D0500032CC4B0030C9480028BB3B0082A18200FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00F8F9F8006594650023952D0032C54C003BD4
          58003AD3550037D0550036CF51002BBE4200407E400082A1820096B19600B3C7
          B300E8EBE800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00F6F6F6009BB09B00268C2C0030BF480042DB630042DB630042DB
          630041DA620040D960003DD65C003CD559002DC046002BBF420021AF3400199F
          26000E8B16001E8A220072AF7300B4C4B400F6F6F600FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00F5F8F5005D955D0026A435003BCF590046DF680047E06A0047E06B0049E2
          6D0048E16C0046DF670043DC64003ED75E003BD4590037D0530032CC4B002DC7
          450027C23C0020B8320016A3210015901B0053915300BDCEBD00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00F3F6F300518F510027AC3B0041D763004CE572004EE7750050E9780050E9
          79004FE876004DE6730049E26D0045DE680041DC62003CD75B0037D1530032CC
          4B002BC5410026BF390020B930001BB62A0013A91D0007850A00729F7200ECEE
          EC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00F3F3F30088BB8800228D2A0043D5650055F0810056EF810056EF
          820055EE7F0050E97B004DE6740042D56200138A1D00138D1C00148F1F001B9C
          280020AE310026BF3A0023BD34001DB72B0016AF210011AB190009870C00579E
          5700FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00EAEDEA00538A54002DA43D0055EA80005EF8
          8E005BF4880056EF820051EB790040D4620083A08300FF00FF00EFF3EF00CBD8
          CB0093AE93005C945C003193350015951D0013A71E0012AB1B000CA612000389
          07008AAF8A00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00F6F6F600A6C1A6002A95340045D0
          680060FB91005AF3850052EB7B0042D4640081A18100FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00DBDFDB00A8C2A80028892A0007880D000CA6130007A0
          0A002F892F00E0E4E000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EDEEED0079A7
          79002CA13D0047D76B0053EE7C0041D4620081A18100FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FEFEFE00AFC4AF00228322000493
          070004890400B4BDB400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00ADBEAD0039923C0033BC4D003FD4600080A08000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00CAD5CA00338B
          330000850000ACB6AC00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0082A882002388270082A38200FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00EAED
          EA001A7B1A00B3BCB300FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FEFEFE00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FEFEFE00CED3CE00D1DED100FF00FF00FF00FF00FAFA
          FA00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00CBD7CB00E8EBE800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00B9C2B9003F923F00F7F7F700FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0084A5
          84003E933E00BDCBBD00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00ACB6AC0008810C007DAB7D00FBFBFB00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0080A1
          80000F9816001A881D0088B28800F4F4F400FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00B0B9B00017A22200169C220049994A00DADEDA00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0081A1
          810018AC25001BB628000E95150021862200BBD0BB00F7F7F700FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00C9CFC90024922A002CC6420022AF34002C8A2E007DA4
          7D00F8F9F800FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0081A1
          81001DAF2B001EB72E0019B3260013A71D001587180064946400F1F3F100FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FCFCFC006A9E6A0026B63A0035CF500037D1530028B4
          3D001B8A21005BA55D00ACC5AC00CCD1CC00E7E9E700F9F9F900FF00FF0082A1
          820022B5320024BD36001FB82F001BB4290015B020000E9E150019811A0097AD
          9700F6F6F600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00E9EAE9001E8521002FBF47003ED85D003ED8
          5F003DD55C0035C54F0029AD3D00329F3C003E9742004A954A00557E55002B70
          2B0024B7380028C13C0022BB35001FB82E0018B1260015AF1F000DA114000888
          0B005F985F00DFE7DF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00D3DFD300428943002EB9460044DC
          660048E16C0047E16B0048E26B0046E1690044DE660040DB61003CD85A0038D3
          540031CA4A002DC6440027C03B0022BB34001DB62A0018B1240012AB1B000DA7
          1400069709000179010092B29200F9FAF900FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E5E6E50064A865001F92
          290042D5640050EA78004DE775004BE4710047E16A0043DC65003ED75D003AD3
          570033CC4E0030C9480029C23E0025BE38001EB72F001AB3270014AD1D000FA8
          170009A40D000393050019741900EEF5EE00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00E9EC
          E9006C986C003F99450034B1480038C355003DCF5D003FD35F003ED45D0039CF
          570038D2540033CC4C002DC6430027C03B0021BA31001CB52B0015AF21000FA3
          1600108613005B8E5B00E9ECE900FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00E7EAE700BCC3BC008FB38F0060A86300479E4B0035983B002088
          260034C84D0035CE4F002DC6440029C23E0022BB34001FBA2D00109A18001984
          1B00B2CAB200F6F6F600FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0083A1
          830031C54A0036CF50002FC847002BC540001CAB2A001B8C1F007CAA7C00F0F1
          F000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0081A1
          810031C54A0036CF51002EC946001BA42A00368E3800B5C4B500FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0081A1
          810030C3480029BA3F00258A29008AAD8A00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF007F9F
          7F00118D1C0041944300D1D5D100FEFEFE00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00C9D7
          C900C5DDC500FEFEFE00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        MaskColor = clFuchsia
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000020000
          0003000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000005000000030000000200000000000000050000
          00080000000B0000000E00000010000000140000001700000028000000310001
          02350211357203236DAD05319BD2063BBBFF053DB9FF063CBAFF063ABBFF063B
          BBFF053098EC042067B70210338900000051000000460000003E0000003B0000
          0033000000310000002800000019000000140000001000000000000000040000
          000400000006000000090000000D00000010000000140001041D0219527F0536
          AEEF043CBEFF0346CAFF0250D8FF0252DBFF0256E0FF0257E1FF0255DFFF0252
          DBFF024ED6FF0342C6FF053ABBFF0634A8EB03184C820001022A000000240000
          00200000001C0000001800000014000000100000000D00000000000000000000
          000000000000000000000000000000000000011035480431A2DE043EC2FF0251
          DAFF0257E2FF0257E2FF0154DCFF0252D7FF0150D4FF014FD1FF0150D4FF0252
          D7FF0154DBFF0257E2FF0257E2FF024ED7FF053ABDFF04309FDB010E2E3F0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000021A55750537B9FC024FD6FF0258E3FF0254
          DBFF024ED2FF024ECEFF024ECEFF024DCCFF024DCDFF014ECEFF014DCDFF014D
          CCFF014DCCFF014DCCFF014FD0FF0254DBFF0257E2FF0349D1FF0535B3F90217
          4E6C000000000000000000000000000000000000000000000000000000000000
          0000000000000000000003206B93063EC1FF0355DFFF0255DDFF0250D3FF0350
          D0FF0652D0FF0A54D3FF0B57D5FF0955D7FF0451D1FF034DCBFF024ECEFF0251
          D5FF0252D8FF0251D5FF024FD0FF014ECEFF014FD1FF0255DDFF0353DCFF0538
          BAFF041E63880406090900000000000000000000000000000000000000000000
          000000000000021853720742C3FF0357E0FF0153D9FF0351D3FF0552D3FF0C58
          D4FF155CD1FF1253C1FF084ABBFF0C4DBBFF1C57BEFF3368C2FF245DC0FF1150
          BCFF0749BAFF084CC1FF0251D5FF0252D7FF024FD2FF024FD1FF0153D9FF0354
          DEFF053BBDFF04194C67070B1010000000000000000000000000000000000000
          0000010F3245083DBDFC0456DEFF0252D7FF0351D3FF0553D5FF105AD6FF205E
          C9FF0548BBFF416DB9FFA1A6AEFFC6C6C6FFD7D7D7FFDFDFDFFFE3E3E3FFE4E4
          E4FFD7DBE2FF6D91CEFF094BBCFF084EC3FF0354DAFF0252D6FF0250D3FF0252
          D8FF0351DAFF0537B5F908163645060A0E0E0000000000000000000000000001
          04060530A0DB0856DBFF0254DAFF0352D5FF0555D8FF0855D6FF0855D6FF0855
          D6FF0551D2FF0F41AFFFA7A7A7FFAAAAAAFFC4C4C4FFD6D6D6FFDFDFDFFFE3E3
          E3FFE4E4E4FFE5E5E5FFE0E1E4FF4072C7FF084BBEFF0354D8FF0353D8FF0351
          D3FF0253D9FF044BD2FF1340A4D6010204050000000000000000000000000217
          4D690A4CCDFF0557DDFF0353D8FF0354DAFF0A57D7FF0855D6FF0855D6FF0755
          D5FF0655D5FF0551D3FF0E41B0FFA5A5A5FFA9A9A9FFC3C3C3FFD6D6D6FFDFDF
          DFFFE3E3E3FFE4E4E4FFE5E5E5FFE5E5E5FF6D90CFFF074BBEFF0455DBFF0354
          D9FF0352D6FF0353DAFF0A46C7FF0C23536A0000000000000000000102030534
          AAE80B5ADDFF0354DAFF0455DBFF7EB2F6FF98C5FAFF1764DBFF0F59D6FF0855
          D6FF0855D6FF0755D6FF0551D3FF0E41B1FFA5A5A5FFA9A9A9FFC3C3C3FFD6D6
          D6FFDEDEDEFFE3E3E3FFE4E4E4FFE5E5E5FFE5E5E5FF4877C9FF094EC3FF0456
          DDFF0455D9FF0353D8FF0C55D8FF0432A3DF00000000000000000212394E0849
          CCFF0B5DDFFF0456DDFF0D5EDFFF5291EBFF0449BFFF3787E8FF1564DBFF0856
          D6FF0856D6FF0856D6FF0756D6FF0551D3FF0E42B2FFA5A5A5FFA9A9A9FFC4C4
          C4FFD7D7D7FFE0E0E0FFE5E5E5FFE6E6E6FFE7E7E7FFE5E6E7FF0E4FC0FF0453
          D5FF0557DDFF0455DBFF0755D9FF0540C3FF010E2C3C0000000003236E960B52
          D5FF0658DDFF0658DEFF0759DEFF0D52C7FF648CD1FF0E57D1FF3989EAFF1765
          DCFF0957D7FF0957D7FF0957D7FF0857D7FF0551D3FF0D42B4FFA5A5A5FFA9A9
          A9FFC5C5C5FFD9D9D9FFE2E2E2FFE7E7E7FFE8E8E8FFE9E9E9FF8EA9D8FF094E
          C3FF0558DEFF0557DEFF0457DDFF0447CCFF031F6387000000000537A3D80855
          D8FF0558DEFF0859DDFF0B5BDEFF074CC0FFE3E5E9FFE5E7E9FF0E57D1FF3B8B
          EAFF1766DDFF0958D8FF0958D8FF0958D8FF0858D8FF0551D3FF0D42B5FFA5A5
          A5FFA9A9A9FFC5C5C5FFDADADAFFE4E4E4FFE9E9E9FFEAEAEAFFE8E9EAFF074B
          C0FF0658DEFF0658DEFF0558DEFF054BD1FF053197C9000000000643C0FA0655
          DCFF075ADFFF0B5CDEFF0E5AD8FF1555C3FFEAEAEAFFEBEBEBFFE8EAECFF0E57
          D1FF3E8DEAFF1867DDFF0958D8FF0958D8FF0958D8FF0858D8FF0551D3FF0C43
          B6FFA5A5A5FFA9A9A9FFC6C6C6FFDCDCDCFFE5E5E5FFEAEAEAFFECECECFF2661
          C7FF0655D7FF075ADFFF0659DFFF054FD3FF063DB7EF000000000746C6FF0658
          DEFF095BDFFF0F5EDEFF0E58D1FF2D64C3FFEAEAEAFFECECECFFEFEFEFFFEAEC
          EEFF0E57D1FF4290EBFF1968DEFF0A59D9FF0A59D9FF0A59D9FF0959D9FF0551
          D3FF0C43B8FFA5A5A5FFA9A9A9FFC7C7C7FFDDDDDDFFE6E6E6FFEAEAEAFF497A
          CEFF0755D4FF085BE0FF075ADFFF0651D6FF0643C4FF000000000848C6FF075A
          E0FF0A5CDFFF1160DFFF0E55CCFF4774C1FFE8E8E8FFECECECFFF0F0F0FFF1F1
          F1FFECEEF0FF0E57D1FF4692ECFF1A69DFFF0A5ADAFF0A5ADAFF0A5ADAFF095A
          DAFF0551D3FF0B42B9FFA5A5A5FFA9A9A9FFC6C6C6FFDBDBDBFFE4E4E4FF648C
          D3FF0854D1FF0A5CE0FF085BE0FF0754D8FF0745C4FF00000000094BC7FF085A
          DFFF0B5DE0FF1262DFFF1259CEFF386AC0FFE0E0E0FFEAEAEAFFF0F0F0FFF2F2
          F2FFF2F2F2FFEDEFF1FF0E57D1FF4995EDFF1C6BE0FF0B5BDBFF0B5BDBFF0B5B
          DBFF0A5BDBFF0551D3FF0B43BAFFA5A5A5FFA8A8A8FFBBBBBBFFC7C7C7FF4274
          CBFF125BD2FF0E60E0FF0A5DE1FF0854D7FF0847C5FF00000000094AC2F90859
          DDFF0B5EE2FF1363E0FF175FD2FF225DC2FFC9C9C9FFE8E8E8FFEFEFEFFFF3F3
          F3FFF3F3F3FFF4F4F4FFF0F1F3FF0E57D1FF4C97EEFF1C6BE0FF0B5BDBFF0B5B
          DBFF0B5BDBFF0A5BDBFF0551D3FF0A43BCFFA3A4A4FFA8A8A8FFB1B1B1FF205C
          C4FF1D64D4FF1363E0FF0B5DE1FF0955D6FF0745B8ED000000000843A6D20959
          DAFF0B5FE2FF1262E1FF2069DDFF094EC5FFB0B1B1FFE0E0E0FFECECECFFF2F2
          F2FFF5F5F5FFF5F5F5FFF6F6F6FFF2F3F5FF0E57D1FF509AF0FF1E6EE1FF0C5D
          DCFF0C5DDCFF0C5DDCFF0B5DDCFF0551D3FF0943BDFFA6A7A7FFABACACFF0B4E
          C3FF286BD6FF1564E0FF1363E2FF0D56D2FF073B97C000000000063177960A57
          D4FF0F62E3FF1062E3FF1D68E0FF0E52C8FF7990B8FFBEBEBEFFE7E7E7FFEFEF
          EFFFF3F3F3FFF7F7F7FFF8F8F8FFF8F8F8FFF4F5F7FF0E57D1FF549CF0FF206F
          E2FF0D5EDDFF0D5EDDFF0D5EDDFF0C5EDDFF0551D3FF0943BDFF6A86B6FF1856
          C2FF3573D6FF1664E1FF2776E7FF0B54CEFF052B6B8700000000031838450B57
          CEFF1E6FE8FF1265E5FF1868E3FF1C63D5FF1456C7FFB1B1B1FFC7C7C7FFE6E6
          E6FFEFEFEFFFF5F5F5FFF8F8F8FFF8F8F8FFF9F9F9FFF6F7F9FF0E57D1FF579F
          F1FF206FE3FF0D5EDEFF0D5EDEFF0D5EDEFF0C5EDEFF0551D3FF0348C4FF2E6B
          CEFF236CDDFF1D6CE5FF2D7AE6FF0A54CCFF02122B3600000000000102030B4F
          B7E11667E1FF2F7EEBFF1365E5FF206DE1FF0F54CAFF567DC1FFB1B1B1FFC0C0
          C0FFE3E3E3FFECECECFFF2F2F2FFF5F5F5FFF6F6F6FFF7F7F7FFF3F4F6FF0E57
          D1FF5AA0F2FF2170E4FF0D5FDFFF0D5FDFFF0D5FDFFF0C5FDFFF0D55CDFF2B72
          DCFF1A68E1FF3D88ECFF1463D9FF0A49AED80000000000000000000000000628
          5B6F0C5DD4FF3985EEFF1D6FE8FF186AE5FF256EDFFF094FC8FF738DBAFFB2B2
          B2FFB2B2B2FFCDCDCDFFE4E4E4FFEAEAEAFFEEEEEEFFEFEFEFFFEFEFEFFFEBEC
          EEFF0E57D1FF5CA2F3FF2272E4FF0E60DFFF0E60DFFF0E60DFFF256DDBFF216D
          E1FF2776E8FF3984EBFF0D5CD0FF05224E600000000000000000000000000002
          05060B52B4DB1466DFFF4992F0FF196CE9FF1D6DE5FF246CDCFF0950C9FF567E
          C1FFB1B1B1FFAFAFAFFFB1B1B1FFBDBDBDFFC7C7C7FFCCCCCCFFCDCDCDFFC8C8
          C8FFBDBDBEFF0E57D1FF5FA4F3FF2372E5FF0E61E0FF2E72DAFF2972E0FF2072
          E8FF5099F1FF1362D7FF0B4BAACF000102030000000000000000000000000000
          00000317323C0D60D1FC2172E6FF4E97F2FF176CE9FF1E6FE7FF2872E1FF1056
          CCFF1759CAFF8094B8FFAFAFAFFFB3B3B3FFB4B4B4FFB2B2B2FFB2B2B2FFB4B4
          B4FFB4B5B5FF6F8ABAFF044ECBFFA2CDFDFF327BE3FF2873E3FF1D6EE7FF569E
          F4FF1D6EE0FF0E5ECEF903142C36000000000000000000000000000000000000
          000000000000072D62750E62D5FF307FEBFF559CF4FF1C71EBFF1D70E9FF2976
          E7FF236AD9FF0E54CDFF0B51CAFF2C65C8FF4573C4FF557CC0FF3C6EC4FF245F
          C7FF0D52CAFF145ACEFF66A4F3FF3382F1FF2272E7FF2273EAFF59A1F4FF2A7A
          E7FF0E61D4FF0628576900000000000000000000000000000000000000000000
          0000000000000000000008336E840F65D5FF2677E8FF5CA2F5FF287AEEFF1B71
          EDFF2174EBFF2977E8FF2E78E4FF2167D8FF1A61D4FF195ED1FF1D63D5FF266D
          DAFF3077E2FF2D79E7FF2373E9FF1669E6FF2E7FEFFF5CA3F4FF2072E3FF0E64
          D5FF0730677B0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000062651600F63D0F91A6DE1FF519BF3FF4F99
          F4FF2478EFFF1C72EEFF2074EDFF2476EDFF2677ECFF2778EBFF2778ECFF2576
          ECFF2275EDFF1D72EEFF277AEEFF4F99F3FF4F98F2FF166BDDFF0E61CEF60521
          4654000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000418323C0C53AFCF0E65D8FF2477
          E7FF569EF4FF579FF6FF3E8CF3FF257BF1FF1D74F1FF1E74F1FF1D73F0FF277C
          F1FF408EF3FF5AA1F6FF549CF3FF2073E3FF0F66D7FF0C50A9C903142B330000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000005264E5D0D59
          BBDE0F67D7FF176CDCFF3584EAFF5BA3F4FF60A7F5FF66ACF6FF63A9F5FF57A0
          F3FF3281E9FF156ADBFF0F67D7FF0D56B6D80523495700000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000003152B3308346D810C50A7C60D5BBEE10F67D7FF0F67D7FF0F67D7FF0D5A
          BBDE0B4EA4C307336A7E0312262D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          000000000000000000000203060C12193B75202D65CA354683EB415490F54255
          92F93E4F90FA394A8BFA2F3E82FA24337BF91C2972F7192369F2151F5DE30F15
          3EB903050F68000000320000000F000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000303070C1A22437737477FDE485C9AFB556AA6FF566CA8FF5267
          A6FF4A5EA1FF415399FF35478EFF293986FF1E2C7CFF1A2474FF1A246DFC1720
          61F00F153FC4040611870102055D000000230000000100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000415281C890ADD8FF98B6DFFF88A4D1FF7690C3FF657DB7FF556A
          AAFF495B9AFF3F4F8FFF364687FF2E3E84FF253481FF1F2B7AFF1B236AFF171C
          5BFF171A53FF191E5CFD151B54E6000000510000001300000000000000000000
          0000000000000000000000000000000000000000000000000000010103060000
          0000000000007D96C1ED8BA7CFFF4E5F96FF313D7FFF273174FF1D2356FF1A1D
          3FFF21253EFF272C41FF262A3BFF212435FF222640FF2F3962FF394882FF2734
          77FF1B245FFF191A52FF181A52F90000003D0000002D0000000C000000000000
          0000000000000000000000000000000000000000000000000000020207100000
          0102000000004D608ECB475894FF172165FF1B266FFF121A4DFF2D3948FF6082
          8EFF79A9BBFF7AAAC1FF6F99B2FF607784FF3C4140FF1C222AFF2F3D6BFF4457
          98FF273475FF191F60FC171D54D600000008000000220000002D0000001D0000
          000A000000000000000000000000000000000000000000000000000000000000
          0000000000000709121F2E3C81F42E3F82FF2A356FF66B7584FC8DBCCEFF9DE4
          FFFF97D9F6FF8CC5E7FF83B8DFFF7EABD1FF869AAEFF827A75FE353358F93244
          8BFF273477FF0C12398A02030A1A00000000000000000000000D000000200000
          0022000000120000000400000000000000000000000000000000000000000000
          00000000000000000000303E80EE273980FF493F46B87C725E80A6AEA2C492C6
          DAF98CCDE9FF85BDDFFF74ABD4FF7F9EB4F9A28F7FCFA0826FAC80654CB04F5D
          89FA304490FA02040F2B00000017000000170000000E00000008000000080000
          00120000001A0000001500000009000000010000000000000000000000000000
          00000000000000000000253261A6475893FFACA49DD7B5AF9FB5716F5C71A0A5
          9EC68FB0C8FF87BBD8FF98ADB4EB8E80679C7C684C809E7969A5A37D6AAC8F90
          9DFB243B8BFA0305143F00000024000000270000001F00000019000000140000
          000D0000000C0000000F0000000E000000080000000200000001000000000000
          00000000000000000000202C589B5567A1FFC5C2C3E6C3BEACC3777461779D9E
          92B09C9DADFDA5B7BBF991886F9B7B6C527B8E74588E9C79619CAC8275AD9CA0
          B6FC253B89FD04071D5600000019000000120000000D0000000D0000000C0000
          000B0000000A0000000800000007000000090000000600000002000000000000
          00000000000000000000314072BE4F62A1FFB2ACB0DEB8B4A3B883806D83ABB0
          A6C49A9EB2FEA4B5B9F9988F76A38A775D8A8B71558B97765C97AC8573B0919B
          B6FC263C87FE05082156000000070000000D0000000C0000000B000000090000
          0008000000060000000500000003000000010000000100000001000000000000
          00000000000000000000405185D1485B9AFFA9A3A5DFC4C3B5C4BCBAADBCECED
          EBF2A4A5B7FF98ABB4FA998F78A389765C898D73588DA07E63A0A5846BAA8D99
          B9FC283C87FF0609266100000000000000030000000400000002000000010000
          0001000000000000000000000000000000000000000000000000000000000000
          000000000000000000004C6095E0425697FF6D6568D4CACABCCACECDC3CEE2E5
          E7F09FA1B5FF93A5B1FB8E846D987B684F7BC1AA94C1EADECEEA8B71589E7484
          AAFB2E408AFF0609286A00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010102566AA1EA485EA3FF1C1628A1867C699ED0D0D0D0CFD2
          D2E9B7BBCAFFADBDC7FDA29A85AB8B775F8BCDBBA3CDD8CEB8DC4F3D33AA5C75
          ADFD37468AFF080C2E7100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002020406596EA6F04E63A1FF111445C50E0B091DA39D8FB5DEE2
          E2EDBEC1CEFFBEC8D2FDC6C2B8CCCCC9BACCCAC2ABCC34261552120C10596482
          C6FF354281FF0A0E348800000102000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002030508576DA6F4596CA5FF0B1961D8000000001C170F2CA8A9
          A8E2C5C8D6FFB9C4D0FED2D1C5D4B6B29EBE3A2D1E63000000001A213D676781
          BEFF445695FF0F16419801010306000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000010102043F4F89EB475995FF1D2761C100000000000000003E4B
          74CE8B93ACFFB0C0CAFE9B927CAA20180F420000000000000000182243653F4D
          89FF324383FF0C11326B00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001020408303F81F2334280FF121A4FB4000000000102060C2231
          72DD303B73FF859DAEFD3C34265E0000000000000000000000000D1237764052
          93FF283779FF080C275D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000293F7DE8334F8FFF131F55B50000000000000000202D
          6ACE446299FF7BB0D2FF515756B90000000000000000000000000A102A593652
          91FE294387FF0D17387700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000202040849609BF34B619CFF111A56D60000000001020408304F
          7CE083BBD8FF8FC6E8FF81ACD2FF404647AB06050311000000000A143D716780
          B5FF354483FF0B123D9900000102000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000002020306596CA5F15567A1FF0E1650CA000000004B6779C4A7D5
          E6FF9AD6EFFF85BCDDFF87B2D9FF85A6C7FF525652C306050616080E2D526D87
          C2FF384483FF090D307C00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010102566AA2EB51639FFF0C1749B4365064B6B2D3DCFFB0E5
          F7FF8DCAE7FF86BEDEFF7DB1D3FF84A5C5FF8C9BB2FF524E3FBE1411134B6783
          BEFD374486FF070B2B6E00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000004E6198E14A5D9DFF334572F2A1B7BBFDACD4E2FF92D2
          EBFF90CDE8FF87BEDEFF7EB1D4FF79A3C0FF8896AAFF8C8B90FD443E3AC46477
          A7FC36478FFF0508286800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000425488D4475B9CFF838B9AFFCADAD4FF8BBFD3FF8CD0
          EBFF8ED0EBFF7FBCE0FF75ACD6FF719FC2FF7190A4FF878992FF797A7AFF6D83
          ABFF34448CFE0508245D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000334376C14C609EFFB3B5B3FFD4D8C6E9A3B7B6DAABCB
          D7F191B2C8FC91A2A5D5909B9BCC949795D5928F83D59E8C7DD8A58D81DF8393
          B3FF2C3E89FE0507205000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000024305EA45466A1FFCBCABAF2C3C3ACC3D9D9CCD9FDFC
          FBFDBCB8C2F179725F867067507083715783806B4F808B70558BA07F66A09C9D
          B3FC253A89FD04061B4100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000151E488C55649CFFD7D6BFF1C7C7AFC7E3E1D6E3FFFF
          FFFFD1D0D4F189816D9280725A8089745B89886F548895775995A28463A2A49F
          ABFB223989F80204102600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000001010204212D70E2263375FFB6B5ADF2D3D3BFD3C4C3B3C4D3D4
          D6F3B8B9C6FCB4AFA3BF998F7A998F7C638F866D5386957A5995A18663A75E5C
          7DFD162875FB0608193800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000314183EE263880FF626370B9C9C9B7C9CDCDC4CDC0C4
          C6E8B2B5C4FEC4C3BCD7C5C2B5C7BFB5A7C2BBAD98BBBAAB8CBA817066D13344
          87FF27367BFF080C255500000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000A0F2B5D37478AFB344383FF323D78FB747993F3B9BABCF9CDD0
          D4FCC1C3CCFFBFBDC3F9C7C1C6FBC6BFC3F9BEB5BBFB787594FF1A2263FC394B
          8EFF2B397CFF111950BB0A0D285C000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000253477E842528FFF404C81FF292E63FF151954FF222458FF595A
          7FFF80819BFF838299FF7D7B93FF615F83FF2D3169FF08125EFF141F6AFF3D4C
          8DFF28306CFF151650FF191C56F5000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000001F2A65C93C4F92FF586DA7FF718AB9FF849EC7FF829DC6FF738C
          BAFF6D85B5FF687EB0FF566BA2FF3F5493FF2D3E86FF202D78FF19236AFF151A
          5CFF171A55FF191C59FA151A51D7000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000003040B1712193F80222F68C3394B87E5586EA8F36F89BDF87791
          C4FB758EC3FC6A82BAFC5B73AEFC4A5E9DFB32428AF91E2A76F418236AEA161E
          5ACC0F153F900608193C02030814000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000FFFFFF00FEFE
          FE00FFFFFF00E4E4E400B1B1B100B5B5B500B4B4B400B4B4B400B4B4B400B4B4
          B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B400B4B4B500B5B6
          B600B0B1B100D1D1D100FFFFFF01FCFCFC00FEFEFE00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FDFDFD00FFFF
          FF00DEDEDE001A1A1A14060606360B0B0B350A0A0A350A0A0A350A0A0A350A0A
          0A350A0A0A350909093509090935090909350909093509090935090909350909
          09340B0B0A370100002662626200FFFFFF02FCFCFC00FEFEFE00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90005050537E2E2E2F5F1F1F0FCEDEDEDF9ECECECFAEDEDEDFAEFEF
          EFFAEFEFEFFAEFEFEFFAEFEFEFFAF0F0F0FAF0F0F0FAF0F0F0FAEFEFEFFAEBEA
          E9F9E5E4E1FBC3C1BAF01A1A185856565700FFFFFF03FCFCFC01FEFEFE00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02BABABA0007070736EEEEEEF9FFFFFFFFFBFBFBFEFFFFFFFFFFFFFFFFFCFC
          FCFFFDFEFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFD
          FDFFECECEDFED6D5D5FFD0D0CDFB2B2C2C6855555500FFFFFF03FCFCFC01FEFE
          FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636EBEBEBF8F9F9F9FFFBFBFBFDBBBDBFFEDBDCDDFEFFFF
          FFFEFDFDFDFEF9FAFAFEFAFAFAFEFBFBFBFEFCFCFCFEFDFDFDFEFCFCFCFEF7F7
          F7FEEAEAEAFEC7C6C6FDBBBAB9FDE5E5E5FF3739387757565600FFFFFF03FCFD
          FC01FEFEFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9F8F8F7FFFFFFFFFE9B9D9FFF202122FF7877
          77FFE0E3E5FFFFFFFFFFFEFEFEFFFEFFFFFFFDFEFEFFFEFEFEFFFDFDFDFFF9F9
          F9FFEDEDEDFFD4D3D3FFABAAA9FED5D5D5FCF2F3F3FF4143428658585800FFFF
          FF02FCFCFD01FEFEFE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9F9F9F9FFFDFCFCFEE4E6E8FF373433FF2849
          53FF55A1BBFF92BDCDFFCCD3D7FFEEEAEAFFFDFAF9FFFEFFFFFFFFFFFFFFFBFB
          FBFFF2F2F2FFDDDDDDFFB9B8B8FFB8B8B7FEFFFFFFFBEDEEEEFF424442865656
          5600FFFFFF03FCFCFC01FEFEFE00FFFFFF00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9FAFAFAFFF7F7F7FEFCFAF9FF8BA2ABFF6BB5
          CBFF4FA8C3FF399AB7FF509DB6FF7DA1AFFFA3AFB4FFD0CBC9FFE2E4E4FFF0F0
          EFFFF2F2F2FFE8E8E8FFCECDCDFFB3B3B2FFF1F1F1FEFEFEFFFCF1F2F2FF3839
          387755555500FFFFFF03FCFCFC00FEFEFE00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9FAFAFAFFF5F6F6FEFFFDFCFFCAE0EBFF81B9
          CCFF69B0C6FF46A1BDFF3DA1BFFF34ACD1FF10BAEFFF618C99FF978C88FFAAAE
          AFFFC2C2C2FFD7D8D8FFE0E0E0FFC6C6C5FFB6B6B5FFBDBCBCFEDAD9D9FDE4E5
          E5FF2C2D2C6957575700FFFFFF02FDFDFD00FFFFFF00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9FAFAF9FFF6F6F6FEFDFCFAFFE7ECF0FFA0C6
          D5FF8BC1D2FF6BB8D0FF36616FFF185C72FF09C8FFFF14BEF1FF637C84FF857D
          7AFF909394FFADADACFFD6D6D6FFE4E4E4FFD4D3D3FFC3C2C1FFB8B6B6FEC9C8
          C8FED6D6D4FB1F1F1E5A5F5F5F00FFFFFF01FEFEFE00FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636ECECECF9FAFAF9FFF6F6F6FEF9F9F8FFF7F8F9FFC1D4
          DEFFB5D6E2FF587078FF211A19FF281C18FF186C86FF08CAFFFF1EB9E8FF757F
          82FF8C8786FF989A9BFFB8B8B8FFE1E1E1FFEDEDEDFFE6E6E6FFE1E0E0FFD8D8
          D8FDEBEAE9FFCECCC8E907070728D4D4D400FFFFFF01FDFDFD00FCFCFC00FFFF
          FF02B9B9B90006060636ECECEBF9F9F9F9FFF6F6F6FEF6F7F7FFFFFDFCFFCCDB
          E3FF6CCDECFF277891FF201B19FF262829FF261E1BFF157C9CFF05CAFFFF29B2
          DCFF838382FF949291FFA3A4A4FFC6C6C6FFECECECFFF6F6F6FFF3F3F3FFF3F3
          F3FEF7F7F7FFEBEAEAF808080837B9B9B900FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EBEBEBF9F9F9F8FFF5F5F5FEF6F7F7FFFEFBFAFFDFED
          F2FF42C6EEFF74E5FFFF357183FF211A18FF262727FF252322FF118BB1FF04CA
          FFFF36ACD0FF908886FF9B9B9CFFAEAEAFFFD2D2D2FFF4F4F4FFFCFCFCFFFBFB
          FBFEFFFFFFFFEEEEEDF805050536BABABA00FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EBEBEBF9F8F8F8FFF5F5F5FEF7F7F7FFF6F7F7FFFFFE
          FBFFC1E7F2FF59CDF1FF8AE7FFFF3E6673FF211B19FF272525FF23292BFF0E98
          C3FF04C7FFFF47A8C5FF9C908DFFA3A5A6FFB8B9B9FFDEDEDEFFFAFAFAFFFDFD
          FDFEFFFFFFFFF0F0F0F905050536B9B9B900FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EBEBEBF9F7F7F7FFF4F4F4FEF6F6F6FFF7F7F7FFF5F7
          F8FFFFFEFBFFAFE1F0FF66D4F5FF8BE2FAFF405A63FF201C1BFF282322FF2132
          38FF0BA3D3FF05C4FFFF5AA5BDFFA79995FFABAEAFFFC4C3C3FFE7E7E7FFFBFB
          FBFEFFFFFFFFF0F0F0F905050536B9B9B900FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EBEBEAF9F7F7F6FFF4F4F3FEF5F5F5FFF6F6F6FFF8F8
          F7FFF6F8F8FFFFFFFBFF9EDCEEFF6BD8F8FF87DAF2FF3F4D51FF1F1D1DFF2921
          1FFF1C3D47FF08ACE0FF09C1FAFF6EA5B7FFB0A39FFFB4B7B8FFCDCDCDFFECEC
          ECFEFFFFFFFFF0F0F0F905050536B9B9B900FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EAEAEAF9F6F6F6FFF3F3F2FEF4F4F4FFF6F6F6FFF7F7
          F7FFF8F8F7FFF7F8F9FFFFFDFAFF90D7EDFF6CDBFCFF80D0E7FF3C4142FF1E1F
          20FF291F1CFF174857FF05B3EAFF0FBDF3FF83A8B3FFB7ADAAFFBCC0C0FFD6D6
          D6FEF7F7F7FFF0F0F0F905050536B9B9B900FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EAEAE9F9F5F5F5FFF2F2F2FEF4F4F4FFF5F5F4FFF6F6
          F6FFF7F7F7FFF7F7F8FFF9F9F9FFFCFBF9FF80D2ECFF69DEFFFF79C3D8FF3634
          34FF1F2223FF281D19FF115469FF03B8F1FF19B9EBFF97ABB2FFBDB6B4FFC5C8
          C8FEE4E4E4FFEAEAEAFA06060637B8B8B800FFFFFF02FCFCFC00FCFCFC00FFFF
          FF02B9B9B90006060636EAEAE9F9F5F5F4FFF1F1F1FEF3F3F2FFF4F4F4FFF5F5
          F5FFF6F6F6FFF7F7F7FFF7F7F8FFFBFAFAFFF3F7F7FF6FCDEBFF65DFFFFF75B6
          CAFF312C2AFF212425FF241A17FF0C617CFF01BBF6FF25B7E4FFAAB1B4FFC3BF
          BDFED4D5D6FFDDDCDCFA05050539B5B5B500FFFFFF02FBFBFB00FCFCFC00FFFF
          FF02B9B9B90006060636E9E9E9F9F4F4F3FFF1F1F0FEF2F2F2FFF4F4F3FFF4F4
          F4FFF5F5F5FFF6F6F6FFF7F7F7FFF6F7F7FFFEFBFAFFE7F3F5FF5DC9EBFF60E0
          FFFF6AA2B3FF2B2220FF242727FF1F1816FF086F8FFF00BCF7FF36B5DDFFBAB7
          B7FECDCCCBFFCFD0D0FB0404044042424200FFFFFF01FDFDFD00FCFCFC00FFFF
          FF02B9B9B90006060636E9E9E8F9F3F3F2FFEFEFEFFEF1F1F1FFF2F2F2FFF4F4
          F3FFF4F4F4FFF5F5F5FFF6F6F6FFF7F7F7FFF4F6F7FFFFFCFAFFD8ECF2FF4BC6
          ECFF5CDFFFFF64919EFF261E1BFF262727FF181717FF067FA4FF00BBF5FF4AB5
          D7FECDC4C1FFC4C5C5FB121212490000000183838302FFFFFF00FCFCFC00FFFF
          FF02B9B9B90006060636E8E8E8F9F2F2F1FFEFEFEEFEF1F1F0FFF1F1F1FFF2F2
          F2FFF3F3F3FFF4F4F4FFF5F5F5FFF6F6F5FFF7F6F6FFF4F6F7FFFFFCF9FFC3E5
          EFFF3BC3EEFF57DBFFFF50737DFF221B19FF262525FF10181BFF058DB8FF00B7
          F1FE65BDD7FFCBBFBBFB0E1112510000000B0B0B0B05D1D1D100FCFCFC00FFFF
          FF02B9B9B90006060636E8E8E7F9F1F1F0FFEEEEEDFEF0F0EFFFF1F1F0FFF1F1
          F1FFF2F2F2FFF3F3F3FFF4F4F3FFF5F5F4FFF5F5F5FFF6F6F6FFF3F5F6FFFFFC
          F9FFABDCEBFF2DC3F1FF4FD4FCFF435B63FF221D1BFF24201FFF091E25FF059A
          C9FE03B8F0FF6FB4C9FA160F0D51000001140000000A56565601FCFCFC00FFFF
          FF02B9B9B90006060736E7E7E7F9F0F0EFFFEDEDECFEEEEEEEFFEFEFEFFFF1F1
          F0FFF1F1F1FFF2F2F1FFF3F3F2FFF4F4F3FFF4F4F4FFF5F5F5FFF5F5F5FFF2F5
          F5FFFFFBF7FF90D3E8FF23C4F6FF44C7EEFF344247FF242121FF1F1816FF0428
          33FF04A4D7FE09B6ECFF0740527C000000110003041304040403FCFCFC00FFFF
          FF02B9B9B90007070736E7E7E6F9EFEFEEFFECECEBFEEEEEEDFFEFEFEEFFEFEF
          EFFFF0F0EFFFF1F1F0FFF2F2F1FFF2F2F2FFF3F3F3FFF4F4F3FFF4F4F4FFF4F4
          F4FFF3F5F5FFFFF9F5FF73CAE4FF1AC7FBFF36B5DCFF2C3133FF262625FF180E
          0CFF023647FE06AEE3FF07B2E7F50332404D0000000B0103050CFCFCFC00FFFF
          FF02B9B9B90007070736E6E6E6F9EEEEECFFEBEBEAFEEDEDECFFEDEDEDFFEFEF
          EEFFEFEFEEFFF0F0EFFFF0F0F0FFF1F1F0FFF2F2F1FFF2F2F2FFF3F3F3FFF3F3
          F3FFF3F4F3FFF4F5F5FFFBF5F3FF58C2E3FF14C9FFFF2BA0C4FF282524FF2628
          28FF100604FF00475DFD02B2EAFF07A8DBE702272C3100000002FCFCFC00FFFF
          FF02B9B9B90007070736E6E5E5F9EDECEBFFEAEAE9FEECECEBFFEDEDECFFEDED
          ECFFEEEEEDFFEFEFEEFFF0F0EFFFF0F0F0FFF1F1F0FFF1F1F1FFF2F2F1FFF2F2
          F1FFF3F3F2FFF1F2F2FFF6F5F4FFEFF0EFFF3EBBE2FF11CCFFFF218AAAFF261F
          1CFF242727FF090100FF005975F902BBEAFF0351ABD20100131DFCFCFC00FFFF
          FF02B9B9B90007070736E4E4E4F8EAEAE9FFE8E8E7FDEAEAE9FEEBEBEAFEEBEB
          EBFEECECEBFEEDEDECFEEEEEEDFEEEEEEDFEEFEFEEFEEFEFEFFEF0F0EFFEF0F0
          EFFEF0F0F0FEF1F1F0FEEEF0F0FEF8F5F3FEDFE9EBFE29B7E3FE13CCFFFE1D73
          8EFF281C1AFF201F1DFF040B0EFF012FA5FA0502A2FF0F0F8FC0FCFCFC00FFFF
          FF02B9B9B90009090936E6E6E5F9EDEDEBFFEBEBE9FEEDECEBFFEEEEEDFFEFEF
          EEFFF0F0EFFFF0F0F0FFF1F1F0FFF2F2F1FFF3F3F2FFF3F3F2FFF3F3F3FFF4F4
          F3FFF4F4F4FFF5F5F4FFF5F5F4FFF2F4F4FFFFFAF7FFCFE5EBFF21BCEBFF18CB
          FFFD196073FE272220FF121497FF0501C8FC0404AFFF1313ABB9FCFCFC00FFFF
          FF02B9B9B90005050536D7D7D7F3E1E0E0FADDDDDDF7DFDEDEF8DFDFDEF8DFDF
          DFF8E0E0DFF8E0E0E0F8E1E1E0F8E1E1E1F8E1E1E1F8E1E1E1F8E2E2E1F8E2E2
          E2F8E2E2E2F8E2E2E2F8E2E2E2F8E3E2E2F8DEE1E2F8F2E9E5F8A4C8D4F80EBB
          EEFD1DBCEAFE2E39BFFF2422E5FC191CBCFF0708BCEFA7A7E730FEFEFE00FFFF
          FF00F0F0F0004A4A4A0C1F1F1F26292929262627272626262726262626262626
          2626262626262626262626262626262626262626262626262626262626262626
          26262626262626262626262626262626262626262626242728282C201B1C1C5C
          75694772E7FF564FD7FE2021C8FF2121C5F3B5B5ED46FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FAFAFA00E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8
          E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8
          E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E5E7E702F4ED
          EB00908AE3834243CAFF5F5FD6B7D2D2F52BFFFFFF00FCFCFE03}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000777777002A2A
          2A00363636003434340034343400343434003434340034343400343434003434
          3400343434003434340034343400343434003434340134343402343434023434
          3401343434003434340034343400343434003434340034343400343434003434
          3400343434003434340034343400363636002D2D2D005D5D5D00545454000000
          0000030303000000000000000000000000000000000000000000000000000000
          000000000001000000050000000A0000000D0000000F0000000F0000000F0000
          000F0000000D0000000A00000005000000010000000000000000000000000000
          0000000000000000000000000000020202000000000034343400565656000000
          0000050505000202020002020201030303040303030503030405030304080304
          040E0304041403040415030404120304040E0304040B0304040A0304040A0304
          040B0304040E0304041203040415030404140303040E03030408030304050303
          0305030303050202020102020200040404000000000036363600545454000000
          000003030300000000000101010F0000002F00000042000000440000004B0000
          004C00000046000000400000003F0000003F0000003F0000003F0000003F0000
          003F0000003F0000003F00000040000000460000004C0000004B000000440000
          0042000000330000001300000001020202000000000034343400545454000000
          00000303030000000000050606314D5C63C9596971DE596971DC5D6D75DE6070
          79DA63737CD966767EDA687981DB6B7C84DB6E7E86DB708088DB6F8088DB6D7E
          86DB6B7B83DB687880DB64757DDA617179D95D6E76DA5A6A72DE56666FDC5565
          6EDC4F5E66D1080A0B3F00000001020203000000000034343400545454000000
          0000040405010000000022282B66869FABFF839BA7FD859DA9FF879FACFF8AA2
          AFFF8FA8B4FF96AFBBFF9DB5C1FFA2BAC6FFA4BDC9FFA7BFCBFFA6BFCBFFA5BD
          C9FFA2BAC7FF9FB8C4FF9CB5C1FF99B2BEFF96AEBBFF93ABB7FF90A9B5FF8CA4
          B0FC93ACB8FF2E363A7500000000040404010000000034343400545454000000
          000006060702000000005F676A84E4F7FFFFCDE0E9F9CCDFE9FDC4D8E2FDB9CD
          D8FDABC1CBFD9CB3BEFD90A8B4FD8CA4B0FD8EA6B2FD95ADB8FD99B1BDFD9CB4
          C0FDA1B9C4FDA7BFCAFDAEC6D1FDB5CDD8FDBED5E1FDC5DDE9FDCCE4EFFDCEE6
          F1FAE6FFFFFF5E676A8400000000050606020000000034343400545454000000
          00000606070200000000555B5E7CF4FFFFFFE2F2FBFCE6F7FFFFE5F7FFFFE4F6
          FFFFE4F6FFFFDEF0FAFFCEE2ECFFB6CCD7FF9BB3BFFF879FABFF859CA8FF8EA7
          B3FF95ADB9FF9AB2BEFFA1B9C6FFAAC2CEFFB3CBD7FFBDD5E1FFC6DEEBFFCCE3
          EFFCE3FDFFFF4D55597B00000000050506020000000034343400545454000000
          000005050504000000065B60626FEDFEFFFFDFF0FAFDE1F3FDFFE3F4FDFFE9FA
          FBFFE3F5FFFFE0F2FDFFE1F3FEFFDEF2FEFFD3EAF7FFBDD6E4FF9AB2BFFF7F96
          A1FF7F97A2FF8AA3ADFF92AAB4FF98B0BAFFA0B8C2FFAAC2CCFFB6CED8FFC0D7
          E1FDD5EEF7FF535A5C6F00000006040404040000000034343400545454000000
          0000000000093E3D3D0B7478795DE1F0F5FFDFEEF4FDE9F9F6FFABB8E0FF6570
          CEFFD9E8E7FFE3F3F9FFDFEFF4FFDCEDF2FFD7EAF1FFD3E8F0FFCEE4EEFFB6CC
          D4FF93A5ADFF7E939CFF7F97A2FF8BA4AFFF95ADB8FF9CB4BFFFA7BFCAFFB5CD
          D8FDC3DCE7FF696E715B3F3E3D0D0000000A0000000034343400555555000000
          00010A0A0A11B4B3B30E6A6D6E4DDCECF2FFE0F0FAFEE4F4E9FF717BCCFF0005
          CDFF98A4CBFFDBEAE7FFDEEFF7FFE0F1F6FFDFEFF5FFDBECF3FFD5E8F0FFD2E8
          F2FFCCE4F0FFB9C9D2FF98A6ADFF7A8F99FF7B939FFF8EA6B2FF9FB7C3FFADC5
          D1FEB5CEDAFF595E604FB1B0AF0F0C0C0C120000000135353500575757000000
          00044848481AF2F1F1105E616141D9E9F3FFEBFCFFFFCADAF8FF4953DFFF8490
          EAFF5B66E2FFE2F3F3FFD8E9F5FFE1F3FEFFE2F4FEFFE2F3FDFFE0F2FCFFDCEF
          FAFFD7EBF7FFD2EAF7FFD4E5EEFFC4CCD1FF8E9EA6FF6E848EFF738B97FE93AB
          B6FFA6BDC7FD45484945E6E5E5124D4D4D1A0000000437373700585858000000
          00078F8F8F23FFFFFF1461626236CFDDE2F8F2FFFCFF737DDFFE8F9AE4FFFCFF
          FAFF6670DEFFA7B4EEFFEEFEF7FFDEECF4FFE2F0F5FFE1F0F5FFE1F0F5FFE0EF
          F4FFDEEEF3FFDAEBF1FFD2E6EEFFDDEBF0FFE8ECEEFFBAC5CAFF82959FFE657C
          87FF68808BFD4A4D4F44FBFAF913949494230000000838383800565656000000
          000CC3C3C32CFFFFFF186C6D6D2DC9D7DBF0E8F7FEFFDFEEF6FEE7F7F8FFE3F2
          F7FFD5E3F5FF6E78E1FFD7E6F5FFE7F6F8FFE1F0F7FFE2F1F7FFE2F1F7FFE2F1
          F7FFE1F1F7FFE0F0F6FFDEEEF5FFD5E8F1FFE0EEF7FFF8FAFBFFDFE5E8FEB0BE
          C5FF89A5B5FC5F636641FFFEFE15C7C8C82D0000000C36363600525252000000
          0010E1E1E135FFFFFF1F7E7E7E27C3D1D9E7ECFCFFFFE3F4FDFEE4F5FEFFEAFA
          FDFFEFFFFFFFC3D1F7FF93A0EDFFE7F8FFFFE4F5FEFFE3F4FEFFE4F4FEFFE4F4
          FEFFE3F4FEFFE3F4FEFFE2F3FEFFE1F2FDFFD9ECF8FFE1F0F9FFFEFEFFFEDCEC
          F5FFA2BECEF26F717235FFFFFF1DE4E4E4360000001132323201505050000202
          0213EDEDED3EFFFFFF2792929224BBC7CBDDE9F8FEFFE8F7F5FDACB9E0FF6771
          CEFFD8E6E6FFEDFCFBFFC1CEEEFFB1BEEDFFE6F4F6FFE2F1F5FFE2F0F5FFE2F0
          F5FFE2F0F5FFE2F0F5FFE1F0F5FFE1F0F5FFE0EFF4FFDAEBF2FFDCEAEFFED5E9
          F1FFABBFC7EA84848530FFFFFF25F0F0F03E080808142F2F2F00505050000202
          0214EDEDED46FFFFFF32A8A7A725B8C4C8D3E9F9FFFFE4F2E8FD727BCDFF0005
          CDFF9AA6CBFFDAE8E7FFE6F6F8FFCEDDF4FFC8D6F2FFE3F2F7FFE2F1F7FFE2F1
          F7FFE2F1F7FFE2F1F7FFE2F1F7FFE2F1F7FFE1F0F7FFE0F0F6FFDBECF3FDE0F3
          FAFFB4C4CBE19A9A9A2FFFFFFF31F0F0F046090909142F2F2F00525252000000
          0010E1E1E14BFFFFFF3FBCBBBA29B9C5CCCAF1FFFFFFCDDCF7FD4A53DFFF808B
          E9FF5D67E2FFE3F3F3FFD8E8F5FFE7F8FEFFDDEDFDFFDAEAFDFFE4F5FEFFE4F4
          FEFFE3F4FEFFE3F4FEFFE3F4FEFFE3F4FEFFE2F4FEFFE2F4FEFFE0F2FCFDE7FA
          FFFFBAC9D2D8AEAEAD32FFFFFF3EE2E2E24B0000001132323200565656010000
          000AC3C3C349FFFFFF4ECCCBCB32BAC3C7C1F6FFFFFF727BDEFC8B96E4FFFCFF
          FAFF636DDDFFAAB7EEFFEEFDF7FFDDECF4FFE3F2F6FFE2F1F5FFE2F0F5FFE1F0
          F5FFE1F0F5FFE1F0F5FFE1F0F5FFE1F0F5FFE1F0F5FFE1F0F5FFDFEFF4FDE7F8
          FDFFBDC8CCD0C0BFBF39FFFFFF4CC5C5C54A0000000A36363601585858010000
          00038F8F8F3EFFFFFF5BD9D8D73DC0C9CBBAE9F9FEFFDDECF6FCE7F6F9FFE6F5
          F7FFD3E2F6FF6D77E0FFD9E7F5FFE6F5F8FFE1EFF7FFE2F1F7FFE2F1F7FFE1F1
          F7FFE1F1F7FFE1F1F7FFE1F1F7FFE1F1F7FFE1F1F7FFE1F1F7FFE0F0F6FDE7F8
          FFFFBFCACEC8CECDCD41FFFFFF5A9292923F0000000438383801575757010000
          000047474728FFFFFF62E4E3E248C4CDD2B4E9FBFFFFE4F5FDFCE2F3FEFFE2F2
          FEFFEDFDFFFFC1D0F6FF93A0EEFFE8F9FFFFE3F4FEFFE3F4FEFFE2F4FEFFE3F4
          FEFFE3F4FEFFE2F4FEFFE2F4FEFFE2F4FEFFE2F4FEFFE2F4FEFFE1F3FDFDE6FB
          FFFFC2CED3C3DBD9D94CFFFFFF624A4A4A2A0000000037373701555555000000
          00010606060FD7D7D759F3F2F156C2C9CBB0E4F4FAFFE0EFF4FDE1F0F5FFE1F0
          F5FFDEEEF4FFEAF9F6FFBFCDEEFFB1BFEDFFE4F4F6FFE0F0F5FFE0F0F5FFE0F0
          F5FFE0F0F5FFDFF0F5FFDFF0F5FFDFF0F5FFDFF0F5FFDFEFF5FFDEEEF4FDE3F5
          FBFFBFC8CBBDEAE9E859DADADA590707070F0000000135353500545454000000
          00010000000268696936FBFAF961BFC5C7ABE4F5FBFFE0F0F6FDE1F1F7FFE0F0
          F7FFE0F0F7FFDEEEF7FFE7F8F8FFCCDBF4FFC6D5F1FFE1F1F7FFE0F0F7FFDFF0
          F7FFDFF0F7FFDFF0F7FFDFF0F7FFDFF0F7FFDFF0F7FFDFF0F7FFDEEFF6FDE3F5
          FDFFBBC3C6B8F4F3F2636D6D6E37000000020000000134343400545454000000
          0000030303020303040EBFBDBD50D4DCDFA9DBF3FEFFDDF4FFFDDCF4FFFFDCF3
          FFFFDCF3FFFFDCF3FFFFDAF1FFFFDFF7FFFFD4ECFEFFD2E9FEFFDCF4FFFFDBF3
          FFFFDBF3FFFFDAF3FFFFDAF2FFFFDAF2FFFFDAF2FFFFDAF2FFFFDAF2FEFDDAF4
          FFFFCED8DDB5BBB9B8520506060F010101020000000034343400545454000000
          0000050505010000000334333318ACAFB19C606D73FF667278FD657177FF6571
          77FF657177FF657177FF657177FF647077FF677377FF667277FF657177FF6571
          77FF657177FF647076FF647076FF647076FF647076FF647076FF667277FD5F6B
          72FFA0A5A7A83534341900000004040404010000000034343400545454000000
          00000303030001010104000000002B2A2A67403E3DFF3B3A39FA3C3A39FD3D3B
          3AFE3D3C3BFE3D3B3AFE3D3B3AFE3C3B39FE3C3A3AFE3C3B3AFE3C3B3AFE3C3B
          3AFE3C3A39FE3C3A39FE3C3A39FE3B3938FE3A3837FE393736FD383635F93C3A
          39FF282727760000000001010105020202000000000034343400545454000000
          000003030300010101030000000019191948565757FF525253FD515152FF4748
          48FF484849FF4A4A4BFF494A4AFF494A4AFF4A4A4AFF494A4AFF494A4AFF4949
          4AFF484949FF484849FF484949FF444444FF3F4040FF494949FF4A4A4AFC5151
          51FF1D1D1D590000000001010103020202000000000034343400545454000000
          000003030300010101020000000015151531555555E14E4E4EDB565656E08080
          80EF7C7C7CEC6E6E6EE86D6D6DE86E6E6EE86F6F6FE8707070E9707070E96E6E
          6EE86D6D6DE86C6C6CE86D6D6DE87A7A7AEB7F7E7FEF545454E14C4B4BD95353
          53E61818183F0000000001010103020202000000000034343400545454000000
          0000030303000000000000000000010101010404040403030307050505116161
          6137D7D7D75EFFFFFF6CFEFEFE6DFFFFFF6BFFFFFF69FFFFFF68FFFFFF68FFFF
          FF69FFFFFF6BFEFEFE6DFFFFFF6CD8D8D85F6262623806060611030303070404
          0504010101010000000000000000020202000000000034343400565656000000
          0000060606000303030003030300030303000202020002020200020202000000
          00010808080F4747472B93939345C7C7C756E4E4E45FF0F0F063F0F0F063E5E5
          E55FC8C8C856949494454949492B0808080F0000000102020201020202000202
          0200030303000303030003030300050505000000000037373700545454000000
          0000030303000000000000000000000000000000000100000001000000000000
          00020000000100000000000000030000000A0000001103030316030303160000
          00110000000A0000000300000000000000010000000200000000000000010000
          00010000000000000000000000000202020000000000343434008C8C8C004C4C
          4C00565656005454540054545400545454005454540054545400545454005454
          5400555555005858580159595901565656015252520050505000505050005252
          5200565656015959590158585801555555005454540054545400545454005454
          5400545454005454540054545400555555004E4E4E0077777700}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0002000000100000002400000037000000320000002000000011000000070000
          0002000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000040000
          002100111B7A00283FB4002B44C2001B2DAA00070C7B000000590000003D0000
          0028000000140000000900000001000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000C134B064A
          76D32686C5FF42A2E2FF43A8E8FF3699D7FF207EB7FC115D8DEC063550C90018
          26A800090E80000205600000003E0000002A000000150000000A000101030000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000034064AD2884
          CFFF45A6EBFF4BACEDFF5DA9E0FF50B0F1FF48B4F7FF4AB0EEFF40A1DDFE2F8C
          C4FA166692EC044367DA002135B6000F1A9200010263000000490000002E0000
          001B0000010C0000010400000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000084D77C2318D
          DAFF43A3E9FF4CA7E7FFE08D6FFFDA9781FFAC9598FF889CB4FF68A9D6FF5DB6
          EDFF57BFFCFF53BCF8FF43A8E2FF2F8BBFFF145A80F409405FDB011D2DB2000D
          15920005086B0000004C0000002D000000140000000200000000000000000000
          0000000000000000000000000000000000000000000000000000084D77C2338F
          DAFF42A2E9FF4BA3E5FFE6A992FFFEE7DDFFFAD7CBFFF0BCAAFFDB9886FFBF88
          7CFF978F99FF78A2C3FF5EBCF4FF51B0E6FF448DB6FF428AB1FF317AA0FC2062
          87F30C4464E1022A43C8000C158E000000540000001800000002000000000000
          0000000000000000000000000000000000000000000000000000084D77C23591
          DBFF41A0E7FF49A1E4FFE6AF9AFFFEF6F1FFF6EEE8FFFAF6EFFFFFFAF6FFFEF3
          EBFFF9CFBEFFF3AB94FFDE8D75FFA7776FFF70717CFF607E96FF538EAFFF4F97
          BDFF4D96BEFF478EB4FF20698FFA022942C70000005500000014000000000000
          0000000000000000000000000000000000000000000000000000084E77C23693
          DCFF3E9EE7FF479FE3FFE7B19BFFFFF8F4FF9691CDFF9692CCFFFFFAE4FFFEEF
          DDFFFDEFE3FFFEF1E5FFFCEBE1FFD2BBB3FFB18C83FFA7756AFF946056FF8060
          60FF677687FF568AA9FF50A2CCFF247FB0FA000D168D00000028000000000000
          0000000000000000000000000000000000000000000000000000084E77C23895
          DEFF3D9DE6FF459DE1FFE8B39EFFFFF8F3FFCAC9E0FF303BC6FF9390CCFF3942
          C9FF9792C5FFFFEDD7FFF6DFCBFFC8B3A2FFBEADA0FFBCB3AAFFBBB1ABFFB9A2
          9BFFB38174FFB85A41FF82A4BAFF51BBEDFF002137B10000002F000000000000
          0000000000000000000000000000000000000000000000000000084E77C23A98
          DFFF3C9BE6FF449CE0FFE8B5A0FFFFF8F3FFF9F6EDFF6E73CBFF021BDDFF4E57
          CBFFE3DBD9FFFEDFC9FFF2CFBBFFC0AD9EFFC1A997FFD0B097FFD7B69CFFDDBD
          A3FFE5D7C9FFEFB09DFF8B97A6FF50BBEEFF002338B20000002F000000000000
          0000000000000000000000000000000000000000000000000000084E77C23C9A
          E1FF3B98E4FF4299DFFFE9B7A1FFFFF9F5FF888AD2FF1F31D0FF5B61CAFF464F
          C5FFEEEAE0FFFFDDCAFFD5A089FF39302EFF5B6979FFA0BEDDFFB9C9DBFFD8BA
          AFFFF5BB9EFFF4AE9AFF8999AAFF4FBAEDFF002338B20000002F000000000000
          00000000000000000000000000000000000000000000000000000A4E77C23E9C
          E2FF3A97E4FF4097DEFFE9B9A3FFFFF8F5FFD8D5E7FFCBC9E4FFEEEAEEFF5156
          C6FF9190CAFFFFF5E4FFDED4C7FF515B67FF75B5EAFF73BEFFFF9E889CFFC858
          3FFFD7532FFFE05D37FF86A1B4FF4DBAEDFF002338B20000002F000000000000
          00000000000000000000000000000000000000000000000000000A4E79C23FA0
          E3FF3795E3FF3E95DDFFEABBA6FFFFF8F5FFFFFBF8FFE3EDDCFF89C786FFEFED
          E7FFF0E6E4FFFFF1E6FFF5EADFFF96C1E9FF7ABAF4FFB28F99FFFC8960FFEA75
          51FFD54B29FFD84924FFB27269FF59ABD9FF00223AB300000030000000000000
          00000000000000000000000000000000000000000000000000000B5079C241A1
          E4FF3793E2FF3D92DCFFEABDA7FFFFF8F5FFFDFBF9FF90D398FF2FC349FFD0E5
          CDFFFFF6EDFFFFF2E9FFFDF0E3FFA2CFF7FF9B91AAFFF38864FFF9936EFFF993
          6FFFE46A47FFD54B28FFD84E29FFA76962FF012539B70000003E000000030000
          00000000000000000000000000000000000000000000000000000B5079C244A4
          E5FF3592E1FF3A91DBFFEBC0ABFFFFF9F5FFC5DFC5FF6AC97AFF48D165FF72D0
          81FFFEF8F5FFFEE7DBFFFEE5D6FFBEC3D2FFEAA787FFFBA47EFFF88F6BFFF992
          6EFFF9936FFFE97450FFD44B28FFDB4B25FF7B3220DB0E0503820000002C0000
          00080000000000000000000000000000000000000000000000000B5079C245A6
          E6FF3490E0FF398EDAFFEBC2ADFFFFFAF6FFECF3EBFFEBF2EBFFA9DCB1FF42D2
          66FFD4E6D2FFFEE0D4FFFBB59BFFE99A80FFFBCAA2FFFECAA4FFF89975FFF890
          6CFFF9926EFFF9936FFFE46A46FFD54B28FFD24A28FC822E18D70301006B0000
          002C0000000300000000000000000000000000000000000000000B517AC248A8
          E8FF328EDEFF368CD9FFECC4B0FFFFF9F6FFFFFDFBFFFFFCFAFFFBFBFAFFB3DE
          B9FF70C97FFFE2EDDFFFFFFAF5FFFBE5DAFFF1AC8BFFFCCAA3FFFECAA3FFF9A1
          7DFFF8906CFFF9926EFFF8936FFFE8734FFFD54B28FFD74D2AFF7F2C18D50D04
          027F0000002B00000009000000000000000000000000000000000B507AC249AB
          E8FF318CDDFF3589D8FFECC7B1FFFFF9F5FFFFFDFCFFFCFBFAFFE1ECDEFFFEFB
          FAFFC1DDC1FFC6DBC0FFFFF8F3FFFFF6EFFFF6D8CAFFF1AC8BFFFED2AAFFFECA
          A3FFF89975FFF8906CFFF9926EFFF8936FFFE36945FFD54B28FFD14B27FB7E2A
          15D40100006C000000380000000C0000000000000000000000000B517AC24CAD
          EAFF2F8ADCFF3286D6FFEDC9B3FFFFFAF6FFFFFDFEFFB0DAB4FF37B747FFDFED
          DFFFFFFCFBFFFFFCFAFFFFF7F2FFFFF6EFFFFFF6EEFFF9E1D6FFF1AC8BFFFCCC
          A3FFFEC9A3FFF9A07CFFF8906CFFF9926EFFF8926EFFE8704CFFC8573AFFBA77
          63FF79635BE221201DA6000000430000000D00000000000000000B517AC24DAF
          EBFF2E88DCFF3184D6FFEDCBB5FFFFFAF7FFE2F0E4FF56C66BFF2ECF50FFA1D7
          A8FFFFFDFCFFFFFAF6FFFFFCF9FFFFFAF7FFFFF6EFFFFFF6EFFFF6D6C7FFF1AC
          8BFFFFD1ABFFFEC9A2FFF89874FFF8906CFFFA906CFFDA8E74FFB6A79EFF9798
          ACFF5562BEFF474B95F30605067B0000002D00000003000000000D517AC24FB2
          ECFF2C87DAFF2D82D5FFEECDB8FFFFFBF7FFD2E7D3FFBCDDBFFF7CD48EFF4BD0
          6BFFEFF3EEFFFDDBCFFFFAB69DFFFBC7B4FFFDDDD0FFFDE9DFFFFEF4ECFFF9E1
          D6FFF1AD8CFFFDCBA4FFFEC8A2FFFBA079FFD79982FFC4C2BFFF6C75C5FF2748
          E9FF1344FFFF0939FDFF06106ED20000077A00000027000000060B517AC251B4
          EDFF2B85D9FF2C7FD3FFEECFB9FFFFFBF6FFFEFEFFFFFFFFFFFFE2F0E3FF68D6
          83FF98DAA6FFFBF7F4FFFDEAE2FFFDDBCEFFFAC4AFFFF9B79EFFFAB79EFFFCCD
          B9FFF5D5C8FFF1B191FFFFD4ABFFECBB99FFD4CCC8FF8A90CCFF3E5FF2FF3760
          FEFF2551FFFF1A48FFFF0A34EAFB071069D00000005D000000190D517AC253B6
          EFFF2983D8FF297CD2FFEED1BCFFF7F4F1FFEEEEEEFFFCFBFBFFFFFFFFFFF5F8
          F5FF86C990FFC5DFC6FFFFFDFCFFFFFCF8FFFFFEFAFFFFFBF6FFFEEAE0FFFCD6
          C6FFFFF6F1FFF2C6B9FFE59673FFD5C1AFFFB1B4DEFF5F78EDFF5A7BFFFF4A6E
          FFFF355EFFFF1947FFFF0637FFFF0537FDFF05052F9B000000230E537AC255BA
          F0FF2881D8FF287AD1FFF4D5BBFFDAD7D1FFADABABFFBEBDBCFFD4D2D2FFEEEC
          ECFFF4F9F4FFE9F2E9FFFFFEFEFFFFFCF9FFFFFBF7FFFFF8F4FFFFF8F2FFFFF7
          F1FFFFF9F5FFFBD0BEFF9A8890FFB6B7B4FF8992DFFF7691FBFF6785FFFF597A
          FFFF2954FFFF1443FFFF1442FFFF1544FFFF0506298700000014074569AA53B7
          E9FF3F9DE6FF2076D1FF95A6BBFFB0A69FFFCDCBCBFFDCDCDBFFCAC8C7FFBAB8
          B7FFBAB8B7FFC7C5C5FFDDDBDAFFEAE9E9FFFAF8F6FFFEFBF8FFFFFAF5FFFFF8
          F0FFFFF9F5FFFFD1BEFF7E99B6FF73AACBFF8188D2FE8FA4FCFF7B96FFFF5275
          FFFF2F59FFFF335AFFFF335CFEFF1E33ADDE000002290000000300203256247E
          AFEF5DC3FAFF3B97E3FF1D76D4FF3F78B5FFD5CFCAFFD1D0D0FFD3D2D1FFDBDA
          D9FFDBDAD9FFD7D6D5FFCBC9C8FFBFBEBDFFBDBBB9FFCAC8C5FFE7E4E0FFFCF6
          F1FFFFFEFBFFFFD2BFFF829CB8FF3FA4E3FF234072DD646DC1EC8BA4FFFF5779
          FFFF5073FFFF5377FFFF3F59D6ED0708226C000000070000000000010102001E
          3055136390D73596C8F640A1E0FF3C98DCFFA4AFB7FFEFECEAFFE5E5E4FFC9C9
          C8FFCDCCCAFFD3D2D1FFDBDADAFFE5E4E4FFE7E6E5FFDBDAD9FFC4C2C1FFBBB9
          B7FFC7C7C6FFFAD3C0FF819AB9FF3BA1E5FF002334B00201135A5761BBDA809A
          FBFF7B91F4FC515FBADB0405144F000000060000000000000000000000000000
          0000000F182A00203258074061A111618FD8347BA5FD7C94A1FFBCBFC0FFBBB9
          B8FFBCB9BAFFC2C1C0FFC9C8C7FFCECDCCFFD7D6D5FFDDDDDCFFE4E4E4FFE9EA
          EAFFB9AFACFFF7BB9EFF819DBAFF3A9FE5FF002436B20000002E0605173F3338
          7EA11F22548204040E3500000003000000000000000000000000000000000000
          0000000000000000000000000000000000000010192D00233A65496371C9BCB8
          B5FEA3A1A0FFABA9A7FFB3B2B1FFBBB8B7FFC0BFBEFFD5D4D4FFE6E6E6FFE8E7
          E6FF97989EFFAD9393FF70A2CDFF3A9DE2FF002237AC00000027000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000002D2B2A54BBB9
          B8F1CFCECDFFB5B3B2FFA4A2A1FFA4A3A1FFADABAAFFB1B0AFFFD9D5D4FFDAD2
          CDFF5E93BEFF42A0E9FF44A1E6FF2586C5FF0017257C00000010000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000030302052E2D
          2C5B828080C4B0AFAFE2D7D6D5FCD7D7D6FFCAC7C5FF818B90F10D4261CA1E5F
          87E22887C9FA1D77B1F004466BC1001B2B740000000800000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000C0C0C191B1A1A353A39386D656363A8888684CF3034377F00020422000F
          1835001F3158000D154700040719000000020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000001010100010101000102020001
          0202000102020001020200010202000102020001020200010101000101010000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000052A6D4FF51A5D4FF51A5D3FF52A4D4FF51A4
          D3FF52A4D2FF53A3D2FF53A3D1FF54A2D0FF54A2D0FF56A2CFFF58A1CEFF59A1
          CCFF00000000000000000000000000000000000000005C9FC9FF5D9EC8FF5D9E
          C8FF5E9EC7FF0000000000000000000000000000000000000000000000000000
          000000000000000000000001010150A6D4FFCFEBFAFFBBE3F8FFAEDDF7FFAADA
          F6FFA8D9F7FFA7D8F7FFA7D8F7FFA6D7F7FFAAD8F8FFB5DDF9FFC9E7FBFF58A1
          CEFF00010101000000000000000000000000000000005C9FC9FFD6EDFCFFD5ED
          FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
          000000000000000000000001010150A7D6FFBCE3F8FF90D2F3FF74C6F0FF6BC1
          F0FF69BFF0FF68BDF0FF67BCF0FF67BBF1FF6EBDF2FF8AC9F5FFB5DDF9FF56A1
          CFFF00010101000000000000000000000000000000005B9FCAFFD6EDFCFFD6ED
          FCFF5D9EC8FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA8D6FFB0E0F6FF76C9EFFF50B9ECFF45B2
          EBFF42B0EBFF41AEECFF40ABECFF41AAEDFF4AADEEFF6FBCF3FFA9D7F8FF55A2
          D0FF00010202000000000000000000000000000000005B9FCAFFD6EEFCFFD6ED
          FCFF5D9EC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA8D7FFADDFF6FF6EC6EEFF46B6EBFF3AAF
          E9FF37ACEAFF36AAEAFF34A8EBFF35A6EBFF40A9EDFF67B9F1FFA6D6F7FF54A2
          D0FF00010202000000000000000000000000000000005BA0CBFFD7EEFCFFD6EE
          FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA9D7FFAEDFF5FF6EC7EEFF45B6EAFF38B0
          E9FF36ADE9FF35AAEAFF34A8EAFF34A7EBFF3FAAEDFF67BAF1FFA6D7F7FF54A2
          D1FF00010202000000000000000000000000000000005AA0CBFFD7EEFCFFD7EE
          FCFF5C9FC9FF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024EA9D8FFAEDFF5FF6FC8EEFF46B8EAFF39B1
          E8FF37AEE9FF36ACEAFF35AAEAFF35A8EBFF40ABECFF68BBF1FFA6D7F7FF53A3
          D1FF00010202000000000000000000000000000000005AA0CBFFD8EEFCFFD7EE
          FCFF5B9FCAFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024DA9D8FFAFE0F5FF6FC9EEFF47B9EAFF3AB3
          E8FF38AFE9FF37ADE9FF35ABEAFF35AAEAFF40ACECFF68BCF0FFA6D7F7FF53A3
          D2FF000102020000000000000000000000000000000059A1CCFFD7EEFCFFD8EE
          FCFF5BA0CAFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024DAAD9FFAFE1F5FF70CAEEFF47BAE9FF3AB3
          E8FF38B1E8FF37AFE9FF36ACE9FF36ABEAFF41ADECFF65BBF0FFA2D5F6FF51A3
          D3FF010305050001020200010202000102020102030358A1CDFFD4ECFCFFD5EC
          FCFF5AA0CBFF0000000000000000000000000000000000000000000000000000
          00000000000000000000000102024CAAD9FFB0E1F5FF70CBEEFF48BBE9FF3BB4
          E8FF39B2E8FF38B0E9FF37AEE9FF37ABEAFF3EADEBFF5BB8EFFF90CEF4FF7ABC
          E5FF51A3D3FF53A3D1FF54A2D1FF54A1D1FF54A1D1FF82BCE2FFC0E2FAFFCBE8
          FBFF59A0CCFF0001010100000000000000000000000000000000000000000000
          00000000000000000000000102024CABDAFFB0E1F5FF71CCEDFF49BDE9FF3CB6
          E7FF3AB3E8FF38B1E8FF37AFE9FF37ADE9FF3AACEAFF49B1ECFF6CBFF1FF90CD
          F5FFA2D5F6FFA7D7F7FFA6D6F7FFA5D5F8FFA0D2F7FF98CEF7FF9CCFF7FFB8DE
          FAFF589FCEFF0001010100000000000000000000000000000000000000000000
          00000000000000000000000202024BACDBFFB1E2F5FF72CDEDFF49BEE8FF3CB7
          E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF37ADEAFF3CADEBFF49B0EDFF5BB7
          EFFF65BAF1FF66B9F1FF66B8F2FF64B6F2FF62B4F2FF63B4F2FF78BEF5FFABD7
          F8FF56A0CEFF0001020200000000000000000000000000000000000000000000
          00000000000000000000010202024BACDBFFB1E2F5FF72CDEDFF4ABEE8FF3DB8
          E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37ADE9FF36ACEAFF39ABEAFF3CAA
          ECFF3EA9ECFF3FA8EDFF3EA6EDFF3DA4EEFF3CA3EEFF44A5F0FF67B5F3FFA4D3
          F8FF55A0D0FF0001020200000000000000000000000000000000000000000000
          00000000000000000000010304044AACDCFFACE1F4FF6FCDECFF4ABFE8FF3DB9
          E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AFE9FF36ACE9FF36AAEAFF35A8
          EBFF34A6EBFF33A4ECFF32A2ECFF31A0EDFF329EEDFF3CA2EFFF61B3F2FF9FD1
          F8FF54A0D0FF010204040000000000000000000000000000000048ADDEFF48AC
          DDFF49ACDDFF4AABDCFF4AACDCFF7FC8E8FF9EDDF2FF67CBEBFF47BFE7FF3EBA
          E6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0E9FF37AEE9FF36ABEAFF35A9
          EAFF33A7EBFF32A5EBFF31A2ECFF30A0ECFF309EEDFF38A1EEFF57AFF1FF8FCA
          F6FF7EBBE3FF56A1CEFF59A1CCFF59A1CCFF5AA0CBFF5BA0CBFF010203032F70
          90A58DCBEAFFE7F4FCFFDAF1FAFFBAE6F5FF86D6EFFF59C7E9FF43BFE6FF3EBB
          E6FF3CBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1E8FF37AFE9FF36ADE9FF35AA
          EAFF34A8EAFF33A6EBFF32A4ECFF31A1ECFF319FEDFF34A0EDFF49A8F0FF77BE
          F4FFACD7F8FFCDE8FBFFCDE8F8FF8EC1E0FF2F536A8400000000000000000102
          03032B69889C8ACAEAFFD5EFF9FFB3E4F4FF80D4EDFF57C8E8FF45C0E6FF3FBD
          E6FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B0E8FF37AEE9FF36AC
          EAFF35AAEAFF34A7EBFF33A5EBFF32A3ECFF31A2ECFF36A1EEFF48A9EFFF72BB
          F3FFA6D5F8FFBDDFF6FF8CC1E0FF2D5168810000000000000000000000000000
          0000000000002A64819486CAEAFFBBE6F5FF98DCF1FF6DCFEBFF4EC4E7FF41BF
          E5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38AFE9FF37AD
          E9FF35ABEAFF34A9EAFF33A6EBFF33A5EBFF36A4EDFF44A9EEFF60B5F1FF8BC9
          F5FFAAD6F5FF87BFE2FF2A4F657D000000000000000000000000000000000000
          00000000000000010101265C748783C9E9FFAFE1F3FF8CD8EFFF5FCAE9FF46C0
          E6FF3FBDE5FF3DBBE6FF3CB9E6FF3BB7E7FF3AB5E7FF39B3E8FF38B1E8FF37AF
          E9FF36ACE9FF35AAEAFF35A9EBFF38A7ECFF46ACEEFF61B7F0FF89C9F5FFA4D3
          F4FF85BFE3FF294F647B00010101000000000000000000000000000000000000
          000000000000000000000001010124566E7F81C9E9FFA6E1F2FF6DCFEBFF4AC2
          E6FF40BEE5FF3EBCE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB4E8FF39B2E8FF38B0
          E9FF37AEE9FF37ACEAFF3AABEBFF47B0EDFF63BAF0FF8BCBF4FFA4D4F3FF84C0
          E4FF274D64790001010100000000000000000000000000000000000000000000
          00000000000000000000000000000102030348AEDEFFB2E5F4FF74D2EBFF4DC4
          E7FF41C0E5FF3FBDE5FF3EBBE6FF3DBAE6FF3CB8E7FF3BB6E7FF3AB3E8FF38B1
          E8FF38B0E9FF3BAFEAFF49B2ECFF65BDEFFF8ECDF4FFA4D4F3FF84C1E4FF264C
          6276000101010000000000000000000000000000000000000000000000000000
          00000000000000000000000000000102020247AEDEFFB5E5F4FF77D2ECFF52C7
          E7FF49C3E6FF49C1E6FF47C0E7FF42BDE7FF3DB9E6FF3BB7E7FF3AB5E7FF3AB4
          E8FF3EB2E9FF4BB6EBFF67C0EFFF90D0F3FFA3D6F3FF85C1E5FF24485E700001
          0101000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000102020248AFDEFFBAE7F5FF82D6EDFF64CD
          E9FF65CCEAFF67CCEBFF61C9EAFF51C2E9FF44BDE7FF3EB9E7FF3CB6E7FF40B6
          E9FF4DBAEAFF6AC3EFFF92D2F3FFA4D6F2FF83C1E5FF22475B6D000101010000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000001010147AEDFFFC7EAF7FF9EDFF1FF8FDA
          EFFF98DCF1FF9BDCF1FF91D8F0FF73CEECFF56C3E9FF45BCE7FF43BAE8FF4FBD
          EAFF6CC6EEFF94D4F3FFA4D7F1FF81C2E5FF2145596A00010101000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000001010147AEDFFFDAF1FAFFC9EBF7FFC4EA
          F7FF9CD6EFFF45AAD6F796D4EEFFA0DDF2FF79D0EEFF5DC5EAFF59C2EBFF6FC9
          EDFF96D6F3FFA4D7F1FF80C2E5FF204357670001010100000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000046AEDFFF47AEDFFF47AEDEFF48AE
          DEFF48ADDDFF132F3B4446A6D3F499D5EEFFA6DFF3FF87D4F0FF82D2EFFF9BDA
          F3FFA6D9F2FF4AA7D5FC1E425564000101010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000010101000101010001
          010100010101000101011129343C43A1CDEE98D3EDFFB9E5F6FFB6E3F6FFA9DB
          F1FF4AA7D6FC1E43556400010101000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000101010E232C33429FCBEB9BD3EDFFB5DEF3FF48A9
          D7FC1B3E505D0001010100000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000C1C23294199C5E448A9D7FC1A3B
          4C58000101010000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000009161C211734434E0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000001100000011000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00060000001C00000037000000370000001C0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          001C0000003D008B4BFF008B4BFF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000060000001C0000
          003D008848FF00C382FF00C382FF008848FF0000003D0000001C000000060000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000060000001C0000003D0088
          48FF00BF80FF00DFA0FF00DFA0FF00BF80FF008848FF0000003D0000001C0000
          0006000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000060000001C0000003D008848FF00BD
          7FFF00DC9FFF00D89BFF00D89BFF00DC9FFF00BD7FFF008848FF0000003D0000
          001C000000060000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000060000001C0000003D008948FF00BD81FF00D9
          A0FF00D69CFF00D49AFF00D49AFF00D69CFF00D9A0FF00BD81FF008948FF0000
          003D0000001C0000000600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000060000001C0000003D008948FF00BC81FF00D7A0FF00D4
          9CFF00D29AFF00D29AFF00D29AFF00D29AFF00D49CFF00D7A0FF00BC81FF0089
          48FF0000003D0000001C00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000001C0000003D008948FF00B981FF00D5A0FF00D29CFF00D0
          9AFF00D09AFF00D09AFF00D09AFF00D09AFF00D09AFF00D29CFF00D5A0FF00B9
          81FF008948FF0000003D0000001C000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00060000001C0000003D008948FF00B881FF00D3A0FF00D09CFF00CE9AFF00CE
          9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00CE9AFF00D09CFF00D3
          A0FF00B881FF008948FF0000003D0000001C0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000060000
          001C0000003D008948FF00B780FF00D1A0FF00CE9CFF00CC9AFF00CC9AFF00CC
          9AFF00CC9AFF00CB9AFF00CB9AFF00CC9AFF00CC9AFF00CC9AFF00CC9AFF00CE
          9CFF00D1A0FF00B780FF008948FF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000110000
          0037008948FF00B680FF00CFA0FF00CB9CFF00CA9AFF00CA9AFF00CA9AFF00CA
          9AFF00CA9BFF21E6B0FF21E6B0FF00CA9BFF00CA9AFF00CA9AFF00CA9AFF00CA
          9AFF00CB9CFF00CFA0FF00B680FF008948FF0000003D0000001C000000060000
          000000000000000000000000000000000000000000000000000000000011008B
          4AFF00B581FF00CDA1FF00CA9DFF00C99BFF00C99BFF00C99BFF00C99BFF00CA
          9DFF2BEBB7FF008341FF008341FF2BEBB7FF00CA9DFF00C99BFF00C99BFF00C9
          9BFF00C99BFF00CB9DFF00CEA1FF00B580FF008948FF0000003D0000001C0000
          000600000000000000000000000000000000000000000000000000000006008A
          49FF34F0C1FF00C99EFF00C79BFF00C79BFF00C79BFF00C79BFF00C79DFF34ED
          BDFF008644FF0000000600000006008644FF34EDBDFF00C79DFF00C79BFF00C7
          9BFF00C79BFF00C79BFF00C89DFF00CCA1FF00B482FF008A47FF0000003D0000
          001C000000060000000000000000000000000000000000000000000000000000
          0006008644FF3DEDC1FF00C59CFF00C49BFF00C49BFF00C59CFF3DEDC1FF0086
          43FF00000006000000000000000000000006008643FF3DEDC1FF00C59CFF00C4
          9BFF00C59BFF00C59BFF00C59BFF00C69DFF00C9A1FF00B282FF008A47FF0000
          003D0000001C0000000600000000000000000000000000000000000000000000
          000000000006008643FF45EEC6FF00C29CFF00C29CFF45EEC6FF008643FF0000
          00060000000000000000000000000000000000000006008643FF45EEC6FF00C3
          9CFF00C29BFF00C39BFF00C39BFF00C39BFF00C49DFF00C7A1FF00B182FF008A
          47FF0000003D0000001C00000006000000000000000000000000000000000000
          00000000000000000006008643FF4DF1CBFF4DF1CBFF008643FF000000060000
          0000000000000000000000000000000000000000000000000006008642FF4EF0
          C9FF00C09BFF00C09BFF00C19BFF00C19BFF00C19BFF00C29DFF00C5A1FF00B0
          82FF008A47FF0000003D0000001C000000060000000000000000000000000000
          0000000000000000000000000006008947FF008947FF00000006000000000000
          0000000000000000000000000000000000000000000000000000000000060086
          42FF58F0CFFF00BE9BFF00BE9BFF00BF9BFF00BF9BFF00BF9BFF00C09DFF00C3
          A1FF00AF82FF008A47FF0000003D0000001C0000000600000000000000000000
          0000000000000000000000000000000100010000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0006008642FF61F2D4FF00BB9BFF00BC9AFF00BD9BFF00BD9BFF00BD9BFF00BE
          9DFF00C1A1FF00AE82FF008B47FF0000003D0000001C00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000006008642FF6AF3D8FF00B99CFF00BA9BFF00BB9CFF00BB9CFF00BB
          9CFF00BC9EFF00BEA2FF00AC81FF008B47FF0000003700000011000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000006008641FF73F3DCFF00B79CFF00B89BFF00B99CFF00B9
          9CFF00B99CFF00BA9DFF00BBA1FF00A981FF008C49FF00000011000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000006008641FF7BF5E0FF00B59BFF00B59BFF00B7
          9CFF00B79CFF00B69BFF00B59DFF7DF8E5FF008A48FF00000006000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000006008641FF84F6E5FF00B29BFF00B3
          9AFF00B39AFF00B29BFF84F6E5FF008641FF0000000600000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000006008641FF8EF6E9FF00B2
          9AFF00B29AFF8EF6E9FF008641FF000000060000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000006008640FF95F8
          EFFF95F8EFFF008640FF00000006000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000060089
          46FF008946FF0000000600000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001515
          1527353534631515152700000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000525251998989
          87FF898987FF868684F93232315D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000052525199898987FF8A8A
          88FFB6B6B5FF898987FF898987FF3232315D0000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000052525199898987FF8A8A88FFD4D4
          D3FFFDFDFDFFCFCFCEFF898987FF898987FF3232315D00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000052525199898987FF8A8A88FFD5D5D5FFFDFD
          FDFFFDFDFDFFFDFDFDFFCFCFCEFF898987FF898987FF3232315D000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000052525199898987FF8A8A88FFD5D5D5FFFDFDFDFFFDFD
          FDFFFDFDFDFFFDFDFDFFFDFDFDFFCFCFCEFF898987FF898987FF3232315D0000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000052525199898987FF8A8A88FFD5D5D5FFFDFDFDFFFDFDFDFFFDFD
          FDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFCFCFCEFF898987FF898987FF3232
          315D000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000052525199898987FF8A8A88FFD5D5D5FFFDFDFDFFFDFDFDFFFDFDFDFFF9E6
          DAFFF4C5A6FFF9E6DAFFFDFDFDFFFDFDFDFFFDFDFDFFCFCFCEFF898987FF8989
          87FF3232315D0000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000005252
          5199898987FF8A8A88FFD5D5D5FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD
          80FFF0AD80FFF0AD80FFF8DCCAFFFDFDFDFFFDFDFDFFFDFDFDFFCFCFCEFF8989
          87FF898987FF3232315D00000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000525251998989
          87FF8A8A88FFD5D5D5FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD
          80FFF2C092FFF0AD80FFF0AD80FFF8DDCBFFFDFDFDFFFDFDFDFFFDFDFDFFCFCF
          CEFF898987FF898987FF3232315D000000000000000000000000000000000000
          0000000000000000000000000000000000000000000052525199898987FF8A8A
          88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD80FFF3C1
          93FFF5D4A4FFF2C092FFF0AD80FFF0AD80FFF8DDCBFFFDFDFDFFFDFDFDFFFDFD
          FDFFCFCFCEFF898987FF898987FF3232315D0000000000000000000000000000
          00000000000000000000000000000000000052525199898987FF8A8A88FFD7D7
          D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD80FFF3C193FFF5D4
          A4FFF5D4A4FFF5D4A4FFF2C092FFF0AD80FFF0AD80FFF8DDCBFFFDFDFDFFFDFD
          FDFFFDFDFDFFCFCFCEFF898987FF898987FF3232315D00000000000000000000
          000000000000000000000000000052525199898987FF8A8A88FFD7D7D6FFFDFD
          FDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF5D4
          A4FFF5D4A4FFF5D4A4FFF5D4A4FFF2C091FFF0AD80FFF0AD80FFF8DECCFFFDFD
          FDFFFDFDFDFFFDFDFDFFCFCFCEFF898987FF595957A500000000000000000000
          0000000000000000000052525199898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFD
          FDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4
          A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF2C091FFF0AD80FFF0AD80FFFDFC
          FCFFFDFDFDFFFDFDFDFFE9E9E8FF90908EFF898987FF09090810000000000000
          00000000000052525199898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFD
          FDFFF7D8C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4
          A4FFF5D4A4FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD80FFF0AD80FFF8DECCFFFDFD
          FDFFFDFDFDFFE9E9E8FF939391FF898987FF595957A500000000000000000000
          00005454539C898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8
          C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4
          A4FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFD
          FDFFE9E9E8FF939391FF898987FF5C5C5BAB0000000000000000000000005454
          539C898987FF8C8C8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD
          80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4
          A4FFF5D4A4FFF4CA9BFFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEA
          EAFF939391FF898987FF5C5C5BAB0000000000000000000000005454539C8989
          87FF8C8C8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFFCF5F0FFF0AD80FFF0AD
          80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4
          A4FFF4CA9BFFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF9393
          91FF898987FF5C5C5BAB00000000000000000000000000000000898987FF8C8C
          8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFCF8F6FFF0AD80FFF0AD
          80FFF2BF91FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF4CA
          9BFFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF8989
          87FF5C5C5BAB0101010200000000000000000000000000000000898987FFBBBB
          BAFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFF8E0CFFFF0AD
          80FFF0AD80FFF2BF91FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD
          80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C
          5BAB010101020000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFFDFDFDFFF9F9F9FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF4EB
          E4FFF1D6C4FFF1C0A0FFF2BF91FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD80FFF0AD
          80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB0101
          0102000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFFDFDFDFFF6F6F6FFF2F2F2FFF2F2F2FFF2F2F2FFF2F2F2FFF2F2
          F2FFF2F2F2FFF1CDB6FFF0AD80FFF2BF91FFF4CA9BFFF0AD80FFF0AD80FFF6D0
          B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB010101020000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFC8C8C9FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFC4C4
          C5FFF2F2F2FFF5E9E1FFF0AD80FFF0AD80FFF0AD80FFF0AD80FFF6D0B6FFFDFD
          FDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB01010102000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFF8E1D1FFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFD
          FDFFEAEAEAFF939391FF898987FF5C5C5BAB0101010200000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFFDFDFDFFFCF9F7FFF9E7DBFFFDFDFDFFFDFDFDFFEAEA
          EAFF939391FF898987FF5C5C5BAB010101020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF9494
          92FF898987FF5C5C5BAB01010102000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF9F9F9FFFBFBFBFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF8989
          87FF5D5D5CAE0101010200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3
          A5FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D
          5CAE010101020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFC8C8C9FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFC8C8
          C9FFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D5CAE0101
          0102000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFBABA
          B9FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
          FDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D5CAE010101020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FF8989
          87FF898987FF898987FF898987FF898987FF898987FF898987FF898987FF9D9D
          9CFFA0A09FFFA0A09FFF91918FFF898987FF5D5D5CAE01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001A1A19306969
          67C3898987FF898987FF898987FF898987FF898987FF898987FF898987FF8989
          87FF898987FF898987FF898987FF5A5A59A80101010200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000001515
          15274444446F70706F7BA0A0A0A0B0B3C0C7A2A8C0CFA2A7C0CFB0B3BFC7A0A0
          A0A0636363631313131300000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000052525199B5B5
          B4FFF3F3F3FF9AA4E1FF6272CFFF5C6DCDFF5F70D1FF5C6ECFFF5469CAFF556A
          C8FF8F9DDAFFE6E6E6E65F5F5F5F000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000052525199C5C5C4FFF1F2
          FBFF6B79D3FF6B78D9FF6E79E8FF6D77ECFF6A73EBFF666FEAFF626BE9FF5D67
          E5FF5769D1FF556BC8FFEFF1FAFF828282820000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000052525199B4B4B3FFF2F3FBFF6470
          D1FF7A84EBFF7983EEFF6976DCFF6473D2FF5467C9FF5467C9FF5D6ECFFF5A6A
          D5FF5E68E9FF5863E3FF465FC1FFEFF1F9FF5E5E5E5E00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000052525199929290FFF2F2F1FF7680D9FF818B
          ECFF7F89EEFF6E7AD7FF8A94DDFFE0E3F5FFFEFEFFFFFAFAFDFFDBDFF3FF596D
          CBFF606AE5FF5E67E9FF5863E3FF546AC7FFE1E1E1E113131313000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000052525199898987FFB9B9B8FFA7ACE8FF7C85E1FF8993
          F1FF737ED9FFA9B0E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7C8AD8FF6070
          D6FF656EEAFF5E6AE5FF5E67E9FF5568D1FF8E9DDAFF68686868000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000052525199898987FF8A8A88FFF0F0F0FF797FDBFF8D97F0FF7C85
          E2FF979EE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7F8BD9FF6674D9FF6C76
          EBFF606FD6FF5B6ECBFF596AD5FF5C66E4FF5167C6FFA7A7A7A7000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000052525199898987FF8A8A88FFD5D5D5FFEAEBF9FF7C82DEFF959EF4FF7980
          DDFFE5E6F8FFFFFFFFFFFFFFFFFFFFFFFFFF848EDCFF6C78DBFF747EEDFF6574
          D8FF7B8AD7FFDFE2F5FF5A6CCDFF616AE9FF5468C9FFAFB2C2CC000000000000
          0000000000000000000000000000000000000000000000000000000000005252
          5199898987FF8A8A88FFD5D5D5FFFDFDFDFFDADCF6FF848AE2FF98A2F4FF6F77
          D8FFFDFDFFFFFFFFFFFFFFFFFFFF8891DEFF727DDDFF7C86EFFF6B78DBFF7E8B
          D9FFFFFFFFFFFFFFFFFF4F64C6FF646EEAFF5A6DCEFFA2A9C2D3000000000000
          0000000000000000000000000000000000000000000000000000525251998989
          87FF8A8A88FFD5D5D5FFFDFDFDFFFDFDFDFFDBDCF6FF868CE3FF9CA5F5FF7177
          D9FFFEFEFFFFFFFFFFFF8C93E0FF7883DFFF848EF0FF727DDDFF838EDBFFFFFF
          FFFFFFFFFFFFFFFFFFFF5165C8FF6871EBFF5C6ED0FFA3A9C3D3000000000000
          0000000000000000000000000000000000000000000052525199898987FF8A8A
          88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFE9E3EEFF8287E1FF9FA9F6FF8085
          DFFFE5E6F8FF9197E2FF7E87E2FF8C96F2FF7882DFFF8791DDFFFFFFFFFFFFFF
          FFFFFFFFFFFFE2E5F6FF6071D0FF6B75EBFF5A6DCCFFB1B4C3CC000000000000
          00000000000000000000000000000000000052525199898987FF8A8A88FFD7D7
          D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFFAE2D3FF8386E1FF9EA7F3FF8A8F
          E7FF7E83DEFF848BE4FF949EF3FF7E87E2FF8B93DFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8A95DCFF6674DAFF6C77E9FF5B6DCCFFA6A6A6A6000000000000
          000000000000000000000000000052525199898987FF8A8A88FFD7D7D6FFFDFD
          FDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD80FFF6CEB3FFB0B2ECFF8E93E8FFA2AC
          F6FF99A2F2FF9BA5F5FF838AE3FF9197E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFA7AFE5FF6A78D5FF7781EEFF6775D8FF97A3DFFF67676767000000000000
          0000000000000000000052525199898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFD
          FDFFFDFDFDFFF7D8C4FFF0AD80FFF0AD80FFF4C69BFFFEFAF4FF898BE3FF9FA7
          F3FFA1ABF6FF99A2F2FF7E83DEFFE2E3F7FFFAFAFEFFFEFEFFFFE2E4F7FF8E96
          DFFF707CD8FF7D87EDFF7680E9FF6877D1FFF1F1F1FF1B1B1B22000000000000
          00000000000052525199898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFD
          FDFFF7D8C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF9E4C5FFF4F4FCFF7A7D
          DFFF9EA7F3FFA1ABF6FF8890E7FF7F85DFFF7179D9FF6C74D6FF777FDBFF7984
          E1FF8791F1FF7E89ECFF5D6CCEFFF1F2FBFF949494C500000000000000000000
          00005454539C898987FF8A8A88FFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8
          C4FFF0AD80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFFAEAD2FFF4F4
          FCFF878AE2FF8C91E7FF9CA5F3FF9EA8F5FF9AA4F5FF97A0F4FF939DF3FF8A94
          EFFF7983DFFF727DD6FFF1F2FBFFAEAEADD50000000000000000000000005454
          539C898987FF8C8C8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFF7D8C4FFF0AD
          80FFF0AD80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF9E4
          C5FFFEF9F2FFB0B1ECFF8286E1FF7F84E0FF8489E1FF8188E1FF777FDBFF777F
          DAFFA5ABE6FFF6F6F6FF979797CA0000000000000000000000005454539C8989
          87FF8C8C8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFFCF5F0FFF0AD80FFF0AD
          80FFF3C193FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4
          A4FFF5CEA2FFF6CDB1FFF9E0CFFFECE4EAFFDCDDF6FFDBDDF5FFE9E9F6FFD6D6
          D6FFB6B6B5FF676766B100000000000000000000000000000000898987FF8C8C
          8AFFD7D7D6FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFCF8F6FFF0AD80FFF0AD
          80FFF2BF91FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF4CA
          9BFFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF8989
          87FF5C5C5BAB0101010200000000000000000000000000000000898987FFBBBB
          BAFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFF8E0CFFFF0AD
          80FFF0AD80FFF2BF91FFF5D4A4FFF5D4A4FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD
          80FFF0AD80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C
          5BAB010101020000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFFDFDFDFFF9F9F9FFF6F6F6FFF6F6F6FFF6F6F6FFF6F6F6FFF4EB
          E4FFF1D6C4FFF1C0A0FFF2BF91FFF5D4A4FFF5D4A4FFF4CA9BFFF0AD80FFF0AD
          80FFF6D0B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB0101
          0102000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFFDFDFDFFF6F6F6FFF2F2F2FFF2F2F2FFF2F2F2FFF2F2F2FFF2F2
          F2FFF2F2F2FFF1CDB6FFF0AD80FFF2BF91FFF4CA9BFFF0AD80FFF0AD80FFF6D0
          B6FFFDFDFDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB010101020000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFC8C8C9FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFC4C4
          C5FFF2F2F2FFF5E9E1FFF0AD80FFF0AD80FFF0AD80FFF0AD80FFF6D0B6FFFDFD
          FDFFFDFDFDFFEAEAEAFF939391FF898987FF5C5C5BAB01010102000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFF8E1D1FFF0AD80FFF0AD80FFF6D0B6FFFDFDFDFFFDFD
          FDFFEAEAEAFF939391FF898987FF5C5C5BAB0101010200000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFFDFDFDFFFCF9F7FFF9E7DBFFFDFDFDFFFDFDFDFFEAEA
          EAFF939391FF898987FF5C5C5BAB010101020000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF2F2F2FFF8F8F8FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF9494
          92FF898987FF5C5C5BAB01010102000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFF2F2F2FFF2F2F2FFF2F2F2FFA3A3A5FFA3A3
          A5FFF9F9F9FFFBFBFBFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF8989
          87FF5D5D5CAE0101010200000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3
          A5FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D
          5CAE010101020000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFC0C0
          BFFFFDFDFDFFC8C8C9FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFA3A3A5FFC8C8
          C9FFFDFDFDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D5CAE0101
          0102000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FFBABA
          B9FFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFDFDFFFDFD
          FDFFFDFDFDFFFDFDFDFFEAEAEAFF949492FF898987FF5D5D5CAE010101020000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000898987FF8989
          87FF898987FF898987FF898987FF898987FF898987FF898987FF898987FF9D9D
          9CFFA0A09FFFA0A09FFF91918FFF898987FF5D5D5CAE01010102000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000000000000000000000000000000000000000000001A1A19306969
          67C3898987FF898987FF898987FF898987FF898987FF898987FF898987FF8989
          87FF898987FF898987FF898987FF5A5A59A80101010200000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000001B0000002C0000
          002C000000380000003800000038000000380000003800000038000000380000
          003800000038000000380000003800000038000000380000002C0000002C0000
          001B000000000000000000000000000000000000000000000000000000000000
          000000000000000000000000000000000000000000000000000000000000FED6
          AFFFFED6AFFFFED6AFFFFED6AFFFFED6AFFFFED7B1FFFED7B1FFFED8B2FFFED9
          B4FFFED9B5FFFEDAB7FFFEDAB7FFFEDBB9FFFEDCBAFFFEDCBAFF000000000000
          00000000000000000000000000000000000000000000000000000000000E0000
          001000000013000000140000001600000018000000191212123114141431FED4
          ABFFFED4ABFFFED4ABFFFED4ABFFFED4ADFFFED5ADFFFED5AFFFFED6B0FFFED7
          B2FFFED8B3FFFED8B3FFFED9B5FFFED9B6FFFEDAB8FFFFDBBAFF121212310000
          001900000018000000160000001400000013000000100000000E000000170000
          0030000000344040407FADADADFFADADADFFADADADFFADADADFF646363FFFFCD
          9EFFFFCD9EFFFFCD9EFFFFCEA0FFFFCFA2FFFFD0A4FFFFD2A7FFFFD4ABFFFFD6
          AEFFFFD7B0FFFFD8B2FFFFD9B6FFFFDBB9FFFFDDBDFFFFDDBDFF646464FFADAD
          ADFFADADADFFADADADFF4C4C4C8C000000340000003000000017000000090000
          00174848487AADADADFFCECECEFFCECECEFFCECECEFFCECECEFF333333FFE8BB
          90FFE8BB90FFE8BB91FFE8BC94FFE8BC95FFE8BE98FFE8C09BFFE8C29EFFE8C4
          A1FFE8C4A2FFE8C5A5FFE8C6A8FFE8C8ABFFE8CAAEFFE8CAAEFF333333FFCCCC
          CCFFCCCCCCFFCCCCCCFFADADADFF5F5F5F940000001700000009000000000000
          0000ADADADFFD1D1D1FFD1D1D1FFD1D1D1FFD1D1D1FFD1D1D1FF333333FFC5A0
          7CFFC5A07CFFC5A17FFFC5A280FFC5A282FFC5A484FFC5A688FFC5A789FFC5A7
          8AFFC5A88CFFC5A98FFFC5AA92FFC5AC95FFC5AD97FFC5AD97FF333333FFCCCC
          CCFFCCCCCCFFCCCCCCFFCCCCCCFFADADADFF0000000000000000000000000000
          0000ADADADFFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FFD4D4D4FF343232FF9F84
          74FF9F8474FF9F8474FF9F8474FF9F8575FF9F8575FF9F8575FF9F8576FF9F86
          76FF9F8677FF9F8677FF9F8678FF9F8678FF9F8678FF9F8678FF343232FFCFCF
          CFFFC8CCCAFFCECECEFFCECECEFFADADADFF0000000000000000000000000000
          0000ADADADFFD7D7D7FFD7D7D7FFD7D7D7FFD7D7D7FFD7D7D7FFD6D6D6FFD6D6
          D6FFD6D6D6FFD5D5D5FFD5D5D5FFD5D5D5FFD5D5D5FFD4D4D4FFD4D4D4FFD4D4
          D4FFD4D4D4FFD3D3D3FFD3D3D3FFD3D3D3FFD3D3D3FFD2D2D2FFD2D2D2FFCDD0
          CFFF3AA577FFC5CDCAFFD1D1D1FFADADADFF0000000000000000000000000000
          0000ADADADFFD9D9D9FFD9D9D9FFF0F0F0FFE5ECE9FFDEE9E4FFDEE9E4FFDEE9
          E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9
          E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFDEE9E4FFECEE
          EDFFEAECEBFFD4D4D4FFD4D4D4FFADADADFF0000000000000000000000000000
          0000ADADADFFDCDCDCFFEDEDEDFFB2DCCAFF279C6BFF289C6BFF289C6BFF299C
          6AFF289C6AFF289B6AFF299B6AFF289A6AFF299B69FF299B69FF299B69FF299A
          69FF299A69FF299A69FF299A69FF299A69FF299A69FF299A69FF299A69FF299A
          69FFCBE8DCFFEAEAEAFFD7D7D7FFADADADFF0000000000000000000000000000
          0000A5A5A5ECE0E0E0FFF9FCFBFF26A06EFF4BCCA0FF4BCCA0FF4BCCA0FF4BCC
          A0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCC
          A0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCCA0FF4BCC
          A0FF299A69FFFFFFFFFFDADADAFFA8A8A8F00000000000000000000000000000
          0000A1A1A1E6EAEAEAFFF4FAF7FF24A471FF48CA9EFF24A775FF24A775FF25A5
          73FF25A573FF25A573FF25A573FF25A573FF25A573FF25A573FF25A573FF25A5
          73FF25A573FF25A573FF25A573FF25A573FF25A573FF25A573FF24A775FF48CA
          9EFF289C6CFFFFFFFFFFE5E5E5FFA8A8A8EC0000000000000000000000000000
          0000A4A4A4E3F6F6F6FFF3FAF7FF22A776FF43C99CFF22AF7CFF1FBA86FF1FBA
          86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA
          86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF1FBA86FF22AF7CFF44C9
          9CFF26A06FFFFFFFFFFFF3F3F3FFABABABEC0000000000000000000000000000
          0000A7A7A7E3FFFFFFFFEEFAF5FF1FAD79FF5BE0B7FF27C290FF27C896FF27C8
          96FF27C896FF27C896FF27C896FF27C896FF27C896FF27C896FF27C896FF27C8
          96FF27C896FF27C896FF27C896FF27C896FF27C896FF27C896FF27C18FFF63E6
          BFFF24A371FFFFFFFFFFFFFFFFFFB3B3B3EE0000000000000000000000000000
          0000AAAAAAE0FFFFFFFFE6F8F2FF1DB07DFF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF22A875FFFFFFFFFFFFFFFFFFBABABAEE0000000000000000000000000000
          0000A0A0A0D5FFFFFFFFDDF7EFFF1BB481FF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEEC8FF6DEE
          C8FF1FAC79FFFFFFFFFFFFFFFFFFB4B4B4E70000000000000000000000000000
          0000979797C9FEFEFEFFE6FAF4FF19B884FF6DEEC8FF48DEB1FF4CB588FF4CB5
          88FF4CB588FF4CB588FF4CB587FF4CB487FF4CB487FF4CB488FF4CB488FF4CB5
          88FF4CB588FF4CB589FF4CB588FF4CB488FF4CB488FF44B88AFF32D4A3FF62E8
          C0FF1DB17DFFFFFFFFFFFEFEFEFFACACACDC0000000000000000000000000000
          00007D7D7DA8EFEFEFFFF6FCFAFF17BC87FF5DE7BEFF24C18EFFECBE97FFECBF
          98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF
          98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF98FFECBF98FF25BE8CFF26CE
          9CFF1AB480FFFFFFFFFFF3F3F3FF909090BE0000000000000000000000000000
          00000D0D0D13969696C6C3CDC9FD16BE89FF55C09FFF4AB48FFEF1C9A1FFF3CC
          A4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CC
          A4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CCA4FFF3CBA3FF48B28CFE53BE
          9EFD19B884FFBCBCBCE99D9D9DCE1A1A1A240000000000000000000000000000
          0000000000000000000023232332161616201616162000000000F5D1ACFFF9D8
          B3FFF9D8B3FFF8D8B3FFF9D8B3FFF9D8B2FFF8D8B2FFF9D8B3FFF9D8B3FFF9D8
          B2FFF8D8B2FFF9D8B2FFF9D8B3FFF9D8B3FFF9D8B2FFF7D5B0FF171818231616
          1620161616201111111800000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F4D0ACFFF9D9
          B5FFF9D9B5FFF9D9B5FFF9D9B5FFF9D9B6FFF9D9B6FFF9DAB6FFF9DAB6FFF9D9
          B5FFF9DAB5FFF9DAB5FFF9D9B6FFF9D9B5FFF9D9B5FFF6D4AFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F3CEAAFFF9DA
          B8FFFADBB8FFFADBB8FFF9DAB8FFFADBB8FFFADBB8FFF9DBB8FFF9DBB8FFF9DB
          B9FFF9DBB8FFF9DAB8FFFADBB8FFFADBB8FFF9DBB8FFF5D2AFFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F2CCA9FFF9DC
          BBFFFADDBBFFFADCBBFFFADCBBFFFADDBAFFFADDBBFFF9DCBBFFFADDBBFFF9DC
          BBFFFADCBBFFF9DCBBFFFADCBBFFF9DCBBFFFADCBAFFF4D0AEFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F1CBA7FFFADE
          BEFFFADEBDFFFADEBEFFFADDBDFFFADDBDFFFADDBDFFFADEBEFFFADEBEFFFADD
          BDFFFADDBEFFFADEBDFFFADEBEFFFADDBEFFFADDBEFFF3CFADFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000F0C9A7FFFADF
          C1FFFAE0C0FFFADFC0FFFADFC0FFFBDFC0FFFADFC0FFFBDFC0FFFADFC1FFFADF
          C0FFFBDFC0FFFADFC0FFFADFC0FFFADFC0FFFBDFC0FFF1CCABFF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EFC8A5FFFAE1
          C3FFFAE0C2FFFBE0C3FFFAE0C2FFFAE0C2FFFBE0C3FFFAE0C3FFFAE0C3FFFAE0
          C3FFFAE0C3FFFAE0C2FFFBE0C2FFFAE0C2FFFAE0C3FFF1CBA9FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EEC7A5FFFBE2
          C5FFFBE2C4FFFAE1C5FFFBE1C4FFFBE2C5FFFAE2C4FFFBE1C5FFFAE1C5FFFBE1
          C5FFFBE1C5FFFBE1C4FFFBE1C4FFFBE2C4FFFBE1C5FFEFC8A7FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000EFC7A5FFEFC7
          A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7
          A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A5FFEFC7A6FF000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000}
      end
      item
        Image.Data = {
          36100000424D3610000000000000360000002800000020000000200000000100
          2000000000000010000000000000000000000000000000000000000000000000
          0000000000000000000200000006000000060000000600000006000000060000
          0006000000060000000600000006000000060000000600000006000000060000
          0006000000060000000500000002000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000020000001D1D1D1D4C1D1D1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D
          1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D1D4F1D1D
          1D4F1D1D1D4F1B1B1B4A01010120000000070000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00000000000500000038D9D9D9EBDCDCDCEEDDDDDDEEDEDEDEEEDFDFDFEEDFDF
          DFEEE1E1E1EEE1E1E1EEE2E2E2EEE2E2E2EEE3E3E3EEE2E2E2EEDEDEDEEEDADA
          DAEED8D8D8EECECECEEC5C5C5C9A040404300000000A00000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000003CF2F2F2FCF7F7F6FFF8F8F8FFF9F9F9FFFAFAFAFFFBFB
          FBFFFBFBFBFFFCFCFCFFFCFCFCFFFDFDFDFFFDFDFDFFFDFDFDFFF9F9F9FFF4F4
          F4FFEEEEEEFFE5E5E5FECFCFCFFA6C6C6CAE060606380000000C000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000060000003CF2F2F2FCF7F7F6FFF8F8F8FFF9F9F9FFFAFAFAFFFBFB
          FBFFFBFBFBFFFCFCFCFFFCFCFCFFFDFDFDFFFEFEFEFFFCFCFCFFF9F9F9FFF4F4
          F4FFEEEEEEFFE7E7E7FFD1D0D0FED1D1D1FE7E7E7EC00D0D0D460000000F0000
          0001000000000000000000000000000000000000000000000000000000000000
          0000000000060000003CF2F2F2FCF6F6F6FFF5F5F5FFF6F6F6FFF9F9F9FFF7F7
          F7FFF7F7F7FFFBFBFBFFF8F8F8FFF9F9F9FFFDFDFDFFF9F9F9FFF6F6F6FFF4F4
          F4FFEFEFEFFFE9E9E9FFD8D8D8FFD3D3D2FFD9D9D9FE8C8C8CCD0E0E0E480000
          0010000000000000000000000000000000000000000000000000000000000000
          0000000000060000003CF2F2F2FCF4F4F4FFE2E2E2FFDFDFDFFFF7F7F7FFE2E2
          E2FFE2E2E2FFF9F9F9FFE0E0E0FFE3E3E3FFF8F8F8FFE2E2E2FFE1E1E1FFF3F3
          F3FFEEEEEEFFEDEDEDFFE0E0E0FFD0D0D0FFF0F0F0FFD9D9D9FE878787CA0C0C
          0C450000000E0000000000000000000000000000000000000000000000000000
          0000000000060000003CF2F2F1FCF3F3F3FFDEDEDEFFBFBFBFFFF6F6F6FFDDDD
          DDFFC8C8C8FFF6F6F6FFDCDCDCFFC7C7C7FFF3F3F3FFE0E0E0FFCECECEFFF1F1
          F1FFEEEEEEFFEDEDEDFFE6E6E6FFD0D0D0FFF4F4F4FFF0F0F0FED5D5D5FD7A7A
          7ABD0606063B0000000A00000000000000000000000000000000000000000000
          0000000000060000003CF1F1F1FCF6F6F5FFF5F5F5FFF6F6F6FFF9F9F9FFF8F8
          F8FFF8F8F8FFFBFBFBFFF9F9F9FFFAFAFAFFFCFCFCFFFBFBFBFFFAFAFAFFF9F9
          F9FFF4F4F4FFEFEFEFFFECECECFFD8D8D8FFC4C4C3FFCACACAFFD7D2D4FED4CF
          D1FE6E6D6EB30505053700000007000000000000000000000000000000000000
          0000000000060000003CF1F1F1FCF3F3F3FFE1E1E1FFDFDFDFFFF7F7F7FFE1E1
          E1FFE0E0E0FFF8F8F8FFDFDFDFFFE2E2E2FFF7F7F7FFE1E1E1FFE1E1E1FFF6F6
          F6FFE2E2E2FFDEDEDEFFEFEFEFFFDEDEDEFFDBDBDBFFE2E2E2FFF5BBD8FFF1BE
          D7FFD8D2D5FB5F5F5FA000000022000000020000000000000000000000000000
          0000000000060000003CF1F1F1FCF3F3F3FFE1E1E1FFCDCDCDFFF6F6F6FFE0E0
          E0FFCECECEFFF6F6F6FFDEDEDEFFD1D1D1FFF2F2F2FFE2E2E2FFCECECEFFF4F4
          F4FFE2E2E2FFCACACAFFF3F3F3FFDFDFDFFFCACACAFFE9E9E9FFFBC2DEFFF0BA
          D3FFECE4E8FFE5E5E5F90303033B000000050000000000000000000000000000
          0000000000060000003CF1F1F1FCF4F4F4FFF4F4F4FFF2F2F2FFF8F8F8FFF6F6
          F6FFF5F5F5FFFAFAFAFFF7F7F7FFF6F6F6FFFAFAFAFFF9F9F9FFF7F7F7FFFCFC
          FCFFFAFAFAFFF6F6F6FFF9F9F9FFF5F5F5FFF0F0F0FFF2F2F2FFF3ECEFFFF0EB
          EDFFF1F0F1FFF3F3F3FE0404043F000000060000000000000000000000000000
          0000000000060000003CF1F1F0FCF2F2F1FFDFDFDFFFDCDCDCFFF5F5F5FFDFDF
          DFFFDEDEDEFFF7F7F7FFDDDDDDFFDFDFDFFFF5F5F5FFDFDFDFFFDFDFDFFFF6F6
          F6FFE0E0E0FFDCDCDCFFF9F9F9FFDFDFDFFFDEDEDEFFF6F6F6FFFEBFDEFFFDC4
          E0FFF8F1F4FFF7F7F7FE04040440000000060000000000000000000000000000
          0000000000060000003CF0F0F0FCF2F2F1FFE1E1E1FFCECECEFFF4F4F4FFE1E1
          E1FFD3D3D3FFF5F5F5FFE0E0E0FFD3D3D3FFF2F2F2FFE2E2E2FFD7D7D7FFF5F5
          F5FFE3E3E3FFCBCBCBFFFAFAFAFFE2E2E2FFD3D3D3FFF7F7F7FFFDC5E1FFEFBA
          D2FFF9F2F5FFFAFAFAFE04040440000000070000000000000000000000000000
          0000000000060000003CF0F0EFFCF3F3F2FFF0F0F0FFEDEDEDFFF6F6F6FFF2F2
          F2FFF1F1F1FFF9F9F9FFF4F4F4FFF3F3F3FFF9F9F9FFF6F6F6FFF3F3F3FFFAFA
          FAFFF7F7F7FFF3F3F3FFFCFCFCFFF7F7F7FFF4F4F4FFFCFCFCFFFDF1F7FFFAEF
          F4FFFDFBFCFFFBFBFBFE04040440000000070000000000000000000000000000
          0000000000060000003CEFEFEFFCF0F0F0FFE1E1E1FFDEDEDEFFF3F3F3FFE1E1
          E1FFE1E1E1FFF7F7F7FFDFDFDFFFE2E2E2FFF5F5F5FFE1E1E1FFE1E1E1FFF6F6
          F6FFE2E2E2FFDFDFDFFFF9F9F9FFE2E2E2FFE1E1E1FFFAFAFAFFFEC4E1FFFEC9
          E4FFFDF6FAFFFBFBFBFE04040440000000060000000000000000000000000000
          0000000000060000003CEFEFEEFCF0F0EFFFE1E1E1FFD6D6D6FFF3F3F2FFE1E1
          E1FFD2D2D2FFF3F3F3FFDFDFDFFFD8D8D8FFF1F1F1FFE1E1E1FFCECECEFFF3F3
          F3FFE2E2E2FFD2D2D2FFF9F9F9FFE1E1E1FFD2D2D2FFF8F8F8FFFEC4E1FFF5BF
          D8FFF9F2F5FFFBFBFBFE04040440000000060000000000000000000000000000
          0000000000060000003CEEEEEEFCF1F1F1FFF0F0F0FFEFEFEFFFF4F4F3FFF1F1
          F0FFF0F0F0FFF6F6F6FFF2F2F2FFF2F2F2FFF8F8F8FFF4F4F4FFF2F2F2FFF9F9
          F9FFF5F5F5FFF3F3F3FFFBFBFBFFF6F6F6FFF4F4F4FFFBFBFBFFFCF0F6FFFBEF
          F5FFFBFAFBFFFAFAFAFE04040440000000070000000000000000000000000000
          0000000000060000003CEEEEEDFCF0F0F0FFEEEEEEFFEEEEEEFFF2F2F1FFE1E1
          E1FFE1E1E1FFF4F4F4FFDFDFDFFFE2E2E2FFF4F4F4FFE2E2E2FFE2E2E2FFF4F4
          F4FFE3E3E3FFE0E0E0FFF8F8F8FFE2E2E2FFE2E2E2FFF9F9F9FFFEC6E2FFFECA
          E4FFFBF5F8FFFAFAFAFE04040440000000070000000000000000000000000000
          0000000000060000003CEDEDEDFCEFEFEFFFEEEEEEFFEEEEEEFFF0F0F0FFDFDF
          DEFFD9D9D9FFF1F1F1FFDDDDDDFFD6D6D6FFEEEEEEFFDFDFDFFFD6D6D6FFF0F0
          F0FFE0E0E0FFD0D0D0FFF7F7F7FFE0E0E0FFD5D5D5FFF6F6F6FFFEC0DFFFF5BB
          D7FFF8F0F4FFF9F9F9FE04040440000000060000000000000000000000000000
          0000000000060000003CECECECFCEFEFEEFFF0F0EFFFF1F1F0FFF2F2F1FFF3F3
          F2FFF4F4F3FFF4F4F4FFF5F5F5FFF6F6F6FFF7F7F7FFF7F7F7FFF8F8F8FFF9F9
          F9FFF9F9F9FFF9F9F9FFF9F9F9FFF9F9F9FFFAFAFAFFFAFAFAFFFAFAFAFFFAFA
          FAFFFAFAFAFFF9F9F9FE04040440000000060000000000000000000000000000
          0000000000060000003CD2DFD2FCD3E1D2FFD3E2D3FFD4E4D3FFD5E5D4FFD6E6
          D6FFD8E7D7FFD9E8D9FFDAEADAFFDBEADBFFDBEBDCFFDCECDDFFDDECDEFFDDED
          DEFFDDEDDEFFDDEDDEFFDDEDDEFFDDEDDEFFDDEDDDFFDDEDDDFFDDECDDFFDDEC
          DDFFDDEBDDFFDCEADCFE03040340000000070000000000000000000000000000
          0000000000060000003C1A871AFC198919FF198C19FF199119FF199419FF1B98
          1CFF1E9B21FF209E26FF23A02AFF25A32EFF26A431FF27A533FF27A533FF27A5
          32FF26A430FF24A22CFF229F28FF1F9C23FF209B23FF259B25FF1E951EFF1A8F
          1AFF1A8B1AFF1B8A1BFE00020040000000070000000000000000000000000000
          0000000000060000003C007D00FC007F00FF008400FF008800FF008C02FF0390
          07FF06930DFF0A9713FF0D9B19FF0F9D1EFF11A021FF12A123FF12A123FF11A0
          22FF109E1FFF0E9C1BFF0B9815FF07940FFF149818FF2FA130FF1F9820FF1890
          18FF058305FF007F00FE00020040000000060000000000000000000000000000
          0000000000060000003C007E00FC008200FF008600FF008B01FF038F07FF0693
          0DFF0A9815FF0E9C1CFF12A023FF15A428FF17A62CFF18A72EFF18A72FFF17A7
          2DFF15A429FF13A124FF0F9D1EFF0B9917FF149A1BFF29A12DFF2CA02EFF2C9D
          2CFF0A880AFF008000FE00020040000000060000000000000000000000000000
          0000000000060000003C007F00FC008400FF008900FF018D03FF05920BFF0997
          13FF0E9C1CFF12A124FF16A62BFF19A932FF1CAC37FF1EAE3AFF1EAF3AFF1DAD
          38FF1AAA33FF17A72DFF13A226FF0F9D1EFF0F9A1AFF199B1FFF129615FF078E
          08FF028602FF008100FE00020040000000070000000000000000000000000000
          0000000000060000003C008000FC008600FF008B00FF038F07FF07940FFF0C9A
          18FF11A021FF16A52AFF1AAA33FF1FAF3BFF23B442FF25B646FF25B647FF23B4
          44FF20B13DFF1BAC36FF17A62CFF12A124FF1EA22AFF38AA3FFF28A02CFF1E99
          1FFF068A06FF008200FE00020040000000070000000000000000000000000000
          0000000000060000003C008100FC008700FF008C02FF049109FF099612FF0E9C
          1CFF13A226FF18A830FF1EAF3AFF24B544FF28BA4DFF2BBD53FF2CBE53FF29BB
          4FFF25B647FF20B03DFF1AAA32FF15A428FF19A228FF28A431FF28A12DFF269E
          28FF088C08FF008400FE00020040000000060000000000000000000000000000
          0000000000060000003C008200FC008800FF018D03FF05920BFF0A9814FF0F9E
          1FFF15A429FF1AAA34FF21B240FF28B94CFF2DC056FF32C55FFF33C660FF2FC1
          59FF29BB4FFF23B443FF1CAC37FF16A62CFF119F22FF139C1EFF0F9716FF0C93
          0FFF028A03FF008400FE0002003F000000060000000000000000000000000000
          00000000000500000034007300E6007800E9017B02E905810BE90F8718E9148F
          23E91A952DE9229A37E928A243E930A74FE937AF59E93BB563E93CB765E938B0
          5BE930AB50E92BA445E9249C3AE91C9730E9178F25E9108A1AE907830EE9027C
          05E9007900E9007700E900020037000000050000000000000000000000000000
          00000000000100000014000E0039000E003B000E003B020F023B0512063B0713
          083B09140A3B0A150C3B0C160D3B0D160F3B0E17103B0F18113B0F18113B0F17
          113B0D170F3B0C160E3B0B150C3B09140A3B0813093B0612073B0310033B000F
          003B000E003B000E003900000014000000010000000000000000000000000000
          0000000000000000000000000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000020000000200000002000000020000000200000002000000020000
          0002000000020000000200000000000000000000000000000000}
      end>
  end
  object Sourse_ART: TDataSource
    DataSet = ART_NAME_S
    Left = 80
    Top = 304
  end
  object Sourse_DEP: TDataSource
    DataSet = DEP_NAME_S
    Left = 80
    Top = 350
  end
  object TypeOrderPopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'Type_Out'
      end
      item
        Visible = True
        ItemName = 'Type_In'
      end
      item
        Visible = True
        ItemName = 'Type_Factory'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    OnPopup = TypeOrderPopupMenuPopup
    Left = 48
    Top = 192
  end
  object MenuGrid: TPopupMenu
    Left = 136
    Top = 192
    object N2: TMenuItem
      Caption = #1042#1077#1088#1085#1091#1090#1100' '#1090#1072#1073#1083#1080#1094#1091' '#1055#1086#1091#1084#1086#1083#1095#1072#1085#1080#1102
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1079#1072#1082#1072#1079#1077
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' "'#1040#1088#1090#1080#1082#1091#1083' ('#1076#1083#1103' '#1079#1072#1074#1086#1076#1072')"'
      OnClick = N4Click
    end
    object N6: TMenuItem
      Caption = #1042#1067#1044#1045#1051#1048#1058#1068' '#1047#1040#1050#1040#1047' '#1062#1042#1045#1058#1054#1052
      OnClick = N6Click
    end
  end
  object OfficePopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'Center'
      end
      item
        Visible = True
        ItemName = 'Kursk'
      end
      item
        Visible = True
        ItemName = 'Olimp'
      end
      item
        Visible = True
        ItemName = 'Belgorod'
      end
      item
        Visible = True
        ItemName = 'Vester'
      end
      item
        Visible = True
        ItemName = 'Oskol'
      end
      item
        Visible = True
        ItemName = 'Moll'
      end
      item
        Visible = True
        ItemName = 'Rio'
      end
      item
        Visible = True
        ItemName = 'Orel'
      end
      item
        Visible = True
        ItemName = 'Europe'
      end>
    ItemOptions.Size = misLarge
    UseOwnFont = False
    OnPopup = OfficePopupMenuPopup
    Left = 96
    Top = 192
  end
  object Printer: TdxComponentPrinter
    CurrentLink = PrinterLink1
    Version = 0
    Left = 256
    Top = 320
    object PrinterLink1: TdxGridReportLink
      Active = True
      Component = Grid
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6193
      PrinterPage.Header = 5193
      PrinterPage.Margins.Bottom = 6193
      PrinterPage.Margins.Left = 5192
      PrinterPage.Margins.Right = 5192
      PrinterPage.Margins.Top = 5193
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageFooter.Font.Color = clBlack
      PrinterPage.PageFooter.Font.Height = -16
      PrinterPage.PageFooter.Font.Name = 'Tahoma'
      PrinterPage.PageFooter.Font.Style = []
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clBlack
      PrinterPage.PageHeader.Font.Height = -16
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = []
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ReverseTitlesOnEvenPages = True
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 43091.602620659720000000
      OptionsExpanding.ExpandGroupRows = True
      OptionsExpanding.ExpandMasterRows = True
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsPreview.MaxLineCount = 2
      OptionsView.FilterBar = False
      StyleRepository = cxStyleRepository1
      Styles.BandHeader = cxStyle1
      Styles.Caption = cxStyle2
      Styles.ContentEven = cxStyle7
      Styles.ContentOdd = cxStyle6
      Styles.Header = cxStyle3
      Styles.Preview = cxStyle5
      Styles.Selection = cxStyle4
      BuiltInReportLink = True
    end
  end
  object ART_NAME_S: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      '    d_art.art  '
      'FROM '
      '  d_art;')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 304
    WaitEndMasterScroll = True
    dcForceOpen = True
    object ART_NAME_SART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
  end
  object EMP_DEP_S: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct   D_emp.fio,'
      'D_DEP.name'
      ''
      'from d_dep, D_emp'
      
        'where    (D_emp.d_depid=D_dep.d_depid) and (D_dep.sname=:DEP) an' +
        'd (discharge<>1);')
    BeforeOpen = EMP_DEP_SBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 392
    WaitEndMasterScroll = True
    dcForceOpen = True
    object EMP_DEP_SFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object EMP_DEP_SNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
  end
  object Source_EMP: TDataSource
    DataSet = EMP_DEP_S
    Left = 80
    Top = 392
  end
  object cxPropertiesStore_Grid: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore_Grid'
    Left = 648
    Top = 184
  end
  object DEP_NAME_S: TpFIBDataSet
    SelectSQL.Strings = (
      ' select '
      
        '     d_dep.sname as name from d_dep where (( d_dep.name <>( sele' +
        'ct d_dep.name from d_dep where   upper (d_dep.name)  like upper ' +
        '('#39'%'#1062#1077#1085#1090#1088'%'#39') )) and (d_dep.name <> '#39'$$$$$'#39') and ((STRETRIM (d_dep' +
        '.sname)) <> :main_dep ));'
      ''
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 344
    WaitEndMasterScroll = True
    dcForceOpen = True
    object DEP_NAME_SNAME: TFIBStringField
      FieldName = 'NAME'
      EmptyStrToNull = True
    end
  end
  object ReportDataSet: TfrDBDataSet
    DataSet = dmIndividualOrder.dtOrder
    Left = 88
    Top = 504
  end
  object Report: TfrReport
    Dataset = ReportDataSet
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 128
    Top = 504
    ReportForm = {19000000}
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 768
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = []
      TextColor = clBlack
    end
  end
end
