object fmRestFind: TfmRestFind
  Left = 262
  Top = 217
  BorderStyle = bsDialog
  Caption = #1055#1086#1080#1089#1082' '#1080#1079#1076#1077#1083#1080#1103
  ClientHeight = 242
  ClientWidth = 310
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 50
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 104
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 56
    Width = 76
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 80
    Width = 78
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1087#1088#1080#1093#1086#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 8
    Top = 128
    Width = 44
    Height = 13
    Caption = #1055#1088'. '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 8
    Top = 152
    Width = 49
    Height = 13
    Caption = #1056#1072#1089'. '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 176
    Width = 42
    Height = 13
    Caption = #1054#1089#1090#1072#1090#1086#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edArt: TEdit
    Left = 96
    Top = 4
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 0
    Text = 'edArt'
  end
  object deADate: TDateEdit
    Left = 96
    Top = 52
    Width = 109
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 2
  end
  object edPrice: TEdit
    Left = 96
    Top = 124
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 5
    Text = 'edPrice'
  end
  object edN: TEdit
    Left = 96
    Top = 76
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 3
    Text = 'edN'
  end
  object lcSup: TDBLookupComboBox
    Left = 96
    Top = 100
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_COMPID'
    ListField = 'CODE'
    ListSource = dm.dsSup
    TabOrder = 4
    OnKeyDown = lcSupKeyDown
  end
  object bbFind: TBitBtn
    Tag = 1
    Left = 220
    Top = 8
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080
    TabOrder = 9
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF6000F000050FFFFFFFFFFFF6660FF000050FFF000000FF6666FFF
      000050FF000000000666FFFF000050F00EEEEEE0066FFFFF00005500EEEEEEEE
      00FFFFFF0000500E99E99EEEE00F999F0000500E99999EE9E00F99F90000500E
      99E99E99999F999F0000500E99E99EE9E00F99F90000500EE999EEEEE00F999F
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object bbFindNext: TBitBtn
    Tag = 2
    Left = 220
    Top = 36
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 10
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF2200F000050FFFFFFFFFFFF2222FF000050FFF000000FF2222FFF
      000050FF000000000222FFFF000050F00EEEEEE0022FFFFF00005500EEEEEEEE
      00FF00FF0000500E0E0E000EE00FFFFF0000500E000E000EE00FFFFF0000500E
      0E0E00EEE00FFFFF0000500EE0EE000EE00FFFFF0000500EEEEEEEEEE00FFFFF
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object BitBtn3: TBitBtn
    Left = 220
    Top = 64
    Width = 75
    Height = 25
    TabOrder = 11
    Kind = bkOK
  end
  object edArt2: TEdit
    Left = 96
    Top = 28
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 1
    Text = 'edArt'
  end
  object edPrice2: TEdit
    Left = 96
    Top = 148
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 6
    Text = 'edPrice'
  end
  object cb1: TCheckBox
    Left = 8
    Top = 210
    Width = 143
    Height = 17
    Caption = #1063#1072#1089#1090#1080#1095#1085#1086#1077' '#1089#1088#1072#1074#1085#1077#1085#1080#1077
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 8
  end
  object edTW: TEdit
    Left = 96
    Top = 172
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 7
    Text = 'edPrice'
  end
end
