unit ListInventory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, ActnList, Menus, TB2Item,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmListInventory = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siEdit: TSpeedItem;
    dg1: TDBGridEh;
    acList: TActionList;
    acView: TAction;
    acClose: TAction;
    siPrint: TSpeedItem;
    acPrint: TAction;
    pmprint: TTBPopupMenu;
    ibInventoryB: TTBItem;
    acPrintInventoryB: TAction;
    acPrintInventoryE: TAction;
    acPrintSubscriptionB: TAction;
    acPrintSubscriptionE: TAction;
    ibprord: TTBSubmenuItem;
    sitAct: TTBSubmenuItem;
    iborganisation: TTBItem;
    acExport: TAction;
    siExport: TSpeedItem;
    siFactInv: TTBSubmenuItem;
    ibFactorganisation: TTBItem;
    acDelete: TAction;
    siDelete: TSpeedItem;
    itActOrganization: TTBSubmenuItem;
    ibsubscriptionB: TTBSubmenuItem;
    ibsubscriptionE: TTBSubmenuItem;
    ibsubscriptionBOrg: TTBItem;
    ibsubscriptionEOrg: TTBItem;
    procedure acCloseExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintInventoryBExecute(Sender: TObject);
    procedure acPrintSubscriptionBExecute(Sender: TObject);
    procedure acPrintSubscriptionEExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acExportUpdate(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acDeleteUpdate(Sender: TObject);
  private
    { Private declarations }
    FParentLogId : string;
    procedure pmDepClick(Sender: TObject);
    procedure ActPrintClick(Sender: TObject);
    procedure FactPrintClick(Sender: TObject);             
  public
    { Public declarations }
  end;

var
  fmListInventory: TfmListInventory;

implementation

uses servdata, comdata, inventory, M207Proc, data, JewConst, Data3,
  ReportData, dbUtil, InsRepl, DB, MsgDialog;

{$R *.dfm}

procedure TfmListInventory.FactPrintClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintAfter, FParentLogId);
  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  dmcom.FilterRepDepID:=TTBItem(Sender).Tag;
  ReOpenDataSet(dmserv.quInventory);
  dmReport.PrintDocumentA(VarArrayOf([TTBItem(Sender).Tag]), inventory_e);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmListInventory.acViewExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation('�������� �������������� �� ������ ��������������', FParentLogId);
  dmServ.CurInventory := dmServ.quListInventorySINVID.AsInteger;
  ShowAndFreeForm(TfmInventory, Self, TForm(fmInventory), True, False);
  dmServ.quListInventory.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.pmDepClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintOrder, FParentLogId);
  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  ReOpenDataSet(dmserv.quInventory);
  dmReport.PrintDocumentA(VarArrayOf([(Sender as TComponent).Tag]), inventory_order);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.ActPrintClick(Sender: TObject);
var
  Tag, c, DepId1, DepId2 : integer;
  LogOperationID: string;
begin
  Tag := (Sender as TComponent).Tag;
  // ��������� ����� ����� ���� ��������
  if (Tag=0) then
  begin
    DepId1 := -MAXINT;
    DepId2 := MAXINT;
  end
  else begin
    DepId1 := Tag;
    DepId2 := Tag;
  end;
  if (Sender as TTbItem).Caption = '����.����' then
    dmReport.InventOutPrice := 0
  else
    dmReport.InventOutPrice := 1;
  
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintBefore, FParentLogId);
  if centerdep then
   c := ExecSelectSQL('select count(*) from Inventory inv, Inventory_Head ih where inv.ISIN in (0,2) and '+
     'inv.DepId between '+IntToStr(DepId1)+' and '+IntToStr(DepId2)+' and '+
     'inv.sinvid=ih.sinvid and ih.hid='+inttostr(dmserv.CurInventory), dm.quTmp)
  else
   c := ExecSelectSQL('select count(*) from Inventory where ISIN in (0,2) and '+
     'DepId between '+IntToStr(DepId1)+' and '+IntToStr(DepId2)+' and '+
     'sinvid = '+inttostr(dmServ.CurInventory), dm.quTmp);

  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  ReOpenDataSet(dmserv.quInventory);
  if(c <> 0)then
     dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([Tag]), VarArrayOf([DepId1, DepId2]), VarArrayOf([DepId1, DepId2])]), inventory_act)
  else
    dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([Tag]), VarArrayOf([DepId1, DepId2])]), inventory_act_null);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.FormCreate(Sender: TObject);
var
  pm: TTBItem;
  pmit: TTBSubmenuItem;
  SubPm: TTBItem;
begin
 dm.WorkMode:='INVENTORY';
 tb1.Wallpaper:=wp;
 ReOpenDataSets([dmserv.quListInventory]);
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 FParentLogId := dm3.defineOperationId(dm3.LogUserID);

 if CenterDep then
 begin
  {��������� �������}
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid, wh from d_dep where d_depid<>-1000';
  dm.quTmp.ExecQuery;

  while not (dm.qutmp.Eof) do
  begin
{   if dm.quTmp.Fields[2].AsInteger<>1 then
   begin}
    // ��� ��������
    pm:=TTBItem.Create(ibprord);
    pm.Caption:=dm.quTmp.Fields[0].Asstring;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=pmDepClick;
    ibprord.Add(pm);

    // ��� �����
    if {dm.quTmp.Fields[1].AsInteger = CenterDepId}true then
    begin
      pmit:=TTBSubmenuItem.Create(sitAct);
      pmit.Caption:=dm.quTmp.Fields[0].AsString;
      pmit.Tag:=dm.quTmp.Fields[1].AsInteger;

      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      SubPm :=TTBItem.Create(pmit);
      SubPm.Caption:='����.����';
      SubPm.Tag:=dm.quTmp.Fields[1].AsInteger;
      SubPm.OnClick:=ActPrintClick;
      pmit.Add(SubPm);
      sitAct.Add(pmit);
    end
    else
    begin
      pm:=TTBItem.Create(sitAct);
      pm.Caption:=dm.quTmp.Fields[0].AsString;
      pm.Tag:=dm.quTmp.Fields[1].AsInteger;
      pm.OnClick:=ActPrintClick;
      sitAct.Add(pm);
    end;

    //���������� �� ������
    pm:=TTBItem.Create(siFactInv);
    pm.Caption:=dm.quTmp.Fields[0].AsString;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=FactPrintClick;
    siFactInv.Add(pm);
    //��������
    pm:=TTBItem.Create(ibsubscriptionB);
    pm.Caption:=dm.quTmp.Fields[0].AsString;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=acPrintSubscriptionBExecute;
    ibsubscriptionB.Add(pm);

    pm:=TTBItem.Create(ibsubscriptionE);
    pm.Caption:=dm.quTmp.Fields[0].AsString;
    pm.Tag:=dm.quTmp.Fields[1].AsInteger;
    pm.OnClick:=acPrintSubscriptionEExecute;
    ibsubscriptionE.Add(pm);

//   end;
   dm.quTmp.Next;
  end;

  {dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select c.Name from d_comp c, d_rec r where c.d_compid=r.d_compid';
  dm.quTmp.ExecQuery;}
  iborganisation.Caption:= dmCom.SelfCompName;//dm.quTmp.Fields[0].Asstring;
  iborganisation.OnClick := pmDepClick;

  itActOrganization.Caption := dmCom.SelfCompName;//dm.quTmp.Fields[0].Asstring;

  SubPm :=TTBItem.Create(itActOrganization);
  SubPm.Caption:='����.����';
  SubPm.Tag:= 0;
  SubPm.OnClick:=ActPrintClick;
  itActOrganization.Add(SubPm);
  SubPm :=TTBItem.Create(itActOrganization);
  SubPm.Caption:='����.����';
  SubPm.Tag:=0;
  SubPm.OnClick:=ActPrintClick;
  itActOrganization.Add(SubPm);

  ibFactorganisation.Caption:=dmCom.SelfCompName;
  ibFactorganisation.OnClick :=FactPrintClick;

  ibsubscriptionBOrg.Caption:=dmCom.SelfCompName;
  ibsubscriptionBOrg.OnClick:=acPrintSubscriptionBExecute;
  ibsubscriptionEOrg.Caption:=dmCom.SelfCompName;
  ibsubscriptionEOrg.OnClick:=acPrintSubscriptionEExecute;



  {dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;}
 end
 else
 begin
   iborganisation.Visible:=false;
   itActOrganization.Visible := False;
   ibFactorganisation.Visible := False;
   ibsubscriptionBOrg.Visible:=false;
   ibsubscriptionEOrg.Visible:=false;

   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid='+inttostr(selfdepid);
   dm.quTmp.ExecQuery;

   // ��� ��������
   pm:=TTBItem.Create(ibprord);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;
   ibprord.Add(pm);


   // ��� �����
   pm:=TTBItem.Create(sitAct);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=ActPrintClick;
   sitAct.Add(pm);

   // ���������� �� ������
   pm:=TTBItem.Create(siFactInv);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=FactPrintClick;
   siFactInv.Add(pm);

  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
 end;

 dmserv.FInvIn:='';
 {����������}
  dmcom.D_MatId:='*';
  dmcom.D_GoodId:='*';
  dmcom.D_CompId:=-1;
  dmcom.D_SupId := -1;
  dmcom.D_Att1ID:=ATT1_DICT_ROOT;
  dmcom.D_Att2ID:=ATT2_DICT_ROOT;
  dmcom.D_Note1:=-1;
  dmcom.D_Note2:=-1;
  dm.SD_MatID_I:='';
  dm.SD_GoodID_I:='';
  dm.SD_SupID_I:='';
  dm.SD_CompID_I:='';
  dm.SD_Note1_I:='';
  dm.SD_Note2_I:='';
  dm.SD_Att1_I:='';
  dm.SD_Att2_I:='';
 {**********}
 if not centerdep then siPrint.Visible:=false;
 
end;

procedure TfmListInventory.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmListInventory.acPrintExecute(Sender: TObject);
begin
 //
end;

procedure TfmListInventory.acPrintInventoryBExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintBefore, FParentLogId);
  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  ReOpenDataSet(dmserv.quInventory);
  dmreport.PrintDocumentA(VarArrayOf([dmServ.quListInventoryDEPID.AsINteger]), inventory_b);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.acPrintSubscriptionBExecute(Sender: TObject);
var
  LogOperationID: string;
  DEpID1, DepID2:integer;
begin
  Tag := (Sender as TComponent).Tag;
  // ��������� ����� ����� ���� ��������
  if (Tag=0) then
  begin
    DepId1 := -MAXINT;
    DepId2 := MAXINT;
  end
  else begin
    DepId1 := Tag;
    DepId2 := Tag;
  end;
  LogOperationID := dm3.insert_operation(sLog_InventoryPrintSubsB, FParentLogId);
  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  ReOpenDataSet(dmserv.quInventory);
  dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([DepID1, DepID2])]), subscription_b);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.acPrintSubscriptionEExecute(Sender: TObject);
var
  LogOperationID: string;
  DEpID1, DepID2:integer;
begin
  Tag := (Sender as TComponent).Tag;
  // ��������� ����� ����� ���� ��������
  if (Tag=0) then
  begin
    DepId1 := -MAXINT;
    DepId2 := MAXINT;
  end
  else begin
    DepId1 := Tag;
    DepId2 := Tag;
  end;
  LogOperationID := dm3.insert_operation(sLog_inventoryPrintSubsE, FParentLogId);
  dmserv.CurInventory:=dmServ.quListInventorySINVID.AsInteger;
  ReOpenDataSet(dmserv.quInventory);
  dmReport.PrintDocumentA(VarArrayOf([VarArrayOf([DepID1, DepID2])]),subscription_e);
  CloseDataSet(dmserv.quInventory);
  dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.acExportExecute(Sender: TObject);
var NeedSPR, NeedSPR1, PacketId:integer;
    CodeDep, CodedepC:string;
    flDescription:boolean;
    LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_InventoryExport, FParentLogId);
 with dm do
 begin
  qutmp.SQL.Text:='select r_code from d_dep where WH=1';
  qutmp.ExecQuery;
  CodeDepC:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_code from d_dep where d_depid='+inttostr(SelfDepId);
  qutmp.ExecQuery;
  CodeDep:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_packetid from r_packet where PacketState=0 and r_item='#39
                  +'DESCRIPTION'+#39' and sourceid='#39+CodeDep+#39;
  qutmp.ExecQuery;
  if qutmp.Fields[0].IsNull then  flDescription:=false else flDescription:=true;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
 end;

 NeedSPR1:=0;
 NeedSPR:=0;
 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);
// InsRPacket(dmCom.db, 'INVENTORY', CodeDepC, 1, CodeDep, '��������������', 1,NeedSPR);
  InsRPacket(dmCom.db, 'INVENTORY', CodeDepC, 2,
               dmserv.quListInventorySINVID.AsString, '�������������� �'+ dmserv.quListInventorySN.AsString, 1,NeedSPR, PacketId);

 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);

 MessageDialog('������������ ������ ��� �������� �������������� ���������!', mtInformation, [mbOk], 0); 
 dm3.update_operation(LogOperationID);
end;

procedure TfmListInventory.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dmServ.quListInventoryISCLOSED.AsInteger=1 then Background := clBtnFace
  else Background := clInfoBk;
end;

procedure TfmListInventory.acExportUpdate(Sender: TObject);
begin
  acExport.Visible := not centerDep;
end;

procedure TfmListInventory.acDeleteExecute(Sender: TObject);
var LogOperationID:string;
begin
 if MessageDialog('������� ��������������?', mtInformation, [mbOK, mbCancel], 0)=mrOk then
 begin
  LogOperationID := dm3.insert_operation('�������� ��������������', FParentLogId);
  ExecSQL('execute procedure DELETE_INVENTORY(1, '+dmServ.quListInventorySINVID.AsString+')', dmCom.quTmp);
  ReOpenDataSet(dmserv.quListInventory);
  dm3.update_operation(LogOperationID);
 end
end;

procedure TfmListInventory.acDeleteUpdate(Sender: TObject);
begin
  tAction(Sender).Enabled := dmServ.quListInventoryISCLOSED.AsInteger=0;
end;

end.
