object fmSellEdit: TfmSellEdit
  Left = 239
  Top = 174
  Caption = #1042#1099#1073#1086#1088' '#1080#1079#1076#1077#1083#1080#1103' '#1076#1083#1103' '#1087#1088#1086#1076#1072#1078#1080
  ClientHeight = 554
  ClientWidth = 769
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object DBEdit7: TM207DBEdit
    Left = 228
    Top = -36
    Width = 43
    Height = 21
    Color = clInfoBk
    DataField = 'UNITID'
    DataSource = dm.dsSellItem
    TabOrder = 0
  end
  object pc2: TPageControl
    Left = 0
    Top = 289
    Width = 769
    Height = 265
    ActivePage = tshWH
    Align = alClient
    TabOrder = 1
    TabStop = False
    object tshWH: TTabSheet
      Caption = #1057#1082#1083#1072#1076' '#1080' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082
      object Splitter1: TSplitter
        Left = 52
        Top = 0
        Height = 237
        ExplicitHeight = 230
      end
      object Splitter2: TSplitter
        Left = 188
        Top = 0
        Height = 237
        ExplicitHeight = 230
      end
      object Splitter3: TSplitter
        Left = 251
        Top = 0
        Height = 237
        ExplicitHeight = 230
      end
      object Splitter4: TSplitter
        Left = 319
        Top = 0
        Height = 237
        ExplicitHeight = 230
      end
      object Splitter5: TSplitter
        Left = 120
        Top = 0
        Height = 237
        ExplicitHeight = 230
      end
      object Panel1: TPanel
        Left = 322
        Top = 0
        Width = 439
        Height = 237
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 439
          Height = 33
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label18: TLabel
            Left = 124
            Top = 8
            Width = 41
            Height = 13
            Caption = #1040#1088#1090#1080#1082#1091#1083
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edSearch: TEdit
            Left = 168
            Top = 4
            Width = 77
            Height = 21
            Color = clInfoBk
            TabOrder = 0
            OnChange = edSearchChange
            OnKeyDown = edSearchKeyDown
          end
          object Button1: TButton
            Left = 4
            Top = 4
            Width = 75
            Height = 21
            Caption = #1054#1090#1082#1088#1099#1090#1100
            TabOrder = 1
            OnClick = Button1Click
          end
        end
        object pc1: TPageControl
          Left = 0
          Top = 33
          Width = 439
          Height = 204
          ActivePage = TabSheet1
          Align = alClient
          TabOrder = 1
          OnChange = pc1Change
          object TabSheet1: TTabSheet
            Caption = #1057#1082#1083#1072#1076
            object dgWH: TM207IBGrid
              Left = 0
              Top = 0
              Width = 431
              Height = 176
              Align = alClient
              Color = clBtnFace
              DataSource = dm.dsD_WH
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
              PopupMenu = pm1
              ReadOnly = True
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = siSelectClick
              OnKeyDown = dgWHKeyDown
              IniStorage = FormStorage1
              TitleButtons = True
              MultiShortCut = 0
              ColorShortCut = 0
              InfoShortCut = 0
              ClearHighlight = False
              SortOnTitleClick = True
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PRODCODE'
                  Title.Alignment = taCenter
                  Title.Caption = #1048#1079#1075'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 38
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_MATID'
                  Title.Alignment = taCenter
                  Title.Caption = #1052#1072#1090'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 42
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_GOODID'
                  Title.Alignment = taCenter
                  Title.Caption = #1053#1072#1080#1084'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_INSID'
                  Title.Alignment = taCenter
                  Title.Caption = #1054#1042
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 32
                  Visible = True
                end
                item
                  Color = clInfoBk
                  Expanded = False
                  FieldName = 'ART'
                  Title.Alignment = taCenter
                  Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 134
                  Visible = True
                end
                item
                  Color = clInfoBk
                  Expanded = False
                  FieldName = 'ART2'
                  Title.Alignment = taCenter
                  Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2 '
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 75
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UNITID'
                  Title.Alignment = taCenter
                  Title.Caption = #1045#1048
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 42
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PRICE2'
                  Title.Alignment = taCenter
                  Title.Caption = #1062#1077#1085#1072
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 69
                  Visible = True
                end>
            end
          end
          object tsPrice: TTabSheet
            Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
            ImageIndex = 1
            object dgPrice: TM207IBGrid
              Left = 0
              Top = 0
              Width = 431
              Height = 176
              Align = alClient
              Color = clBtnFace
              DataSource = dm.dsDPrice
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
              PopupMenu = pm1
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnDblClick = siSelectClick
              OnKeyDown = dgWHKeyDown
              IniStorage = FormStorage1
              MultiShortCut = 0
              ColorShortCut = 0
              InfoShortCut = 0
              ClearHighlight = False
              SortOnTitleClick = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PRODCODE'
                  Title.Alignment = taCenter
                  Title.Caption = #1048#1079#1075'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_MATID'
                  Title.Alignment = taCenter
                  Title.Caption = #1052#1072#1090'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_GOODID'
                  Title.Alignment = taCenter
                  Title.Caption = #1053#1072#1080#1084'.'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'D_INSID'
                  Title.Alignment = taCenter
                  Title.Caption = #1054#1042
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 25
                  Visible = True
                end
                item
                  Color = clInfoBk
                  Expanded = False
                  FieldName = 'ART'
                  Title.Alignment = taCenter
                  Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 49
                  Visible = True
                end
                item
                  Color = clInfoBk
                  Expanded = False
                  FieldName = 'ART2'
                  Title.Alignment = taCenter
                  Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'UNITID'
                  Title.Alignment = taCenter
                  Title.Caption = #1045#1048
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PRICE'
                  ReadOnly = True
                  Title.Alignment = taCenter
                  Title.Caption = #1062#1077#1085#1072
                  Title.Font.Charset = DEFAULT_CHARSET
                  Title.Font.Color = clNavy
                  Title.Font.Height = -11
                  Title.Font.Name = 'MS Sans Serif'
                  Title.Font.Style = []
                  Width = 62
                  Visible = True
                end>
            end
          end
        end
      end
      object lbComp: TListBox
        Left = 0
        Top = 0
        Width = 52
        Height = 237
        TabStop = False
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 123
        Top = 0
        Width = 65
        Height = 237
        TabStop = False
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 191
        Top = 0
        Width = 60
        Height = 237
        TabStop = False
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 254
        Top = 0
        Width = 65
        Height = 237
        TabStop = False
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 55
        Top = 0
        Width = 65
        Height = 237
        TabStop = False
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 5
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object tshRet: TTabSheet
      Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 761
        Height = 33
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label19: TLabel
          Left = 4
          Top = 8
          Width = 7
          Height = 13
          Caption = #1057
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label20: TLabel
          Left = 116
          Top = 8
          Width = 14
          Height = 13
          Caption = #1055#1086
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label21: TLabel
          Left = 231
          Top = 8
          Width = 60
          Height = 13
          Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label22: TLabel
          Left = 524
          Top = 8
          Width = 21
          Height = 13
          Caption = #1040#1088#1090'.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 616
          Top = 8
          Width = 52
          Height = 13
          Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object deBD: TDateEdit
          Left = 16
          Top = 4
          Width = 93
          Height = 21
          Color = clInfoBk
          NumGlyphs = 2
          TabOrder = 2
        end
        object deED: TDateEdit
          Left = 132
          Top = 4
          Width = 93
          Height = 21
          Color = clInfoBk
          NumGlyphs = 2
          TabOrder = 3
          OnAcceptDate = deEDAcceptDate
          OnKeyDown = deEDKeyDown
        end
        object bbOpenSelled: TBitBtn
          Left = 452
          Top = 4
          Width = 65
          Height = 21
          Caption = #1054#1090#1082#1088#1099#1090#1100
          TabOrder = 4
          OnClick = bbOpenSelledClick
        end
        object lcRetClient: TDBLookupComboBox
          Left = 296
          Top = 4
          Width = 145
          Height = 21
          Color = clInfoBk
          KeyField = 'CLIENTID'
          ListField = 'NAME'
          ListSource = dm2.dsClient
          TabOrder = 5
          OnKeyPress = lcRetClientKeyPress
        end
        object edSArt: TEdit
          Left = 548
          Top = 4
          Width = 53
          Height = 21
          Color = clInfoBk
          TabOrder = 1
          OnChange = edSArtChange
          OnKeyDown = edSArtKeyDown
        end
        object edUID: TEdit
          Left = 672
          Top = 4
          Width = 53
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          OnChange = edUIDChange
          OnKeyDown = edSArtKeyDown
        end
      end
      object dgRet: TM207IBGrid
        Left = 0
        Top = 33
        Width = 761
        Height = 204
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsSelled
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        PopupMenu = pm1
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = siSelectClick
        OnKeyDown = dgWHKeyDown
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dgRetGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRODCODE'
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'MATID'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'GOODID'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'INSID'
            Title.Alignment = taCenter
            Title.Caption = #1054'.'#1042'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            Title.Alignment = taCenter
            Title.Caption = #1045#1048
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1062#1077#1085#1072' '#1089#1086' '#1089#1082#1080#1076#1082#1086#1081
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SUPCODE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'COST'
            Title.Alignment = taCenter
            Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN0'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1074#1085#1091#1090#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SSF0'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1074#1085#1077#1096
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SPRICE0'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1080#1093#1086#1076'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE0'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1087#1088#1080#1093#1086#1076#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SELLDATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'CLIENT'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE0'
            Title.Alignment = taCenter
            Title.Caption = #1062#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 769
    Height = 289
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Panel5: TPanel
      Left = 433
      Top = 0
      Width = 336
      Height = 289
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object bbOk: TBitBtn
        Left = 44
        Top = 28
        Width = 89
        Height = 25
        Caption = 'OK'
        ModalResult = 1
        TabOrder = 0
        Glyph.Data = {
          DE010000424DDE01000000000000760000002800000024000000120000000100
          0400000000006801000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333330000333333333333333333333333F33333333333
          00003333344333333333333333388F3333333333000033334224333333333333
          338338F3333333330000333422224333333333333833338F3333333300003342
          222224333333333383333338F3333333000034222A22224333333338F338F333
          8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
          33333338F83338F338F33333000033A33333A222433333338333338F338F3333
          0000333333333A222433333333333338F338F33300003333333333A222433333
          333333338F338F33000033333333333A222433333333333338F338F300003333
          33333333A222433333333333338F338F00003333333333333A22433333333333
          3338F38F000033333333333333A223333333333333338F830000333333333333
          333A333333333333333338330000333333333333333333333333333333333333
          0000}
        NumGlyphs = 2
      end
      object bbCnl: TBitBtn
        Left = 44
        Top = 56
        Width = 89
        Height = 25
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 1
        Kind = bkCancel
      end
      object bbAdd: TBitBtn
        Left = 44
        Top = 84
        Width = 89
        Height = 25
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        TabOrder = 2
        Visible = False
        OnClick = bbAddClick
        Glyph.Data = {
          36050000424D3605000000000000360000002800000015000000140000000100
          1800000000000005000000000000000000000000000000000000BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF0000000000
          00000000000000000000BFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
          BFBFBF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00000000FF
          0000FF0000FF000000007F7F7F7F7F7F7F7F7FBFBFBFBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00000000FF
          0000FF0000FF00000000FFFFFF00FFFFFFFFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000FF
          0000FF0000FF000000000000000000000000007F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFF00000000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF000000007F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF000000007F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFF00000000FF0000FF0000FF0000FF
          0000FF0000FF0000FF0000FF0000FF000000007F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000FF
          0000FF0000FF000000000000000000000000007F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00000000FF
          0000FF0000FF00000000FFFFFF00FFFFFFFFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF00000000FF
          0000FF0000FF0000000000FFFFFFFFFF00FFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF0000000000
          00000000000000000000FFFFFF00FFFFFFFFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FF
          FFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFF
          FF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FF
          FFFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F00FFFFFFFFFF00FFFFFFFF
          FF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF7F7F7FBFBFBF00BFBFBFBFBFBF
          7F7F7FFFFFFF00FFFFFFFFFF00FFFFFFFFFF00FFFF7F7F7F7F7F7F7F7F7F7F7F
          7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FBFBFBFBFBFBF00BFBFBFBFBFBF
          BFBFBF7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7FBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00BFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF
          BFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBF00}
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 237
        Width = 336
        Height = 52
        TabStop = False
        Align = alBottom
        Color = clInfoBk
        DataField = 'MEMO'
        DataSource = dm.dsSellItem
        TabOrder = 3
      end
      object btHelp: TBitBtn
        Left = 44
        Top = 120
        Width = 89
        Height = 25
        Caption = #1057#1087#1088#1072#1074#1082#1072
        TabOrder = 4
        OnClick = btHelpClick
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          1800000000000003000000000000000000000000000000000000FF00FFFF00FF
          FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B393939FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
          FFFF7BFFFF007B7B397B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B007B7B007B7B9C9C9CFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD00393900
          7B7B007B7B007B7B003939848484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FF7B7B7B7BFFFF7BFFFF7BFFFF003939397B7B9C9C9CFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
          FFFF7BFFFF007B7B007B7B9C9C9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFBDBDBD7BBDBD7BFFFF7BFFFF7BBDBD003939848484FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF397B7B7B
          FFFF7BFFFF7BFFFF7BBDBD6363639C9C9CFF00FFFF00FFFF00FFFF00FFFF00FF
          393939397B7B397B7B7B7B7BFF00FF397B7B7BFFFF7BFFFF7BFFFF0039396363
          639C9C9CFF00FFFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7B7B7BFF00FFFF
          00FF397B7B7BFFFF7BFFFF397B7B007B7B848484FF00FFFF00FFFF00FFFF00FF
          397B7B7BFFFF7BFFFF397B7BFF00FFBDBDBD39BDBD7BFFFF7BFFFF397B7B00FF
          FF424242BDBDBDFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7BFFFF397B7B39
          BDBD7BFFFF7BFFFF7BFFFF397B7B00FFFF424242BDBDBDFF00FFFF00FFFF00FF
          BDBDBD7BBDBD7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF39BDBD00BDBD00FF
          FF424242BDBDBDFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B7BFFFF7BFFFF7B
          FFFF7BFFFF397B7B00BDBD00FFFF007B7B848484FF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFBDBDBD003939007B7B007B7B007B7B00FFFF00FFFF00BDBD6363
          63DEDEDEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B00
          7B7B007B7B007B7B215A5A7B7B7BDEDEDEFF00FFFF00FFFF00FF}
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 433
      Height = 289
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 29
        Height = 13
        Caption = #1044#1072#1090#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 164
        Top = 8
        Width = 34
        Height = 13
        Caption = #1054#1090#1076#1077#1083':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 6
        Top = 152
        Width = 131
        Height = 13
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100' ('#1076#1080#1089#1082'. '#1082#1072#1088#1090#1072'):'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 194
        Top = 32
        Width = 86
        Height = 13
        Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 228
        Top = 56
        Width = 53
        Height = 13
        Caption = #1040#1088#1090#1080#1082#1091#1083' 2:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 144
        Top = 104
        Width = 29
        Height = 13
        Caption = #1062#1077#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 8
        Top = 56
        Width = 42
        Height = 13
        Caption = #1056#1072#1079#1084#1077#1088':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 8
        Top = 80
        Width = 22
        Height = 13
        Caption = #1042#1077#1089':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 32
        Width = 55
        Height = 13
        Caption = #1048#1076'. '#1085#1086#1084#1077#1088':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 230
        Top = 8
        Width = 53
        Height = 13
        Caption = #1055#1088#1086#1076#1072#1074#1077#1094':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 116
        Top = 80
        Width = 52
        Height = 13
        Caption = #1056#1072#1089'. '#1094#1077#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 258
        Top = 104
        Width = 30
        Height = 13
        Caption = #1048#1090#1086#1075#1086
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 6
        Top = 128
        Width = 40
        Height = 13
        Caption = #8470' '#1095#1077#1082#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBText7: TDBText
        Left = 48
        Top = 128
        Width = 42
        Height = 13
        AutoSize = True
        DataField = 'PRICE0'
      end
      object Label14: TLabel
        Left = 136
        Top = 56
        Width = 39
        Height = 13
        Caption = #1045#1076'.'#1080#1079#1084'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TLabel
        Left = 258
        Top = 80
        Width = 34
        Height = 13
        Caption = #1057#1091#1084#1084#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 6
        Top = 104
        Width = 51
        Height = 13
        Caption = #1057#1082#1080#1076#1082#1072', %'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label17: TLabel
        Left = 134
        Top = 128
        Width = 79
        Height = 13
        Caption = #1055#1088#1080#1084'. '#1082' '#1089#1082#1080#1076#1082#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object laRetDate: TLabel
        Left = 4
        Top = 240
        Width = 79
        Height = 13
        Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object laRetCause: TLabel
        Left = 200
        Top = 238
        Width = 46
        Height = 13
        Caption = #1055#1088#1080#1095#1080#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbRetPrice: TLabel
        Left = 6
        Top = 264
        Width = 87
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1090#1085#1072#1103' '#1094#1077#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label25: TLabel
        Left = 334
        Top = 263
        Width = 73
        Height = 13
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label24: TLabel
        Left = 6
        Top = 176
        Width = 110
        Height = 13
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100' ('#1074#1086#1079#1074#1088#1072#1090')'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label26: TLabel
        Left = 6
        Top = 197
        Width = 92
        Height = 26
        Caption = #1040#1076#1088#1077#1089' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103#13#10'      ('#1074#1086#1079#1074#1088#1072#1090')'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DBEdit1: TM207DBEdit
        Left = 198
        Top = 4
        Width = 27
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'DEP'
        DataSource = dm.dsSellItem
        TabOrder = 0
      end
      object DBEdit3: TM207DBEdit
        Left = 64
        Top = 52
        Width = 65
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'SZ'
        DataSource = dm.dsSellItem
        TabOrder = 1
      end
      object DBEdit4: TM207DBEdit
        Left = 64
        Top = 76
        Width = 45
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'W'
        DataSource = dm.dsSellItem
        TabOrder = 2
      end
      object lcEmp: TDBLookupComboBox
        Left = 284
        Top = 4
        Width = 141
        Height = 21
        Color = clInfoBk
        DataField = 'EMPID'
        DataSource = dm.dsSellItem
        KeyField = 'D_EMPID'
        ListField = 'FIO'
        ListSource = dm.dsEmp
        TabOrder = 3
      end
      object ceDiscount: TRxDBComboEdit
        Left = 64
        Top = 100
        Width = 73
        Height = 21
        TabStop = False
        ButtonHint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1089#1082#1080#1076#1086#1082
        Color = clInfoBk
        DataField = 'DISCOUNT'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          333333333337F33333333333333033333333333333373F333333333333090333
          33333333337F7F33333333333309033333333333337373F33333333330999033
          3333333337F337F33333333330999033333333333733373F3333333309999903
          333333337F33337F33333333099999033333333373333373F333333099999990
          33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333309033333333333337F7F333333333333090333
          33333333337F7F33333333333300033333333333337773333333}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 4
        OnButtonClick = ceDiscountButtonClick
      end
      object DBEdit2: TM207DBEdit
        Left = 64
        Top = 28
        Width = 89
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'UID'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
      object DBEdit5: TM207DBEdit
        Left = 284
        Top = 28
        Width = 141
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'FULLART'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
      end
      object DBEdit6: TM207DBEdit
        Left = 284
        Top = 52
        Width = 141
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'ART2'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object DBEdit8: TM207DBEdit
        Left = 176
        Top = 76
        Width = 73
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'PRICE0'
        DataSource = dm.dsSellItem
        TabOrder = 8
      end
      object DBEdit9: TM207DBEdit
        Left = 296
        Top = 76
        Width = 129
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'Cost0'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
      end
      object edPrice: TM207DBEdit
        Left = 176
        Top = 100
        Width = 73
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'PRICE'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
      end
      object DBEdit11: TM207DBEdit
        Left = 296
        Top = 100
        Width = 129
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'Cost'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
      end
      object DBEdit10: TM207DBEdit
        Left = 220
        Top = 124
        Width = 205
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'DISCMEMO'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 12
      end
      object deRetDate: TDBDateEdit
        Left = 96
        Top = 233
        Width = 97
        Height = 21
        DataField = 'RETDATE'
        DataSource = dm.dsSellItem
        Color = clInfoBk
        NumGlyphs = 2
        TabOrder = 15
      end
      object M207DBEdit1: TM207DBEdit
        Left = 40
        Top = 4
        Width = 113
        Height = 21
        TabStop = False
        Color = clInfoBk
        DataField = 'ADATE'
        DataSource = dm.dsSellItem
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
      end
      object edRetPrice: TDBEdit
        Left = 96
        Top = 260
        Width = 97
        Height = 21
        Color = clInfoBk
        DataField = 'RETPRICE'
        DataSource = dm.dsSellItem
        TabOrder = 16
      end
      object lcbxRetCause: TDBLookupComboboxEh
        Left = 252
        Top = 234
        Width = 174
        Height = 19
        DataField = 'D_RETID'
        DataSource = dm.dsSellItem
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        DropDownBox.AutoDrop = True
        EditButtons = <>
        Flat = True
        KeyField = 'D_RETID'
        ListField = 'RET'
        ListSource = dmCom.dsRet
        Style = csDropDownEh
        TabOrder = 14
        Visible = True
        OnKeyDown = lcbxRetCauseKeyDown
        OnNotInList = lcbxRetCauseNotInList
      end
      object dbehretclient: TDBEditEh
        Left = 144
        Top = 176
        Width = 281
        Height = 21
        DataField = 'RETCLIENT'
        DataSource = dm.dsSellItem
        EditButtons = <>
        TabOrder = 17
        Visible = True
        OnKeyDown = dbehretclientKeyDown
      end
      object ednodcard: TEdit
        Left = 144
        Top = 152
        Width = 105
        Height = 21
        TabOrder = 18
        OnKeyDown = ednodcardKeyDown
        OnKeyPress = ednodcardKeyPress
        OnKeyUp = ednodcardKeyUp
      end
      object edclient: TEdit
        Left = 252
        Top = 152
        Width = 173
        Height = 21
        Enabled = False
        TabOrder = 19
      end
      object dbehRetaddress: TDBEditEh
        Left = 144
        Top = 200
        Width = 281
        Height = 21
        DataField = 'RETADDRESS'
        DataSource = dm.dsSellItem
        EditButtons = <>
        TabOrder = 20
        Visible = True
        OnKeyDown = dbehRetaddressKeyDown
      end
    end
  end
  object pm1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 365
    Top = 422
    object siSelect: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1090#1100
      ImageIndex = 38
      ShortCut = 32
      OnClick = siSelectClick
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 440
    Top = 4
  end
end
