object frDTotal: TfrDTotal
  Left = 0
  Top = 0
  Width = 214
  Height = 49
  TabOrder = 0
  object pa2: TPanel
    Tag = 1
    Left = 0
    Top = 0
    Width = 214
    Height = 49
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object laQC: TLabel
      Left = 4
      Top = 16
      Width = 25
      Height = 13
      Caption = #1050'-'#1074#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object laWC: TLabel
      Left = 4
      Top = 32
      Width = 22
      Height = 13
      Caption = #1042#1077#1089':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtQ: TDBText
      Left = 36
      Top = 20
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Color = clWhite
      DataField = 'QUANTITY'
      ParentColor = False
    end
    object dtW: TDBText
      Left = 36
      Top = 32
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Color = clWhite
      DataField = 'WEIGHT'
      ParentColor = False
    end
    object dtDQ: TDBText
      Left = 92
      Top = 18
      Width = 25
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DQ'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtDW: TDBText
      Left = 89
      Top = 32
      Width = 28
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DW'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 52
      Top = 2
      Width = 30
      Height = 13
      Caption = #1056#1072#1079#1084':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtSZ: TDBText
      Left = 84
      Top = 2
      Width = 23
      Height = 13
      AutoSize = True
      DataField = 'SZ'
    end
    object Bevel1: TBevel
      Left = 120
      Top = 0
      Width = 3
      Height = 49
      Shape = bsLeftLine
    end
    object Bevel2: TBevel
      Left = 0
      Top = 16
      Width = 211
      Height = 5
      Shape = bsTopLine
    end
    object Bevel3: TBevel
      Left = 32
      Top = 0
      Width = 5
      Height = 49
      Shape = bsLeftLine
    end
    object Label2: TLabel
      Left = 150
      Top = 2
      Width = 35
      Height = 13
      Caption = #1042#1057#1045#1043#1054
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtQT: TDBText
      Left = 124
      Top = 20
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Color = clWhite
      DataField = 'QUANTITY'
      ParentColor = False
    end
    object dtWT: TDBText
      Left = 124
      Top = 32
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Color = clWhite
      DataField = 'WEIGHT'
      ParentColor = False
    end
    object dtQDT: TDBText
      Left = 177
      Top = 18
      Width = 32
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DQ'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dtWDT: TDBText
      Left = 174
      Top = 32
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = True
      DataField = 'DW'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Bevel4: TBevel
      Left = 210
      Top = 1
      Width = 3
      Height = 49
      Shape = bsLeftLine
    end
  end
end
