unit RetCash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, dxSkinsCore, dxSkinsdxBarPainter, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinsDefaultPainters, dxBar,
  ActnList, cxClasses, frmPopup, FIBQuery, pFIBQuery, DB, FIBDataSet,
  pFIBDataSet, ImgList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxGridCustomView, cxGrid;

type
  TFmRetCash = class(TForm)
    BarManager: TdxBarManager;
    BarFilter: TdxBar;
    ButtonRange: TdxBarLargeButton;
    ButtonRefresh: TdxBarLargeButton;
    ButtonSettings: TdxBarLargeButton;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonView: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    ButtonOpen: TdxBarLargeButton;
    ButtonPrint: TdxBarLargeButton;
    ButtonDepartment: TdxBarLargeButton;
    ActionList: TActionList;
    acChangeRange: TAction;
    DepartmentsPopupMenu: TdxBarPopupMenu;
    quTemp: TpFIBQuery;
    OrdersList: TpFIBDataSet;
    Image32: TcxImageList;
    OrderSource: TDataSource;
    CASHRETView: TcxGridDBTableView;
    cashRet: TcxGridLevel;
    cxGrid1: TcxGrid;
    OrdersListNAME: TFIBStringField;
    OrdersListS1: TFIBFloatField;
    OrdersListS2: TFIBFloatField;
    OrdersListS3: TFIBFloatField;
    OrdersListS4: TFIBFloatField;
    OrdersListEXCHANGE: TFIBIntegerField;
    CASHRETViewColumn1: TcxGridDBColumn;
    CASHRETViewColumn2: TcxGridDBColumn;
    CASHRETViewColumn3: TcxGridDBColumn;
    CASHRETViewColumn4: TcxGridDBColumn;
    CASHRETViewColumn5: TcxGridDBColumn;
    OrdersListCASHCORR: TFIBIntegerField;
    OrdersListCASHIER: TFIBIntegerField;
    OrdersListRETCLIENT: TFIBStringField;
    OrdersListUID: TFIBIntegerField;
    OrdersListDEPID: TFIBIntegerField;
    CASHRETViewColumn6: TcxGridDBColumn;
    CASHRETViewColumn7: TcxGridDBColumn;
    CASHRETViewColumn8: TcxGridDBColumn;
    CASHRETViewColumn9: TcxGridDBColumn;
    CASHRETViewColumn10: TcxGridDBColumn;
    procedure acChangeRangeExecute(Sender: TObject);
    procedure OnRangeOk(Popup: TPopup);
    procedure ButtonDepartmentClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DepartmentMenuButtonClick(Sender: TObject);
    function  GetSelectedDep: Integer;
    procedure OrdersListAfterDelete(DataSet: TDataSet);
    procedure OrdersListAfterPost(DataSet: TDataSet);
    procedure OrdersListBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    RangeStart: TDate;
    RangeEnd: TDate;
  end;

var
  FmRetCash: TFmRetCash;
  SelfDepID: Integer;
  SelectedDep: Integer;

implementation
   uses frmBuyRangePopup, ComData;
{$R *.dfm}

procedure TFmRetCash.ButtonDepartmentClick(Sender: TObject);
var
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
  Button: TdxBarLargeButton;
begin
  if Sender is TdxBarLargeButton then
  begin
    Button := TdxBarLargeButton(Sender);

    if Button = ButtonDepartment then
    begin
      GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

      Y := BarWindowRect.Bottom + 4;

      ButtonRect := ButtonDepartment.ClickItemLink.ItemRect;

      X := BarWindowRect.Left + ButtonRect.Left;

      DepartmentsPopupMenu.Popup(X, Y);
    end;
  end;

end;

function  TFmRetCash.GetSelectedDep: Integer;
begin
  Result := ButtonDepartment.Tag;
end;
procedure TFmRetCash.DepartmentMenuButtonClick(Sender: TObject);
var Button: TdxBarButton;
begin
  if Sender is TdxBarButton then
    begin
      Button := TdxBarButton(Sender);
      ButtonDepartment.Caption := Button.Caption;
      ButtonDepartment.Tag := Button.Tag;
      SelectedDep := GetSelectedDep;

      if not SelfDepID=1 then
      begin
        Exit;
      end;


      with OrdersList do
        begin
          DisableControls;
          DisableScrollEvents;

          if Filtered then
            begin
              Filtered := false;
            end;

          if ButtonDepartment.Tag <> -1000 then
            begin
              Filter := 'DepID = ' + IntToStr(ButtonDepartment.Tag);
              Filtered := true;
            end;

          EnableControls;
          EnableScrollEvents;
        end;
    end;
end;


procedure TFmRetCash.FormCreate(Sender: TObject);
var
  Button: TdxBarButton;
  Button2: TdxBarButton;
  ButtonLink: TdxBarItemLink;
  TextSQL: String;
begin

  RangeEnd:=now;

  RangeStart:= ( strtodate ( (FormatDateTime('dd/mm',RangeEnd)) + (FormatDateTime('/yyyy', Date)) )        - 30);

with quTemp do
    begin
      Close;
      SQL.Text := 'select gen_id(selfdepid,0) from rdb$database';
      ExecQuery;
      SelfDepID := Fields[0].AsInteger;
    end;

    
  with quTemp do
    begin
      Close;

      TextSQL := 'select d_depid, coalesce(sname,' + #39 + '  ���  ' + #39 + ') from d_dep where d_depid <> 1 and d_depid<>26';

      if not SelfDepID=1 then
      begin
        TextSQL := TextSQL + ' and d_depid = ' + IntToStr(SelfDepID);
      end;

      SQL.Text := TextSQL;

      ExecQuery;

      while not EOF do
        begin
          Button := TdxBarButton(BarManager.AddItem(TdxBarButton));

          Button.ButtonStyle := bsChecked;

          Button.GroupIndex := 6;

          Button.LargeImageIndex := ButtonDepartment.LargeImageIndex;

          Button.Tag := Fields[0].AsInteger;

          Button.Caption := trim(Fields[1].AsString);

          Button.OnClick := DepartmentMenuButtonClick;

          ButtonLink := DepartmentsPopupMenu.ItemLinks.Add;

          ButtonLink.Item := Button;

          if DepartmentsPopupMenu.Itemlinks.Count = 2 then
          begin
            ButtonLink.BeginGroup := True;
          end;

          if SelfDepID=1 then
            begin
              if Button.Tag = -1000 then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end
          else
            begin
              if (Button.Tag = SelfDepID) then //or(SelfDepID=1 and Button.Tag =-1000) then
                begin
                  Button.DoClick;
                  Button.Down := true;
                end;
            end;

          Next;
        end;
    end;

  //ButtonDepartment.
 OrdersList.Active:=true;
end;

procedure TFmRetCash.OnRangeOk(Popup: TPopup);
var
  Dialog: TDialogBuyRangePopup;
begin
  Dialog := TDialogBuyRangePopup(Popup);

  RangeStart := Dialog.RangeBegin;

  RangeEnd := Dialog.RangeEnd;

 // acRefresh.Execute;

  //RepaintBandCaption(GridView.Bands[0]);

  //GridView.DataController.SelectAll
end;

procedure TFmRetCash.OrdersListAfterDelete(DataSet: TDataSet);
begin
if DataSet is TpFIBDataSet then
  with tpFIBDataSet(DataSet).Transaction do
    begin
      if Active then
        CommitRetaining;
    end;
end;

procedure TFmRetCash.OrdersListAfterPost(DataSet: TDataSet);
begin
if DataSet is TpFIBDataSet then
  with tpFIBDataSet(DataSet).Transaction do
    begin
      if Active then
        CommitRetaining;
    end;
end;

procedure TFmRetCash.OrdersListBeforeOpen(DataSet: TDataSet);
begin
  OrdersList.ParamByName('BD').AsDateTime := RangeStart;
  OrdersList.ParamByName('ED').AsDateTime := RangeEnd;
end;

procedure TFmRetCash.acChangeRangeExecute(Sender: TObject);
var
  Dialog: TDialogBuyRangePopup;
  BarWindowRect: TRect;
  ButtonRect: TRect;
  X, Y: Integer;
begin
  GetWindowRect(BarFilter.Control.Handle, BarWindowRect);

  Y := BarWindowRect.Bottom + 4;

  ButtonRect := ButtonRange.ClickItemLink.ItemRect;

  X := BarWindowRect.Left + ButtonRect.Left;

  Dialog := TDialogBuyRangePopup.Create(Self);

  Dialog.RangeBegin := RangeStart;

  Dialog.RangeEnd := RangeEnd;

  Dialog.OnOk := OnRangeOk;

  Dialog.Popup(X, Y);

end;

end.
