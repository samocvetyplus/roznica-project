object fmORet: TfmORet
  Left = 222
  Top = 132
  HelpContext = 100262
  Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1086#1087#1090#1086#1074#1099#1093' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
  ClientHeight = 684
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 93
    Width = 843
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter6: TSplitter
    Left = 0
    Top = 422
    Width = 843
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 427
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 665
    Width = 843
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 843
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      ImageIndex = 0
      Spacing = 1
      Left = 523
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00000000FF0000FF00000000FFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        000000000000FF0000FF000000000000000000007B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFF00000000FF0000FF0000FF0000FF0000FF0000FF
        0000FF007B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00000000
        FF0000FF0000FF0000FF0000FF0000FF0000FF007B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00000000000000000000FF0000FF000000000000
        000000007B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF00000000FF0000FF00000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        FFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF00000000FF0000FF0000000000FF
        FFFFFFFF7B7B7BFF00FFFF00FFFF00FF00FFFFFFFFFF00FFFF00FFFFFFFFFF00
        FFFF000000000000000000000000FFFFFF00FFFF7B7B7BFF00FFFF00FFFF00FF
        00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFFFFF00FFFFFFFFFF7B7B7BFF00FFFF00FFFF00FF
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00FFFF00FFFFFFFFFF00FFFF00FFFFFFFF
        FF00FFFF7B7B7BFF00FFFF00FFFF00FFFFFFFF00FFFFFFFFFFFFFFFF00FFFF7B
        7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7BFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103' '#1095#1077#1088#1077#1079' '#1087#1086#1080#1089#1082' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1091
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 5
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = siCloseInvClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      Action = acHelp
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B393939FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B397B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B007B7B007B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD00393900
        7B7B007B7B007B7B003939848484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B7BFFFF7BFFFF7BFFFF003939397B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B007B7B9C9C9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFBDBDBD7BBDBD7BFFFF7BFFFF7BBDBD003939848484FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF397B7B7B
        FFFF7BFFFF7BFFFF7BBDBD6363639C9C9CFF00FFFF00FFFF00FFFF00FFFF00FF
        393939397B7B397B7B7B7B7BFF00FF397B7B7BFFFF7BFFFF7BFFFF0039396363
        639C9C9CFF00FFFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7B7B7BFF00FFFF
        00FF397B7B7BFFFF7BFFFF397B7B007B7B848484FF00FFFF00FFFF00FFFF00FF
        397B7B7BFFFF7BFFFF397B7BFF00FFBDBDBD39BDBD7BFFFF7BFFFF397B7B00FF
        FF424242BDBDBDFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7BFFFF397B7B39
        BDBD7BFFFF7BFFFF7BFFFF397B7B00FFFF424242BDBDBDFF00FFFF00FFFF00FF
        BDBDBD7BBDBD7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF39BDBD00BDBD00FF
        FF424242BDBDBDFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B7BFFFF7BFFFF7B
        FFFF7BFFFF397B7B00BDBD00FFFF007B7B848484FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFBDBDBD003939007B7B007B7B007B7B00FFFF00FFFF00BDBD6363
        63DEDEDEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B00
        7B7B007B7B007B7B215A5A7B7B7BDEDEDEFF00FFFF00FFFF00FF}
      Hint = #1057#1087#1088#1072#1074#1082#1072' '#1087#1086' '#1088#1072#1073#1086#1090#1077' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1086#1082#1085#1077
      ImageIndex = 73
      Spacing = 1
      Left = 459
      Top = 3
      Visible = True
      OnClick = acHelpExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 843
    Height = 52
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 28
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 87
      Height = 13
      Caption = #8470' '#1072#1082#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 216
      Top = 8
      Width = 34
      Height = 13
      Caption = 'C'#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 216
      Top = 28
      Width = 60
      Height = 13
      Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 488
      Top = 8
      Width = 87
      Height = 13
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 578
      Top = 8
      Width = 97
      Height = 17
      Alignment = taRightJustify
      DataField = 'COST'
      DataSource = dm.dsORetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 286
      Top = 8
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEP'
      DataSource = dm.dsORetList
    end
    object edSN: TDBEdit
      Left = 116
      Top = 4
      Width = 93
      Height = 21
      Cursor = crDrag
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsORetList
      TabOrder = 0
    end
    object deSDate: TDBDateEdit
      Left = 116
      Top = 24
      Width = 93
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsORetList
      Color = clInfoBk
      NumGlyphs = 2
      TabOrder = 1
    end
    object lcComp: TDBLookupComboBox
      Left = 284
      Top = 24
      Width = 191
      Height = 21
      Color = clInfoBk
      DataField = 'COMPID'
      DataSource = dm.dsORetList
      KeyField = 'D_COMPID'
      ListField = 'SNAME'
      ListSource = dm2.dsBuyer
      TabOrder = 2
      OnCloseUp = lcCompCloseUp
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 425
    Width = 843
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 297
      Top = 0
      Height = 240
    end
    object Panel3: TPanel
      Left = 300
      Top = 0
      Width = 543
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 543
        Height = 29
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object Label13: TLabel
          Left = 136
          Top = 6
          Width = 32
          Height = 13
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 320
          Top = 6
          Width = 52
          Height = 13
          Caption = #1048#1076'. '#1085#1086#1084#1077#1088
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edArt: TEdit
          Left = 176
          Top = 4
          Width = 79
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          Text = 'edArt'
          OnChange = edArtChange
          OnKeyDown = edArtKeyDown
        end
        object edUID: TEdit
          Left = 380
          Top = 4
          Width = 73
          Height = 21
          Color = clInfoBk
          TabOrder = 1
          Text = 'edUID'
          OnKeyDown = edUIDKeyDown
        end
        object Button1: TButton
          Left = 4
          Top = 4
          Width = 75
          Height = 21
          Caption = #1054#1090#1082#1088#1099#1090#1100
          TabOrder = 2
          OnClick = Button1Click
        end
      end
      object pc1: TPageControl
        Left = 0
        Top = 29
        Width = 543
        Height = 211
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 1
        OnChange = pc1Change
        object tsPrice: TTabSheet
          Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
          ImageIndex = 1
          object dgDPrice: TM207IBGrid
            Left = 0
            Top = 0
            Width = 535
            Height = 183
            Align = alClient
            Color = clBtnFace
            DataSource = dm.dsDPrice
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
            PopupMenu = pmWH
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleButtons = True
            OnGetCellParams = dgDPriceGetCellParams
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = False
            SortOnTitleClick = True
            Columns = <
              item
                Expanded = False
                FieldName = 'PRODCODE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1048#1079#1075'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_MATID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1052#1072#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 31
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_GOODID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1053#1072#1080#1084'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 33
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_INSID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1054#1042
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 26
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'D_COUNTRYID'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ART'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 69
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ART2'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'UNITID'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1045#1048
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 27
                Visible = True
              end
              item
                Color = clYellow
                Expanded = False
                FieldName = 'OPTPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Width = 71
                Visible = True
              end
              item
                Color = clAqua
                Expanded = False
                FieldName = 'SPRICE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1055#1088'.'#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRICE'
                ReadOnly = True
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end>
          end
        end
        object TabSheet1: TTabSheet
          Caption = #1055#1088#1086#1076#1072#1078#1072
          ImageIndex = 2
          object dgSelled: TM207IBGrid
            Left = 0
            Top = 29
            Width = 535
            Height = 154
            Align = alClient
            Color = clBtnFace
            DataSource = dm.dsOSelled
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
            PopupMenu = pmWH
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            IniStorage = FormStorage1
            TitleButtons = True
            MultiShortCut = 0
            ColorShortCut = 0
            InfoShortCut = 0
            ClearHighlight = False
            SortOnTitleClick = True
            Columns = <
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRODCODE'
                Title.Alignment = taCenter
                Title.Caption = #1048#1079#1075'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'MATID'
                Title.Alignment = taCenter
                Title.Caption = #1052#1072#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'GOODID'
                Title.Alignment = taCenter
                Title.Caption = #1053#1072#1080#1084'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'INSID'
                Title.Alignment = taCenter
                Title.Caption = #1054'.'#1042'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'UID'
                Title.Alignment = taCenter
                Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'ART'
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'ART2'
                Title.Alignment = taCenter
                Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'UNITID'
                Title.Alignment = taCenter
                Title.Caption = #1045#1076'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'NDSNAME'
                Title.Alignment = taCenter
                Title.Caption = #1053#1044#1057
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1055#1088'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'W'
                Title.Alignment = taCenter
                Title.Caption = #1042#1077#1089
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SZ'
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1079#1084#1077#1088
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'COST'
                Title.Alignment = taCenter
                Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SN0'
                Title.Alignment = taCenter
                Title.Caption = #1042#1085#1091#1090#1088'. '#8470
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SSF0'
                Title.Alignment = taCenter
                Title.Caption = #1042#1085#1077#1096#1085'. '#8470
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SDATE0'
                Title.Alignment = taCenter
                Title.Caption = #1042#1085#1091#1090#1088'. '#1076#1072#1090#1072' '#1087#1086#1089#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SN'
                Title.Alignment = taCenter
                Title.Caption = #8470' '#1085#1072#1082#1083'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SDATE'
                Title.Alignment = taCenter
                Title.Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'NDATE0'
                Title.Alignment = taCenter
                Title.Caption = #1042#1085#1077#1096#1085'. '#1076#1072#1090#1072' '#1087#1086#1089#1090'.'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'OPTPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1062#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'SUP'
                Title.Alignment = taCenter
                Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'BUYER'
                Title.Alignment = taCenter
                Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'OPRICE'
                Title.Alignment = taCenter
                Title.Caption = #1054#1087#1090'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end
              item
                Color = clInfoBk
                Expanded = False
                FieldName = 'PRICE2'
                Title.Alignment = taCenter
                Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clNavy
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = []
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 535
            Height = 29
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 1
            object Label3: TLabel
              Left = 4
              Top = 8
              Width = 7
              Height = 13
              Caption = 'C'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label7: TLabel
              Left = 104
              Top = 8
              Width = 14
              Height = 13
              Caption = #1055#1086
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label8: TLabel
              Left = 212
              Top = 8
              Width = 60
              Height = 13
              Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object deBD: TDateEdit
              Left = 16
              Top = 4
              Width = 85
              Height = 21
              Color = clInfoBk
              NumGlyphs = 2
              TabOrder = 0
              OnChange = deBDChange
            end
            object deED: TDateEdit
              Left = 120
              Top = 4
              Width = 85
              Height = 21
              Color = clInfoBk
              NumGlyphs = 2
              TabOrder = 1
              OnChange = deEDChange
            end
            object lcRetClient: TDBLookupComboBox
              Left = 276
              Top = 4
              Width = 145
              Height = 21
              Color = clInfoBk
              KeyField = 'D_COMPID'
              ListField = 'SNAME'
              ListSource = dm2.dsBuyer
              TabOrder = 2
              OnCloseUp = lcRetClientCloseUp
              OnKeyPress = lcRetClientKeyPress
            end
          end
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 297
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 238
      end
      object Splitter4: TSplitter
        Left = 181
        Top = 1
        Height = 238
      end
      object Splitter5: TSplitter
        Left = 244
        Top = 1
        Height = 238
      end
      object Splitter7: TSplitter
        Left = 113
        Top = 1
        Height = 238
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 116
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 184
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 247
        Top = 1
        Width = 49
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbcountry: TListBox
        Left = 56
        Top = 1
        Width = 57
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
  end
  object dgEl: TM207IBGrid
    Left = 0
    Top = 96
    Width = 843
    Height = 326
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsORet
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    PopupMenu = pmEl
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 1
    IniStorage = FormStorage1
    TitleButtons = True
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RecNo'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 33
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PROD'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 33
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'MATID'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 33
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'GOODID'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 38
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'INSID'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 21
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 56
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 59
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UNITID'
        ReadOnly = True
        Title.Alignment = taCenter
        Title.Caption = #1045#1076'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 29
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UID'
        Title.Alignment = taCenter
        Title.Caption = #1048#1076'.'#1085#1086#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'W'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SZ'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'OPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1054#1087#1090'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Sup'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NDATE'
        Title.Alignment = taCenter
        Title.Caption = #1042#1085#1077#1096#1085'. '#1076#1072#1090#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SDATE'
        Title.Alignment = taCenter
        Title.Caption = #1042#1085#1091#1090#1088'. '#1076#1072#1090#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SSF'
        Title.Alignment = taCenter
        Title.Caption = #1042#1085#1077#1096#1085'. '#8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SN'
        Title.Alignment = taCenter
        Title.Caption = #1042#1085#1091#1090#1088'. '#8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NDS'
        Title.Alignment = taCenter
        Title.Caption = #1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RET'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1080#1095#1080#1085#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE0'
        Title.Alignment = taCenter
        Title.Caption = #1062#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Cost'
        Title.Alignment = taCenter
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbcountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 28
    Top = 140
  end
  object pmEl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 156
    Top = 140
    object N1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
    end
  end
  object pmWH: TPopupMenu
    Images = dmCom.ilButtons
    Left = 156
    Top = 186
    object N2: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 13
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 384
    Top = 192
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103' '#1095#1077#1088#1077#1079' '#1087#1086#1080#1089#1082' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1091
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acHelp: TAction
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072' '#1087#1086' '#1088#1072#1073#1086#1090#1077' '#1074' '#1090#1077#1082#1091#1097#1077#1084' '#1086#1082#1085#1077
      ImageIndex = 73
      OnExecute = acHelpExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
  end
end
