unit Period;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons, rxToolEdit;

type
  TfmPeriod = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    deBD: TDateEdit;
    deED: TDateEdit;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPeriod: TfmPeriod;

function GetPeriod(var BD, ED: TDateTime): boolean;
function GetPeriodForRepl(var BD, ED: TDateTime): boolean;

implementation

uses Data, comdata;

{$R *.DFM}


function GetPeriod(var BD, ED: TDateTime): boolean;
begin
  fmPeriod:=TfmPeriod.Create(NIL);
  try
    with fmPeriod do
      begin
        deBD.Date:=BD;
        deED.Date:=ED;
        Result:=ShowModal=mrOK;
        if Result then
          begin
            BD:=deBD.Date;
            ED:=deED.Date+0.99999;
          end;
      end;
  finally
    fmPeriod.Free;
  end;
end;

function GetPeriodForRepl(var BD, ED: TDateTime): boolean;
begin
  if dmcom.UserId=1 then
  begin
  fmPeriod:=TfmPeriod.Create(NIL);
  try
    with fmPeriod do
      begin
        deBD.Date:=BD;
        deED.Date:=ED;
        Result:=ShowModal=mrOK;
        if Result then
          begin
            BD:=deBD.Date;
            ED:=deED.Date+0.9999;
          end;
      end;
  finally
    fmPeriod.Free;
  end;
  end
    else
  begin
     ED:=dm.RED+0.9999;
     BD:=ED-100; //dm.RBD;
     Result:=true;
  end
end;

procedure TfmPeriod.FormActivate(Sender: TObject);
var year, month, day: word;
begin
if (dm.WorkMode='AnlzSelEmp') and not Centerdep then
begin
DeCodeDate(Date, year, month, day);
deBD.MinDate:=StrToDate('01.01.'+ intToStr(year));
deED.MinDate:=StrToDate('01.01.'+ intToStr(year));
end;
end;

end.
