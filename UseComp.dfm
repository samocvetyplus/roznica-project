object fmUseComp: TfmUseComp
  Left = 158
  Top = 135
  Width = 783
  Height = 540
  Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082'/'#1080#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100' '#1080#1089#1087#1086#1083#1100#1079#1091#1077#1090#1089#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pbutton: TPanel
    Left = 0
    Top = 472
    Width = 775
    Height = 41
    Align = alBottom
    TabOrder = 0
    object btclose: TButton
      Left = 360
      Top = 8
      Width = 75
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 0
      OnClick = btcloseClick
    end
  end
  object pc1: TPageControl
    Left = 0
    Top = 0
    Width = 775
    Height = 472
    ActivePage = sinfo
    Align = alClient
    TabIndex = 2
    TabOrder = 1
    object Art2: TTabSheet
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1090#1086#1088#1099#1093' '#1072#1088#1090#1080#1082#1091#1083#1086#1074' (art2)'
      object dgart2: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        AllowedOperations = []
        ColumnDefValues.Title.TitleButton = True
        DataSource = dm3.dsCompArt2
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'D_COMPID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SUPID'
            Footers = <>
          end>
      end
    end
    object D_art: TTabSheet
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1087#1077#1088#1074#1099#1093' '#1072#1088#1090#1080#1082#1091#1083#1086#1074' (d_art)'
      ImageIndex = 1
      object dgd_art: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        DataSource = dm3.dsCompD_art
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'D_ARTID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'D_MATID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'D_GOODID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'D_INSID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'UNITID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'D_COMPID'
            Footers = <>
          end>
      end
    end
    object sinfo: TTabSheet
      Caption = 'Sinfo'
      ImageIndex = 3
      object dgSinfo: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        DataSource = dm3.dsCompSinfo
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'SINFOID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SN'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SINVID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SELID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SSF'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SDATE'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'NDATE'
            Footers = <>
          end>
      end
    end
    object sinv: TTabSheet
      Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1103' (Sinv)'
      ImageIndex = 4
      object dgsinv: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        DataSource = dm3.dsCompSinv
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'SINVID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SN'
            Footers = <>
            Title.Caption = #1042#1085#1091#1090'. '#1085#1086#1084#1077#1088
          end
          item
            EditButtons = <>
            FieldName = 'SDATE'
            Footers = <>
            Title.Caption = #1042#1085#1091#1090'. '#1076#1072#1090#1072
          end
          item
            EditButtons = <>
            FieldName = 'NDATE'
            Footers = <>
            Title.Caption = #1042#1085#1077#1096'. '#1076#1072#1090#1072
          end
          item
            EditButtons = <>
            FieldName = 'SSF'
            Footers = <>
            Title.Caption = #1042#1085#1077#1096'. '#1085#1086#1084#1077#1088
          end
          item
            EditButtons = <>
            FieldName = 'TypeSinv'
            Footers = <>
            Title.Caption = #1058#1080#1087' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
          end>
      end
    end
    object tbPayMent: TTabSheet
      Caption = #1054#1087#1083#1072#1090#1099
      ImageIndex = 4
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        DataSource = dm3.dsCompBalans
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'PAY'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'COMPID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'PDATE'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'DESCRIPTON'
            Footers = <>
          end>
      end
    end
    object tbRemains: TTabSheet
      Caption = #1057#1090'. '#1086#1089#1090#1072#1090#1082#1080' (Remains)'
      ImageIndex = 5
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 767
        Height = 444
        Align = alClient
        ColumnDefValues.Title.TitleButton = True
        DataSource = dm3.dsCompRemains
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'ID'
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
            Width = 91
          end
          item
            EditButtons = <>
            FieldName = 'FULLART'
            Footers = <>
          end>
      end
    end
  end
end
