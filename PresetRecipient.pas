unit PresetRecipient;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrlsEh, Buttons;

type
  TfrmPresetRecipient = class(TForm)
    DBEditEh1: TDBEditEh;
    Label1: TLabel;
    Label2: TLabel;
    DBEditEh2: TDBEditEh;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPresetRecipient: TfrmPresetRecipient;

implementation

{$R *.dfm}
uses data3, db, dbUtil;

procedure TfrmPresetRecipient.FormCreate(Sender: TObject);
begin
  OpenDataSet(dm3.quReciepGift);
end;

procedure TfrmPresetRecipient.BitBtn1Click(Sender: TObject);
begin
  execSql('update Writeoff_sinv  set ACTNAME_ID = 8' +
          ' where SInvID = ' + dm3.quActAllowancesListSINVID.AsString , dm3.quTmp);

  if (dm3.quReciepGift.State = dsInsert) or (dm3.quReciepGift.State = dsEdit) then
     dm3.quReciepGift.Post;
end;

procedure TfrmPresetRecipient.BitBtn2Click(Sender: TObject);
begin
  if (dm3.quReciepGift.State = dsInsert) or (dm3.quReciepGift.State = dsEdit) then
    dm3.quReciepGift.Cancel;
end;

procedure TfrmPresetRecipient.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CloseDataSet(dm3.quReciepGift);
end;

end.

