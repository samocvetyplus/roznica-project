object fmWriteLogBase: TfmWriteLogBase
  Left = 255
  Top = 276
  Width = 652
  Height = 258
  Caption = #1047#1072#1087#1080#1089#1100' '#1076#1072#1085#1085#1099#1093' '#1074' Log-'#1073#1072#1079#1091
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cbDB: TGroupBox
    Left = 0
    Top = 0
    Width = 443
    Height = 188
    Align = alClient
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1073#1072#1079#1077' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1077
    TabOrder = 0
    object LBD: TLabel
      Left = 8
      Top = 24
      Width = 52
      Height = 13
      Caption = #1055#1091#1090#1100' '#1082' '#1041#1044
    end
    object lbUserName: TLabel
      Left = 4
      Top = 48
      Width = 73
      Height = 13
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
    end
    object lbPsw: TLabel
      Left = 4
      Top = 72
      Width = 49
      Height = 13
      Caption = 'Password:'
    end
    object lbRole: TLabel
      Left = 4
      Top = 96
      Width = 28
      Height = 13
      Caption = #1056#1086#1083#1100':'
    end
    object lbCharSet: TLabel
      Left = 4
      Top = 124
      Width = 58
      Height = 13
      Caption = #1050#1086#1076#1080#1088#1086#1074#1082#1072':'
    end
    object lbAddInf: TLabel
      Left = 210
      Top = 48
      Width = 148
      Height = 13
      Caption = 'Additional Connect Parameters:'
    end
    object btTest: TButton
      Left = 24
      Top = 152
      Width = 169
      Height = 25
      Caption = #1058#1077#1089#1090#1080#1088#1086#1074#1072#1090#1100' '#1089#1086#1077#1076#1077#1085#1077#1085#1080#1077
      TabOrder = 0
      OnClick = btTestClick
    end
    object filenamedblog: TFilenameEdit
      Left = 83
      Top = 21
      Width = 345
      Height = 21
      NumGlyphs = 1
      TabOrder = 1
    end
    object edtUserName: TEdit
      Left = 84
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object edtPw: TEdit
      Left = 84
      Top = 72
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 3
    end
    object edtRole: TEdit
      Left = 84
      Top = 96
      Width = 121
      Height = 21
      TabOrder = 4
    end
    object cbCharset: TComboBox
      Left = 84
      Top = 120
      Width = 121
      Height = 21
      ItemHeight = 13
      ItemIndex = 4
      TabOrder = 5
      Text = 'WIN1251'
      Items.Strings = (
        'NONE'
        'ASCII'
        'DOS850'
        'WIN1250'
        'WIN1251'
        'WIN1252'
        'WIN1253'
        'WIN1254')
    end
    object memAddInfo: TMemo
      Left = 213
      Top = 64
      Width = 217
      Height = 77
      TabOrder = 6
    end
  end
  object pbutton: TPanel
    Left = 0
    Top = 188
    Width = 644
    Height = 41
    Align = alBottom
    TabOrder = 1
    object btok: TButton
      Left = 160
      Top = 8
      Width = 75
      Height = 25
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      TabOrder = 0
      OnClick = btokClick
    end
    object btClose: TButton
      Left = 344
      Top = 8
      Width = 75
      Height = 25
      Caption = #1042#1099#1093#1086#1076
      TabOrder = 1
      OnClick = btCloseClick
    end
  end
  object pInfo: TPanel
    Left = 443
    Top = 0
    Width = 201
    Height = 188
    Align = alRight
    TabOrder = 2
    object LBDate: TLabel
      Left = 8
      Top = 64
      Width = 81
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1076#1072#1090#1072
    end
    object LEDate: TLabel
      Left = 8
      Top = 112
      Width = 74
      Height = 13
      Caption = #1050#1086#1085#1077#1095#1085#1072#1103' '#1076#1072#1090#1072
    end
    object chbDel: TCheckBox
      Left = 6
      Top = 16
      Width = 185
      Height = 17
      Alignment = taLeftJustify
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1089#1083#1077' '#1087#1077#1088#1077#1076#1072#1095#1080' '#1076#1072#1085#1085#1099#1093
      TabOrder = 0
    end
    object debd: TDateEdit
      Left = 40
      Top = 80
      Width = 121
      Height = 21
      NumGlyphs = 2
      TabOrder = 1
    end
    object deed: TDateEdit
      Left = 40
      Top = 128
      Width = 121
      Height = 21
      NumGlyphs = 2
      TabOrder = 2
    end
  end
  object dbLog: TpFIBDatabase
    DefaultTransaction = trLog
    SQLDialect = 1
    Timeout = 0
    LibraryName = 'fbclient.dll'
    WaitForRestoreConnect = 0
    Left = 232
    Top = 144
  end
  object trLog: TpFIBTransaction
    DefaultDatabase = dbLog
    TimeoutAction = TARollback
    Left = 288
    Top = 144
  end
  object qutmpLog: TpFIBQuery
    Transaction = trLog
    Database = dbLog
    SQL.Strings = (
      
        'execute procedure R_RECIEVE_LOG$TABLE(:FTABLE, :L_USERID, :HOST,' +
        ' :CONNCTTIME,'
      
        '  :SYSUSER, :DISCRIPT, :CURR_CONTID, :USERNAME, :L_OPRID, :OPERT' +
        'IME, :OPERTYPE,'
      
        '  :OPER_ENTER, :REF, :OPERTIME_ED, :TREE_LEVEL, :CHILD, :L_SYSSQ' +
        'L, :TABLENAME,'
      
        '  :DATA, :OPTYPE, :OPDATE, :L_BLOBFIELDSID, :OLDVALUE, :NEWVALUE' +
        ', :SYSSQL_ID)')
    Left = 352
    Top = 144
  end
  object quLogUser: TpFIBDataSet
    SelectSQL.Strings = (
      'select L_USERID, HOST, CONNCTTIME, SYSUSER, DISCRIPT,'
      '       CURR_CONTID, USERNAME'
      'from R_LOG$TABLE (:BD,:ED, 0)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 232
    Top = 72
    poSQLINT64ToBCD = True
    object quLogUserL_USERID: TFIBIntegerField
      FieldName = 'L_USERID'
    end
    object quLogUserHOST: TFIBStringField
      FieldName = 'HOST'
      Size = 50
      EmptyStrToNull = True
    end
    object quLogUserCONNCTTIME: TFIBStringField
      FieldName = 'CONNCTTIME'
      Size = 110
      EmptyStrToNull = True
    end
    object quLogUserSYSUSER: TFIBStringField
      FieldName = 'SYSUSER'
      EmptyStrToNull = True
    end
    object quLogUserDISCRIPT: TFIBStringField
      FieldName = 'DISCRIPT'
      Size = 50
      EmptyStrToNull = True
    end
    object quLogUserCURR_CONTID: TFIBIntegerField
      FieldName = 'CURR_CONTID'
    end
    object quLogUserUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object quLogOperation: TpFIBDataSet
    SelectSQL.Strings = (
      'select L_USERID, L_OPRID, OPERTIME,'
      '       OPERTYPE, OPER_ENTER, REF,'
      '       OPERTIME_ED, TREE_LEVEL, CHILD'
      'from R_LOG$TABLE (:BD,:ED, 1)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 304
    Top = 72
    poSQLINT64ToBCD = True
    object quLogOperationL_USERID: TFIBIntegerField
      FieldName = 'L_USERID'
    end
    object quLogOperationL_OPRID: TFIBIntegerField
      FieldName = 'L_OPRID'
    end
    object quLogOperationOPERTIME: TFIBDateTimeField
      FieldName = 'OPERTIME'
    end
    object quLogOperationOPERTYPE: TFIBStringField
      FieldName = 'OPERTYPE'
      Size = 50
      EmptyStrToNull = True
    end
    object quLogOperationOPER_ENTER: TFIBIntegerField
      FieldName = 'OPER_ENTER'
    end
    object quLogOperationREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object quLogOperationOPERTIME_ED: TFIBDateTimeField
      FieldName = 'OPERTIME_ED'
    end
    object quLogOperationTREE_LEVEL: TFIBIntegerField
      FieldName = 'TREE_LEVEL'
    end
    object quLogOperationCHILD: TFIBIntegerField
      FieldName = 'CHILD'
    end
  end
  object quLogSysSql: TpFIBDataSet
    SelectSQL.Strings = (
      'select  L_SYSSQL, TABLENAME, L_OPRID, L_USERID,'
      '        DATA, OPTYPE, OPDATE'
      'from R_LOG$TABLE (:BD,:ED, 2)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 376
    Top = 72
    poSQLINT64ToBCD = True
    object quLogSysSqlL_SYSSQL: TFIBIntegerField
      FieldName = 'L_SYSSQL'
    end
    object quLogSysSqlTABLENAME: TFIBStringField
      FieldName = 'TABLENAME'
      EmptyStrToNull = True
    end
    object quLogSysSqlL_OPRID: TFIBIntegerField
      FieldName = 'L_OPRID'
    end
    object quLogSysSqlL_USERID: TFIBIntegerField
      FieldName = 'L_USERID'
    end
    object quLogSysSqlDATA: TFIBStringField
      FieldName = 'DATA'
      Size = 4096
      EmptyStrToNull = True
    end
    object quLogSysSqlOPTYPE: TFIBStringField
      FieldName = 'OPTYPE'
      Size = 60
      EmptyStrToNull = True
    end
    object quLogSysSqlOPDATE: TFIBDateTimeField
      FieldName = 'OPDATE'
    end
  end
  object quLogBlobFields: TpFIBDataSet
    SelectSQL.Strings = (
      'select  l_blobfieldsid, oldvalue, newvalue, syssql_id'
      'from R_LOG$TABLE (:BD,:ED, 3)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 424
    Top = 144
    poSQLINT64ToBCD = True
    object quLogBlobFieldsL_BLOBFIELDSID: TFIBIntegerField
      FieldName = 'L_BLOBFIELDSID'
    end
    object quLogBlobFieldsOLDVALUE: TFIBBlobField
      FieldName = 'OLDVALUE'
      Size = 8
    end
    object quLogBlobFieldsNEWVALUE: TFIBBlobField
      FieldName = 'NEWVALUE'
      Size = 8
    end
    object quLogBlobFieldsSYSSQL_ID: TFIBIntegerField
      FieldName = 'SYSSQL_ID'
    end
  end
  object qutmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 512
    Top = 160
  end
end
