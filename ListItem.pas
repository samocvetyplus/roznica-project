unit ListItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst;

type
  TfmListItem = class(TForm)
    CheckListBox1: TCheckListBox;
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmListItem: TfmListItem;

implementation

uses NotSaleItems, uUtils;

{$R *.dfm}

procedure TfmListItem.Button1Click(Sender: TObject);
var
i: integer;
begin
  with fmNotSaleItems do
  begin
    for i:=0 to cxGrid2DBTableView1.ColumnCount-1 do
    begin
       if CheckListBox1.Checked[i] then
       cxGrid2DBTableView1.Columns[i].Visible:=True
       else cxGrid2DBTableView1.Columns[i].Visible:=False;
    end;
    fmNotSaleItems.dxComponentPrinter1Link1.Preview;
    cxGrid2DBTableView1.RestoreFromIniFile(GetIniFileName);
  end;
  fmListItem.Close;
end;

procedure TfmListItem.Button2Click(Sender: TObject);
begin
close;
end;

end.
