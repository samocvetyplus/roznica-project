unit CostCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DBCtrls, StdCtrls, Buttons, ActnList, MsgDialog;

type
  TfmCostCard = class(TForm)
    plCost: TPanel;
    plButtom: TPanel;
    LCass: TLabel;
    LCostCard: TLabel;
    edCostCard: TEdit;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    acList: TActionList;
    acEnter: TAction;
    acEsc: TAction;
    LCassValue: TLabel;
    procedure edCostCardKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure acEscExecute(Sender: TObject);
    procedure acEnterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCostCard: TfmCostCard;

implementation
uses data, dbUtil, DB;
{$R *.dfm}

procedure TfmCostCard.edCostCardKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8, ',', '.':;
  else SysUtils.Abort;
 end
end;

procedure TfmCostCard.FormCreate(Sender: TObject);
begin
 if dm.taSellItemCHECKNO.IsNull then LCassValue.Caption:=trim(dm.taSellItemCOST.AsString)
 else begin
  with dm do
  begin
   qutmp.Close;
   qutmp.SQL.Text:='select sum(smartround (q0*price, 1, 100)) from sellitem '+
    'where sellid='+taSellItemSELLID.AsString+' and checkno='+taSellItemCHECKNO.AsString;
   quTmp.ExecQuery;
   LCassValue.Caption:=quTmp.Fields[0].AsString;
   quTmp.Close;
   quTmp.Transaction.CommitRetaining;
  end
 end
end;

procedure TfmCostCard.acEscExecute(Sender: TObject);
begin
 ModalResult:=mrCancel;
end;

procedure TfmCostCard.acEnterExecute(Sender: TObject);
begin
 if ActiveControl=edCostCard then ActiveControl:=btOk
 else if ActiveControl=btOk then ModalResult:=mrOk
 else if ActiveControl=btCancel then ModalResult:=mrCancel;
end;

procedure TfmCostCard.FormClose(Sender: TObject; var Action: TCloseAction);
var cost, costold:real;
    ch:char;
begin
 cost := 0;
 costold := 0;
 if ModalResult=mrok then
 begin
  if edCostCard.Text='' then begin
   MessageDialog('������� �����, ������� �������� �� ���������', mtInformation, [mbOk], 0);
   ActiveControl:=edCostCard;
   SysUtils.Abort;
  end;
  ch:=DecimalSeparator;
  try
   DecimalSeparator:='.';
   cost:=StrToFloat(edCostCard.Text);
   costold:=strtofloat(LCassValue.Caption);
  except
   try
    DecimalSeparator:=',';
    cost:=StrToFloat(edCostCard.Text);
    costold:=strtofloat(LCassValue.Caption);
   except
    MessageDialog('�� ����� ������� ����� �� ���������', mtInformation, [mbOk], 0);
    ActiveControl:=edCostCard;
    SysUtils.Abort;
   end
  end;

  if cost>costold then begin
   MessageDialog('����� �� ����� ���� ������, ��� ����� �� �����', mtInformation, [mbOk], 0);
   ActiveControl:=edCostCard;
   SysUtils.Abort;
  end;

  DecimalSeparator:='.';
  ExecSQL('update sellitem set costcard='+floattostr(cost)+' where sellitemid='+dm.taSellItemSELLITEMID.AsString, dm.quTmp);
  DecimalSeparator:=ch;  
 end
end;

end.
