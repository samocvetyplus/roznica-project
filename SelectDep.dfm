object fmSelectDep: TfmSelectDep
  Left = 370
  Top = 216
  AutoSize = True
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1080#1083#1080#1072#1083
  ClientHeight = 255
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object grDep: TRadioGroup
    Left = 0
    Top = 0
    Width = 304
    Height = 214
    Align = alClient
    Caption = #1060#1080#1083#1080#1072#1083#1099
    TabOrder = 0
  end
  object plButton: TPanel
    Left = 0
    Top = 214
    Width = 304
    Height = 41
    Align = alBottom
    TabOrder = 1
    object btOk: TBitBtn
      Left = 56
      Top = 8
      Width = 81
      Height = 25
      Caption = #1042#1099#1073#1088#1072#1090#1100
      TabOrder = 0
      Kind = bkOK
    end
    object btCancel: TBitBtn
      Left = 192
      Top = 8
      Width = 83
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object acList: TActionList
    Left = 224
    Top = 72
    object acEsc: TAction
      Caption = 'acEsc'
      ShortCut = 27
      OnExecute = acEscExecute
    end
    object acEnter: TAction
      Caption = 'acEnter'
      ShortCut = 13
      OnExecute = acEnterExecute
    end
  end
end
