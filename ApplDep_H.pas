unit ApplDep_H;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmApplDep_H = class(TForm)
    Label1: TLabel;
    dg1: TDBGridEh;
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    OpSBtn: TSpeedButton;
    ClSBtn: TSpeedButton;
    CnlSBtn: TSpeedButton;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    dsEmp: TDataSource;
    LogEd: TEdit;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    Panel1: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    Label4: TLabel;
    DBText3: TDBText;
    Panel5: TPanel;
    Panel3: TPanel;
    Label8: TLabel;
    Panel2: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label5: TLabel;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    Panel4: TPanel;
    Panel6: TPanel;
    DBText8: TDBText;
    Panel7: TPanel;
    Panel8: TPanel;
    DBText9: TDBText;
    Label9: TLabel;
    procedure OpSBtnClick(Sender: TObject);
    procedure ClSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ClAllSBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
 

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmApplDep_H: TfmApplDep_H;
  var log,s:string;
  procedure Hist(var user, st: string);
implementation

   uses comdata, data, data3, ApplDep, MsgDialog, DInvItem, M207IBLogin, Hist,
  DList;

   {$R *.dfm}
   procedure Hist(var user, st: string); //���������� ������ � ������� INVHIST
begin
     fmApplDep_H.taHist.Append;
     fmApplDep_H.taHistDOCID.AsInteger := dm3.quApplDepListSINVID.AsInteger;
     fmApplDep_H.taHistFIO.Value := user;
     fmApplDep_H.taHistSTATUS.Value := st;
     fmApplDep_H.taHist.Post;
end;


procedure TfmApplDep_H.OpSBtnClick(Sender: TObject);
  //�������� ���������
begin
 taEmp.Active:=true;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin
   log:=LogEd.Text;
   s:='�������';
   Hist(log, s);
   fmApplDep.acOpen.Execute;
   close;
   end;
     taEmp.Active:=false;
end;

procedure TfmApplDep_H.ClSBtnClick(Sender: TObject);  //�������� ���������
begin
 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
 if LogEd.Text='' then log:=dmCom.User.Alias;
     s:='�������';
     Hist(log, s);
     fmApplDep.acCloseAppl.Execute;
     close;
    end;
end;

procedure TfmApplDep_H.CnlSBtnClick(Sender: TObject);  //������
begin
 taHist.Active:=false;
 close;
end;

procedure TfmApplDep_H.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  taHist.ParamByName('INVID').AsInteger := dm3.quApplDepListSINVID.AsInteger;
end;

procedure TfmApplDep_H.FormCreate(Sender: TObject);
begin
   taHist.Active := True;
   dg1.DataSource.DataSet.Last;    //��������� ���������
end;

procedure TfmApplDep_H.FormActivate(Sender: TObject);
begin
  Pswed.SetFocus;
  Caption:='�'+(dm3.quApplDepListSN.AsString);
  if dm3.quApplDepListISCLOSED.AsInteger=1 then
   begin
   fmAppldep_H.ClSBtn.Enabled:=false;
     //���������� ���� �����������
    Label9.Visible:=true;
    LogEd.Visible:=true;
    LogEd.Text:= dmCom.User.Alias;
    PswEd.Visible:=true;
    LaLogin.Visible:=true;
    LaPsw.Visible:=true;
   end
  else
   begin
   fmAppldep_H.OpSBtn.Enabled:=false;
   //�������� ���� �����������
   Label9.Visible:=false;
   LogEd.Visible:=false;
   PswEd.Visible:=false;
   LaLogin.Visible:=false;
   LaPsw.Visible:=false;
   end;
end;

procedure TfmApplDep_H.ClAllSBtnClick(Sender: TObject);
begin
  taHist.Active := false;
  close;
end;

procedure TfmApplDep_H.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 taHist.Active := false;
end;



procedure TfmApplDep_H.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmApplDep_H.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmApplDep_H.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (key=vk_return) and (ClSBtn.Enabled=true) then ClSBtn.Click;
if key=vk_escape then cnlSbtn.Click;
end;

end.
