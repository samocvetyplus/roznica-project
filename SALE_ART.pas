unit SALE_ART;

interface

uses
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, rxPlacemnt, StdCtrls, Buttons, jpeg, DB, FIBDatabase,
  pFIBDatabase, FIBDataSet,  DBClient,   FileCtrl,
  Mask, DBCtrls,  IBCustomDataSet, IBDatabase,Grids,
  IBQuery, IBStoredProc, IBTable,  
  pFIBQuery, ShlObj,  ShellApi,
  dxGDIPlusClasses,     Consts,   
     RxGrdCpt,     IniFiles,
  AppEvnts, Winsock, ComDrv32, dbUtil, TypInfo, ComCtrls,
  Provider, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, 
  pFIBDataSet, DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, uDialog;

type
  TSALE_ART_FORM = class(TForm)
    Label1: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    ART: TcxGridDBColumn;
    UID: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    EMP: TcxGridDBColumn;
    CLIENTNAME: TcxGridDBColumn;
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SALE_ART_FORM: TSALE_ART_FORM;

implementation
     uses comdata, ind_order, IndividualOrderData, sale_jew;
{$R *.dfm}

procedure TSALE_ART_FORM.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin

if dmIndividualOrder.mode=0 then
 begin


if (dmIndividualOrder.DataSet_ART_SELL.FieldByName('Art').AsString <>'') then
  begin

     if dmIndividualOrder.dtOrder.State in [dsEdit, dsInsert] then
      begin

          dmIndividualOrder.dtOrder.Post;

      end;
      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with dmIndividualOrder.dtOrder do
         begin
          Edit;
          FieldByName('ID_State').Value:=9;
          FieldByName('EMP_STATE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('EMP').AsString;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('Adate').AsDateTime ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SELL_DataSet.FieldByName('RN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('PRICE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('COST').AsFloat;
          FieldByName('UID').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('UID').AsInteger;
          Post;
         end;


      TDialog.Information('����� �'+dmIndividualOrder.dtOrder.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

      with dmIndividualOrder.dtHistory do
        begin
          Append;
          Post;
      end;

      with dmIndividualOrder.dtOrder do
        begin
          Edit;
          FieldByName('ID_State').AsInteger := 11;
          FieldByName('DATE_DOC').Clear;
          FieldByName('NUMBER_DOC').Clear;
          FieldByName('Emp_State').AsString := dmCom.UserName;
          FieldByName('Date_State').AsDateTime := now;
          //FieldByName('PRICE').Clear;
          Post;
          Refresh;
        end;

          dmIndividualOrder.dthistory.Refresh;




  end
else     TDialog.Information('�������� ������ �����, ��� ����������� �������!') ;

 end;

if (dmIndividualOrder.mode=2) then
 begin


if (dmIndividualOrder.DataSet_ART_SELL.FieldByName('Art').AsString <>'') then
  begin


     if dmIndividualOrder.dtOrder_factory.State in [dsEdit, dsInsert] then
      begin

          dmIndividualOrder.dtOrder_factory.Post;

      end;
      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with dmIndividualOrder.dtOrder_factory do
         begin
          Edit;
          FieldByName('ID_State').Value:=9;
          FieldByName('EMP_STATE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('EMP').AsString;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('Adate').AsDateTime ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SELL_DataSet.FieldByName('RN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('PRICE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('COST').AsFloat;
          FieldByName('UID').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('UID').AsInteger;
          Post;
         end;
         
      TDialog.Information('����� �'+dmIndividualOrder.dtOrder_factory.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

      with dmIndividualOrder.dtHistory do
        begin
          Append;
          Post;
      end;

      with dmIndividualOrder.dtOrder_factory do
        begin
          Edit;
          FieldByName('ID_State').AsInteger := 11;
          FieldByName('DATE_DOC').Clear;
          FieldByName('NUMBER_DOC').Clear;
          FieldByName('Emp_State').AsString := dmCom.UserName;
          FieldByName('Date_State').AsDateTime := now;
          //FieldByName('PRICE').Clear;
          Post;
          Refresh;
        end;

          dmIndividualOrder.dthistory.Refresh;
  end
   else

TDialog.Information('�������� ������ �����, ��� ����������� �������!') ;

 end;

Close;

end;

procedure TSALE_ART_FORM.FormCreate(Sender: TObject);
begin
cxGrid1DBTableView1.DataController.DataSource:=  dmIndividualOrder.DataSource_ART_SELL;
end;

procedure TSALE_ART_FORM.FormDestroy(Sender: TObject);
begin

 Close;






end;

end.
