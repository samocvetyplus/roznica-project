unit RetCltList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  StdCtrls, db, Menus, DBGridEh, ActnList, TB2Item,
  ComCtrls, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmRetCltList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    siPeriod: TSpeedItem;
    fmstrRetClt: TFormStorage;
    tb3: TSpeedBar;
    lbFindUID: TLabel;
    spitPrint: TSpeedItem;
    edFindUID: TEdit;
    lbFindArt: TLabel;
    edFindArt: TEdit;
    gridRetItem: TDBGridEh;
    acEvent: TActionList;
    ppPrint: TTBPopupMenu;
    acPrintAppl: TAction;
    acPrintDisv: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    StatusBar1: TStatusBar;
    siHelp: TSpeedItem;
    tbTagPrint: TTBSubmenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edFindArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edFindUIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridRetItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintUpdate(Sender: TObject);
    procedure edFindUIDKeyPress(Sender: TObject; var Key: Char);
    procedure acPrintApplExecute(Sender: TObject);
    procedure acPrintDisvExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
  private
    FSearchUID : boolean;
    FSearchArt : boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
    procedure TagPrintRet (Sender: TObject);
  end;

var
  fmRetCltList: TfmRetCltList;

implementation

uses comdata, Data, Period, ServData, ReportData, M207Proc, InsRepl, Data2,
     MsgDialog, dbUtil;

{$R *.DFM}

{ TfmRetClt }

procedure TfmRetCltList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;

procedure TfmRetCltList.TagPrintRet (Sender: TObject);
var arr : TarrDoc;
begin
 try
  arr:= gen_arr(gridRetItem, dmServ.taRetCltListSITEMID);
  PrintTag(arr, ret_tag, TMenuItem(Sender).Tag);
 except
  Finalize(arr); 
 end;
end;  

procedure TfmRetCltList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmRetCltList.FormCreate(Sender: TObject);
begin
  laDepFrom.Caption:='';
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  tb3.Wallpaper := wp;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  dm.SDepId := -1;
  OpenDataSets([dmServ.taRetCltList]);
  dm.WorkMode:='RETCLT';
  dm.SetBeginDate;
  ShowPeriod;
  FSearchArt := False;
  FSearchUID := True;
  edFindUID.Text := '';
  edFindArt.Text := '';
  FSearchArt := True;
  FSearchUID := True;
  dmcom.FillTagMenu(tbTagPrint, TagPrintRet, 1);
  
  gridRetItem.FieldColumns['SPRICE'].Visible := CenterDep;
  gridRetItem.FieldColumns['SCOST'].Visible := CenterDep;
end;

procedure TfmRetCltList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmServ.taRetCltList]);
  dmcom.tr.CommitRetaining;
  dm.WorkMode:='';
end;

procedure TfmRetCltList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRetCltList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRetCltList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dmserv.taRetCltList]);
      ShowPeriod;
    end;
end;

procedure TfmRetCltList.FormActivate(Sender: TObject);
begin
  dm.ClickUserDepMenu(dmServ.pmRetClt);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRetCltList.edFindArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN :if Shift = [] then ActiveControl := gridRetItem;
    VK_RETURN: if  not dmServ.taRetCltList.Locate('Art', edFindArt.Text, [loCaseInsensitive, loPartialKey]) then
     MessageDialog('������� �� ������!', mtWarning, [mbOk], 0);
  end;
end;

procedure TfmRetCltList.edFindUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN : if Shift = [] then ActiveControl := gridRetItem;
    VK_RETURN: if  not dmServ.taRetCltList.Locate('UID', edFindUID.Text, [loCaseInsensitive, loPartialKey]) then
     MessageDialog('������� �� �������!', mtWarning, [mbOk], 0);
  end;
end;

procedure TfmRetCltList.gridRetItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName='DEP')then Background := dm2.GetDepColor(dmServ.taRetCltListDEPID.AsInteger);
  if(Column.FieldName='UID')then Background := dmCom.clCream;
end;

procedure TfmRetCltList.acPrintUpdate(Sender: TObject);
begin
  with gridRetItem.DataSource.DataSet do
    (Sender as TAction).Enabled := Active and (not IsEmpty);
end;

procedure TfmRetCltList.edFindUIDKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '0'..'9', #8:;
 else sysutils.Abort;
 end;
end;

procedure TfmRetCltList.acPrintApplExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridRetItem, dmServ.taRetCltListSITEMID);
  PrintDocument(arr, appl);
end;

procedure TfmRetCltList.acPrintDisvExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridRetItem, dmServ.taRetCltListSITEMID);
  PrintDocument(arr, disv);
end;

procedure TfmRetCltList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100242)
end;

end.
