unit Rest;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  ExtCtrls, db, Menus, Buttons, ActnList, jpeg,
  rxPlacemnt, rxSpeedbar;

type
  TfmRest = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    dg1: TM207IBGrid;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    FormStorage1: TFormStorage;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    M207IBGrid1: TM207IBGrid;
    siCreate: TSpeedItem;
    siView: TSpeedItem;
    siDel: TSpeedItem;
    siCloseRest: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siDelRest: TSpeedItem;
    pmRest: TPopupMenu;
    N1: TMenuItem;
    siIns: TSpeedItem;
    siFind: TSpeedItem;
    N2: TMenuItem;
    N3: TMenuItem;
    siMerge: TSpeedItem;
    siReport: TSpeedItem;
    SpeedItem5: TSpeedItem;
    pmCloseInv: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    pmRep: TPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    SpeedItem6: TSpeedItem;
    edFind: TEdit;
    lbFind: TLabel;
    SpeedButton1: TSpeedButton;
    nTagNew: TMenuItem;
    lbCountry: TListBox;
    Splitter2: TSplitter;
    N6: TMenuItem;
    N9: TMenuItem;
    acList: TActionList;
    acAdd: TAction;
    acDelRest: TAction;
    acViewRest: TAction;
    acClose: TAction;
    acCloseRest: TAction;
    acCloseAllRest: TAction;
    procedure FormCreate(Sender: TObject);
    procedure lbInsClick(Sender: TObject);
    procedure lbInsKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ShowUID(Sender: TObject);
    procedure dg1DblClick(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormResize(Sender: TObject);
    procedure siDelRestClick(Sender: TObject);
    procedure siInsClick(Sender: TObject);
    procedure siFindClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    function dg1GetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;
    procedure siMergeClick(Sender: TObject);
    procedure siReportClick(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem6Click(Sender: TObject);
    procedure edFindKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1KeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelRestExecute(Sender: TObject);
    procedure acViewRestExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acCloseRestExecute(Sender: TObject);
    procedure acCloseAllRestExecute(Sender: TObject);
  protected
    FSearchEnable : boolean;
//    procedure
  private
    LogOprIdForm:string;  
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure CheckIns;
  end;

var
  fmRest: TfmRest;

implementation

uses Data2, comdata, Data, DBTree, UIDWH, RI, RInv, Ins, RestFind,
  ReportData, WHItem, M207Proc, data3, jewconst, MsgDialog;

{$R *.DFM}

procedure TfmRest.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRest.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dmCom, dm, dm2 do
    begin
      WorkMode:='REST';
      if not tr.Active then tr.StartTransaction;
      FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, nil,nil);
      dmCom.D_Att1Id := ATT1_DICT_ROOT;
      dmCom.D_Att2Id := ATT2_DICT_ROOT;
      OpenDataSets([taRInv, quSup, quProd, quMat, quGood, quIns, taNDS, quCountry]);
    end;
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbcountry.ItemIndex:=0;
  lbInsClick(NIL);
  FSearchEnable := False;
  edFind.Text := '';
  FSearchEnable := True;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);  
end;

procedure TfmRest.lbInsClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  with dm2.taRest do
    begin
      Active:=False;
      Open;
    end;
end;

procedure TfmRest.lbInsKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmRest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm2, dmCom, dm do
    begin
      CloseDataSets([taRest, taRInv, quSup, quProd, quMat, quGood, quIns, taNDS]);
      tr.CommitRetaining;
    end;
end;

procedure TfmRest.ShowUID(Sender: TObject);
begin
  if dg1.DataSource.DataSet.State = dsInsert then eXit;
  if (dg1.SelectedField.FieldName = 'RW') or (dg1.SelectedField.FieldName = 'RQ')
  then ShowAndFreeForm(TfmWHItem, Self, TForm(fmWHItem), True, False)
  else ShowAndFreeForm(TfmRI, Self, TForm(fmRI), True, False);
end;

procedure TfmRest.dg1DblClick(Sender: TObject);
begin
  if dg1.DataSource.DataSet.State = dsInsert then eXit;
{  with dg1.SelectedField do
  if (FieldName<>'TODO') and
     (FieldName<>'DONE') and
     (FieldName<>'MTO') and
     (FieldName<>'MFROM') then ShowUID(NIL);}
end;

procedure TfmRest.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight then Background:=clNavy
  else
      if dm2.taRInvIsClosed.AsInteger=0 then Background:=clInfoBk
      else Background:=clBtnFace;
end;

procedure TfmRest.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRest.siDelRestClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_DelArtRest,LogOprIdForm);
  dm2.taRest.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.siInsClick(Sender: TObject);
var LogOperationID:string;
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taRest]);
      if taRestId.IsNull then raise Exception.Create('���������� ������ �������');
      LogOperationID:=dm3.insert_operation(SLog_Ins,LogOprIdForm);
      Art2Id:=taRestArt2Id.AsInteger;
      FullArt:=taRestFullArt.AsString;
      Art2:=taRestArt2.AsString;
      ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);
      dm3.update_operation(LogOperationID);
    end;
end;

procedure TfmRest.siFindClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_FindRest,LogOprIdForm);
  ShowAndFreeForm(TfmRestFind, Self, TForm(fmRestFind), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight then Background:=clHighlight
  else begin
    if dm2.taRestTW.AsFloat > 0 then
      if dm2.taRestUNITID.AsInteger=1 then
      begin
        if dm2.taRestW.AsFloat = 0 then Background := clInfoBk
        else if abs(dm2.taRestTW.AsFloat - dm2.taRestW.AsFloat)<=0.01
             then Background := clBtnFace
             else Background := clYellow;
      end
      else begin
        if dm2.taRestQ.AsFloat = 0 then Background := clInfoBk
        else if abs(dm2.taRestTW.AsFloat - dm2.taRestQ.AsFloat)<=0.01
             then Background := clBtnFace
             else Background := clYellow;
      end
    end;
end;

function TfmRest.dg1GetCellCheckBox(Sender: TObject; Field: TField;
  var StateCheckBox: Integer): Boolean;
begin
  Result:=(Field.FieldName='TODO') or
          (Field.FieldName='DONE') or
          (Field.FieldName='MTO') or
          (Field.FieldName='MFROM');
end;

procedure TfmRest.siMergeClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_MergeRest,LogOprIdForm);
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taRest]);
      with quTmp do
        begin
          SQL.Text:='EXECUTE PROCEDURE MERGEREST';
          ExecQuery;
        end;
      tr.CommitRetaining;
      ReOpenDataSets([taRInv, taRest]);
    end;
   dm3.update_operation(LogOperationID)
end;

procedure TfmRest.siReportClick(Sender: TObject);
begin
//  PrintTag(dm2.taRInvSInvId.AsInteger, FACTURE);
end;

procedure TfmRest.SpeedItem5Click(Sender: TObject);
begin
  with dm2 do
    begin
      try
        Screen.Cursor:=crSQLWait;
        with quRestCost do
          begin
            Active:=False;
            Open;
          end;
      finally
        Screen.Cursor:=crDefault;
      end;

      MessageDialog('����� ��������'+#10#13+
                 '  ������������ ������� '+#10#13+
                 '    � ��������� �����: '+quRestCostSCost0.DisplayText+#10#13+
                 '    � ��������� �����: '+quRestCostCost0.DisplayText+#10#13+
                 '  ��������� �������'+#10#13+
                 '    � ��������� �����: '+quRestCostSCost.DisplayText+#10#13+
                 '    � ��������� �����: '+quRestCostCost.DisplayText
                , mtInformation, [mbOK], 0);
      quRestCost.Active:=False;
    end;
end;

procedure TfmRest.N7Click(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID:string;
begin
  case TMenuItem(Sender).Tag of
  0: LogOperationID:=dm3.insert_operation(sLog_PrintNewTagRest,LogOprIdForm);
  1: LogOperationID:=dm3.insert_operation(sLog_PrintTagRest,LogOprIdForm);
  2: LogOperationID:=dm3.insert_operation(sLog_PrintBlackTagRest,LogOprIdForm);
  3: LogOperationID:=dm3.insert_operation(sLog_PrintBarCodeTagRest,LogOprIdForm);
  end;

  gen_arr(arr, M207IBGrid1, dm2.taRInvSINVID);
  try
    PrintTag(arr, f_tag, TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.N8Click(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_PrintPrordRest,LogOprIdForm);
  
  gen_arr(arr, M207IBGrid1, dm2.taRInvSINVID);
  try
    PrintDocument(arr, f_order);
  finally
    Finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.dg1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  if key = 45 then CheckIns;
end;

procedure TfmRest.SpeedItem6Click(Sender: TObject);
var LogOperationID:string;
begin
  CheckIns;
  LogOperationID:=dm3.insert_operation(sLog_AddArtRest,LogOprIdForm);
  dm2.taRest.Insert;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.CheckIns;
begin
  if lbComp.Items[lbComp.Itemindex] = '*���' then
      raise Exception.Create('�������� �������������');
  if lbMat.Items[lbMat.Itemindex] = '*���' then
    raise Exception.Create('�������� ��������');
  if lbGood.Items[lbGood.Itemindex] = '*���' then
    raise Exception.Create('��������  �����');
  if lbIns.Items[lbIns.Itemindex] = '*���' then
    raise Exception.Create('�������� �������');
  if lbCountry.Items[lbCountry.Itemindex] = '*���' then
    raise Exception.Create('�������� ������');
end;

procedure TfmRest.edFindKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_RETURN: if FSearchEnable then
                    dm2.taRest.Locate('ART', edFind.Text, [loCaseInsensitive, loPartialKey]);
    VK_DOWN : ActiveControl := dg1;
  end;
end;


procedure TfmRest.dg1KeyPress(Sender: TObject; var Key: Char);
begin
  if (dg1.SelectedField.FieldName='W') and (Key in ['0'..'9']) and dg1.EditorMode
  then begin
    PostDataSets([dm2.taRest]);
    dm.WChar:=Key;
    ShowAndFreeForm(TfmRI, Self, TForm(fmRI), True, False);
  end
end;

procedure TfmRest.SpeedButton1Click(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_CompareRest,LogOprIdForm);
  with dm.quTmp do
  begin
    if Active then Close;
    SQL.Text := 'execute procedure Compare_RS';
    ExecQuery;
    Close;
  end;
  ReOpenDataSets([dm2.taRest]);
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.acAddExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_CreateRest,LogOprIdForm);
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taRest]);
      with quTmp do
        begin
          SQL.Text:='EXECUTE PROCEDURE CREATERESTINV';
          ExecQuery;
        end;
      tr.CommitRetaining;
      ReOpenDataSets([taRInv, taRest]);
    end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.acDelRestExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_DelRest,LogOprIdForm);
  with dmcom, dm2 do
  begin
    taRInv.Delete;
    tr.CommitRetaining;
    ReOpenDataSets([taRInv, taRest]);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.acViewRestExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_ViewRest,LogOprIdForm);
  ShowAndFreeForm(TfmRInv, Self, TForm(fmRInv), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmRest.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmRest.acCloseRestExecute(Sender: TObject);
var LogOperationID:string;
begin
  with dm, dm2 do
    begin
      if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
        begin
          LogOperationID:=dm3.insert_operation(sLog_CloseRest,LogOprIdForm);
          with quTmp do
            begin
              SQL.Text:='EXECUTE PROCEDURE CloseInv '+taRInvSInvId.AsString+', 5, '+IntToStr(dmCom.UserId);
              ExecQuery;
            end;
          ReOpenDataSets([taRInv]);
          dm3.update_operation(LogOperationID);
        end;
    end;
end;

procedure TfmRest.acCloseAllRestExecute(Sender: TObject);
var LogOperationID:string;
begin
  with dm, dm2 do
    begin
      if MessageDialog('������� ��� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
        with taRInv do
          try
            DisableControls;
            First;
            LogOperationID:=dm3.insert_operation(sLog_CloseAllRest,LogOprIdForm);
            while NOT EOF do
              begin
                if taRInvIsClosed.AsInteger=0 then
                  with quTmp do
                    begin
                      SQL.Text:='EXECUTE PROCEDURE CloseInv '+taRInvSInvId.AsString+', 5, '+IntToStr(dmCom.UserId);
                      ExecQuery;
                    end;
                Next;
             end;
          finally
            ReOpenDataSets([taRInv]);
            dm3.update_operation(LogOperationID);
            EnableControls;
          end;
    end;
end;

end.






