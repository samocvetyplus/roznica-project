object fmNewArt: TfmNewArt
  Left = 333
  Top = 274
  Caption = #1042#1099#1073#1086#1088' '#1072#1088#1090#1080#1082#1091#1083#1072
  ClientHeight = 522
  ClientWidth = 697
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter8: TSplitter
    Tag = 2
    Left = 46
    Top = 0
    Height = 522
    AutoSnap = False
    MinSize = 20
    ExplicitHeight = 527
  end
  object Splitter4: TSplitter
    Tag = 2
    Left = 98
    Top = 0
    Height = 522
    AutoSnap = False
    MinSize = 20
    ExplicitHeight = 527
  end
  object Splitter5: TSplitter
    Tag = 3
    Left = 147
    Top = 0
    Height = 522
    AutoSnap = False
    MinSize = 20
    ExplicitHeight = 527
  end
  object Splitter9: TSplitter
    Left = 199
    Top = 0
    Height = 522
    ExplicitHeight = 527
  end
  object Splitter10: TSplitter
    Left = 254
    Top = 0
    Height = 522
    ExplicitHeight = 527
  end
  object Splitter11: TSplitter
    Left = 309
    Top = 0
    Height = 522
    Visible = False
    ExplicitHeight = 527
  end
  object Panel1: TPanel
    Left = 380
    Top = 0
    Width = 240
    Height = 522
    Align = alRight
    Alignment = taRightJustify
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 0
      Top = 329
      Width = 240
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 240
      Height = 29
      Align = alTop
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 32
        Height = 13
        Caption = #1055#1086#1080#1089#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object edArt: TEdit
        Left = 44
        Top = 4
        Width = 113
        Height = 21
        Color = clInfoBk
        TabOrder = 0
        Text = 'edArt'
        OnChange = edArtChange
        OnEnter = edArtEnter
        OnKeyDown = edArtKeyDown
      end
    end
    object dgArt: TM207IBGrid
      Left = 0
      Top = 29
      Width = 240
      Height = 300
      Align = alTop
      Color = clBtnFace
      DataSource = dm.dsADict
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      PopupMenu = pmA
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = dgArtKeyDown
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = False
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART'
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 67
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FULLART'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 107
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UNITID'
          PickList.Strings = (
            #1043#1088
            #1064#1090)
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1045#1076'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 32
          Visible = True
        end>
    end
    object M207IBGrid1: TM207IBGrid
      Left = 0
      Top = 332
      Width = 244
      Height = 133
      Color = clBtnFace
      DataSource = dm.dsA2Dict
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      PopupMenu = pmA2
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = False
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART2'
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 206
          Visible = True
        end>
    end
  end
  object Panel3: TPanel
    Left = 620
    Top = 0
    Width = 77
    Height = 522
    Align = alRight
    Alignment = taRightJustify
    TabOrder = 1
    ExplicitLeft = 626
    object btnOk: TButton
      Left = 4
      Top = 12
      Width = 69
      Height = 25
      Caption = 'Ok'
      ModalResult = 1
      TabOrder = 0
    end
    object btnCancel: TButton
      Left = 4
      Top = 44
      Width = 69
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      ModalResult = 2
      TabOrder = 1
    end
  end
  object lbComp: TListBox
    Left = 0
    Top = 0
    Width = 46
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbCountry: TListBox
    Left = 49
    Top = 0
    Width = 49
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 3
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbGood: TListBox
    Left = 101
    Top = 0
    Width = 46
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 4
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbIns: TListBox
    Left = 150
    Top = 0
    Width = 49
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 5
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbMat: TListBox
    Left = 202
    Top = 0
    Width = 52
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 6
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbAtt1: TListBox
    Left = 257
    Top = 0
    Width = 52
    Height = 522
    Align = alLeft
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 7
    Visible = False
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object lbAtt2: TListBox
    Left = 312
    Top = 0
    Width = 68
    Height = 522
    Align = alClient
    Color = clBtnFace
    Constraints.MinWidth = 24
    ItemHeight = 13
    TabOrder = 8
    Visible = False
    OnClick = lbCompClick
    OnKeyPress = lbCompKeyPress
  end
  object pmA2: TPopupMenu
    Left = 525
    Top = 268
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 16430
      OnClick = N2Click
    end
  end
  object pmA: TPopupMenu
    Left = 493
    Top = 108
    object N3: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 16430
      OnClick = N4Click
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 336
    Top = 264
  end
end
