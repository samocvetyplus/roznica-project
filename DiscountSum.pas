unit DiscountSum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, ActnList, PrnDbgeh,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmDiscountSum = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TDBGridEh;
    colordl: TColorDialog;
    acList: TActionList;
    acAdd: TAction;
    acdel: TAction;
    acClose: TAction;
    acPrint: TAction;
    pdg1: TPrintDBGridEh;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure dg1Columns2EditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acAddExecute(Sender: TObject);
    procedure acdelExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmDiscountSum: TfmDiscountSum;

implementation
uses comdata, M207Proc, data, DB;
{$R *.dfm}

procedure TfmDiscountSum.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 with dmCom do
  begin
   dg1.ReadOnly:=not Centerdep;
   SpeedItem2.Enabled:=Centerdep;
   SpeedItem1.Enabled:=Centerdep;
   OpenDataSets([taDiscount, quDiscountSum]);
  end;
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;  
end;

procedure TfmDiscountSum.dg1Columns2EditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
 if (not dmCom.quDiscountSumD_DISCOUNTSUMID.IsNull) and (not dg1.ReadOnly) then
 begin
  if not colordl.Execute then exit;

  if not (dmcom.quDiscountSum.State in [dsEdit, dsInsert]) then
   dmcom.quDiscountSum.Edit;
  dmcom.quDiscountSumCOLOR.AsInteger:=colordl.Color;
  dmcom.quDiscountSum.Post;
  dmcom.quDiscountSum.Refresh;
 end
end;

procedure TfmDiscountSum.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (AnsiUpperCase(Column.FieldName)='COLORFIELD') and (dmCom.quDiscountSumCOLOR.AsInteger<>0) then
  begin
    Background:=dmCom.quDiscountSumCOLOR.AsInteger;
  end
end;

procedure TfmDiscountSum.acAddExecute(Sender: TObject);
begin
 dmcom.quDiscountSum.Insert;
end;

procedure TfmDiscountSum.acdelExecute(Sender: TObject);
begin
 dmcom.quDiscountSum.Delete;
end;

procedure TfmDiscountSum.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmDiscountSum.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 with dmCom do
  begin
   PostDataSets([quDiscountSum]);
   CloseDataSets([quDiscountSum, taDiscount]);
  end;
end;

procedure TfmDiscountSum.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDiscountSum.acPrintExecute(Sender: TObject);
begin
 pdg1.Print;
end;

procedure TfmDiscountSum.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100118)
end;

procedure TfmDiscountSum.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
