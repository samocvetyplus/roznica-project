unit ShopReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ActnList, Grids, DBGridEh, DB, FIBDataSet,
  pFIBDataSet, ComCtrls, Menus, TB2Item, PrnDbgeh, FR_DSet,
  FR_DBSet, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmShopReport = class(TForm)           
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    acEvent: TActionList;
    acExit: TAction;
    gridShopReport: TDBGridEh;
    taShopReport: TpFIBDataSet;
    dsrShopReport: TDataSource;
    taShopReportID: TFIBIntegerField;
    taShopReportSINVID: TFIBIntegerField;
    taShopReportT: TFIBSmallIntField;
    taShopReportDOCNO: TFIBStringField;
    taShopReportDOCDATE: TFIBDateTimeField;
    taShopReportQ: TFIBFloatField;
    taShopReportW: TFIBFloatField;
    taShopReportCOST: TFIBFloatField;
    taShopReportK: TFIBSmallIntField;
    stbrMain: TStatusBar;
    taShopReportTNAME: TFIBStringField;
    spitPrint: TSpeedItem;
    acPrint: TAction;
    fms: TFormStorage;
    acExport: TAction;
    taShopReportERRDOC: TFIBSmallIntField;
    taShopReportCOST2: TFIBFloatField;
    taShopReportCOST3: TFIBFloatField;
    taShopReportBUHREP: TFIBStringField;
    taShopReportDEPSEL: TFIBIntegerField;
    pmgr: TTBPopupMenu;
    biSet: TTBItem;
    biNoSet: TTBItem;
    acSet: TAction;
    acNotSet: TAction;
    taShopReportCOLOR: TFIBIntegerField;
    taShopReportSN: TFIBIntegerField;
    prgrid: TPrintDBGridEh;
    acPrintGrid: TAction;
    siHelp: TSpeedItem;
    pmPrint: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure acExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure taShopReportBeforeOpen(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridShopReportGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure acNotSetUpdate(Sender: TObject);
    procedure acSetExecute(Sender: TObject);
    procedure taShopReportAfterPost(DataSet: TDataSet);
    procedure acNotSetExecute(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmShopReport: TfmShopReport;

implementation

uses comdata, fmUtils, ShopReportList, dbUtil, UtilLib, ReportData, Data;

{$R *.dfm}

procedure TfmShopReport.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmShopReport.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmShopReport.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  OpenDataSet(taShopReport);
  if fmShopReportList.taShopReportListDEPID.AsInteger<>CenterDepId then
  begin
   gridShopReport.FieldColumns['COST'].Title.Caption:='�����';
   gridShopReport.FieldColumns['COST2'].Visible:=false;
   gridShopReport.FieldColumns['COST3'].Visible:=false;
   gridShopReport.FieldColumns['BUHREP'].Visible:=false;
   gridShopReport.FieldColumns['SN'].Visible:=true;
   gridShopReport.AllowedOperations:=[];
  end
  else
  begin
   gridShopReport.FieldColumns['COST'].Title.Caption:='����� � ����.';
   gridShopReport.FieldColumns['COST2'].Visible:=true;
   gridShopReport.FieldColumns['COST3'].Visible:=true;
   gridShopReport.FieldColumns['BUHREP'].Visible:=true;
   gridShopReport.FieldColumns['SN'].Visible:=false;   
   gridShopReport.AllowedOperations:=[alopUpdateEh];
  end
end;

procedure TfmShopReport.taShopReportBeforeOpen(DataSet: TDataSet);
begin
  taShopReport.ParamByName('SINVID').AsInteger := fmShopReportList.taShopReportListSINVID.AsInteger;
end;

procedure TfmShopReport.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmShopReport.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taShopReport);
  Action := caFree;
end;

procedure TfmShopReport.gridShopReportGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(taShopReportT.AsInteger = 0)or(taShopReportT.AsInteger = 999)
  then Background := $00FEECCD;
  if(taShopReportID.IsNull) then Background := clMoneyGreen;
  if(Column.FieldName='DOCNO')and(taShopReportERRDOC.AsInteger=1)then Background := clRed;
  if fmShopReportList.taShopReportListDEPID.AsInteger=CenterDepId then
  begin
   if (taShopReportT.AsInteger=102) or (taShopReportT.AsInteger=105) or
      (taShopReportT.AsInteger=106) or (taShopReportT.AsInteger=107) or
      (taShopReportT.AsInteger=108) then
   begin
    if taShopReportCOLOR.AsInteger=-1 then Background:=clWindow
    else Background:=taShopReportCOLOR.AsInteger
   end
  end 
end;

procedure TfmShopReport.acPrintExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := fmShopReportList.taShopReportListSINVID.AsInteger;
    if fmShopReportList.taShopReportListDEPID.AsInteger=CenterDepId then
     PrintDocument(arr, center_report)
    else PrintDocument(arr, shop_report);
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReport.acNotSetUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (fmShopReportList.taShopReportListDEPID.AsInteger=CenterDepId)
end;

procedure TfmShopReport.acSetExecute(Sender: TObject);
begin
 case taShopReportT.AsInteger of
  102, 105, 107: begin
    taShopReport.Edit;
    taShopReportCOLOR.AsInteger:=$00C080FF;
    taShopReport.Post;
    taShopReport.Refresh;
   end;
  106, 108: begin
    taShopReport.Edit;
    taShopReportCOLOR.AsInteger:=$00FF8080;
    taShopReport.Post;
    taShopReport.Refresh;
   end;
 end;
end;

procedure TfmShopReport.taShopReportAfterPost(DataSet: TDataSet);
begin
 taShopReport.Transaction.CommitRetaining;
end;

procedure TfmShopReport.acNotSetExecute(Sender: TObject);
begin
 case taShopReportT.AsInteger of
  102, 105, 107, 106, 108: begin
    taShopReport.Edit;
    taShopReportCOLOR.AsInteger:=-1;
    taShopReport.Post;
    taShopReport.Refresh;
   end;
 end;
end;

procedure TfmShopReport.acPrintGridExecute(Sender: TObject);
begin
 prgrid.Print;
end;

procedure TfmShopReport.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100431)
end;

procedure TfmShopReport.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmShopReport.N1Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := fmShopReportList.taShopReportListSINVID.AsInteger;
    if fmShopReportList.taShopReportListDEPID.AsInteger=CenterDepId then
     PrintDocument(arr, center_report)
    else PrintDocument(arr, shop_report);
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReport.N2Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := fmShopReportList.taShopReportListSINVID.AsInteger;
    PrintDocument(arr, center_report_com);
  finally
    Finalize(arr);
  end;
end;

procedure TfmShopReport.N3Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  SetLength(arr, 1);
  try
    arr[0] := fmShopReportList.taShopReportListSINVID.AsInteger;
    PrintDocument(arr, center_report_tol);
  finally
    Finalize(arr);
  end;
end;

end.
