unit NSellItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, StdCtrls, PrnDbgeh,
  ActnList, DB, FIBDataSet, pFIBDataSet, jpeg, DBGridEhGrouping, GridsEh,
  rxSpeedbar;

type
  TfmNSellItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    Label1: TLabel;
    siclose: TSpeedItem;
    pdg1: TPrintDBGridEh;
    aclist: TActionList;
    acPrint: TAction;
    DBGridEh1: TDBGridEh;
    pFIBDataSet1: TpFIBDataSet;
    DataSource1: TDataSource;
    pFIBDataSet1DEP_NAME: TFIBStringField;
    pFIBDataSet1UID: TFIBIntegerField;
    pFIBDataSet1RN: TFIBIntegerField;
    pFIBDataSet1DATE_SELL_RET: TFIBDateTimeField;
    pFIBDataSet1COST: TFIBFloatField;
    procedure sicloseClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure DBGridEh1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmNSellItem: TfmNSellItem;

implementation
uses comdata;
{$R *.dfm}

procedure TfmNSellItem.sicloseClick(Sender: TObject);
begin
 close;
end;

procedure TfmNSellItem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 dmcom.quNSellitem.Close;
end;

procedure TfmNSellItem.FormCreate(Sender: TObject);
begin
 tb1.Wallpaper:=wp;
 dmcom.quNSellitem.open;
 label1.Caption:= '���������� ����� � '+trim(dmcom.taClientNODCARD.AsString);
 siclose.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmNSellItem.FormResize(Sender: TObject);
begin
 siclose.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmNSellItem.acPrintExecute(Sender: TObject);
begin
 pdg1.Title.Clear;
 pdg1.Title.Add('��������� ������� � ���������� ������ � '+trim(dmcom.taClientNODCARD.AsString));
 pdg1.Print;
end;

procedure TfmNSellItem.DBGridEh1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dmCom.quNSellitemRET.AsInteger=1 then Background:=clAqua;
  if (Column.Field.FieldName='SNAME') then Background:=dmcom.quNSellitemCOLOR.asinteger;
end;

end.
