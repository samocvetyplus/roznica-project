object fmOptBuyerCase: TfmOptBuyerCase
  Left = 280
  Top = 205
  Width = 535
  Height = 270
  Caption = #1054#1087#1090#1086#1074#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plAll: TPanel
    Left = 0
    Top = 0
    Width = 527
    Height = 202
    Align = alClient
    TabOrder = 0
    object LName: TLabel
      Left = 1
      Top = 1
      Width = 133
      Height = 200
      Align = alLeft
      Caption = #13#10#13#10' '#1054#1087#1090#1086#1074#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LOptBuyer: TListBox
      Left = 134
      Top = 1
      Width = 392
      Height = 200
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnDblClick = LOptBuyerDblClick
      OnKeyDown = LOptBuyerKeyDown
    end
  end
  object plButton: TPanel
    Left = 0
    Top = 202
    Width = 527
    Height = 41
    Align = alBottom
    TabOrder = 1
    object btCancel: TBitBtn
      Left = 432
      Top = 8
      Width = 83
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 0
      Kind = bkCancel
    end
    object btOk: TBitBtn
      Left = 328
      Top = 8
      Width = 91
      Height = 25
      Caption = #1042#1099#1073#1088#1072#1090#1100
      TabOrder = 1
      Kind = bkOK
    end
  end
end
