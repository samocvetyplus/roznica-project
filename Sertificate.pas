unit Sertificate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,DBGridEh, DBGridEhGrouping, rxSpeedbar, ExtCtrls, GridsEh, DB,
  FIBDataSet, pFIBDataSet, StdCtrls, DBCtrls, Mask, DBCtrlsEh, DBLookupEh,
  ActnList, jpeg, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, MsgDialog,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, cxCalc, ImgList, dxBar, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, dxSkinsdxBarPainter;

type
  TEditMode = (emInsert, emDelete, emReadOnly);

  TfmSertificate = class(TForm)
    taSert: TpFIBDataSet;
    dsrSert: TDataSource;
    taSertSERT_ID: TFIBIntegerField;
    taSertNOMINAL: TFIBIntegerField;
    taSertDEPNAME: TFIBStringField;
    taSertSTATENAME: TFIBStringField;
    taSertSTATEDATE: TFIBDateTimeField;
    dsrDep: TDataSource;
    taDep: TpFIBDataSet;
    taDepSNAME: TFIBStringField;
    taDepD_DEPID: TFIBIntegerField;
    taSertD_DEPID: TFIBIntegerField;
    ActionList1: TActionList;
    acDel: TAction;
    taSertCOLOR: TFIBIntegerField;
    acRefresh: TAction;
    GridView: TcxGridDBTableView;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    GridViewSERT_ID: TcxGridDBColumn;
    GridViewNOMINAL: TcxGridDBColumn;
    GridViewDEPNAME: TcxGridDBColumn;
    GridViewSTATENAME: TcxGridDBColumn;
    GridViewSTATEDATE: TcxGridDBColumn;
    taSertCOST: TFIBFloatField;
    GridViewCOLOR: TcxGridDBColumn;
    GridViewCOST: TcxGridDBColumn;
    acExit: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    btnAdding: TdxBarLargeButton;
    btnDeleting: TdxBarLargeButton;
    btnExit: TdxBarLargeButton;
    btnRefresh: TdxBarLargeButton;
    LargeImages: TImageList;
    taSertSTATE: TFIBIntegerField;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionHistory: TAction;
    EditorNominal: TdxBarEdit;
    BarAttribute: TdxBar;
    EditorOffice: TdxBarCombo;
    dxBarLargeButton2: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure taSertAfterDelete(DataSet: TDataSet);
    procedure acRefreshExecute(Sender: TObject);
    procedure GridViewDEPNAMECustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure GridViewCustomDrawColumnHeader(
      Sender: TcxGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
    procedure acExitExecute(Sender: TObject);
    procedure btnAddingClick(Sender: TObject);
    procedure btnDeletingClick(Sender: TObject);
    procedure ActionHistoryUpdate(Sender: TObject);
    procedure ActionHistoryExecute(Sender: TObject);
    procedure dxBarLargeButton2Click(Sender: TObject);
    procedure taSertBeforeClose(DataSet: TDataSet);
  private
    SertificateEditMode: TEditMode;
    LogOprIdForm:string;
  public
    property EditMode: TEditMode read SertificateEditMode;
  end;

var
  fmSertificate: TfmSertificate;
  p:integer;
implementation
 uses Data, ComData, ServData, Data3, frmCertificateHistory, frmCertificateCollation;
{$R *.dfm}

procedure TfmSertificate.acDelExecute(Sender: TObject);
var
  CancelMessage: string;
begin

  Screen.Cursor := crSQLWait;

  CancelMessage := '';

  if taSertSTATE.AsInteger = 0 then
  begin
    CancelMessage := '������';
  end;

  if taSertSTATE.AsInteger = -1 then
  begin
    CancelMessage := '��� �����';
  end;

  if CancelMessage <> '' then
  begin

    MessageDlg('���������� ' + taSertSERT_ID.AsString + ' ' + CancelMessage + '. �������� ����������!', mtWarning, [mbOk], 0);

    Screen.Cursor := crDefault;

    Exit;

  end;

  taSert.Edit;

  taSertSTATE.AsInteger := -1;

  taSert.Post;

  taSert.Transaction.CommitRetaining;

  Screen.Cursor := crDefault;

end;

procedure TfmSertificate.acExitExecute(Sender: TObject);
begin
  taSert.Active:=false;
  taDep.Active:=false;
  close;
end;

procedure TfmSertificate.acRefreshExecute(Sender: TObject);
begin
  taSert.Active:=false;
  taSert.Active:=true;
  taSert.Locate('Sert_id', p, []);
end;




procedure TfmSertificate.GridViewCustomDrawColumnHeader(
  Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
   var  columnId: integer;
       Cellvalue: integer;
begin
 {  columnId:=cxGrid1DBTableView1.GetColumnByFieldName('COLOR').Index;
   CellValue:=AviewInfo.;
   if (cellvalue<>null) then
    begin
      if (CellValue<>-1677721) then ACanvas.Brush.Color:=CellValue;
    end;}
end;

procedure TfmSertificate.GridViewDEPNAMECustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
   var  columnId: integer;
       Cellvalue: Variant;
begin
   columnId:=GridView.GetColumnByFieldName('COLOR').Index;
   CellValue:=AviewInfo.GridRecord.Values[ColumnId];
   if not VarIsNull(Cellvalue) then
    begin
      if (CellValue<>-1677721) then ACanvas.Brush.Color := CellValue;
    end;
end;

procedure TfmSertificate.btnAddingClick(Sender: TObject);
const
  InformationMessage = '�� ���������� � ����� ���������� ������������ �� �����.'#13#10 +
                       '����������?';
begin

  if not btnAdding.Down and (SertificateEditMode = emInsert) then
  begin

    SertificateEditMode := emReadOnly;

    Exit;

  end;

  if MessageDialog(InformationMessage, mtInformation, [mbYes, mbNo],0) = mrYes then
  begin

    btnDeleting.Down := false;

    btnAdding.Down := true;

    SertificateEditMode := emInsert;

  end;
end;

procedure TfmSertificate.btnDeletingClick(Sender: TObject);
const
  InformationMessage = '�� ���������� � ����� �������� ������������ �� ������.'#13#10 +
                       '��� ��������������� � ������ ������ ����������� ����� �������.'#13#10 +
                       '����������?';
begin

  if not btnDeleting.Down and (SertificateEditMode = emDelete) then
  begin

    SertificateEditMode := emReadOnly;

    Exit;

  end;

  if MessageDialog(InformationMessage, mtInformation, [mbYes, mbNo],0) = mrYes then
  begin

    btnDeleting.Down := true;

    btnAdding.Down := false;

    SertificateEditMode := emDelete;

  end;

end;


procedure TfmSertificate.FormCreate(Sender: TObject);

procedure PopulateEditorOffice;
var
  OfficeID: Integer;
  OfficeName: string;
begin
 taDep.First;

 while not taDep.Eof do
 begin
   OfficeID := taDepD_DEPID.AsInteger;

   OfficeName := taDepSNAME.AsString;

   EditorOffice.Items.AddObject(OfficeName, TObject(OfficeID));

   taDep.Next;
 end;
end;

begin

  taSert.Active:=true;

  taDep.Active:=true;

  dm.WorkMode:='Sert';

  if CenterDep and dmCom.User.Adm then
  begin
    PopulateEditorOffice;

    BarAttribute.Visible := True;

  end else
  begin

    btnAdding.Enabled := false;

    btnDeleting.Enabled := false;

  end;

  GridView.Controller.TopRowIndex := 0;

  GridView.Controller.FocusedRowIndex := 0;

  SertificateEditMode := emReadOnly;
end;

procedure TfmSertificate.taSertAfterDelete(DataSet: TDataSet);
begin

  dmCom.tr.CommitRetaining;

end;

procedure TfmSertificate.taSertBeforeClose(DataSet: TDataSet);
begin
 p:=DataSet.FieldByName('Sert_ID').AsInteger;
end;

procedure TfmSertificate.ActionHistoryExecute(Sender: TObject);
var
  ID: Integer;
begin
  ID := taSertSERT_ID.AsInteger;

  TDialogCertificateHistory.Execute(ID);
end;

procedure TfmSertificate.ActionHistoryUpdate(Sender: TObject);
begin
  ActionHistory.Enabled := taSert.RecordCount <> 0;
end;

procedure TfmSertificate.dxBarLargeButton2Click(Sender: TObject);
begin
  DialogCertificateCollation := TDialogCertificateCollation.Create(Self);

  DialogCertificateCollation.Execute;

  FreeAndNil(DialogCertificateCollation);
end;

end.

