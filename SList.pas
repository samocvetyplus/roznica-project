
unit SList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, db, Menus, RxMenus, DBCtrls,
  DepSel, ZReport, ZRCtrls, DBGridEh, ActnList, TB2Item, PrnDbgeh, PrntsEh,
  Printers, FIBQuery, pFIBQuery, FIBDataSet, pFIBDataSet, jpeg,
  DBGridEhGrouping, rxPlacemnt, GridsEh, EhLibIBX, rxSpeedbar, QExport3Dialog;

type
  TfmSList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    laDep: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    siFind: TSpeedItem;
    siRep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    laPeriod: TLabel;
    SpeedItem2: TSpeedItem;
    dg1: TDBGridEh;
    pmmain: TTBPopupMenu;
    IbAdd: TTBItem;
    IbDel: TTBItem;
    IbView: TTBItem;
    IbFind: TTBItem;
    acList: TActionList;
    acAdd: TAction;
    pmAdd: TTBPopupMenu;
    IbSamosvety: TTBItem;
    acDel: TAction;
    acView: TAction;
    acFind: TAction;
    pmprint1: TTBPopupMenu;
    IbPrint: TTBSubmenuItem;
    IbTagmain: TTBItem;
    N14: TTBItem;
    mnitPrList1: TTBItem;
    N15: TTBItem;
    N16: TTBSeparatorItem;
    mnitTagact1: TTBItem;
    mnitactOV1: TTBItem;
    N17: TTBSeparatorItem;
    mnitFacture1: TTBItem;
    mnitInvoice1: TTBItem;
    mnitInvoiceItem1: TTBItem;
    N4: TTBSeparatorItem;
    N5: TTBItem;
    N7: TTBSeparatorItem;
    SInvId1: TTBItem;
    acPrintOrder: TAction;
    acPrintClick: TAction;
    acPrintActOV: TAction;
    acPrintF: TAction;
    acSinvID: TAction;
    mnitOrder: TTBItem;
    mnitPrList: TTBItem;
    mnitF: TTBItem;
    N8: TTBItem;
    N10: TTBItem;
    N11: TTBSeparatorItem;
    mnitactOV: TTBItem;
    N13: TTBSeparatorItem;
    mnitFacture: TTBItem;
    mnitInvoice: TTBItem;
    mnitInvoiceItem: TTBItem;
    acRest: TAction;
    acClose: TAction;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    taComp_SupImport: TpFIBQuery;
    tbsCreateDInv: TTBSubmenuItem;
    TBItem1: TTBItem;
    biSurplus: TTBItem;
    acSurPlus: TAction;
    spitImport: TSpeedItem;
    acImport: TAction;
    lbDep: TLabel;
    siHelp: TSpeedItem;
    tbTagInv: TTBSubmenuItem;
    tbTagAct: TTBSubmenuItem;
    SpeedItem3: TSpeedItem;
    IBSamocvetyTolling: TTBItem;
    DataSetExport: TpFIBDataSet;
    DataSetExportART: TFIBStringField;
    DataSetExportD_GOOD: TFIBStringField;
    DataSetExportMAT: TFIBStringField;
    DataSetExportINS: TFIBStringField;
    DataSetExportPROVIDERS: TFIBStringField;
    DataSetExportPRICE: TFIBFloatField;
    DataSetExportBARCODE: TFIBIntegerField;
    DataSetExportSZ: TFIBStringField;
    DataSetExportW: TFIBFloatField;
    DataSetExportH1: TFIBStringField;
    SpeedItem4: TSpeedItem;
    acExport: TAction;
    ExportDialog: TQExport3Dialog;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acFindExecute(Sender: TObject);
    procedure acPrintOrderExecute(Sender: TObject);
    procedure acPrintClickExecute(Sender: TObject);
    procedure acPrintActOVExecute(Sender: TObject);
    procedure acPrintFExecute(Sender: TObject);
    procedure mnitFacture1Click(Sender: TObject);
    procedure mnitInvoice1Click(Sender: TObject);
    procedure mnitInvoiceItem1Click(Sender: TObject);
    procedure mnitPrList1Click(Sender: TObject);
    procedure acSinvIDExecute(Sender: TObject);
    procedure mnitTagInvClick(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure acRestExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acCreateDInvExecute(Sender: TObject);
    procedure acCreateDInvUpdate(Sender: TObject);
    procedure acPrintOrderUpdate(Sender: TObject);
    procedure mnitTagactClick(Sender: TObject);
    procedure acSurPlusExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure dg1TitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure IBSamocvetyTollingClick(Sender: TObject);
    procedure DataSetExportBeforeOpen(DataSet: TDataSet);
    procedure acExportExecute(Sender: TObject);
  private
    LogOprIdForm: string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  end;

var
  fmSList: TfmSList;

implementation

uses comdata, Data, SInv, SInvFind, ReportData, {ImportData,} {FileCtrl,}
   ErrMsg, RecSet, Period, ServData, M207Proc, Data2, Data3,
  dbUtil, MsgDialog, Variants, SurPlus, SInvImport;

{$R *.DFM}

procedure TfmSList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmSList.FormCreate(Sender: TObject);
var
  it : TTBItem;
  i : integer;
  ac : TAction;
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  dm.SDepId:=CenterDepId;
  dm.SDep:= SelfDepName;
  laDep.Caption:=dm.SDep;
  IbSamosvety.Caption := dmCom.SelfCompName;
  with dmCom, dm do
    begin
      PActAfterSInv:=False;
      tr.Active:=True;
      taRec.Active:=True;
      WorkMode:='SINV';
      DistrT:=2;
      SetBeginDate;
      ShowPeriod;
    end;
   ReOpenDataSet(dm.taSList);
  // ��������� ������ ������������� ����������� ��� ��
  for i:=0 to Pred(dmCom.DepCount) do
  begin
    ac := TAction.Create(acList);
    ac.OnUpdate := acCreateDInvUpdate;
    ac.OnExecute := acCreateDInvExecute;
    ac.Caption := dmCom.DepInfo[i].SName;
    ac.Tag := dmCom.DepInfo[i].DepId;
    it := TTBItem.Create(pmMain);
    it.Action := ac;
    it.Caption := dmCom.DepInfo[i].SName;
    it.Tag := dmCom.DepInfo[i].DepId;
    tbsCreateDInv.Add(it);
  end;

  dmcom.FillTagMenu(tbTagInv, mnitTagInvClick, 1);
  dmcom.FillTagMenu(tbTagAct, mnitTagactClick, 1);

  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmSList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      CloseDataSets([dm.taSList, taRec]);
      tr.CommitRetaining;
    end;
  with dm do
    begin
      WorkMode:='';
    end
end;

procedure TfmSList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSList.IBSamocvetyTollingClick(Sender: TObject);
begin
  dm.IsTolling := 1;
  acAddExecute(IBSamocvetyTolling);
  dm.IsTolling := 0;
end;

procedure TfmSList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then Close;
end;

procedure TfmSList.SpeedItem1Click(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taSList]);
      ShowPeriod;
    end;
end;

procedure TfmSList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;


procedure TfmSList.N10Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr := gen_arr(dg1, dm.taSListSInvId);
  try
    PrintDocument(arr, f_reduce3);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSList.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if dm.taSListCURR_COLOR.AsInteger = 1 then Background:=clPurple else
  begin
  if dm.taSListColor.AsInteger = 0 then
   begin
     if dm.taSListIsClosed.AsInteger=0 then Background:=clInfoBk //��������� �������
     else Background:=clBtnFace;   //��������� �������
   end
  else
   begin
     if Column.FieldName = 'SN' then Background:=clRed //������
     else
      begin
        if dm.taSListIsClosed.AsInteger=0 then Background:=clInfoBk
        else Background:=clBtnFace;
      end;
   end;
   end;
end;

procedure TfmSList.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_SPACE, VK_RETURN: acViewExecute(Sender);
    end;
end;

procedure TfmSList.dg1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var Msg: TMessage;
begin
  TWMMouse(Msg).Pos.x:=-32000;
  TWMMouse(Msg).Pos.y:=-32000;
  Application.HintMouseMessage(dg1, Msg);
  TWMMouse(Msg).Pos.x:=x;
  TWMMouse(Msg).Pos.y:=y;
  Application.HintMouseMessage(dg1, Msg);
end;

procedure TfmSList.dg1TitleBtnClick(Sender: TObject; ACol: Integer;
  Column: TColumnEh);
 begin

 end;
{I:integer;
 begin

 for i := 1 to dg1.columns.count do
 if  dg1.columns[i-1].title.sortMarker <> smNoneEh then
 dm.taSlist.doSort([dg1.columns[i-1].fieldName],[(dg1.columns[i-1].title.sortMarker= smUpEh)]);
 dm.taSList.first;
end; }
{ var i,j:Integer;
  sort_str: String;

begin

  for j:=0 to Column.Grid.Columns.Count-1 do
    if (Column.Grid.Columns[j].Title.SortMarker <> smNoneEh) and (Column.Grid.Columns[j]<>Column)
      then Column.Grid.Columns[j].Title.SortMarker := smNoneEh;
  case Column.Title.SortMarker of
    smNoneEh: Column.Title.SortMarker := smDownEh;
    smDownEh: Column.Title.SortMarker := smUpEh;
    smUpEh: Column.Title.SortMarker := smNoneEh;
  end;

dg1.OptionsEh := dg1.OptionsEh + [dghAutoSortMarking];

for i:=1 to dg1.SortMarkedColumns.Count do
begin

if i=1 then sort_str:=sort_str+dg1.SortMarkedColumns.Items[i-1].Field.FieldName
else sort_str:=sort_str+';'+dg1.SortMarkedColumns.Items[i-1].Field.FieldName;

case Column.Title.SortMarker of
smUpEh :Sort_str:=sort_str+' ASC';
smDownEh :Sort_str:=sort_str+' DESC';
smNoneEh : sort_str:='';
end;
end;

dg1.OptionsEh := dg1.OptionsEh - [dghAutoSortMarking];

dm.taSList.FIELDByName('NAMEPAYTYPE').AsString:=sort_str;
end;    }

procedure TfmSList.acAddExecute(Sender: TObject);
var   SavePlace :TBookmark;
      LogOperationID: string;
begin
 if dm.SDepId<>-1 then
    begin
      {log}

      LogOperationID:=dm3.insert_operation('���������� ��������',LogOprIdForm);
      {***}

      dm.SItype:=1;
      dm.taSList.Append;
      dm.taSList.Post;
      SavePlace := dm.taSList.GetBookmark;
      try
        ShowAndFreeForm(TfmSInv, Self, TForm(fmSInv), True, False);
        dm.taSList.GotoBookmark(SavePlace);
        dm.taSlist.Locate('SINVID',SinvId,[]);
      finally
        dm.taSList.FreeBookmark(SavePlace);
      end;

      {log}
      dm3.update_operation(LogOperationID);
      {***}
    end
  else MessageDialog('���������� ������� �����', mtInformation, [mbOK], 0);
end;


procedure TfmSList.acDelExecute(Sender: TObject);
var LogOperationID: string;         
begin
  Screen.Cursor:=crSQLWait;
 {log}
  LogOperationID:=dm3.insert_operation('�������� ��������',LogOprIdForm);
 {***}

  dm.taSList.Delete;

 {log}
  dm3.update_operation(LogOperationID);
 {***}
  Screen.Cursor:=crDefault;
end;

procedure TfmSList.acExportExecute(Sender: TObject);
begin
  try
    //QExportLocale.LoadDll('QERussian.dll'); //����������� �������
    DataSetExport.Open;
   // ExportDialog.FileName := GetSpecialFolderLocation(CSIDL_Personal) + '\' + dm.taSListSN.AsString;
    ExportDialog.Execute;
    //QExportLocale.UnloadDll;
  finally
    DataSetExport.Close;
  end;
end;

procedure TfmSList.acViewExecute(Sender: TObject);
var SavePlace:TBookMark;
    LogOperationID: string;
begin
 SavePlace := dm.taSList.GetBookmark;
 DmCom.tr.CommitRetaining;
 try
  dm.taSList.Refresh;
  if dm.taSListSINVID.IsNull then
  begin
    MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
    ReOpenDataSet(dm.taSList);
  end
    else
  begin
    if dm.taSListITYPE.AsInteger=1 then
    begin
      LogOperationID:=dm3.insert_operation('�������� ��������',LogOprIdForm);
      ShowAndFreeForm(TfmSInv, Self, TForm(fmSInv), True, False);
    end
      else
    begin
       LogOperationID:=dm3.insert_operation('�������� �������',LogOprIdForm);
       ShowAndFreeForm(TfmSurPlus, Self, TForm(fmSurPlus), True, False);
    end;
  dm3.update_operation(LogOperationID);
  end;
 dm.taSList.GotoBookmark(SavePlace);
 finally
  dm.taSList.FreeBookmark(SavePlace);
 end;
end;


procedure TfmSList.DataSetExportBeforeOpen(DataSet: TDataSet);
begin
 DataSetExport.ParamByName('sinvid').AsInteger := dm.taSListsINVID.AsInteger;
end;

procedure TfmSList.acFindExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmSInvFind, Self, TForm(fmSInvFind), True, False);
end;

procedure TfmSList.acPrintOrderExecute(Sender: TObject);
var
  arr : TarrDoc;
  i:integer;

 function create_fmDepSel : boolean;
 begin
  if CenterDep
  then
  begin
   fmDepSel:=TfmDepSel.Create(NIL);
   try
    with fmDepSel do
    if ShowModal=mrOK
    then result:=true
    else result:=false;
   finally
    fmDepSel.Free;
   end;
  end
  else result:=true;
 end;

begin
  try
   dmcom.flcall_delsel:=1;
   setlength(dmcom.mas_dep,0);
   if create_fmDepSel then
    for i:=0 to length(dmcom.mas_dep)-1 do
    begin
     with dm, qutmp do
     begin
      close;
      sql.Text:='select prordid from prord where depid = '+inttostr(dmcom.mas_dep[i])+
                ' and invid = '+dm.taSListSINVID.AsString;
      ExecQuery;
      if Fields[0].AsInteger<>0 then
      begin
       SetLength(arr,1);
       while not eof do
       begin
        arr[0]:=Fields[0].AsInteger;
        PrintDocument(arr, order);
        next;
       end;
      end
      else MessageDialog('��� ��������', mtInformation, [mbOk], 0);
      close;
     end;
    end
  finally
    Finalize(arr);
  end;
end;

procedure TfmSList.acPrintClickExecute(Sender: TObject);
begin
  case TComponent(Sender).Tag of
    3: begin
         dmReport.KindPrList := 1;
         dmReport.PrintDocumentA(gen_arr(dg1, dm.taSListSInvId), pricelist);
    end;
    10 : dmReport.PrintDocumentA(gen_arr(dg1, dm.taSListSinvId), facture);
    11 : dmReport.PrintDocumentA(gen_arr(dg1, dm.taSListSinvId), invoice);
    12 : dmReport.PrintDocumentA(gen_arr(dg1, dm.taSListSinvId), invoice_u);
  end;
end;

procedure TfmSList.acPrintActOVExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(dg1, dm.taSListSInvId);
  try
    dmReport.ActType := 1;
    PrintDocument(arr, act_uid);
{    if MessageDialog('����������� ����� �� ���� ����������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes
    then begin
     acPrintTagActExecute(Sender);
    end}
  finally
    Finalize(arr);
  end;
end;

procedure TfmSList.acPrintFExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  if dm.taSListSINVID.IsNull then raise Exception.Create('��� ���������');

  arr := gen_arr(dg1, dm.taSListSInvId);
  try
   if TMenuItem(Sender).Tag=0 then PrintDocument(arr, f_reduce)
     else    PrintDocument(arr, f_reduce2);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSList.mnitFacture1Click(Sender: TObject);
begin
  acPrintClickExecute(mnitFacture1)
end;

procedure TfmSList.mnitInvoice1Click(Sender: TObject);
begin
 acPrintClickExecute(mnitInvoice1)
end;

procedure TfmSList.mnitInvoiceItem1Click(Sender: TObject);
begin
 acPrintClickExecute(mnitInvoiceItem1)
end;

procedure TfmSList.mnitPrList1Click(Sender: TObject);
begin
 acPrintClickExecute(mnitPrList1)
end;

procedure TfmSList.acSinvIDExecute(Sender: TObject);
begin
   MessageDialog(dm.taSListSInvId.AsString, mtInformation, [mbOk], 0);
end;

procedure TfmSList.mnitTagInvClick(Sender: TObject);
var
  arr : TarrDoc;
  DocumentID: Integer;
  ContractClassID: Integer;
  IsCommission: Integer;
begin
  DocumentID := dm.taSListSInvId.AsInteger;

  if dm.taSListISCLOSED.AsInteger = 0 then
  begin
    ContractClassID := dm.taSListCONTRACTCLASSID.AsInteger;

    if ContractClassID in [2, 4] then
    begin
      IsCommission := 0;

      if ContractClassID = 2 then
      begin
        IsCommission := 1;
      end;

      dm.trCommission.StartTransaction;

      dm.spSetCommission.ParamByName('SINVID').AsInteger := DocumentID;
      
      dm.spSetCommission.ParamByName('COMMISSION').AsInteger := IsCommission;

      dm.spSetCommission.ExecProc;

      dm.trCommission.Commit;
    end;
  end;

  arr := gen_arr(dg1, dm.taSListSInvId);
  try
    PrintTag(arr, f_tag,TComponent(SEnder).Tag);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSList.N8Click(Sender: TObject);
begin
  acPrintFExecute(N8)
end;

procedure TfmSList.acRestExecute(Sender: TObject);
var s: string;
begin
  with dm, dm2, quInvRestCost do
    begin
      Active:=False;
      Params[0].AsInteger:=taSListSInvId.AsInteger;
      Active:=True;
      s:=Fields[0].DisplayTExt;
      if s='' then s:='��� ��������';
      MessageDialog(s , mtInformation, [mbOK], 0);
      Active:=False;
    end;
end;

procedure TfmSList.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmSList.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdg1.Print;
end;

procedure TfmSList.acCreateDInvExecute(Sender: TObject);
var
  msg : string;
  r : Variant;
begin
  msg := '������������ ��������� �� � '+dm.taSListDEP.AsString+
    ' �� '+TAction(Sender).Caption+#13' �� ��������� ��������� ��������� '+dm.taSListSN.AsString+'?';
  if MessageDialog(msg, mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  try
     r := ExecSelectSQL('select SN from CreateDINV_FROM_SINVID('+dm.taSListDEPID.AsString+','+
      IntToStr(TAction(Sender).Tag)+','+IntToStr(dmCom.UserId)+','+
      dm.taSListSINVID.AsString+')', dm.quTmp);
     if VarIsNull(r) then msg := '����� ����������' else msg := VarToStr(r);
     MessageDialog('������������ ��������� �� �'+msg, mtInformation, [mbOk], 0);
  except
    on E:Exception do
      MessageDialog('��� �������� ��������� �� �������� ������'#13+E.Message, mtError, [mbOk], 0);
  end;
end;

procedure TfmSList.acCreateDInvUpdate(Sender: TObject);
begin
  with (Sender as TAction) do
    Enabled := (Tag <> dm.taSListDEPID.AsInteger)and(dm.taSListISCLOSED.AsInteger=1);
end;

procedure TfmSList.acPrintOrderUpdate(Sender: TObject);
begin
 if dm.taSListSINVID.IsNull then TAction(Sender).Enabled:=false
 else TAction(Sender).Enabled:= dm.taSListITYPE.AsInteger=1;
end;

procedure TfmSList.mnitTagactClick(Sender: TObject);
var
  arr, arr_ : TarrDoc;
  i : integer;
begin
  arr := gen_arr(dg1, dm.taSListSInvId);
  with dmServ do
  try
    if not dmcom.tr.Active then dmcom.tr.StartTransaction;
    for i:=Low(arr) to High(arr) do
    begin
      quAct.Close;
      quAct.SelectSQL.Text := 'select R_Link from Doc_ActOv('+IntToStr(arr[i])+', 1)';
      quAct.Open;
      while not quAct.Eof do
      begin
        SetLength(arr_, Length(arr_)+1);
        arr_[Length(arr_)-1] := quAct.Fields[0].AsInteger;
        quAct.Next;
      end;
    end;
    quAct.Close;
    dmcom.tr.CommitRetaining;
    PrintTag(arr_, act_tag,TComponent(SEnder).Tag);
  finally
    Finalize(arr);
    if Assigned(arr_) then Finalize(arr_);
  end;
end;

procedure TfmSList.acSurPlusExecute(Sender: TObject);
var   SavePlace :TBookmark;
      LogOperationID: string;
begin
  if dm.SDepId<>-1 then
    begin
      {log}
      LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);
      {***}

      dm.SItype:=21;
      dm.taSList.Append;
      dm.taSList.Post;
      dm.taSList.Refresh;
      SavePlace := dm.taSList.GetBookmark;
      try
        ShowAndFreeForm(TfmSurPlus, Self, TForm(fmSurPlus), True, False);
        dm.taSList.GotoBookmark(SavePlace);
      finally
        dm.taSList.FreeBookmark(SavePlace);
      end;

      {log}
      dm3.update_operation(LogOperationID);
      {***}
    end
  else MessageDialog('���������� ������� �����', mtInformation, [mbOK], 0);
end;

procedure TfmSList.acImportExecute(Sender: TObject);
var LogOperationID: string;
begin
  {Log}
  LogOperationID:=dm3.insert_operation('������ ��������',LogOprIdForm);
  ShowAndFreeForm(TfmSInvImport, Self, TForm(fmSInvImport), True, False);
  ReopenDataSet(dm.taSList);
  {Log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100201)
end;

procedure TfmSList.SpeedItem3Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taSList);
  tr.CommitRetaining;
  ReOpenDataSet(taSList);
  taSList.Refresh;
  end;
end;



end.
