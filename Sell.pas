unit Sell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, StdCtrls, DBCtrls, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmSell = class(TForm)
    FormStorage1: TFormStorage;
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    pa1: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    dg1: TM207IBGrid;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmSell: TfmSell;

implementation

uses Data, Data2, comdata, M207Proc;

{$R *.DFM}

procedure TfmSell.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmSell.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSell.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmSell.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dm do
    begin
      SellId:=taSellListSellId.AsInteger;
      OpenDataSets([taSellItem]);
    end;
end;

procedure TfmSell.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight then Background:=clNavy
  else if (Field<>NIL) and
          ((Field.FieldName='SZ') or (Field.FieldName='W') or (Field.FieldName='UID')) then
          if dm.taSellItemSItemId.IsNull then Background:=clRed
          else Background:=clInfoBk;
end;

procedure TfmSell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    begin
      CloseDataSets([taSellItem]);
    end;
end;

procedure TfmSell.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight then Background:=clNavy
  else if (Field<>NIL) then
         if ((Field.FieldName='SZ') or (Field.FieldName='W') or (Field.FieldName='UID')) then
           begin
             if dm.taSellItemSItemId.IsNull then Background:=clRed
             else Background:=clInfoBk
           end
         else
         if Field.FieldName='PRICE' then
           begin
             if Abs(dm.taSellItemPrice0.AsFloat-dm.taSellItemPrice.AsFloat)>1e-6 then Background:=clRed
             else Background:=clInfoBk;
           end;
end;

end.
