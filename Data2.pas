unit Data2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, PFIBDataSet, PFIBQuery, Menus, RxMemDS, PFIBStoredProc, FIBDataSet,
  FIBQuery, fib;

type
  Tdm2 = class(TDataModule)
    quInsTmpP2: TpFIBQuery;
    quInsTmpP2OP: TpFIBQuery;
    quInsGr2PrOrd: TpFIBQuery;
    quGetPrice: TpFIBQuery;
    quGetDiscount: TpFIBQuery;
    quGetUID: TpFIBDataSet;
    quGetUIDSITEMID: TIntegerField;
    quGetUIDSZ: TFIBStringField;
    quGetUIDW: TFloatField;
    quGetUIDUID: TIntegerField;
    quGetUIDART2ID: TIntegerField;
    quGetUIDART2: TFIBStringField;
    quGetUIDFULLART: TFIBStringField;
    quGetUIDUNITID: TIntegerField;
    quGetUIDPRICE: TFloatField;
    pmDiscount: TPopupMenu;
    quMargin2Dep: TpFIBQuery;
    quMargin2Opt: TpFIBQuery;
    quClearTmpP2Dep: TpFIBQuery;
    quClearTmpP2Opt: TpFIBQuery;
    quClearTmpP2All: TpFIBQuery;
    quMargin2Gr: TpFIBQuery;
    quSetTmp2Price: TpFIBQuery;
    quUpdateDistrPrice: TpFIBQuery;
    quBuyer: TpFIBDataSet;
    quBuyerD_COMPID: TIntegerField;
    quBuyerNAME: TFIBStringField;
    dsBuyer: TDataSource;
    quBuyerSNAME: TFIBStringField;
    quWHA2UIDT: TpFIBDataSet;
    dsWHA2UIDT: TDataSource;
    quAllBuyer: TpFIBDataSet;
    quAllBuyerD_COMPID: TIntegerField;
    quAllBuyerNAME: TFIBStringField;
    dsAllBuyer: TDataSource;
    dsRest: TDataSource;
    taRI: TpFIBDataSet;
    taRIID: TIntegerField;
    taRISZ: TFIBStringField;
    taRIW: TFloatField;
    taRIUID: TIntegerField;
    taRIREMAINS: TIntegerField;
    taRIDONE: TSmallintField;
    dsRI: TDataSource;
    dsRInv: TDataSource;
    quProd: TpFIBDataSet;
    quProdD_COMPID: TIntegerField;
    quProdCODE: TFIBStringField;
    quMat: TpFIBDataSet;
    quGood: TpFIBDataSet;
    quGoodD_GOODID: TFIBStringField;
    quIns: TpFIBDataSet;
    quInsD_INSID: TFIBStringField;
    quRestCost: TpFIBDataSet;
    quRestCostCOST: TFloatField;
    quRestCostCOST0: TFloatField;
    quRestCostSCOST: TFloatField;
    quRestCostSCOST0: TFloatField;
    quAllDepSz: TpFIBDataSet;
    quAllDepSzSZ: TFIBStringField;
    quAllDepSzDEP: TFIBStringField;
    quAllDepSzDEPID: TFIBStringField;
    quAllDepSzQUANTITY: TIntegerField;
    quAllDepSzWEIGHT: TFloatField;
    dsAllDepSz: TDataSource;
    quAllDepUID: TpFIBDataSet;
    quAllDepUIDUID: TIntegerField;
    quAllDepUIDW: TFloatField;
    quAllDepUIDSZ: TFIBStringField;
    quAllDepUIDDEP: TFIBStringField;
    dsAllDepUID: TDataSource;
    quAllDepSzCOLOR: TIntegerField;
    quAllDepUIDCOLOR: TIntegerField;
    quBuyerOPTMARGIN: TFloatField;
    dsProd: TDataSource;
    dsMat: TDataSource;
    dsGood: TDataSource;
    dsIns: TDataSource;
    quInsDSItem: TpFIBQuery;
    ibdsWHItem: TpFIBDataSet;
    ibdsWHItemSITEMID: TIntegerField;
    ibdsWHItemSZ: TFIBStringField;
    ibdsWHItemW: TFloatField;
    ibdsWHItemUID: TIntegerField;
    dsrWHItem: TDataSource;
    taRest: TpFIBDataSet;
    taRestID: TIntegerField;
    taRestART2ID: TIntegerField;
    taRestPRICE: TFloatField;
    taRestPRICE2: TFloatField;
    taRestD_COMPID: TIntegerField;
    taRestADATE: TDateTimeField;
    taRestTW: TFloatField;
    taRestN: TFIBStringField;
    taRestNDSID: TIntegerField;
    taRestFULLART: TFIBStringField;
    taRestQ: TFloatField;
    taRestW: TFloatField;
    taRestCOST: TFloatField;
    taRestUNITID: TIntegerField;
    taRestSUP: TFIBStringField;
    taRestNDSNAME: TFIBStringField;
    taRestNDSVALUE: TFloatField;
    taRestART2: TFIBStringField;
    taRestTODO: TSmallintField;
    taRestCOMPID: TIntegerField;
    taRestMATID: TFIBStringField;
    taRestGOODID: TFIBStringField;
    taRestINSID: TFIBStringField;
    taRestART: TFIBStringField;
    taRestMAT: TFIBStringField;
    taRestDONE: TSmallintField;
    taRestGOOD: TFIBStringField;
    taRestINS: TFIBStringField;
    taRestD_ARTID: TIntegerField;
    taRestPROD: TFIBStringField;
    taRestMTO: TSmallintField;
    taRestMFROM: TSmallintField;
    taRestRQ: TFloatField;
    taRestRW: TFloatField;
    taRInv: TpFIBDataSet;
    taRInvSINVID: TIntegerField;
    taRInvSUPID: TIntegerField;
    taRInvDEPID: TIntegerField;
    taRInvSDATE: TDateTimeField;
    taRInvNDATE: TDateTimeField;
    taRInvSSF: TFIBStringField;
    taRInvCOST: TFloatField;
    taRInvISCLOSED: TSmallintField;
    taRInvSUP: TFIBStringField;
    taRInvDEP: TFIBStringField;
    taRInvTW: TFloatField;
    taRInvTQ: TSmallintField;
    taRInvCRDATE: TDateTimeField;
    quInsDUID: TpFIBQuery;
    quGetIns: TpFIBQuery;
    quClient: TpFIBDataSet;
    quClientCLIENTID: TIntegerField;
    quClientNAME: TFIBStringField;
    dsClient: TDataSource;
    quInsClRet1: TpFIBQuery;
    quProdSNAME: TFIBStringField;
    quDepColor: TpFIBQuery;
    quGetUIDRES: TSmallintField;
    quInsSNAME: TFIBStringField;
    quCheckUIDOSelled: TpFIBQuery;
    quSetSP: TpFIBQuery;
    quCol: TpFIBDataSet;
    quColCU_SBD0: TIntegerField;
    quColCU_SBD1: TIntegerField;
    quColCU_R: TIntegerField;
    quColCU_S0: TIntegerField;
    quColCU_S1: TIntegerField;
    quColCU_D0: TIntegerField;
    quColCU_D1: TIntegerField;
    quColCU_RT0: TIntegerField;
    quColCU_RT1: TIntegerField;
    quColCU_SL: TIntegerField;
    quColCU_SO0: TIntegerField;
    quColCU_SO1: TIntegerField;
    quColCU_SR0: TIntegerField;
    quColCU_SR1: TIntegerField;
    dsCol: TDataSource;
    quColCU_RO0: TIntegerField;
    quColCU_RO1: TIntegerField;
    quWHA2UIDTW: TFloatField;
    quMaxSDate: TpFIBQuery;
    quPHist: TpFIBDataSet;
    dsPHist: TDataSource;
    quPHistPRORDID: TIntegerField;
    quPHistPRORD: TIntegerField;
    quPHistSETDATE: TDateTimeField;
    quPHistITYPE: TIntegerField;
    quPHistCLOSEEMPID: TIntegerField;
    quPHistSETEMPID: TIntegerField;
    quPHistCLOSEEMP: TFIBStringField;
    quPHistSETEMP: TFIBStringField;
    quPHistPRICE: TFloatField;
    quPHistPRICE2: TFloatField;
    quPHistOPTPRICE: TFloatField;
    quPHistOLDP: TFloatField;
    quPHistOLDP2: TFloatField;
    quPHistOLDOP: TFloatField;
    quPHistSN: TIntegerField;
    quPHistSDATE: TDateTimeField;
    quPHistSNStr: TStringField;
    quPHistSSF: TFIBStringField;
    quUIDHist: TpFIBDataSet;
    quUIDHistADATE: TDateTimeField;
    quUIDHistOP: TSmallintField;
    quUIDHistSRC: TFIBStringField;
    quUIDHistDST: TFIBStringField;
    quUIDHistDOC: TFIBStringField;
    quUIDHistITYPE: TSmallintField;
    dsUIDHist: TDataSource;
    quUIDHistSOp: TStringField;
    quCheckSupUID: TpFIBQuery;
    quMatD_MATID: TFIBStringField;
    quMatSNAME: TFIBStringField;
    quGoodNAME: TFIBStringField;
    quMatNAME: TFIBStringField;
    quUIDHistRN: TIntegerField;
    quUIDHistCHECKNO: TIntegerField;
    quUIDHistSDoc: TStringField;
    quCheckA2Exist: TpFIBQuery;
    quCheckAExist: TpFIBQuery;
    quUIDPHist: TpFIBDataSet;
    dsUIDPHist: TDataSource;
    quUIDHistPRORDID: TIntegerField;
    quGetA: TpFIBDataSet;
    quGetAFULLART: TFIBStringField;
    quSameUID: TpFIBDataSet;
    quSameUIDUID: TIntegerField;
    quSameUIDCNT: TIntegerField;
    dsSameUID: TDataSource;
    quD_WH2: TpFIBDataSet;
    quD_WH2D_ARTID: TIntegerField;
    quD_WH2FULLART: TFIBStringField;
    quD_WH2QUANTITY: TIntegerField;
    quD_WH2WEIGHT: TFloatField;
    quD_WH2UNITID: TIntegerField;
    quD_WH2ART: TFIBStringField;
    quD_WH2D_COMPID: TIntegerField;
    quD_WH2D_MATID: TFIBStringField;
    quD_WH2D_GOODID: TFIBStringField;
    quD_WH2D_INSID: TFIBStringField;
    quD_WH2PRODCODE: TFIBStringField;
    quD_WH2DQ: TIntegerField;
    quD_WH2DW: TFloatField;
    quD_WH2DEPID: TIntegerField;
    dsD_WH2: TDataSource;
    quD_UID2: TpFIBDataSet;
    quD_UID2SITEMID: TIntegerField;
    quD_UID2UID: TIntegerField;
    quD_UID2W: TFloatField;
    quD_UID2SZ: TFIBStringField;
    quD_UID2PRICE: TFloatField;
    quD_UID2SUP: TFIBStringField;
    quD_UID2SUPID: TIntegerField;
    quD_UID2NDS: TFIBStringField;
    quD_UID2ISCLOSED: TSmallintField;
    quD_UID2ART2: TFIBStringField;
    dsD_UID2: TDataSource;
    quD_SZ2: TpFIBDataSet;
    quD_SZ2SZ: TFIBStringField;
    quD_SZ2QUANTITY: TFloatField;
    quD_SZ2WEIGHT: TFloatField;
    quD_SZ2DQ: TFloatField;
    quD_SZ2DW: TFloatField;
    quD_SZ2T: TSmallintField;
    dsD_SZ2: TDataSource;
    quD_T2: TpFIBDataSet;
    dsD_T2: TDataSource;
    quD_T2QUANTITY: TIntegerField;
    quD_T2WEIGHT: TFloatField;
    quD_T2DQ: TIntegerField;
    quD_T2DW: TFloatField;
    quD_Q2: TpFIBDataSet;
    quD_Q2D_ARTID: TIntegerField;
    quD_Q2DEPID: TIntegerField;
    quD_Q2DEP: TFIBStringField;
    quD_Q2Q1: TIntegerField;
    quD_Q2W1: TFloatField;
    quD_Q2Q2: TIntegerField;
    quD_Q2W2: TFloatField;
    dsD_Q2: TDataSource;
    quDst2: TpFIBDataSet;
    dsDst2: TDataSource;
    quDst2DEP: TFIBStringField;
    quDst2ISCLOSED: TSmallintField;
    quDst2UID: TIntegerField;
    quDst2SITEMID: TIntegerField;
    quDst2W: TFloatField;
    quDst2SZ: TFIBStringField;
    quDst2DEPID: TIntegerField;
    quDst2REF: TIntegerField;
    quDst2COLOR: TIntegerField;
    quDst2ART2: TFIBStringField;
    quD_UID2ART2ID: TIntegerField;
    quGetUIDA2: TpFIBDataSet;
    dsGetUIDA2: TDataSource;
    quInsDUID2: TpFIBQuery;
    quDepPrice: TpFIBQuery;
    quD_WH2RESTQ: TFIBStringField;
    quD_WH2RESTW: TFIBStringField;
    quDepById: TpFIBQuery;
    quDst2PRICE2: TFloatField;
    quInvRestCost: TpFIBDataSet;
    quInvRestCostRESTSUM: TFloatField;
    quRestQ: TpFIBDataSet;
    quRestQDEPID: TIntegerField;
    quRestQDEP: TFIBStringField;
    quRestQQ: TIntegerField;
    dsRestQ: TDataSource;
    quUIDPrice: TpFIBDataSet;
    quUIDPricePRICE2: TFloatField;
    quDst2NDS: TFIBStringField;
    quDistredT: TpFIBDataSet;
    dsDistredT: TDataSource;
    quDistredTQ: TIntegerField;
    quDistredTW: TFloatField;
    quDistredTCOST: TFloatField;
    quDistredTSCOST: TFloatField;
    taItBuf: TpFIBDataSet;
    taItBufITBUFID: TIntegerField;
    taItBufUSERID: TIntegerField;
    taItBufUID: TIntegerField;
    taItBufW: TFloatField;
    taItBufSZ: TFIBStringField;
    taItBufART: TFIBStringField;
    taItBufART2: TFIBStringField;
    taItBufPRODCODE: TFIBStringField;
    taItBufD_MATID: TFIBStringField;
    taItBufD_GOODID: TFIBStringField;
    taItBufD_INSID: TFIBStringField;
    dsItBuf: TDataSource;
    taItBufART2ID: TIntegerField;
    taItBufSPRICE: TFloatField;
    taItBufPRICE: TFloatField;
    quD_WH_T: TpFIBDataSet;
    quD_WH_TTQ: TFIBStringField;
    quD_WH_TTA: TFIBStringField;
    quD_WH_TTW: TFIBStringField;
    dsD_WH_T: TDataSource;
    taInvMarg: TpFIBDataSet;
    taInvMargINVMARGID: TIntegerField;
    taInvMargDEPID: TIntegerField;
    taInvMargINVID: TIntegerField;
    taInvMargMARGIN: TFloatField;
    taInvMargROUNDNORM: TSmallintField;
    taInvMargSNAME: TFIBStringField;
    dsInvMarg: TDataSource;
    quColCU_IM0: TIntegerField;
    quColCU_IM1: TIntegerField;
    quCheckOpenPr: TpFIBDataSet;
    quNewCl: TpFIBQuery;
    quUIDPHistPRICE2: TFloatField;
    quUIDPHistOLDP2: TFloatField;
    quUIDPHistPRORDID: TIntegerField;
    quUIDPHistCLOSEEMP: TFIBStringField;
    quUIDPHistSETEMP: TFIBStringField;
    quUIDPHistPRICE: TFloatField;
    quUIDPHistOPTPRICE: TFloatField;
    quUIDPHistOLDP: TFloatField;
    quUIDPHistOLDOP: TFloatField;
    quUIDPHistITYPE: TIntegerField;
    quUIDPHistSN: TIntegerField;
    quUIDPHistSINVID: TIntegerField;
    quUIDPHistSSF: TFIBStringField;
    quUIDPHistDEPNAME: TFIBStringField;
    quUIDPHistCOLOR: TIntegerField;
    quUIDPHistSNstr: TStringField;
    quUIDPHistPRORD: TFIBStringField;
    quUIDPHistCLOSEDATE: TDateTimeField;
    quSetInvRState: TpFIBQuery;
    quSetPrOrdRState: TpFIBQuery;
    quSetPActRState: TpFIBQuery;
    quSetSellRState: TpFIBQuery;
    quFRest: TpFIBDataSet;
    dsFRest: TDataSource;
    quFRestD_ARTID: TIntegerField;
    quFRestART: TFIBStringField;
    quFRestFULLART: TFIBStringField;
    quFRestRQ: TIntegerField;
    quFRestRW: TFloatField;
    quFRestD_MATID: TFIBStringField;
    quFRestD_GOODID: TFIBStringField;
    quFRestD_INSID: TFIBStringField;
    quFRestPRODCODE: TFIBStringField;
    quCountry: TpFIBDataSet;
    dsCountry: TDataSource;
    quCountryD_COUNTRYID: TFIBStringField;
    quCountryNAME: TFIBStringField;
    quD_WH2D_COUNTRYID: TFIBStringField;
    quRDInv: TpFIBDataSet;
    quRDInvSINVID: TIntegerField;
    quRDInvDEPID: TIntegerField;
    quRDInvSN: TIntegerField;
    quRPrOrd: TpFIBDataSet;
    quRPrOrdPRORDID: TIntegerField;
    quRPrOrdDEPID: TIntegerField;
    quRPrOrdPRORD: TIntegerField;
    quRPAct: TpFIBDataSet;
    quRPActPRORDID: TIntegerField;
    quRPActDEPID: TIntegerField;
    quRPActPRORD: TIntegerField;
    quRPActNACT: TIntegerField;
    quRSell: TpFIBDataSet;
    quRSellSELLID: TIntegerField;
    quRRet: TpFIBDataSet;
    quRRetSINVID: TIntegerField;
    quRRetSN: TIntegerField;
    quD_WH3: TpFIBDataSet;
    dsD_WH3: TDataSource;
    quD_Q3: TpFIBDataSet;
    dsD_Q3: TDataSource;
    quDst3: TpFIBDataSet;
    quDst3DEP: TFIBStringField;
    quDst3ISCLOSED: TSmallintField;
    quDst3UID: TIntegerField;
    quDst3SITEMID: TIntegerField;
    quDst3W: TFloatField;
    quDst3SZ: TFIBStringField;
    quDst3DepID: TIntegerField;
    quDst3REF: TIntegerField;
    quDst3Color: TIntegerField;
    quDst3ART2: TFIBStringField;
    dsDst3: TDataSource;
    quD_WH3D_ARTID: TIntegerField;
    quD_WH3FULLART: TFIBStringField;
    quD_WH3QUANTITY: TIntegerField;
    quD_WH3WEIGHT: TFloatField;
    quD_WH3UNITID: TIntegerField;
    quD_WH3ART: TFIBStringField;
    quD_WH3SZ: TFIBStringField;
    quD_WH3D_COMPID: TIntegerField;
    quD_WH3D_MATID: TFIBStringField;
    quD_WH3D_GOODID: TFIBStringField;
    quD_WH3D_INSID: TFIBStringField;
    quD_WH3D_COUNTRYID: TFIBStringField;
    quD_WH3PRODCODE: TFIBStringField;
    quD_WH3DQ: TIntegerField;
    quD_WH3DW: TFloatField;
    quD_WH3DEPID: TIntegerField;
    quD_WH3RESTQ: TFIBStringField;
    quD_WH3RESTW: TFIBStringField;
    quD_Q3D_ARTID: TIntegerField;
    quD_Q3DEPID: TIntegerField;
    quD_Q3DEP: TFIBStringField;
    quD_Q3Q1: TIntegerField;
    quD_Q3W1: TFloatField;
    quD_Q3Q2: TIntegerField;
    quD_Q3W2: TFloatField;
    quDst3PRICE2: TFloatField;
    quDst3NDS: TFIBStringField;
    quACount: TpFIBDataSet;
    quSItemIType: TpFIBDataSet;
    quSItemITypeITYPE: TSmallintField;
    quRSellRN: TIntegerField;
    quCheckUnclosedInv: TpFIBDataSet;
    quCheckArtOnFil: TpFIBDataSet;
    quCheckArtOnFilEX: TSmallintField;
    quCheckARt2OnFil: TpFIBDataSet;
    SmallintField1: TSmallintField;
    quFSZRest: TpFIBDataSet;
    quFSZRestD_ARTID: TIntegerField;
    quFSZRestART: TFIBStringField;
    quFSZRestFULLART: TFIBStringField;
    quFSZRestD_MATID: TFIBStringField;
    quFSZRestD_GOODID: TFIBStringField;
    quFSZRestD_INSID: TFIBStringField;
    quFSZRestPRODCODE: TFIBStringField;
    quFSZRestRQ: TIntegerField;
    quFSZRestRW: TFloatField;
    quFSZRestSZ: TFIBStringField;
    quRecalcPrOrdQ: TpFIBQuery;
    quGetAD_ARTID: TIntegerField;
    quGetAD_COMPID: TIntegerField;
    quGetAD_INSID: TFIBStringField;
    quGetAD_MATID: TFIBStringField;
    quGetAD_GOODID: TFIBStringField;
    quGetAD_COUNTRYID: TFIBStringField;
    taRestcountryid: TStringField;
    quClientADDRESS: TFIBStringField;
    quClientNODCARD: TFIBStringField;
    quRSINFO: TpFIBDataSet;
    quSetSInfoRState: TpFIBQuery;
    quRSINFODEPID: TIntegerField;
    quRSINFOSN: TIntegerField;
    quRPConfirm: TpFIBDataSet;
    quRPConfirmPRORDID: TIntegerField;
    quRPConfirmDEPID: TIntegerField;
    quRPConfirmPRORD: TIntegerField;
    quRPConfirmNACT: TIntegerField;
    quUIDHistINSCOUNTER: TIntegerField;
    quRSINFOSINFOID: TIntegerField;
    quClientADDRESSID: TIntegerField;
    quClientHOME_FLAT: TFIBStringField;
    dsaddress: TDataSource;
    quaddress: TpFIBDataSet;
    quaddressD_ADDRESS_ID: TIntegerField;
    quaddressADDRESS: TFIBStringField;
    quRUid_info: TpFIBDataSet;
    quClientDEPID: TIntegerField;
    quFSZRestSQ1: TFIBStringField;
    quFSZRestSW1: TFIBStringField;
    quFRestSQ1: TFIBStringField;
    quFRestSW1: TFIBStringField;
    quRDelRec: TpFIBDataSet;
    quSetDelRec: TpFIBQuery;
    quRDelRecDELRECORDSID: TIntegerField;
    quRDelRecDEPID: TIntegerField;
    quRPActFACT: TSmallintField;
    quUIDHistSPRICE: TFloatField;
    quUIDHistPRICE: TFloatField;
    quUIDHistSSF: TFIBStringField;
    quUIDHistNDATE: TDateTimeField;
    quRSINFOREP_SINFOID: TIntegerField;
    quGrById: TpFIBQuery;
    quTmp: TpFIBQuery;
    quClientFMAIN: TIntegerField;
    quGetAATT1: TFIBIntegerField;
    quGetAATT2: TFIBIntegerField;
    taAtt1: TpFIBDataSet;
    taAtt2: TpFIBDataSet;
    dsAtt1: TDataSource;
    dsAtt2: TDataSource;
    taAtt1ID: TFIBIntegerField;
    taAtt1NAME: TFIBStringField;
    taAtt1SNAME: TFIBStringField;
    taAtt1SORTIND: TFIBIntegerField;
    taAtt2ID: TFIBIntegerField;
    taAtt2NAME: TFIBStringField;
    taAtt2SNAME: TFIBStringField;
    taAtt2SORTIND: TFIBIntegerField;
    quD_WH2ATT1: TFIBIntegerField;
    quD_WH2ATT2: TFIBIntegerField;
    quD_WH3ATT1: TFIBIntegerField;
    quD_WH3ATT2: TFIBIntegerField;
    quD_WH3FLAG: TFIBIntegerField;
    quD_WH3RESTA: TFIBStringField;
    quClientFDEL: TFIBSmallIntField;
    quRprOrd2: TpFIBDataSet;
    quRprOrd2PRORDID: TFIBIntegerField;
    quRprOrd2DEPID: TFIBIntegerField;
    quRprOrd2PRORD: TFIBIntegerField;
    quFRestSZ: TFIBStringField;
    quClientBIRTHDAY: TFIBDateTimeField;
    quCheckDeleteAct: TpFIBQuery;
    quBirthday: TpFIBQuery;
    quRBeforePrord: TpFIBDataSet;
    quRBeforePrordSNVP: TFIBIntegerField;
    quRBeforePrordSTPRORD: TFIBStringField;
    quRBeforePAct: TpFIBDataSet;
    quRBeforePActSNVP: TFIBIntegerField;
    quRBeforePActSTPRORD: TFIBStringField;
    quColCU_AO: TFIBIntegerField;
    quColCU_AC: TFIBIntegerField;
    quRActAllowances: TpFIBDataSet;
    quRActAllowancesSINVID: TFIBIntegerField;
    quRActAllowancesSN: TFIBIntegerField;
    quRActAllowancesDEPFROMID: TFIBIntegerField;
    quSetActAllowances: TpFIBQuery;
    quRApplDep: TpFIBDataSet;
    quRApplDepSINVID: TFIBIntegerField;
    quRApplDepDEPID: TFIBIntegerField;
    quRApplDepSN: TFIBIntegerField;
    quSetApplDEpRState: TpFIBQuery;
    quRApplDepArt: TpFIBDataSet;
    quRApplDepArtSINVID: TFIBIntegerField;
    quRApplDepArtDEPID: TFIBIntegerField;
    quRApplDepArtSN: TFIBIntegerField;
    quUIDHistISCLOSED: TFIBSmallIntField;
    quUIDHistStateInv: TStringField;
    quGetUIDA2ART2ID: TFIBIntegerField;
    quGetUIDA2D_ARTID: TFIBIntegerField;
    quGetUIDA2FULLART: TFIBStringField;
    quGetUIDA2ART2: TFIBStringField;
    quRApplDepDEPFROMID: TFIBIntegerField;
    quRAppl: TpFIBDataSet;
    quRApplAPPLID: TFIBIntegerField;
    quRApplNOAPPL: TFIBIntegerField;
    quSetAppl: TpFIBQuery;
    quSameUidP: TpFIBDataSet;
    dsSameUidP: TDataSource;
    quSameUidPUID: TFIBIntegerField;
    quSameUidPPRICE: TFIBFloatField;
    quSameUidPPRICEINV: TFIBFloatField;
    quClientOLDNODCARD: TFIBStringField;
    quColCU_SI: TFIBIntegerField;
    quRSuspItem: TpFIBDataSet;
    quRSuspItemSINVID: TFIBIntegerField;
    quRSuspItemDEPID: TFIBIntegerField;
    quRSuspItemSN: TFIBIntegerField;
    quD_UID2FREPAIR: TFIBSmallIntField;
    quColCU_RO: TFIBIntegerField;
    quColCU_RC: TFIBIntegerField;
    quRPactC: TpFIBDataSet;
    quRPactCPRORDID: TFIBIntegerField;
    quRPactCDEPID: TFIBIntegerField;
    quRPactCPRORD: TFIBIntegerField;
    quRPactCNACT: TFIBIntegerField;
    quRBuyInvoice: TpFIBDataSet;
    quRBuyOrder: TpFIBDataSet;
    quRBuyOrderID: TFIBIntegerField;
    quRBuyOrderN: TFIBIntegerField;
    quRBuyOrderADATE: TFIBDateTimeField;
    quRBuyOrderUSERID: TFIBIntegerField;
    quRBuyOrderMDATE: TFIBDateTimeField;
    quRBuyOrderDEPID: TFIBIntegerField;
    quRBuyOrderSTATE: TFIBIntegerField;
    quRBuyInvoiceCLASSCODE: TFIBIntegerField;
    quRBuyInvoiceN: TFIBIntegerField;
    quRBuyInvoiceID: TFIBIntegerField;
    quRBuyOrderCR_DATE: TFIBDateTimeField;
    quRBuyInvoiceSOURCEDEPARTMENTID: TFIBIntegerField;
    quUIDHistCONTRACT: TFIBStringField;

    procedure DataModuleCreate(Sender: TObject);
    procedure DiscountClick(Sender: TObject);
    procedure quWHA2UIDTBeforeOpen(DataSet: TDataSet);
    procedure ibePrEventAlert(Sender: TObject; EventName: String;
      EventCount: Integer; var CancelAlerts: Boolean);
    procedure taRestBeforeOpen(DataSet: TDataSet);
    procedure taRestUNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taRIBeforeDelete(DataSet: TDataSet);
    procedure taRIAfterDelete(DataSet: TDataSet);
    procedure taRINewRecord(DataSet: TDataSet);
    procedure taRIBeforeOpen(DataSet: TDataSet);
    procedure taRIBeforePost(DataSet: TDataSet);
    procedure taRestUNITIDSetText(Sender: TField; const Text: String);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure quRestCostBeforeOpen(DataSet: TDataSet);
    procedure quAllDepSzBeforeOpen(DataSet: TDataSet);
    procedure quAllDepUIDBeforeOpen(DataSet: TDataSet);
    procedure taRestAfterInsert(DataSet: TDataSet);
    procedure taRestBeforePost(DataSet: TDataSet);
    procedure ibdsWHItemBeforeOpen(DataSet: TDataSet);
    procedure ibdsWHItemAfterDelete(DataSet: TDataSet);
    procedure ibdsWHItemBeforeDelete(DataSet: TDataSet);
    procedure ibdsWHTmpUNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure ibdsWHTmpUNITIDSetText(Sender: TField; const Text: String);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taRestNewRecord(DataSet: TDataSet);
    procedure quPHistBeforeOpen(DataSet: TDataSet);
    procedure quPHistCalcFields(DataSet: TDataSet);
    procedure quUIDHistCalcFields(DataSet: TDataSet);
    procedure quD_WH2BeforeOpen(DataSet: TDataSet);
    procedure quD_UID2FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure quD_UID2BeforeOpen(DataSet: TDataSet);
    procedure quD_SZ2BeforeOpen(DataSet: TDataSet);
    procedure quD_UID2AfterOpen(DataSet: TDataSet);
    procedure quD_Q2BeforeOpen(DataSet: TDataSet);
    procedure quDst2BeforeOpen(DataSet: TDataSet);
    procedure quD_WH2CalcFields(DataSet: TDataSet);
    procedure quRestQBeforeOpen(DataSet: TDataSet);
    procedure taItBufNewRecord(DataSet: TDataSet);
    procedure taItBufBeforeInsert(DataSet: TDataSet);
    procedure taItBufBeforeOpen(DataSet: TDataSet);
    procedure quD_WH_TBeforeOpen(DataSet: TDataSet);
    procedure quD_WH_TTQGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taInvMargBeforeOpen(DataSet: TDataSet);
    procedure taInvMargROUNDNORMGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taInvMargROUNDNORMSetText(Sender: TField;
      const Text: String);
    procedure quFRestCalcFields(DataSet: TDataSet);
    procedure quRDInvBeforeOpen(DataSet: TDataSet);
    procedure quRPrOrdBeforeOpen(DataSet: TDataSet);
    procedure quRPActBeforeOpen(DataSet: TDataSet);
    procedure quRSellBeforeOpen(DataSet: TDataSet);
    procedure quRRetBeforeOpen(DataSet: TDataSet);
    procedure quD_WH3BeforeOpen(DataSet: TDataSet);
    procedure quD_WH3CalcFields(DataSet: TDataSet);
    procedure quD_Q3BeforeOpen(DataSet: TDataSet);
    procedure quFRestBeforeOpen(DataSet: TDataSet);
    procedure quSameUIDBeforeOpen(DataSet: TDataSet);
    procedure quClientNewRecord(DataSet: TDataSet);
    procedure quRPConfirmBeforeOpen(DataSet: TDataSet);
    procedure quFSZRestCalcFields(DataSet: TDataSet);
    procedure quD_WH2AfterRefresh(DataSet: TDataSet);
    procedure quInsDSItemExecuteError(pFIBQuery: TpFIBQuery; E: EFIBError;
      var Action: TDataAction);
    procedure quaddressNewRecord(DataSet: TDataSet);
    procedure quRprOrd2BeforeOpen(DataSet: TDataSet);
    procedure quRBeforePrordBeforeOpen(DataSet: TDataSet);
    procedure quRBeforePActBeforeOpen(DataSet: TDataSet);
    procedure quRActAllowancesBeforeOpen(DataSet: TDataSet);
    procedure quRApplDepBeforeOpen(DataSet: TDataSet);
    procedure quRApplDepArtBeforeOpen(DataSet: TDataSet);
    procedure quSameUidPBeforeOpen(DataSet: TDataSet);
    procedure quRSuspItemBeforeOpen(DataSet: TDataSet);
    procedure quRPactCBeforeOpen(DataSet: TDataSet);
    procedure quDst2BeforePost(DataSet: TDataSet);
    procedure quDst3BeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    lDCol: TList;
  public
    (*********************************************************)
    NEWSINVID: integer;
    slDepSname: TStringList;
    slDepDepId: TStringList;
    slGrId: TStringList;
    slGrname: TStringList;
    function GetPrice(DepId: integer; Art2Id: integer): extended;
    function GetDiscount(DiscountId: integer): extended;
    function GetDepColor(DepId: integer): TColor;
    function CheckUnclosedInv: boolean;
    procedure CheckUnclosedAppl;
    function CheckDeleteAct: boolean;
    function CheckUnConfirmedPrord: boolean;
    procedure CheckArtOnFil(ArtId: integer);
    procedure CheckArt2OnFil(Art2Id: integer);

    { Public declarations }
  end;

var
  dm2: Tdm2;

implementation

uses comdata, RxStrUtils, DBTree, Rest, M207Proc, StrUtils, Data3, MsgDialog,
  frmUnConfirmedOrders, Data;

{$R *.DFM}

function Tdm2.GetDiscount(DiscountId: integer): extended;
begin
  with quGetDiscount do
    begin
      Params.ByName['DISCOUNTID'].AsInteger:=DiscountId;
      ExecQuery;
      Result:=Fields[0].AsFloat;
      Close;
    end;
end;


function Tdm2.GetPrice(DepId: integer; Art2Id: integer): extended;
begin
  with quGetPrice do
    begin
      Params.ByName['DEPID'].AsInteger:=DepId;
      Params.ByName['ART2ID'].AsInteger:=Art2Id;
      ExecQuery;      
      Result:=Fields[0].AsFloat;
      Close;
    end;
end;

procedure Tdm2.DataModuleCreate(Sender: TObject);
var mi: TMenuItem;
    nd: TNodeData;
    fi: TIntegerField;
    ff: TFloatField;
    SName: string;
    DepId: integer;
    DepIds, s: string;
    b:boolean;
begin
  b := False;

  with dmCom, dm, quTmp do
    try
      b:=tr.Active;
      if not b then tr.StartTransaction;
      SQL.Text:='SELECT D_DISCOUNTID, NAME, DISCOUNT FROM D_DISCOUNT ORDER BY DISCOUNT';
      ExecQuery;
      while NOT EOF do
        begin
          mi:=TMenuItem.Create(pmDiscount);
          mi.Tag:=Fields[0].AsInteger;
          mi.Caption:=DelRSpace(Fields[1].AsString);
          mi.OnClick:=DiscountClick;
          pmDiscount.Items.Add(mi);
          Next;
        end;
      Close;

      lDCol:=TList.Create;

      SQL.Text:='SELECT D_DEPID, COLOR FROM D_DEP';
      ExecQuery;
      while NOT EOF do
        begin
          nd:=TNodeData.Create;
          nd.Code:=Fields[0].AsInteger;
          nd.Name:=Fields[1].AsString;
          lDCol.Add(nd);
          Next;
        end;
      Close;
      quCol.Active:=True;
      CU_SBD0:=quColCU_SBD0.AsInteger;
      CU_SBD1:=quColCU_SBD1.AsInteger;
      CU_R:=quColCU_R.AsInteger;
      CU_S0:=quColCU_S0.AsInteger;
      CU_S1:=quColCU_S1.AsInteger;
      CU_D0:=quColCU_D0.AsInteger;
      CU_D1:=quColCU_D1.AsInteger;
      CU_RT0:=quColCU_RT0.AsInteger;
      CU_RT1:=quColCU_RT1.AsInteger;
      CU_SL:=quColCU_SL.AsInteger;
      CU_SO0:=quColCU_SO0.AsInteger;
      CU_SO1:=quColCU_SO1.AsInteger;
      CU_SR0:=quColCU_SR0.AsInteger;
      CU_SR1:=quColCU_SR1.AsInteger;
      CU_RO0:=quColCU_RO0.AsInteger;
      CU_RO1:=quColCU_RO1.AsInteger;
      CU_IM0:=quColCU_IM0.AsInteger;
      CU_IM1:=quColCU_IM1.AsInteger;
      CU_AO:=quColCU_AO.AsInteger;
      CU_AC:=quColCU_AC.AsInteger;
      CU_SI:=quColCU_SI.AsInteger;
      CU_RO:=quColCU_RO.AsInteger;
      quCol.Active:=False;

      slDepSname:=TStringList.Create;
      slDepDepId:=TStringList.Create;
      with quDepById do
        begin
          ExecQuery;
          while NOT EOF do
            begin
              DepIds:=DelRSpace(Fields[0].AsString);
              DepId:=StrToInt(DepIds);
              SName:=DelRSpace(Fields[1].AsString);
              slDepDepId.Add(DepIds);
              nd:=TNodeData.Create;
              nd.Code:=Fields[0].AsInteger;
              nd.Name:=DelRSpace(Fields[1].AsString);
              slDepSName.AddObject(DelRSpace(Fields[1].AsString), nd);

              ff:=TFloatField.Create(quD_WH2);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quD_WH2;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quD_WH2);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quD_WH2;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              fi:=TIntegerField.Create(quD_WH2);
              fi.FieldName:='A_'+DepIds;
              fi.DataSet:=quD_WH2;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;


              //17.10.02
              ff:=TFloatField.Create(quD_WH3);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quD_WH3;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quD_WH3);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quD_WH3;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;   //

              fi:=TIntegerField.Create(quD_WH3);
              fi.FieldName:='A_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quD_WH3;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;   //

              (*****************������������� �� ������ ���������***************)
              ff:=TFloatField.Create(quD_WH);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quD_WH;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quD_WH);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quD_WH;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              fi:=TIntegerField.Create(quD_WH);
              fi.FieldName:='A_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quD_WH;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;
              (*****************************************************************)
              if Centerdep then
              begin
               fi:=TIntegerField.Create(dm.taSellItem);
               fi.FieldName:='APPLQ_'+DepIds;
               fi.DisplayLabel:=SName;
               fi.DataSet:=dm.taSellItem;
               fi.Tag:=DepId;
               fi.FieldKind:=fkCalculated;
              end;

              ff:=TFloatField.Create(dm.taSellItem);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=dm.taSellItem;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(dm.taSellItem);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=dm.taSellItem;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              ff:=TFloatField.Create(dm.taSellItem);
              ff.FieldName:='RWA_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=dm.taSellItem;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(dm.taSellItem);
              fi.FieldName:='RQA_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=dm.taSellItem;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              (**********************************************)
              ff:=TFloatField.Create(quFRest);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quFRest;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quFRest);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quFRest;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;


              ff:=TFloatField.Create(quFSZRest);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quFSZRest;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quFSZRest);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quFSZRest;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;


              (**********************************************)

              ff:=TFloatField.Create(quDInvItem);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quDInvItem;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quDInvItem);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quDInvItem;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              Next;
            end;
          Close;
        end;


////////////////
      slGRId:=TStringList.Create;
      slGrName:=TStringList.Create;
      with quGrById do
        begin
          ExecQuery;
          while NOT EOF do
            begin
              DepIds:=DelRSpace(Fields[0].AsString);
              DepId:=StrToInt(DepIds);
              SName:=DelRSpace(Fields[1].AsString);
              slGrId.Add(DepIds);
              slGrName.Add(SName);

              ff:=TFloatField.Create(quUidWhSumGr);
              ff.FieldName:='RW_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quUidWhSumGr;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.##';

              fi:=TIntegerField.Create(quUidWhSumGr);
              fi.FieldName:='RQ_'+DepIds;
              fi.DisplayLabel:=SName;
              fi.DataSet:=quUidWhSumGr;
              fi.Tag:=DepId;
              fi.FieldKind:=fkCalculated;

              ff:=TFloatField.Create(quUidWhSumGr);
              ff.FieldName:='RP_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quUidWhSumGr;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.###';

              ff:=TFloatField.Create(quUidWhSumGr);
              ff.FieldName:='RP2_'+DepIds;
              ff.DisplayLabel:=SName;
              ff.DataSet:=quUidWhSumGr;
              ff.Tag:=DepId;
              ff.FieldKind:=fkCalculated;
              ff.DisplayFormat:='0.###';

              Next;
            end;
          Close;
        end;

////////////////
      with quTmp do
        begin
          s:=' in (';
          SQL.Text:='select NCode from R_Dep where UseDep=1';
          ExecQuery;
          while not EOF do
            begin
              s:=s+Fields[0].AsString+',';
              Next;
            end;
          Close;
          s:=s+'-999) ';
          quRPrOrd.SelectSQL[7]:=' DepID '+s;
          quRDInv.SelectSQL[7]:=' DepID '+s;
          quRUid_info.SelectSQL[7]:=' s.DepID '+s;
          quRSINFO.SelectSQL[3]:=' DepID '+s;
          quRDelRec.SelectSQL[3]:='and s.DepID '+s;
          quRActAllowances.SelectSQL[7]:=' and s.DepFromID '+s;
          quRApplDep.SelectSQL[7]:=' and s.DepID '+s;
          quRApplDepArt.SelectSQL[7]:=' and s.DepID '+s;
          quRPactC.SelectSQL[3]:=' and DepID '+s;                                        
        end;

    finally
      if b then tr.CommitRetaining else  tr.CommitRetaining;
    end;
end;

procedure Tdm2.DiscountClick(Sender: TObject);
begin
  with dm, taSellItem, dm2 do
    begin
      if NOT (State in [dsInsert, dsEdit]) then Edit;
//      taSellItemPrice.AsFloat:=taSellItemPrice0.AsFloat*(1-GetDiscount(TMenuItem(Sender).Tag)/100);
      taSellItemDiscount.AsFloat:=GetDiscount(TMenuItem(Sender).Tag);
    end;
end;


procedure Tdm2.quWHA2UIDTBeforeOpen(DataSet: TDataSet);
var DID: integer;
    A2ID: integer;
begin
  DID := 0;
  A2ID := 0;

  with dm do
    begin
      if Sells then
        begin
          DID:=SDepId;
          A2ID:=quD_WHArt2Id.AsInteger
        end
      else
        if WorkMode='' then
          begin
            DID:=SDepId;
          end
        else if WorkMode='DINV' then
               begin
                 DID:=taDListDepFromId.AsInteger;
                 A2ID:=taSElArt2Id.AsInteger;
               end
             else if WorkMode='OPT' then
               begin
                 DID:=taOptListDepFromId.AsInteger;
                 A2ID:=taSElArt2Id.AsInteger;
               end;

      with TpFIBDataSet(DataSet).Params do
        begin
          ByName['ART2ID'].AsInteger:=A2ID;
          if DID=-1 then
            begin
              ByName['DEPID1'].AsInteger:=-MAXINT;
              ByName['DEPID2'].AsInteger:=MAXINT;
            end
          else
            begin
              ByName['DEPID1'].AsInteger:=DID;
              ByName['DEPID2'].AsInteger:=DID;
            end;

          if SupFilter then
            begin
              ByName['SUPID1'].AsInteger:=SupFilterId;
              ByName['SUPID2'].AsInteger:=SupFilterId;
            end
          else
            begin
              ByName['SUPID1'].AsInteger:=-MAXINT;
              ByName['SUPID2'].AsInteger:=MAXINT;
            end;

          if PriceFilter then
            begin
              ByName['P1'].AsFloat:=PriceFilterValue;
              ByName['P2'].AsFloat:=PriceFilterValue;
            end
          else
            begin
              ByName['P1'].AsFloat:=-MAXINT;
              ByName['P2'].AsFloat:=MAXINT;
            end
        end;
      end;

end;

procedure Tdm2.ibePrEventAlert(Sender: TObject; EventName: String;
  EventCount: Integer; var CancelAlerts: Boolean);
begin
  MessageDialog('���� ����������� ��������� ���!!!', mtInformation, [mbOK], 0);
  with dm do
    begin
      dmCom.tr.CommitRetaining;
      with taSEl do
        begin
          Active:=False;
          Open;
        end;

      with taA2 do
        begin
          Active:=False;
          Open;
        end;

      with taSetPrice do
        if Active then
          begin
            Active:=False;
            Open;
          end;
    end;

end;

procedure Tdm2.taRestBeforeOpen(DataSet: TDataSet);
begin
   dm.SetAFilter(taRest.Params);
   taRest.ParamByName('USERID').Value := dmcom.UserId;
end;

procedure Tdm2.taRestUNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure Tdm2.taRIBeforeDelete(DataSet: TDataSet);
begin
  dm.DelConf(DataSet);
end;

procedure Tdm2.taRIAfterDelete(DataSet: TDataSet);
begin
  dm.CommitRetaining(DataSet);
  taRest.Refresh;
end;

procedure Tdm2.taRINewRecord(DataSet: TDataSet);
begin
  taRIRemains.AsInteger:=taRestID.AsInteger;
  taRIID.AsInteger:=dmCom.GetId(28);
  taRIW.AsFloat:=0;
  taRISZ.AsString:='-';
  taRIUID.AsInteger:=dmCom.GetId(17);
end;

procedure Tdm2.taRIBeforeOpen(DataSet: TDataSet);
begin
  taRI.Params[0].AsInteger:=taRestID.AsInteger;
end;

procedure Tdm2.taRIBeforePost(DataSet: TDataSet);
begin
  if taRISZ.IsNull then raise exception.create('���������� ������ �������� � ���� ������');
  if taRIW.IsNull then raise exception.create('���������� ������ �������� � ���� ���');
  if taRestUnitId.AsInteger=1 then
    if taRIW.AsFloat=0  then raise exception.create('���������� ������ �������� � ���� ���');
end;

procedure Tdm2.taRestUNITIDSetText(Sender: TField; const Text: String);
begin
  if Text='��' then Sender.AsInteger:=0
  else Sender.AsInteger:=1;
end;

procedure Tdm2.CommitRetaining(DataSet: TDataSet);
begin
  dmCom.tr.CommitRetaining;
end;

procedure Tdm2.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSets([DataSet]);
end;

procedure Tdm2.DelConf(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure Tdm2.quRestCostBeforeOpen(DataSet: TDataSet);
begin
   dm.SetAFilter(quRestCost.Params);
end;

procedure Tdm2.quAllDepSzBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Params[0].AsInteger:=dm.taSElD_ArtId.AsInteger;
end;

procedure Tdm2.quAllDepUIDBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Params[0].AsInteger:=dm.taSElD_ArtId.AsInteger;
end;

procedure Tdm2.taRestAfterInsert(DataSet: TDataSet);
begin
{  taRestD_CompID.AsInteger := TNodeData(fmRest.lbComp.Items.Objects[fmRest.lbComp.ItemIndex]).Code;
  taRestInsId.AsString := TNodeData(fmRest.lbIns.Items.Objects[fmRest.lbIns.ItemIndex]).Code;
  taRestGOODID.AsString := TNodeData(fmRest.lbGood.Items.Objects[fmRest.lbGood.ItemIndex]).Code;
  taRestMATID.AsString := TNodeData(fmRest.lbMat.Items.Objects[fmRest.lbMat.ItemIndex]).Code;
  taRestPRICE.Value := 0;
  taRestPRICE2.Value := 0;
  taRestNDSID.Value := 0;
  taRestADATE.AsString := '01.01.2001';
  taRestUNITID.Value := 0;
  taRestCOMPID.AsInteger := taRestD_CompID.AsInteger;}
end;

procedure Tdm2.taRestBeforePost(DataSet: TDataSet);
begin
  if Dataset.State = dsInsert then
  begin
    if taRestPRICE.AsFloat = 0 then
      raise Exception.Create('������� ��������� ����');
    if taRestPrice2.AsFloat = 0 then
      raise Exception.Create('������� ��������� ����');
    if taRestN.AsString = '' then
      raise Exception.Create('������� ����� ��������� ���������');
    {if taRestNDSID.AsInteger = 0 then
      raise Exception.Create('������� ������� ��� (0, 1 ��� 2)');} 
  end;
end;


procedure Tdm2.ibdsWHItemBeforeOpen(DataSet: TDataSet);
begin
  with ibdsWHItem do
  begin
    Params[0].AsInteger := taRestART2ID.AsInteger;
    Params[1].AsTimeStamp := DateTimeToTimeStamp(taRestADATE.Asdatetime);
    Params[2].AsString := taRestN.AsString;
    Params[3].AsInteger := taRestD_COMPID.AsInteger;
  end;
end;


procedure Tdm2.ibdsWHItemAfterDelete(DataSet: TDataSet);
begin
  dm.CommitRetaining(DataSet);
  taRest.Refresh;
end;

procedure Tdm2.ibdsWHItemBeforeDelete(DataSet: TDataSet);
begin
  dm.DelConf(DataSet);
end;

procedure Tdm2.ibdsWHTmpUNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure Tdm2.ibdsWHTmpUNITIDSetText(Sender: TField; const Text: String);
begin
  if Text='��' then Sender.AsInteger:=0
  else  Sender.AsInteger:=1;
end;

procedure Tdm2.DataModuleDestroy(Sender: TObject);
begin
  lDCol.Free;
  slDepSname.Free;
  slDepDepId.Free;
  slGrId.Free;
  slGrName.Free;
end;

function Tdm2.GetDepColor(DepId: integer): TColor;
var i: integer;
begin
  for i:=0 to lDCol.Count-1 do
    with TNodeData(lDCol.Items[i]) do
      if Code=DepId then
        begin
          Result:=StrToInt(TNodeData(lDCol.Items[i]).Name);
          Exit;
        end;
  Result := clInfoBk;

end;


procedure Tdm2.taRestNewRecord(DataSet: TDataSet);
begin
  taRestID.AsInteger := dmcom.GetID(29);
  taRestD_CompID.AsInteger := TNodeData(fmRest.lbComp.Items.Objects[fmRest.lbComp.ItemIndex]).Code;
  taRestInsId.AsString := TNodeData(fmRest.lbIns.Items.Objects[fmRest.lbIns.ItemIndex]).Code;
  taRestGOODID.AsString := TNodeData(fmRest.lbGood.Items.Objects[fmRest.lbGood.ItemIndex]).Code;
  taRestMATID.AsString := TNodeData(fmRest.lbMat.Items.Objects[fmRest.lbMat.ItemIndex]).Code;
  taRestCountryID.AsString := TNodeData(fmRest.lbCountry.Items.Objects[fmRest.lbCountry.ItemIndex]).Code;
  taRestPRICE.Value := 0;
  taRestPRICE2.Value := 0;
  taRestNDSID.Value := 0;
  taRestADATE.AsString := '01.01.2001';
  taRestUNITID.Value := 0;
  taRestCOMPID.AsInteger := taRestD_CompID.AsInteger;
end;

procedure Tdm2.quPHistBeforeOpen(DataSet: TDataSet);
begin
  with quPHist, Params do
    begin
      ByName['ART2ID'].AsInteger:=dm.PrHistArt2Id;
      ByName['DEPID'].AsInteger:=dm.PrHistDepId;
    end;
end;

procedure Tdm2.quPHistCalcFields(DataSet: TDataSet);
var s: string;
begin
  s:=DataSet.FieldByName('SN').AsString;
  case DataSet.FieldByName('IType').AsInteger of
      1: s:=s+' ��';
      2: s:=s+' ��';
      5: s:=DataSet.FieldByName('SSF').AsString+' ���';
      6: s:=s+' ��';
     10: s:='�����������';
     11: s:='��������� ��.';
     12: s:='��������� ���.';
     13: s:='����. ���. ���. �� ������';
    end;
{      1: s:='�� '+s;
      2: s:='�� '+s;
      5: s:='��� '+DataSet.FieldByName('SSF').AsString;
    end;                         }
  DataSet.FieldByName('SNStr').AsString:=s;
{  s:=quPHistSN.AsString;
  case quPHistIType.AsInteger of
      1: s:='�� '+s;
      2: s:='�� '+s;
      5: s:='��� '+quPHistSSF.AsString;
    end;
  quPHistSNStr.AsString:=s;}
end;

procedure Tdm2.quUIDHistCalcFields(DataSet: TDataSet);
begin
  case quUIDHistOp.AsInteger of
      1: case quUIDHistIType.AsInteger of
             1: quUIDHistSOp.AsString:='��������';
             2: quUIDHistSOp.AsString:='���������� �����������';
             3: quUIDHistSOp.AsString:='������� �������';
             5: quUIDHistSOp.AsString:='�������';
             8: quUIDHistSOp.AsString:='������� �������';
             10: quUIDHistSOp.AsString:='���������� �������';
             19: quUIDHistSOp.AsString:='��� ��������';
             21: quUIDHistSOp.AsString:='�������';
           end;
      2: quUIDHistSOp.AsString:='������� ����������';
      3: case quUIDHistIType.AsInteger of
             0: quUIDHistSOp.AsString:='��������� �������';
             1: quUIDHistSOp.AsString:='��������� �������';
           end;
      4: quUIDHistSOp.AsString:='����������';
      5: quUIDHistSOp.AsString:='���. ��������';
      6: quUIDHistSOp.AsString:='������';
      7: quUIDHistSOp.AsString:='����� � ���. ��.';
     end;
 case quUIDHistISCLOSED.AsInteger of
 1: case quUIDHistOp.AsInteger of
     1,2, 6: quUIDHistStateInv.AsString:='��������� �������';
     3,5: quUIDHistStateInv.AsString:='������� �������';
     4: quUIDHistStateInv.AsString:='��� ���������� ������';
    end;
 0: case quUIDHistOp.AsInteger of
     1,2,6: quUIDHistStateInv.AsString:='��������� �������';
     3,5: quUIDHistStateInv.AsString:='������� �������';
     4: quUIDHistStateInv.AsString:='��� ���������� ������';
    end;
 2: quUIDHistStateInv.AsString:='��������� �� �������';
 end;

  if quUIDHistOp.AsInteger in [1, 2, 4, 5, 6, 7] then quUIDHistSDoc.AsString:=quUIDHistDoc.AsString
  else quUIDHistSDoc.AsString:=quUIDHistRN.AsString+'/'+quUIDHistCheckNo.AsString;
end;

procedure Tdm2.quD_WH2BeforeOpen(DataSet: TDataSet);
var DepId,nPos_: integer;
    s: string;
    fl_seach:boolean;
    s_seach :string;
    len_seach, i: integer;
    num_z:integer;
begin
  DepId := 0;

  nPos_:=NPos('*',dm.D_WHArt,1);
  fl_seach:=false;
  if (Length(dm.D_WHArt)>0) and (nPos_>0) then
   begin
    if ((NPos('*',dm.D_WHArt,2)>0)or(NPos_<>1)) then
    begin
     quD_WH2.SelectSQL[3]:='D_WH2L';
     {������� ��������� � ������}
     num_z:=0;
     for i:=1 to length(dm.D_WHArt) do if dm.D_WHArt[i]='*' then inc(num_z);
     {����������� ����� ������ ����������� %}
     len_seach:=(20-length(dm.D_WHArt))div num_z;
     s_seach:='';
     for i:=1 to len_seach do s_seach:=s_seach+'%';
     fl_seach:=true;
     s:=ReplaceStr(dm.D_WHArt,'*',s_seach);
    end
    else
    begin
     quD_WH2.SelectSQL[3]:='D_WH2R';
     s:=ReverseString(Copy(dm.D_WHArt, 2, MAXINT));
    end;
   end
  else
   begin
     quD_WH2.SelectSQL[3]:='D_WH2';
     s:=dm.D_WHArt;
   end;

  quD_WH2.Prepare;

  if dm.Sells then DepId:=dm.SDepId
   else if dm.Distr then
     begin
       if dm.DstOne2Many then DepId:=dm.DstDepId
        else DepId:=dm.taDListDEPFROMID.AsInteger;
      end
        else if dm.WorkMode='DINV' then DepId:=dm.taDListDepFromId.AsInteger
             else if dm.WorkMode='OPT' then DepId:=dm.taOptListDepFromId.AsInteger
                  else if dm.WorkMode='SRET' then DepId:=dm.taSRetListDepFromId.AsInteger;

   quD_WH2.ParamByName('ADEPID').AsInteger:=DepId;
   dmcom.SetArtFilter(quD_WH2.Params);

   quD_WH2.ParamByName('USERID').AsInteger:=dmCom.UserId;
   quD_WH2.ParamByName('ART_1').AsString:=s;
   if fl_seach then quD_WH2.ParamByName('ART_2').AsString:=s
   else quD_WH2.ParamByName('ART_2').AsString:=s+'��';
end;

procedure Tdm2.quD_UID2FilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  Accept:=True;
  if dm.SupFilter then Accept:= Accept and (quD_UID2SupId.AsInteger=dm.SupFilterId);
  if dm.PriceFilter then Accept:= Accept and (Abs(quD_UID2Price.AsFloat-dm.PriceFilterValue)<0.0001);
  if dm.SZFilter then Accept:=Accept and (quD_UID2SZ.AsString=dm.SZFilterValue);
end;

procedure Tdm2.quD_UID2BeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with dm, TpFIBDataSet(DataSet), Params do
    begin

      if DstOne2Many then DepId:= DstDepId
      else DepId:= taDListDepFromId.AsInteger;

      ByName['DEPID'].AsInteger:=DepID;
      ByName['D_ARTID'].AsInteger:=DstD_ArtId;

      if DataSet=quD_T2 then
        begin
          if SupFilter then
            begin
              ByName['SUPID1'].AsInteger:=SupFilterId;
              ByName['SUPID2'].AsInteger:=SupFilterId;
            end
          else
            begin
              ByName['SUPID1'].AsInteger:=-MAXINT;
              ByName['SUPID2'].AsInteger:=MAXINT;
            end;

          if PriceFilter then
            begin
              ByName['P1'].AsFloat:=PriceFilterValue-0.0001;
              ByName['P2'].AsFloat:=PriceFilterValue+0.0001;
            end
          else
            begin
              ByName['P1'].AsFloat:=-MAXINT;
              ByName['P2'].AsFloat:=MAXINT;
            end;
          if SZFilter then
            begin
              ByName['SZ1'].AsString:=SZFilterValue;
              ByName['SZ2'].AsString:=SZFilterValue;
            end
          else
            begin
              ByName['SZ1'].AsString:='!';
              ByName['SZ2'].AsString:='��';
            end;
        end
      else
        begin
          if WorkMode='SINV' then ByName['IsClosed'].AsInteger:=0
          else ByName['IsClosed'].AsInteger:=1;
        end;
    end;

end;

procedure Tdm2.quD_SZ2BeforeOpen(DataSet: TDataSet);
var DID: integer;
begin
  with dm do
    if WorkMode='SINV' then DID:=DstDepId
    else
      if Distr then DID:=DstDepId
      else DID:=taDListDepFromID.AsInteger;

  with dm, quD_SZ2, Params do
    begin
      ByName['DEPID'].AsInteger:=DID;
      ByName['D_ARTID'].AsInteger:=DstD_ArtID;
      if WorkMode='SINV' then ByName['INVID'].AsInteger:=taSListSInvID.AsInteger
      else ByName['INVID'].IsNull:=True;
    end;

end;

procedure Tdm2.quD_UID2AfterOpen(DataSet: TDataSet);
begin
  ReopenDataSets([quD_T2]);
end;

procedure Tdm2.quD_Q2BeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with dm, quD_Q2, Params do
    begin
      ByName['D_ARTID'].AsInteger:=DstD_ArtId;
      if DstOne2Many then DepId:=DstDepId
      else DepId:= taDListDepFromId.AsInteger;
      ByName['DEPFROMID'].AsInteger:=DepId;
    end;
end;

procedure Tdm2.quDst2BeforeOpen(DataSet: TDataSet);
var DepId: integer;          //quDst2,quDst3,
begin
  with dm, TpFIBDataSet(DataSet).Params do
    begin
      if DstOne2Many then
        begin
          DepId:=DstDepId;
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          DepId:=taDListDepFromId.AsInteger;
          ByName['DEPID1'].AsInteger:=taDListDepId.AsInteger;
          ByName['DEPID2'].AsInteger:=taDListDepId.AsInteger;
        end;
      ByName['DepFromId'].AsInteger:=DepId;
      ByName['D_ArtId'].AsInteger:=DstD_ArtId;
      try
        ByName['ASZ'].AsString:=SZFilterValue;
      except
      end;
    end;

end;

procedure Tdm2.quD_WH2CalcFields(DataSet: TDataSet);
var
  i,q: integer;
  qs, ws: string;
begin
  qs:=quD_WH2RestQ.AsString;
  ws:=ReplaceStr(quD_WH2RestW.AsString, '.', ',');
  for i:=0 to slDepDepId.Count-1 do
    begin
      DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(dm.EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
      q:=StrToInt(dm.EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=q;
      if q<>0 then DataSet.FieldByName('A_'+slDepDepId[i]).AsInteger:=1
      else DataSet.FieldByName('A_'+slDepDepId[i]).AsInteger:=0;
    end;
end;

procedure Tdm2.quRestQBeforeOpen(DataSet: TDataSet);
begin
  with dmCom, TpFIBDataSet(DataSet) do
    begin
      SetArtFilter(Params);
      Params.ByName['USERID'].AsInteger:=UserId;
    end;
end;

procedure Tdm2.taItBufNewRecord(DataSet: TDataSet);
begin
  taItBufItBufId.AsInteger:=dmCom.GetId(33);
  taItBufUserId.AsInteger:=dmCom.UserId;
  taItBufUID.AsInteger:=dm.taSItemUID.AsInteger;
  taItBufSZ.AsString:=dm.taSItemSS.AsString;
  taItBufW.AsFloat:=dm.taSItemW.AsFloat;
  taItBufArt2Id.AsInteger:=dm.taSElArt2Id.AsInteger;
  taItBufSPrice.AsFloat:=dm.taSElPrice.AsFloat;
  taItBufPrice.AsFloat:=dm.taSElPRICE2.AsFloat;
end;

procedure Tdm2.taItBufBeforeInsert(DataSet: TDataSet);
begin
  if dm.taSItemSITEMID.IsNull then SysUtils.Abort;
end;

procedure Tdm2.taItBufBeforeOpen(DataSet: TDataSet);
begin
  taItBuf.Params[0].AsInteger:=dmCom.UserId;
end;

procedure Tdm2.quD_WH_TBeforeOpen(DataSet: TDataSet);
var s: string; nPos_:integer;
begin
  nPos_:=NPos('*',dm.D_WHArt,1);
  if (Length(dm.D_WHArt)>0) and (nPos_>0) then
    begin
     if ((NPos('*',dm.D_WHArt,2)>0)or(NPos_<>1)) then
      begin
       TpFIBDataSet(DataSet).SelectSQL[2]:='WHL_T';
       s:=ReplaceStr(dm.D_WHArt,'*','%%');
      end
     else
      begin
       TpFIBDataSet(DataSet).SelectSQL[2]:='WHR_T';
       s:=ReverseString(Copy(dm.D_WHArt, 2, MAXINT));
      end;
    end
   else
    begin
     TpFIBDataSet(DataSet).SelectSQL[2]:='WH_T';
     s:=dm.D_WHArt;
    end;

    TpFIBDataSet(DataSet).Prepare;

    dmCom.SetArtFilter(TpFIBDataSet(DataSet).Params);
    with dmCom, dm, TpFIBDataSet(DataSet), Params do
    begin
     ByName['ART1'].AsString := s;
     ByName['ART2'].AsString := s+'��';
    end;
end;

procedure Tdm2.quD_WH_TTQGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
var
  i : integer;
begin
  Text :='';
  i:=1;
  while ExtractWord(i, Sender.AsString, [';'])<>'' do
  begin
    Text := Text + dmcom.DepInfo[i-1].SSName + ': '+ExtractWord(i, Sender.AsString, [';'])+'  ';
    inc(i);
  end;
end;

procedure Tdm2.taInvMargBeforeOpen(DataSet: TDataSet);
begin
  taInvMarg.Params[0].AsInteger:=dm.taSListSInvId.AsInteger;
end;

procedure Tdm2.taInvMargROUNDNORMGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  dm.taDepMarginROUNDNORMGetText(Sender, Text, DisplayText);
end;

procedure Tdm2.taInvMargROUNDNORMSetText(Sender: TField;
  const Text: String);
begin
  dm.taDepMarginROUNDNORMSetText(Sender, Text);
end;


procedure Tdm2.quFRestCalcFields(DataSet: TDataSet);
var i: integer;
    qs, ws: string;
begin
  qs:=DataSet.FieldByName('SQ1').AsString;
  ws:=ReplaceStr(DataSet.FieldByName('SW1').AsString, '.', ',');
  with dm, dm2 do
    for i:=0 to slDepDepId.Count-1 do
      begin
        DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
        DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      end;
end;

procedure Tdm2.quRDInvBeforeOpen(DataSet: TDataSet);
begin
  with quRDInv, Params do
    begin
      ByName['DEPFROMID'].AsInteger:=SelfDepId;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
    end;
end;

procedure Tdm2.quRPrOrdBeforeOpen(DataSet: TDataSet);
begin
   with quRPrOrd, Params do
     begin
       ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
       ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
     end;
end;

procedure Tdm2.quRPActBeforeOpen(DataSet: TDataSet);
begin
  with quRPAct, Params do
    begin
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
    end

end;

procedure Tdm2.quRSellBeforeOpen(DataSet: TDataSet);
begin
   with quRSell, Params do
     begin
       ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
       ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
       ByName['DEPID'].AsInteger:=SelfDepId;
     end;

end;

procedure Tdm2.quRRetBeforeOpen(DataSet: TDataSet);
begin
  with quRRet, Params do
    begin
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
      ByName['DEPID'].AsInteger:=SelfDepId;
    end;
end;

procedure Tdm2.quD_WH3BeforeOpen(DataSet: TDataSet);
var DepId,nPos_: integer;
    s: string;
    fl_seach:boolean;
    s_seach :string;
    len_seach, i: integer;
    num_z:integer;
begin
  DepId := 0;
  fl_seach := False;
 nPos_:=NPos('*',dm.D_WHArt,1);
 if (Length(dm.D_WHArt)>0) and (nPos_>0) then
   begin
    if ((NPos('*',dm.D_WHArt,2)>0)or(NPos_<>1)) then
    begin
     quD_WH3.SelectSQL[3]:='D_WH3L';
     {������� ��������� � ������}
     num_z:=0;
     for i:=1 to length(dm.D_WHArt) do if dm.D_WHArt[i]='*' then inc(num_z);
     {����������� ����� ������ ����������� %}
     len_seach:=(20-length(dm.D_WHArt))div num_z;
     s_seach:='';
     for i:=1 to len_seach do s_seach:=s_seach+'%';
     fl_seach:=true;
     s:=ReplaceStr(dm.D_WHArt,'*',s_seach);
    end
    else
    begin
     quD_WH3.SelectSQL[3]:='D_WH3R';
     s:=ReverseString(Copy(dm.D_WHArt, 2, MAXINT));
    end;
   end
   else
   begin
    quD_WH3.SelectSQL[3]:='D_WH3';
    s:=dm.D_WHArt;
   end;

   quD_WH3.Prepare;
   if dm.Sells then DepId:=dm.SDepId
   else if dm.Distr then
    begin
     if dm.DstOne2Many then DepId:=dm.DstDepId
     else DepId:=dm.taDListDEPFROMID.AsInteger;
    end
       else if dm.WorkMode='DINV' then DepId:=dm.taDListDepFromId.AsInteger
            else if dm.WorkMode='OPT' then DepId:=dm.taOptListDepFromId.AsInteger
                 else if dm.WorkMode='SRET' then DepId:=dm.taSRetListDepFromId.AsInteger;

   dmCom.SetArtFilter(quD_WH3.Params);
   
   quD_WH3.ParamByName('ADEPID').AsInteger:=DepId;
   quD_WH3.ParamByName('ART_1').AsString:=s;
   if fl_seach then quD_WH3.ParamByName('ART_2').AsString:=s
   else quD_WH3.ParamByName('ART_2').AsString:=s+'��';
end;

procedure Tdm2.quD_WH3CalcFields(DataSet: TDataSet);
var i: integer;
     qs, ws, as_: string;
begin
  qs:=quD_WH3RestQ.AsString;
  as_:=ReplaceStr(quD_WH3RestA.AsString, '.', ',');
  ws:=ReplaceStr(quD_WH3RestW.AsString, '.', ',');
  for i:=0 to slDepDepId.Count-1 do
    begin
      DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(dm.EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
      DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(dm.EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      DataSet.FieldByName('A_'+slDepDepId[i]).AsInteger:=
        round(StrToFloat(dm.EmptyStrToZero(ExtractWord(i+1, as_, [';']))));
    end;
end;

procedure Tdm2.quD_Q3BeforeOpen(DataSet: TDataSet);
var DepId: integer;
begin
  with dm, quD_Q3, Params do
    begin
      ByName['D_ARTID'].AsInteger:=DstD_ArtId;
      ByName['ASZ'].AsString:=SZFilterValue;
      if DstOne2Many then DepId:=DstDepId
      else DepId:= taDListDepFromId.AsInteger;
      ByName['DEPFROMID'].AsInteger:=DepId;
    end;
end;

function Tdm2.CheckUnConfirmedPrord: boolean;
begin
  Result:=False;
  DialogUnconfirmedOrders := TDialogUnconfirmedOrders.Create(Self);
  DialogUnconfirmedOrders.Execute;
  DialogUnconfirmedOrders.Free;
end;

function Tdm2.CheckDeleteAct: boolean;
var s: string;
    d: integer;
begin
  Result:=False;
  with quCheckDeleteAct do
    begin
      close;
      ExecQuery;
      try
       if Fields[2].IsNull then s:=''
       else
       begin
        s:='������� ����: ';
        s:=s+#13#10+trim(Fields[1].AsString)+': ';
        d:=Fields[2].AsInteger;
        while not eof do
        begin
         if d=Fields[2].AsInteger then s:=s+trim(Fields[0].AsString)+'; '
         else s:=s+#13#10+trim(Fields[1].AsString)+': '+trim(Fields[0].AsString)+'; ';
         d:=Fields[2].AsInteger;
         next;
        end;
       end;
       if s<>'' then MessageDialog(s, mtInformation, [mbOK], 0);
      finally
       close;
       if s<>'' then result:=true;
      end;
    end;
end;

procedure Tdm2.CheckUnclosedAppl;
var s: string;
begin
  with quCheckUnclosedInv do
    begin
      Active:=False;
      Params[0].AsInteger:=SelfDepId;
      Active:=True;
      try
        if (Fields[2].AsInteger=0) then s:=''
        else
          begin
            s:='������� ���������� ������ �� �������';
          end;
        if s<>'' then MessageDialog(s, mtInformation, [mbOK], 0);
      finally
        Active:=False;
      end;
    end;
end;

function Tdm2.CheckUnclosedInv: boolean;
var s: string;
begin
  Result:=False;
  with quCheckUnclosedInv do
    begin
      Active:=False;
      Params[0].AsInteger:=SelfDepId;
      Active:=True;
      try
        if (Fields[0].AsInteger=0) and (Fields[1].AsInteger=0) and (Fields[2].AsInteger=0) then s:=''
        else
        if (Fields[0].AsInteger>0) and (Fields[1].AsInteger=0) and (Fields[2].AsInteger=0) then
          begin
            s:='������� ���������� ���������';
          end
        else
        if (Fields[0].AsInteger=0) and (Fields[1].AsInteger=0) and (Fields[2].AsInteger>0) then
          begin
            s:='������� ���������� ������ �� �������';
          end
        else
        if (Fields[0].AsInteger>0) and (Fields[1].AsInteger=0) and (Fields[2].AsInteger>0) then
          begin
            s:='������� ���������� ��������� � ������ �� �������';
          end
        else
        if (Fields[0].AsInteger=0) and (Fields[1].AsInteger>0) and (Fields[2].AsInteger=0)  then
          begin
            Result:=True;
            s:='������� ��������������� �������';
          end
        else
        if (Fields[0].AsInteger=0) and (Fields[1].AsInteger>0) and (Fields[2].AsInteger>0)  then
          begin
            Result:=True;
            s:='������� ��������������� ������� � ���������� ������ �� �������';
          end
        else
        if (Fields[0].AsInteger>0) and (Fields[1].AsInteger>0) and (Fields[2].AsInteger=0)  then
          begin
            Result:=True;
            s:='������� ���������� ��������� � ��������������� �������';
          end
        else
          begin
            Result:=True;
            s:='������� ���������� ���������, ������ �� ������� � ��������������� �������';
          end;
        if s<>'' then MessageDialog(s, mtInformation, [mbOK], 0);
      finally
        Active:=False;
      end;
    end;
end;


procedure Tdm2.CheckArtOnFil(ArtId: integer);
begin
  exit;
  with quCheckARtOnFil do
    try
      Active:=False;
      Params[0].AsInteger:=ArtId;
      Active:=True;
      if Fields[0].AsInteger>0 then raise Exception.Create('������� '+dm.quArtFullArt.AsString+' ���������� ����������');
    finally
      Active:=False;
    end;
end;

procedure Tdm2.CheckArt2OnFil(Art2Id: integer);
begin
  exit;
  with quCheckARt2OnFil do
    try
      Active:=False;
      Params[0].AsInteger:=Art2Id;
      Active:=True;
      if Fields[0].AsInteger>0 then raise Exception.Create('������� '+dm.quArtFullArt.AsString+' '+dm.taA2Art2.AsString+' ���������� ����������');
    finally
      Active:=False;
    end;
end;

procedure Tdm2.quFRestBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      ByName['ART1'].AsString:=dmCom.FilterArt;
      ByName['ART2'].AsString:=dmCom.FilterArt+'��';
    end;
end;

procedure Tdm2.quSameUIDBeforeOpen(DataSet: TDataSet);
begin
  with dmCom, quSameUid, Params do
    begin
      Prepare;
//      quSameUid.ByName('USERID').AsInteger:=dmCom.UserId;
      if not CenterDep (*dmCom.DefaultDep.DepId = 109*)then
      begin
       ByName['DEPID1'].AsInteger:=dmCom.DefaultDep.DepId;
       ByName['DEPID2'].AsInteger:=dmCom.DefaultDep.DepId;
      end
                                      else
      begin
       ByName['DEPID1'].AsInteger:=108-1;
       ByName['DEPID2'].AsInteger:=209+1;
      end;
    end;

end;

procedure Tdm2.quClientNewRecord(DataSet: TDataSet);
begin
  quClientCLIENTID.AsInteger := dmCom.GetID(23);
  quClientDEPID.AsInteger:=SelfDepId;
  quClientFMAIN.AsInteger:=1;
  quClientFDEL.AsInteger:=0;
end;

procedure Tdm2.quRPConfirmBeforeOpen(DataSet: TDataSet);
begin
  with quRPConfirm, Params do
    begin
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
    end
end;

procedure Tdm2.quFSZRestCalcFields(DataSet: TDataSet);
var i: integer;
    qs, ws: string;
begin
  qs:=DataSet.FieldByName('SQ1').AsString;
  ws:=ReplaceStr(DataSet.FieldByName('SW1').AsString, '.', ',');
  with dm, dm2 do
    for i:=0 to slDepDepId.Count-1 do
      begin
        DataSet.FieldByName('RW_'+slDepDepId[i]).AsFloat:=StrToFloat(EmptyStrToZero(ExtractWord(i+1, ws, [';'])));
        DataSet.FieldByName('RQ_'+slDepDepId[i]).AsInteger:=StrToInt(EmptyStrToZero(ExtractWord(i+1, qs, [';'])));
      end;
end;

procedure Tdm2.quD_WH2AfterRefresh(DataSet: TDataSet);
begin
//  ReopenDataSets([dm2.quD_WH_T]);
end;

procedure Tdm2.quInsDSItemExecuteError(pFIBQuery: TpFIBQuery; E: EFIBError;
  var Action: TDataAction);
begin
 if (Pos('��������� �������', E.Message)<>0) then
   raise Exception.Create('��������� �������!');

  if (Pos('��������� �������', E.Message)<>0) then
   raise Exception.Create('��������� �������!');
end;

procedure Tdm2.quaddressNewRecord(DataSet: TDataSet);
begin
 quaddressD_ADDRESS_ID.AsInteger:=dmcom.GetID(39);
end;

procedure Tdm2.quRprOrd2BeforeOpen(DataSet: TDataSet);
begin
  quRPrOrd2.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.RBD);
  quRPrOrd2.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.RED);
end;

procedure Tdm2.quRBeforePrordBeforeOpen(DataSet: TDataSet);
begin
 with quRBeforePrord, Params do
  begin
   ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
   ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
  end
end;

procedure Tdm2.quRBeforePActBeforeOpen(DataSet: TDataSet);
begin
 with quRBeforePAct, Params do
  begin
   ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
   ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
  end
end;

procedure Tdm2.quRActAllowancesBeforeOpen(DataSet: TDataSet);
begin
 with quRActAllowances, Params do
 begin
   ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
   ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
 end;
end;

procedure Tdm2.quRApplDepBeforeOpen(DataSet: TDataSet);
begin
  if not CenterDep then begin
   quRApplDep.SelectSQL[8]:=' and s.Modeid = '+inttostr(SelfDepId);
   quRApplDep.SelectSQL[7]:=''
  end;
  quRApplDep.Prepare;
  quRApplDep.Params.ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm3.AppldepBD);
  quRApplDep.Params.ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm3.AppldepED);
end;

procedure Tdm2.quRApplDepArtBeforeOpen(DataSet: TDataSet);
begin
  if not CenterDep then begin
   quRApplDepArt.SelectSQL[8]:=' and s.Modeid = '+inttostr(SelfDepId);
   quRApplDepArt.SelectSQL[7]:=''
  end;
  quRApplDepArt.Prepare;
  quRApplDepArt.Params.ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm3.AppldepBD);
  quRApplDepArt.Params.ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm3.AppldepED);
end;

procedure Tdm2.quSameUidPBeforeOpen(DataSet: TDataSet);
begin
 if dm.UIDWHType=3 then
  quSameUidP.ParamByName('USERID').AsInteger:=UserDefault
 else
  quSameUidP.ParamByName('USERID').AsInteger:=dmcom.UserId 
end;

procedure Tdm2.quRSuspItemBeforeOpen(DataSet: TDataSet);
begin
  with quRSuspItem, Params do
    begin
      ByName['DEPFROMID'].AsInteger:=SelfDepId;
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
    end;
end;

procedure Tdm2.quRPactCBeforeOpen(DataSet: TDataSet);
begin
 with quRPactC, Params do
  begin
   ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
   ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
  end;
end;

procedure Tdm2.quDst2BeforePost(DataSet: TDataSet);
begin
  if not dm.CheckForUIDWHCalc then
    raise Exception.Create('�������������� �� �� ��������. ���� �������� ������.��� ������ ������� Esc');
end;

procedure Tdm2.quDst3BeforePost(DataSet: TDataSet);
begin
  if not dm.CheckForUIDWHCalc then
    raise Exception.Create('�������������� �� �� ��������. ���� �������� ������.��� ������ ������� Esc');
end;

end.

