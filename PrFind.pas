unit PrFind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask,  Variants, rxPlacemnt,
  rxToolEdit;

type
  TfmPrFind = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edN: TEdit;
    edSN: TEdit;
    deCloseDate: TDateEdit;
    bbFind: TBitBtn;
    bbFindNext: TBitBtn;
    bbOK: TBitBtn;
    FormPlacement1: TFormPlacement;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNChange(Sender: TObject);
    procedure bbFindClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPrFind: TfmPrFind;

implementation

uses comdata, Data, M207Proc, MsgDialog;

{$R *.DFM}

procedure TfmPrFind.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_F12, VK_ESCAPE] then Close;
end;

procedure TfmPrFind.edNChange(Sender: TObject);
begin
  bbOK.Default:=False;
  bbFindNext.Default:=False;
  bbFind.Default:=True;
end;

procedure TfmPrFind.bbFindClick(Sender: TObject);
var s: string;
    FldValues: Variant;
    i: integer;
begin
  s:='';

  i:=-1;
  FldValues:=VarArrayCreate([0, 2], varVariant);
  if edN.Text<>'' then
    begin
      Inc(i);
      s:='PRORD;';
      FldValues[i]:=edN.Text;
    end;

  if deCloseDate.Date>1 then
    begin
      Inc(i);
      s:=s+'CLOSEDATE;';
      FldValues[i]:=DateToSTr(deCloseDate.Date);
    end;

  if edSN.Text<>'' then
    begin
      Inc(i);
      s:=s+'SN;';
      FldValues[i]:=edSN.Text;
    end;

  if not Locate(dm.taPrOrd, s, FldValues, [], TComponent(Sender).Tag=2) then
     MessageDialog('������ �� �������', mtInformation, [mbOK], 0);

  case TComponent(Sender).Tag of
      1:
         begin
           bbOK.Default:=False;
           bbFind.Default:=False;
           bbFindNext.Default:=True;
         end;
     end;
end;

end.
