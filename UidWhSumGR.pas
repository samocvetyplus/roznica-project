unit UidWhSumGR;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, DB, jpeg, rxSpeedbar;

type
  Tfmuidwhsumgr = class(TForm)
    dg1: TM207IBGrid;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    siCalc: TSpeedItem;
    SpeedItem3: TSpeedItem;
    siFind: TSpeedItem;
    spitHistory: TSpeedItem;
    siEdit: TSpeedItem;
    SpeedItem6: TSpeedItem;
    SpeedItem5: TSpeedItem;
    spItog: TSpeedItem;
    siText: TSpeedItem;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmuidwhsumgr: Tfmuidwhsumgr;

implementation
uses data, comdata, M207Proc, Data2;
{$R *.dfm}

procedure Tfmuidwhsumgr.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure Tfmuidwhsumgr.FormCreate(Sender: TObject);
var  c: TColumn;
     i:integer;
begin
  tb1.Wallpaper:=wp;
  ReOpenDataSets([dm.quUidWhSumGr]);
  with dm, dm2 do
    begin
      for i:=0 to slGRId.Count-1 do
        begin
          c:=dg1.Columns.Add;
          c.Field:=quUidWhSumGr.FieldByName('RW_'+slGrId[i]);
          c.Title.Caption:=slGrName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dg1.Columns.Add;
          c.Field:=quUidWhSumGr.FieldByName('RQ_'+slGrId[i]);
          c.Title.Caption:=slGrName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dg1.Columns.Add;
          c.Field:=quUidWhSumGr.FieldByName('RP_'+slGrId[i]);
          c.Title.Caption:=slGrName[i]+' - ����. ����';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dg1.Columns.Add;
          c.Field:=quUidWhSumGr.FieldByName('RP2_'+slGrId[i]);
          c.Title.Caption:=slGrName[i]+' - ����. ����';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

        end;
    end;
end;

procedure Tfmuidwhsumgr.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
if field<>Nil then
  Background:=dg1.DataSource.DataSet.FieldByName('Color').AsInteger;
end;

procedure Tfmuidwhsumgr.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm.quUidWhSumGr]);
end;

procedure Tfmuidwhsumgr.siExitClick(Sender: TObject);
begin
 close;
end;

end.
