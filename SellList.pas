{ ������� }
unit SellList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, StdCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, Menus, db, DBCtrls, DBGridEh,
  ActnList, PrnDbgeh, jpeg, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar, FIBDataSet, pFIBDataSet, XLSExportComp, XLSFormat, Math;

type
  TfmSellList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    laDep: TLabel;
    fmsSell: TFormStorage;
    siView: TSpeedItem;
    pm1: TPopupMenu;
    N1: TMenuItem;
    siClose: TSpeedItem;
    N2: TMenuItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    N3: TMenuItem;
    N4: TMenuItem;
    spitPrint: TSpeedItem;
    pmPrint: TPopupMenu;
    mnitBill: TMenuItem;
    mnitActDisc: TMenuItem;
    nSell: TMenuItem;
    N5: TMenuItem;
    NSellArt: TMenuItem;
    acEvent: TActionList;
    acView: TAction;
    acAdd: TAction;
    acDel: TAction;
    acClose: TAction;
    acPrintBill: TAction;
    acPrintActDisc: TAction;
    acPrintSellEntry: TAction;
    acPrintSellOutlay: TAction;
    acPrintSellArt: TAction;
    StatusBar1: TStatusBar;
    NSellNump: TMenuItem;
    acPrintSellSinv: TAction;
    prgSell: TPrintDBGridEh;
    acPrintGrid: TAction;
    acPrintActRet: TAction;
    N6: TMenuItem;
    siHelp: TSpeedItem;
    spitTermCard: TSpeedItem;
    acTermCard: TAction;
    SpeeItem2: TSpeedItem;
    gridSell: TDBGridEh;
    acOpen: TAction;
    dsrDlist_H: TDataSource;
    taHist: TpFIBDataSet;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    siSert: TSpeedItem;
    acSert: TAction;
    DBGridEh1: TDBGridEh;
    siItogo: TSpeedItem;
    siProfit: TSpeedItem;
    SpeedbarSection3: TSpeedbarSection;
    acProfit: TAction;
    XLSExportFile1: TXLSExportFile;
    dsrProfit: TDataSource;
    taProfit: TpFIBDataSet;
    taProfitDEP: TFIBStringField;
    taProfitCOST: TFIBFloatField;
    taProfitSCOST: TFIBFloatField;
    taProfitRCOST: TFIBFloatField;
    SD1: TSaveDialog;
    taProfitITOG: TFIBFloatField;
    DBGridEh2: TDBGridEh;
    DBGridEh3: TDBGridEh;
    dsrProfit_Itog: TDataSource;
    taProfit_Itog: TpFIBDataSet;
    taProfit_ItogCOST: TFIBFloatField;
    taProfit_ItogSCOST: TFIBFloatField;
    taProfit_ItogRCOST: TFIBFloatField;
    taProfit_ItogITOG: TFIBFloatField;
    taProfitCITY: TFIBStringField;
    DBGridEh4: TDBGridEh;
    DBGridEh5: TDBGridEh;
    DBGridEh6: TDBGridEh;
    DBGridEh7: TDBGridEh;
    taProfit_city: TpFIBDataSet;
    dsProfit_city: TDataSource;
    taProfit_cityCITY: TFIBStringField;
    taProfit_cityCOST: TFIBFloatField;
    siSum: TSpeedItem;
    acKassaSum: TAction;
    SpeedItem2: TSpeedItem;
    ActionRet: TAction;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siPeriodClick(Sender: TObject);
    procedure nSellClick(Sender: TObject);
    procedure gridSellDblClick(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure gridSellGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acViewUpdate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acCloseUpdate(Sender: TObject);
    procedure acPrintBillExecute(Sender: TObject);
    procedure acPrintBillUpdate(Sender: TObject);
    procedure acPrintActDiscExecute(Sender: TObject);
    procedure acPrintActDiscUpdate(Sender: TObject);
    procedure acPrintSellExecute(Sender: TObject);
    procedure acPrintSellArtUpdate(Sender: TObject);
    procedure acPrintSellEntryUpdate(Sender: TObject);
    procedure acPrintSellOutlayUpdate(Sender: TObject);
    procedure acPrintSellSinvUpdate(Sender: TObject);
    procedure gridSellKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPrintGridExecute(Sender: TObject);
    procedure acPrintActRetUpdate(Sender: TObject);
    procedure acPrintActRetExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure acTermCardExecute(Sender: TObject);
    procedure SpeeItem2Click(Sender: TObject);
    procedure dsSellListDataChange(Sender: TObject; Field: TField);
    procedure siCloseClick(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acSertExecute(Sender: TObject);
    procedure acProfitExecute(Sender: TObject);
    procedure taProfitBeforeOpen(DataSet: TDataSet);
    procedure acKassaSumExecute(Sender: TObject);
    procedure ActionRetExecute(Sender: TObject);

  private
    LogOprIdForm:string;  
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  end;

var
  WMode: string[10];
  fmSellList: TfmSellList;

implementation

uses Data, Data2, comdata, Sell, SellItem, Period, ReportData, ClSell, M207Proc,
  InsRepl, Data3, TerminalCard, MsgDialog, dbUtil, Hist, SellSert,SellDeviceSum,
  RetCash;

{$R *.DFM}

(* �������� ��������� ���������� � dm.SellClick *)

procedure TfmSellList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmSellList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSellList.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  dmcom.IsActSellList:=false;
  with dmCom, dm do
    begin
      SetBeginDate;
      ShowPeriod;
      WorkMode:='ALLSELL';
      if not tr.Active then tr.StartTransaction;
      tr.CommitRetaining;
      if not Centerdep then
      begin
      gridSell.FieldColumns['real_vych'].Visible:=false;
      gridSell.FieldColumns['real_razn'].Visible:=false;
      end;
      
    end;
 gridSell.FieldColumns['SCOST'].Visible:=CenterDep;
 gridSell.FieldColumns['RSCOST'].Visible:=CenterDep;
 nSell.Enabled:=CenterDep;
 siProfit.Visible:=CenterDep;
 LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
 //
 if not centerdep   then
 siSum.visible:=false;
    
end;

procedure TfmSellList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
    begin
      PostDataSets([taSellList]);
      CloseDataSets([taSellList]);
      tr.CommitRetaining;
      WMode:='';
      WorkMode:='';
    end;
end;

procedure TfmSellList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSellList.FormActivate(Sender: TObject);
begin
  WMode:='SELLLIST';
  dm.dsSellList.OnDataChange:=fmSellList.dsSellListDataChange;
  dm.ClickUserDepMenu(dm.pmSell);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSellList.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  with dm, dm2 do
    if taSellListED.IsNull then Background:=clInfoBk
    else if taSellListClosed.AsInteger=0 then Background:=GetDepColor(taSellListDepId.AsInteger)
         else  Background:=clBtnFace;
end;

procedure TfmSellList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taSellList]);
      ShowPeriod;
    end;
end;

procedure TfmSellList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;


procedure TfmSellList.nSellClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridSell, dm.taSellListSELLID);
  try
    case (Sender as TMenuItem).Tag of
      0 :  PrintDocument(arr,sell_ticket);
      1 :  PrintDocument(arr,sell_ticketOutlay);
      2 :  PrintDocument(arr,sell_ticketart);
    end;
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellList.gridSellDblClick(Sender: TObject);
begin
  acView.Execute;
end;

procedure TfmSellList.acViewExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�������� �������',LogOprIdForm);
  ShowAndFreeForm(TfmSellItem, Self, TForm(fmSellItem), True, False);
  dm.taSellList.Refresh;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSellList.gridSellGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  with dm, dm2 do
    if taSellListED.IsNull then Background:=clInfoBk
    else if taSellListClosed.AsInteger=0 then Background:=GetDepColor(taSellListDepId.AsInteger)
             else Background:=clBtnFace;
end;

procedure TfmSellList.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acAddExecute(Sender: TObject);
var LogOperationID:string;
begin
if (dm.SDepId <> dmcom.User.DepId) and (not dmcom.USerAllWh) then
  raise Exception.Create('������������ �� ����� ������� ����� �� ������� �����');
  with dm do
  begin
    LogOperationID:=dm3.insert_operation('���������� �������',LogOprIdForm);
    taSellList.Append;
    taSellListCnt.AsInteger:=0;
    taSellList.Post;
    ShowAndFreeForm(TfmSellItem, Self, TForm(fmSellItem), True, False);
    dm.taSellList.Refresh;
    dm3.update_operation(LogOperationID);
  end;
end;

procedure TfmSellList.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dm.taSellList.Active;
end;

procedure TfmSellList.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  if (MessageDialog('�� �������, ��� ������ ������� �����?', mtWarning, [mbOk, mbCancel],0))=mrok then
  begin
  LogOperationID:=dm3.insert_operation('������� �������',LogOprIdForm);
  dm.taSellList.Delete;
  dm3.update_operation(LogOperationID);
  end;
end;

procedure TfmSellList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;




procedure TfmSellList.acKassaSumExecute(Sender: TObject);
begin
ShowAndFreeForm(TFmDeviceSell, Self, TForm(FmDeviceSell), True, False);
end;


procedure TfmSellList.acCloseExecute(Sender: TObject);
var LogOperationID:string;
begin
  fmSellList.taHist.Active:=true;
  fmSellList.taHist.Open;
  with fmSellList.taHist do
  begin
  fmSellList.taHist.Append;
  fmSellList.taHistDOCID.AsInteger := dm.taSellListSellid.AsInteger;
  If (Hist.log='') then fmSellList.taHistFIO.Value := dmCom.User.Alias else
  fmSellList.taHistFIO.Value :=Hist.log;
  fmSellList.taHistSTATUS.Value := '�������';
  fmSellList.taHist.Post;
  fmSellList.taHist.Transaction.CommitRetaining;
  end;
  fmSellList.taHist.Close;
  fmSellList.taHist.Active:=false;

  LogOperationID:=dm3.insert_operation('������� �������',LogOprIdForm);
  with dm do
    begin
      if (taSellListED.AsFloat<1) and (taSellListClosed.AsInteger<>1) then
        begin
          if ShowAndFreeForm(TfmClSell, Self, TForm(fmClSell), True, False)=mrOK then
            with taSellList do
              begin
                if NOT (State in [dsInsert, dsEdit]) then Edit;
                taSellListClosed.AsInteger:=1;
                Post;
              end
           else CancelDataSets([taSellList]);
        end
      else
        if taSellListClosed.AsInteger<>1 then
              with taSellList do
              begin
                if NOT (State in [dsInsert, dsEdit]) then Edit;
                taSellListClosed.AsInteger:=1;
                Post;
              end;
    end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSellList.acCloseUpdate(Sender: TObject);
begin
  acClose.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintBillExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridSell, dm.taSellListSELLID);
  try
    PrintDocument(arr,bill);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellList.acPrintBillUpdate(Sender: TObject);
begin
  acPrintBill.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintActDiscExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridSell, dm.taSellListSELLID);
  try
    PrintDocument(arr,act_ticket); 
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellList.acPrintActDiscUpdate(Sender: TObject);
begin
  acPrintActDisc.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintSellExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridSell, dm.taSellListSELLID);
  try
    case (Sender as TAction).Tag of
      0 :  PrintDocument(arr,sell_ticket);
      1 :  PrintDocument(arr,sell_ticketOutlay);
      2 :  PrintDocument(arr,sell_ticketart);
      3 :  PrintDocument(arr,sell_ticketsinv);
    end;
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellList.acPrintSellArtUpdate(Sender: TObject);
begin
  acPrintSellArt.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintSellEntryUpdate(Sender: TObject);
begin
  acPrintSellEntry.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintSellOutlayUpdate(Sender: TObject);
begin
  acPrintSellOutlay.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintSellSinvUpdate(Sender: TObject);
begin
  acPrintSellSinv.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acProfitExecute(Sender: TObject);
var
  i, j, k:integer;
  proc_cost,proc_cost_sity, proc_allcost, cost_city:real;
begin
  proc_cost:=0;
  proc_cost_sity:=0;
  if GetPeriod(dm.BeginDate, dm.EndDate) then // ������ ������ ��������
    begin
      ReOpenDataSets([taProfit, taProfit_Itog, taProfit_city]);
      ShowPeriod;
    end;
  XLSExportFile1.Workbook.Create(1);
  if (sd1.Execute) then // ��������� ���� � xls �����
  begin
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].Value:='������� �������';
    XLSExportFile1.Workbook.Sheets[0].Cells[3,0].Value:='�������';
    XLSExportFile1.Workbook.Sheets[0].Cells[4,0].Value:='������������� ����-� ���-�';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,0].Value:='��������';
    XLSExportFile1.Workbook.Sheets[0].Cells[6,0].Value:='������� �������';

    //����� � ����� ����� ���������� �� ������ � ������������
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].FontBold:=true;
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].HAlign:=xlHAlignCenter;
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].Wrap:=true;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,0].HAlign:=xlHAlignCenter;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,0].Wrap:=true;
    XLSExportFile1.Workbook.Sheets[0].Cells[6,0].FontBold:=true;
    XLSExportFile1.Workbook.Sheets[0].Cells[6,0].HAlign:=xlHAlignCenter;
    XLSExportFile1.Workbook.Sheets[0].Cells[6,0].Wrap:=true;

    i:=1;
    taProfit.First;
    cost_city:=0;
    While not taProfit.Eof do
    begin
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].Value:=taProfitDEP.Value;  // �������
      XLSExportFile1.Workbook.Sheets[0].Cells[3,i].Value:=taProfitCOST.Value; //�������
      XLSExportFile1.Workbook.Sheets[0].Cells[4,i].Value:=taProfitSCOST.Value; //�������������
      XLSExportFile1.Workbook.Sheets[0].Cells[5,i].Value:=taProfitRCOST.Value; //��������
      XLSExportFile1.Workbook.Sheets[0].Cells[6,i].Value:=taProfitITOG.Value;  // ������� �������

      taProfit_city.First;
      While not taProfit_city.Eof do
      begin
         if taProfit_cityCITY.Value = taProfitCITY.Value then
           cost_city:=RoundTo(((taProfitCOST.Value*100)/taProfit_cityCOST.Value), -2);
         taProfit_city.Next;
      end;
      XLSExportFile1.Workbook.Sheets[0].Cells[2,i].Value:=cost_city; //% ������� �� ������

      //�������� ���������� ����� ������� (����-� ���������)
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].FontBold:=true;
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].HAlign:=xlHAlignCenter;
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].Wrap:=true;
      // ���������� ����� ������� ������
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].BorderColorRGB[xlBorderAll]:=xlcolorblack;
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].BorderStyle[xlBorderAll]:=bsThin;
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].BorderStyle[xlBorderTop]:=bsMedium;
      XLSExportFile1.Workbook.Sheets[0].Cells[0,i].BorderStyle[xlBorderBottom]:=bsMedium;
      // ���������� ����� ������� ������ ������
      for k := 1 to 7 do
      begin
         XLSExportFile1.Workbook.Sheets[0].Cells[k,i].BorderColorRGB[xlBorderAll]:=xlcolorblack;
         XLSExportFile1.Workbook.Sheets[0].Cells[k,i].BorderStyle[xlBorderAll]:=bsThin;
      end;
      taProfit.Next;
      inc(i);
    end;

    // ���������� ����� 1�� � ��������� ��������
    for k := 0 to 7 do
    begin
      XLSExportFile1.Workbook.Sheets[0].Cells[k,0].BorderColorRGB[xlBorderAll]:=xlcolorblack; //1�� �����
      XLSExportFile1.Workbook.Sheets[0].Cells[k,0].BorderStyle[xlBorderAll]:=bsThin;

      XLSExportFile1.Workbook.Sheets[0].Cells[k,i].BorderColorRGB[xlBorderAll]:=xlcolorblack; // ����� - ���. ����
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i].BorderStyle[xlBorderAll]:=bsThin;
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i].FontBold:=true;
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i].HAlign:=xlHAlignCenter;
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i].Wrap:=true;

      XLSExportFile1.Workbook.Sheets[0].Cells[k,i+1].BorderColorRGB[xlBorderAll]:=xlcolorblack; // ����� - �����
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i+1].BorderStyle[xlBorderAll]:=bsThin;
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i+1].BorderStyle[xlBorderRight]:=bsMedium;
      XLSExportFile1.Workbook.Sheets[0].Cells[k,i+1].BorderStyle[xlBorderLeft]:=bsMedium;
    end;

    // ����� �� ���� ���������
    XLSExportFile1.Workbook.Sheets[0].Cells[0,i].Value:='�������� �������-�';
    XLSExportFile1.Workbook.Sheets[0].Cells[0,i+1].Value:='�����';
    XLSExportFile1.Workbook.Sheets[0].Cells[1,i+1].Value:=100;
    XLSExportFile1.Workbook.Sheets[0].Cells[3,i+1].Value:= taProfit_ItogCOST.Value;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,i+1].Value:= taProfit_ItogSCOST.Value;
    XLSExportFile1.Workbook.Sheets[0].Cells[5,i+1].Value:= taProfit_ItogRCOST.Value;
    XLSExportFile1.Workbook.Sheets[0].Cells[6,i+1].Value:= taProfit_ItogITOG.Value;

    j:=1;
    taProfit.First;
    While not taProfit.Eof do // ��������� �������� �� ������� � ������� �������
    begin
      proc_cost := RoundTo(((taProfitCOST.Value*100)/taProfit_ItogCOST.Value), -2);
      XLSExportFile1.Workbook.Sheets[0].Cells[1,j].Value:=proc_cost;
      proc_allcost := RoundTo(((taProfitITOG.Value*100)/taProfit_ItogITOG.Value), -2);
      XLSExportFile1.Workbook.Sheets[0].Cells[7,j].Value:=proc_allcost;
      taProfit.Next;
      inc(j);
    end;

    XLSExportFile1.Workbook.Sheets[0].Rows[0].FontBold:=true;
    XLSExportFile1.Workbook.Sheets[0].Rows[1].FontBold:=true;
    XLSExportFile1.Workbook.Sheets[0].Rows[6].FontBold:=true;
    XLSExportFile1.Workbook.Sheets[0].Rows[7].FontBold:=true;

    XLSExportFile1.SaveToFile(sd1.FileName);
    XLSExportFile1.Workbook.Clear;
    MessageDlg('������� �������� ������� � ���� ' + sd1.FileName, mtInformation, [mbOk], 0);

    taProfit.Active:=false;
    taProfit_Itog.Active:=false;
    taProfit_city.Active:=false;
    CloseDataSets([taProfit, taProfit_Itog, taProfit_city]);
  end;
end;

procedure TfmSellList.acSertExecute(Sender: TObject);
begin
ShowAndFreeForm(TfmSellSert, Self, TForm(fmSellSert), True, False);
end;

procedure TfmSellList.gridSellKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_RETURN: if not dm.taSellList.IsEmpty then acView.Execute;
 end;
end;

procedure TfmSellList.acPrintGridExecute(Sender: TObject);
begin
  prgSell.Preview;
end;      

procedure TfmSellList.acPrintActRetUpdate(Sender: TObject);
begin
 acPrintActRet.Enabled := dm.taSellList.Active and (not dm.taSellList.IsEmpty);
end;

procedure TfmSellList.acPrintActRetExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(gridSell, dm.taSellListSELLID);
  try
    PrintDocument(arr,actret_ticket);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSellList.siHelpClick(Sender: TObject);
begin
  Application.HelpContext(100233);
end;

procedure TfmSellList.acTermCardExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmTermCards, Self, TForm(fmTermCards), True, False);
end;

procedure TfmSellList.ActionRetExecute(Sender: TObject);
begin
 ShowAndFreeForm(TFmRetCash, Self, TForm(FmRetCash), True, False);
end;

procedure TfmSellList.SpeeItem2Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taSellList);
  tr.CommitRetaining;
  ReOpenDataSet(taSellList);
  taSellList.Refresh;
  end;
end;
procedure TfmSellList.taProfitBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['DEPID1'].AsInteger:=-MAXINT;
    ByName['DEPID2'].AsInteger:=MAXINT;

    ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TfmSellList.dsSellListDataChange(Sender: TObject; Field: TField);
begin
  if dm.taSellListRN.IsNull then siClose.Enabled:=false else siClose.Enabled:=true;
  if (dm.taSellListCLOSED.AsInteger = 0) then
   dm.SetCloseInvBtn(siClose,0) else dm.SetCloseInvBtn(siClose, 1);
end;

procedure TfmSellList.siCloseClick(Sender: TObject);
begin
   If dm.taSellListCLOSED.AsInteger=1 then
   ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False)
   else
   if MessageDialog('������� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
   acClose.Execute;
end;

procedure TfmSellList.acOpenExecute(Sender: TObject);
var IsCalc: Integer;
    LogOperationID:string;
begin
    LogOperationID:=dm3.insert_operation('������� �����',LogOprIdForm);
    with dm do
  begin
    if WorkMode<>'ALLSELL' then
      begin
        fmSellItem.acCloseSellExecute(NIL);
        dm.OpenSell;
      end
    else
         with taSellList do
          begin
            if NOT (State in [dsInsert, dsEdit]) then Edit;
            taSellListClosed.AsInteger:=0;
            Post;
          end ;
   with quSellElDel do
    begin
        SQL.Text := 'select sellelcalc  from d_Rec ';
        ExecQuery;
        IsCalc := Fields[0].AsInteger;
        close;
    end;

    if IsCalc = 0 then
    begin
      Screen.Cursor:=crSQLWait;
      with quSellElDel do
      begin
        SQL.Text := 'update d_Rec  set sellelcalc = 1';
        ExecQuery;
        Close;
        dmCom.tr.CommitRetaining;
      end ;
      with quToCheckShiftTime do
      begin
        SQL.Text := 'execute procedure RETTIMECHECK(?BD, ?ED, ?CLIENTID1, ?CLIENTID2,?UserID)';
        Prepare;
        ParamByName('ED').AsTimeStamp:= DateTimeToTimeStamp(dmCom.GetServerTime);
        ParamByName('BD').AsTimeStamp:= DateTimeToTimeStamp(IncMonth(dmCom.GetServerTime, -3));
        ParamByName('CLIENTID1').AsInteger:=-MAXINT;
        ParamByName('CLIENTID2').AsInteger:=MAXINT;
        ParamByName('UserID').AsInteger := -1000;
        if not transaction.Active then transaction.StartTransaction;
        ExecQuery;
        close;
        Transaction.CommitRetaining;
      end;
      with quSellElDel do
      begin
        SQL.Text := 'update d_Rec  set sellelcalc = 0';
        ExecQuery;
        Close;
        Transaction.CommitRetaining;
      end;
      Screen.Cursor:=crDefault;
    end;
  end;
  dm.SetCloseInvBtn(siClose, 0);
  dm3.update_operation(LogOperationID);
end;

procedure TfmSellList.FormDestroy(Sender: TObject);
begin
dm.dsSellList.OnDataChange:=nil;
end;

end.
