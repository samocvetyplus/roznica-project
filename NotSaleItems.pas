unit NotSaleItems;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls, Buttons, StdCtrls, DB,
  FIBDataSet, pFIBDataSet, Mask, dbUtil, CheckLst, DBCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxPrnDev, dxPrnDlg,  dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore,dxPScxCommon, cxPropertiesStore,
  jpeg, dxPScxGrid6Lnk, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, rxToolEdit, rxSpeedbar, cxLookAndFeels,
  cxLookAndFeelPainters, 
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, 
  dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxSkinsdxBarPainter, dxBarSkinnedCustForm,
  dxSkinsdxRibbonPainter, dxPSPrVwStd, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin;

type
  TfmNotSaleItems = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    spitPrint: TSpeedItem;
    SpeedItem3: TSpeedItem;
    siFind: TSpeedItem;
    spitHistory: TSpeedItem;
    siEdit: TSpeedItem;
    SpeedItem6: TSpeedItem;
    spItog: TSpeedItem;
    siCalc: TSpeedItem;
    siText: TSpeedItem;
    siAppl: TSpeedItem;
    siExit: TSpeedItem;
    siHelp: TSpeedItem;
    SpeedBar3: TSpeedBar;
    Label3: TLabel;
    Label6: TLabel;
    edMonth: TComboEdit;
    SpeedbarSection5: TSpeedbarSection;
    quNew: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBFloatField3: TFIBFloatField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    FIBStringField5: TFIBStringField;
    FIBStringField6: TFIBStringField;
    FIBFloatField4: TFIBFloatField;
    FIBStringField7: TFIBStringField;
    FIBStringField8: TFIBStringField;
    FIBStringField9: TFIBStringField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBStringField10: TFIBStringField;
    FIBDateField1: TFIBDateField;
    FIBDateField2: TFIBDateField;
    FIBDateField3: TFIBDateField;
    FIBDateField4: TFIBDateField;
    FIBStringField11: TFIBStringField;
    FIBStringField12: TFIBStringField;
    FIBIntegerField4: TFIBIntegerField;
    dsNew: TDataSource;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1Column1: TcxGridDBColumn;
    cxGrid2DBTableView1Column2: TcxGridDBColumn;
    cxGrid2DBTableView1Column3: TcxGridDBColumn;
    cxGrid2DBTableView1Column4: TcxGridDBColumn;
    cxGrid2DBTableView1Column5: TcxGridDBColumn;
    cxGrid2DBTableView1Column6: TcxGridDBColumn;
    cxGrid2DBTableView1Column7: TcxGridDBColumn;
    cxGrid2DBTableView1Column8: TcxGridDBColumn;
    cxGrid2DBTableView1Column9: TcxGridDBColumn;
    cxGrid2DBTableView1Column10: TcxGridDBColumn;
    cxGrid2DBTableView1Column11: TcxGridDBColumn;
    cxGrid2DBTableView1Column12: TcxGridDBColumn;
    cxGrid2DBTableView1Column13: TcxGridDBColumn;
    cxGrid2DBTableView1Column14: TcxGridDBColumn;
    cxGrid2DBTableView1Column15: TcxGridDBColumn;
    cxGrid2DBTableView1Column16: TcxGridDBColumn;
    cxGrid2DBTableView1Column17: TcxGridDBColumn;
    cxGrid2DBTableView1Column18: TcxGridDBColumn;
    cxGrid2DBTableView1Column19: TcxGridDBColumn;
    cxGrid2DBTableView1Column20: TcxGridDBColumn;
    cxGrid2DBTableView1Column21: TcxGridDBColumn;
    cxGrid2DBTableView1Column22: TcxGridDBColumn;
    cxGrid2DBTableView1Column23: TcxGridDBColumn;
    cxGrid2DBTableView1Column24: TcxGridDBColumn;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    quNewCOST1: TFIBFloatField;
    cxGrid2DBTableView1Column25: TcxGridDBColumn;
    quNewART2ID: TFIBIntegerField;
    quNewINS_STR: TFIBStringField;
    cxGrid2DBTableView1Column26: TcxGridDBColumn;
    procedure edMonthButtonClick(Sender: TObject);
    procedure edMonthKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edMonthKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure quNewBeforeOpen(DataSet: TDataSet);
    procedure siExitClick(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
  private
    { Private declarations }

    FilterMonth : Integer;
  protected
    procedure Loaded; override;
  public
    { Public declarations }

  end;

var
  fmNotSaleItems: TfmNotSaleItems;

implementation

uses Data, comdata, Data2, jewconst, Data3, reportdata, cxGridStrs,
  ListItem, DBTree, cxGridDBDataDefinitions, uUtils, Ins, fmUtils;

{$R *.dfm}

// ********** �������� ������ ��� ������� ******************//
procedure TfmNotSaleItems.edMonthButtonClick(Sender: TObject);
var
 Key : word;
begin
 Key := VK_RETURN;
 edMonthKeyDown(Sender, Key, []);
end;

procedure TfmNotSaleItems.edMonthKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_RETURN :
       try
         Screen.Cursor := crSQLWait;
         FilterMonth := StrToInt(edMonth.Text);
         ReOpenDataSets([dsNew.DataSet]);
        finally
         Screen.Cursor := crDefault;
       end;
  end;
end;

procedure TfmNotSaleItems.edMonthKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_DOWN :
    begin
      ActiveControl := cxGrid2;
      ActiveControl.SetFocus;
    end;
end;
end;

//********************* �������� ����� *************************//
procedure TfmNotSaleItems.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
cxGrid2DBTableView1.StoreToIniFile(GetIniFileName, False); // ���������� ��������� ������� �����
  with fmNotSaleItems, dmCom do
    begin
      CloseDataSets([dsNew.DataSet]);
      if not tr.Active then tr.StartTransaction;
      tr.CommitRetaining;
      if not tr1.Active then tr1.StartTransaction;
      tr1.CommitRetaining;
      FilterMonth := 0;
    end;
    Deactivate;
end;

procedure TfmNotSaleItems.quNewBeforeOpen(DataSet: TDataSet);
begin
    with TpFIBDataSet(DataSet), dsNew do
    begin
      if (FilterMonth>0) then // ���� �������� ���������
         ParamByName('COUNT_M').AsInteger := FilterMonth; // ���������� �������
    end;
end;

// *********** ������ �������� ���� **************** //
procedure TfmNotSaleItems.siExitClick(Sender: TObject);
begin
  Close;
end;

//** ���� ���������� �� ������ ������ ListItem, ���. ����������� ����. �������� ����� **//
//** ���� ��� ������ ������� � �����, ������� �� ������ ���� ���������� �� �������� **//
procedure TfmNotSaleItems.spitPrintClick(Sender: TObject);
var
  nd: TNodeData;
  i: integer;
begin
   { cxGrid2DBTableView1.StoreToIniFile(GetIniFileName, False); // ���������� ������� �����
    fmListItem:=TfmListItem.Create(Application); // �������� ����� ������ ����� ��� ������
    with fmListItem do
    begin
      CheckListBox1.Items.Clear;
      with cxGrid2 do
      begin
        for i:=0 to cxGrid2DBTableView1.ColumnCount-1 do
        begin //���������� ������ CheckListBox1
          CheckListBox1.Items.AddObject(cxGrid2DBTableView1.Columns[i].Caption, nd);
          // ���������� (Checked) ���� �� ���������
          if (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'UID') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'ART') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'ART2') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'SPRICE0') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'COST1') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'W') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'UNIT_') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'PRODCODE') or
             (cxGrid2DBTableView1.Columns[i].DataBinding.FieldName = 'COUNT_MONTH') then
             CheckListBox1.Checked[cxGrid2DBTableView1.Columns[i].Index]:=True;
        end;
      end;
      fmListItem.ShowModal; // ������� ����� ������ ����� ��� ������
    end; }
    
end;

 //** �������� ������� ����� (��������� �����, �� ��������� � �.�.) � ���-����� **//
procedure TfmNotSaleItems.Loaded;
begin
  inherited;
  cxGrid2DBTableView1.RestoreFromIniFile(GetIniFileName);
end;

procedure TfmNotSaleItems.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  SpeedBar3.Wallpaper:=wp;
end;

procedure TfmNotSaleItems.SpeedItem3Click(Sender: TObject);
begin
  with dm do
    begin
       Art2Id:=dsNew.DataSet.FieldValues['Art2Id'];
   //    Art2:=dsUIDWH.DataSet.FieldValues['Art2'];
    //   dsUIDWH.DataSet.Tag:=dsUIDWH.DataSet.FieldValues['UID'];

       ShowAndFreeForm(TfmIns, Self, TForm(fmIns), True, False);
    end;
end;

end.



