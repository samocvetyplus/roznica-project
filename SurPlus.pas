unit SurPlus;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, DBCtrls, RXDBCtrl, Mask,
  M207DBCtrls, Grids, DBGridEh, PrnDbgeh, ActnList, Printers, PrntsEh,
  M207Ctrls, ComDrv32, Buttons, DBCtrlsEh, jpeg,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  TfmSurPlus = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siCloseInv: TSpeedItem;
    spitPrint: TSpeedItem;
    paHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    DBText3: TDBText;
    Label16: TLabel;
    Label17: TLabel;
    DBText2: TDBText;
    DBText5: TDBText;
    dbSup: TDBText;
    DBText7: TDBText;
    Label13: TLabel;
    edSN: TM207DBEdit;
    deNDate: TDBDateEdit;
    dg1: TDBGridEh;
    acList: TActionList;
    prdg1: TPrintDBGridEh;
    acDelete: TAction;
    acClose: TAction;
    acPrint: TAction;
    acCloseInv: TAction;
    fr1: TM207FormStorage;
    acOpenInv: TAction;
    puid: TPanel;
    eduid: TEdit;
    Label3: TLabel;
    dbSsf: TDBText;
    dbedSdate: TDBEditEh;
    btDate: TBitBtn;
    siHelp: TSpeedItem;
    procedure acDeleteUpdate(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure acOpenInvExecute(Sender: TObject);
    procedure eduidKeyPress(Sender: TObject; var Key: Char);
    procedure eduidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spitPrintClick(Sender: TObject);
    procedure btDateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSurPlus: TfmSurPlus;

implementation
uses data, data3, comdata, M207Proc, FIBQuery, dbUtil, pFIBQuery, DB, reportdata,
     servdata, SetSDate, MsgDialog;
{$R *.dfm}

procedure TfmSurPlus.acDeleteUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quSurPlusItemSITEMID.IsNull)
  and (dm.taSListISCLOSED.AsInteger=0);
end;

procedure TfmSurPlus.acDeleteExecute(Sender: TObject);
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
 dm3.quSurPlusItem.Delete;
 dm.taSList.Refresh;
end;

procedure TfmSurPlus.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmSurPlus.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 ReOpenDataSets([dm3.quSurPlusItem]);
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 if dm.taSListISCLOSED.AsInteger=1 then siCloseInv.Action:=acOpenInv
 else siCloseInv.Action:=acCloseInv;
 ActiveControl:=eduid;
 dm.WorkMode:='SURPLUS';
 Label2.Visible := not dm.taSListCLTID.IsNull;
 deNDate.Visible := not dm.taSListCLTID.IsNull;
 dbSsf.Visible := not dm.taSListCLTID.IsNull;
{ if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
  else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
   else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
    else  dmserv.ComScan.ComPort:=pnCOM4;

 dmServ.ComScan.Connect;
 dmcom.SScanZ:='';   }

end;

procedure TfmSurPlus.acPrintUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= not dm3.quSurPlusItemSITEMID.IsNull;
end;

procedure TfmSurPlus.acPrintExecute(Sender: TObject);
begin
// VirtualPrinter.Orientation := poLandscape;
 prdg1.Print;
end;

procedure TfmSurPlus.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 PostDataSets([dm.taSList]);
 dm.taSList.Refresh;
 if dm.taSListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSet(dm.taSList);
 end else if not dm.closeinv(dm.taSListSINVID.AsInteger,0) then
 begin
  if dm.EmptyInv(dm.taSListSINVID.AsInteger,0) then
  begin
   ExecSQL('delete from sinv where sinvid='+dm.taSListSINVID.AsString, dm.quTmp);
   ReOpenDataSet(dm.taSList);
  end
  else if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
 end;
 dm.taSList.Refresh;
 CloseDataSets([dm3.quSurPlusItem]);
// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 dm.WorkMode:='SINV';
end;

procedure TfmSurPlus.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
end;

procedure TfmSurPlus.acCloseInvExecute(Sender: TObject);
var i:integer;
    s_close, s_closed:string;
begin
 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;

 try
  Screen.Cursor := crSQLWait;
  PostDataSet(dm.taSList);
  if dm.taSListSN.IsNull then
  begin
   with dm, quTmp do
   begin
    close;
    SQL.Text:='SELECT max(sn) FROM SINV WHERE ITYPE in (1, 21) AND DEPID='+inttostr(taSListDEPID.AsInteger)+' AND FYEAR(SDATE)='+GetSYear(taSListSDate.AsDateTime);
    ExecQuery;
    if Fields[0].IsNull then i:=1 else  i:=Fields[0].AsInteger+1;
    Close;
    Transaction.CommitRetaining;
   end;
   dm.taSList.Edit;
   dm.taSListSN.AsInteger:=i;
   dm.taSList.Post;
   dm.taSList.Refresh;
  end;

  with dm, quTmp do
  begin
   close;
   SQL.Text:='SELECT COUNT(*) FROM SINV WHERE ITYPE in (1, 21) AND DEPID='+inttostr(taSListDEPID.AsInteger)+' and SN='+taSListSN.AsString+' AND FYEAR(SDATE)='+GetSYear(taSListSDate.AsDateTime);
   ExecQuery;
   i:=Fields[0].AsInteger;
   Close;
   Transaction.CommitRetaining;
   if i>1 then raise Exception.Create('������������ ����� ���������!!!');
  end;

  with dm, quTmp do
  begin
   {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
   SQL.Text:='select noedit, noedited from Edit_Date_Inv ('+taSListSINVID.AsString+')';
   ExecQuery;
   s_close:=trim(Fields[0].AsString);
   s_closed:=trim(Fields[1].AsString);
   Transaction.CommitRetaining;
   close;
  end;
  Screen.Cursor := crDefault;
  if (s_close='') and (s_closed='') then
  begin
   Screen.Cursor := crSQLWait;
   with dm, qutmp do
   begin
    close;
    sql.Text:='SELECT PR FROM CloseInv('+dm.taSListSINVID.AsString+', 21, '+IntToStr(dmCom.UserId)+')';
    ExecQuery;
    PActAfterSInv:=(Fields[0].AsInteger=1);
    close;
    Transaction.CommitRetaining
   end;
   Screen.Cursor := crDefault;
  end
  else begin
    if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
    if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
  end;

  dm.taSList.Refresh;

  if (s_close='') and (s_closed='') then siCloseInv.Action:=acOpenInv;
 finally
  Screen.Cursor := crDefault;
 end;
end;

procedure TfmSurPlus.acOpenInvExecute(Sender: TObject);
begin
 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
 try
  Screen.Cursor := crSQLWait;
  ExecSQL('update sinv set isclosed=0 where sinvid='+dm.taSListSINVID.AsString, dm.quTmp);
  dm.taSList.Refresh;
  siCloseInv.Action:=acCloseInv;
 finally
  Screen.Cursor := crDefault;
 end;
end;

procedure TfmSurPlus.eduidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else SysUtils.Abort;
 end;
end;

procedure TfmSurPlus.eduidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var fexsits:boolean;
    i, sitemid:integer;
begin
  sitemid := 0;
 case key of
  VK_RETURN: begin
   if dm.taSListISCLOSED.AsInteger=1 then
   begin
     MessageDialog('��������� �������', mtWarning, [mbOk], 0);
     edUid.Text:='';
   end
   else
   if eduid.Text='' then MessageDialog('������� ����� �������!', mtError, [mbOk], 0)
   else begin
    with dm, qutmp do
    begin
     close;
     sql.Text:='select fres, sitemid from SurPlus_I ('+eduid.Text+', '+dm.taSListSINVID.AsString+')';
     ExecQuery;
     if (not Fields[1].IsNull) then sitemid:=Fields[1].AsInteger;
     i:=Fields[0].AsInteger;
     close;
     Transaction.CommitRetaining;
    end;

    fexsits:=false;
    case i of
     -1 : MessageDialog('������� � ����� ������� �� ������������ �� ������', mtWarning, [mbOk], 0);
      0 : MessageDialog('������� �� ������', mtWarning, [mbOk], 0);
      2 : MessageDialog('������� �������', mtWarning, [mbOk], 0);
      4 : MessageDialog('������� ������� �����', mtWarning, [mbOk], 0);
      5 : MessageDialog('������� ���������� ����������', mtWarning, [mbOk], 0);
      6 : fexsits:=true
     else MessageDialog('������ � ��. ���������� � �������������!', mtWarning, [mbOk], 0);
    end;

    if fexsits then
    begin
     ReOpenDataSets([dm3.quSurPlusItem]);
     dm3.quSurPlusItem.Locate('SITEMID', sitemid, []);
     dm.taSList.Refresh;
    end;
    eduid.Text:='';
   end
  end;
  VK_DOWN: ActiveControl:=dg1;
 end
end;

procedure TfmSurPlus.spitPrintClick(Sender: TObject);
var arr : TarrDoc;
begin
  try
    arr:=gen_arr(dg1, dm3.quSurPlusItemSITEMID);
    PrintTag(arr, it_tag,3);
  finally
    Finalize(arr);
  end;
end;

procedure TfmSurPlus.btDateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taSListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm.taSListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm.taSListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm.taSlist.Edit;
    dm.taSlistSDATE.AsDateTime:=d;
    dm.taSlist.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmSurPlus.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100204)
end;

procedure TfmSurPlus.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

end.
