unit SRet_H;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmSRet_H = class(TForm)
    Label1: TLabel;
    dg1: TDBGridEh;
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    OpSBtn: TSpeedButton;
    ClSBtn: TSpeedButton;
    CnlSBtn: TSpeedButton;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    DataSource1: TDataSource;
    LogEd: TEdit;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    Panel1: TPanel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBText2: TDBText;
    dbSup: TDBText;
    Label8: TLabel;
    dbt_Gruzootprav: TDBText;
    edSN: TDBEdit;
    dbedSdate: TDBEditEh;
    bttime: TBitBtn;
    Label12: TLabel;
    DBText1: TDBText;
    Label9: TLabel;
    procedure OpSBtnClick(Sender: TObject);
    procedure ClSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure ClAllSBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmSRet_H: TfmSRet_H;
  var log,s:string;
  procedure Hist(var user, st: string);
implementation

   uses comdata, data, SInv, MsgDialog, DInvItem, M207IBLogin, SRet;

   {$R *.dfm}
   procedure Hist(var user, st: string); //���������� ������ � ������� INVHIST
begin
     fmSRet_H.taHist.Append;
     fmSRet_H.taHistDOCID.AsInteger:=dm.taSRetListSINVID.AsInteger;
     fmSRet_H.taHistFIO.Value := user;
     fmSRet_H.taHistSTATUS.Value := st;
     fmSRet_H.taHist.Post;
end;


procedure TfmSRet_H.OpSBtnClick(Sender: TObject);
  //�������� ���������
begin
 taEmp.Active:=true;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin
   log:=LogEd.Text;
   s:='�������';
   Hist(log, s);
   fmSRet.acOpenInv.Execute;
   close;
   end;
     taEmp.Active:=false;
end;

procedure TfmSRet_H.ClSBtnClick(Sender: TObject);  //�������� ���������
begin
 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
 if LogEd.Text='' then log:=dmCom.User.Alias;
     s:='�������';
     Hist(log, s);
     fmSRet.acCloseInv.Execute;
     close;
    end;
end;

procedure TfmSRet_H.CnlSBtnClick(Sender: TObject);  //������
begin
 taHist.Active:=false;
 close;
end;

procedure TfmSRet_H.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  taHist.ParamByName('INVID').AsInteger := dm.taSRetListSINVID.AsInteger;
end;

procedure TfmSRet_H.FormCreate(Sender: TObject);
begin
   taHist.Active := True;
   dg1.DataSource.DataSet.Last;    //��������� ���������
end;

procedure TfmSRet_H.FormActivate(Sender: TObject);
begin
  Pswed.SetFocus;
  Caption:='�'+(dm.taSRetListSN.AsString);
  if dm.taSRetListISCLOSED.AsInteger=1 then
   begin
   fmSRet_H.ClSBtn.Enabled:=false;
     //���������� ���� �����������
    LogEd.Visible:=true;
    LogEd.Text:= dmCom.User.Alias;
    PswEd.Visible:=true;
    LaLogin.Visible:=true;
    LaPsw.Visible:=true;
   end
  else
   begin
   fmSRet_H.OpSBtn.Enabled:=false;//�������� ���� �����������
   LogEd.Visible:=false;
   PswEd.Visible:=false;
   LaLogin.Visible:=false;
   LaPsw.Visible:=false;
   end;
end;

procedure TfmSRet_H.ClAllSBtnClick(Sender: TObject);
begin
  taHist.Active := false;
  close;
end;

procedure TfmSRet_H.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 taHist.Active := false;
end;

procedure TfmSRet_H.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSRet_H.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmSRet_H.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if (key=vk_return) and (ClSBtn.Enabled=true) then ClSBtn.Click;
if key=vk_escape then cnlSbtn.Click;
end;

end.
