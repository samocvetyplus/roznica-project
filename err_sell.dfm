object fmErr_sell: TfmErr_sell
  Left = 207
  Top = 107
  Width = 681
  Height = 316
  AutoSize = True
  Caption = #1054#1096#1080#1073#1082#1080' '#1087#1088#1080' '#1087#1088#1086#1076#1072#1078#1072#1093' ('#1085#1077#1076#1086#1087#1088#1086#1076#1072#1078#1072')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 0
    Width = 673
    Height = 257
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataSource = dmServ.dserr_sell
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RN'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'BUSYTYPE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SELLITEMID'
        Footers = <>
      end>
  end
  object BCor: TButton
    Left = 264
    Top = 264
    Width = 75
    Height = 25
    Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
    TabOrder = 1
    OnClick = BCorClick
  end
end
