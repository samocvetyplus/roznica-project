{*************************************}
{  ������ ����� ����                  }
{  Copyrigth (C) 2005 basile for CDM  }
{  v0.02                              }
{*************************************}
unit CashBank;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, Placemnt, ImgList, SpeedBar, ExtCtrls,
  ComCtrls, Grids, DBGridEh, TB2Dock, TB2Toolbar, TB2Item, DB, FIBDataSet,
  pFIBDataSet, StdCtrls, Mask, DBCtrlsEh;

type
  TfmCashBank = class(TForm)
    stbrStatus: TStatusBar;
    ilButtons: TImageList;
    ActionList2: TActionList;
    gridCash: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    acPeriod: TAction;
    taCashBank: TpFIBDataSet;
    dsrCashBank: TDataSource;
    taCashBankID: TFIBIntegerField;
    taCashBankCOMPID: TFIBIntegerField;
    taCashBankCOST: TFIBFloatField;
    taCashBankPAYDATE: TFIBDateTimeField;
    acAdd: TAction;
    acDel: TAction;
    taCashBankCOMPNAME2: TFIBStringField;
    TBSeparatorItem1: TTBSeparatorItem;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    cbComp: TDBComboBoxEh;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure taCashBankBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taCashBankNewRecord(DataSet: TDataSet);
    procedure cbCompChange(Sender: TObject);
    procedure taCashBankBeforeClose(DataSet: TDataSet);
  end;

var
  fmCashBank: TfmCashBank;

implementation

uses InvData, dbUtil, DictData, Period, dbTree;

{$R *.dfm}

procedure TfmCashBank.FormCreate(Sender: TObject);
begin
  // ������ �� �����������
  cbComp.OnChange := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  cbComp.OnChange := cbCompChange;
  inherited;
  acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
  OpenDataSet(taCashBank);
end;

procedure TfmCashBank.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taCashBank);
  inherited;
end;

procedure TfmCashBank.acPeriodExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  Bd := dm.BeginDate;
  Ed := dm.EndDate;
  if ShowPeriodForm(Bd, Ed) then
  begin
    dm.BeginDate := Bd;
    dm.EndDate := Ed;
    acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
    Application.ProcessMessages;
    ReOpenDataSet(taCashBank);
    Application.ProcessMessages;
  end;
end;

procedure TfmCashBank.acAddExecute(Sender: TObject);
begin
  taCashBank.Append;
end;

procedure TfmCashBank.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := taCashBank.Active;
end;

procedure TfmCashBank.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := taCashBank.Active and (not taCashBank.IsEmpty);
end;

procedure TfmCashBank.acDelExecute(Sender: TObject);
begin
  taCashBank.Delete;
end;

procedure TfmCashBank.taCashBankBeforeOpen(DataSet: TDataSet);
begin
  taCashBank.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
  taCashBank.ParamByName('BD').AsDateTime := dm.BeginDate;
  taCashBank.ParamByName('ED').AsDateTime := dm.EndDate;
  if cbComp.ItemIndex = 0 then
  begin
    taCashBank.ParamByName('COMPID1').AsInteger := -MAXINT;
    taCashBank.ParamByName('COMPID2').AsInteger := MAXINT;
  end
  else begin
    taCashBank.ParamByName('COMPID1').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
    taCashBank.ParamByName('COMPID2').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
  end;
end;

procedure TfmCashBank.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmCashBank.taCashBankNewRecord(DataSet: TDataSet);
begin
  taCashBankID.AsInteger := dm.GetId(94);
  taCashBankCOST.AsFloat := 0;
  taCashBankPAYDATE.AsDateTime := Now;
end;

procedure TfmCashBank.cbCompChange(Sender: TObject);
begin
  PostDataSet(taCashBank);
  ReOpenDataSet(taCashBank);
end;

procedure TfmCashBank.taCashBankBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

end.
