unit ShopReport_CheckData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet,
  ActnList, ComCtrls, StdCtrls, DBCtrls, jpeg, DBGridEhGrouping,
  rxSpeedbar, GridsEh;

type
  TfmShopReport_CheckData = class(TForm)
    gridCheckData: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    taShopReport_CheckData: TpFIBDataSet;
    dsrShopReport_CheckData: TDataSource;
    taShopReport_CheckDataID: TFIBIntegerField;
    taShopReport_CheckDataUSERID: TFIBIntegerField;
    taShopReport_CheckDataUID: TFIBIntegerField;
    taShopReport_CheckDataQ0: TFIBFloatField;
    taShopReport_CheckDataWHBC2: TFIBFloatField;
    taShopReport_CheckDataWHEC2: TFIBFloatField;
    taShopReport_CheckDataDC2: TFIBFloatField;
    taShopReport_CheckDataDC22: TFIBFloatField;
    taShopReport_CheckDataAPC2: TFIBFloatField;
    taShopReport_CheckDataSELLC2: TFIBFloatField;
    taShopReport_CheckDataRETC2: TFIBFloatField;
    acEvent: TActionList;
    acRefresh: TAction;
    taShopReport_CheckDataC2: TFIBFloatField;
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    taT: TpFIBDataSet;
    dsrT: TDataSource;
    txtDP: TDBText;
    txtDM: TDBText;
    txtI: TDBText;
    txtSell: TDBText;
    txtSellRet: TDBText;
    Label7: TLabel;
    taTWHBC2: TFIBFloatField;
    taTWHEC2: TFIBFloatField;
    taTDC2: TFIBFloatField;
    taTDC22: TFIBFloatField;
    taTAPC2: TFIBFloatField;
    taTSELLC2: TFIBFloatField;
    taTRETC2: TFIBFloatField;
    txtOut: TDBText;
    txtAP: TDBText;
    Label8: TLabel;
    txtR: TDBText;
    taTT: TFloatField;
    acClear: TAction;
    siClear: TSpeedItem;
    taShopReport_CheckDataAAC2: TFIBFloatField;
    taTAAC2: TFIBFloatField;
    Label9: TLabel;
    txtAAC2: TDBText;
    SpeedItem1: TSpeedItem;
    acUIDHistory: TAction;
    procedure taShopReport_CheckDataBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure gridCheckDataGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure taTBeforeOpen(DataSet: TDataSet);
    procedure taTCalcFields(DataSet: TDataSet);
    procedure acClearExecute(Sender: TObject);
    procedure acUIDHistoryExecute(Sender: TObject);
    procedure acUIDHistoryUpdate(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmShopReport_CheckData: TfmShopReport_CheckData;

implementation

uses comdata, fmUtils, dbUtil, ShopReportList, UIDHist, Data;

{$R *.dfm}

{ TfmShopReport_CheckData }

procedure TfmShopReport_CheckData.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmShopReport_CheckData.taShopReport_CheckDataBeforeOpen(DataSet: TDataSet);
begin
  taShopReport_CheckData.ParamByName('USERID').AsInteger :=  fmShopReportList.taShopReportListSINVID.AsInteger;
end;

procedure TfmShopReport_CheckData.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  OpenDataSets([taShopReport_CheckData, taT]);
end;

procedure TfmShopReport_CheckData.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taShopReport_CheckData, taT]);
  Action := caFree;
end;

procedure TfmShopReport_CheckData.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmShopReport_CheckData.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmShopReport_CheckData.acRefreshExecute(Sender: TObject);
begin
  CloseDataSets([taShopReport_CheckData, taT]);
  ExecSQL('execute procedure ShopReport_CheckData('+fmShopReportList.taShopReportListSINVID.AsString+')', dmCom.quTmp);
  OpenDataSets([taShopReport_CheckData, taT]);
end;

procedure TfmShopReport_CheckData.gridCheckDataGetCellParams(
  Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if abs(taShopReport_CheckDataWHEC2.AsFloat - taShopReport_CheckDataC2.AsFloat)>0.001
  then Background := clRed
  else Background := clWindow;
end;

procedure TfmShopReport_CheckData.taTBeforeOpen(DataSet: TDataSet);
begin
  taT.ParamByName('USERID').AsInteger := fmShopReportList.taShopReportListSINVID.AsInteger;
end;

procedure TfmShopReport_CheckData.taTCalcFields(DataSet: TDataSet);
begin
  taTT.AsFloat := taTWHBC2.AsFloat + (taTDC2.AsFloat + taTAPC2.AsFloat + taTRETC2.AsFloat)
   - (taTDC22.AsFloat + taTSELLC2.AsFloat);
end;

procedure TfmShopReport_CheckData.acClearExecute(Sender: TObject);
begin
  CloseDataSets([taShopReport_CheckData, taT]);
  ExecSQL('delete from TMP_CHECKDATA_SHOPREPORT where USERID='+fmShopReportList.taShopReportListSINVID.AsString, dmCom.quTmp);
  OpenDataSets([taShopReport_CheckData, taT]);
end;

procedure TfmShopReport_CheckData.acUIDHistoryExecute(Sender: TObject);
begin
  dm.UID2Find := taShopReport_CheckDataUID.AsInteger;
  ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
end;

procedure TfmShopReport_CheckData.acUIDHistoryUpdate(Sender: TObject);
begin
  acUIDHistory.Enabled := taShopReport_CheckData.Active and (not taShopReport_CheckData.IsEmpty);
end;

end.
