unit ClSell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Buttons, db;

type
  TfmClSell = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ed1: TDBEdit;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    b: boolean;
    MaxSDate: TDateTime;
  public
    { Public declarations }
  end;

var
  fmClSell: TfmClSell;

implementation

uses Data, Data2, comdata;

{$R *.DFM}

procedure TfmClSell.FormCreate(Sender: TObject);
var t1: TDateTime;
    i: integer;
begin
  with dm, ed1 do
    begin
      if (WorkMode='SELL')or(WorkMode='SELLCH') then DataSource:=dsCurSell
      else DataSource:=dsSellList;
      with DataSource.DataSet do
        if FieldByName('ED').AsFloat<1 then
          begin
            b:=True;
            t1:=dmCom.GetServerTime;
            i:=FieldByName('SELLID').AsInteger;
            with dm2.quMaxSDate do
              begin
                Params[0].AsInteger:=i;
                ExecQuery;
                MaxSDate:=Fields[0].AsDateTime;
                Close;
              end;
            if NOT (State in [dsEdit, dsInsert]) then Edit;
            if t1>MaxSDate then FieldByName('ED').AsDateTime:=t1
            else  FieldByName('ED').AsDateTime:=MaxSDate;
          end;
    end;
end;

procedure TfmClSell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult=mrCancel then
    begin
      with dm, ed1.DataSource.DataSet do
        if b then
          begin
            if NOT (State in [dsEdit, dsInsert]) then Edit;
            FieldByName('ED').Clear;
         end
        else if  ed1.DataSource.DataSet.State in [dsInsert, dsEdit] then ed1.DataSource.DataSet.Cancel;
    end    
  else if ed1.DataSource.DataSet.FieldByName('ED').AsDateTime<MaxSDate then
         raise Exception.Create('����� �� ����� ���� ������� ������ '+DateTimeToStr(MaxSDate));
end;

end.
