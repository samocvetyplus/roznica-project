object fmRetCltList: TfmRetCltList
  Left = 204
  Top = 226
  HelpContext = 100242
  Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
  ClientHeight = 486
  ClientWidth = 885
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 885
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 546
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 482
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 885
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dmServ.pmRetClt
      Hint = #1042#1099#1073#1086#1088' '#1080#1089#1093#1086#1076#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 306
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb3: TSpeedBar
    Left = 0
    Top = 69
    Width = 885
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 24
    BtnHeight = 23
    TabOrder = 2
    InternalVer = 1
    object lbFindUID: TLabel
      Left = 8
      Top = 8
      Width = 105
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1080#1076'. '#1085#1086#1084#1077#1088#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbFindArt: TLabel
      Left = 232
      Top = 8
      Width = 70
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1087#1086' '#1072#1088#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object edFindUID: TEdit
      Left = 120
      Top = 4
      Width = 97
      Height = 21
      TabOrder = 0
      Text = 'edFindUID'
      OnKeyDown = edFindUIDKeyDown
      OnKeyPress = edFindUIDKeyPress
    end
    object edFindArt: TEdit
      Left = 304
      Top = 4
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'edFindArt'
      OnKeyDown = edFindArtKeyDown
    end
  end
  object gridRetItem: TDBGridEh
    Left = 0
    Top = 98
    Width = 885
    Height = 369
    Align = alClient
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsRetCltList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridRetItemGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.ValueType = fvtCount
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'SELLN'
        Footers = <>
        Title.EndEllipsis = True
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'CHECKNO'
        Footers = <>
        Title.Caption = #8470' '#1085#1072#1082#1083'. '#1074#1086#1079#1074#1088#1072#1090#1072
      end
      item
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1044#1072#1090#1072
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'CLIENTNAME'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        Width = 81
      end
      item
        EditButtons = <>
        FieldName = 'RET'
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1055#1088#1080#1095#1080#1085#1072
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
      end
      item
        Color = clAqua
        EditButtons = <>
        FieldName = 'SPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072'|'#1055#1088#1080#1093#1086#1076#1072
      end
      item
        EditButtons = <>
        FieldName = 'PRICE2'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072'|'#1056#1077#1072#1083#1080#1079#1072#1094#1080#1080
        Title.EndEllipsis = True
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'RETPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072'| '#1056#1072#1089#1093#1086#1076#1085#1072#1103
        Title.EndEllipsis = True
      end
      item
        Color = clWhite
        EditButtons = <>
        FieldName = 'CURRPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072'|'#1058#1077#1082#1091#1097#1072#1103
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.FieldName = 'SCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1055#1088#1080#1093#1086#1076#1072
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'COST2'
        Footer.FieldName = 'COST2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1056#1077#1072#1083#1080#1079#1072#1094#1080#1080
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST'
        Footer.FieldName = 'RETCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'|'#1056#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNT'
        Footers = <
          item
            FieldName = 'ACTDISCOUNT'
            ValueType = fvtSum
          end>
        Title.Caption = #1040#1082#1090' '#1089#1082#1080#1076#1082#1080
      end
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Width = 118
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      end
      item
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 467
    Width = 885
    Height = 19
    Panels = <>
  end
  object fmstrRetClt: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 24
    Top = 156
  end
  object acEvent: TActionList
    Left = 88
    Top = 156
    object acPrintAppl: TAction
      Tag = 3
      Caption = #1047#1072#1103#1074#1083#1077#1085#1080#1077
      OnExecute = acPrintApplExecute
      OnUpdate = acPrintUpdate
    end
    object acPrintDisv: TAction
      Tag = 2
      Caption = #1056#1072#1089#1093#1086#1076#1085#1099#1081' '#1086#1088#1076#1077#1088
      OnExecute = acPrintDisvExecute
      OnUpdate = acPrintUpdate
    end
  end
  object ppPrint: TTBPopupMenu
    Left = 89
    Top = 204
    object tbTagPrint: TTBSubmenuItem
      Caption = #1041#1080#1088#1082#1072
    end
    object TBItem2: TTBItem
      Action = acPrintAppl
    end
    object TBItem1: TTBItem
      Action = acPrintDisv
    end
  end
end
