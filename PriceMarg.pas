unit PriceMarg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, Menus, StdCtrls, Mask, DBCtrls, RxDBComb, jpeg,
  rxSpeedbar;

type
  TfmPrMargin = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    dg1: TM207IBGrid;
    pm1: TPopupMenu;
    siSet: TSpeedItem;
    N1: TMenuItem;
    tb2: TSpeedBar;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siSetClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmPrMargin: TfmPrMargin;

implementation

uses comdata, Data, M207Proc;

{$R *.DFM}

procedure TfmPrMargin.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmPrMargin.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPrMargin.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmPrMargin.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dm, quTmp do
      begin
        SQL.Text:='update d_dep set tmargin=margin, troundnorm=roundnorm ';
        ExecQuery;
      end;
  with dmCom, quTmp do
      begin
        SQL.Text:='update d_rec set topmargin=opmargin, toptrnorm=optrnorm';
        ExecQuery;
        tr.CommitRetaining;
      end;
  with dmCom, dm do
    begin
      OpenDataSets([taPrMargin, taRec]);
    end;
end;

procedure TfmPrMargin.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmCom, dm  do
    begin
      PostDataSets([taPrMargin, taRec]);
      CloseDataSets([taPrMargin, taRec]);
    end;
end;

procedure TfmPrMargin.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='NAME') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmPrMargin.siSetClick(Sender: TObject);
begin
  with dmCom, dm, quTmp do
    begin
      PostDataSets([taPrMargin]);
      SQL.Text:='execute procedure SETMARGINT '+taPrMarginD_DepId.AsString;
      ExecQuery;
      tr.CommitRetaining;
      with taPrMArgin do
        begin
          Active:=False;
          Open;
        end;
    end;
end;

procedure TfmPrMargin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;  
end;

end.
