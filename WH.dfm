object fmWH: TfmWH
  Left = 224
  Top = 177
  HelpContext = 100302
  Caption = #1057#1082#1083#1072#1076
  ClientHeight = 563
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object spltWH: TSplitter
    Left = 0
    Top = 350
    Width = 843
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 843
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 70
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 743
      Top = 3
      Visible = True
      OnClick = siExitClick0
      SectionName = 'Untitled (0)'
    end
    object spitCurrStArt: TSpeedItem
      BtnCaption = #1048#1085#1092#1086#1088#1084'.'
      Caption = #1048#1085#1092#1086#1088#1084'.'
      Hint = #1048#1085#1092#1086#1088#1084'.|'
      ImageIndex = 62
      Spacing = 1
      Left = 73
      Top = 3
      Visible = True
      OnClick = spitCurrStArtClick
      SectionName = 'Untitled (0)'
    end
    object spitHist: TSpeedItem
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103
      Hint = #1048#1089#1090#1086#1088#1080#1103'|'
      Spacing = 1
      Left = 143
      Top = 3
      Visible = True
      OnClick = spitHistClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 283
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object spitAppl: TSpeedItem
      BtnCaption = #1047#1072#1103#1074#1082#1072
      Caption = #1047#1072#1103#1074#1082#1072
      DropDownMenu = pmappldep
      Hint = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1091'|'
      ImageIndex = 72
      Spacing = 1
      Left = 213
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object spcalc: TSpeedItem
      BtnCaption = #1055#1077#1088#1077#1089#1095#1077#1090'(F5)'
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      Hint = #1055#1077#1088#1077#1089#1095#1077#1090'|'
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spcalcClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 673
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object stbtWh: TStatusBar
    Left = 0
    Top = 544
    Width = 843
    Height = 19
    Panels = <
      item
        Text = 'F2 - '#1040#1088#1090#1080#1082#1091#1083#1099'; F3 - '#1048#1079#1076#1077#1083#1080#1103'; F4 - '#1056#1072#1079#1084#1077#1088#1099'; F6 - '#1055#1086#1089#1090#1072#1074#1082#1072
        Width = 300
      end
      item
        Text = 'Ctrl+P - '#1055#1077#1095#1072#1090#1100'; F5 - '#1042#1099#1076#1077#1083#1077#1085#1080#1077'; F12 - '#1042#1099#1093#1086#1076' '
        Width = 250
      end
      item
        Text = #1056#1072#1089#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Width = 200
      end>
    SimpleText = 
      'F2 - '#1040#1088#1090#1080#1082#1091#1083#1099'; F3 - '#1048#1079#1076#1077#1083#1080#1103'; F4 - '#1056#1072#1079#1084#1077#1088#1099'; F6 - '#1055#1086#1089#1090#1072#1074#1082#1072'; F12 - ' +
      #1042#1099#1093#1086#1076'; F5 - '#1042#1099#1076#1077#1083#1077#1085#1080#1077';'
  end
  object plWh: TPanel
    Left = 0
    Top = 497
    Width = 843
    Height = 47
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 2
    object lbWHInfo: TLabel
      Left = 7
      Top = 2
      Width = 67
      Height = 13
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtArt: TDBText
      Left = 589
      Top = 3
      Width = 792
      Height = 13
      DataField = 'TA'
      DataSource = dmServ.dsrWH_T
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object txtQ: TDBText
      Left = 588
      Top = 18
      Width = 793
      Height = 15
      DataField = 'TQ'
      DataSource = dmServ.dsrWH_T
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label2: TLabel
      Left = 8
      Top = 16
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbW: TLabel
      Left = 8
      Top = 32
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtW: TDBText
      Left = 588
      Top = 32
      Width = 781
      Height = 13
      DataField = 'TW'
      DataSource = dmServ.dsrWH_T
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object lblArt: TLabel
      Left = 80
      Top = 4
      Width = 23
      Height = 13
      Caption = 'lblArt'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblQ: TLabel
      Left = 80
      Top = 16
      Width = 18
      Height = 13
      Caption = 'lblQ'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lblW: TLabel
      Left = 80
      Top = 32
      Width = 21
      Height = 13
      Caption = 'lblW'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object pgctrl: TPageControl
    Left = 0
    Top = 353
    Width = 843
    Height = 144
    ActivePage = tbshUID
    Align = alBottom
    Images = dmCom.ilButtons
    TabOrder = 3
    OnChange = pgctrlChange
    object tbshUID: TTabSheet
      Tag = 4
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      object ibgrWHUID: TM207IBGrid
        Left = 0
        Top = 0
        Width = 835
        Height = 115
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsWHUID
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = ibgrWHUIDGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 76
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 38
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 51
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SPRICE'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1087#1088#1080#1093
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 65
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 70
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SUP'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 135
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'D_GOODS_SAM1'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077' 1'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 123
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'D_GOODS_SAM2'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 131
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SNAME'
            ReadOnly = True
            Title.Caption = #1057#1082#1083#1072#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'APPLDEPQ'
            Title.Caption = #1047#1072#1103#1074#1082#1080' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end>
      end
    end
    object tbshSz: TTabSheet
      Tag = 1
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 47
      object grSz: TM207IBGrid
        Left = 0
        Top = 0
        Width = 835
        Height = 115
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHSZ
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = grSzGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Alignment = taCenter
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 72
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 73
            Visible = True
          end
          item
            Alignment = taRightJustify
            Color = clInfoBk
            Expanded = False
            FieldName = 'SNAME'
            Title.Alignment = taCenter
            Title.Caption = #1057#1082#1083#1072#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
    object tbshArt2: TTabSheet
      Tag = 2
      Caption = #1040#1088#1090'2'
      ImageIndex = 9
      object ibgrA2: TM207IBGrid
        Left = 0
        Top = 0
        Width = 835
        Height = 115
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHArt2
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleButtons = True
        OnGetCellParams = ibgrA2GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = False
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Caption = #1062#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SNAME'
            Title.Caption = #1057#1082#1083#1072#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 64
            Visible = True
          end>
      end
    end
    object tbshInv: TTabSheet
      Tag = 3
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      ImageIndex = 18
      object dbgrWHInv: TM207IBGrid
        Left = 0
        Top = 0
        Width = 835
        Height = 115
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHInv
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dbgrWHInvGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        TitleDblClick = True
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 101
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SSF0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN0'
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 97
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SPRICE'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SC'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SW'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
  end
  object spbr4: TSpeedBar
    Left = 0
    Top = 40
    Width = 843
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 4
    InternalVer = 1
    object Label1: TLabel
      Left = 172
      Top = 8
      Width = 23
      Height = 13
      Caption = #1052#1072#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 320
      Top = 8
      Width = 31
      Height = 13
      Caption = #1058#1086#1074#1072#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 484
      Top = 8
      Width = 22
      Height = 13
      Caption = #1048#1079#1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object SpeedButton1: TSpeedButton
      Left = 660
      Top = 3
      Width = 25
      Height = 22
      Hint = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnDoClick
    end
    object lbFind: TLabel
      Left = 4
      Top = 8
      Width = 61
      Height = 13
      Caption = #1055#1086#1080#1089#1082' ('#1072#1088#1090'.)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 719
      Top = 7
      Width = 66
      Height = 13
      Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
      Transparent = True
    end
    object chbxInfo: TCheckBox
      Left = 701
      Top = 8
      Width = 12
      Height = 12
      Hint = #1054#1090#1086#1073#1088#1072#1078#1072#1090#1100' '#1087#1086#1083#1085#1091#1102' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1089#1086#1089#1090#1086#1103#1085#1080#1080' '#1072#1088#1090#1080#1082#1091#1083#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = chbxInfoClick
    end
    object edArt: TComboEdit
      Left = 68
      Top = 4
      Width = 101
      Height = 21
      Color = 16776176
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      NumGlyphs = 1
      TabOrder = 1
      Text = 'edArt'
      OnButtonClick = edArtButtonClick
      OnChange = edArtChange
      OnKeyDown = edArtKeyDown
      OnKeyUp = edArtKeyUp
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem8: TSpeedItem
      BtnCaption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Hint = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 515
      Top = 3
      Visible = True
      OnClick = SpeedItem8Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem9: TSpeedItem
      BtnCaption = #1058#1086#1074#1072#1088
      Caption = #1058#1086#1074#1072#1088
      Hint = #1058#1086#1074#1072#1088'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 355
      Top = 3
      Visible = True
      OnClick = SpeedItem9Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem10: TSpeedItem
      BtnCaption = #1052#1072#1090#1077#1088#1080#1072#1083
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = SpeedItem10Click
      SectionName = 'Untitled (0)'
    end
  end
  object ibgrWH: TDBGridEh
    Left = 0
    Top = 69
    Width = 843
    Height = 281
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsrWH
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = ibgrWHGetCellParams
    OnKeyDown = ibgrWHKeyDown
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ATT1'
        Footers = <>
        Title.Caption = #1040#1090#1088#1080#1073#1091#1090' 1'
        Width = 132
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'ATT2'
        Footers = <>
        Title.Caption = #1040#1090#1088#1080#1073#1091#1090' 2'
        Width = 115
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COUNTRYID'
        Footers = <>
        ReadOnly = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'COMPCODE'
        Footers = <>
        ReadOnly = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UNITID'
        Footers = <>
        ReadOnly = True
        Width = 49
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'MAT'
        Footers = <>
        ReadOnly = True
        Width = 39
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'GOOD'
        Footers = <>
        ReadOnly = True
        Width = 41
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'INS'
        Footers = <>
        ReadOnly = True
        Width = 30
      end
      item
        Color = clMoneyGreen
        EditButtons = <>
        FieldName = 'ZQ'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1072
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        ReadOnly = True
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        ReadOnly = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object chlbMat: TCheckListBox
    Left = 196
    Top = 75
    Width = 140
    Height = 130
    OnClickCheck = chlbMatClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 5
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbGood: TCheckListBox
    Left = 355
    Top = 65
    Width = 140
    Height = 129
    OnClickCheck = chlbGoodClickCheck
    Enabled = False
    Flat = False
    ItemHeight = 13
    TabOrder = 6
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbProd: TCheckListBox
    Left = 515
    Top = 67
    Width = 140
    Height = 129
    OnClickCheck = chlbProdClickCheck
    Flat = False
    ItemHeight = 13
    TabOrder = 7
    Visible = False
    OnExit = chlbMatExit
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 136
    Top = 160
  end
  object ppWH: TPopupMenu
    Images = dmCom.ilButtons
    OnPopup = ppWHPopup
    Left = 432
    Top = 176
    object N1: TMenuItem
      Caption = #1040#1088#1090#1080#1082#1091#1083#1099
      ImageIndex = 9
      ShortCut = 113
      OnClick = siArtClick
    end
    object N2: TMenuItem
      Caption = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      ShortCut = 114
      OnClick = siUIDClick
    end
    object N3: TMenuItem
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 47
      ShortCut = 115
      OnClick = siSzClick
    end
    object mnitInv: TMenuItem
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      ShortCut = 117
      OnClick = mnitInvClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object mnitLayoutArt: TMenuItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1072#1088#1090#1080#1082#1091#1083#1099
      OnClick = mnitPrintArtClick
    end
    object mnitLayoutUID: TMenuItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1080#1079#1076#1077#1083#1080#1103
      OnClick = mnitPrintUIDClick
    end
  end
  object pmPrint: TPopupMenu
    Left = 324
    Top = 176
    object mnitPrintArt: TMenuItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1100#1085#1086
      OnClick = mnitPrintArtClick
    end
    object mnitPrintUID: TMenuItem
      Caption = #1056#1072#1089#1082#1083#1072#1076#1082#1072' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
      Enabled = False
      OnClick = mnitPrintUIDClick
    end
  end
  object pmAppl: TPopupMenu
    Images = dmCom.ilButtons
    Left = 376
    Top = 176
    object mnitCrAppl: TMenuItem
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1091
      ImageIndex = 71
      OnClick = mnitCrApplClick
    end
    object mnitAddAppl: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1079#1072#1103#1074#1082#1077
      Enabled = False
      Visible = False
      OnClick = mnitAddApplClick
    end
    object mnitShowAppl: TMenuItem
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1079#1072#1103#1074#1082#1091
      OnClick = mnitShowApplClick
    end
    object mnitClearAppl: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' "'#1047#1072#1103#1074#1082#1072'"'
      OnClick = mnitClearApplClick
    end
  end
  object ActionList1: TActionList
    Left = 560
    Top = 240
    object acCalc: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090
      ShortCut = 116
      OnExecute = spcalcClick
    end
    object acApplDepCreate: TAction
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1079#1072#1103#1074#1082#1091
      OnExecute = acApplDepCreateExecute
      OnUpdate = acApplDepCreateUpdate
    end
    object acApplDepClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1087#1086#1083#1077' '#1079#1072#1103#1074#1082#1080
      OnExecute = acApplDepClearExecute
      OnUpdate = acApplDepCreateUpdate
    end
  end
  object pmappldep: TTBPopupMenu
    Left = 272
    Top = 240
    object biApplDepCreate: TTBItem
      Action = acApplDepCreate
    end
    object biApplDepClear: TTBItem
      Action = acApplDepClear
    end
  end
end
