unit DIns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, ActnList, rxSpeedbar;

type
  TfmDIns = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    spitPrint: TSpeedItem;
    siSort: TSpeedItem;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure siSortClick(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmDIns: TfmDIns;

implementation

uses comdata,  Sort, DBTree, M207Proc, Data;

{$R *.DFM}

procedure TfmDIns.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDIns.FormCreate(Sender: TObject);
var c: TColumn;
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      OpenDataSets([taIns, quEdgS, quEdgT]);
      c:=dg1['EDGTID'];
      with quEdgT do
        begin
          First;
          while NOT EOF do
            begin
              c.PickList.Add(quEdgTEdgetionID.AsString);
              Next;
            end;
        end;
    end;
end;

procedure TfmDIns.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSet(taIns);
      CloseDataSets([taIns, quEdgS, quEdgT]);
      tr.CommitRetaining;
    end;

end;

procedure TfmDIns.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDIns.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDIns.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taIns.Append;
end;

procedure TfmDIns.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taIns.Delete;
end;

procedure TfmDIns.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsIns, 33); // ins
end;

procedure TfmDIns.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_INSID, NAME '+
                       'FROM D_INS '+
                       'where D_INSid<>'#39+'-1000'+#39' '+                       
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_INS set SortInd=?SortInd where D_INSId=?D_INSId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taIns]);
            end
        end;
    finally
      fmSort.Free;
    end;

end;

procedure TfmDIns.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var j: integer;
    i: string;
begin
  with dmCom, taIns do
    if (Key in [9, VK_RETURN]) and (State=dsInsert) then
      begin
        j:=dg1.SelectedIndex;
        i:=taInsD_InsId.AsString;
        Post;
        Active:=False;
        Open;
        Locate('D_INSID', i, []);
        ActiveControl:=dg1;
        dg1.SelectedIndex:=j;
      end;
end;

procedure TfmDIns.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100105)
end;

procedure TfmDIns.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
