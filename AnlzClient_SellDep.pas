unit AnlzClient_SellDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, FIBDataSet,
  pFIBDataSet, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, dbUtil,
  ExtCtrls, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, rxSpeedbar;

type
  TfmAnlzClient_SellDep = class(TForm)
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    quAnlzClient: TpFIBDataSet;
    dsAnlzClient: TDataSource;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    spitPrint: TSpeedItem;
    siExit: TSpeedItem;
    siHelp: TSpeedItem;
    cxGridDBTableView1Column4: TcxGridDBColumn;
    quAnlzClientCLIENTID: TFIBIntegerField;
    quAnlzClientCARD: TFIBStringField;
    quAnlzClientDEPID: TFIBIntegerField;
    quAnlzClientDEP: TFIBStringField;
    quAnlzClientMAX_BALANCE: TFIBFloatField;
    quAnlzClientSUM_CLIENT: TFIBFloatField;
    quAnlzClientDISCOUNT: TFIBIntegerField;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle2: TcxStyle;
    quAnlzClientCLIENT_NAME: TFIBStringField;
    quAnlzClientADDRESS: TFIBStringField;
    quAnlzClientNO_HOME: TFIBStringField;
    quAnlzClientCITY: TFIBStringField;
    cxGridDBTableView1Column5: TcxGridDBColumn;
    cxGridDBTableView1Column6: TcxGridDBColumn;
    cxGridDBTableView1Column7: TcxGridDBColumn;
    cxGridDBTableView1Column8: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitPrintClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAnlzClient_SellDep: TfmAnlzClient_SellDep;

implementation

uses comdata;

{$R *.dfm}

procedure TfmAnlzClient_SellDep.FormCreate(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  OpenDataSet(quAnlzClient);
  Screen.Cursor:=crDefault;
  tb1.Wallpaper:=wp;
end;

procedure TfmAnlzClient_SellDep.siExitClick(Sender: TObject);
begin
   close;
end;

procedure TfmAnlzClient_SellDep.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    with dmCom do
    begin
      CloseDataSet(quAnlzClient);
      if not tr.Active then tr.StartTransaction;
      tr.CommitRetaining;
      if not tr1.Active then tr1.StartTransaction;
      tr1.CommitRetaining;
    end;
    Deactivate;
end;

procedure TfmAnlzClient_SellDep.spitPrintClick(Sender: TObject);
begin
   dxComponentPrinter1.Preview;
end;

end.
