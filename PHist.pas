unit PHist;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmPHist = class(TForm)
    StatusBar1: TStatusBar;
    Panel2: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter1: TSplitter;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    laDep: TLabel;
    SpeedItem2: TSpeedItem;
    Label13: TLabel;
    edArt: TEdit;
    siExit: TSpeedItem;
    dg1: TM207IBGrid;
    Splitter2: TSplitter;
    M207IBGrid2: TM207IBGrid;
    FormStorage1: TFormStorage;
    lbCountry: TListBox;
    Splitter6: TSplitter;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure dg1DblClick(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtChange(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmPHist: TfmPHist;

implementation

uses comdata, Data, Data2, DBTree, M207Proc;

{$R *.DFM}

procedure TfmPHist.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmPHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dmCom, dm ,dm2 do
    begin
      WorkMode:='PHIST';
      if not tr.Active then tr.StartTransaction;
      dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry,nil,nil);
      dmCom.D_Att1Id := ATT1_DICT_ROOT;
      dmCom.D_Att2Id := ATT2_DICT_ROOT;
      lbComp.ItemIndex:=0;
      lbMat.ItemIndex:=0;
      lbGood.ItemIndex:=0;
      lbIns.ItemIndex:=0;
      lbCountry.ItemIndex:=0;      
      lbCompClick(NIL);
  //--------- ��������� ����� ��� ��������� �� �������� ----------------//
  dg1.ColumnByName['SPRICE'].Visible := CenterDep;
  dg1.ColumnByName['OPTPRICE'].Visible := CenterDep;
  M207IBGrid2.ColumnByName['PRICE'].Visible := CenterDep;
  M207IBGrid2.ColumnByName['OPTPRICE'].Visible := CenterDep;
  //---------------------------------------------------------------------//
    end;
end;

procedure TfmPHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm ,dm2 do
    begin
      CloseDataSets([quDPrice, quPHist]);
      tr.CommitRetaining;
    end;
end;

procedure TfmPHist.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPHist.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmPHist.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;  
end;

procedure TfmPHist.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmPHist.FormActivate(Sender: TObject);
begin
  dm.pmPHist.Items[0].Click;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmPHist.SpeedItem2Click(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId)or
       (Old_D_CountryID<>D_CountryID) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryID:=D_CountryID;
         Screen.Cursor:=crSQLWait;
         quDPrice.Active:=False;
         quDPrice.Open;
         Screen.Cursor:=crDefault;
       end;

end;

procedure TfmPHist.dg1DblClick(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dm, dm2.quPHist do
    begin
      Active:=False;
      PrHistArt2Id:=quDPriceArt2Id.AsInteger;
      PrHistDepId:=SDepId;
      Open;
    end;
  Screen.Cursor:=crDefault;
end;

procedure TfmPHist.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then dg1DblClick(NIL);
end;

procedure TfmPHist.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
     begin
       with dm.quDPrice do
         begin
           try
             DisableControls;
             Next;
             if NOT LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
           finally
             EnableControls;
           end
        end;
     end;
end;

procedure TfmPHist.edArtChange(Sender: TObject);
begin
  dm.quDPrice.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmPHist.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100520)
end;

end.
