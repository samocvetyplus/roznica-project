unit Comp2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  StdCtrls, Menus, RxMenus, SpeedBar, DBCtrls, ComCtrls, db, Mask;

type
  TfmComp2 = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ppPrint: TRxPopupMenu;
    Dct1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    tb2: TSpeedBar;
    laCat: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem4: TSpeedItem;
    dg1: TM207IBGrid;
    Panel1: TPanel;
    Splitter1: TSplitter;
    ppCat: TRxPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    lcNDS: TDBLookupComboBox;
    lcPayType: TDBLookupComboBox;
    cbS: TDBCheckBox;
    edPostIndex: TDBEdit;
    Label4: TLabel;
    Label7: TLabel;
    edPhone: TDBEdit;
    Label6: TLabel;
    edAdress: TDBEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    Label9: TLabel;
    Label10: TLabel;
    PageControl1: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label15: TLabel;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    Label19: TLabel;
    DBEdit14: TDBEdit;
    Label20: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label21: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Label22: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MenuItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure Dct1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }

  end;

var
  fmComp2: TfmComp2;

implementation

uses comdata, Data, Data2, InsRepl, ReportData, Sort, M207Proc, DBTree;

{$R *.dfm}
procedure TfmComp2.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmComp2.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taComp.Append;
end;

procedure TfmComp2.MenuItem1Click(Sender: TObject);
begin
with dmCom,taComp do
  begin
    Active:=false;
    CompCat:=TComponent(Sender).Tag;
    case compCat of
      1: SelectSQL [SelectSQL.Count-2]:='WHERE  SELLER=1';
      2: SelectSQL [SelectSQL.Count-2]:='WHERE  BUYER=1';
      3: SelectSQL [SelectSQL.Count-2]:='WHERE  PRODUCER=1';
      4: SelectSQL [SelectSQL.Count-2]:='WHERE  SBUYER=1';
      else SelectSQL [SelectSQL.Count-2]:=' ';
    end;
    Open;
    case CompCat of
      1: laCat.Caption:='����������';
      2: laCat.Caption:='������� ����������';
      3: laCat.Caption:='������������';
      4: laCat.Caption:='��������� ����������';
      else laCat.Caption:=' ��� ���������';
    end;
  end;
end;


procedure TfmComp2.FormCreate(Sender: TObject);
begin
  Width:=Screen.Width;
  Height:=screen.Height;
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dmCom,dm2 do
    begin
       CompCat:=100;
       OpenDataSets([quProd,taPayType,taNDS]);
    end;
  SetCBDropDown(lcPayType);
  SetCBDropDown(lcNDS);
  dm.R_Item:='D_COMP';  
end;

procedure TfmComp2.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmComp2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 with dmCom, dm2 do
   begin
     PostDataSets([taComp,taMOL]);
     CloseDataSets([taMol,taComp,quProd,taPayType, taNDS]);
     dm.LoadArtSL(COMP_DICT);
     tr.CommitRetaining;
   end;
 dm.R_Item:='';  
end;

procedure TfmComp2.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var wc:TWinControl;
begin
  if Key=VK_Return then
   begin
     Key:=VK_TAB;
     wc:=FindNextControl(ActiveControl,True,True,False);
     if wc<>Nil then ActiveControl:=wc;
    end; 
end;

procedure TfmComp2.SpeedItem1Click(Sender: TObject);
begin
 dmCom.taComp.Delete;
end;

procedure TfmComp2.siExitClick(Sender: TObject);
begin
  close;
end;

procedure TfmComp2.FormActivate(Sender: TObject);
begin
 ppCat.Items[4].Click;
 dmCom.taMol.Active:=True;
end;

procedure TfmComp2.Dct1Click(Sender: TObject);
begin
 PrintDict(dmCom.dsComp,35);
end;

procedure TfmComp2.N1Click(Sender: TObject);
begin
 PrintDict(dmCom.dsComp,36);
end;

procedure TfmComp2.N2Click(Sender: TObject);
var arr:TArrDoc;
begin
  gen_arr(arr,dg1,dmCom.taCompD_COMPID);
  try
    PrintDocument(arr,cpa);
  finally
   Finalize(arr);
  end;   
end;

procedure TfmComp2.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i,j:integer;
begin
  with dmCom,taComp do
   if (Key in [9,VK_RETURN]) and (State=dsInsert) then
     begin
       j:=dg1.SelectedIndex;
       i:=taCompD_COMPID.AsInteger;
       Post;
       Active:=False;
       open;
       Locate('D_COMPID',i,[]);
       ActiveControl:=dg1;
       dg1.SelectedIndex:=j;
     end;
end;

end.
