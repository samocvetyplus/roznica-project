unit Ins;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Menus, db, rxPlacemnt, rxSpeedbar;

type
  TfmIns = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    dg1: TM207IBGrid;
    fs1: TFormStorage;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dg1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmIns: TfmIns;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmIns.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmIns.FormCreate(Sender: TObject);
var c: TColumn;
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      Caption:=FullArt+'    '+Art2+' - �������';
      OpenDataSets([taIns, taA2Ins, quEdgT, quEdgS]);
      c:=dg1['EDGTYPEID'];
      with quEdgT do
        begin
          First;
          while NOT EOF do
            begin
              c.PickList.Add(quEdgTEdgetionID.AsString);
              Next;
            end;
        end;
    end;

  if (not CenterDep) and (dmCom.UserId <> 1) then
  begin
     sidel.Enabled:=False;
     siAdd.Enabled:=False;
     dg1.ReadOnly := true;
     N1.Enabled:=False;
     N2.Enabled:=False;
  end

end;

procedure TfmIns.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm, dm2 do
    begin
      PostDataSets([taA2Ins]);
      CloseDataSets([taA2Ins, taIns, quEdgT, quEdgS]);
      dm.LoadArtSL(INS_DICT);
    end;
end;

procedure TfmIns.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmIns.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmIns.siAddClick(Sender: TObject);
var i: integer;
begin
  dm.taA2Ins.Append;
  with dg1 do
    begin
      i:=0;
      while Columns[i].Field.FieldName<>'Ins' do Inc(i);
      SelectedIndex:=i;
    end

end;

procedure TfmIns.siDelClick(Sender: TObject);
begin
  dm.taA2Ins.Delete;
end;

procedure TfmIns.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then Close;
end;

procedure TfmIns.dg1KeyPress(Sender: TObject; var Key: Char);
var f: TField;
begin
  if Key=#8 then
    begin
      f:=NIL;
      if dg1.SelectedField.Fieldname='EDGTYPE' then f:=dm.taA2InsEdgTypeId;
      if dg1.SelectedField.Fieldname='EDGSHAPE' then f:=dm.taA2InsEdgShapeId;
      if f<>NIL then
        begin
          with dm.taA2Ins do
            if NOT (State in [dsInsert, dsEdit]) then Edit;
          f.Clear;
        end;
    end
end;

end.
