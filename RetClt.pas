unit RetClt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid;

type
  TfmRetClt = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    grRetClt: TM207IBGrid;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRetClt: TfmRetClt;

implementation

uses ServData, comdata;

{$R *.DFM}

procedure TfmRetClt.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRetClt.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRetClt.FormCreate(Sender: TObject);
begin
  if not dmCom.tr.Active then dmCom.tr.StartTransaction
  else dmCom.tr.CommitRetaining;
  OpenDataSets([dmServ.taRetClt]);
end;

procedure TfmRetClt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  CloseDataSets([dmServ.taRetClt]);
  Action := caFree;
end;

end.
