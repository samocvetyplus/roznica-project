object fmInventory: TfmInventory
  Left = 154
  Top = 139
  HelpContext = 100410
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
  ClientHeight = 505
  ClientWidth = 887
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 887
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 60
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 783
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object sidescription: TSpeedItem
      BtnCaption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103
      Caption = #1042#1099#1073#1086#1088
      Hint = #1042#1099#1073#1086#1088'|'
      ImageIndex = 79
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = sidescriptionClick
      SectionName = 'Untitled (0)'
    end
    object siSetting: TSpeedItem
      Action = acSetting
      BtnCaption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CD108CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08
        CF08CF08D108AD08DF7BDF77DF73BF6FBF6B9F679F639F5F7F5F7F5B5F575F53
        5F533F4FCF08AC08FF7BDF779F675E535E53FF7FFF7F5F579F5F7F5B7F575F57
        5F533F4FAE08AC08FF7B1A5F0C0D8C318C31524AFF7FFF7F9F637F5F7F5B5F57
        5F535F53AE08AC08FF7F2D190B3EE07EE07EB27F524AFF7F9F637F5F7F5B7F5B
        5F575F53AE08AC04FF7FCB08E07E20012001E07E8C315E539F671A121A121A12
        1A125F57AE08AC04FF7FCB08F07F20012001E07E8C315E539F671A121A121A12
        1A125F57AE08AC08FF7F4D19734EFF7FE07E333E0C0D7F5FBF6B9F679F637F5F
        7F5B7F5BAE08AC04FF7F3A634D19CB08CB082D19195BBF6FBF6FBF6B9F679F63
        7F5F7F5BAE08AD08FF7FFF7FFF7FFF7FFF7BDF77DF77DF73BF6FBF6B9F679F63
        9F5F7F5FCF085311592238223822382238223822382238223822792A5922792A
        5826572E5411130999099A09990999099909990999099909BA0DBE32DC09BE32
        931D466114051F7C120D120D120D120D120D120D120D120D120D130DF208130D
        F10CF0101F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Spacing = 1
      Left = 63
      Top = 3
      Visible = True
      OnClick = acSettingExecute
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 183
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siClearInventory: TSpeedItem
      Action = acClear
      BtnCaption = #1054#1095#1080#1089#1090#1080#1090#1100
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1102
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FF2B90EF278DE7FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF2A8FEC27
        8CED2489E42388DD1E84D5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF298FD63DA2EB3EA3F0379CEA2186DA1A81D1187ECA157C
        C41177BBFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF3CA2E158BDF260
        C4F93D9EE557BCF73398DF1E83CD1076BC0B73B40B72AF086FAAFF00FFFF00FF
        FF00FFFF00FF046B164AB0F953B7F72F87D163C7FB3D9EE55BBFFB55BAFA3499
        DE2D93D80F76B3066DA7FF00FFFF00FFFF00FF187D5F2B7A83046B162884DE3C
        99D9227BCE40A0EA5ABEFE4FB2F556B9FF56B9FF46AAF3FF00FFFF00FFFF00FF
        0F7D153CBE6131C64831C64831C648046B16046B162D87B63998E844A5F052B6
        FF52B5FFFF00FFFF00FFFF00FF15872231AF4A62F99250EB6F31C6481DA74623
        987631C64831C648046B16046B16FF00FFFF00FFFF00FFFF00FFFF00FF30AD48
        2BA6414FE77837D0531AB427029D01009B0010A41E32B97246A7ACFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FF0A75102AAE3F22BC32049A060094000C
        A118027804027804FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        259E3942DC640B9F110077000278040B8717FF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF1D912C44DE680FA315006F00FF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF13831D43D964
        12AB1C007300FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF1C9A2A1AB127007900FF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF18A024027F04
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1102
      ImageIndex = 80
      Spacing = 1
      Left = 243
      Top = 3
      Visible = True
      OnClick = acClearExecute
      SectionName = 'Untitled (0)'
    end
    object siMol: TSpeedItem
      Action = acMOL
      BtnCaption = #1052#1054#1051
      Caption = #1052#1054#1051
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C524AAD35EF3DEF3D735EE72CEF3DEF3DAD35524A1F7C
        1F7C1F7C1F7C94521F7C9452EF3DEF3D8410AD35E72CEF3DEF3DEF3D94521F7C
        1F7C1F7C1F7CE71C8C31D65AAD35EF3DBD77BD77BD77F75E0000EF3DCE399452
        1F7C8C31EF3DFF7FF75EEF3D8410F75EB566AD55EF5DBD77F75EE71C8C31E71C
        F75EFF7FFF7FFF7FFF7FFF7FEF3DFF7FBD7718637B6F7B6FFF7F735EE71CFF7F
        FF7FFF7FFF7FFF7FFF7FFF7F3156BD77525A6B2D7B6FFF7FFF7FF76EEF3DFF7F
        FF7FFF7FFF7FFF7FFF7F3967F75EFF7F1052BD77FF7FFF7FBD77FF7FE71CFF7F
        FF7FFF7FFF7FFF7FFF7FEF3DFF7F39674A39FF7FBD77BD77BD77B566734EEF3D
        FF7FFF7FFF7FFF7FFF7FEF3DF75E6B3D6B2DFF7F734EE72CBD77E71C7B6F6B2D
        FF7FFF7FFF7FFF7FF75EEF3D734E734EF75EF75E6B2DEF3DF75E0000734E6B2D
        FF7FFF7FFF7FFF7FEF3DE71CFF7FFF7FFF7F7B6FFF7FFF7FFF7F00000000EF3D
        FF7FFF7FFF7FFF7FEF3D0000F75EFF7FFF7FFF7FFF7FFF7FFF7F00000000EF3D
        FF7FFF7FFF7FFF7FEF3D0000000000000000EF3DEF3DEF3DE71C00000000EF3D
        FF7FFF7FFF7FFF7F7B6F0000000000000000000000000000000000000000F75E
        FF7FFF7FFF7FFF7FFF7FF75EE7000000000000000000000000000000F75EFF7F
        FF7FFF7FFF7FFF7FFF7FFF7FFF7FBD57F73EEF3DEF3DEF3DF73EFF5FFF7FFF7F
        FF7FFF7FFF7F}
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072'|'
      ImageIndex = 57
      Spacing = 1
      Left = 123
      Top = 3
      Visible = True
      OnClick = acMOLExecute
      SectionName = 'Untitled (0)'
    end
    object siClosed: TSpeedItem
      Action = acClosed
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = 'siClosed'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF0000007B7B7B7B7B7BBDBDBD7B7B7B0000007B7B7BBDBDBD7B7B7B7B7B
        7B000000FF00FFFF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD7B
        7B7B0000007B7B7BBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7BBDBDBDBDBDBD000000BDBDBDBDBDBD7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBD00
        0000000000000000BDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B0000000000000000007B7B7B7B7B7B7B7B
        7B7B7B7B000000FF00FFFF00FFFF00FF000000BDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FF
        FF00FF0000000000000000000000000000000000000000000000000000000000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FF000000BDBDBD0000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBD000000FF
        00FFFF00FFFF00FF000000BDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FF7B7B7B7B7B7BBDBDBD000000000000000000BDBDBD7B7B7B7B7B
        7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBD000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF000000000000000000000000000000FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1102
      Spacing = 1
      Left = 303
      Top = 3
      Visible = True
      OnClick = acClosedExecute
      SectionName = 'Untitled (0)'
    end
    object siText: TSpeedItem
      BtnCaption = #1058#1077#1082#1089#1090
      Caption = 'siText'
      DropDownMenu = pmtext
      Hint = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1090#1077#1082#1089#1090#1086#1074#1099#1081' '#1092#1072#1081#1083
      Spacing = 1
      Left = 363
      Top = 3
      Visible = True
      OnClick = acTextExecute
      SectionName = 'Untitled (0)'
    end
    object siActLack: TSpeedItem
      Action = acActLack
      BtnCaption = #1057#1087#1080#1089#1072#1085#1080#1077#13#10#1085#1077#1076#1086#1089#1090#1072#1095#1080
      Caption = 'siActLack'
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1072#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103', '#1085#1077#1076#1086#1089#1090#1072#1095#1080
      Spacing = 1
      Left = 423
      Top = 3
      Visible = True
      OnClick = acActLackExecute
      SectionName = 'Untitled (0)'
    end
    object siSurplus: TSpeedItem
      Action = acSurPlus
      BtnCaption = #1048#1079#1083#1080#1096#1082#1080
      Caption = 'siSurplus'
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1072#1082#1090' '#1087#1086' '#1080#1079#1083#1080#1096#1082#1072#1084
      Spacing = 1
      Left = 483
      Top = 3
      Visible = True
      OnClick = acSurPlusExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 653
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object siFindAll: TSpeedItem
      Action = acFindAll
      BtnCaption = #1054#1090#1084'. '#1082#1072#1082#13#10#1085#1072#1081#1076#1077#1085'.'
      Caption = 'siFindAll'
      Spacing = 1
      Left = 543
      Top = 3
      Visible = True
      OnClick = acFindAllExecute
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 131
    Width = 887
    Height = 353
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsInventory
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clBtnFace
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pmChange
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    OnMouseDown = dg1MouseDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1042#1077#1089
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'PRODCODE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1048#1079#1075'.'
        Title.EndEllipsis = True
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1052#1072#1090'.'
        Title.EndEllipsis = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1053#1072#1080#1084'. '#1080#1079#1076'.'
        Title.EndEllipsis = True
        Width = 91
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1054#1042
        Title.EndEllipsis = True
        Width = 28
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1040#1088#1090#1080#1082#1091#1083
        Title.EndEllipsis = True
        Width = 115
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.EndEllipsis = True
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1056#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'COSTP'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1055#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'.||'#1056#1072#1089#1093'.'
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'PRICE0'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'.||'#1055#1088#1080#1093'.'
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1057#1082#1083#1072#1076
        Title.EndEllipsis = True
        Width = 115
      end
      item
        EditButtons = <>
        FieldName = 'Description'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1054#1087#1080#1089#1072#1085#1080#1077
        Title.EndEllipsis = True
        Width = 138
      end
      item
        EditButtons = <>
        FieldName = 'ORDER_'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1086#1088#1103#1076#1086#1082' '#1089#1082#1072#1085#1080#1088#1086#1074#1072#1085#1080#1103
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'NAMESUP'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Width = 232
      end
      item
        EditButtons = <>
        FieldName = 'NAMEATT1'
        Footers = <>
        Title.Caption = #1040#1090#1088#1080#1073#1091#1090'1'
        Width = 434
      end
      item
        EditButtons = <>
        FieldName = 'NAMEATT2'
        Footers = <>
        Title.Caption = #1040#1090#1088#1080#1073#1091#1090'2'
      end
      item
        EditButtons = <>
        FieldName = 'GOODS1'
        Footers = <>
        Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'1'
      end
      item
        EditButtons = <>
        FieldName = 'GOODS2'
        Footers = <>
        Title.Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'2'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object SpeedBar: TSpeedBar
    Left = 0
    Top = 42
    Width = 887
    Height = 31
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 100
    BtnHeight = 25
    Images = dmCom.ilButtons
    TabOrder = 3
    InternalVer = 1
    object Label1: TLabel
      Left = 48
      Top = 8
      Width = 52
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbFind: TLabel
      Left = 240
      Top = 8
      Width = 56
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1080#1079#1076'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 428
      Top = 9
      Width = 129
      Height = 13
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1082#1072#1082' '#1074#1099#1073#1088#1072#1085#1085#1086#1077
      Transparent = True
    end
    object edUID: TEdit
      Left = 109
      Top = 5
      Width = 92
      Height = 21
      TabOrder = 0
      OnKeyDown = edUIDKeyDown
      OnKeyPress = edUIDKeyPress
    end
    object edFindUid: TEdit
      Left = 298
      Top = 5
      Width = 95
      Height = 21
      TabOrder = 1
      OnKeyDown = edFindUidKeyDown
      OnKeyPress = edFindUidKeyPress
    end
    object chFindSelect: TCheckBox
      Left = 413
      Top = 10
      Width = 12
      Height = 11
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Caption = 'siDep'
      DropDownMenu = pmdep
      Hint = 'siDep|'
      Spacing = 1
      Left = 653
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object spbr4: TSpeedBar
    Left = 0
    Top = 73
    Width = 887
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 4
    InternalVer = 1
    object lbMat: TLabel
      Left = 16
      Top = 8
      Width = 23
      Height = 13
      Caption = #1052#1072#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbGood: TLabel
      Left = 172
      Top = 8
      Width = 31
      Height = 13
      Caption = #1058#1086#1074#1072#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbComp: TLabel
      Left = 336
      Top = 8
      Width = 22
      Height = 13
      Caption = #1048#1079#1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbSup: TLabel
      Left = 494
      Top = 8
      Width = 28
      Height = 13
      Caption = #1055#1086#1089#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object ScrollBar1: TScrollBar
      Left = 488
      Top = 32
      Width = 121
      Height = 13
      PageSize = 0
      TabOrder = 0
    end
    object SpeedbarSection3: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object sdiMat: TSpeedItem
      BtnCaption = #1052#1072#1090#1077#1088#1080#1072#1083
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 43
      Top = 3
      Visible = True
      OnClick = sdiMatClick
      SectionName = 'Untitled (0)'
    end
    object sdiGood: TSpeedItem
      BtnCaption = #1058#1086#1074#1072#1088
      Caption = #1058#1086#1074#1072#1088
      Hint = #1058#1086#1074#1072#1088'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 203
      Top = 3
      Visible = True
      OnClick = sdiGoodClick
      SectionName = 'Untitled (0)'
    end
    object sdicomp: TSpeedItem
      BtnCaption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
      Hint = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 363
      Top = 3
      Visible = True
      OnClick = sdicompClick
      SectionName = 'Untitled (0)'
    end
    object sdisup: TSpeedItem
      BtnCaption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080
      Hint = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080'|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 531
      Top = 3
      Visible = True
      OnClick = sdisupClick
      SectionName = 'Untitled (0)'
    end
  end
  object spbr5: TSpeedBar
    Left = 0
    Top = 102
    Width = 887
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 5
    InternalVer = 1
    object lbnote1: TLabel
      Left = 8
      Top = 8
      Width = 37
      Height = 13
      Caption = #1055#1088#1080#1084'.1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbnote2: TLabel
      Left = 166
      Top = 8
      Width = 37
      Height = 13
      Caption = #1055#1088#1080#1084'.2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 328
      Top = 8
      Width = 27
      Height = 13
      Caption = #1040#1090#1088'.1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 496
      Top = 8
      Width = 27
      Height = 13
      Caption = #1040#1090#1088'.2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object btnDo: TSpeedButton
      Left = 660
      Top = 3
      Width = 25
      Height = 22
      Hint = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888478888888888888748888844444888848888888444488884888888884
        4488884888888848448888748888448884888887444488888888888888888888
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = btnDoClick
    end
    object ScrollBar2: TScrollBar
      Left = 488
      Top = 32
      Width = 121
      Height = 13
      PageSize = 0
      TabOrder = 0
    end
    object SpeedbarSection5: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object sdinote1: TSpeedItem
      BtnCaption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'1'
      Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'1'
      Hint = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'1|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 43
      Top = 3
      Visible = True
      OnClick = sdinote1Click
      SectionName = 'Untitled (0)'
    end
    object sdinote2: TSpeedItem
      BtnCaption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'2'
      Caption = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'2'
      Hint = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077'2|'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 203
      Top = 3
      Visible = True
      OnClick = sdinote2Click
      SectionName = 'Untitled (0)'
    end
    object sdiAtr1: TSpeedItem
      BtnCaption = #1040#1090#1088#1080#1073#1091#1090'1'
      Caption = #1040#1090#1088#1080#1073#1091#1090'1'
      Hint = #1042#1099#1073#1086#1088' '#1072#1090#1088#1080#1073#1091#1090#1072' 1'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 363
      Top = 3
      Visible = True
      OnClick = sdiAtr1Click
      SectionName = 'Untitled (0)'
    end
    object sdiAtr2: TSpeedItem
      BtnCaption = #1040#1090#1088#1080#1073#1091#1090'2'
      Caption = #1040#1090#1088#1080#1073#1091#1090'2'
      Hint = #1042#1099#1073#1086#1088' '#1072#1090#1088#1080#1073#1091#1090#1072' 2'
      ImageIndex = 78
      Layout = blGlyphRight
      Spacing = 1
      Left = 531
      Top = 3
      Visible = True
      OnClick = sdiAtr2Click
      SectionName = 'Untitled (0)'
    end
  end
  object chlbGood: TCheckListBox
    Left = 173
    Top = 97
    Width = 153
    Height = 113
    OnClickCheck = chlbGoodClickCheck
    ItemHeight = 13
    TabOrder = 7
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbComp: TCheckListBox
    Left = 333
    Top = 97
    Width = 153
    Height = 113
    OnClickCheck = chlbCompClickCheck
    ItemHeight = 13
    TabOrder = 8
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbSup: TCheckListBox
    Left = 500
    Top = 97
    Width = 153
    Height = 113
    OnClickCheck = chlbSupClickCheck
    ItemHeight = 13
    TabOrder = 9
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbMat: TCheckListBox
    Left = 13
    Top = 97
    Width = 153
    Height = 113
    OnClickCheck = chlbMatClickCheck
    ItemHeight = 13
    TabOrder = 6
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbNote1: TCheckListBox
    Left = 13
    Top = 128
    Width = 153
    Height = 113
    OnClickCheck = chlbNote1ClickCheck
    ItemHeight = 13
    TabOrder = 10
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbNote2: TCheckListBox
    Left = 173
    Top = 128
    Width = 153
    Height = 113
    OnClickCheck = chlbNote2ClickCheck
    ItemHeight = 13
    TabOrder = 11
    Visible = False
    OnExit = chlbMatExit
  end
  object chlbAtr1: TCheckListBox
    Left = 333
    Top = 128
    Width = 153
    Height = 113
    OnClickCheck = chlbAtr1ClickCheck
    ItemHeight = 13
    TabOrder = 12
    Visible = False
  end
  object chlbAtr2: TCheckListBox
    Left = 500
    Top = 128
    Width = 153
    Height = 113
    OnClickCheck = chlbAtr2ClickCheck
    ItemHeight = 13
    TabOrder = 13
    Visible = False
    OnExit = chlbMatExit
  end
  object rlbfilter: TRxCheckListBox
    Left = 9
    Top = 70
    Width = 203
    Height = 58
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 2
    Visible = False
    OnClickCheck = rlbfilterClickCheck
    InternalVersion = 202
    Strings = (
      #1042#1089#1077' '#1080#1079#1076#1077#1083#1080#1103
      1
      True
      #1048#1079#1076#1077#1083#1080#1103' '#1085#1072#1081#1076#1077#1085#1085#1099#1077' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      0
      True
      #1048#1079#1076#1077#1083#1080#1103' '#1085#1077' '#1085#1072#1081#1076#1077#1085#1085#1099#1077' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      0
      True
      #1048#1079#1076#1077#1083#1080#1103' '#1085#1077' '#1095#1080#1089#1083#1103#1090#1089#1103' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      0
      True)
  end
  object prbar1: TcxProgressBar
    Left = 0
    Top = 484
    Align = alBottom
    TabOrder = 14
    ExplicitLeft = -8
    ExplicitTop = 452
    Width = 887
  end
  object fr: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 400
    Top = 304
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 480
    Top = 120
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 632
    Top = 316
    object acPrintGrid: TAction
      Category = #1055#1077#1095#1072#1090#1100
      ShortCut = 16464
      OnExecute = acPrintGridExecute
    end
    object acSetting: TAction
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      ImageIndex = 77
      OnExecute = acSettingExecute
    end
    object acAddUID: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      OnUpdate = acAddUIDUpdate
    end
    object acDelExcessUID: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1083#1080#1096#1085#1077#1077' '#1080#1079#1076#1077#1083#1080#1077
      OnExecute = acDelExcessUIDExecute
      OnUpdate = acDelExcessUIDUpdate
    end
    object acMOL: TAction
      Caption = #1052#1054#1051
      Hint = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072'|'
      ImageIndex = 57
      OnExecute = acMOLExecute
      OnUpdate = acMOLUpdate
    end
    object acPrintInventoryB: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1086#1085#1085#1072#1103' '#1086#1087#1080#1089#1100'-'#1072#1082#1090' '#1090#1086#1074#1072#1088#1086#1074' '#1080' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnExecute = acPrintInventoryBExecute
    end
    object acPrintInventoryE: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1077' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1088#1086#1074#1077#1088#1082#1080' '#1094#1077#1085#1085#1086#1089#1090#1077#1081
    end
    object acPrintSubscriptionB: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1086#1076#1087#1080#1089#1082#1072' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
      OnExecute = acPrintSubscriptionBExecute
    end
    object acPrintSubscriptionE: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1086#1076#1087#1080#1089#1082#1072' '#1085#1072' '#1082#1086#1085#1077#1094' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
      OnExecute = acPrintSubscriptionEExecute
    end
    object acClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      Hint = #1054#1095#1080#1089#1090#1080#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1102
      ImageIndex = 80
      OnExecute = acClearExecute
      OnUpdate = acClearUpdate
    end
    object acClosed: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1102
      ImageIndex = 5
      OnExecute = acClosedExecute
      OnUpdate = acClosedUpdate
    end
    object acText: TAction
      Caption = #1088#1072#1089#1093#1086#1076#1085#1099#1077
      OnExecute = acTextExecute
    end
    object acTextpr: TAction
      Caption = #1087#1088#1080#1093#1086#1076#1085#1099#1077
      OnExecute = acTextprExecute
    end
    object acActLack: TAction
      Caption = #1057#1087#1080#1089#1072#1090#1100' '#1085#1077#1076#1086#1089#1090#1072#1095#1091
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1072#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103', '#1085#1077#1076#1086#1089#1090#1072#1095#1080
      OnExecute = acActLackExecute
      OnUpdate = acActLackUpdate
    end
    object acSurPlus: TAction
      Caption = #1048#1079#1083#1080#1096#1082#1080
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1072#1082#1090' '#1087#1086' '#1080#1079#1083#1080#1096#1082#1072#1084
      OnExecute = acSurPlusExecute
      OnUpdate = acSurPlusUpdate
    end
    object acFindAll: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1082#1072#1082' '#1085#1072#1081#1076#1077#1085#1085#1099#1077
      OnExecute = acFindAllExecute
      OnUpdate = acFindAllUpdate
    end
    object acChangeStateTo0: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1082#1072#1082' '#1085#1077' '#1085#1072#1081#1076#1077#1085#1085#1086#1077
      OnExecute = acChangeStateTo0Execute
      OnUpdate = acChangeStateTo0Update
    end
    object acChangeStateTo1: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1082#1072#1082' '#1085#1072#1081#1076#1077#1085#1085#1086#1077
      OnExecute = acChangeStateTo1Execute
      OnUpdate = acChangeStateTo1Update
    end
    object acChangeStateTo2: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077' '#1082#1072#1082' '#1083#1080#1096#1085#1077#1077
      OnExecute = acChangeStateTo2Execute
      OnUpdate = acChangeStateTo2Update
    end
  end
  object pmChange: TTBPopupMenu
    Left = 200
    Top = 188
    object TBItem1: TTBItem
      Action = acChangeStateTo0
    end
    object biisnotin: TTBItem
      Action = acChangeStateTo1
    end
    object biisin: TTBItem
      Tag = 1
      Action = acChangeStateTo2
    end
    object bidel: TTBItem
      Tag = 2
      Action = acDelExcessUID
    end
  end
  object pmPrint: TTBPopupMenu
    Left = 200
    Top = 132
    object ibInventoryB: TTBItem
      Action = acPrintInventoryB
    end
    object siFactInv: TTBSubmenuItem
      Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1077' '#1088#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1088#1086#1074#1077#1088#1082#1080' '#1094#1077#1085#1085#1086#1089#1090#1077#1081
      object ibFactorganisation: TTBItem
      end
    end
    object ibsubscriptionB: TTBItem
      Action = acPrintSubscriptionB
    end
    object ibsubscriptionE: TTBItem
      Action = acPrintSubscriptionE
    end
    object ibprord: TTBSubmenuItem
      Caption = #1055#1088#1080#1082#1072#1079#1099
      object iborganisation: TTBItem
      end
    end
    object sitAct: TTBSubmenuItem
      Caption = #1040#1082#1090#1099
      object itActOrganization: TTBSubmenuItem
      end
    end
  end
  object svdFile: TSaveDialog
    Left = 240
    Top = 328
  end
  object pmdep: TTBPopupMenu
    Left = 304
    Top = 272
  end
  object pmtext: TTBPopupMenu
    Left = 496
    Top = 304
    object biprice: TTBItem
      Action = acText
    end
    object bisprice: TTBItem
      Action = acTextpr
    end
  end
end
