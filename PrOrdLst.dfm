object fmPrOrdList: TfmPrOrdList
  Left = 91
  Top = 99
  HelpContext = 100310
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1094#1077#1085
  ClientHeight = 526
  ClientWidth = 1106
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 277
    Width = 1106
    Height = 3
    Cursor = crVSplit
    Align = alBottom
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1106
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = siAddClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      Action = acView
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FF00FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00000000000000FFFFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B
        FFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFF7B7B7B000000FF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B000000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewExecute
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 770
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siMargin: TSpeedItem
      BtnCaption = #1053#1072#1094#1077#1085#1082#1080
      Caption = #1053#1072#1094#1077#1085#1082#1080
      Hint = #1053#1072#1094#1077#1085#1082#1080'|'
      ImageIndex = 55
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = siMarginClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1074#1089#1077' '#1087#1088#1080#1082#1072#1079#1099
      ImageIndex = 5
      Spacing = 1
      Left = 322
      Top = 2
      Visible = True
      OnClick = CheckCloseExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Hint = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1099
      ImageIndex = 32
      Spacing = 1
      Left = 386
      Top = 2
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 386
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siAct: TSpeedItem
      BtnCaption = #1040#1082#1090
      Caption = #1040#1082#1090
      Hint = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      ImageIndex = 11
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = siActClick
      SectionName = 'Untitled (0)'
    end
    object siFind: TSpeedItem
      BtnCaption = #1055#1086#1080#1089#1082
      Caption = #1055#1086#1080#1089#1082
      Hint = #1055#1086#1080#1089#1082' '#1087#1088#1080#1082#1072#1079#1072
      ImageIndex = 48
      Spacing = 1
      Left = 450
      Top = 2
      Visible = True
      OnClick = siFindClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 706
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 1106
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDep: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 388
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 587
      Top = 6
      Width = 81
      Height = 13
      Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      Transparent = True
    end
    object Label3: TLabel
      Left = 707
      Top = 6
      Width = 89
      Height = 13
      Caption = #1054#1096#1080#1073#1086#1095'. '#1087#1088#1080#1082#1072#1079#1099
      Transparent = True
    end
    object Label7: TLabel
      Left = 831
      Top = 6
      Width = 167
      Height = 13
      Hint = #1054#1090#1086#1073#1088#1072#1079#1080#1090#1100' / '#1089#1082#1088#1099#1090#1100' '#1074#1089#1077' '#1087#1088#1080#1082#1072#1079#1099' '#1048#1079#1084#1077#1085#1077#1085#1080#1103' '#1087#1086#1083#1085#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072'.'
      Caption = #1055#1088#1080#1082#1072#1079#1099' '#1085#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1077' '#1087#1086#1083'. '#1072#1088#1090'.'
      Transparent = True
    end
    object cbAct: TCheckBox
      Left = 572
      Top = 7
      Width = 12
      Height = 12
      Color = clBtnFace
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = cbActClick
    end
    object cbNonReVal: TCheckBox
      Left = 692
      Top = 6
      Width = 12
      Height = 12
      TabOrder = 1
      OnClick = cbNonReValClick
    end
    object VisiblePrord_FullArt: TCheckBox
      Left = 816
      Top = 6
      Width = 12
      Height = 12
      Caption = 'VisiblePrord_FullArt'
      TabOrder = 2
      OnClick = VisiblePrord_FullArtClick
    end
    object Panel4: TPanel
      Left = 8
      Top = 32
      Width = 73
      Height = 1
      Caption = 'Panel4'
      TabOrder = 3
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmPrOrd
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 298
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 280
    Width = 1106
    Height = 246
    Align = alBottom
    TabOrder = 2
    object Splitter2: TSplitter
      Left = 313
      Top = 1
      Height = 244
      AutoSnap = False
      MinSize = 20
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 312
      Height = 244
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 242
        AutoSnap = False
        MinSize = 20
        ExplicitLeft = 44
        ExplicitTop = -7
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 242
        AutoSnap = False
        MinSize = 20
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 242
        AutoSnap = False
        MinSize = 20
      end
      object Splitter6: TSplitter
        Left = 121
        Top = 1
        Height = 242
        AutoSnap = False
        MinSize = 20
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 242
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 242
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 242
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 56
        Height = 242
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 242
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object Panel2: TPanel
      Left = 316
      Top = 1
      Width = 789
      Height = 244
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel2'
      TabOrder = 1
      object dg1: TM207IBGrid
        Left = 0
        Top = 71
        Width = 789
        Height = 173
        Align = alClient
        Color = clBtnFace
        DataSource = dm.dsPrice
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        ParentFont = False
        PopupMenu = pm1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clBlack
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleButtons = True
        OnGetCellParams = dg1GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = False
        SortOnTitleClick = True
        Columns = <
          item
            Expanded = False
            FieldName = 'APRODCODE'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_MATID'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 26
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_GOODID'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_INSID'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1054#1042
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 23
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'D_COUNTRYID'
            Title.Caption = #1057#1090#1088'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ART'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 77
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ART2'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 62
            Visible = True
          end>
      end
      object tb3: TSpeedBar
        Left = 0
        Top = 0
        Width = 789
        Height = 42
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        BoundLines = [blRight]
        Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 60
        BtnHeight = 36
        Images = dmCom.ilButtons
        TabOrder = 1
        InternalVer = 1
        object SpeedbarSection3: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object siClearAll: TSpeedItem
          BtnCaption = #1054#1090#1084#1077#1085#1080#1090#1100
          Caption = #1054#1090#1084#1077#1085#1080#1090#1100
          DropDownMenu = pmClear
          Hint = #1042#1086#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1090#1072#1088#1091#1102' '#1094#1077#1085#1091
          ImageIndex = 40
          Spacing = 1
          Left = 63
          Top = 3
          Visible = True
          OnClick = siClearAllClick
          SectionName = 'Untitled (0)'
        end
        object siCreate: TSpeedItem
          BtnCaption = #1057#1086#1079#1076#1072#1090#1100
          Caption = #1057#1086#1079#1076#1072#1090#1100
          Hint = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1080#1082#1072#1079#1099' '#1085#1072' '#1094#1077#1085#1099
          ImageIndex = 32
          Spacing = 1
          Left = 303
          Top = 3
          Visible = True
          OnClick = siCreateClick
          SectionName = 'Untitled (0)'
        end
        object siCopy: TSpeedItem
          BtnCaption = #1050#1086#1087#1080#1103
          Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
          Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1094#1077#1085#1091
          ImageIndex = 14
          Spacing = 1
          Left = 183
          Top = 3
          Visible = True
          OnClick = miCopyClick
          SectionName = 'Untitled (0)'
        end
        object siApplyMargin2Art2: TSpeedItem
          BtnCaption = #1053#1072#1094#1077#1085#1082#1080
          Caption = #1053#1072#1094#1077#1085#1082#1080
          DropDownMenu = pmMargin
          Hint = #1053#1072#1094#1077#1085#1082#1080
          ImageIndex = 55
          Spacing = 1
          Left = 123
          Top = 3
          Visible = True
          OnClick = siApplyMargin2Art2Click
          SectionName = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1062#1077#1085#1072
          Caption = #1062#1077#1085#1072
          Hint = #1062#1077#1085#1072
          ImageIndex = 35
          Margin = 0
          Spacing = 1
          Left = 243
          Top = 3
          Visible = True
          OnClick = SpeedItem1Click
          SectionName = 'Untitled (0)'
        end
        object siOpen: TSpeedItem
          BtnCaption = #1054#1090#1082#1088#1099#1090#1100
          Caption = #1054#1090#1082#1088#1099#1090#1100
          Hint = #1054#1090#1082#1088#1099#1090#1100
          ImageIndex = 68
          Spacing = 1
          Left = 3
          Top = 3
          Visible = True
          OnClick = siOpenClick
          SectionName = 'Untitled (0)'
        end
      end
      object tb4: TSpeedBar
        Left = 0
        Top = 42
        Width = 789
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        BoundLines = [blBottom, blLeft, blRight]
        Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 64
        BtnHeight = 23
        TabOrder = 2
        InternalVer = 1
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 32
          Height = 13
          Caption = #1055#1086#1080#1089#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label4: TLabel
          Left = 153
          Top = 7
          Width = 100
          Height = 13
          Caption = #1058#1086#1083#1100#1082#1086' '#1080#1079#1084#1077#1085#1077#1085#1085#1099#1077
          Transparent = True
        end
        object Label5: TLabel
          Left = 281
          Top = 7
          Width = 76
          Height = 13
          Caption = #1058#1086#1083#1100#1082#1086' '#1088#1072#1079#1085#1099#1077
          Transparent = True
        end
        object Label6: TLabel
          Left = 389
          Top = 7
          Width = 70
          Height = 13
          Caption = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Transparent = True
        end
        object edArt: TEdit
          Left = 48
          Top = 4
          Width = 81
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          OnChange = edArtChange
          OnKeyDown = edArtKeyDown
        end
        object cbCh: TCheckBox
          Left = 135
          Top = 6
          Width = 12
          Height = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = cbChClick
        end
        object cbDif: TCheckBox
          Left = 264
          Top = 8
          Width = 12
          Height = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = cbChClick
        end
        object cbAll: TCheckBox
          Left = 372
          Top = 8
          Width = 12
          Height = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          OnClick = cbAllClick
        end
        object SpeedbarSection4: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
      end
    end
  end
  object dg2: TDBGridEh
    Left = 0
    Top = 104
    Width = 1106
    Height = 173
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsPrOrd
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    PopupMenu = pm2
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = acViewExecute
    OnGetCellParams = dg2GetCellParams
    OnKeyDown = dg2KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'PRORD'
        Footers = <
          item
            FieldName = 'PRORD'
            ValueType = fvtCount
          end>
        Title.Caption = #8470' '#1087#1088#1080#1082#1072#1079#1072
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'SNstr'
        Footers = <>
        Title.Caption = #8470' '#1085#1072#1082#1083'.'
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'DEP'
        Footers = <>
        Title.Caption = #1057#1082#1083#1072#1076
        Width = 141
      end
      item
        EditButtons = <>
        FieldName = 'SETDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1080' '#1074#1088#1077#1084#1103' '#1079#1072#1082#1088#1099#1090#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'SETFIO'
        Footers = <>
        Title.Caption = #1047#1072#1082#1088#1099#1083
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'PRDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.Caption = #1050#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <
          item
            FieldName = 'COST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090'-'#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'OLDCOST'
        Footers = <
          item
            FieldName = 'OLDCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1090'. '#1089#1090'-'#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'DCOST'
        Footers = <
          item
            FieldName = 'DCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1079#1085#1080#1094#1072' '#1089#1090'-'#1090#1080
      end
      item
        EditButtons = <>
        FieldName = 'NACT'
        Footers = <>
        Title.Caption = #8470' '#1072#1082#1090#1072
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE_P'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090'. '#1087#1088#1080#1082#1072#1079#1072
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE_A'
        Footers = <>
        Title.Caption = #1057#1086#1089#1090'. '#1072#1082#1090#1072
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'ONLYACT'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.Caption = #1040#1082#1090
      end
      item
        EditButtons = <>
        FieldName = 'CLOSEFIO'
        Footers = <>
        Title.Caption = #1057#1086#1079#1076#1072#1083
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 69
    Width = 1106
    Height = 35
    Align = alTop
    TabOrder = 4
    object Panel6: TPanel
      Left = 0
      Top = -2
      Width = 202
      Height = 35
      Alignment = taRightJustify
      BevelInner = bvLowered
      Caption = '- '#1087#1088#1080#1082#1072#1079' '#1079#1072#1082#1088#1099#1090' '#1080' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085' '
      TabOrder = 0
      object Label8: TLabel
        Left = 16
        Top = 8
        Width = 20
        Height = 20
        AutoSize = False
        Color = clAqua
        ParentColor = False
        Transparent = False
        WordWrap = True
      end
    end
    object Panel7: TPanel
      Left = 202
      Top = -2
      Width = 215
      Height = 35
      Alignment = taRightJustify
      BevelInner = bvLowered
      Caption = '- '#1087#1088#1080#1082#1072#1079' '#1085#1077' '#1079#1072#1082#1088#1099#1090' '#1080' '#1085#1077' '#1091#1089#1090#1072#1085#1086#1074#1083#1077#1085' '
      TabOrder = 1
      object Label9: TLabel
        Left = 6
        Top = 8
        Width = 20
        Height = 20
        AutoSize = False
        Color = clInfoBk
        ParentColor = False
        Transparent = False
        WordWrap = True
      end
    end
    object Panel8: TPanel
      Left = 415
      Top = -2
      Width = 194
      Height = 35
      Alignment = taRightJustify
      BevelInner = bvLowered
      Caption = '- '#1086#1090#1082#1088#1099#1090#1099#1077' '#1072#1082#1090#1099' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080'  '
      TabOrder = 2
      object Label10: TLabel
        Left = 8
        Top = 8
        Width = 20
        Height = 20
        AutoSize = False
        Color = clRed
        ParentColor = False
        Transparent = False
      end
    end
    object Panel9: TPanel
      Left = 607
      Top = -2
      Width = 138
      Height = 35
      Alignment = taRightJustify
      BevelInner = bvLowered
      Caption = '- '#1086#1096#1080#1073#1086#1095#1085#1099#1081' '#1087#1088#1080#1082#1072#1079' '
      TabOrder = 3
      object Label11: TLabel
        Left = 8
        Top = 8
        Width = 20
        Height = 20
        AutoSize = False
        Caption = '-'
        Color = clBlue
        ParentColor = False
        Transparent = False
      end
    end
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'Panel2.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 124
    Top = 148
  end
  object pm2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 178
    Top = 146
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnClick = siAddClick
    end
    object N3: TMenuItem
      Action = acDel
    end
    object N2: TMenuItem
      Action = acView
    end
    object N26: TMenuItem
      Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      ShortCut = 115
      OnClick = siActClick
    end
    object N29: TMenuItem
      Caption = #1055#1086#1080#1089#1082
      ShortCut = 118
      OnClick = siFindClick
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Caption = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      ShortCut = 117
      OnClick = N5Click
    end
    object N12: TMenuItem
      Caption = #1053#1072#1094#1077#1085#1082#1080
      GroupIndex = 1
      ImageIndex = 55
      ShortCut = 120
      OnClick = siMarginClick
    end
    object NAddUidAct: TMenuItem
      Action = acAddUiAct
      GroupIndex = 1
    end
    object N32: TMenuItem
      Caption = '-'
      GroupIndex = 1
    end
    object N36: TMenuItem
      Tag = 1
      Caption = #1057#1073#1088#1086#1089' '#1089#1086#1089#1090#1086#1103#1085#1080#1103' '#1087#1088#1080#1082#1072#1079#1072
      GroupIndex = 1
      OnClick = N37Click
    end
    object N37: TMenuItem
      Tag = 2
      Caption = #1057#1073#1088#1086#1089' '#1089#1086#1089#1090#1086#1103#1085#1080#1103' '#1072#1082#1090#1072
      GroupIndex = 1
      OnClick = N37Click
    end
  end
  object pm1: TPopupMenu
    Left = 321
    Top = 358
    object N6: TMenuItem
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1099' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1075#1088#1091#1087#1087#1099
      ImageIndex = 40
      ShortCut = 113
      OnClick = siClearAllClick
    end
    object N7: TMenuItem
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1099' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 12
      ShortCut = 114
      OnClick = siClearDepClick
    end
    object N8: TMenuItem
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1099' '#1074#1099#1073#1088#1072#1085#1085#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072
      ImageIndex = 13
      ShortCut = 115
      OnClick = siClearArtClick
    end
    object N9: TMenuItem
      Caption = #1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1094#1077#1085#1091
      ImageIndex = 39
      ShortCut = 116
      OnClick = siClearClick
    end
    object N20: TMenuItem
      Caption = '-'
    end
    object N15: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
      ShortCut = 117
      OnClick = siApplyMargin2Art2Click
    end
    object N25: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1089#1082#1083#1072#1076#1072
      ShortCut = 118
      OnClick = miMargin2DepClick
    end
    object N13: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1075#1088#1091#1087#1087#1099
      ShortCut = 119
      OnClick = miMargin2GrClick
    end
    object N14: TMenuItem
      Caption = #1053#1072#1094#1077#1085#1082#1080
      ShortCut = 120
      OnClick = siMarginClick
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object miCopy: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1094#1077#1085#1091
      ImageIndex = 14
      ShortCut = 121
      OnClick = miCopyClick
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object N10: TMenuItem
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1087#1088#1080#1082#1072#1079#1099' '#1085#1072' '#1094#1077#1085#1099
      ImageIndex = 32
      ShortCut = 122
      OnClick = siCreateClick
    end
    object N27: TMenuItem
      Caption = '-'
    end
    object N28: TMenuItem
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1094#1077#1085#1099
      ShortCut = 16497
      OnClick = N28Click
    end
    object N33: TMenuItem
      Caption = '-'
    end
    object N34: TMenuItem
      Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
      OnClick = N34Click
    end
  end
  object pmClear: TPopupMenu
    Left = 463
    Top = 364
    object N16: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1072#1103' '#1075#1088#1091#1087#1087#1072
      OnClick = siClearAllClick
    end
    object N17: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1081' '#1089#1082#1083#1072#1076
      OnClick = siClearDepClick
    end
    object N18: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      OnClick = siClearArtClick
    end
    object N19: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1091#1102' '#1094#1077#1085#1091
      OnClick = siClearClick
    end
  end
  object pmMargin: TPopupMenu
    Left = 368
    Top = 360
    object miMargin2Art: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
      OnClick = siApplyMargin2Art2Click
    end
    object miMargin2Dep: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1089#1082#1083#1072#1076#1072
      OnClick = miMargin2DepClick
    end
    object miMargin2Gr: TMenuItem
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100' '#1085#1072#1094#1077#1085#1082#1091' '#1076#1083#1103' '#1075#1088#1091#1087#1087#1099
      OnClick = miMargin2GrClick
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object N24: TMenuItem
      Caption = #1053#1072#1094#1077#1085#1082#1080
      OnClick = siMarginClick
    end
  end
  object pmPrint: TRxPopupMenu
    Style = msBtnLowered
    Left = 424
    Top = 32
    object mnitOrder: TMenuItem
      Caption = #1055#1088#1080#1082#1072#1079
      OnClick = mnitOrderClick
    end
    object mnitActOV: TMenuItem
      Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      OnClick = mnitActOVClick
    end
    object NTagPrint: TMenuItem
      Tag = 1
      Caption = #1041#1080#1088#1082#1080' ('#1072#1082#1090')'
    end
  end
  object pdg2: TPrintDBGridEh
    DBGridEh = dg2
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 416
    Top = 152
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 296
    Top = 136
    object acPribtGrid: TAction
      Caption = 'acPribtGrid'
      ShortCut = 16464
      OnExecute = acPribtGridExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1080#1082#1072#1079
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acView: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewExecute
      OnUpdate = acViewUpdate
    end
    object acAddUiAct: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1082#1090' '#1085#1072' '#1080#1079#1076#1077#1083#1080#1077
      ShortCut = 119
      OnExecute = acAddUiActExecute
      OnUpdate = acAddUiActUpdate
    end
    object CheckClose: TAction
      Caption = 'CheckClose'
      OnExecute = CheckCloseExecute
    end
  end
end
