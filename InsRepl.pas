unit InsRepl;

interface

uses pFIBDatabase, pFIBQuery, SysUtils, pFIBDataSet, Menus, Classes;

procedure  InsRPacket(db: TpFIBDatabase; R_Item: string; DestId, ParamType: integer; KeyValue, Memo: string; NeedAck: integer);
procedure  FillReplMenu(db: TpFIBDatabase; pm: TPopUpMenu; ev: TNotifyEvent);
procedure  GetHereId(db: TpFIBDatabase);


implementation

uses RxStrUtils;

var slDep: TStringList;

var HereId: string;


procedure  InsRPacket(db: TpFIBDatabase; R_Item: string; DestId, ParamType: integer; KeyValue, Memo: string; NeedAck: integer);
var q: TpFIBQuery;
    tr: TpFIBTransaction;
    b: boolean;
    DID: string;
begin
  if R_Item='' then raise Exception.Create('R_ITEM is empty');
  q:=TpFIBQuery.Create(NIL);
  try
    q.Database:=db;
    tr:=db.DefaultTransaction;
    q.Transaction:=tr;
    b:=not tr.Active;
    if b then tr.StartTransaction;
    DID:=slDep.Values[IntToStr(DestId)];
    q.SQL.Text:='INSERT INTO R_PACKET(R_ITEM, SOURCEID, DESTID, PARAMTYPE, DIRECTION, CDATE, KEYVAL, MEMO, NEEDACK) '+
                'VALUES ('''+R_ITEM+''','''+HereId+''','''+DID+''','+
                         IntToStr(ParamType)+', 1, ''NOW'', '''+KeyValue+''', '''+Memo+''','+IntToStr(NeedAck)+')';
    q.ExecQuery;
    if b then tr.Commit
    else tr.CommitRetaining;
  finally
    q.Free;
  end;
end;


procedure FillReplMenu(db: TpFIBDatabase; pm: TPopUpMenu; ev: TNotifyEvent);
var i, t: integer;
    mi: TMenuItem;
    b, b1: boolean;
    q: TpFIBDataSet;
    tr: TpFIBTransaction;

begin
  q:=TpFIBDataSet.Create(NIL);
  try
    q.Database:=db;
    tr:=db.DefaultTransaction;
    q.Transaction:=tr;
    b:=not tr.Active;
    if b then tr.StartTransaction;
    q.SelectSQL.Text:='SELECT ID, DEP FROM R_DEP';
    with q do
      begin
        try
          Active:=True;
        except
        end;  
        i:=0;
        t:=0;
        while NOT EOF do
          begin
            if HereId<>Fields[0].AsString then
              begin
                mi:=TMenuItem.Create(pm);
                mi.Caption:=q.Fields[1].AsString;
                mi.Tag:=t;
                mi.OnClick:=ev;
                mi.GroupIndex:=0;
                mi.RadioItem:=True;
                slDep.Values[IntToStr(t)]:=Fields[0].AsString;                
                Inc(i);
                Inc(t);
                if i>30 then
                  begin
                    mi.Break:=mbBreak;
                    i:=0;
                  end;
                pm.Items.Add(mi);
              end;
            Next;
          end;
        Active:=False;
      end;
  finally
    if b then tr.Commit
    else tr.CommitRetaining;
    q.Free;
  end;
end;

procedure  GetHereId(db: TpFIBDatabase);
var q: TpFIBQuery;
    tr: TpFIBTransaction;
    b: boolean;
begin
  q:=TpFIBQuery.Create(NIL);
  try
    q.Database:=db;
    tr:=db.DefaultTransaction;
    q.Transaction:=tr;
    b:=not tr.Active;
    if b then tr.StartTransaction;
    q.SQL.Text:='SELECT ID FROM R_DEP WHERE HERE=1';
    q.ExecQuery;
    HereId:=DelRSpace(q.Fields[0].AsString);
    q.Close;
    if b then tr.Commit
    else tr.CommitRetaining;
  finally
    q.Free;
  end;
end;


initialization

  slDep:=TStringList.Create;

finalization

  slDep.Free;

end.
