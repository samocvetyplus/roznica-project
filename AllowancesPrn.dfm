object fmActAllowancesPrn: TfmActAllowancesPrn
  Left = 337
  Top = 243
  Width = 455
  Height = 434
  Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1089#1087#1080#1089#1072#1085#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 368
    Top = 20
    Width = 75
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 0
    OnClick = btnOkClick
  end
  object bntCancel: TButton
    Left = 368
    Top = 56
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 1
    OnClick = bntCancelClick
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 365
    Height = 405
    Caption = #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103' '#1086' '#1089#1087#1080#1089#1072#1085#1080#1080
    TabOrder = 2
    object Label1: TLabel
      Left = 20
      Top = 92
      Width = 114
      Height = 13
      Caption = #1057#1087#1080#1089#1072#1085#1080#1080' '#1074#1089#1083#1077#1076#1089#1090#1074#1080#1080':'
    end
    object Label2: TLabel
      Left = 20
      Top = 156
      Width = 173
      Height = 13
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1072'  '#1076#1077#1092#1077#1082#1090#1072' '#1090#1086#1074#1072#1088#1072':'
    end
    object Label3: TLabel
      Left = 20
      Top = 248
      Width = 108
      Height = 13
      Caption = #1055#1088#1080#1095#1080#1085#1072' '#1087#1086#1088#1095#1080' '#1080' '#1073#1086#1103':'
    end
    object Label4: TLabel
      Left = 20
      Top = 324
      Width = 86
      Height = 13
      Caption = #1055#1086#1090#1077#1088#1080' '#1086#1090#1085#1077#1089#1090#1080' :'
    end
    object Label5: TLabel
      Left = 24
      Top = 16
      Width = 76
      Height = 13
      Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1072#1082#1090#1072
    end
    object edtWRITEOFFRESON: TDBRichEdit
      Left = 20
      Top = 108
      Width = 245
      Height = 41
      DataField = 'WriteOffReason'
      DataSource = dm3.dsActAllowParamPrn
      TabOrder = 0
    end
    object edtDEFECT: TDBRichEdit
      Left = 20
      Top = 176
      Width = 245
      Height = 65
      DataField = 'DefectDiscript'
      DataSource = dm3.dsActAllowParamPrn
      TabOrder = 1
    end
    object edtSPOILING: TDBRichEdit
      Left = 20
      Top = 264
      Width = 245
      Height = 57
      DataField = 'spoilingreason'
      DataSource = dm3.dsActAllowParamPrn
      TabOrder = 2
    end
    object edtSOLUTION: TDBRichEdit
      Left = 20
      Top = 340
      Width = 245
      Height = 61
      DataField = 'Solution'
      DataSource = dm3.dsActAllowParamPrn
      TabOrder = 3
    end
    object edtActName: TDBRichEdit
      Left = 20
      Top = 32
      Width = 245
      Height = 57
      DataField = 'ActName'
      DataSource = dm3.dsActAllowParamPrn
      TabOrder = 4
    end
    object btnActName: TBitBtn
      Left = 268
      Top = 36
      Width = 85
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 5
      OnClick = btnActNameClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
        00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
        FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
        00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
        00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
        000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
        FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
        FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
        00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
        0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
    end
    object btnSpoiling: TBitBtn
      Left = 268
      Top = 108
      Width = 85
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 6
      OnClick = btnActNameClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
        00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
        FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
        00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
        00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
        000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
        FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
        FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
        00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
        0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
    end
    object btnDeffect: TBitBtn
      Left = 268
      Top = 176
      Width = 85
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 7
      OnClick = btnActNameClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
        00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
        FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
        00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
        00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
        000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
        FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
        FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
        00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
        0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
    end
    object btnWriteReson: TBitBtn
      Left = 272
      Top = 264
      Width = 85
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 8
      OnClick = btnActNameClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
        00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
        FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
        00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
        00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
        000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
        FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
        FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
        00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
        0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
    end
    object btnSolution: TBitBtn
      Left = 268
      Top = 340
      Width = 85
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      TabOrder = 9
      OnClick = btnActNameClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF0000000000000000000000000000000000000000
        00000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000000000
        FF00FF000000000000000000000000000000FFFFFFFFFFFF000000FFFFFF0000
        00000000FFFFFF000000FFFF0000000000000000FFFFFFFFFF00FFFFFFFFFF00
        FFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF000000000000000000FFFFFFFFFFFFFFFFFFFFFF
        FF000000FFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF000000FFFFFF000000000000FFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF00FFFFFFFFFF00000000000000000000000000000000FFFF0000
        00FFFFFFFFFFFF000000FFFF00000000FFFFFF00FFFFFFFFFF00FFFFFFFFFF00
        FFFFFFFFFF00FFFFFFFFFF000000FFFFFFFFFFFFFFFFFF000000FFFF00000000
        00FFFFFFFFFF000000000000000000000000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000000000000FFFFFFFFFF00FFFF00000000
        000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF000000FF00FFFF00FF
        FF00FF00000000000000000000000000FFFF000000FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF00000000FFFF00
        0000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000000000000FF00FFFF00FF
        FF00FFFF00FF00000000FFFF000000FFFFFFFFFFFF000000000000FFFFFF0000
        00FFFFFFFFFFFF000000FF00FFFF00FFFF00FF00000000FFFF000000000000FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF000000FF00FFFF00FFFF00FF
        0000000000FF000000FF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FFFF00FF00000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
    end
  end
end
