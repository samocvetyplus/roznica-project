unit PrOrd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, RxToolEdit, RXDBCtrl, StdCtrls, Mask, DBCtrls,
  Grids, DBGrids, M207Grid, M207IBGrid, db, Menus, RXSpin,
  M207Ctrls, ActnList, jpeg, rxPlacemnt, rxSpeedbar;

type
  TfmPrOrd = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    siExit: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    laDep: TLabel;
    pa2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    dgWH: TM207IBGrid;
    Panel4: TPanel;
    Label13: TLabel;
    edArt: TEdit;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    dg1: TM207IBGrid;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pm2: TPopupMenu;
    N3: TMenuItem;
    Label4: TLabel;
    DBText1: TDBText;
    N4: TMenuItem;
    DBEdit1: TDBEdit;
    siClose: TSpeedItem;
    DBText3: TDBText;
    Label6: TLabel;
    DBText5: TDBText;
    seP1: TRxSpinEdit;
    cbP1: TCheckBox;
    cbP2: TCheckBox;
    seP2: TRxSpinEdit;
    cbFilter: TCheckBox;
    N5: TMenuItem;
    Button1: TButton;
    lbCountry: TListBox;
    Splitter6: TSplitter;
    fr1: TM207FormStorage;
    acList: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acClosed: TAction;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure N4Click(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbFilterClick(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acClosedExecute(Sender: TObject);
    procedure siCloseClick(Sender: TObject);
  private
    { Private declarations }
    SearchEnable: boolean;
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmPrOrd: TfmPrOrd;

implementation

uses comdata, Data, DBTree, AddToPr, Data2, M207Proc, Data3, dbUtil,
  pFIBQuery, FIBQuery, MsgDialog;

{$R *.DFM}

procedure TfmPrOrd.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmPrOrd.FormCreate(Sender: TObject);
var
  b: boolean;
//  stat_prord: String; // ������ ��������� (������� / �������)
begin
  b:=dm.WorkMode='PRICE';
  pa2.Visible:=b;
  siAdd.Visible:=b;
  siDel.Visible:=b;
  siClose.Visible:=b;

  tb1.WallPaper:=wp;
  laDep.Caption:=dm.taPrOrdDep.AsString;

  with dmCom, dm do
    begin
      DPriceDepId:=taPrOrdDepId.AsInteger;
      taComp.SelectSQL[taComp.SelectSQL.Count-2]:='WHERE PRODUCER=1';
      taPrOrdItem.SelectSQL[4]:=' ';
      OpenDataSets([taPrOrdItem]);
      FillListBoxes(lbComp, lbMat, lbGood, lbIns,lbCountry, nil, nil);
      dmCom.D_Att1Id := ATT1_DICT_ROOT;
      dmCom.D_Att2Id := ATT2_DICT_ROOT;
      if taPrOrdItemCLOSEDATE.AsString='' then siclose.Enabled:=True else  siclose.Enabled:=False;
    end;
  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbcountry.ItemIndex:=0;  
  lbCompClick(NIL);
  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;
  //--------- ��������� ����� ��� ��������� �� �������� ----------------//
  dgWH.ColumnByName['SPRICE'].Visible := CenterDep;
  dgWH.ColumnByName['OPTPRICE'].Visible := CenterDep;
  dg1.ColumnByName['PRICE'].Visible := CenterDep;
  dg1.ColumnByName['OPTPRICE'].Visible := CenterDep;
  dg1.ColumnByName['OLDP'].Visible := CenterDep;
  dg1.ColumnByName['OLDOP'].Visible := CenterDep;
  dg1.ColumnByName['CURSP'].Visible := CenterDep;
  dg1.ColumnByName['CUROP'].Visible := CenterDep;
  //---------------------------------------------------------------------//
  with dm do
    begin
      Old_D_MatId:='.';
      cbP1.Checked:=DPriceP1Filter;
      cbP2.Checked:=DPriceP2Filter;
      seP1.Value:=DPriceP1;
      seP2.Value:=DPriceP2;
      cbFilter.Checked:=DPriceFilter;
    end;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);    
end;

procedure TfmPrOrd.siCloseClick(Sender: TObject);
var smsg:string;
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
    with dm, taPrOrd do
      begin
        PostDataSets([taPrOrd, taPrOrdItem]);
   with dm, quTmp do
    begin
     close;
     SQL.Text:='select WARN from CL_FROM_PRORD('+dm.taPrOrdPRORDID.AsString+')';
     ExecQuery;
     smsg :=trim(Fields[0].AsString);
     Close;
    end;
     if smsg='' then acClosed.Execute else
       begin
    if (MessageDialog(smsg+' ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes) then acClosed.Execute;
      end;
      end;
      dmcom.tr.CommitRetaining;
end;

procedure TfmPrOrd.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmPrOrd.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmPrOrd.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
end;

procedure TfmPrOrd.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmPrOrd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm do
    begin
      PostDataSets([taPrOrdItem]);
      if(taPrOrd.Active ) and (not closeinv (taPrOrdPRORDID.AsInteger, 1))  then
       if EmptyInv(taPrOrdPRORDID.AsInteger,1) then
       begin
        ExecSQL('delete from prord where prordid='+dm.taPrOrdPRORDID.AsString, dm.quTmp);
        ReOpenDataSet(dm.taPrOrd);
       end;
      CloseDataSets([taComp, taMat, taGood, taArt, taIns, quDPrice, taPrOrdItem]);
    end;
end;

procedure TfmPrOrd.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND ((Field.FieldName='FULLART') or (Field.FieldName='ART2')) then
    BAckground:=dmCom.clMoneyGreen;
  if dm.taPrOrdItem.FieldByName('IsReVal').AsInteger = 1 then Background:=clRed;
end;

procedure TfmPrOrd.N4Click(Sender: TObject);
begin
  dm.CheckClosedPr;
  try
    fmAddToPr:=TfmAddToPr.Create(NIL);
    if fmAddToPr.ShowModal=mrOK then
      with dm, dmCom, dm2 do
        begin
          PostDataSets([taPrOrd]);
          with quInsGr2PrOrd do
            begin

              Params.ByName['PRORDID'].AsInteger:=taPrOrdPrOrdId.AsInteger;
              Params.ByName['USERID'].AsInteger:=UserId;
              Params.ByName['D_COMPID'].AsInteger:=D_COMPID;
              Params.ByName['D_MATID'].AsString:=D_MATID;
              Params.ByName['D_GOODID'].AsString:=D_GOODID;
              Params.ByName['D_INSID'].AsString:=D_INSID;
              Params.ByName['DEPID'].AsInteger:=taPrOrdDepId.AsInteger;
              Params.ByName['P1'].AsFloat:=DPriceP1;
              Params.ByName['P2'].AsFloat:=DPriceP2;
              Params.ByName['P1F'].AsInteger:=Integer(DPriceP1Filter AND DPriceFilter);
              Params.ByName['P2F'].AsInteger:=Integer(DPriceP2Filter AND DPriceFilter);

              with fmAddToPr do
                begin
                  Params.ByName['SPRICE'].IsNull:=True;

                  if cb2.Checked then Params.ByName['PRICE2'].AsFloat:=fmAddToPr.seP2.Value
                  else Params.ByName['PRICE2'].IsNull:=True;

                  if cb3.Checked then Params.ByName['OPTPRICE'].AsFloat:=fmAddToPr.seP3.Value
                  else  Params.ByName['OPTPRICE'].IsNull:=True;
                end;

              ExecQuery;
            end;
          with taPrOrdItem do
            begin
              Active:=False;
              Open;
            end;
      end;
  finally
    fmAddToPr.Free;
  end;
end;

procedure TfmPrOrd.edArtChange(Sender: TObject);
begin
 if SearchEnable then
 begin
  if not dm.quDPrice.Active then dm.quDPrice.Active:=true; 
  dm.quDPrice.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey])
 end
end;

procedure TfmPrOrd.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with dm do
    case  Key  of
        VK_RETURN: begin
         if not quDPrice.Active then quDPrice.Active:=true;
          with quDPrice do
            begin
              try
                DisableControls;
                Next;
                if NOT LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
              finally
                EnableControls;
              end;
           end;
         end;
        VK_DOWN: ActiveControl:=dgWH;
      end;     
end;

procedure TfmPrOrd.cbFilterClick(Sender: TObject);
begin
  with dm, quDPrice do
    begin
      Active:=False;
      DPriceFilter:=cbFilter.Checked;
      DPriceP1Filter:=cbP1.Checked;
      DPriceP2Filter:=cbP2.Checked;
      DPriceP1:=seP1.Value;
      DPriceP2:=seP2.Value;
      Open;
    end;
end;

procedure TfmPrOrd.N5Click(Sender: TObject);
var LogOperationID:string;
begin
  dm.CheckClosedPr;
  if MessageDialog('������� ��������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
    with dm do
      begin
        LogOperationID:=dm3.insert_operation('������� ��������� ������',LogOprIdForm);
        quTmp.SQL.Text:='execute procedure DelGrFromPr'+IntToStr(dmCom.UserId);
        quTmp.ExecQuery;
        with taPrOrdItem do
          begin
            Active:=False;
            Open;
          end;
        dm3.update_operation(LogOperationID);  
      end;
end;

procedure TfmPrOrd.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;

end;

procedure TfmPrOrd.Button1Click(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_CountryId<>D_CountryId) or
       (Old_D_InsId<>D_InsId) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryId:=D_CountryId;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quDPrice]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmPrOrd.acAddExecute(Sender: TObject);
var i: integer;
    LogOperationID:string;
begin
  with dm, qutmp do
  begin
   close;
   sql.Text:='select count(*) from prorditem where prordid = '+taPrOrdPRORDID.AsString+
    ' and art2id='+quDPriceART2ID.AsString;
   ExecQuery;
   i:=Fields[0].AsInteger;
   Transaction.CommitRetaining;
   close;
  end;
  if i>0 then raise Exception.Create('������� ��� �������� � ������');


  LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
  with dm, taPrOrdItem do
    begin
      Append;
      taPrOrdItemArt2.AsString:=quDPriceArt2.AsString;
      taPrOrdItemCURSP.AsFloat:=quDPriceSPRICE.AsFloat;
      taPrOrdItemCUROP.AsFloat:=quDPriceOPTPRICE.AsFloat;
      ActiveControl:=dg1;
      with dg1 do
        begin
          i:=0;
          while Columns[i].Field.FieldName<>'PRICE2' do Inc(i);
          SelectedIndex:=i;
        end
    end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmPrOrd.acAddUpdate(Sender: TObject);
begin
 Taction(Sender).Enabled:= True;
 //Taction(Sender).Enabled:= dm.taPrOrdISCLOSED.AsInteger=0
end;

procedure TfmPrOrd.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�������',LogOprIdForm);
  with dm, taPrOrdItem do
    begin
      Delete;
    end;
  dm3.update_operation(LogOperationID)
end;

procedure TfmPrOrd.acClosedExecute(Sender: TObject);
begin
    with dm, taPrOrd do
      begin
    //  showmessage('1' +', '+ taPrOrdQ.AsString);
        PostDataSets([taPrOrd, taPrOrdItem]);
        if taPrOrdIsClosed.AsInteger=0 then;
          begin
     //     showmessage('2' +', '+ taPrOrdQ.AsString);
            Edit;
            taPrOrdIsClosed.AsInteger:=1;
            taPrOrdCloseDate.AsDateTime:=dmCom.GetServerTime;
            taPrOrdCloseEmpId.AsInteger:=dmCom.UserId;
            
            Post;
       //     showmessage('3'+', '+ taPrOrdQ.AsString);
          end;
         try
          Screen.Cursor:=crSQLWait;
          with quTmp do
            begin
       //     showmessage('4' +', '+ taPrOrdQ.AsString);
              SQL.Text:='execute procedure CLOSEPRORD '+taPrOrdPrOrdId.AsString+', '+IntToStr(dmCom.UserId);
              ExecQuery;
       //       showmessage('5' +', '+ taPrOrdQ.AsString);
            end;
          taPrOrd.Refresh;
      //    showmessage('6' +', '+ taPrOrdQ.AsString);
          with taPrOrdItem do
            begin
       //     showmessage('7' +', '+ taPrOrdQ.AsString);
              Active:=False;
        //      showmessage('8' +', '+ taPrOrdQ.AsString);
              Open;
       //       showmessage('9' +', '+ taPrOrdQ.AsString);
            end;
          with quDPrice do
            begin
      //      showmessage('10' +', '+ taPrOrdQ.AsString);
              Active:=False;
              Open;
      //        showmessage('11' +', '+ taPrOrdQ.AsString);
            end;
          siClose.Enabled:=False; // ������ "������� ������" ���������� �� ��������. ������� ����� �� �������������� �� ��������
        finally
          Screen.Cursor:=crDefault;
        end;
    end;
  dmCom.tr.CommitRetaining;
end;


end.
