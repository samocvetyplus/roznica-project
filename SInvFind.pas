unit SInvFind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Buttons, db, Variants, rxToolEdit;

type
  TfmSInvFind = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edN: TEdit;
    deNDate: TDateEdit;
    edSF: TEdit;
    edCost: TEdit;
    deSFDate: TDateEdit;
    lcSup: TDBLookupComboBox;
    lcDep: TDBLookupComboBox;
    bbFind: TBitBtn;
    bbFindNext: TBitBtn;
    BitBtn3: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcSupKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbFindClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSInvFind: TfmSInvFind;

implementation

uses comdata, Data, MsgDialog;

{$R *.DFM}

procedure TfmSInvFind.FormCreate(Sender: TObject);
begin
  with dm, dmCom do
    begin
      quDep.Active:=True;
      quSup.Active:=True;
    end;
  edN.Text:='';
  edSF.Text:='';
  edCost.Text:='';
  lcSup.KeyValue:=-1;
  lcDep.KeyValue:=-1;
end;

procedure TfmSInvFind.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
    begin
      quDep.Active:=False;
      quSup.Active:=False;
    end;
end;

procedure TfmSInvFind.lcSupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=8 then
    TDBLookupComboBox(Sender).KeyValue:=-1;
end;

procedure TfmSInvFind.bbFindClick(Sender: TObject);
var s: string;
    i: integer;
    FldValues: Variant;
    V: Variant;
    b: boolean;
begin
  b := False;
  s:='';
  i:=-1;
  FldValues:=VarArrayCreate([0, 6], varVariant);
  if edN.Text<>'' then
    begin
      Inc(i);
      s:='SN;';
      FldValues[i]:=edN.Text;
      v:=edN.Text;
    end;

  if deNDate.Date>1 then
    begin
      Inc(i);
      s:=s+'NDATE;';
      FldValues[i]:=deNDate.Date;
      v:=deNDate.Date;
    end;

  if lcSup.KeyValue<>-1 then
    begin
      Inc(i);
      s:=s+'SUPID;';
      FldValues[i]:=lcSup.KeyValue;
      v:=lcSup.KeyValue;
    end;

  if lcDep.KeyValue<>-1 then
    begin
      Inc(i);
      s:=s+'DepID;';
      FldValues[i]:=lcDep.KeyValue;
      v:=lcDep.KeyValue;
    end;

  if edSF.Text<>'' then
    begin
      Inc(i);
      s:='SSF;';
      FldValues[i]:=edSF.Text;
      v:=edSF.Text;
    end;

  if deSFDate.Date>1 then
    begin
      Inc(i);
      s:=s+'SFDATE;';
      FldValues[i]:=deSFDate.Date;
      v:=deSFDate.Date;
    end;

  if edCost.Text<>'' then
    begin
      Inc(i);
      s:='TotalCost;';
      FldValues[i]:=StrToFloat(edCost.Text);
      v:=StrToFloat(edCost.Text);
    end;

  if i>-1  then
    with dm.taSList do
      try
        DisableControls;
        SetLength(s, Length(s)-1);
          case TComponent(Sender).Tag of
              1:
                if i=0 then b:=Locate(s, V, [])
                else  b:=Locate(s, FldValues, []);
              2:
                 begin
                   Next;
                   if i=0 then   b:=LocateNext(s, V, [])
                   else   b:=LocateNext(s, FldValues, []);
                   if not b then Prior;
                 end
            end;
        if NOT b then MessageDialog('������ �� �������', mtInformation, [mbOK], 0);            
      finally
        EnableControls;
      end;
end;

procedure TfmSInvFind.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_F12, VK_ESCAPE] then Close;
end;

end.
