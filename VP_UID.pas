unit VP_UID;

interface

uses
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, rxPlacemnt, StdCtrls, Buttons, jpeg, DB, FIBDatabase,
  pFIBDatabase, FIBDataSet,  DBClient,   FileCtrl,
  Mask, DBCtrls,  IBCustomDataSet, IBDatabase,Grids,
  IBQuery, IBStoredProc, IBTable,  
  pFIBQuery, ShlObj,  ShellApi,
  dxGDIPlusClasses,     Consts,   
     RxGrdCpt,     IniFiles,
  AppEvnts, Winsock, ComDrv32, dbUtil, TypInfo, ComCtrls,
  Provider, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, 
  pFIBDataSet, DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, uDialog;

type
  TVP_UID_FORM = class(TForm)
    Label1: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    ART: TcxGridDBColumn;
    UID: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VP_UID_FORM: TVP_UID_FORM;
  dataSet:TDataSet;

implementation
    uses comdata, ind_order, IndividualOrderData, VP;
{$R *.dfm}

procedure TVP_UID_FORM.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin



if (dmIndividualOrder.mode=2) then
 DataSet:=dmIndividualOrder.dtOrder_factory
 else    DataSet:=dmIndividualOrder.dtOrder_in;

if (dmIndividualOrder.DataSet_ART_SINV.FieldByName('Art').AsString <>'') then
          begin
           if DataSet.State in [dsEdit, dsInsert] then
              begin

                DataSet.Post;

              end;

            with dmIndividualOrder.dtHistory do
              begin
                Append;
                Post;
              end;

            with DataSet do
              begin
                Edit;
                FieldByName('ID_State').Value:=7;
                FieldByName('EMP_STATE').Value:= dmCom.UserName;
                FieldByName('COMMENT').Clear;
                FieldByName('DATE_DOC').Value:= dmIndividualOrder.SINV_DataSet.FieldByName('SDATE').AsString ;
                FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SINV_DataSet.FieldByName('SN').AsString ;
                FieldByName('UID').Value:=dmIndividualOrder.DataSet_ART_SINV.FieldByName('UID').AsInteger;
                FieldByName('DATE_STATE').Value:= now;
                Refresh;
              end;
            dmIndividualOrder.dtHistory.Refresh;
            Screen.Cursor:=crDefault;

          end ;

Close;
end;

procedure TVP_UID_FORM.FormCreate(Sender: TObject);
begin
cxGrid1DBTableView1.DataController.DataSource:= dmIndividualOrder.DataSource_ART_Sinv;
end;

end.
