unit RespStoring;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, ExtCtrls, PrnDbgeh,
  ActnList, M207Ctrls, Menus, TB2Item, Printers, PrntsEh, 
  jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmRespStoring = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitPrint: TSpeedItem;
    siHelp: TSpeedItem;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    siPeriod: TSpeedItem;
    tb3: TSpeedBar;
    lbFindUID: TLabel;
    lbFindArt: TLabel;
    edFindUID: TEdit;
    edFindArt: TEdit;
    dg1: TDBGridEh;
    prdg1: TPrintDBGridEh;
    acList: TActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acPrintGrid: TAction;
    acDel: TAction;
    siDel: TSpeedItem;
    fr1: TM207FormStorage;
    pmDep: TTBPopupMenu;
    acRespStoring: TAction;
    siRespStoring: TSpeedItem;
    procedure acCloseExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure edFindUIDKeyPress(Sender: TObject; var Key: Char);
    procedure edFindUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFindArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acPrintGridUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acRespStoringUpdate(Sender: TObject);
    procedure acRespStoringExecute(Sender: TObject);
  private
    { Private declarations }
    procedure pmDepClick (Sender: TObject);
    procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  fmRespStoring: TfmRespStoring;

implementation

uses comdata, data3, M207Proc, DB, data, Period, reportdata, dbUtil, SetSDate, MsgDialog;

{$R *.dfm}

procedure TfmRespStoring.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmRespStoring.ShowPeriod;
begin
 laPeriod.Caption:='� '+DateToStr(dm3.RespBd) + ' �� ' +DateToStr(dm3.RespEd);
end;

procedure TfmRespStoring.pmDepClick (Sender: TObject);
begin
 if TTBItem(Sender).Tag=-1000 then begin
  dm3.RespDepid1:=-MaxInt;
  dm3.RespDepid2:=MaxInt;
 end else begin
  dm3.RespDepid1:=TTBItem(Sender).Tag;
  dm3.RespDepid2:=TTBItem(Sender).Tag;
 end;
 ReOpenDataSets([dm3.taRespStoring]);
end;

procedure TfmRespStoring.FormCreate(Sender: TObject);
var  pm:TTBItem;
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  tb3.Wallpaper := wp;
  with dm3 do
  begin
   FilterArtResp:='';
   FilterUidResp:=-1;
   RespDepid1:=-MaxInt;
   RespDepid2:=MaxInt;
   RespBd:=dmcom.FirstMonthDate;
   RespEd:=strtodatetime(datetostr(dmCom.GetServerTime))+0.9999;
   ReOpenDataSets([taRespStoring]);
  end;

 {��������� ������ ��������}
 if CenterDep then begin
  pm:=TTBItem.Create(pmdep);
  pm.Caption:='��� ������';
  pm.Tag:=-1000;
  pm.OnClick:=pmDepClick;
  pmdep.Items.Add(pm);

  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid<>-1000 and ISDELETE <> 1';
  dm.quTmp.ExecQuery;
  while not (dm.qutmp.Eof) do
  begin
   pm:=TTBItem.Create(pmdep);
   pm.Caption:=dm.quTmp.Fields[0].Asstring;
   pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;
   pmdep.Items.Add(pm);
   dm.quTmp.Next;
  end;
  dm.quTmp.Close;
  dm.quTmp.Transaction.CommitRetaining;
 end else begin
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid<>-1000 and ISDELETE <> 1 and d_depid='+inttostr(SelfDepId);
  dm.quTmp.ExecQuery;

  pm:=TTBItem.Create(pmdep);
  pm.Caption:=dm.quTmp.Fields[0].Asstring;
  pm.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm.OnClick:=pmDepClick;
  pmdep.Items.Add(pm);
 end;

 ShowPeriod;
end;

procedure TfmRespStoring.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRespStoring.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRespStoring.acHelpExecute(Sender: TObject);
begin
//
end;

procedure TfmRespStoring.acPrintGridExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 prdg1.Print;
end;

procedure TfmRespStoring.acDelExecute(Sender: TObject);
begin
 if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
   dm3.taRespStoring.Delete;
end;

procedure TfmRespStoring.acPrintExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(dg1, dm3.taRespStoringRESPSTORINGID);
  PrintDocument(arr, applresp);
end;

procedure TfmRespStoring.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRespStoringRESPSTORINGID.IsNull) and
  (dm3.taRespStoringDEPID.AsInteger=SelfDepId) and (dm3.taRespStoringSTATEUID.AsInteger=0);
end;

procedure TfmRespStoring.acPrintUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRespStoringRESPSTORINGID.IsNull)
end;

procedure TfmRespStoring.siPeriodClick(Sender: TObject);
begin
 if GetPeriod(dm3.RespBd, dm3.RespEd) then
 begin
  ReOpenDataSets([dm3.taRespStoring]);
  ShowPeriod;
 end;
end;

procedure TfmRespStoring.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<>nil then begin
  if Column.FieldName='UID' then Background:=$009FBABB
  else if Column.FieldName='SNAME' then Background:=dm3.taRespStoringCOLOR.AsInteger
  else if Column.FieldName='Sate' then
  case dm3.taRespStoringSTATEUID.AsInteger of
   0: Background:=clWindow;
   1: Background:=$00FFFF80;
   2: Background:=$00FF80FF;
   3: Background:=$00C08080;
   4: Background:=$0000FF80
  end
  else if dm3.taRespStoringCONFIRM.AsInteger=0 then Background:=dm3.taRespStoringCOLOR.AsInteger
  else Background:=clSilver;

 end;
end;

procedure TfmRespStoring.edFindUIDKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else sysutils.Abort
 end
end;

procedure TfmRespStoring.edFindUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: begin
   if edFindUID.Text='' then dm3.FilterUidResp:=-1
   else dm3.FilterUidResp:=strtoint(edFindUID.Text);
   ReOpenDataSets([dm3.taRespStoring]);
   edFindUID.Text:='';
  end;
 end
end;

procedure TfmRespStoring.edFindArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: begin
   if edFindArt.Text='' then dm3.FilterArtResp:=''
   else dm3.FilterArtResp:=edFindArt.Text;
   ReOpenDataSets([dm3.taRespStoring]);
   edFindArt.Text:='';
  end;
 end
end;

procedure TfmRespStoring.acPrintGridUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=not dm3.taRespStoringRESPSTORINGID.IsNull
end;

procedure TfmRespStoring.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([dm3.taRespStoring]);
end;

procedure TfmRespStoring.acRespStoringUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRespStoringRESPSTORINGID.IsNull) and
  ((dm3.taRespStoringSTATEUID.AsInteger=0) or (dm3.taRespStoringSTATEUID.AsInteger=3))
end;

procedure TfmRespStoring.acRespStoringExecute(Sender: TObject);
var  d, m:tdatetime;
     s_close:string;
begin
  if MessageDialog('����� � �������������� ��������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then sysUtils.Abort;
  m:=dmcom.GetServerTime;
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=m;
   fmSetSDate.tp1.Time:=m;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    with dm, quTmp do  begin
    {�������� ���� ��� ������ � ������������� ��������}
     SQL.Text:='select noedit from Edit_Date_RESP ('+dm3.taRespStoringRESPSTORINGID.AsString+','#39+datetimetostr(d)+#39')';
     ExecQuery;
     s_close:=trim(Fields[0].AsString);
     Transaction.CommitRetaining;
     close;
    end;
    if (s_close='') then begin
     Screen.Cursor:=crSQLWait;
     dm3.taRespStoring.Edit;
     dm3.taRespStoringDATENRESP.AsDateTime:=d;
     dm3.taRespStoring.Post;
     ExecSQL('update respstoring set stateuid=4 where respstoringid='+dm3.taRespStoringRESPSTORINGID.AsString, dm.quTmp);
     dm3.taRespStoring.Refresh;
     dg1.SumList.RecalcAll;
     Screen.Cursor:=crDefault;
    end else if s_close<>'' then  MessageDialog(s_close, mtWarning, [mbOk], 0);
   end
  finally
    fmSetSDate.Free;
  end;
end;

end.
