unit WHA2ALL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Placemnt, db, Menus;

type
  TfmWHA2All = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    M207IBGrid1: TM207IBGrid;
    siUID: TSpeedItem;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    siSZ: TSpeedItem;
    N2: TMenuItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siUIDClick(Sender: TObject);
    procedure siSZClick(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHA2All: TfmWHA2All;

implementation

uses comdata, Data, WHA2UID, WHSZ, Data2, M207Proc;

{$R *.DFM}

procedure TfmWHA2All.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWHA2All.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHA2All.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  dm.quWHA2All.Active:=True;
end;

procedure TfmWHA2All.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHA2All.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.quWHA2All.Active:=False;
end;

procedure TfmWHA2All.M207IBGrid1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='ART2') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmWHA2All.siUIDClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmWHA2UID, Self, TForm(fmWHA2UID), True, False)
end;

procedure TfmWHA2All.siSZClick(Sender: TObject);
begin
  dm.WHSZMode:=2;
  ShowAndFreeForm(TfmWHSZ, Self, TForm(fmWHSZ), True, False)
end;

end.
