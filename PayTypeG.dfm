object fmPayG: TfmPayG
  Left = 308
  Top = 185
  BorderStyle = bsDialog
  Caption = #1043#1088#1072#1092#1080#1082' '#1086#1087#1083#1072#1090
  ClientHeight = 290
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object gridPayG: TDBGridEh
    Left = 0
    Top = 0
    Width = 411
    Height = 290
    Align = alClient
    AllowedOperations = [alopUpdateEh, alopAppendEh]
    DataSource = dsrPayG
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SORTIND'
        Footers = <>
        Width = 38
      end
      item
        EditButtons = <>
        FieldName = 'PT'
        Footers = <>
        KeyList.Strings = (
          '1'
          '2')
        PickList.Strings = (
          #1054#1090#1089#1088#1086#1095#1082#1072
          #1056#1077#1072#1083#1080#1079#1072#1094#1080#1103)
        Width = 69
      end
      item
        EditButtons = <>
        FieldName = 'DC'
        Footers = <>
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'PERC'
        Footers = <>
        Width = 98
      end>
  end
  object taPayG: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE D_PAYG'
      'SET '
      '    PT = ?PT,'
      '    DC = ?DC,'
      '    PERC = ?PERC,'
      '    SORTIND = ?SORTIND,'
      '    PAYTYPEID = ?PAYTYPEID'
      'WHERE'
      '    ID = ?OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    D_PAYG'
      'WHERE'
      '        ID = ?OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO D_PAYG('
      '    ID,'
      '    PT,'
      '    DC,'
      '    PERC,'
      '    SORTIND,'
      '    PAYTYPEID'
      ')'
      'VALUES('
      '    ?ID,'
      '    ?PT,'
      '    ?DC,'
      '    ?PERC,'
      '    ?SORTIND,'
      '    ?PAYTYPEID '
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PT,'
      '    DC,'
      '    PERC,'
      '    SORTIND,'
      '    PAYTYPEID'
      'FROM'
      '    D_PAYG '
      ' WHERE '
      '        D_PAYG.ID = ?OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    PT,'
      '    DC,'
      '    PERC,'
      '    SORTIND,'
      '    PAYTYPEID '
      'FROM'
      '    D_PAYG '
      'where PayTypeId=:PAYTYPEID')
    AfterDelete = CommitRetaining
    BeforeClose = taPayGBeforeClose
    BeforeOpen = taPayGBeforeOpen
    OnNewRecord = taPayGNewRecord
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 80
    poSQLINT64ToBCD = True
    object taPayGSORTIND: TFIBSmallIntField
      DisplayLabel = #8470#1087'.'#1087'.'
      FieldName = 'SORTIND'
    end
    object taPayGPT: TFIBSmallIntField
      DisplayLabel = #1058#1080#1087' '#1086#1087#1083#1072#1090#1099
      FieldName = 'PT'
    end
    object taPayGDC: TFIBSmallIntField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1076#1085#1077#1081
      FieldName = 'DC'
    end
    object taPayGPERC: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
      FieldName = 'PERC'
      DisplayFormat = '0.##%'
    end
    object taPayGID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taPayGPAYTYPEID: TFIBIntegerField
      FieldName = 'PAYTYPEID'
    end
  end
  object dsrPayG: TDataSource
    DataSet = taPayG
    Left = 16
    Top = 128
  end
  object acEvent: TActionList
    Left = 68
    Top = 80
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ShortCut = 45
      OnExecute = acAddExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ShortCut = 46
      OnExecute = acDelExecute
    end
  end
end
