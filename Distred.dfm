object fmDistred: TfmDistred
  Left = 268
  Top = 179
  Width = 711
  Height = 414
  Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 703
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 379
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 42
    Width = 703
    Height = 321
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsDistred
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = FormStorage1
    TitleButtons = True
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRODCODE'
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1075'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 32
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_MATID'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 35
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_GOODID'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 44
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_INSID'
        Title.Alignment = taCenter
        Title.Caption = #1054#1042
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 37
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART2'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Q'
        Title.Alignment = taCenter
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'W'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SZ'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NDS'
        Title.Alignment = taCenter
        Title.Caption = #1053#1044#1057
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 80
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 363
    Width = 703
    Height = 22
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 397
      Top = 0
      Width = 72
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1042' '#1087#1088'. '#1094#1077#1085#1072#1093
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object M207DBPanel1: TM207DBPanel
      Left = 469
      Top = 0
      Width = 88
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = 'M207DBPanel1'
      TabOrder = 1
      DataField = 'SCOST'
      DataSource = dm2.dsDistredT
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 53
      Height = 22
      Align = alLeft
      BevelOuter = bvNone
      Caption = #1042#1057#1045#1043#1054
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel4: TPanel
      Left = 53
      Top = 0
      Width = 44
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object M207DBPanel2: TM207DBPanel
      Left = 97
      Top = 0
      Width = 52
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = 'M207DBPanel1'
      TabOrder = 4
      DataField = 'Q'
      DataSource = dm2.dsDistredT
    end
    object Panel5: TPanel
      Left = 253
      Top = 0
      Width = 48
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1057#1091#1084#1084#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
    object M207DBPanel3: TM207DBPanel
      Left = 301
      Top = 0
      Width = 96
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = 'M207DBPanel1'
      TabOrder = 6
      DataField = 'COST'
      DataSource = dm2.dsDistredT
    end
    object Panel6: TPanel
      Left = 149
      Top = 0
      Width = 36
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object M207DBPanel4: TM207DBPanel
      Left = 185
      Top = 0
      Width = 68
      Height = 22
      Align = alLeft
      BevelOuter = bvLowered
      Caption = 'M207DBPanel1'
      TabOrder = 8
      DataField = 'W'
      DataSource = dm2.dsDistredT
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 206
    Top = 146
  end
end
