unit SRetLst;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl,
  StdCtrls, db, Menus, RxMenus, DBCtrls, DBGridEh, ActnList, TB2Item,
  PrnDbgeh,
  Return_Act_Print,
  ComData, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmSRetList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    laDepFrom: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    spitPrint: TSpeedItem;
    siPeriod: TSpeedItem;
    laPeriod: TLabel;
    ppRet: TPopupMenu;
    mnitCDM: TMenuItem;
    N6: TMenuItem;
    svdFile: TSaveDialog;
    mnitOther: TMenuItem;
    acEvent: TActionList;
    gridSRetList: TDBGridEh;
    StatusBar1: TStatusBar;
    acDel: TAction;
    acView: TAction;
    ppSRetList: TTBPopupMenu;
    acAddJew: TAction;
    acCreateSInvBuSRet: TAction;
    acExportToTxt: TAction;
    acPrintFacture: TAction;
    acPrintInvoiceUID: TAction;
    acPrintInvoiceArt: TAction;
    ppPrint: TTBPopupMenu;
    TBItem4: TTBItem;
    acPrintInvoiceUID2: TAction;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem8: TTBItem;
    acChangeSDate: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSubmenuItem2: TTBSubmenuItem;
    TBItem10: TTBItem;
    TBItem12: TTBItem;
    TBItem13: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem11: TTBItem;
    SpeedItem1: TSpeedItem;
    ppAdditional: TTBPopupMenu;
    TBItem14: TTBItem;
    TBItem15: TTBItem;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBItem18: TTBItem;
    TBItem19: TTBItem;
    TBItem20: TTBItem;
    pgPrint: TPrintDBGridEh;
    acPrintGrid: TAction;
    lbDepName: TLabel;
    siHelp: TSpeedItem;
    TBItem21: TTBItem;
    acPrintActRet: TAction;
    siCheck: TSpeedItem;
    acCheck: TAction;
    acDelCheck: TAction;
    TBItem7: TTBItem;
    acPrintInvoice12sup: TAction;
    TBItem9: TTBItem;
    SpeedItem2: TSpeedItem;
    TBItem22: TTBItem;
    acPrintInvoiceArt2: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acAddJewExecute(Sender: TObject);
    procedure acAddJewUpdate(Sender: TObject);
    procedure acCreateSInvBuSRetExecute(Sender: TObject);
    procedure acExportToTxtExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure acChangeSDateExecute(Sender: TObject);
    procedure gridSRetListDblClick(Sender: TObject);
    procedure gridSRetListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridSRetListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acExportToTxtUpdate(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acPrintActRetUpdate(Sender: TObject);
    procedure acCheckExecute(Sender: TObject);
    procedure acDelCheckExecute(Sender: TObject);
    procedure acPrintActRetExecute(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);

  private
    LogOprIdForm:string;
    dm_Return_Act_Print: Tdm_Return_Act_Print;

    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  end;

var
  fmSRetList: TfmSRetList;

implementation

uses Data, ReportData, Period, SRet, M207Proc, bsUtils,
     rxStrUtils, SupCase, getdata, ShellAPI, data3, jewconst,
     FIBQuery, MsgDialog, ServData, DInvCheck, dbUtil;

{$R *.DFM}

procedure TfmSRetList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmSRetList.FormCreate(Sender: TObject);
begin
  laDepFrom.Caption:='';
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  with dmCom do
    begin
      tr.Active:=True;
      taRec.Active:=True;
    end;
  dm.WorkMode:='SRET';
  dm.SetBeginDate;
  ShowPeriod;
  dmcom.bdata:=dmCom.GetServerTime;
  dmcom.edata:=dmCom.GetServerTime;

  dm.DDepFromId:= CenterDepId;
  dm.DDepFrom:= SelfDepName;
  laDepFrom.Caption:=dm.DDepFrom;
  OpenDataSets([dm.taSRetList]);

  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  dm_Return_Act_Print := Tdm_Return_Act_Print.Create(Self);
  with dm_Return_Act_Print do
  begin
    DataBase := dmCom.db;
    if DocPreview = pvDesign then
      Preview := True
    else
      Preview := False;
  end;
end;

procedure TfmSRetList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm_Return_Act_Print.Free;
  with dmCom do
    begin
      CloseDataSets([dm.taSRetList, taRec]);
      tr.CommitRetaining;
    end;
  dm.WorkMode:='';
end;

procedure TfmSRetList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSRetList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSRetList.siPeriodClick(Sender: TObject);
begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then
    begin
      ReOpenDataSets([dm.taSRetList]);
      ShowPeriod;
    end;
end;

procedure TfmSRetList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm.BeginDate) + ' �� ' +DateToStr(dm.EndDate);
end;


procedure TfmSRetList.acDelExecute(Sender: TObject);
var
  LogOperationID:string;
begin
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  LogOperationID:=dm3.insert_operation(sLog_SRetLstDel,LogOprIdForm);
  dm.taSRetList.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSRetList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dm.taSRetList.Active and (not dm.taSRetList.IsEmpty);
end;

procedure TfmSRetList.acViewExecute(Sender: TObject);
var LogOperationID:string;
begin
  dm.taSRetList.Refresh;
  if dm.taSRetListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSets([dm.taSRetList]);
  end else begin
   LogOperationID:=dm3.insert_operation(sLog_SretLstView,LogOprIdForm);
   ShowAndFreeForm(TfmSRet, Self, TForm(fmSRet), True, False);
   dm3.update_operation(LogOperationID);
  end
end;

procedure TfmSRetList.acViewUpdate(Sender: TObject);
begin
  acView.Enabled := dm.taSRetList.Active and (not dm.taSRetList.IsEmpty);
end;

procedure TfmSRetList.acAddJewExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_SretLstAddJew,LogOprIdForm);
  if dm.DDepFromID=-1 then MessageDialog('���������� ������� �������� �����',
                                      mtInformation, [mbOK], 0)
      else
        begin
          dm.taSRetList.Append;
          ShowAndFreeForm(TfmSRet, Self, TForm(fmSRet), True, False);
        end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSRetList.acAddJewUpdate(Sender: TObject);
begin
  acAddJew.Enabled := dm.taSRetList.Active;
end;

procedure TfmSRetList.acCreateSInvBuSRetExecute(Sender: TObject);
var LogOperationID:string;
begin
 LogOperationID:=dm3.insert_operation(sLog_acCreateSInvBuSRet,LogOprIdForm);
 if dm.taSRetListISCLOSED.AsInteger = 1 then
   begin
    if dm.taSRetListCOMPID.AsInteger <> -1 then
    begin
     dm.qutmp.Close;
     Screen.Cursor := crSQLWait;
     dm.qutmp.SQL.Text := 'select sn from sret_sinv ('+dm.taSRetListSINVID.Asstring+')';
     dm.qutmp.ExecQuery;
     if dm.qutmp.Fields[0].AsInteger<>0 then
      MessageDialog('���������� �������� '+dm.qutmp.Fields[0].AsString+' ���������!', mtInformation, [mbOk], 0);
     Screen.Cursor := crDefault;
     dmcom.tr.CommitRetaining;
     dm.quTmp.Close;
    end
    else MessageDialog('��� ��������� � ����������� "���" �� ����� ���� ������� ��������� ��������!', mtInformation, [mbOk], 0);
   end
 else MessageDialog('��������� ������ ���� �������!', mtInformation, [mbOk], 0);
 dm3.update_operation(LogOperationID);
end;

procedure TfmSRetList.acExportToTxtExecute(Sender: TObject);
  function sz_sup(const good,sz:string):string;
  begin
    with dm.quTmp do
    begin
      Close;
      SQL.Text := 'select Contrsize from D_Good where d_goodid='#39+Good+#39;
      ExecQuery;
      if Fields[0].AsInteger=0 then result:='-'
      else result:=sz;
    end;
  end;

  function CompUID(const c:integer;const Art2:String):string;
  begin
    if c=0 then result:='-'
    else result:=Art2;
  end;

  function Art_cdm (art_sup:string):string;
  begin
   dm.quTmp.Close;
   dm.quTmp.SQL.Text:='select first 1 art_cdm, art from d_art where d_compid=311 and art='#39+art_sup+#39;
   dm.quTmp.ExecQuery;
   if dm.qutmp.fields[0].isNull then Result:=dm.qutmp.fields[1].AsString
   else Result:= dm.qutmp.fields[0].AsString;
   dm.quTmp.Close;
   dm.quTmp.Transaction.CommitRetaining;
  end;
var
  arr : Tarr;
  ftext:textfile;
  sDir, Path, art:string;
  i,inv:integer;
  LogOperationID:string;
begin
{  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  svdFile.FileName:=dm.taSRetListSN.AsString;
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='jew';
  if not svdFile.Execute then eXit;}

  gen_arr(arr, gridSRetList, dm.taSRetListSInvId);
  Screen.Cursor:=crSQLWait;
  try
   //�������� � ��������� ����
   with dmReport do
    begin
      with quTmp do
        begin
         Close;
         SQL.Text:='select sinvid from sinv where itype=1 and REFRETSINVID='+inttostr(dm.taSRetListSInvId.AsInteger);
         ExecQuery;
         if not Fields[0].IsNull then
           inv:=Fields[0].asinteger
         else inv:=0;
         dmCom.tr.CommitRetaining;
         Close;
         end;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsBillItem]);
          ibdsBillItem.Params[0].Value := arr[i];
          ibdsBillItem.Params[1].Value := 43;
          OpenDataSets([ibdsBillItem]);
          svdFile.FileName:=dm.taSRetListSN.AsString;
          svdFile.InitialDir:=sDir;
          svdFile.DefaultExt:='jew';
          if svdFile.Execute then
           begin
             LogOperationID:=dm3.insert_operation(sLog_ExportToTextCdm,LogOprIdForm);
             Path:=svdFile.FileName;
             AssignFile(ftext, Path);
             try
               Rewrite(ftext);
               with ibdsBillItem do
                begin
                 First;
                 while not EOf do
                  begin
                    if dm.taSRetListCOMPID.AsInteger=311 then  art:=Art_cdm(art);
                    writeln(ftext,format('%d %f',
                    [ibdsBillItemR_UID_Sup.asInteger, ibdsBillItemR_PRICE.asFloat]));
                    Next;
                  end;
                end;
             finally
              if inv>0 then
               begin
                 CloseDataSets([ibdsBillItem]);
                 ibdsBillItem.Params[0].Value := inv;
                 ibdsBillItem.Params[1].Value := 2;
                 OpenDataSets([ibdsBillItem]);
                 with ibdsBillItem do
                 begin
                   First;
                   while not EOf do
                   begin
                    if dm.taSRetListCOMPID.AsInteger=311 then  art:=Art_cdm(art);

                     writeln(ftext,format('%d %f',
                       [ibdsBillItemR_UID_Sup.asInteger, ibdsBillItemR_PRICE.asFloat]));
                     Next;
                   end;
                 end;
               end;
              CloseFile(fText);
//              ShellExecute(Handle, 'open', PChar(Path), nil, nil, SW_SHOW);
             end;
           end;
        end;
    end;
    Screen.Cursor:=crDefault;
  finally
    Finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSRetList.acPrintExecute(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID:string;
begin
  case (Sender as TAction).Tag of
//   0: LogOperationID:=dm3.insert_operation(sLog_PrintFactureSRetLst,LogOprIdForm); //���� �������
//   1: LogOperationID:=dm3.insert_operation(sLog_PrintInvoiceUIDSRetLst,LogOprIdForm);
   2: LogOperationID:=dm3.insert_operation(sLog_PrintInvoiceArtSRetLst,LogOprIdForm);
   3: LogOperationID:=dm3.insert_operation(sLog_PrintInvoiceUID2SRetLst,LogOprIdForm);
   4: LogOperationID:=dm3.insert_operation(sLog_PrintInvoice12supSRetLst,LogOprIdForm);  //����12 � ���������
   5: LogOperationID:=dm3.insert_operation(sLog_PrintInvoiceArtSRetLst2,LogOprIdForm);  //����12 � ����.�����
  end;

  arr:=gen_arr(gridSRetList, dm.taSRetListSInvId);
  try
    case (Sender as TAction).Tag of
//      0 : PrintDocument(arr, facture_ret);  //���� �������
//      1 : PrintDocument(arr, invoice_ret);
      2 : PrintDocument(arr, invoice_art_ret);
      3 : PrintDocument(arr, invoice_ret_01);
      4 : PrintDocument(arr, invoice_12_sup); //����12 � ���������
      5 : PrintDocument(arr, invoice_art_ret_2); //����12 � ����.�����
    end;          
  finally
    Finalize(arr);
  end;

  dm3.update_operation(LogOperationID);
end;

procedure TfmSRetList.gridSRetListDblClick(Sender: TObject);
begin
  acView.Execute;
end;

procedure TfmSRetList.gridSRetListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dm.taSRetListIsClosed.AsInteger=0 then Background:=clInfoBk
  else Background:=clBtnFace;
end;

procedure TfmSRetList.acPrintUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := dm.taSRetList.Active and (not dm.taSretList.IsEmpty);
end;

procedure TfmSRetList.gridSRetListKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
     VK_RETURN: acView.Execute;
  end;
end;

procedure TfmSRetList.acChangeSDateExecute(Sender: TObject);
var
  MornthDate:TDateTime;
  yy,mm,dd,h,m,s,ms:word;
  LogOperationID:string;
begin
 mornthDate:=dmCom.GetServerTime;
 if Getdate(mornthDate) then
 begin
   LogOperationID:=dm3.insert_operation(sLog_ChangeSDate,LogOprIdForm);
   DecodeDate(mornthDate,yy,mm,dd);
   DecodeTime(dmCom.GetServerTime,h,m,s,ms);
   mornthDate:=(EncodeDate(yy,mm,dd)+EncodeTime(h,m,s,ms));
   with dm do
   begin
     qutmp.Close;
     qutmp.SQL.Text:='select noedit from edit_date_rinv('#39+datetimetostr(mornthDate)+#39')';
     qutmp.ExecQuery;
     if strtoint(qutmp.Fields[0].AsString)>0 then MessageDialog('� '+trim(qutmp.Fields[0].AsString)+' ��������� �� ����� ���� �������� ����', mtWarning, [mbOk], 0);
     qutmp.Transaction.CommitRetaining;
     qutmp.Close;
     ReOpenDataSets([taSRetList]);
   end;
   dm3.update_operation(LogOperationID);
 end
end;

procedure TfmSRetList.acExportToTxtUpdate(Sender: TObject);
begin
  acExportToTxt.Enabled := dm.taSRetList.Active and (not dm.taSRetList.IsEmpty);
end;

procedure TfmSRetList.acPrintGridExecute(Sender: TObject);
begin
 pgPrint.Print;
end;

procedure TfmSRetList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100211);
end;

procedure TfmSRetList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSRetList.acPrintActRetUpdate(Sender: TObject);
begin
  {$IFDEF RGOST}
  acPrintActRet.Visible := True;
  {$ELSE}
  acPrintActRet.Visible := False;
  {$ENDIF}
  if acPrintActRet.Visible then
    acPrintActRet.Enabled := dm.taSRetList.Active and (not dm.taSRetList.IsEmpty);
end;

procedure TfmSRetList.acCheckExecute(Sender: TObject);
begin
  if dm.taSRetListISCLOSED.AsInteger = 1 then
     MessageDialog('�������� �� �������� ��� �������� ���������', mtInformation, [mbOk], 0)
  else
  begin
    dm3.CHECKMode := 'SRET';
    ExecSQL('execute procedure DInvCHECK_FILL('+ dm.taSRetListSINVID.AsString+ ')', dmServ.quTmp );
    //ExecSQL('execute procedure DInvCHECK_FILL(dm.taSRetListSINVID.AsInteger);', dmServ.quTmp );
    ShowAndFreeForm(TfrmDInvCheck, Self, TForm(frmDInvCheck), True, False);
  end;

end;

procedure TfmSRetList.acDelCheckExecute(Sender: TObject);
begin
  ExecSQL('delete from Dinvcheck  where SInvID = '+ dm.taSRetListSINVID.AsString, dm.quTmp);
end;

procedure TfmSRetList.acPrintActRetExecute(Sender: TObject);
begin
  with dm_Return_Act_Print do
  begin
    SInvID := dm.taSRetList.FieldByName('SinvID').AsInteger;
    Report;                 
  end;
end;

procedure TfmSRetList.SpeedItem2Click(Sender: TObject);
begin
with dm, dmCom do
  begin
  PostDataSet(taSRetList);
  tr.CommitRetaining;
  ReOpenDataSet(taSRetList);
  taSRetList.Refresh;
  end;
end;

end.
