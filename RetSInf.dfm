object fmRetSInf: TfmRetSInf
  Left = 515
  Top = 233
  Width = 336
  Height = 209
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 60
    Height = 13
    Caption = #1044#1072#1090#1072' '#1074#1085#1091#1090#1088'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 58
    Height = 13
    Caption = #8470' '#1074#1085#1077#1096#1085#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 80
    Width = 72
    Height = 13
    Caption = #8470' '#1074#1085#1091#1090#1088#1077#1085#1085#1080#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 104
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 128
    Width = 82
    Height = 13
    Caption = #1055#1088#1080#1093#1086#1076#1085#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 152
    Width = 24
    Height = 13
    Caption = #1053#1044#1057
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 32
    Width = 64
    Height = 13
    Caption = #1044#1072#1090#1072' '#1074#1085#1077#1096#1085'.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 240
    Top = 4
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 240
    Top = 32
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkCancel
  end
  object deSDate: TDateEdit
    Left = 100
    Top = 4
    Width = 121
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 2
  end
  object edSSF: TEdit
    Left = 100
    Top = 52
    Width = 121
    Height = 21
    Color = clInfoBk
    TabOrder = 3
  end
  object edSN: TEdit
    Left = 100
    Top = 76
    Width = 121
    Height = 21
    Color = clInfoBk
    TabOrder = 4
  end
  object lcSup: TDBLookupComboBox
    Left = 100
    Top = 100
    Width = 121
    Height = 21
    Color = clInfoBk
    KeyField = 'D_COMPID'
    ListField = 'SNAME'
    ListSource = dm.dsSup
    TabOrder = 5
  end
  object cePrice: TCurrencyEdit
    Left = 100
    Top = 124
    Width = 121
    Height = 21
    AutoSize = False
    Color = clInfoBk
    TabOrder = 6
  end
  object lcNDS: TDBLookupComboBox
    Left = 100
    Top = 148
    Width = 121
    Height = 21
    Color = clInfoBk
    KeyField = 'NDSID'
    ListField = 'NAME'
    ListSource = dmCom.dsNDS
    TabOrder = 7
  end
  object deNDate: TDateEdit
    Left = 100
    Top = 28
    Width = 121
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 8
  end
end
