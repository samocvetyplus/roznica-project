{*********************************************************}
{          ��������� ������� �������                      }
{     Copyrigth (C) 2000-2004 SZinkov                     }
{ dm.SellMode                                             }
{   True - ����� ������� � ��������� �� ��������� ������� }
{   False - ����� ������� � ��������� �� �����������      }
{*********************************************************}
unit ORet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, RXDBCtrl, StdCtrls, Mask,
  Grids, DBGrids, M207Grid, M207IBGrid, db, Menus, pFIBDataSet,
  MsgDialog, ActnList, jpeg, rxPlacemnt, rxToolEdit, rxSpeedbar;

type
  TfmORet = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    edSN: TDBEdit;
    deSDate: TDBDateEdit;
    FormStorage1: TFormStorage;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel4: TPanel;
    Label13: TLabel;
    edArt: TEdit;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    Splitter6: TSplitter;
    DBText2: TDBText;
    pmEl: TPopupMenu;
    N1: TMenuItem;
    pmWH: TPopupMenu;
    N2: TMenuItem;
    siCloseInv: TSpeedItem;
    lcComp: TDBLookupComboBox;
    pc1: TPageControl;
    tsPrice: TTabSheet;
    dgDPrice: TM207IBGrid;
    Label2: TLabel;
    edUID: TEdit;
    Button1: TButton;
    dgEl: TM207IBGrid;
    TabSheet1: TTabSheet;
    dgSelled: TM207IBGrid;
    Panel6: TPanel;
    Label3: TLabel;
    deBD: TDateEdit;
    Label7: TLabel;
    deED: TDateEdit;
    lcRetClient: TDBLookupComboBox;
    Label8: TLabel;
    lbcountry: TListBox;
    Splitter7: TSplitter;
    siHelp: TSpeedItem;
    acList: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acHelp: TAction;
    acClose: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgElGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siCloseInvClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dgDPriceGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure deBDChange(Sender: TObject);
    procedure deEDChange(Sender: TObject);
    procedure lcRetClientKeyPress(Sender: TObject; var Key: Char);
    procedure lcCompCloseUp(Sender: TObject);
    procedure lcRetClientCloseUp(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
  private
    { Private declarations }
    SearchEnable: boolean;
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmORet: TfmORet;

implementation

uses comdata, Data, DBTree, DItem, Data2, OptItem,
     ReportData, SellUid, Variants, M207Proc, Data3, JewConst, dbUtil;

{$R *.DFM}

procedure TfmORet.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmORet.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm, dm2 do
    begin
      Old_D_MatId:='.';
      OpenDataSets([taNDS, taRet,taORet, quBuyer]);
      deBD.Date:=BeginDate;
      deED.Date:=EndDate;
      OptBuyerId:=taORetListCompId.Value;
    end;

  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbcountry,nil,nil);
  dmCom.D_Att1Id := ATT1_DICT_ROOT;
  dmCom.D_Att2Id := ATT2_DICT_ROOT;

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;  
  lbCompClick(NIL);
  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;
  SetCBDropDown(lcComp);
  edUID.Text:='';
  dm.SetCloseInvBtn(siCloseInv, dm.taORetListIsClosed.AsInteger);
  lcRetClient.KeyValue:=dm.OptBuyerId;
  dm.SellMode:=true;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);  
end;

procedure TfmORet.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmORet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom, dm2 do
    begin
      PostDataSets([taORetList, taORet]);
      taORetList.Refresh;
      if dm.taORetListSINVID.IsNull then begin
       MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
       ReOpenDataSet(dm.taORetList);
      end else begin
       if not closeinv(taORetListSINVID.AsInteger,0) then
       begin
        if EmptyInv(taORetListSINVID.AsInteger,0) then
        begin
         ExecSQL('delete from sinv where sinvid='+taORetListSINVID.AsString, dm.quTmp);
         ReOpenDataSet(taORetList);
        end
       else if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
       end;
      end; 
      taORetList.Refresh;
      CloseDataSets([taORet, quD_WH, quSup, taRet, taNDS]);
      tr.CommitRetaining;
    end;
end;

procedure TfmORet.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;  
//  pc1Change(NIL);
end;

procedure TfmORet.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmORet.edArtChange(Sender: TObject);

begin
  if SearchEnable then
    with dm do
      if pc1.ActivePage.TabIndex=0 then
        begin
          with quDPrice do
            if Active then Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey])
        end
      else
        begin
          with quOSelled do
            if Active then Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
        end;
end;

procedure TfmORet.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var d: TpFIBDataSet;
begin
  case Key of
    VK_RETURN:
      if SearchEnable then
        with dm do
          begin
            if pc1.ActivePage.TabIndex=1 then d:= quOSelled
            else d:= quDPrice;
            with d do
              if Active then
                try
                  DisableControls;
                  Next;
                  if NOT LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
                finally
                  EnableControls;
                end
          end;
    VK_DOWN:
       if pc1.ActivePage.TabIndex=1 then ActiveControl:= dgSelled
       else ActiveControl:= dgDPrice;
    end;
end;

procedure TfmORet.dgElGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and ((Field.FieldName='FULLART') or (Field.FieldName='ART2')) then
    case dm.taSElWHSell.AsInteger of
        0: Background:=clAqua;
        1: Background:=dmCom.clMoneyGreen;
      end;
end;

procedure TfmORet.siCloseInvClick(Sender: TObject);
var i: integer;
    LogOperationID:string;
begin
  with dm do
    begin
      PostDataSets([taORet, taORetList]);
      taORetList.Refresh;
      if taORetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
      else begin
       if taORetListIsClosed.AsInteger=0 then
        begin
          PostDataSets([taORet, taORetList]);
          with quTmp do
            begin
              SQL.Text:='SELECT COUNT(*) FROM SINV '+
                        'WHERE ITYPE=8 AND SN='+taORetListSN.AsString+
                        ' AND DepId='+taORetListDepId.AsString +' AND FYEAR(SDATE)='+GetSYear(taORetListSDate.AsDateTime);
              ExecQuery;
              i:=Fields[0].AsInteger;
              Close;
              if i>1 then raise Exception.Create('������������ ����� ���������!!!');
            end;

          if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            begin
              LogOperationID:=dm3.insert_operation(sLog_CloseORet,LogOprIdForm);
              with quTmp do
                begin
                  SQL.Text:='EXECUTE PROCEDURE CloseInv '+taORetListSInvId.AsString+', 2, '+IntToStr(dmCom.UserId);
                  ExecQuery;
                end;
              taORetList.Refresh;
              dm3.update_operation(LogOperationID);
            end;
          SetCloseInvBtn(siCloseInv, 1);
        end
      else
        if MessageDialog('������� ���������', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
          begin
            LogOperationID:=dm3.insert_operation(sLog_OpenORet,LogOprIdForm);
            with quTmp do
              begin
                SQL.Text:='UPDATE SInv SET ISCLOSED=0 WHERE SINVID='+taORetListSInvId.AsString;
                ExecQuery;
              end;
            taORetList.Refresh;
            SetCloseInvBtn(siCloseInv, 0);
            dm3.update_operation(LogOperationID);
          end;
      end;    
    end;
end;

procedure TfmORet.pc1Change(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    if pc1.ActivePage=tsPrice then
      with dm, quDPrice do
        begin
          SellMode:=False;
          Active:=False;
          Open;
        end
    else
      with dm, quOSelled do
        begin
          SellMode:=True;
          Active:=False;
          Open;
        end;
  finally
    Screen.Cursor:=crDefault;  
  end;
end;

procedure TfmORet.dgWHGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Highlight then
    if Field.FieldName='OPTPRICE' then
      case dm.quD_WHOPEQ.AsInteger of
          0: Background:=clRed;
          1: Background:=clYellow;
        end
    else if (Field.FieldName='DQ') or (Field.FieldName='DW') then Background:=dmCom.clCream;
end;

procedure TfmORet.dgDPriceGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Highlight then
    if Field.FieldName='OPTPRICE' then
      case dm.quDPriceOPEQ.AsInteger of
          0: Background:= clRed;
          1: Background:= clYellow;
        end;
end;

procedure TfmORet.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        taORetList.Refresh;
        if taORetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
        else begin
         if taORetListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
         with quCheckUIDOSelled, Params do
          begin
            ByName['AUID'].AsInteger:=StrToInt(edUID.Text);
            ByName['DEPID'].AsInteger:=taORetListDepId.AsInteger;
            ByName['OPT_BUYER'].AsInteger:=  taORetListCOMPID.AsInteger;
            ExecQuery;

            if FieldByName('SITEMID').IsNull then MessageDialog('������� �� �������', mtInformation, [mbOK], 0)
            else if FieldByName('SITEMID').AsInteger = -1 then
                 MessageDialog('�� ������ ���. ����������', mtInformation, [mbOK], 0)
            else if FieldByName('SITEMID').AsInteger = -2 then
                 MessageDialog('������� ������� ������� ����������', mtInformation, [mbOK], 0)
            else
              begin
                taORet.Append;
                taORetART2ID.AsInteger:=FieldByName('ART2ID').AsInteger;
                taORetUID.AsInteger:=FieldByName('UID').AsInteger;
                taORetW.AsFloat:=FieldByName('W').AsFloat;
                taORetSZ.AsString:=FieldByName('SZ').AsString;
                taORetSPRICE.AsFloat:=FieldByName('SPRICE').AsFloat;
                taORetSDATE.AsDateTime:=FieldByName('SDATE').AsDateTime;
                taORetSSF.AsString:=FieldByName('SSF').AsString;
                taORetSN.AsInteger:=FieldByName('SN').AsInteger;
                taORetNDATE.AsDateTime:=FieldByName('NDATE').AsDateTime;
                taORetNDSID.AsInteger:=FieldByName('NDSID').AsInteger;
                taORetSUPID.AsInteger:=FieldByName('SUPID').AsInteger;
                taORetOPRICE.AsFloat:=FieldByName('OPRICE').AsFloat;
                taORetPRICE2.AsFloat:=FieldByName('PRICE2').AsFloat;
                taORetSINFOID.AsInteger:=FieldByName('SINFOID').AsInteger;
                taORetPRICE0.AsFloat:=FieldByName('PRICE0').AsFloat;
                taORet.Post;
                if quOSelled.Active then
                 if quOSelled.Locate('UID',StrToInt(edUID.Text),[]) then quOSelled.Delete;
             end;
            Close;
          end;
         end; 
        edUID.Text:='';
      end;
 if Key=VK_DOWN then
   if pc1.ActivePage.TabIndex=1 then ActiveControl:= dgSelled
   else ActiveControl:= dgDPrice;
end;

procedure TfmORet.Button1Click(Sender: TObject);
begin
  with dm, dmCOm do
    begin
      Old_D_CompId:=D_CompId;
      Old_D_MatId:=D_MatId;
      Old_D_GoodId:=D_GoodId;
      Old_D_InsId:=D_InsId;
      Old_D_CountryID:=D_CountryID;
    end;
  Screen.Cursor:=crSQLWait;
  pc1Change(nil);
  Screen.Cursor:=crDefault;
end;

procedure TfmORet.deBDChange(Sender: TObject);
begin
  dm.OBeginDate:=deBD.Date;
end;

procedure TfmORet.deEDChange(Sender: TObject);
begin
  dm.OEndDate:=deED.Date;
end;

procedure TfmORet.lcRetClientKeyPress(Sender: TObject;
  var Key: Char);
begin
  case Key of
      #8:
          begin
            lcRetClient.KeyValue:=NULL;
            dm.OptBuyerId:=NULL;
          end;
    end;
end;

procedure TfmORet.lcCompCloseUp(Sender: TObject);
begin
  if (not dm.taORet.IsEmpty) then
    raise Exception.Create('���������� �������� ����������, �.�. ��������� ����� ��������� ������ �������� �� ������ ����������');
  with dm do
    begin
      OptBuyerId:=taORetListCompId.Value;
      lcRetClient.KeyValue:=OptBuyerId;
    end;
end;

procedure TfmORet.lcRetClientCloseUp(Sender: TObject);
begin
  dm.OptBuyerId:=lcRetClient.KeyValue;
end;

procedure TfmORet.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmORet.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmORet.acAddExecute(Sender: TObject);
var
  LogOperationID:string;
begin
  dm.taORetList.Refresh;
  if dm.taORetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
  else begin
  // ��������� ���� ��� ����� ����������
  if dm.taORetListCOMPID.IsNull then
  begin
    raise Exception.Create('�������� ����������');
    ActiveControl := lcComp;
  end;
  with dm, taORet do
    begin
      LogOperationID:=dm3.insert_operation(sLog_AddORet,LogOprIdForm);
      if SellMode then
        begin
          if quOSelledSItemId.IsNull then SysUtils.Abort;
          with dm2.quCheckUIDOSelled, Params do
          begin
            close;
            ByName['AUID'].AsInteger:=quOSelledUID.AsInteger;
            ByName['DEPID'].AsInteger:=taORetListDepId.AsInteger;
            ByName['OPT_BUYER'].AsInteger:=  taORetListCOMPID.AsInteger;
            ExecQuery;

            if FieldByName('SITEMID').IsNull then MessageDialog('������� �� �������', mtInformation, [mbOK], 0)
            else if FieldByName('SITEMID').AsInteger = -1 then
                 MessageDialog('�� ������ ���. ����������', mtInformation, [mbOK], 0)
            else if FieldByName('SITEMID').AsInteger = -2 then
                 MessageDialog('������� ������� ������� ����������', mtInformation, [mbOK], 0)
            else
            begin
              Append;
              taORetART2ID.AsInteger:=quOSelledArt2Id.AsInteger;
              taORetUID.AsInteger:=quOSelledUID.AsInteger;
              taORetW.AsFloat:=quOSelledW.AsFloat;
              taORetSZ.AsString:=quOSelledW.AsString;
              taORetSPRICE.AsFloat:=quOSelledSPrice.AsFloat;
              taORetSDATE.AsDateTime:=quOSelledSDate0.AsDateTime;
              taORetSSF.AsString:=quOSelledSSF0.AsString;;
              taORetSN.AsInteger:=quOSelledSN0.AsInteger;
              taORetNDATE.AsDateTime:=quOSelledNDate0.AsDateTime;
              taORetNDSID.AsInteger:=quOSelledNDSID.AsInteger;
              taORetSUPID.AsInteger:=quOSelledSUPID.AsInteger;
              taORetOPRICE.AsFloat:=quOSelledOPrice.AsFloat;
              taORetPRICE2.AsFloat:=quOSelledPrice2.AsFloat;
              taORetSINFOID.AsInteger:=quOSelledSInfoID.AsInteger;
              taORetPRICE0.AsFloat:=quOSelledOptPrice.AsFloat;
              Post;
              quOSelled.Delete;
            end;
          end;
        end
      else
        begin
          if quDPriceArt2Id.IsNull then SysUtils.Abort;
          Append;
          taORetUID.AsInteger:=dmCom.GetId(17);
          taORetART2ID.AsInteger:=quDPriceArt2Id.AsInteger;
          taORetW.AsFloat:=0;
          taORetSZ.AsString:='-';
          taORetSPRICE.AsFloat:=quDPricePrice.AsFloat;
          taORetSDATE.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime));
          taORetSUPID.Value:=taORetListCompId.AsInteger;
          taORetNDATE.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime));
          taORetOPrice.AsFloat:=quDPriceOptPrice.AsFloat;
          taORetPrice2.AsFloat:=quDPricePrice.AsFloat;
          taORetPrice0.AsFloat:=quDPriceOptPrice.AsFloat;
          Post;
          quOSelled.Delete;
        end;
      dm3.update_operation(LogOperationID)
    end;
  end;  
end;

procedure TfmORet.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(sLog_DelORet,LogOprIdForm);
  dm.taORet.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmORet.acHelpExecute(Sender: TObject);
begin
 Application.HelpContext(100262)
end;

procedure TfmORet.acAddUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taORetListSINVID.IsNull) and
  ((not dm.quOSelledART2ID.IsNull) or (not dm.quDPriceART2ID.IsNull)) and
  (dm.taORetListISCLOSED.AsInteger=0)
end;

procedure TfmORet.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taORetListSINVID.IsNull) and
  (not dm.taORetSITEMID.IsNull) and
  (dm.taORetListISCLOSED.AsInteger=0)
end;

end.


