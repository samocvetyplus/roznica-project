unit Data3;

interface

uses
  Controls, SysUtils, Classes, DB, pFIBDataSet, pFIBQuery, FIBQuery,
  FIBDataSet, pFIBStoredProc, Variants, StrUtils;

type
  Tdm3 = class(TDataModule)
    quAM: TpFIBDataSet;
    quAMAMID: TIntegerField;
    quAMARTID1: TIntegerField;
    quAMARTID2: TIntegerField;
    quAMFULLART1: TFIBStringField;
    quAMFULLART2: TFIBStringField;
    quAMMDATE: TDateTimeField;
    quAMR_DATE: TDateTimeField;
    quAMR_STATE: TSmallintField;
    dsAM: TDataSource;
    quA2M: TpFIBDataSet;
    dsA2M: TDataSource;
    quA2MA2MID: TIntegerField;
    quA2MART2ID1: TIntegerField;
    quA2MART2ID2: TIntegerField;
    quA2MART21: TFIBStringField;
    quA2MART22: TFIBStringField;
    quA2MMDATE: TDateTimeField;
    quA2MR_DATE: TDateTimeField;
    quA2MR_STATE: TSmallintField;
    quA2MFULLART: TFIBStringField;
    quRAM: TpFIBDataSet;
    quRAMAMID: TIntegerField;
    quRA2M: TpFIBDataSet;
    quRA2MA2MID: TIntegerField;
    taUIDStoreList: TpFIBDataSet;
    dsrUIDStoreList: TDataSource;
    taCheckUIDStoreData: TpFIBDataSet;
    dsrCheckUIDStoreData: TDataSource;
    taUIDStoreListSINVID: TIntegerField;
    taUIDStoreListSDATE: TDateTimeField;
    taUIDStoreListNDATE: TDateTimeField;
    taUIDStoreListUSERID: TIntegerField;
    taUIDStoreListSN: TIntegerField;
    taUIDStoreListFIO: TFIBStringField;
    quTmp: TpFIBQuery;
    taUIDStore: TpFIBDataSet;
    dsrUIDStore: TDataSource;
    taUIDStoreSINVID: TIntegerField;
    taUIDStoreARTID: TIntegerField;
    taUIDStoreSZ: TFIBStringField;
    taUIDStoreGR: TFIBStringField;
    taUIDStoreDEPID: TIntegerField;
    taUIDStoreUNITID: TSmallintField;
    taUIDStoreART: TFIBStringField;
    taUIDStoreDEPNAME: TFIBStringField;
    taCheckUIDStoreDataID: TIntegerField;
    taCheckUIDStoreDataUID: TIntegerField;
    taCheckUIDStoreDataDEPID: TIntegerField;
    taCheckUIDStoreDataFLAG: TSmallintField;
    taCheckUIDStoreDataDEPNAME: TFIBStringField;
    taUidItem: TpFIBDataSet;
    dsUidItem: TDataSource;
    taUidItemSINVID: TIntegerField;
    taUidItemARTID: TIntegerField;
    taUidItemSZ: TFIBStringField;
    taUidItemGR: TFIBStringField;
    taUidItemDEPID: TIntegerField;
    taUidItemUNITID: TSmallintField;
    taUidItemENTRYNUM: TSmallintField;
    taUidItemSINVNUM: TSmallintField;
    taUidItemDINVNUMFROM: TSmallintField;
    taUidItemDINVNUM: TSmallintField;
    taUidItemSELLNUM: TSmallintField;
    taUidItemRETNUM: TSmallintField;
    taUidItemOPTSELLNUM: TSmallintField;
    taUidItemOPTRETNUM: TSmallintField;
    taUidItemSINVRETNUM: TSmallintField;
    taUidItemRESNUM: TSmallintField;
    taUidItemF: TIntegerField;
    taUidItemART: TFIBStringField;
    taUidItemDEPNAME: TFIBStringField;
    taUidItemUID: TIntegerField;
    quUidRepeat: TpFIBDataSet;
    dsUidRepeat: TDataSource;
    quUidRepeatUID: TIntegerField;
    quUidRepeatCOUNT: TIntegerField;
    taUidItemW: TFloatField;
    taUidItemQ0: TFloatField;
    quUIDChg: TpFIBDataSet;
    dsUIDChg: TDataSource;
    quDetalChg: TpFIBDataSet;
    quUIDChgUID: TIntegerField;
    quUIDChgDISCRIP: TFIBStringField;
    quUIDChgCH_DATE: TDateTimeField;
    quDetalChgNEWID: TIntegerField;
    quDetalChgOLDID: TIntegerField;
    dsUIDDetalChg: TDataSource;
    quDetalChgDEPID: TIntegerField;
    quUIDChgNEWVAL: TFIBStringField;
    quUIDChgOLDVAL: TFIBStringField;
    quUIDChgISNEW: TFIBStringField;
    quDetalChgTABLENAME: TFIBStringField;
    quDetalChgOLDPRICE: TFloatField;
    quDetalChgNEWPRICE: TFloatField;
    quUIDChgNEWVALID: TIntegerField;
    quUIDChgOLDVALID: TIntegerField;
    taUIDStoreENTRYNUM: TFIBBCDField;
    taUIDStoreSINVNUM: TFIBBCDField;
    taUIDStoreDINVNUMFROM: TFIBBCDField;
    taUIDStoreDINVNUM: TFIBBCDField;
    taUIDStoreSELLNUM: TFIBBCDField;
    taUIDStoreOPTSELLNUM: TFIBBCDField;
    taUIDStoreOPTRETNUM: TFIBBCDField;
    taUIDStoreSINVRETNUM: TFIBBCDField;
    taUIDStoreRETNUM: TFIBBCDField;
    taUIDStoreRESNUM: TFIBBCDField;
    taUIDStoreF: TFIBBCDField;
    quTmpLog: TpFIBQuery;
    quGetSmallestSell: TpFIBQuery;
    taUIDStore_Detail: TpFIBDataSet;
    dsrUIDStore_Detail: TDataSource;
    taUIDStore_DetailUIDSTOREID: TFIBIntegerField;
    taUIDStore_DetailUID: TFIBIntegerField;
    taUIDStore_DetailRESNUM: TFIBSmallIntField;
    taUIDStore_DetailF: TFIBIntegerField;
    taUidItemUIDSTOREID: TFIBIntegerField;
    taUIDStore_DetailENTRYNUM: TFIBSmallIntField;
    taUIDStore_DetailSINVNUM: TFIBSmallIntField;
    taUIDStore_DetailDINVNUMFROM: TFIBSmallIntField;
    taUIDStore_DetailDINVNUM: TFIBSmallIntField;
    taUIDStore_DetailSELLNUM: TFIBSmallIntField;
    taUIDStore_DetailRETNUM: TFIBSmallIntField;
    taUIDStore_DetailOPTSELLNUM: TFIBSmallIntField;
    taUIDStore_DetailOPTRETNUM: TFIBSmallIntField;
    taUIDStore_DetailSINVRETNUM: TFIBSmallIntField;
    quSelectLog: TpFIBDataSet;
    dsSelectLog: TDataSource;
    quSetLogRState: TpFIBQuery;
    quDest: TpFIBQuery;
    quEditArt: TpFIBDataSet;
    quEditArt2: TpFIBDataSet;
    quEditIns: TpFIBDataSet;
    dsEditArt: TDataSource;
    dsEditArt2: TDataSource;
    dsEditIns: TDataSource;
    quEditArtAHSTID: TFIBIntegerField;
    quEditArtD_ARTID: TFIBIntegerField;
    quEditArtD_COMPID: TFIBIntegerField;
    quEditArtD_MATID: TFIBStringField;
    quEditArtD_GOODID: TFIBStringField;
    quEditArtART: TFIBStringField;
    quEditArtUNITID: TFIBIntegerField;
    quEditArtD_INSID: TFIBStringField;
    quEditArtADATE: TFIBDateTimeField;
    quEditArtOLD_D_COMPID: TFIBIntegerField;
    quEditArtOLD_D_MATID: TFIBStringField;
    quEditArtOLD_D_GOODID: TFIBStringField;
    quEditArtOLD_ART: TFIBStringField;
    quEditArtOLD_UNITID: TFIBSmallIntField;
    quEditArtOLD_D_INSID: TFIBStringField;
    quEditArtD_COUNTRYID: TFIBStringField;
    quEditArtOLD_D_COUNTRYID: TFIBStringField;
    quEditArtATT1ID: TFIBIntegerField;
    quEditArtATT2ID: TFIBIntegerField;
    quEditArtOLD_ATT1ID: TFIBIntegerField;
    quEditArtOLD_ATT2ID: TFIBIntegerField;
    quEditArtFULLART: TFIBStringField;
    quEditArtCOMP: TFIBStringField;
    quEditArtOLD_COMP: TFIBStringField;
    quEditArt2A2HIST_ID: TFIBIntegerField;
    quEditArt2ART2ID: TFIBIntegerField;
    quEditArt2ART2: TFIBStringField;
    quEditArt2ADATE: TFIBDateTimeField;
    quEditArt2OLD_ART2: TFIBStringField;
    quEditArt2FULLART: TFIBStringField;
    quEditArt2ART: TFIBStringField;
    quEditInsINSHIST_ID: TFIBIntegerField;
    quEditInsINSID: TFIBIntegerField;
    quEditInsART2ID: TFIBIntegerField;
    quEditInsD_INSID: TFIBStringField;
    quEditInsQUANTITY: TFIBSmallIntField;
    quEditInsWEIGHT: TFIBFloatField;
    quEditInsCOLOR: TFIBStringField;
    quEditInsCHROMATICITY: TFIBStringField;
    quEditInsCLEANNES: TFIBStringField;
    quEditInsGR: TFIBStringField;
    quEditInsSHAPE: TFIBStringField;
    quEditInsEDGETION: TFIBStringField;
    quEditInsMAIN: TFIBSmallIntField;
    quEditInsEDGTYPEID: TFIBStringField;
    quEditInsEDGSHAPEID: TFIBStringField;
    quEditInsADATE: TFIBDateTimeField;
    quEditInsOLD_ART2ID: TFIBIntegerField;
    quEditInsOLD_D_INSID: TFIBStringField;
    quEditInsOLD_QUANTITY: TFIBSmallIntField;
    quEditInsOLD_WEIGHT: TFIBFloatField;
    quEditInsOLD_COLOR: TFIBStringField;
    quEditInsOLD_CHROMATICITY: TFIBStringField;
    quEditInsOLD_CLEANNES: TFIBStringField;
    quEditInsOLD_GR: TFIBStringField;
    quEditInsOLD_SHAPE: TFIBStringField;
    quEditInsOLD_EDGETION: TFIBStringField;
    quEditInsOLD_MAIN: TFIBSmallIntField;
    quEditInsOLD_EDGTYPEID: TFIBStringField;
    quEditInsOLD_EDGSHAPEID: TFIBStringField;
    quEditInsSTATE: TFIBSmallIntField;
    quEditInsISPRORD: TFIBSmallIntField;
    quEditInsART: TFIBStringField;
    quEditInsFULLART: TFIBStringField;
    quEditArtATT1: TFIBStringField;
    quEditArtOLD_ATT1: TFIBStringField;
    quEditArtATT2: TFIBStringField;
    quEditArtOLD_ATT2: TFIBStringField;
    quEditArtUNIT: TStringField;
    quEditArtOLD_UINT: TStringField;
    quEditInsART2: TFIBStringField;
    quEditInsOperation: TStringField;
    quEditInsWhere_: TStringField;
    quEditInsOLD_ART2: TFIBStringField;
    quCompD_art: TpFIBDataSet;
    quCompSinfo: TpFIBDataSet;
    quCompSinv: TpFIBDataSet;
    dsCompD_art: TDataSource;
    dsCompSinfo: TDataSource;
    dsCompSinv: TDataSource;
    dsCompArt2: TDataSource;
    quCompArt2: TpFIBDataSet;
    quCompD_artD_ARTID: TFIBIntegerField;
    quCompD_artART: TFIBStringField;
    quCompD_artD_MATID: TFIBStringField;
    quCompD_artD_GOODID: TFIBStringField;
    quCompD_artD_INSID: TFIBStringField;
    quCompD_artUNITID: TFIBIntegerField;
    quCompD_artD_COMPID: TFIBIntegerField;
    quCompSinfoSINFOID: TFIBIntegerField;
    quCompSinfoSN: TFIBIntegerField;
    quCompSinfoSINVID: TFIBIntegerField;
    quCompSinfoSELID: TFIBIntegerField;
    quCompSinfoSSF: TFIBStringField;
    quCompSinfoSDATE: TFIBDateTimeField;
    quCompSinfoNDATE: TFIBDateTimeField;
    quCompSinvSINVID: TFIBIntegerField;
    quCompSinvSN: TFIBIntegerField;
    quCompSinvSDATE: TFIBDateTimeField;
    quCompSinvNDATE: TFIBDateTimeField;
    quCompSinvSSF: TFIBStringField;
    quCompArt2ART2ID: TFIBIntegerField;
    quCompArt2ART: TFIBStringField;
    quCompArt2ART2: TFIBStringField;
    quCompArt2D_COMPID: TFIBIntegerField;
    quCompArt2SUPID: TFIBIntegerField;
    quApplDepList: TpFIBDataSet;
    dsApplDepList: TDataSource;
    quApplDepListSINVID: TFIBIntegerField;
    quApplDepListDEPID: TFIBIntegerField;
    quApplDepListSDATE: TFIBDateTimeField;
    quApplDepListUSERID: TFIBIntegerField;
    quApplDepListSNAME: TFIBStringField;
    quApplDepListCOLOR: TFIBIntegerField;
    quApplDepListFIO: TFIBStringField;
    quApplDepListC: TFIBIntegerField;
    quApplDepListW: TFIBFloatField;
    quApplDepItem: TpFIBDataSet;
    dsApplDepItem: TDataSource;
    quApplDepItemAPPLDEPID: TFIBIntegerField;
    quApplDepItemUID: TFIBIntegerField;
    quApplDepItemW: TFIBFloatField;
    quApplDepItemSZ: TFIBStringField;
    quApplDepItemSINVID: TFIBIntegerField;
    quApplDepItemART2ID: TFIBIntegerField;
    quApplDepItemART: TFIBStringField;
    quApplDepItemPRODCODE: TFIBStringField;
    quApplDepItemD_GOODID: TFIBStringField;
    quApplDepItemD_INSID: TFIBStringField;
    quApplDepItemD_MATID: TFIBStringField;
    quApplDepItemART2: TFIBStringField;
    quApplDepArt: TpFIBDataSet;
    dsApplDepArt: TDataSource;
    quApplDepArtUID: TFIBIntegerField;
    quApplDepArtW: TFIBFloatField;
    quApplDepArtSZ: TFIBStringField;
    quApplDepArtAPPLDEPID: TFIBIntegerField;
    quApplDepArtART2: TFIBStringField;
    quSelectUid: TpFIBDataSet;
    quSelectUidSITEMID: TFIBIntegerField;
    quSelectUidUID: TFIBIntegerField;
    quSelectUidSZ: TFIBStringField;
    quSelectUidW: TFIBFloatField;
    quSelectUidART2: TFIBStringField;
    dsSelectUid: TDataSource;
    quSelectUidART2ID: TFIBIntegerField;
    quApplDepArtSINVID: TFIBIntegerField;
    quApplDepArtART2ID: TFIBIntegerField;
    quSelectUidSINVID: TFIBIntegerField;
    quApplDepListCOST: TFIBFloatField;
    quApplDepItemCOST: TFIBFloatField;
    quSelectUidCOST: TFIBFloatField;
    quApplDepArtCOST: TFIBFloatField;
    quApplDepListCOSTP: TFIBFloatField;
    quApplDepItemCOSTP: TFIBFloatField;
    quApplDepArtCOSTP: TFIBFloatField;
    quSelectUidCOSTP: TFIBFloatField;
    quSelectLogL_USERID: TFIBIntegerField;
    quRepSetting: TpFIBDataSet;
    dsRepSetting: TDataSource;
    quRepSettingD_DEPID: TFIBIntegerField;
    quRepSettingNAME: TFIBStringField;
    quRepSettingCOLOR: TFIBIntegerField;
    quRepSettingR_CODE: TFIBStringField;
    quRepSettingR_GENOFFSET: TFIBIntegerField;
    quRepSettingR_USERDEP: TFIBSmallIntField;
    quRepSettingISFTPREP: TFIBSmallIntField;
    quRepSettingHERE: TFIBSmallIntField;
    quRepSettingUSEDEP: TFIBSmallIntField;
    quRepSettingISRECORD: TFIBSmallIntField;
    quGenSetting: TpFIBQuery;
    taCheckUIDStoreDataWHCOST: TFIBFloatField;
    taCheckUIDStoreDataUIDSTORECOST: TFIBFloatField;
    taUIDStoreENTRYCOST: TFIBFloatField;
    taUIDStoreSINVCOST: TFIBFloatField;
    taUIDStoreDINVCOST: TFIBFloatField;
    taUIDStoreDINVFROMCOST: TFIBFloatField;
    taUIDStoreSELLCOST: TFIBFloatField;
    taUIDStoreRETCOST: TFIBFloatField;
    taUIDStoreOPTSELLCOST: TFIBFloatField;
    taUIDStoreOPTRETCOST: TFIBFloatField;
    taUIDStoreSINVRETCOST: TFIBFloatField;
    taUIDStoreRESCOST: TFIBFloatField;
    taUIDStoreFCOST: TFIBFloatField;
    taUidItemENTRYCOST: TFIBFloatField;
    taUidItemSINVCOST: TFIBFloatField;
    taUidItemDINVCOST: TFIBFloatField;
    taUidItemDINVFROMCOST: TFIBFloatField;
    taUidItemSELLCOST: TFIBFloatField;
    taUidItemRETCOST: TFIBFloatField;
    taUidItemOPTSELLCOST: TFIBFloatField;
    taUidItemOPTRETCOST: TFIBFloatField;
    taUidItemSINVRETCOST: TFIBFloatField;
    taUidItemRESCOST: TFIBFloatField;
    taUidItemFCOST: TFIBFloatField;
    quActAllowancesList: TpFIBDataSet;
    dsActAllowancesList: TDataSource;
    quActAllowancesListSINVID: TFIBIntegerField;
    quActAllowancesListDEPFROMID: TFIBIntegerField;
    quActAllowancesListNDATE: TFIBDateTimeField;
    quActAllowancesListSDATE: TFIBDateTimeField;
    quActAllowancesListSN: TFIBIntegerField;
    quActAllowancesListISCLOSED: TFIBSmallIntField;
    quActAllowancesListITYPE: TFIBSmallIntField;
    quActAllowancesListUSERID: TFIBIntegerField;
    quActAllowancesListCRUSERID: TFIBIntegerField;
    quActAllowancesListUSERNAME: TFIBStringField;
    quActAllowancesListCRUSERNAME: TFIBStringField;
    quActAllowancesListDEPNAME: TFIBStringField;
    quActAllowancesListCOLOR: TFIBIntegerField;
    quActAllowancesListCOSTP: TFIBFloatField;
    quActAllowancesListCOST: TFIBFloatField;
    quActAllowancesListQ: TFIBIntegerField;
    quActAllowancesListW: TFIBFloatField;
    quActAllowancesListOpenCostP: TFloatField;
    quActAllowancesListCloseCostP: TFloatField;
    quActAllowancesListOpenCost: TFloatField;
    quActAllowancesListCloseCost: TFloatField;
    quActAllowancesListOpenQ: TIntegerField;
    quActAllowancesListCloseQ: TIntegerField;
    quActAllowancesListOpenW: TFloatField;
    quActAllowancesListCloseW: TFloatField;
    quActAllowancesListOpenSn: TIntegerField;
    quActAllowancesListCloseSn: TIntegerField;
    quActAllowances: TpFIBDataSet;
    dsActAllowances: TDataSource;
    quActAllowancesSITEMID: TFIBIntegerField;
    quActAllowancesSELID: TFIBIntegerField;
    quActAllowancesPRODCODE: TFIBStringField;
    quActAllowancesD_MATID: TFIBStringField;
    quActAllowancesD_GOODID: TFIBStringField;
    quActAllowancesD_INSID: TFIBStringField;
    quActAllowancesART: TFIBStringField;
    quActAllowancesART2: TFIBStringField;
    quActAllowancesUID: TFIBIntegerField;
    quActAllowancesW: TFIBFloatField;
    quActAllowancesSZ: TFIBStringField;
    quActAllowancesSPRICE: TFIBFloatField;
    quActAllowancesPRICE2: TFIBFloatField;
    quActAllowancesCOST: TFIBFloatField;
    quActAllowancesSCOST: TFIBFloatField;
    taUIDStoreListCRDATE: TFIBDateTimeField;
    dsDInvCheck: TDataSource;
    quDInvCheck: TpFIBDataSet;
    quDInvCheckO_ID: TFIBIntegerField;
    quDInvCheckO_UID: TFIBIntegerField;
    quDInvCheckO_COMP: TFIBStringField;
    quDInvCheckO_GOODS: TFIBStringField;
    quDInvCheckO_MAT: TFIBStringField;
    quDInvCheckO_INS: TFIBStringField;
    quDInvCheckO_UNIT: TFIBStringField;
    quDInvCheckO_ART: TFIBStringField;
    quDInvCheckO_W: TFIBFloatField;
    quDInvCheckO_SZ: TFIBStringField;
    quDInvCheckO_PRICE: TFIBFloatField;
    quDInvCheckO_STATUS: TFIBIntegerField;
    taUIDStoreACTALLOWANCESNUM: TFIBBCDField;
    taUIDStoreACTALLOWANCESCOST: TFIBFloatField;
    taUidItemACTALLOWANCESNUM: TFIBIntegerField;
    taUidItemACTALLOWANCESCOST: TFIBFloatField;
    quActAllowParamPrn: TpFIBDataSet;
    dsActAllowParamPrn: TDataSource;
    quApplDepListDEPFROMID: TFIBIntegerField;
    quApplDepListSNAMEFROM: TFIBStringField;
    quApplDepListCOLORDEPFROM: TFIBIntegerField;
    quApplDepListCRUSERID: TFIBIntegerField;
    quApplDepListCRUSERNAME: TFIBStringField;
    quApplDepListISCLOSED: TFIBIntegerField;
    quApplDepListSN: TFIBIntegerField;
    quApplDepListMODEID: TFIBIntegerField;
    quApplDepListITYPE: TFIBIntegerField;
    quApplDepListSELLID: TFIBIntegerField;
    quDetalChgSNAME: TFIBStringField;
    quDetalChgCOLOR: TFIBIntegerField;
    quApplDepListCOMMENT: TFIBStringField;
    quApplDepItemD_ARTID: TFIBIntegerField;
    quSelectUidFEXISTS: TFIBSmallIntField;
    dsWriteoff_Param: TDataSource;
    quWriteoff_Param: TpFIBDataSet;
    quWriteoff_ParamID: TFIBIntegerField;
    quActAllowParamPrnActName: TStringField;
    quActAllowParamPrnACTNAME_ID: TFIBIntegerField;
    quActAllowParamPrnWRITEOFFREASON_ID: TFIBIntegerField;
    quActAllowParamPrnDEFECTDISCRIPT_ID: TFIBIntegerField;
    quActAllowParamPrnSPOILINGREASON_ID: TFIBIntegerField;
    quActAllowParamPrnSOLUTION_ID: TFIBIntegerField;
    quActAllowParamPrnWriteOffReason: TStringField;
    quActAllowParamPrnDefectDiscript: TStringField;
    quActAllowParamPrnspoilingreason: TStringField;
    quActAllowParamPrnSolution: TStringField;
    quActAllowParamPrnSINVID: TFIBIntegerField;
    quD_writeoff: TpFIBDataSet;
    dsD_writeoff: TDataSource;
    quWriteoff_ParamDISCRIP: TFIBStringField;
    quD_writeoffID: TFIBIntegerField;
    quD_writeoffDISCRIP: TFIBStringField;
    dsRecipGift: TDataSource;
    quReciepGift: TpFIBDataSet;
    quReciepGiftRECIEPCOMPANY: TFIBStringField;
    quReciepGiftRECIEPADDRESS: TFIBStringField;
    quSurPlusItem: TpFIBDataSet;
    quSurPlusItemUID: TFIBIntegerField;
    quSurPlusItemSITEMID: TFIBIntegerField;
    quSurPlusItemSELID: TFIBIntegerField;
    quSurPlusItemART2ID: TFIBIntegerField;
    quSurPlusItemPRICE: TFIBFloatField;
    quSurPlusItemPRICE2: TFIBFloatField;
    quSurPlusItemUNITID: TFIBIntegerField;
    quSurPlusItemART: TFIBStringField;
    quSurPlusItemART2: TFIBStringField;
    quSurPlusItemPRODCODE: TFIBStringField;
    quSurPlusItemD_MATID: TFIBStringField;
    quSurPlusItemD_GOODID: TFIBStringField;
    quSurPlusItemD_INSID: TFIBStringField;
    quSurPlusItemSZ: TFIBStringField;
    quSurPlusItemW: TFIBFloatField;
    quSurPlusItemQ0: TFIBFloatField;
    quSurPlusItemD_COUNTRYID: TFIBStringField;
    quSurPlusItemSCost: TFloatField;
    quSurPlusItemCost2: TFloatField;
    quSurPlusItemUnit: TStringField;
    quSurPlusItemRno: TIntegerField;
    dsSurPlusItem: TDataSource;
    quActAllowancesListDESCRIP: TFIBStringField;
    quActAllowancesListREF_SINVID: TFIBIntegerField;
    taUIDStoreSURPLUSNUM: TFIBBCDField;
    taUIDStoreSURPLUSCOST: TFIBFloatField;
    taUidItemSURPLUSNUM: TFIBIntegerField;
    taUidItemSURPLUSCOST: TFIBFloatField;
    quActAllowancesListSDATE1: TFIBDateTimeField;
    quApplDepListEDITTR: TFIBSmallIntField;
    quUIDChgNEWPRICE: TFIBFloatField;
    quUIDChgOLDPRICE: TFIBFloatField;
    quEquipment: TpFIBDataSet;
    dsEquipment: TDataSource;
    quEquipmentSINVID: TFIBIntegerField;
    quEquipmentSUPID: TFIBIntegerField;
    quEquipmentSUP: TFIBStringField;
    quEquipmentDEPID: TFIBIntegerField;
    quEquipmentDEPNAME: TFIBStringField;
    quEquipmentSDATE: TFIBDateTimeField;
    quEquipmentNDATE: TFIBDateTimeField;
    quEquipmentSSF: TFIBStringField;
    quEquipmentSN: TFIBIntegerField;
    quEquipmentISCLOSED: TFIBSmallIntField;
    quEquipmentITYPE: TFIBIntegerField;
    quEquipmentUSERID: TFIBIntegerField;
    quEquipmentCLOSEUSER: TFIBStringField;
    quEquipmentCRUSERID: TFIBIntegerField;
    quEquipmentCREATEUSER: TFIBStringField;
    quEquipmentCOST: TFIBFloatField;
    quEquipmentCOLOR: TFIBIntegerField;
    quActAllowancesSDATE: TFIBDateTimeField;
    taUidStoreDepItem: TpFIBDataSet;
    taUidStoreDepItemUIDSTOREDEPID: TFIBIntegerField;
    taUidStoreDepItemSINVID: TFIBIntegerField;
    taUidStoreDepItemUID: TFIBIntegerField;
    taUidStoreDepItemARTID: TFIBIntegerField;
    taUidStoreDepItemART2ID: TFIBIntegerField;
    taUidStoreDepItemSZ: TFIBStringField;
    taUidStoreDepItemGR: TFIBStringField;
    taUidStoreDepItemDEPID: TFIBIntegerField;
    taUidStoreDepItemUNITID: TFIBSmallIntField;
    taUidStoreDepItemQ0: TFIBFloatField;
    taUidStoreDepItemW: TFIBFloatField;
    taUidStoreDepItemENTRYNUM: TFIBSmallIntField;
    taUidStoreDepItemDINVNUMFROM: TFIBSmallIntField;
    taUidStoreDepItemDINVNUM: TFIBSmallIntField;
    taUidStoreDepItemSELLNUM: TFIBSmallIntField;
    taUidStoreDepItemRETNUM: TFIBSmallIntField;
    taUidStoreDepItemRESNUM: TFIBSmallIntField;
    taUidStoreDepItemENTRYCOST: TFIBFloatField;
    taUidStoreDepItemDINVCOST: TFIBFloatField;
    taUidStoreDepItemDINVFROMCOST: TFIBFloatField;
    taUidStoreDepItemSELLCOST: TFIBFloatField;
    taUidStoreDepItemRETCOST: TFIBFloatField;
    taUidStoreDepItemRESCOST: TFIBFloatField;
    taUidStoreDepItemSELLCOST2: TFIBFloatField;
    taUidStoreDepItemRETCOST2: TFIBFloatField;
    taUidStoreDepItemACTALLOWANCESNUM: TFIBIntegerField;
    taUidStoreDepItemACTALLOWANCESCOST: TFIBFloatField;
    taUidStoreDepItemENTRYNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemDINVNUMFROMCOM: TFIBSmallIntField;
    taUidStoreDepItemDINVNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemSELLNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemRETNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemRESNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemENTRYCOSTCOM: TFIBFloatField;
    taUidStoreDepItemDINVCOSTCOM: TFIBFloatField;
    taUidStoreDepItemDINVFROMCOSTCOM: TFIBFloatField;
    taUidStoreDepItemSELLCOSTCOM: TFIBFloatField;
    taUidStoreDepItemRETCOSTCOM: TFIBFloatField;
    taUidStoreDepItemRESCOSTCOM: TFIBFloatField;
    taUidStoreDepItemSELLCOST2COM: TFIBFloatField;
    taUidStoreDepItemRETCOST2COM: TFIBFloatField;
    taUidStoreDepItemACTALLOWANCESNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemACTALLOWANCESCOSTCOM: TFIBFloatField;
    taUidStoreDepItemACTDISCOUNTSELLNUM: TFIBSmallIntField;
    taUidStoreDepItemACTDISCOUNTRETNUM: TFIBSmallIntField;
    taUidStoreDepItemACTDISCOUNTSELLCOST: TFIBFloatField;
    taUidStoreDepItemACTDISCOUNTRETCOST: TFIBFloatField;
    taUidStoreDepItemACTNUM: TFIBSmallIntField;
    taUidStoreDepItemACTCOST: TFIBFloatField;
    taUidStoreDepItemACTDISCOUNTSELLNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemACTDISCOUNTRETNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemACTDISCOUNTSELLCOSTCOM: TFIBFloatField;
    taUidStoreDepItemACTDISCOUNTRETCOSTCOM: TFIBFloatField;
    taUidStoreDepItemACTNUMCOM: TFIBSmallIntField;
    taUidStoreDepItemACTCOSTCOM: TFIBFloatField;
    taUidStoreDepItemART: TFIBStringField;
    taUidStoreDepItemUNIT: TStringField;
    dsUidStoreDepItem: TDataSource;
    taUidItemENTRYNUMCOM: TFIBSmallIntField;
    taUidItemSINVNUMCOM: TFIBSmallIntField;
    taUidItemDINVNUMFROMCOM: TFIBSmallIntField;
    taUidItemDINVNUMCOM: TFIBSmallIntField;
    taUidItemSELLNUMCOM: TFIBSmallIntField;
    taUidItemRETNUMCOM: TFIBSmallIntField;
    taUidItemOPTSELLNUMCOM: TFIBSmallIntField;
    taUidItemOPTRETNUMCOM: TFIBSmallIntField;
    taUidItemSINVRETNUMCOM: TFIBSmallIntField;
    taUidItemRESNUMCOM: TFIBSmallIntField;
    taUidItemENTRYCOSTCOM: TFIBFloatField;
    taUidItemSINVCOSTCOM: TFIBFloatField;
    taUidItemDINVCOSTCOM: TFIBFloatField;
    taUidItemSELLCOSTCOM: TFIBFloatField;
    taUidItemRETCOSTCOM: TFIBFloatField;
    taUidItemOPTSELLCOSTCOM: TFIBFloatField;
    taUidItemOPTRETCOSTCOM: TFIBFloatField;
    taUidItemSINVRETCOSTCOM: TFIBFloatField;
    taUidItemRESCOSTCOM: TFIBFloatField;
    taUidItemSELLCOST2COM: TFIBFloatField;
    taUidItemRETCOST2COM: TFIBFloatField;
    taUidItemACTALLOWANCESNUMCOM: TFIBSmallIntField;
    taUidItemACTALLOWANCESCOSTCOM: TFIBFloatField;
    quApplDepListRSTATE_S: TFIBStringField;
    quApplDepItemREFUSAL: TFIBSmallIntField;
    quCompSinvITYPE: TFIBSmallIntField;
    quCompSinvTypeSinv: TStringField;
    quCompBalans: TpFIBDataSet;
    dsCompBalans: TDataSource;
    quCompBalansID: TFIBIntegerField;
    quCompBalansPAY: TFIBFloatField;
    quCompBalansCOMPID: TFIBIntegerField;
    quCompBalansPDATE: TFIBDateTimeField;
    quCompBalansDESCRIPTON: TFIBStringField;
    quCompRemains: TpFIBDataSet;
    dsCompRemains: TDataSource;
    quCompRemainsID: TFIBIntegerField;
    quCompRemainsART2ID: TFIBIntegerField;
    quCompRemainsFULLART: TFIBStringField;
    taRespStoring: TpFIBDataSet;
    dsRespStoring: TDataSource;
    taRespStoringRN: TFIBIntegerField;
    taRespStoringADATE: TFIBDateTimeField;
    taRespStoringPRODCODE: TFIBStringField;
    taRespStoringART: TFIBStringField;
    taRespStoringART2: TFIBStringField;
    taRespStoringD_GOODID: TFIBStringField;
    taRespStoringD_INSID: TFIBStringField;
    taRespStoringD_MATID: TFIBStringField;
    taRespStoringD_COUNTRYID: TFIBStringField;
    taRespStoringUID: TFIBIntegerField;
    taRespStoringSTATEUID: TFIBIntegerField;
    taRespStoringUSERID: TFIBIntegerField;
    taRespStoringFIO: TFIBStringField;
    taRespStoringCONFIRM: TFIBSmallIntField;
    taRespStoringRETCLIENT: TFIBStringField;
    taRespStoringRETADDRESS: TFIBStringField;
    taRespStoringRET: TFIBStringField;
    taRespStoringDATERESP: TFIBDateTimeField;
    taRespStoringRESPSTORINGID: TFIBIntegerField;
    taRespStoringDEPID: TFIBIntegerField;
    taRespStoringSNAME: TFIBStringField;
    taRespStoringCOLOR: TFIBIntegerField;
    taRespStoringPRICE: TFIBFloatField;
    taRespStoringPRICE0: TFIBFloatField;
    taRespStoringCOST: TFloatField;
    taRespStoringCOST0: TFloatField;
    taRespStoringUNIT: TStringField;
    taRespStoringSate: TStringField;
    taRespStoringQ0: TFIBFloatField;
    taRespStoringSZ: TFIBStringField;
    taRespStoringW: TFIBFloatField;
    taRespStoringSCOST0: TFloatField;
    taRespStoringSCOST1: TFloatField;
    taRespStoringSCOST2: TFloatField;
    taRespStoringSCOST3: TFloatField;
    taRespStoringSCOST00: TFloatField;
    taRespStoringSCOST01: TFloatField;
    taRespStoringSCOST02: TFloatField;
    taRespStoringSCOST03: TFloatField;
    taRespStoringSQ0: TIntegerField;
    taRespStoringSQ1: TIntegerField;
    taRespStoringSQ2: TIntegerField;
    taRespStoringSQ3: TIntegerField;
    taRespStoringSW0: TFloatField;
    taRespStoringSW1: TFloatField;
    taRespStoringSW2: TFloatField;
    taRespStoringSW3: TFloatField;
    taRepairList: TpFIBDataSet;
    dsRepairList: TDataSource;
    taRepairListSINVID: TFIBIntegerField;
    taRepairListDEPFROMID: TFIBIntegerField;
    taRepairListSDATE: TFIBDateTimeField;
    taRepairListSN: TFIBIntegerField;
    taRepairListPMARGIN: TFIBFloatField;
    taRepairListTR: TFIBFloatField;
    taRepairListCOST: TFIBFloatField;
    taRepairListR_COUNT: TFIBIntegerField;
    taRepairListAKCIZ: TFIBFloatField;
    taRepairListISCLOSED: TFIBSmallIntField;
    taRepairListDEPFROM: TFIBStringField;
    taRepairListCOMPID: TFIBIntegerField;
    taRepairListCOMP: TFIBStringField;
    taRepairListUSERID: TFIBIntegerField;
    taRepairListFIO: TFIBStringField;
    taRepairListW: TFIBFloatField;
    taRepairListPTR: TFIBFloatField;
    taRepairListPTRNDS: TFIBFloatField;
    taRepairListTRNDS: TFIBFloatField;
    taRepairListRET_NONDS: TFIBSmallIntField;
    taRepairListSCOSTOPEN: TFloatField;
    taRepairListSCOSTCLOSED: TFloatField;
    taRepairListSTROPEN: TFloatField;
    taRepairListSTRCLOSED: TFloatField;
    taRepairListCOUNTOPEN: TIntegerField;
    taRepairListCOUNTCLOSED: TIntegerField;
    taRepairListSWOPEN: TFloatField;
    taRepairListSWCLOSED: TFloatField;
    taRepairListSCOST1: TFloatField;
    taRepairListSTR1: TFloatField;
    taRepairListCOUNT1: TIntegerField;
    taRepairListSW1: TFloatField;
    taRepairListSQOPEN: TIntegerField;
    taRepairListSQCLOSED: TIntegerField;
    taRepairListSQ1: TIntegerField;
    taRepair: TpFIBDataSet;
    dsRepair: TDataSource;
    taRepairREPAIRID: TFIBIntegerField;
    taRepairSINVID: TFIBIntegerField;
    taRepairART2ID: TFIBIntegerField;
    taRepairSITEMID: TFIBIntegerField;
    taRepairUID: TFIBIntegerField;
    taRepairW: TFIBFloatField;
    taRepairSZ: TFIBStringField;
    taRepairQ0: TFIBFloatField;
    taRepairSPRICE: TFIBFloatField;
    taRepairCOST: TFIBFloatField;
    taRepairSDATE: TFIBDateTimeField;
    taRepairSUPID: TFIBIntegerField;
    taRepairSSF: TFIBStringField;
    taRepairSN: TFIBIntegerField;
    taRepairNDATE: TFIBDateTimeField;
    taRepairNDSID: TFIBIntegerField;
    taRepairPRODID: TFIBIntegerField;
    taRepairPROD: TFIBStringField;
    taRepairGOODID: TFIBStringField;
    taRepairINSID: TFIBStringField;
    taRepairMATID: TFIBStringField;
    taRepairART: TFIBStringField;
    taRepairART2: TFIBStringField;
    taRepairSUP: TFIBStringField;
    taRepairUNITID: TFIBSmallIntField;
    taRepairD_RETID: TFIBStringField;
    taRepairUNIT: TStringField;
    taRepairRET: TStringField;
    taRepairRESPSTORINGID: TFIBIntegerField;
    taRespStoringDATENRESP: TFIBDateTimeField;
    taSuspItemList: TpFIBDataSet;
    taSuspItemListSINVID: TFIBIntegerField;
    taSuspItemListSN: TFIBIntegerField;
    taSuspItemListDEPID: TFIBIntegerField;
    taSuspItemListITYPE: TFIBIntegerField;
    taSuspItemListUSERID: TFIBIntegerField;
    taSuspItemListRSTATE: TFIBIntegerField;
    taSuspItemListFIO: TFIBStringField;
    taSuspItemListSNAME: TFIBStringField;
    taSuspItemListCOST: TFIBFloatField;
    taSuspItemListW: TFIBFloatField;
    taSuspItemListQ: TFIBIntegerField;
    taSuspItemListDEPCOLOR: TFIBIntegerField;
    dsSuspItemList: TDataSource;
    taSuspItemListState: TStringField;
    taSuspItemListSDATE: TFIBDateTimeField;
    taSuspItem: TpFIBDataSet;
    taSuspItemSITEMID: TFIBIntegerField;
    taSuspItemUID: TFIBIntegerField;
    taSuspItemSZ: TFIBStringField;
    taSuspItemW: TFIBFloatField;
    taSuspItemUNIT: TFIBStringField;
    taSuspItemSPRICE: TFIBFloatField;
    taSuspItemPRICE2: TFIBFloatField;
    taSuspItemSCOST: TFIBFloatField;
    taSuspItemCOST2: TFIBFloatField;
    taSuspItemART2ID: TFIBIntegerField;
    taSuspItemPRORDCODE: TFIBStringField;
    taSuspItemD_MATID: TFIBStringField;
    taSuspItemD_GOODID: TFIBStringField;
    taSuspItemD_INSID: TFIBStringField;
    taSuspItemARTID: TFIBIntegerField;
    taSuspItemD_COUNTRYID: TFIBStringField;
    dsSuspItem: TDataSource;
    taSuspItemART: TFIBStringField;
    taSuspItemART2: TFIBStringField;
    taUSerUidWh: TpFIBDataSet;
    taUSerUidWhUSERID: TFIBIntegerField;
    taUSerUidWhFIO: TFIBStringField;
    taUSerUidWhUIDWHBD: TFIBDateTimeField;
    taUSerUidWhUIDWHED: TFIBDateTimeField;
    dsUserUidWh: TDataSource;
    taUSerUidWhCALCUIDWH: TFIBSmallIntField;
    quActAllowancesMATERIALNAME: TFIBStringField;
    quActAllowancesPROBENAME: TFIBStringField;
    quActAllowancesMATERIALID: TFIBIntegerField;
    quActAllowancesPROBEID: TFIBIntegerField;
    DBProbe: TpFIBDataSet;
    DataSourceProbe: TDataSource;
    DBProbePROBEID: TFIBIntegerField;
    DBProbePROBENAME: TFIBStringField;
    DBProbePROBE: TFIBFloatField;
    DBProbeMATERIALID: TFIBIntegerField;
    DBProbeMATERIALNAME: TFIBStringField;
    DBProbeNAME: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure quAMBeforeOpen(DataSet: TDataSet);
    procedure quAMR_STATEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure quRAMBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taUIDStoreListNewRecord(DataSet: TDataSet);
    procedure taUIDStoreListBeforeOpen(DataSet: TDataSet);
    procedure taUIDStoreListBeforeDelete(DataSet: TDataSet);
    procedure taUIDStoreBeforeOpen(DataSet: TDataSet);
    procedure UNITIDGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taCheckUIDStoreDataFLAGGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure taUidItemBeforeOpen(DataSet: TDataSet);
    procedure taUidItemAfterClose(DataSet: TDataSet);
    procedure quDetalChgBeforeOpen(DataSet: TDataSet);
    procedure quUIDChgBeforeOpen(DataSet: TDataSet);
    procedure quUIDChgAfterScroll(DataSet: TDataSet);
    procedure quUIDChgAfterOpen(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure taUIDStore_DetailBeforeOpen(DataSet: TDataSet);
    procedure quSelectLogBeforeOpen(DataSet: TDataSet);
    procedure quEditArtCalcFields(DataSet: TDataSet);
    procedure quEditArtBeforeOpen(DataSet: TDataSet);
    procedure quEditArt2BeforeOpen(DataSet: TDataSet);
    procedure quEditInsBeforeOpen(DataSet: TDataSet);
    procedure quEditInsCalcFields(DataSet: TDataSet);
    procedure quCompD_artBeforeOpen(DataSet: TDataSet);
    procedure quApplDepItemBeforeOpen(DataSet: TDataSet);
    procedure quApplDepArtBeforeOpen(DataSet: TDataSet);
    procedure quSelectUidBeforeOpen(DataSet: TDataSet);
    procedure quApplDepArtNewRecord(DataSet: TDataSet);
    procedure quApplDepItemNewRecord(DataSet: TDataSet);
    procedure quApplDepListBeforeDelete(DataSet: TDataSet);
    procedure quActAllowancesListNewRecord(DataSet: TDataSet);
    procedure quActAllowancesListBeforeEdit(DataSet: TDataSet);
    procedure quActAllowancesListCalcFields(DataSet: TDataSet);
    procedure quActAllowancesListBeforeOpen(DataSet: TDataSet);
    procedure quActAllowancesBeforeOpen(DataSet: TDataSet);
    procedure quActAllowancesBeforeDelete(DataSet: TDataSet);
    procedure quActAllowancesBeforeEdit(DataSet: TDataSet);
    procedure quDInvCheckBeforeOpen(DataSet: TDataSet);
    procedure quDInvCheckAfterPost(DataSet: TDataSet);
    procedure quDInvCheckNewRecord(DataSet: TDataSet);
    procedure quApplDepListBeforeOpen(DataSet: TDataSet);
    procedure quApplDepListNewRecord(DataSet: TDataSet);
    procedure quActAllowParamPrnBeforeOpen(DataSet: TDataSet);
    procedure quSurPlusItemCalcFields(DataSet: TDataSet);
    procedure quSurPlusItemBeforeOpen(DataSet: TDataSet);
    procedure quD_writeoffNewRecord(DataSet: TDataSet);
    procedure quReciepGiftBeforeOpen(DataSet: TDataSet);
    procedure quEquipmentBeforeOpen(DataSet: TDataSet);
    procedure quEquipmentNewRecord(DataSet: TDataSet);
    procedure taUidStoreDepItemBeforeOpen(DataSet: TDataSet);
    procedure taUidStoreDepItemCalcFields(DataSet: TDataSet);
    procedure quCompSinvCalcFields(DataSet: TDataSet);
    procedure taRespStoringBeforeOpen(DataSet: TDataSet);
    procedure taRespStoringCalcFields(DataSet: TDataSet);
    procedure taRepairListBeforeOpen(DataSet: TDataSet);
    procedure taRepairListNewRecord(DataSet: TDataSet);
    procedure taRepairListCalcFields(DataSet: TDataSet);
    procedure taRepairListBeforeDelete(DataSet: TDataSet);
    procedure taRepairBeforeOpen(DataSet: TDataSet);
    procedure taRepairNewRecord(DataSet: TDataSet);
    procedure taRepairBeforeDelete(DataSet: TDataSet);
    procedure taRepairBeforeEdit(DataSet: TDataSet);
    procedure taRepairListBeforeEdit(DataSet: TDataSet);
    procedure taSuspItemListCalcFields(DataSet: TDataSet);
    procedure taSuspItemListBeforeOpen(DataSet: TDataSet);
    procedure taSuspItemListNewRecord(DataSet: TDataSet);
    procedure taSuspItemBeforeOpen(DataSet: TDataSet);
    procedure taSuspItemListBeforeEdit(DataSet: TDataSet);
    procedure quDInvCheckBeforePost(DataSet: TDataSet);
    procedure DBProbeCalcFields(DataSet: TDataSet);
  private
    FUIDStoreYear: word;
    FFilterUIDStoreDepId: integer;
    FFilterUIDStore: string;
    FFilterUIDStoreF: integer;
    FFilterUIDStoreUID: integer;
    FCHECKMode: string;
  public
    SuspItemDepid1, SuspItemDepid2: integer; //��� ���������� �������
    {��� ���� ��������}
    ActDepid1, ActDepID2:integer;
    ActBD, ActEd: tdatetime;
    {*****************}
    {��� ��}
     DinvDepId:integer;
    {******}
    {��� ������ �� ������}
    AppldepDepID1, ApplDepDepID2, ApplDepDepfromid1, ApplDepDepFromID2:integer;
    AppldepBD, ApplDepEd: TDateTime;
    ApplDepAllShow:boolean;
    {********************}
    {����� �� ������}
     FilterUIDStoreCom:integer;    
    {***************}
    {������������ ��������}
     FilterArtResp:string;
     FilterUidResp, RespDepid1, RespDepid2:integer;
     RespBd, RespEd:TDateTime;
    {*********************}
    FUIDApllDep: boolean;
    D_Compid:integer;
    MBD: TDateTime;
    MED: TDateTime;
    FilterEdit:boolean;
    FilterUID: string;
    FilterArt: string;
    LogUserID: string;
    property UIDStoreYear : word read FUIDStoreYear write FUIDStoreYear;
    property FilterUIDStoreDepId : integer read FFilterUIDStoreDepId write FFilterUIDStoreDepId;
    property FilterUIDStoreGrMat : string read FFilterUIDStore write FFilterUIDStore;
    property FilterUIDStoreF : integer read FFilterUIDStoreF write FFilterUIDStoreF;
    property FilterUIDStoreUID : integer read FFilterUIDStoreUID write FFilterUIDStoreUID;
    property CHECKMode: string read FCHECKMode write FCHECKMode;
    function defineOperationId (userid :string):string;
    function insert_operation (name_opr:string; ref:string):string;
    procedure update_operation (LogOperationID:string);
  end;

var
  dm3: Tdm3;

implementation

uses comdata, Data, Data2, Dialogs,M207Proc,UtilLib, dbUtil, UidStoreDepList,
     MsgDialog;

{$R *.dfm}

{ Tdm3 }

procedure Tdm3.DataModuleCreate(Sender: TObject);
var ip,host :string;
    sqlstr: string;
begin
  MBD:= dmCom.FirstMonthDate;
  MED:= dmCom.GetServerTime;

  if dmcom.IsMakeLog then
  begin
   LogUserID :=IntToStr(dmCom.LogGetID(46));
   GetLocalIP(ip,host);
   if (ip = '') then ip  := 'ip isn''t define';
   if (host = '') then host  := ' host isn''t define';
   sqlstr := ' insert into Log$User(L_UserID, Host, UserName, DISCRIPT) ' +
                ' values('+ LogUserID +',''' + ip + ':' + host + ''', '#39 + dmCom.UserName +  #39', ''Connected'')';
   if (trim(LogUserID)<>'')and(trim(ip)<>'')and(trim(host)<>'')and(trim(dmCom.UserName)<>'')then 
    ExecSQL(sqlstr,quTmpLog);
  end;
end;

procedure Tdm3.quAMBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      ByName['MBD'].AsTimeStamp:= DateTimeToTimeStamp(MBD);
      ByName['MED'].AsTimeStamp:= DateTimeToTimeStamp(MED);
    end;
end;

procedure Tdm3.quAMR_STATEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           1: Text:='��������� � �������';
           2: Text:='�������';
           3: Text:='������';
           4: Text:='����������';
         end;
end;

function Tdm3.defineOperationId (userid :string):string;
begin
 if dmcom.IsMakeLog then
 begin
  if not quTmpLog.Transaction.Active then quTmpLog.Transaction.StartTransaction;
  quTmpLog.Close;
  quTmpLog.SQL.Text := 'select max(L_OPRID) from LOG$OPERATION where L_USERID = '+UserID;
  if (trim(UserId)<>'') then
  begin
   quTmpLog.ExecQuery;
   quTmpLog.Transaction.CommitRetaining;
   result:=quTmpLog.Fields[0].AsString;
  end
  else result:=''; 
  quTmpLog.Close;
 end
 else result:='';
end;

function Tdm3.insert_operation (name_opr:string; ref:string):string;
var sqlstr: string;
    level:integer;
begin
 if dmcom.IsMakeLog then
 begin
  if (ref<>'-1000') then
  begin
   quTmpLog.Close;
   quTmpLog.SQL.Text:='select tree_level from Log$Operation where l_oprid = '+ref;
   if (ref<>'') then
   begin
    quTmpLog.ExecQuery;
    level:=quTmpLog.Fields[0].Asinteger+1;
    quTmpLog.Transaction.CommitRetaining;
   end
   else Level:=-2;
   quTmpLog.Close;
  end
  else level:=0;

  sqlstr := ' insert into Log$Operation(L_UserID,OperType,OPER_ENTER, Ref, tree_level) ' +
            ' values(' + dm3.LogUserID +  ', '#39+name_opr+#39', 0, '+ ref +', '+inttostr(level)+')';
  if (trim(dm3.LogUserID)<>'')and(trim(name_opr)<>'')and(trim(ref)<>'')and(level<>-2) then
  begin
   ExecSQL(sqlstr,quTmpLog);
   result:=defineOperationId (LogUserID);
  end
  else result:=''
 end
 else result:=''
end;

procedure Tdm3.update_operation (LogOperationID:string);
var sqlstr: string;
begin
 if dmcom.IsMakeLog and (trim(LogOperationID)<>'') then
 begin
  sqlstr := ' update Log$Operation set OperTime_ED='#39+datetimetostr(dmcom.GetServerTime)+#39+
            ' where l_oprid = '+LogOperationID;
  ExecSQL(sqlstr,dm3.quTmpLog);
 end
end;

procedure Tdm3.quRAMBeforeOpen(DataSet: TDataSet);
begin
   with TpFIBDataSet(DataSet), Params do
     begin
       ByName['BD'].AsTimeStamp:= DateTimeToTimeStamp(dm.RBD);
       ByName['ED'].AsTimeStamp:= DateTimeToTimeStamp(dm.RED);
     end;
end;

procedure Tdm3.CommitRetaining(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
end;

procedure Tdm3.taUIDStoreListNewRecord(DataSet: TDataSet);
begin
  taUIDStoreListSINVID.AsInteger := dmCom.GetId(8);
  taUIDStoreListSN.AsInteger := dmCom.GetID(37);
  taUIDStoreListUSERID.AsInteger := dmCom.UserId;
end;

procedure Tdm3.taUIDStoreListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['EYEAR'].AsInteger := UIDStoreYear;
end;

procedure Tdm3.taUIDStoreListBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������������ ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort
end;

procedure Tdm3.taUIDStoreBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['SINVID'].AsInteger := taUIDStoreListSINVID.AsInteger;
    if(FilterUIDStoreDepId = -1)then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := FilterUIDStoreDepId;
      ByName['DEPID2'].AsInteger := FilterUIDStoreDepId;
    end;
    if(FilterUIDStoreGrMat = '*���')then
    begin
      ByName['GR1'].AsString := '!';
      ByName['GR2'].AsString := '��';
    end
    else begin
      ByName['GR1'].AsString := FilterUIDStoreGrMat;
      ByName['GR2'].AsString := FilterUIDStoreGrMat;
    end
  end
end;

procedure Tdm3.UNITIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��';
           1: Text:='��';
         end;
end;

procedure Tdm3.taCheckUIDStoreDataFLAGGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
     0 : Text := '���� � ������';
     1 : Text := '���� � �������� �� ���������';
     2 : Text := '�� ������������� ���������';
  end;
end;

procedure Tdm3.taUidItemBeforeOpen(DataSet: TDataSet);
begin
   with taUidItem, SelectSQL do
   begin
     case FilterUIDStoreF of
       0: SelectSQL[Count-1] :='';
       1: SelectSQL[Count-1] :=' and ((s.F=1) or (s.RESNUMCOM=1)) ';
       2: SelectSQL[Count-1] :=' and ((s.F=0) and (s.RESNUMCOM=0)) ';
       3: SelectSQL[Count-1] :=' and ((s.F not in (0,1)) or (s.RESNUMCOM not in (0,1))) ';
      end;
     case FilterUIDStoreCom of
       0: SelectSQL[Count-2] :='';
       1: SelectSQL[Count-2] :=' and ((s.ENTRYNUMCOM=1) or (s.SINVNUMCOM=1) or '+
          '(s.DINVNUMFROMCOM=1) or (s.DINVNUMCOM=1) or (s.SELLNUMCOM=1) or (s.RETNUMCOM=1) or '+
          '(s.OPTSELLNUMCOM=1) or (s.OPTRETNUMCOM=1) or (s.SINVRETNUMCOM=1) or (s.RESNUMCOM=1) or '+
          '(s.ACTALLOWANCESNUMCOM=1)) ';
       2: SelectSQL[Count-2] :=' and ((s.ENTRYNUM=1) or (s.SINVNUM=1) or '+
          '(s.DINVNUMFROM=1) or (s.DINVNUM=1) or (s.SELLNUM=1) or (s.RETNUM=1) or '+
          '(s.OPTSELLNUM=1) or (s.OPTRETNUM=1) or (s.SINVRETNUM=1) or (s.RESNUM=1) or '+
          '(s.F=1) or (s.ACTALLOWANCESNUM=1) or (s.SURPLUSNUM=1)) ';
      end;
   end;

  taUidItem.Prepare;

  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['SINVID'].AsInteger := taUIDStoreListSINVID.AsInteger;
    if(FilterUIDStoreUID = -1)then
    begin
      ByName['UID1'].AsInteger := -MAXINT;
      ByName['UID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['UID1'].AsInteger := FilterUIDStoreUID;
      ByName['UID2'].AsInteger := FilterUIDStoreUID;
    end;
    if(FilterUIDStoreDepId = -1)then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := FilterUIDStoreDepId;
      ByName['DEPID2'].AsInteger := FilterUIDStoreDepId;
    end;
    if(FilterUIDStoreGrMat = '*���')then
    begin
      ByName['GR1'].AsString := '!';
      ByName['GR2'].AsString := '��';
    end
    else begin
      ByName['GR1'].AsString := FilterUIDStoreGrMat;
      ByName['GR2'].AsString := FilterUIDStoreGrMat;
    end;
  end
end;

procedure Tdm3.taUidItemAfterClose(DataSet: TDataSet);
begin
  taUidItem.SelectSQL[taUidItem.SelectSQL.Count-1] := ' ';
end;

procedure Tdm3.quDetalChgBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['UID'].AsInteger := quUIDChgUID.AsInteger;
    ByName['CDate'].AsTimeStamp := DateTimeToTimeStamp(quUIDChgCH_DATE.AsDateTime);
    ByName['discrip'].AsString := quUIDChgDISCRIP.AsString;
  end

end;

procedure Tdm3.quUIDChgBeforeOpen(DataSet: TDataSet);
begin
  with quUIDChg do
  begin
     SelectSQL[3] := '';
     if (FilterUID<>'') and (FilterArt <> '')  then
       SelectSQL[3] :=' where uid = ' + FilterUID +
                      '   and ((NEWVAL Between ''' + FilterArt + ''' AND ''' +
                             FilterArt + '��'') '  +
                      '   or (OLDVAL Between ''' + FilterArt + ''' AND ''' +
                             FilterArt + '��'')) '

     else if (FilterUID<>'') then
       SelectSQL[3] :=' where uid = ' + FilterUID
     else if (FilterArt <> '') then
       SelectSQL[3] :=' where ((NEWVAL Between ''' + FilterArt + ''' AND ''' +
                             FilterArt + '��'') '  +
                      '   or (OLDVAL Between ''' + FilterArt + ''' AND ''' +
                             FilterArt + '��'')) '
  end;

  quUIDChg.Prepare;
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm3.MBD);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm3.MED);
  end

end;

procedure Tdm3.quUIDChgAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSets([dm3.quDetalChg]);
end;

procedure Tdm3.quUIDChgAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSets([dm3.quDetalChg]);
end;

procedure Tdm3.DataModuleDestroy(Sender: TObject);
var sqlstr: string;
begin
  if (dmcom.IsMakeLog) and (trim(LogUserID)<>'') then
  begin
   sqlstr := ' Update Log$User' +
             '   Set  DISCRIPT = stretrim(DISCRIPT) || ''/Disconnected''' +
             ' where L_UserID = ' + LogUserID;
   ExecSQL(sqlstr,quTmpLog);
  end;

{  if not(quTmpLog.Transaction.Active) then quTmpLog.Transaction.StartTransaction;
  quTmpLog.SQL.Text := sqlstr;
  quTmpLog.ExecQuery;
  quTmpLog.Close;
  quTmpLog.Transaction.CommitRetaining;}
end;                                                


procedure Tdm3.DBProbeCalcFields(DataSet: TDataSet);

function LeftPad(PadString : string ; HowMany : integer ; PadValue : string): string;
var
   Counter : integer;
   x : integer;
   NewString : string;
begin
   NewString := '';

   Counter := HowMany - Length(PadString);

   for x := 1 to Counter do
   begin
      NewString := NewString + PadValue;
   end;

   Result := NewString + PadString;
end;

function RightPad(PadString : string ; HowMany : integer ; PadValue : string): string;
var
   Counter : integer;
   x : integer;
   NewString : string;
begin
   NewString := '';

   Counter := HowMany - Length(PadString);

   for x := 1 to Counter do
   begin
      NewString := NewString + PadValue;
   end;

   Result := PadString + NewString;
end;

begin
  DBProbeNAME.AsString := RightPad(DBProbeMATERIALNAME.AsString, 10, ' ') + LeftPad(DBProbePROBENAME.AsString, 6, ' ');
end;

procedure Tdm3.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure Tdm3.taUIDStore_DetailBeforeOpen(DataSet: TDataSet);
begin
  taUIDStore_Detail.ParamByName('SINVID').AsInteger := taUIDStoreSINVID.AsInteger;
  taUIDStore_Detail.ParamByName('ARTID').AsInteger := taUIDStoreARTID.AsInteger;
  taUIDStore_Detail.ParamByName('SZ').AsString := taUIDStoreSZ.AsString;
  taUIDStore_Detail.ParamByName('DEPID').AsInteger := taUIDStoreDEPID.AsInteger;
end;

procedure Tdm3.quSelectLogBeforeOpen(DataSet: TDataSet);
begin
   with quSelectLog, Params do
     begin
       ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.RBD);
       ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.RED);
     end;
end;

procedure Tdm3.quEditArtCalcFields(DataSet: TDataSet);
begin
 if quEditArtUNITID.IsNull then quEditArtUNIT.AsString:=''
 else if quEditArtUNITID.AsInteger=0 then quEditArtUNIT.AsString:='��.'
 else quEditArtUNIT.AsString:='��.';

 if quEditArtOLD_UNITID.IsNull then quEditArtOLD_UINT.AsString:=''
 else if quEditArtOLD_UNITID.AsInteger=0 then quEditArtOLD_UINT.AsString:='��.'
 else quEditArtOLD_UINT.AsString:='��.';
end;

procedure Tdm3.quEditArtBeforeOpen(DataSet: TDataSet);
begin
 if FilterArt='' then
  begin
   quEditArt.SelectSQL[8]:='';
   if FilterEdit then quEditArt.SelectSQL[9]:='where (OLD_D_COMPID is not null) or '+
                     '(OLD_D_MATID is not null) or (OLD_D_GOODID is  not null) or '+
                     '(OLD_ART is not null) or (OLD_UNITID is not null) or'+
                     '(OLD_D_INSID is not null) or (OLD_D_COUNTRYID is not null) or'+
                     '(OLD_ATT1ID is not null) or (OLD_ATT2ID is not null) '
   else quEditArt.SelectSQL[9]:=''
  end
 else
  begin
   quEditArt.SelectSQL[8]:='where art like '#39+FilterArt+'%'#39;
   if FilterEdit then quEditArt.SelectSQL[9]:='and ((OLD_D_COMPID is not null) or '+
                     '(OLD_D_MATID is not null) or (OLD_D_GOODID is  not null) or '+
                     '(OLD_ART is not null) or (OLD_UNITID is not null) or'+
                     '(OLD_D_INSID is not null) or (OLD_D_COUNTRYID is not null) or'+
                     '(OLD_ATT1ID is not null) or (OLD_ATT2ID is not null)) '
   else quEditArt.SelectSQL[9]:=''
  end;
 quEditArt.Prepare;
 quEditArt.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(MBD);
 quEditArt.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(MED);
end;

procedure Tdm3.quEditArt2BeforeOpen(DataSet: TDataSet);
begin
 if FilterArt='' then quEditArt2.SelectSQL[4]:=''
 else quEditArt2.SelectSQL[4]:=' where art like '#39+FilterArt+'%'#39;
 quEditArt2.Prepare;
 quEditArt2.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(MBD);
 quEditArt2.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(MED);
end;

procedure Tdm3.quEditInsBeforeOpen(DataSet: TDataSet);
begin
 if FilterArt='' then quEditIns.SelectSQL[10]:=''
 else quEditIns.SelectSQL[10]:='where art like '#39+FilterArt+'%'#39;
 quEditIns.Prepare;
 quEditIns.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(MBD);
 quEditIns.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(MED);
end;

procedure Tdm3.quEditInsCalcFields(DataSet: TDataSet);
begin
 case quEditInsSTATE.AsInteger of
  0: quEditInsOperation.AsString:='����������';
  1: quEditInsOperation.AsString:='��������������';
  2: quEditInsOperation.AsString:='��������';
 end;

 case quEditInsISPRORD.AsInteger of
  0: quEditInsWhere_.AsString:='���. ������ �� ���.';
  1: quEditInsWhere_.AsString:='';
  2: quEditInsWhere_.AsString:='�����������';
 end;
end;

procedure Tdm3.quCompD_artBeforeOpen(DataSet: TDataSet);
begin
 TpFibDataSet(DataSet).ParamByName('D_Compid').AsInteger:=D_Compid;
end;

procedure Tdm3.quApplDepItemBeforeOpen(DataSet: TDataSet);
begin
 quApplDepItem.ParamByName('SINVID').AsInteger:=quApplDepListSINVID.AsInteger;
end;

procedure Tdm3.quApplDepArtBeforeOpen(DataSet: TDataSet);
begin
 quApplDepArt.ParamByName('D_ARTID').AsInteger:=dm2.quD_WH2D_ARTID.AsInteger;
 quApplDepArt.ParamByName('SINVID').AsInteger:=quApplDepListSINVID.AsInteger; 
end;

procedure Tdm3.quSelectUidBeforeOpen(DataSet: TDataSet);
begin
 quSelectUid.ParamByName('SINVID').AsInteger:=quApplDepListSINVID.AsInteger;
 quSelectUid.ParamByName('DEPID').AsInteger:=quApplDepListDEPID.AsInteger;
 if not FUIDApllDep then
 begin
  quSelectUid.ParamByName('D_ARTID').AsInteger:=dm2.quD_WH2D_ARTID.AsInteger;
  quSelectUid.ParamByName('UID').IsNull:=true;
 end
 else quSelectUid.ParamByName('D_ARTID').IsNull:=true;
end;

procedure Tdm3.quApplDepArtNewRecord(DataSet: TDataSet);
begin
 quApplDepArtAPPLDEPID.AsInteger:=dmcom.GetID(48);
 quApplDepArtSINVID.AsInteger:=quApplDepListSINVID.AsInteger;
end;

procedure Tdm3.quApplDepItemNewRecord(DataSet: TDataSet);
begin
 quApplDepItemAPPLDEPID.AsInteger:=dmcom.GetID(48);
 quApplDepItemSINVID.AsInteger:=quApplDepListSINVID.AsInteger;
end;

procedure Tdm3.quApplDepListBeforeDelete(DataSet: TDataSet);
begin
 dm.DelConf(DataSet);
end;

procedure Tdm3.quActAllowancesListNewRecord(DataSet: TDataSet);
begin
  quActAllowancesListSINVID.AsInteger:=dmCom.GetId(8);
  quActAllowancesListNDATE.AsDateTime:=dmCom.GetServerTime;
  quActAllowancesListSDATE.AsDateTime:=dmCom.GetServerTime;
  quActAllowancesListDEPFROMID.AsInteger:=ActDepid1;

  with quTmp do
  begin
   Close;
   SQL.Text:='SELECT max(SN) FROM SINV WHERE ITYPE=19 AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') AND depfromid = '+inttostr(ActDepid1);
   ExecQuery;
   if Fields[0].IsNull then  quActAllowancesListSN.AsInteger:=1
   else quActAllowancesListSN.AsInteger:=Fields[0].AsInteger+1;
   Close;
   Transaction.CommitRetaining;
  end;
  quActAllowancesListCRUSERID.AsInteger:=dmCom.UserId;
end;

procedure Tdm3.quActAllowancesListBeforeEdit(DataSet: TDataSet);
begin
  if quActAllowancesListISCLOSED.AsInteger=1 then
    raise Exception.Create('��� �������� ������!!!');
end;

procedure Tdm3.quActAllowancesListCalcFields(DataSet: TDataSet);
begin
 if quActAllowancesListISCLOSED.AsInteger=0 then
 begin
  quActAllowancesListOpenCostP.AsFloat:=quActAllowancesListCOSTP.AsFloat;
  quActAllowancesListCloseCostP.AsFloat:=0;
  quActAllowancesListOpenCost.AsFloat:=quActAllowancesListCOST.AsFloat;
  quActAllowancesListCloseCost.AsFloat:=0;
  quActAllowancesListOpenW.AsFloat:=quActAllowancesListW.AsFloat;
  quActAllowancesListCloseW.AsFloat:=0;
  quActAllowancesListOpenQ.AsInteger:=quActAllowancesListQ.AsInteger;
  quActAllowancesListCloseQ.AsInteger:=0;
  quActAllowancesListOpenSn.AsInteger:=1;
  quActAllowancesListCloseSn.AsInteger:=0;
 end
 else
 begin
  quActAllowancesListOpenCostP.AsFloat:=0;
  quActAllowancesListCloseCostP.AsFloat:=quActAllowancesListCOSTP.AsFloat;
  quActAllowancesListOpenCost.AsFloat:=0;
  quActAllowancesListCloseCost.AsFloat:=quActAllowancesListCOST.AsFloat;
  quActAllowancesListOpenW.AsFloat:=0;
  quActAllowancesListCloseW.AsFloat:=quActAllowancesListW.AsFloat;
  quActAllowancesListOpenQ.AsInteger:=0;
  quActAllowancesListCloseQ.AsInteger:=quActAllowancesListQ.AsInteger;
  quActAllowancesListOpenSn.AsInteger:=0;
  quActAllowancesListCloseSn.AsInteger:=1;
 end;
end;

procedure Tdm3.quActAllowancesListBeforeOpen(DataSet: TDataSet);
begin
 quActAllowancesList.ParamByName('DEPID1').AsInteger:=ActDepid1;
 quActAllowancesList.ParamByName('DEPID2').AsInteger:=ActDepid2;
 quActAllowancesList.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(ActBD);
 quActAllowancesList.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(ActED);
end;

procedure Tdm3.quActAllowancesBeforeOpen(DataSet: TDataSet);
begin
 quActAllowances.ParamByName('SINVID').AsInteger:=quActAllowancesListSINVID.AsInteger;
end;

procedure Tdm3.quActAllowancesBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then  SysUtils.Abort
end;

procedure Tdm3.quActAllowancesBeforeEdit(DataSet: TDataSet);
begin
  if quActAllowancesListISCLOSED.AsInteger=1 then
    raise Exception.Create('��� �������� ������!!!');
end;

procedure Tdm3.quDInvCheckBeforeOpen(DataSet: TDataSet);
begin
   with quDInvCheck, Params do
     begin
       if FCHECKMode = 'DINV' then
         ByName['SInvID'].AsInteger:=dm.taDListSINVID.AsInteger
       else if FCHECKMode = 'SRET' then
         ByName['SInvID'].AsInteger:=dm.taSRetListSINVID.AsInteger;
     end;
end;

procedure Tdm3.quDInvCheckAfterPost(DataSet: TDataSet);
begin
  dm3.quDInvCheck.Transaction.CommitRetaining;
end;

procedure Tdm3.quDInvCheckNewRecord(DataSet: TDataSet);
begin
   quDInvCheckO_ID.AsInteger:=dmCom.GetId(51);
   with quDInvCheck, Params do
     begin
       if FCHECKMode = 'DINV' then
         ByName['SInvID'].AsInteger:=dm.taDListSINVID.AsInteger
       else if FCHECKMode = 'SRET' then
         ByName['SInvID'].AsInteger:=dm.taSRetListSINVID.AsInteger;
     end;
end;

procedure Tdm3.quApplDepListBeforeOpen(DataSet: TDataSet);
begin
//:I_DEPID1, :I_DEPID2,
// :I_DEPFROMID1, :I_DEPFROMID2, :I_BD, :I_ED
 quApplDepList.ParamByName('I_DEPID1').AsInteger:=AppldepDepID1;
 quApplDepList.ParamByName('I_DEPID2').AsInteger:=AppldepDepID2;
 quApplDepList.ParamByName('I_DEPFROMID1').AsInteger:=ApplDepDepfromid1;
 quApplDepList.ParamByName('I_DEPFROMID2').AsInteger:=ApplDepDepFromID2;
 quApplDepList.ParamByName('I_BD').AsTimeStamp:=DateTimeToTimeStamp(AppldepBD);
 quApplDepList.ParamByName('I_ED').AsTimeStamp:=DateTimeToTimeStamp(ApplDepEd);
 if ApplDepAllShow then
 begin
  quApplDepList.ParamByName('EDITTR1').AsInteger:=0;
  quApplDepList.ParamByName('EDITTR2').AsInteger:=2;
 end
 else
 begin
  quApplDepList.ParamByName('EDITTR1').AsInteger:=0;
  quApplDepList.ParamByName('EDITTR2').AsInteger:=0;
 end
end;

procedure Tdm3.quApplDepListNewRecord(DataSet: TDataSet);
var sn:integer;
begin
 quApplDepListSINVID.AsInteger:=dmcom.GetID(8);
 quApplDepListDEPID.AsInteger:=ApplDepDepID2;
 quApplDepListSDATE.AsDateTime:=dmcom.GetServerTime;
 quApplDepListDEPFROMID.AsInteger:=ApplDepDepFromID2;
 quApplDepListMODEID.AsInteger:=SelfDepId;
 quApplDepListCRUSERID.AsInteger:=dmcom.UserId;
 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select max(sn) from sinv where itype = 17 and  '+
  ' fyear(sdate)=fyear(current_timestamp) and depid = '+inttostr(ApplDepDepID2);
 dm.quTmp.ExecQuery;
 sn:=dm.quTmp.Fields[0].AsInteger+1;
 dm.quTmp.Close;
 dm.quTmp.Transaction.CommitRetaining;
 quApplDepListSN.AsInteger:=sn;
end;

procedure Tdm3.quActAllowParamPrnBeforeOpen(DataSet: TDataSet);
begin
  execSql('execute procedure CheckWriteOff_Param(' + quActAllowancesListSINVID.AsString + ')' , dm3.quTmp);
  quActAllowParamPrn.Params.ByName['SINVID'].AsInteger := quActAllowancesListSINVID.AsInteger;
end;

procedure Tdm3.quSurPlusItemCalcFields(DataSet: TDataSet);
begin
 quSurPlusItemRno.AsInteger:=quSurPlusItem.RecNo;
 if quSurPlusItemUNITID.AsInteger=1 then
 begin
  quSurPlusItemSCost.AsFloat:=quSurPlusItemW.AsFloat*quSurPlusItemPRICE.AsFloat;
  quSurPlusItemCost2.AsFloat:=quSurPlusItemW.AsFloat*quSurPlusItemPRICE2.AsFloat;
  quSurPlusItemUnit.AsString:='��.';
 end
 else
 begin
  quSurPlusItemSCost.AsFloat:=quSurPlusItemPRICE.AsFloat;
  quSurPlusItemCost2.AsFloat:=quSurPlusItemPRICE2.AsFloat;
  quSurPlusItemUnit.AsString:='��.';
 end
end;

procedure Tdm3.quSurPlusItemBeforeOpen(DataSet: TDataSet);
begin
 quSurPlusItem.ParamByName('SINVID').AsInteger:=dm.taSListSINVID.AsInteger;
end;

procedure Tdm3.quD_writeoffNewRecord(DataSet: TDataSet);
begin
  quD_writeoffId.AsInteger:=dmCom.GetId(53);
end;

procedure Tdm3.quReciepGiftBeforeOpen(DataSet: TDataSet);
begin
  execSql('execute procedure CheckWriteOff_Param(' + quActAllowancesListSINVID.AsString + ')' , dm3.quTmp);
  quReciepGift.Params.ByName['SINVID'].AsInteger := quActAllowancesListSINVID.AsInteger;
end;

procedure Tdm3.quEquipmentBeforeOpen(DataSet: TDataSet);
begin
 quEquipment.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(dm.BeginDate);
 quEquipment.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(dm.EndDate);
end;

procedure Tdm3.quEquipmentNewRecord(DataSet: TDataSet);
begin
  quEquipmentSINVID.AsInteger:=dmCom.GetId(8);
  quEquipmentDEPID.AsInteger:=SelfDepId;
  quEquipmentNDATE.AsDateTime:=dmCom.GetServerTime;
  quEquipmentSDATE.AsDateTime:= strtodatetime(datetostr(dmCom.GetServerTime));
  quEquipmentCOST.AsFloat:=0;
  quEquipmentISCLOSED.AsInteger:=0;
  quEquipmentITYPE.AsInteger:=22;
  quEquipmentCRUSERID.AsInteger:=dmcom.UserId;  

  with quTmp do
    begin
      SQL.Text:='SELECT max(SN) FROM SINV WHERE ITYPE in (22) AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') AND depid = '+inttostr(SelfDepId);
      ExecQuery;
      if Fields[0].IsNull then  quEquipmentSN.AsInteger:=1
      else quEquipmentSN.AsInteger:=Fields[0].AsInteger+1;
      Close;
    end;
end;

procedure Tdm3.taUidStoreDepItemBeforeOpen(DataSet: TDataSet);
begin
   with taUidStoreDepItem, SelectSQL do
   begin
     case FilterUIDStoreF of
       0: SelectSQL[Count-1] :='';
       1: SelectSQL[Count-1] :=' and u.resnum=1 ';
       2: SelectSQL[Count-1] :=' and u.resnum=0 ';
       3: SelectSQL[Count-1] :=' and u.resnum not in (0,1) ';
      end;
   end;

  taUidStoreDepItem.Prepare;

  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['SINVID'].AsInteger := fmUidStoreDepList.taUIDStoreDepListSINVID.AsInteger;
    if(FilterUIDStoreUID = -1)then
    begin
      ByName['UID1'].AsInteger := -MAXINT;
      ByName['UID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['UID1'].AsInteger := FilterUIDStoreUID;
      ByName['UID2'].AsInteger := FilterUIDStoreUID;
    end;
    if(FilterUIDStoreGrMat = '*���')then
    begin
      ByName['GR1'].AsString := '!';
      ByName['GR2'].AsString := '��';
    end
    else begin
      ByName['GR1'].AsString := FilterUIDStoreGrMat;
      ByName['GR2'].AsString := FilterUIDStoreGrMat;
    end;
  end

end;

procedure Tdm3.taUidStoreDepItemCalcFields(DataSet: TDataSet);
begin
 if taUidStoreDepItemUNITID.AsInteger=0 then taUidStoreDepItemUNIT.AsString:='��.'
 else taUidStoreDepItemUNIT.AsString:='��.'
end;

procedure Tdm3.quCompSinvCalcFields(DataSet: TDataSet);
begin
 case quCompSinvITYPE.AsInteger of
  1: quCompSinvTypeSinv.AsString:='��������';
  2: quCompSinvTypeSinv.AsString:='��';
  3: quCompSinvTypeSinv.AsString:='���. �������';
  5: quCompSinvTypeSinv.AsString:='�������';
  6: quCompSinvTypeSinv.AsString:='���. �������';
  7: quCompSinvTypeSinv.AsString:='������� ������.';
  8: quCompSinvTypeSinv.AsString:='���. �������';
 end
end;

procedure Tdm3.taRespStoringBeforeOpen(DataSet: TDataSet);
begin
 if FilterArtResp<>'' then taRespStoring.SelectSQL[taRespStoring.SelectSQL.Count-1]:=' and a2.art like '#39+FilterArtResp+'%'#39+' '
 else   taRespStoring.SelectSQL[taRespStoring.SelectSQL.Count-1]:='';

 if FilterUidResp<>-1 then taRespStoring.SelectSQL[taRespStoring.SelectSQL.Count-2]:=' and rs.UID = '+inttostr(FilterUidResp)+' '
 else   taRespStoring.SelectSQL[taRespStoring.SelectSQL.Count-2]:='';

 taRespStoring.Prepare;
 taRespStoring.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(RespBd);
 taRespStoring.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(RespEd);
 taRespStoring.ParamByName('DEPID1').AsInteger:=RespDepid1;
 taRespStoring.ParamByName('DEPID2').AsInteger:=RespDepid2;   
end;

procedure Tdm3.taRespStoringCalcFields(DataSet: TDataSet);
begin
 dm3.taRespStoringCOST.AsFloat:=dm3.taRespStoringQ0.AsFloat*dm3.taRespStoringPRICE.AsFloat;
 dm3.taRespStoringCOST0.AsFloat:=dm3.taRespStoringQ0.AsFloat*dm3.taRespStoringPRICE0.AsFloat;

 case dm3.taRespStoringSTATEUID.AsInteger of
  0, 1: begin
      if dm3.taRespStoringSTATEUID.AsInteger=0 then
       dm3.taRespStoringSate.AsString:='������� �� ������������� ��������'
      else
       dm3.taRespStoringSate.AsString:='�������� ��������� �������'; 
      dm3.taRespStoringSCOST0.AsFloat:=dm3.taRespStoringCOST.AsFloat;
      dm3.taRespStoringSCOST00.AsFloat:=dm3.taRespStoringCOST0.AsFloat;
      dm3.taRespStoringSQ0.AsInteger:=1;
      dm3.taRespStoringSW0.AsFloat:=dm3.taRespStoringW.AsFloat;
      dm3.taRespStoringSCOST1.AsFloat:=0;
      dm3.taRespStoringSCOST01.AsFloat:=0;
      dm3.taRespStoringSQ1.AsInteger:=0;
      dm3.taRespStoringSW1.AsFloat:=0;
      dm3.taRespStoringSCOST2.AsFloat:=0;
      dm3.taRespStoringSCOST02.AsFloat:=0;
      dm3.taRespStoringSQ2.AsInteger:=0;
      dm3.taRespStoringSW2.AsFloat:=0;
      dm3.taRespStoringSCOST3.AsFloat:=0;
      dm3.taRespStoringSCOST03.AsFloat:=0;
      dm3.taRespStoringSQ3.AsInteger:=0;
      dm3.taRespStoringSW3.AsFloat:=0;
     end;
  2: begin
      dm3.taRespStoringSate.AsString:='������� �� �������';
      dm3.taRespStoringSCOST0.AsFloat:=0;
      dm3.taRespStoringSCOST00.AsFloat:=0;
      dm3.taRespStoringSQ0.AsInteger:=0;
      dm3.taRespStoringSW0.AsFloat:=0;
      dm3.taRespStoringSCOST1.AsFloat:=dm3.taRespStoringCOST.AsFloat;
      dm3.taRespStoringSCOST01.AsFloat:=dm3.taRespStoringCOST0.AsFloat;
      dm3.taRespStoringSQ1.AsInteger:=1;
      dm3.taRespStoringSW1.AsFloat:=dm3.taRespStoringW.AsFloat;
      dm3.taRespStoringSCOST2.AsFloat:=0;
      dm3.taRespStoringSCOST02.AsFloat:=0;
      dm3.taRespStoringSQ2.AsInteger:=0;
      dm3.taRespStoringSW2.AsFloat:=0;
      dm3.taRespStoringSCOST3.AsFloat:=0;
      dm3.taRespStoringSCOST03.AsFloat:=0;
      dm3.taRespStoringSQ3.AsInteger:=0;
      dm3.taRespStoringSW3.AsFloat:=0;
     end;
  3: begin
      dm3.taRespStoringSate.AsString:='������� ������� ����� �������';
      dm3.taRespStoringSCOST0.AsFloat:=0;
      dm3.taRespStoringSCOST00.AsFloat:=0;
      dm3.taRespStoringSQ0.AsInteger:=0;
      dm3.taRespStoringSW0.AsFloat:=0;
      dm3.taRespStoringSCOST1.AsFloat:=0;
      dm3.taRespStoringSCOST01.AsFloat:=0;
      dm3.taRespStoringSQ1.AsInteger:=0;
      dm3.taRespStoringSW1.AsFloat:=0;
      dm3.taRespStoringSCOST2.AsFloat:=dm3.taRespStoringCOST.AsFloat;
      dm3.taRespStoringSCOST02.AsFloat:=dm3.taRespStoringCOST0.AsFloat;
      dm3.taRespStoringSQ2.AsInteger:=1;
      dm3.taRespStoringSW2.AsFloat:=dm3.taRespStoringW.AsFloat;
      dm3.taRespStoringSCOST3.AsFloat:=0;
      dm3.taRespStoringSCOST03.AsFloat:=0;
      dm3.taRespStoringSQ3.AsInteger:=0;
      dm3.taRespStoringSW3.AsFloat:=0;
     end;
  4: begin
      dm3.taRespStoringSate.AsString:='������� ����� � ������������� ��������';
      dm3.taRespStoringSCOST0.AsFloat:=0;
      dm3.taRespStoringSCOST00.AsFloat:=0;
      dm3.taRespStoringSQ0.AsInteger:=0;
      dm3.taRespStoringSW0.AsFloat:=0;
      dm3.taRespStoringSCOST1.AsFloat:=0;
      dm3.taRespStoringSCOST01.AsFloat:=0;
      dm3.taRespStoringSQ1.AsInteger:=0;
      dm3.taRespStoringSW1.AsFloat:=0;
      dm3.taRespStoringSCOST2.AsFloat:=0;
      dm3.taRespStoringSCOST02.AsFloat:=0;
      dm3.taRespStoringSQ2.AsInteger:=0;
      dm3.taRespStoringSW2.AsFloat:=0;
      dm3.taRespStoringSCOST3.AsFloat:=dm3.taRespStoringCOST.AsFloat;
      dm3.taRespStoringSCOST03.AsFloat:=dm3.taRespStoringCOST0.AsFloat;
      dm3.taRespStoringSQ3.AsInteger:=1;
      dm3.taRespStoringSW3.AsFloat:=dm3.taRespStoringW.AsFloat;
     end;
 end;
end;

procedure Tdm3.taRepairListBeforeOpen(DataSet: TDataSet);
begin
 with TpFIBDataSet(DataSet), Params do
 begin
  ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.BeginDate);
  ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.EndDate);
 end;
end;

procedure Tdm3.taRepairListNewRecord(DataSet: TDataSet);
var
 r : Variant;
begin
  taRepairListSINVID.AsInteger:=dmCom.GetId(8);
  taRepairListDEPFROMID.AsInteger:=CenterDepId;
  taRepairListSDATE.AsDateTime:=dmCom.GetServerTime;
  taRepairListTR.AsFloat:=0;
  taRepairListPMARGIN.AsFloat:=0;
  taRepairListCOST.AsFloat:=0;
  taRepairListAKCIZ.AsFloat:=dmCom.taRecAkciz.AsFloat;
  taRepairListISCLOSED.AsInteger:=0;
  taRepairListDEPFROM.AsString:= dm.DDepFrom;
  taRepairListUSERID.AsInteger := dmCom.UserId;
  r := ExecSelectSQL('select RET_NONDS from D_REC', dm.quTmp);
  if not VarIsNull(r)then taRepairListRET_NONDS.AsInteger := r
  else taRepairListRET_NONDS.AsInteger := 0;
end;

procedure Tdm3.taRepairListCalcFields(DataSet: TDataSet);
begin
 case taRepairListISCLOSED.AsInteger of
  0: begin
      taRepairListSCOSTOPEN.AsFloat:=taRepairListCOST.AsFloat;
      taRepairListSTROPEN.AsFloat:=taRepairListTR.AsFloat;
      taRepairListCOUNTOPEN.AsInteger:=1;
      taRepairListSWOPEN.AsFloat:=taRepairListW.AsFloat;
      taRepairListSQOPEN.AsInteger:=taRepairListR_COUNT.AsInteger;
      taRepairListSCOSTCLOSED.AsFloat:=0;
      taRepairListSTRCLOSED.AsFloat:=0;
      taRepairListCOUNTCLOSED.AsInteger:=0;
      taRepairListSWCLOSED.AsFloat:=0;
      taRepairListSQCLOSED.AsInteger:=0;
      taRepairListSCOST1.AsFloat:=0;
      taRepairListSTR1.AsFloat:=0;
      taRepairListCOUNT1.AsInteger:=0;
      taRepairListSW1.AsFloat:=0;
      taRepairListSQ1.AsInteger:=0;
     end;
  1: begin
      taRepairListSCOSTOPEN.AsFloat:=0;
      taRepairListSTROPEN.AsFloat:=0;
      taRepairListCOUNTOPEN.AsInteger:=0;
      taRepairListSWOPEN.AsFloat:=0;
      taRepairListSQOPEN.AsInteger:=0;
      taRepairListSCOSTCLOSED.AsFloat:=taRepairListCOST.AsFloat;
      taRepairListSTRCLOSED.AsFloat:=taRepairListTR.AsFloat;
      taRepairListCOUNTCLOSED.AsInteger:=1;
      taRepairListSWCLOSED.AsFloat:=taRepairListW.AsFloat;
      taRepairListSQCLOSED.AsInteger:=taRepairListR_COUNT.AsInteger;
      taRepairListSCOST1.AsFloat:=0;
      taRepairListSTR1.AsFloat:=0;
      taRepairListCOUNT1.AsInteger:=0;
      taRepairListSW1.AsFloat:=0;
      taRepairListSQ1.AsInteger:=0;
     end;
  2: begin
      taRepairListSCOSTOPEN.AsFloat:=0;
      taRepairListSTROPEN.AsFloat:=0;
      taRepairListCOUNTOPEN.AsInteger:=0;
      taRepairListSWOPEN.AsFloat:=0;
      taRepairListSQOPEN.AsInteger:=0;
      taRepairListSCOSTCLOSED.AsFloat:=0;
      taRepairListSTRCLOSED.AsFloat:=0;
      taRepairListCOUNTCLOSED.AsInteger:=0;
      taRepairListSWCLOSED.AsFloat:=0;
      taRepairListSQCLOSED.AsInteger:=0;
      taRepairListSCOST1.AsFloat:=taRepairListCOST.AsFloat;
      taRepairListSTR1.AsFloat:=taRepairListTR.AsFloat;
      taRepairListCOUNT1.AsInteger:=1;
      taRepairListSW1.AsFloat:=taRepairListW.AsFloat;
      taRepairListSQ1.AsInteger:=taRepairListR_COUNT.AsInteger;      
     end;
 end;
end;

procedure Tdm3.taRepairListBeforeDelete(DataSet: TDataSet);
begin
 dm.DelConf(DataSet)
end;

procedure Tdm3.taRepairBeforeOpen(DataSet: TDataSet);
begin
 taRepair.ParamByName('SINVID').AsInteger:=taRepairListSINVID.AsInteger;
end;

procedure Tdm3.taRepairNewRecord(DataSet: TDataSet);
begin
 taRepairREPAIRID.AsInteger:=dmcom.GetID(58);
end;

procedure Tdm3.taRepairBeforeDelete(DataSet: TDataSet);
begin
 dm.DelConf(DataSet)
end;

procedure Tdm3.taRepairBeforeEdit(DataSet: TDataSet);
begin
 if taRepairListISCLOSED.AsInteger <>0 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm3.taRepairListBeforeEdit(DataSet: TDataSet);
begin
 if taRepairListISCLOSED.AsInteger <>0 then raise Exception.Create('��������� �������!!!');
end;

procedure Tdm3.taSuspItemListCalcFields(DataSet: TDataSet);
begin
 case taSuspItemListRSTATE.AsInteger of
 0: taSuspItemListState.AsString:='';
 1: taSuspItemListState.AsString:='��������� � �������';
 2: taSuspItemListState.AsString:='�������';
 3: taSuspItemListState.AsString:='������';
 end;
end;

procedure Tdm3.taSuspItemListBeforeOpen(DataSet: TDataSet);
begin
 taSuspItemList.ParamByName('DEPID1').AsInteger:=SuspItemDepid1;
 taSuspItemList.ParamByName('DEPID2').AsInteger:=SuspItemDepid2; 
end;

procedure Tdm3.taSuspItemListNewRecord(DataSet: TDataSet);
begin
 taSuspItemListSInvId.AsInteger:=dmCom.GetId(8);
 taSuspItemListSDATE.AsDateTime:=dmcom.GetServerTime;
 taSuspItemListUSERID.AsInteger:=dmCom.UserId;
 taSuspItemListDEPID.AsInteger:=SelfDepId;

 with quTmp do
  begin
   SQL.Text:='SELECT max(SN) FROM SINV WHERE ITYPE in (10) AND FYEAR(SDATE)=FYEAR('#39+'TODAY'+#39') AND depid = '+inttostr(SelfDepId);
   ExecQuery;
   if Fields[0].IsNull then  taSuspItemListSN.AsInteger:=1
   else taSuspItemListSN.AsInteger:=Fields[0].AsInteger+1;
   Close;
  end;
end;

procedure Tdm3.taSuspItemBeforeOpen(DataSet: TDataSet);
begin
 taSuspItem.ParamByName('SINVID').AsInteger:= taSuspItemListSINVID.AsInteger;
end;

procedure Tdm3.taSuspItemListBeforeEdit(DataSet: TDataSet);
begin
 if taSuspItemListDEPID.AsInteger<>SelfDepId then
  raise Exception.Create('��������� ������� ��������');
end;

procedure Tdm3.quDInvCheckBeforePost(DataSet: TDataSet);
begin
{  with quDInvCheck, Params do
  begin
   if FCHECKMode = 'DINV' then
     ByName['I_TYPE'].AsInteger:=2
   else if FCHECKMode = 'SRET' then
     ByName['I_TYPE'].AsInteger:=7
  end;  }
end;

end.
