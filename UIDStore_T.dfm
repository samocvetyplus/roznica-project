object fmUIDStore_T: TfmUIDStore_T
  Left = 256
  Top = 7
  Width = 719
  Height = 705
  Caption = #1048#1090#1086#1075#1086#1074#1099#1077' '#1089#1091#1084#1084#1099' '#1073#1077#1079' '#1091#1095#1077#1090#1072' '#1082#1086#1084#1080#1089#1089#1080#1080'.'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel
    Left = 8
    Top = 72
    Width = 97
    Height = 13
    Caption = #1048#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 144
    Top = 72
    Width = 48
    Height = 13
    Caption = 'lbCurrSum'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 272
    Top = 12
    Width = 87
    Height = 13
    Caption = #1042#1085#1091#1090#1088'. '#1086#1087#1090'. '#1087#1088#1080#1093'.:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 393
    Top = 14
    Width = 25
    Height = 13
    Caption = 'lbOpt'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object plSum: TPanel
    Left = 0
    Top = 41
    Width = 711
    Height = 616
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 0
    object grbxDep: TGroupBox
      Left = 2
      Top = 132
      Width = 707
      Height = 189
      Align = alTop
      Anchors = [akLeft, akRight]
      Caption = #1056#1086#1079#1085#1080#1094#1072' '
      TabOrder = 0
      object plSellOut: TPanel
        Left = 2
        Top = 116
        Width = 703
        Height = 71
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object gridSell2: TDBGridEh
          Left = 0
          Top = 0
          Width = 703
          Height = 71
          Align = alClient
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell2
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Checkboxes = False
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Title.Caption = #1043#1088#1091#1087#1087#1072
              Width = 84
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.Color = clWhite
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1057#1091#1084#1084#1072
              Width = 61
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1050#1086#1083'-'#1074#1086
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Title.Caption = #1042#1077#1089
              Width = 69
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object RxSplitter2: TRxSplitter
        Left = 2
        Top = 113
        Width = 703
        Height = 3
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
      end
      object plSellIn: TPanel
        Left = 2
        Top = 15
        Width = 703
        Height = 98
        Align = alTop
        BevelOuter = bvNone
        Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1099
        TabOrder = 2
        object gridSell: TDBGridEh
          Left = 0
          Top = 0
          Width = 703
          Height = 98
          Align = alClient
          AllowedOperations = []
          AllowedSelections = []
          AutoFitColWidths = True
          DataGrouping.GroupLevels = <>
          DataSource = dsrSell
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          FooterRowCount = 1
          FrozenCols = 1
          HorzScrollBar.Visible = False
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
          ReadOnly = True
          RowDetailPanel.Color = clBtnFace
          RowLines = 2
          SumList.Active = True
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          VertScrollBar.VisibleMode = sbNeverShowEh
          Columns = <
            item
              Color = clBtnFace
              EditButtons = <>
              FieldName = 'GROUPNAME'
              Footer.Value = #1048#1058#1054#1043#1054
              Footer.ValueType = fvtStaticText
              Footers = <>
              Width = 85
            end
            item
              EditButtons = <>
              FieldName = 'COST'
              Footer.FieldName = 'COST'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clMaroon
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 60
            end
            item
              EditButtons = <>
              FieldName = 'Q'
              Footer.FieldName = 'Q'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clGreen
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 51
            end
            item
              EditButtons = <>
              FieldName = 'W'
              Footer.FieldName = 'W'
              Footer.Font.Charset = DEFAULT_CHARSET
              Footer.Font.Color = clPurple
              Footer.Font.Height = -11
              Footer.Font.Name = 'MS Sans Serif'
              Footer.Font.Style = []
              Footer.ValueType = fvtSum
              Footers = <>
              Width = 68
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
    object grbxMain: TGroupBox
      Left = 2
      Top = 2
      Width = 707
      Height = 127
      Align = alTop
      Caption = #1054#1073#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077
      TabOrder = 1
      object lbInCap: TLabel
        Left = 12
        Top = 12
        Width = 185
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbIn: TLabel
        Left = 204
        Top = 12
        Width = 17
        Height = 13
        Caption = 'lbIn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInvCap: TLabel
        Left = 348
        Top = 12
        Width = 182
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1079#1072' '#1086#1090#1095#1077#1090#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbInv: TLabel
        Left = 544
        Top = 12
        Width = 23
        Height = 13
        Caption = 'lbInv'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object gridIn: TDBGridEh
        Left = 12
        Top = 28
        Width = 309
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrIn
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.Value = '999'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridSInv: TDBGridEh
        Left = 348
        Top = 28
        Width = 332
        Height = 93
        AllowedOperations = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInv
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 58
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object RxSplitter1: TRxSplitter
      Left = 2
      Top = 129
      Width = 707
      Height = 3
      ControlFirst = grbxMain
      Align = alTop
      BevelInner = bvLowered
      BevelOuter = bvNone
    end
    object RxSplitter3: TRxSplitter
      Left = 2
      Top = 321
      Width = 707
      Height = 3
      ControlFirst = grbxDep
      Align = alTop
      BevelOuter = bvLowered
    end
    object grbxCurr: TGroupBox
      Left = 2
      Top = 324
      Width = 707
      Height = 290
      Align = alClient
      Caption = #1054#1087#1090', '#1074#1086#1079#1074#1088#1072#1090', '#1080#1089#1093#1086#1076#1103#1097#1072#1103' '#1089#1091#1084#1084#1072
      TabOrder = 4
      object Label1: TLabel
        Left = 20
        Top = 237
        Width = 84
        Height = 13
        Caption = #1054#1087#1090'. '#1087#1088#1080#1093'. '#1094#1077#1085#1072': '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbOptIn: TLabel
        Left = 160
        Top = 237
        Width = 34
        Height = 13
        Caption = 'lbOptIn'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label2: TLabel
        Left = 256
        Top = 237
        Width = 81
        Height = 13
        Caption = #1054#1087#1090'. '#1088#1072#1089#1093'. '#1094#1077#1085#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbOptOut: TLabel
        Left = 203
        Top = 237
        Width = 42
        Height = 13
        Caption = 'lbOptOut'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbRetSeller: TLabel
        Left = 160
        Top = 256
        Width = 51
        Height = 13
        Caption = 'lbRetSeller'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label3: TLabel
        Left = 20
        Top = 256
        Width = 135
        Height = 13
        Caption = #1042#1054#1047#1042#1056#1040#1058' ('#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084'): '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbCurr: TLabel
        Left = 12
        Top = 12
        Width = 183
        Height = 13
        Caption = #1057#1091#1084#1084#1072' '#1085#1072' '#1082#1086#1085#1077#1094' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbCurrSum: TLabel
        Left = 198
        Top = 12
        Width = 48
        Height = 13
        Caption = 'lbCurrSum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbFSum: TLabel
        Left = 464
        Top = 12
        Width = 35
        Height = 13
        Caption = 'lbFSum'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 360
        Top = 12
        Width = 105
        Height = 13
        Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1072#1103' '#1089#1091#1084#1084#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 20
        Top = 272
        Width = 69
        Height = 13
        Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lbActAllowances: TLabel
        Left = 160
        Top = 272
        Width = 78
        Height = 13
        Caption = 'lbActAllowances'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object gridOut: TDBGridEh
        Left = 12
        Top = 30
        Width = 309
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrOut
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridMinus: TDBGridEh
        Left = 363
        Top = 143
        Width = 333
        Height = 98
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        BorderStyle = bsNone
        DataGrouping.GroupLevels = <>
        DataSource = dsrOutDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 59
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object gridPlus: TDBGridEh
        Left = 19
        Top = 143
        Width = 333
        Height = 98
        AllowedOperations = [alopAppendEh]
        AllowedSelections = []
        AutoFitColWidths = True
        BorderStyle = bsNone
        DataGrouping.GroupLevels = <>
        DataSource = dsrSInvDetail
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'NAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 81
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 59
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBGridEh1: TDBGridEh
        Left = 364
        Top = 30
        Width = 333
        Height = 93
        AllowedOperations = []
        AllowedSelections = []
        AutoFitColWidths = True
        DataGrouping.GroupLevels = <>
        DataSource = dsrF
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        HorzScrollBar.Visible = False
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghClearSelection, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        TitleLines = 1
        VertScrollBar.VisibleMode = sbNeverShowEh
        Columns = <
          item
            Color = clBtnFace
            EditButtons = <>
            FieldName = 'GROUPNAME'
            Footer.Value = #1048#1058#1054#1043#1054
            Footer.ValueType = fvtStaticText
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'COST'
            Footer.FieldName = 'COST'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clMaroon
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clGreen
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clPurple
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 68
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object stbrStatus: TStatusBar
    Left = 0
    Top = 657
    Width = 711
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 711
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 643
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
  end
  object fmstrTSum: TFormStorage
    IniFileName = 'jew.ini'
    IniSection = 'TfrTSum'
    UseRegistry = False
    StoredProps.Strings = (
      'plSellOut.Height'
      'plSum.Height'
      'grbxMain.Height'
      'grbxCurr.Height'
      'grbxCurr.Width'
      'grbxDep.Height'
      'grbxDep.Width')
    StoredValues = <>
    Left = 532
    Top = 9
  end
  object taIn: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select count(ENTRYNUM) Q, sum(W) W, sum(ENTRYCOST) COST, GR GROU' +
        'PNAME'
      'from UID_Store'
      'where SINVID=:INVID '
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 12
    Top = 92
    object taInQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taInW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taInCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taInGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrIn: TDataSource
    DataSet = taIn
    Left = 12
    Top = 140
  end
  object taSInvDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1055#1086#1089#1090#1072#1074#1082#1072#39' as char(20)) NAME, count(*) Q, sum(W) W, ' +
        'sum(SINVCOST) COST'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SINVNUM<>0 '
      'union all'
      
        'select cast('#39#1056#1086#1079#1085'.'#1074#1086#1079#1074#1088#1072#1090#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(RETCOST) COST'
      'from UID_store'
      'where SINVID=:INVID and'
      '      RETNUM<>0 '
      'union all'
      
        'select cast('#39#1054#1087#1090'.'#1074#1086#1079#1074#1088#1072#1090#39' as char(20)) NAME, count(*) Q, sum(W) ' +
        'W, sum(OPTRETCOST) COST'
      'from UID_store'
      'where SINVID=:INVID and'
      '      OPTRETNUM<>0 '
      'union all'
      
        'select cast('#39#1048#1079#1083#1080#1096#1082#1080#39' as char(20)) NAME, count(*) Q, sum(W) W, s' +
        'um(SURPLUSCOST) COST'
      'from UID_store'
      'where SINVID=:INVID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 148
    Top = 504
    object taSInvDetailQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSInvDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object taSInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, '
      '          sum(SINVCOST)+ '
      '          sum(RETCOST)+'
      '          sum(OPTRETCOST)+'
      '          sum(SURPLUSCOST) COST, '
      '          GR GROUPNAME'
      'from UID_Store'
      'where SINVID=:INVID and'
      '     (SINVNUM<>0 or OPTRETNUM<>0 or'
      '      RETNUM<>0 or SURPLUSNUM<>0)'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 372
    Top = 72
    object taSInvQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSInvW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSInvCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSInvGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrSInvDetail: TDataSource
    DataSet = taSInvDetail
    Left = 148
    Top = 552
  end
  object dsrSInv: TDataSource
    DataSet = taSInv
    Left = 372
    Top = 120
  end
  object taOut: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select count(RESNUM) Q, sum(W) W, sum(RESCOST) COST, GR GROUPNAM' +
        'E'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      RESCOST<>0'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 16
    Top = 480
    object taOutQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taOutW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taOutGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrOut: TDataSource
    DataSet = taOut
    Left = 20
    Top = 532
  end
  object taSell: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, sum(SELLCOST) COST, '
      '          GR GROUPNAME'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '           SELLNUM<>0 '
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 76
    Top = 264
    object taSellQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSellW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSellCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSellGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object taSell2: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub(0)')
    SelectSQL.Strings = (
      'select count(*) Q, sum(W) W, sum(SELLCOST2) COST, '
      '          GR GROUPNAME'
      ''
      'from UID_Store'
      'where SINVID=:INVID and'
      '           SELLNUM<>0 '
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 372
    object taSell2Q: TIntegerField
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSell2W: TFloatField
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taSell2COST: TFloatField
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
    object taSell2GROUPNAME: TFIBStringField
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrSell: TDataSource
    DataSet = taSell
    Left = 136
    Top = 264
  end
  object taSellDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(u.W) W, sum(u.SELLCOST) COST, '
      '          u.GR GROUPNAME, u.DEPID, sum(u.SELLCOST2) COST1'
      'from UID_Store u,  D_Dep  d'
      'where u.SINVID=:INVID and'
      '      u.SELLNUM<>0 and'
      '      u.DepID = d.D_DepID and'
      '      d.ISDELETE <>1  '
      'group by GR, DEPID')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 264
    object taSellDepQ: TIntegerField
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taSellDepW: TFloatField
      FieldName = 'W'
      ProviderFlags = []
    end
    object taSellDepCOST: TFloatField
      FieldName = 'COST'
      ProviderFlags = []
    end
    object taSellDepGROUPNAME: TFIBStringField
      FieldName = 'GROUPNAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSellDepDEPID: TIntegerField
      FieldName = 'DEPID'
      ProviderFlags = []
    end
    object taSellDepCOST1: TFloatField
      FieldName = 'COST1'
      ProviderFlags = []
    end
  end
  object taDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select D_DEPID, SNAME'
      'from D_Dep'
      'where D_DEPID<>-1000 and ISDELETE <> 1'
      'order by SORTIND')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 20
    Top = 312
    object taDepD_DEPID: TIntegerField
      FieldName = 'D_DEPID'
      Origin = 'D_DEP.D_DEPID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object taDepSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrSell2: TDataSource
    DataSet = taSell2
    Left = 76
    Top = 372
  end
  object qutmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 472
    Top = 524
  end
  object taOutDetail: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast('#39#1056#1086#1079#1085'.'#1087#1088#1086#1076#1072#1078#1080#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(SELLCOST) COST'
      'from UID_store'
      'where SINVID=:INVID and'
      '      SELLNUM<>0'
      'union all'
      
        'select cast('#39#1054#1087#1090'.'#1087#1088#1086#1076#1072#1078#1080#39' as char(20)) NAME, count(*) Q, sum(W) ' +
        'W, sum(OPTSELLCOST) COST'
      'from UID_store'
      'where SINVID=:INVID and'
      '      OPTSELLNUM<>0'
      'union all'
      
        'select cast('#39#1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084#39' as char(20)) NAME, count(*) Q,' +
        ' sum(W) W, sum(SINVRETCOST) COST'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      SINVRETNUM<>0'
      'union all'
      
        'select cast('#39#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103#39' as char(20)) NAME, count(*) Q, sum(W)' +
        ' W, sum(ACTALLOWANCESCOST) COST'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      ACTALLOWANCESNUM<>0')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 380
    Top = 504
    object taOutDetailNAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'NAME'
      ProviderFlags = []
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutDetailQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      ProviderFlags = []
    end
    object taOutDetailW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      ProviderFlags = []
      DisplayFormat = '0.##'
    end
    object taOutDetailCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      ProviderFlags = []
      currency = True
    end
  end
  object dsrOutDetail: TDataSource
    DataSet = taOutDetail
    Left = 384
    Top = 552
  end
  object frIn: TfrDBDataSet
    DataSet = taIn
    Left = 64
    Top = 92
  end
  object acEvent: TActionList
    Left = 128
    Top = 12
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintExecute
    end
  end
  object frSell: TfrDBDataSet
    DataSet = taSell
    Left = 76
    Top = 312
  end
  object frSell2: TfrDBDataSet
    DataSet = taSell2
    Left = 20
    Top = 412
  end
  object frOut: TfrDBDataSet
    DataSet = taOut
    Left = 60
    Top = 481
  end
  object frSInv: TfrDBDataSet
    DataSet = taSInv
    Left = 424
    Top = 72
  end
  object frSInvDetail: TfrDBDataSet
    DataSet = taSInvDetail
    Left = 220
    Top = 504
  end
  object frOutDetail: TfrDBDataSet
    DataSet = taOutDetail
    Left = 432
    Top = 504
  end
  object taF: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(F) Q, sum(W) W, sum(FCOST) COST, GR GROUPNAME'
      'from UID_Store'
      'where SINVID=:INVID and'
      '      FCOST<>0'
      'group by GR')
    BeforeOpen = BeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Description = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1085#1072#1083#1080#1095#1080#1077
    Left = 412
    Top = 384
    poSQLINT64ToBCD = True
    object taFQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taFW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taFCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taFGROUPNAME: TFIBStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldName = 'GROUPNAME'
      EmptyStrToNull = True
    end
  end
  object dsrF: TDataSource
    DataSet = taF
    Left = 460
    Top = 384
  end
  object frF: TfrDBDataSet
    DataSet = taF
    Left = 412
    Top = 428
  end
end
