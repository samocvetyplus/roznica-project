object fmAccess: TfmAccess
  Left = 360
  Top = 301
  AutoSize = True
  ClientHeight = 472
  ClientWidth = 409
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 28
    Width = 96
    Height = 13
    Caption = #1048#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 10
    Top = 55
    Width = 38
    Height = 13
    Caption = #1055#1072#1088#1086#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 0
    Top = 0
    Width = 401
    Height = 17
    AutoSize = False
  end
  object BitBtn1: TBitBtn
    Left = 304
    Top = 21
    Width = 79
    Height = 23
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 304
    Top = 51
    Width = 79
    Height = 22
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object edUserName: TDBEdit
    Left = 156
    Top = 23
    Width = 94
    Height = 21
    Color = clInfoBk
    DataField = 'ALIAS'
    DataSource = dmCom.dsEmp
    TabOrder = 2
  end
  object edPswd: TDBEdit
    Left = 156
    Top = 51
    Width = 94
    Height = 21
    Color = clInfoBk
    DataField = 'PSWD'
    DataSource = dmCom.dsEmp
    Enabled = False
    PasswordChar = '*'
    TabOrder = 3
  end
  object rgPrmissions: TRadioGroup
    Left = 8
    Top = 78
    Width = 247
    Height = 91
    Caption = #1055#1088#1072#1074#1072' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Items.Strings = (
      #1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088
      #1052#1077#1085#1077#1076#1078#1077#1088
      #1055#1088#1086#1076#1072#1074#1077#1094
      #1041#1091#1093#1075#1072#1083#1090#1077#1088)
    ParentFont = False
    TabOrder = 4
    OnClick = rgPrmissionsClick
  end
  object pgrigth: TcxPageControl
    Left = 0
    Top = 175
    Width = 409
    Height = 297
    ActivePage = taballrigth
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    LookAndFeel.NativeStyle = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    Style = 3
    TabOrder = 5
    ClientRectBottom = 297
    ClientRectRight = 409
    ClientRectTop = 23
    object taballrigth: TcxTabSheet
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object M207DBBits1: TM207DBBits
        Left = 0
        Top = 56
        Width = 409
        Height = 217
        OnClickCheck = M207DBBits1ClickCheck
        Color = clInfoBk
        Columns = 2
        ItemHeight = 13
        Items.Strings = (
          #1055#1086#1089#1090#1072#1074#1082#1080
          #1062#1077#1085#1099
          #1057#1082#1083#1072#1076' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1072#1084
          #1057#1082#1083#1072#1076' '#1087#1086' '#1080#1079#1076#1077#1083#1080#1103#1084
          #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
          #1055#1088#1086#1076#1072#1078#1080
          #1057#1084#1077#1085#1072
          #1054#1087#1090#1086#1074#1099#1077' '#1087#1088#1086#1076#1072#1078#1080
          #1057#1090'. '#1054#1089#1090#1072#1090#1082#1080
          #1042#1086#1079#1074#1088#1072#1090#1099'  '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
          #1042#1086#1079#1074#1088#1072#1090#1099' '#1086#1090' '#1086#1087#1090#1086#1074#1099#1093' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
          #1042#1086#1079#1074#1088#1072#1090#1099' '#1086#1090' '#1088#1086#1079#1085#1080#1095#1085#1099#1093' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
          #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
          #1048#1085#1092#1086#1088#1084#1072#1094#1080#1103
          #1054#1090#1095#1105#1090#1099
          #1053#1072#1089#1090#1088#1086#1081#1082#1080' '
          #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
          #1047#1072#1103#1074#1082#1080' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
          #1054#1089#1090#1072#1090#1082#1080
          #1069#1082#1089#1087#1086#1088#1090' '#1089' '#1086#1089#1090#1072#1090#1082#1072#1084#1080
          #1069#1082#1089#1087#1086#1088#1090' '#1073#1077#1079' '#1086#1089#1090#1072#1090#1082#1086#1074
          #1069#1082#1089#1087#1086#1088#1090' '#1089' '#1086#1089#1090#1072#1090#1082#1072#1084#1080' '#1073#1077#1079' '#1087#1077#1088#1077#1089#1095#1077#1090#1072
          #1047#1072#1103#1074#1082#1080' '#1085#1072' '#1092#1080#1083#1080#1072#1083#1099
          #1040#1082#1090#1099' '#1089#1087#1080#1089#1072#1085#1080#1103
          #1054#1073#1086#1088#1091#1076#1086#1074#1072#1085#1080#1077
          #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1093#1088#1072#1085#1077#1085#1080#1077
          #1056#1077#1084#1086#1085#1090
          #1054#1087#1083#1072#1090#1099
          #1043#1088#1072#1092#1080#1082' '#1086#1087#1083#1072#1090
          #1054#1090#1083#1086#1078#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103)
        TabOrder = 0
        DataField = 'ACC'
        DataSource = dmCom.dsEmp
      end
      object chballsell: TDBCheckBoxEh
        Left = 0
        Top = 32
        Width = 177
        Height = 17
        Caption = #1055#1086#1083#1085#1099#1081' '#1076#1086#1089#1090#1091#1087' '#1082' '#1087#1088#1086#1076#1072#1078#1077
        DataField = 'ALLSELL'
        DataSource = dmCom.dsEmp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBCheckBox2: TDBCheckBox
        Left = 0
        Top = 8
        Width = 217
        Height = 17
        Caption = #1056#1072#1073#1086#1090#1072' '#1089#1086' '#1074#1089#1077#1084#1080' '#1089#1082#1083#1072#1076#1072#1084#1080' '#1074' '#1089#1084#1077#1085#1077
        DataField = 'ALLWH'
        DataSource = dmCom.dsEmp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object tabRefBook: TcxTabSheet
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label6: TLabel
        Left = 8
        Top = 8
        Width = 122
        Height = 13
        Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072':'
      end
      object Label7: TLabel
        Left = 200
        Top = 8
        Width = 155
        Height = 13
        Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1072':'
      end
      object bitsVIEWREFBOOK: TM207DBBits
        Left = 0
        Top = 32
        Width = 193
        Height = 233
        Color = clInfoBk
        ItemHeight = 13
        Items.Strings = (
          #1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
          #1090#1086#1074#1072#1088#1086#1074
          #1076#1086#1087'. '#1080#1085#1092#1086#1088'.'
          #1074#1089#1090#1072#1074#1086#1082
          #1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
          #1072#1088#1090#1080#1082#1091#1083#1086#1074
          #1053#1044#1057
          #1086#1075#1088#1072#1085#1082#1080
          #1074#1080#1076' '#1086#1087#1083#1072#1090
          #1089#1082#1080#1076#1086#1082
          #1087#1088#1080#1095#1080#1085' '#1074#1086#1079#1074#1088#1072#1090#1072
          #1082#1083#1080#1077#1085#1090#1086#1074
          #1089#1090#1088#1072#1085
          #1072#1076#1088#1077#1089#1086#1074
          #1072#1090#1088#1080#1073#1091#1090#1086#1074' 1'
          #1072#1090#1088#1080#1073#1091#1090#1086#1074' 2'
          #1086#1073#1086#1079#1085#1072#1095'. '#1087#1086#1089#1090#1072#1074'.'
          #1094#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072)
        TabOrder = 0
        DataField = 'VIEWREFBOOK'
        DataSource = dmCom.dsEmp
      end
      object dbbtEDITREFBOOK: TM207DBBits
        Left = 192
        Top = 32
        Width = 217
        Height = 233
        ItemHeight = 13
        Items.Strings = (
          #1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
          #1090#1086#1074#1072#1088#1086#1074
          #1076#1086#1087'. '#1080#1085#1092#1086#1088'.'
          #1074#1089#1090#1072#1074#1086#1082
          #1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
          #1072#1088#1090#1080#1082#1091#1083#1086#1074
          #1053#1044#1057
          #1086#1075#1088#1072#1085#1082#1080
          #1074#1080#1076' '#1086#1087#1083#1072#1090
          #1089#1082#1080#1076#1086#1082
          #1087#1088#1080#1095#1080#1085' '#1074#1086#1079#1074#1088#1072#1090#1072
          #1082#1083#1080#1077#1085#1090#1086#1074
          #1089#1090#1088#1072#1085
          #1072#1076#1088#1077#1089#1086#1074
          #1072#1090#1088#1080#1073#1091#1090#1086#1074' 1'
          #1072#1090#1088#1080#1073#1091#1090#1086#1074' 2'
          #1086#1073#1086#1079#1085#1072#1095'. '#1087#1086#1089#1090#1072#1074'.'
          #1094#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072)
        TabOrder = 1
        DataField = 'EDITREFBOOK'
        DataSource = dmCom.dsEmp
      end
    end
  end
  object btPswd: TButton
    Left = 304
    Top = 119
    Width = 81
    Height = 33
    Caption = #1057#1084#1077#1085#1072' '#1087#1072#1088#1086#1083#1103
    TabOrder = 6
    OnClick = btPswdClick
  end
end
