unit RInv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid, ExtCtrls, 
  ComCtrls, DBCtrls, StdCtrls, ActnList, jpeg, rxPlacemnt,
  rxSpeedbar;

type
  TfmRInv = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    M207IBGrid1: TM207IBGrid;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    DBText7: TDBText;
    SpeedItem1: TSpeedItem;
    acList: TActionList;
    acView: TAction;
    acClose: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
  private
    { Private declarations }
    LogOprIdForm:string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmRInv: TfmRInv;

implementation

uses Data, Data2, comdata, SItem, M207Proc, data3, jewconst;

{$R *.DFM}

procedure TfmRInv.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRInv.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dm, dm2 do
    begin
      taSEl.Active:=True
    end;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmRInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dm2 do
    begin
      taSEl.Active:=False;
    end;
end;

procedure TfmRInv.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRInv.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmRInv.acViewExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation(aLog_ViewRinv,LogOprIdForm);
  with dm do
    begin
      PostDataSet(taSEl);
      if NOT taSElSElId.IsNull then
          ShowAndFreeForm(TfmSItem, Self, TForm(fmSItem), True, False);
    end;
  dm3.update_operation(LogOperationID);
end;

end.
