object fmSuspItem: TfmSuspItem
  Left = 241
  Top = 103
  Width = 739
  Height = 480
  Caption = #1054#1090#1083#1086#1078#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 731
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 2
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      Action = acHelp
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B393939FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B397B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B397B7B397B7B397B7B007B7B007B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD00393900
        7B7B007B7B007B7B003939848484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF7B7B7B7BFFFF7BFFFF7BFFFF003939397B7B9C9C9CFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7BFFFF7B
        FFFF7BFFFF007B7B007B7B9C9C9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFBDBDBD7BBDBD7BFFFF7BFFFF7BBDBD003939848484FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF397B7B7B
        FFFF7BFFFF7BFFFF7BBDBD6363639C9C9CFF00FFFF00FFFF00FFFF00FFFF00FF
        393939397B7B397B7B7B7B7BFF00FF397B7B7BFFFF7BFFFF7BFFFF0039396363
        639C9C9CFF00FFFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7B7B7BFF00FFFF
        00FF397B7B7BFFFF7BFFFF397B7B007B7B848484FF00FFFF00FFFF00FFFF00FF
        397B7B7BFFFF7BFFFF397B7BFF00FFBDBDBD39BDBD7BFFFF7BFFFF397B7B00FF
        FF424242BDBDBDFF00FFFF00FFFF00FF397B7B7BFFFF7BFFFF7BFFFF397B7B39
        BDBD7BFFFF7BFFFF7BFFFF397B7B00FFFF424242BDBDBDFF00FFFF00FFFF00FF
        BDBDBD7BBDBD7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF7BFFFF39BDBD00BDBD00FF
        FF424242BDBDBDFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B7BFFFF7BFFFF7B
        FFFF7BFFFF397B7B00BDBD00FFFF007B7B848484FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFBDBDBD003939007B7B007B7B007B7B00FFFF00FFFF00BDBD6363
        63DEDEDEFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBDBDBD397B7B00
        7B7B007B7B007B7B215A5A7B7B7BDEDEDEFF00FFFF00FFFF00FF}
      Hint = #1057#1087#1088#1072#1074#1082#1072
      ImageIndex = 73
      Spacing = 1
      Left = 602
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 666
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
  end
  object plInfo: TPanel
    Left = 0
    Top = 41
    Width = 731
    Height = 56
    Align = alTop
    TabOrder = 1
    object LNumInv: TLabel
      Left = 8
      Top = 8
      Width = 68
      Height = 13
      Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object LUser: TLabel
      Left = 264
      Top = 8
      Width = 73
      Height = 13
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
    end
    object Luid: TLabel
      Left = 8
      Top = 32
      Width = 62
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edSn: TDBEdit
      Left = 92
      Top = 5
      Width = 121
      Height = 21
      DataField = 'SN'
      DataSource = dm3.dsSuspItemList
      TabOrder = 0
    end
    object dbUser: TDBLookupComboBox
      Left = 342
      Top = 4
      Width = 203
      Height = 21
      DataField = 'USERID'
      DataSource = dm3.dsSuspItemList
      DropDownRows = 12
      KeyField = 'D_EMPID'
      ListField = 'FIO'
      ListSource = dmCom.dsEmp
      TabOrder = 1
    end
    object edUid: TEdit
      Left = 92
      Top = 28
      Width = 121
      Height = 21
      TabOrder = 2
      OnKeyDown = edUidKeyDown
      OnKeyPress = edUidKeyPress
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 97
    Width = 731
    Height = 354
    Align = alClient
    AllowedOperations = []
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsSuspItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'PRORDCODE'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1048#1079#1075'.'
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'D_MATID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1052#1072#1090'.'
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'D_GOODID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1053#1072#1080#1084'. '#1080#1079#1076'.'
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'D_INSID'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1054#1042
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083'||'#1040#1088#1090#1080#1082#1091#1083
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'UNIT'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.'
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'D_COUNTRYID'
        Footers = <>
        Title.Caption = #1057#1090#1088'.'
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'UID'
        Footer.FieldName = 'UID'
        Footer.ValueType = fvtCount
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Width = 63
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1077#1089
        Width = 38
      end
      item
        Color = clWhite
        EditButtons = <>
        FieldName = 'SPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1087#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'PRICE2'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1088#1072#1089#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.FieldName = 'SCOST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1087#1088#1080#1093'.'
      end
      item
        EditButtons = <>
        FieldName = 'COST2'
        Footer.FieldName = 'COST2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1088#1072#1089#1093'.'
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 280
    Top = 240
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 456
    Top = 216
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1077
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      ShortCut = 27
      OnExecute = acCloseExecute
    end
    object acHelp: TAction
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072
      ImageIndex = 73
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
      OnUpdate = acPrintUpdate
    end
  end
  object prdg1: TPrintDBGridEh
    DBGridEh = dg1
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 136
    Top = 184
  end
end
