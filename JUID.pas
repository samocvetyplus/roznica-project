unit JUID;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, StdCtrls, rxPlacemnt, rxSpeedbar;

type
  TfmJUID = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitCalc: TSpeedItem;
    stbrJUID: TStatusBar;
    gr: TM207IBGrid;
    fmstrJUID: TFormStorage;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    spExport: TSpeedItem;
    svdFile: TOpenDialog;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure spitCalcClick(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitPrintClick(Sender: TObject);
    procedure spitPeriodClick(Sender: TObject);
    procedure spExportClick(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  public

  end;

var
  fmJUID: TfmJUID;

implementation

uses comdata, Data, ServData, M207Proc, ReportData, Period, RxDateUtil, RxStrUtils, MsgDialog;

{$R *.DFM}

procedure TfmJUID.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmJUID.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmJUID.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;
  stbrJUID.Panels[0].Text := dmCom.UserName;
  dmServ.L3BD:=dmCom.User.L3BD;
  dmServ.L3ED:=dmCom.User.L3ED;  
  ShowPeriod;
  with dmServ.taJUID do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    Open;
    Transaction.CommitRetaining;
  end;
end;

procedure TfmJUID.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmJUID.spitCalcClick(Sender: TObject);
begin
  Application.Minimize;
  with dmServ.quTmp do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    SQL.Text := 'execute procedure MainDocUID('+IntToStr(dmCom.UserId)+', '#39+
      DateToStr(dmServ.L3BD)+#39', '#39+DateToStr(dmServ.L3ED)+#39', -100, 500, 311)';
    ExecQuery;
    Transaction.Commit;
  end;
  with dmServ.taJUID do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    Open;
    Transaction.CommitRetaining;
  end;
  Application.Restore;
end;

procedure TfmJUID.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not HighLight or gr.ClearHighlight then
  if (Field.FieldName='DEBTORS')or
     (Field.FieldName='DEBTORW')or
     (Field.FieldName='DEBTORQ')then begin Background := dm.CU_SBD1; exit; end;

  if (Field.FieldName='SALES')or
     (Field.FieldName='SALEW')or
     (Field.FieldName='SALEQ') then begin Background := dm.CU_SL; exit; end;

  if (Field.FieldName='OPTS')or
     (Field.FieldName='OPTW')or
     (Field.FieldName='OPTQ') then begin Background := dm.CU_SO1; exit; end;

  if (Field.FieldName='RETOPTS')or
     (Field.FieldName='RETOPTW')or
     (Field.FieldName='RETOPTQ')then begin Background := dm.CU_RO1; exit; end;

  if (Field.FieldName='RETS')or
     (Field.FieldName='RETW')or
     (Field.FieldName='RETQ')then begin Background := dm.CU_RT1; exit; end;

  if (Field.FieldName='RETVENDORS')or
     (Field.FieldName='RETVENDORW')or
     (Field.FieldName='RETVENDORQ')then Background := dm.CU_SR1;
end;

procedure TfmJUID.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmServ.taJUID]);
  dmServ.taJUID.Transaction.Commit;
  Action := caFree;
end;

procedure TfmJUID.spitPrintClick(Sender: TObject);
begin
  dmReport.PrintDocumentB(maindoc);
end;

procedure TfmJUID.spitPeriodClick(Sender: TObject);
var
  d1, d2 : TDateTime;
begin
  d1 := dmServ.L3BD;
  d2 := dmServ.L3ED;
  if GetPeriod(d1, d2) then
  begin
    dmServ.L3BD := d1;
    dmServ.L3ED := d2;
    ShowPeriod;
  end;
end;

procedure TfmJUID.ShowPeriod;
begin
  if(dmServ.L3BD = StrToDate('05.02.1981'))or(not ValidDate(dmServ.L3BD)or
    (not ValidDate(dmServ.L3ED))) then laPeriod.Caption := '������ �� ���������';
  laPeriod.Caption := 'C '+DateToStr(dmServ.L3BD)+' �� '+DateToStr(dmServ.L3ED);//Com.User.L3ED);
end;

procedure TfmJUID.spExportClick(Sender: TObject);
var
  ftext:textfile;
  sDir, Path:string;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  svdFile.InitialDir:=sDir;
  svdFile.FileName:=ReplaceStr(datetostr(dmCom.GetServerTime),'.','_');
  try
 //�������� � ��������� ����
   if svdFile.Execute then
    begin
     Path:=svdFile.FileName;
     AssignFile(ftext, Path);
     try
       Rewrite(ftext);
       writeln(ftext,datetostr(dmServ.L3BD));
       writeln(ftext,datetostr(dmServ.L3ED));
       with dmServ.taJUID do
         begin
          First;
          while not EOf do
            begin                     (* 1          2           3          4            5           6*)
              writeln(ftext,format('%10d %10s %10s %15.2f %10d %10d %10d %10d %10d %10d %10d',
              [FieldByName('UID_SUP').asInteger, ExtractWord(1,FieldByName('ART').asString,[' ']),FieldByName('SZ').asString,
              FieldByName('INW').asFloat,FieldByName('INQ').asInteger,
              FieldByName('DEBTORQ').asInteger{+FieldByName('RETOPTQ').asInteger},
              FieldByName('SALEQ').asInteger{+FieldByName('OPTQ').asInteger},
              FieldByName('RETQ').asInteger, FieldByName('RETVENDORQ').asInteger,
              FieldByName('OUTQ').asInteger,FieldByName('CURRQ').asInteger]));
              next;
             end;
        end;
      finally
         CloseFile(fText);
      end;
    end;
  finally
    MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
  end;
end;

end.
