unit VisCol;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls, dbgrids, RXSpin, RxDbCtrl,
  Mask, rxPlacemnt;

type
  TfmVisCol = class(TForm)
    fs1: TFormStorage;
    lb1: TCheckListBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    seFixCol: TRxSpinEdit;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmVisCol: TfmVisCol;

procedure SetVisCol(g: TRxDBGrid);

implementation

{$R *.DFM}

procedure SetVisCol(g: TRxDBGrid);
var i: integer;
begin
  try
    fmVisCol:=TfmVisCol.Create(NIL);

    with fmVisCol.lb1 do
      begin
        for i:=0 to g.Columns.Count-1 do
          begin
            Items.Add(g.Columns[i].Title.Caption);
            Checked[i]:=g.Columns[i].Visible;
          end;
      end;

    with fmVisCol do
      begin
        seFixCol.MinValue:=0;
        seFixCol.MaxValue:=g.Columns.Count;
        seFixCol.Value:=g.FixedCols;
      end;

    if fmVisCol.ShowModal=mrOK then
      begin
        for i:=0 to g.Columns.Count-1 do
          begin
            g.Columns[i].Visible:=fmVisCol.lb1.Checked[i];
          end;
        g.FixedCols:=Round(fmVisCol.seFixCol.Value);  
      end;
  finally
    fmVisCol.Free;
  end;
end;


end.
