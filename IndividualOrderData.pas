unit IndividualOrderData;

interface

uses
  SysUtils, Classes, DB, DBAccess, MyAccess, MemDS, FIBDataSet, pFIBDataSet,
  Forms, Controls, IniFiles, Dialogs, uUtils, ShlObj, ImgList, cxGraphics;

type
  TdmIndividualOrder = class(TDataModule)
    Connection: TMyConnection;
    dtOrder: TMyQuery;
    dsOrder: TMyDataSource;
    dtHistory: TMyQuery;
    dsHistory: TMyDataSource;
    dtHistoryID_ORDER: TIntegerField;
    dtHistoryNAME: TStringField;
    dtHistoryID_STATE: TIntegerField;
    dtHistoryEMP_STATE: TStringField;
    dtHistoryDATE_STATE: TDateTimeField;
    dtHistoryNUMBER_DOC: TIntegerField;
    dtHistoryDATE_DOC: TStringField;
    dtHistoryCOMMENT: TStringField;
    dtHistoryPRICE: TFloatField;
    dtOrderID_ORDER: TIntegerField;
    dtOrderFIO: TStringField;
    dtOrderPHONE: TStringField;
    dtOrderDEP_SEND: TStringField;
    dtOrderART: TStringField;
    dtOrderCOMP: TStringField;
    dtOrderGOOD: TStringField;
    dtOrderMAT: TStringField;
    dtOrderSZ: TStringField;
    dtOrderEMP_CREATE: TStringField;
    dtOrderDATE_CREATE: TDateTimeField;
    dtOrderDEP_CUSTOM: TStringField;
    dtOrderNAME: TStringField;
    dtOrderID_STATE: TIntegerField;
    dtOrderTYPE_ORDER: TIntegerField;
    dtOrderEMP_STATE: TStringField;
    dtOrderDATE_STATE: TDateTimeField;
    dtOrderNUMBER_DOC: TIntegerField;
    dtOrderDATE_DOC: TStringField;
    dtOrderCOMMENT: TStringField;
    dtOrderPRICE: TFloatField;
    Command: TMyCommand;
    dtOrderDocumentName: TStringField;
    dtHistoryDocumentName: TStringField;
    dtOrder_in: TMyQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    DateTimeField1: TDateTimeField;
    StringField10: TStringField;
    StringField11: TStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    StringField12: TStringField;
    DateTimeField2: TDateTimeField;
    IntegerField4: TIntegerField;
    StringField13: TStringField;
    StringField14: TStringField;
    FloatField1: TFloatField;
    StringField15: TStringField;
    Sourse_COMP: TDataSource;
    COMP_NAME_S: TpFIBDataSet;
    COMP_NAME_SNAME: TFIBStringField;
    dtOrderDEL: TIntegerField;
    dtCenter: TMyQuery;
    IntegerField15: TIntegerField;
    StringField46: TStringField;
    StringField47: TStringField;
    StringField48: TStringField;
    StringField49: TStringField;
    StringField50: TStringField;
    StringField51: TStringField;
    StringField52: TStringField;
    StringField53: TStringField;
    StringField54: TStringField;
    DateTimeField7: TDateTimeField;
    StringField55: TStringField;
    StringField56: TStringField;
    IntegerField16: TIntegerField;
    IntegerField17: TIntegerField;
    StringField57: TStringField;
    DateTimeField8: TDateTimeField;
    IntegerField18: TIntegerField;
    StringField58: TStringField;
    StringField59: TStringField;
    FloatField4: TFloatField;
    StringField60: TStringField;
    IntegerField19: TIntegerField;
    dtOrder_factory: TMyQuery;
    IntegerField10: TIntegerField;
    StringField31: TStringField;
    StringField32: TStringField;
    StringField33: TStringField;
    StringField34: TStringField;
    StringField35: TStringField;
    StringField36: TStringField;
    StringField37: TStringField;
    StringField38: TStringField;
    StringField39: TStringField;
    DateTimeField5: TDateTimeField;
    StringField40: TStringField;
    StringField41: TStringField;
    IntegerField11: TIntegerField;
    IntegerField12: TIntegerField;
    StringField42: TStringField;
    DateTimeField6: TDateTimeField;
    IntegerField13: TIntegerField;
    StringField43: TStringField;
    StringField44: TStringField;
    FloatField3: TFloatField;
    StringField45: TStringField;
    IntegerField14: TIntegerField;
    dtOrder_factoryINFO_ORDER: TStringField;
    dtOrder_factoryPREPAY: TFloatField;
    dtOrder_factoryTIME_ORDER: TStringField;
    dtOrder_off: TMyQuery;
    IntegerField5: TIntegerField;
    StringField16: TStringField;
    StringField17: TStringField;
    StringField18: TStringField;
    StringField19: TStringField;
    StringField20: TStringField;
    StringField21: TStringField;
    StringField22: TStringField;
    StringField23: TStringField;
    StringField24: TStringField;
    DateTimeField3: TDateTimeField;
    StringField25: TStringField;
    StringField26: TStringField;
    IntegerField6: TIntegerField;
    IntegerField7: TIntegerField;
    StringField27: TStringField;
    DateTimeField4: TDateTimeField;
    IntegerField8: TIntegerField;
    StringField28: TStringField;
    StringField29: TStringField;
    FloatField2: TFloatField;
    StringField30: TStringField;
    IntegerField9: TIntegerField;
    dtOrder_offINFO_ORDER: TStringField;
    dtOrder_offPREPAY: TFloatField;
    dtOrder_offTIME_ORDER: TStringField;
    dtOrder_inID_PREVIEW: TIntegerField;
    dtOrder_factoryID_PREVIEW: TIntegerField;
    COUNT_PREWIEW: TMyQuery;
    COUNT_PREWIEWCOUNT_PREWIEW: TLargeintField;
    COUNT_PREWIEWDEP_SEND: TStringField;
    dtOrderART_FACTORY: TStringField;
    dtOrderUID: TIntegerField;
    dtOrder_factoryUID: TIntegerField;
    dtOrder_inUID: TIntegerField;
    GOOD_NAME_S: TpFIBDataSet;
    FIBStringField3: TFIBStringField;
    Sourse_GOOD: TDataSource;
    MAT_NAME_S: TpFIBDataSet;
    Sourse_MAT: TDataSource;
    MAT_NAME_SMAT: TFIBStringField;
    dtOrder_factoryART_FACTORY: TStringField;
    dtCenterART_FACTORY: TStringField;
    SELL_DataSet: TpFIBDataSet;
    DataSourse_List: TDataSource;
    DataSet_ART_SELL: TpFIBDataSet;
    DataSource_ART_SELL: TDataSource;
    dtOrder_factoryINS_FACTORY: TStringField;
    dtOrder_offART_FACTORY: TStringField;
    dtOrder_offINS_FACTORY: TStringField;
    dtOrderINS_FACTORY: TStringField;
    dtOrder_inART_FACTORY: TStringField;
    dtCenterINS_FACTORY: TStringField;
    COUNT_PREWIEWDEP_CUSTOM: TStringField;
    dtOrder_offCOST: TStringField;
    dtOrder_factoryCOST: TStringField;
    dtOrder_factoryREMARK: TIntegerField;
    dtOld_state: TMyQuery;
    dsOld_state: TMyDataSource;
    dtOld_stateID_ORDER: TIntegerField;
    dtOld_statemax_state: TIntegerField;
    dtOrder_offOLD_STATE: TStringField;
    dtState: TMyQuery;
    dsState: TMyDataSource;
    dtStateNAME: TStringField;
    dtOrderOLD_STATE: TStringField;
    dtOrder_inOLD_STATE: TStringField;
    dtCenterOLD_STATE: TStringField;
    dtOrder_factoryOLD_STATE: TStringField;
    dtHistoryUID: TIntegerField;
    DataSet_ART_SINV: TpFIBDataSet;
    DataSet_ART_SINVART: TFIBStringField;
    DataSet_ART_SINVUID: TFIBIntegerField;
    DataSource_ART_SINV: TDataSource;
    SINV_DataSet: TpFIBDataSet;
    SINV_DataSetSDATE: TFIBDateTimeField;
    SINV_DataSetSINVID: TFIBIntegerField;
    SINV_DataSetSN: TFIBIntegerField;
    SINV_DataSetSNAME: TFIBStringField;
    DataSourse_List1: TDataSource;
    Comment: TMyQuery;
    CommentCOMMENT: TStringField;
    ImageCalendar: TcxImageList;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure SetDepParam(DataSet: TDataSet);
    procedure CalcDocNameField(DataSet: TDataSet);
    procedure dtOrderNewRecord(DataSet: TDataSet);
    procedure dtHistoryNewRecord(DataSet: TDataSet);
    procedure dtOrderBeforePost(DataSet: TDataSet);
    procedure dtOrder_offBeforeOpen(DataSet: TDataSet);
  
  private
    DepartmentName: String;
    UserName: String;
  public
    mode: integer;
    CancelFlag : integer;
    BD, ED: TDate;
    { Public declarations }
  end;

var
  dmIndividualOrder: TdmIndividualOrder;
  INI: TIniFile;


implementation

{$R *.dfm}

uses  ComData, ind_order,Report, main;

procedure TdmIndividualOrder.CalcDocNameField(DataSet: TDataSet);
var
  DocumentName: String;
  StateID: SmallInt;
  OldMaxState, com: String;
begin
    if DataSet <> dthistory then begin
    
  StateID := DataSet.FieldByName('ID_STATE').AsInteger;

  DocumentName := '';

  case StateID of
    7: DocumentName := '��';
    9: DocumentName := '�����';
  end;

  if DocumentName = '' then
  begin
    DataSet.FieldByName('DocumentName').AsString := DocumentName;
  end else
  begin
    DataSet.FieldByName('DocumentName').AsString := DocumentName + ' � ' + DataSet.FieldByName('NUMBER_DOC').AsString +
                            ' �� ' + DataSet.FieldByName('DATE_DOC').AsString;
  end;




      OldMaxState:= ' ';

      //dtOld_state.ParamByName('id').AsString:= DataSet.FieldByName('ID_ORDER').AsString;
      dtOld_state.Open;
      dtOld_state.First;
      while not(dtOld_state.Eof) do

     if  dtOld_state.FieldByName('ID_ORDER').AsString=DataSet.FieldByName('ID_ORDER').AsString   then

     begin
//      dtState.Close;
//      dtState.ParamByName('id_state').AsInteger :=  dtOld_state.FieldByName('max_state').AsInteger;
//      dtState.Open;

//      if dtOld_state.FieldByName('max_state').AsString <> '' then
//      OldMaxState:=dtState.FieldByName('NAME').AsString;

       case dtOld_state.FieldByName('max_state').AsInteger of
    1: OldMaxState:=  '� ���������';
        2: OldMaxState:=  '�����������';
            3: OldMaxState:=  '������ �� ������';
                4: OldMaxState:=  '����� �����';
                    5: OldMaxState:=  '���������� � �����������';
                        6: OldMaxState:=  '����������� � �������������';
                            7: OldMaxState:=  '���������';
                                8: OldMaxState:=  '����� ��������';
                                    9: OldMaxState:=  '�������';
                                        10: OldMaxState:=  '����� ��� �������';
                 
  end;

           DataSet.FieldByName('OLD_STATE').AsString:= OldMaxState;
       exit;

        

     end else 
  dtOld_state.next;
    end;
//   dtOrder_factory.first;
//    while not dtOrder_factory.eof do
//     begin
//     comment.close;
//
//           comment.ParamByName('param').Value:= dtOrder_factory.FieldByName('id_order').AsInteger;
//           comment.open;
//
//           while not comment.eof do
//             begin
//             com:= com + commentcomment.AsString;
//            comment.next;
//             end;
//
//             dtOrder_factory.Edit;
//            dtOrder_factorycommplus.value:=com;
//            dtOrder_factory.post;
//           dtOrder_factory.Next;
//           com:='';
//       end;
end;



procedure TdmIndividualOrder.DataModuleCreate(Sender: TObject);
var fl, InDir :string;
begin
try
  InDir:=ExtractFilePath(Application.ExeName);
  INI := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'order.ini');
  fl:= inDir+'order.ini';
    if FileExists(Fl) then
    begin
      Connection.Server := INI.ReadString( 'order','Server','������ ��� ������!');
      Connection.Database := INI.ReadString( 'order','Database','������ ��� ������!');
      Connection.Username:= INI.ReadString( 'order','Username','������ ��� ������!');
      Connection.Password := INI.ReadString( 'order','Password','������ ��� ������!');
    end;

  DepartmentName := dmCom.HereName;

  UserName := dmCom.UserName;

  Screen.Cursor := crHourGlass;

  ED:=now;

  BD := ( strtodate ( (FormatDateTime('dd/mm',ED)) + (FormatDateTime('/yyyy', Date)) )        - 30);

 // try

   // Connection.Connected := true;

   if  Connection.Connected then
     Begin
   Connection.StartTransaction;
    try
    dtOrder.Open;
    except
     dtOrder.Close;
    end;
    try
    dtOrder_in.Open;
    except
     dtOrder_in.Close;
    end;
    try
    dtCenter.Open;
    except
     dtCenter.Close;
    end;
    try
    dtHistory.Open;
    except
     dtHistory.Close;
    end;
    try
    dtOrder_factory.Open;
    except
      dtOrder_factory.Close;
    end;
     End;
  except

    on E: Exception do
    begin

      raise E.Create(E.Message);

      Screen.Cursor := crDefault;

    Connection.Connected:= false;
   
    end;

  end;

  Screen.Cursor := crDefault;
  mode:=-1;
end;

procedure TdmIndividualOrder.DataModuleDestroy(Sender: TObject);
begin



  dtHistory.Close;

  dtOrder.Close;

  dtOrder_in.Close;

  dtOrder_off.Close;

  COMP_NAME_S.Close;

  dtOrder_factory.Close;

  dtCenter.Close;

  if Connection.InTransaction then
  begin

    Connection.Commit;

  end;

  Connection.Connected := false;

end;


procedure TdmIndividualOrder.dtHistoryNewRecord(DataSet: TDataSet);
var
  I: Integer;
  FieldName: String;
begin

  for i := 0 to dtHistory.FieldCount - 1 do
  begin

    FieldName := dtHistory.Fields[i].FieldName;

    if (FieldName <> 'NAME') and (FieldName <> 'DocumentName') then
    begin

      if mode=0 then dtHistory.Fields[i].AsVariant := dtOrder.FieldByName(FieldName).AsVariant;

       if mode=1 then  dtHistory.Fields[i].AsVariant := dtOrder_in.FieldByName(FieldName).AsVariant;

       if mode=2 then  dtHistory.Fields[i].AsVariant := dtOrder_factory.FieldByName(FieldName).AsVariant;
    end;

  end;


end;






procedure TdmIndividualOrder.dtOrderBeforePost(DataSet: TDataSet);
begin

  if DataSet.FieldByName('ART').OldValue <> DataSet.FieldByName('ART').NewValue then
    Begin
      DataSet.FieldByName('COMP').Clear;
      DataSet.FieldByName('GOOD').Clear;
      DataSet.FieldByName('MAT').Clear;
    End;




end;

procedure TdmIndividualOrder.dtOrderNewRecord(DataSet: TDataSet);
begin

  DataSet.FieldByName('DEP_CUSTOM').AsString := DepartmentName;


  //DataSet.FieldByName('EMP_CREATE').AsString := UserName;

  DataSet.FieldByName('EMP_STATE').AsString := UserName;

  if mode =2  then
     DataSet.FieldByName('DEP_SEND').AsString := '�����'
  else
     DataSet.FieldByName('DEP_SEND').AsString := '';

end;







procedure TdmIndividualOrder.dtOrder_offBeforeOpen(DataSet: TDataSet);
begin
  dmIndividualOrder.dtOrder_off.ParamByName('BD').AsDATE := dmIndividualOrder.BD;
  dmIndividualOrder.dtOrder_off.ParamByName('ED').AsDATE:= dmIndividualOrder.ED;
end;

procedure TdmIndividualOrder.PostBeforeClose(DataSet: TDataSet);
begin

  if DataSet.State in [dsEdit, dsInsert] then
  begin

    DataSet.Post;

  end;

end;

procedure TdmIndividualOrder.SetDepParam(DataSet: TDataSet);
begin

  if DataSet is TMyQuery then
  begin

    TMyQuery(DataSet).ParamByName('DEPARTMENT').AsString := DepartmentName;

//    if  TMyQuery(DataSet).Name= 'dtOrder_off' then
//       begin
//         TMyQuery(DataSet).ParamByName('BD').AsString := Datetostr(dmIndividualOrder.BD);
//         TMyQuery(DataSet).ParamByName('BD').AsString:= Datetostr(dmIndividualOrder.ED);
//       end;

  end;

end;

end.
