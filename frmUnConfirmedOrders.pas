unit frmUnConfirmedOrders;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters, StdCtrls,
  cxButtons, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid, Provider,
  DBClient, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase, Menus,
  cxLookAndFeels, dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter;

type
  TDialogUnconfirmedOrders = class(TForm)
    GridDBTableView1: TcxGridDBTableView;
    GridLevel1: TcxGridLevel;
    Grid: TcxGrid;
    GridDBTableView1Column1: TcxGridDBColumn;
    GridDBTableView1Column2: TcxGridDBColumn;
    GridDBTableView1Column3: TcxGridDBColumn;
    ButtonOk: TcxButton;
    DataSource: TDataSource;
    UnConfirmedOrders: TpFIBDataSet;
    Transaction: TpFIBTransaction;
    cxStyleRepository1: TcxStyleRepository;
    StyleOrder: TcxStyle;
    GridDBTableView1Column4: TcxGridDBColumn;
    UnConfirmedOrdersN: TFIBIntegerField;
    UnConfirmedOrdersORDERDATE: TFIBDateTimeField;
    UnConfirmedOrdersDEPARTMENT: TFIBStringField;
    UnConfirmedOrdersSTATE: TFIBSmallIntField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure UnConfirmedOrdersBeforeOpen(DataSet: TDataSet);
    procedure GridDBTableView1Column1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure UnConfirmedOrdersSTATEGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
  public
    procedure Execute;
  end;

var
  DialogUnconfirmedOrders: TDialogUnconfirmedOrders;

implementation

uses comdata, Data;

{$R *.dfm}

{ TDialogUnconfirmedOrders }

procedure TDialogUnconfirmedOrders.Execute;
begin
  if UnConfirmedOrders.Active and not UnConfirmedOrders.IsEmpty then ShowModal;
end;

procedure TDialogUnconfirmedOrders.FormCreate(Sender: TObject);
begin
  Transaction.StartTransaction;
  UnConfirmedOrders.Active := True;
end;

procedure TDialogUnconfirmedOrders.FormDestroy(Sender: TObject);
begin
  UnConfirmedOrders.Active := False;
  Transaction.Commit;
end;

procedure TDialogUnconfirmedOrders.UnConfirmedOrdersBeforeOpen(DataSet: TDataSet);
begin
  UnConfirmedOrders.ParamByName('OrderDate').AsDateTime := dmCom.GetServerTime  - 12;
  UnConfirmedOrders.ParamByName('SelfDepartmentID').AsInteger := SelfDepId;
end;

procedure TDialogUnconfirmedOrders.GridDBTableView1Column1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  State: Variant;
begin
  State := ARecord.Values[0];
  StyleOrder.Color := clNone;
  if not VarIsNull(State) then
  begin
    case State of
      0: StyleOrder.Color := clRed;
      2: StyleOrder.Color := clYellow;
      3: StyleOrder.Color := clGray;
    end;
  end;
  if StyleOrder.Color <> clNone then
  AStyle := StyleOrder;
end;

procedure TDialogUnconfirmedOrders.UnConfirmedOrdersSTATEGetText(
  Sender: TField; var Text: String; DisplayText: Boolean);
begin
  DisplayText := True;
  case Sender.AsInteger of
    0: Text := '0 - �� �������';
    1: Text := '1 - ��������� � �������';
    2: Text := '2 - �������';
    3: Text := '3 - ������';
  else
    Text := '�� ����������' ;
  end;
end;



end.
