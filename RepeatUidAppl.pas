unit RepeatUidAppl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, ActnList, DB, FIBDataSet,
  pFIBDataSet, PrnDbgeh, jpeg, DBGridEhGrouping, rxSpeedbar, GridsEh;

type
  TfmRepeatUidAppl = class(TForm)
    dg1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    acList: TActionList;
    acClose: TAction;
    acPrint: TAction;
    siPrint: TSpeedItem;
    quRepeatUid: TpFIBDataSet;
    quRepeatUidUID: TFIBIntegerField;
    quRepeatUidSN: TFIBIntegerField;
    quRepeatUidDEPNAME: TFIBStringField;
    quRepeatUidDEPFROMNAME: TFIBStringField;
    quRepeatUidCOLORDEP: TFIBIntegerField;
    quRepeatUidCOLORDEPFROM: TFIBIntegerField;
    dsRepeatUid: TDataSource;
    pdg1: TPrintDBGridEh;
    siDel: TSpeedItem;
    acDel: TAction;
    quRepeatUidAPPLDEPID: TFIBIntegerField;
    quRepeatUidMADEIN: TFIBStringField;
    quRepeatUidISCLOSED: TFIBIntegerField;
    quRepeatUidSINVID: TFIBIntegerField;
    quRepeatUidISCENTER: TFIBSmallIntField;
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRepeatUidAppl: TfmRepeatUidAppl;

implementation
uses comdata, M207Proc, dbUtil, data, data3;
{$R *.dfm}

procedure TfmRepeatUidAppl.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmRepeatUidAppl.acPrintExecute(Sender: TObject);
begin
 pdg1.Print;
end;

procedure TfmRepeatUidAppl.FormCreate(Sender: TObject);
begin
 tb1.WallPaper:=wp;
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 OpenDataSets([quRepeatUid]);
end;

procedure TfmRepeatUidAppl.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRepeatUidAppl.dg1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
 if Column.Field<>nil then
 begin
  if Column.FieldName='DEPNAME' then Background:=quRepeatUidCOLORDEP.AsInteger;
  if Column.FieldName='DEPFROMNAME' then Background:=quRepeatUidCOLORDEPFROM.AsInteger
 end
end;

procedure TfmRepeatUidAppl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSets([quRepeatUid]);
 ReOpenDataSets([dm3.quApplDepList]);
end;

procedure TfmRepeatUidAppl.acDelExecute(Sender: TObject);
begin
 if quRepeatUidISCLOSED.AsInteger=1 then
  ExecSQL('update sinv set isclosed=0 where sinvid='+quRepeatUidSINVID.AsString, dm.quTmp);

 if quRepeatUidISCENTER.AsInteger=1 then
  ExecSQL('delete from appldep where appldepid='+quRepeatUidAPPLDEPID.AsString, dm.quTmp)
 else
  ExecSQL('update appldep set refusal=1 where appldepid='+quRepeatUidAPPLDEPID.AsString, dm.quTmp);

 ReOpenDataSets([quRepeatUid]);
end;

end.
