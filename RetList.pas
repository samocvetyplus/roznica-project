unit RetList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, StdCtrls, Placemnt, db, Menus;

type
  TfmRetList = class(TForm)
    sb: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    M207IBGrid1: TM207IBGrid;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    laDep: TLabel;
    siEdit: TSpeedItem;
    fs1: TFormStorage;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    cbAll: TCheckBox;
    N4: TMenuItem;
    N5: TMenuItem;
    miAll: TMenuItem;
    N6: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure siEditClick(Sender: TObject);
    procedure M207IBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure N5Click(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmRetList: TfmRetList;

implementation

uses comdata, Data, SInv, SInvFind, ReportData, Ret;

{$R *.DFM}

procedure TfmRetList.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRetList.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  with dmCom, dm do
    begin
      tr.Active:=True;
      taRec.Active:=True;
      WorkMode:='RET';
      DistrT:=2;
    end;
end;

procedure TfmRetList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      CloseDataSets([dm.taSList, taRec]);
      tr.Commit;
    end;
  with dm do
    begin
      WorkMode:='';
    end
end;

procedure TfmRetList.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRetList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRetList.siAddClick(Sender: TObject);
begin
  if dm.SDepId<>-1 then
    begin
      dm.taRetList.Append;
      ShowAndFreeForm(TfmRet, Self, TForm(fmRet), True, False);
    end
  else MessageDlg('���������� ������� �����', mtInformation, [mbOK], 0);
end;

procedure TfmRetList.siDelClick(Sender: TObject);
begin
  dm.taRetList.Delete;
end;

procedure TfmRetList.FormActivate(Sender: TObject);
begin
  dm.pmRetList.Items[0].Click;
end;

procedure TfmRetList.siEditClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmRet, Self, TForm(fmRet), True, False);
end;

procedure TfmRetList.M207IBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_SPACE, VK_RETURN: siEditClick(Sender);
    end;
end;

procedure TfmRetList.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if NOT Highlight then
    if dm.taRetListIsClosed.AsInteger=0 then Background:=clInfoBk
    else Background:=clBtnFace;
end;

procedure TfmRetList.N5Click(Sender: TObject);
var p: TPoint;
begin
  p:=tb1.ClientToScreen(Point(tb2.Left, tb2.Top+tb2.Height));
  siDep.DropDownMenu.PopUp(p.x, p.y);
end;

procedure TfmRetList.cbAllClick(Sender: TObject);
begin
{  if Sender=cbAll then
    begin
      if miAll.Checked<>cbAll.Checked then miAll.Checked:=cbAll.Checked
    end
  else
    begin
      miAll.Checked:=not miAll.Checked;
      if  cbAll.Checked<>miAll.Checked then cbAll.Checked:=miAll.Checked;
    end;
  with dm do
    if cbAll.Checked<>SInvAll then
      begin
        taRetList.Active:=False;
        SInvAll:=cbAll.Checked;
        taRetList.Open;
    end;}
end;

procedure TfmRetList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F12 then Close;
end;

end.
