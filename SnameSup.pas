unit SNameSup;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, TB2Item, Menus, StdCtrls,
  ActnList, DB, jpeg, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmSnameSup = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    siSup: TSpeedItem;
    dg1: TDBGridEh;
    sitable: TSpeedItem;
    pmsup: TTBPopupMenu;
    biAdamas: TTBItem;
    bicdm: TTBItem;
    biestet: TTBItem;
    bikabar: TTBItem;
    biallsup: TTBItem;
    pmtable: TTBPopupMenu;
    bid_good: TTBItem;
    bid_ins: TTBItem;
    bid_mat: TTBItem;
    bid_country: TTBItem;
    Label1: TLabel;
    ac1: TActionList;
    acadd: TAction;
    acdel: TAction;
    acclose: TAction;
    achelp: TAction;
    biGuliver: TTBItem;
    siHelp: TSpeedItem;
    TBItem1: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure biallsupClick(Sender: TObject);
    procedure bid_goodClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure acaddExecute(Sender: TObject);
    procedure acdelExecute(Sender: TObject);
    procedure accloseExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure achelpExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSnameSup: TfmSnameSup;

implementation
uses comdata, M207Proc, HelpDIns;
{$R *.dfm}

procedure TfmSnameSup.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  with dmcom do
  begin
   FSup1:=0;
   FSup2:=4;
   NewFsup:=-1;
  end;
  bid_goodClick(bid_good);
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;  
end;

procedure TfmSnameSup.biallsupClick(Sender: TObject);
begin
with dmcom do
begin
 case Tcomponent(Sender).Tag of
  0: begin FSup1:=0; FSup2:=5; end
  else begin FSup1:=Tcomponent(Sender).Tag-1; FSup2:=Tcomponent(Sender).Tag-1; end;
 end;
 NewFsup:=Tcomponent(Sender).Tag-1;
 siSup.BtnCaption:=TTBItem(Sender).Caption;
 dg1.FieldColumns['A_PATTERN'].Visible:=((Ftable=1)and(Fsup2=0)); 
 ReOpenDataSets([taSnameSup]);
end
end;

procedure TfmSnameSup.bid_goodClick(Sender: TObject);
begin
with dmcom do
begin
 Ftable:=Tcomponent(Sender).Tag;
 sitable.BtnCaption:=TTBItem(Sender).Caption;
 dg1.FieldColumns['A_PATTERN'].Visible:=((Ftable=1)and(Fsup2=0));
 ReOpenDataSets([taSnameSup]);
end
end;

procedure TfmSnameSup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if (dmcom.taSnameSup.State=dsInsert)or(dmcom.taSnameSup.State=dsEdit) then
  dmcom.taSnameSup.Post;
  
 CloseDataSets([dmcom.taSnameSup]);
end;

procedure TfmSnameSup.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmSnameSup.acaddExecute(Sender: TObject);
begin
 dmcom.taSnameSup.Append;
end;

procedure TfmSnameSup.acdelExecute(Sender: TObject);
begin
 dmcom.taSnameSup.Delete;
end;

procedure TfmSnameSup.accloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmSnameSup.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSnameSup.achelpExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmHelpDins, Self, TForm(fmHelpDins), True, False);
end;

procedure TfmSnameSup.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100117)
end;

procedure TfmSnameSup.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
