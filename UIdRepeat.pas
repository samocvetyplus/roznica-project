unit UIdRepeat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, Grids, DBGridEh, jpeg,
  DBGridEhGrouping, rxSpeedbar, GridsEh;

type
  TfmUidRepeat = class(TForm)
    DBGridEh1: TDBGridEh;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitUIDHistory: TSpeedItem;
    spitUpdate: TSpeedItem;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure spitUpdateClick(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure spitUIDHistoryClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUidRepeat: TfmUidRepeat;

implementation
uses data3, m207Proc, Data, UIDHist, comdata;
{$R *.dfm}

procedure TfmUidRepeat.FormCreate(Sender: TObject);
begin
 tb1.Wallpaper:= wp;
 ReOpenDataSets([dm3.quUidRepeat]);
end;

procedure TfmUidRepeat.spitUpdateClick(Sender: TObject);
begin
 ReOpenDataSets([dm3.quUidRepeat]);
end;

procedure TfmUidRepeat.siExitClick(Sender: TObject);
begin
 CloseDataSets([dm3.quUidRepeat]);
 close;
end;

procedure TfmUidRepeat.spitUIDHistoryClick(Sender: TObject);
begin
  dm.UID2Find := dm3.taCheckUIDStoreDataUID.Value;
  ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
end;

end.
