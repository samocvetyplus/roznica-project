object fmCol: TfmCol
  Left = 384
  Top = 382
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1094#1074#1077#1090#1086#1074
  ClientHeight = 553
  ClientWidth = 525
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 440
    Top = 8
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 440
    Top = 40
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 421
    Height = 553
    ActivePage = TabSheet1
    Align = alLeft
    TabOrder = 2
    ExplicitHeight = 536
    object TabSheet1: TTabSheet
      Caption = #1057#1082#1083#1072#1076' '#1087#1086#1080#1079#1076#1077#1083#1100#1085#1086
      ExplicitHeight = 508
      object Label1: TLabel
        Left = 4
        Top = 8
        Width = 256
        Height = 13
        Caption = #1055#1088#1080#1093#1086#1076' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 32
        Width = 135
        Height = 13
        Caption = #1055#1088#1080#1093#1086#1076' '#1085#1072' '#1085#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 4
        Top = 56
        Width = 42
        Height = 13
        Caption = #1054#1089#1090#1072#1090#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 4
        Top = 104
        Width = 49
        Height = 13
        Caption = #1055#1086#1089#1090#1072#1074#1082#1080
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 4
        Top = 80
        Width = 170
        Height = 13
        Caption = #1055#1086#1089#1090#1072#1074#1082#1080', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 4
        Top = 128
        Width = 101
        Height = 13
        Caption = #1056#1086#1079#1085#1080#1095#1085#1072#1103' '#1087#1088#1086#1076#1072#1078#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 4
        Top = 152
        Width = 211
        Height = 13
        Caption = #1054#1087#1090#1086#1074#1072#1103' '#1087#1088#1086#1076#1072#1078#1072', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 4
        Top = 176
        Width = 90
        Height = 13
        Caption = #1054#1087#1090#1086#1074#1072#1103' '#1087#1088#1086#1076#1072#1078#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 4
        Top = 200
        Width = 236
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 4
        Top = 228
        Width = 115
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072#1084
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 4
        Top = 252
        Width = 244
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 4
        Top = 276
        Width = 123
        Height = 13
        Caption = #1042#1086#1079#1074#1088#1072#1090' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label15: TLabel
        Left = 4
        Top = 300
        Width = 210
        Height = 13
        Caption = #1054#1087#1090#1086#1074#1099#1081' '#1074#1086#1079#1074#1088#1072#1090', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 4
        Top = 324
        Width = 89
        Height = 13
        Caption = #1054#1087#1090#1086#1074#1099#1081' '#1074#1086#1079#1074#1088#1072#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 4
        Top = 349
        Width = 254
        Height = 13
        Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 4
        Top = 373
        Width = 133
        Height = 13
        Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label17: TLabel
        Left = 8
        Top = 400
        Width = 69
        Height = 13
        Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 8
        Top = 424
        Width = 111
        Height = 13
        Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103', '#1086#1090#1082#1088#1099#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 8
        Top = 448
        Width = 110
        Height = 13
        Caption = #1054#1090#1083#1086#1078#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label20: TLabel
        Left = 8
        Top = 472
        Width = 188
        Height = 13
        Caption = #1056#1077#1084#1086#1085#1090' '#1080#1079#1076#1077#1083#1080#1081', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1086#1090#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label21: TLabel
        Left = 8
        Top = 496
        Width = 189
        Height = 13
        Caption = #1056#1077#1084#1086#1085#1090' '#1080#1079#1076#1077#1083#1080#1081', '#1085#1072#1082#1083#1072#1076#1085#1072#1103' '#1079#1072#1082#1088#1099#1090#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object M207DBColor1: TM207DBColor
        Left = 284
        Top = 8
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 0
        DataField = 'CU_SBD0'
        DataSource = dm2.dsCol
      end
      object M207DBColor2: TM207DBColor
        Left = 284
        Top = 32
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 1
        DataField = 'CU_SBD1'
        DataSource = dm2.dsCol
      end
      object M207DBColor3: TM207DBColor
        Left = 284
        Top = 56
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 2
        DataField = 'CU_R'
        DataSource = dm2.dsCol
      end
      object M207DBColor4: TM207DBColor
        Left = 284
        Top = 80
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 3
        DataField = 'CU_S0'
        DataSource = dm2.dsCol
      end
      object M207DBColor5: TM207DBColor
        Left = 284
        Top = 104
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 4
        DataField = 'CU_S1'
        DataSource = dm2.dsCol
      end
      object M207DBColor8: TM207DBColor
        Left = 284
        Top = 128
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 5
        DataField = 'CU_SL'
        DataSource = dm2.dsCol
      end
      object M207DBColor9: TM207DBColor
        Left = 284
        Top = 152
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 6
        DataField = 'CU_SO0'
        DataSource = dm2.dsCol
      end
      object M207DBColor10: TM207DBColor
        Left = 284
        Top = 176
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 7
        DataField = 'CU_SO1'
        DataSource = dm2.dsCol
      end
      object M207DBColor11: TM207DBColor
        Left = 284
        Top = 200
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 8
        DataField = 'CU_SR0'
        DataSource = dm2.dsCol
      end
      object M207DBColor12: TM207DBColor
        Left = 284
        Top = 224
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 9
        DataField = 'CU_SR1'
        DataSource = dm2.dsCol
      end
      object M207DBColor13: TM207DBColor
        Left = 284
        Top = 248
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 10
        DataField = 'CU_RT0'
        DataSource = dm2.dsCol
      end
      object M207DBColor14: TM207DBColor
        Left = 284
        Top = 272
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 11
        DataField = 'CU_RT1'
        DataSource = dm2.dsCol
      end
      object M207DBColor15: TM207DBColor
        Left = 284
        Top = 296
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 12
        DataField = 'CU_RO0'
        DataSource = dm2.dsCol
      end
      object M207DBColor16: TM207DBColor
        Left = 284
        Top = 320
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 13
        DataField = 'CU_RO1'
        DataSource = dm2.dsCol
      end
      object M207DBColor6: TM207DBColor
        Left = 284
        Top = 345
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 14
        DataField = 'CU_IM0'
        DataSource = dm2.dsCol
      end
      object M207DBColor7: TM207DBColor
        Left = 284
        Top = 369
        Width = 121
        Height = 21
        Color = clBlack
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 15
        DataField = 'CU_IM1'
        DataSource = dm2.dsCol
      end
      object M207DBColor17: TM207DBColor
        Left = 283
        Top = 393
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 16
        DataField = 'CU_AC'
        DataSource = dm2.dsCol
      end
      object M207DBColor18: TM207DBColor
        Left = 282
        Top = 419
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 17
        DataField = 'CU_AO'
        DataSource = dm2.dsCol
      end
      object M207DBColor19: TM207DBColor
        Left = 282
        Top = 444
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 18
        DataField = 'CU_SI'
        DataSource = dm2.dsCol
      end
      object M207DBColor20: TM207DBColor
        Left = 282
        Top = 469
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 19
        DataField = 'CU_RO'
        DataSource = dm2.dsCol
      end
      object M207DBColor21: TM207DBColor
        Left = 281
        Top = 493
        Width = 121
        Height = 21
        DirectInput = False
        NumGlyphs = 1
        TabOrder = 20
        DataField = 'CU_RC'
        DataSource = dm2.dsCol
      end
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 448
    Top = 104
  end
end
