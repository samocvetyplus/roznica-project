unit A2M;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, StdCtrls, Grids,
  DBGridEh, jpeg, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmA2M = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fs: TFormStorage;
    SpeedItem1: TSpeedItem;
    laPeriod: TLabel;
    dg1: TDBGridEh;
    siHelp: TSpeedItem;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  fmA2M: TfmA2M;

implementation

uses Appl, comdata, Data, Data2, Data3, M207Proc, Period;

{$R *.dfm}

procedure TfmA2M.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmA2M.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmA2M.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmA2M.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dmCom, dm3 do
    begin
      if not tr.Active then tr.StartTransaction;
      OpenDataSets([quA2M]);
    end;
  ShowPeriod;
end;

procedure TfmA2M.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom, dm3 do
    begin
      CloseDataSets([quA2M]);
      tr.CommitRetaining;
    end;
end;

procedure TfmA2M.SpeedItem1Click(Sender: TObject);
begin
  with dm3 do
    if GetPeriod(MBD, MED) then
      begin
        ReOpenDataSets([quA2M]);
        ShowPeriod;
      end;
end;

procedure TfmA2M.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm3.MBD) + ' �� ' +DateToStr(dm3.MED);
end;

procedure TfmA2M.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100540)
end;

procedure TfmA2M.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
