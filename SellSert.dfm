object fmSellSert: TfmSellSert
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1076#1072#1085#1085#1099#1077' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1099
  ClientHeight = 419
  ClientWidth = 620
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 620
    Height = 419
    Align = alClient
    BorderStyle = cxcbsNone
    TabOrder = 0
    LookAndFeel.Kind = lfStandard
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsSert
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = #1053#1086#1084#1080#1085#1072#1083#1100#1085#1072#1103' '#1089#1091#1084#1084#1072':0'
          Kind = skSum
          FieldName = 'NOMINAL'
          Column = cxGrid1DBTableView1NOMINAL
        end
        item
          Format = #1060#1072#1082#1090'.'#1089#1091#1084#1084#1072':0.0'
          Kind = skSum
          FieldName = 'COST'
          Column = cxGrid1DBTableView1COST
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          FieldName = 'NOMINAL'
          Column = cxGrid1DBTableView1NOMINAL
        end
        item
          Kind = skSum
          FieldName = 'COST'
          Column = cxGrid1DBTableView1COST
          Sorted = True
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.ColumnHeaderHints = False
      OptionsView.Footer = True
      object cxGrid1DBTableView1RN: TcxGridDBColumn
        Caption = #1057#1084#1077#1085#1072
        DataBinding.FieldName = 'RN'
        Width = 49
      end
      object cxGrid1DBTableView1DEPNAME: TcxGridDBColumn
        Caption = #1060#1080#1083#1080#1072#1083
        DataBinding.FieldName = 'DEPNAME'
        OnCustomDrawCell = cxGrid1DBTableView1DEPNAMECustomDrawCell
        Options.Editing = False
        Width = 75
      end
      object cxGrid1DBTableView1SERT_ID: TcxGridDBColumn
        Caption = #1057#1077#1088#1090#1080#1092#1080#1082#1072#1090
        DataBinding.FieldName = 'SERT_ID'
        Options.Editing = False
        Options.Filtering = False
        Options.Grouping = False
        Width = 70
      end
      object cxGrid1DBTableView1NOMINAL: TcxGridDBColumn
        Caption = #1053#1086#1084#1080#1085#1072#1083
        DataBinding.FieldName = 'NOMINAL'
        Options.Editing = False
      end
      object cxGrid1DBTableView1COST: TcxGridDBColumn
        Caption = #1060#1072#1082#1090'.'#1089#1091#1084#1084#1072
        DataBinding.FieldName = 'COST'
        Options.Editing = False
        Options.Filtering = False
      end
      object cxGrid1DBTableView1STATE: TcxGridDBColumn
        Caption = #1042#1080#1076' '#1086#1087#1077#1088#1072#1094#1080#1080
        DataBinding.FieldName = 'STATE'
        Options.Editing = False
        Width = 77
      end
      object cxGrid1DBTableView1CLNAME: TcxGridDBColumn
        Caption = #1050#1083#1080#1077#1085#1090
        DataBinding.FieldName = 'CLNAME'
        Options.Editing = False
        Options.Filtering = False
        Width = 110
      end
      object cxGrid1DBTableView1SDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1086#1087#1077#1088#1072#1094#1080#1080
        DataBinding.FieldName = 'SDATE'
        Options.Editing = False
        Width = 107
      end
      object cxGrid1DBTableView1COLOR: TcxGridDBColumn
        DataBinding.FieldName = 'COLOR'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object taSert: TpFIBDataSet
    SelectSQL.Strings = (
      'select s.rn, s.depname, s.color, s.sert_id,'
      's.nominal, s.cost, s.clname, s.sdate, s.color, '
      's.state'
      'from SELL_SERT_S(:BD, :ED) S')
    BeforeOpen = taSertBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 360
    Top = 232
    object taSertRN: TFIBIntegerField
      FieldName = 'RN'
    end
    object taSertDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taSertSERT_ID: TFIBIntegerField
      FieldName = 'SERT_ID'
    end
    object taSertNOMINAL: TFIBIntegerField
      FieldName = 'NOMINAL'
    end
    object taSertCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSertCLNAME: TFIBStringField
      FieldName = 'CLNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taSertSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taSertCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taSertSTATE: TFIBStringField
      FieldName = 'STATE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsSert: TDataSource
    DataSet = taSert
    Left = 416
    Top = 240
  end
end
