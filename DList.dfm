object fmDList: TfmDList
  Left = 167
  Top = 24
  HelpContext = 100221
  Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1077#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
  ClientHeight = 419
  ClientWidth = 888
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 888
    Height = 38
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 74
    BtnHeight = 40
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acCloseForm
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      ImageIndex = 0
      Spacing = 1
      Left = 816
      Top = 2
      Visible = True
      OnClick = acCloseFormExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      ImageIndex = 2
      Spacing = 1
      Left = 76
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      DropDownMenu = pmAdd
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      DropDownMenu = pmView
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1080' '#1088#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ImageIndex = 4
      Spacing = 1
      Left = 150
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siOpenDinv: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100'...'
      Caption = #1054#1090#1082#1088#1099#1090#1100'...'
      Hint = #1054#1090#1082#1088#1099#1090#1100'...|'
      ImageIndex = 6
      Spacing = 1
      Left = 224
      Top = 2
      Visible = True
      OnClick = siOpenDinvClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = pmprint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 298
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siAddAll: TSpeedItem
      BtnCaption = #1056#1072#1079#1085#1086#1077
      Caption = 'siDifferent'
      DropDownMenu = pmother
      Spacing = 1
      Left = 446
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acCheck
      BtnCaption = #1055#1088#1086#1074#1077#1088#1082#1072
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072
      ImageIndex = 71
      Spacing = 1
      Left = 372
      Top = 2
      Visible = True
      OnClick = acCheckExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'siHelp'
      Hint = 'siHelp|'
      ImageIndex = 73
      Spacing = 1
      Left = 742
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      Glyph.Data = {
        C6030000424DC60300000000000036000000280000000F000000130000000100
        18000000000090030000E6440000E64400000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF598455FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF036A018BB089FFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF339032269A28289C2B2C962D378F37579555FFFFFF43864009A211508E
        4EFFFFFFFFFFFF000000FFFFFFFFFFFF1B981D46C64E5DD06568D57067D56F5C
        CF6448C04F2F9F322DA83222B92C218022FFFFFFFFFFFF000000FFFFFF209421
        4BCD555ED06668D16E7AD87F94E99A99EB9F80DD876CD87453C95B3CC245128A
        16FFFFFFFFFFFF00000059995622B02A34B33A3D993C5A9C58609D5E599E586E
        BD6F8CE29275D67B5DCA6449C5511A9F21FFFFFFFFFFFF00000030923014A51A
        5A9656FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF49A54A70DA7857CA5F45C74E1DB5
        27398138FFFFFF0000001A8C1A429841FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF37B83E40C24827A72D198F1D1C7E1D598B5750814D000000157610FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF127911349A3641883FFFFFFFFFFFFFFFFF
        FFFFFFFF51864E0000002E7529FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF35
        6C32FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E752900000051864EFFFFFF
        FFFFFFFFFFFF739D71458A433E9E40FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF157610000000FFFFFF5A8B5823812324942734AE3A50C95846BE4CFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4198401A8C1A000000FFFFFF3C833B
        2EBD3753CD5C62D06A7BDF82FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5C97
        581EA924319231000000FFFFFF7A9E7727A52D55CA5D65CE6C7BD9818FE4956E
        BD6F5A9E58619D5F5C9C5A429B4142BA4834B93C5A9A57000000FFFFFFFFFFFF
        1C8E1F4AC9535DCD6474DB7B84DF8B9AECA196E99C7EDB8470D5766AD5725CD5
        65289729FFFFFF000000FFFFFFFFFFFF2481242FBF3937AD3D36A33950C45764
        D36C6FD97771DA7967D57053CC5B249C26FFFFFFFFFFFF000000FFFFFFFFFFFF
        4F8D4D0AA212448641FFFFFF5995563B913A32993330A0332E9D30379136FFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF8BB089036A01FFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFF598455FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1089#1087#1080#1089#1086#1082'|'
      Spacing = 1
      Left = 520
      Top = 2
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Caption = 'SpeedItem3'
      Hint = 'SpeedItem3|'
      Spacing = 1
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Caption = 'SpeedItem4'
      Hint = 'SpeedItem4|'
      Spacing = 1
      Top = 2
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 38
    Width = 888
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object laDepFrom: TLabel
      Left = 84
      Top = 8
      Width = 61
      Height = 13
      Caption = 'laDepFrom'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laDepTo: TLabel
      Left = 300
      Top = 8
      Width = 49
      Height = 13
      Caption = 'laDepTo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 540
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmDListFrom
      Hint = #1042#1099#1073#1086#1088' '#1080#1089#1093#1086#1076#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siDepFrom: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmDListTo
      Hint = #1042#1099#1073#1086#1088' '#1082#1086#1085#1077#1095#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 218
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siPeriod: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 458
      Top = 2
      Visible = True
      OnClick = siPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object dg2: TDBGridEh
    Left = 0
    Top = 67
    Width = 888
    Height = 352
    Align = alClient
    AllowedOperations = []
    AllowedSelections = [gstRecordBookmarks, gstAll]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    Ctl3D = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsDList
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clInfoBk
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 3
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    PopupMenu = pmmain
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = dg2DblClick
    OnGetCellParams = dg2GetCellParams
    OnKeyDown = dg2KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SDATE'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold, fsItalic]
        Footers = <
          item
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1047#1072#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Color = clRed
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1054#1090#1082#1088#1099#1090#1099#1077
            ValueType = fvtStaticText
          end
          item
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Value = #1042#1089#1077#1075#1086
            ValueType = fvtStaticText
          end>
        Title.Caption = #1044#1072#1090#1072
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'SN'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InSn'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutSn'
            ValueType = fvtSum
          end
          item
            FieldName = 'SN'
            ValueType = fvtCount
          end>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'DEPFROM'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076'|'#1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Width = 88
      end
      item
        EditButtons = <>
        FieldName = 'COST0'
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InCost0'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutCost0'
            ValueType = fvtSum
          end
          item
            FieldName = 'COST0'
            ValueType = fvtSum
          end>
        Title.Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076'|'#1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'DEPTO'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1089#1082#1083#1072#1076'|'#1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Width = 118
      end
      item
        EditButtons = <>
        FieldName = 'Cost1'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InCost1'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutCost1'
            ValueType = fvtSum
          end
          item
            FieldName = 'Cost1'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1085#1077#1095#1085#1099#1081' '#1089#1082#1083#1072#1076'|'#1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'SCOST'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InSCost'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutSCost'
            ValueType = fvtSum
          end
          item
            FieldName = 'SCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1057#1091#1084#1084#1072' '#1074' '#1087#1088'. '#1094#1077#1085#1072#1093
        Width = 101
      end
      item
        EditButtons = <>
        FieldName = 'CRFIO'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1057#1086#1079#1076#1072#1083
        Width = 131
      end
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1047#1072#1082#1088#1099#1083
        Width = 103
      end
      item
        EditButtons = <>
        FieldName = 'QUID'
        Footer.Alignment = taLeftJustify
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InQuid'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutQuid'
            ValueType = fvtSum
          end
          item
            FieldName = 'QUID'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'WUID'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
            FieldName = 'InWUid'
            ValueType = fvtSum
          end
          item
            Color = clRed
            FieldName = 'OutWuid'
            ValueType = fvtSum
          end
          item
            FieldName = 'WUID'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'SCOSTD'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1057#1091#1084#1084#1072' '#1074' '#1086#1087#1090'. '#1094#1077#1085#1072#1093
      end
      item
        EditButtons = <>
        FieldName = 'RSTATE'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = [fsBold]
        Footers = <
          item
            Color = clBtnFace
          end
          item
            Color = clRed
          end
          item
          end>
        Title.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fs1: TFormStorage
    Options = []
    UseRegistry = False
    StoredValues = <>
    Left = 28
    Top = 108
  end
  object pmmain: TTBPopupMenu
    Images = dmCom.ilButtons
    Left = 592
    Top = 176
    object MAddItem: TTBItem
      Action = acAddItem
    end
    object MAddDinv: TTBItem
      Action = acAddDinv
    end
    object MDel: TTBItem
      Action = acDel
    end
    object MViewItem: TTBItem
      Action = acViewItem
    end
    object MViewDinv: TTBItem
      Action = acViewDinv
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object MSeachFirstD: TTBItem
      Action = acSeachFirstD
    end
    object MSeachSecondD: TTBItem
      Action = acSeachSecondD
    end
    object TBSeparatorItem6: TTBSeparatorItem
    end
    object biFictionInv: TTBItem
      Action = acFictionInv
    end
  end
  object aclisst: TActionList
    Images = dmCom.ilButtons
    Left = 592
    Top = 120
    object acAddItem: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' ('#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086')'
      ImageIndex = 1
      ShortCut = 45
      OnExecute = acAddItemExecute
    end
    object acAddDinv: TAction
      Tag = 1
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' ('#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1100#1085#1086')'
      ImageIndex = 1
      ShortCut = 16429
      OnExecute = acAddItemExecute
    end
    object acViewItem: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' ('#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086')'
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewItemExecute
    end
    object acViewDinv: TAction
      Tag = 1
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' ('#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1100#1085#1086')'
      ImageIndex = 4
      ShortCut = 16498
      OnExecute = acViewItemExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnExecute = acDelExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1073#1088#1072#1085#1085#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acCloseExecute
    end
    object acAllClose: TAction
      Caption = #1042#1089#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
      OnExecute = acAllCloseExecute
    end
    object acCloseForm: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      ShortCut = 123
      OnExecute = acCloseFormExecute
    end
    object acSeachFirstD: TAction
      Caption = #1042#1099#1073#1086#1088' '#1080#1089#1093#1086#1076#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ShortCut = 117
      OnExecute = acSeachFirstDExecute
    end
    object acSeachSecondD: TAction
      Caption = #1042#1099#1073#1086#1088' '#1082#1086#1085#1077#1095#1085#1086#1075#1086' '#1089#1082#1083#1072#1076#1072
      ShortCut = 118
      OnExecute = acSeachSecondDExecute
    end
    object acPrint: TAction
      Caption = 'acPrint'
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acOpenExecute
    end
    object acCreateInvFromPrord: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1076#1083#1103' '#1074#1089#1077#1093' '#1080#1079#1076#1077#1083#1080#1081' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      OnExecute = acCreateInvFromPrordExecute
      OnUpdate = acCreateInvFromPrordUpdate
    end
    object acCreateInvAll: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1042#1086#1079#1074#1088#1072#1090' '#1074#1089#1077#1093' '#1080#1079#1076#1077#1083#1080#1081' '#1089' '#1092#1080#1083#1080#1072#1083#1072
      OnExecute = acCreateInvAllExecute
      OnUpdate = acCreateInvAllUpdate
    end
    object acCretaeOpt: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1087#1090#1086#1074#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1085#1072' '#1042#1055
      OnExecute = acCretaeOptExecute
      OnUpdate = acCretaeOptUpdate
    end
    object acCreateInvFromInv: TAction
      Category = #1056#1072#1079#1085#1086#1077
      Caption = #1055#1077#1088#1077#1084#1077#1089#1090#1080#1090#1100' '#1080#1079#1076#1077#1083#1080#1103' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1085#1072'...'
      Hint = 
        #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1089' '#1080#1079#1076#1077#1083#1080#1103#1084#1080' '#1080#1079' '#1074#1099#1073#1088#1072#1085#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1085#1072' '#1074#1099#1073#1088#1072#1085#1085#1099 +
        #1081' '#1092#1080#1083#1080#1072#1083
      ShortCut = 113
      OnExecute = acCreateInvFromInvExecute
      OnUpdate = acCreateInvFromInvUpdate
    end
    object acCheck: TAction
      Caption = #1055#1088#1086#1074#1077#1088#1082#1072
      OnExecute = acCheckExecute
    end
    object acCheckDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1088#1086#1074#1077#1088#1082#1091
      OnExecute = acCheckDelExecute
    end
    object acFictionInv: TAction
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1077' '#1085#1072' '#1089#1082#1083#1072#1076#1077
      OnExecute = acFictionInvExecute
      OnUpdate = acFictionInvUpdate
    end
  end
  object pmAdd: TTBPopupMenu
    Left = 48
    Top = 32
    object addItem: TTBItem
      Action = acAddItem
    end
    object addDinv: TTBItem
      Action = acAddDinv
    end
  end
  object pmView: TTBPopupMenu
    Left = 160
    Top = 32
    object ViewItem: TTBItem
      Action = acViewItem
    end
    object ViewDinv: TTBItem
      Action = acViewDinv
    end
  end
  object pmClose: TTBPopupMenu
    Left = 272
    Top = 16
    object pClose: TTBItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnClick = siOpenDinvClick
    end
    object AllClose: TTBItem
      Action = acAllClose
    end
  end
  object pmprint: TTBPopupMenu
    Left = 344
    Top = 136
    object pStandart: TTBItem
      Tag = 1
      Caption = #1058#1086#1074#1072#1088#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103
      OnClick = pStandartClick
    end
    object pItem: TTBItem
      Tag = 2
      Caption = #1058#1086#1074#1072#1088#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103' ('#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086')'
      OnClick = pStandartClick
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem4: TTBItem
      Tag = 7
      Caption = #1058#1086#1074#1072#1088#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103' ('#1087#1088#1080#1093'. '#1094#1077#1085#1099')'
      OnClick = pStandartClick
    end
    object TBItem5: TTBItem
      Tag = 3
      Caption = #1056#1077#1077#1089#1090#1088' '#1094#1077#1085
      Visible = False
      OnClick = pStandartClick
    end
  end
  object pdg1: TPrintDBGridEh
    DBGridEh = dg2
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -13
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 168
    Top = 264
  end
  object pmother: TTBPopupMenu
    Left = 488
    Top = 32
    object bicreateInvFromSinv: TTBItem
      Action = acCreateInvFromPrord
      Enabled = False
    end
    object biCreateInvAll: TTBItem
      Action = acCreateInvAll
      Enabled = False
    end
    object ciCreateOpt: TTBItem
      Action = acCretaeOpt
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1087#1090#1086#1074#1091#1102' '#1087#1088#1086#1076#1072#1078#1091' '#1089' '#1042#1055
    end
    object TBSeparatorItem5: TTBSeparatorItem
    end
    object biCreateInvFrominv: TTBItem
      Action = acCreateInvFromInv
    end
    object TBItem6: TTBItem
      Action = acCheckDel
    end
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   2'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 2'
      'order by HDATE')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 664
    Top = 8
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 608
    Top = 8
  end
end
