unit WHAnlz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, CheckLst, Buttons,XMLIntf,
  Math, xmldom, XMLDoc, DateUtils, RXStrUtils, rxSpeedbar;

type
  TInsArr = array[0..10000] of record
                                    Id: integer;
                                    Name: string;
                                    Color: string;
                                   end;
  TCompArr = array[0..10000] of string;

  TfmSellWH = class(TForm)
    chlbProd: TCheckListBox;
    lbComp: TLabel;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siCalc: TSpeedItem;
    svdFile: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure siCalcClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure siExitClick(Sender: TObject);
  private
    { Private declarations }
   I_Bd: TDateTime;
   I_Ed: TDateTime;

    procedure GetAnlzData(var XmlDoc: IXMLNode; BD:TDate;
                          InsArr:TInsArr; InsCount: integer;CompName:String);
    function ApplyLight(Color:TColor; HowMuch:Byte):TColor;
    function RemoveSpecSymb(SrStr:string):string;
    procedure CheckMultiContr(chlb:TCheckListBox;var arr: TCompArr;var sz: integer);
    procedure CheckComp;
    function  GetCompStr(CompArr: TCompArr; CompCount: integer): string;
    procedure MakeCalcAnlz(BD,ED:TDateTime);
    Function CreateNeedUidStore(bd : TDate): Integer;

  public
    { Public declarations }
  end;

var
  fmSellWH: TfmSellWH;

implementation

uses Data, ServData, comdata,M207Proc,Period,DBTree, DBUtil, Data3, UIDStoreList;

{$R *.dfm}

procedure TfmSellWH.FormCreate(Sender: TObject);
const
    mDay : array[1..12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var d,m,y : word;

begin
  tb1.Wallpaper := wp;

  DecodeDate(Date, y, m, d);
  if (m = 1)  then
  begin
   m := 13;
   y := y-1;
  end;
  if (m = 2)  then
  begin
   m := 14;
   y := y-1;
  end;
  I_Bd := EncodeDate(y, m-2, 1);
  if IsLeapYear(y)and((m-1)=2)then I_Ed := EncodeDate(y, m-1, 29)
  else I_Ed := EncodeDate(y, m-1, mDay[m-1]);

  chlbProd.MultiSelect := True;
  chlbProd.Items.Assign(dm.slProd);
  chlbProd.Items.Delete(0);
  laPeriod.Caption := 'C '+DateToStr(I_Bd)+' �� '+DateToStr(I_Ed);
end;

function TfmSellWH.ApplyLight(Color:TColor; HowMuch:Byte):TColor;
var r,g,b:Byte;
begin
	Color:=ColorToRGB(Color);
	r:=GetRValue(Color);
	g:=GetGValue(Color);
	b:=GetBValue(Color);
	if r<HowMuch then r:=r+HowMuch;
	if g<HowMuch then g:=g+HowMuch;
	if b<HowMuch then b:=b+HowMuch;
	result:=RGB(r,g,b);
end;
function TfmSellWH.RemoveSpecSymb(SrStr:string):string;
var strtmp: string;
begin
  strtmp := trim(SrStr);
  strtmp := ReplaceStr(strtmp, ' ','_');
  strtmp := ReplaceStr(strtmp, '.','_');
  strtmp := ReplaceStr(strtmp, '"','_');
  strtmp := ReplaceStr(strtmp, '''','_');
  RESULT := ReplaceStr(strtmp, ',','_');

end;

procedure TfmSellWH.GetAnlzData(var XmlDoc: IXMLNode; BD:TDate;
                          InsArr:TInsArr; InsCount: integer; CompName:String);
var
    d,m,y : word;
    Info, DInf,Item,Att: IXMLNode;
    j, k: integer;
    strtmp: string;
    tmp: real;
    DefaultSeparator:char;
begin
 DefaultSeparator := DecimalSeparator;
 try
   DecimalSeparator := '.';

   with dmServ do
   begin
      Info := XmlDoc.AddChild('������');

      DecodeDate(BD, y, m, d);
      Att := XMLSell.CreateNode('���������_�����', ntAttribute);
      Att.NodeValue :=m;
      Info.AttributeNodes.Add(Att);

      Att := XMLSell.CreateNode('���������_���', ntAttribute);
      Att.NodeValue := y;
      Info.AttributeNodes.Add(Att);


      Att := XMLSell.CreateNode('��������_�����', ntAttribute);
      Att.NodeValue := m;
      Info.AttributeNodes.Add(Att);

      Att := XMLSell.CreateNode('��������_���', ntAttribute);
      Att.NodeValue := y;
      Info.AttributeNodes.Add(Att);



      for j:=0 to Pred(dmCom.DepCount) do
      begin
        DInf := XmlDoc.AddChild('�������');
        Att := XMLSell.CreateNode('�������', ntAttribute);
        Att.NodeValue := dmCom.DepInfo[j].SName;
        DInf.AttributeNodes.Add(Att);
        with taWH_SellDate do
        begin
          Active := false;
          SelectSQL.Text := 'select T_Name, sum(INCOMPRICE) ' +
                            'from Proceeds ' +
                            'where DepID = ' + IntToStr(dmCom.DepInfo[j].DepId) +
                            '     and BD = '#39 + DateTimeToStr(BD) + #39 +
                            '     and CompName ' + CompName +
                            ' group by T_Name ' +
                            ' order by T_Name ';
          Prepare;
          Active := true;
        end;
        for k := 0 to Pred(InsCount) do
          begin
            strtmp := trim(InsArr[k].Name);
      //            InsNo :=GetInsNoByName(strtmp, InsArr, InsCount);
            strtmp := RemoveSpecSymb(Strtmp);
            Item :=  DInf.AddChild(strtmp);
            Att := XMLSell.CreateNode('ID', ntAttribute);
            Att.NodeValue := InsArr[k].Id;
            Item.AttributeNodes.Add(Att);

            Att := XMLSell.CreateNode('COLOR', ntAttribute);
            Att.NodeValue := InsArr[k].Color;
            Item.AttributeNodes.Add(Att);

            if taWH_SellDate.Locate('T_Name', InsArr[k].Name, []) then
                 Item.AddChild('INCOMPRICE').NodeValue := FloatToStr(RoundTo(taWH_SellDate.Fields[1].AsFloat, -2))
            else
               Item.AddChild('INCOMPRICE').NodeValue := '0';
          end;

        with quTmp do
        begin
          Close;
          SQL.Text := 'select sum(INCOMPRICE) ' +
                      'from Proceeds ' +
                      'where DepID = ' + IntToStr(dmCom.DepInfo[j].DepId) +
                      '     and BD = '#39 + DateTimeToStr(BD) + #39 +
                      '     and CompName ' + CompName;


          ExecQuery;
          Item :=  DInf.AddChild('�����');
          Item.AddChild('INCOMPRICE').NodeValue := FloatToStr(RoundTo(Fields[0].asFloat,-2));
          Close;
        end;
      end;


      for j:=0 to Pred(dmCom.DepCount) do
      begin
        DInf := XmlDoc.AddChild('�����');
        Att := XMLSell.CreateNode('�������', ntAttribute);
        Att.NodeValue := dmCom.DepInfo[j].SName;
        DInf.AttributeNodes.Add(Att);

        with taWH_SellDate do
        begin
          Active := false;
          SelectSQL.Text := 'select T_Name, sum(BD_INPRICEWH), sum(ED_INPRICEWH) ' +
                            'from Proceeds ' +
                            'Where DepID = ' + IntToStr(dmCom.DepInfo[j].DepId) +
                            '     and BD = '#39 + DateTimeToStr(BD) + #39 +
                            '     and CompName ' + CompName +
                            ' group by T_Name ' +
                            ' order by T_Name' ;
          Prepare;
          Active := true;
        end;

        for k:=0 to Pred(InsCount) do
          begin
            strtmp := trim(InsArr[k].Name);
//            InsNo :=GetInsNoByName(strtmp, InsArr, InsCount);
            strtmp := RemoveSpecSymb(strtmp);
            Item :=  DInf.AddChild(strtmp);
            Att := XMLSell.CreateNode('ID', ntAttribute);
            Att.NodeValue := InsArr[k].Id;
            Item.AttributeNodes.Add(Att);

            Att := XMLSell.CreateNode('COLOR', ntAttribute);
            Att.NodeValue := InsArr[k].Color;
            Item.AttributeNodes.Add(Att);

            if taWH_SellDate.Locate('T_Name', InsArr[k].Name, []) then
              begin
                 Item.AddChild('BD_INPRICEWH').NodeValue := FloatToStr(RoundTo(taWH_SellDate.Fields[1].AsFloat, -2));
                 Item.AddChild('ED_INPRICEWH').NodeValue := FloatToStr(RoundTo(taWH_SellDate.Fields[2].AsFloat, -2));
              end
            else
              begin
               Item.AddChild('BD_INPRICEWH').NodeValue := '0';
               Item.AddChild('ED_INPRICEWH').NodeValue := '0';
              end;
          end;

        with quTmp do
        begin
          Close;
          SQL.Text := 'select sum(BD_INPRICEWH),sum(ED_INPRICEWH)' +
                      'from Proceeds ' +
                      'where DepID = ' + IntToStr(dmCom.DepInfo[j].DepId) +
                      '     and BD = '#39 + DateTimeToStr(BD) + #39 +
                      '     and CompName ' + CompName;
          ExecQuery;
          Item :=  DInf.AddChild('�����');
          Item.AddChild('BD_INPRICEWH').NodeValue := FloatToStr(RoundTo(Fields[0].asFloat,-2));
          Item.AddChild('ED_INPRICEWH').NodeValue := FloatToStr(RoundTo(Fields[1].asFloat,-2));
          Close;
        end;
      end;


      DInf := XmlDoc.AddChild('�����������');

       with taWH_SellDate do
        begin
          Active := false;
          SelectSQL.Text := 'select T_Name, sum(INCOMPRICE), sum(BD_INPRICEWH), sum(ED_INPRICEWH) ' +
                    'from Proceeds ' +
                    'where BD = '#39 + DateTimeToStr(BD) + #39 +
                    '     and CompName ' + CompName +
                    ' group by T_Name ' +
                    'order by T_Name';

          Prepare;
          Active := true;
        end;

        for k := 0 to Pred(InsCount) do
        begin
          strtmp := trim(InsArr[k].Name);
//          InsNo :=GetInsNoByName(strtmp, InsArr, InsCount);
          strtmp := RemoveSpecSymb(strtmp);
          Item :=  DInf.AddChild(strtmp);
          Att := XMLSell.CreateNode('ID', ntAttribute);
          Att.NodeValue := InsArr[k].Id;
          Item.AttributeNodes.Add(Att);

          Att := XMLSell.CreateNode('COLOR', ntAttribute);
          Att.NodeValue := InsArr[k].Color;
          Item.AttributeNodes.Add(Att);

          if taWH_SellDate.Locate('T_Name', InsArr[k].Name, []) then
            begin
              tmp := RoundTo(taWH_SellDate.Fields[1].AsFloat, -2);
              Item.AddChild('INCOMPRICE').NodeValue := FloatToStr(tmp);
              tmp := RoundTo(taWH_SellDate.Fields[2].AsFloat, -2);
              Item.AddChild('BD_INPRICEWH').NodeValue := FloatToStr(tmp);
              tmp := RoundTo(taWH_SellDate.Fields[3].AsFloat, -2);
              Item.AddChild('ED_INPRICEWH').NodeValue := FloatToStr(tmp);
             end
          else
            begin
              Item.AddChild('INCOMPRICE').NodeValue := 0;
              Item.AddChild('BD_INPRICEWH').NodeValue := 0;
              Item.AddChild('ED_INPRICEWH').NodeValue := 0;
            end;
        end;

        with quTmp do
        begin
          Close;
          SQL.Text := 'select sum(INCOMPRICE), sum(BD_INPRICEWH),sum(ED_INPRICEWH) ' +
                      'from Proceeds ' +
                      'where    BD = '#39 + DateTimeToStr(BD) + #39 +
                      '     and CompName  ' + CompName ;
          ExecQuery;

          Item :=  DInf.AddChild('�����');
          tmp := RoundTo(Fields[0].AsFloat, -2);
          Item.AddChild('INCOMPRICE').NodeValue := FloatToStr(tmp);
          tmp := RoundTo(Fields[1].AsFloat , -2);
          Item.AddChild('BD_INPRICEWH').NodeValue := FloatToStr(tmp);
          tmp := RoundTo(Fields[2].AsFloat, -2);
          Item.AddChild('ED_INPRICEWH').NodeValue := FloatToStr(tmp);
          Close;
      end;
   end;
 finally
    DecimalSeparator :=DefaultSeparator;
 end;
end;
procedure TfmSellWH.MakeCalcAnlz(BD, ED:TDateTime);
var tmpBD, tmpED :TDate;
    d,m,y : word;
begin
  with dmServ do
  begin
    tmpBD := BD;
    DecodeDate(tmpBD, y, m, d);

    if m = 12 then
    begin
     y := y + 1;
     m := 1;
    end;

    tmpED := EncodeDate(y, m+1, 1);
    if not quWHAnlz1.Transaction.Active then quWHAnlz1.Transaction.StartTransaction;

    while (tmpED <= (ED+1)) do
    begin
      quWHAnlz1.Params.ByName['I_BD'].AsDate := tmpBD;
      quWHAnlz1.Params.ByName['I_SINVID'].AsINTEGER := CreateNeedUidStore(tmpbd);

      quWHAnlz1.ExecQuery;
      quWHAnlz1.Transaction.CommitRetaining;
      quWHAnlz1.Close;

      tmpBD := tmpED ;
      DecodeDate(tmpBD, y, m, d);
      if m = 12 then
       tmpED := EncodeDate(y+1, 1, 1)
      else
        tmpED := EncodeDate(y, m+1, 1)
    end;
    quWHAnlz1.Transaction.CommitRetaining;
    quWHAnlz1.Close;
  end;
end;
Function TfmSellWH.CreateNeedUidStore(bd: TDate): integer;
const  mDay : array[1..12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var 
    sqlstr: string;
    FieldsRes: Variant;
    ed: TDateTime;
    y,m,d: word;
begin
  sqlstr := 'SELECT SINVID FROM SINV where  itype = 15 and '+
                                       'OnlyDate(SDate) = '#39 + DateToStr(BD) + #39;
  FieldsRes := ExecSelectSQL(sqlstr, dmServ.qutmp);
  if (FieldsRes[0] <> null) then
  begin
   RESULT := Integer(FieldsRes[0]);
   exit;
  end;
  DecodeDate(BD, y, m, d);
  if IsLeapYear(y)and(m=2)then ed := EncodeDate(y, m, 29)
  else ed := EncodeDate(y, m, mDay[m]);

//  LogOperationID := dm3.insert_operation(sLog_UIDStoreAdd, LogOperIdForm);
  dm3.taUIDStoreList.Append;
  dm3.taUIDStoreListSDATE.AsDateTime := StartOfTheDay(bd);
  dm3.taUIDStoreListNDATE.AsDateTime := EndOfTheDay(ed);
  with dm, quTmp do
  begin
    Close;
    SQL.Text:='select max(sn) from sinv where itype = 15 and FYEAR(Sdate)= FYEAR('''+
              dm3.taUIDStoreListSDATE.Asstring+''')';
    ExecQuery;
    if fields[0].IsNull then  dm3.taUIDStoreListSN.AsInteger := 1
    else dm3.taUIDStoreListSN.AsInteger := fields[0].AsInteger+1;
    Close;
  end;
  dm3.taUIDStoreList.Post;
  RESULT := dm3.taUIDStoreListSINVID.asInteger;
  Application.ProcessMessages;
  fmUIDStoreList.Create_UIDStore;
//  dm3.update_operation(LogOperationID);
end;

procedure TfmSellWH.siCalcClick(Sender: TObject);
var Att, FilterN, ItemFilter, ItemFilter1 : IXMLNode;
    i: integer;
    strtmp: string;
    InsArr: TInsArr;
    CompArr: TCompArr;
    InsCount, CompCount: integer;
    NewColor: TColor;
    CompStr: String;
    sDir, Path: string;
    BD, ED :TDate;
    d,m,y : word;

begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  svdFile.InitialDir:=sDir;
  svdFile.DefaultExt:='xml';
  svdFile.FileName:=ReplaceStr(DateTimeToStr(now),'.','');
  svdFile.FileName:=ReplaceStr(svdFile.FileName,':','');
  svdFile.FileName:=ReplaceStr(svdFile.FileName,'_','');

  if svdFile.Execute then  Path:=svdFile.FileName
  else exit;

  Application.Minimize;

  CheckComp;
  Randomize;


  with dmServ do
  begin
    ExecSQL('delete from Proceeds ', quTmp);

    MakeCalcAnlz(I_BD,I_ED); // �������� �������

    XMLSell.Active := True;
    FilterN := XMLSell.DocumentElement.AddChild('����������');
    Att := XMLSell.CreateNode('������_�������', ntAttribute);
    Att.NodeValue := 'C__' + DateTimeToStr(I_BD) + '__��__' + DateTimeToStr(I_ED);
    FilterN.AttributeNodes.Add(Att);


    Att := XMLSell.CreateNode('����_�������', ntAttribute);
    Att.NodeValue := DateTimeToStr(now);
    FilterN.AttributeNodes.Add(Att);


    FilterN := XMLSell.DocumentElement.AddChild('FILTER');

    ItemFilter:=  FilterN.AddChild('�������������');
    for i:=0 to Pred(dmCom.DepCount) do
    begin
      ItemFilter1 := ItemFilter.AddChild('�����');
      Att := XMLSell.CreateNode('ID', ntAttribute);
      Att.NodeValue :=i+1;
      ItemFilter1.AttributeNodes.Add(Att);
      ItemFilter1.NodeValue := dmCom.DepInfo[i].SName;
    end;


    InsCount := 0;

    OpenDataSets([taGrIns]);
    ItemFilter:=  FilterN.AddChild('������_�����������');
    while not taGrIns.Eof do
    begin
      InsArr[InsCount].Id := InsCount+1;
      InsArr[InsCount].Name := trim(taGrIns.Fields[0].AsString);
      NewColor := ApplyLight(TColor(Random(16777215)),100);
      strtmp := ColorToString(NewColor);
      delete(strtmp,2,2);
      InsArr[InsCount].Color := strtmp;

      ItemFilter1 := ItemFilter.AddChild('������');
      Att := XMLSell.CreateNode('ID', ntAttribute);
      Att.NodeValue :=InsCount+1;
      ItemFilter1.AttributeNodes.Add(Att);

      Att := XMLSell.CreateNode('����', ntAttribute);
      Att.NodeValue :=InsArr[InsCount].Color;
      ItemFilter1.AttributeNodes.Add(Att);

      ItemFilter1.NodeValue := RemoveSpecSymb(taGrIns.Fields[0].AsString);

      taGrIns.Next;
      InsCount := InsCount + 1;
    end;
    CloseDataSets([taGrIns]);

    CheckMultiContr(chlbProd, CompArr, CompCount);

    ItemFilter:=  FilterN.AddChild('������������');
    for i:=0 to Pred(CompCount) do
    begin
      ItemFilter1 := ItemFilter.AddChild('������������');
      Att := XMLSell.CreateNode('ID', ntAttribute);
      Att.NodeValue :=i+1;
      ItemFilter1.AttributeNodes.Add(Att);

      strtmp := '_' + CompArr[i];
      strtmp := RemoveSpecSymb(strtmp);
      ItemFilter1.NodeValue := strtmp;
    end;

    CompStr := GetCompStr(CompArr, CompCount);

    ItemFilter:=  XMLSell.DocumentElement.AddChild('���_������������');
    for i:=0 to Pred(CompCount) do
    begin
      ItemFilter1 := ItemFilter.AddChild('������������');
      Att := XMLSell.CreateNode('ID', ntAttribute);
      Att.NodeValue :=i+1;
      ItemFilter1.AttributeNodes.Add(Att);

      strtmp := '_' + CompArr[i];
      strtmp := RemoveSpecSymb(strtmp);
      ItemFilter1.NodeValue := strtmp;

      BD := I_BD;
      DecodeDate(BD, y, m, d);

      if m = 12 then
      begin
       y := y + 1;
       m := 1;
      end;

      ED := EncodeDate(y, m+1, 1);

      while (ED <= (I_ED+1)) do
      begin
        if i = 0  then
          GetAnlzData(ItemFilter1, BD,InsArr,InsCount,' not in ('#39 + #39')')
        else if i < pred(CompCount) then
          GetAnlzData(ItemFilter1, BD,InsArr,InsCount,' = '#39 +  CompArr[i] + #39)
        else
          GetAnlzData(ItemFilter1, BD,InsArr,InsCount,' not in (' + CompStr + ')');

        BD := ED;
        DecodeDate(BD, y, m, d);
        if m = 12 then
          ED := EncodeDate(y+1, 1, 1)
        else
          ED := EncodeDate(y, m+1, 1);
      end;
    end;


    XMLSell.SaveToFile(Path);
    XMLSell.Active := False;
  end;
  Application.Restore;
  ShowMessage('�������� � ���� ���������');


end;

procedure TfmSellWH.SpeedItem1Click(Sender: TObject);
const
    mDay : array[1..12] of integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
var
  d1, d2 : TDateTime;
  d,m,y, m1, y1 : word;
begin
  d1 := I_BD;
  d2 := I_ED;
  if GetPeriod(d1, d2) then
  begin
    DecodeDate(d1, y, m, d);
    I_Bd := EncodeDate(y, m, 1);
    DecodeDate(d2, y, m, d);
    DecodeDate(Date, y1, m1, d);
    if (y1 = y) and (m = m1) then m := m-1; //����� ����������;
    if IsLeapYear(y)and(m=2)then I_Ed := EncodeDate(y, m, 29)
    else I_Ed := EncodeDate(y, m, mDay[m]);
    laPeriod.Caption := 'C '+DateToStr(I_Bd)+' �� '+DateToStr(I_Ed);
  end;
end;

procedure TfmSellWH.siExitClick(Sender: TObject);
begin
  Close;
end;
procedure TfmSellWH.CheckMultiContr(chlb:TCheckListBox;var arr: TCompArr;var sz: integer);
var i:integer;
    s:string;
    count: integer;
begin
  arr[0] := '�_�����';
  count := 1;
  for i:=0 to chlb.Items.Count-1 do
   if chlb.Checked[i] then
   begin
     s:=chlb.Items[i];
     arr[count] := s;
     count := count + 1;
   end;
  arr[count] := '������_������������';
  sz := count+1;
end;

function TfmSellWH.GetCompStr(CompArr: TCompArr; CompCount: integer): string;
var i:integer;
    tmpstr : string;
begin
  tmpstr :='';
  for i:=0 to Pred(CompCount-1) do
    tmpstr := tmpstr + '''' + CompArr[i] + ''',';

  delete(tmpstr,length(tmpstr),1);
  RESULT := tmpstr;
end;
procedure TfmSellWH.CheckComp;
var i:integer;
    count: integer;
begin
  count := 0;
  for i:=0 to chlbProd.Items.Count-1 do
   if chlbProd.Checked[i] then
   begin
     count := 1;
     break;
   end;
 if count = 0 then raise exception.Create('�� ������ ������������');
end;


end.
