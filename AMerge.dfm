object fmAMerge: TfmAMerge
  Left = 275
  Top = 161
  Width = 807
  Height = 480
  Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1086#1074
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 249
    Top = 42
    Height = 385
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 427
    Width = 799
    Height = 19
    Panels = <>
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 799
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 475
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siOpen: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 68
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siOpenClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 42
    Width = 249
    Height = 385
    Align = alLeft
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 53
      Top = 1
      Height = 383
    end
    object Splitter4: TSplitter
      Left = 121
      Top = 1
      Height = 383
    end
    object Splitter5: TSplitter
      Left = 184
      Top = 1
      Height = 383
    end
    object lbComp: TListBox
      Left = 1
      Top = 1
      Width = 52
      Height = 383
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 0
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbMat: TListBox
      Left = 56
      Top = 1
      Width = 65
      Height = 383
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 1
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbGood: TListBox
      Left = 124
      Top = 1
      Width = 60
      Height = 383
      Align = alLeft
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 2
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
    object lbIns: TListBox
      Left = 187
      Top = 1
      Width = 61
      Height = 383
      Align = alClient
      Color = clBtnFace
      ItemHeight = 13
      TabOrder = 3
      OnClick = lbCompClick
      OnKeyPress = lbCompKeyPress
    end
  end
  object Panel1: TPanel
    Left = 252
    Top = 42
    Width = 547
    Height = 385
    Align = alClient
    TabOrder = 3
    object Splitter2: TSplitter
      Left = 1
      Top = 169
      Width = 545
      Height = 3
      Cursor = crVSplit
      Align = alTop
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 545
      Height = 32
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
    end
    object dgArt: TM207IBGrid
      Left = 1
      Top = 33
      Width = 545
      Height = 136
      Align = alTop
      Color = clBtnFace
      DataSource = dm.dsArt
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      IniStorage = FormStorage1
      TitleButtons = True
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = True
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 57
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FULLART'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 84
          Visible = True
        end
        item
          Alignment = taLeftJustify
          Color = clInfoBk
          Expanded = False
          FieldName = 'UNITID'
          PickList.Strings = (
            #1043#1088
            #1064#1090)
          Title.Alignment = taCenter
          Title.Caption = #1045#1048
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 30
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Expanded = False
          FieldName = 'RQ'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1050'-'#1074#1086' '#1086#1089#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 35
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Expanded = False
          FieldName = 'RW'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 49
          Visible = True
        end>
    end
    object dgA2: TM207IBGrid
      Left = 1
      Top = 172
      Width = 545
      Height = 212
      Align = alClient
      Color = clBtnFace
      DataSource = dm.dsA2
      Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      IniStorage = FormStorage1
      TitleButtons = True
      MultiShortCut = 0
      ColorShortCut = 0
      InfoShortCut = 0
      ClearHighlight = False
      SortOnTitleClick = True
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'ART2'
          Title.Alignment = taCenter
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 89
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'nds'
          Title.Alignment = taCenter
          Title.Caption = #1053#1044#1057
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 40
          Visible = True
        end
        item
          Alignment = taLeftJustify
          Color = clInfoBk
          Expanded = False
          FieldName = 'USEMARGIN'
          PickList.Strings = (
            #1044#1072
            #1053#1077#1090)
          Title.Alignment = taCenter
          Title.Caption = #1053#1072#1094'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 29
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Color = clAqua
          Expanded = False
          FieldName = 'PRICE1'
          Title.Alignment = taCenter
          Title.Caption = #1055#1088'.'#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 48
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRICE2'
          Title.Alignment = taCenter
          Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 53
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'W'
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 35
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Expanded = False
          FieldName = 'RQ'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 42
          Visible = True
        end
        item
          ButtonStyle = cbsEllipsis
          Expanded = False
          FieldName = 'RW'
          ReadOnly = True
          Title.Alignment = taCenter
          Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clNavy
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 40
          Visible = True
        end>
    end
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 152
    Top = 162
  end
end
