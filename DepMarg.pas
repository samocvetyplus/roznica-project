unit DepMarg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, Menus, StdCtrls, Mask, DBCtrls, RxDBComb, jpeg,
  rxSpeedbar;

type
  TfmDepMargin = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    dg1: TM207IBGrid;
    pm1: TPopupMenu;
    siSet: TSpeedItem;
    N1: TMenuItem;
    tb2: TSpeedBar;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siSetClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmDepMargin: TfmDepMargin;

implementation

uses comdata, Data, M207Proc;

{$R *.DFM}

procedure TfmDepMargin.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDepMargin.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDepMargin.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmDepMargin.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dmCom, dm do
    begin
      OpenDataSets([taDepMargin, taRec]);
    end;
end;

procedure TfmDepMargin.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmCom, dm  do
    begin
      PostDataSets([taDepMargin, taRec]);
      CloseDataSets([taDepMargin, taRec]);
    end;
end;

procedure TfmDepMargin.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and (Field.FieldName='NAME') then Background:=dmCom.clMoneyGreen;
end;

procedure TfmDepMargin.siSetClick(Sender: TObject);
begin
  with dmCom, dm, quTmp do
    begin
      PostDataSets([taDepMargin]);
      SQL.Text:='execute procedure SETMARGIN '+taDepMarginD_DepId.AsString;
      ExecQuery;
      tr.CommitRetaining;
      with taDepMArgin do
        begin
          Active:=False;
          Open;
        end;
    end;
end;

procedure TfmDepMargin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;  
end;

end.
