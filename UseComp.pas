unit UseComp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGridEh, ComCtrls;

type
  TfmUseComp = class(TForm)
    pbutton: TPanel;
    btclose: TButton;
    pc1: TPageControl;
    Art2: TTabSheet;
    D_art: TTabSheet;
    sinfo: TTabSheet;
    sinv: TTabSheet;
    dgart2: TDBGridEh;
    dgd_art: TDBGridEh;
    dgSinfo: TDBGridEh;
    dgsinv: TDBGridEh;
    tbPayMent: TTabSheet;
    DBGridEh1: TDBGridEh;
    tbRemains: TTabSheet;
    DBGridEh2: TDBGridEh;
    procedure btcloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmUseComp: TfmUseComp;

implementation
uses data3, comdata, M207Proc;
{$R *.dfm}

procedure TfmUseComp.btcloseClick(Sender: TObject);
begin
 close;
end;

procedure TfmUseComp.FormCreate(Sender: TObject);
begin
 OpenDataSets([dm3.quCompD_art, dm3.quCompSinfo, dm3.quCompSinv, dm3.quCompArt2,
  dm3.quCompBalans, dm3.quCompRemains]);
end;

procedure TfmUseComp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 CloseDataSets([dm3.quCompD_art, dm3.quCompSinfo, dm3.quCompSinv, dm3.quCompArt2,
  dm3.quCompBalans, dm3.quCompRemains]);
end;

end.
