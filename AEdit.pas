unit AEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, RxDBComb, DBCtrlsEh,
  DBLookupEh, DBGridEh, rxPlacemnt;

type
  TfmAEdit = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lcProd: TDBLookupComboBox;
    lcMat: TDBLookupComboBox;
    lcGood: TDBLookupComboBox;
    lcIns: TDBLookupComboBox;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    lcUnit: TRxDBComboBox;
    FormStorage1: TFormStorage;
    lcCountry: TDBLookupComboBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lcAtt1: TDBLookupComboboxEh;
    lcAtt2: TDBLookupComboboxEh;
    btnOk: TButton;
    btnCancel: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAEdit: TfmAEdit;

implementation

uses Data, Data2, comdata, M207Proc;

{$R *.DFM}

procedure TfmAEdit.FormCreate(Sender: TObject);
begin
  OpenDataSets([dm2.quProd, dm2.quMat, dm2.quGood, dm2.quIns, dm2.quCountry, dm2.taAtt1, dm2.taAtt2]);
  SetCBDropDown(lcProd);
  SetCBDropDown(lcMat);
  SetCBDropDown(lcGood);
  SetCBDropDown(lcIns);
  SetCBDropDown(lcCountry);
  dm.quArt.Edit;
end;

procedure TfmAEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if ModalResult=mrOK then PostDataSet(dm.quArt)
  else CancelDataSet(dm.quArt);
  with dm2 do
    CloseDataSets([quProd, quMat, quGood, quIns, quCountry, taAtt1, taAtt2]);
end;

procedure TfmAEdit.lcProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key = VK_RETURN) then
     ModalResult := mrOk;
end;

end.

