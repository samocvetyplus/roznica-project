unit RI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, DBCtrls, StdCtrls, db, jpeg, rxSpeedbar;

type
  TfmRI = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmRI: TfmRI;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmRI.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmRI.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dm2 do
    begin
      taRI.Active:=True;
    end;
end;

procedure TfmRI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 with dm, dm2 do
    begin
      PostDataSet(taRI);
      taRI.Active:=False;
      WChar := #0
    end;
end;

procedure TfmRI.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmRI.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmRI.SpeedItem2Click(Sender: TObject);
begin
  with dm2, taRI do
    begin
      Append;
      taRISZ.AsString:='-';
    end;
end;

procedure TfmRI.SpeedItem1Click(Sender: TObject);
begin
  dm2.taRI.Delete;
end;

procedure TfmRI.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field<>NIL then
    if Field.FieldName='UID' then
      if dm2.taRIDone.AsInteger=0 then Background:=dmCom.clMoneyGreen
      else Background:=clBtnFace;
end;

procedure TfmRI.FormActivate(Sender: TObject);
begin
  ActiveControl:=dg1;
  dg1.SelectedIndex:=1;
  with dm, dm2, taRI do
  begin
    Open;
    dg1.SelectedIndex:=1;
    if WChar<>#0 then
    begin
      Insert;
      taRIW.AsString:=WChar;
    end;
  end;
end;

procedure TfmRI.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE : with dg1.DataSource.DataSet do
                begin
                  if (State <> dsInsert) and (State <> dsEdit) then Self.Close;
                end;
  end;
end;

end.
