object fmUIDPHist: TfmUIDPHist
  Left = 437
  Top = 226
  Width = 649
  Height = 474
  HelpContext = 100530
  Caption = #1048#1089#1090#1086#1088#1080#1103' '#1094#1077#1085#1099' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 641
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 51
      Height = 13
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object seUID: TRxSpinEdit
      Left = 68
      Top = 4
      Width = 121
      Height = 21
      Color = clInfoBk
      TabOrder = 0
      OnKeyDown = seUIDKeyDown
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 555
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siOpen: TSpeedItem
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = siOpenClick
      SectionName = 'Untitled (0)'
    end
    object siPrOrd: TSpeedItem
      BtnCaption = #1055#1088#1080#1082#1072#1079
      Caption = #1055#1088#1080#1082#1072#1079
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1094#1077#1085#1099
      ImageIndex = 4
      Layout = blGlyphLeft
      Spacing = 1
      Left = 275
      Top = 3
      Visible = True
      OnClick = siPrOrdClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Layout = blGlyphLeft
      Spacing = 1
      Left = 451
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 426
    Width = 641
    Height = 19
    Panels = <>
  end
  object M207IBGrid2: TM207IBGrid
    Left = 0
    Top = 29
    Width = 641
    Height = 397
    Align = alClient
    Color = clBtnFace
    DataSource = dm2.dsUIDPHist
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = FormStorage1
    OnGetCellParams = M207IBGrid2GetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Expanded = False
        FieldName = 'CLOSEDATE'
        Title.Alignment = taCenter
        Title.Caption = #1044#1072#1090#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRORD'
        Title.Alignment = taCenter
        Title.Caption = #8470' '#1087#1088'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 86
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SNstr'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 76
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CLOSEEMP'
        Title.Alignment = taCenter
        Title.Caption = #1047#1072#1082#1088#1099#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SETEMP'
        Title.Alignment = taCenter
        Title.Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 91
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'PRICE'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088'. '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'PRICE2'
        Title.Alignment = taCenter
        Title.Caption = #1053#1086#1074'.'#1088#1072#1089'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clYellow
        Expanded = False
        FieldName = 'OPTPRICE'
        Title.Alignment = taCenter
        Title.Caption = #1054#1087#1090' '#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DEPNAME'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OLDP2'
        Title.Alignment = taCenter
        Title.Caption = #1057#1090'.'#1088#1072#1089'.'#1094#1077#1085#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 228
    Top = 192
  end
  object pm1: TPopupMenu
    Left = 36
    Top = 116
    object N1: TMenuItem
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088' '#1087#1088#1080#1082#1072#1079#1072' '#1085#1072' '#1094#1077#1085#1099
      ShortCut = 114
      OnClick = siPrOrdClick
    end
  end
end
