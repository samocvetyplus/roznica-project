object ot_form: Tot_form
  Left = 0
  Top = 0
  Caption = #1054#1090#1082#1072#1079' '#1086#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1079#1072#1082#1072#1079#1072
  ClientHeight = 244
  ClientWidth = 424
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 424
    Height = 41
    Align = alTop
    TabOrder = 0
    ExplicitLeft = 48
    ExplicitWidth = 185
    object Label1: TLabel
      Left = 28
      Top = 14
      Width = 344
      Height = 15
      Caption = #1042#1074#1077#1076#1080#1090#1077' '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081' ('#1087#1088#1080#1095#1080#1085#1091' '#1086#1090#1082#1072#1079#1072') '#1086#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1079#1072#1082#1072#1079#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 424
    Height = 104
    Align = alTop
    Caption = 'Panel2'
    TabOrder = 1
    ExplicitWidth = 622
    object Coment: TMemo
      Left = 1
      Top = 1
      Width = 422
      Height = 102
      Align = alClient
      Lines.Strings = (
        'Coment')
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = -28
      ExplicitWidth = 622
      ExplicitHeight = 133
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 145
    Width = 424
    Height = 56
    Align = alTop
    TabOrder = 2
    object OKBtn: TBitBtn
      Left = 28
      Top = 22
      Width = 144
      Height = 31
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Times New Roman'
      Font.Style = []
      ModalResult = 1
      ParentFont = False
      TabOrder = 0
      OnClick = OKBtnClick
      NumGlyphs = 2
    end
    object CancelBtn: TButton
      Left = 228
      Top = 21
      Width = 144
      Height = 31
      Cancel = True
      Caption = #1054#1090#1084#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Times New Roman'
      Font.Style = []
      ModalResult = 2
      ParentFont = False
      TabOrder = 1
      OnClick = CancelBtnClick
    end
  end
end
