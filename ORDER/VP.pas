unit VP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ADODB, dxSkinsdxBarPainter, dxBarDBNav, Tabs, DockTabSet, dxBar,
  ComCtrls, ToolWin, ActnList, ImgList, rxSpeedbar, jpeg, ExtCtrls, StdCtrls,
  cxDBLookupComboBox, cxDropDownEdit,
  StrUtils, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase, cxButtonEdit,
  cxGridBandedTableView, cxGridDBBandedTableView,  

   Provider, dxLayoutControl, Menus, dxLayoutLookAndFeels,
   cxButtons, DateUtils,   cxTextEdit, cxBarEditItem,
  cxCurrencyEdit,   pFIBScripter, cxCalendar, Grids, DBGrids, Buttons, uDialog;

type
  TVP_FORM = class(TForm)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1SDATE: TcxGridDBColumn;
    cxGrid1DBTableView1SN: TcxGridDBColumn;
    cxGrid1DBTableView1SNAME: TcxGridDBColumn;
    Label1: TLabel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    procedure OkBtnClick(Sender: TObject);
    procedure CancelBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    ART_SINV: String;
  end;

var
  VP_FORM: TVP_FORM;

implementation
  uses comdata, ind_order, IndividualOrderData, VP_UID;
{$R *.dfm}

procedure TVP_FORM.CancelBtnClick(Sender: TObject);
begin

close;
end;

procedure TVP_FORM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
dmIndividualOrder.SINV_DataSet.Close;
dmIndividualOrder.DataSet_ART_SINV.Close;

end;

procedure TVP_FORM.FormCreate(Sender: TObject);
begin
dmIndividualOrder.SINV_DataSet.Close;
dmIndividualOrder.SINV_DataSet.Open;


cxGrid1DBTableView1.DataController.DataSource:= dmIndividualOrder.DataSourse_List1;

end;

procedure TVP_FORM.FormDestroy(Sender: TObject);
begin
dmIndividualOrder.SINV_DataSet.Close;
dmIndividualOrder.DataSet_ART_SINV.Close;
end;

procedure TVP_FORM.OkBtnClick(Sender: TObject);
var
DataSet: TDataSet;
SALE_ART: TVP_UID_FORM;

begin

if (dmCom.HereName <> '�����') then

   DataSet:=dmIndividualOrder.dtOrder_in;


if (dmCom.HereName = '�����') then

   DataSet:=dmIndividualOrder.dtOrder_factory;

Screen.Cursor:=crHourGlass;
dmIndividualOrder.DataSet_ART_SINV.ParamByName('SINVID').Value:= dmIndividualOrder.SINV_DataSet.FieldByName('SINVID').AsString ;
dmIndividualOrder.DataSet_ART_SINV.ParamByName('Art').Value:= DataSet.FieldByName('Art').AsString ;
dmIndividualOrder.DataSet_ART_SINV.Close;
dmIndividualOrder.DataSet_ART_SINV.Open;
dmIndividualOrder.DataSet_ART_SINV.First;

dmIndividualOrder.DataSet_ART_SINV.FetchAll;
 if dmIndividualOrder.DataSet_ART_SINV.RecordCount>1 then
     begin
         SALE_ART := TVP_UID_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;
            try
                try
                  SALE_ART.ShowModal;
                  Close;
                  exit;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SALE_ART);
                Screen.Cursor := crDefault;
              end;
     end;






if (dmIndividualOrder.DataSet_ART_SINV.FieldByName('Art').AsString <>'') then
  begin
     if DataSet.State in [dsEdit, dsInsert] then
      begin

          DataSet.Post;

      end;

      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with DataSet do
         begin
          Edit;
          FieldByName('ID_State').Value:=7;
          FieldByName('EMP_STATE').Value:= dmCom.UserName;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.SINV_DataSet.FieldByName('SDATE').AsString ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SINV_DataSet.FieldByName('SN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('UID').Value:=dmIndividualOrder.DataSet_ART_SINV.FieldByName('UID').AsInteger;
          Refresh;
         end;
      dmIndividualOrder.dtHistory.Refresh;
      Screen.Cursor:=crDefault;

  end
else
    if (dmCom.HereName = '�����') then
    begin

      dmIndividualOrder.DataSet_ART_SINV.ParamByName('SINVID').Value:= dmIndividualOrder.SINV_DataSet.FieldByName('SINVID').AsString ;
      dmIndividualOrder.DataSet_ART_SINV.ParamByName('Art').Value:= DataSet.FieldByName('Art_factory').AsString ;
      dmIndividualOrder.DataSet_ART_SINV.Close;
      dmIndividualOrder.DataSet_ART_SINV.Open;
      dmIndividualOrder.DataSet_ART_SINV.First;

      dmIndividualOrder.DataSet_ART_SINV.FetchAll;
 if dmIndividualOrder.DataSet_ART_SINV.RecordCount>1 then
     begin
         SALE_ART := TVP_UID_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;
            try
                try
                  SALE_ART.ShowModal;
                  Close;
                  exit;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SALE_ART);
                Screen.Cursor := crDefault;
              end;
     end;


      if (dmIndividualOrder.DataSet_ART_SINV.FieldByName('Art').AsString <>'') then
          begin
           if DataSet.State in [dsEdit, dsInsert] then
              begin

                DataSet.Post;

              end;

            with dmIndividualOrder.dtHistory do
              begin
                Append;
                Post;
              end;

            with DataSet do
              begin
                Edit;
                FieldByName('ID_State').Value:=7;
                FieldByName('EMP_STATE').Value:= dmCom.UserName;
                FieldByName('COMMENT').Clear;
                FieldByName('DATE_DOC').Value:= dmIndividualOrder.SINV_DataSet.FieldByName('SDATE').AsString ;
                FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SINV_DataSet.FieldByName('SN').AsString ;
                FieldByName('UID').Value:=dmIndividualOrder.DataSet_ART_SINV.FieldByName('UID').AsInteger;
                FieldByName('DATE_STATE').Value:= now;
                Refresh;
              end;
            dmIndividualOrder.dtHistory.Refresh;
            Screen.Cursor:=crDefault;

          end
        else
        TDialog.Information('��������� ��������� �� �������� "��������/�������� (�� �����)" �� ������! �������� ������ ��������� ��� �������� "������� (�� �����)"!') ;

      end

    else

    TDialog.Information('��������� ��������� �� �������� "��������" �� ������! �������� ������ ���������!') ;




    Close;
 end;


end.
