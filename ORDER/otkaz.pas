unit otkaz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB, ExtCtrls;

type
  DenyType = (dtSell, dtExcecute);

  Tot_form = class(TForm)
    Coment: TMemo;
    CancelBtn: TButton;
    OKBtn: TBitBtn;
    Label1: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    procedure OKBtnClick(Sender: TObject);

    procedure CancelBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StateNext(DataSet: TDataSet; State_n: integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ot_form: Tot_form;

implementation
    uses comdata, IndividualOrderData,uDialog, ind_order;
{$R *.dfm}



procedure Tot_form.CancelBtnClick(Sender: TObject);
begin
 close;
end;

procedure Tot_form.OKBtnClick(Sender: TObject);
  var
  state: integer;
  DataSet: TDataSet;

begin
if (dmIndividualOrder.CancelFlag <> 1) then
begin
   


if (dmIndividualOrder.dtOrder_in.FieldByName('ID_STATE').AsString = '2') and (dmIndividualOrder.mode = 1)  then
Begin

  StateNext(dmIndividualOrder.dtOrder_in, 8);
  dmIndividualOrder.dtOrder_in. Refresh;

end ;

if (dmIndividualOrder.dtOrder.FieldByName('ID_STATE').AsString = '7') and (dmIndividualOrder.mode = 0)  then

begin

  StateNext(dmIndividualOrder.dtOrder, 10);
  TDialog.Information('����� �'+dmIndividualOrder.dtOrder.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');
  StateNext(dmIndividualOrder.dtOrder, 11);

  dmIndividualOrder.dtOrder. Refresh;
end ;


 if (dmIndividualOrder.dtOrder_factory.FieldByName('ID_STATE').AsString = '7') and (dmIndividualOrder.mode = 2) then

 begin

  StateNext(dmIndividualOrder.dtOrder_factory, 10);
  TDialog.Information('����� �'+dmIndividualOrder.dtOrder_factory.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');
  StateNext(dmIndividualOrder.dtOrder_factory, 11);
  dmIndividualOrder.dtOrder_factory. Refresh;
 end ;

if (dmIndividualOrder.dtOrder_factory.FieldByName('ID_STATE').AsString = '2') and (dmCom.HereName = '�����') and (dmIndividualOrder.mode = 2) then

 begin

  StateNext(dmIndividualOrder.dtOrder_factory, 4);
  dmIndividualOrder.dtOrder_factory. Refresh;
 end ;


if (dmIndividualOrder.dtOrder_factory.FieldByName('ID_STATE').AsString = '6') and (dmCom.HereName = '�����')and (dmIndividualOrder.mode = 2) then

 begin

  StateNext(dmIndividualOrder.dtOrder_factory, 8);
  dmIndividualOrder.dtOrder_factory. Refresh;
 end;
end;

if (dmIndividualOrder.CancelFlag = 1) then

begin
if dmIndividualOrder.mode=0 then    DataSet:= dmIndividualOrder.dtOrder;
if dmIndividualOrder.mode=1 then    DataSet:= dmIndividualOrder.dtOrder_in;

if dmIndividualOrder.mode=2 then    DataSet:= dmIndividualOrder.dtOrder_factory;

        StateNext(DataSet, 11);

        TDialog.Information('����� �'+DataSet.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

        DataSet. Refresh;
        dmIndividualOrder.CancelFlag := 0;

end;




end;


procedure Tot_Form.StateNext(DataSet: TDataSet; State_n: integer);
Begin

 Screen.Cursor:=crHourGlass;
 if DataSet.State in [dsEdit, dsInsert] then
   begin

     DataSet.Post;

   end;

 with dmIndividualOrder.dtHistory do
  begin
    Append;
    Post;
  end;

 with DataSet do
  begin
   Edit;
   FieldByName('ID_State').AsInteger := State_n;
   FieldByName('DATE_DOC').Clear;
   FieldByName('NUMBER_DOC').Clear;
   FieldByName('Emp_State').AsString := dmCom.UserName;
   FieldByName('Date_State').AsDateTime := now;
   FieldByName('COMMENT').Value:=Coment.text;
   Post;
//   Refresh;
  end;
 Coment.Clear;
 Screen.Cursor:=crDefault;


End;



procedure Tot_form.FormCreate(Sender: TObject);
begin



      Coment.Clear;

      if (dmIndividualOrder.CancelFlag = 1) then
      begin

        label1.Caption:='';
        label1.Caption:= '������� ������� ������ ����� �� ������� �� �������?';
      end;
end;

end.
