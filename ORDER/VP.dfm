object VP_FORM: TVP_FORM
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1042#1055
  ClientHeight = 323
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 541
    Height = 13
    Align = alTop
    Alignment = taCenter
    Caption = 
      #1042' '#1101#1090#1086#1084' '#1086#1082#1085#1077' '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086' '#1091#1082#1072#1079#1072#1090#1100' '#1042#1055' '#1074' '#1082#1086#1090#1086#1088#1086#1084' '#1073#1099#1083#1086' '#1086#1090#1087#1088#1072#1074#1083#1077#1085#1086' '#1080#1079#1076#1077 +
      #1083#1080#1077
    ExplicitWidth = 374
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 13
    Width = 541
    Height = 235
    Align = alTop
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmIndividualOrder.DataSourse_List1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1086' '#1089#1091#1097#1077#1089#1090#1074#1091#1102#1097#1080#1093' '#1042#1055
      object cxGrid1DBTableView1SN: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        DataBinding.FieldName = 'SN'
        GroupSummaryAlignment = taCenter
        HeaderAlignmentHorz = taCenter
        Width = 100
      end
      object cxGrid1DBTableView1SNAME: TcxGridDBColumn
        Caption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100' ('#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077')'
        DataBinding.FieldName = 'SNAME'
        GroupSummaryAlignment = taCenter
        HeaderAlignmentHorz = taCenter
        Width = 243
      end
      object cxGrid1DBTableView1SDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        DataBinding.FieldName = 'SDATE'
        HeaderAlignmentHorz = taCenter
        Width = 184
      end
      object cxGrid1DBTableView1Column1: TcxGridDBColumn
        DataBinding.FieldName = 'SINVID'
        Visible = False
        Width = 150
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object OkBtn: TBitBtn
    Left = 120
    Top = 266
    Width = 93
    Height = 25
    Caption = #1054#1090#1087#1088#1072#1074#1082#1072
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = OkBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object CancelBtn: TBitBtn
    Left = 284
    Top = 266
    Width = 93
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelBtnClick
    NumGlyphs = 2
  end
end
