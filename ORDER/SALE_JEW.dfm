object SALE_JEW_FORM: TSALE_JEW_FORM
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1089#1084#1077#1085#1099' '#1074' '#1088#1086#1079#1085#1080#1094#1077
  ClientHeight = 323
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 541
    Height = 13
    Align = alTop
    Alignment = taCenter
    Caption = 
      #1042' '#1101#1090#1086#1084' '#1086#1082#1085#1077' '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1086' '#1091#1082#1072#1079#1072#1090#1100' '#1057#1052#1045#1053#1059' '#1074' '#1082#1086#1090#1086#1088#1086#1081' '#1089#1086#1074#1077#1088#1096#1077#1085#1072' '#1087#1088#1086#1076#1072#1078#1072 +
      ' '#1079#1072#1082#1072#1079#1072
    ExplicitWidth = 402
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 13
    Width = 541
    Height = 200
    Align = alTop
    TabOrder = 0
    object cxGrid1DBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dmIndividualOrder.DataSourse_List
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = #1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1086' '#1087#1088#1086#1076#1072#1078#1072#1093
      object RN: TcxGridDBColumn
        Caption = #1053#1086#1084#1077#1088' '#1089#1084#1077#1085#1099
        DataBinding.FieldName = 'RN'
        HeaderAlignmentHorz = taCenter
        Width = 213
      end
      object BD: TcxGridDBColumn
        Caption = #1044#1072#1090#1072' '#1089#1084#1077#1085#1099
        DataBinding.FieldName = 'BD'
        HeaderAlignmentHorz = taCenter
        Width = 313
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object OkBtn: TBitBtn
    Left = 120
    Top = 266
    Width = 93
    Height = 25
    Caption = #1055#1088#1086#1076#1072#1078#1072
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = OkBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object CancelBtn: TBitBtn
    Left = 284
    Top = 266
    Width = 93
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ModalResult = 2
    ParentFont = False
    TabOrder = 2
    OnClick = CancelBtnClick
    NumGlyphs = 2
  end
end
