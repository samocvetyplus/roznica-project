unit sale_jew;

interface

uses
     Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, rxPlacemnt, StdCtrls, Buttons, jpeg, DB, FIBDatabase,
  pFIBDatabase, FIBDataSet,  DBClient,   FileCtrl,
  Mask, DBCtrls,  IBCustomDataSet, IBDatabase,Grids,
  IBQuery, IBStoredProc, IBTable,  
  pFIBQuery, {IdTCPServer,} ShlObj,  ShellApi,
  dxGDIPlusClasses,     Consts,   
     RxGrdCpt,     IniFiles,
  AppEvnts, Winsock, ComDrv32, dbUtil, TypInfo, ComCtrls,
  Provider, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, 
  pFIBDataSet, DBGrids, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, uDialog;

type
  TSALE_JEW_FORM = class(TForm)
    Label1: TLabel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    RN: TcxGridDBColumn;
    BD: TcxGridDBColumn;
    procedure CancelBtnClick(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SALE_JEW_FORM: TSALE_JEW_FORM;

implementation
   uses comdata, ind_order, IndividualOrderData, SALE_ART;
{$R *.dfm}

procedure TSALE_JEW_FORM.CancelBtnClick(Sender: TObject);
begin
Close;
end;

procedure TSALE_JEW_FORM.FormCreate(Sender: TObject);
begin
dmIndividualOrder.SELL_DataSet.Active:=true;

cxGrid1DBTableView1.DataController.DataSource:= dmIndividualOrder.DataSourse_List;
end;

procedure TSALE_JEW_FORM.FormDestroy(Sender: TObject);
begin
dmIndividualOrder.SELL_DataSet.close;

dmIndividualOrder.DataSet_ART_SELL.close;
end;

procedure TSALE_JEW_FORM.OkBtnClick(Sender: TObject);
var
SALE_ART: TSALE_ART_Form;
begin
 Screen.Cursor:=crHourGlass;

 if dmIndividualOrder.mode=0 then
 begin

dmIndividualOrder.DataSet_ART_SELL.ParamByName('sellid').Value:= dmIndividualOrder.SELL_DataSet.FieldByName('sellid').AsString ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('userid').Value:= 1 ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('Art').Value:= dmIndividualOrder.dtOrder.FieldByName('Art').AsString ;
dmIndividualOrder.DataSet_ART_SELL.Close;
dmIndividualOrder.DataSet_ART_SELL.Open;
dmIndividualOrder.DataSet_ART_SELL.First;

if (dmIndividualOrder.DataSet_ART_SELL.FieldByName('Art').AsString <>'') then
  begin
        dmIndividualOrder.DataSet_ART_SELL.FetchAll;
        if  dmIndividualOrder.DataSet_ART_SELL.RecordCount>1 then
        begin

              SALE_ART := TSALE_ART_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;
            try
                try
                  SALE_ART.ShowModal;
                  Close;
                  exit;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SALE_ART);
                Screen.Cursor := crDefault;
              end;

         end;



     if dmIndividualOrder.dtOrder.State in [dsEdit, dsInsert] then
      begin

          dmIndividualOrder.dtOrder.Post;

      end;
      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with dmIndividualOrder.dtOrder do
         begin
          Edit;
          FieldByName('ID_State').Value:=9;
          FieldByName('EMP_STATE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('EMP').AsString;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('Adate').AsDateTime ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SELL_DataSet.FieldByName('RN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('PRICE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('COST').AsFloat;
          FieldByName('UID').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('UID').AsInteger;
          Post;
         end;


      TDialog.Information('����� �'+dmIndividualOrder.dtOrder.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

      with dmIndividualOrder.dtHistory do
        begin
          Append;
          Post;
      end;

      with dmIndividualOrder.dtOrder do
        begin
          Edit;
          FieldByName('ID_State').AsInteger := 11;
          FieldByName('DATE_DOC').Clear;
          FieldByName('NUMBER_DOC').Clear;
          FieldByName('Emp_State').AsString := dmCom.UserName;
          FieldByName('Date_State').AsDateTime := now;
          //FieldByName('PRICE').Clear;
          Post;
          Refresh;
        end;

          dmIndividualOrder.dthistory.Refresh;




  end
else     TDialog.Information('�������� ������ �����, ��� ����������� �������!') ;

 end;

if (dmIndividualOrder.mode=2) then
 begin

dmIndividualOrder.DataSet_ART_SELL.ParamByName('sellid').Value:= dmIndividualOrder.SELL_DataSet.FieldByName('sellid').AsString ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('userid').Value:= 1 ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('Art').Value:= dmIndividualOrder.dtOrder_factory.FieldByName('Art').AsString ;
dmIndividualOrder.DataSet_ART_SELL.Close;
dmIndividualOrder.DataSet_ART_SELL.Open;
dmIndividualOrder.DataSet_ART_SELL.First;

if (dmIndividualOrder.DataSet_ART_SELL.FieldByName('Art').AsString <>'') then
  begin
        dmIndividualOrder.DataSet_ART_SELL.FetchAll;
     if  dmIndividualOrder.DataSet_ART_SELL.RecordCount>1 then
        begin

              SALE_ART := TSALE_ART_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;

            try
                try
                  SALE_ART.ShowModal;
                  Close;
                  exit;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SALE_ART);
                Screen.Cursor := crDefault;
              end;

         end;

     if dmIndividualOrder.dtOrder_factory.State in [dsEdit, dsInsert] then
      begin

          dmIndividualOrder.dtOrder_factory.Post;

      end;
      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with dmIndividualOrder.dtOrder_factory do
         begin
          Edit;
          FieldByName('ID_State').Value:=9;
          FieldByName('EMP_STATE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('EMP').AsString;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('Adate').AsDateTime ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SELL_DataSet.FieldByName('RN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('PRICE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('COST').AsFloat;
          FieldByName('UID').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('UID').AsInteger;
          Post;
         end;

      TDialog.Information('����� �'+dmIndividualOrder.dtOrder_factory.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

      with dmIndividualOrder.dtHistory do
        begin
          Append;
          Post;
      end;

      with dmIndividualOrder.dtOrder_factory do
        begin
          Edit;
          FieldByName('ID_State').AsInteger := 11;
          FieldByName('DATE_DOC').Clear;
          FieldByName('NUMBER_DOC').Clear;
          FieldByName('Emp_State').AsString := dmCom.UserName;
          FieldByName('Date_State').AsDateTime := now;
          //FieldByName('PRICE').Clear;
          Post;
          Refresh;
        end;

          dmIndividualOrder.dthistory.Refresh;



  end
else
  Begin
dmIndividualOrder.DataSet_ART_SELL.ParamByName('sellid').Value:= dmIndividualOrder.SELL_DataSet.FieldByName('sellid').AsString ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('userid').Value:= 1 ;
dmIndividualOrder.DataSet_ART_SELL.ParamByName('Art').Value:= dmIndividualOrder.dtOrder_factory.FieldByName('Art_factory').AsString ;
dmIndividualOrder.DataSet_ART_SELL.Close;
dmIndividualOrder.DataSet_ART_SELL.Open;
dmIndividualOrder.DataSet_ART_SELL.First;

if (dmIndividualOrder.DataSet_ART_SELL.FieldByName('Art').AsString <>'') then
  begin
     dmIndividualOrder.DataSet_ART_SELL.FetchAll;
     if  dmIndividualOrder.DataSet_ART_SELL.RecordCount > 1 then
         begin

              SALE_ART := TSALE_ART_FORM.Create(Application);
              Screen.Cursor:=crHourGlass;
            try
                try
                  SALE_ART.ShowModal;
                  Close;
                  exit;
                except
                  On E: Exception do
                  begin
                    Screen.Cursor := crDefault;
                    MessageDlg('������ �������� �����!', mtError, [mbOk], 0);
                  end;
                end;
              finally
                FreeAndNil(SALE_ART);
                Screen.Cursor := crDefault;
              end;
             
         end;

     if dmIndividualOrder.dtOrder_factory.State in [dsEdit, dsInsert] then
      begin

          dmIndividualOrder.dtOrder_factory.Post;

      end;
      with dmIndividualOrder.dtHistory do
         begin
          Append;
          Post;
         end;

       with dmIndividualOrder.dtOrder_factory do
         begin
          Edit;
          FieldByName('ID_State').Value:=9;
          FieldByName('EMP_STATE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('EMP').AsString;
          FieldByName('COMMENT').Clear;
          FieldByName('DATE_DOC').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('Adate').AsDateTime ;
          FieldByName('NUMBER_DOC').Value:=dmIndividualOrder.SELL_DataSet.FieldByName('RN').AsString ;
          FieldByName('DATE_STATE').Value:= now;
          FieldByName('PRICE').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('COST').AsFloat;
          FieldByName('UID').Value:= dmIndividualOrder.DataSet_ART_SELL.FieldByName('UID').AsInteger;
          Post;
         end;
          
      TDialog.Information('����� �'+dmIndividualOrder.dtOrder_factory.FieldByName('ID_Order').AsString +' ������ � ��������� �� ������� "����������� ������" ');

      with dmIndividualOrder.dtHistory do
        begin
          Append;
          Post;
      end;

      with dmIndividualOrder.dtOrder_factory do
        begin
          Edit;
          FieldByName('ID_State').AsInteger := 11;
          FieldByName('DATE_DOC').Clear;
          FieldByName('NUMBER_DOC').Clear;
          FieldByName('Emp_State').AsString := dmCom.UserName;
          FieldByName('Date_State').AsDateTime := now;
         // FieldByName('PRICE').Clear;
          Post;
          Refresh;
        end;

          dmIndividualOrder.dthistory.Refresh;



  end

 else

TDialog.Information('�������� ������ �����, ��� ����������� �������!') ;

 end;

 end;


Close;
Screen.Cursor:=crDefault;



end;

end.
