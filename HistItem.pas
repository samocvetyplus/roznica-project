unit HistItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  StdCtrls, ComCtrls, db, Buttons, DBCtrls, rxPlacemnt,
  rxSpeedbar;

type
  TfmHistItem = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    grHistArt: TM207IBGrid;
    fmstHistItem: TFormStorage;
    Splitter1: TSplitter;
    grItem: TM207IBGrid;
    stbrHistArt: TStatusBar;
    tb3: TSpeedBar;
    spitHelp: TSpeedItem;
    tb2: TSpeedBar;
    lbFind: TLabel;
    lbArt: TLabel;
    laPeriod: TLabel;
    spbtnPeriod: TSpeedButton;
    edFindUID: TEdit;
    edFindArt: TEdit;
    SpeedbarSection2: TSpeedbarSection;
    spitFindFirst: TSpeedItem;
    spitFindNext: TSpeedItem;
    spitFindArt: TSpeedItem;
    spitCase: TSpeedButton;
    plTotal: TPanel;
    Label1: TLabel;
    lbR: TLabel;
    lbSel: TLabel;
    txtD: TDBText;
    txtS: TDBText;
    txtR: TDBText;
    Label2: TLabel;
    txtRet: TDBText;
    Label3: TLabel;
    DBText1: TDBText;
    Label4: TLabel;
    txtC: TDBText;
    Label5: TLabel;
    txtAct: TDBText;
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitFindFirstClick(Sender: TObject);
    procedure edFindUIDKeyPress(Sender: TObject; var Key: Char);
    procedure spitFindNextClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edFindUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grHistArtDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure grHistArtGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edFindArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure spitHelpClick(Sender: TObject);
    procedure spbtnPeriodClick(Sender: TObject);
    procedure spitFindArtClick(Sender: TObject);
    procedure spitCaseClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
   
  private
    FSearchEnableUID : boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure FindFirstUID;
    procedure FindNextUID;
    procedure ShowPeriod;
  public
  end;

var
  fmHistItem: TfmHistItem;

implementation

uses comdata, ServData, Data, M207Proc, StrUtils, Period, MsgDialog;

{$R *.DFM}

procedure TfmHistItem.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  spitHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmHistItem.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmHistItem.FormCreate(Sender: TObject);
var
  d,m,y : word;
begin
  tb1.Wallpaper := wp;
  tb2.Wallpaper := wp;
  tb3.Wallpaper := wp;
  if not dmCom.tr.Active then dmCom.tr.StartTransaction
  else dmCom.tr.CommitRetaining;
  Caption := '������� ��������';
  DecodeDate(dm.EndDate, y, m, d);
  dmServ.HistBD := EncodeDate(y-1, m, d);
  dmServ.HistED := dm.EndDate;
  ShowPeriod;
  dmServ.quHistItem.DataSource := dmServ.dsrHistArt1;
  FSearchEnableUID := False;
  edFindUID.Text := '';
  edFindArt.Text := dm.FindArt;
  ActiveControl := edFindArt;
    //--------- ��������� ����� ��� ��������� �� �������� ----------------//
  grHistArt.ColumnByName['R_PRICE'].Visible := CenterDep;
  grItem.ColumnByName['R_PRICE'].Visible := CenterDep;
  //---------------------------------------------------------------------//
  

  if spitCase.Tag = 1 then
  begin
    spitCase.Glyph.Handle := LoadBitMap(hInstance, 'LOCK');
    dmServ.WholeArt := 0;
    spitCase.Hint := '������ ����������';
  end
  else begin
    spitCase.Glyph.Handle := LoadBitMap(hInstance, 'UNLOCK');
    dmServ.WholeArt := 1;
    spitCase.Hint := '��������� ����������';
   end;
  if edFindArt.Text<>'' then   spitFindArtClick(nil);
end;

procedure TfmHistItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (dm.WorkMode = 'UIDWH')or(dm.WorkMode = 'SINV') then dmCom.tr.CommitRetaining
  else if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  CloseDataSets([dmServ.quHistArt1, dmServ.quHistItem,dmServ.quHistArtSum]);
  Action := caFree;
end;

procedure TfmHistItem.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmHistItem.spitFindFirstClick(Sender: TObject);
begin

  if not dmServ.quHistItem.Active then
  begin
    ActiveControl := edFindArt;
    raise Exception.Create('������� �������!');
  end;
  dmServ.UID := StrToIntDef(edFindUID.Text, 0);
  if dmServ.UID = 0 then
  begin
    ActiveControl := edFindUID;
    raise Exception.Create('������� ���������� ����������������� �����!');
  end;
  OpenDataSets([dmServ.quHistArt1, dmServ.quHistItem,dmServ.quHistArtSum]);
  FindFirstUID;
end;

procedure TfmHistItem.edFindUIDKeyPress(Sender: TObject; var Key: Char);
begin
  case key of
    #13 : if FSearchEnableUID then FindNextUID else FindFirstUID;
  end;
end;

procedure TfmHistItem.spitFindNextClick(Sender: TObject);
begin
  if not dmServ.quHistItem.Active then
  begin
    ActiveControl := edFindArt;
    raise Exception.Create('������� �������!');
  end;
  if edFindUID.Text='' then eXit;
  FindNextUID;
end;

procedure TfmHistItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    ord('z'), ord('Z'), ord('�'), ord('�') :
      begin
        if ssAlt in Shift then
          if FSearchEnableUID then FindNextUID else FindFirstUID;
      end;
    VK_F12:Close;
  end;
end;

procedure TfmHistItem.FindFirstUID;
var
  bm : TBookMark;
begin
  Screen.Cursor := crFind;
  with dmServ, quHistArt1 do
  try
    bm := GetBookmark;
    First;
    while not Eof do
    begin
      if quHistItem.Locate('R_UID', edFindUID.Text, [])
      then begin
        FSearchEnableUID := True;
        FreeBookmark(bm);
        EnableControls;
        eXit;
      end;
      Next;
    end;
    GotoBookmark(bm);
    EnableControls;
    FreeBookmark(bm);
    Screen.Cursor := crDefault;
    MessageDialog('������� �� �������', mtWarning, [mbOk], 0);
    FSearchEnableUID := False;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmHistItem.FindNextUID;
var
  bm : TBookMark;
begin
  Screen.Cursor := crFind;
  with dmServ, quHistArt1 do
  try
    quHistItem.Next;
    bm := GetBookmark;
    while not Eof do
    begin
      if quHistItem.FindNext then
        if quHistItem.LocateNext('R_UID', edFindUID.Text, []) then
        begin
          FSearchEnableUID := True;
          FreeBookmark(bm);
          eXit;
        end
        else begin
        end
      else begin
        Next;
        if quHistItem.Locate('R_UID', edFindUID.Text, [])
        then begin
          FSearchEnableUID := True;
          FreeBookmark(bm);
          eXit;
        end;
      end;
    end;
    GotoBookmark(bm);
    FreeBookmark(bm);
    EnableControls;
    MessageDialog('������� �� �������', mtWarning, [mbOk], 0);
    FSearchEnableUID := False;
  finally
    Screen.Cursor := crDefault;
  end;
end;


procedure TfmHistItem.edFindUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN : ActiveControl := grHistArt;
  end;
end;

procedure TfmHistItem.grHistArtDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  with grHistArt.Canvas do
  begin
    if Column.Field.Name  = 'Descr' then
    begin
      case dmServ.quHistArt1R_I.AsInteger of
        1  : Brush.Color := dm.CU_SBD1; // ������
        2  : Brush.Color := dm.CU_D1;   // ��
        3  : Brush.Color := dm.CU_SO1;  // ������� �������
        5  : Brush.Color := dm.CU_R;    // ������ (�������)
        6  : Brush.Color := dm.CU_RT1;  // ������ (������� �� ����������)
        7  : Brush.Color := dm.CU_SR1;  // ������� �����������
        8  : Brush.Color := dm.CU_RO1;  // ������� �� ���. �����������
        10 : Brush.Color := dm.CU_SL;   // ��������� �������;
      end;
      FillRect(Rect);
    end;
    if Column.Field.Name = 'R_DEPNAME' then
    begin
      FillRect(Rect);
    end;
  end;
end;

procedure TfmHistItem.grHistArtGetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  with grHistArt.Canvas do
  begin
    if Field.FieldName  = 'Descr' then
    begin
      case dmServ.quHistArt1R_I.AsInteger of
        1,21  : Background := dm.CU_SBD1; // ������
        2  : Background := dm.CU_D1;   // ��
        3  : Background := dm.CU_SO1;  // ������� �������
        5  : Background := dm.CU_R;    // ������ (�������)
        6  : Background := dm.CU_RT1;  // ������ (������� �� ����������)
        7  : Background := dm.CU_SR1;  // ������� �����������
        8  : Background := dm.CU_RO1;  // ������� �� ���. �����������
        10 : Background := dm.CU_SL;   // ��������� �������;
        19 : Background := dm.CU_AC;   // ���� ��������;
      end;
    end;
  end;
end;

procedure TfmHistItem.edFindArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN : ActiveControl := grHistArt;
    VK_RETURN : if Shift = [] then spitFindArtClick(nil);
  end;
end;

procedure TfmHistItem.spitHelpClick(Sender: TObject);
begin
  Application.HelpContext(100500);
end;

procedure TfmHistItem.spbtnPeriodClick(Sender: TObject);
var
  d1, d2 :TDateTime;
begin
  d1 := dmServ.HistBD;
  d2 := dmServ.HistED;
  if GetPeriod(d1, d2) then
  begin
    dmServ.HistBD := d1;
    dmServ.HistED := d2;
    ShowPeriod;
    ReOpenDataSets([dmServ.quHistArt1,dmServ.quHistArt1Sum]);
  end;
end;

procedure TfmHistItem.ShowPeriod;
begin
  laPeriod.Caption := 'C '+ DateToStr(dmServ.HistBD)+
    ' �� '+DateToStr(dmServ.HistED);
end;

procedure TfmHistItem.spitFindArtClick(Sender: TObject);
begin
  if edFindArt.Text = '' then eXit;
  Application.Minimize;
  dmServ.HistStrict := 1;
  dmServ.HistArt := AnsiReplaceStr(edFindArt.Text, '*', '%');
  dmServ.HistArt := AnsiReplaceStr(dmServ.HistArt, '.', '_');
  dmServ.WholeArt:=integer(spitCase.Tag=1);
  ReOpenDataSets([dmServ.quHistArt1, dmServ.quHistItem,dmServ.quHistArt1Sum]);
  dmServ.quHistItem.DataSource := dmServ.dsrHistArt1;
  Application.Restore;
end;

procedure TfmHistItem.spitCaseClick(Sender: TObject);
begin
  if spitCase.Tag = 1 then
  begin
    spitCase.Glyph.Handle := LoadBitMap(hInstance, 'UNLOCK');
    dmServ.WholeArt := 0;
    spitCase.Tag := 2;
    spitCase.Hint := '��������� ����������';
  end
  else begin
    spitCase.Glyph.Handle := LoadBitMap(hInstance, 'LOCK');
    dmServ.WholeArt := 1;
    spitCase.tag := 1;
    spitCase.Hint := '������ �����������';
  end;
end;

procedure TfmHistItem.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  spitHelp.Left := tb1.Width-2*tb1.BtnWidth-10;
end;

end.
