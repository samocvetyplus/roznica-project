object fmCostCard: TfmCostCard
  Left = 321
  Top = 142
  Width = 387
  Height = 186
  AutoSize = True
  Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plCost: TPanel
    Left = 0
    Top = 0
    Width = 379
    Height = 118
    Align = alClient
    TabOrder = 0
    object LCass: TLabel
      Left = 8
      Top = 24
      Width = 132
      Height = 20
      Caption = #1057#1091#1084#1084#1072' '#1087#1086' '#1082#1072#1089#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LCostCard: TLabel
      Left = 9
      Top = 65
      Width = 172
      Height = 40
      Caption = #1057#1091#1084#1084#1072' '#1087#1088#1086#1074#1077#1076#1077#1085#1085#1072#1103#13#10#1087#1086' '#1076#1080#1089#1082'. '#1082#1072#1088#1090#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LCassValue: TLabel
      Left = 216
      Top = 24
      Width = 97
      Height = 20
      Caption = 'LCassValue'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edCostCard: TEdit
      Left = 216
      Top = 72
      Width = 121
      Height = 28
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyPress = edCostCardKeyPress
    end
  end
  object plButtom: TPanel
    Left = 0
    Top = 118
    Width = 379
    Height = 41
    Align = alBottom
    TabOrder = 1
    object btOk: TBitBtn
      Left = 76
      Top = 9
      Width = 93
      Height = 25
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      TabOrder = 0
      Kind = bkOK
    end
    object btCancel: TBitBtn
      Left = 200
      Top = 8
      Width = 89
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object acList: TActionList
    Left = 64
    Top = 40
    object acEnter: TAction
      Caption = 'acEnter'
      ShortCut = 13
      OnExecute = acEnterExecute
    end
    object acEsc: TAction
      Caption = 'acEsc'
      ShortCut = 27
      OnExecute = acEscExecute
    end
  end
end
