unit DList_H;   {enoi?ey iaeeaaiuo ai}

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  db, Dialogs, DBGridEhGrouping, GridsEh, DBGridEh, OleServer, StdCtrls,
  FIBDataSet, pFIBDataSet, rxPlacemnt, Buttons, DBCtrlsEh, Mask, DBCtrls,
  ExtCtrls, FIBDatabase, pFIBDatabase;


type
  TfmHist = class(TForm)
    LaLogin: TLabel;
    LaPsw: TLabel;
    PswEd: TEdit;
    fs1: TFormStorage;
    taHist: TpFIBDataSet;
    dsrDlist_H: TDataSource;
    taEmp: TpFIBDataSet;
    taEmpALIAS: TFIBStringField;
    taEmpFIO: TFIBStringField;
    taEmpPSWD: TFIBStringField;
    DataSource1: TDataSource;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    Label9: TLabel;
    dg1: TDBGridEh;
    Button1: TButton;
    OpSBtn: TBitBtn;
    CnlSBtn: TBitBtn;
    LogEd: TComboBox;
    procedure OpSBtnClick(Sender: TObject);
    procedure CnlSBtnClick(Sender: TObject);
    procedure taHistBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PswEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LogEdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
   

  private
    { Private declarations }
  public
     { Public declarations }
  end;

var
  fmHist: TfmHist;
  var log,s:string;
  Check_Detal: boolean;
  procedure Hist(var user, st: string);
implementation

   uses comdata, data, DList, MsgDialog, DInvItem, M207IBLogin, SInv,
  SellItem, SellList;

   {$R *.dfm}
procedure Hist(var user, st: string); //���������� ������ � ������� INVHIST
begin
fmHist.taHist.Open;
with  fmHist.taHist do
begin
     fmHist.taHist.Append;

     if (dm.WorkMode='DLIST') or (dm.WorkMode='DINVITEM') then
     fmHist.taHistTYPEDOC.AsInteger := 2;

     if (dm.WorkMode='SINV') then
     fmHist.taHistTYPEDOC.AsInteger := 1;


     if (SellList.WMode='SELLLIST') or (Sellitem.WMode='SELLITEM') then
     fmHist.taHistTYPEDOC.AsInteger := 8;

      if (dm.WorkMode='DLIST') or (dm.WorkMode='DINVITEM') then
     fmHist.taHistDOCID.AsInteger := dm.taDListSINVID.AsInteger;

     if (dm.WorkMode='SINV') then
     fmHist.taHistDOCID.AsInteger := dm.taSListSINVID.AsInteger;


     if (SellList.WMode='SELLLIST') or (Sellitem.WMode='SELLITEM') then
     fmHist.taHistDOCID.AsInteger := dm.taSellListSELLID.AsInteger;

     fmHist.taHistFIO.Value := user;
     fmHist.taHistSTATUS.Value := st;
     fmHist.taHist.Post;
     fmHist.taHist.Transaction.CommitRetaining;
end;
fmHist.taHist.Close;
end;



procedure TfmHist.OpSBtnClick(Sender: TObject);
 var i: integer; //�������� ���������
begin
 taEmp.Active:=true;
  with LogEd, Items do
              begin
                 i:=IndexOf(LogEd.Text);
                 if i=-1 then
                    Insert(0, LogEd.Text)
                    else Move(i, 0);
              end;
 If not taEmp.Locate('ALIAS; PSWD',     //�������� ������������
   VarArrayOf([LogEd.Text, PswEd.Text]),
   [loCaseInsensitive, loPartialKey])
   or ((LogEd.text)='') or ((PswEd.text)='')
   or (((LogEd.text)='') and ((PswEd.text)=''))then
    begin
     showmessage('�������� ��� ������������ ��� ������!');
     pswed.Text:='';
    end
  else
   begin

   log:=LogEd.Text;
   s:='�������';
   Hist(log, s);
   if dm.WorkMode='DLIST' then fmDlist.acOpen.Execute;
   If dm.WorkMode='DINVITEM' then   fmDInvItem.acOpen.Execute;
   if (dm.WorkMode='SINV') then   fmSinv.acOpen.Execute;
   if (SellList.WMode='SELLLIST') then   fmSellList.acOpen.Execute;
   If (SellItem.WMode='SELLITEM') then  fmSellItem.acOpenSell.Execute;
  end;
     taEmp.Active:=false;
     close;
end;

procedure TfmHist.CnlSBtnClick(Sender: TObject);  //������
begin
 taHist.Active:=false;
 close;
end;

procedure TfmHist.taHistBeforeOpen(DataSet: TDataSet); //������� ������ ����� ���������
begin
  if (dm.WorkMode='DLIST') or (dm.WorkMode='DINVITEM') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taDListSINVID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 2;
  end;

   if (dm.WorkMode='SINV') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taSListSINVID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 1;
  end;

     if (SellItem.WMode='SELLITEM') or (SellList.WMode='SELLLIST') then
  begin
  taHist.ParamByName('INVID').AsInteger := dm.taSellListSELLID.AsInteger;
  taHist.ParamByName('DTYPE').AsInteger := 8;
  end;
end;

procedure TfmHist.FormCreate(Sender: TObject);
begin
   taHist.Active := True;
     // �� ��������� ������� ������
  dg1.Visible:=False;
  dg1.Enabled:=False;
  Check_Detal:=False;
end;

procedure TfmHist.FormActivate(Sender: TObject);
begin
  //LogEd.Text:=dmCom.UserName;
  with LogEd do
    Text:=Items[0];
  Pswed.SetFocus;
  if (dm.WorkMode='DLIST') or (dm.WorkMode='DINVITEM') then Caption:='�'+(dm.taDListSN.AsString);
   if (dm.WorkMode='SINV') then Caption:='�'+(dm.taSListSN.AsString);
   if (SellItem.WMode='SELLITEM')or (SellList.WMode='SELLIST') then Caption:='�'+(dm.taSellListSELLID.AsString);
 end;


procedure TfmHist.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 taHist.Active := false;
end;



procedure TfmHist.PswEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmHist.LogEdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 If (key=vk_return) and (opSBtn.Enabled=true) then OpSBtn.Click;
  if key=vk_escape then cnlSbtn.Click;
end;

procedure TfmHist.Button1Click(Sender: TObject);
begin
if not Check_Detal then // �������� �����������
  begin
     dg1.Enabled:=True;
     dg1.Visible:=True;  // ������������� ������
     Button1.Caption:='������� ��������� <<';
     Check_Detal:=True;
     dg1.DataSource.DataSet.Last; //��������� ���������
  end
  else // ��������� �����������
  begin
     dg1.Enabled:=False;
     dg1.Visible:=False;   // ������������� ������
     Button1.Caption:='������� ��������� >>';
     Check_Detal:=False;
  end
end;

end.
