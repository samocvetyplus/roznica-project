object fmRetList: TfmRetList
  Left = 9
  Top = 95
  Width = 801
  Height = 458
  Caption = '�������'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object sb: TStatusBar
    Left = 0
    Top = 412
    Width = 793
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 793
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = '�����'
      Caption = '�����'
      Hint = '������� ����'
      ImageIndex = 0
      Spacing = 1
      Left = 538
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = '�������'
      Caption = '�������'
      Hint = '������� ������'
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = '��������'
      Caption = '��������'
      Hint = '�������� ������'
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = siAddClick
      SectionName = 'Untitled (0)'
    end
    object siEdit: TSpeedItem
      BtnCaption = '��������'
      Caption = '��������'
      Hint = '�������� � ��������������'
      ImageIndex = 4
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = siEditClick
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 69
    Width = 793
    Height = 343
    Align = alClient
    Color = clBtnFace
    DataSource = dm.dsRetList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = siEditClick
    OnKeyDown = M207IBGrid1KeyDown
    IniStorage = fs1
    TitleButtons = True
    OnGetCellParams = M207IBGrid1GetCellParams
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SN'
        Title.Alignment = taCenter
        Title.Caption = '���������'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 94
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SDATE'
        Title.Alignment = taCenter
        Title.Caption = '���� ���������'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 91
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SUP'
        Title.Alignment = taCenter
        Title.Caption = '����������'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 203
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'DEP'
        Title.Alignment = taCenter
        Title.Caption = '�����'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 235
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'COST'
        Title.Alignment = taCenter
        Title.Caption = '���������'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 121
        Visible = True
      end>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 793
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 3
    InternalVer = 1
    object laDep: TLabel
      Left = 84
      Top = 8
      Width = 34
      Height = 13
      Caption = 'laDep'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object cbAll: TCheckBox
      Left = 380
      Top = 4
      Width = 97
      Height = 17
      Caption = '��� ���������'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = cbAllClick
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = '�����'
      Caption = '�����'
      DropDownMenu = dm.pmRetList
      Hint = '����� ������'
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object fs1: TFormStorage
    StoredValues = <>
    Left = 84
    Top = 132
  end
  object PopupMenu1: TPopupMenu
    Images = dmCom.ilButtons
    Left = 384
    Top = 192
    object N1: TMenuItem
      Caption = '��������'
      ImageIndex = 1
      ShortCut = 45
      OnClick = siAddClick
    end
    object N3: TMenuItem
      Caption = '�������'
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelClick
    end
    object N2: TMenuItem
      Caption = '��������'
      ImageIndex = 4
      ShortCut = 114
      OnClick = siEditClick
    end
    object N6: TMenuItem
      Caption = '�����'
      ImageIndex = 48
      ShortCut = 118
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Caption = '����� ������'
      ImageIndex = 3
      ShortCut = 117
      OnClick = N5Click
    end
    object miAll: TMenuItem
      Caption = '��� ���������'
      GroupIndex = 1
      ShortCut = 119
      OnClick = cbAllClick
    end
  end
end
