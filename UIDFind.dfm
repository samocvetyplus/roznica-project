object fmUIDFind: TfmUIDFind
  Left = 512
  Top = 135
  BorderStyle = bsDialog
  Caption = #1055#1086#1080#1089#1082' '#1080#1079#1076#1077#1083#1080#1103
  ClientHeight = 472
  ClientWidth = 331
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 52
    Height = 13
    Caption = #1048#1076'. '#1085#1086#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 71
    Height = 13
    Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 56
    Width = 50
    Height = 13
    Caption = #1052#1072#1090#1077#1088#1080#1072#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 80
    Width = 76
    Height = 13
    Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 104
    Width = 67
    Height = 13
    Caption = #1054#1089#1085'. '#1074#1089#1090#1072#1074#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 128
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 248
    Width = 55
    Height = 13
    Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 8
    Top = 152
    Width = 50
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 8
    Top = 200
    Width = 39
    Height = 13
    Caption = #1056#1072#1079#1084#1077#1088
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 8
    Top = 176
    Width = 19
    Height = 13
    Caption = #1042#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 8
    Top = 224
    Width = 26
    Height = 13
    Caption = #1062#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 8
    Top = 272
    Width = 50
    Height = 13
    Caption = #1042#1085#1077#1096#1085'. '#8470
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 8
    Top = 296
    Width = 46
    Height = 13
    Caption = #1042#1085#1091#1090#1088'. '#8470
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label14: TLabel
    Left = 8
    Top = 320
    Width = 67
    Height = 13
    Caption = #1055#1088#1080#1093#1086#1076'. '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 8
    Top = 344
    Width = 58
    Height = 13
    Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel
    Left = 8
    Top = 368
    Width = 76
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 8
    Top = 392
    Width = 29
    Height = 13
    Caption = #1042#1055' '#8470
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label18: TLabel
    Left = 8
    Top = 416
    Width = 73
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edUID: TEdit
    Left = 96
    Top = 4
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 0
    Text = 'edUID'
    OnChange = edUIDChange
  end
  object deSellDate: TDateEdit
    Left = 96
    Top = 412
    Width = 113
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 1
    OnChange = edUIDChange
  end
  object edSN: TEdit
    Left = 96
    Top = 388
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 2
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edArt: TEdit
    Left = 96
    Top = 124
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 3
    Text = 'edArt'
    OnChange = edUIDChange
  end
  object deSDate: TDateEdit
    Left = 96
    Top = 364
    Width = 109
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 4
    OnChange = edUIDChange
  end
  object lcProd: TDBLookupComboBox
    Left = 96
    Top = 28
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_COMPID'
    ListField = 'SNAME'
    ListSource = dm2.dsProd
    TabOrder = 5
    OnClick = edUIDChange
    OnKeyDown = lcSupKeyDown
  end
  object bbFind: TBitBtn
    Tag = 1
    Left = 240
    Top = 8
    Width = 75
    Height = 25
    Caption = #1053#1072#1081#1090#1080
    TabOrder = 6
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF6000F000050FFFFFFFFFFFF6660FF000050FFF000000FF6666FFF
      000050FF000000000666FFFF000050F00EEEEEE0066FFFFF00005500EEEEEEEE
      00FFFFFF0000500E99E99EEEE00F999F0000500E99999EE9E00F99F90000500E
      99E99E99999F999F0000500E99E99EE9E00F99F90000500EE999EEEEE00F999F
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object bbFindNext: TBitBtn
    Tag = 2
    Left = 240
    Top = 36
    Width = 75
    Height = 25
    Caption = #1044#1072#1083#1077#1077
    TabOrder = 7
    OnClick = bbFindClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0050FFFFFFFFFF
      FFFFFFFF000050F000FFF00000FFF0FF000050FFFFFFFFFFFFFF000F0000500F
      00000FFFFFF2200F000050FFFFFFFFFFFF2222FF000050FFF000000FF2222FFF
      000050FF000000000222FFFF000050F00EEEEEE0022FFFFF00005500EEEEEEEE
      00FF00FF0000500E0E0E000EE00FFFFF0000500E000E000EE00FFFFF0000500E
      0E0E00EEE00FFFFF0000500EE0EE000EE00FFFFF0000500EEEEEEEEEE00FFFFF
      00005500EEEEEEEE00000000000055500EEEEEE0055555550000555500000000
      5555555500005555500000055555555500005555555555555555555500005555
      55555555555555550000}
  end
  object bbOK: TBitBtn
    Left = 240
    Top = 92
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 8
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object edSPrice: TEdit
    Left = 96
    Top = 316
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 9
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edSF0: TEdit
    Left = 96
    Top = 268
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 10
    Text = 'edN'
    OnChange = edUIDChange
  end
  object lcMat: TDBLookupComboBox
    Left = 96
    Top = 52
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_MATID'
    ListField = 'NAME'
    ListSource = dm2.dsMat
    TabOrder = 11
    OnClick = edUIDChange
    OnKeyDown = lcSupKeyDown
  end
  object lcGood: TDBLookupComboBox
    Left = 96
    Top = 76
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_GOODID'
    ListField = 'NAME'
    ListSource = dm2.dsGood
    TabOrder = 12
    OnClick = edUIDChange
    OnKeyDown = lcSupKeyDown
  end
  object lcIns: TDBLookupComboBox
    Left = 96
    Top = 100
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_INSID'
    ListField = 'SNAME'
    ListSource = dm2.dsIns
    TabOrder = 13
    OnClick = edUIDChange
    OnKeyDown = lcSupKeyDown
  end
  object edArt2: TEdit
    Left = 96
    Top = 148
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 14
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edW: TEdit
    Left = 96
    Top = 172
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 15
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edSZ: TEdit
    Left = 96
    Top = 196
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 16
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edPrice: TEdit
    Left = 96
    Top = 220
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 17
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edCost: TEdit
    Left = 96
    Top = 244
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 18
    Text = 'edN'
    OnChange = edUIDChange
  end
  object edSN0: TEdit
    Left = 96
    Top = 292
    Width = 109
    Height = 21
    Color = clInfoBk
    TabOrder = 19
    Text = 'edN'
    OnChange = edUIDChange
  end
  object lcSup: TDBLookupComboBox
    Left = 96
    Top = 340
    Width = 109
    Height = 21
    Color = clInfoBk
    KeyField = 'D_COMPID'
    ListField = 'SNAME'
    ListSource = dm.dsSup
    TabOrder = 20
    OnClick = edUIDChange
    OnKeyDown = lcSupKeyDown
  end
  object cbPart: TCheckBox
    Left = 8
    Top = 444
    Width = 197
    Height = 17
    Caption = #1063#1072#1089#1090#1080#1095#1085#1086#1077' '#1089#1088#1072#1074#1085#1077#1085#1080#1077
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 21
  end
  object BitBtn1: TBitBtn
    Left = 240
    Top = 64
    Width = 75
    Height = 25
    Caption = #1057#1090#1086#1087
    TabOrder = 22
    OnClick = BitBtn1Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000130B0000130B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
      33333337777FF377FF3333993370739993333377FF373F377FF3399993000339
      993337777F777F3377F3393999707333993337F77737333337FF993399933333
      399377F3777FF333377F993339903333399377F33737FF33377F993333707333
      399377F333377FF3377F993333101933399377F333777FFF377F993333000993
      399377FF3377737FF7733993330009993933373FF3777377F7F3399933000399
      99333773FF777F777733339993707339933333773FF7FFF77333333999999999
      3333333777333777333333333999993333333333377777333333}
    NumGlyphs = 2
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 260
    Top = 272
  end
end
