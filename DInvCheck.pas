unit DInvCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, ExtCtrls, ActnList, Menus,
  PrnDbgeh, ComDrv32, jpeg, DBGridEhGrouping, GridsEh, rxSpeedbar ;

type
  TfrmDInvCheck = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    siCloseInv: TSpeedItem;
    siPrint: TSpeedItem;
    siUID: TSpeedItem;
    Panel2: TPanel;
    Label2: TLabel;
    edUid: TEdit;
    gr: TDBGridEh;
    acLst: TActionList;
    acDel: TAction;
    acClear: TAction;
    acExit: TAction;
    acCheckUID: TAction;
    acFilter: TAction;
    SpeedItem1: TSpeedItem;
    PMFilter: TPopupMenu;
    N2: TMenuItem;
    N1: TMenuItem;
    N3: TMenuItem;
    PrintDB: TPrintDBGridEh;
    acPrint: TAction;
    filtLb: TLabel;
    siHelp: TSpeedItem;
    procedure acDelExecute(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acClearExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acCheckUIDExecute(Sender: TObject);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acFilterExecute(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDInvCheck: TfrmDInvCheck;

implementation

{$R *.dfm}
uses data3, Data, ServData, dbUtil, Db, comdata, MsgDialog;

procedure TfrmDInvCheck.acDelExecute(Sender: TObject);
begin
  if dm3.quDInvCheckO_STATUS.AsInteger <> 2  then
    MessageDialog('��������� ����� ������ �������', mtWarning, [mbOk], 0)
  else
  begin
    dm3.quDInvCheck.Delete
  end
end;

procedure TfrmDInvCheck.grGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dm3.quDInvCheckO_STATUS.AsInteger=0 then Background:=clInfoBk // ��������� ��������
  else if dm3.quDInvCheckO_STATUS.AsInteger=1 then Background:=clBtnFace // ���������
  else if dm3.quDInvCheckO_STATUS.AsInteger=2 then Background:=clRed // �� �������
end;

procedure TfrmDInvCheck.acClearExecute(Sender: TObject);
var id:string;
begin
  with dm3 do
  begin
    if CHECKMode = 'DINV' then
    begin
       Id:=dm.taDListSINVID.AsString;
       ExecSQL('update DInvCheck  Set Status=0 where Status <> 2 and  SInvID = '+ dm.taDListSINVID.AsString, dmServ.quTmp );
       ReOpenDataSet(quDInvCheck);
       dmServ.quTmp.SQL.Clear;
       ExecSQL('delete from DInvCheck where Status = 2 and  SInvID = '+ Id, dmServ.quTmp);

    end
    else if CHECKMode = 'SRET' then
    begin
       ExecSQL('update DInvCheck  Set Status=0 where Status <> 2 and  SInvID = '+ dm.taSRetListSINVID.AsString, dmServ.quTmp);
       ExecSQL('delete from DInvCheck where Status = 2 and  SInvID = '+ dm.taSRetListSINVID.AsString, dmServ.quTmp);
    end;
    ReOpenDataSet(quDInvCheck);
  end;
end;

procedure TfrmDInvCheck.FormCreate(Sender: TObject);
begin
   tb1.WallPaper:=wp;
   filtLb.Caption  := '';
   dm.WorkMode := 'CHECKDINV';
   ReOpenDataSet(dm3.quDInvCheck);
   siExit.Left:=tb1.Width-tb1.BtnWidth-10;

{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;
 dmServ.ComScan.Connect;
 dmcom.SScanZ:='';        }

end;

procedure TfrmDInvCheck.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmDInvCheck.acCheckUIDExecute(Sender: TObject);
var
  FieldsRes:Variant;
  sqlstr: string;
begin
  with dm3, dm3.quDInvCheck do
  begin
    if Locate('O_UID', edUID.Text, [])  then
    begin
      if quDInvCheckO_Status.AsInteger <> 2 then
      begin
        if State <> dsEdit then Edit;
        quDInvCheckO_Status.AsInteger := 1;
        Post;
      end
    end
    else
    begin
       if CHECKMode = 'DINV' then
          sqlstr := 'select O_STATUS from DInvCheck_S('+ dm.taDListSINVID.AsString +') where O_UID = ' + edUID.Text
          else
          if CHECKMode = 'SRET' then
             sqlstr := 'select O_STATUS from DInvCheck_S('+ dm.taSRetListSINVID.AsString +') where O_UID = ' + edUID.Text;


       FieldsRes := ExecSelectSQL(sqlstr, quTmp);

       if VarIsNull(FieldsRes) then
       begin
         if State <> dsInsert then Insert;
         quDInvCheckO_UID.AsString := edUID.Text;
         quDInvCheckO_STATUS.AsInteger := 2;
         Post;
       end
    end;
    Refresh;
  end;
  edUID.Text := '';
end;

procedure TfrmDInvCheck.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
  VK_RETURN: acCheckUIDExecute(nil);
  end;

end;

procedure TfrmDInvCheck.acFilterExecute(Sender: TObject);
begin
//
end;

procedure TfrmDInvCheck.N2Click(Sender: TObject);
var sqlstr, lbstr: string;
    i: integer;
begin
  with dm3.quDInvCheck  do
  begin
    if not Transaction.Active  then Transaction.StartTransaction;
    Close;

    sqlstr := '';
    lbstr := '';
    for i := 0 to PMFilter.Items.Count-1 do
      if PMFilter.Items[i].Checked  then
      begin
        sqlstr := sqlstr + IntToStr(i) + ',';
        lbstr := lbstr + PMFilter.Items[i].Caption + ',';
      end;
    if sqlstr <>'' then
    begin
      System.delete(sqlstr, Length(sqlstr),1);
      System.delete(lbstr, Length(lbstr),1);
      SelectSQL[4] := 'where O_Status in ('+ sqlstr + ')';
      filtLb.Caption := '����������: ' + lbstr;
    end
    else
    begin
      SelectSQL[4] := ' ';
      filtLb.Caption := '';
    end;
    Open;
    Transaction.CommitRetaining;
  end
end;

procedure TfrmDInvCheck.acPrintExecute(Sender: TObject);
begin
  PrintDB.Pageheader.centertext.text :='��������� � ' + dm.taDListSN.AsString  + ' �� ' + dm.taDList.FieldByName('SDATE').AsString + ' ' + dm.taDListDEPFROM.AsString + ' - ' +  dm.taDListDEPTO.AsString+ ' ('+filtLb.Caption  +')'  ;

  PrintDB.Print;
end;

procedure TfrmDInvCheck.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfrmDInvCheck.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  dm3.quDInvCheck.Transaction.CommitRetaining;
  CloseDataSet(dm3.quDInvCheck);
  dm3.quDInvCheck.SelectSQL[4] := ' ';
  dm.WorkMode := 'DINV';  
//  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
end;

procedure TfrmDInvCheck.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100226);
end;

procedure TfrmDInvCheck.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
end;

end.
