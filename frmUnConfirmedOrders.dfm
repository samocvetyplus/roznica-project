object DialogUnconfirmedOrders: TDialogUnconfirmedOrders
  Left = 306
  Top = 150
  ActiveControl = ButtonOk
  BorderStyle = bsDialog
  Caption = #1053#1077' '#1087#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1085#1099#1077' '#1087#1088#1080#1082#1072#1079#1099
  ClientHeight = 422
  ClientWidth = 376
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 8
    Top = 8
    Width = 361
    Height = 377
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object GridDBTableView1: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = #1050#1086#1083'-'#1074#1086' = 0'
          Kind = skCount
          Column = GridDBTableView1Column3
        end>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ScrollBars = ssVertical
      OptionsView.GroupRowStyle = grsOffice11
      OptionsView.Indicator = True
      object GridDBTableView1Column4: TcxGridDBColumn
        Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
        DataBinding.FieldName = 'STATE'
        Visible = False
        GroupIndex = 0
        Options.Moving = False
      end
      object GridDBTableView1Column1: TcxGridDBColumn
        Caption = #8470
        DataBinding.FieldName = 'N'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Grouping = False
        Styles.OnGetContentStyle = GridDBTableView1Column1StylesGetContentStyle
      end
      object GridDBTableView1Column2: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'OrderDate'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Grouping = False
        Width = 264
      end
      object GridDBTableView1Column3: TcxGridDBColumn
        Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
        DataBinding.FieldName = 'Department'
        Visible = False
        GroupIndex = 1
        Options.Filtering = False
        Width = 199
      end
    end
    object GridLevel1: TcxGridLevel
      GridView = GridDBTableView1
    end
  end
  object ButtonOk: TcxButton
    Left = 296
    Top = 392
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1044#1072#1083#1077#1077
    Default = True
    ModalResult = 2
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object DataSource: TDataSource
    DataSet = UnConfirmedOrders
    Left = 32
    Top = 312
  end
  object UnConfirmedOrders: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.Prord N, p.SetDate OrderDate, d.Name Department, p.rSta' +
        'te_p State from Prord p, d_dep d'
      'where '
      ' d.d_depid = p.depid and'
      ' p.RState_P < 4 and'
      ' p.DepID <> :SelfDepartmentID'
      ' and p.SetDate < :OrderDate'
      'order by d.name, p.SetDate                '
      '                ')
    BeforeOpen = UnConfirmedOrdersBeforeOpen
    Transaction = Transaction
    Database = dmCom.db
    Left = 64
    Top = 312
    object UnConfirmedOrdersSTATE: TFIBSmallIntField
      FieldName = 'STATE'
      OnGetText = UnConfirmedOrdersSTATEGetText
    end
    object UnConfirmedOrdersN: TFIBIntegerField
      FieldName = 'N'
    end
    object UnConfirmedOrdersORDERDATE: TFIBDateTimeField
      FieldName = 'ORDERDATE'
    end
    object UnConfirmedOrdersDEPARTMENT: TFIBStringField
      FieldName = 'DEPARTMENT'
      EmptyStrToNull = True
    end
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dmCom.db
    TimeoutAction = TARollback
    Left = 96
    Top = 312
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 128
    Top = 312
    PixelsPerInch = 96
    object StyleOrder: TcxStyle
      AssignedValues = [svColor]
      Color = clNone
    end
  end
end
