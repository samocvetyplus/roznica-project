unit Opt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, RXDBCtrl, StdCtrls, Mask,
  Grids, DBGrids, M207Grid, M207IBGrid, db, Menus,  ComDrv32, DateUtils,
  DBGridEh, PrnDbgeh, ActnList, PrntsEh, Printers, DBCtrlsEh, Buttons,
  FIBQuery, pFIBQuery, FIBDataSet, pFIBDataSet, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  TfmOpt = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    edSN: TDBEdit;
    fr1: TFormStorage;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    Splitter6: TSplitter;
    DBText2: TDBText;
    pmEl: TPopupMenu;
    N1: TMenuItem;
    pmWH: TPopupMenu;
    N2: TMenuItem;
    siUID: TSpeedItem;
    N3: TMenuItem;
    siCloseInv: TSpeedItem;
    lcComp: TDBLookupComboBox;
    pc1: TPageControl;
    tsWH: TTabSheet;
    tsPrice: TTabSheet;
    dgWH: TM207IBGrid;
    M207IBGrid1: TM207IBGrid;
    spitPrint: TSpeedItem;
    Label3: TLabel;
    DBText3: TDBText;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Label2: TLabel;
    edUID: TEdit;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    Splitter7: TSplitter;
    lbCountry: TListBox;
    Splitter1: TSplitter;
    dgEl: TDBGridEh;
    pdgel: TPrintDBGridEh;
    acList: TActionList;
    acPrint: TAction;
    acAdd: TAction;
    acDelete: TAction;
    dbEdSdate: TDBEditEh;
    btDate: TBitBtn;
    siHelp: TSpeedItem;
    acUid: TAction;
    SpeedItem2: TSpeedItem;
    taUID: TpFIBDataSet;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure siCloseInvClick(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure dgWHMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure spitPrintClick(Sender: TObject);
    procedure dgWHGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgElDblClick(Sender: TObject);
    procedure dgElEditButtonClick(Sender: TObject);
    procedure dgElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure acAddUpdate(Sender: TObject);
    procedure acDeleteUpdate(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure btDateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acUidExecute(Sender: TObject);
    procedure acUidUpdate(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
    SearchEnable: boolean;
    StopFlag: boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    function  Stop: boolean;
  public
    { Public declarations }
  end;

var
  fmOpt: TfmOpt;

implementation

uses comdata, Data, DBTree, DItem, Data2, OptItem,
     ReportData, M207Proc, ServData, dbUtil, SetSDate, MsgDialog;

{$R *.DFM}

procedure TfmOpt.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmOpt.FormCreate(Sender: TObject);
var i: integer;
    c: TColumn;
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;

  with dm, dm2 do
    begin
      dgWH['DQ'].Title.Caption:=taOptListDepFrom.AsString+' - ���-��';
      dgWH['DW'].Title.Caption:=taOptListDepFrom.AsString+' - ���';

      Old_D_MatId:='.';
      D_WHArt:='';
      // taSEl.SelectSQL[4]:='ORDER BY SELID';
      OpenDataSets([taSEl, quBuyer]);
      SellMode:=True;
    end;
  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns,lbCountry,nil,nil);
  dmCom.D_Att1Id := ATT1_DICT_ROOT;
  dmCom.D_Att2Id := ATT2_DICT_ROOT;

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbCompClick(NIL);
  SearchEnable:=False;
  ceArt.Text:='';
  SearchEnable:=True;
  SetCBDropDown(lcComp);
  tsWh.Caption:=dm.taOptListDepFrom.AsString;
  edUID.Text:='';
  dm.SetCloseInvBtn(siCloseInv, dm.taOptListIsClosed.AsInteger);
  with dm, dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
          c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

        end;
    end;
  pc1.ActivePage:=tsWH;

{  if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
  if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
   else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
    else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
     else  dmserv.ComScan.ComPort:=pnCOM4;

  dmServ.ComScan.Connect;
  dmcom.SScanZ:='';    }
  ActiveControl:=edUID;
end;

procedure TfmOpt.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmOpt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom, dm2 do
    begin
      PostDataSets([taOptList, taSEl ]);
      taOptList.Refresh;
      if taOptListSINVID.IsNull then begin
       MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
       ReOpenDataSet(taOptList);
      end else if not closeinv(taOptListSINVID.AsInteger,0) then
      begin
       if EmptyInv(taOptListSINVID.AsInteger,0) then
       begin
        ExecSQL('delete from sinv where sinvid='+taOptListSINVID.AsString, dm.quTmp);
        ReOpenDataSet(taOptList);
       end
      else if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
      end;

      CloseDataSets([taSEl, quD_WH, quBuyer]);
      tr.CommitRetaining;
      taOptList.Refresh;
    end;
// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
end;

procedure TfmOpt.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmOpt.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
//  pc1Change(NIL);
end;

procedure TfmOpt.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmOpt.siCloseInvClick(Sender: TObject);
var i: integer;
    s_close, s_closed:string;
begin
  with dm do
    begin
      PostDataSets([taSEl, taOptList]);
      taOptList.Refresh;
      if taOptListSINVID.IsNull then  raise   Exception.Create('��������� �������!!!');

      if taOptListIsClosed.AsInteger=0 then
        begin
          PostDataSets([taSEl, taOptList]);
          {�������� ������� ������ � ���� ���, �� ��������� ������������� �
           ������� ����}
          if taOptListSN.IsNull then
          with quTmp do
           begin
            close;
            SQL.Text:='SELECT max(sn) FROM SINV '+
                      'WHERE ITYPE=3 '+
                      ' AND DepFromId='+taSRetListDepFromId.AsString+
                      ' AND FYEAR(SDATE)='+GetSYear(taOptListSDATE.AsDateTime);
            ExecQuery;
            if Fields[0].IsNull then i:=1
            else i:=Fields[0].AsInteger+1;
            Close;
            Transaction.CommitRetaining;
            taOptList.Edit;
            taOptListSN.AsInteger:=i;
            taOptList.Post;
            taOptList.Refresh;
           end;

          {�������� �� ������������ ������}
          with quTmp do
            begin
              SQL.Text:='SELECT COUNT(*) FROM SINV '+
                        'WHERE ITYPE=3 AND SN='+taOptListSN.AsString+
                        ' AND DepFromId='+taOptListDepFromId.AsString+
                        ' AND FYEAR(SDATE)='+GetSYear(taOptListSDATE.AsDateTime);
              ExecQuery;
              i:=Fields[0].AsInteger;
              Close;
              if i>1 then raise Exception.Create('������������ ����� ���������!!!');
            end;

          if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            begin
             with quTmp do
              begin
               {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
                SQL.Text:='select noedit, noedited from Edit_Date_Inv ('+taOptListSINVID.AsString+')';
                ExecQuery;
                s_close:=trim(Fields[0].AsString);
                s_closed:=trim(Fields[1].AsString);                
                Transaction.CommitRetaining;
                close;
              end;
             if (s_close='') and (s_closed='') then
             begin
              Screen.Cursor:=crSQLWait;
              ExecSQL('EXECUTE PROCEDURE CloseInv '+taOptListSInvId.AsString+', 3, '+IntToStr(dmCom.UserId), qutmp);
              Screen.Cursor:=crDefault;
             end
             else begin
              if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
              if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
             end;
             taOptList.Refresh;
            end;
          if s_close='' then SetCloseInvBtn(siCloseInv, 1);
        end
      else
        if MessageDialog('������� ���������', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
          begin
           Screen.Cursor:=crSQLWait;
           {�������� ���������}
            ExecSQL('UPDATE SInv SET ISCLOSED=0 WHERE SINVID='+taOptListSINVID.AsString, qutmp);
           {��������� ������� tmpdata ���� ���� ������� ������� ����� �����������}
            ExecSQL('execute procedure Before_Open_InvRet('+dm.taOptListSINVID.AsString+
                    ', 2, '+inttostr(dmcom.UserId)+')', qutmp);
           with quTmp do
           begin
            sql.Text:='select id1 from tmpdata where datatype=8 and '+
                      ' userid='+inttostr(dmcom.UserId)+' and id2='+dm.taOptListSINVID.AsString;
            ExecQuery;
            s_close:='';
            while not eof do
            begin
             s_close:=s_close+#13#10+Fields[0].AsString;
             Next;
            end;
            Transaction.CommitRetaining;
            close;
           end;
           Screen.Cursor:=crDefault;
           if s_close<>'' then
           begin
            s_close:='���� ��������� ���� �� �������� �� ���������. �������:'+s_close+
                     #13#10+'��������� �����������.';
            if MessageDialog(s_close, mtConfirmation, [mbYes, mbNo], 0)=mrYes then
            begin
             {������� ���� ����������}
             Screen.Cursor:=crSQLWait;
             ExecSQL('execute procedure Before_Open_InvRet('+dm.taOptListSINVID.AsString+
                     ', 3, '+inttostr(dmcom.UserId)+')', qutmp);
             Screen.Cursor:=crDefault;
             MessageDialog('��� ���������� ������!', mtInformation, [mbOk], 0);
            end;
            {�������� ������� tmpdata}
            ExecSQL('delete from tmpdata where datatype=8 and id2='+dm.taOptListSINVID.AsString, qutmp);
           end;
           taOptList.Refresh;
           SetCloseInvBtn(siCloseInv, 0);
          end;
    end;
end;

procedure TfmOpt.pc1Change(Sender: TObject);
begin
 Screen.Cursor:=crSQLWait;
 if pc1.ActivePage=tsPrice then
    with dm, quDPrice do
      begin
        SellMode:=False;
        Active:=False;
        Open;
      end
  else
    with dm, quD_WH do
      begin
        SellMode:=True;
        Active:=False;
        Open;
      end;
 Screen.Cursor:=crDefault;
end;

procedure TfmOpt.dgWHMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (x<12) and (y<16) and (Shift=[ssCtrl..ssLeft]) then
    with TM207IBGrid(Sender) do
      if FixedCols=0 then FixedCols:=5
      else FixedCols:=0;
end;

procedure TfmOpt.spitPrintClick(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:= gen_arr(dgel, dm.taOptListSINVID);
  try
    PrintDocument(arr, invoice_wh);
    PrintDocument(arr, facture_wh);
  finally
    Finalize(arr);
  end;

end;

procedure TfmOpt.dgWHGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Highlight then
    if Field.FieldName='OPTPRICE' then
      case dm.quD_WHOPEQ.AsInteger of
          0: Background:=clRed;
          1: Background:=clYellow;
        end
    else if (Field.FieldName='DQ') or (Field.FieldName='DW') then Background:=dmCom.clCream;    
end;

procedure TfmOpt.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var SElId, T: integer;
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        dmCom.tr.CommitRetaining;
        PostDataSets([taOptList]);
        taOptList.Refresh;
        if taOptListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
        else begin
        if taOptListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
        if lcComp.Text = '' then raise Exception.Create('������� ����������');
        with quInsDUID, Params do
          begin
            ByName['UID'].AsInteger:=StrToInt(edUID.Text);
            ByName['SINVID'].AsInteger:=taOptListSInvId.AsInteger;
            ByName['DEPFROMID'].AsInteger:=taOptListDepFromId.AsInteger;
            ByName['OPT'].AsInteger:=1;
            ExecQuery;

            SElId:=FieldByName('SELID').AsInteger;
            T:=FieldByName('T').AsInteger;
            Close;
            Transaction.CommitRetaining;
          end;
        case T of
            0: begin
                 if taSEl.Locate('SELID', SElID, []) then taSEl.Refresh;
                 taOptList.Refresh;
               end;
            1: begin
                 ReOpenDataSets([taSEl]);
                 taSEl.Locate('SELID', SElId, []);
                 taOptList.Refresh;
               end;
            2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
            3: MessageDialog('������� �������', mtInformation, [mbOK], 0);
            4: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
          end;
        end;
        edUID.Text:='';
      end;

end;

procedure TfmOpt.SpeedItem1Click(Sender: TObject);
begin
  with dmCom, dm do
  begin
   Old_D_CompId:=D_CompId;
   Old_D_MatId:=D_MatId;
   Old_D_GoodId:=D_GoodId;
   Old_D_InsId:=D_InsId;
   D_WHArt:=ceArt.Text;
   Screen.Cursor:=crSQLWait;
   ReOpenDataSets([quD_WH]);
   Screen.Cursor:=crDefault;
  end;
end;

procedure TfmOpt.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmOpt.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if cbSearch.Checked then
    begin
      StopFlag:=False;
      with dm, quD_WH do
        if Active then LocateF(dm.quD_WH, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
    end;
end;

procedure TfmOpt.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then  ceArt.SelectAll
                     else if cbSearch.Tag=0 then
                            begin
                              dm.D_WHArt:=ceArt.Text;
                              ReopenDataSets([dm.quD_WH]);
                              cbSearch.Tag:=1;
                            end
                          else
                            begin
                              ceArt.SelectAll;
                              cbSearch.Tag:=0;
                            end;
                   end;
      VK_ESCAPE: StopFlag:=True;
    end;
end;

procedure TfmOpt.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then ActiveControl:=dgWH;
end;

function TfmOpt.Stop: boolean;
begin
  Result:=StopFlag;
end;


procedure TfmOpt.dgElDblClick(Sender: TObject);
begin
  with dm do
    begin
      if taSElSElID.IsNull then exit;
      case taSElWHSell.AsInteger of
          0: ShowAndFreeForm(TfmOptItem, Self, TForm(fmOptItem), True, False);
          1: ShowAndFreeForm(TfmDItem, Self, TForm(fmDItem), True, False);
        end;
      if taSElQuantity.AsFloat=0 then
        with taSEl do
          begin
            Tag:=1;
            Delete;
            Tag:=0;
          end;
      taOptList.Refresh;
    end;
end;

procedure TfmOpt.dgElEditButtonClick(Sender: TObject);
begin
  with dgEl.SelectedField do
    if (FieldName='TOTALWEIGHT') or (FieldName='QUANTITY') then acUidExecute(NIL);
end;

procedure TfmOpt.dgElGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if (Column.Field<>NIL) and ((Column.Field.FieldName='FULLART') or (Column.Field.FieldName='ART2')) then
    case dm.taSElWHSell.AsInteger of
        0: Background:=clAqua;
        1: Background:=dmCom.clMoneyGreen;
      end;
end;

procedure TfmOpt.acPrintExecute(Sender: TObject);
begin
 VirtualPrinter.Orientation := poLandscape;
 pdgel.Title.Clear;
 pdgel.Title.Add('');
 pdgel.Title.Add('��������� ������� ������� �'+ dm.taOptListSN.AsString);
 pdgel.Title.Add('');
 pdgel.Print;
end;

procedure TfmOpt.acPrintUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=not dm.taOptListSINVID.IsNull;
end;

procedure TfmOpt.acAddExecute(Sender: TObject);
var selid:integer;
begin
  if lcComp.Text = '' then raise Exception.Create('������� ����������');

 with dm do
 begin
  if SellMode then art2id:=quD_WHArt2Id.AsInteger
  else art2id:=quDPriceArt2Id.AsInteger;

  with qutmp do
  begin
   close;
   sql.Text:='select  selid from sel where sinvid='+dm.taOptListSINVID.AsString+
             ' and art2id='+inttostr(art2id);
   ExecQuery;
   if Fields[0].IsNull then selid:=-1 else selid:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
  end;
  if selid=-1 then
   with taSEl do
   begin
    Append;
    taSElArt2Id.AsInteger:=art2id;
    Post;
    acUidExecute(NIL);
    if sellmode then quD_WH.Refresh
   end
  else
  begin
   if not taSEl.Locate('SELID', Selid, []) then
    begin
     ReOpenDataSet(taSEl);
     taSEl.Locate('SELID', Selid, [])
    end;
   acUidExecute(NIL);
   if sellmode then quD_WH.Refresh   
  end
 end; 
end;

procedure TfmOpt.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if not Highlight then
    if Field.FieldName='OPTPRICE' then
      case dm.quDPriceOPEQ.AsInteger of
          0: Background:= clRed;
          1: Background:= clYellow;
        end;
end;

procedure TfmOpt.acAddUpdate(Sender: TObject);
var i:integer;
begin
 if not dm.taOptListSINVID.isnull then
 begin
  with dm, qutmp do
  begin
   close;
   sql.Text:='select isclosed from sinv where sinvid='+taOptListSINVID.AsString;
   ExecQuery;
   i:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
  end;
  TAction(Sender).Enabled:= (i=0) and (not dm.quD_WHD_ARTID.IsNull);
 end else TAction(Sender).Enabled:=false;
end;

procedure TfmOpt.acDeleteUpdate(Sender: TObject);
var i:integer;
begin
 if not dm.taOptListSINVID.isnull then
 begin
  with dm, qutmp do
  begin
   close;
   sql.Text:='select isclosed from sinv where sinvid='+taOptListSINVID.AsString;
   ExecQuery;
   i:=Fields[0].AsInteger;
   close;
   Transaction.CommitRetaining;
  end;
  TAction(Sender).Enabled:= (i=0) and (not dm.taSElSELID.IsNull);
 end else TAction(Sender).Enabled:=false;
end;

procedure TfmOpt.acDeleteExecute(Sender: TObject);
begin
 dm.taSEl.Delete;
end;

procedure TfmOpt.btDateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taOptListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');

  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm.taOptListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm.taOptListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm.taOptList.Edit;
    dm.taOptListSDATE.AsDateTime:=d;
    dm.taOptList.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmOpt.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100252)
end;

procedure TfmOpt.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmOpt.acUidExecute(Sender: TObject);
begin
  with dm do
    begin
      if taSElSElID.IsNull then exit;
      case taSElWHSell.AsInteger of
          0: ShowAndFreeForm(TfmOptItem, Self, TForm(fmOptItem), True, False);
          1: ShowAndFreeForm(TfmDItem, Self, TForm(fmDItem), True, False);
        end;
      if taSElQuantity.AsFloat=0 then
        with taSEl do
          begin
            Tag:=1;
            Delete;
            Tag:=0;
          end;
      taOptList.Refresh;
    end;
end;

procedure TfmOpt.acUidUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=(not dm.taOptListSINVID.IsNull) and
  (not dm.taSElSELID.IsNull)
end;

procedure TfmOpt.SpeedItem2Click(Sender: TObject);
var
  Field:TField;
  Key: Word;
begin
  Key := VK_RETURN;
  taUID.Active := True;
  if taUID.Active then
  begin
    Field := taUID.FieldByName('UID');
    taUID.last;
    taUID.First;
    while not taUID.Eof do
    begin
      edUID.Text := Field.AsString;
      edUIDKeyDown(edUID, Key, []);
      taUID.Next;
    end;
  end;
end;

end.

