unit NodCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DBCtrlsEh, DBLookupEh, Buttons, DBCtrls,  rxstrutils,
  ActnList;

type
  TfmNodCard = class(TForm)
    edNodCard: TEdit;
    edClient: TEdit;
    btnok: TButton;
    btnno: TButton;
    Lbirthday: TLabel;
    btVib: TButton;
    acList: TActionList;
    acVibcard: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure edNodCardKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNodCardKeyPress(Sender: TObject; var Key: Char);
    procedure edNodCardKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edClientKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edClientKeyPress(Sender: TObject; var Key: Char);
    procedure btnokKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btnnoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edClientKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acVibcardUpdate(Sender: TObject);
    procedure acVibcardExecute(Sender: TObject);
  private
   {����� �������}
   oldclientid, newidclient:integer;
   oldclientname, newclientname, oldnodcard, newnodcard:string;
   CanselFl:boolean;
   LogOprIdForm:string;
   FlagEnter:boolean;
  end;

var
  fmNodCard: TfmNodCard;

implementation

uses Data, Data2, eClient,  M207Proc, comdata, DB, data3, JewConst, dbUtil,
     Vibcard, MsgDialog;

{$R *.dfm}


procedure TfmNodCard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
  begin
    if (ModalResult=mrOK)  then
    begin
     if ((dm.taSellItem.State=dsInsert)or(dm.taSellItem.State=dsEdit)) then
       PostDataSet(taSellItem);
     if dm.WorkMode = 'SELLCH' then
     begin
      taSellItem.First;
      while not taSellItem.Eof do
      begin
       taSellItem.Edit;
       taSellItemCLIENTID.AsInteger:=newidclient;
       taSellItemNODCARD.AsString:= newnodcard;
       taSellItem.Post;
       taSellItem.Next;
      end;
{      ExecSQL('update sellitem set CLIENTID='+inttostr(newidclient)+
              ' where CHECKNO=0 and sellid='+taCurSellSELLID.AsString, dmCom.quTmp);
      ReOpenDataSet(taSellItem);}
     end
     else
     begin
       taSellItem.Edit;
       taSellItemCLIENTID.AsInteger:=newidclient;
       taSellItemNODCARD.AsString:= newnodcard;
       taSellItem.Post;
     end;
     tr.CommitRetaining;
    end
    else
    begin
      CancelDataSet(taSellItem);
    end;
    if(SellRet)then CloseDataSets([dm.quSelled]);
  end;
end;

procedure TfmNodCard.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  case Key of
   VK_RETURN : begin
     if(ActiveControl = btnOk)then begin ModalResult := mrOk; close; end
     else if(ActiveControl = btnNo)then begin ModalResult := mrCancel; close; end;
   end;
   VK_ESCAPE : begin
      ModalResult := mrCancel; close;
    end;
  end;}
end;

procedure TfmNodCard.FormCreate(Sender: TObject);
begin
  CanselFl:=false;
  oldclientid:=dm.taSellItemCLIENTID.AsInteger;
  newidclient := dm.taSellItemCLIENTID.AsInteger;
  oldclientname:= dm.taSellItemCLIENTNAME.AsString;
  newclientname:= dm.taSellItemCLIENTNAME.AsString;
  oldnodcard:= dm.taSellItemNODCARD.AsString;
  newnodcard:= dm.taSellItemNODCARD.AsString;
  edNodCard.Text:= oldnodcard;
  edClient.Text := oldclientname;
  ActiveControl:=edNodCard;
  edNodCard.SelectAll;
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
  FlagEnter:=false;  
{ if dm.taSellItemRET.AsInteger = 1 then  edClient.Enabled:=true;}
end;

procedure TfmNodCard.edNodCardKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var FieldsRes:t_array_var;
    st, LogOperationID:string;
    FindVal :boolean;
 function FindChar (s:string; ch:char):boolean;
 var i:integer;
 begin
  result:=false;
  if (s<>'') then
  begin
   i:=0;
   while (i<=length(s)) and (s[i]<>ch) do inc(i) ;
   if (i<=length(s)) then result:=true;
  end; 
 end;
begin

with dm do
case key of
VK_RETURN:
 begin
  {����� ���������� �� ������ �����}
  ReOpenDataSets([dm2.quClient]);
  FindVal:=false;
  try
   if CenterDep then // ���� �� ���� ��������������
    begin
     dmCom.ExecuteQutmp(quTmp, 'select clientid, name, nodcard, f_lose from client where '+
                        ' fmain=1 and fdel=0 and nodcard='#39+DelRSpace(edNodCard.Text)+#39,
                        4, FieldsRes);
     if (FieldsRes[0]=null)then
     begin
      Finalize(FieldsRes);
      dmCom.ExecuteQutmp(qutmp, 'select first 1 clientid, name, nodcard, f_lose from client where '+
                         '  fdel=0 and nodcard='#39+DelRSpace(edNodCard.Text)+#39,
                          4, FieldsRes);
     end;
    end
   else // ���� ������ ��� �������� �������������
    dmCom.ExecuteQutmp(qutmp, 'select clientid, name, nodcard, f_lose from client where '+
                       ' fdel=0 and depid=gen_id(selfdepid, 0) and nodcard='#39+DelRSpace(edNodCard.Text)+#39,
                       4, FieldsRes);
   if (FieldsRes[0]<>null) and (FieldsRes[3]=1) then
   begin
     MessageDialog('����� �������!', mtWarning, [mbOk], 0);
     ActiveControl:= btnok;
     eXit;
   end;
   if ((FieldsRes[0]=null) or ((dmcom.ShowInfClient) and (FieldsRes[0]<>-1))) then
   begin
    if (FieldsRes[0]=null) then
    begin
     LogOperationID:=dm3.insert_operation(sLog_AddClient,LogOprIdForm);
     dm2.quClient.Insert;
     dm2.quClientNODCARD.AsString:= edNodCard.Text;
    end
    else
    begin
     LogOperationID:=dm3.insert_operation(sLog_EditClient,LogOprIdForm);
     if not dm2.quClient.Locate('CLIENTID', FieldsRes[0], []) then
      begin
       ReOpenDataSets([dm2.quClient]);
       dm2.quClient.Locate('CLIENTID', FieldsRes[0], [])
      end;
     FindVal:= FindChar (dm2.quClientNAME.AsString,'*');
     if not FindVal then dm2.quClient.Edit;
    end;

    if FindVal then
     begin
      newidclient := dm2.quClientCLIENTID.AsInteger;
      newclientname := dm2.quClientNAME.AsString;
      newnodcard := dm2.quClientNODCARD.AsString;
      edNodCard.Text := newnodcard;
      edClient.Text := newclientname;
      ActiveControl:= btnok;
     end
    else
    if ShowAndFreeForm(TfmeClient, Self, TForm(fmeClient), True, False)<> mrOk then
     begin
       edNodCard.Text:= oldnodcard;
       edClient.Text:= oldclientname;
       ActiveControl:= edNodCard;
       edNodCard.SelectAll;
       if (dm2.quClient.State=dsEdit) or (dm2.quClient.State=dsInsert) then
       dm2.quClient.Cancel;
       dm2.quClient.Locate('CLIENTID', oldclientid, []);
     end
     else
      begin
       if (dm2.quClient.State=dsEdit) or (dm2.quClient.State=dsInsert) then
       dm2.quClient.Post;
       newidclient := dm2.quClientCLIENTID.AsInteger;
       newclientname := dm2.quClientNAME.AsString;
       newnodcard := dm2.quClientNODCARD.AsString;
       edNodCard.Text:= newnodcard;
       edClient.Text:= newclientname;
       ActiveControl:= btnok;
      end
    end
     else
     begin
      LogOperationID:=dm3.insert_operation(sLog_EditClient,LogOprIdForm);
      newidclient := FieldsRes[0];
      if FieldsRes[1]=null then st:='' else st:= FieldsRes[1];
      newclientname := st;
      if FieldsRes[2]=null then st:='' else st:= FieldsRes[2];
      newnodcard := st;
      edNodCard.Text := newnodcard;
      edClient.Text := newclientname;
      ActiveControl:= btnok;
     end;
  finally
    dm2.quBirthday.Close;
//    dm2.quBirthday.ParamByName('CLIENTID').AsInteger:=dm2.quClientCLIENTID.AsInteger;
    dm2.quBirthday.ParamByName('CLIENTID').AsInteger:=NewidClient;
    dm2.quBirthday.ExecQuery;
    if dm2.quBirthday.Fields[0].AsInteger=1 then Lbirthday.Caption:='���� ��������!'
    else Lbirthday.Caption:='';
    dm2.quBirthday.Close;
    dm2.quBirthday.Transaction.CommitRetaining;
    FlagEnter:=true;
   Finalize(FieldsRes);
   dm3.update_operation(LogOperationID);
  end;
 end;
VK_ESCAPE: begin ModalResult := mrCancel end;
end;
end;


procedure TfmNodCard.edNodCardKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '�', '�': CanselFl:=true;
 else sysUtils.Abort;
 end;
end;

procedure TfmNodCard.edNodCardKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if CanselFl
  then begin edNodCard.Text:='��� ����. �����'; CanselFl:=false; end;

{ if (dm.taSellItemRET.AsInteger = 1) and (edNodCard.Text='') then  edClient.Enabled:=true;
 if (dm.taSellItemRET.AsInteger = 1) and (edNodCard.Text<>'') then  edClient.Enabled:=false;}   
end;

procedure TfmNodCard.edClientKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
with dm do
case key of
VK_RETURN:
 begin
  {����� ���������� �� ������ �����}
  If (dm.taSellItem.State = dsEdit) or (dm.taSellItem.State = dsInsert) then dm.taSellItem.Post;
  dmcom.tr.CommitRetaining;
  newidclient := dm.taSellItemCLIENTID.AsInteger;
  newnodcard := dm.taSellItemNODCARD.AsString;
  dm.taSellItem.Edit;
  dm.taSellItemRETCLIENT.AsString:=edClient.Text;
{ dm.taSellItem.Post;
  dmcom.tr.CommitRetaining;}
  ActiveControl := btnok;
 end;
end;
end;

procedure TfmNodCard.edClientKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '�'..'�',#8,'�'..'�','a'..'z','A'..'Z',' ', '.' :;
 else sysUtils.Abort;
 end;
end;

procedure TfmNodCard.btnokKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case key of
    VK_ESCAPE: ModalResult := mrCancel;
  end;
end;

procedure TfmNodCard.btnnoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
 VK_ESCAPE: begin ModalResult := mrCancel; close; end;
 end;
end;

procedure TfmNodCard.edClientKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{ if edClient.Text = '' then edNodCard.Enabled:=true
 else edNodCard.Enabled:=false;}
end;

procedure TfmNodCard.acVibcardUpdate(Sender: TObject);
begin
 if (not dm2.quClientCLIENTID.IsNull) and (not dm2.quClientNODCARD.IsNull) and
    (FlagEnter) then
 begin
  if (dm2.quClientCLIENTID.asInteger<>-1) and
     (dm2.quClientDEPID.AsInteger=SelfDepID) then TAction(Sender).Enabled:=true;
 end
 else TAction(Sender).Enabled:=false;
end;

procedure TfmNodCard.acVibcardExecute(Sender: TObject);
begin
 dm.IsClient:=1;
 ShowAndFreeForm(TfmVibcard, Self, TForm(fmVibcard), True, False);
 newnodcard:=dm2.quClientNODCARD.AsString;
 newidclient:=dm2.quClientCLIENTID.AsInteger;
 edNodCard.Text:=dm2.quClientNODCARD.AsString;
 edClient.Text:=dm2.quClientNAME.AsString;
end;

end.
