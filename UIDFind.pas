unit UIDFind;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Buttons, db, RxDBFilter, Variants,
  rxPlacemnt, rxToolEdit;

type
  TfmUIDFind = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edUID: TEdit;
    deSellDate: TDateEdit;
    edSN: TEdit;
    edArt: TEdit;
    deSDate: TDateEdit;
    lcProd: TDBLookupComboBox;
    bbFind: TBitBtn;
    bbFindNext: TBitBtn;
    bbOK: TBitBtn;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edSPrice: TEdit;
    edSF0: TEdit;
    lcMat: TDBLookupComboBox;
    lcGood: TDBLookupComboBox;
    lcIns: TDBLookupComboBox;
    edArt2: TEdit;
    edW: TEdit;
    edSZ: TEdit;
    edPrice: TEdit;
    edCost: TEdit;
    edSN0: TEdit;
    lcSup: TDBLookupComboBox;
    FormStorage1: TFormStorage;
    cbPart: TCheckBox;
    BitBtn1: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcSupKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bbFindClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edUIDChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    StopFlag: boolean;
    function Stop: boolean;
  public
    { Public declarations }
  end;

var
  fmUIDFind: TfmUIDFind;

implementation

uses comdata, Data, Data2, M207Proc, MsgDialog;

{$R *.DFM}

procedure TfmUIDFind.FormCreate(Sender: TObject);
begin
  with dm, dmCom, dm2 do
    begin
      quSup.Active:=True;
      quProd.Active:=True;
      quMat.Active:=True;
      quGood.Active:=True;
      quIns.Active:=True;
    end;

  SetCBDropDown(lcProd);
  SetCBDropDown(lcMat);
  SetCBDropDown(lcGood);
  SetCBDropDown(lcIns);
  SetCBDropDown(lcSup);

  lcSup.KeyValue:=-1;
  lcProd.KeyValue:=-1;
  lcMat.KeyValue:=-1;
  lcGood.KeyValue:=-1;
  lcIns.KeyValue:=-1;

  edUID.Text:='';
  edArt.Text:='';
  edArt2.Text:='';
  edW.Text:='';
  edSZ.Text:='';
  edPrice.Text:='';
  edCost.Text:='';
  edSF0.Text:='';
  edSN0.Text:='';
  edSPrice.Text:='';
  edSN.Text:='';

end;

procedure TfmUIDFind.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom, dm2 do
    begin
      quSup.Active:=True;
      quProd.Active:=True;
      quMat.Active:=True;
      quGood.Active:=True;
      quIns.Active:=True;
    end;
end;

procedure TfmUIDFind.lcSupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=8 then
    TDBLookupComboBox(Sender).KeyValue:=-1;
end;

procedure TfmUIDFind.bbFindClick(Sender: TObject);
var s: string;
    FldValues: Variant;
    Opt: TLocateOptions;
    i: integer;
begin
  StopFlag:=False;
  
  s:='';

  i:=-1;
  FldValues:=VarArrayCreate([0, 17], varVariant);
  if edUID.Text<>'' then
    begin
      Inc(i);
      s:='UID;';
      FldValues[i]:=edUID.Text;
    end;

  if lcProd.Text<>'' then
    begin
      Inc(i);
      s:=s+'D_COMPID;';
      FldValues[i]:=lcProd.KeyValue;
    end;

  if lcMat.Text<>'' then
    begin
      Inc(i);
      s:=s+'MATID;';
      FldValues[i]:=lcMat.KeyValue;
    end;

  if lcGood.Text<>'' then
    begin
      Inc(i);
      s:=s+'GOODID;';
      FldValues[i]:=lcGood.KeyValue;
    end;


  if lcIns.Text<>'' then
    begin
      Inc(i);
      s:=s+'INSID;';
      FldValues[i]:=lcIns.KeyValue;
    end;

  if edArt.Text<>'' then
    begin
      Inc(i);
      s:=s+'ART;';
      FldValues[i]:=edArt.Text;
    end;

  if edArt2.Text<>'' then
    begin
      Inc(i);
      s:=s+'ART2;';
      FldValues[i]:=edArt2.Text;
    end;

  if edSZ.Text<>'' then
    begin
      Inc(i);
      s:=s+'SZ;';
      FldValues[i]:=edSZ.Text;
    end;

  if edW.Text<>'' then
    begin
      Inc(i);
      s:=s+'W;';
      FldValues[i]:=edW.Text;
    end;

  if edPrice.Text<>'' then
    begin
      Inc(i);
      s:=s+'PRICE;';
      FldValues[i]:=edPrice.Text;
    end;

  if edCost.Text<>'' then
    begin
      Inc(i);
      s:=s+'COST;';
      FldValues[i]:=edCost.Text;
    end;

  if edSF0.Text<>'' then
    begin
      Inc(i);
      s:=s+'SSF0;';
      FldValues[i]:=edSF0.Text;
    end;

  if edSN0.Text<>'' then
    begin
      Inc(i);
      s:=s+'SN0;';
      FldValues[i]:=edSN0.Text;
    end;

  if edSPrice.Text<>'' then
    begin
      Inc(i);
      s:=s+'SPRICE0;';
      FldValues[i]:=edSPrice.Text;
    end;

  if (lcSup.Text<>'') and (lcSup.Text<>'���') then
    begin
      Inc(i);
      s:=s+'SUPID0;';
      FldValues[i]:=lcSup.KeyValue;
    end;

  if edSN.Text<>'' then
    begin
      Inc(i);
      s:=s+'SN;';
      FldValues[i]:=edSN.Text;
    end;

  if deSDate.Date>1 then
    begin
      Inc(i);
      s:=s+'SDATE;';
      FldValues[i]:=DateToSTr(deSDate.Date);
    end;

  if deSellDate.Date>1 then
    begin
      Inc(i);
      s:=s+'SELLDATE;';
      FldValues[i]:=DateToStr(deSellDate.Date);
    end;

  if cbPart.Checked then Opt:=[loPartialKey]
  else Opt:=[];

  if not Locate(dm.dsUIDWH.DataSet{quUIDWH}, s, FldValues, Opt, TComponent(Sender).Tag=2, Stop) then
     MessageDialog('������ �� �������', mtInformation, [mbOK], 0);

  case TComponent(Sender).Tag of
      1:
         begin
           bbOK.Default:=False;
           bbFind.Default:=False;
           bbFindNext.Default:=True;
         end;
     end;
end;

procedure TfmUIDFind.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_F12, VK_ESCAPE] then Close;
end;

procedure TfmUIDFind.edUIDChange(Sender: TObject);
begin
  bbOK.Default:=False;
  bbFindNext.Default:=False;
  bbFind.Default:=True;
end;

function TfmUIDFind.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmUIDFind.BitBtn1Click(Sender: TObject);
begin
  StopFlag:=True;
end;

end.
