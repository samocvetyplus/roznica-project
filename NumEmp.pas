unit NumEmp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, dbUtil;

type
  TfmNumEmp = class(TForm)
    edNumEmp: TEdit;
    edNameEmp: TEdit;
    btnok: TButton;
    btnNo: TButton;
    procedure FormCreate(Sender: TObject);
    procedure edNumEmpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edNumEmpKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
   {����� �������}
   newempid, newnumemp:integer;
   newempFIO:string;
  public
    { Public declarations }
  end;

var
  fmNumEmp: TfmNumEmp;

implementation

uses comdata, data, sellitem, M207Proc, DB, MsgDialog;

{$R *.dfm}


procedure TfmNumEmp.FormCreate(Sender: TObject);
begin
 newempid:=dm.taSellItemEMPID.AsInteger;
 newempFIO:= dm.taSellItemEMP.AsString;
 ////////////////����� ������ ��� ��������
 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select numemp from d_emp where d_empid='+dm.taSellItemEMPID.AsString;
 dm.quTmp.ExecQuery;
 edNumEmp.Text:= dm.quTmp.Fields[0].AsString;
 edNameEmp.Text := newempFIO;
 dm.quTmp.Close;
 ActiveControl:=edNumEmp;
 edNumEmp.SelectAll;
end;

procedure TfmNumEmp.edNumEmpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
with dm do
case key of
VK_RETURN:
 begin
  {����� ���������� �� ������ �����}
   if trim(edNumEmp.Text)='' then
   begin
     MessageDialog('������� ����� ��������', mtError, [mbOk], 0);
     exit;
   end;

   quTmp.Close;
   quTmp.SQL.Text:='select d_empid, numemp, FIO from d_emp '+
                   'where (d_depid='+inttostr(SelfDepId)+' or allwh=1) and discharge<>1 and numemp = '+edNumEmp.Text;
   quTmp.ExecQuery;
   if qutmp.Fields[0].IsNull then
   begin
    MessageDialog('������ ����� �������� �����������', mtError, [mbOk], 0);
   end
   else begin
      newempid := quTmp.Fields[0].AsInteger;
      newempFIO := quTmp.Fields[2].AsString;
      newnumemp := quTmp.Fields[1].AsInteger;
      edNumEmp.Text := quTmp.Fields[1].AsString;
      edNameEmp.Text := quTmp.Fields[2].AsString;
      ActiveControl:= btnok;
   end;
   qutmp.Close;

 end;
//VK_TAB: begin ActiveControl:=edClient; edClient.SelectAll; end;
VK_ESCAPE: begin ModalResult := mrCancel; close; end;
 end;
end;

procedure TfmNumEmp.edNumEmpKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
 '0'..'9',#8 : ;
 else sysUtils.Abort;
 end;

end;

procedure TfmNumEmp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm, dmCom do
  begin
    if (ModalResult=mrOK)  then
    begin
     PostDataSet(taSellItem);
     if dm.WorkMode = 'SELLCH' then
     begin
{      taSellItem.First;
      while not taSellItem.Eof do
      begin
       taSellItem.Edit;
       taSellItemEMPID.AsInteger:=newempid;
       taSellItemEMP.AsString:= newempFIO;
       taSellItem.Post;
       taSellItem.Next;
      end;}
      ExecSQL('update sellitem set EMPID='+inttostr(newempid)+
              ' where CHECKNO=0 and sellid='+taCurSellSELLID.AsString, dmCom.quTmp);
      ReOpenDataSet(taSellItem);
     end;
    end
    else
    begin
      CancelDataSet(taSellItem);
    end;
  end;
end;

end.
