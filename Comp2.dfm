object fmComp2: TfmComp2
  Left = -4
  Top = -4
  Width = 1032
  Height = 776
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081' '#1080' '#1092#1080#1079#1080#1095#1077#1089#1082#1080#1093' '#1083#1080#1094
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 441
    Top = 69
    Width = 4
    Height = 680
    Cursor = crHSplit
    AutoSnap = False
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1024
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Wallpaper.Data = {
      07544269746D6170760F0000424D760F00000000000076000000280000008000
      00003C0000000100040000000000000F00000000000000000000100000000000
      0000000000000000800000800000008080008000000080008000808000008080
      8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF00888888888888888888888888787878888888888888788887888888888888
      888088F8FF8FFFF888F88F88F8F8F8F8F8F8F8F8F88888888888788878888888
      8888888888888888888888888887878878888888888888888878888888888888
      888888888FFFFF8F8F8F88F8F8F8F8F8F8F8F8F8888888888888887878888888
      8888888888888888888888888888887878888888888888888788888888888888
      88888888888FF8FF8F88888888F8F8FF8F8F88F88F8888888888888878888888
      8888888888888888888888888888788788888888888888888788888888888888
      8888888888888FFFFF8F8888F8F8F8F8F8F88F88888888888888888787888888
      8888888888888888888888888888887888888888888888888888888888888888
      888888888888888FF8F8F8F888F8F8F8F8F8F8F8F88888888888888878888888
      8888888888888888888888888888888888888888888888888888888888888888
      8888888888888888FFFF8F8F8F8F8F8F8F8F888888888F888888888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      88888888888888888FF8F8F8F8F8F8FFFFF8F8F8F8F888888888888887887878
      8888888888888888888888888888888888888888888888888888888888888888
      888888888888888788FFFFF8F8888F88F8F8F88F88888F888888888888878788
      8888888888888888888888888888878888888888888888888888888888888888
      8888888888888888888FFFFFF8F8F88F8F8FF8F8F88F88888888888888888878
      8888888888888888888888888888888888788888888888888888888888888888
      88888888888888888788FFFF8F8F88F8F8FF8FF8F8F8F88F8888888888878787
      8888888888888888888888888888887888788888888888888888888888888888
      888888888888888888888FFFFF8F8888F8F8FF8FF8F88F888888888888888888
      8888887888888888888888888888888788888888888888888888888888888888
      8888888888888888888888FFFF8F88F8F8F8F8F8F8F8F88F8F88888888888787
      8887888788788888888888888888888888888888888888888888888888888888
      8888888888888888888888FFFFFF8F88F8F8F8FF8FF8F8F8888F888888888888
      7878878888888888888888888888888878888888888888888888888888888888
      88888888888888888888888FFFF8F8F888F8F8F8F8F8F888F8888F8888888887
      8887888888888888888888888888888887888888888888888888888888888888
      88888888888888888888888F8FFF8F88F8F88F8FFF8F8F8F888F888888888878
      7878878888888F88888888888888888888888788888888888888888888888888
      88888888888888888888888FFFF8F8888888F8F8F8F8F8F88F88F88888888888
      87887888888F8888F8F888888888888888788888888878888888888888888888
      888888888888888888888788FFFFF8F888F88F8F8FF8F8F8F88F88F888888888
      787878788888F888888F8F888888888888878878888787888888888888888888
      888888888888888888888888FFFF8F888888F8F8FF8F8F8F8F8F8F8888888888
      888787887888888888888F8F8888888888887888788888888888888888888888
      8888888888888888888888788FFFF8F8F88F8F8FF8F8FF8F8F8F8F8F88888888
      8878788788888888788888888888888888888787887888878888888888888888
      8888888888888888888888888FFFF8F88888888F8F8FF8F8F8F8F888F8888888
      8887887878888887888888888888788888888878888788888888888888888888
      888888888888888888888887888F8F8F88888F8F8F8F8F8F8F8F8F8F88888888
      8888887878878878888888888888888888888788888888888888888888888888
      8888888888888888888888888888F8F8F8F88F8F8F8F8F8F8F8F8888888F8888
      8888878878787878888888888878788888887888888888888888888888888888
      888888888888888888888888888888F8F88888888F8F8F8F8F8F8F8F8F888888
      8888888888878888888888888888878888888788888888888888888888888888
      8888888888888888888888888888888F8F88888F8F8F8F8F8F8F8F888888F88F
      8888887888878888888888888888888888888778888888888888888888888888
      88888888888888888788888888888888F8F8888888F88F8F8FF8F8F8F8F88888
      8888888888878888888888888888888888888888888888888888888888888888
      88888888888888888888888888888888888888888F88F8F8F8F8F8F88888F888
      8888888887888888888888888888888888888787888888888888888888888888
      8888888888888888888888788888887888888888888F8F8F8F8F8F8F8F8F888F
      8888888888878888888888888888888888888878788888888888888888888888
      888888888888888887888888888888888888888888F8F8F8F8F8F8F8F8F88F88
      8888888888788888888888888888888888888878888888888888888888888888
      888888888888887888888888888888878888888888888F88F8F8F8F8F8F8F888
      888888888888888888888888888888888888888888888888888F888888888888
      88888888888887878787888888888888787888888888F88F8F8F8F8F8F8F8F88
      8888888888888888887888888888888888888888888888888788F88888888888
      8888888888888878787888788888888878887888888888F8F8F8F8FF8F8F88F8
      88888888888887878888888888888888888888888888888888888F8888888888
      888888888888888888878788888888888787878788888F88F8F8F8F8F8F8F88F
      88888888888887878788888788888888888888888888888888888FF8F8888788
      88888888888888887878888888888888887888888888888F8F8F8F8F8F8F88F8
      F888888888888888888888887888888888888888888888888888888888888888
      8888888888888888878888888888888888888888878888F88F8F8F8F8F8F8F8F
      88F8888888888887878888888888888888888888888888888888788888888888
      888888888888888888788888888888888888888888888888F8F88F8F8F8F8F8F
      F888F88888888878888888F88888888888888888888888888888888887878887
      888888888888888888788888888888888888888887888888888F88F8F8F8F8F8
      88F8888888888888787888F8F888888888888878888888888888888888888888
      8888888888888888888888888888888888888888788788888F88F8F8F8F8F8F8
      F8F8F8F8888888888888888F8888888888888888888888888888888888888888
      888888888888888888888888888888888888888887888888888F88F8F8F8F8F8
      F8F88888F88888888878888F8888888888888887878888888888888888888888
      88888888888888888888888888888888888888888878888888888F8F8F8F8F8F
      8888F8F888888888888888888F88888888888888887888888888888888888888
      8888888888888888888888888888888888888888888888888888F8F8F8F8F8F8
      F8F8F88888888888888888888888888888888888788888888888888888888888
      888888888888888888888888888888888888888888878887888888F8F88F8F8F
      8F8F88F888888888888888888888888888888888887878888888888788888888
      88888888888888888888888888888888888888888888788888888F8F88F8F8F8
      F8F88F888F888888888888888888878888888888888888888888887878888888
      888888888888888888888888888888888788888888878787888888888F8F8FF8
      F8F8F88F88888888888888888887888888888888887878888888888888888888
      8888888888888888888888888888888888788888888877878888888F8F8F8F8F
      8F88F8F8F8F88888888888888878787878888888888888888888888887888888
      888F888888888888888888888888888888888888888778778888888888F88F8F
      8F8F88F88888F888888888887878888888888888888888888888888888888888
      88F8F8888888888888888888888888888887888888887778788888888F88F8F8
      F888F8F8F8F88888888888888878788888888888888888888888888888888888
      8888888888888888888888888888888888888888888887888888888888F88F8F
      8F8F8F8F888F8888888888887878888888888888888888888888888888888888
      888888888888888888888888888888888888888888888888888788888888F88F
      8F88F8F8F8F88F88888888888878788888888888888888888888888888888888
      88888888888888888888888888888888888888888888888888788888888F8F8F
      8F8F8F8F888F888888888888888888888F8F8888887888888888888888888888
      88888888878888888888888888888888888888888888888888878888888888F8
      F8F88F8F8F8F88F888888888888888888F8F8F88888888888888888888888888
      888888788888888888888888788888888888888888887888888878888888F8F8
      F88F8F8F8F88F888888888888888888888FFF88F888887888888888888888888
      8888888888788888888888888888888888888888888878888888878888888888
      88F8F8F8F88F88F888F8888888888888888888F8888888888888888888888888
      888888888888888888888888888888888888888888888888888888787888888F
      8F88F8F8F8F88F88F88888888888888888888888F8F888888888888888888888
      8888888888888888888888888878888888888888888888888888878888888888
      888F8F8F8F88F88F88F88888888F88888878888888F888888888888888888888
      8888888888888888888888888887888888888888888888888888888887888888
      88F8F8F88F8F88F88F88F88888888F8888887888888888888888888888888888
      8888888888888888888888888888788888888888888888888888888888888888
      8F88F8F8F888F888F88F88888888888888788788888888788888888888888888
      8888888888888888888888888888888887888888888888888888888888788888
      888F8F8F8F8F88F8F8F888888888888888887878788888888888888888888888
      8888888888888888888888888888878888888888888887888888888888878888
      88888888F888F888888888888888888888878787878888788888888888888888
      8888888888888888888888888888888888888888888888888888888888888888
      88888F8F8F8F8888F88F88F88888888888888788787878888888888888888888
      8888888888888888888888888888888888888888888888888888888888878888
      8888}
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 258
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      DropDownMenu = ppPrint
      Hint = #1055#1077#1095#1072#1090#1100'|'
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1057#1086#1088#1090'.'
      Caption = #1057#1086#1088#1090'.'
      Hint = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      ImageIndex = 3
      Spacing = 1
      Left = 194
      Top = 2
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 1024
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 100
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object laCat: TLabel
      Left = 120
      Top = 8
      Width = 23
      Height = 13
      Caption = #1042#1089#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
      Caption = #1050#1072#1090#1077#1075#1086#1088#1080#1103
      DropDownMenu = dm.pmWH
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TM207IBGrid
    Left = 0
    Top = 69
    Width = 441
    Height = 680
    Align = alLeft
    Color = clBtnFace
    DataSource = dmCom.dsComp
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = dg1KeyDown
    FixedCols = 1
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    PrintDataSet = 0
    ClearHighlight = False
    SortOnTitleClick = False
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RecNo'
        Title.Alignment = taCenter
        Title.Caption = #8470
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 32
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'NAME'
        Title.Alignment = taCenter
        Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 234
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SNAME'
        Title.Caption = #1050#1088'.'#1085#1072#1080#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'CODE'
        Title.Caption = #1050#1086#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Prod'
        Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 445
    Top = 69
    Width = 579
    Height = 680
    Align = alClient
    TabOrder = 3
    object Label4: TLabel
      Left = 4
      Top = 34
      Width = 43
      Height = 13
      AutoSize = False
      Caption = #1043#1086#1088#1086#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 4
      Top = 80
      Width = 51
      Height = 13
      AutoSize = False
      Caption = #1058#1077#1083#1077#1092#1086#1085
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 4
      Top = 56
      Width = 43
      Height = 13
      AutoSize = False
      Caption = #1040#1076#1088#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 204
      Top = 34
      Width = 43
      Height = 13
      AutoSize = False
      Caption = #1048#1085#1076#1077#1082#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 4
      Top = 104
      Width = 51
      Height = 13
      AutoSize = False
      Caption = #1060#1072#1082#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 4
      Top = 127
      Width = 51
      Height = 13
      AutoSize = False
      Caption = #1057#1090#1088#1072#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 208
      Top = 80
      Width = 40
      Height = 13
      AutoSize = False
      Caption = 'E-mail'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 208
      Top = 101
      Width = 49
      Height = 13
      AutoSize = False
      Caption = #1048#1085#1090#1077#1088#1085#1077#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object pc1: TPageControl
      Left = 1
      Top = 152
      Width = 577
      Height = 527
      ActivePage = TabSheet2
      Align = alBottom
      TabIndex = 1
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = #1054#1073#1097#1080#1077
        object Label9: TLabel
          Left = 180
          Top = 10
          Width = 64
          Height = 13
          AutoSize = False
          Caption = #1042#1080#1076' '#1086#1087#1083#1072#1090#1099
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 182
          Top = 34
          Width = 59
          Height = 13
          AutoSize = False
          Caption = #1053#1044#1057
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lcNDS: TDBLookupComboBox
          Left = 244
          Top = 29
          Width = 156
          Height = 21
          Color = clInfoBk
          DataField = 'NDSURF'
          DataSource = dmCom.dsDep1
          KeyField = 'NDSID'
          ListField = 'NAME'
          ListSource = dmCom.dsNDS
          TabOrder = 0
        end
        object lcPayType: TDBLookupComboBox
          Left = 244
          Top = 5
          Width = 156
          Height = 21
          Color = clInfoBk
          DataField = 'NDSURF'
          DataSource = dmCom.dsDep1
          KeyField = 'NDSID'
          ListField = 'NAME'
          ListSource = dmCom.dsNDS
          TabOrder = 1
        end
        object DBCheckBox1: TDBCheckBox
          Left = 10
          Top = 10
          Width = 119
          Height = 17
          Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
          DataField = 'SELLER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 10
          Top = 28
          Width = 127
          Height = 17
          Caption = #1041#1099#1074#1096#1080#1081' '#1087#1086#1089#1090#1072#1074#1097#1080#1082
          DataField = 'SELLER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          ValueChecked = '2'
          ValueUnchecked = '0'
        end
        object DBCheckBox3: TDBCheckBox
          Left = 10
          Top = 46
          Width = 119
          Height = 17
          Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
          DataField = 'PRODUCER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 10
          Top = 64
          Width = 135
          Height = 17
          Caption = #1054#1087#1090#1086#1074#1099#1081' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1100
          DataField = 'BUYER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox5: TDBCheckBox
          Left = 10
          Top = 82
          Width = 143
          Height = 17
          Caption = #1056#1086#1079#1085#1080#1095#1085#1099#1081' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1100
          DataField = 'SBUYER'
          DataSource = dmCom.dsComp
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099
        ImageIndex = 1
        object PageControl1: TPageControl
          Left = 0
          Top = 0
          Width = 569
          Height = 499
          ActivePage = TabSheet7
          Align = alClient
          TabIndex = 0
          TabOrder = 0
          object TabSheet7: TTabSheet
            Caption = #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
            object Label11: TLabel
              Left = 3
              Top = 10
              Width = 40
              Height = 13
              AutoSize = False
              Caption = #1048#1053#1053
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label12: TLabel
              Left = 3
              Top = 34
              Width = 40
              Height = 13
              AutoSize = False
              Caption = #1054#1050#1055#1054
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label13: TLabel
              Left = 247
              Top = 10
              Width = 23
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1041#1048#1050
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label14: TLabel
              Left = 228
              Top = 34
              Width = 42
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = #1054#1050#1054#1053#1061
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label15: TLabel
              Left = 3
              Top = 57
              Width = 40
              Height = 13
              AutoSize = False
              Caption = #1041#1072#1085#1082
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label16: TLabel
              Left = 3
              Top = 81
              Width = 40
              Height = 13
              AutoSize = False
              Caption = #1057#1095#1077#1090
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label17: TLabel
              Left = 3
              Top = 105
              Width = 49
              Height = 13
              AutoSize = False
              Caption = #1050#1086#1088'.'#1073#1072#1085#1082
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label18: TLabel
              Left = 3
              Top = 129
              Width = 45
              Height = 13
              AutoSize = False
              Caption = #1050#1086#1088'.'#1089#1095#1077#1090
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label19: TLabel
              Left = 1
              Top = 153
              Width = 54
              Height = 13
              AutoSize = False
              Caption = #1040#1076#1088'.'#1089#1095#1077#1090#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label20: TLabel
              Left = 131
              Top = 174
              Width = 190
              Height = 13
              AutoSize = False
              Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label21: TLabel
              Left = 131
              Top = 213
              Width = 190
              Height = 13
              AutoSize = False
              Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1075#1083#1072#1074#1085#1086#1075#1086' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object Label22: TLabel
              Left = 131
              Top = 254
              Width = 190
              Height = 13
              AutoSize = False
              Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1075#1086' '#1083#1080#1094#1072
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
            end
            object DBEdit6: TDBEdit
              Left = 52
              Top = 6
              Width = 193
              Height = 21
              Color = clInfoBk
              DataField = 'INN'
              DataSource = dmCom.dsComp
              TabOrder = 0
            end
            object DBEdit7: TDBEdit
              Left = 52
              Top = 30
              Width = 169
              Height = 21
              Color = clInfoBk
              DataField = 'OKPO'
              DataSource = dmCom.dsComp
              TabOrder = 1
            end
            object DBEdit8: TDBEdit
              Left = 272
              Top = 6
              Width = 141
              Height = 21
              Color = clInfoBk
              DataField = 'BIK'
              DataSource = dmCom.dsComp
              TabOrder = 2
            end
            object DBEdit9: TDBEdit
              Left = 272
              Top = 30
              Width = 141
              Height = 21
              Color = clInfoBk
              DataField = 'OKONH'
              DataSource = dmCom.dsComp
              TabOrder = 3
            end
            object DBEdit10: TDBEdit
              Left = 52
              Top = 53
              Width = 362
              Height = 21
              Color = clInfoBk
              DataField = 'BANK'
              DataSource = dmCom.dsComp
              TabOrder = 4
            end
            object DBEdit11: TDBEdit
              Left = 52
              Top = 77
              Width = 362
              Height = 21
              Color = clInfoBk
              DataField = 'BILL'
              DataSource = dmCom.dsComp
              TabOrder = 5
            end
            object DBEdit12: TDBEdit
              Left = 52
              Top = 101
              Width = 362
              Height = 21
              Color = clInfoBk
              DataField = 'KBANK'
              DataSource = dmCom.dsComp
              TabOrder = 6
            end
            object DBEdit13: TDBEdit
              Left = 52
              Top = 125
              Width = 362
              Height = 21
              Color = clInfoBk
              DataField = 'KBILL'
              DataSource = dmCom.dsComp
              TabOrder = 7
            end
            object DBEdit14: TDBEdit
              Left = 52
              Top = 149
              Width = 362
              Height = 21
              Color = clInfoBk
              DataField = 'ADRBILL'
              DataSource = dmCom.dsComp
              TabOrder = 8
            end
            object DBEdit15: TDBEdit
              Left = 1
              Top = 189
              Width = 334
              Height = 21
              Color = clInfoBk
              DataField = 'BOSS'
              DataSource = dmCom.dsComp
              TabOrder = 9
            end
            object DBEdit16: TDBEdit
              Left = 337
              Top = 189
              Width = 85
              Height = 21
              Color = clInfoBk
              DataField = 'BOSSPHONE'
              DataSource = dmCom.dsComp
              TabOrder = 10
            end
            object DBEdit17: TDBEdit
              Left = 1
              Top = 228
              Width = 334
              Height = 21
              Color = clInfoBk
              DataField = 'BUH'
              DataSource = dmCom.dsComp
              TabOrder = 11
            end
            object DBEdit18: TDBEdit
              Left = 337
              Top = 228
              Width = 85
              Height = 21
              Color = clInfoBk
              DataField = 'BUHPHONE'
              DataSource = dmCom.dsComp
              TabOrder = 12
            end
            object DBEdit19: TDBEdit
              Left = 1
              Top = 269
              Width = 334
              Height = 21
              Color = clInfoBk
              DataField = 'OFIO'
              DataSource = dmCom.dsComp
              TabOrder = 13
            end
            object DBEdit20: TDBEdit
              Left = 337
              Top = 269
              Width = 85
              Height = 21
              Color = clInfoBk
              DataField = 'OFIOPHONE'
              DataSource = dmCom.dsComp
              TabOrder = 14
            end
          end
          object TabSheet8: TTabSheet
            Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
            ImageIndex = 1
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = #1055#1088#1072#1074#1072' '#1076#1086#1089#1090#1091#1087#1072
        ImageIndex = 2
      end
      object TabSheet4: TTabSheet
        Caption = #1051#1086#1075#1086#1090#1080#1087
        ImageIndex = 3
      end
      object TabSheet5: TTabSheet
        Caption = #1052#1054#1051
        ImageIndex = 4
      end
      object TabSheet6: TTabSheet
        Caption = #1044#1086#1075#1086#1074#1086#1088
        ImageIndex = 5
      end
    end
    object cbS: TDBCheckBox
      Left = 10
      Top = 10
      Width = 119
      Height = 17
      Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
      DataField = 'PHIS'
      DataSource = dmCom.dsComp
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object edPostIndex: TDBEdit
      Left = 48
      Top = 30
      Width = 150
      Height = 21
      Color = clInfoBk
      DataField = 'CITY'
      DataSource = dmCom.dsComp
      TabOrder = 2
    end
    object edPhone: TDBEdit
      Left = 56
      Top = 74
      Width = 142
      Height = 21
      Color = clInfoBk
      DataField = 'PHONE'
      DataSource = dmCom.dsComp
      TabOrder = 3
    end
    object edAdress: TDBEdit
      Left = 48
      Top = 52
      Width = 350
      Height = 21
      Color = clInfoBk
      DataField = 'ADDRESS'
      DataSource = dmCom.dsComp
      TabOrder = 4
    end
    object DBEdit1: TDBEdit
      Left = 248
      Top = 30
      Width = 99
      Height = 21
      Color = clInfoBk
      DataField = 'POSTINDEX'
      DataSource = dmCom.dsComp
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Left = 56
      Top = 98
      Width = 142
      Height = 21
      Color = clInfoBk
      DataField = 'FAX'
      DataSource = dmCom.dsComp
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Left = 56
      Top = 121
      Width = 142
      Height = 21
      Color = clInfoBk
      DataField = 'COUNTRY'
      DataSource = dmCom.dsComp
      TabOrder = 7
    end
    object DBEdit4: TDBEdit
      Left = 256
      Top = 74
      Width = 142
      Height = 21
      Color = clInfoBk
      DataField = 'EMAIL'
      DataSource = dmCom.dsComp
      TabOrder = 8
    end
    object DBEdit5: TDBEdit
      Left = 256
      Top = 98
      Width = 142
      Height = 21
      Color = clInfoBk
      DataField = 'WWW'
      DataSource = dmCom.dsComp
      TabOrder = 9
    end
  end
  object ppPrint: TRxPopupMenu
    Left = 200
    Top = 88
    object Dct1: TMenuItem
      Caption = #1042#1089#1077
      OnClick = Dct1Click
    end
    object N1: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1082#1088#1072#1090#1082#1086
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1077' '#1087#1086#1083#1085#1086
      OnClick = N2Click
    end
  end
  object ppCat: TRxPopupMenu
    Left = 48
    Top = 80
    object MenuItem1: TMenuItem
      Tag = 1
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082#1080
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Tag = 2
      Caption = #1054#1087#1090#1086#1074#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080
    end
    object N3: TMenuItem
      Tag = 3
      Caption = #1048#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1080
    end
    object MenuItem3: TMenuItem
      Tag = 4
      Caption = #1056#1086#1079#1085#1080#1095#1085#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080
    end
    object N4: TMenuItem
      Tag = 5
      Caption = #1042#1089#1077
    end
  end
end
