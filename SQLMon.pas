unit SQLMon;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TfmSQLMon = class(TForm)
    me: TMemo;
    cbEn: TCheckBox;
    cbTop: TCheckBox;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure cbTopClick(Sender: TObject);
    procedure monSQL(EventText: String; EventTime: TDateTime);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSQLMon: TfmSQLMon;

implementation

{$R *.dfm}

procedure TfmSQLMon.Button1Click(Sender: TObject);
begin
  me.Lines.Clear;
end;

procedure TfmSQLMon.cbTopClick(Sender: TObject);
begin
  if cbTop.Checked then FormStyle:=fsStayOnTop
  else FormStyle:=fsNormal;
end;

procedure TfmSQLMon.monSQL(EventText: String; EventTime: TDateTime);
begin
  me.Lines.Add(TimeToStr(EventTime));
  me.Lines.Add(EventText);
  me.Lines.Add('');
end;

end.
