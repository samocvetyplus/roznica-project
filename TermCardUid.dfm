object fmTermCardUid: TfmTermCardUid
  Left = 312
  Top = 106
  Width = 652
  Height = 480
  Caption = #1048#1079#1076#1077#1083#1080#1103' '#1087#1086' '#1095#1077#1082#1091
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object plTermCard: TPanel
    Left = 0
    Top = 0
    Width = 644
    Height = 451
    Align = alClient
    TabOrder = 0
    object dg1: TDBGridEh
      Left = 1
      Top = 1
      Width = 642
      Height = 449
      Align = alClient
      AllowedOperations = []
      DataGrouping.GroupLevels = <>
      DataSource = dsTermUid
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          EditButtons = <>
          FieldName = 'UID'
          Footers = <>
          Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        end
        item
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.Caption = #1056#1072#1079#1084#1077#1088
          Width = 47
        end
        item
          EditButtons = <>
          FieldName = 'W'
          Footers = <>
          Title.Caption = #1042#1077#1089
          Width = 49
        end
        item
          EditButtons = <>
          FieldName = 'FULLART'
          Footers = <>
          Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
          Width = 161
        end
        item
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.Caption = #1040#1088#1090#1080#1082#1091#1083'2'
          Width = 84
        end
        item
          EditButtons = <>
          FieldName = 'COST'
          Footers = <>
          Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' ('#1088#1077#1072#1083'.)'
        end
        item
          EditButtons = <>
          FieldName = 'COST0'
          Footers = <>
          Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' ('#1088#1072#1089#1093'.)'
        end
        item
          EditButtons = <>
          FieldName = 'ADATE'
          Footers = <>
          Title.Caption = #1044#1072#1090#1072' '#1087#1088#1086#1076#1072#1078#1080
          Width = 82
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object quTermUid: TpFIBDataSet
    SelectSQL.Strings = (
      'select sl.uid, sl.ADATE , sl.SZ, sl.W ,'
      
        '       a2.FULLART, a2.ART2, smartround(sl.Q0*sl.PRICE, 1, 100) a' +
        's Cost,'
      '       smartround(sl.Q0*sl.PRICE0, 1, 100) as Cost0'
      'from SellItem sl, Art2 a2'
      'where sl.checkno = :checkno and'
      '      sl.ART2ID = a2.ART2ID'
      'order by sl.adate')
    BeforeOpen = quTermUidBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 216
    Top = 96
    object quTermUidUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quTermUidADATE: TFIBDateTimeField
      FieldName = 'ADATE'
    end
    object quTermUidSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quTermUidW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object quTermUidFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object quTermUidART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quTermUidCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object quTermUidCOST0: TFIBFloatField
      FieldName = 'COST0'
      currency = True
    end
  end
  object dsTermUid: TDataSource
    DataSet = quTermUid
    Left = 216
    Top = 152
  end
end
