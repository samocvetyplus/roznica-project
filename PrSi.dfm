object fmPrSi: TfmPrSi
  Left = 119
  Top = 169
  Width = 776
  Height = 404
  Caption = #1055#1077#1088#1077#1086#1094#1077#1085#1105#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 768
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 699
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 42
    Width = 768
    Height = 333
    Align = alClient
    AllowedOperations = []
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm.dsPrSI
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'UID'
        Footers = <
          item
            FieldName = 'UID'
            ValueType = fvtCount
          end>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'W'
        Footers = <
          item
            FieldName = 'W'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1077#1089
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SUP'
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SN'
        Footers = <>
        Title.Caption = #8470' '#1087#1088#1080#1093#1086#1076#1072
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'SPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1087#1088#1080#1093#1086#1076#1085#1072#1103
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'NEWPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1085#1086#1074#1072#1103
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'OLDPRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1079#1072' '#1077#1076'. '#1090#1086#1074#1072#1088#1072'||'#1089#1090#1072#1088#1072#1103
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'NEWCOST'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1085#1086#1074#1072#1103
      end
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'OLDCOST'
        Footers = <>
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100'||'#1089#1090#1072#1088#1072#1103
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 172
    Top = 152
  end
end
