object fmSInv_H: TfmSInv_H
  Left = 236
  Top = 297
  Cursor = crArrow
  ActiveControl = LogEd
  BorderStyle = bsToolWindow
  ClientHeight = 262
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 134
    Height = 19
    Caption = #1048#1089#1090#1086#1088#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaLogin: TLabel
    Left = 22
    Top = 202
    Width = 78
    Height = 15
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaPsw: TLabel
    Left = 56
    Top = 227
    Width = 44
    Height = 15
    Caption = #1055#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object OpSBtn: TSpeedButton
    Left = 264
    Top = 204
    Width = 73
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    OnClick = OpSBtnClick
  end
  object ClSBtn: TSpeedButton
    Left = 352
    Top = 203
    Width = 81
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    OnClick = ClSBtnClick
  end
  object CnlSBtn: TSpeedButton
    Left = 444
    Top = 202
    Width = 65
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    OnClick = CnlSBtnClick
  end
  object Label9: TLabel
    Left = 1
    Top = 176
    Width = 294
    Height = 17
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1074#1086#1077' '#1080#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1087#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dg1: TDBGridEh
    Left = -8
    Top = 32
    Width = 521
    Height = 137
    Color = clBtnHighlight
    DataGrouping.GroupLevels = <>
    DataSource = dsrDlist_H
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 291
      end
      item
        EditButtons = <>
        FieldName = 'HDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
      end
      item
        EditButtons = <>
        FieldName = 'STATUS'
        Footers = <>
        Title.Caption = #1044#1077#1081#1089#1090#1074#1080#1077
        Width = 95
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object PswEd: TEdit
    Left = 108
    Top = 225
    Width = 145
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = PswEdKeyDown
  end
  object LogEd: TEdit
    Left = 108
    Top = 200
    Width = 145
    Height = 21
    TabOrder = 2
    OnKeyDown = LogEdKeyDown
  end
  object fs1: TFormStorage
    Active = False
    Options = [fpState]
    UseRegistry = False
    StoredValues = <>
    Left = 468
    Top = 4
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :INVID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   1'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 1'
      'order by HDATE')
    BeforeOpen = taHistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 352
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 312
  end
  object taEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ALIAS, FIO, PSWD'
      'FROM D_EMP')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 424
    object taEmpALIAS: TFIBStringField
      FieldName = 'ALIAS'
      EmptyStrToNull = True
    end
    object taEmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taEmpPSWD: TFIBStringField
      FieldName = 'PSWD'
      EmptyStrToNull = True
    end
  end
  object DataSource1: TDataSource
    DataSet = taEmp
    Left = 392
  end
end
