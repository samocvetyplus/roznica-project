unit UIDStoreCheck;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ComCtrls, ExtCtrls, ActnList,
  PrnDbgeh, jpeg, DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  TfmUIDStoreCheck = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    StatusBar1: TStatusBar;
    gridUIDStoreCheck: TDBGridEh;
    acEvent: TActionList;
    acExit: TAction;
    spitUIDHistory: TSpeedItem;
    acUIDHistory: TAction;
    spitUpdate: TSpeedItem;
    acUpdate: TAction;
    siRepeat: TSpeedItem;
    pdg1: TPrintDBGridEh;
    acPrint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acUIDHistoryExecute(Sender: TObject);
    procedure acUIDHistoryUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure acUpdateExecute(Sender: TObject);
    procedure siRepeatClick(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  private
    LogOperIdForm: string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmUIDStoreCheck: TfmUIDStoreCheck;

implementation

uses Data3, Data, comdata, UIDHist, m207Proc, UIdRepeat, JewConst;

{$R *.dfm}

procedure TfmUIDStoreCheck.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmUIDStoreCheck.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  OpenDataSets([dm3.taCheckUIDStoreData]);
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUIDStoreCheck.acUIDHistoryExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDHist, LogOperIdForm);
  dm.UID2Find := dm3.taCheckUIDStoreDataUID.Value;
  ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreCheck.acUIDHistoryUpdate(Sender: TObject);
begin
  acUIDHistory.Enabled := dm3.taCheckUIDStoreData.Active and (not dm3.taCheckUIDStoreData.IsEmpty);
end;

procedure TfmUIDStoreCheck.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dm3.taCheckUIDStoreData]);
  Action := caFree;
end;

procedure TfmUIDStoreCheck.acExitExecute(Sender: TObject);
begin
  Self.Close;
end;

procedure TfmUIDStoreCheck.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmUIDStoreCheck.acUpdateExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreCheckData, LogOperIdForm);
  Screen.Cursor := crSQLWait;
  try
    dm3.quTmp.Transaction.CommitRetaining;
    dm3.quTmp.Close;
    dm3.quTmp.SQL.Text := 'execute procedure UIDSTORE_CHECK('+dm3.taUIDStoreListSINVID.AsString+')';
    dm3.quTmp.ExecQuery;
    dm3.quTmp.Transaction.CommitRetaining;
    dm3.quTmp.Close;
    ReOpenDataSets([dm3.taCheckUIDStoreData]);
    //dm3.taUIDStoreList.Refresh;
  finally
    Screen.Cursor := crDefault;
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreCheck.siRepeatClick(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreRepeat, LogOperIdForm);
  ShowAndFreeForm(TfmUIdRepeat, Self, TForm(fmUIdRepeat), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmUIDStoreCheck.acPrintExecute(Sender: TObject);
begin
 pdg1.Preview;
end;

end.
