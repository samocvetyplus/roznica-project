object fmAnlzSelEmp: TfmAnlzSelEmp
  Left = 266
  Top = 148
  Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1086#1076#1072#1078' '#1087#1086' '#1087#1088#1086#1076#1072#1074#1094#1072#1084
  ClientHeight = 500
  ClientWidth = 753
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 753
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 120
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object laDep: TLabel
      Left = 128
      Top = 6
      Width = 108
      Height = 13
      Caption = #1042#1099#1073#1088#1072#1085#1085#1099#1081' '#1089#1082#1083#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object laPeriod: TLabel
      Left = 424
      Top = 8
      Width = 92
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086' '#1084#1077#1089#1103#1094#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dm.pmAnlzSelDed
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 299
      Top = 3
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 70
    Width = 753
    Height = 430
    Align = alClient
    AllowedOperations = []
    AllowedSelections = [gstRecordBookmarks, gstRectangle, gstAll]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    Ctl3D = True
    DataGrouping.GroupLevels = <>
    DataSource = dsAnlzSelEmp
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clInfoBk
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentFont = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'SELLERNAME'
        Footers = <>
        Title.Caption = #1060#1048#1054
        Width = 217
      end
      item
        EditButtons = <>
        FieldName = 'SEL_Q'
        Footer.FieldName = 'SEL_Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1074#1077#1094'|'#1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        Width = 112
      end
      item
        EditButtons = <>
        FieldName = 'SEL_COST'
        Footer.FieldName = 'SEL_COST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1074#1077#1094'|'#1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        Width = 148
      end
      item
        EditButtons = <>
        FieldName = 'CASH_Q'
        Footers = <>
        Title.Caption = #1050#1072#1089#1089#1080#1088'|'#1050#1086#1083'-'#1074#1086' '#1087#1088#1086#1076#1072#1078
        Width = 129
      end
      item
        EditButtons = <>
        FieldName = 'CASH_COST'
        Footers = <>
        Title.Caption = #1050#1072#1089#1089#1080#1088'|'#1057#1091#1084#1084#1072' '#1087#1088#1086#1076#1072#1078
        Width = 116
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 753
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 683
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1054#1090#1084#1077#1095#1077#1085#1085#1099#1077
      ImageIndex = 67
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siPrintClick
      SectionName = 'Untitled (0)'
    end
  end
  object quAnlzSelEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from seller_s(:i_bd,:i_ed,:i_depid, :i_userid)')
    BeforeOpen = quAnlzSelEmpBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 104
    Top = 192
    object quAnlzSelEmpSEL_Q: TFIBIntegerField
      FieldName = 'SEL_Q'
    end
    object quAnlzSelEmpSEL_COST: TFIBFloatField
      FieldName = 'SEL_COST'
    end
    object quAnlzSelEmpSELLERNAME: TFIBStringField
      FieldName = 'SELLERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quAnlzSelEmpCASH_Q: TFIBIntegerField
      FieldName = 'CASH_Q'
    end
    object quAnlzSelEmpCASH_COST: TFIBIntegerField
      FieldName = 'CASH_COST'
    end
    object quAnlzSelEmpUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
  end
  object dsAnlzSelEmp: TDataSource
    DataSet = quAnlzSelEmp
    Left = 104
    Top = 160
  end
end
