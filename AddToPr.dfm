object fmAddToPr: TfmAddToPr
  Left = 382
  Top = 206
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1075#1088#1091#1087#1087#1091' '#1074' '#1087#1088#1080#1082#1072#1079
  ClientHeight = 132
  ClientWidth = 346
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 210
    Top = 4
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 210
    Top = 32
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object seP1: TRxSpinEdit
    Left = 120
    Top = 104
    Width = 75
    Height = 21
    ValueType = vtFloat
    Color = clInfoBk
    TabOrder = 2
    Visible = False
  end
  object seP2: TRxSpinEdit
    Left = 120
    Top = 4
    Width = 75
    Height = 21
    MaxValue = 1000000000
    ValueType = vtFloat
    Color = clInfoBk
    TabOrder = 3
  end
  object seP3: TRxSpinEdit
    Left = 120
    Top = 28
    Width = 75
    Height = 21
    MaxValue = 1000000000
    ValueType = vtFloat
    Color = clInfoBk
    TabOrder = 4
  end
  object cb1: TCheckBox
    Left = 4
    Top = 104
    Width = 109
    Height = 17
    Caption = #1055#1088#1080#1093#1086#1076#1085#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    Visible = False
  end
  object cb2: TCheckBox
    Left = 8
    Top = 6
    Width = 109
    Height = 17
    Caption = #1056#1072#1089#1093#1086#1076#1085#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object cb3: TCheckBox
    Left = 8
    Top = 30
    Width = 109
    Height = 17
    Caption = #1054#1087#1090#1086#1074#1072#1103' '#1094#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
  end
  object bDep: TButton
    Left = 8
    Top = 56
    Width = 185
    Height = 25
    Caption = #1057#1082#1083#1072#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    PopupMenu = dm.pmAddTopr
    TabOrder = 8
    OnClick = bDepClick
  end
end
