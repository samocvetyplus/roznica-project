unit uScanCode;

interface

uses Windows, Messages, Forms, SysUtils, TypInfo;

type

  TApplicationHook = class(TObject)
  protected
    function Hook(var Message: TMessage): Boolean;
  public
    constructor Create;
    destructor Destroy; override;
  end;

var
  WM_SCANCODE: UINT;
  Hook: TApplicationHook;


implementation

uses Controls, ServData;

type

  TOnScanCode = procedure (ScanCode: string) of object;




constructor TApplicationHook.Create;
begin
  Application.HookMainWindow(Hook);
end;

destructor TApplicationHook.Destroy;
begin
  Application.UnhookMainWindow(Hook);
  inherited Destroy;
end;

function TApplicationHook.Hook(var Message: TMessage): Boolean;
var
  Data: PCopyDataStruct;
  Form: TForm;
  ScanCode: string;
  MethodAddress: Pointer;
  Method: TMethod;
  Handle: THandle;
  Control: TWinControl;
begin
  Result := False;
  if Message.Msg = WM_COPYDATA then
  begin
    Data := TWMCopyData(Message).CopyDataStruct;
    if Data^.dwData = WM_SCANCODE then
    begin
      ScanCode := StrPas(Data^.lpData);
      MethodAddress := dmServ.MethodAddress('OnScanCode');

      if MethodAddress <> nil then
      begin
        Method.Code := MethodAddress;
        Method.Data := dmServ;
        TOnScanCode(Method)(ScanCode);
        Result := True;
      end;
    end;
  end;
end;

initialization

  WM_SCANCODE := RegisterWindowMessage('WM_SCANCODE');

  Hook := TApplicationHook.Create;


finalization

  if Hook <> nil then Hook.Free;

end.


