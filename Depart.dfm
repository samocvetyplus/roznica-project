object fmDepart: TfmDepart
  Left = 250
  Top = 123
  Caption = #1057#1090#1088#1091#1082#1090#1091#1088#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 643
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter
    Left = 0
    Top = 353
    Width = 850
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 624
    Width = 850
    Height = 19
    Panels = <>
  end
  object pa1: TPanel
    Left = 0
    Top = 42
    Width = 850
    Height = 311
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 469
      Top = 0
      Height = 311
    end
    object tv1: TTreeView
      Left = 0
      Top = 0
      Width = 469
      Height = 311
      Align = alLeft
      DragMode = dmAutomatic
      HideSelection = False
      Images = dmCom.ilGoodTree
      Indent = 19
      PopupMenu = pmDepart
      RightClickSelect = True
      TabOrder = 0
      OnChange = tv1Change
      OnChanging = tv1Changing
      OnDeletion = tv1Deletion
      OnDragDrop = tv1DragDrop
      OnDragOver = tv1DragOver
      OnEdited = tv1Edited
      OnEditing = tv1Editing
      OnExit = tv1Exit
      OnExpanding = tv1Expanding
      OnGetImageIndex = tv1GetImageIndex
      OnGetSelectedIndex = tv1GetSelectedIndex
      OnStartDrag = tv1StartDrag
    end
    object pc1: TPageControl
      Left = 472
      Top = 0
      Width = 378
      Height = 311
      ActivePage = tsDep
      Align = alClient
      TabOrder = 1
      object tsDep: TTabSheet
        Caption = 'tsDep'
        object Label1: TLabel
          Left = 4
          Top = 52
          Width = 95
          Height = 13
          AutoSize = False
          Caption = #1053#1086#1088#1084#1072' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 202
          Top = 52
          Width = 53
          Height = 13
          AutoSize = False
          Caption = #1053#1072#1094#1077#1085#1082#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 4
          Top = 106
          Width = 43
          Height = 13
          AutoSize = False
          Caption = #1048#1085#1076#1077#1082#1089
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 154
          Top = 106
          Width = 35
          Height = 13
          AutoSize = False
          Caption = #1043#1086#1088#1086#1076
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 4
          Top = 128
          Width = 43
          Height = 13
          AutoSize = False
          Caption = #1040#1076#1088#1077#1089
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 4
          Top = 152
          Width = 51
          Height = 13
          AutoSize = False
          Caption = #1058#1077#1083#1077#1092#1086#1085
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 4
          Top = 4
          Width = 121
          Height = 13
          AutoSize = False
          Caption = #1050#1088#1072#1090#1082#1086#1077' '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 140
          Top = 192
          Width = 19
          Height = 13
          Caption = #1050#1086#1076
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 4
          Top = 264
          Width = 201
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1076#1083#1103' '#1089#1086#1079#1076#1072#1085#1080#1103' '#1079#1072#1103#1074#1082#1080' '#1085#1072' '#1092#1080#1083#1080#1072#1083
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object cbS: TDBCheckBox
          Left = 60
          Top = 24
          Width = 67
          Height = 17
          Caption = #1052#1072#1075#1072#1079#1080#1085
          DataField = 'SHOP'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object cbDL: TDBCheckBox
          Left = 4
          Top = 24
          Width = 53
          Height = 17
          Caption = #1057#1082#1083#1072#1076
          DataField = 'WH'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object RxDBComboBox1: TRxDBComboBox
          Left = 104
          Top = 48
          Width = 95
          Height = 21
          Color = clInfoBk
          DataField = 'ROUNDNORM'
          DataSource = dmCom.dsDep1
          ItemHeight = 13
          Items.Strings = (
            #1050#1086#1087#1077#1081#1082#1080
            #1044#1077#1089#1103#1090#1082#1080' '#1082#1086#1087#1077#1077#1082
            #1056#1091#1073#1083#1080
            #1044#1077#1089#1103#1090#1082#1080' '#1088#1091#1073#1083#1077#1081
            #1057#1086#1090#1085#1080' '#1088#1091#1073#1083#1077#1081
            #1058#1099#1089#1103#1095#1080' '#1088#1091#1073#1083#1077#1081)
          TabOrder = 2
          Values.Strings = (
            '2'
            '1'
            '0'
            '-1'
            '-2'
            '-3')
        end
        object edMargin: TDBEdit
          Left = 254
          Top = 48
          Width = 63
          Height = 21
          Color = clInfoBk
          DataField = 'MARGIN'
          DataSource = dmCom.dsDep1
          TabOrder = 3
        end
        object edPostIndex: TDBEdit
          Left = 48
          Top = 102
          Width = 99
          Height = 21
          Color = clInfoBk
          DataField = 'POSTIND'
          DataSource = dmCom.dsDep1
          TabOrder = 4
        end
        object edCity: TDBEdit
          Left = 190
          Top = 102
          Width = 91
          Height = 21
          Color = clInfoBk
          DataField = 'CITY'
          DataSource = dmCom.dsDep1
          TabOrder = 5
        end
        object edAdress: TDBEdit
          Left = 48
          Top = 124
          Width = 233
          Height = 21
          Color = clInfoBk
          DataField = 'ADDRESS'
          DataSource = dmCom.dsDep1
          TabOrder = 6
        end
        object edPhone: TDBEdit
          Left = 56
          Top = 146
          Width = 91
          Height = 21
          Color = clInfoBk
          DataField = 'PHONE'
          DataSource = dmCom.dsDep1
          TabOrder = 7
        end
        object DBEdit1: TDBEdit
          Left = 128
          Top = 0
          Width = 189
          Height = 21
          Color = clInfoBk
          DataField = 'SNAME'
          DataSource = dmCom.dsDep1
          TabOrder = 8
        end
        object M207DBColor1: TM207DBColor
          Left = 4
          Top = 188
          Width = 121
          Height = 21
          Alignment = taCenter
          DirectInput = False
          NumGlyphs = 1
          TabOrder = 9
          Text = #1062#1074#1077#1090
          DataField = 'COLOR'
          DataSource = dmCom.dsDep1
        end
        object DBCheckBox2: TDBCheckBox
          Left = 6
          Top = 74
          Width = 291
          Height = 17
          Caption = #1055#1088#1080#1082#1072#1079#1099' '#1087#1088#1080' '#1080#1079#1084#1077#1085#1077#1085#1080#1080' '#1087#1088#1080#1093#1086#1076#1085#1099#1093' '#1094#1077#1085' '#1074' '#1087#1086#1089#1090#1072#1074#1082#1072#1093
          DataField = 'SPR'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 10
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox3: TDBCheckBox
          Left = 4
          Top = 240
          Width = 161
          Height = 17
          Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
          DataField = 'ISPROG'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          ValueChecked = '1'
          ValueUnchecked = '0'
          OnKeyUp = DBCheckBox3KeyUp
          OnMouseUp = DBCheckBox3MouseUp
        end
        object DBEdit2: TDBEdit
          Left = 172
          Top = 188
          Width = 109
          Height = 21
          Color = clInfoBk
          DataField = 'SSNAME'
          DataSource = dmCom.dsDep1
          TabOrder = 12
        end
        object DBCheckBox4: TDBCheckBox
          Left = 4
          Top = 224
          Width = 153
          Height = 17
          Caption = #1053#1077' '#1091#1089#1090#1072#1085#1072#1074#1083#1080#1074#1072#1090#1100' '#1094#1077#1085#1099
          DataField = 'NOTSETPR'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox6: TDBCheckBox
          Left = 136
          Top = 24
          Width = 69
          Height = 17
          Caption = #1062#1077#1085#1090#1088
          DataField = 'CENTERDEP'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object edApplDepNum: TDBEdit
          Left = 216
          Top = 256
          Width = 89
          Height = 21
          DataField = 'APPLDEPNUM'
          DataSource = dmCom.dsDep1
          TabOrder = 15
        end
        object chbxDEL: TDBCheckBox
          Left = 208
          Top = 24
          Width = 113
          Height = 17
          Caption = #1056#1072#1089#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085
          DataField = 'ISDELETE'
          DataSource = dmCom.dsDep1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
          ValueChecked = '1'
          ValueUnchecked = '0'
          OnMouseUp = chbxDELMouseUp
        end
      end
      object tsHead: TTabSheet
        Caption = 'tsHead'
        ImageIndex = 1
        object Label2: TLabel
          Left = 4
          Top = 8
          Width = 67
          Height = 13
          Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lcRoot: TDBLookupComboBox
          Left = 4
          Top = 24
          Width = 285
          Height = 21
          Color = clInfoBk
          DataField = 'D_COMPID'
          DataSource = dmCom.dsRec
          KeyField = 'D_COMPID'
          ListField = 'NAME'
          ListSource = dmCom.dsAllComp
          TabOrder = 0
          OnCloseUp = lcRootCloseUp
        end
      end
    end
  end
  object pa2: TPanel
    Left = 353
    Top = 356
    Width = 497
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Splitter3: TSplitter
      Left = 0
      Top = 0
      Height = 268
      ExplicitHeight = 273
    end
    object Panel3: TPanel
      Left = 3
      Top = 0
      Width = 494
      Height = 268
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel1: TBevel
        Left = 6
        Top = 29
        Width = 385
        Height = 73
      end
      object tb2: TSpeedBar
        Left = 0
        Top = 0
        Width = 494
        Height = 31
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Position = bpCustom
        Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 1
        BtnOffsetVert = 1
        BtnWidth = 25
        BtnHeight = 25
        Images = dmCom.ilButtons
        BevelOuter = bvSpace
        BorderStyle = bsSingle
        TabOrder = 0
        InternalVer = 1
        object SpeedbarSection1: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          Action = acAddEmp
          BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
          ImageIndex = 1
          Spacing = 1
          Left = 1
          Top = 1
          Visible = True
          OnClick = acAddEmpExecute
          SectionName = 'Untitled (0)'
        end
        object SpeedItem2: TSpeedItem
          Action = acDelEmp
          BtnCaption = #1059#1076#1072#1083#1080#1090#1100
          Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
          ImageIndex = 2
          Spacing = 1
          Left = 26
          Top = 1
          Visible = True
          OnClick = acDelEmpExecute
          SectionName = 'Untitled (0)'
        end
        object SpeedItem3: TSpeedItem
          Action = acKeyEmp
          BtnCaption = 'acKeyEmp'
          Hint = #1087#1088#1072#1074#1072' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
          ImageIndex = 16
          Spacing = 1
          Left = 51
          Top = 1
          Visible = True
          OnClick = acKeyEmpExecute
          SectionName = 'Untitled (0)'
        end
        object siUser_Discharge: TSpeedItem
          Action = acuser_discharge
          Caption = #1091#1074#1086#1083#1080#1090#1100
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FF9632099932089932089C32099C32099C32099C31079C
            30079A3309983309FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF953209973106
            C23B13ED4631F65039F75F41F7664CF75542EE4630B23A18953108FF00FFFF00
            FFFF00FFFF00FFFF00FF9332099A3108CF3D19F3573DFB734EFA6541FBAD9CFA
            B7B0EF452EBF3D2195330BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9E320B
            DC4725FE7552FF7048FF7757FFE9E5FFE9E5F46447B1391A92320BFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FF9A340DCC4C28FC7042F2663FD4A195E1F3F2FE
            CCBBFF7752C14B27923009FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            DE52237A2F2830235316296D213270533B52CE512AC14819FF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0D1E7D00229803228C011C8700
            1374181B64FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            000A290A3EBB0D3BAA0D3CAA0D3AA90C3AAA012297FF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FF000000092766145CEF104ECC0F49C10F47BF11
            4DC50F43B5FF00FF0000BE0000BEFF00FF0000BE0000BEFF00FFFF00FF000000
            07275A145BCE114AAB0F48BB0E41B21357D01354D0FF00FF0000BE0000BE0000
            BE0000BE0000BEFF00FFFF00FF0101010507080003070A2D571D7DEE1A70ED1C
            7AF6155FE7FF00FFFF00FF0000BE0000BE0000BEFF00FFFF00FFFF00FF242220
            242220221E18071F3A1673C9208BEF1E86E90D36A4FF00FF0000BE0000BE0000
            BE0000BE0000BEFF00FFFF00FFFF00FF2725235A5959514E4C2529320A172807
            1120FF00FFFF00FF0000BE0000BEFF00FF0000BE0000BEFF00FFFF00FFFF00FF
            FF00FF7877757877756B655F1F1A16FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          Hint = #1091#1074#1086#1083#1080#1090#1100' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
          ImageIndex = 81
          Spacing = 1
          Left = 76
          Top = 1
          Visible = True
          OnClick = acuser_dischargeExecute
          SectionName = 'Untitled (0)'
        end
      end
      object rdgrPreview: TDBRadioGroup
        Left = 8
        Top = 108
        Width = 169
        Height = 65
        Caption = #1054#1090#1095#1077#1090#1099
        DataField = 'PRWKIND'
        DataSource = dmCom.dsEmp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Items.Strings = (
          #1044#1080#1079#1072#1081#1085#1077#1088
          #1055#1088#1086#1089#1084#1086#1090#1088
          #1055#1077#1095#1072#1090#1100)
        ParentBackground = True
        ParentFont = False
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2')
        OnClick = rdgrPreviewClick
      end
      object lcDep: TDBLookupComboBox
        Left = 12
        Top = 72
        Width = 373
        Height = 21
        Color = clInfoBk
        DataField = 'D_DEPID'
        DataSource = dmCom.dsEmp
        KeyField = 'D_DEPID'
        ListField = 'NAME'
        ListSource = dmCom.dsDep
        TabOrder = 2
        OnCloseUp = lcDepCloseUp
      end
      object cbAllWh: TDBCheckBoxEh
        Left = 16
        Top = 40
        Width = 289
        Height = 17
        Caption = #1056#1072#1073#1086#1090#1072' '#1074' '#1089#1084#1077#1085#1077' '#1089#1086' '#1074#1089#1077#1084#1080' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103#1084#1080
        DataField = 'ALLWH'
        DataSource = dmCom.dsEmp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
        OnClick = cbAllWhClick
      end
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 850
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Position = bpCustom
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 3
    InternalVer = 1
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C
        1F7C1F7C1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F0000E003E003E003E003E003E003
        E003EF3D1F7C1F7C1F7CFF7FE07FFF7FFF7F000000000000E003E00300000000
        0000EF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000E003E0030000FF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7F0000E003E0030000E07F
        FF7FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07F0000000000000000FF7F
        E07FEF3D1F7C1F7C1F7CE07FFF7FE07FE07FFF7FE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FFF7FFF7FE07FFF7FFF7FE07F
        FF7FEF3D1F7C1F7C1F7CEF3DEF3DEF3DEF3DEF3DE07FE07FFF7FE07FE07FFF7F
        E07FEF3D1F7C1F7C1F7CFF7FE07FFF7FFF7FE07FEF3DEF3DEF3DEF3DEF3DEF3D
        EF3D1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 1
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07F007C007C007C007C007C007C007C007C007C
        007C007C007C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7CE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07FE07F
        E07FE07F1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7CE07FE07FE07F
        E07FE07F1F7C1F7C1F7CE07FE07FE07FE07FE07FE07FE07F1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 2
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C00000040E07FE07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C000000400040E07FE07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07FE07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C0000004000400040E07F0000E07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040004000400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF0300400000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000040FF03FF030000E07FE07FE07FE07FE07F0000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C}
      ImageIndex = 0
      Spacing = 1
      Left = 402
      Top = 2
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object spitSort: TSpeedItem
      Action = acSort
      BtnCaption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      Caption = #1057#1086#1088#1090'.'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        000000000000000000001F7C000000000000000000001F7C1F7C1F7C1F7C1F7C
        000000000000FF0300000000E07FFF7FE07FFF7FE07F00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7CFF030000E07FFF7FE07FFF7F0000000000001F7C1F7C1F7C1F7C
        1F7C00000000FF030000FF7FE07FFF7FE07FFF7FE07FFF7F00001F7C1F7C1F7C
        1F7C00000000FF030000E07FFF7FE07FFF7F0000000000000000000000001F7C
        1F7C1F7C1F7CFF030000FF7FE07FFF7FE07FFF7FE07FFF7FE07FFF7FE07F0000
        1F7C007C007CFF030000E07FFF7F000000000000000000000000000000001F7C
        1F7C007C007C000000000000E07FFF7FE07F00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 3
      Spacing = 1
      Left = 194
      Top = 2
      Visible = True
      OnClick = acSortExecute
      SectionName = 'Untitled (0)'
    end
    object siViewUserDischarge: TSpeedItem
      Action = acViewDischarge
      BtnCaption = #1059#1074#1086#1083#1077#1085#1085#1099#1077
      Caption = 'siViewUserDischarge'
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1091#1074#1086#1083#1077#1085#1085#1099#1093' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1077#1081
      ImageIndex = 83
      Spacing = 1
      Left = 130
      Top = 2
      Visible = True
      OnClick = acViewDischargeExecute
      SectionName = 'Untitled (0)'
    end
  end
  object dgEmp: TDBGridEh
    Left = 0
    Top = 356
    Width = 353
    Height = 268
    Align = alLeft
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmCom.dsEmp
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = dgEmpKeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'FNAME'
        Footers = <>
        Width = 106
      end
      item
        EditButtons = <>
        FieldName = 'LNAME'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'SNAME'
        Footers = <>
        Width = 84
      end
      item
        EditButtons = <>
        FieldName = 'NUMEMP'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pmDepart: TPopupMenu
    Left = 256
    Top = 144
    object N1: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077
      ShortCut = 113
      OnClick = N3Click
    end
  end
  object fs1: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 180
    Top = 180
  end
  object acEvent: TActionList
    Images = dmCom.ilButtons
    Left = 256
    Top = 192
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddExecute
    end
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDelExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acSort: TAction
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1082#1072
      ImageIndex = 3
      OnExecute = acSortExecute
    end
    object acAddEmp: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddEmpExecute
    end
    object acDelEmp: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelEmpExecute
    end
    object acKeyEmp: TAction
      Caption = 'acKeyEmp'
      OnExecute = acKeyEmpExecute
      OnUpdate = acKeyEmpUpdate
    end
    object acuser_discharge: TAction
      Hint = #1059#1074#1086#1083#1077#1085#1085#1099#1077' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
      ImageIndex = 81
      OnExecute = acuser_dischargeExecute
    end
    object acViewDischarge: TAction
      Caption = #1059#1074#1086#1083#1077#1085#1085#1099#1077
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1091#1074#1086#1083#1077#1085#1085#1099#1093' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1077#1081
      ImageIndex = 83
      OnExecute = acViewDischargeExecute
    end
  end
  object prgr: TPrintDBGridEh
    DBGridEh = dgEmp
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 396
    Top = 588
  end
end
