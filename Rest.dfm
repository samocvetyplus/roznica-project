object fmRest: TfmRest
  Left = 208
  Top = 169
  Caption = #1054#1089#1090#1072#1090#1082#1080
  ClientHeight = 463
  ClientWidth = 797
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 444
    Width = 797
    Height = 19
    Panels = <>
  end
  object Panel2: TPanel
    Left = 0
    Top = 204
    Width = 797
    Height = 240
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'Panel2'
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 321
      Top = 0
      Height = 240
    end
    object Panel3: TPanel
      Left = 324
      Top = 0
      Width = 473
      Height = 240
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object dg1: TM207IBGrid
        Left = 0
        Top = 29
        Width = 473
        Height = 211
        Align = alClient
        Color = clBtnFace
        DataSource = dm2.dsRest
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        PopupMenu = pmRest
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = dg1DblClick
        OnEditButtonClick = ShowUID
        OnKeyDown = dg1KeyDown
        OnKeyPress = dg1KeyPress
        IniStorage = FormStorage1
        TitleButtons = True
        OnGetCellParams = dg1GetCellParams
        MultiShortCut = 16472
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = False
        OnGetCellCheckBox = dg1GetCellCheckBox
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 60
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ADATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 79
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'NDSID'
            Title.Alignment = taCenter
            Title.Caption = #1053#1044#1057
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 42
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089#1093'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'TW'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088#1080#1093#1086#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 46
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'N'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1087#1088#1080#1093#1086#1076#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Color = clInfoBk
            Expanded = False
            FieldName = 'Q'
            ReadOnly = True
            Title.Caption = #1050'. '#1042#1074
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 30
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1042#1074#1086#1076
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 70
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'COST'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1057#1091#1084#1084#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 38
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UNITID'
            PickList.Strings = (
              #1064#1090
              #1043#1088)
            Title.Alignment = taCenter
            Title.Caption = #1045#1048
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 34
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SUP'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TODO'
            Title.Alignment = taCenter
            Title.Caption = '*'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 29
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PROD'
            Title.Alignment = taCenter
            Title.Caption = #1048#1079#1075'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'MAT'
            Title.Alignment = taCenter
            Title.Caption = #1052#1072#1090'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'GOOD'
            Title.Alignment = taCenter
            Title.Caption = #1053#1072#1080#1084'.'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'INS'
            Title.Alignment = taCenter
            Title.Caption = #1054#1042
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DONE'
            Title.Alignment = taCenter
            Title.Caption = '**'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MTO'
            Title.Alignment = taCenter
            Title.Caption = '***'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MFROM'
            Title.Alignment = taCenter
            Title.Caption = '****'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Color = clInfoBk
            Expanded = False
            FieldName = 'RQ'
            ReadOnly = True
            Title.Caption = #1050'. '#1054#1089#1090
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            ButtonStyle = cbsEllipsis
            Color = clInfoBk
            Expanded = False
            FieldName = 'RW'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = #1054#1089#1090#1072#1090#1086#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 72
            Visible = True
          end>
      end
      object tb2: TSpeedBar
        Left = 0
        Top = 0
        Width = 473
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        BoundLines = [blTop, blBottom, blLeft, blRight]
        Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 1
        InternalVer = 1
        object lbFind: TLabel
          Left = 176
          Top = 8
          Width = 35
          Height = 13
          Caption = #1055#1086#1080#1089#1082':'
          Transparent = True
        end
        object SpeedButton1: TSpeedButton
          Left = 368
          Top = 4
          Width = 81
          Height = 22
          Caption = #1057#1086#1087#1086#1089#1090#1072#1074#1080#1090#1100
          Visible = False
          OnClick = SpeedButton1Click
        end
        object edFind: TEdit
          Left = 216
          Top = 4
          Width = 113
          Height = 21
          TabOrder = 0
          Text = 'edFind'
          OnKeyDown = edFindKeyDown
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object siDelRest: TSpeedItem
          Caption = 'siDelRest'
          Hint = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
          ImageIndex = 2
          Spacing = 1
          Left = 3
          Top = 3
          Visible = True
          OnClick = siDelRestClick
          SectionName = 'Untitled (0)'
        end
        object siIns: TSpeedItem
          Caption = 'siIns'
          Hint = #1042#1089#1090#1072#1074#1082#1080
          ImageIndex = 45
          Spacing = 1
          Left = 51
          Top = 3
          Visible = True
          OnClick = siInsClick
          SectionName = 'Untitled (0)'
        end
        object siFind: TSpeedItem
          Caption = 'siFind'
          Hint = #1055#1086#1080#1089#1082
          ImageIndex = 48
          Spacing = 1
          Left = 75
          Top = 3
          Visible = True
          OnClick = siFindClick
          SectionName = 'Untitled (0)'
        end
        object siMerge: TSpeedItem
          Caption = 'siMerge'
          Hint = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100
          ImageIndex = 10
          Spacing = 1
          Left = 99
          Top = 3
          Visible = True
          OnClick = siMergeClick
          SectionName = 'Untitled (0)'
        end
        object SpeedItem5: TSpeedItem
          Caption = 'SpeedItem5'
          Hint = #1057#1091#1084#1084#1072' '#1086#1089#1090#1072#1090#1082#1086#1074
          ImageIndex = 63
          Spacing = 1
          Left = 123
          Top = 3
          Visible = True
          OnClick = SpeedItem5Click
          SectionName = 'Untitled (0)'
        end
        object SpeedItem6: TSpeedItem
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
          Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100'|'
          ImageIndex = 1
          Spacing = 1
          Left = 27
          Top = 3
          Visible = True
          OnClick = SpeedItem6Click
          SectionName = 'Untitled (0)'
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 321
      Height = 240
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 15
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 15
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 15
      end
      object Splitter2: TSplitter
        Left = 121
        Top = 1
        Height = 238
        AutoSnap = False
        MinSize = 15
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbInsClick
        OnKeyPress = lbInsKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbInsClick
        OnKeyPress = lbInsKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbInsClick
        OnKeyPress = lbInsKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 65
        Height = 238
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbInsClick
        OnKeyPress = lbInsKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 238
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbInsClick
        OnKeyPress = lbInsKeyPress
      end
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 797
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 2
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 731
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siCreate: TSpeedItem
      Action = acAdd
      BtnCaption = #1057#1086#1079#1076#1072#1090#1100
      Caption = #1057#1086#1079#1076#1072#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FF000000
        000000000000000000000000000000000000000000000000FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFF
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF000000000000000000000000000000
        00FF00FF000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
        000000FFFFFFFFFF00FFFFFFFFFF00FFFF000000000000FFFF00000000FFFFFF
        000000FFFF00FFFF00FFFF00FFFF00000000000000000000FFFFFF00FFFFFFFF
        FF00FFFF000000FFFF00000000FFFFFFFFFFFF000000000000FFFFFF000000FF
        FFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF000000FFFF00000000FFFFFF
        FFFF0000000000FFFF000000000000000000000000000000FFFFFF00FFFFFFFF
        FF00FFFF000000FFFF00000000FFFFFFFFFFFFFFFFFF000000FFFFFF00FFFFFF
        FFFF00FFFFFFFFFF00FFFFFFFFFF00FFFFFFFFFF000000FFFF00000000FFFFFF
        FFFF00FFFF00FFFF00000000000000000000000000000000000000000000FFFF
        FF00FFFF000000FFFF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000
        FFFF00000000000000FFFFFFFFFF00FFFF000000000000000000000000FFFFFF
        FFFF00FFFF00FFFF00FFFF00FFFF0000000000FFFF0000000000000000000000
        00FF00FFFF00FFFF00FF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF00000000FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF000000000000
        FFFFFF000000FFFFFF000000FFFFFF000000FFFFFF00000000FFFF000000FF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B0000007B7B7B0000007B7B7B00
        00007B7B7B00000000000000FFFF000000FF00FFFF00FFFF00FFFF00FF000000
        7B7B7B0000007B7B7B0000007B7B7B0000007B7B7B000000FF00FF0000000000
        FF000000FF00FFFF00FF000000FF00FF000000FF00FF000000FF00FF000000FF
        00FF000000FF00FFFF00FFFF00FF000000FF00FFFF00FFFF00FF}
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 32
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acAddExecute
      SectionName = 'Untitled (0)'
    end
    object siView: TSpeedItem
      Action = acViewRest
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000
        00000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FF000000000000000000FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FF00FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B00000000000000FFFFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF0000007B7B7B
        FFFFFF0000FF0000FF0000FF0000FF0000FFFFFFFF7B7B7B000000FF00FFFF00
        FFFF00FFFF00FFFF00FF0000007B7B7BBDBDBDFFFFFFBDBDBD0000FFBDBDBDFF
        FFFFBDBDBD7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B7B7B7B
        FFFFFFBDBDBDFFFFFF0000FFFFFFFFBDBDBDFFFFFF7B7B7B7B7B7BFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF0000007B7B7BFFFFFFBDBDBDFFFFFFBDBDBDFF
        FFFF7B7B7B000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        0000007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B000000FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7B7B7B0000000000000000007B
        7B7BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 4
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acViewRestExecute
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDelRest
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF0000FF0000FF0000FF00
        00FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FFFF00FF00FFFF
        00FFFF00FFFF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF0000
        FF0000FF0000FF0000FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF00FFFF00FFFF00FF
        FF00FFFF00FFFFFF00FFFF00FFFF00FF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 2
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acDelRestExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseRest: TSpeedItem
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1047#1072#1082#1088#1099#1090#1100
      DropDownMenu = pmCloseInv
      Hint = #1047#1072#1082#1088#1099#1090#1100'|'
      ImageIndex = 5
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object siReport: TSpeedItem
      BtnCaption = #1054#1090#1095#1077#1090#1099
      Caption = #1054#1090#1095#1077#1090#1099
      DropDownMenu = pmRep
      Hint = #1055#1077#1095#1072#1090#1100' '#1086#1090#1095#1077#1090#1086#1074
      ImageIndex = 11
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = siReportClick
      SectionName = 'Untitled (0)'
    end
  end
  object M207IBGrid1: TM207IBGrid
    Left = 0
    Top = 40
    Width = 797
    Height = 164
    Align = alClient
    Color = clBtnFace
    DataSource = dm2.dsRInv
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = acViewRestExecute
    TitleButtons = True
    OnGetCellParams = M207IBGrid1GetCellParams
    MultiShortCut = 116
    ColorShortCut = 0
    InfoShortCut = 0
    MultiSelectMode = msmTeapot
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SDATE'
        Title.Alignment = taCenter
        Title.Caption = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 82
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SSF'
        Title.Alignment = taCenter
        Title.Caption = #8470' '#1087#1088#1080#1093#1086#1076#1072
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 87
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'COST'
        Title.Alignment = taCenter
        Title.Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 127
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SUP'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 128
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'DEP'
        Title.Alignment = taCenter
        Title.Caption = #1057#1082#1083#1072#1076
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 143
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'TW'
        Title.Alignment = taCenter
        Title.Caption = #1054#1073#1097#1080#1081' '#1074#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clHighlightText
        Expanded = False
        FieldName = 'TQ'
        Title.Caption = #1050#1086#1083'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clHighlightText
        Expanded = False
        FieldName = 'CRDATE'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 80
        Visible = True
      end>
  end
  object FormStorage1: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 236
    Top = 108
  end
  object pmRest: TPopupMenu
    Images = dmCom.ilButtons
    Left = 336
    Top = 315
    object N1: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = siDelRestClick
    end
    object N2: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      ShortCut = 114
      OnClick = siInsClick
    end
    object N3: TMenuItem
      Caption = #1055#1086#1080#1089#1082
      ImageIndex = 48
      ShortCut = 118
      OnClick = siFindClick
    end
  end
  object pmCloseInv: TPopupMenu
    Left = 124
    Top = 108
    object N4: TMenuItem
      Action = acCloseRest
    end
    object N5: TMenuItem
      Action = acCloseAllRest
    end
  end
  object pmRep: TPopupMenu
    Left = 64
    Top = 104
    object N9: TMenuItem
      Tag = 3
      Caption = #1041#1080#1088#1082#1080' '#1096#1090#1088#1080#1093#1082#1086#1076
      OnClick = N7Click
    end
    object N7: TMenuItem
      Tag = 1
      Caption = #1041#1080#1088#1082#1080
      OnClick = N7Click
    end
    object nTagNew: TMenuItem
      Caption = #1041#1080#1088#1082#1080' '#1085#1086#1074#1099#1077
      OnClick = N7Click
    end
    object N6: TMenuItem
      Tag = 2
      Caption = #1041#1080#1088#1082#1080' '#1095#1077#1088#1085#1099#1077
      Visible = False
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = #1055#1088#1080#1082#1072#1079
      OnClick = N8Click
    end
  end
  object acList: TActionList
    Images = dmCom.ilButtons
    Left = 368
    Top = 112
    object acAdd: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100
      Hint = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1086#1089#1090#1072#1090#1082#1086#1074
      ImageIndex = 32
      ShortCut = 45
      OnExecute = acAddExecute
    end
    object acDelRest: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 2
      ShortCut = 46
      OnExecute = acDelRestExecute
    end
    object acViewRest: TAction
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 4
      ShortCut = 114
      OnExecute = acViewRestExecute
    end
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      OnExecute = acCloseExecute
    end
    object acCloseRest: TAction
      Caption = #1042#1099#1073#1088#1072#1085#1085#1091#1102' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acCloseRestExecute
    end
    object acCloseAllRest: TAction
      Caption = #1042#1089#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
      OnExecute = acCloseAllRestExecute
    end
  end
end
