unit PayTypeG;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, Grids, DBGridEh, ActnList;

type
  TfmPayG = class(TForm)
    taPayG: TpFIBDataSet;
    dsrPayG: TDataSource;
    gridPayG: TDBGridEh;
    taPayGPT: TFIBSmallIntField;
    taPayGDC: TFIBSmallIntField;
    taPayGPERC: TFIBFloatField;
    taPayGSORTIND: TFIBSmallIntField;
    taPayGID: TFIBIntegerField;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    taPayGPAYTYPEID: TFIBIntegerField;
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taPayGNewRecord(DataSet: TDataSet);
    procedure taPayGBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure taPayGBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPayG: TfmPayG;

implementation

uses comdata, dbUtil;

{$R *.dfm}

procedure TfmPayG.acAddExecute(Sender: TObject);
begin
  taPayG.Append;
end;

procedure TfmPayG.acDelExecute(Sender: TObject);
begin
  taPayG.Delete;
end;

procedure TfmPayG.CommitRetaining(DataSet: TDataSet);
begin
  CommitRetaining(DataSet);
end;

procedure TfmPayG.taPayGNewRecord(DataSet: TDataSet);
begin
  taPayGID.AsInteger := dmCom.GetID(59);
  taPayGPT.AsInteger := 1;
  taPayGDC.AsInteger := 0;
  taPayGPERC.AsFloat := 0;
  taPayGSORTIND.AsInteger := ExecSelectSQL('select coalesce(max(SortInd), 0)+1 from D_PayG where PayTypeId='+dmCom.taPayTypePAYTYPEID.AsString, dmCom.quTmp);
  taPayGPAYTYPEID.AsInteger := dmCom.taPayTypePAYTYPEID.AsInteger;
end;

procedure TfmPayG.taPayGBeforeOpen(DataSet: TDataSet);
begin
  taPayG.ParamByName('PAYTYPEID').AsInteger := dmCom.taPayTypePAYTYPEID.AsInteger;
end;

procedure TfmPayG.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taPayG);
end;

procedure TfmPayG.FormShow(Sender: TObject);
begin
  OpenDataSet(taPayG);
end;

procedure TfmPayG.taPayGBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

end.
