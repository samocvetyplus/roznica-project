inherited fmCurrStArt: TfmCurrStArt
  Left = 201
  Top = 74
  Caption = #1058#1077#1082#1091#1097#1077#1077' '#1089#1086#1089#1090#1086#1103#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1072' #'
  ClientHeight = 272
  ClientWidth = 628
  OldCreateOrder = True
  OnClose = FormClose
  ExplicitWidth = 636
  ExplicitHeight = 306
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbr: TStatusBar
    Top = 253
    Width = 628
    ExplicitTop = 253
    ExplicitWidth = 628
  end
  object pgctrl: TPageControl [1]
    Left = 0
    Top = 42
    Width = 628
    Height = 211
    ActivePage = tbshSz
    Align = alClient
    Images = dmCom.ilButtons
    TabOrder = 2
    OnChange = pgctrlChange
    object tbshUID: TTabSheet
      Tag = 4
      Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      object ibgrWHUID: TM207IBGrid
        Left = 0
        Top = 0
        Width = 620
        Height = 182
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsWHUID
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = fmstrStArt
        TitleButtons = True
        OnGetCellParams = ibgrWHUIDGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'UID'
            Title.Alignment = taCenter
            Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 76
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'W'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 38
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 51
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SPRICE'
            Title.Alignment = taCenter
            Title.Caption = #1055#1088'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1089'.'#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN'
            Title.Alignment = taCenter
            Title.Caption = #8470' '#1087#1088#1080#1093
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 65
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE'
            Title.Alignment = taCenter
            Title.Caption = #1044#1072#1090#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 70
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SUP'
            Title.Alignment = taCenter
            Title.Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 146
            Visible = True
          end>
      end
    end
    object tbshSz: TTabSheet
      Tag = 1
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 47
      object grSz: TM207IBGrid
        Left = 0
        Top = 0
        Width = 620
        Height = 182
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHSZ
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = fmstrStArt
        TitleButtons = True
        OnGetCellParams = grSzGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SZ'
            Title.Alignment = taCenter
            Title.Caption = #1056#1072#1079#1084#1077#1088
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 74
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Alignment = taCenter
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 72
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 73
            Visible = True
          end>
      end
    end
    object tbshArt2: TTabSheet
      Tag = 2
      Caption = #1040#1088#1090'2'
      ImageIndex = 9
      object ibgrA2: TM207IBGrid
        Left = 0
        Top = 0
        Width = 620
        Height = 182
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHArt2
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = fmstrStArt
        TitleButtons = True
        OnGetCellParams = ibgrA2GetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        TitleDblClick = True
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Alignment = taCenter
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 84
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'QUANTITY'
            Title.Alignment = taCenter
            Title.Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 90
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = #1042#1077#1089
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 85
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Alignment = taCenter
            Title.Caption = #1062#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 92
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'COST'
            Title.Alignment = taCenter
            Title.Caption = #1057#1091#1084#1084#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 82
            Visible = True
          end>
      end
    end
    object tbshInv: TTabSheet
      Tag = 3
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      ImageIndex = 18
      object dbgrWHInv: TM207IBGrid
        Left = 0
        Top = 0
        Width = 620
        Height = 182
        Align = alClient
        Color = clBtnFace
        DataSource = dmServ.dsrWHInv
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FixedCols = 1
        IniStorage = fmstrStArt
        TitleButtons = True
        OnGetCellParams = dbgrWHInvGetCellParams
        MultiShortCut = 0
        ColorShortCut = 0
        InfoShortCut = 0
        TitleDblClick = True
        ClearHighlight = True
        SortOnTitleClick = True
        Columns = <
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'ART2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 101
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SSF0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SN0'
            Title.Alignment = taCenter
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SDATE0'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 97
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SPRICE'
            Title.Caption = #1055#1088#1080#1093'. '#1094#1077#1085#1072
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'PRICE2'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SC'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end
          item
            Color = clInfoBk
            Expanded = False
            FieldName = 'SW'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clNavy
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Visible = True
          end>
      end
    end
  end
  inherited tb1: TSpeedBar
    Width = 628
    ExplicitTop = -6
    ExplicitWidth = 628
    object spitAddAppl: TSpeedItem
      BtnCaption = #1047#1072#1103#1074#1082#1072
      Caption = #1047#1072#1103#1074#1082#1072
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1079#1072#1103#1074#1082#1077'|'
      ImageIndex = 71
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  object fmstrStArt: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'pgctrl.Height')
    StoredValues = <>
    Left = 156
    Top = 132
  end
end
