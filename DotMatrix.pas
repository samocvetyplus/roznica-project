unit DotMatrix;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZReport, ZRCtrls, StdCtrls;

type
  TForm1 = class(TForm)
    zrCheck: TZReport;
    zrCheckHeader: TZRBand;
    ZRLabel1: TZRLabel;
    ZRLabel2: TZRLabel;
    zrCheckDetail: TZRBand;
    ZRLabel4: TZRLabel;
    zrslDate: TZRSystemLabel;
    ZRSystemLabel1: TZRSystemLabel;
    ZRLabel5: TZRLabel;
    zrvR_City: TZRField;
    zrvR_Address: TZRField;
    zrvPhone: TZRField;
    zrvSellDate: TZRField;
    zrvChekNo: TZRField;
    ZRLabel6: TZRLabel;
    ZRLabel7: TZRLabel;
    ZRLabel8: TZRLabel;
    ZRLabel9: TZRLabel;
    ZRLabel10: TZRLabel;
    ZRLabel11: TZRLabel;
    ZRLabel12: TZRLabel;
    ZRLabel13: TZRLabel;
    ZRLabel14: TZRLabel;
    ZRLabel15: TZRLabel;
    zrvR_Mat: TZRField;
    zrvR_Good: TZRField;
    zrvR_Art: TZRField;
    zrvR_Art2: TZRField;
    zrvUID: TZRField;
    zrvW: TZRField;
    zrvSz: TZRField;
    zrvUNIT: TZRField;
    zrvPrice: TZRField;
    zrvSum: TZRField;
    zrvDiscount: TZRField;
    zrvSum1: TZRField;
    Button1: TButton;
    zrvTotalSum: TZRAggregator;
    zrvTotalSum1: TZRAggregator;
    zrCheckFooter: TZRBand;
    ZRLabel49: TZRLabel;
    ZRTotalLabel1: TZRTotalLabel;
    ZRTotalLabel2: TZRTotalLabel;
    ZRLabel50: TZRLabel;
    ZRLabel51: TZRLabel;
    ZRTotalLabel3: TZRTotalLabel;
    ZRTotalLabel4: TZRTotalLabel;
    ZRLabel52: TZRLabel;
    ZRLabel16: TZRLabel;
    ZRLabel17: TZRLabel;
    ZRLabel19: TZRLabel;
    ZRLabel18: TZRLabel;
    ZRLabel3: TZRLabel;
    ZRLabel20: TZRLabel;
    ZRLabel21: TZRLabel;
    ZRLabel22: TZRLabel;
    ZRLabel28: TZRLabel;
    ZRLabel23: TZRLabel;
    ZRLabel29: TZRLabel;
    ZRLabel24: TZRLabel;
    ZRLabel30: TZRLabel;
    ZRLabel25: TZRLabel;
    ZRLabel26: TZRLabel;
    ZRLabel31: TZRLabel;
    ZRLabel27: TZRLabel;
    ZRLabel32: TZRLabel;
    ZRLabel33: TZRLabel;
    ZRLabel34: TZRLabel;
    ZRLabel35: TZRLabel;
    ZRLabel36: TZRLabel;
    ZRLabel37: TZRLabel;
    ZRLabel38: TZRLabel;
    ZRLabel39: TZRLabel;
    ZRLabel40: TZRLabel;
    ZRLabel41: TZRLabel;
    ZRLabel42: TZRLabel;
    ZRLabel43: TZRLabel;
    ZRLabel44: TZRLabel;
    ZRLabel45: TZRLabel;
    ZRLabel46: TZRLabel;
    ZRLabel47: TZRLabel;
    ZRLabel48: TZRLabel;
    procedure Button1Click(Sender: TObject);
    procedure zrCheckBeforePrint(Sender: TObject; var DoPrint: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses ReportData;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
 zrCheck.Print;
end;

procedure TForm1.zrCheckBeforePrint(Sender: TObject; var DoPrint: Boolean);
begin
// zrCheck.Printer.
end;

end.
