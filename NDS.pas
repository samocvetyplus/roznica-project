unit NDS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ActnList, rxSpeedbar;

type
  TfmNDS = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    dg1: TM207IBGrid;
    spitPrint: TSpeedItem;
    aclist: TActionList;
    acDNDSID: TAction;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure acDNDSIDExecute(Sender: TObject);
    procedure acDNDSIDUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmNDS: TfmNDS;

implementation

uses comdata, M207Proc, Data;

{$R *.DFM}

procedure TfmNDS.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmNDS.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom,dm do
    begin
      tr.Active:=True;
      OpenDataSets([taNDS]);
    end;
 dg1.ColumnByName['NDSID'].Visible:=true;   
end;

procedure TfmNDS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSets([taNDS]);
      CloseDataSets([taNDS]);
      tr.CommitRetaining;
    end;
end;

procedure TfmNDS.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmNDS.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmNDS.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taNDS.Append;
end;

procedure TfmNDS.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taNDS.Delete;
end;

procedure TfmNDS.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsNDS, 37); // nds
end;

procedure TfmNDS.acDNDSIDExecute(Sender: TObject);
begin
 dg1.ColumnByName['NDSID'].Visible:=not dg1.ColumnByName['NDSID'].Visible;
end;

procedure TfmNDS.acDNDSIDUpdate(Sender: TObject);
begin
 acDNDSID.Enabled:= dmcom.Adm;
end;

procedure TfmNDS.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100115)
end;

procedure TfmNDS.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
