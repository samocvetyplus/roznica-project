object fmUidStoreDepItem: TfmUidStoreDepItem
  Left = 122
  Top = 153
  Width = 830
  Height = 480
  HelpContext = 100441
  Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 822
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFF00FF
        0000000000000000000000000000000000000000000000000000000000000000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400FFFF00000000FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000008400008400008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        00000000008400008400008400000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF000000000084FFFF0000008400000000
        FFFF00FFFF00FFFF00FFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FF
        000000000084FFFF00FFFF0000000000FFFF00FFFF00FFFF00FFFF00FFFF0000
        00FF00FFFF00FFFF00FFFF00FFFF00FF00000000000000000000000000000000
        0000000000000000000000000000000000FF00FFFF00FFFF00FF}
      ImageIndex = 0
      Spacing = 1
      Left = 738
      Top = 2
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 674
      Top = 2
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 40
    Width = 822
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object Label2: TLabel
      Left = 12
      Top = 8
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
      Transparent = True
    end
    object Label3: TLabel
      Left = 168
      Top = 8
      Width = 99
      Height = 13
      Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077' '#1080#1079#1076#1077#1083#1080#1103
      Transparent = True
    end
    object Label4: TLabel
      Left = 400
      Top = 8
      Width = 77
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1080#1079#1076#1077#1083#1080#1103
      Transparent = True
    end
    object cmbxGrMat: TDBComboBoxEh
      Left = 52
      Top = 4
      Width = 105
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxGrMatChange
    end
    object cbstate: TComboBox
      Left = 272
      Top = 4
      Width = 113
      Height = 21
      Style = csDropDownList
      Ctl3D = True
      ItemHeight = 13
      ItemIndex = 0
      ParentCtl3D = False
      TabOrder = 1
      Text = '*'#1042#1089#1077
      OnChange = cbstateChange
      Items.Strings = (
        '*'#1042#1089#1077
        #1087#1088#1080#1089#1091#1090#1089#1090#1074#1091#1077#1090
        #1086#1090#1089#1091#1090#1089#1090#1074#1091#1077#1090
        #1085#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1086)
    end
    object eduid: TEdit
      Left = 480
      Top = 4
      Width = 81
      Height = 21
      TabOrder = 2
      OnKeyDown = eduidKeyDown
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 432
    Width = 822
    Height = 19
    Panels = <>
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 69
    Width = 822
    Height = 363
    Align = alClient
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsUidStoreDepItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        EditButtons = <>
        FieldName = 'GR'
        Footers = <>
        Title.Caption = #1043#1088#1091#1087#1087#1072
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1042#1077#1089
      end
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
      end
      item
        EditButtons = <>
        FieldName = 'UNIT'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.'
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUM'
        Footers = <
          item
            FieldName = 'ENTRYNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1093#1086#1076'.||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOST'
        Footers = <
          item
            FieldName = 'ENTRYCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1042#1093#1086#1076'.||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUM'
        Footers = <
          item
            FieldName = 'DINVNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1055'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'DINVCOST'
        Footers = <
          item
            FieldName = 'DINVCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1055'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RETNUM'
        Footers = <
          item
            FieldName = 'RETNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST'
        Footers = <
          item
            FieldName = 'RETCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST2'
        Footers = <
          item
            FieldName = 'RETCOST2'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRETNUM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRETNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRETCOST'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRETCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMFROM'
        Footers = <
          item
            FieldName = 'DINVNUMFROM'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1042#1055'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'DINVFROMCOST'
        Footers = <
          item
            FieldName = 'DINVFROMCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1042#1055'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'SELLNUM'
        Footers = <
          item
            FieldName = 'SELLNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOST'
        Footers = <
          item
            FieldName = 'SELLCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1089#1090'-'#1089#1090#1100' ('#1088#1072#1089#1093'.)'
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOST2'
        Footers = <
          item
            FieldName = 'SELLCOST2'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1089#1090'-'#1089#1090#1100' ('#1088#1077#1072#1083'.)'
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTSELLNUM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTSELLNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTSELLCOST'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTSELLCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESNUM'
        Footers = <
          item
            FieldName = 'ACTALLOWANCESNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESCOST'
        Footers = <
          item
            FieldName = 'ACTALLOWANCESCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1056#1072#1089#1093#1086#1076'||'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ACTNUM'
        Footers = <
          item
            FieldName = 'ACTNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTCOST'
        Footers = <
          item
            FieldName = 'ACTCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RESNUM'
        Footers = <
          item
            FieldName = 'RESNUM'
            ValueType = fvtSum
          end>
        Title.Caption = #1048#1089#1093#1086#1076'.||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RESCOST'
        Footers = <
          item
            FieldName = 'RESCOST'
            ValueType = fvtSum
          end>
        Title.Caption = #1048#1089#1093#1086#1076'.||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYNUMCOM'
        Footers = <
          item
            FieldName = 'ENTRYNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1042#1093#1086#1076'.||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ENTRYCOSTCOM'
        Footers = <
          item
            FieldName = 'ENTRYCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1042#1093#1086#1076'.||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMCOM'
        Footers = <
          item
            FieldName = 'DINVNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1055'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'DINVCOSTCOM'
        Footers = <
          item
            FieldName = 'DINVCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1055'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RETNUMCOM'
        Footers = <
          item
            FieldName = 'RETNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RETCOSTCOM'
        Footers = <
          item
            FieldName = 'RETCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1089#1090'-'#1089#1090#1100' ('#1088#1072#1089#1093'.)'
      end
      item
        EditButtons = <>
        FieldName = 'RETCOST2COM'
        Footers = <
          item
            FieldName = 'RETCOST2COM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1089#1090'-'#1089#1090#1100' ('#1088#1077#1072#1083'.)'
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRETNUMCOM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRETNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTRETCOSTCOM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTRETCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1055#1088#1080#1093#1086#1076'||'#1042#1086#1079#1074#1088#1072#1090'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'DINVNUMFROMCOM'
        Footers = <
          item
            FieldName = 'DINVNUMFROMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1042#1055'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'DINVFROMCOSTCOM'
        Footers = <
          item
            FieldName = 'DINVFROMCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1042#1055'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'SELLNUMCOM'
        Footers = <
          item
          end
          item
            FieldName = 'SELLNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOSTCOM'
        Footers = <
          item
            FieldName = 'SELLCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1089#1090'-'#1089#1090#1100' ('#1088#1072#1089#1093'.)'
      end
      item
        EditButtons = <>
        FieldName = 'SELLCOST2COM'
        Footers = <
          item
            FieldName = 'SELLCOST2COM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1089#1090'-'#1089#1090#1100' ('#1088#1077#1072#1083'.)'
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTSELLNUMCOM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTSELLNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTDISCOUNTSELLCOSTCOM'
        Footers = <
          item
            FieldName = 'ACTDISCOUNTSELLCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1055#1088#1086#1076#1072#1078#1072'||'#1040#1082#1090' '#1089#1082#1080#1076#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESNUMCOM'
        Footers = <
          item
            FieldName = 'ACTALLOWANCESNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTALLOWANCESCOSTCOM'
        Footers = <
          item
            FieldName = 'ACTALLOWANCESCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1056#1072#1089#1093#1086#1076'||'#1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'ACTNUMCOM'
        Footers = <
          item
            FieldName = 'ACTNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080'||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'ACTCOSTCOM'
        Footers = <
          item
            FieldName = 'ACTCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080'||'#1089#1090'-'#1089#1090#1100
      end
      item
        EditButtons = <>
        FieldName = 'RESNUMCOM'
        Footers = <
          item
            FieldName = 'RESNUMCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1048#1089#1093#1086#1076'.||'#1082#1086#1083'-'#1074#1086
      end
      item
        EditButtons = <>
        FieldName = 'RESCOSTCOM'
        Footers = <
          item
            FieldName = 'RESCOSTCOM'
            ValueType = fvtSum
          end>
        Title.Caption = #1050#1086#1084#1080#1089#1089#1080#1103'||'#1048#1089#1093#1086#1076'.||'#1089#1090'-'#1089#1090#1100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 328
    Top = 224
  end
  object aclist: TActionList
    Images = dmCom.ilButtons
    Left = 416
    Top = 224
    object acClose: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      ShortCut = 27
      OnExecute = acCloseExecute
    end
  end
end
