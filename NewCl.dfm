object fmNewCl: TfmNewCl
  Left = 289
  Top = 251
  BorderStyle = bsDialog
  Caption = #1053#1086#1074#1099#1081' '#1082#1083#1080#1077#1085#1090
  ClientHeight = 110
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 132
    Top = 16
    Width = 27
    Height = 13
    Caption = #1060#1048#1054
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 40
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 4
    Top = 16
    Width = 45
    Height = 13
    Caption = #8470' '#1082#1072#1088#1090#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object edCard: TEdit
    Left = 60
    Top = 12
    Width = 61
    Height = 21
    Color = clBtnFace
    Enabled = False
    ReadOnly = True
    TabOrder = 0
    Text = 'edCard'
  end
  object edName: TEdit
    Left = 164
    Top = 12
    Width = 205
    Height = 21
    Color = clInfoBk
    MaxLength = 60
    TabOrder = 1
  end
  object edAdr: TEdit
    Left = 60
    Top = 36
    Width = 309
    Height = 21
    Color = clInfoBk
    MaxLength = 200
    TabOrder = 2
  end
  object BitBtn1: TBitBtn
    Left = 112
    Top = 76
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 208
    Top = 76
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 4
    Kind = bkCancel
  end
  object FormStorage1: TFormStorage
    StoredValues = <>
    Left = 312
    Top = 72
  end
end
