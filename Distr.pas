unit Distr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, SpeedBar, StdCtrls, Grids, DBGrids, RXDBCtrl,
  M207Grid, M207IBGrid, db, Buttons, pFIBDataSet, Placemnt, Menus,
  DBCtrls, DTotal, RXSplit, RXCtrls;

type
  TfmDistr = class(TForm)
    sb2: TStatusBar;
    tb1: TSpeedBar;
    paWH: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    dgWH: TM207IBGrid;
    Panel4: TPanel;
    Label13: TLabel;
    edArt: TEdit;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spWH: TSplitter;
    Panel1: TPanel;
    pa1: TPanel;
    paDepFrom: TPanel;
    dg1: TM207IBGrid;
    sb1: TScrollBox;
    FormStorage1: TFormStorage;
    Splitter6: TSplitter;
    pm1: TPopupMenu;
    N1: TMenuItem;
    pm2: TPopupMenu;
    MenuItem1: TMenuItem;
    miSupFilter: TMenuItem;
    miPriceFilter: TMenuItem;
    paUID: TPanel;
    dg2: TM207IBGrid;
    Label4: TLabel;
    laSup: TLabel;
    Label5: TLabel;
    laPrice: TLabel;
    frDTotal1: TfrDTotal;
    procedure FormCreate(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure dg1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure N1Click(Sender: TObject);
    procedure dg2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edArtChange(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetFilter(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure SpMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure DblClck(Sender: TObject);
    procedure DrgOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure DrgDrop(Sender, Source: TObject; X, Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    lWH: TList;
    lUID: TList;
    lTot: TList;
    parr: array[0..100] of TPanel;
    PCount: integer;
    sp: TPanel;
    DistredDepId: integer;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure BeforeOpenWH(DataSet: TDataSet);
    procedure BeforeOpenUID(DataSet: TDataSet);
    procedure AftOpen(DataSet: TDataSet);
    procedure ShowUID(Sender: TObject);
    procedure AddClick(Sender: TObject);
    procedure DelClick(Sender: TObject);
    procedure ViewClick(Sender: TObject);
    function  GetSZGrid(DepId: integer): TM207IBGrid;
    function  GetUIDGrid(DepId: integer): TM207IBGrid;
  public

    { Public declarations }
    procedure UpdateWh;

  end;

var
  fmDistr: TfmDistr;

implementation

uses comdata, Data, DBTree, strutils, Distred, Data2;

{$R *.DFM}

procedure TfmDistr.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmDistr.FormCreate(Sender: TObject);
var p, p1, p2: TPanel;
    g, g1: TM207IBGrid;
    ds: TDataSource;
    q: TpFIBDataSet;
    pm: TPopUpMenu;
    mi: TMenuItem;
    sbAdd, sbDel, sbView: TSpeedButton;
    fr: TfrDTotal;
    b: boolean;
    laP: TLabel;
    deP: TDBEdit;
begin
  tb1.WallPaper:=wp;
  lWH:=TList.Create;
  lUID:=TList.Create;
  lTot:=TList.Create;
  dg2.Align:=alClient;
  paUID.Align:=alTop;
  paUID.Visible:=False;

  with dm do
    begin
      Distr:=True;
      if WorkMode='DINV' then
        begin
          FillListBoxes(lbComp, lbMat, lbGood, lbIns);
          lbComp.ItemIndex:=0;
          lbMat.ItemIndex:=0;
          lbGood.ItemIndex:=0;
          lbIns.ItemIndex:=0;
        end
      else
        if WorkMode='SINV' then
          begin
            spWH.Visible:=False;
            paWH.Visible:=False;
          end;

      PriceFilter:=False;
      SupFilter:=False;

      lbCompClick(NIL);
      SetFilter(NIL);
    end;

  if dm.WorkMode='DINV' then
    begin
      edArt.Text:='';
      paDepFrom.Caption:=dm.DDepFrom;
      DistredDepId:=dm.DDepFromId;
    end
  else
    begin
      paDepFrom.Caption:=dm.SDep;
      DistredDepId:=dm.taSListDepId.AsInteger;
    end;

  b:=True;
  PCount:=0;
  with dmCom.quDep do
    begin
      Active:=True;
      while NOT EOF do
        begin
          if Fields[0].AsInteger<>dm.DDepFromId then
            begin
              p:=TPanel.Create(sb1);
              if dm.DistrW=-1 then p.Width:=pa1.Width
              else  p.Width:=dm.DistrW;
              p.Align:=alLeft;
              p.Parent:=sb1;
              PArr[PCount]:=p;
              Inc(PCount);

              if b then
                begin
                  sp:=TPanel.Create(sb1);
                  sp.Width:=2;
                  sp.Align:=alLeft;
                  sp.Parent:=sb1;
                  sp.Cursor:=crHSplit;
                  sp.OnMouseMove:=SpMouseMove;
                  b:=False;
                end;

              p2:=TPanel.Create(p);
              p2.Height:=paDepFrom.Height;
              p2.Align:=alTop;
              with  p2.Font do
                begin
                  Style:=Style+[fsBold];
                  Color:=clNavy;
                end;
              p2.Caption:=Fields[1].AsString;
              p2.BevelOuter:=bvLowered;
              p2.Parent:=p;
              p2.OnDragOver:=DrgOver;
              p2.OnDragDrop:=DrgDrop;
              p2.DragMode:=dmAutomatic;


              p1:=TPanel.Create(p);
              p1.Height:=26;//paDepFrom.Height
              p1.Align:=alTop;
              with  p1.Font do
                begin
                  Style:=Style+[fsBold];
                  Color:=clNavy;
                end;
              p1.Caption:='';//Fields[1].AsString;
              p1.BevelOuter:=bvLowered;
              p1.Parent:=p;

              sbAdd:=TSpeedButton.Create(p1);
              sbAdd.Top:=2;
              sbAdd.Left:=2;
              sbAdd.Glyph:=dm.bmpAdd;
              sbAdd.OnClick:=AddClick;
              sbAdd.ShowHint:=True;
              sbAdd.Hint:='��������';
              sbAdd.Tag:=Fields[0].AsInteger;
              sbAdd.Flat:=True;
              sbAdd.Parent:=p1;

              sbDel:=TSpeedButton.Create(p1);
              sbDel.Top:=2;
              sbDel.Left:=4+sbAdd.Width;
              sbDel.Glyph:=dm.bmpDel;
              sbDel.OnClick:=DelClick;
              sbDel.ShowHint:=True;
              sbDel.Hint:='�������';
              sbDel.Tag:=Fields[0].AsInteger;
              sbDel.Flat:=True;
              sbDel.Parent:=p1;

              sbView:=TSpeedButton.Create(p1);
              sbView.Top:=2;
              sbView.Left:=6+sbDel.Width+sbAdd.Width;
              sbView.Glyph:=dm.bmpZoomIn;
              sbView.OnClick:=ViewClick;
              sbView.ShowHint:=True;
              sbView.Hint:='��������';
              sbView.Tag:=Fields[0].AsInteger;
              sbView.Flat:=True;
              sbView.Parent:=p1;

              if dm.WorkMode='DINV' then
                begin
                  laP:=TLabel.Create(p1);
                  laP.Autosize:=True;
                  laP.Caption:='���. ����';
                  laP.Font.Color:=clNavy;
                  laP.Left:=18+sbDel.Width+sbAdd.Width+sbView.Width;
                  laP.Height:=p1.Height-2;
                  laP.Top:=1;
                  laP.Layout:=tlCenter;
                  laP.Parent:=p1;

                  deP:=TDBEdit.Create(p1);
                  deP.ParentFont:=False;
                  deP.Color:=clInfoBk;
                  deP.DataSource:=dm.dsDistrPrice;
                  deP.DataField:='P'+IntToStr(Fields[0].AsInteger);
                  deP.Left:=20+sbDel.Width+sbAdd.Width+sbView.Width+laP.Width;
                  deP.Top:=(p1.Height-deP.Height) div 2;
                  deP.Width:=60;
                  deP.Parent:=p1;
                end;  

              fr:=TfrDTotal.Create(p);
              fr.Align:=alBottom;
              fr.Parent:=p;

              g:=TM207IBGrid.Create(p);
              p1.Tag:=Integer(g);
              g.Align:=alClient;
              g.Columns.Assign(dg1.Columns);
              g.OnGetCellParams:=dg1GetCellParams;
              g.OnDblClick:=ShowUID;
              g.FixedCols:=1;
              g.Name:='A';
              g.Parent:=p;

              ds:=TDataSource.Create(Self);
              g.DataSource:=ds;

              q:=TpFIBDataSet.Create(Self);
              q.Tag:=Fields[0].AsInteger;
              q.Database:=dmCom.db;
              q.Transaction:=dmCom.tr;
              ds.DataSet:=q;

              q.SelectSQL.Text:=' SELECT SZ, QUANTITY, WEIGHT, DQ, DW, COST '+
                                ' FROM WHA2SZ_DISTR( ?ART2ID,  ?ADEPID, ?USERID, ?T) '+
                                ' ORDER BY SZ ';

              q.RefreshSQL.Text:=' SELECT SZ, QUANTITY, WEIGHT, DQ, DW, COST '+
                                ' FROM WHA2SZ_DISTRR(?SZ, ?ART2ID,  ?ADEPID, ?USERID, ?T) ';

              q.BeforeOpen:=BeforeOpenWH;
              q.AfterOpen:=AftOpen;
              q.AfterRefresh:=AftOpen;
              q.Active:=True;
              lWH.Add(g);

              fr.dtQ.DataSource:=ds;
              fr.dtW.DataSource:=ds;
              fr.dtDQ.DataSource:=ds;
              fr.dtDW.DataSource:=ds;
              fr.dtSZ.DataSource:=ds;

              g1:=TM207IBGrid.Create(p);
              g1.Align:=alClient;
              g1.Columns.Assign(dg2.Columns);
              g1.OnGetCellParams:=dg2GetCellParams;
              g1.OnDblClick:=ShowUID;
              g1.FixedCols:=1;
              g1.Visible:=False;
              g1.Name:='B';
              g1.Parent:=p;

              ds:=TDataSource.Create(Self);
              g1.DataSource:=ds;

              q:=TpFIBDataSet.Create(Self);
              q.Tag:=Fields[0].AsInteger;
              q.Database:=dmCom.db;
              q.Transaction:=dmCom.tr;
              ds.DataSet:=q;

              q.SelectSQL.Text:=' SELECT SITEMID, UID, W, TAG, DISTRID '+
                                ' FROM WHART2UI_DISTR( ?ART2ID, ?ADEPID, ?SZ, ?T)';
              q.DeleteSQL.Text:=' DELETE FROM DISTR WHERE DISTRID=?OLD_DISTRID';
              q.InsertSQL.Text:=' INSERT INTO DISTR(DistrID, UserId, SItemId, DepId, T) '+
                                ' VALUES (?DistrId, '+IntToStr(dmCom.UserId)+', ?SItemId, '+Fields[0].AsString+', '+IntToStr(dm.DistrT)+')';

              q.BeforeOpen:=BeforeOpenUID;
              q.AfterOpen:=AftOpen;

              lUID.Add(g1);
//              q.Active:=True;

              pm:=TPopUpMenu.Create(Self);
              pm.Images:=dmCom.ilButtons;
              pm.Tag:=Integer(g);

              g1.PopupMenu:=pm;
              g.PopupMenu:=pm;

              mi:=TMenuItem.Create(pm);
              mi.Caption:='�������/��.������';
              mi.Tag:=Fields[0].AsInteger;
              mi.ShortCut:=ShortCut(VK_F3, []);
              mi.OnClick:=ShowUID;
              mi.ImageIndex:=27;
              pm.Items.Add(mi);

              mi:=TMenuItem.Create(pm);
              mi.Caption:='��������';
              mi.Tag:=Fields[0].AsInteger;
              mi.ShortCut:=ShortCut(VK_INSERT, []);
              mi.OnClick:=AddClick;
              mi.ImageIndex:=1;
              pm.Items.Add(mi);

              mi:=TMenuItem.Create(pm);
              mi.Caption:='�������';
              mi.Tag:=Fields[0].AsInteger;
              mi.ShortCut:=ShortCut(VK_DELETE, [ssCtrl]);
              mi.OnClick:=DelClick;
              mi.ImageIndex:=2;
              pm.Items.Add(mi);

              g.Tag:=Fields[0].AsInteger;
              g1.Tag:=Fields[0].AsInteger;

              ds:=TDataSource.Create(Self);
              q:=TpFIBDataSet.Create(Self);
              q.Tag:=Fields[0].AsInteger;
              q.Database:=dmCom.db;
              q.Transaction:=dmCom.tr;
              ds.DataSet:=q;
              q.SelectSQL.Text:=' SELECT QUANTITY, WEIGHT, DQ, DW, COST '+
                                ' FROM WHA2SZ_DISTRT( ?ART2ID,  ?ADEPID, ?USERID, ?T) ';

              q.BeforeOpen:=BeforeOpenWH;
              lTot.Add(q);

              fr.dtQT.DataSource:=ds;
              fr.dtWT.DataSource:=ds;
              fr.dtQDT.DataSource:=ds;
              fr.dtWDT.DataSource:=ds;
            end;
          Next;
        end;
      Active:=False;
    end;
end;

procedure TfmDistr.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDistr.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmDistr.FormClose(Sender: TObject; var Action: TCloseAction);
var i: integer;
begin
  with dm do
    begin
      PostDataSets([quDistrPrice]);
      CloseDataSets([quD_WH, quD_SZ, quD_UID, quD_T, quDistrPrice]);
    end;

  with lWH do
    for i:=0 to Count-1 do
       TM207IBGrid(Items[i]).DataSource.DataSet.Active:=False;

  with lUID do
    for i:=0 to Count-1 do
       TM207IBGrid(Items[i]).DataSource.DataSet.Active:=False;

  with lTot do
    for i:=0 to Count-1 do
       TpFIBDataSet(Items[i]).Active:=False;

  with dmCom do
    begin
      CloseDataSets([taComp, taMat, taGood, taArt, taIns]);
      tr.CommitRetaining;
    end;
  lWH.Free;
  lUID.Free;
  lTot.Free;
  dm.Distr:=False;
  fmDistr:=NIL;
end;

procedure TfmDistr.lbCompClick(Sender: TObject);
begin
  with dmCom, dm do
    begin
      if WorkMode='DINV' then
        begin
          D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
          D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
          D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
          D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
          ReOpenDataSets([quD_WH, quDistrPrice]);
        end;
      ReOpenDataSets([quD_SZ]);
    end;
  UpdateWH;
end;

procedure TfmDistr.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmDistr.dg1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) AND (Field.FieldName='SZ') then
    case dm.quD_SZT.AsInteger of
        1: Background:=dmCom.clMoneyGreen;
        2: Background:=clBtnFace;
      end;  
end;

procedure TfmDistr.BeforeOpenWH(DataSet: TDataSet);
begin
  with dm, TpFIBDataSet(DataSet).Params do
    begin
      if WorkMode='DINV' then ByName('ART2ID').AsInteger:=quD_WHART2ID.AsInteger
      else ByName('ART2ID').AsInteger:=taSElART2ID.AsInteger;
      ByName('ADEPID').AsInteger:=DataSet.Tag;
      ByName('USERID').AsInteger:=dmCom.UserId;
      ByName('T').AsInteger:=dm.DistrT;
    end;
end;

procedure TfmDistr.UpdateWh;
var i: integer;
begin
  with lWH do
    for i:=0 to Count-1 do ReOpenDataSets([TM207IBGrid(Items[i]).DataSource.DataSet]);
  with lUID do
    for i:=0 to Count-1 do ActDataSets([TM207IBGrid(Items[i]).DataSource.DataSet]);
end;

procedure TfmDistr.N1Click(Sender: TObject);
begin
  miSupFilter.Checked:=False;
  miPriceFilter.Checked:=False;
  SetFilter(NIL);

  with dg2 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=dg2;
          ReOpenDataSets([dm.quD_UID]);
        end
      else dm.quD_UID.Active:=False;
    end;

  with dg1 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=dg1;
          dg1.DataSource.DataSet.Refresh;
        end
    end;

  paUID.Visible:=dg2.Visible;
end;

procedure TfmDistr.dg2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and ((Field.FieldName='UID') or (Field.FieldName='W')) then
    begin
      if TM207IBGrid(Sender).DataSource.DataSet.FieldByName('Tag').AsInteger=0 then Background:=dmCom.clMoneyGreen
      else  Background:=clAqua;
    end;
end;

procedure TfmDistr.ShowUID(Sender: TObject);
var g, g1: TM207IBGrid;
begin
  g:=GetSZGrid(TComponent(Sender).Tag);
  g1:=GetUIDGrid(TComponent(Sender).Tag);

  with g1 do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=g1;
          ReOpenDataSets([g1.DataSource.DataSet]);
        end
      else g1.DataSource.DataSet.Active:=False;
    end;

  with g do
    begin
      Visible:=NOT Visible;
      if Visible then
        begin
          ActiveControl:=g;
          ReOpenDataSets([g.DataSource.DataSet]);
        end;
    end;
end;

procedure TfmDistr.BeforeOpenUID(DataSet: TDataSet);
begin
  with dm, TpFIBDataSet(DataSet).Params do
    begin
      ByName('ADEPID').AsInteger:=DataSet.Tag;
      if WorkMode='DINV' then ByName('ART2ID').AsInteger:=quD_WHArt2Id.AsInteger
      else ByName('ART2ID').AsInteger:=taSElArt2Id.AsInteger;
      ByName('SZ').AsString:=TpFIBDataSet(GetSZGrid(DataSet.Tag).DataSource.DataSet).FieldByName('SZ').AsString;
      ByName('T').AsInteger:=dm.DistrT;
    end;
end;

procedure TfmDistr.AddClick(Sender: TObject);
var q, q1: TpFIBDataSet;
    ds: TDataSet;
    sz: string[20];
begin
  q:=TpFIBDataSet(GetSZGrid(TComponent(Sender).Tag).DataSource.DataSet);
  q1:=TpFIBDataSet(GetUIDGrid(TComponent(Sender).Tag).DataSource.DataSet);

  with dm do
  if dg1.Visible then // N->...
    begin
      ds:=dg1.DataSource.DataSet;
      if ds.Fields[0].IsNull or
        (ds.FieldByName('Quantity').AsFloat=ds.FieldByName('DQ').AsFloat) then  Exit;

      sz:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;

{      with quInsGrDistr, Params do
        begin
          if WorkMode='DINV' then
            begin
              ByName('ART2ID').AsInteger:=  quD_WHArt2Id.AsInteger;
            end
          else
            begin
              ByName('ART2ID').AsInteger:=  taSElArt2Id.AsInteger;
            end;

          case quD_SZT.AsInteger of
              1: ByName('INVID').IsNull:=True;
              2: ByName('INVID').AsInteger:=taSListSInvId.AsInteger;
            end;
          ByName('DEPFROMID').AsInteger:= DistredDepId;
          ByName('DEPTOID').AsInteger:=TComponent(Sender).Tag;
          ByName('SZ').AsString:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;
          ByName('USERID').AsInteger:=dmCom.UserId;
          ByName('T').AsInteger:=dm.DistrT;
          ExecQuery;
        end;}

      dmCom.tr.CommitRetaining;

      quD_SZ.Refresh;

      ReOpenDataSets([quD_T]);
      ReOpenSZQ(q, sz);
      ActDataSets([q1]);
    end
  else
    begin
      if quD_UIDTag.AsInteger=1 then Exit;
      sz:=dg1.DataSource.DataSet.FieldByName('SZ').AsString;
      if q1.Active then // 1->1
        begin
          q1.Append;
          q1.FieldByName('SITEMID').AsInteger:=quD_UIDSItemId.AsInteger;
          q1.FieldByName('UID').AsInteger:=quD_UIDUID.AsInteger;
          q1.FieldByName('W').AsFloat:=quD_UIDW.AsFloat;
          q1.FieldByName('TAG').AsInteger:=1;
          q1.FieldByName('DISTRID').AsInteger:=dmCom.GetId(15);
          q1.Post;

          q1.AfterOpen(q1);
        end
      else  // 1->N
        with quInsDistr, Params do
          begin
            ByName('UserId').AsInteger:=dmCom.UserId;
            ByName('DepId').AsInteger:=TComponent(Sender).Tag;
            ByName('SItemId').AsInteger:=quD_UIDSItemId.AsInteger;
            ByName('T').AsInteger:=dm.DistrT;
            ExecQuery;
          end;
      dmCom.tr.CommitRetaining;
      ReOpenSZQ(q, sz);

      with quD_UID do
        begin
          Refresh;
          while NOT EOF and (quD_UIDTag.AsInteger=1) do Next;
        end;

      quD_SZ.Refresh;

      ReOpenDataSets([quD_T]);
    end;
end;

procedure TfmDistr.DelClick(Sender: TObject);
var i: integer;
    q, q1: TpFIBDataSet;
    sz: string[20];
begin
  q:=TpFIBDataSet(GetSZGrid(TComponent(Sender).Tag).DataSource.DataSet);
  q1:=TpFIBDataSet(GetUIDGrid(TComponent(Sender).Tag).DataSource.DataSet);

  if q1.Active then //1->...
    begin
      if q1.FieldByName('Tag').AsInteger=0 then Exit;
      i:=q1.FieldByName('SITEMID').AsInteger;
      sz:=q.FieldByName('SZ').AsString;
      q1.Delete;
      dmCom.tr.CommitRetaining;
      q1.AfterOpen(q1);

      with dm do
        begin
          ReOpenSZQ(q, sz);
          with quD_UID do
            if Active then
              if quD_UID.Locate('SITEMID', i, []) then quD_UID.Refresh;
          with quTmp do
            begin
              SQL.Text:='SELECT SZ FROM SITEM WHERE SITEMID='+IntToStr(i);
              ExecQuery;
              sz:=DelRSpace(Fields[0].AsString);
              Close;
            end;
          with dm.quD_SZ do
            try
              DisableControls;
              if Locate('SZ', sz, []) then Refresh;
              Next;
              if LocateNext('SZ', sz, []) then Refresh;
            finally
              EnableControls;
            end;
          ReOpenDataSets([quD_T]);
        end;
    end
  else if q.Active then
         begin
           if q.Fields[0].IsNull then Exit;
           sz:=q.FieldByName('SZ').AsString;
{           with dm.quDelDistr, Params do
             begin
               ByName('DEPID').AsInteger:=TComponent(Sender).Tag;
               ByName('USERID').AsInteger:=dmCom.UserId;
               ByName('SZ').AsString:=q.FieldByName('SZ').AsString;
               ByName('T').AsInteger:=dm.DistrT;
               ExecQuery;
               Close;
             end;}
           dmCom.tr.CommitRetaining;
           with dm do
             begin
               ReOpenSZQ(q, sz);
               ReOpenDataSets([quD_UID]);
             end;

           with dm do
             begin
               with quD_SZ do
                 try
                   DisableControls;
                   if Locate('SZ', sz , []) then Refresh;
                   Next;
                   if LocateNext('SZ', sz , []) then Refresh;
                 finally
                   EnableControls;                 
                 end;
               ReOpenDataSets([quD_T])
             end
         end
       else raise Exception.Create('Error!!!');
end;

function  TfmDistr.GetSZGrid(DepId: integer): TM207IBGrid;
var j: integer;
begin
  Result:=NIL;
  with lWH do
    for j:=0 to Count-1 do
      if TM207IBGrid(Items[j]).Tag=DepId then
        begin
          Result:=TM207IBGrid(Items[j]);
          exit;
        end;
  if Result=NIL then raise Exception.Create('NOT FOUND!!!');
end;

function  TfmDistr.GetUIDGrid(DepId: integer): TM207IBGrid;
var j: integer;
begin
  Result:=NIL;
  with lUID do
    for j:=0 to Count-1 do
      if TM207IBGrid(Items[j]).Tag=DepId then
        begin
          Result:=TM207IBGrid(Items[j]);
          exit;
        end;
  if Result=NIL then raise Exception.Create('NOT FOUND!!!');
end;

procedure TfmDistr.AftOpen(DataSet: TDataSet);
var i: integer;
begin
  with lTot do
    for i:=0 to Count-1 do
      if TpFIBDataSet(Items[i]).Tag=DataSet.Tag then
        begin
          ReOpenDataSets([TpFIBDataSet(Items[i])]);
          exit;
        end
end;

procedure TfmDistr.edArtChange(Sender: TObject);
begin
  dm.quD_WH.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
end;

procedure TfmDistr.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  with dm do
    if Key=VK_RETURN then
      with quD_WH do
      begin
        try
          DisableControls;
          Next;
          if NOT LocateNext('ART', edArt.Text, [loCaseInsensitive, loPartialKey]) then Prior;
        finally
          EnableControls;
        end
    end;
end;

procedure TfmDistr.ViewClick(Sender: TObject);
begin
{  with dm2, quDistred, Params do
    begin
      Active:=False;
      ByName('USERID').AsInteger:=dmCom.UserId;
      ByName('DEPID').AsInteger:=TComponent(Sender).Tag;
      ByName('T').AsInteger:=dm.DistrT;
      Active:=True;
    end;
  ShowAndFreeForm(TfmDistred, Self, TForm(fmDistred), True, False);}
end;

procedure TfmDistr.SetFilter(Sender: TObject);
begin
  if Sender<>NIL then
    with TMenuItem(Sender) do
      Checked:=NOT Checked;
  with dm do
    begin
      PriceFilter:=miPriceFilter.Checked;
      SupFilter:=miSupFilter.Checked;
      PriceFilterValue:=quD_UIDPrice.AsFloat;
      SupFilterId:=quD_UIDSupId.AsInteger;
      ReOpenDataSets([quD_UID, quD_T{, quD_A2T2}]);

      if PriceFilter then laPrice.Caption:=quD_UIDPrice.DisplayText
      else laPrice.Caption:='��� ����';
      if SupFilter then laSup.Caption:=quD_UIDSup.DisplayText
      else laSup.Caption:='��� ����������';
    end;
end;

procedure TfmDistr.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
{  if Field<>NIL then
    if Field.FieldName='DEP' then BAckground:=dmCom.clMoneyGreen
    else if Field.FieldName='PRICE2' then
        if Highlight then
          begin
            if dm.quDistrPriceT.AsInteger=1 then Background:=clGreen
            else Background:=clNavy;
          end
        else
          begin
            if dm.quDistrPriceT.AsInteger=1 then Background:=clRed
            else Background:=clInfoBk;
          end}

end;

procedure TfmDistr.SpMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var p: TPoint;
    i: integer;
begin
  if  Shift=[ssLeft] then
    begin
      p:=sp.ClientToScreen(Point(X,Y));
      p:=sb1.ScreenToClient(p);
      for i:=0 to PCount-1 do PArr[i].Width:=p.X;
      dm.DistrW:=p.X;
    end;
end;

procedure TfmDistr.DblClck(Sender: TObject);
begin
  TPanel(TPanel(Sender).Parent).Left:=0;
end;

procedure TfmDistr.DrgOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept:=Source<>Sender;
end;

procedure TfmDistr.DrgDrop(Sender, Source: TObject; X, Y: Integer);
begin
  TPanel(TPanel(Source).Parent).Left:=  TPanel(TPanel(Sender).Parent).Left;
end;

procedure TfmDistr.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_F12: Close;
      end;
end;

end.
