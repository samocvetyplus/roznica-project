unit NewArt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, db, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, Menus, rxPlacemnt;

type
  TfmNewArt = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    dgArt: TM207IBGrid;
    Splitter2: TSplitter;
    M207IBGrid1: TM207IBGrid;
    pmA2: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pmA: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    Panel3: TPanel;
    lbComp: TListBox;
    Splitter8: TSplitter;
    lbCountry: TListBox;
    Splitter4: TSplitter;
    lbGood: TListBox;
    Splitter5: TSplitter;
    lbIns: TListBox;
    Splitter9: TSplitter;
    lbMat: TListBox;
    Splitter10: TSplitter;
    lbAtt1: TListBox;
    Splitter11: TSplitter;
    lbAtt2: TListBox;
    edArt: TEdit;
    btnOk: TButton;
    btnCancel: TButton;
    FormStorage1: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure lbMatKeyPress(Sender: TObject; var Key: Char);
    procedure lbGoodKeyPress(Sender: TObject; var Key: Char);
    procedure lbInsKeyPress(Sender: TObject; var Key: Char);
    procedure edArtEnter(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure dgArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    private
    { Private declarations }

    SearchEnable: boolean;
    ClickCount: integer;
    procedure miInsFromSearchClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  fmNewArt: TfmNewArt;

implementation

uses Data, comdata, DBTree, M207Proc, ServData, RxDbUtils;

{$R *.DFM}

procedure TfmNewArt.FormCreate(Sender: TObject);
begin
  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;
  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbcountry, lbAtt1, lbAtt2);
  dmCom.D_Att1Id := ATT1_DICT_ROOT;
  dmCom.D_Att2Id := ATT2_DICT_ROOT;

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbAtt1.ItemIndex := 0;
  lbAtt2.ItemIndex := 0;
  
  lbCompClick(NIL);
  with dm do
    begin
      OpenDataSets([quA2Dict]);
      edArtEnter(edArt);
      OpenDataSets([quADict]);
      with quTmp do
        begin
          SQL.Text:='SELECT D_ARTID FROM ART2 WHERE ART2ID='+ dmServ.SearchArt;
          ExecQuery;
          D_ArtId:=Fields[0].AsInteger;
          Close;
        end;
      quADict.Locate('D_ARTID', D_ArtId, []);
      quA2Dict.Locate('ART2ID', quUIDWHARt2Id.AsInteger, []);
    end;
  ClickCount := 0;  
end;

procedure TfmNewArt.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
end;

procedure TfmNewArt.edArtChange(Sender: TObject);
begin
  if SearchEnable then
    with dm do
      begin
        quADict.Locate('ART', edArt.Text, [loCaseInsensitive, loPartialKey]);
      end;
end;

procedure TfmNewArt.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmNewArt.lbMatKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmNewArt.lbGoodKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmNewArt.lbInsKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmNewArt.edArtEnter(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_countryId<>D_CountryId) or
       (Old_D_InsId<>D_InsId) or
       (Old_D_Att1Id<>D_Att1Id)or
       (Old_D_Att2Id<>D_Att2Id)
       then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryId:=D_countryId;
         Old_D_Att1Id := D_Att1Id;
         Old_D_Att2Id := D_Att2Id;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quADict]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmNewArt.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     VK_RETURN:
     begin
       ClickCount := 0;
       with edArt do
         if SelLength=0 then SelectAll
         else SelStart:=Length(edArt.Text);
       if (dm.quADict.FieldByName('ART').asString<>edArt.Text) then miInsFromSearchClick(Sender);
      ActiveControl:=dgArt;
     end;
     VK_DOWN: ActiveControl:=dgArt;
  end;
end;

procedure TfmNewArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    begin
      Art2Id:=-1;
      if ModalResult=mrOK then
        begin
          PostDataSets([quADict, quA2Dict]);
          if NOT quA2DictArt2Id.IsNull then
            begin
              if (quUIDWHUnitId.AsInteger<> quADictUnitId.AsInteger) and (quUIDWHUnitId.AsString <> '') then
                raise Exception.Create('������� ��������� ������� � ������ ��������� �� ���������');
              Art2Id:=quA2DictArt2Id.AsInteger;
            end;
        end
      else  CancelDataSets([quADict, quA2Dict]);
      CloseDataSets([quADict, quA2Dict]);
    end;
end;

procedure TfmNewArt.N1Click(Sender: TObject);
begin
  dm.quA2Dict.Insert;
end;

procedure TfmNewArt.N2Click(Sender: TObject);
begin
  dm.quA2Dict.Delete;
end;

procedure TfmNewArt.N3Click(Sender: TObject);
begin
  dm.quADict.Insert;
end;

procedure TfmNewArt.N4Click(Sender: TObject);
begin
  dm.quADict.Delete;
end;
procedure TfmNewArt.miInsFromSearchClick(Sender: TObject);
begin
  with dm, quADict do
    begin
      Append;
      quADictArt.AsString:=edArt.Text;
    end;
end;




procedure TfmNewArt.dgArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     VK_RETURN:
     begin
        inc(ClickCount);
        if (dm.quADict.State = dsInsert) or (dm.quADict.State =dsEdit) then
        begin
         dm.quADict.Post;
         ReOpenDataSets([dm.quA2Dict])
        end;
        if ClickCount = 2 then
          ModalResult := mrOK;
     end;
  end;
end;


end.
