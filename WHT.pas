unit WHT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, Placemnt, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ComCtrls, db;

type
  TfmWHT = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    M207IBGrid1: TM207IBGrid;
    FormStorage1: TFormStorage;
    StatusBar1: TStatusBar;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmWHT: TfmWHT;

implementation

uses comdata, Data, Data2, M207Proc;

{$R *.DFM}

procedure TfmWHT.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmWHT.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmWHT.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  with dm, dm2 do
    begin
      Caption:='����� �� ������ - '+SDep;
      quWHT.Active:=True;
    end;
end;

procedure TfmWHT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm2 do
    begin
      quWHT.Active:=False;
    end;
end;

procedure TfmWHT.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWHT.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Highlight then Background:=clNavy
  else case dm2.quWHTT.AsInteger of
          1: Background:=clInfoBk;
          2: Background:=clBtnFace;
          3: Background:=clAqua;
          4: Background:=dmCom.clMoneyGreen;
        end;
end;

end.

  case dm2.quWHTT.AsInteger of
      1: Font.Style:=Font.Style+[fsBold];
      2: Font.Style:=Font.Style-[fsBold];

    end;

