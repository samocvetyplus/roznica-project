unit SItemCDM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SpeedBar, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  Placemnt, DBCtrls, db, StdCtrls, Mask, ToolEdit, Menus;

type
  TfmSItemCDM = class(TForm)
    ibgrWH: TM207IBGrid;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    fmstrSItemCDM: TFormStorage;
    Splitter6: TSplitter;
    Panel1: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    Splitter8: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    lbCountry: TListBox;
    Splitter1: TSplitter;
    Panel2: TPanel;
    tb2: TSpeedBar;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    dgArt: TM207IBGrid;
    Label2: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    Label3: TLabel;
    pmPrint: TPopupMenu;
    mnitPrList: TMenuItem;
    N1: TMenuItem;
    procedure siExitClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure ceArtEnter(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbSearchClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure miInsFromSearchClick(Sender: TObject);
    procedure siAddClick(Sender: TObject);
    procedure dgArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgArtExit(Sender: TObject);
    procedure siPrintClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
  private
    { Private declarations }
     SearchEnable: boolean;
     StopFlag: boolean;
     function  Stop: boolean;
  public
  end;

var
  fmSItemCDM: TfmSItemCDM;

implementation

uses ServData, WH, Comdata, ReportData, Data, M207Proc, DBTree;

{$R *.DFM}

procedure TfmSItemCDM.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSItemCDM.FormCreate(Sender: TObject);
var i:integer;
begin
  tb1.WallPaper:=wp;
  with dmServ, dmCom do
  begin
    OpenDataSets([taSItemCDM]);
    Caption:='��������� � '+taSInvCDMNum.AsString;

    D_CompId:=311;
    D_MatId:='';
    D_GoodId:='';
    D_CountryId:='*';
    SearchEnable:=False;
    ceArt.Text:='';
    SearchEnable:=True;
    { TODO -obasile : �������� ���������� }
    dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbcountry,nil,nil);
    lbMat.ItemIndex:=0;
    lbGood.ItemIndex:=0;
    lbIns.ItemIndex:=0;
    lbCountry.ItemIndex:=0;
    with lbComp do
    for i:=0 to Items.Count-1 do
      if TNodeData(Items.Objects[i]).Code=D_CompId then
        begin
          ItemIndex:=i;
          lbCompClick(lbComp);
          break;
        end;
  end;
end;

procedure TfmSItemCDM.siDelClick(Sender: TObject);
begin
  dmServ.taSItemCDM.Delete;
end;

function TfmSItemCDM.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmSItemCDM.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmSItemCDM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmServ.taSItemCDM, dmCom.quComp]);
  dmServ.taSItemCDM.Transaction.CommitRetaining;
  Action := caFree;
end;

procedure TfmSItemCDM.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
end;

procedure TfmSItemCDM.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmSItemCDM.ceArtEnter(Sender: TObject);
begin
 with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_CountryId<>D_CountryId) or
       (Old_D_InsId<>D_InsId) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_countryId:=D_countryId;
         if cbSearch.Checked then Art:=''
         else Art:=ceArt.Text;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quArt]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmSItemCDM.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmSItemCDM.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if SearchEnable then
    begin
      if cbSearch.Checked then
        begin
          StopFlag:=False;
          with dm, quArt do
            if Active then
             begin
              LocateF(dm.quArt, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
             end;
        end;
    end;
end;

procedure TfmSItemCDM.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then
                       begin
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                         if (quArt.FieldByName('ART').asString<>ceArt.Text) then miInsFromSearchClick(Sender);
                       end
                     else
                       begin
                         if cbSearch.Tag=0 then
                           begin
                             Art:=ceArt.Text;
                             ReopenDataSets([quArt]);
                             cbSearch.Tag:=1;
                             if (quArt.FieldByName('ART').asString<>ceArt.Text) then miInsFromSearchClick(Sender);
                           end;
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                       end;
                    ActiveControl:=dgArt;
                   end;
     VK_INSERT: if Shift=[ssCtrl] then  miInsfromSearchClick(nil);
     VK_ESCAPE: StopFlag:=True;
    end;
end;

procedure TfmSItemCDM.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then ActiveControl:=dgArt;
end;

procedure TfmSItemCDM.cbSearchClick(Sender: TObject);
begin
 if cbSearch.Checked then
    with dm do
      begin
        Art:='';
        ReopenDataSets([quArt]);
      end;
  cbSearch.Tag:=0;
end;

procedure TfmSItemCDM.SpeedItem1Click(Sender: TObject);
begin
{ lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  with dmCom, dm do
    begin
      Art:='';
      D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
      D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
      D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
      D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
      D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
      Old_D_CompId:=D_CompId;
      Old_D_MatId:=D_MatId;
      Old_D_GoodId:=D_GoodId;
      Old_D_InsId:=D_InsId;
      Old_D_CountryId:=D_CountryId;
    end;

  Screen.Cursor:=crSQLWait;
  ReopenDataSets([dm.quArt]);
  Screen.Cursor:=crDefault;}
end;

procedure TfmSItemCDM.miInsFromSearchClick(Sender: TObject);
begin
  with dm, quArt do
    begin
      Append;
      quArtArt.AsString:=ceArt.Text;
    end
end;

procedure TfmSItemCDM.siAddClick(Sender: TObject);
begin
 with dmServ,taSItemCDM do
  begin
   Append;
//   taSItemCDM.AsInteger :=0;
   Post;
  end; 
end;

procedure TfmSItemCDM.dgArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_SPACE) and (dgArt.SelectedField.FieldName='UNITID') then
    with dm, quArt do
      begin
        if NOT (State in [dsInsert, dsEdit]) then Edit;
        with quArtUnitId do
          AsInteger:=Integer(NOT Boolean(AsInteger));
      end
  else
  if (Key=VK_RETURN) then
    begin
      siAddClick(Sender);
      ActiveControl:=ibgrWH;
      ibgrWH.ColumnByName['Q'].Field.FocusControl;
      Key:=0;
    end
  else
   with dm, quArt do
    // begin
      if NOT (State in [dsInsert, dsEdit]) then Edit;
end;

procedure TfmSItemCDM.dgArtExit(Sender: TObject);
begin
  PostDataSets([dm.quArt]);
end;

procedure TfmSItemCDM.siPrintClick(Sender: TObject);
var
  arr : TarrDoc;
begin
{  SetLength(arr, 1);
  arr[0] := dmServ.NoSItemCDM;
 try
  PrintDocument(arr, SItemCDM_sup);
 finally
  finalize(arr);
 end;}
end;

procedure TfmSItemCDM.N1Click(Sender: TObject);
begin
// dmReport.SItemCDMeText(dmServ.NoSItemCDM,DBText1.Field.AsString);
end;

end.
