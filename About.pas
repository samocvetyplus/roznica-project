unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, RxGIF, jpeg;

type
  TfmAbout = class(TForm)
    imLogo: TImage;
    ProductName: TLabel;
    Version: TLabel;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAbout: TfmAbout;

implementation

{$R *.dfm}

uses UtilLib;

procedure TfmAbout.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key=#27 then Close;
end;

procedure TfmAbout.FormCreate(Sender: TObject);
var
  FileVersionInfo : TFileVersionInfo;
begin
 // FileVersionInfo := GetFileVersionInfo(Application.ExeName);
  ProductName.Caption := FileVersionInfo.ProductName;
  Version.Caption := FileVersionInfo.FileVersion;
 // Copyright.Caption := FileVersionInfo.LegalCopyright;
  {$IFDEF BUSSINESS}
  //ProgramIcon.Visible := False;
  imLogo.Visible := True;
  {$ELSE}
  //ProgramIcon.Visible := True;
  imLogo.Visible := False;
  {$ENDIF}
end;

end.
