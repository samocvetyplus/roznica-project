unit SRet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, RXDBCtrl, StdCtrls, Mask,
  Grids, DBGrids, M207Grid, M207IBGrid, db, Menus,
  DateUtils, ComDrv32, ActnList, DBCtrlsEh, Buttons, M207DBCtrls, jpeg,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, DBGridEh, DBGridEhGrouping, rxPlacemnt, GridsEh, rxToolEdit,
  rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, 
  dxSkinscxPCPainter, cxGridCustomPopupMenu, cxGridPopupMenu, cxContainer,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, FIBDataSet, pFIBDataSet, cxDBExtLookupComboBox,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinPumpkin,
  dxSkinsDefaultPainters;

type
  TfmSRet = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    edSN: TDBEdit;
    FormStorage1: TFormStorage;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter2: TSplitter;
    Splitter6: TSplitter;
    DBText2: TDBText;
    pmEl: TPopupMenu;
    N1: TMenuItem;
    pmWH: TPopupMenu;
    N2: TMenuItem;
    siCloseInv: TSpeedItem;
    pc1: TPageControl;
    tsWH: TTabSheet;
    tb2: TSpeedBar;
    Label2: TLabel;
    edUID: TEdit;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    lbCountry: TListBox;
    Splitter7: TSplitter;
    dbSup: TDBText;
    acList: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acCloseInv: TAction;
    acClose: TAction;
    acAddArt: TAction;
    dbedSdate: TDBEditEh;
    bttime: TBitBtn;
    Label3: TLabel;
    edPTr: TM207DBEdit;
    edTrans: TM207DBEdit;
    edPTrNDS: TM207DBEdit;
    DBEdit1: TM207DBEdit;
    SpeedItem2: TSpeedItem;
    acReCalc: TAction;
    Label7: TLabel;
    chbxRet_NoNDS: TDBCheckBoxEh;
    siHelp: TSpeedItem;
    Label8: TLabel;
    Label9: TLabel;
    dbt_Proizvoditel: TDBText;
    dbt_Gruzootprav: TDBText;
    Label10: TLabel;
    dgEl: TDBGridEh;
    dgWH: TDBGridEh;
    acOpenInv: TAction;
    lc_Gruzootprav: TcxDBLookupComboBox;
    lc_Proizvoditel: TcxDBLookupComboBox;
    lcComp: TcxDBLookupComboBox;
    taContract: TpFIBDataSet;
    Label11: TLabel;
    dsContract: TDataSource;
    GridContract: TcxGrid;
    GridContractView: TcxGridDBTableView;
    GridContractViewNUMBER: TcxGridDBColumn;
    GridContractViewCONTRACTDATE: TcxGridDBColumn;
    GridContractViewNAME: TcxGridDBColumn;
    GridContractLevel: TcxGridLevel;
    lcContract: TcxDBExtLookupComboBox;
    taContractID: TFIBIntegerField;
    taContractNUMBER: TFIBStringField;
    taContractCONTRACTDATE: TFIBDateTimeField;
    taContractCONTRACTENDDATE: TFIBDateTimeField;
    taContractISUNLIMITED: TFIBSmallIntField;
    taContractCLASSID: TFIBIntegerField;
    taContractNAME: TFIBStringField;
    taContractNAMEFORMATED: TFIBStringField;
    GridContractViewFORMATEDNAME: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
 //   procedure dgElGetCellParams(Sender: TObject; Field: TField;
 //     AFont: TFont; var Background: TColor; Highlight: Boolean);
///    procedure dgWHGetCellParams(Sender: TObject; Field: TField;
 ///     AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure dgWHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem1Click(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acCloseExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure bttimeClick(Sender: TObject);
    procedure acReCalcExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edPTrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edTransKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPTrNDSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acReCalcUpdate(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure ButtonRefresh;
    procedure dgElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siCloseInvClick(Sender: TObject);
    procedure acOpenInvExecute(Sender: TObject);
    procedure lc_GruzootpravPropertiesChange(Sender: TObject);
    procedure taContractBeforeOpen(DataSet: TDataSet);
    procedure taContractAfterOpen(DataSet: TDataSet);
    procedure lcCompPropertiesEditValueChanged(Sender: TObject);
    procedure lcContractPropertiesCloseUp(Sender: TObject);
    procedure lcContractPropertiesEditValueChanged(Sender: TObject);
    procedure taContractCalcFields(DataSet: TDataSet);
  private
    SearchEnable: boolean;
    StopFlag: boolean;
    LogOprIdForm:string;
    ExistsNum:boolean;
    StoredSupplierID: Integer;    
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    function  Stop: boolean;
  end;

var
  fmSRet: TfmSRet;

implementation

uses comdata, Data, DBTree, DItem, Data2, OptItem,
     ReportData, SellUid, SRetUid, M207Proc, FIBQuery, SRetLst,
     ServData, Data3, JewConst, dbUtil, SetSDate, MsgDialog, Variants;

{$R *.DFM}

procedure TfmSRet.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmSRet.FormCreate(Sender: TObject);
var
    i:integer;
begin
  StoredSupplierID := dm.taSRetListCOMPID.AsInteger;

  taContract.Active := True;

  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
    with dmCom, dm, dm2 do
    begin
      dgWH['DQ'].Title.Caption:=taSRetListDepFrom.AsString+' - ���-��';
      dgWH['DW'].Title.Caption:=taSRetListDepFrom.AsString+' - ���';
      D_WHArt:='';
      Old_D_MatId:='.';
      CloseDataSets([taRet,taSRet, quSupR]);
      OpenDataSets([taRet,taSRet, quSupR]);
    end;



  {***********����������� ������*********}
   if dm.taSRetListISCLOSED.AsInteger=1 then ExistsNum:=true
   else ExistsNum:= not dm.taSRetListSN.IsNull;
   if not ExistsNum then
    with dm, quTmp do
    begin
     close;
     SQL.Text:='SELECT max(sn) FROM SINV '+
               'WHERE ITYPE=7 '+
               ' AND DepFromId='+taSRetListDepFromId.AsString+' AND FYEAR(SDATE)='+GetSYear(taSRetListSDATE.AsDateTime);
     ExecQuery;
     if Fields[0].IsNull then i:=1
     else i:=Fields[0].AsInteger+1;
     Close;
     Transaction.CommitRetaining;
     taSRetList.Edit;
     taSRetListSN.AsInteger:=i;
     taSRetList.Post;
     taSRetList.Refresh;
    end;
  {**************************************}

  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns,lbCountry,nil,nil);
  dmCom.D_Att1Id := ATT1_DICT_ROOT;
  dmCom.D_Att2Id := ATT2_DICT_ROOT;

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbCompClick(NIL);
  SearchEnable:=False;
  ceArt.Text:='';
  SearchEnable:=True;
  SetCBDropDown(lcComp);
  tsWh.Caption:=dm.taSRetListDepFrom.AsString;
  edUID.Text:='';
  dm.SetCloseInvBtn(siCloseInv, dm.taSRetListIsClosed.AsInteger);

  ButtonRefresh;

{ if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
  else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
   else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
    else  dmserv.ComScan.ComPort:=pnCOM4;

 dmServ.ComScan.Connect;
 dmcom.SScanZ:='';   }
 ActiveControl:=edUid;
 dm.SellMode:=true;
end;

procedure TfmSRet.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSRet.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Bookmark : Pointer;
begin
  with dm.taSRetList, dm.taSRet do
      if State in [dsEdit, dsInsert] then Post;

  dmCom.tr.CommitRetaining;
  dm.taSRetList.Refresh;

  if dm.taSRetListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSets([dm.taSRetList]);
  end else begin
   ReOpenDataSets([dm.taSRet]);
   if not dm.closeinv(dm.taSRetListSINVID.AsInteger,0) then
   begin{
    if dm.EmptyInv(dm.taSRetListSINVID.AsInteger,2) then
    begin
     if not dm.taSRetListSINVID.IsNull then
      ExecSQL('delete from sinv where sinvid='+dm.taSRetListSINVID.AsString, dm.quTmp);
    if dm.taSRetList.Active then  ReOpenDataSet(dm.taSRetList);
    end
    else }
    begin
     if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
     if (not ExistsNum) and (not dm.taSRetListSINVID.IsNull) then
     begin
      ExecSQL('update sinv set sn=null where sinvid='+dm.taSRetListSINVID.AsString, dm.quTmp);
      dm.taSRetList.Refresh;
     end;
    end
   end;
   dm.taSRetList.Refresh;
  end;

  CloseDataSets([dm.taSRet, dm.quD_WH, dm.quSupR, dmCom.taRet]);
  dmCom.tr.CommitRetaining;
  // ����������� ����� � ������ ���������� ���������
  dm.taSRetList.DisableScrollEvents;
  dm.taSRetList.DisableControls;
  Bookmark := dm.taSRetList.GetBookmark;
  try
    fmSRetList.gridSRetList.SumList.RecalcAll;
  finally
    dm.taSRetList.GotoBookmark(Bookmark);
    dm.taSRetList.EnableScrollEvents;
    dm.taSRetList.EnableControls;
  end;

// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
end;

procedure TfmSRet.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
end;

procedure TfmSRet.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmSRet.lcCompPropertiesEditValueChanged(Sender: TObject);
var
  SupplierID: Integer;
begin
  if varToStr(lcComp.EditValue) <> '' then
  begin
    dm.taSRetList.Edit;

    dm.taSRetList.FieldByName('CompID').AsInteger := Integer(lcComp.EditValue);

    SupplierID := Integer(lcComp.EditValue);

    if StoredSupplierID <> SupplierID then
    begin
      dm.taSRetList.FieldByName('contract$id').AsVariant := null;

      StoredSupplierID := SupplierID;
    end;

    dm.taSRetList.Post;

    dm.taSRetList.Refresh;

    ReOpenDataSet(taContract);
  end;
end;

procedure TfmSRet.lcContractPropertiesCloseUp(Sender: TObject);
begin
  ActiveControl := dgEl;
end;

procedure TfmSRet.lcContractPropertiesEditValueChanged(Sender: TObject);
var
  SupplierID: Integer;
begin
  if varToStr(lcContract.EditValue) <> '' then
  begin
    dm.taSRetList.Edit;

    dm.taSRetList.FieldByName('Contract$ID').AsInteger := Integer(lcContract.EditValue);

    dm.taSRetList.Post;

    dm.taSRetList.Refresh;
  end;
end;

procedure TfmSRet.lc_GruzootpravPropertiesChange(Sender: TObject);
var
  SupplierID: Integer;
begin
  if varToStr(lc_Gruzootprav.EditValue) <> '' then
  begin
    lcComp.Properties.OnEditValueChanged := nil;

    try

    dm.taSRetList.Edit;

    dm.taSRetList.FieldByName('CompID').AsInteger := Integer(lc_Gruzootprav.EditValue);

    SupplierID := Integer(lc_Gruzootprav.EditValue);

    if StoredSupplierID <> SupplierID then
    begin
      dm.taSRetList.FieldByName('contract$id').AsVariant := null;

      StoredSupplierID := SupplierID;
    end;

    dm.taSRetList.Post;

    dm.taSRetList.Refresh;

    ReOpenDataSet(taContract);

    finally
      lcComp.Properties.OnEditValueChanged := lcCompPropertiesEditValueChanged;
    end;
  end;
end;

procedure TfmSRet.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var SItemId, T: integer;
    Contract: string;
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        taSRetList.Refresh;
        if taSRetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
        else begin
         if taSRetListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');
         with quCheckSupUID, Params do
          begin
            ByName['UID'].AsInteger:=StrToInt(edUID.Text);
            ByName['DEPID'].AsInteger:=taSRetListDepFromId.AsInteger;
            ByName['SUPID'].AsInteger:=taSRetListCompId.AsInteger;
            ByName['CONTRACT$ID'].AsInteger:= taSRetListCONTRACTID.AsInteger;
            
            ExecQuery;

            SItemId:=FieldByName('SItemID').AsInteger;
            T:=FieldByName('T').AsInteger;
            Contract := FieldByName('contract').AsString;
            Close;
          end;

         {
         if T  > 100 then
         begin
           T := 1;
         end;
         }
         if T > 99 then
         begin
           if T = 100 then
           begin
             MessageDialog('�� ������ �������.', mtError, [mbOk], 0);
           end else
           if T = 101 then
           begin
             MessageDialog('������� �� ����� �������� ��������, �� ���� ��������� � ���������. ', mtError, [mbOk], 0);

             with taSRet do
                  begin
                    Append;
                    taSRetSItemId.AsInteger:=SItemId;
                    Post;
                    Last;
                    refresh;  // dm.taSRet.Locate('UID', edUID.text, []);
                  end;

           end else
           if T = 102 then
           begin
             MessageDialog('������� ������� �� ��������� � ��������� ��������� ��  �������� : "��� ��������".', mtError, [mbOK], 0);
           end else
           if T = 103 then
           begin
             MessageDialog('������ ��������� ����������� � �� ������������ �������.'#13#10 + '������� �������: ' + Contract, mtError, [mbOK], 0);
           end;

         end;

         case T of
            1: with taSRet do
                  begin
                    Append;
                    taSRetSItemId.AsInteger:=SItemId;
                    Post;
                    Last;
                    refresh;  // dm.taSRet.Locate('UID', edUID.text, []);
                  end;
            2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);

            3: MessageDialog('������� �������', mtInformation, [mbOK], 0);

            4: MessageDialog('������� ���������� ������ �����������', mtInformation, [mbOK], 0);

            5: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
          end;
        end;

        edUID.Text:='';

      end;
end;

procedure TfmSRet.Button1Click(Sender: TObject);
begin
  with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_InsId<>D_InsId)or
       (Old_D_CountryId<>D_CountryID) then
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryID:=D_CountryID;
         Screen.Cursor:=crSQLWait;
         dm.quD_WH.Active:=False;
         dm.quD_WH.Open;
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmSRet.dgWHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ((Key=VK_SPACE) or (Key=VK_RETURN)) then acAddExecute(nil);
end;

procedure TfmSRet.SpeedItem1Click(Sender: TObject);
begin
  with dmCom, dm do
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryId:=D_CountryId;
         D_WHArt:=ceArt.Text;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([dm.quD_WH]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmSRet.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmSRet.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if cbSearch.Checked then
    begin
      StopFlag:=False;
      with dm, quD_WH do
        if Active then LocateF(dm.quD_WH, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
    end;

end;

procedure TfmSRet.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then  ceArt.SelectAll
                     else if cbSearch.Tag=0 then
                            begin
                              dm.D_WHArt:=ceArt.Text;
                              ReopenDataSets([dm.quD_WH]);
                              cbSearch.Tag:=1;
                            end
                          else
                            begin
                              ceArt.SelectAll;
                              cbSearch.Tag:=0;
                            end;
                   end;
      VK_ESCAPE: StopFlag:=True;
    end;

end;

procedure TfmSRet.ceArtKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=VK_DOWN then ActiveControl:=dgWH;
end;

function TfmSRet.Stop: boolean;
begin
  Result:=StopFlag;
end;


procedure TfmSRet.taContractAfterOpen(DataSet: TDataSet);
begin
//  lcContract.Properties.DropDownRows := taContract.RecordCount;
end;

procedure TfmSRet.taContractBeforeOpen(DataSet: TDataSet);
var
  ContractClassID: Integer;
begin
  ContractClassID := 0;

  taContract.ParamByName('agent$id').AsInteger := StoredSupplierID;

  taContract.ParamByName('invoice$date').AsDate := dm.taSRetListSDATE.AsDateTime;

  taContract.ParamByName('contract$class$id').AsInteger := ContractClassID;    
end;

procedure TfmSRet.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmSRet.acAddExecute(Sender: TObject);
var i: integer;
    LogOperationID:string;
begin
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');

  LogOperationID:=dm3.insert_operation(sLog_AddSRet,LogOprIdForm);

with dm, taSRet do
   try
    fmSRetUID:=TfmSRetUID.Create(NIL);

    fmSRetUID.ContractID := dm.taSRetListCONTRACTID.AsInteger;

    fmSRetUID.ContractClassID := dm.taSRetListCONTRACTCLASSID.AsInteger;

    quSRetUID.Active:=True;
    with fmSRetUID do
     if ShowModal=mrOK then
      if not quSRetUIDSItemId.IsNull then
        if dg1.SelectedRows.Count=0 then
         with taSRet do
         begin
          Append;
          taSRetSItemId.AsInteger:=quSRetUIDSItemId.AsInteger;
          Post;
         end
        else
     for i := 0 to dg1.SelectedRows.Count - 1 do
      with taSRet do
       begin
        quSRetUID.Bookmark:=dg1.SelectedRows[i];
        Append;
        taSRetSItemId.AsInteger:=quSRetUIDSItemId.AsInteger;
        Post;
       end;
   finally
    quSRetUID.Active:=False;
    fmSRetUID.Free;
    dm.quD_WH.Refresh;
   end;
 dm3.update_operation(LogOperationID);
end;

procedure TfmSRet.acDelExecute(Sender: TObject);
var LogOperationID:string;
begin
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');

  LogOperationID:=dm3.insert_operation(sLog_DelSRet,LogOprIdForm);
  dm.taSRet.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmSRet.acCloseInvExecute(Sender: TObject);
var i: integer;
    s_close, s_closed, LogOperationID:string;
begin
  with dm do
    begin
      PostDataSets([taSEl, taSRetList]);
      taSRetList.Refresh;
      if taSRetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
      else begin
       if taSRetListIsClosed.AsInteger=0 then
        begin
          PostDataSets([taSEl, taSRetList]);
          with quTmp do
            begin
              if taSRetListSN.IsNull then
              begin
               close;
               SQL.Text:='SELECT max(sn) FROM SINV '+
                         'WHERE ITYPE=7 '+
                         ' AND DepFromId='+taSRetListDepFromId.AsString+
                         ' AND FYEAR(SDATE)='+GetSYear(taSRetListSDATE.AsDateTime);
               ExecQuery;
               if Fields[0].IsNull then i:=1
               else i:=Fields[0].AsInteger+1;
               Close;
               Transaction.CommitRetaining;
               taSRetList.Edit;
               taSRetListSN.AsInteger:=i;
               taSRetList.Post;
               taSRetList.Refresh;
              end;

              SQL.Text:='SELECT COUNT(*) FROM SINV '+
                        'WHERE ITYPE=7 AND SN='+taSRetListSN.AsString+
                        ' AND DepFromId='+taSRetListDepFromId.AsString+
                        ' AND FYEAR(SDATE)='+GetSYear(taSRetListSDATE.AsDateTime);

              ExecQuery;
              i:=Fields[0].AsInteger;
              Close;
              Transaction.CommitRetaining;
              if i>1 then raise Exception.Create('������������ ����� ���������!!!');
            end;
              LogOperationID:=dm3.insert_operation(sLog_CloseSRet,LogOprIdForm);
              with quTmp do
                begin
                  {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
                  SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+taSRetListSInvId.AsString+')';
                  ExecQuery;
                  s_close:=trim(Fields[0].AsString);
                  s_closed:=trim(Fields[1].AsString);
                  Transaction.CommitRetaining;
                  close;
                end;
              if (s_close='') and (s_closed='') then
              begin
               Screen.Cursor:=crSQLWait;
               ExecSQL('EXECUTE PROCEDURE CloseInv '+taSRetListSInvId.AsString+', 2, '+IntToStr(dmCom.UserId), qutmp);
               Screen.Cursor:=crDefault;
              end
              else begin
               if s_close<>'' then  MessageDialog(s_close + ' �������� ���� ���������. ', mtWarning, [mbOk], 0);
               if s_closed<>'' then  MessageDialog(s_closed + ' �������� ���� ���������. ', mtWarning, [mbOk], 0);
              end;
              taSRetList.Refresh;
              dm3.update_operation(LogOperationID);

          if s_close='' then SetCloseInvBtn(siCloseInv, 1);
        end;
      end;
    end;
  ButtonRefresh;
end;

procedure TfmSRet.bttimeClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm.taSRetListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm.taSRetListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm.taSRetList.Edit;
    dm.taSRetListSDATE.AsDateTime:=d;
    dm.taSRetList.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmSRet.acReCalcExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  if dm.taSRetListISCLOSED.AsInteger=1 then raise Exception.Create(rsInvClose);
  {log}
  LogOperationID:=dm3.insert_operation('��������',LogOprIdForm);
  {***}
  PostDataSets([dm.taSRet, dm.taSRetList]);
  ExecSQL('execute procedure RECALCINV_SRET('+dm.taSRetListSINVID.AsString+')', dm.quTmp);
  dm.taSRetList.Refresh;
  ReOpenDataSet(dm.taSRet);
  {log}
  dm3.update_operation(LogOperationID);
  {***}
end;

procedure TfmSRet.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100212);
end;

procedure TfmSRet.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmSRet.edPTrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmSRet.edTransKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmSRet.edPTrNDSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmSRet.DBEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmSRet.acReCalcUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taSRetListSINVID.IsNull) and (dm.taSRetListISCLOSED.AsInteger=0)
end;

procedure TfmSRet.acAddUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := (not dm.taSRetListSINVID.IsNull) and (not dm.taSRetListCOMPID.IsNull) and (dm.taSRetListISCLOSED.AsInteger=0);

  TAction(Sender).Enabled := Enabled;

  Enabled := (not dm.taSRetListSINVID.IsNull) and (not dm.taSRetListCOMPID.IsNull) and (dm.taSRetListISCLOSED.AsInteger=0);

  lcContract.Enabled := not taContract.IsEmpty;
end;

procedure TfmSRet.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm.taSRetListSINVID.IsNull) and (not dm.taSRetSRETID.IsNull)
end;

procedure TfmSRet.ButtonRefresh;
begin
  dm.SetVisEnabled(TWinControl(dbSup),dm.taSRetListISCLOSED.AsInteger=1);
  dm.SetVisEnabled(TWinControl(dbt_Gruzootprav),dm.taSRetListISCLOSED.AsInteger=1);
  dm.SetVisEnabled(TWinControl(dbt_Proizvoditel),dm.taSRetListISCLOSED.AsInteger=1);

  dm.SetVisEnabled(lcComp,dm.taSRetListISCLOSED.AsInteger <> 1);
  dm.SetVisEnabled(lc_Gruzootprav,dm.taSRetListISCLOSED.AsInteger <> 1);
  dm.SetVisEnabled(lc_Proizvoditel,dm.taSRetListISCLOSED.AsInteger <> 1);
end;

procedure TfmSRet.dgElGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
   if (Column<>NIL) and
      ((Column.FieldName='FULLART') or
       (Column.FieldName='ART2') or
       (Column.FieldName='RecNo')) then
       case dm.taSElWHSell.AsInteger of
          0: Background:=dmcom.clMoneyGreen;
          1: Background:=dmCom.clCream;
       end;
end;

procedure TfmSRet.dgWHGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
    if Column.FieldName='OPTPRICE' then
       case dm.quD_WHOPEQ.AsInteger of
          0: Background:=clRed;
          1: Background:=clYellow;
       end
       else
       if (Column.FieldName='DQ') or
          (Column.FieldName='DW')
          then Background:=dmCom.clCream;
end;

procedure TfmSRet.siCloseInvClick(Sender: TObject);
begin
if (dm.taSRetListISCLOSED.AsInteger=1) then
   acOpenInv.execute else acCloseInv.execute;
end;

procedure TfmSRet.acOpenInvExecute(Sender: TObject);
var i: integer;
    s_close, s_closed, LogOperationID:string;
begin
   with dm do
    begin
    PostDataSets([taSEl, taSRetList]);
      taSRetList.Refresh;
      if taSRetListSINVID.IsNull then MessageDialog('��������� �������!!!', mtInformation, [mbOk], 0)
      else begin

            LogOperationID:=dm3.insert_operation(sLog_OpenSRet,LogOprIdForm);
            Screen.Cursor:=crSQLWait;
           {�������� ���������}
            ExecSQL('UPDATE SInv SET ISCLOSED=0 WHERE SINVID='+taSRetListSInvId.AsString, qutmp);
           {��������� ������� tmpdata ���� ���� ������� ������� ����� �����������}
            {
            ExecSQL('execute procedure Before_Open_InvRet('+dm.taSRetListSINVID.AsString+
                    ', 0, '+inttostr(dmcom.UserId)+')', qutmp);
            with quTmp do
            begin
             sql.Text:='select id1 from tmpdata where datatype=8 and '+
                       ' userid='+inttostr(dmcom.UserId)+' and id2='+dm.taSRetListSINVID.AsString;
             ExecQuery;
             s_close:='';
             while not eof do
             begin
              s_close:=s_close+#13#10+Fields[0].AsString;
              Next;
             end;
             Transaction.CommitRetaining;
             close;
            end;
            Screen.Cursor:=crDefault;


            if s_close<>'' then
            begin
             s_close:='���� ��������� ���� �� �������� �� ���������. �������:'+s_close+ #13#10+'��������� �����������.';
             if MessageDialog(s_close, mtConfirmation, [mbYes, mbNo], 0)=mrYes then
             begin
              //������� ���� ����������
              Screen.Cursor:=crSQLWait;
              ExecSQL('execute procedure Before_Open_InvRet('+dm.taSRetListSINVID.AsString+
                      ', 1, '+inttostr(dmcom.UserId)+')', qutmp);
              Screen.Cursor:=crDefault;
              MessageDialog('��� ���������� ������!', mtInformation, [mbOk], 0);
             end;
            //�������� ������� tmpdata
             ExecSQL('delete from tmpdata where datatype=8 and id2='+dm.taSRetListSINVID.AsString, qutmp);
            end;
            }
            taSRetList.Refresh;
            SetCloseInvBtn(siCloseInv, 0);
            dm3.update_operation(LogOperationID);
          end;
          end;
          ButtonRefresh;
end;

procedure TfmSRet.taContractCalcFields(DataSet: TDataSet);
var
  N: string;
  Date: string;
  Name: string;
begin
  N := taContractNUMBER.AsString;

  Name := taContractNAME.AsString;

  Name := Name + ' � ' + N;

  Date := FormatDateTime('yyyy.mm.dd', taContractCONTRACTDATE.AsDateTime);

  Name := Name + ' �� ' + Date;

  if taContractISUNLIMITED.AsInteger = 0 then
  begin
    Date := FormatDateTime('yyyy.mm.dd', taContractCONTRACTENDDATE.AsDateTime);

    Name := Name + ' �� ' + Date;
  end;

  taContractNAMEFORMATED.AsString := Name;
end;


end.


