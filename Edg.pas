unit Edg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, rxSpeedbar;

type
  TfmEdg = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    pc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    dg1: TM207IBGrid;
    dg2: TM207IBGrid;
    spitPrint: TSpeedItem;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure pc1Change(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmEdg: TfmEdg;

implementation

uses comdata, M207Proc, Data;

{$R *.DFM}

procedure TfmEdg.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmEdg.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      tr.Active:=True;
      EdgT:=1;
      OpenDataSets([taEdg]);
      pc1.ActivePage:=ts1;
    end;
end;

procedure TfmEdg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSets([taEdg]);
      CloseDataSets([taEdg]);
      tr.CommitRetaining;
    end;
end;

procedure TfmEdg.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmEdg.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmEdg.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taEdg.Append;
end;

procedure TfmEdg.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taEdg.Delete;
end;

procedure TfmEdg.pc1Change(Sender: TObject);
begin
  with dmCom, taEdg do
    begin
      Active:=False;
      EdgT:=pc1.ActivePage.TabIndex+1;
      Active:=True;
    end
end;

procedure TfmEdg.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsEdg, 38); // 'edg'
end;

procedure TfmEdg.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100107)
end;

procedure TfmEdg.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
