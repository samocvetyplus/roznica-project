unit JDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, ExtCtrls, Buttons, Grids, DBGrids,
  RXDBCtrl, M207Grid, M207IBGrid, M207Proc, DB, rxSpeedbar;

type
  TfmJDep = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    spitCalc: TSpeedItem;
    spitPrint: TSpeedItem;
    tb2: TSpeedBar;
    laPeriod: TLabel;
    laDep: TLabel;
    lAll: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    spbr4: TSpeedBar;
    lbMat: TLabel;
    lbGood: TLabel;
    lbComp: TLabel;
    lbSup: TLabel;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem8: TSpeedItem;
    SpeedItem7: TSpeedItem;
    SpeedItem9: TSpeedItem;
    SpeedItem10: TSpeedItem;
    chlbProd: TCheckListBox;
    chlbGood: TCheckListBox;
    chlbGrMat: TCheckListBox;
    chlbMat: TCheckListBox;
    btnDo: TSpeedButton;
    M207IBGrid1: TM207IBGrid;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem8Click(Sender: TObject);
    procedure SpeedItem9Click(Sender: TObject);
    procedure SpeedItem7Click(Sender: TObject);
    procedure SpeedItem10Click(Sender: TObject);
    procedure chlbMatClickCheck(Sender: TObject);
    procedure chlbGrMatClickCheck(Sender: TObject);
    procedure chlbGoodClickCheck(Sender: TObject);
    procedure chlbProdClickCheck(Sender: TObject);
    procedure spitCalcClick(Sender: TObject);
    procedure btnDoClick(Sender: TObject);
    procedure chlbMatExit(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure spitPrintClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    LogOprIdForm:string;
    function GetIdxByCode(sl: TStringList; Code: Variant): integer;
    function ChlbClickCheck(chlb:TCheckListBox;var st: string):variant;
    function CheckMultiContr(chlb:TCheckListBox;var st:string; lCanModif:boolean=false):boolean;
  end;

var
  fmJDep: TfmJDep;
  I_BD, I_ED: TDateTime;

implementation

uses comdata, Data, Data2, ServData, DBTree, Period, ReportData, Data3, JewConst, dbUtil;

{$R *.dfm}
function TfmJDep.GetIdxByCode(sl: TStringList; Code: Variant): integer;
var
  i : integer;
begin
  Result := 0;
  for i := 0 to Pred(sl.Count) do
    if TNodeData(sl.Objects[i]).Code = Code then
    begin
      Result := i;
      exit;
    end;
end;

procedure TfmJDep.FormCreate(Sender: TObject);
var  d,m,y : word;
begin
  LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);

  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  spbr4.WallPaper:=wp;

  DecodeDate(dmCom.GetServerTime, y, m, d);
  if (m = 1)  then
  begin
   m := 13;
   y := y-1;
  end;
  if (m = 2)  then
  begin
   m := 14;
   y := y-1;
  end;
  I_Bd := EncodeDate(y, m-2, d);
  I_Ed := dmCom.GetServerTime;

  laPeriod.Caption := 'C '+DateToStr(I_Bd)+' �� '+DateToStr(I_Ed);


(*****************������������, ���������, ���� ����������, ������*****)



  with dmCom, dm, dm2 do
    begin
      D_MatId:='*';
      D_GoodId:='*';
      D_CompId:=-1;
      D_GrMatID:='*';

      lMultiSelect:=false;
      lSellSelect:=false;
      AllUIDWH:=true;
      chlbProd.MultiSelect := True;
      chlbGrMat.MultiSelect := True;
      chlbMat.MultiSelect := True;
      chlbGood.MultiSelect := True;

      chlbProd.Items.Assign(dm.slProd);
      chlbProd.ItemIndex := GetIdxByCode(dm.slProd, D_CompId);
      chlbProd.Checked[chlbProd.ItemIndex]:=true;
      chlbProdClickCheck(chlbProd);

      chlbGrMat.Items.Assign(dm.slGrMat);
      chlbGrMat.ItemIndex := GetIdxByCode(dm.slGrMat, D_GrMatId);
      chlbGrMat.Checked[chlbGrMat.ItemIndex]:=true;
      chlbGrMatClickCheck(chlbGrMat);

      chlbMat.Items.Assign(dm.slMat);
      chlbMat.ItemIndex := GetIdxByCode(dm.slMat, D_MatId);
      chlbMat.Checked[chlbMat.ItemIndex]:=true;
      chlbMatClickCheck(chlbMat);

      chlbGood.Items.Assign(dm.slGood);
      chlbGood.ItemIndex := GetIdxByCode(dm.slGood, D_GoodId);
      chlbGood.Checked[chlbGood.ItemIndex]:=true;
      chlbGoodClickCheck(chlbGood);

    end;
(********************************************************************)

  siExit.Left:=tb1.Width-tb1.BtnWidth-10;

end;

procedure TfmJDep.SpeedItem8Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbProd,True);
  chlbProd.SetFocus;
end;

procedure TfmJDep.SpeedItem9Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGood,True);
  chlbGood.SetFocus;
end;

procedure TfmJDep.SpeedItem7Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbGrMat,True);
  chlbGrMat.SetFocus;

end;

procedure TfmJDep.SpeedItem10Click(Sender: TObject);
begin
  dm.SetVisEnabled(chlbMat,True);
  chlbMat.SetFocus;
end;
procedure TfmJDep.chlbMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_MatId:=ChlbClickCheck(chlbMat,s);
 SpeedItem10.BtnCaption:=s;

end;

procedure TfmJDep.chlbGrMatClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GrMatId:=ChlbClickCheck(chlbGrMat,s);
 SpeedItem7.BtnCaption:=s;

end;

procedure TfmJDep.chlbProdClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_CompId:=ChlbClickCheck(chlbProd,s);
 SpeedItem8.BtnCaption:=s;

end;
function TfmJDep.ChlbClickCheck(chlb:TCheckListBox;var st: string):variant;
var i: integer;
begin
  i:=chlb.ItemIndex;
  WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
  if chlb.Checked[i] then
    begin
     st:=chlb.Items[i];
     if (i<>0) then     chlb.Checked[0]:=false;
    end
  else
    begin
     i:=chlb.Items.Count-1;
     WHILE (i>0) and (not chlb.Checked[i]) do dec(i);
     st:=chlb.Items[i];
     chlb.Checked[i]:=true;
    end;
  Result:=TNodeData(chlb.Items.Objects[i]).Code;
end;


procedure TfmJDep.chlbGoodClickCheck(Sender: TObject);
var s:string;
begin
 dmCom.D_GoodId:=ChlbClickCheck(chlbGood,s);
 SpeedItem9.BtnCaption:=s;

end;


procedure TfmJDep.spitCalcClick(Sender: TObject);
var LogOperationID:string;
begin
  if dm.countmaxpross then sysUtils.Abort;
  LogOperationID:=dm3.insert_operation(sLog_CalcJDep,LogOprIdForm);
  ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);

  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbProd,False);
  Application.Minimize;
  ExecSQL('execute procedure ANALIZ_DEP(''' + DateTimeToStr(I_BD) + ''',''' +
                                                    DateTimeToStr(I_ED) + ''')', dm.quTmp);

  if not dmServ.taJDep.Transaction.Active then dmServ.taJDep.Transaction.StartTransaction;
  ReopenDataSets([dmServ.taJDep]);
  btnDoClick(nil);
  Application.Restore;

  ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);
  dm3.update_operation(LogOperationID);
end;

procedure TfmJDep.btnDoClick(Sender: TObject);
var l1,l2,l3,l4:boolean;
    NCount: integer;
    Res: Variant;
begin

  Application.Minimize;
  with dmServ.taJDep do
  begin
    Close;
    l1:=CheckMultiContr(chlbProd,dm.SD_CompId);
    if l1 then dmCom.D_CompId:=-1;
    l2:=CheckMultiContr(chlbGrMat,dm.SD_GrMatId,true);
    if l2 then dmCom.D_GrMatId:='*';
    l3:=CheckMultiContr(chlbMat,dm.SD_MatId,true);
    if l3 then dmCom.D_MatId:='*';
    l4:=CheckMultiContr (chlbGood,dm.SD_GoodId,true);
    if l4 then dmCom.D_GoodId:='*';

    if l1 or l2 or l3 or l4 then
    begin
        if (dm.SD_MatID <>'')then
            SelectSQL[5]:=' and s.D_MatId in ('+dm.SD_MatID+') '
        else
            SelectSQL[5]:=' ';

        if (dm.SD_GoodID<>'')then
            SelectSQL[6]:=' and s.D_GoodId in ('+dm.SD_GoodID+')'
        else
            SelectSQL[6]:=' ';

        if (dm.SD_CompID<>'')then
            SelectSQL[7]:=' and s.CompId in ('+dm.SD_CompID+') '
        else
            SelectSQL[7]:=' ';

        if (dm.SD_GrMatID<>'')then
            SelectSQL[8]:=' and s.Gr in ('+dm.SD_GrMatID+') '
        else
            SelectSQL[8]:=' ';

    end
    else
    begin
      SelectSQL[5]:=' '; SelectSQL[6]:=' '; SelectSQL[7]:=' '; SelectSQL[8]:=' ';
    end;
    Prepare;
  end;
  Res := ExecSelectSQL('select count(*) from depanlz', dm.quTmp);
  if not VarIsNull(Res) then
    Ncount :=Integer(Res)
  else
    Ncount :=0;

  if not dmServ.taJDep.Transaction.Active then dmServ.taJDep.Transaction.StartTransaction;
  if NCount <> 0 then dmServ.taJDep.Open;

  dm.SetVisEnabled(chlbMat,False);
  dm.SetVisEnabled(chlbGrMat,False);
  dm.SetVisEnabled(chlbGood,False);
  dm.SetVisEnabled(chlbProd,False);

  Application.Restore;
end;

function TfmJDep.CheckMultiContr(chlb:TCheckListBox;var st:string; lCanModif:boolean=false):boolean;
var i:integer;
    s:string;
begin
  Result:=False;
  st:='';s:='';
  if chlb.Checked[0] then
   begin
    for i:=1 to chlb.Items.Count-1 do
      chlb.Checked[i]:=False;
    st:='';
   end
  else
   begin
    for i:=0 to chlb.Items.Count-1 do
     if chlb.Checked[i] then
     begin
       Result:=true;
       s:=TNodeData(chlb.Items.Objects[I]).Code;
       if lCanModif then s:=#39+s+#39;
       st:=st+s+',';
     end;
     delete(st,length(st),1);
   end;
end;

procedure TfmJDep.chlbMatExit(Sender: TObject);
begin
   dm.SetVisEnabled(TWinControl(sender),False);
end;

procedure TfmJDep.SpeedItem1Click(Sender: TObject);
begin
  if GetPeriod(I_Bd, I_Ed) then
  begin
    laPeriod.Caption := 'C '+DateToStr(I_Bd)+' �� '+DateToStr(I_Ed);
    spitCalcClick(Sender);
  end;
end;

procedure TfmJDep.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmJDep.M207IBGrid1GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field.FieldKind = fkCalculated then
    Background := dm.CU_SO1
  else
    Background:=dmServ.taJDep.FieldByName('Colour').AsInteger;
    //Background := dmCom[dmServ.taJDepD_DEPID.AsInteger].Color
end;


procedure TfmJDep.spitPrintClick(Sender: TObject);
begin
  dmReport.PrintDocumentB(dep_alaiz);
end;

procedure TfmJDep.FormResize(Sender: TObject);
begin
   siExit.Left:=tb1.Width-tb1.BtnWidth-10;
   siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmJDep.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100402)
end;

procedure TfmJDep.FormActivate(Sender: TObject);
begin
   siExit.Left:=tb1.Width-tb1.BtnWidth-10;
   siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
