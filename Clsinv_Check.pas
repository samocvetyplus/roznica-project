{/************************************/
 /*������ �������� ��������� ���******/
 /***�������� ��������� ���������*****/
 /************************************/}
unit CLSINV_CHECK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, StdCtrls, Buttons, GridsEh, DBGridEh, DB,
  FIBDataSet, pFIBDataSet;

type
  TfmCLSINV_CHECK = class(TForm)
    DBGridEh1: TDBGridEh;
    btnOk: TBitBtn;
    clsinv: TpFIBDataSet;
    dsClsinv: TDataSource;
    BitBtn1: TBitBtn;
    clsinvSN: TFIBIntegerField;
    clsinvRESNAME: TFIBStringField;
    clsinvSSUM: TFIBFloatField;
    clsinvSDATE: TFIBDateTimeField;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure clsinvBeforeOpen(DataSet: TDataSet);
  
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCLSINV_CHECK: TfmCLSINV_CHECK;

implementation
uses UIDStoreList, comdata, dateUtils;
{$R *.dfm}



procedure TfmCLSINV_CHECK.clsinvBeforeOpen(DataSet: TDataSet);
begin
clsinv_check.fmCLSINV_CHECK.clsinv.ParamByName('bd').AsDateTime:=StartOfTheDay(uidstorelist.bd);
clsinv_check.fmCLSINV_CHECK.clsinv.ParamByName('ed').AsDateTime:=EndOfTheDay(uidstorelist.ed);
end;

procedure TfmCLSINV_CHECK.FormActivate(Sender: TObject);
begin
   clsinv.Active:=true;

end;

procedure TfmCLSINV_CHECK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
clsinv.Active:=false;
end;

end.
