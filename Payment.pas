{****************************************}
{  ������ �� �����                       }
{  Copyrigth (C) 2005 Kornejchouk Basile }
{  v0.02                                 }
{****************************************}
unit Payment;

interface

uses
  Windows, Messages, SysUtils, Forms, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxDropDownEdit,
  FIBDataSet, FR_DSet, FR_DBSet, PrnDbgeh, M207Ctrls, Dialogs,
  cxLookAndFeels, pFIBDataSet, Classes, ActnList, ImgList, Controls,
  cxGridDBTableView, ExtCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridBandedTableView, cxGridDBBandedTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, StdCtrls, Mask,
  DBCtrlsEh, TB2Item, TB2Dock, TB2Toolbar, Grids, DBGridEh, cxPC, ComCtrls,
  dxSkinsCore, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  DBGridEhGrouping, rxPlacemnt, GridsEh, cxLookAndFeelPainters,
  Menus, cxLocalization, cxDBLookupComboBox,
  StrUtils;

type
  TfmPayment = class(TForm)
    stbrStatus: TStatusBar;
    ilButtons: TImageList;
    ActionList2: TActionList;
    acPeriod: TAction;
    taPayment: TpFIBDataSet;
    dsrPayment: TDataSource;
    acAdd: TAction;
    acDel: TAction;
    taPaymentID: TFIBIntegerField;
    taPaymentC: TFIBFloatField;
    taPaymentPD: TFIBDateTimeField;
    taComp: TpFIBDataSet;
    taCompD_COMPID: TFIBIntegerField;
    taCompNAME: TFIBStringField;
    acBalanceB: TAction;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem3: TTBItem;
    TBItem2: TTBItem;
    TBItem1: TTBItem;
    TBItem4: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBControlItem1: TTBControlItem;
    TBControlItem2: TTBControlItem;
    Label1: TLabel;
    cbComp: TDBComboBoxEh;
    taBalance: TpFIBDataSet;
    dsrBalance: TDataSource;
    TBDock2: TTBDock;
    taBalanceID: TFIBIntegerField;
    taBalanceSESSIONID: TFIBIntegerField;
    taBalanceCOMPID: TFIBIntegerField;
    taBalanceBALANCE_B: TFIBFloatField;
    taBalanceBALANCE_E: TFIBFloatField;
    taBalanceICOST: TFIBFloatField;
    taBalanceRCOST: TFIBFloatField;
    taBalanceCASHCOST: TFIBFloatField;
    taBalanceCOMPNAME: TFIBStringField;
    taCompSNAME: TFIBStringField;
    cxTabSheet3: TcxTabSheet;
    cxLookAndFeelController: TcxLookAndFeelController;
    acClose: TAction;
    fr1: TM207FormStorage;
    taBalanceCOMPSNAME: TFIBStringField;
    GridBalance: TcxGrid;
    GridBalanceView: TcxGridDBBandedTableView;
    GridBalanceViewCOMPNAME: TcxGridDBBandedColumn;
    GridBalanceViewBALANCE_B: TcxGridDBBandedColumn;
    GridBalanceViewBALANCE_E: TcxGridDBBandedColumn;
    GridBalanceViewICOST: TcxGridDBBandedColumn;
    GridBalanceViewRCOST: TcxGridDBBandedColumn;
    GridBalanceViewCASHCOST: TcxGridDBBandedColumn;
    GridBalanceViewCOMPSNAME: TcxGridDBBandedColumn;
    GridBalanceLevel: TcxGridLevel;
    TBToolbar2: TTBToolbar;
    TBItem5: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBControlItem3: TTBControlItem;
    TBControlItem4: TTBControlItem;
    Label4: TLabel;
    cbCompBalance: TDBComboBoxEh;
    taPaymentBANK: TFIBStringField;
    taPaymentBILL: TFIBStringField;
    prgr: TPrintDBGridEh;
    TBItem6: TTBItem;
    acBalancePrint: TAction;
    frBalance: TfrDBDataSet;
    taBalanceBD: TDateField;
    taBalanceED: TDateField;
    Panel2: TPanel;
    taPayGraphF: TpFIBDataSet;
    dsrPayGraphF: TDataSource;
    cxPageControl2: TcxPageControl;
    tshFuturePay: TcxTabSheet;
    gridPaymentFuture: TcxGrid;
    gridPaymentFutureDBTableView: TcxGridDBTableView;
    gridPaymentFutureDBTableViewColumn1: TcxGridDBColumn;
    gridPaymentFutureLevel1: TcxGridLevel;
    tshPastPay: TcxTabSheet;
    taPayGraphFD: TFIBDateTimeField;
    taPayGraphFIC: TFIBFloatField;
    taPayGraphFPC: TFIBFloatField;
    taPayGraphFIPC: TFIBFloatField;
    taPayGraphFOPC: TFIBFloatField;
    taPayGraphFPAYTYPENAME: TFIBStringField;
    taPayGraphFCOMPNAME: TFIBStringField;
    taPayGraphFCOMPSNAME: TFIBStringField;
    taPayGraphFINVNO: TFIBIntegerField;
    taPayGraphFINVDATE: TFIBDateTimeField;
    taPayGraphFINVID: TFIBIntegerField;
    taPayGraphFT: TFIBSmallIntField;
    gridPaymentFutureDBTableViewColumn2: TcxGridDBColumn;
    gridPaymentFutureDBTableViewColumn4: TcxGridDBColumn;
    gridPaymentFutureDBTableViewColumn3: TcxGridDBColumn;
    gridPaymentFutureDBTableViewColumn5: TcxGridDBColumn;
    gridPaymentFutureDBTableViewColumn6: TcxGridDBColumn;
    TBDock3: TTBDock;
    TBToolbar3: TTBToolbar;
    TBControlItem5: TTBControlItem;
    TBControlItem6: TTBControlItem;
    Label2: TLabel;
    cbCompPastPay: TDBComboBoxEh;
    TBDock4: TTBDock;
    TBToolbar4: TTBToolbar;
    TBControlItem7: TTBControlItem;
    TBControlItem8: TTBControlItem;
    Label3: TLabel;
    cbCompFuturePay: TDBComboBoxEh;
    gridPastPay: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    taPayGraphPast: TpFIBDataSet;
    dsrPayGraphPast: TDataSource;
    taPayGraphPastD: TFIBDateTimeField;
    taPayGraphPastIC: TFIBFloatField;
    taPayGraphPastPC: TFIBFloatField;
    taPayGraphPastIPC: TFIBFloatField;
    taPayGraphPastOPC: TFIBFloatField;
    taPayGraphPastPAYTYPENAME: TFIBStringField;
    taPayGraphPastCOMPNAME: TFIBStringField;
    taPayGraphPastCOMPSNAME: TFIBStringField;
    taPayGraphPastINVNO: TFIBIntegerField;
    taPayGraphPastINVDATE: TFIBDateTimeField;
    taPayGraphPastINVID: TFIBIntegerField;
    taPayGraphPastT: TFIBSmallIntField;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    cxGridDBTableView1Column4: TcxGridDBColumn;
    cxGridDBTableView1Column5: TcxGridDBColumn;
    cxGridDBTableView1Column6: TcxGridDBColumn;
    taPayGraphPastP: TFIBFloatField;
    TBSeparatorItem3: TTBSeparatorItem;
    TBControlItem9: TTBControlItem;
    Label5: TLabel;
    TBControlItem10: TTBControlItem;
    cbPaytypeBalance: TDBComboBoxEh;
    taPaymentCONTRACTCLASSID: TFIBIntegerField;
    taContractClass: TpFIBDataSet;
    taContractClassID: TFIBIntegerField;
    taContractClassNAME: TFIBStringField;
    dsContractClass: TDataSource;
    GridPaymentView: TcxGridDBTableView;
    GridPaymentLevel: TcxGridLevel;
    GridPayment: TcxGrid;
    GridPaymentViewC: TcxGridDBColumn;
    GridPaymentViewPD: TcxGridDBColumn;
    GridPaymentViewBANK: TcxGridDBColumn;
    GridPaymentViewBILL: TcxGridDBColumn;
    GridPaymentViewCONTRACTCLASS: TcxGridDBColumn;
    Localizer: TcxLocalizer;
    taPaymentCOMPANYID: TFIBIntegerField;
    GridPaymentViewCOMPANYID: TcxGridDBColumn;
    taCompany: TpFIBDataSet;
    taCompanyID: TFIBIntegerField;
    taCompanyNAME: TFIBStringField;
    taCompanySNAME: TFIBStringField;
    dsCompany: TDataSource;
    taBalanceCONTRACTCLASSNAME: TFIBStringField;
    GridBalanceViewCONTRACTCLASSNAME: TcxGridDBBandedColumn;
    taBalanceTag: TStringField;
    GridBalanceViewTag: TcxGridDBBandedColumn;
    taBalanceReport: TpFIBDataSet;
    taBalanceReportID: TFIBIntegerField;
    taBalanceReportSESSIONID: TFIBIntegerField;
    taBalanceReportCOMPID: TFIBIntegerField;
    taBalanceReportBALANCE_B: TFIBFloatField;
    taBalanceReportBALANCE_E: TFIBFloatField;
    taBalanceReportICOST: TFIBFloatField;
    taBalanceReportRCOST: TFIBFloatField;
    taBalanceReportCASHCOST: TFIBFloatField;
    taBalanceReportCOMPNAME: TFIBStringField;
    taBalanceReportCOMPSNAME: TFIBStringField;
    taBalanceReportCONTRACTCLASSNAME: TFIBStringField;
    taBalanceReportBD: TDateField;
    taBalanceReportED: TDateField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure taPaymentBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taPaymentNewRecord(DataSet: TDataSet);
    procedure cbCompChange(Sender: TObject);
    procedure taPaymentBeforeClose(DataSet: TDataSet);
    procedure acBalanceBExecute(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure cxGrid1DBBandedTableView1ICOSTPropertiesPopup(Sender: TObject);
    procedure cxGrid1DBBandedTableView1ICOSTPropertiesCloseUp(Sender: TObject);
    procedure cxGrid1DBBandedTableView1RCOSTPropertiesPopup(Sender: TObject);
    procedure cxGrid1DBBandedTableView1RCOSTPropertiesCloseUp(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CASHCOSTPropertiesPopup(Sender: TObject);
    procedure cxGrid1DBBandedTableView1CASHCOSTPropertiesCloseUp(Sender: TObject);
    procedure taBalanceBeforeOpen(DataSet: TDataSet);
    procedure cbCompBalanceChange(Sender: TObject);
    procedure gridCashKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acBalancePrintExecute(Sender: TObject);
    procedure taBalanceCalcFields(DataSet: TDataSet);
    procedure taPayGraphPastBeforeOpen(DataSet: TDataSet);
    procedure taPayGraphFBeforeOpen(DataSet: TDataSet);
    procedure cbCompFuturePayChange(Sender: TObject);
    procedure cbCompPastPayChange(Sender: TObject);
    procedure bComClick(Sender: TObject);
    procedure cbPaytypeBalanceChange(Sender: TObject);
    procedure taPaymentAfterOpen(DataSet: TDataSet);
    procedure GridPaymentViewDataControllerGroupingChanged(Sender: TObject);
    procedure acBalanceBUpdate(Sender: TObject);
    procedure GridPaymentViewInitEdit(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
    procedure GridPaymentViewMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure taBalanceTagGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure taBalanceReportBeforeOpen(DataSet: TDataSet);
    procedure taBalanceReportCalcFields(DataSet: TDataSet);
    procedure GridBalanceViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure taBalanceAfterOpen(DataSet: TDataSet);
  private
    IsBookKeeper: Boolean;
    Column: TcxGridColumn;
  end;

var
  fmPayment: TfmPayment;

implementation                               

uses dbUtil, Period, dbTree, comdata, Data, BalanceB, fmUtils, ReportData,
  BalanceInv, M207Proc;

{$R *.dfm}

procedure TfmPayment.FormCreate(Sender: TObject);
begin
  Localizer.Active := True;

  Localizer.Locale := 1049;

  Localizer.Translate;



  cxPageControl1.ActivePage := cxTabSheet2;
  ActiveControl := cxTabSheet2;
  
  cxTabSheet3.TabVisible := GetBit(dmCom.Acc, 28);
  // ������ �� �����������
  with cbComp do
  begin
    OnChange := nil;
    Items.Clear;
    Items.Assign(dm.slSup);
    ItemIndex := 0;
    OnChange := cbCompChange;
  end;
  with cbCompBalance do
  begin
    OnChange := nil;
    Items.Clear;
    Items.Assign(dm.slSup);
    ItemIndex := 0;
    OnChange := cbCompBalanceChange;
  end;
  with cbCompPastPay do
  begin
    OnChange := nil;
    Items.Clear;
    Items.Assign(dm.slSup);
    ItemIndex := 0;
    OnChange := cbCompPastPayChange;
  end;
  with cbCompFuturePay do
  begin
    OnChange := nil;
    Items.Clear;
    Items.Assign(dm.slSup);
    ItemIndex := 0;
    OnChange := cbCompFuturePayChange;
  end;
  inherited;
  dm.SetBeginDate;
  stbrStatus.SimpleText := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
  OpenDataSets([taComp]);
  OpenDataSets([taPayment]);
  ExecSQL('execute procedure WWW_CREATE_SUPBALANCE("'+DateTimeToStr(dm.BeginDate)+'", "'+DateTimeToStr(dm.EndDate)+'")', dm.quTmp);
  cbPaytypeBalance.ItemIndex:=0;
  OpenDataSet(taBalance);
  if GetBit(dmCom.Acc, 28) then
  begin
    ExecSQL('execute procedure CREATE_PAYMENTGRAPH', dm.quTmp);
    OpenDataSets([taPayGraphF, taPayGraphPast]);
  end;

  fmBalanceInv := TfmBalanceInv.Create(nil);

  taContractClass.Active := True;

  taCompany.Active := True;

  IsBookKeeper := dmCom.IsBookKeeper;

  if IsBookKeeper then
  begin
    GridPaymentView.OptionsData.Editing := True;

    GridPaymentView.OptionsSelection.CellSelect := True;
  end;
end;

procedure TfmPayment.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taComp, taPayment]);
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  fmBalanceInv.Free;
  ExecSQL('delete from TMP_BALANCE where SESSIONID=current_connection',dm.quTmp);
  if GetBit(dmCom.Acc, 28) then
    ExecSQL('delete from TMP_PAYG where SESSIONID=:SESSIONID', dm.quTmp);
  inherited;
end;

procedure TfmPayment.acPeriodExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  Bd := dm.BeginDate;
  Ed := dm.EndDate;
  if GetPeriod(Bd, Ed) then
  begin
    dm.BeginDate := Bd;
    dm.EndDate := Ed;
    stbrStatus.SimpleText := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
    Application.ProcessMessages;
    ReOpenDataSet(taPayment);
    ExecSQL('execute procedure WWW_CREATE_SUPBALANCE("'+DateTimeToStr(dm.BeginDate)+'", "'+DateTimeToStr(dm.EndDate)+'")', dm.quTmp);
    ReOpenDataSet(taBalance);
    Application.ProcessMessages;
  end;
end;

procedure TfmPayment.acAddExecute(Sender: TObject);
begin
  ActiveControl := gridPayment;

  taPayment.Append;
end;

procedure TfmPayment.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := taPayment.Active and IsBookKeeper;
end;

procedure TfmPayment.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := taPayment.Active and (not taPayment.IsEmpty) and IsBookKeeper;
end;

procedure TfmPayment.acDelExecute(Sender: TObject);
begin
  taPayment.Delete;
end;

procedure TfmPayment.taPaymentBeforeOpen(DataSet: TDataSet);
begin
  taPayment.ParamByName('BD').AsDateTime := dm.BeginDate;
  taPayment.ParamByName('ED').AsDateTime := dm.EndDate;
  if cbComp.ItemIndex = 0 then
  begin
    taPayment.ParamByName('COMPID1').AsInteger := -MAXINT;
    taPayment.ParamByName('COMPID2').AsInteger := MAXINT;
  end
  else begin
    taPayment.ParamByName('COMPID1').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
    taPayment.ParamByName('COMPID2').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
  end;
end;

procedure TfmPayment.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);

end;

procedure TfmPayment.taPaymentNewRecord(DataSet: TDataSet);
begin
  taPaymentID.AsInteger := dmCom.GetId(55);
  taPaymentC.AsFloat := 0;
  taPaymentPD.AsDateTime := Now;
end;

procedure TfmPayment.cbCompChange(Sender: TObject);
begin
  PostDataSet(taPayment);
  ReOpenDataSet(taPayment);
end;

procedure TfmPayment.taPaymentAfterOpen(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);

  GridPaymentView.DataController.Groups.FullCollapse;

  GridPaymentView.Controller.TopRowIndex := 0;

  GridPaymentView.Controller.FocusedRowIndex := 0;
end;

procedure TfmPayment.taPaymentBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TfmPayment.acBalanceBExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmBalanceB, Self, TForm(fmBalanceB), True, False);
end;

procedure TfmPayment.acBalanceBUpdate(Sender: TObject);
begin
  acBalanceB.Enabled := IsBookKeeper;
end;

procedure TfmPayment.cxPageControl1Change(Sender: TObject);
begin
 if cxPageControl1.ActivePage = cxTabSheet2 then
 begin
  Application.ProcessMessages;
  ReOpenDataSet(taPayment);
  ExecSQL('execute procedure WWW_CREATE_SUPBALANCE("'+DateTimeToStr(dm.BeginDate)+'", "'+DateTimeToStr(dm.EndDate)+'")', dm.quTmp);
  ReOpenDataSet(taBalance);
  Application.ProcessMessages;
 end
end;

procedure TfmPayment.acCloseExecute(Sender: TObject);
begin
 if not (taPayment.State in [dsEdit, dsInsert]) then close
 else taPayment.Cancel
end;

procedure TfmPayment.cxGrid1DBBandedTableView1ICOSTPropertiesPopup(
  Sender: TObject);
begin
  with fmBalanceInv do
  begin
    gr.DataSource := dsrSInv;
    OpenDataSet(taSInv);
    gr.FieldColumns['DISCRIPTION'].Visible := true;
    fmBalanceInv.Caption := '��������';
  end;
end;

procedure TfmPayment.cxGrid1DBBandedTableView1ICOSTPropertiesCloseUp(
  Sender: TObject);
begin
  CloseDataSet(fmBalanceInv.taSInv);
end;

procedure TfmPayment.cxGrid1DBBandedTableView1RCOSTPropertiesPopup(
  Sender: TObject);
begin
  with fmBalanceInv do
  begin
    gr.DataSource := dsrRInv;
    OpenDataSet(taRInv);
    gr.FieldColumns['DISCRIPTION'].Visible := false;    
    fmBalanceInv.Caption := '��������';
  end;
end;

procedure TfmPayment.cxGrid1DBBandedTableView1RCOSTPropertiesCloseUp(
  Sender: TObject);
begin
  CloseDataSet(fmBalanceInv.taRInv);
end;

procedure TfmPayment.cxGrid1DBBandedTableView1CASHCOSTPropertiesPopup(
  Sender: TObject);
begin
  with fmBalanceInv do
  begin
    gr.DataSource := dsrPayInv;
    OpenDataSet(taPayInv);
    gr.FieldColumns['SN'].Visible := false;
    gr.FieldColumns['DISCRIPTION'].Visible := false;
    fmBalanceInv.Caption := '������';
  end;
end;

procedure TfmPayment.cxGrid1DBBandedTableView1CASHCOSTPropertiesCloseUp(
  Sender: TObject);
begin
  fmBalanceInv.gr.Columns[0].Visible := true;
  CloseDataSet(fmBalanceInv.taPayInv);
end;

procedure TfmPayment.taBalanceBeforeOpen(DataSet: TDataSet);
var
  t: Integer;
begin
  t := StrToInt(cbPaytypeBalance.KeyItems[cbPaytypeBalance.ItemIndex]);

  if t = 0 then
  begin
    t := High(Integer);
  end;

  taBalance.ParamByName('t').AsInteger := t;

  if cbCompBalance.ItemIndex = 0 then
  begin
    taBalance.ParamByName('company$id').AsInteger := 0;
  end
  else begin
    taBalance.ParamByName('company$id').AsInteger := TNodeData(cbCompBalance.Items.Objects[cbCompBalance.ItemIndex]).Code;
  end;
end;

procedure TfmPayment.bComClick(Sender: TObject);
begin
  ReOpenDataSet(taBalance);
  showmessage(inttostr(dmCom.db.AttachmentID));
end;

procedure TfmPayment.cbCompBalanceChange(Sender: TObject);
begin
  ReOpenDataSet(taBalance);
end;

procedure TfmPayment.gridCashKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl])  and ((char(Key) = 'p') or (char(Key) = 'P')) then prgr.Print;
end;

procedure TfmPayment.GridPaymentViewDataControllerGroupingChanged(
  Sender: TObject);
begin
  GridPaymentView.DataController.Groups.FullCollapse;

  GridPaymentView.Controller.TopRowIndex := 0;

  GridPaymentView.Controller.FocusedRowIndex := 0;

end;


procedure TfmPayment.acBalancePrintExecute(Sender: TObject);
begin
  dmReport.PrintDocumentB(Balance);
end;

procedure TfmPayment.taBalanceCalcFields(DataSet: TDataSet);
var
  Tag: string;
begin
  With DataSet as TpFIBDataSet do
  begin
    FieldByName('BD').AsDateTime := dm.BeginDate;
    FieldByName('ED').AsDateTime := dm.EndDate;
  end;

  Tag := taBalanceCOMPSNAME.AsString;

  Tag := Tag + DupeString(' ', 20 - Length(Tag));

  Tag := Tag + taBalanceCOMPID.AsString;

  taBalanceTag.AsString := Tag;
end;

procedure TfmPayment.taBalanceReportBeforeOpen(DataSet: TDataSet);
var
  t: Integer;
begin
  t := StrToInt(cbPaytypeBalance.KeyItems[cbPaytypeBalance.ItemIndex]);

  if t = 0 then
  begin
    t := Low(Integer);
  end;

  taBalanceReport.ParamByName('t').AsInteger := t;

  if cbCompBalance.ItemIndex = 0 then
  begin
    taBalanceReport.ParamByName('company$id').AsInteger := 0;
  end
  else begin
    taBalanceReport.ParamByName('company$id').AsInteger := TNodeData(cbCompBalance.Items.Objects[cbCompBalance.ItemIndex]).Code;
  end;
end;

procedure TfmPayment.taBalanceReportCalcFields(DataSet: TDataSet);
begin
  With DataSet as TpFIBDataSet do
  begin
    FieldByName('BD').AsDateTime := dm.BeginDate;
    
    FieldByName('ED').AsDateTime := dm.EndDate;
  end;
end;

procedure TfmPayment.taBalanceTagGetText(Sender: TField; var Text: string; DisplayText: Boolean);
begin
  Text := taBalanceCOMPSNAME.AsString;
end;

procedure TfmPayment.taPayGraphPastBeforeOpen(DataSet: TDataSet);
begin
  taPayGraphPast.ParamByName('SESSIONID').AsInteger := dmCom.db.AttachmentID;
  if cbCompPastPay.ItemIndex = 0 then
  begin
    taPayGraphPast.ParamByName('COMPID1').AsInteger := -MAXINT;
    taPayGraphPast.ParamByName('COMPID2').AsInteger := MAXINT;
  end
  else begin
    taPayGraphPast.ParamByName('COMPID1').AsInteger := TNodeData(cbCompPastPay.Items.Objects[cbCompPastPay.ItemIndex]).Code;
    taPayGraphPast.ParamByName('COMPID2').AsInteger := TNodeData(cbCompPastPay.Items.Objects[cbCompPastPay.ItemIndex]).Code;
  end;
end;

procedure TfmPayment.taPayGraphFBeforeOpen(DataSet: TDataSet);
begin
  taPayGraphF.ParamByName('SESSIONID').AsInteger := dmCom.db.AttachmentID;
  if cbCompFuturePay.ItemIndex = 0 then
  begin
    taPayGraphF.ParamByName('COMPID1').AsInteger := -MAXINT;
    taPayGraphF.ParamByName('COMPID2').AsInteger := MAXINT;
  end
  else begin
    taPayGraphF.ParamByName('COMPID1').AsInteger := TNodeData(cbCompFuturePay.Items.Objects[cbCompFuturePay.ItemIndex]).Code;
    taPayGraphF.ParamByName('COMPID2').AsInteger := TNodeData(cbCompFuturePay.Items.Objects[cbCompFuturePay.ItemIndex]).Code;
  end;
end;

procedure TfmPayment.cbCompFuturePayChange(Sender: TObject);
begin
  ReOpenDataSet(taPayGraphF);
end;

procedure TfmPayment.cbCompPastPayChange(Sender: TObject);
begin
  ReOpenDataSet(taPayGraphPast);
end;

procedure TfmPayment.cbPaytypeBalanceChange(Sender: TObject);
begin
  ReOpenDataSet(taBalance);
end;

type

  TcxCustomEditAccess = class(TcxCustomEdit);

  TControlAccess = class(TControl);


procedure TfmPayment.GridPaymentViewInitEdit(Sender: TcxCustomGridTableView;  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit);
begin
  TcxCustomEditAccess(AEdit).OnMouseWheel := GridPaymentViewMouseWheel;
end;


procedure TfmPayment.GridPaymentViewMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
var
  AClassName: string;
  LookupComboBox: TcxLookupComboBox;
  DefaultMouseWheel: Boolean;
  MouseWheelEvent: TMouseWheelEvent;
begin
  with GridPaymentView.Controller.EditingController do

  if Edit <> nil then
  begin
     DefaultMouseWheel := True;

     if Edit is TcxLookupComboBox then
     begin
       LookupComboBox := TcxLookupComboBox(Edit);

       if not LookupComboBox.DroppedDown then
       begin
         DefaultMouseWheel := False;
       end;
     end;

     if DefaultMouseWheel then
     begin
        MouseWheelEvent := TcxCustomEditAccess(Edit).OnMouseWheel;

        TcxCustomEditAccess(Edit).OnMouseWheel := nil;

        TcxCustomEditAccess(Edit).DoMouseWheel(Shift, WheelDelta, MousePos);

        TcxCustomEditAccess(Edit).OnMouseWheel := MouseWheelEvent;
     end else
     begin
       HideEdit(False);

       TControlAccess(GridPaymentView.Site).DoMouseWheel(Shift, WheelDelta, MousePos);
     end;
  end;
end;

procedure TfmPayment.GridBalanceViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  if (AColumn <> nil) then
  begin
    if (AColumn.Tag <> 0) then
    begin
      GridBalanceView.DataController.Groups.FullCollapse;

      AColumn.Summary.SortByGroupSummary := True;

      GridBalanceView.Controller.TopRowIndex := 0;

      GridBalanceView.Controller.FocusedRowIndex := 0;

      Column := AColumn;
    end else
    begin
      if Column <> nil then
      begin
        Column.Summary.SortByGroupSummary := False;

        GridBalanceView.Controller.TopRowIndex := 0;

        GridBalanceView.Controller.FocusedRowIndex := 0;

        Column := nil;
      end;
    end;
  end;

end;

procedure TfmPayment.taBalanceAfterOpen(DataSet: TDataSet);
begin
  GridBalanceView.Controller.TopRowIndex := 0;

  GridBalanceView.Controller.FocusedRowIndex := 0;
end;



end.

//
