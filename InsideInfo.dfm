object fInsideInfo: TfInsideInfo
  Left = 262
  Top = 238
  Width = 783
  Height = 542
  Caption = #1042#1085#1091#1090#1088#1077#1085#1085#1103#1103' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 29
    Width = 775
    Height = 484
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsrTaSQL
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 775
    Height = 29
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acClose
      BtnCaption = #1047#1072#1082#1088#1099#1090#1100
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      Layout = blGlyphLeft
      Spacing = 1
      Left = 595
      Top = 3
      Visible = True
      OnClick = acCloseExecute
      SectionName = 'Untitled (0)'
    end
    object siOpen: TSpeedItem
      Action = acFlOpen
      BtnCaption = #1054#1090#1082#1088#1099#1090#1100
      Caption = #1054#1090#1082#1088#1099#1090#1100
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acFlOpenExecute
      SectionName = 'Untitled (0)'
    end
    object siSaveTofile: TSpeedItem
      Action = acSavetoFile
      BtnCaption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      Caption = 'siSaveTofile'
      Spacing = 1
      Left = 83
      Top = 3
      Visible = True
      OnClick = acSavetoFileExecute
      SectionName = 'Untitled (0)'
    end
  end
  object PrintDBGridEh1: TPrintDBGridEh
    DBGridEh = DBGridEh1
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 228
    Top = 300
  end
  object ActionList1: TActionList
    Left = 64
    Top = 136
    object acPrn: TAction
      Caption = 'acPrn'
      ShortCut = 16464
      OnExecute = acPrnExecute
    end
    object acFlOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      OnExecute = acFlOpenExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      OnExecute = acCloseExecute
    end
    object acSavetoFile: TAction
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1074' '#1092#1072#1081#1083
      OnExecute = acSavetoFileExecute
      OnUpdate = acSavetoFileUpdate
    end
  end
  object dsrTaSQL: TDataSource
    DataSet = taSQL
    Left = 20
    Top = 304
  end
  object taSQL: TpFIBDataSet
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 132
    Top = 304
    poSQLINT64ToBCD = True
  end
  object opnFile: TOpenDialog
    Filter = '*.sql|*.sql|*.*|*.*'
    Left = 340
    Top = 320
  end
  object svfile: TSaveDialog
    Filter = '*.*|*.*'
    Left = 176
    Top = 96
  end
end
