object fmMol: TfmMol
  Left = 279
  Top = 117
  Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072
  ClientHeight = 506
  ClientWidth = 707
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 321
    Top = 67
    Height = 439
    ExplicitHeight = 444
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 707
    Height = 42
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 60
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 643
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siadd: TSpeedItem
      Action = ActionAdd
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1072#1073#1072#1074#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = siaddClick
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7CD108CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08CF08
        CF08CF08D108AD08DF7BDF77DF73BF6FBF6B9F679F639F5F7F5F7F5B5F575F53
        5F533F4FCF08AC08FF7BDF779F675E535E53FF7FFF7F5F579F5F7F5B7F575F57
        5F533F4FAE08AC08FF7B1A5F0C0D8C318C31524AFF7FFF7F9F637F5F7F5B5F57
        5F535F53AE08AC08FF7F2D190B3EE07EE07EB27F524AFF7F9F637F5F7F5B7F5B
        5F575F53AE08AC04FF7FCB08E07E20012001E07E8C315E539F671A121A121A12
        1A125F57AE08AC04FF7FCB08F07F20012001E07E8C315E539F671A121A121A12
        1A125F57AE08AC08FF7F4D19734EFF7FE07E333E0C0D7F5FBF6B9F679F637F5F
        7F5B7F5BAE08AC04FF7F3A634D19CB08CB082D19195BBF6FBF6FBF6B9F679F63
        7F5F7F5BAE08AD08FF7FFF7FFF7FFF7FFF7BDF77DF77DF73BF6FBF6B9F679F63
        9F5F7F5FCF085311592238223822382238223822382238223822792A5922792A
        5826572E5411130999099A09990999099909990999099909BA0DBE32DC09BE32
        931D466114051F7C120D120D120D120D120D120D120D120D120D130DF208130D
        F10CF0101F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086'|'
      ImageIndex = 2
      Spacing = 1
      Left = 63
      Top = 3
      Visible = True
      OnClick = siDelClick
      SectionName = 'Untitled (0)'
    end
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 67
    Width = 321
    Height = 439
    Align = alLeft
    AllowedOperations = [alopUpdateEh]
    Color = clBtnFace
    ColumnDefValues.Title.Alignment = taCenter
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmServ.dsMol
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clNavy
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = dg1GetCellParams
    Columns = <
      item
        Color = clInfoBk
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1060#1048#1054' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086'-'#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1075#1086' '#1083#1080#1094#1072
        Width = 276
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object pdescription: TPanel
    Left = 324
    Top = 67
    Width = 383
    Height = 439
    Align = alClient
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 72
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 72
      Width = 58
      Height = 13
      Caption = #1044#1086#1083#1078#1085#1086#1089#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LCBDepid: TDBLookupComboboxEh
      Left = 120
      Top = 24
      Width = 257
      Height = 19
      DataField = 'D_DEPID'
      DataSource = dmServ.dsMol
      EditButtons = <>
      Flat = True
      KeyField = 'D_DEPID'
      ListField = 'NAME'
      ListSource = dmCom.dsDep
      TabOrder = 0
      Visible = True
      OnDropDownBoxGetCellParams = LCBDepidDropDownBoxGetCellParams
      OnExit = LCBDepidExit
      OnKeyValueChanged = LCBDepidKeyValueChanged
    end
    object DBEditEh1: TDBEditEh
      Left = 120
      Top = 72
      Width = 257
      Height = 19
      DataField = 'POST'
      DataSource = dmServ.dsMol
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
    end
    object gr: TGroupBox
      Left = 8
      Top = 128
      Width = 369
      Height = 121
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1086#1085#1085#1072#1103' '#1082#1086#1084#1080#1089#1089#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object cbChairMan: TDBCheckBoxEh
        Left = 16
        Top = 56
        Width = 329
        Height = 17
        Caption = #1087#1088#1077#1076#1089#1077#1076#1072#1090#1077#1083#1100' '#1082#1086#1084#1080#1089#1089#1080#1080
        DataField = 'IS$CHAIRMAN'
        DataSource = dmServ.dsMol
        TabOrder = 0
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object cbMember: TDBCheckBoxEh
        Left = 16
        Top = 88
        Width = 329
        Height = 17
        Caption = #1095#1083#1077#1085' '#1082#1086#1084#1080#1089#1089#1080#1080
        DataField = 'IS$MEMBER'
        DataSource = dmServ.dsMol
        TabOrder = 1
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object cbMol: TDBCheckBoxEh
        Left = 16
        Top = 24
        Width = 329
        Height = 17
        Caption = #1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086' '#1086#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1086#1077' '#1083#1080#1094#1086
        DataField = 'IS$MOL'
        DataSource = dmServ.dsMol
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
  end
  object SpeedBar1: TSpeedBar
    Left = 0
    Top = 42
    Width = 707
    Height = 25
    Hint = #1043#1086#1088#1103#1095#1080#1077' '#1082#1085#1086#1087#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 60
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 3
    InternalVer = 1
    object Label3: TLabel
      Left = 8
      Top = 8
      Width = 43
      Height = 13
      Caption = #1060#1080#1083#1100#1090#1088':'
      Transparent = True
    end
    object cmbxFilterDep: TDBComboBoxEh
      Left = 58
      Top = 4
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxFilterDepChange
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
  end
  object ActionList1: TActionList
    Left = 104
    Top = 224
    object ActionAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = siaddClick
      OnUpdate = ActionAddUpdate
    end
  end
end
