unit DInv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, RXDBCtrl, StdCtrls, Mask,
  Grids, DBGrids, M207Grid, M207IBGrid, db, Menus, RxMenus, DateUtils,
  Variants, ComDrv32, Buttons, DBCtrlsEh, rxPlacemnt, rxToolEdit,
  rxSpeedbar, ActnList, FIBDataSet, pFIBDataSet;

type
  TfmDInv = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siAdd: TSpeedItem;
    siDel: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    laCost: TLabel;
    DBTextTo: TDBText;
    edSN: TDBEdit;
    fs: TFormStorage;
    pa2: TPanel;
    Panel3: TPanel;
    Panel5: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    Splitter6: TSplitter;
    DBText2: TDBText;
    DBText3: TDBText;
    dgEl: TM207IBGrid;
    pmEl: TPopupMenu;
    N1: TMenuItem;
    pmWH: TPopupMenu;
    N2: TMenuItem;
    siUID: TSpeedItem;
    N3: TMenuItem;
    siCloseInv: TSpeedItem;
    dgWH: TM207IBGrid;
    Label3: TLabel;
    DBText4: TDBText;
    siUID2: TSpeedItem;
    N4: TMenuItem;
    laCostD: TLabel;
    dbtCostD: TDBText;
    tb2: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    Label2: TLabel;
    edUID: TEdit;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    Splitter2: TSplitter;
    lbcountry: TListBox;
    Splitter1: TSplitter;
    SpeedItem2: TSpeedItem;
    DBCheckBox1: TDBCheckBox;
    laCost0: TLabel;
    DBTextFr: TDBText;
    siPrint: TSpeedItem;
    ppPrint: TRxPopupMenu;
    Splitter7: TSplitter;
    lbAtt1: TListBox;
    Splitter8: TSplitter;
    lbAtt2: TListBox;
    dbSdate: TDBEditEh;
    btdate: TBitBtn;
    siHelp: TSpeedItem;
    Label7: TLabel;
    aclisst: TActionList;
    acClose: TAction;
    acOpen: TAction;
    acCreateInvFromPrord: TAction;
    acCreateInvAll: TAction;
    acCretaeOpt: TAction;
    acCreateInvFromInv: TAction;
    dsrDlist_H: TDataSource;
    taHist: TpFIBDataSet;
    taHistHISTID: TFIBIntegerField;
    taHistDOCID: TFIBIntegerField;
    taHistFIO: TFIBStringField;
    taHistHDATE: TFIBDateTimeField;
    taHistSTATUS: TFIBStringField;
    taHistTYPEDOC: TFIBSmallIntField;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure siAddClick(Sender: TObject);
    procedure siDelClick(Sender: TObject);
    procedure dgElGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure siUIDClick(Sender: TObject);
    procedure siCloseInvClick(Sender: TObject);
    procedure dgElEditButtonClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgWHGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siUID2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem2Click(Sender: TObject);
    procedure deSDateChange(Sender: TObject);
    procedure deSDateButtonClick(Sender: TObject);
    procedure btdateClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
    StopFlag: boolean;
    nSaveDep1,nSaveDep2:integer;
    LogOprIdForm:string;
    Flcreate:boolean;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    function  Stop: boolean;
    procedure PrintTagDinv(Sender: TObject);
  public
    { Public declarations }
  end;

var
  fmDInv: TfmDInv;
  WMode: string[255];
implementation

uses comdata, Data, DBTree, DItem, Data2, Dst, M207Proc, ReportData,
  ServData, Data3, SetSDate, dbUtil, MsgDialog, Hist, DList;

{$R *.DFM}

procedure TfmDInv.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmDInv.PrintTagDinv(Sender: TObject);
var arr : TarrDoc;
begin
  try
    gen_arr(arr, dgEl, dm.taSElSELID);
    PrintTag(arr, s_tag,TMenuItem(Sender).Tag);
  finally
    Finalize(arr);
  end;
end;

procedure TfmDInv.FormCreate(Sender: TObject);
var i: integer;
    c: TColumn;
begin
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  Flcreate:=false;
  with dm do
    begin
     nSaveDep1:=SDepId;     nSaveDep2:=DDepFromId;
      Old_D_MatId:='.';
      D_WHArt:='';
      DDepFromId:= taDList.FieldByName('DEPFROMID').asInteger;
      sWorkInvID:='*'+taDListSINVID.AsString+';';
      OpenDataSets([taSEl]);
      dgEl['PRICE'].Title.Caption:='���� - '+taDListDepFrom.AsSTring;    // � ������ ������
      dgEl['EP2'].Title.Caption:='���� - '+taDListDepTo.AsSTring;  // �� �����
      laCost.Caption:='����� - '+taDListDepTo.AsSTring+':';
      laCost0.Caption:='����� - '+taDListDepFrom.AsSTring+':';


       if not CenterDep then
       begin
        dgEl['PRICE'].Visible:=DDepFromId=Selfdepid;
        dgEl['PRICE'].Color:=clInfoBk;
        laCost0.Visible:=DDepFromId=Selfdepid;
        DBTextFr.Visible:=DDepFromId=Selfdepid;
        dgEl['EP2'].Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
        laCost.Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
        DBTextTo.Visible:=taDList.FieldByName('DEPID').asInteger=Selfdepid;
        DBText4.Visible:=False;
        Label3.Visible:=False;
        dgEl.ColumnByName['SSUMD'].Visible:=False;
       end
    end;


  dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbCountry, lbAtt1, lbAtt2);

  lbComp.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbGood.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  lbCountry.ItemIndex:=0;
  lbAtt1.ItemIndex := 0;
  lbAtt2.ItemIndex := 0;
  dmcom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmcom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmcom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmcom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmcom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmcom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmcom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;

//  lbCompClick(NIL);
  dm.SetCloseInvBtn(siCloseInv, dm.taDListIsClosed.AsInteger);
  dm.DstDepId:=dm.taDListDepFromId.AsInteger;
  if(dm.taDListURF_FROM.AsInteger = 1)or(dm.taDListURF_TO.AsInteger = 1) then
  begin
    laCostD.Visible:=True;
    dbtCostD.Visible:=True;
  end
  else begin
    laCostD.Visible:=False;
    dbtCostD.Visible:=False;
  end;

  with dm, dm2 do
    begin
      for i:=0 to slDepDepId.Count-1 do
        begin
         c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RW_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+' - ���';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

          c:=dgWH.Columns.Add;
          c.Field:=quD_WH.FieldByName('RQ_'+slDepDepId[i]);
          c.Title.Caption:=slDepSName[i]+' - �-��';
          c.Title.Alignment:=taCenter;
          c.Title.Font.Color:=clNavy;
          c.Width:=80;

        end;
    end;

  dm.ClosedInvId:=-1;
  if not CenterDep then begin
   dgel.ColumnByName['SSUM'].Visible:=false;
//   if dm.taDListDEPFROMID.AsInteger = SelfDepId then siPrint.Visible:=false;
  end;

{ if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
 if dmcom.ScanComZ='COM1' then dmserv.ComScan.ComPort:=pnCOM1
  else  if dmcom.ScanComZ='COM2' then dmserv.ComScan.ComPort:=pnCOM2
   else  if dmcom.ScanComZ='COM3' then dmserv.ComScan.ComPort:=pnCOM3
    else  dmserv.ComScan.ComPort:=pnCOM4;

 dmServ.ComScan.Connect;
 dmcom.SScanZ:='';     }

 dmcom.FillTagMenu(ppPrint, PrintTagDinv, 0);
 LogOprIdForm:=dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmDInv.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
WMode:='';
  with dm, dmCom do
    begin
      PostDataSets([taDList, taSEl ]);
      tr.CommitRetaining;
      SDepId:=nSaveDep1;     DDepFromId:=nSaveDep2;
      ReOpenDataSets([taSEl]);
      taDList.Refresh;
      if taDListSINVID.IsNull then begin
       MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
       ReOpenDataSet(taDList);
      end else if not dm.closeinv(dm.taDListSINVID.AsInteger,0) then
      begin
       if dm.EmptyInv(dm.taDListSINVID.AsInteger,0) then
       begin
        ExecSQL('delete from sinv where sinvid='+dm.taDListSINVID.AsString, dm.quTmp);
        ReOpenDataSet(dm.taDList);
       end
      end;
      CloseDataSets([taSEl, quD_WH]);
      CloseDataSets([taComp, taMat, taGood, taArt, taIns]);
      tr.CommitRetaining;
      taDList.Refresh;
    end;

// if dmServ.ComScan.Connected then dmServ.ComScan.Disconnect;
end;

procedure TfmDInv.siExitClick(Sender: TObject);
begin
 Close;
end;

procedure TfmDInv.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id := TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id := TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
  ReOpenDataSets([dm.quD_WH]);
end;

procedure TfmDInv.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmDInv.siAddClick(Sender: TObject);
var i:integer;
    FieldsRes:t_array_var;
    LogOperationID:String;
begin
 i := 0;
  with dm, taSEl do
    begin
     try
       if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
          raise Exception.Create('������ ������������� ��������� � ������� '#39 +
             dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������');

      dmcom.ExecuteQutmp(quTmp,'  select uidwhcalc  from d_rec ',1, FieldsRes);
      if FieldsRes[0]=null then i:=0
      else i:=FieldsRes[0];
     finally
      Finalize(FieldsRes);
     end;
      If i = 1 then Raise Exception.Create('��������� ������ �����������');

      LogOperationID:=dm3.insert_operation('�������� ������� � ��������� ��',LogOprIdForm);
      if NOT Locate('ART2ID', quD_WHArt2Id.AsInteger, []) then
        begin
          Append;
          taSElArt2Id.AsInteger:=quD_WHArt2Id.AsInteger;
          Post;
        end;
      dm2.NewSinvID:=0;
      siUIDClick(NIL);
      dm3.update_operation(LogOperationID);
    end;
end;

procedure TfmDInv.siDelClick(Sender: TObject);
var LogOperationID:string;
begin
 if not dm.checkInv_RUseDep(dm.taDListDEPFROMID.AsInteger) then MessageDialog('������ ������������� ��������� � ������� '#39 +
                 dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName  + #39' �� ��������� ���� ������', mtInformation, [mbOK], 0)
 else
 begin
  LogOperationID:=dm3.insert_operation('������� ������� �� ���������',LogOprIdForm);
  dm.taSEl.Delete;
  dm3.update_operation(LogOperationID);
 end;  
end;

procedure TfmDInv.dgElGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Field<>NIL) and
    ((Field.FieldName='ART') or
    (Field.FieldName='ART2') or
    (Field.FieldName='PRODCODE') or
    (Field.FieldName='D_MATID') or
    (Field.FieldName='D_GOODID') or
    (Field.FieldName='D_COUNTRYID') or
    (Field.FieldName='D_INSID')) then
    Background:=dmCom.clMoneyGreen;
end;

procedure TfmDInv.siUIDClick(Sender: TObject);
var art2idd: integer;
    LogOperationID:string;
begin
  with dm do
    begin
      if taSElSElID.IsNull then exit;
//      ShowAndFreeForm(TfmDItem, Self, TForm(fmDItem), True, False);
      DstOne2Many:= False;
      DstEl:=True;
      DstArt2Id:= taSElArt2Id.AsInteger;
      art2idd:=DstArt2Id;
      quDst.selectsql[8]:=' ';
      quDst.selectsql[12]:=' and e.Sinvid='+inttostr(taDListSINVID.AsInteger);

      LogOperationID:=dm3.insert_operation('��. ������',LogOprIdForm);
      ShowAndFreeForm(TfmDst, Self, TForm(fmDst), True, False);
      dm3.update_operation(LogOperationID);

      quDst.selectsql[12]:='';
      quDst.selectsql[8]:=' si.BusyType=0 and ';
      with taSEl do
        begin
          Active:=False;
          Open;
          Locate('ART2ID', DstArt2Id, []);
        end;

      if taSElQuantity.AsFloat=0 then
        with taSEl do
          begin
            Tag:=1;
            Delete;
            Tag:=0;
          end;
      if quD_WH.Active then
      begin
       qud_wh.Locate('ART2ID',art2idd,[]);
       qud_wh.Refresh;
      end;
      taDList.Refresh;
      ActiveControl:=ceArt;
      edUID.Text:='';
      ceArt.Text:=''
    end;
end;

procedure TfmDInv.siCloseInvClick(Sender: TObject);
var
  sn:string;
  v: Variant;
begin
If dm.taDListISCLOSED.AsInteger=1 then
begin
   v := dmCom.db.QueryValue('select sinvid from dinv where sinvid = :sinvid', 0, [dm.taDListSINVID.AsInteger]);

   if not VarIsNull(v) then
   begin
     Application.MessageBox('��������� �� ����� �������. ��� ��������.', '���������', 0);
     exit;
   end else
   begin
     ShowAndFreeForm(TfmHist, Self, TForm(fmHist), True, False);
   end;
end
 else
 begin
  {�������� ������������ ������� ��������� � �������� �����}
   with dm.quTmp do
   begin
      close;
      SQL.Text:='select str_pr from CHECK_CLOSE_Sinv2Prod ('+dm.taDListSINVID.AsString +')';
      ExecQuery;
      if not Fields[0].IsNull then sn:=trim(Fields[0].AsString)
         else sn:='';
      Transaction.CommitRetaining;
      close;
   end;
 end;
  if (sn='') then
  begin
//   acClose.Execute;
   if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
   begin
     acClose.Execute;
     if not dm.pCreatePrord(dm.taDListSINVID.AsString) then
     begin
       with dm.quTmp do
       begin
         close;
         SQL.Text:='update sinv set isclosed=0, userid = null where sinvid='+dm.taDListSINVID.AsString;
         ExecQuery;
         Transaction.CommitRetaining;
         close;
       end;
       dm.quDInvItem.Refresh;
       dm.SetCloseInvBtn(siCloseInv, 0);

       exit;// �������� ������� � ���������, � ������� �������� ����.���� ������ � �������
     end; // �������� ������� � ���������, � ������� �������� ����.���� ������ � �������
   end;
  end
   else
   Application.MessageBox(pchar(sn),'��������!!',0);
end;

procedure TfmDInv.dgElEditButtonClick(Sender: TObject);
begin
  with dgEl.SelectedField do
    if (FieldName='TOTALWEIGHT') or (FieldName='QUANTITY') then siUIDClick(NIL);
end;

procedure TfmDInv.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;
end;

procedure TfmDInv.dgWHGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if NOT Highlight or dgWH.ClearHighlight then
    if Field.FieldName='PRICE2' then
      case dm.quD_WHPEQ.AsInteger of
          0: Background:=clRed;
          1: Background:=clInfoBk;
        end
    else
      if (Field.FieldName='DQ') or (Field.FieldName='DW') then Background:=dmCom.clCream;
end;

procedure TfmDInv.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var SElId, T,i: integer;
    FieldsRes:t_array_var;
begin
  if Key=VK_RETURN then
    with dm, dm2, dmCom do
      begin
        if not checkInv_RUseDep(taDListDEPFROMID.AsInteger) then
             raise Exception.Create('������ ������������� ��������� � ������� '#39 +
                 dmCom.Dep[dm.taDListDEPFROMID.AsInteger].SName + #39' �� ��������� ���� ������');

        PostDataSets([taDList]);
        dmCom.tr.CommitRetaining;
       try
        dmcom.ExecuteQutmp(quTmp,'  select uidwhcalc  from d_rec ',1, FieldsRes);
        if FieldsRes[0]=null then i:=0 else i:=FieldsRes[0];
       finally
        Finalize(FieldsRes);
       end;
       If i = 1 then Raise Exception.Create('��������� ������ �����������');


        if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� �������!!!');

        with quInsDUID, Params do
          begin
            ByName['UID'].AsInteger:=StrToInt(edUID.Text);
            ByName['SINVID'].AsInteger:=taDListSInvId.AsInteger;
            ByName['DEPID'].AsInteger:=taDListDepId.AsInteger;
            ByName['DEPFROMID'].AsInteger:=taDListDepFromId.AsInteger;
            ByName['OPT'].AsInteger:=0;
            ExecQuery;

            SElId:=FieldByName('SELID').AsInteger;
            T:=FieldByName('T').AsInteger;
            Close;
          end;
        case T of
            0: begin

                 if taSEl.Locate('SELID', SElID, []) then taSEl.Refresh;
                 taDList.Refresh;
               end;
            1: begin
                 ReOpenDataSets([taSEl]);
                 taSEl.Locate('SELID', SElId, []);
               end;
            2: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);
            3: MessageDialog('������� �������', mtInformation, [mbOK], 0);
            4: MessageDialog('������� �� �������', mtInformation, [mbOK], 0);            
          end;

        edUID.Text:='';
        ceArt.Text:='';
        dmCom.tr.CommitRetaining;
        taDList.Refresh;
      end;
end;

procedure TfmDInv.siUID2Click(Sender: TObject);
var LogOperationID:string;
begin
  with dm do
    begin
      if taSElSElID.IsNull then exit;
      LogOperationID:=dm3.insert_operation('�������',LogOprIdForm);
      ShowAndFreeForm(TfmDItem, Self, TForm(fmDItem), True, False);
      dm3.update_operation(LogOperationID);
      DstOne2Many:= False;
      DstArt2Id:= taSElArt2Id.AsInteger;
      with taSEl do
        begin
          Active:=False;
          Open;
          Locate('ART2ID', DstArt2Id, []);
        end;

      if taSElQuantity.AsFloat=0 then
        with taSEl do
          begin
            Tag:=1;
            Delete;
            Tag:=0;
          end;
      taDList.Refresh;
      ActiveControl:=ceArt;
      edUID.Text:='';
      ceArt.Text:=''
    end;
end;

procedure TfmDInv.SpeedItem1Click(Sender: TObject);
begin
  with dmCom, dm do
       begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_CountryId:=D_CountryId;
         Old_D_Att1Id := D_Att1Id;
         Old_D_Att2Id := D_Att2Id;
         D_WHArt:=ceArt.Text;
         Screen.Cursor:=crSQLWait;
         ReopenDataSets([quD_WH]);
         Screen.Cursor:=crDefault;
       end;
end;

procedure TfmDInv.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmDInv.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if cbSearch.Checked then
    begin
      StopFlag:=False;
      with dm, quD_WH do
        if Active then LocateF(dm.quD_WH, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
    end;
end;

procedure TfmDInv.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then  ceArt.SelectAll
                     else if cbSearch.Tag=0 then
                            begin
                              dm.D_WHArt:=ceArt.Text;
                              ReopenDataSets([dm.quD_WH]);
                              cbSearch.Tag:=1;
                            end
                          else
                            begin
                              ceArt.SelectAll;
                              cbSearch.Tag:=0;
                            end;
                   end;
      VK_ESCAPE: StopFlag:=True;
    end;
end;

procedure TfmDInv.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then ActiveControl:=dgWH;
end;

function TfmDInv.Stop: boolean;
begin
  Result:=StopFlag;
end;


procedure TfmDInv.SpeedItem2Click(Sender: TObject);
begin
 //
end;

procedure TfmDInv.deSDateChange(Sender: TObject);
var AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond,
    AYear1, AMonth1, ADay1, AHour1, AMinute1, ASecond1, AMilliSecond1:word;
    st:TDataSetState;
begin
 if Flcreate then
 begin
  DecodeDateTime(dmcom.GetServerTime, AYear, AMonth, ADay, AHour, AMinute, ASecond, AMilliSecond);
  DecodeDateTime(dm.taDListSDATE.AsDateTime, AYear1, AMonth1, ADay1, AHour1, AMinute1, ASecond1, AMilliSecond1);
  st:=dm.taDList.State;
  dm.taDList.Edit;
  dm.taDListSDATE.AsDateTime:=EncodeDateTime(AYear1, AMonth1, ADay1, AHour, AMinute, ASecond, AMilliSecond);
  dm.taDList.Post;
  if st in [dsedit, dsinsert] then dm.taDList.edit;
  Flcreate:=false;  
 end;
end;

procedure TfmDInv.deSDateButtonClick(Sender: TObject);
begin
 Flcreate:=true;
end;

procedure TfmDInv.acCloseExecute(Sender: TObject);
var sn:string;
    FieldsRes:t_array_var;
    LogOperationID, s_close, s_closed:string;
begin
  fmDList.taHist.Active:=true;
  taHist.Open;
  with taHist do
  begin
    fmDList.taHist.Append;
    fmDList.taHistDOCID.AsInteger := dm.taDListSINVID.AsInteger;
    If (Hist.log='') then fmDList.taHistFIO.Value := dmCom.User.Alias
       else fmDList.taHistFIO.Value :=Hist.log;
    fmDList.taHistSTATUS.Value := '�������';
    fmDList.taHist.Post;
    taHist.Transaction.CommitRetaining;
  end;
  taHist.Close;
  fmDList.taHist.Active:=false;
  with dm do
    begin
      if taDListIsClosed.AsInteger=0 then
        begin
          if taDListCLTID.AsInteger=1 then raise Exception.Create('������ ������� ���������. ��������� �� �� ������.');
          PostDataSets([taSEl, taDList]);
          CheckDInvN;
              taDList.Refresh;
              if taDListIsClosed.AsInteger=1 then raise Exception.Create('��������� ���� ������� ������ �������������');
              Screen.Cursor:=crSQLWait;
             {�������� �������� ��������, ��������� �����}
              try
            //    if CenterDep then
             //      dmcom.ExecuteQutmp(quTmp,'select str_pr from CHECK_CLOSE_SinvVP_Centr ('+taDListSINVID.AsString +')',1, FieldsRes) else
                   dmcom.ExecuteQutmp(quTmp,'select str_pr from CHECK_CLOSE_SinvVP ('+taDListSINVID.AsString +')',1, FieldsRes);
               if FieldsRes[0]=null then sn:=''
               else sn:=trim(FieldsRes[0]);
              finally
                Finalize(FieldsRes);
              end;

             if sn = '' then
             begin
              try
               with quTmp do
                begin
                {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
                 SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+taDListSINVID.AsString+')';
                 ExecQuery;
                 s_close:=trim(Fields[0].AsString);
                 s_closed:=trim(Fields[1].AsString);
                 Transaction.CommitRetaining;
                 close;
                end;
               if (s_close='') and (s_closed='') then
               begin
                Screen.Cursor:=crSQLWait;
                LogOperationID:=dm3.insert_operation('������� ��������� ��',LogOprIdForm);
                dm.SetSocketClose:=true;
                dmcom.ExecuteQutmp(quTmp,'SELECT R_STR from CloseInv('+taDListSInvId.AsString+', 2, '
                                   +IntToStr(dmCom.UserId)+')',1, FieldsRes);
                if not(FieldsRes[0]=null) then
                  MessageDialog('��������� � '+taDListSN.AsString+' ������������� � ��� ��������� '+FieldsRes[0], mtWarning, [mbOK], 0);
                Screen.Cursor:=crDefault;
               end
               else begin
                if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
                if s_closed<>'' then  MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
              end;
              finally
                Finalize(FieldsRes);
                dm3.update_operation(LogOperationID);
              end;
             end
               else Application.MessageBox(pchar(sn),'��������!!',0);
//**/

              taDList.Refresh;

 //*******/
{              ClosedInvId:=taDListSInvId.AsInteger;
              ReopenDatasets([taDList]);
              taDList.Locate('SINVID', ClosedInvId, []);}
//              taDList.Refresh;
              Screen.Cursor:=crDefault;
              if (sn = '') and (s_close='') and (s_closed='') then  SetCloseInvBtn(siCloseInv, 1);
            end;
        end
    end;

procedure TfmDInv.acOpenExecute(Sender: TObject);
var sn:string;
    FieldsRes:t_array_var;
    LogOperationID, s_close, s_closed:string;
begin
with dm do
begin
  Screen.Cursor:=crSQLWait;
            try
             dmcom.ExecuteQutmp(quTmp,'EXECUTE PROCEDURE OpenInv('+taDListSInvId.AsString+', 2, '+IntToStr(dmCom.UserId) +')',0, FieldsRes);
            finally
             Finalize(FieldsRes);
            end;
            taDList.Refresh;
            SetCloseInvBtn(siCloseInv, 0);
            Screen.Cursor:=crDefault;
end;
end;

procedure TfmDInv.btdateClick(Sender: TObject);
var d:tdatetime;
begin
  if dm.taDListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm.taDListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm.taDListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm.taDList.Edit;
    dm.taDListSDATE.AsDateTime:=d;
    dm.taDList.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmDInv.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100223);
end;

procedure TfmDInv.FormActivate(Sender: TObject);
begin
  WMode:='DINV';
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
end;

end.

