unit Repair;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DBCtrlsEh, M207DBCtrls, StdCtrls, Buttons,
  DBCtrls, Mask, ExtCtrls, ActnList, M207Ctrls, jpeg,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmRepair = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siCloseInv: TSpeedItem;
    siRecalc: TSpeedItem;
    siHelp: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    dbSup: TDBText;
    Label3: TLabel;
    Label7: TLabel;
    edSN: TDBEdit;
    lcComp: TDBLookupComboBox;
    dbedSdate: TDBEditEh;
    bttime: TBitBtn;
    edPTr: TM207DBEdit;
    edTrans: TM207DBEdit;
    edPTrNDS: TM207DBEdit;
    DBEdit1: TM207DBEdit;
    chbxRet_NoNDS: TDBCheckBoxEh;
    dg1: TDBGridEh;
    acList: TActionList;
    acRecalc: TAction;
    plUid: TPanel;
    LUid: TLabel;
    edUid: TEdit;
    acClose: TAction;
    fr1: TM207FormStorage;
    acDel: TAction;
    acCloseInv: TAction;
    siOpenInv: TSpeedItem;
    acOpenInv: TAction;
    acHelp: TAction;
    procedure acRecalcExecute(Sender: TObject);
    procedure acRecalcUpdate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure edUidKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acDelExecute(Sender: TObject);
    procedure acCloseInvUpdate(Sender: TObject);
    procedure acCloseInvExecute(Sender: TObject);
    procedure bttimeClick(Sender: TObject);
    procedure edPTrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edTransKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edPTrNDSKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acOpenInvUpdate(Sender: TObject);
    procedure acOpenInvExecute(Sender: TObject);
    procedure edUidKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
   ExistsNum:boolean;
  public
    { Public declarations }
  end;

var
  fmRepair: TfmRepair;

implementation

uses comdata, data, data3, M207Proc, Data2, dbUtil, FIBQuery, SetSDate, DB, MsgDialog;

{$R *.dfm}

procedure TfmRepair.acRecalcExecute(Sender: TObject);
begin
  PostDataSets([dm3.taRepair, dm3.taRepairList]);
  ExecSQL('execute procedure RECALCINV_REPAIR('+dm3.taRepairListSINVID.AsString+')', dm.quTmp);
  dm3.taRepairList.Refresh;
  ReOpenDataSet(dm3.taRepair);
end;

procedure TfmRepair.acRecalcUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (dm3.taRepairListISCLOSED.AsInteger=0)
end;

procedure TfmRepair.acCloseExecute(Sender: TObject);
begin
 close;
end;

procedure TfmRepair.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.taRepairREPAIRID.IsNull) and
   (dm3.taRepairListISCLOSED.AsInteger=0)
end;

procedure TfmRepair.FormCreate(Sender: TObject);
var i:integer;
begin
  tb1.WallPaper:=wp;
  with dmCom, dm3, dm2, dm do
  begin
   CloseDataSets([taRet,taRepair, quSupR]);
   OpenDataSets([taRet,taRepair, quSupR]);
  end;

 {***********����������� ������*********}
  if dm3.taRepairListISCLOSED.AsInteger=1 then ExistsNum:=true
  else ExistsNum:= not dm3.taRepairListSN.IsNull;
  if not ExistsNum then
  with dm, quTmp do begin
   close;
   SQL.Text:='SELECT max(sn) FROM SINV '+
             'WHERE ITYPE=9 '+
             ' AND DepFromId='+dm3.taRepairListDEPFROMID.AsString+' AND FYEAR(SDATE)='+GetSYear(dm3.taRepairListSDATE.AsDateTime);
   ExecQuery;
   if Fields[0].IsNull then i:=1
   else i:=Fields[0].AsInteger+1;
   Close;
   Transaction.CommitRetaining;
   dm3.taRepairList.Edit;
   dm3.taRepairListSN.AsInteger:=i;
   dm3.taRepairList.Post;
   dm3.taRepairList.Refresh;
  end;
 {**************************************}
  SetCBDropDown(lcComp);
  dm.SetVisEnabled(TWinControl(dbSup),dm3.taRepairListISCLOSED.AsInteger>0);
  dm.SetVisEnabled(TWinControl(lcComp),dm3.taRepairListISCLOSED.AsInteger=0);
  dm.quSupR.Locate('D_COMPID', dm3.taRepairListCOMPID.AsInteger, []);
  edUID.Text:='';
  ActiveControl:=edUid;
end;

procedure TfmRepair.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 PostDataSets([dm3.taRepairList, dm3.taRepair]);

 dm3.taRepairList.Refresh;
 if dm3.taRepairListSINVID.IsNull then begin
   MessageDialog('��������� �������!!!', mtInformation, [mbOK], 0);
   ReOpenDataSet(dm3.taRepairList);
 end else begin
  ReOpenDataSets([dm3.taRepair]);
  if not dm.closeinv(dm3.taRepairListSINVID.AsInteger,0) then
  begin
   if dm.EmptyInv(dm3.taRepairListSINVID.AsInteger,3) then begin
    if not dm3.taRepairListSINVID.IsNull then
     ExecSQL('delete from sinv where sinvid='+dm3.taRepairListSINVID.AsString, dm.quTmp);
    ReOpenDataSet(dm3.taRepairList);
   end else begin
    if MessageDialog('��������� �� �������!!!', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
    if (not ExistsNum) and (not dm3.taRepairListSINVID.IsNull) then
    begin
     ExecSQL('update sinv set sn=null where sinvid='+dm3.taRepairListSINVID.AsString, dm.quTmp);
     dm3.taRepairList.Refresh;
    end;
   end
  end;
  dm3.taRepairList.Refresh;
 end;
 
 CloseDataSets([dm3.taRepair, dm.quSupR, dmCom.taRet]);
 dmCom.tr.CommitRetaining;
end;

procedure TfmRepair.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRepair.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmRepair.edUidKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: begin
   if edUid.Text='' then raise Exception.Create('������� ����� �������');
   PostDataSet(dm3.taRepairList);
   dm3.taRepairList.Refresh;
   if dm3.taRepairListSINVID.IsNull then begin eduid.Text:=''; raise Exception.Create('��������� �������!!!') end;
   if dm3.taRepairListCOMPID.IsNull then begin eduid.Text:=''; raise Exception.Create('������� ����������') end;
   with dm, dm3 do begin
    quTmp.Close;
    quTmp.SQL.Text:='select fres, sitemid, respstoringid, d_retid from Select_UID_REPAIR ('+edUid.Text+','+dm3.taRepairListCOMPID.AsString+')';
    qutmp.ExecQuery;
    if qutmp.Fields[0].AsInteger=1 then MessageDialog('������� ��������� �� �� ������������ �������� � �� �� ������', mtWarning, [mbOk], 0)
    else if qutmp.Fields[0].AsInteger=2 then MessageDialog('������� ��������� � ��������� �� ������', mtWarning, [mbOk], 0)
    else if qutmp.Fields[0].AsInteger=3 then MessageDialog('������� ������� ����������', mtWarning, [mbOk], 0)    
    else begin
     taRepair.Append;
     taRepairSINVID.AsInteger:=taRepairListSINVID.AsInteger;
     taRepairUID.AsInteger:=strtoint(edUid.Text);
     taRepairSITEMID.AsInteger:=quTmp.Fields[1].AsInteger;
     if not quTmp.Fields[2].IsNull then begin
      taRepairRESPSTORINGID.AsInteger:=quTmp.Fields[2].AsInteger;
      taRepairD_RETID.AsString:=quTmp.Fields[3].AsString;
     end;
     taRepair.Post;
     taRepair.Refresh;
    end;
    qutmp.Close;
    quTmp.Transaction.CommitRetaining;
    eduid.Text:='';
    ActiveControl:=edUid;
   end;
  end
 end;
end;

procedure TfmRepair.acDelExecute(Sender: TObject);
begin
 dm3.taRepair.Delete;
end;

procedure TfmRepair.acCloseInvUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (dm3.taRepairListISCLOSED.AsInteger=0)
end;

procedure TfmRepair.acCloseInvExecute(Sender: TObject);
var i:integer;
    s_close, s_closed:string;
begin
  PostDataSets([dm3.taRepairList, dm3.taRepair]);
  with dm, quTmp do  begin
   if dm3.taRepairListSN.IsNull then  begin
    close;
    SQL.Text:='SELECT max(sn) FROM SINV '+
              'WHERE ITYPE=9 '+
              ' AND DepFromId='+dm3.taRepairListDEPFROMID.AsString+
              ' AND FYEAR(SDATE)='+GetSYear(dm3.taRepairListSDATE.AsDateTime);
    ExecQuery;
    if Fields[0].IsNull then i:=1
    else i:=Fields[0].AsInteger+1;
    Close;
    Transaction.CommitRetaining;
    dm3.taRepairList.Edit;
    dm3.taRepairListSN.AsInteger:=i;
    dm3.taRepairList.Post;
    dm3.taRepairList.Refresh;
   end;

   SQL.Text:='SELECT COUNT(*) FROM SINV '+
             'WHERE ITYPE=9 AND SN='+dm3.taRepairListSN.AsString+
             ' AND DepFromId='+dm3.taRepairListDEPFROMID.AsString+
             ' AND FYEAR(SDATE)='+GetSYear(dm3.taRepairListSDATE.AsDateTime);
   ExecQuery;
   i:=Fields[0].AsInteger;
   Close;
   Transaction.CommitRetaining;
   if i>1 then raise Exception.Create('������������ ����� ���������!!!');
 end;

 if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
 begin
  with dm, quTmp do  begin
 {�������� ��������� ����� ���������. ��� �� � ��������� �������, � ������� ���� ����. ��������� ������ �������}
   SQL.Text:='select noedit, NOEDITED from Edit_Date_Inv ('+dm3.taRepairListSINVID.AsString+')';
   ExecQuery;
   s_close:=trim(Fields[0].AsString);
   s_closed:=trim(Fields[1].AsString);
   Transaction.CommitRetaining;
   close;
  end;
  if (s_close='') and (s_closed='') then begin
   Screen.Cursor:=crSQLWait;
   dm3.taRepairList.Edit;
   dm3.taRepairListISCLOSED.AsInteger:=1;
   dm3.taRepairList.Post;
   dm3.taRepairList.Refresh;
   Screen.Cursor:=crDefault;
  end else begin
   if s_close<>'' then  MessageDialog(s_close+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
   if s_closed<>'' then MessageDialog(s_closed+' �������� ���� ���������. ', mtWarning, [mbOk], 0);
  end;
 end;
end;

procedure TfmRepair.bttimeClick(Sender: TObject);
var d:tdatetime;
begin
  if dm3.taRepairListISCLOSED.AsInteger<>0 then raise Exception.Create('��������� �������!!!');
  fmSetSDate:=TfmSetSDate.Create(Application);
  try
   fmSetSDate.mc1.Date:=dm3.taRepairListSDATE.AsDateTime;
   fmSetSDate.tp1.Time:=dm3.taRepairListSDATE.AsDateTime;
   if fmSetSDate.ShowModal=mrOK then
   begin
    d:=Trunc(fmSetSDate.mc1.Date)+Frac(fmSetSDate.tp1.Time);
    dm3.taRepairList.Edit;
    dm3.taRepairListSDATE.AsDateTime:=d;
    dm3.taRepairList.Post;
   end
  finally
    fmSetSDate.Free;
  end;
end;

procedure TfmRepair.edPTrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmRepair.edTransKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmRepair.edPTrNDSKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmRepair.DBEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: acReCalcExecute(nil);
 end;
end;

procedure TfmRepair.acOpenInvUpdate(Sender: TObject);
begin
 Taction(Sender).Enabled:= (dm3.taRepairListISCLOSED.AsInteger=1)
end;

procedure TfmRepair.acOpenInvExecute(Sender: TObject);
begin
 ExecSQL('update sinv set isclosed=0 where sinvid='+dm3.taRepairListSINVID.AsString, dm.quTmp); 
 dm3.taRepairList.Refresh;
end;

procedure TfmRepair.edUidKeyPress(Sender: TObject; var Key: Char);
begin
 case key of
  '0'..'9', #8:;
  else sysutils.Abort;
 end;
end;

end.
