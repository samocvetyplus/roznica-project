object fmArtDict: TfmArtDict
  Left = -38
  Top = 401
  HelpContext = 100101
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1088#1090#1080#1082#1091#1083#1086#1074
  ClientHeight = 531
  ClientWidth = 1028
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 1028
    Height = 42
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Position = bpCustom
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086'|'
      ImageIndex = 0
      Spacing = 1
      Left = 515
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 203
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 512
    Width = 1028
    Height = 19
    Panels = <>
  end
  object paDict: TPanel
    Left = 0
    Top = 42
    Width = 1028
    Height = 470
    Align = alClient
    BevelOuter = bvNone
    Caption = 'paDict'
    TabOrder = 2
    object Splitter1: TSplitter
      Left = 433
      Top = 0
      Height = 470
      ExplicitHeight = 475
    end
    object Splitter7: TSplitter
      Left = 753
      Top = 0
      Height = 470
      ExplicitHeight = 475
    end
    object Panel3: TPanel
      Left = 436
      Top = 0
      Width = 317
      Height = 470
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 1
      object dgArt: TDBGridEh
        Left = 0
        Top = 29
        Width = 317
        Height = 441
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        Color = clBtnFace
        ColumnDefValues.Title.Alignment = taCenter
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsArt
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
        ParentFont = False
        PopupMenu = pmA
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnEditButtonClick = dgArtEditButtonClick
        OnEnter = dgArtEnter
        OnExit = dgArtExit
        OnGetCellParams = dgArtGetCellParams
        OnKeyDown = dgArtKeyDown
        OnMouseDown = dgArtMouseDown
        Columns = <
          item
            Color = clInfoBk
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Title.Caption = #1040#1088#1090#1080#1082#1091#1083
            Width = 62
          end
          item
            EditButtons = <>
            FieldName = 'FULLART'
            Footers = <>
            Title.Caption = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
            Width = 93
          end
          item
            Color = clInfoBk
            EditButtons = <>
            FieldName = 'UNITID'
            Footers = <>
            Title.Caption = #1045#1048
            Width = 22
          end
          item
            EditButtons = <>
            FieldName = 'RQ'
            Footers = <>
            Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'RW'
            Footers = <>
            Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            Width = 47
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'F1'
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            Title.Caption = '*'
            Width = 33
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'F2'
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            Title.Caption = '**'
            Width = 27
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object tb2: TSpeedBar
        Left = 0
        Top = 0
        Width = 317
        Height = 29
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        Images = dmCom.ilButtons
        TabOrder = 1
        InternalVer = 1
        object edArt: TEdit
          Left = 8
          Top = 4
          Width = 97
          Height = 21
          Color = clInfoBk
          TabOrder = 0
          Text = 'edArt'
          OnChange = edArtChange
          OnEnter = edArtEnter
          OnKeyDown = edArtKeyDown
        end
        object SpeedbarSection2: TSpeedbarSection
          Caption = 'Untitled (0)'
        end
        object SpeedItem1: TSpeedItem
          BtnCaption = #1042#1089#1077
          Caption = 'SpeedItem1'
          Hint = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Spacing = 1
          Left = 155
          Top = 3
          Visible = True
          OnClick = SpeedItem1Click
          SectionName = 'Untitled (0)'
        end
        object siMergeComplex: TSpeedItem
          BtnCaption = 'acBegEndMerge'
          Caption = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103
          Hint = #1054#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103'|'
          ImageIndex = 53
          Spacing = 1
          Left = 187
          Top = 3
          Visible = True
          OnClick = siMergeComplexClick
          SectionName = 'Untitled (0)'
        end
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 433
      Height = 470
      Align = alLeft
      TabOrder = 0
      object Splitter3: TSplitter
        Left = 53
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object Splitter4: TSplitter
        Left = 189
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object Splitter5: TSplitter
        Left = 252
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object Splitter2: TSplitter
        Left = 121
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object Splitter6: TSplitter
        Left = 305
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object Splitter8: TSplitter
        Left = 361
        Top = 1
        Height = 468
        ExplicitHeight = 473
      end
      object lbComp: TListBox
        Left = 1
        Top = 1
        Width = 52
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbMat: TListBox
        Left = 124
        Top = 1
        Width = 65
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 1
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbGood: TListBox
        Left = 192
        Top = 1
        Width = 60
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 2
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbIns: TListBox
        Left = 255
        Top = 1
        Width = 50
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 3
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbCountry: TListBox
        Left = 56
        Top = 1
        Width = 65
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 4
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt1: TListBox
        Left = 308
        Top = 1
        Width = 53
        Height = 468
        Align = alLeft
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 5
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
      object lbAtt2: TListBox
        Left = 364
        Top = 1
        Width = 68
        Height = 468
        Align = alClient
        Color = clBtnFace
        ItemHeight = 13
        TabOrder = 6
        Visible = False
        OnClick = lbCompClick
        OnKeyPress = lbCompKeyPress
      end
    end
    object Panel6: TPanel
      Left = 756
      Top = 0
      Width = 272
      Height = 470
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 2
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 270
        Height = 468
        Align = alClient
        TabOrder = 0
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 268
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label18: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = #1055#1088'. '#1094#1077#1085#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label19: TLabel
            Left = 8
            Top = 16
            Width = 52
            Height = 13
            Caption = #1056#1072#1089'. '#1094#1077#1085#1072':'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBText4: TDBText
            Left = 99
            Top = 4
            Width = 42
            Height = 13
            Alignment = taRightJustify
            AutoSize = True
            DataField = 'PRICE'
            DataSource = dm.dsMaxArtPrice
          end
          object DBText6: TDBText
            Left = 99
            Top = 16
            Width = 42
            Height = 13
            Alignment = taRightJustify
            AutoSize = True
            DataField = 'PRICE2'
            DataSource = dm.dsMaxArtPrice
          end
        end
        object dgA2: TDBGridEh
          Left = 1
          Top = 33
          Width = 268
          Height = 434
          Align = alClient
          AllowedOperations = [alopUpdateEh]
          Color = clBtnFace
          ColumnDefValues.Title.Alignment = taCenter
          ColumnDefValues.Title.TitleButton = True
          DataGrouping.GroupLevels = <>
          DataSource = dm.dsA2
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
          ParentFont = False
          PopupMenu = pmA2
          RowDetailPanel.Color = clBtnFace
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clNavy
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Color = clInfoBk
              EditButtons = <>
              FieldName = 'ART2'
              Footers = <>
              Title.Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
              Width = 75
            end
            item
              Checkboxes = False
              Color = clInfoBk
              EditButtons = <>
              FieldName = 'nds'
              Footers = <>
              Title.Caption = #1053#1044#1057
              Width = 31
            end
            item
              Color = clInfoBk
              EditButtons = <>
              FieldName = 'USEMARGIN'
              Footers = <>
              Title.Caption = #1053#1072#1094'.'
              Width = 32
            end
            item
              Color = clAqua
              EditButtons = <>
              FieldName = 'PRICE1'
              Footers = <>
              Title.Caption = #1055#1088'. '#1094#1077#1085#1072
              Width = 53
            end
            item
              Color = clInfoBk
              EditButtons = <>
              FieldName = 'PRICE2'
              Footers = <>
              Title.Caption = #1056#1072#1089'. '#1094#1077#1085#1072
            end
            item
              Color = clInfoBk
              EditButtons = <>
              FieldName = 'W'
              Footers = <>
              Title.Caption = #1042#1077#1089
              Width = 27
            end
            item
              EditButtons = <>
              FieldName = 'RQ'
              Footers = <>
              Title.Caption = #1050#1086#1083'-'#1074#1086' '#1086#1089#1090'.'
            end
            item
              EditButtons = <>
              FieldName = 'RW'
              Footers = <>
              Title.Caption = #1042#1077#1089' '#1086#1089#1090'.'
            end
            item
              Checkboxes = True
              EditButtons = <>
              FieldName = 'F1'
              Footers = <>
              KeyList.Strings = (
                '1'
                '0')
              Title.Caption = '*'
              Width = 18
            end
            item
              Checkboxes = True
              EditButtons = <>
              FieldName = 'F2'
              Footers = <>
              KeyList.Strings = (
                '1'
                '0')
              Title.Caption = '**'
              Width = 22
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
    end
  end
  object pmA: TPopupMenu
    Images = dmCom.ilButtons
    Left = 472
    Top = 156
    object miAddArt: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      ShortCut = 45
      OnClick = miAddArtClick
    end
    object miInsFromSearch: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079' '#1087#1086#1080#1089#1082#1072
      ShortCut = 16429
      OnClick = miInsFromSearchClick
    end
    object miAEdit: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1072
      ShortCut = 115
      OnClick = miAEditClick
    end
    object N22: TMenuItem
      Caption = #1042#1089#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 116
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object N16: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = N16Click
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 16397
      OnClick = N18Click
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object N28: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
      OnClick = N28Click
    end
    object N27: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnClick = N27Click
    end
    object N26: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1090#1086#1074#1072#1088#1086#1074
      OnClick = N26Click
    end
    object NCountry: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1089#1090#1088#1072#1085
      OnClick = NCountryClick
    end
    object N25: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1089#1090#1072#1074#1086#1082
      OnClick = N25Click
    end
    object N11: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1090#1088#1080#1073#1091#1090#1086#1074' 1'
      OnClick = N11Click
    end
    object N21: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1072#1090#1088#1080#1073#1091#1090#1086#1074' 2'
      OnClick = N21Click
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object DArtId1: TMenuItem
      Caption = 'D_ArtId'
      OnClick = DArtId1Click
    end
  end
  object pmA2: TPopupMenu
    Images = dmCom.ilButtons
    Left = 824
    Top = 216
    object miIns: TMenuItem
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 45
      ShortCut = 117
      OnClick = miInsClick
    end
    object N4: TMenuItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1086#1074#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      ShortCut = 16429
      OnClick = N4Click
    end
    object N13: TMenuItem
      Caption = '-'
    end
    object N14: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 2
      ShortCut = 16430
      OnClick = N14Click
    end
    object N19: TMenuItem
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099
      ShortCut = 16397
      OnClick = N19Click
    end
    object N20: TMenuItem
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1085#1080#1077' '#1087#1088#1080#1093#1086#1076#1085#1086#1081' '#1094#1077#1085#1099
      OnClick = N20Click
    end
    object DArt2ID1: TMenuItem
      Caption = 'Art2ID'
      OnClick = DArt2ID1Click
    end
  end
  object fr1: TM207FormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'Panel5.Width'
      'lbComp.Width'
      'lbCountry.Width'
      'lbGood.Width'
      'lbIns.Width'
      'lbMat.Width')
    StoredValues = <>
    Left = 528
    Top = 184
  end
  object prdgA2: TPrintDBGridEh
    DBGridEh = dgA2
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 856
    Top = 136
  end
  object acList: TActionList
    Left = 808
    Top = 288
    object acPrintA2: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = 'acPrintA2'
      ShortCut = 16464
      OnExecute = acPrintA2Execute
    end
  end
end
