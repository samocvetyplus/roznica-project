unit frmDemo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery;

type
  TForm2 = class(TForm)
    CenterDB: TpFIBDatabase;
    FillialDB: TpFIBDatabase;
    CenterTransaction: TpFIBTransaction;
    FilialTransaction: TpFIBTransaction;
    pFIBQuery1: TpFIBQuery;
    pFIBQuery2: TpFIBQuery;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.FormCreate(Sender: TObject);
begin
  CenterDB.Connected := True;
  FillialDB.Connected := True;
  pFIBQuery1.ExecProc;
  pFIBQuery1.ExecProc;  
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
  CenterDB.Connected := False;
  FillialDB.Connected := False;

end;

end.
