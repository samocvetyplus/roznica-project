object frmDInvCheck: TfrmDInvCheck
  Left = 252
  Top = 80
  HelpContext = 100226
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
  ClientHeight = 513
  ClientWidth = 723
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 723
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blBottom, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    TabOrder = 0
    ExplicitTop = -1
    InternalVer = 1
    object filtLb: TLabel
      Left = 280
      Top = 14
      Width = 64
      Height = 13
      Caption = #1060#1080#1083#1100#1090#1088#1072#1094#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      Action = acExit
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      Spacing = 1
      Left = 659
      Top = 3
      Visible = True
      OnClick = acExitExecute
      SectionName = 'Untitled (0)'
    end
    object siAdd: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1079#1072#1087#1080#1089#1100
      ImageIndex = 1
      Spacing = 1
      Left = 195
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object siDel: TSpeedItem
      Action = acDel
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acDelExecute
      SectionName = 'Untitled (0)'
    end
    object siCloseInv: TSpeedItem
      Action = acClear
      BtnCaption = #1054#1095#1080#1089#1090#1080#1090#1100
      Caption = #1053#1072#1095#1072#1090#1100' '#1079#1072#1085#1086#1074#1086
      ImageIndex = 80
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acClearExecute
      SectionName = 'Untitled (0)'
    end
    object siPrint: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
        0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00FFFF
        FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000FF000000FF000000
        FF00C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
        FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF000000FF00
        0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF00
        0000FF000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      ImageIndex = 67
      Spacing = 1
      Left = 195
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
    object siUID: TSpeedItem
      BtnCaption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Caption = #1048#1076'.'#1085#1086#1084#1077#1088#1072
      Hint = #1048#1076#1077#1085#1090#1080#1092#1080#1082#1072#1094#1080#1086#1085#1085#1099#1077' '#1085#1086#1084#1077#1088#1072
      ImageIndex = 35
      Spacing = 1
      Left = 387
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acFilter
      BtnCaption = #1060#1080#1083#1100#1090#1088
      Caption = #1060#1080#1083#1100#1090#1088
      DropDownMenu = PMFilter
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000052000000520000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00005200009CDEAD0042AD21000052
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00105A10009CDEAD0042AD2100085A
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00105A100094CE9C0042A51800085A
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00085A080084CE940039A52100005A
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF001084210021BD520010A529000073
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00108C210029D65A0018BD420008A521000094
        0800006B0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF001894290039E76B0021CE5A0018B53900089C1800008C
        000000840000006B0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00189C29004AFF8C0039EF730021CE5A0010B53900089C1800008C
        00000084000000840000006B0000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00108C290031DE630029C6520018A53900088C2100007B100000730000006B
        0000006B0000006B00000073000000630000FF00FF00FF00FF00FF00FF000052
        00000063000010630000106B000000730800299C390039CECE0000C6CE000084
        7B002194310021A5310018942900007B0800005A0800FF00FF00004A0000005A
        0000738C0000EF9C2100E79410007B8C080008AD210052D6840031EFFF0000DE
        FF00089CA50042D6730029DE52007BC69C0010841800004A0000FF00FF00004A
        0000D6A56300F7E79C00E7CE6300D68C08000052000000520000428C4A0039EF
        FF00005A4A000042000042734200F75AF70039103900FF00FF00FF00FF00FF00
        FF00D6B58C00FFFFEF00F7DE8C00D6941000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF9CFF00FF00FF00AD00AD007B007B00FF00FF00FF00
        FF00FF00FF00C6A57300C6944200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FFD6FF00FF29FF008C008C00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF9CFF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acFilterExecute
      SectionName = 'Untitled (0)'
    end
    object siHelp: TSpeedItem
      BtnCaption = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1057#1087#1088#1072#1074#1082#1072
      Hint = #1057#1087#1088#1072#1074#1082#1072'|'
      ImageIndex = 73
      Spacing = 1
      Left = 595
      Top = 3
      Visible = True
      OnClick = siHelpClick
      SectionName = 'Untitled (0)'
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 723
    Height = 32
    Align = alTop
    TabOrder = 1
    object Label2: TLabel
      Left = 96
      Top = 8
      Width = 52
      Height = 13
      Caption = #1048#1085'. '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edUid: TEdit
      Left = 154
      Top = 5
      Width = 121
      Height = 21
      TabOrder = 0
      OnKeyDown = edUidKeyDown
    end
  end
  object gr: TDBGridEh
    Left = 0
    Top = 73
    Width = 723
    Height = 440
    Align = alClient
    Color = clHighlightText
    ColumnDefValues.Title.Color = clActiveBorder
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsDInvCheck
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnGetCellParams = grGetCellParams
    Columns = <
      item
        EditButtons = <>
        FieldName = 'O_UID'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_COMP'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_GOODS'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_MAT'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_INS'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_UNIT'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_ART'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_W'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_SZ'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end
      item
        EditButtons = <>
        FieldName = 'O_PRICE'
        Footers = <>
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object acLst: TActionList
    Images = dmCom.ilButtons
    Left = 300
    Top = 288
    object acDel: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDelExecute
    end
    object acClear: TAction
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100
      ImageIndex = 80
      OnExecute = acClearExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acCheckUID: TAction
      Caption = 'acCheckUID'
      OnExecute = acCheckUIDExecute
    end
    object acFilter: TAction
      Caption = #1060#1080#1083#1100#1090#1088
      ImageIndex = 79
      OnExecute = acFilterExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 67
      OnExecute = acPrintExecute
    end
  end
  object PMFilter: TPopupMenu
    Left = 296
    Top = 232
    object N1: TMenuItem
      AutoCheck = True
      Caption = #1053#1077' '#1087#1088#1086#1074#1077#1088#1077#1085#1099
      OnClick = N2Click
    end
    object N2: TMenuItem
      AutoCheck = True
      Caption = #1055#1088#1086#1074#1077#1088#1077#1085#1099
      OnClick = N2Click
    end
    object N3: TMenuItem
      AutoCheck = True
      Caption = #1053#1077' '#1095#1080#1089#1083#1103#1090#1089#1103
      OnClick = N2Click
    end
  end
  object PrintDB: TPrintDBGridEh
    DBGridEh = gr
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Title.Strings = (
      #1056#1077#1079#1091#1083#1100#1090#1072#1090#1099' '#1087#1088#1086#1074#1077#1088#1082#1080' '#1085#1072#1082#1083#1072#1076#1085#1086#1081)
    Units = MM
    Left = 236
    Top = 284
  end
end
