object fmJUID: TfmJUID
  Left = 269
  Top = 129
  Width = 696
  Height = 480
  Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' ('#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086')'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 688
    Height = 41
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 3
    BtnOffsetVert = 3
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 0
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1086#1082#1085#1086
      ImageIndex = 0
      Spacing = 1
      Left = 483
      Top = 3
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitCalc: TSpeedItem
      BtnCaption = #1056#1072#1089#1095#1077#1090
      Caption = #1056#1072#1089#1095#1077#1090
      Hint = #1056#1072#1089#1095#1077#1090
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitCalcClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 67
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
    object spExport: TSpeedItem
      BtnCaption = #1058#1077#1082#1089#1090
      Caption = #1058#1077#1082#1089#1090
      Hint = #1058#1077#1082#1089#1090'|'
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = spExportClick
      SectionName = 'Untitled (0)'
    end
  end
  object stbrJUID: TStatusBar
    Left = 0
    Top = 432
    Width = 688
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Width = 250
      end
      item
        Width = 50
      end>
  end
  object gr: TM207IBGrid
    Left = 0
    Top = 70
    Width = 688
    Height = 362
    Align = alClient
    Color = clBtnFace
    DataSource = dmServ.dsrJUID
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    IniStorage = fmstrJUID
    TitleButtons = True
    OnGetCellParams = grGetCellParams
    MultiShortCut = 0
    ColorShortCut = 0
    InfoShortCut = 0
    TitleDblClick = True
    ClearHighlight = True
    SortOnTitleClick = True
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'COMP'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1089#1090#1072#1074#1097'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UID_SUP'
        Title.Caption = #8470' '#1091' '#1087#1086#1089#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_MATID'
        Title.Alignment = taCenter
        Title.Caption = #1052#1072#1090'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_GOODID'
        Title.Alignment = taCenter
        Title.Caption = #1048#1079#1076'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'D_INSID'
        Title.Alignment = taCenter
        Title.Caption = #1042#1089#1090#1072#1074'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'UNITID'
        Title.Alignment = taCenter
        Title.Caption = #1045#1076'.'#1080#1079#1084'.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ART'
        Title.Alignment = taCenter
        Title.Caption = #1040#1088#1090#1080#1082#1091#1083
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SZ'
        Title.Alignment = taCenter
        Title.Caption = #1056#1072#1079#1084#1077#1088
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'INW'
        Title.Alignment = taCenter
        Title.Caption = #1042#1077#1089
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clAqua
        Expanded = False
        FieldName = 'INQ'
        Title.Alignment = taCenter
        Title.Caption = #1042#1093'.'#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'OUTQ'
        Title.Alignment = taCenter
        Title.Caption = #1048#1089#1093'.'#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'DEBTORQ'
        Title.Alignment = taCenter
        Title.Caption = #1055#1086#1089#1090'.'#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'SALEQ'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080' '#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RETQ'
        Title.Alignment = taCenter
        Title.Caption = #1042#1086#1079#1074#1088'.'#1087#1086#1082'.'#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RETOPTQ'
        Title.Alignment = taCenter
        Title.Caption = #1042#1086#1079#1074#1088'.'#1086#1087#1090'. '#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'RETVENDORQ'
        Title.Alignment = taCenter
        Title.Caption = #1042#1086#1079#1074#1088'.'#1087#1086#1089#1090'. '#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'OPTQ'
        Title.Alignment = taCenter
        Title.Caption = #1055#1088#1086#1076'.'#1086#1087#1090' '#1082'-'#1074#1086
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'UID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ARTID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CURRQ'
        Title.Caption = #1058#1077#1082'.'#1082'-'#1074#1086
        Visible = True
      end>
  end
  object tb2: TSpeedBar
    Left = 0
    Top = 41
    Width = 688
    Height = 29
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 80
    BtnHeight = 23
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 3
    InternalVer = 1
    object laDep: TLabel
      Left = 84
      Top = 8
      Width = 71
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      Visible = False
    end
    object laPeriod: TLabel
      Left = 364
      Top = 8
      Width = 47
      Height = 13
      Caption = 'laPeriod'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedbarSection2: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076
      DropDownMenu = dmServ.pmList3
      Hint = #1042#1099#1073#1086#1088' '#1089#1082#1083#1072#1076#1072
      ImageIndex = 3
      Layout = blGlyphRight
      Spacing = 1
      Left = 2
      Top = 2
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1088#1080#1086#1076
      Caption = #1055#1077#1088#1080#1086#1076
      Hint = #1055#1077#1088#1080#1086#1076'|'
      ImageIndex = 3
      Layout = blGlyphLeft
      Spacing = 1
      Left = 282
      Top = 2
      Visible = True
      OnClick = spitPeriodClick
      SectionName = 'Untitled (0)'
    end
  end
  object fmstrJUID: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 136
    Top = 152
  end
  object svdFile: TOpenDialog
    DefaultExt = 'nlz'
    Left = 264
    Top = 192
  end
end
