unit InvMarg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, db, Menus, StdCtrls, Mask, DBCtrls, RxDBComb, rxSpeedbar;

type
  TfmInvMargin = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    dg1: TM207IBGrid;
    pm1: TPopupMenu;
    siSet: TSpeedItem;
    N1: TMenuItem;
    tb2: TSpeedBar;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    N2: TMenuItem;
    N3: TMenuItem;
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siSetClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmInvMargin: TfmInvMargin;

implementation

uses comdata, Data, M207Proc, Data2;

{$R *.DFM}

procedure TfmInvMargin.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmInvMargin.siExitClick(Sender: TObject);
begin
  Close;               
end;

procedure TfmInvMargin.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmInvMargin.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  tb2.Wallpaper:=wp;
  with dmCom, dm, dm2 do
    begin
      OpenDataSets([taInvMarg]);
    end;
end;

procedure TfmInvMargin.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmCom, dm, dm2  do
    begin
      PostDataSets([taInvMarg, taSList]);
      CloseDataSets([taInvMarg]);
    end;
end;

procedure TfmInvMargin.siSetClick(Sender: TObject);
begin
  with dmCom, dm, dm2, quTmp do
    begin
      PostDataSets([taInvMarg]);
      SQL.Text:='execute procedure SETINVMARGIN '+taInvMargDepId.AsString+', '+taSListSInvId.AsString;
      ExecQuery;
      tr.CommitRetaining;
      ReOpenDataSets([taInvMarg]);
    end;
end;

procedure TfmInvMargin.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
      VK_F12: Close;
    end;
end;

procedure TfmInvMargin.N2Click(Sender: TObject);
begin
  with dmCom, dm, dm2, quTmp do
    begin
      SQL.Text:='execute procedure CopyToGeneralMargin '+taSListSInvId.AsString;
      ExecQuery;
      tr.CommitRetaining;
      taSList.Refresh;
    end;
end;

procedure TfmInvMargin.N3Click(Sender: TObject);
begin
  with dmCom, dm, dm2, quTmp do
    begin
      PostDataSets([taInvMarg]);
      SQL.Text:='execute procedure CopyToInvMargin '+taSListSInvId.AsString;
      ExecQuery;
      tr.CommitRetaining;
      ReOpenDataSets([taInvMarg]);
    end;
end;

end.
