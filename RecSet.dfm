object fmRecSet: TfmRecSet
  Left = 279
  Top = 141
  BorderStyle = bsDialog
  Caption = #1047#1085#1072#1095#1077#1085#1080#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
  ClientHeight = 264
  ClientWidth = 375
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 12
    Top = 8
    Width = 31
    Height = 13
    Caption = #1040#1082#1094#1080#1079
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 8
    Top = 112
    Width = 101
    Height = 13
    Caption = #1057#1082#1083#1072#1076' '#1076#1083#1103' '#1086#1089#1090#1072#1090#1082#1086#1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 169
    Width = 160
    Height = 13
    Caption = #1053#1044#1057' '#1085#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090#1085#1099#1077' '#1088#1072#1089#1093#1086#1076#1099
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 8
    Top = 136
    Width = 108
    Height = 13
    Caption = #1057#1082#1083#1072#1076' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 200
    Width = 194
    Height = 26
    Caption = 
      #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086' '#1076#1086#1087#1091#1089#1090#1080#1084#1086#1077' '#1082#1086#1083'-'#1074#1086' '#13#10#1086#1076#1085#1086#1074#1088#1077#1084#1077#1085#1085#1086' '#1079#1072#1087#1091#1089#1082#1072#1077#1084#1099#1093' '#1088#1072#1089#1095#1077#1090#1086 +
      #1074
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 160
    Top = 136
    Width = 32
    Height = 13
    Caption = 'Label8'
  end
  object Label9: TLabel
    Left = 8
    Top = 232
    Width = 184
    Height = 26
    Caption = #1052#1072#1082#1089#1080#1083#1072#1083#1100#1085#1086#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1082#1083#1072#1076#1086#1074' '#13#10#1088#1072#1089#1089#1095#1080#1090#1072#1085#1085#1099#1093' '#1085#1072' '#1087#1077#1088#1080#1086#1076
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object DBEdit1: TDBEdit
    Left = 52
    Top = 4
    Width = 45
    Height = 21
    Color = clInfoBk
    DataField = 'AKCIZ'
    DataSource = dmCom.dsRec
    TabOrder = 0
  end
  object BitBtn1: TBitBtn
    Left = 296
    Top = 4
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 296
    Top = 34
    Width = 75
    Height = 25
    BiDiMode = bdRightToLeft
    Caption = #1054#1090#1084#1077#1085#1072
    ParentBiDiMode = False
    TabOrder = 2
    Kind = bkCancel
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 30
    Width = 263
    Height = 73
    Caption = #1054#1087#1090
    TabOrder = 3
    object Label2: TLabel
      Left = 8
      Top = 18
      Width = 47
      Height = 13
      Caption = #1053#1072#1094#1077#1085#1082#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 6
      Top = 46
      Width = 98
      Height = 13
      Caption = #1053#1086#1088#1084#1072' '#1086#1082#1088#1091#1075#1083#1077#1085#1080#1103':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBEdit2: TDBEdit
      Left = 60
      Top = 14
      Width = 45
      Height = 21
      Color = clInfoBk
      DataField = 'OPMARGIN'
      DataSource = dmCom.dsRec
      TabOrder = 0
    end
    object RxDBComboBox1: TRxDBComboBox
      Left = 106
      Top = 42
      Width = 145
      Height = 21
      Color = clInfoBk
      DataField = 'OPTRNORM'
      DataSource = dmCom.dsRec
      EnableValues = False
      ItemHeight = 13
      Items.Strings = (
        #1050#1086#1087#1077#1081#1082#1080
        #1044#1077#1089#1103#1090#1082#1080' '#1082#1086#1087#1077#1077#1082
        #1056#1091#1073#1083#1080
        #1044#1077#1089#1103#1090#1082#1080' '#1088#1091#1073#1083#1077#1081
        #1057#1086#1090#1085#1080' '#1088#1091#1073#1083#1077#1081
        #1058#1099#1089#1103#1095#1080' '#1088#1091#1073#1083#1077#1081
        ' ')
      TabOrder = 1
      Values.Strings = (
        '2'
        '1'
        '0'
        '-1'
        '-2'
        '-3'
        ' ')
    end
  end
  object lcRestWH: TDBLookupComboBox
    Left = 124
    Top = 108
    Width = 145
    Height = 21
    Color = clInfoBk
    DataField = 'RESTWHID'
    DataSource = dmCom.dsRec
    KeyField = 'D_DEPID'
    ListField = 'SNAME'
    ListSource = dmCom.dsDep
    TabOrder = 4
  end
  object DBEdit3: TDBEdit
    Left = 176
    Top = 165
    Width = 45
    Height = 21
    Color = clInfoBk
    DataField = 'PTRNDS'
    DataSource = dmCom.dsRec
    TabOrder = 5
  end
  object DBLookupComboBox1: TDBLookupComboBox
    Left = 124
    Top = 136
    Width = 145
    Height = 21
    Color = clInfoBk
    DataField = 'DEFDEP'
    DataSource = dmCom.dsRec
    KeyField = 'D_DEPID'
    ListField = 'SNAME'
    ListSource = dmCom.dsDep
    TabOrder = 6
  end
  object RxDBCalcEdit1: TRxDBCalcEdit
    Left = 296
    Top = 165
    Width = 65
    Height = 21
    NumGlyphs = 2
    TabOrder = 7
  end
  object DBEdit4: TDBEdit
    Left = 216
    Top = 200
    Width = 65
    Height = 21
    DataField = 'MAXCOUNTWH'
    DataSource = dmCom.dsRec
    TabOrder = 8
  end
  object DBEdit5: TDBEdit
    Left = 216
    Top = 232
    Width = 65
    Height = 21
    DataField = 'MAXCOUNTUIDWH'
    DataSource = dmCom.dsRec
    TabOrder = 9
  end
end
