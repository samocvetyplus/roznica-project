object fmUidRepeat: TfmUidRepeat
  Left = 265
  Top = 103
  Width = 360
  Height = 542
  Caption = #1055#1086#1074#1090#1086#1088#1099' '#1085#1072' '#1089#1082#1083#1072#1076#1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 40
    Width = 352
    Height = 454
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dm3.dsUidRepeat
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088
        Width = 171
      end
      item
        EditButtons = <>
        FieldName = 'COUNT'
        Footers = <>
        Title.Caption = #1050#1086#1083'-'#1074#1086' '#1087#1086#1074#1090#1086#1088#1086#1074
        Width = 164
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object tb1: TSpeedBar
    Left = 0
    Top = 0
    Width = 352
    Height = 40
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    BoundLines = [blTop, blLeft, blRight]
    Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    BtnOffsetHorz = 2
    BtnOffsetVert = 2
    BtnWidth = 64
    BtnHeight = 36
    Images = dmCom.ilButtons
    BevelOuter = bvSpace
    TabOrder = 1
    InternalVer = 1
    object SpeedbarSection1: TSpeedbarSection
      Caption = 'Untitled (0)'
    end
    object siExit: TSpeedItem
      BtnCaption = #1042#1099#1093#1086#1076
      Caption = #1042#1099#1093#1086#1076
      Hint = #1042#1099#1093#1086#1076'|'
      ImageIndex = 0
      Spacing = 1
      Left = 282
      Top = 2
      Visible = True
      OnClick = siExitClick
      SectionName = 'Untitled (0)'
    end
    object spitUIDHistory: TSpeedItem
      BtnCaption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      Hint = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103'|'
      Spacing = 1
      Left = 2
      Top = 2
      Visible = True
      OnClick = spitUIDHistoryClick
      SectionName = 'Untitled (0)'
    end
    object spitUpdate: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100'|'
      Spacing = 1
      Left = 66
      Top = 2
      Visible = True
      OnClick = spitUpdateClick
      SectionName = 'Untitled (0)'
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 494
    Width = 352
    Height = 19
    Panels = <>
  end
end
