unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, Menus, Db, pFIBDataSet, StdCtrls,
  Grids, DBGrids, ActnList, DataFR, DepSel, DateUtils,XMLIntf,
  Math, xmldom, XMLDoc, RXStrUtils, NotifClient, Log_Function, ShellAPI, M207IBLogin,
  jpeg, AppEvnts, RXShell, rxPlacemnt, rxSpeedbar, EhLibIBX, EhLibFIB, DBGridEh, XLSExportComp, XLSFormat,
  pFIBDatabase, Variants, cxLocalization;

type
//  t_depmas = array of integer;
  TfmMain = class(TForm)
    tb1: TSpeedBar;
    sb1: TStatusBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    siSList: TSpeedItem;
    miSList: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    siPrice: TSpeedItem;
    N7: TMenuItem;
    siWH: TSpeedItem;
    siDList: TSpeedItem;
    siSellList: TSpeedItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    siOpt: TSpeedItem;
    N10: TMenuItem;
    siSRet: TSpeedItem;
    N13: TMenuItem;
    pmWH: TPopupMenu;
    miArtWH: TMenuItem;
    miUIDWH: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    FormStorage1: TFormStorage;
    N30: TMenuItem;
    N31: TMenuItem;
    N29: TMenuItem;
    pmRet: TPopupMenu;
    miSRet: TMenuItem;
    miORet: TMenuItem;
    N34: TMenuItem;
    mnitRetClt: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    mnitPrint: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    mnitJSz: TMenuItem;
    acEvent: TActionList;
    actSList: TAction;
    actPrOrd: TAction;
    actWH: TAction;
    actWHArt: TAction;
    actWHUID: TAction;
    actDList: TAction;
    actCurSell: TAction;
    actSellList: TAction;
    actRet: TAction;
    actSRet: TAction;
    actORet: TAction;
    actSellRet: TAction;
    actOpt: TAction;
    actRest: TAction;
    N110: TMenuItem;
    N210: TMenuItem;
    N310: TMenuItem;
    N41: TMenuItem;
    N42: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    enduidwhcalc: TMenuItem;
    siRest: TSpeedItem;
    N24: TMenuItem;
    actFRest: TAction;
    N54: TMenuItem;
    N55: TMenuItem;
    N56: TMenuItem;
    actApplList: TAction;
    spiAppl: TSpeedItem;
    N112: TMenuItem;
    N211: TMenuItem;
    N57: TMenuItem;
    endSellElemCalc1: TMenuItem;
    errsellbusytype1: TMenuItem;
    NsuppInfor: TMenuItem;
    N58: TMenuItem;
    N60: TMenuItem;
    N61: TMenuItem;
    actHelp: TAction;
    N67: TMenuItem;
    NNewDepClear: TMenuItem;
    N68: TMenuItem;
    Warehouseanalysis1: TMenuItem;
    mnitJDep: TMenuItem;
    sbmErrPrPrice: TMenuItem;
    sbmCnf: TMenuItem;
    acLoadReport: TAction;
    acLoadSQL: TAction;
    N69: TMenuItem;
    N70: TMenuItem;
    N71: TMenuItem;
    dlgOpenFile: TOpenDialog;
    N72: TMenuItem;
    acExportNoRest: TAction;
    N73: TMenuItem;
    N74: TMenuItem;
    N75: TMenuItem;
    N4: TMenuItem;
    NExportErr: TMenuItem;
    N62: TMenuItem;
    acInfoUIDInsHist: TAction;
    acAtt1: TAction;
    acAtt2: TAction;
    N63: TMenuItem;
    N113: TMenuItem;
    N212: TMenuItem;
    N64: TMenuItem;
    NSnameSup: TMenuItem;
    N66: TMenuItem;
    HelpUser: TMenuItem;
    NClearDelRecords: TMenuItem;
    NEditFile: TMenuItem;
    acUIDStore: TAction;
    acInfoChangePrice: TAction;
    acInfoArtHist: TAction;
    acInfoArtMergeHist: TAction;
    acInfoArt2MergeHist: TAction;
    acInfoUIDHist: TAction;
    acInfoUIDPriceHist: TAction;
    acSetting: TAction;
    acColorSchema: TAction;
    acSetting1: TAction;
    acAnalizSz: TAction;
    acAnalizDep: TAction;
    acPriceAnalysis: TAction;
    acWarehouseAnalysis: TAction;
    acDep: TAction;
    acExportWithRest: TAction;
    acExportWithRestNoReCalc: TAction;
    NExportLog: TMenuItem;
    acExportLog: TAction;
    acDelLog: TAction;
    NDelLog: TMenuItem;
    acAdmin: TAction;
    acUidWhCalc: TAction;
    acEndSellCalc: TAction;
    acErrPrPrice: TAction;
    acCnf: TAction;
    acErrSellBusyType: TAction;
    acErrBracking: TAction;
    acErrInSell: TAction;
    acErrInPrord: TAction;
    acClearBase: TAction;
    acClearDelRecords: TAction;
    acAddClientFromFile: TAction;
    acEditFile: TAction;
    acInformation: TAction;
    acDMat: TAction;
    acDGood: TAction;
    acDGoodsSam: TAction;
    acDIns: TAction;
    acDComp: TAction;
    acDArt: TAction;
    acDNDS: TAction;
    acDEDGETION: TAction;
    acDPayType: TAction;
    acDDiscount: TAction;
    acDRet: TAction;
    acDClient: TAction;
    acDCountry: TAction;
    acDAddress: TAction;
    acDSupName: TAction;
    acReferenceBook: TAction;
    acSettingMain: TAction;
    acReport: TAction;
    acExport: TAction;
    acIsSell_Run: TAction;
    Nissell_run: TMenuItem;
    acListInventory: TAction;
    NListInventory: TMenuItem;
    acInsideInfo: TAction;
    N33: TMenuItem;
    acInfoEditArtHist: TAction;
    NInfoEditArtHist: TMenuItem;
    NAddArtCDM: TMenuItem;
    actApplListDep: TAction;
    acAppl: TAction;
    pmAppl: TPopupMenu;
    NApplList: TMenuItem;
    NApplDep: TMenuItem;
    NDeleteAct: TMenuItem;
    acDeleteAct: TAction;
    NdiscountSum: TMenuItem;
    acDDiscountSum: TAction;
    acEditBadRef: TAction;
    NEditBadRef: TMenuItem;
    svFile: TSaveDialog;
    acShopReport: TAction;
    N38: TMenuItem;
    NLog: TMenuItem;
    acLog: TAction;
    acWriteInfoLog: TAction;
    NDelScript: TMenuItem;
    acAddArtCDM: TAction;
    acExportErr: TAction;
    acImportDep: TAction;
    NImportDep: TMenuItem;
    acExportDepSell: TAction;
    N43: TMenuItem;
    acAddSell: TAction;
    N32: TMenuItem;
    NExportSell: TMenuItem;
    acSupInf: TAction;
    N44: TMenuItem;
    actActAllowances: TAction;
    acNodCard: TAction;
    acEquipment: TAction;
    acUIDStoreDep: TAction;
    N45: TMenuItem;
    acPayment: TAction;
    N46: TMenuItem;
    N47: TMenuItem;
    NAppl: TMenuItem;
    NApplSUP: TMenuItem;
    NApplD: TMenuItem;
    NActAllowances: TMenuItem;
    NEquipmnet: TMenuItem;
    acCountPross: TAction;
    NCountPross: TMenuItem;
    acRespStoring: TAction;
    NRespStoring: TMenuItem;
    acRepair: TAction;
    NRepair: TMenuItem;
    NRepair1: TMenuItem;
    acSuspItem: TAction;
    NSuspItem: TMenuItem;
    acUserUidWH: TAction;
    NUserUidWh: TMenuItem;
    N48: TMenuItem;
    acIsMergeRun: TAction;
    N51: TMenuItem;
    NotSale: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    N59: TMenuItem;
    N65: TMenuItem;
    tiRepExe: TRxTrayIcon;
    siSert: TSpeedItem;
    acSert: TAction;
    N76: TMenuItem;
    XLSExportFile1: TXLSExportFile;
    SD1: TSaveDialog;
    siBuy: TSpeedItem;
    acBuy: TAction;
    N77: TMenuItem;
    N78: TMenuItem;
    miExperiment: TMenuItem;
    siCoupon: TSpeedItem;
    siIind_order: TSpeedItem;
    TrayIcon: TRxTrayIcon;
    TimerIndOrder: TTimer;
    N79: TMenuItem;
    N80: TMenuItem;
    N81: TMenuItem;
    N82: TMenuItem;
    N83: TMenuItem;
    N84: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure actSListExecute(Sender: TObject);
    procedure actPrOrdExecute(Sender: TObject);
    procedure actWHArtExecute(Sender: TObject);
    procedure actWHUIDExecute(Sender: TObject);
    procedure actDListExecute(Sender: TObject);
    procedure actSellListExecute(Sender: TObject);
    procedure actCurSellExecute(Sender: TObject);
    procedure actWHExecute(Sender: TObject);
    procedure actRetExecute(Sender: TObject);
    procedure actSRetExecute(Sender: TObject);
    procedure actORetExecute(Sender: TObject);
    procedure actSellRetExecute(Sender: TObject);
    procedure actOptExecute(Sender: TObject);
    procedure actRestExecute(Sender: TObject);
    procedure actFRestExecute(Sender: TObject);
    procedure N56Click(Sender: TObject);
    procedure actApplListExecute(Sender: TObject);
    procedure actHelpExecute(Sender: TObject);
    procedure NNewDepClearClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure acLoadReportExecute(Sender: TObject);
    procedure acLoadSQLExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure acInfoUIDInsHistExecute(Sender: TObject);
    procedure acAtt1Execute(Sender: TObject);
    procedure acAtt2Execute(Sender: TObject);
    procedure HelpUserClick(Sender: TObject);
    procedure HeplProgrammClick(Sender: TObject);
    procedure acUIDStoreExecute(Sender: TObject);
    procedure acInfoChangePriceExecute(Sender: TObject);
    procedure acInfoArtHistExecute(Sender: TObject);
    procedure acInfoArtMergeHistExecute(Sender: TObject);
    procedure acInfoArt2MergeHistExecute(Sender: TObject);
    procedure acInfoUIDHistExecute(Sender: TObject);
    procedure acInfoUIDPriceHistExecute(Sender: TObject);
    procedure acSettingExecute(Sender: TObject);
    procedure acColorSchemaExecute(Sender: TObject);
    procedure acSetting1Execute(Sender: TObject);
    procedure acAnalizSzExecute(Sender: TObject);
    procedure acAnalizDepExecute(Sender: TObject);
    procedure acPriceAnalysisExecute(Sender: TObject);
    procedure acWarehouseAnalysisExecute(Sender: TObject);
    procedure acDepExecute(Sender: TObject);
    procedure acExportLogExecute(Sender: TObject);
    procedure acDelLogExecute(Sender: TObject);
    procedure acUidWhCalcExecute(Sender: TObject);
    procedure acEndSellCalcExecute(Sender: TObject);
    procedure acErrPrPriceExecute(Sender: TObject);
    procedure acCnfExecute(Sender: TObject);
    procedure acAdminExecute(Sender: TObject);
    procedure acErrSellBusyTypeExecute(Sender: TObject);
    procedure acErrBrackingExecute(Sender: TObject);
    procedure acErrInSellExecute(Sender: TObject);
    procedure acErrInPrordExecute(Sender: TObject);
    procedure acClearBaseExecute(Sender: TObject);
    procedure acClearDelRecordsExecute(Sender: TObject);
    procedure acAddClientFromFileExecute(Sender: TObject);
    procedure acEditFileExecute(Sender: TObject);
    procedure acInformationExecute(Sender: TObject);
    procedure acDMatExecute(Sender: TObject);
    procedure acDGoodExecute(Sender: TObject);
    procedure acDGoodsSamExecute(Sender: TObject);
    procedure acDInsExecute(Sender: TObject);
    procedure acDCompExecute(Sender: TObject);
    procedure acDArtExecute(Sender: TObject);
    procedure acDNDSExecute(Sender: TObject);
    procedure acDEDGETIONExecute(Sender: TObject);
    procedure acDPayTypeExecute(Sender: TObject);
    procedure acDDiscountExecute(Sender: TObject);
    procedure acDRetExecute(Sender: TObject);
    procedure acDClientExecute(Sender: TObject);
    procedure acDCountryExecute(Sender: TObject);
    procedure acDAddressExecute(Sender: TObject);
    procedure acDSupNameExecute(Sender: TObject);
    procedure acReferenceBookExecute(Sender: TObject);
    procedure acSettingMainExecute(Sender: TObject);
    procedure acReportExecute(Sender: TObject);
    procedure acExportWithRestExecute(Sender: TObject);
    procedure acErrInPrordUpdate(Sender: TObject);
    procedure acAddClientFromFileUpdate(Sender: TObject);
    procedure acClearBaseUpdate(Sender: TObject);
    procedure acClearDelRecordsUpdate(Sender: TObject);
    procedure acEditFileUpdate(Sender: TObject);
    procedure acLoadReportUpdate(Sender: TObject);
    procedure acLoadSQLUpdate(Sender: TObject);
    procedure acExportLogUpdate(Sender: TObject);
    procedure acDelLogUpdate(Sender: TObject);
    procedure acExportUpdate(Sender: TObject);
    procedure acIsSell_RunExecute(Sender: TObject);
    procedure acListInventoryExecute(Sender: TObject);
    procedure acInsideInfoExecute(Sender: TObject);
    procedure acInfoEditArtHistExecute(Sender: TObject);
    procedure acInfoEditArtHistUpdate(Sender: TObject);
    procedure actApplListDepExecute(Sender: TObject);
    procedure acApplUpdate(Sender: TObject);
    procedure acDeleteActExecute(Sender: TObject);
    procedure acDDiscountSumExecute(Sender: TObject);
    procedure acEditBadRefUpdate(Sender: TObject);
    procedure acEditBadRefExecute(Sender: TObject);
    procedure acShopReportExecute(Sender: TObject);
    procedure acLogUpdate(Sender: TObject);
    procedure acLogExecute(Sender: TObject);
    procedure acWriteInfoLogExecute(Sender: TObject);
    procedure acWriteInfoLogUpdate(Sender: TObject);
    procedure acInsideInfoUpdate(Sender: TObject);
    procedure acDeleteActUpdate(Sender: TObject);
    procedure acAddArtCDMExecute(Sender: TObject);
    procedure acAddArtCDMUpdate(Sender: TObject);
    procedure acExportWithRestUpdate(Sender: TObject);
    procedure acExportWithRestNoReCalcUpdate(Sender: TObject);
    procedure acExportErrUpdate(Sender: TObject);
    procedure acExportErrExecute(Sender: TObject);
    procedure acSettingUpdate(Sender: TObject);
//    procedure acImportDepExecute(Sender: TObject);
    procedure acExportDepUpdate(Sender: TObject);
//    procedure acExportDepSellExecute(Sender: TObject);
//    procedure acAddSellExecute(Sender: TObject);
    procedure acSupInfExecute(Sender: TObject);
    procedure acSupInfUpdate(Sender: TObject);
    procedure actActAllowancesExecute(Sender: TObject);
    procedure acNodCardExecute(Sender: TObject);
    procedure acEquipmentExecute(Sender: TObject);
    procedure acUIDStoreDepExecute(Sender: TObject);
    procedure acUIDStoreUpdate(Sender: TObject);
    procedure acPaymentExecute(Sender: TObject);
    procedure acApplExecute(Sender: TObject);
    procedure acCountProssExecute(Sender: TObject);
    procedure acRespStoringExecute(Sender: TObject);
    procedure acRepairExecute(Sender: TObject);
    procedure acSuspItemExecute(Sender: TObject);
    procedure acPaymentUpdate(Sender: TObject);
    procedure acUserUidWHUpdate(Sender: TObject);
    procedure acUserUidWHExecute(Sender: TObject);
    procedure acIsMergeRunExecute(Sender: TObject);
    procedure acPriceAnalysisUpdate(Sender: TObject);
    procedure acWarehouseAnalysisUpdate(Sender: TObject);
    procedure NotSaleClick(Sender: TObject);
    procedure N52Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N53Click(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
    procedure N59Click(Sender: TObject);
    procedure siSertClick(Sender: TObject);
    procedure acSertExecute(Sender: TObject);
    procedure GenClick(Sender: TObject);
    procedure N76Click(Sender: TObject);
    procedure acBuyExecute(Sender: TObject);
    procedure N77Click(Sender: TObject);
    procedure miExperimentClick(Sender: TObject);
    procedure siCouponClick(Sender: TObject);
    procedure siIind_orderClick(Sender: TObject);
    procedure TimerIndOrderTimer(Sender: TObject);
    procedure N79Click(Sender: TObject);
    procedure N83Click(Sender: TObject);
    procedure N84Click(Sender: TObject);
  private
    FHotKey_UIDHist : Atom; // Shift+Ctrl+H
    DefaultSeparator: char;
    procedure WMHotKey(var Message : TMessage); message WM_HOTKEY;
    procedure NCRecieve(var Msg : TNCRecieve); message NC_Recieve;
    procedure CheckNewBuyOrder;
    function  create_fmDepSel : boolean;
  protected
    procedure WndProc(var Message: TMessage); override;

  public
    NumSellID:integer;
    NumErrClear, NumErr:integer;
    ActivFm: Boolean; // ��������� ���������� ����������
    Show_msg: boolean;
  end;

  TControlExeRep = procedure(ConnectDB: String); stdcall;

  TControlRepClose = procedure; stdcall;

var
  fmMain: TfmMain;
  ImpError : TStringList;
  Log_jew: TStringList;
  WM_MSG: Cardinal;
  Log_wh: TStringList;

  function CreateNewThread(hwndOwner: HWND; ConnectDB: string): boolean; stdcall; external 'DllControlJew.dll';
  procedure DestroyNewThread; stdcall; external 'DllControlJew.dll';

implementation

uses comdata, SList, RecSet, WH, DList, PrOrdLst, SellList,
  SellItem, OptList, Data, Rest, Ins, Mat, Good, Comp, Art, Country,
  Depart, NDS, Edg, PayType, Discount, DIns, AMerge, SRetLst, RetDict,
  SupInf, ORetList, Col, TmpDir, RetCltList, PHist, UIDHist,
  ReportData, UIDPHist, HistItem, ServData, List3, JSz, M207Proc, ArtDict,
  SQLMon, ErrRq, Client, FRest, Data2, Period, InsRepl, JUID, About,
  AplList, AM, A2M, SplashFR, err_sell,
  goods_sam, address, ExpFileClient, helpmain, WHAnlz,
  UIDStoreList, JDep, Data3, UidChgHst, dAtt1, dAtt2, SnameSup, dbUtil,
  Progress, MsgDialog, Inventory, pFIBQuery, FIBQuery, Dlg, JewConst,
  FIBDataSet, ListInventory, InsideInfo, InfoEditArtHist,  ApplDep,
  DiscountSum, ShopReportList, UtilLib, WriteLogBase, RepSetting,
  ActAllowancesList, Equipment, UidStoreDepList, Payment,
  RespStoring, RepairList, SuspItemList, UserUidWh, frmDialogJournalClient,
  NotSaleItems, uPluginClients, uClient, AnlzSelEmp, ErrClearDb, ControlSplash,
  frmAnalizeCDM, Sertificate, uUtils, ShlObj, GenReport, SelDep_anlz, DepAndDate,
  frmBuy, uDialog, frmAnalisisSalary, frmAnalisisClientSell, frmAnalisisDateSell,
  UIDWH, UIDWH2, frmCampaign, ind_order, IndividualOrderData, AnalizSkl, ActionST;

{$R *.DFM}

function RepDep(q:TpFIBDataSet):boolean;
begin
  Result:=CEnterDep or (q.FieldByName('D_DEPID').AsInteger=SelfDepId);
end;

procedure TfmMain.CheckNewBuyOrder;
var
  SQL: String;
  Value: Variant;
begin

  SQL := 'select count(*) from orders where state = 3 and depid in (-1000, :department$id)';

  Value := dmCom.db.QueryValue(SQL, 0, [SelfDepID]);

  if Value > 0 then
  begin
    TDialog.Information('������� �������� ������� �� ������: ' + VarToStr(Value) + ' ��.');
  end;

end;

procedure TfmMain.FormCreate(Sender: TObject);
var i: integer;
    b: boolean;
begin

  Screen.Cursor:=crHourGlass;

  //dmIndividualOrder  := nil;

   
//  try
//
//    try
//
//   dmIndividualOrder := TdmIndividualOrder.Create(nil);
//      
//   Screen.Cursor := crDefault;
//
//    except
//
//      On E: Exception do
//      begin
//
//        Screen.Cursor := crDefault;
//
//        MessageDlg('������ �������� �����:' + E.Message, mtError, [mbOk], 0);
//
//      end;
//
//    end;
//
//  finally
//
// 
//
//    FreeAndNil(dmIndividualOrder);   //
//
//    Screen.Cursor := crDefault;
//
//  end;





     try

    try
   dmIndividualOrder := TdmIndividualOrder.Create(nil);

     except

    on E: Exception do
    begin

    end;
    end;
   finally

    FreeAndNil(dmIndividualOrder);
    TimerIndOrder.Enabled:=False;
    TrayIcon.Active :=false;
   end; 

   TrayIcon.Active:=false;

   Screen.Cursor := crDefault;

  //UseIni:=true;
  ActivFm:=False;
  Show_msg := False;
  WM_MSG:= RegisterWindowMessage('WM_MSG');
  CreateNewThread(fmMain.Handle, dmcom.db.DBName);

  N34.Visible:=False;
  N53.Visible:=True;
  N76.Visible:=CenterDep;
  N77.Visible:=CenterDep;
  miExperiment.Visible:=CenterDep;


  wp:=tb1.Wallpaper;
  DefaultSeparator := DecimalSeparator;
  DecimalSeparator := ',';

  FHotKey_UIDHist := GlobalAddAtom('UIDHist#0');
  RegisterHotKey(Handle, FHotKey_UIDHist, MOD_CONTROL or MOD_SHIFT, Ord('H'));

  for i:=0 to acEvent.ActionCount-1 do
    begin
      if acEvent.Actions[i].Tag>=100 then
       b:=GetBit(dmCom.VIEWREFBOOK, (acEvent.Actions[i].Tag-100))
      else if acEvent.Actions[i].Tag = -1 then b:=true
      else
      begin
       b:=GetBit(dmCom.Acc, acEvent.Actions[i].Tag);
       if (acEvent.Actions[i].Name='actSList') or
          (acEvent.Actions[i].Name='actSRet') or
          (acEvent.Actions[i].Tag=10) or
          (acEvent.Actions[i].Tag=7) or
          (acEvent.Actions[i].Tag=26) or
          (acEvent.Actions[i].Tag=27) then b:=b and CenterDep;
      end;

      TAction(acEvent.Actions[i]).Visible:=b;
      TAction(acEvent.Actions[i]).Enabled:=b;
    end;
  NotSale.Visible:=CenterDep;
  {�����������������}
  b:=actWHArt.Visible or actWHUID.Visible;
  actWH.Visible:=b;
  actWH.Enabled:=b;
  b:=actSRet.Visible or actORet.Visible or actSellRet.Visible;
  actRet.Visible:=b;
  actRet.Enabled:=b;

  {*****************}

  ArrangeSB(tb1, 0);
  Caption:=dmCom.HereName+' - '+dmCom.UserName;
  sb1.Panels[1].Text:=dmCom.db.DatabaseName;


  if CenterDep then NNewDepClear.Visible:=false
  else NNewDepClear.Caption:='������ ���� �� '+ dmCom.HereName+ '�';

  if not dmCom.tr.Active then dmCom.tr.StartTransaction
  else dmcom.tr.CommitRetaining;

  if not dmcom.Seller then
  begin
   if not CenterDep then b:=dm2.CheckUnclosedInv
   else b:=dm2.CheckUnConfirmedPrord;
   dmCom.tr.CommitRetaining;
   b:=b or dm2.CheckDeleteAct;
   if b then actPrOrd.Execute;
  end;

  if CenterDep then
  begin
    dm2.CheckUnclosedAppl;
  end else
  begin
    CheckNewBuyOrder;
  end;

  NLog.Checked:=dmcom.IsMakeLog;

  TrayIcon.Active:=True;

end;

procedure TfmMain.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;

  siIind_order.Left:=siCoupon.left + 64;
end;

procedure TfmMain.GenClick(Sender: TObject);
begin
  ShowAndFreeForm(Tform4, Self, TForm(form4), True, False);
end;

procedure TfmMain.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.siIind_orderClick(Sender: TObject);
var
 OrderForm: TDialogOrder;
begin

  Screen.Cursor:=crHourGlass;

  orderForm := nil;

  try

    try
      // if not (assigned(dmIndividualOrder))  then

      dmIndividualOrder := TdmIndividualOrder.Create(nil);     

      if  dmIndividualOrder.Connection.Connected  then

      begin
      OrderForm := TDialogOrder.Create(Self);

      TimerIndOrder.Enabled:=true;
      
      OrderForm.ShowModal;

      end
      else 
         MessageDlg('����������� � ������� ����������.���������� ������������ �������', mtError, [mbOk], 0);
      
    except

      On E: Exception do
      begin

        Screen.Cursor := crDefault;

        MessageDlg('������ �������� �����:' + E.Message, mtError, [mbOk], 0);

      end;

    end;

  finally

    FreeAndNil(OrderForm);

   // FreeAndNil(dmIndividualOrder);   

    Screen.Cursor := crDefault;

  end;


end;

procedure TfmMain.siSertClick(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�����������','-1000');
  ShowAndFreeForm(TfmSertificate, Self, TForm(fmSertificate), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.TimerIndOrderTimer(Sender: TObject);
begin

Screen.Cursor := crHourGlass;

if  ((dmIndividualOrder.Connection.Connected) and (assigned(dmIndividualOrder))) or ((dmIndividualOrder.Connection.Connected) and not(assigned(dmIndividualOrder))) then
 Begin

 dmIndividualOrder.COUNT_PREWIEW.Close;
 dmIndividualOrder.COUNT_PREWIEW.ParamByName('DEP').AsString := dmCom.HereName;
 dmIndividualOrder.COUNT_PREWIEW.Open;

 if dmIndividualOrder.COUNT_PREWIEW.FieldByName('COUNT_PREWIEW').AsInteger <> 0 then
   Begin
   TrayIcon.Active:=True;
   TrayIcon.Animated:=True;
   dmIndividualOrder.COUNT_PREWIEW.First;
   TrayIcon.Hint:='';
   while not dmIndividualOrder.COUNT_PREWIEW.Eof do
    begin
      TrayIcon.Hint:= TrayIcon.Hint+dmIndividualOrder.COUNT_PREWIEW.FieldByName('DEP_Custom').AsString+', ';
      dmIndividualOrder.COUNT_PREWIEW.Next;
    end;
   End
 else
   Begin
   TrayIcon.Active :=false;
   TrayIcon.Animated:=false;
   End;
 End
 else Begin TimerIndOrder.Enabled:=False;  TrayIcon.Active :=false; end;
Screen.Cursor := crDefault;
end;

procedure TfmMain.WMHotKey(var Message: TMessage);
begin
  {case Message.WParam of
    FHotKey_UIDHist : begin
                        dm.UID2Find := 0;
                        ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
                      end;
  end;}
end;

procedure TfmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  GlobalDeleteAtom(FHotKey_UIDHist);
  UnRegisterHotKey(Handle, FHotKey_UIDHist);
  Action := caFree;
  DecimalSeparator := DefaultSeparator;
end;

procedure TfmMain.actSListExecute(Sender: TObject);
var LogOperationID: string;
begin
 LogOperationID:=dm3.insert_operation('��������','-1000');

 if not CenterDep then sysutils.Abort;
 ShowAndFreeForm(TfmSList, Self, TForm(fmSList), True, False);

 dm3.update_operation(LogOperationID);

 if dm.PActAfterSInv then
  if MessageDialog('��������� ��������� ���. �������� ���� ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes then actPrOrd.Execute;

end;

procedure TfmMain.actPrOrdExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�������','-1000');
  ShowAndFreeForm(TfmPrOrdList, Self, TForm(fmPrOrdList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actWHArtExecute(Sender: TObject);
var
    LogOperationID:string;
begin
  dm.quTmp.Close;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  LogOperationID:=dm3.insert_operation('����� �� ����������','-1000');
  ShowAndFreeForm(TfmWH, Self, TForm(fmWH), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actWHUIDExecute(Sender: TObject);
var i:integer;
    LogOperationID:string;
begin
  ///////
  dm.quTmp.Close;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  with dm, quTmp do
   begin
    SQL.Text:='SELECT UIDWHCALC FROM D_REC';
    ExecQuery;
    i:=Fields[0].AsInteger;
    Close;
    if i>0 then raise Exception.Create('��������� ������ ����������� ������ �������������.')
    else  begin
     LogOperationID:=dm3.insert_operation('����� �� ��������','-1000');

     ShowAndFreeForm(TfmUIDWH, Self, TForm(fmUIDWH), True, False);

     fmUIDWH := nil;

     dm3.update_operation(LogOperationID);
    end
   end;
end;

procedure TfmMain.actDListExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('���������� �����������','-1000');
  ShowAndFreeForm(TfmDList, Self, TForm(fmDList), True, False);
  dm3.update_operation(LogOperationID);

  if dm.PActAfterDInv then
  if MessageDialog('��������� ��������� ���. �������� ���� ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes then actPrOrd.Execute;

end;

procedure TfmMain.actSellListExecute(Sender: TObject);
var LogOperationID:string;
begin
  LogOperationID:=dm3.insert_operation('�������','-1000');
  ShowAndFreeForm(TfmSellList, Self, TForm(fmSellList), True, False);
  if dmcom.IsActSellList then
    if MessageDialog('��������� ��������� ���. �������� ���� ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes then actPrOrd.Execute;
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actCurSellExecute(Sender: TObject);
var col:integer;
    LogOperationID:string;
    ISSell_Run:boolean;
begin
  with dmCom do
    begin
      if ((not User.AllWh) and (User.DepId<>SelfDepId)) then
             raise Exception.Create('������������ �� ����� ������� �����');
      if not tr.Active then tr.StartTransaction;

      qutmp.Close;
      qutmp.SQL.Text:='select issell_run from d_emp where  d_empid = '+inttostr(User.UserId);
      qutmp.ExecQuery;
      ISSell_Run:=boolean(qutmp.Fields[0].AsInteger);
      qutmp.Transaction.CommitRetaining;

      if ISSell_Run then raise Exception.Create('� ����� ������ ����� ������ ����� � ��� �� �������������!');
      LogOperationID:=dm3.insert_operation('�����','-1000');
      ExecSQL('update d_emp set issell_run=1 where d_empid = '+inttostr(User.UserId), dmCom.quTmp);
      ShowAndFreeForm(TfmSellItem, Self, TForm(fmSellItem), True, False);
      ExecSQL('update d_emp set issell_run=0 where d_empid = '+inttostr(User.UserId), dmCom.quTmp);
      tr.CommitRetaining;
{      if dm.PActAfterSell then
        if MessageDialog('��������� ��������� ���. �������� ���� ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes then actPrOrd.Execute;}
    if NumSellID>0 then
      begin
       with dm, qutmp do
       begin
        if not tr.Active then tr.StartTransaction;
        sql.Text:='select FlagOpenAct from DefineOpenActSell_(' + inttostr (NumSellID) +')';
        ExecQuery;
        col:= Fields[0].AsInteger;
        close;
        tr.CommitRetaining;
       end;
       if col>0 then
       begin
        if dmcom.Seller then MessageDialog('��������� ��������� ���.', mtInformation, [mbYes], 0)
        else if MessageDialog('��������� ��������� ���. �������� ���� ����������?', mtWarning, [mbYes, mbNo], 0)=mrYes then actPrOrd.Execute;
       end;
     end;

     dm3.update_operation(LogOperationID);
   end;
end;

procedure TfmMain.actWHExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.actRetExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.actSRetExecute(Sender: TObject);
var
  LogOperationID : string;
begin
  LogOperationID := dm3.insert_operation(sLog_SRet, '-1000');
  ShowAndFreeForm(TfmSRetList, Self, TForm(fmSRetList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actORetExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ORet, '-1000');
  ShowAndFreeForm(TfmORetList, Self, TForm(fmORetList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actSellRetExecute(Sender: TObject);
var
  LogOperationID : string;
begin
  LogOperationID := dm3.insert_operation(sLog_SellRet, '-1000');
  ShowAndFreeForm(TfmRetCltList, Self, TForm(fmRetCltList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actOptExecute(Sender: TObject);
var
  LogOperationId: string;
begin
  LogOperationID := dm3.insert_operation(sLog_OptSell,'-1000');
  ShowAndFreeForm(TfmOptList, Self, TForm(fmOptList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actRestExecute(Sender: TObject);
var
  LogOperationID : string;
begin
  LogOperationID := dm3.insert_operation(sLog_Rest, '-1000');
  ShowAndFreeForm(TfmRest, Self, TForm(fmRest), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actFRestExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_FRest, '-1000');
  ShowAndFreeForm(TfmFRest, Self, TForm(fmFRest), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.N56Click(Sender: TObject);
begin
  ShowAndFreeForm(TfmAbout, Self, TForm(fmAbout), True, False);
end;

procedure TfmMain.actApplListExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplList, '-1000');
  ShowAndFreeForm(TfmApplList, Self, TForm(fmApplList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.actHelpExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmHelpMain, Self, TForm(fmHelpMain), True, False);
end;

procedure TfmMain.NNewDepClearClick(Sender: TObject);
begin
end;

{test dataset after migration to dialect 3}
procedure TfmMain.Button2Click(Sender: TObject);

  procedure TestDataSet (dm : TDataModule);
  var
    i : integer;
  begin
    for i:=0 to Pred(dm.ComponentCount) do
      if dm.Components[i].InheritsFrom(TpFIbDataSet) then
        try                                                   
          TpFIBDataSet(dm.Components[i]).Close;
          TpFIBDataSet(dm.Components[i]).Open;
          TpFIBDataSet(dm.Components[i]).Close;
        except
          //Memo1.Lines.Add(dm.Name+'.'+dm.Components[i].Name);
        end
    end;

begin
  //Memo1.Clear;
  TestDataSet(dmcom);
  TestDataSet(dm);
  TestDataSet(dm2);
  TestDataSet(dm3);
  TestDataSet(dmServ);
  TestDataSet(dmReport);
  TestDataSet(dmFR);
end;


const
  cInitialDir = '����������';

procedure TfmMain.acLoadReportExecute(Sender: TObject);

  function LoadDoc(Id : integer; FileName : string):boolean;
  begin
    Result := True;
    with dmReport do
    try
      frReport.LoadFromFile(FileName);
      if not taDoc.Transaction.Active then taDoc.Transaction.StartTransaction;
      taDoc.Open;
      try
        if not taDoc.Locate('ID', Id, []) then
        begin
          taDoc.Insert;
          taDocID.AsInteger := Id;
          taDoc.Post;
          taDoc.Transaction.CommitRetaining;
        end;
        frReport.SaveToDB(taDoc, Id);
      finally
        taDoc.Close;
        taDoc.Transaction.Commit;
      end;
    except
      on E:Exception do begin
        Result := False;
        MessageDialog('������: '+E.Message, mtError, [mbOk], 0);
      end;
    end;
  end;

var
  Dir, Ids : string;
  i, Id : integer;
begin
  Dir := ExtractFileDir(Application.ExeName)+'\'+cInitialDir;
  if not DirectoryExists(Dir) then CreateDir(Dir);
  dlgOpenFile.InitialDir := Dir;
  dlgOpenFile.Filter := '�������� �����|*.frf';
  dlgOpenFile.Options := dlgOpenFile.options+[ofAllowMultiSelect, ofFileMustExist];

  if not dlgOpenFile.Execute then eXit;
  Ids:='';
  for i:=0 to Pred(DlgOpenFile.Files.Count) do
  begin
    Id := StrToIntDef(ExtractFileName(ChangeFileExt(DlgOpenFile.Files[i], '')), -1);
    if(Id = -1)then
    begin
      if MessageDialog('�������� ������ �����'+DlgOpenFile.Files[i]+'. ���������� ��������?', mtWarning, [mbYes, mbNo], 0)=mrNo then Exit;
    end
    else begin
      if LoadDoc(Id, DlgOpenFile.Files[i]) then Ids := Ids+IntToStr(Id)+',';
    end;
  end;
  if(Ids<>'')then
  begin
    Ids := copy(Ids, 1, Length(Ids)-1);
    MessageDialog('�������� ����� '+Ids+' ������� ���������', mtInformation, [mbOk], 0);
  end;
end;

procedure TfmMain.acLoadSQLExecute(Sender: TObject);
var
  SaveDir : string;

  procedure H(Prefix : string; E : Exception; ErrKind : byte);
  var
    fs : TFileStream;
    err, errFileName : string;
    Buffer : array [0..1023] of char;
    i, pos : integer;
  begin
    // ������� log ������ � �����
    case ErrKind of
      0 : err := Prefix+E.Message;
      1 : err := Prefix+E.Message;
      2 : err := Prefix+E.Message+#13'SQLCode: '+dmCom.quLoadUpd.SQL.Text;
      3 : err := Prefix+E.Message+#13'SQLCode: '+dmCom.quTmp.SQL.Text;
      4 : err := Prefix+E.Message;
      5 : err := Prefix+E.Message;
    end;
    errFileName := dlgOpenFile.FileName+'.err';
    fs := TFileStream.Create(errFileName, fmCreate);
    try
      i:=1;
      pos := 0;
      while (i<=Length(err)) do
      begin
        Buffer[pos]:=err[i];
        inc(i);
        if (pos = 1024) then
        begin
          fs.Write(Buffer, pos);
          pos := 0;
        end
        else inc(pos);
      end;
      fs.Write(Buffer, pos-1);
    finally
      fs.Free;
    end;
    case ErrKind of
      0 : dmCom.bk.Active := False;
      1 : dmCom.rr.Active := False;
      2 : begin
        dmCom.quLoadUpd.Close;
        if dmCom.trUpd.Active then dmCom.quLoadUpd.Transaction.Rollback;
        dmCom.dbUpdate.Close;
      end;
      3 : begin
        dmCom.quTmp.Close;
        if dmCom.trUpd.Active then dmCom.quTmp.Transaction.Rollback;
      end;
      4 : dmCom.dbUpdate.Close;
      5 : ;
    end;
    Screen.Cursor := crDefault;
    MessageDialog('��� ��������� ���������� �������� ������.'+#13+
       '���������� ��������� ���� '+errFileName+' �������������.'#13+
       '������: '+err, mtError, [mbOk], 0);
    SetCurrentDir(SaveDir);
    SysUtils.Abort;
  end;

  procedure _CopyFile(f1, f2 : string; Overwrite: boolean);
  begin
    SetProgress('���������� ��...');
    try
       if not CopyFile(PChar(f1), PChar(f2), Overwrite) then
         raise Exception.Create('������ ��� ����������� �� #'+IntToStr(GetLastError));
    finally
      UnProgress;
    end;
  end;

  function StatementNull(s : string) : boolean;
  var
    i : integer;
  begin
    Result := True;
    for i:=1 to Length(s) do
      if s[i]>#32 then
      begin
        Result := False;
        eXit;
      end;
  end;

const
  cExt = 'gem';   // ���������� ����� ����������
  cSetBlob = 'SET BLOBFILE'; // ���������� ��� �������� ���� �����

var
  Dir, dbName, RestoreDbName, BlobFileName : string;
  SQLScript : TStringList;
  fs, BlobData : TFileStream;
  Buffer : array[0..1023] of char;
  i, j, nread : integer;
  pStart, pStartValid : integer;
  sBuffer, stmt : string;
  fBlobContent, FirstRead : boolean;
  SQLSeparator : string;
  p1, p2 : Int64;
  ms : TMemoryStream;
begin
  SaveDir := GetCurrentDir;
  dbName := dmCom.db.DatabaseName;
  if not FileExists(dbName) then
    raise Exception.Create('���������� �������� ������ ��� ��������� ���� ������');
  Dir := ExtractFileDir(Application.ExeName)+'\'+cInitialDir;
  if not DirectoryExists(Dir) then CreateDir(Dir);

  dlgOpenFile.InitialDir := Dir;
  dlgOpenFile.Filter := '����������|*.'+cExt;
  if not dlgopenFile.Execute then
  begin
    SetCurrentDir(SaveDir);
    eXit;
  end;
  Dir := ExtractFileDir(dlgopenFile.FileName);
  SetCurrentDir(Dir);
  Application.ProcessMessages;
  // �������� ��������� ������
  fs := TFileStream.Create(dlgOpenFile.FileName, fmOpenRead  or fmShareDenyNone);
  SQLScript := TStringList.Create();
  try
    SetProgress('��������� �������');
    nread := -1;
    stmt := '';
    FirstRead := True;
    fBlobContent := False;
    while(nread <> 0) do
    begin
      FillChar(Buffer, SizeOf(Buffer), #0);
      nread := fs.Read(Buffer, SizeOf(Buffer));
      // ��������� ������
      sBuffer := Buffer;
      // �������� ����������, �������� �� ������ ����������� ���� �����
      if FirstRead then
      begin
        fBlobContent := Pos(cSetBlob, AnsiUpperCase(sBuffer))<>0;
        if not fBlobContent then SQLSeparator := '***---***' else SQLSeparator := ';';
        Firstread := False;
      end;
      pStart := Pos(SQLSeparator, sBuffer);
      while(pStart <> 0)do
      begin
        stmt := stmt + copy(sBuffer, 1, pStart-1);
        // ��������� ��� �� ����������� SQLSeparator � stmt
        // ����� ��������, ���� SQLSeparator �������� �� ������� ������ ����������� �������
        // ������� SQLSeparator �� ��� ���� ������
        pStartValid := Pos(SQLSeparator, stmt);
        if(pStartValid <> 0)then
        begin
          SQLScript.Add( copy(stmt, 1, pStartValid-1) );
          SQLScript.Add( copy(stmt, pStartValid+Length(SQLSeparator), Length(stmt)) );
        end
        else SQLScript.Add(stmt);
        stmt := '';
        //Application.ProcessMessages;
        sBuffer := copy(sBuffer, pStart+Length(SQLSeparator), Length(sBuffer));
        pStart := Pos(SQLSeparator, sBuffer);
      end;
      if(nread <> 0)then stmt := stmt + copy(sBuffer, 1, Length(sBuffer))
      else if(not StatementNull(stmt))then SQLScript.Add(stmt);
    end;
  finally
    FreeAndNil(fs);
    UnProgress;
  end;
  // �������� ��������� ��� ����� � ����-�������
  if fBlobContent then
  try
    pStart := Pos(#39, SQLScript[0]);
    if(pStart = 0) then raise Exception.Create('��������� ��������� ������� �������� ����-������.'+#13+SQLScript[0]);
    BlobFileName := '';
    inc(pStart);
    while(pStart<Length(SQLScript[0]))do
    begin
      if SQLScript[0][pStart]=#39 then break
      else BlobFileName := BlobFileName+SQLScript[0][pStart];
      inc(pStart);
    end;
    BlobFileName := ExtractFileDir(dlgOpenFile.FileName)+'\'+ExtractFileName(BlobFileName);
    if not FileExists(BlobFileName)then raise Exception.Create('����� � ����-������� �� ����������.'+#13+BlobFileName);
  except
    on E: Exception do H('Parse Blobfilename.', E, 5);
  end;

   // �������� ������� ��
  RestoreDbName := Dir+'\'+ExtractFileName(dmCom.db.DatabaseName);
  if FileExists(RestoreDbName) then
  begin
    if MessageDialog('�������� ����� ��?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
      _copyfile(dmCom.db.Databasename, RestoreDbName, False)
  end
  else _copyfile(dmCom.db.Databasename, RestoreDbName, False);

      (* ������� ��������� ���������� �� ����� ������� �� *)
  dmCom.dbUpdate.Close;
  dmCom.dbUpdate.DBName := RestoreDbName;
  try
    dmCom.dbUpdate.Open;
  except
    on E: Exception do H('Connect.',E,4);
  end;
  Application.ProcessMessages;

  if fBlobContent then
  begin
    BlobData := TFileStream.Create(BlobFileName, fmOpenRead or fmShareDenyNone);
    dmCom.quLoadUpd.ParamCheck := True;
  end;
  SetProgress('���������� ����� ������� ��...');
  try
    for i:=0 to Pred(SQLScript.Count) do
    begin
      if fBlobContent and (i=0)then continue;
      Screen.Cursor := crSQLWait;
      with dmCom do
      try
        if quLoadUpd.Transaction.Active then quLoadUpd.Transaction.Commit;
        quLoadUpd.Transaction.StartTransaction;
        quLoadUpd.Close;
        quLoadUpd.SQL.Clear;
        quLoadUpd.SQL.Add(SQLScript[i]);
        try
          quLoadUpd.Prepare;
        except
          on E : Exception do begin
            H('TryPrepare.',E, 2);
          end;
        end;
        try
          if fBlobContent then
          begin
            // �������� ��������� ����� ����������
            for j:=0 to Pred(quLoadUpd.Params.Count) do
            begin
              if(AnsiUpperCase(quLoadUpd.Params[j].Name[1]) <> 'H')then
                raise Exception.Create('�������� ������ ����� ��������� '+quLoadUpd.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              p1 := StrToIntDef(
                 '$'+copy(quLoadUpd.Params[j].Name, 2, pos('_', quLoadUpd.Params[j].Name)-2),
                 -1);
              if(p1=-1)then raise Exception.Create('�������� ������ ����� ��������� '+quLoadUpd.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              p2 := StrToIntDef(
                 '$'+copy(quLoadUpd.Params[j].Name, pos('_', quLoadUpd.Params[j].Name)+1, Length(quLoadUpd.Params[j].Name)),
                 -1);
              if(p2=-1)then
                raise Exception.Create('�������� ������ ����� ��������� '+quLoadUpd.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              // ������� ������ �� �����
              ms := TMemoryStream.Create;
              try
                BlobData.Position := p1;
                ms.CopyFrom(BlobData, p2);
                quLoadUpd.Params[j].LoadFromStream(ms);
              finally
                FreeAndNil(ms);
              end;
            end;
          end;
          {if quLoadUpd.Transaction.Active then quLoadUpd.Transaction.Commit;
          quLoadUpd.Transaction.StartTransaction;}
          quLoadUpd.ExecQuery;
          quLoadUpd.Close;
          quLoadUpd.Transaction.Commit;
        except
          on E : Exception do
          begin
            H('TryExecute.',E, 2);
          end;
        end;
        Application.ProcessMessages;
      finally
        Screen.Cursor := crDefault;
        if quLoadUpd.Transaction.Active then quLoadUpd.Transaction.Rollback;
      end;
    end;
  finally
    if fBlobContent then FreeAndNil(BlobData);
    UnProgress;
  end;
  Application.ProcessMessages;


  // ���� ��� ���������� ����� ������ ��������� � ������� ��
  if fBlobContent then
  begin
    BlobData := TFileStream.Create(BlobFileName, fmOpenRead or fmShareDenyNone);
    dmCom.quTmp.ParamCheck := True;
  end;
  SetProgress('���������� ������� ��... ');
  try
    for i:=0 to Pred(SQLScript.Count) do
    begin
      if fBlobContent and (i=0)then continue;
      Screen.Cursor := crSQLWait;
      with dmCom do
      try
        quTmp.Close;
        if quTmp.Transaction.Active then quTmp.Transaction.Commit;
        quTmp.Transaction.StartTransaction;
        quTmp.SQL.Clear;
        quTmp.SQL.Add(SQLScript[i]);
        quTmp.Prepare;
        try
          if fBlobContent then
          begin
            // �������� ��������� ����� ����������
            for j:=0 to Pred(quTmp.Params.Count) do
            begin
              if(AnsiUpperCase(quTmp.Params[j].Name[1]) <> 'H')then
                raise Exception.Create('�������� ������ ����� ��������� '+quTmp.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              p1 := StrToIntDef(
                 '$'+copy(quTmp.Params[j].Name, 2, pos('_', quTmp.Params[j].Name)-2),
                 -1);
              if(p1=-1)then raise Exception.Create('�������� ������ ����� ��������� '+quTmp.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              p2 := StrToIntDef(
                 '$'+copy(quTmp.Params[j].Name, pos('_', quTmp.Params[j].Name)+1, Length(quTmp.Params[j].Name)),
                 -1);
              if(p2=-1)then
                raise Exception.Create('�������� ������ ����� ��������� '+quTmp.Params[j].Name+'.'#13+
                  '������ �����: h<�����>_<�����>');
              // ������� ������ �� �����
              ms := TMemoryStream.Create;
              try
                BlobData.Position := p1;
                ms.CopyFrom(BlobData, p2);
                quTmp.Params[j].LoadFromStream(ms);
              finally
                FreeAndNil(ms);
              end;
            end;
          end;
          quTmp.ExecQuery;
          quTmp.Close;
          quTmp.Transaction.Commit;
          Application.ProcessMessages;
        except
          on E : Exception do H('Execute.',E, 3);
        end;
      finally
        Screen.Cursor := crDefault;
        if quTmp.Transaction.Active then quTmp.Transaction.Rollback; 
      end;
    end;
  finally
    if fBlobContent then FreeAndNil(BlobData);
    UnProgress;
  end;
  // ������� ����� ��
  DeleteFile(RestoreDbName);
  SetCurrentDir(SaveDir);
  MessageDialogA('���������� ������� ���������!', mtInformation);
end;

procedure TfmMain.acExportExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acInfoUIDInsHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDInsHist, '-1000');
  ShowAndFreeForm(TfmUChHist, Self, TForm(fmUChHist), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acAtt1Execute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-�������1','-1000');

  with dmCom do
  begin
    if not tr.Active then tr.StartTransaction;
    b:=GetBit(dmCom.EditRefBook, 14) and CenterDep;
    ShowAndFreeFormEnabled(TfmdAtt1, Self, TForm(fmdAtt1), True, False, b, 'siHelp;acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt1;');
    tr.CommitRetaining;
  end;

 dm3.update_operation(LogOperationID)
end;

procedure TfmMain.acAtt2Execute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-�������2','-1000');

  with dmCom do
  begin
    if not tr.Active then tr.StartTransaction;
    b:=GetBit(dmCom.EditRefBook, 15) and CenterDep;
    ShowAndFreeFormEnabled(TfmdAtt2, Self, TForm(fmdAtt2), True, False, b, 'siHelp;acClose;siExit;tb1;spitPrint;siSort;', 'gridAtt2;');
    tr.CommitRetaining;
  end;

 dm3.update_operation(LogOperationID)
end;

procedure TfmMain.NCRecieve(var Msg: TNCRecieve);
begin
  //StrPas(Msg.Text);
end;

procedure TfmMain.HelpUserClick(Sender: TObject);
var path:string;
begin
 path:=ExtractFilePath(Application.ExeName)+'\help';
 ShellExecute(Handle, 'open', 'hh.exe','jewhelp.chm', pchar(path), SW_SHOWNORMAL);
end;

procedure TfmMain.HeplProgrammClick(Sender: TObject);
begin
{ Application.HelpFile:=ExtractFilePath(Application.ExeName)+'\help\helpprog.hlp' ;
 Application.HelpCommand(HELP_INDEX,0);}
end;

procedure TfmMain.acUIDStoreExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStore, '-1000');
  ShowAndFreeForm(TfmUIDStoreList, Self, TForm(fmUIDStoreList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoChangePriceExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ChangePrice, '-1000');
  ShowAndFreeForm(TfmPHist, Self, TForm(fmPHist), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoArtHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  dmServ.Mode := '������� �������. ������� ����';
  dm.FindArt:='';
  LogOperationID := dm3.insert_operation(sLog_ArtHist, '-1000');
  ShowAndFreeForm(TfmHistItem, Self, TForm(fmHistItem), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoArtMergeHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_AMHist, '-1000');
  ShowAndFreeForm(TfmAM, Self, TForm(fmAM), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoArt2MergeHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_A2MHist, '-1000');
  ShowAndFreeForm(TfmA2M, Self, TForm(fmA2M), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoUIDHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  if not dmCom.tr.Active then dmCom.tr.StartTransaction;
  dm.UID2Find:=-1;
  LogOperationID := dm3.insert_operation(sLog_UIDHist, '-1000');
  ShowAndFreeForm(TfmUIDHist, Self, TForm(fmUIDHist), True, False);
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoUIDPriceHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  if not dmCom.tr.Active then dmCom.tr.StartTransaction;
  dm.UID2Find:=-1;
  LogOperationID := dm3.insert_operation(sLog_UIDPriceHist, '-1000');
  ShowAndFreeForm(TfmUIDPHist, Self, TForm(fmUIDPHist), True, False);
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acSettingExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_Setting, '-1000');
  ShowAndFreeForm(TfmRecSet, Self, TForm(fmRecSet), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acColorSchemaExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ColorSchema, '-1000');
  ShowAndFreeForm(TfmCol, Self, TForm(fmCol), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acSertExecute(Sender: TObject);
var
  LogOperationID: string;
begin
    LogOperationID := dm3.insert_operation(sLog_Setting1, '-1000');
  ShowAndFreeForm(TfmSertificate, Self, TForm(fmSertificate), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acSetting1Execute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_Setting1, '-1000');
  ShowAndFreeForm(TfmTmpDir, Self, TForm(fmTmpDir), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acAnalizSzExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  Flag_Per:=false;
  LogOperationID := dm3.insert_operation(sLog_AnalizSz, '-1000');
  ShowAndFreeForm(TfmJSz, Self, TForm(fmJSz), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acAnalizDepExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_AnalizDep, '-1000');
  ShowAndFreeForm(TfmJDep, Self, TForm(fmJDep), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acPriceAnalysisExecute(Sender: TObject);
var I_Bd: TDateTime;
    I_Ed: TDateTime;
    d,m,y : word;
    Info, DInf,Item,Att, FilterN, ItemFilter : IXMLNode;
    i,j: integer;
    LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_PriceAnalysis, '-1000');
  DecodeDate(dmCom.GetServerTime, y, m, d);
  if (m = 1)  then
  begin
   m := 13;
   y := y-1;
  end;
  if (m = 2)  then
  begin
   m := 14;
   y := y-1;
  end;

  I_Bd := EncodeDate(y, m-2, d);
  I_Ed := dmCom.GetServerTime;
  GetPeriod(I_BD,I_ED);
  with dmServ do
  begin

    XMLSell.Active := True;
    Info := XMLSell.DocumentElement.AddChild('������');

    Att := XMLSell.CreateNode('���������_����', ntAttribute);
    Att.NodeValue := I_BD;
    Info.AttributeNodes.Add(Att);

    Att := XMLSell.CreateNode('��������_����', ntAttribute);
    Att.NodeValue := I_ED;
    Info.AttributeNodes.Add(Att);

    FilterN := XMLSell.DocumentElement.AddChild('FILTER');

    ItemFilter:=  FilterN.AddChild('�������������');
    for j:=0 to Pred(dmCom.DepCount) do
       ItemFilter.AddChild('�����').NodeValue := dmCom.DepInfo[j].SName;


    quDiscount.ParamByName('I_BD').AsDate := I_BD;
    quDiscount.ParamByName('I_ED').AsDate := I_ED;

    OpenDataSets([quDiscount]);
    ItemFilter:=  FilterN.AddChild('������');
    while not quDiscount.Eof do
    begin
      ItemFilter.AddChild('������').NodeValue := quDiscount.Fields[0].AsString;
      quDiscount.Next;
    end;
    CloseDataSets([quDiscount]);


    for j:=0 to Pred(dmCom.DepCount) do
    begin
      quSellArt.ParamByName('I_BD').AsDate := I_BD;
      quSellArt.ParamByName('I_ED').AsDate := I_ED;
      quSellArt.ParamByName('I_Dep').AsInteger := dmCom.DepInfo[j].DepId;
      OpenDataSets([quSellArt]);

      DInf := XMLSell.DocumentElement.AddChild('����������_�������');
      Att := XMLSell.CreateNode('�������', ntAttribute);
      Att.NodeValue := dmCom.DepInfo[j].SName;
      DInf.AttributeNodes.Add(Att);

      while not quSellArt.Eof do
      begin
        Item :=  DInf.AddChild('�������');
        for i:=0 to Pred(quSellArt.FieldCount) do
            Item.AddChild(quSellArt.Fields[i].FieldName).NodeValue := quSellArt.Fields[i].AsString;
        quSellArt.Next;
      end;

      CloseDataSets([quSellArt]);

    end;


    XMLSell.SaveToFile('D:\!!\ArtSellPrice.xml');
    XMLSell.Active := False;
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acWarehouseAnalysisExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_WarehouseAnalysis, '-1000');
  ShowAndFreeForm(TfmSellWH, Self, TForm(fmSellWH), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDepExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_Depart, '-1000');
  ShowAndFreeForm(TfmDepart, Self, TForm(fmDepart), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acExportLogExecute(Sender: TObject);
var NeedSPR, NeedSPR1, PacketId:integer;
    Dest :string;
    prc:integer;
begin
  with dm, dm3 do
  if GetPeriod(RBD, RED) then
  begin
   quDest.ExecQuery;
   Dest := quDest.Fields[0].AsString;
   quDest.Transaction.CommitRetaining;

   NeedSPR:=0;
   NeedSPR1:=0;
   prc:=0;

   InsRPacket(dmCom.db, 'DESCRIPTION', Dest , 0, '', '������ ��������', 1,NeedSPR, PacketId);


   {����������������}
    with quSelectLog do
    begin
     Prepare;
     Active:=True;
     while not EOF do
     begin
      InsRPacket(dmCom.db, 'LOG', Dest, 2,
                 quSelectLogL_USERID.AsString, '���������������� �'+quSelectLogL_USERID.AsString, 1,NeedSPR1, PacketId);
      with quSetLogRState, Params do
      begin
       ByName['L_USERID'].AsInteger:=quSelectLogL_USERID.AsInteger;
       ByName['RSTATE'].AsInteger:=1;
       ExecQuery;
       Transaction.CommitRetaining;
      end;
      Inc(prc);
      Next;
     end;
     Active:=False;
    end;

    InsRPacket(dmCom.db, 'DESCRIPTION', Dest, 0, '', '������ ��������', 1,NeedSPR, PacketId);

    if PacketCount>0 then
    begin
     if MessageDialog('������ � ������� ���������� � �������'+NL+
                   ' �����: '+IntToStr(prc)  , mtInformation, [mbOK,mbCancel], 0)  = mrCancel then
       begin
         with   transltmp  do
         begin
           if not Transaction.Active then Transaction.StartTransaction ;
           SQL.Text  := 'delete from   R_Packet where packetstate = 0';
           ExecQuery;
           Transaction.CommitRetaining ;
         end;
       end;
    end
    else MessageDialog('��� ������ ��� ��������', mtInformation, [mbOK], 0);
 end
end;

procedure TfmMain.acDelLogExecute(Sender: TObject);
begin
 Screen.Cursor:=crSQLWait;
 ExecSQL('delete from log$user  where rstate > 2 and l_userid<>-1000', dmCom.quTmp);
 Screen.Cursor:=crDefault;
end;

procedure TfmMain.acUidWhCalcExecute(Sender: TObject);
begin
 with dmCom do
    try
      Screen.Cursor:=crSQLWait;
      if not tr.Active then tr.StartTransaction;
      quTmp.SQL.Text:='execute PROCEDURE enduidwhcalc '+IntToStr(UserId);
      quTmp.ExecQuery;
      tr.CommitRetaining;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmMain.acEndSellCalcExecute(Sender: TObject);
begin
 with dmCom do
    try
      Screen.Cursor:=crSQLWait;
      if not tr.Active then tr.StartTransaction;
      quTmp.SQL.Text:='Update d_rec set  SellElCalc  = 0  ';
      quTmp.ExecQuery;
      tr.CommitRetaining;
    finally
      Screen.Cursor:=crDefault;
    end;
end;

procedure TfmMain.acErrPrPriceExecute(Sender: TObject);
begin
 with dmServ.quPriceCheck do
  begin
    try
      Screen.Cursor := crSQLWait;
      if not Transaction.Active then Transaction.StartTransaction;
      Close;
      SQL.Text := 'Select * from CHECKPRICEINPR';
      ExecQuery;
      Close;
      Transaction.CommitRetaining;
      Transaction.Commit;
    finally
      Screen.Cursor := crDefault;
    end;
    MessageDialog('������ � ����� �� �������� �������� ���� ����������', mtInformation, [mbYes], 0);
  end
end;

procedure TfmMain.acCnfExecute(Sender: TObject);
var  ND: TDateTime;
     i: integer;
     sqlstr: string;
begin
  if Low(dmcom.mas_dep)<> 0  then  finalize(dmcom.mas_dep);

  dmcom.flcall_delsel:=1;
  ND := dmCom.GetServerTime  - 4;
  if create_fmDepSel then
    for i:=0 to length(dmcom.mas_dep)-1 do
    begin
      sqlstr := ' Update Prord ' +
                '    set rstate_p = 4, ' +
                '        rstate_a = 3 ' +
                ' where ((rstate_p < 4) or (rstate_a<3)) ' +
                '  and  depid = ' + inttostr(dmcom.mas_dep[i]) + { ��������������� ��� ������� ��������� �������������}
                '  and  setdate < '#39 + datetimetostr(nd) + #39;
      ExecSQL(sqlstr, dm2.quTmp);
    end;
  MessageDialog('������������� �����������', mtInformation, [mbOk], 0);
end;

procedure TfmMain.acAdminExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acErrSellBusyTypeExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmErr_sell, Self, TForm(fmErr_sell), True, False);
end;

procedure TfmMain.acErrBrackingExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acErrInSellExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acErrInPrordExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acClearBaseExecute(Sender: TObject);
const mask=100000000;
var i, j:integer;
    st:string;
    max_mask, Limit_mask, max_:longint;

 procedure define_mask;
 begin
  Limit_mask:= ((i div mask)+1)*mask;
  with dm, qutmp do
  begin
   close;
   case j of
    1: sql.Text:='select max(selid) from sel where selid<'+inttostr(Limit_mask);
    2: sql.Text:='select max(prordid) from prord where prordid<'+inttostr(Limit_mask);
    3: sql.Text:='select max(sinvid) from sinv where sinvid<'+inttostr(Limit_mask);
    4: sql.Text:='select max(sellid) from sell where sellid<'+inttostr(Limit_mask);
   end;
   ExecQuery;
   max_mask:=Fields[0].AsInteger;
   Transaction.CommitRetaining;
   close;
  end;
 end;

begin
  with dm, qutmp do
  begin
   st := fmMain.Caption;
   if not Transaction.Active then Transaction.StartTransaction;
   close;

   (*��������
     0-������� �� ������� SITEM,
     1-��������� � ��������� �� ������� SEL,
     2-�������� �� ������� Prord, PrordItem, Prsi,
     3-��������� �� ������� SINV,
     4-������ �� ������� SELL*)
//   for j:=0 to 4 do
   if ShowAndFreeForm(TfmErrClearDb, Self, TForm(fmErrClearDb), True, False)= mrOK then
   begin
   for j:=NumErr to 5 do
   begin
    case j of
    0: sql.Text := 'select max(uid) from sitem';
    1: sql.Text := 'select max(selid) from sel';
    2: sql.Text := 'select max(prordid) from prord';
    3: sql.Text := 'select max(sinvid) from sinv';
    4: sql.Text := 'select max(sellid) from sell';
    5: sql.Text := 'select max(uid) from uid_info';
    end;
    ExecQuery;
    max_ := Fields[0].AsInteger;
    close;


    Screen.Cursor:=crSQLWait;
    if j=NumErr then i:=NumErrClear else i:=0;

    if (j=1)or(j=2)or(j=3)or(j=4) then begin
     define_mask;
     if i>max_mask then begin
      i:=Limit_mask;
      define_mask;
     end;
    end;


    while i<=max_ do
     begin
      if (j=0) and ((i=17608)or(i=17702)) then inc(i);
      case j of
       0: fmMain.Caption:='������ UID '+inttostr(i);
       1: fmMain.Caption:='������ SEL '+inttostr(i);
       2: fmMain.Caption:='������ PRORD '+inttostr(i);
       3: fmMain.Caption:='������ SINV '+inttostr(i);
       4: fmMain.Caption:='������ SELL '+inttostr(i);
       5: fmMain.Caption:='������ UID_INFO '+inttostr(i);
      end;

      if (i>max_mask)and((j=1)or(j=2)or(j=3)or(j=4)) then begin
       i:=Limit_mask;
       define_mask;
      end;

      sql.Text:='execute procedure clear_db_2 ('+ inttostr(j)+','+inttostr(i)+', '+inttostr(i)+')';
      ExecQuery;

      Transaction.CommitRetaining;
      close;
      i:=i+1;
     end;
    end;
   end;
  end; 
    Screen.Cursor:=crDefault;
    fmMain.Caption:=st;
end;

procedure TfmMain.acClearDelRecordsExecute(Sender: TObject);
begin
  if AppDebug then
    ExecSQL('delete from delrecords where rstate>2 ', dmCom.quTmp);
end;

procedure TfmMain.acAddClientFromFileExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmExpFileClient, Self, TForm(fmExpFileClient), True, False);
end;

procedure TfmMain.acEditFileExecute(Sender: TObject);
var
  ftext, TmpFile:textfile;
  currStr, Path: string;
begin
 if not dlgopenFile.Execute then
  begin
    eXit;
  end;

 Path:=dlgopenFile.FileName;
 if not FileExists(Path) then
 begin
   ImpError.Add('���� '+Path+' �� ����������!');
   eXit;
 end;

 AssignFile(ftext, Path);
 AssignFile(TmpFile, Application.GetNamePath + '2.0.4.21.gem');
 ReWrite(TmpFile);
 Reset(ftext);
 try
  while not eof(ftext) do
  begin
   readln(ftext,currStr);
   if (ExtractWord(1,currStr,[' '])<> 'COMMIT')then
   begin
    Writeln(TmpFile,currStr);
    Writeln(TmpFile,'***---***');
   end
  end;
 finally
  CloseFile(ftext);
  CloseFile(TmpFile);
  MessageDialog('�������� � ���� ���������!', mtInformation, [mbOk], 0);
 end;
end;

procedure TfmMain.acInformationExecute(Sender: TObject);
begin
//
end;


procedure TfmMain.acDMatExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-���������','-1000');

  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      b:=GetBit(dmCom.EditRefBook, 0) and CenterDep;
      ShowAndFreeFormEnabled(TfmMat, Self, TForm(fmMat), True, False, b, 'siHelp;siExit;tb1;spitPrint;siSort;', 'dg1;');
      tr.CommitRetaining;
    end;

 dm3.update_operation(LogOperationID);
end;


procedure TfmMain.acDGoodExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:= dm3.insert_operation('�����������-������','-1000');

  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      b:=GetBit(dmCom.EditRefBook, 1) and CenterDep;
      ShowAndFreeFormEnabled(TfmGood, Self, TForm(fmGood), True, False, b, 'siHelp;siExit;tb1;spitPrint;siSort;','dg1;');
      tr.CommitRetaining;
    end;

 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDGoodsSamExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-���. ����������','-1000');
 b:=GetBit(dmCom.EditRefBook, 2) and CenterDep;
 ShowAndFreeFormEnabled(Tfmgoods_sam, Self, TForm(fmgoods_sam), True, False, b,'siHelp;siExit;tb1;spitPrint;siSort;','dg1;');
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDInsExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-�������','-1000');

  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      b:=GetBit(dmCom.EditRefBook, 3) and CenterDep;
      ShowAndFreeFormEnabled(TfmDIns, Self, TForm(fmDIns), True, False, b, 'siHelp;siExit;tb1;spitPrint;siSort;','dg1;');
      tr.CommitRetaining;
    end;

 dm3.update_operation(LogOperationID)
end;

procedure TfmMain.acDCompExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-�����������','-1000');
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      b:=GetBit(dmCom.EditRefBook, 4) and CenterDep;
      ShowAndFreeFormEnabled(TfmComp, Self, TForm(fmComp), True, False, b,
        'siHelp;siExit;tb1;spitPrint;siSort;edCode;siCategory;tb2;PageControl1;'+
        'pc1;TabSheet1;TabSheet2;TabSheet3;TabSheet4;TabSheet5;TabSheet6;TabSheet7;'+
        'Panel1;tsMol;mnitAll;st1;mnitCur;N6;N1;N2;N3;N4;N5;edCode;chAllWorkOrg;'+
        'laCompCat;Label36;Splitter1;',
        'dg1;M207IBGrid1;');
      tr.CommitRetaining;
    end;

 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDArtExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-��������','-1000');
 if not dmCom.tr.Active then dmCom.tr.StartTransaction;
 b:=GetBit(dmCom.EditRefBook, 5) and CenterDep;
 ShowAndFreeFormEnabled(TfmArtDict, Self, TForm(fmArtDict), True, False, b,
    'siHelp;siExit;tb1;Panel3;Panel1;paDict;Panel6;', 'dgArt;dgA2;');
 dmCom.tr.CommitRetaining;
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDNDSExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-���','-1000');
 b:=GetBit(dmCom.EditRefBook, 6) and CenterDep;
 ShowAndFreeFormEnabled(TfmNDS, Self, TForm(fmNDS), True, False, b, 'siHelp;siExit;tb1;spitPrint;', 'dg1;');
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDEDGETIONExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-�������','-1000');
 b:=GetBit(dmCom.EditRefBook, 7) and CenterDep;
 ShowAndFreeFormEnabled(TfmEdg, Self, TForm(fmEdg), True, False, b,'siHelp;siExit;tb1;spitPrint;pc1;ts1;ts2;', 'dg1;dg2;');
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDPayTypeExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-���� �����','-1000');
 b:=GetBit(dmCom.EditRefBook, 8) and CenterDep;
 ShowAndFreeFormEnabled(TfmPayType, Self, TForm(fmPayType), True, False, b,'siHelp;siExit;tb1;spitPrint;', 'dg1;');
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDDiscountExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-������','-1000');
 b:=GetBit(dmCom.EditRefBook, 9) and CenterDep;
 ShowAndFreeFormEnabled(TfmDiscount, Self, TForm(fmDiscount), True, False, b,'siHelp;siExit;tb1;spitPrint;', 'dg1;');
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDRetExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-������� ��������','-1000');

 with dmCom do
   begin
     if not tr.Active then tr.StartTransaction;
     b:=GetBit(dmCom.EditRefBook, 10);
     ShowAndFreeFormEnabled(TfmRetDict, Self, TForm(fmRetDict), True, False, b,'siHelp;spitPrint;siExit;tb1;', 'dg1;');
     taRet.Active:=False;
     tr.CommitRetaining;
   end;
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDClientExecute(Sender: TObject);
begin

// LogOperationID:=dm3.insert_operation('�����������-�����������','-1000');
{ with dmCom do
   begin
     if not tr.Active then tr.StartTransaction;
     //ShowAndFreeForm(TfmClient, Self, TForm(fmClient), True, False);
     ShowAndFreeForm(TDialogJournalClient, Self, TForm(DialogJournalClient), True, False);
     tr.Commit;
   end;  }
   try
    PluginClients := nil;

    PluginClients := TPluginClients.Create(dmcom.db);

    PluginClients.Clients.View.Show;

    if not PluginClients.Active then
    begin
      ShowMessage('����������� ������.'#13#10'���������� ���������������� ���������� ��������.');
      Abort;
    end;
  except
    on E: Exception do
    begin
      ShowMessage('����������� ������.'#13#10'���������� ���������������� ���������� ��������.');
      Abort;
    end;
  end;
// dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDCountryExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-������','-1000');

 with dmCom do
   begin
     if not tr.Active then tr.StartTransaction;
     b:=GetBit(dmCom.EditRefBook, 12) and CenterDep;
     ShowAndFreeFormEnabled(TfmCountry, Self, TForm(fmCountry), True, False, b,'siHelp;siExit;tb1;spitPrint;siSort;', 'dg1;');
     tr.CommitRetaining;
   end;

 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDAddressExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean; 
begin
 LogOperationID:=dm3.insert_operation('�����������-������','-1000');

 with dmCom do
  begin
   if not tr.Active then tr.StartTransaction;
   b:=GetBit(dmCom.EditRefBook, 13);
   ShowAndFreeFormEnabled(TfmAddress, Self, TForm(fmAddress), True, False, b,'siHelp;siPrint;siExit;tb1;', 'dg1;');
   tr.CommitRetaining;
  end;

 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acDSupNameExecute(Sender: TObject);
var LogOperationID: string;
    b:boolean;
begin
 LogOperationID:=dm3.insert_operation('�����������-������. �����������','-1000');

  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      b:=GetBit(dmCom.EditRefBook, 16) and CenterDep;
      ShowAndFreeFormEnabled(TfmSnameSup, Self, TForm(fmSnameSup), True, False, b,
       'siHelp;acclose;siExit;tb1;achelp;siSup;sitable;tb2;', 'dg1;');
      tr.CommitRetaining;
    end;

 dm3.update_operation(LogOperationID)
end;

procedure TfmMain.acReferenceBookExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acSettingMainExecute(Sender: TObject);
begin
//
end;

procedure TfmMain.acReportExecute(Sender: TObject);
begin
//
end;
function TfmMain.create_fmDepSel : boolean;
begin
  if CenterDep then
  begin
    fmDepSel:=TfmDepSel.Create(NIL);
    try
      with fmDepSel do
        if ShowModal=mrOK  then result:=true
        else result:=false;
    finally
      fmDepSel.Free;
    end;
  end
 else
  result:=true;
end;
{*************** ������� � ��������� ****************}
procedure TfmMain.acExportWithRestExecute(Sender: TObject);
var i, dc, prc, sc, rc, nds, ppr, paa, pad, ap : integer;
    s, tf, sterr: string;
    shm, flSell:boolean;
    depcurid:integer;
    NeedSPR, PacketIdClient, PacketIdAddress, PacketId :integer;
    NeedSPR_mas:t_array_int;
    STPRORD, STACT:string;
    LogOperationID: string;
    DllInstance: THandle;
    ControlExeRep: TControlExeRep;
    ControlRepClose: TControlRepClose;
    path: string;
    BuyClassName: String;
    BuyInvoiceCount: Integer;
    BuyOrderCount: Integer;
    InvoiceDepartment : String;

// ��������� ������

procedure ReplicateBuyInvoice;
var
  SourceID: string;
  TargetID: string;
  ID: string;
  N: string;
  ClassName: string;
  ClassCode: Integer;
  PacketName: string;
  DataSet: TpFIBDataSet;
  SourceDepartmentID: string;
  TargetDepartmentID: Integer;
  db:TpFIBDatabase;
  sValue: Variant;
begin
  db := dmCom.db;

  sValue := db.QueryValue('select first 1 tablename from r_item where r_item = :r_item', 0, ['BUY_INVOICE']);

  if VarIsNull(sValue) then
  begin
    exit;
  end;


  BuyInvoiceCount := 0;

  SourceID := InsRepl.HereId;

  if CenterDep then
  begin
    TargetID := InsRepl.slDep.Values[IntToStr(dm.pmRepl.Items[2].Tag)]
  end else
  begin
    TargetID := InsRepl.slDep.Values[IntToStr(dm.pmRepl.Items[i].Tag)];
  end;

  DataSet := dm2.quRBuyInvoice;

  if DataSet.Active then
  begin
    DataSet.Active := False;
  end;

  TargetDepartmentID := DataSet.Database.QueryValue('select ncode from r_dep where id = :id', 0, [TargetID]);

  DataSet.ParamByName('Target$Department$ID').AsInteger := TargetDepartmentID;

  try

    DataSet.Active := True;

    DataSet.First;

    if DataSet.RecordCount <> 0 then
    begin

      while not DataSet.eof do
      begin
        ClassCode := DataSet.FieldByName('Class$Code').AsInteger;

        case ClassCode of

          2:  ClassName := '���������. �����';

          3:  ClassName := '���������. �����������. ��������';

          4:  ClassName := '���������. �����������. �����';

          5:  ClassName := '���������. �������. ��������';

          6:  ClassName := '���������. �������. �����';

          7:  ClassName := '���������. �����������. ��������';

          8:  ClassName := '���������. �����������. �����';

          9:  ClassName := '���������. �������';

         10:  ClassName := '���������. �������';

        end;

        ID :=  DataSet.FieldByName('ID').AsString;

        SourceDepartmentID := DataSet.FieldByName('SOURCE$DEPARTMENT$ID').AsString;

        SourceID := slNDep.Values[SourceDepartmentID];

        N :=  DataSet.FieldByName('N').AsString;

        PacketName := ClassName + ' � ' + N;

        InsRPacket(dmCom.db, 'BUY_INVOICE', TargetID,  2, ID, PacketName, 1, NeedSPR, PacketID, SourceID);

        Inc(BuyInvoiceCount);

        DataSet.Next;
      end;

    end;

    DataSet.Active := False;

  except

    on E:Exception do
    begin
      TDialog.Error(E.Message);

      if DataSet.Active then
      begin
        DataSet.Active := False;
      end;

    end;

  end;
end;

// ��������� ������


// ������� �� ������

procedure ReplicateBuyOrder;
var
  SourceID: string;
  TargetID: string;
  ID: string;
  N: string;
  PacketName: string;
  DataSet: TpFIBDataSet;
  db:TpFIBDatabase;
  sValue: Variant;
begin
  db := dmCom.db;

  sValue := db.QueryValue('select first 1 tablename from r_item where r_item = :r_item', 0, ['BUY_ORDER']);

  if VarIsNull(sValue) then
  begin
    exit;
  end;

  BuyOrderCount := 0;

  SourceID := InsRepl.HereId;

  TargetID := InsRepl.slDep.Values[IntToStr(dm.pmRepl.Items[i].Tag)];

  DataSet := dm2.quRBuyOrder;

  if DataSet.Active then
  begin
    DataSet.Active := False;
  end;

  DataSet.ParamByName('Source$ID').AsString := SourceID;

  DataSet.ParamByName('Target$ID').AsString := TargetID;

  try

    DataSet.Active := True;

    DataSet.First;

    if DataSet.RecordCount <> 0 then
    begin

      while not DataSet.eof do
      begin
        ID :=  DataSet.FieldByName('ID').AsString;

        N :=  DataSet.FieldByName('N').AsString;

        PacketName := '���������. ������' +  ' � ' + N;

        InsRPacket(dmCom.db, 'BUY_ORDER', TargetID,  2, ID, PacketName, 1, NeedSPR, PacketID);

        Inc(BuyOrderCount);

        DataSet.Next;
      end;

    end;

    DataSet.Active := false;

  except

    on E:Exception do
    begin
      TDialog.Error(E.Message);

      if DataSet.Active then
      begin
        DataSet.Active := False;
      end;

    end;

  end;

end;

begin
  log_jew:=TStringList.Create;
  dmcom.flcall_delsel:=2;
  setlength(dmcom.mas_dep,0);

  DestroyNewThread;

  case (Sender as TAction).Tag of
    19 : LogOperationID := dm3.insert_operation(sLog_ExportWithRest, '-1000');
    20 : begin
         LogOperationID := dm3.insert_operation(sLog_ExportNoRest, '-1000');
         Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������� ��� ��������');
         end;
    21 : LogOperationID := dm3.insert_operation(sLog_ExportWithRestNoReCalc, '-1000');
  end;

  if create_fmDepSel then
  try
    DllInstance:=LoadLibrary('DllControlRep.dll');
    ControlExeRep := GetProcAddress(DllInstance, 'CreateNewThread');
    //ControlExeRep(dmCom.db.DBName);

  with dm do
  begin
  //  DllInstance:=LoadLibrary('DllControlRep.dll');
 //   ControlExeRep := GetProcAddress(DllInstance, 'CreateNewThread');
  //  ControlExeRep(dmCom.db.DBName);

    sleep(5000);
    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ��������');
    with qutmp do
    begin
      if not CenterDep then
      begin
        close;
        sql.Text := 'select name, d_depid, enabled from GET$R$DEPARTMENTS';
        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' select name, d_depid, enabled from GET$R$DEPARTMENTS');
        ExecQuery;
        close;
        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' enabled = '+Fields[2].AsString);
        if Fields[2].AsInteger <> 1 then
        begin

           showmessage('������� �� ������������ ����� � �����������. ' +
                       '����� ����, ��� ����� ����� ����������, ������� ' +
                       '� ������������, ����� ����� ������������ ����������.');
   //        ControlRepClose := GetProcAddress(DllInstance, 'DestroyNewThread');
   //        ControlRepClose;
   //        FreeLibrary(DllInstance);
           exit;
         Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+'������� �����. ����� �� ��������.');
        end;
      end;
      quTmp.Close;
      for i:=0 to length(dmcom.mas_dep)-1 do
      begin
        if not Transaction.Active then Transaction.StartTransaction;
        sql.Text := 'update d_dep set r_usedep = 0 where d_depid = '+inttostr(dmcom.mas_dep[i]);
        ExecQuery;
        Transaction.CommitRetaining;
        close;
        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' update d_dep set r_usedep = 0 where d_depid = '+inttostr(dmcom.mas_dep[i]));
      end;
      for i:=0 to length(dmcom.mas_dep)-1 do
      begin
        if not Transaction.Active then Transaction.StartTransaction;
        sql.Text := 'update r_dep set usedep = null where ncode = '+inttostr(dmcom.mas_dep[i]);
        ExecQuery;
        Transaction.CommitRetaining;
        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' update d_dep set r_usedep = 0 where d_depid = '+inttostr(dmcom.mas_dep[i]));
      end;
    end;

    with quTmp do
    begin
      if not Transaction.Active then Transaction.StartTransaction;
      s:=' in (';
      SQL.Text:='select NCode from R_Dep where UseDep = 1';
        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' select NCode from R_Dep where UseDep=1');
      ExecQuery;
      while not EOF do
      begin
        s:=s+Fields[0].AsString+',';
        Next;
      end;
      Close;
      s:=s+'-999) ';
      Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' s='+s);
      dm2.quRPrOrd.SelectSQL[7]:=' DepID '+ s;
      dm2.quRDInv.SelectSQL[7]:=' DepID ' + s;
      dm2.quRUid_info.SelectSQL[7]:=' s.DepID '+ s;
      dm2.quRSINFO.SelectSQL[3]:=' DepID '+s;
      dm2.quRDelRec.SelectSQL[3]:='and s.DepID '+s;
      dm2.quRActAllowances.SelectSQL[7]:=' and s.DepFromID '+s;
      dm2.quRApplDep.SelectSQL[7]:=' and s.DepID '+s;
      dm2.quRApplDepArt.SelectSQL[7]:=' and s.DepID '+s;
      dm2.quRPactC.SelectSQL[3]:=' and DepID '+s;
      Transaction.CommitRetaining;
    end;

    for i:= dm.pmRepl.Items.Count-1 downto 2 do  pmRepl.Items.Delete(i);
    FillReplMenu(dmCom.db, pmRepl, ReplClick);

    if GetPeriodForRepl(RBD, RED) then
    begin
       if not dmCom.tr.Active then dmCom.tr.StartTransaction
          else dmCom.tr.CommitRetaining;
        dc:=0;
        prc:=0;
        sc:=0;
        rc:=0;
        nds:=0;
        ppr:=0;
        paa:=0;
        pad:=0;
        ap:=0;
        PacketCount:=0;

        BuyInvoiceCount := 0;

        BuyOrderCount := 0;

        s:='������� �� '+DateToSTr(dmCom.GetServerTime);
        NeedSPR:=0;
        if CenterDep then
          begin
           SetLength(NeedSPR_mas,0);
            for i:=2 to pmRepl.Items.Count-1 do
              begin
                InsRPacket(dmCom.db, 'DESCRIPTION', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 0, '', '������ ��������', 1,NeedSPR, PacketId);

                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������ ��������');

                SetLength(NeedSPR_mas,length(NeedSPR_mas)+1);
                NeedSPR_mas[length(NeedSPR_mas)-1]:=NeedSPR;
                NeedSPR:=0;
                InsRPacket(dmCom.db, 'D_DEP', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���. �����������', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���. �����������');
                InsRPacket(dmCom.db, 'D_PAYTYPE', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ����� ������', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ����� ������');
                InsRPacket(dmCom.db, 'D_COMP', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� �����������', 1,NeedSPR, PacketId);

                InsRPacket(dmCom.db, 'CONTRACT_TYPES', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ����� ���������', 1,NeedSPR, PacketId);

                InsRPacket(dmCom.db, 'CONTRACT', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���������', 1,NeedSPR, PacketId);

                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� �����������');
                InsRPacket(dmCom.db, 'D_EDGETION', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���� � ����� �������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���� � �����');
                InsRPacket(dmCom.db, 'D_MAT', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ����������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ����������');
                InsRPacket(dmCom.db, 'D_GOOD', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ������������', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ������������');
                InsRPacket(dmCom.db, 'D_INS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� �������', 1,NeedSPR, PacketId);
                   //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� �������');

                InsRPacket(dmCom.db, 'D_COUNTRY', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� �����', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���');
                InsRPacket(dmCom.db, 'D_NDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���', 1 ,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���');
                InsRPacket(dmCom.db, 'D_ATT1', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���������� 1 ��������', 1, NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���������� 1 ��������');
                InsRPacket(dmCom.db, 'D_ATT2', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���������� 2 ��������', 1, NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���������� 2 ��������');
                InsRPacket(dmCom.db, 'AM', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '����������� 1-� ���������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ����������� 1-� ���������');
                InsRPacket(dmCom.db, 'A2M', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '����������� 2-� ���������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ����������� 2-� ���������');
                ExecSQL('delete from rep_sinfo where rstate>2 ', dmCom.quTmp);
                  //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' delete from rep_sinfo where rstate>2');
                InsRPacket(dmCom.db, 'SINFO',InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 6,
                            InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], '��������� � ���������', 1,NeedSPR, PacketId);
                           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��������� � ���������');

                {��������� ������� �� ������������}
                ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
                InsRPacket(dmCom.db, 'DELRECORDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 4,
                InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)]+';0;', '��������� ������ �� �����-�� ���������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��������� ������ �� �����-�� ���������');
                InsRPacket(dmCom.db, 'D_ART', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���������');
                InsRPacket(dmCom.db, 'ART2', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ������ ���������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ������ ���������');
                 {���������� ������� � ��������}
                if Centerdep then
                InsRPacket(dmCom.db, 'INS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '��������� ������� � ��������', 1,NeedSPR, PacketId);
                {=============================}
                InsRPacket(dmCom.db, 'D_GOODS_SAM', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ����������', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ����������');
                InsRPacket(dmCom.db, 'D_EMP', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� �������������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� �������������');
                InsRPacket(dmCom.db, 'D_DISCOUNT', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ������', 1,NeedSPR, PacketId);
                 //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ������');
                InsRPacket(dmCom.db, 'D_RET', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ������ ���������', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ������ ���������');
                InsRPacket(dmCom.db, 'D_MOL', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '���������� ���', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ���');
                InsRPacket(dmCom.db, 'D_DISCOUNTSUM', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '�������� �����', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' �������� �����');
                 InsRPacket(dmCom.db, 'SERTIFICATE', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '�����������. �����', 1,NeedSPR, PacketId);

                 InsRPacket(dmCom.db, 'SERT_INFO', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '�����������. ������ � ��������', 1,NeedSPR, PacketId);

                 InsRPacket(dmCom.db, 'SERT_SELL', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 1, '', '�����������. ������� ���������', 1,NeedSPR, PacketId);

                //log_jew.Add(DateTimeToStr(dmcom.GetServerTime) + ' ' + IntToStr(PacketId) + ' '+ ' �����������.�����');

                InsRPacket(dmCom.db, 'CAMPAIGN', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 11, '', '�����', 1,NeedSPR, PacketId);



               PacketIdClient:=-1;
               InsRPacket(dmCom.db, 'CLIENT', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 9, '', '���������� �����������', 1,NeedSPR, PacketIdClient);

           //     Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� �����������');
           //    InsRPacket(dmCom.db, 'D_ADDRESS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 7,
            //   InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], '���������� ���� (����. ������)', 1,NeedSPR, PacketId);
            //   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ���������� ����');
               InsRPacket(dmCom.db, 'RESPSTORING', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 7,
               InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], '������������� ��������', 1,NeedSPR, PacketId);
           //    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ������������� ��������');


               {��������� ��������  � �������� ��������� �������}
               InsRPacket(dmCom.db, 'EDTSEL', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)],5,
               InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], '��������� ���-� �������', 1,NeedSPR, PacketId);
           //    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��������� ���-� �������');
               {��������� ������� �� ������ � ���������}
                ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' delete from DELRECORDS where rstate>=3');
                InsRPacket(dmCom.db, 'DELRECORDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 4,
                InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)]+';1;', '��������� ������ �� �����-�� � ������.', 1,NeedSPR, PacketId);
                //Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��������� ������ �� �����-�� � ������.');

         // ������� �� ������

           ReplicateBuyOrder;

           // ������� �� ������


           // ��������� ������

           ReplicateBuyInvoice;

           // ��������� ������



              end;

            {��������� �����������}
            (*****************************)

            with dm2, quRDInv do
              begin
                Active:=True;

                while not EOF do
                  begin
                    InsRPacket(dmCom.db, 'DINV', slNDep.Values[quRDInvDepId.AsString], 2,
                               quRDInvSInvId.AsString, '��������� ������� �'+quRDInvSN.AsString, 1,NeedSPR, PacketId);
//                    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��������� ������� �'+quRDInvSN.AsString);

                    Inc(dc);
                    Next;
                  end;
                Active := False;
              end;

              // �������� ����������
             with dmcom, dm2, quTmp do
              begin
                for i:=2 to pmRepl.Items.Count-1 do
                begin
                 Close;
                 SQL.Text:='execute procedure REP_UID_INFO('''+
                            InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)]+''','''
                            +datetimetostr(RBD)+''','''+
                            datetimetostr(RED)+''')';
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' execute procedure REP_UID_INFO('''+
                            InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)]+''','''
                            +datetimetostr(RBD)+''','''+
                            datetimetostr(RED)+''')');

                 ExecQuery;
                 tr.CommitRetaining;
                 close;
                 s := '���������� �� ������� �� ���������';
                 InsRPacket(dmCom.db, 'REP_UIDINFO', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 7, InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], s , 1,NeedSPR, PacketId);
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' '+s);
                end;
              end;

             //���������� �� ���������� ������ ���� ���������
             with dm2, quRDInv do
              begin
                Active := True;
                while not EOF do
                  begin
                    with quSetInvRState, Params do
                      begin
                        ByName['INVID'].AsInteger := quRDInvSInvId.AsInteger;
                        ByName['RSTATE'].AsInteger := 1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' �� ����������: INVID='+quRDInvSInvId.AsString+' RSTATE=1');
                        ExecQuery;
                      end;
                    Next;
                  end;
                Active := False;
              end;
            (*********************************************)

           {�������� �������� �� ��������}
            STPRORD:='';
            for i:=2 to pmRepl.Items.Count-1 do
            with dm2, quRBeforePrord do
              begin
                Prepare;
                ParamByName('DEPN').AsString:=InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)];
                Active:=True;
                if trim(quRBeforePrordSTPRORD.AsString)<>'' then
                   STPRORD:=STPRORD+#13#10+trim(quRBeforePrordSTPRORD.AsString)+' (��'+
                   trim(quRBeforePrordSNVP.AsString)+')';
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' stprord'+stprord);
                Active:=False;
              end;

           {�������}
            with dm2, quRPrOrd do
              begin
                Prepare;
                Active:=True;
                while not EOF do
                  begin
                    InsRPacket(dmCom.db, 'PRORD', slNDep.Values[quRPrOrdDepId.AsString], 2,
                         quRPrOrdPrOrdId.AsString, '������ �'+quRPrOrdPrOrd.AsString, 1,NeedSPR, PacketId);
                     Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ������ �'+quRPrOrdPrOrd.AsString);
                    with quSetPrOrdRState, Params do
                      begin
                        ByName['PRORDID'].AsInteger:=quRPrOrdPrOrdId.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        ExecQuery;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' PRORDID='+quPrordPrordId.AsString+' RSTATE=1');
                      end;
                    Inc(prc);
                    Next;
                  end;
                Active:=False;
              end;

           { ������� � ���������� ����� }
          { ReOpenDataSets([dm2.quRprOrd2]);
           while not dm2.quRprOrd2.Eof do
           begin
             InsRPacket(dmCom.db, 'PRORDTYPE', slNDep.Values[dm2.quRprOrd2DEPID.AsString], 8,
               dm2.quRPrOrd2PRORDID.AsString, '������ �'+dm2.quRprOrd2PRORD.AsString, 1, NeedSPR, PacketId);
             inc(prc);
             ExecSQL('update Prord set RState_P1=1 where PrordId='+dm2.quRprOrd2PRORDID.AsString, dmCom.quTmp);
             dm2.quRprOrd2.Next;
           end;
           CloseDataSets([dm2.quRprOrd2]);  }

          {���� ��������}
           with dm2, quRActAllowances do
           begin
            Prepare;
            Active:=True;
            while not EOF do
             begin
              InsRPacket(dmCom.db, 'ACTALLOWANCES', slNDep.Values[quRActAllowancesDEPFROMID.AsString], 2,
                         quRActAllowancesSINVID.AsString, '��� �������� �'+quRActAllowancesSN.AsString, 1,NeedSPR, PacketId);
                         Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' ��� �������� �'+quRActAllowancesSN.AsString);
               with quSetActAllowances, Params do
                begin
                 ByName['SINVID'].AsInteger:=quRActAllowancesSINVID.AsInteger;
                 ByName['RSTATE'].AsInteger:=1;
                 Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' SINVID='+quRActAllowancesSinvid.AsString+' RSTATE=1');
                 ExecQuery;
                end;
               Inc(paa);
               Next;
              end;
            Active:=False;
           end;

          {���� ����������� ������� ��� �� ������ ������ (itype=9)}
           with dm2, quRPactC do
           begin
            Prepare;
            Active:=True;
            while not EOF do
             begin
              InsRPacket(dmCom.db, 'PACT', slNDep.Values[quRPactCDEPID.AsString], 2,
                         quRPactCPRORDID.AsString, '���. ��� �'+quRPactCNACT.AsString, 1,NeedSPR, PacketId);
                 Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntTostr(PacketId)+' ���. ��� �'+quRPactCDEPID.AsString+' RSTATE=1');
               with quSetPActRState, Params do
                begin
                 ByName['PRORDID'].AsInteger:=quRPactCPRORDID.AsInteger;
                 ByName['RSTATE'].AsInteger:=1;
                 Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' PRORDID='+quRPActCPrordId.AsString+' RSTATE=1');
                 ExecQuery;
                end;
               Inc(prc);
               Next;
              end;
            Active:=False;
           end;


            if ((Sender as TAction).Tag = 19) or ((Sender as TAction).Tag = 21) then
             begin
               Screen.Cursor:=crSQLWait;

               if ((Sender as TAction).Tag=19) then
               with dmCom, dm2, quTmp do
                 begin
                   close;
                   SQL.Text:='execute procedure FillFUidRest(0,' + IntToStr(300) + ')';
                  Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' execute procedure FillFUidRest(0,'+IntToStr(300)+')');
                   ExecQuery;
                   Transaction.CommitRetaining;
                   Close;
                 end;

              for i:=2 to pmRepl.Items.Count-1 do
                begin
                { TODO 5 : ���������� �������� �������-��������� ��� ��������� }
                  s := '������� ��-�������� �� '+DateToSTr(dmCom.GetServerTime);
                  InsRPacket(dmCom.db, 'FUIDREST', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 3, '', s , 1,NeedSPR, PacketId);
                  Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+S);
                end;
              end;

           for i:=2 to pmRepl.Items.Count-1 do
           begin
            NeedSPR:=NeedSPR_mas[i-2];
            InsRPacket(dmCom.db, 'DESCRIPTION', InsRepl.slDep.Values[IntToStr(pmRepl.Items[i].Tag)], 0, '', '������ ��������', 1,NeedSPR, PacketId);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������ ��������');
           end;

            /////////////////**************************
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ����������� �������'+IntToStr(PacketCount));
           // if PacketCount>0 then
            //begin
               if STPRORD<>'' then MessageDialog('�� ����� ���� �������� ������� '+STPRORD, mtWarning, [mbOk], 0);
               if MessageDialog('������ � ������� ���������� � �������'+NL+
                             '��������� ��: '+IntToStr(dc)+NL+
                             '�������� � ���������� ��� � ���������: '+IntToStr(nds)+NL+
                             '�������: '+IntToStr(prc)+NL+
                             '���� ��������: '+IntToStr(paa) + NL +
                             '��������� ������: ' + IntToStr(BuyInvoiceCount) + NL +
                             '������� �� ������: ' + IntToStr(BuyOrderCount), mtInformation, [mbYes,mbNo], 0) = mrNo then
               begin
                 with   transltmp  do
                 begin
                   if not Transaction.Active then Transaction.StartTransaction ;
                   SQL.Text  := 'delete from   R_Packet where packetstate = 0';
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������ ��������: delete from R_Packet where packetState = 0');
                   ExecQuery;
//                   Transaction.CommitRetaining ;
                 end;
               end;
            //end
           // else MessageDialog('��� ������ ��� ��������', mtInformation, [mbOk], 0);

          end
        else
          begin
           { TODO : ��������� � ������� }
           flSell:=false;
           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' flSell='+BoolToStr(flSell));
           {�������� ����������� �������� /*����*/}
           SetLength(NeedSPR_mas,0);
           InsRPacket(dmCom.db, 'DESCRIPTION', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 0, '', '������ ��������', 1,NeedSPR, PacketId);
           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(pmRepl.Items[2].Tag)+' ������ ��������');
           SetLength(NeedSPR_mas,length(NeedSPR_mas)+1);
           NeedSPR_mas[length(NeedSPR_mas)-1]:=NeedSPR;
           NeedSPR:=0;

           {��������� ������� �� ������������}
           ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' delet from DELRECORDS where rstate>=3');
           InsRPacket(dmCom.db, 'DELRECORDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 4,
                      InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)]+';0;', '��������� ������ �� ������', 1,NeedSPR, PacketId);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' � ��������� ������ �� ������');
            PacketIdAddress:=-1;
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' PacketIdAddres='+inttostr(packetidclient));
            InsRPacket(dmCom.db, 'D_ADDRESS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '���������� ����', 1,NeedSPR, PacketIdAddress);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' ���������� ����');
            PacketIdClient:=-1;
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' PacketIDClient='+intToStr(PacketIdClient));

            InsRPacket(dmCom.db, 'CLIENT', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '���������� �����������', 1,NeedSPR, PacketIdClient);

            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' ���������� �����������');
            InsRPacket(dmCom.db, 'D_RET', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '���������� ������ ���������', 1,NeedSPR, PacketId);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' ���������� ������ ���������');

            InsRPacket(dmCom.db, 'CASH', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 11, '', '�����', 1,NeedSPR, PacketId);

            InsRPacket(dmCom.db, 'CASH_RET', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 11, '', '��������', 1,NeedSPR, PacketId);

            InsRPacket(dmCom.db, 'CAMPAIGN_HISTORY', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 11, '', '������', 1,NeedSPR, PacketId);

            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' �����');

            InsRPacket(dmCom.db, 'SERTIFICATE', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '�����������. �����', 1,NeedSPR, PacketId);

            InsRPacket(dmCom.db, 'SERT_ADDPAY', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '�����������. �������', 1,NeedSPR, PacketId);

            InsRPacket(dmCom.db, 'SERT_INFO', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '�����������. ������ � ��������', 1,NeedSPR, PacketId);

            InsRPacket(dmCom.db, 'SERT_SELL', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 1, '', '�����������. ������� ���������', 1,NeedSPR, PacketId);

                {��������� ������� �� ������ � ���������}
            ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' delete from DELRECORDS where rstate>=3');
            InsRPacket(dmCom.db, 'DELRECORDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 4,
                           InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)]+';1;', '��������� ������ �� ��������� � ������', 1,NeedSPR, PacketId);
           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' ��������� ������ �� ��������� � ������');

           with dm2, quRRet do
             begin
               Active:=True;
               while not EOF do
                 begin
                   InsRPacket(dmCom.db, 'RET', slNDep.Values[IntToStr(CenterDepId)], 2,
                     quRRetSInvId.AsString, '��������� ������� �'+quRRetSN.AsString, 1,NeedSPR, PacketId);
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ' + IntToStr(PacketID)+' '+IntToStr(pmRepl.Items[2].Tag)+' ��������� ������� �'+quRRetSN.AsString);

                    with dm2.quSetInvRState, Params do
                      begin
                        ByName['INVID'].AsInteger:=quRRetSInvId.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' INVID='+quRRetSinvId.AsString+' RSTATE=1');
                        ExecQuery;
                      end;
                   Inc(rc);
                   Next;
                end;
                Active:=False;
             end;

            with dm2, quRSell do
              begin
                Active:=True;
                while not EOF do
                  begin
                    flSell:=true;
                   Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' flSell='+BoolToStr(flSell)+' RSTATE=1');
                    InsRPacket(dmCom.db, 'SELL', slNDep.Values[IntToStr(CenterDepId)], 2,
                    quRSellSellId.AsString, '����� �'+quRSellRN.AsString, 1,NeedSPR, PacketId);
                    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' SELLID='+quRSellSellId.AsString+' ����� �'+quRSellRN.AsString);
                    with dm2.quSetSellRState, Params do
                      begin
                        ByName['SELLID'].AsInteger:=quRSellSellId.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' SELLID='+quRSellSellId.AsString+' RSTATE=1');
                        ExecQuery;
                      end;
                    Inc(sc);
                       Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������� sc='+IntToStr(sc));
                    Next;
                  end;
                   Active:=False;
              end;

            {��������� ������� �� ������������}
            ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
            InsRPacket(dmCom.db, 'DELRECORDS', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 4,
                       InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)]+';2;', '��������� ������ �� ������������', 1,NeedSPR, PacketId);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PacketId)+' '+IntToStr(pmRepl.Items[2].Tag)+' ��������� ������ �� ������������');

            {�������� ���������, ������� ����� ��������}
            if not CenterDep then
            begin
             ExecSQL('execute procedure R_BEFORE_DINV ('+inttostr(SelfDepId)+
                     ', '#39+DateTimeToStr(dm.RBD)+#39', '#39+DateTimeToStr(dm.RED)+#39')' , dm.quTmp);
            Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' execute procedure R_BEFORE_DINV ('+inttostr(SelfDepId)+
                     ', '#39+DateTimeToStr(dm.RBD)+#39', '#39+DateTimeToStr(dm.RED)+#39')');
             dm.quTmp.Close;
             dm.quTmp.SQL.Text:='select id1, id2 from tmpdata where datatype=4 order by id2';
              Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' select id1, id2 from tmpdata where datatype=4 order by id2');
             dm.quTmp.ExecQuery;
             sterr:='';
             while not dm.quTmp.Eof do
             begin
              sterr:=sterr+#13#10+'�� ('+trim(dm.quTmp.Fields[1].AsString)+') - '+trim(dm.quTmp.Fields[0].AsString);
              dm.quTmp.Next;
             end;
             dm.quTmp.Close;
             dm.quTmp.Transaction.CommitRetaining;
               Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' �� �� �������� sterr='+ sterr);
             if sterr<>'' then MessageDialog('���������� ��������� �� ����� ���� ��������:'+sterr, mtWarning, [mbOk], 0);

            end;

            //�������� ���������
            with dm2, quRDInv do
              begin
                Active:=True;
                while not EOF do
                  begin
                    InsRPacket(dmCom.db, 'DINV', slNDep.Values[IntToStr(CenterDepId)], 2,
                               quRDInvSInvId.AsString, '��������� ������� �'+quRDInvSN.AsString, 1,NeedSPR, PacketId);
                      Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+IntToStr(PacketId)+' ��������� ������� �'+quRDinvSN.asstring);
                    with quSetInvRState, Params do
                      begin
                        ByName['INVID'].AsInteger:=quRDInvSInvId.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' INVID='+quRSellSellId.AsString+' RSTATE=1');
                        ExecQuery;
                      end;
                    Inc(dc);
                    Next;
                  end;
                Active:=False;
              end;

            {�������� ��������� ���������� �������, ������� ����� ��������}
            if not CenterDep then
            begin
             ExecSQL('execute procedure R_BEFORE_SUSPITEM ('+inttostr(SelfDepId)+
                     ', '#39+DateTimeToStr(dm.RBD)+#39', '#39+DateTimeToStr(dm.RED)+#39')' , dm.quTmp);
             Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' execute procedure R_BEFORE_SUSPITEM ('+inttostr(SelfDepId)+
                     ', '#39+DateTimeToStr(dm.RBD)+#39', '#39+DateTimeToStr(dm.RED)+#39')');
             dm.quTmp.Close;
             dm.quTmp.SQL.Text:='select id1, id2 from tmpdata where datatype=9 order by id2';
             dm.quTmp.ExecQuery;
              Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' select id1, id2 from tmpdata where datatype=9 order by id2');
              Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' id1='+trim(dm.quTmp.Fields[0].AsString)+' id2='+trim(dm.quTmp.Fields[1].AsString));
             sterr:='';
             while not dm.quTmp.Eof do
             begin
              sterr:=sterr+#13#10+'����. ('+trim(dm.quTmp.Fields[1].AsString)+') - '+trim(dm.quTmp.Fields[0].AsString);
              dm.quTmp.Next;
             end;
             dm.quTmp.Close;
             dm.quTmp.Transaction.CommitRetaining;
             if sterr<>'' then MessageDialog('��������� � �����. ���. �� ����� ���� ��������:'+sterr, mtWarning, [mbOk], 0);
              Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ��������� � �����.��������� sterr'+ sterr);
            end;

            {�������� ���������� �������}
            with dm2, quRSuspItem do
              begin
                Active:=True;
                while not EOF do
                  begin
                    InsRPacket(dmCom.db, 'SUSPITEM', slNDep.Values[IntToStr(CenterDepId)], 2,
                               quRSuspItemSInvId.AsString, '��������� ���. ������� �'+quRSuspItemSN.AsString, 1,NeedSPR, PacketId);
                    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntTostr(PacketId)+'��������� ���������� ������� �'+ quRSuspItemSN.AsString);
                    with quSetInvRState, Params do
                      begin
                        ByName['INVID'].AsInteger:=quRSuspItemSInvId.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' INVID='+quRSuspItemSinvid.AsString+' RSTATE=1');
                        ExecQuery;
                      end;
                    Inc(dc);
                    Next;
                  end;
                Active:=False;
              end;

           {�������� �������� �� ��������}
           {*****************************}
           STACT:='';
           with dm2, quRBeforePAct do
           begin
            Active:=True;
            if trim(quRBeforePActSTPRORD.AsString)<>'' then
             STACT:=STACT+#13#10+trim(quRBeforePActSTPRORD.AsString)+' (��'+
                  trim(quRBeforePActSNVP.AsString)+')';
              Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' STACT='+STACT);

             Active:=False;
           end;


           with dm2, quRPAct do
             begin
               Active:=True;
               while not EOF do
                 begin
                   if quRPActFACT.Asinteger = 1 then
                   begin
                    InsRPacket(dmCom.db, 'PACT', slNDep.Values[IntToStr(CenterDepId)], 2,
                      quRPActPrOrdId.AsString, '��� �'+quRPActNAct.AsString, 1,NeedSPR, PacketId);
                         Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+IntToStr(PrordID)+' ��� �'+quRPactNact.AsString);
                      Inc(prc);
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ���-��'+IntToStr(prc));
                   end
                   else
                   begin
                    InsRPacket(dmCom.db, 'PACT', slNDep.Values[IntToStr(CenterDepId)], 2,
                      quRPActPRORDID.AsString, '������������� ������� �'+quRPActPRORD.AsString, 1,NeedSPR, PacketId);
                         Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+intToStr(PacketId)+' ������������� ������� �'+quRPActPRORD.AsString);
                     Inc(ppr);
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ppr='+IntToStr(ppr));
                   end;

                   with dm2.quSetPActRState, Params do
                     begin
                       ByName['PRORDID'].AsInteger:=quRPActPrOrdId.AsInteger;
                       ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' PRORDID='+quRPactPrordid.ASString);
                       ExecQuery;
                     end;
                   Next;
                 end;
               Active:=False;
             end;

            {������ �����������}
            with dm2, quRAppl do
              begin
                Active:=True;
                while not EOF do
                  begin
                    InsRPacket(dmCom.db, 'APPL', slNDep.Values[IntToStr(CenterDepId)], 2,
                    quRApplAPPLID.AsString, '������ �'+quRApplNOAPPL.AsString, 1,NeedSPR, PacketId);
                    Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' '+inttostr(PacketId)+' '+quRApplAPPLID.AsString+' ������ �'+quRPactPrordid.ASString);
                    with dm2.quSetAppl, Params do
                      begin
                        ByName['APPLID'].AsInteger:=quRApplAPPLID.AsInteger;
                        ByName['RSTATE'].AsInteger:=1;
                        Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' APPLID='+quRApplApplId.ASString+' RSATATE=1');
                        ExecQuery;
                      end;
                    Inc(ap);
                    Next;
                  end;
                  Active:=false;
              end;

         {********************* ��������� ������ *********************}

         ReplicateBuyInvoice;

         {********************* ��������� ������ *********************}


          if (not flSell) and ((PacketIdClient<>-1) or (PacketIdAddress<>-1)) then
           begin
            if MessageDialog('���������� ���������� �����������?', mtInformation, [mbYes,mbNo], 0) = mrNo then
            begin
             if PacketIdClient<>-1 then
             begin
              ExecSQL('delete from r_packet where r_packetid='+inttostr(PacketIdClient), dm.quTmp);
              PacketCount:=PacketCount-1;
             end;
             if PacketIdAddress<>-1 then
             begin
              ExecSQL('delete from r_packet where r_packetid='+inttostr(PacketIdAddress), dm.quTmp);
              PacketCount:=PacketCount-1;
             end;
            end;
           end;

           NeedSPR:=NeedSPR_mas[0];
           InsRPacket(dmCom.db, 'DESCRIPTION', InsRepl.slDep.Values[IntToStr(pmRepl.Items[2].Tag)], 0, '', '������ ��������', 1,NeedSPR, PacketId);
           Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������ ��������');


            if PacketCount>0 then
             begin
                if STACT<>'' then MessageDialog('�� ����� ���� �������� ���� '+STACT, mtWarning, [mbOk], 0);
                if MessageDialog('������ � ������� ���������� � �������'+NL+
                              '��������� ��: '+IntToStr(dc)+NL+
                              '���� ����������: '+IntToStr(prc)+NL+
                              '������������� ��������: '+IntToStr(ppr)+NL+
                              '�������: '+IntToStr(sc)+NL+
                              '��������: '+IntToStr(rc)+NL+
                              '������ �����������: '+IntToStr(ap) + NL +
                              '��������� ������: ' + IntToStr(BuyInvoiceCount) + NL +
                              '������� �� ������: ' + IntToStr(BuyOrderCount), mtInformation, [mbYes,mbNo], 0) = mrNo then
               begin
                 with   transltmp  do
                 begin
                   if not Transaction.Active then Transaction.StartTransaction ;
                   SQL.Text  := ' delete from   R_Packet where packetstate = 0';
                  Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' ������: delete from   R_Packet where packetstate = 0');
                   ExecQuery;
          //         Transaction.CommitRetaining ;
                 end;
               end
              end
            else MessageDialog('��� ������ ��� ��������', mtInformation, [mbOK], 0);
          end;
        if not dmcom.tr.Active then dmcom.tr.StartTransaction;
        dmCom.tr.CommitRetaining;
      end;

    with qutmp do
    begin
     for i:=0 to length(dmcom.mas_dep)-1 do
     begin
      if not Transaction.Active then Transaction.StartTransaction;
      sql.Text := 'update d_dep set r_usedep = 1 where d_depid = '+inttostr(dmcom.mas_dep[i]);
       Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' update d_dep set r_usedep = 1 where d_depid = '+inttostr(dmcom.mas_dep[i]));
      ExecQuery;
      Transaction.CommitRetaining;
      close;
     end;
     for i:=0 to length(dmcom.mas_dep)-1 do
     begin
      if not Transaction.Active then Transaction.StartTransaction;
      sql.Text := 'update r_dep set usedep = 1 where ncode = '+inttostr(dmcom.mas_dep[i]);
      Log_jew.Add(DateTimeToStr(dmcom.GetServerTime)+' update r_dep set usedep = 1 where ncode = '+inttostr(dmcom.mas_dep[i]));
      ExecQuery;
      Transaction.CommitRetaining;
     end;
    end;

    for i:= dm.pmRepl.Items.Count-1 downto 2 do  pmRepl.Items.Delete(i);
    FillReplMenu(dmCom.db, pmRepl, ReplClick);

//  ControlRepClose := GetProcAddress(DllInstance, 'DestroyNewThread');
//  ControlRepClose;
//  FreeLibrary(DllInstance);
  end
  finally
     ControlRepClose := GetProcAddress(DllInstance, 'DestroyNewThread');
     //ControlRepClose;
     FreeLibrary(DllInstance);
  
  Screen.Cursor:=crDefault;
  dm3.update_operation(LogOperationID);

  CreateNewThread(fmMain.Handle, dmcom.db.DBName);
  end;
    tf:=TimeToStr(dmcom.GetServerTime);
    for i := 0 to length(tf) do                           
       if tf[i]=':' then tf[i]:='_';
   Path := GetSpecialFolderLocation(CSIDL_APPDATA) + '\jew\'+DateToStr(dmcom.GetServerTime)+' '+tf+'~exp.txt';
   Log_jew.SaveToFile(path);
end;

procedure TfmMain.acErrInPrordUpdate(Sender: TObject);
begin
   acErrInPrord.Enabled:= not dmcom.Seller;
   acErrInPrord.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acAddClientFromFileUpdate(Sender: TObject);
begin
   acAddClientFromFile.Enabled:= dmcom.Adm;
   acAddClientFromFile.Visible:= dmcom.Adm;
end;

procedure TfmMain.acClearBaseUpdate(Sender: TObject);
begin
   acClearBase.Enabled:= not dmcom.Seller;
   acClearBase.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acClearDelRecordsUpdate(Sender: TObject);
begin
   acClearDelRecords.Enabled:= dmcom.Adm;
   acClearDelRecords.Visible:= dmcom.Adm;
end;

procedure TfmMain.acEditFileUpdate(Sender: TObject);
begin
   acEditFile.Enabled:= dmcom.Adm;
   acEditFile.Visible:= dmcom.Adm;
end;

procedure TfmMain.acLoadReportUpdate(Sender: TObject);
begin
   acLoadReport.Enabled:= not dmcom.Seller;
   acLoadReport.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acLoadSQLUpdate(Sender: TObject);
begin
   acLoadSQL.Enabled:= (not dmcom.Seller);
   acLoadSQL.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acExportLogUpdate(Sender: TObject);
begin
   acExportLog.Enabled:= (not dmcom.Seller) and false;
   acExportLog.Visible:= (not dmcom.Seller) and false;
end;

procedure TfmMain.acDelLogUpdate(Sender: TObject);
begin
   acDelLog.Enabled:=(not dmcom.Seller) and false;
   acDelLog.Visible:=(not dmcom.Seller) and false;
end;

procedure TfmMain.acExportUpdate(Sender: TObject);
begin
   acExport.Enabled:= acExportNoRest.Enabled or acExportWithRest.Enabled or acExportWithRestNoReCalc.Enabled;
   acExport.Visible:=acExport.Enabled;
end;

procedure TfmMain.acIsSell_RunExecute(Sender: TObject);
begin
  ExecSQL('update d_emp set issell_run=0 where d_empid = '+inttostr(dmcom.User.UserId), dmCom.quTmp);
end;

procedure TfmMain.acListInventoryExecute(Sender: TObject);
var LogOperationID: string;
begin
  LogOperationID:= dm3.insert_operation('������ ��������������','-1000');
  with dmCom do
    begin
      if not tr.Active then tr.StartTransaction;
      ShowAndFreeForm(TfmListInventory, Self, TForm(fmListInventory), True, False);
      tr.CommitRetaining;
    end;
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInsideInfoExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfInsideInfo, Self, TForm(fInsideInfo), True, False);
end;

procedure TfmMain.acInfoEditArtHistExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation('������� ��������� ������������� ��������', '-1000');
  ShowAndFreeForm(TfmInfoEditArtHist, Self, TForm(fmInfoEditArtHist), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acInfoEditArtHistUpdate(Sender: TObject);
begin
 if CenterDep then acInfoEditArtHist.Enabled:=true
 else acInfoEditArtHist.Enabled:=false;
 acInfoEditArtHist.Visible:=acInfoEditArtHist.Enabled;
end;

procedure TfmMain.actApplListDepExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplDepList, '-1000');
  ShowAndFreeForm(TfmApplDep, Self, TForm(fmApplDep), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acApplUpdate(Sender: TObject);
begin
 if actApplList.Enabled or actApplListDep.Enabled then acAppl.Enabled:=true
 else acAppl.Enabled:=false;
 acAppl.Visible:=acAppl.Enabled;
end;

procedure TfmMain.acDeleteActExecute(Sender: TObject);
begin
 ExecSQL('delete from tmpdata where datatype=3', dm.quTmp);
end;

procedure TfmMain.acDDiscountSumExecute(Sender: TObject);
var LogOperationID: string;
begin
  LogOperationID:=dm3.insert_operation('�����������-������ �� �����','-1000');
  {b:=GetBit(dmCom.EditRefBook, 17) and CenterDep;
  ShowAndFreeFormEnabled(TfmDiscountSum, Self, TForm(fmDiscountSum), True, False, b,
  'siHelp;siExit;tb1;spitPrint;acClose;acPrint;', 'dg1;');}
  ShowAndFreeForm(TfmDiscountSum, Self, TForm(fmDiscountSum), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acEditBadRefUpdate(Sender: TObject);
begin
 acEditBadRef.Enabled:= dmcom.Adm;
 acEditBadRef.Visible:= dmcom.Adm;
end;

procedure TfmMain.acEditBadRefExecute(Sender: TObject);
type t_rec= record
       sitemid, ref, inscounter:integer;
     end;
     t_arr=array of t_rec;
var  ftext_read, ftext_write:textfile;
     tmpstr, Path, tmp :string;
     i, l:integer;
     uid, newuid:integer;
     uidinfo:t_arr;

 procedure sort_mass;
 var sitemid,ref,inscounter:integer;
     j,k:integer;
 begin
  for j:=0 to  length(uidinfo)-2 do
  begin
   for k:=j+1  to length(uidinfo)-1 do
    if uidinfo[k].inscounter<uidinfo[j].inscounter then
    begin
     sitemid:=uidinfo[k].sitemid;
     ref:=uidinfo[k].ref;
     inscounter:=uidinfo[k].inscounter;
     uidinfo[k].sitemid:=uidinfo[j].sitemid;
     uidinfo[k].ref:=uidinfo[j].ref;
     uidinfo[k].inscounter:=uidinfo[j].inscounter;
     uidinfo[j].sitemid:=sitemid;
     uidinfo[j].ref:=ref;
     uidinfo[j].inscounter:=inscounter;
    end
  end
 end;
begin
  if not dlgOpenFile.Execute then exit;
  Path:=dlgOpenFile.FileName;
  AssignFile(ftext_read, Path);
  Reset(ftext_read);

  if not svFile.Execute then exit;
  Path:=svFile.FileName;
  AssignFile(ftext_write, Path);
  Rewrite(ftext_write);
  try
   readln (ftext_read,tmpstr);
   uid:=-2;
   i:=1;
   while not Eof (ftext_read) do
   begin
    readln (ftext_read,tmpstr);
    tmp:=ExtractWord(4,tmpstr,['{']);
    delete(tmp,1,2);
    delete(tmp,length(tmp),1);
    newuid:=strtoint(tmp);
    if (newuid<>uid) then
    begin
     if i=1 then
     begin
      SetLength(uidinfo,1);
      tmp:=ExtractWord(2,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[0].sitemid:=strtoint(tmp);
      tmp:=ExtractWord(3,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[0].ref:=strtoint(tmp);
      tmp:=ExtractWord(9,tmpstr,['{']);
      delete(tmp,1,2);
      uidinfo[0].inscounter:=strtoint(tmp);
      uid:=newuid;
     end
     else
     begin
      sort_mass;
      for l:=1  to length(uidinfo)-1  do
       if uidinfo[l].ref=-1000 then
       begin
        writeln(ftext_write,'update sitem ');
        writeln(ftext_write,'set ref='+inttostr(uidinfo[l-1].sitemid)+' ');
        writeln(ftext_write,'where sitemid='+inttostr(uidinfo[l].sitemid)+' ');
        writeln(ftext_write,'***---***');
       end;
      uid:=newuid;
      SetLength(uidinfo,1);
      tmp:=ExtractWord(2,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[length(uidinfo)-1].sitemid:=strtoint(tmp);
      tmp:=ExtractWord(3,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[length(uidinfo)-1].ref:=strtoint(tmp);
      tmp:=ExtractWord(9,tmpstr,['{']);
      delete(tmp,1,2);
      uidinfo[length(uidinfo)-1].inscounter:=strtoint(tmp);
     end
    end
    else
    begin
      SetLength(uidinfo,length(uidinfo)+1);
      tmp:=ExtractWord(2,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[length(uidinfo)-1].sitemid:=strtoint(tmp);
      tmp:=ExtractWord(3,tmpstr,['{']);
      delete(tmp,1,2);
      delete(tmp,length(tmp),1);
      uidinfo[length(uidinfo)-1].ref:=strtoint(tmp);
      tmp:=ExtractWord(9,tmpstr,['{']);
      delete(tmp,1,2);
      uidinfo[length(uidinfo)-1].inscounter:=strtoint(tmp);
    end;
    inc(i);
   end;

   sort_mass;
   for l:=1  to length(uidinfo)-1  do
   if uidinfo[l].ref=-1000 then
   begin
    writeln(ftext_write,'update sitem ');
    writeln(ftext_write,'set ref='+inttostr(uidinfo[l-1].sitemid)+' ');
    writeln(ftext_write,'where sitemid='+inttostr(uidinfo[l].sitemid)+' ');
   end;
  finally
   CloseFile(ftext_read);
   CloseFile(ftext_write);
   MessageDialog('���� ������', mtInformation, [mbOk], 0);
  end;
end;

procedure TfmMain.acShopReportExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ShopReport, '-1000');
  ShowAndFreeForm(TfmShopReportList, Self, TForm(fmShopReportList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acLogUpdate(Sender: TObject);
begin
 acLog.Enabled:=dmcom.Adm and dmcom.IsExistsLog;
 acLog.Visible:=dmcom.Adm and dmcom.IsExistsLog;
end;

procedure TfmMain.acLogExecute(Sender: TObject);
var sqlstr: string;
    fl:boolean;
    ip,host :string;
begin
 fl:=dmcom.IsMakeLog;
 dmcom.IsMakeLog:=NLog.Checked;
 if NLog.Checked then dmcom.LogGetID(51)
 else dmcom.LogGetID(50);

 if dmcom.IsMakeLog<>fl then
 begin
  if dmcom.IsMakeLog then
  begin
   dm3.LogUserID :=IntToStr(dmCom.LogGetID(46));
   GetLocalIP(ip,host);
   if (ip = '') then ip  := 'ip isn''t define';
   if (host = '') then host  := ' host isn''t define';
   sqlstr := ' insert into Log$User(L_UserID, Host, UserName, DISCRIPT) ' +
                ' values('+ dm3.LogUserID +',''' + ip + ':' + host + ''', '#39 + dmCom.UserName +  #39', ''Connected'')';
   if (trim(dm3.LogUserID)<>'')and(trim(ip)<>'')and(trim(host)<>'')and(trim(dmCom.UserName)<>'')then
    ExecSQL(sqlstr,dm3.quTmpLog);
  end
  else
  begin
   if trim(dm3.LogUserID)<>'' then
   begin
    sqlstr := ' Update Log$User' +
              '   Set  DISCRIPT = stretrim(DISCRIPT) || ''/Disconnected''' +
              ' where L_UserID = ' + dm3.LogUserID;
    ExecSQL(sqlstr,dm3.quTmpLog);
   end;
  end
 end
end;

procedure TfmMain.acWriteInfoLogExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmWriteLogBase, Self, TForm(fmWriteLogBase), True, False);
end;

procedure TfmMain.acWriteInfoLogUpdate(Sender: TObject);
begin
 acWriteInfoLog.Enabled:=not dmcom.Seller and dmcom.IsExistsLog;
 acWriteInfoLog.Visible:=not dmcom.Seller and dmcom.IsExistsLog;
end;

procedure TfmMain.acInsideInfoUpdate(Sender: TObject);
begin
 acInsideInfo.Enabled:= not dmcom.Seller;
 acInsideInfo.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acDeleteActUpdate(Sender: TObject);
begin
 acDeleteAct.Enabled:= not dmcom.Seller;
 acDeleteAct.Visible:= not dmcom.Seller;
end;

procedure TfmMain.acAddArtCDMExecute(Sender: TObject);
begin
 {
 if not dlgOpenFile.Execute then exit;
 dmImp.ImportArtCDMXls(dlgOpenFile.FileName);
 }
end;

procedure TfmMain.acAddArtCDMUpdate(Sender: TObject);
begin
 acAddArtCDM.Enabled:=dmcom.Adm;
 acAddArtCDM.Visible:=dmcom.Adm;
end;

procedure TfmMain.acExportWithRestUpdate(Sender: TObject);
begin
 acExportWithRest.Enabled:=acExportWithRest.Enabled and CenterDep;
 acExportWithRest.Visible:=acExportWithRest.Visible and CenterDep;
end;

procedure TfmMain.acExportWithRestNoReCalcUpdate(Sender: TObject);
begin
 acExportWithRestNoReCalc.Enabled:=acExportWithRestNoReCalc.Enabled and CenterDep;
 acExportWithRestNoReCalc.Visible:=acExportWithRestNoReCalc.Visible and CenterDep;
end;

procedure TfmMain.acExportErrUpdate(Sender: TObject);
begin
 acExportErr.Enabled:={CenterDep and }(not dmcom.Seller);
 acExportErr.Visible:={CenterDep and }(not dmcom.Seller);
end;

procedure TfmMain.acExportErrExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ExportSetting, '-1000');
  ShowAndFreeForm(TfmRepSetting, Self, TForm(fmRepSetting), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acSettingUpdate(Sender: TObject);
begin
 acSetting.Enabled:= not dmcom.Seller;
 acSetting.Visible:= not dmcom.Seller;
end;

{procedure TfmMain.acImportDepExecute(Sender: TObject);
var Path:string;
    i, j:integer;
    Att, FilterN, ItemFilter : IXMLNode;
    st:string;
    stins:widestring;
    dt : tdatetime;
begin
 if not dlgOpenFile.Execute then exit
 else Path :=dlgOpenFile.FileName;

 with dmImp do
 begin
  xmlDoc.Active:=false;
  xmldoc.FileName:=Path;
  xmlDoc.Active:=true;
  FilterN:=xmldoc.DocumentElement;
  for i:=0  to Pred(FilterN.ChildNodes.Count)  do
  begin
   quImportArtDep.Active:=false;

   att:=xmldoc.DocumentElement.ChildNodes[i];
   st:=att.NodeName;
   delete(st,1,1);
   try
   with quImportArtDep, Params do
   begin
    ByName['UID'].AsInteger:=strtoint(st);
    ByName['SSF'].AsString:=Att.Attributes['SSF'];
    ByName['SUPID'].AsInteger:=att.Attributes['SUPID'];
    ByName['NDSID'].AsInteger:=Att.Attributes['NDSID'];
    ByName['PRICE'].AsFloat:=att.Attributes['PRICE'];
    ByName['W'].AsFloat:=att.Attributes['W'];
    ByName['SZ'].AsString:=att.Attributes['SZ'];
    ByName['D_GOODS_SAM1'].AsInteger:=att.Attributes['D_GOODS_SAM1'];
    ByName['D_GOODS_SAM2'].AsInteger:=att.Attributes['D_GOODS_SAM2'];
    ByName['ART2'].AsString:=att.Attributes['ART2'];
    ByName['ART'].AsString:=att.Attributes['ART'];
    ByName['COMPID'].AsInteger:=att.Attributes['D_COMPID'];
    ByName['GOODID'].AsString:=att.Attributes['D_GOODID'];
    ByName['INSID'].AsString:=att.Attributes['D_INSID'];
    ByName['MATID'].AsString:=att.Attributes['D_MATID'];
    ByName['UNITID'].AsInteger:=att.Attributes['UNITID'];
    ByName['COUNTRYID'].AsString:=att.Attributes['D_COUNTRYID'];
    ByName['ATT1ID'].AsInteger:=Att.Attributes['ATT1'];
    ByName['ATT2ID'].AsInteger:=Att.Attributes['ATT2'];
    ByName['PRICE2'].AsFloat:=Att.Attributes['PRICE2'];
    dt:=StrToDateTime(Att.Attributes['NDATE']);
    ByName['NDATE'].AsTimeStamp:=DateTimeToTimeStamp(dt);
    ByName['DEPID'].AsInteger:=SelfDepId;
    ByName['USERID'].AsInteger:=dmcom.UserId;
    dt:=StrToDateTime('01.01.2005');
    ByName['I_SDATE'].AsTimeStamp:=DateTimeToTimeStamp(dt);
   end;
   quImportArtDep.Active:=true;
   quImportArtDep.Transaction.CommitRetaining;

   if quImportArtDepINSERTA2.AsInteger=1 then
   for j:=0  to Pred(Att.ChildNodes.Count) do
   begin
    ItemFilter:=Att.ChildNodes[j];
    stins:='insert into ins (INSID, ART2ID, D_INSID, QUANTITY, '+
     'WEIGHT, COLOR, CHROMATICITY, CLEANNES, GR, MAIN, '+
     'EDGTYPEID, EDGSHAPEID) values (GEN_ID(GEN_INS_ID, 1), '+
     quImportArtDepART2ID.AsString;

    stins:=stins+', '#39+ItemFilter.Attributes['INSID']+#39;
    if ItemFilter.Attributes['QUANTITY']='NULL' then stins:=stins+', null'
    else stins:=stins+', '+ItemFilter.Attributes['QUANTITY'];
    if ItemFilter.Attributes['WEIGTH']='NULL' then stins:=stins+', null'
    else stins:=stins+', '+ReplaceStr(ItemFilter.Attributes['WEIGTH'], ',', '.');
    if ItemFilter.Attributes['COLOR']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['COLOR']+#39;
    if ItemFilter.Attributes['CHROMATICITY']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['CHROMATICITY']+#39;
    if ItemFilter.Attributes['CLEANNES']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['CLEANNES']+#39;
    if ItemFilter.Attributes['GR']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['GR']+#39;
    if ItemFilter.Attributes['MAIN']='NULL' then stins:=stins+', null'
    else stins:=stins+', '+ItemFilter.Attributes['MAIN'];
    if ItemFilter.Attributes['EDGTYPEID']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['EDGTYPEID']+#39;
    if ItemFilter.Attributes['EDGSHAPEID']='NULL' then stins:=stins+', null'
    else stins:=stins+', '#39+ItemFilter.Attributes['EDGSHAPEID']+#39;

    stins:=stins+')';
    ExecSQL(stins, sqlimp);
   end;
   except
    on e:Exception do MessageDialog(st+ e.Message, mtError, [mbOk], 0);
   end;
  end;
  quImportArtDep.Active:=false;
  xmlDoc.Active:=false;
  MessageDialog('������ ��������!', mtInformation, [mbOk], 0);
 end
end; }

procedure TfmMain.acExportDepUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= dmcom.Adm;
 TAction(Sender).Visible:= dmcom.Adm;
end;

{procedure TfmMain.acExportDepSellExecute(Sender: TObject);
var Att, FilterN, ItemFilter, ItemFilter1 : IXMLNode;
    Path:string;
    uid:integer;
begin
// sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
// svdFile.InitialDir:=sDir;
// svdFile.DefaultExt:='xml';
// svdFile.FileName:=ReplaceStr(DateTimeToStr(now),'.','');
// svdFile.FileName:=ReplaceStr(svdFile.FileName,':','');
// svdFile.FileName:=ReplaceStr(svdFile.FileName,'_','');
 {
 if svFile.Execute then  Path:=svFile.FileName
 else exit;

 with dmimp do
 begin
  quExportDepSell.Active:=false;
  quExportDepSell.ParamByName('DEPID').AsInteger:=209;
  quExportDepSell.Active:=true;
  xmlDoc.Active:=true;
  while not quExportDepSell.Eof do
  begin
   FilterN:=xmlDoc.DocumentElement.AddChild('_'+trim(quExportDepSellUID.AsString));
   Att := XMLdoc.CreateNode('SSF', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSSF.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SUPID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSUPID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('NDSID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellNDSID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('PRICE', ntAttribute);
   Att.NodeValue := trim(quExportDepSellPRICE.AsString);
   FilterN.AttributeNodes.Add(Att);

   Att := XMLdoc.CreateNode('W', ntAttribute);
   Att.NodeValue := trim(quExportDepSellW.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SZ', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSz.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODS_SAM1', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_GOODS_SAM1.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODS_SAM2', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_GOODS_SAM2.AsString);
   FilterN.AttributeNodes.Add(Att);

   Att := XMLdoc.CreateNode('ART2', ntAttribute);
   Att.NodeValue := trim(quExportDepSellART2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ART', ntAttribute);
   Att.NodeValue := trim(quExportDepSellART.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_COMPID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_COMPID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_GOODID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_GOODID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_INSID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_INSID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_MATID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_MATID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('UNITID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellUNITID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('D_COUNTRYID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellD_COUNTRYID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ATT1', ntAttribute);
   Att.NodeValue := trim(quExportDepSellATT1.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('ATT2', ntAttribute);
   Att.NodeValue := trim(quExportDepSellATT2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('PRICE2', ntAttribute);
   Att.NodeValue := trim(quExportDepSellPRICE2.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('NDATE', ntAttribute);
   Att.NodeValue := trim(quExportDepSellNDATE.AsString);
   FilterN.AttributeNodes.Add(Att);

   Att := XMLdoc.CreateNode('SELLPRICE', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLPRICE.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLPRICE0', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLPRICE0.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLDISCOUNT', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLDISCOUNT.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLCLIENTID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLCLIENTID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLUNITID', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLUNITID.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLQ0', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLQ0.AsString);
   FilterN.AttributeNodes.Add(Att);
   Att := XMLdoc.CreateNode('SELLW', ntAttribute);
   Att.NodeValue := trim(quExportDepSellSELLW.AsString);
   FilterN.AttributeNodes.Add(Att);



    ItemFilter:=FilterN.AddChild('INS');
    ItemFilter1 := XMLdoc.CreateNode('INSID', ntAttribute);
    ItemFilter1.NodeValue := trim(quExportDepSellINSID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('QUANTITY',ntAttribute);
    if quExportDepQUANTITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellQUANTITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('WEIGTH',ntAttribute);
    if quExportDepWEIGTH.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellWEIGTH.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('COLOR',ntAttribute);
    if quExportDepCOLOR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCOLOR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CHROMATICITY',ntAttribute);
    if quExportDepCHROMATICITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCHROMATICITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CLEANNES',ntAttribute);
    if quExportDepCLEANNES.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCLEANNES.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('GR',ntAttribute);
    if quExportDepGR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellGR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('MAIN',ntAttribute);
    if quExportDepMAIN.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellMAIN.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGTYPEID',ntAttribute);
    if quExportDepEDGTYPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellEDGTYPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGSHAPEID',ntAttribute);
    if quExportDepEDGSHAPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellEDGSHAPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);

   uid:=quExportDepSellUID.AsInteger;
   quExportDepSell.Next;

   while (quExportDepSellUID.AsInteger=uid) and (not quExportDepSell.Eof) do
   begin
    ItemFilter:=FilterN.AddChild('INS');
    ItemFilter1 := XMLdoc.CreateNode('INSID', ntAttribute);
    ItemFilter1.NodeValue := trim(quExportDepSellINSID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('QUANTITY',ntAttribute);
    if quExportDepQUANTITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellQUANTITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('WEIGTH',ntAttribute);
    if quExportDepWEIGTH.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellWEIGTH.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('COLOR',ntAttribute);
    if quExportDepCOLOR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCOLOR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CHROMATICITY',ntAttribute);
    if quExportDepCHROMATICITY.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCHROMATICITY.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('CLEANNES',ntAttribute);
    if quExportDepCLEANNES.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellCLEANNES.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('GR',ntAttribute);
    if quExportDepGR.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellGR.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('MAIN',ntAttribute);
    if quExportDepMAIN.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellMAIN.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGTYPEID',ntAttribute);
    if quExportDepEDGTYPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellEDGTYPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);
    ItemFilter1 := XMLdoc.CreateNode('EDGSHAPEID',ntAttribute);
    if quExportDepEDGSHAPEID.IsNull then ItemFilter1.NodeValue := 'NULL'
    else ItemFilter1.NodeValue := trim(quExportDepSellEDGSHAPEID.AsString);
    ItemFilter.AttributeNodes.Add(ItemFilter1);

    uid:=quExportDepSellUID.AsInteger;
    quExportDepSell.Next;
   end;
  end;
  XMLdoc.SaveToFile(Path);
  XMLdoc.Active := False;
  MessageDialog('������� ��������!', mtInformation, [mbOk], 0);
 end
end;  }

{procedure TfmMain.acAddSellExecute(Sender: TObject);
var Path:string;
    i:integer;
    Att, FilterN : IXMLNode;
    st:string;
    clientid:Longint;
    dt : tdatetime;
begin
 if not dlgOpenFile.Execute then exit
 else Path :=dlgOpenFile.FileName;

 with dmImp do
 begin
  xmlDoc.Active:=false;
  xmldoc.FileName:=Path;
  xmlDoc.Active:=true;
  FilterN:=xmldoc.DocumentElement;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  
  for i:=0  to Pred(FilterN.ChildNodes.Count)  do
  begin
//:uid, :Bd, :userid,
// :SELLPRICE, :SELLPRICE0, :SELLDISCOUNT,
// :SELLCLIENTID, :SELLUNITID, :SELLQ0, :SELLW
   quImportSell.Close;

   att:=xmldoc.DocumentElement.ChildNodes[i];
   st:=att.NodeName;
   delete(st,1,1);
   try
    with quImportSell, Params do
    begin
     ByName['UID'].AsInteger:=strtoint(st);
     dt:=StrToDateTime('31.12.2004');
     ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dt);
     ByName['USERID'].AsInteger:=dmcom.UserId;
     ByName['SELLPRICE'].AsFloat:=att.Attributes['SELLPRICE'];
     ByName['SELLPRICE0'].AsFloat:=att.Attributes['SELLPRICE0'];
     ByName['SELLDISCOUNT'].AsFloat:=att.Attributes['SELLDISCOUNT'];
     clientid:=att.Attributes['SELLCLIENTID'];
     ByName['SELLCLIENTID'].AsInteger:=clientid;
     ByName['SELLUNITID'].AsInteger:=att.Attributes['SELLUNITID'];
     ByName['SELLQ0'].AsFloat:=att.Attributes['SELLQ0'];
     ByName['SELLW'].AsFloat:=att.Attributes['SELLW'];
    end;
    quImportSell.ExecQuery;
    quImportArtDep.Transaction.CommitRetaining;

    except
     on e:Exception do MessageDialog(st+ e.Message, mtError, [mbOk], 0);
    end;
  end;
  quImportSell.Close;
  xmlDoc.Active:=false;
  MessageDialog('������ ��������!', mtInformation, [mbOk], 0);
 end
end; }

procedure TfmMain.acSupInfExecute(Sender: TObject);
begin
// ����������
 ShowAndFreeForm(TfmSupInf, Self, TForm(fmSupInf), True, False);
end;

procedure TfmMain.acSupInfUpdate(Sender: TObject);
begin
 acSupInf.Enabled:= CenterDep;
 acSupInf.Visible:= CenterDep; 
end;

procedure TfmMain.actActAllowancesExecute(Sender: TObject);
var LogOperationID:string;
begin
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;

  LogOperationID := dm3.insert_operation(sLog_ActAllowances, '-1000');
  ShowAndFreeForm(TfmActAllowancesList, Self, TForm(fmActAllowancesList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acNodCardExecute(Sender: TObject);
begin
 dmReport.PrintDocumentB(NewNodCard)
end;

procedure TfmMain.acEquipmentExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not dmcom.tr.Active then dmcom.tr.StartTransaction;

 LogOperationID := dm3.insert_operation(sLog_Equipment, '-1000');
 ShowAndFreeForm(TfmEquipment, Self, TForm(fmEquipment), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acUIDStoreDepExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreDep, '-1000');
  ShowAndFreeForm(TfmUIDStoreDepList, Self, TForm(fmUIDStoreDepList), True, False);
  dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acUIDStoreUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:=CenterDep;
 TAction(Sender).Visible:=CenterDep;
end;

procedure TfmMain.acPaymentExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmPayment, Self, TForm(fmPayment), True, False);
end;


procedure TfmMain.acApplExecute(Sender: TObject);
begin
  //
end;

procedure TfmMain.acCountProssExecute(Sender: TObject);
begin
 ExecSQL('update d_rec set calcwhdate=0', dm.quTmp);
end;

procedure TfmMain.acPaymentUpdate(Sender: TObject);
begin
  acPayment.Visible := GetBit(dmCom.Acc, 27) and CenterDep;
end;

procedure TfmMain.acRespStoringExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not dmcom.tr.Active then dmcom.tr.StartTransaction;

 LogOperationID := dm3.insert_operation(sLog_RespStoring, '-1000');
 ShowAndFreeForm(TfmRespStoring, Self, TForm(fmRespStoring), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acRepairExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not dmcom.tr.Active then dmcom.tr.StartTransaction;

 LogOperationID := dm3.insert_operation(sLog_RepairList, '-1000');
 ShowAndFreeForm(TfmRepairList, Self, TForm(fmRepairList), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acSuspItemExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not dmcom.tr.Active then dmcom.tr.StartTransaction;
 LogOperationID := dm3.insert_operation(sLog_SuspItemList, '-1000');
 ShowAndFreeForm(TfmSuspItemList, Self, TForm(fmSuspItemList), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acUserUidWHUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dmcom.Seller);
 TAction(Sender).Visible:= (not dmcom.Seller);
end;

procedure TfmMain.acUserUidWHExecute(Sender: TObject);
var LogOperationID:string;
begin
 if not dmcom.tr.Active then dmcom.tr.StartTransaction;
 LogOperationID := dm3.insert_operation(sLog_UserUidWh, '-1000');
 ShowAndFreeForm(TfmUserUIdWh, Self, TForm(fmUserUIdWh), True, False);
 dm3.update_operation(LogOperationID);
end;

procedure TfmMain.acIsMergeRunExecute(Sender: TObject);
begin
  ExecSQL('update D_EMP  Set IS_Merge_Run = -1' ,dm.quTmp);
end;

procedure TfmMain.acPriceAnalysisUpdate(Sender: TObject);
begin
  acPriceAnalysis.Visible := False;
end;

procedure TfmMain.acWarehouseAnalysisUpdate(Sender: TObject);
begin
  acWarehouseAnalysis.Visible := False;
end;

procedure TfmMain.NotSaleClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmNotSaleItems, Self, TForm(fmNotSaleItems), True, False);
end;
procedure TfmMain.N52Click(Sender: TObject);
begin
   ExecSQL('delete from Curr_User_Inv' ,dm.quTmp);
end;

procedure TfmMain.FormDestroy(Sender: TObject);
//var nid : TNotifyIconData;
begin
  if PluginClients <> nil then
  FreeAndNil(PluginClients);

  DestroyNewThread;



  TrayIcon.Active:=False;

end;

procedure TfmMain.N53Click(Sender: TObject);
begin
  //ShowAndFreeForm(TfmAnlzSelEmp, Self, TForm(fmAnlzSelEmp), True, False);

  TAnalisisSalary.Execute;

end;

procedure TfmMain.WndProc(var Message: TMessage);
begin
  if Message.Msg = WM_MSG then
  begin
    if Message.WParam = 0 then
    begin
      ActivFm := True;
      tiRepExe.Active:=True;
    end
      else
      begin
        ActivFm := False;
        tiRepExe.Active:=false;
      end;
  end
  else
    inherited WndProc(Message);
end;

procedure TfmMain.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
begin
  //Exit;

  if (ActivFm) and (not Show_msg) then
  begin
    case Msg.message of
      CM_MOUSEENTER, CM_MOUSELEAVE,
      WM_LBUTTONDOWN, WM_LBUTTONUP, WM_RBUTTONDOWN, WM_RBUTTONUP,
      WM_CHAR, WM_SYSKEYDOWN, WM_SYSKEYUP, WM_KEYDOWN, WM_KEYUP,
      WM_COMMAND, WM_SYSCOMMAND: Handled := true
      else
      Handled := False;
      end;
  end;

  if Handled then
  begin
    handled:=false;

    Show_msg:=true;

    //ShowMessage('�������� ����������! ��� �������� ��� �������� ���������.');

    TDialog.Warning('�������� ����������!'#13#10'��� �������� ��� �������� ���������.');

    Handled:=true;

    Show_msg:=false;
  end;

end;

procedure TfmMain.N59Click(Sender: TObject);
begin
  TDialogAnalizeCDM.Execute;

end;


//������ ������ �� �����
procedure TfmMain.N76Click(Sender: TObject);
var
  i, j, k:integer;

 function create_fmDepAndDate : boolean;
 begin
   fmDepAndDate:=TfmDepAndDate.Create(NIL);
   try
    with fmDepAndDate do
    if ShowModal=mrOK
    then result:=true
    else result:=false;
   finally
    fmDepAndDate.Free;
   end;
 end;

begin
  if GetPeriod(dm.BeginDate, dm.EndDate) then // ������ ������ ��������
  begin
    if create_fmDepAndDate then
       ReOpenDataSets([dm.taAnlzSell_days]);
  end;

  XLSExportFile1.Workbook.Create(1);
  if (sd1.Execute) then // ��������� ���� � xls �����
  begin
    XLSExportFile1.Workbook.Sheets[0].Cells[1,0].Value:='����� �� �������� (�� �����)';
    dm.quTmp.Close;
    dm.quTmp.SQL.Text := 'select Name from D_Dep where D_DepId='+IntToStr(dm.SDepId);
    dm.quTmp.ExecQuery;
    XLSExportFile1.Workbook.Sheets[0].Cells[0,9].Value:=dm.quTmp.Fields[0].AsString;
    dm.quTmp.Close;

    XLSExportFile1.Workbook.Sheets[0].Cells[1,9].Value:=DateToStr(dm.BeginDate)+' - ';
    XLSExportFile1.Workbook.Sheets[0].Cells[2,9].Value:=DateToStr(dm.EndDate);
    for i := 0 to 4 do
    begin
      XLSExportFile1.Workbook.Sheets[0].Rows[i].FontBold:=true;
      XLSExportFile1.Workbook.Sheets[0].Rows[i].HAlign:=xlHAlignLeft;
    end;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,5].Value:='���������� ��������';
    XLSExportFile1.Workbook.Sheets[0].Cells[4,22].Value:='����� ������� �������';

    //������� �����
    XLSExportFile1.Workbook.Sheets[0].Cells[4,0].BorderColorRGB[xlBorderLeft]:=xlcolorblack;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,0].BorderStyle[xlBorderAll]:=bsThin;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,15].BorderColorRGB[xlBorderLeft]:=xlcolorblack;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,15].BorderStyle[xlBorderAll]:=bsThin;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,27].BorderColorRGB[xlBorderRight]:=xlcolorblack;
    XLSExportFile1.Workbook.Sheets[0].Cells[4,27].BorderStyle[xlBorderAll]:=bsThin;
    for j := 0 to 27 do
    begin
      XLSExportFile1.Workbook.Sheets[0].Cells[4,j].BorderColorRGB[xlBorderTop]:=xlcolorblack;
      XLSExportFile1.Workbook.Sheets[0].Cells[4,j].BorderStyle[xlBorderAll]:=bsThin;
      XLSExportFile1.Workbook.Sheets[0].Cells[4,j].FontBold:=true;
      XLSExportFile1.Workbook.Sheets[0].Cells[4,j].HAlign:=xlHAlignCenter;
      XLSExportFile1.Workbook.Sheets[0].Cells[4,j].Wrap:=true;
      //������ �����
      XLSExportFile1.Workbook.Sheets[0].Cells[5,j].BorderColorRGB[xlBorderAll]:=xlcolorblack;
      XLSExportFile1.Workbook.Sheets[0].Cells[5,j].BorderStyle[xlBorderAll]:=bsThin;
      XLSExportFile1.Workbook.Sheets[0].Cells[5,j].FontBold:=true;
      XLSExportFile1.Workbook.Sheets[0].Cells[5,j].HAlign:=xlHAlignCenter;
      XLSExportFile1.Workbook.Sheets[0].Cells[5,j].Wrap:=true;
    end;

    XLSExportFile1.Workbook.Sheets[0].Cells[5,0].Value:='�./�.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,1].Value:='����';

    XLSExportFile1.Workbook.Sheets[0].Cells[5,2].Value:='10.-11.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,3].Value:='11.-12.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,4].Value:='12.-13.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,5].Value:='13.-14.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,6].Value:='14.-15.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,7].Value:='15.-16';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,8].Value:='16.-17.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,9].Value:='17.-18.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,10].Value:='18.-19.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,11].Value:='19.-20.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,12].Value:='20.-21.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,13].Value:='21.-22.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,14].Value:='����� ��������';

    XLSExportFile1.Workbook.Sheets[0].Cells[5,15].Value:='10.-11.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,16].Value:='11.-12.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,17].Value:='12.-13.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,18].Value:='13.-14.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,19].Value:='14.-15';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,20].Value:='15.-16.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,21].Value:='16.-17.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,22].Value:='17.-18.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,23].Value:='18.-19.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,24].Value:='19.-20.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,25].Value:='20.-21.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,26].Value:='21.-22.';
    XLSExportFile1.Workbook.Sheets[0].Cells[5,27].Value:='����� �������';

    dm.taAnlzSell_days.Active:=true;
    dm.taAnlzSell_days.Open;
    dm.taAnlzSell_days.First;

    i:=6;
    dm.taAnlzSell_days.First;

    While not dm.taAnlzSell_days.Eof do
    begin
      XLSExportFile1.Workbook.Sheets[0].Cells[i,0].Value:=dm.taAnlzSell_daysW_D.Value; // ���� ������
      XLSExportFile1.Workbook.Sheets[0].Cells[i,1].Value:=dm.taAnlzSell_daysD_DAY.Value; //����

      XLSExportFile1.Workbook.Sheets[0].Cells[i,2].Value:=dm.taAnlzSell_daysH10_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,3].Value:=dm.taAnlzSell_daysH11_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,4].Value:=dm.taAnlzSell_daysH12_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,5].Value:=dm.taAnlzSell_daysH13_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,6].Value:=dm.taAnlzSell_daysH14_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,7].Value:=dm.taAnlzSell_daysH15_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,8].Value:=dm.taAnlzSell_daysH16_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,9].Value:=dm.taAnlzSell_daysH17_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,10].Value:=dm.taAnlzSell_daysH18_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,11].Value:=dm.taAnlzSell_daysH19_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,12].Value:=dm.taAnlzSell_daysH20_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,13].Value:=dm.taAnlzSell_daysH21_CL.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,14].Value:=dm.taAnlzSell_daysITOGO_CL.Value;

      XLSExportFile1.Workbook.Sheets[0].Cells[i,15].Value:=dm.taAnlzSell_daysH10_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,16].Value:=dm.taAnlzSell_daysH11_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,17].Value:=dm.taAnlzSell_daysH12_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,18].Value:=dm.taAnlzSell_daysH13_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,19].Value:=dm.taAnlzSell_daysH14_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,20].Value:=dm.taAnlzSell_daysH15_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,21].Value:=dm.taAnlzSell_daysH16_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,22].Value:=dm.taAnlzSell_daysH17_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,23].Value:=dm.taAnlzSell_daysH18_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,24].Value:=dm.taAnlzSell_daysH19_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,25].Value:=dm.taAnlzSell_daysH20_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,26].Value:=dm.taAnlzSell_daysH21_S.Value;
      XLSExportFile1.Workbook.Sheets[0].Cells[i,27].Value:=dm.taAnlzSell_daysITOGO_SELL.Value;

      // ���������� ����� �� ��� ������
      for k := 0 to 27 do
      begin
        XLSExportFile1.Workbook.Sheets[0].Cells[i,k].BorderColorRGB[xlBorderAll]:=xlcolorblack;
        XLSExportFile1.Workbook.Sheets[0].Cells[i,k].BorderStyle[xlBorderAll]:=bsThin;
      end;
      dm.taAnlzSell_days.Next;
      inc(i);
    end;

    XLSExportFile1.SaveToFile(sd1.FileName);
    XLSExportFile1.Workbook.Clear;
    MessageDlg('������� �������� ������� � ���� ' + sd1.FileName, mtInformation, [mbOk], 0);

    dm.taAnlzSell_days.Active:=false;
    CloseDataSets([dm.taAnlzSell_days]);
  end;
end;

procedure TfmMain.N77Click(Sender: TObject);
begin
  TAnalisisClientSell.Execute;
end;

procedure TfmMain.N79Click(Sender: TObject);
var
 Analiz: TfrmAnalizSklad;
 i:integer;
 LogOperationID:string;
begin
  ///////
  dm.quTmp.Close;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  with dm, quTmp do
   begin
    SQL.Text:='SELECT UIDWHCALC FROM D_REC';
    ExecQuery;
    i:=Fields[0].AsInteger;
    Close;
    if i>0 then raise Exception.Create('��������� ������ ����������� ������ �������������.')
    else  begin
     LogOperationID:=dm3.insert_operation('����� �� ��������','-1000');



    end;
    Screen.Cursor:=crHourGlass;
  ////////////////
  Analiz := nil;

  try

    try
      // if not (assigned(dmIndividualOrder))  then

      dmIndividualOrder := TdmIndividualOrder.Create(nil);

      if  dmIndividualOrder.Connection.Connected  then

      begin
      Analiz := TfrmAnalizSklad.Create(Self);

      TimerIndOrder.Enabled:=true;
      
      Analiz.ShowModal;

      end
      else 
         MessageDlg('����������� ����������.���������� ������������ �������', mtError, [mbOk], 0);

    except

      On E: Exception do
      begin

        Screen.Cursor := crDefault;

        MessageDlg('������ �������� �����:' + E.Message, mtError, [mbOk], 0);

      end;

    end;

  finally

    FreeAndNil(Analiz);

   // FreeAndNil(dmIndividualOrder);

    Screen.Cursor := crDefault;

  end;
///////////////////////////////////


     dm3.update_operation(LogOperationID);
    end

end;

procedure TfmMain.N83Click(Sender: TObject);
begin
TAnalisisDateSell.Execute;
end;

procedure TfmMain.N84Click(Sender: TObject);
begin
 ShowAndFreeForm(TFmActionST, Self, TForm(FmActionST), True, False);
end;

procedure TfmMain.miExperimentClick(Sender: TObject);
var i:integer;
    LogOperationID:string;
begin
  dm.quTmp.Close;
  if not dmcom.tr.Active then dmcom.tr.StartTransaction;
  with dm, quTmp do
   begin
    SQL.Text:='SELECT UIDWHCALC FROM D_REC';
    ExecQuery;
    i:=Fields[0].AsInteger;
    Close;
    if i>0 then raise Exception.Create('��������� ������ ����������� ������ �������������.')
    else  begin
     LogOperationID:=dm3.insert_operation('����� �� ��������','-1000');

     ShowAndFreeForm(TfmUIDWH2, Self, TForm(fmUIDWH2), True, False);

     fmUIDWH2 := nil;

     dm3.update_operation(LogOperationID);
    end
   end;
end;
procedure TfmMain.acBuyExecute(Sender: TObject);
var
  Dialog: TDialogBuy;
  Rect: TRect;
  MainMenuHeight: Integer;
begin
  MainMenuHeight := GetSystemMetrics(SM_CYMENU);

  Rect := ClientRect;

  Rect.TopLeft := ClientToScreen(Rect.TopLeft);

  Rect.BottomRight := ClientToScreen(Rect.BottomRight);

  Dialog := TDialogBuy.Create(nil);

  Dialog.X := Rect.Left + siBuy.Left;

  Dialog.Y := Rect.Top + MainMenuHeight + sb1.Height + 4;

  Dialog.Execute;

  Dialog.Free;
end;

procedure TfmMain.siCouponClick(Sender: TObject);
begin
  TDialogCampaign.Execute;
end;

end.




