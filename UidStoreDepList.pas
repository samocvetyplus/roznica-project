unit UidStoreDepList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Mask, DBCtrlsEh, DB, FIBDataSet,
  pFIBDataSet, Grids, DBGridEh, ActnList, ComCtrls, TB2Item, Menus,
  FR_DSet, FR_DBSet, FIBQuery, pFIBQuery, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmUidStoreDepList = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    lbYear: TLabel;
    edYear: TDBNumberEditEh;
    taUIDStoreDepList: TpFIBDataSet;
    taUIDStoreDepListSINVID: TFIBIntegerField;
    taUIDStoreDepListSN: TFIBIntegerField;
    taUIDStoreDepListSDATE: TFIBDateTimeField;
    taUIDStoreDepListDEPNAME: TFIBStringField;
    taUIDStoreDepListMONTHNAME: TStringField;
    dg1: TDBGridEh;
    dsrUIDStoreDepList: TDataSource;
    taUIDStoreDepListDEPID: TFIBIntegerField;
    taUIDStoreDepListUSERID: TFIBIntegerField;
    taUIDStoreDepListUSERNAME: TFIBStringField;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    taUIDStoreDepListNDATE: TFIBDateTimeField;
    stbrMain: TStatusBar;
    taUIDStoreDepListITYPE: TFIBSmallIntField;
    taUIDStoreDepListDEPCOLOR: TFIBIntegerField;
    acExit: TAction;
    siView: TSpeedItem;
    acView: TAction;
    siRecalc: TSpeedItem;
    taUIDStoreDepListCRDATE: TFIBDateTimeField;
    acRefresh: TAction;
    acFillEntryData: TAction;
    ppDoc: TTBPopupMenu;
    TBItem2: TTBItem;
    pmDep: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    fms: TFormStorage;
    acExport: TAction;
    acSumm: TAction;
    siSumm: TSpeedItem;
    siExport: TSpeedItem;
    taUIDStoreDepListRSTATE: TFIBSmallIntField;
    taUIDStoreDepListState: TStringField;
    quTmp: TpFIBQuery;
    siHelp: TSpeedItem;
    acViewManager: TAction;
    taUIDStoreDepListEDITTR: TFIBSmallIntField;
    biViewManager: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taUIDStoreDepListCalcFields(DataSet: TDataSet);
    procedure taUIDStoreDepListBeforeOpen(DataSet: TDataSet);
    procedure acAddExecute(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acExitExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure acViewUpdate(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure acRefreshUpdate(Sender: TObject);
    procedure acFillEntryDataExecute(Sender: TObject);
    procedure acFillEntryDataUpdate(Sender: TObject);
    procedure taUIDStoreDepListBeforeDelete(DataSet: TDataSet);
    procedure acExportExecute(Sender: TObject);
    procedure acExportUpdate(Sender: TObject);
    procedure acSummExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acViewManagerExecute(Sender: TObject);
    procedure acSummUpdate(Sender: TObject);
    procedure acViewManagerUpdate(Sender: TObject);
    procedure dg1DblClick(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    FYear : word;
    LogOperIdForm : string;
    procedure Create_UIDStoreDep;
    procedure WriteTimeExec(CallTime : Cardinal);
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure DepClick(Sender: TObject);
  end;

var
  fmUidStoreDepList: TfmUidStoreDepList;

implementation

{$R *.dfm}

uses dbUtil, DateUtils, comdata, UtilLib, Data3, Data, JewConst, fmUtils,
  UidStoreDepItem, ReportData, MsgDialog, Period, InsRepl, UIDStoreDep_T_C;

function Acc(q: TpFIBDataSet): boolean;
begin
  Result:=true;
end;

procedure TfmUidStoreDepList.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper := wp;
  tb2.WallPaper := wp;
  edYear.OnChange := nil;
  FYear := YearOf(Now);
  edYear.Value := FYear;
  edYear.OnChange := edYearChange;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  dmCom.tr.StartTransaction;
  if CenterDep then
  begin
    dmCom.FillDepMenu(dmCom.quDep, pmDep, Acc, DepClick, True);
    dm.SDep := pmDep.Items[0].Caption;// '��� ������';
    dm.SDepId := pmDep.Items[0].Tag; // -1;
  end
  else begin
    //dmCom.FillDepMenu(dmCom.quDep, pmDep, Acc, DepClick, True);
    siDep.Visible := False;
    dm.SDep := dmCom.Dep[SelfDepId].SName;
    dm.SDepId := SelfDepId;
    laDep.Caption:=dm.SDep;
  end;
  OpenDataSet(taUIDStoreDepList);
  if taUIDStoreDepListEDITTR.AsInteger=0 then acViewManager.Caption:='����������� ����������'
  else acViewManager.Caption:='����� ������� "����������� ����������"';
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmUidStoreDepList.edYearChange(Sender: TObject);
begin
  FYear := edYear.Value;
  ReOpenDataSet(taUIDStoreDepList);
end;

procedure TfmUidStoreDepList.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmUidStoreDepList.taUIDStoreDepListCalcFields(DataSet: TDataSet);
begin
  taUIDStoreDepListMONTHNAME.AsString := cMonth[MonthOf(taUIDStoreDepListSDATE.AsDateTime)];
  if Centerdep then
  begin
   if (taUIDStoreDepListEDITTR.IsNull) or (taUIDStoreDepListEDITTR.AsInteger=0)  then taUIDStoreDepListState.AsString:=''
   else taUIDStoreDepListState.AsString:='�����������';
  end
  else
   case taUIDStoreDepListRSTATE.AsInteger of
    0: taUIDStoreDepListState.AsString:='';
    1: taUIDStoreDepListState.AsString:='��������� � �������';
    2: taUIDStoreDepListState.AsString:='�������';
    3: taUIDStoreDepListState.AsString:='�����������';    
   end;
end;

procedure TfmUidStoreDepList.taUIDStoreDepListBeforeOpen(DataSet: TDataSet);
begin
  taUIDStoreDepList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(FYear));
  taUIDStoreDepList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(FYear));
  if dm.SDepId=-1 then
  begin
   taUIDStoreDepList.ParamByName('DEPID1').AsInteger := -MaxInt;
   taUIDStoreDepList.ParamByName('DEPID2').AsInteger := MaxInt;
  end
  else
  begin
   taUIDStoreDepList.ParamByName('DEPID1').AsInteger := dm.SDepId;
   taUIDStoreDepList.ParamByName('DEPID2').AsInteger := dm.SDepId;
  end
end;

procedure TfmUidStoreDepList.acAddExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Month : byte;
  bd, ed: TDateTime;
  LogOperationID: string;
begin
  if CenterDep and (dm.SDepId = -1) then  raise EWarning.Create('�������� �����');
//  if CenterDep and (dm.SDepId <> CenterDepId) then raise EWarning.Create('�������� ����� ������ ����� �� ����������� �����');

  Bookmark := taUIDStoreDepList.GetBookmark;
  Month := 0;
  taUIDStoreDepList.DisableControls;
  try
    taUIDStoreDepList.First;
    while not taUIDStoreDepList.Eof do
    begin
      if CenterDep then
      begin
        if (MonthOf(taUIDStoreDepListSDATE.AsDateTime) > Month) and (taUIDStoreDepListDEPID.AsInteger = dm.SDepId)then
          Month := MonthOf(taUIDStoreDepListSDATE.AsDateTime);
      end
      else begin
        if (MonthOf(taUIDStoreDepListSDATE.AsDateTime) > Month) and (taUIDStoreDepListDEPID.AsInteger = dm.SDepId)then
          Month := MonthOf(taUIDStoreDepListSDATE.AsDateTime);
      end;
      taUIDStoreDepList.Next;
    end;
  finally
    taUIDStoreDepList.GotoBookmark(Bookmark);
    taUIDStoreDepList.EnableControls;
  end;
  Inc(Month);
  if Month=13 then
  begin
    MessageDialog('��� �������� �� '+IntToStr(FYear)+' ������������', mtInformation, [mbOk], 0);
    eXit;
  end;
  bd := StartOfAMonth(FYear, Month);
  ed := EndOfAMonth(FYear, Month);
  if not GetPeriod(bd, ed) then eXit;
  LogOperationID := dm3.insert_operation(sLog_UidStoreDepAdd, LogOperIdForm);
  if dm.countmaxpross then sysUtils.Abort;  
  ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);
  taUIDStoreDepList.Append;
  taUIDStoreDepListSINVID.AsInteger := dmCom.GetId(8);
  taUIDStoreDepListSN.AsInteger := Month;
  taUIDStoreDepListSDATE.AsDateTime := bd;
  taUIDStoreDepListNDATE.AsDateTime := ed;
  if CenterDep then taUIDStoreDepListDEPID.AsInteger := dm.SDepId
  else taUIDStoreDepListDEPID.AsInteger := SelfDepId;
  taUIDStoreDepListUSERID.AsInteger := dmCom.UserId;
  taUIDStoreDepListCRDATE.AsDateTime := dmCom.GetServerTime;
  taUIDStoreDepList.Post;
  Application.ProcessMessages;
  Create_UIDStoreDep;
  ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);  
  dm3.update_operation(LogOperationID);
end;

procedure TfmUidStoreDepList.Create_UIDStoreDep;
begin
  ExecSQL('execute procedure UIDSTORE_DEP('+taUIDStoreDepListSINVID.AsString+')', dmCom.quTmp);
  WriteTimeExec(dmCom.quTmp.CallTime);
  RefreshDataSet(taUIDStoreDepList);
end;

procedure TfmUidStoreDepList.WriteTimeExec(CallTime: Cardinal);
begin
  stbrMain.SimpleText := '����� ����������: '+Format('%.2f', [CallTime/1000])+'�';
end;

procedure TfmUidStoreDepList.dg1GetCellParams(
  Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if CenterDep then
  begin
   if taUIDStoreDepListState.AsString<>'' then Background := clSilver
   else Background := clWindow
  end else Background := clWindow;

  if (Column.FieldName = 'DEPNAME')and(not taUIDStoreDepListDEPCOLOR.IsNull)
  then Background := taUIDStoreDepListDEPCOLOR.AsInteger;
end;

procedure TfmUidStoreDepList.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then  MinimizeApp
  else inherited;
end;

procedure TfmUidStoreDepList.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUidStoreDepList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taUIDStoreDepList);
  Action := caFree;
end;

procedure TfmUidStoreDepList.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmUidStoreDepList.acAddUpdate(Sender: TObject);
begin
 if AppDebug then   acAdd.Enabled := taUIDStoreDepList.Active
 else acAdd.Enabled := taUIDStoreDepList.Active and (dm.SDepId = SelfDepId);
end;

procedure TfmUidStoreDepList.acDelExecute(Sender: TObject);
var LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_UIDStoreDepRefresh, LogOperIdForm);
  taUIDStoreDepList.Delete;
  dm3.update_operation(LogOperationID);  
end;

procedure TfmUidStoreDepList.acDelUpdate(Sender: TObject);
begin
if AppDebug then
  acDel.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty)
else
  acDel.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty) and
    (taUIDStoreDepListDEPID.AsInteger = SelfDepId);
end;

procedure TfmUidStoreDepList.acViewExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmUidStoreDepItem, Self, TForm(fmUidStoreDepItem), True, False);
end;

procedure TfmUidStoreDepList.acViewUpdate(Sender: TObject);
begin
  TAction(Sender).Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty)
   and (not CenterDep);
end;

procedure TfmUidStoreDepList.acRefreshExecute(Sender: TObject);
var LogOperationID: string;
begin
  if dm.countmaxpross then sysUtils.Abort;
//  if CenterDep and (dm.SDepId <> CenterDepId) then raise EWarning.Create('�������� ����� ������ ����� �� ����������� �����');
  if MessageDialog('�������� ��������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  ExecSQL('update d_rec set calcwhdate=calcwhdate+1', dm.quTmp);
  LogOperationID := dm3.insert_operation(sLog_UIDStoreDepRefresh, LogOperIdForm);
  Create_UIDStoreDep;
  ExecSQL('update sinv set rstate=0 where sinvid='+taUIDStoreDepListSINVID.AsString, qutmp);
  taUIDStoreDepList.Refresh;
  ExecSQL('update d_rec set calcwhdate=calcwhdate-1', dm.quTmp);  
  dm3.update_operation(LogOperationID);
end;

procedure TfmUidStoreDepList.acRefreshUpdate(Sender: TObject);
begin
if AppDebug then
  acRefresh.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty)
else
  acRefresh.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty)
   and (not CenterDep);
end;

procedure TfmUidStoreDepList.acFillEntryDataExecute(Sender: TObject);
begin
//  if CenterDep and (dm.SDepId <> CenterDepId) then raise EWarning.Create('��������� �������� ����� ������ ��� ������ �� ����������� �����');
  ExecSQL('execute procedure UIDSTOREDEP_FILLENTRY_BY_WH('+taUIDStoreDepListSINVID.AsString+')', dm3.quTmp);
  WriteTimeExec(dm3.quTmp.CallTime);
  taUIDStoreDepList.Refresh;
end;

procedure TfmUidStoreDepList.acFillEntryDataUpdate(Sender: TObject);
begin
  acFillEntryData.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty)
   and (not CenterDep);
end;

procedure TfmUidStoreDepList.DepClick(Sender: TObject);
begin
  try
    Screen.Cursor:=crSQLWait;
    taUIDStoreDepList.Active:=False;
    TMenuItem(Sender).Checked:=True;
    dm.SDepId:= TMenuItem(Sender).Tag;
    dm.SDep:= TMenuItem(Sender).Caption;
    laDep.Caption:=dm.SDep;
    taUIDStoreDepList.Active:=True;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure TfmUidStoreDepList.taUIDStoreDepListBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ��������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TfmUidStoreDepList.acExportExecute(Sender: TObject);
var NeedSPR, NeedSPR1, PacketId:integer;
    CodeDep, CodedepC:string;
    flDescription:boolean;
begin
  with dm do
 begin
  qutmp.SQL.Text:='select r_code from d_dep where WH=1';
  qutmp.ExecQuery;
  CodeDepC:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_code from d_dep where d_depid='+inttostr(SelfDepId);
  qutmp.ExecQuery;
  CodeDep:=qutmp.Fields[0].AsString;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
  qutmp.SQL.Text:='select r_packetid from r_packet where PacketState=0 and r_item='#39
                  +'DESCRIPTION'+#39' and sourceid='#39+CodeDep+#39;
  qutmp.ExecQuery;
  if qutmp.Fields[0].IsNull then  flDescription:=false else flDescription:=true;
  qutmp.Transaction.CommitRetaining;
  qutmp.Close;
 end;

 NeedSPR1:=0;
 NeedSPR:=0;

 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);
  
  ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
  InsRPacket(dmCom.db, 'DELRECORDS', CodeDepC, 4, CodeDepC+';4;', '��������� ������ �� �������� ����', 1,NeedSPR, PacketId);

  InsRPacket(dmCom.db, 'UIDSTOREDEP', CodeDepC, 2,
               taUIDStoreDepListSINVID.AsString, '�������� ����� �'+ taUIDStoreDepListSN.AsString, 1,NeedSPR, PacketId);

 ExecSQL('update sinv set rstate=1 where sinvid='+taUIDStoreDepListSINVID.AsString, qutmp);
 taUIDStoreDepList.Refresh;

 if (not flDescription) then
  InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC, 0, '', '������ ��������', 1,NeedSPR1, PacketId);

 MessageDialog('������������ ������ ��� �������� �������� ���� ���������!', mtInformation, [mbOk], 0);
end;

procedure TfmUidStoreDepList.acExportUpdate(Sender: TObject);
begin
  acExport.Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty) and (not CenterDep);
end;

procedure TfmUidStoreDepList.acSummExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmUidStoreDep_T_C, Self, TForm(fmUidStoreDep_T_C), True, False);
end;

procedure TfmUidStoreDepList.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100440)
end;

procedure TfmUidStoreDepList.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUidStoreDepList.acViewManagerExecute(Sender: TObject);
begin
 if taUIDStoreDepListEDITTR.AsInteger=1 then begin
  ExecSQL('update sinv set edittr=0 where sinvid='+taUIDStoreDepListSINVID.AsString, quTmp);
  taUIDStoreDepList.Refresh;
 end
 else begin
  ExecSQL('update sinv set edittr=1 where sinvid='+taUIDStoreDepListSINVID.AsString, quTmp);
  taUIDStoreDepList.Refresh;
 end;
end;

procedure TfmUidStoreDepList.acSummUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty);
end;

procedure TfmUidStoreDepList.acViewManagerUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := taUIDStoreDepList.Active and (not taUIDStoreDepList.IsEmpty) and
  CenterDep;

 if taUIDStoreDepListEDITTR.AsInteger=0 then acViewManager.Caption:='����������� ����������'
 else acViewManager.Caption:='����� ������� "����������� ����������"';
end;

procedure TfmUidStoreDepList.dg1DblClick(Sender: TObject);
begin
 if (not CenterDep) and  (not taUIDStoreDepListSINVID.IsNull)  then acViewExecute(nil);
end;

procedure TfmUidStoreDepList.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN, VK_SPACE:  if (not CenterDep) and
   (not taUIDStoreDepListSINVID.IsNull)  then acViewExecute(nil);
 end
end;

end.
