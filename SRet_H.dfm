object fmSRet_H: TfmSRet_H
  Left = 210
  Top = 89
  Cursor = crArrow
  ActiveControl = LogEd
  BorderStyle = bsToolWindow
  ClientHeight = 370
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 108
    Width = 134
    Height = 19
    Caption = #1048#1089#1090#1086#1088#1080#1103' '#1076#1086#1082#1091#1084#1077#1085#1090#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaLogin: TLabel
    Left = 24
    Top = 306
    Width = 78
    Height = 15
    Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object LaPsw: TLabel
    Left = 59
    Top = 331
    Width = 44
    Height = 15
    Caption = #1055#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentFont = False
  end
  object OpSBtn: TSpeedButton
    Left = 275
    Top = 314
    Width = 73
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    OnClick = OpSBtnClick
  end
  object ClSBtn: TSpeedButton
    Left = 360
    Top = 311
    Width = 81
    Height = 25
    Caption = #1047#1072#1082#1088#1099#1090#1100
    OnClick = ClSBtnClick
  end
  object CnlSBtn: TSpeedButton
    Left = 452
    Top = 311
    Width = 65
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    OnClick = CnlSBtnClick
  end
  object Label9: TLabel
    Left = 1
    Top = 280
    Width = 294
    Height = 17
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1074#1086#1077' '#1080#1084#1103' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103' '#1080' '#1087#1072#1088#1086#1083#1100':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 132
    Width = 521
    Height = 137
    Color = clBtnHighlight
    DataGrouping.GroupLevels = <>
    DataSource = dsrDlist_H
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 224
      end
      item
        DisplayFormat = 'dd.mm.yyyy   hh:nn'
        EditButtons = <>
        FieldName = 'HDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Width = 130
      end
      item
        EditButtons = <>
        FieldName = 'STATUS'
        Footers = <>
        Title.Caption = #1044#1077#1081#1089#1090#1074#1080#1077
        Width = 141
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object PswEd: TEdit
    Left = 116
    Top = 331
    Width = 145
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnKeyDown = PswEdKeyDown
  end
  object LogEd: TEdit
    Left = 116
    Top = 306
    Width = 145
    Height = 21
    TabOrder = 2
    OnKeyDown = LogEdKeyDown
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 527
    Height = 97
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    DesignSize = (
      527
      97)
    object Label2: TLabel
      Left = 64
      Top = 28
      Width = 76
      Height = 13
      Caption = #1044#1072#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 56
      Top = 8
      Width = 87
      Height = 13
      Caption = #8470' '#1072#1082#1090#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 296
      Top = 16
      Width = 87
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1089#1082#1083#1072#1076':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 84
      Top = 73
      Width = 61
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 406
      Top = 16
      Width = 42
      Height = 13
      AutoSize = True
      DataField = 'DEPFROM'
      DataSource = dm.dsSRetList
    end
    object dbSup: TDBText
      Left = 149
      Top = 73
      Width = 31
      Height = 13
      AutoSize = True
      DataField = 'COMP'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label8: TLabel
      Left = 48
      Top = 56
      Width = 96
      Height = 13
      Caption = #1043#1088#1091#1079#1086#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object dbt_Gruzootprav: TDBText
      Left = 149
      Top = 56
      Width = 79
      Height = 13
      AutoSize = True
      DataField = 'Gruzootprav'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label12: TLabel
      Left = 309
      Top = 64
      Width = 74
      Height = 13
      Anchors = [akTop, akRight]
      Caption = #1054#1073#1097#1072#1103' '#1089#1091#1084#1084#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText1: TDBText
      Left = 405
      Top = 64
      Width = 50
      Height = 13
      Anchors = [akTop, akRight]
      AutoSize = True
      DataField = 'COST'
      DataSource = dm.dsSRetList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edSN: TDBEdit
      Left = 148
      Top = 4
      Width = 93
      Height = 21
      Color = clInfoBk
      DataField = 'SN'
      DataSource = dm.dsSRetList
      TabOrder = 0
    end
    object dbedSdate: TDBEditEh
      Left = 148
      Top = 25
      Width = 77
      Height = 21
      DataField = 'SDATE'
      DataSource = dm.dsSRetList
      EditButtons = <>
      Enabled = False
      ReadOnly = True
      TabOrder = 1
      Visible = True
    end
    object bttime: TBitBtn
      Left = 222
      Top = 24
      Width = 20
      Height = 22
      TabOrder = 2
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000530B0000530B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF085A8E08548408548408548408
        5484085484085484085484085484085484085484FF00FFFF00FFFF00FFFF00FF
        0A639B1073AEFEFEFDFCFCFBF8F8F7F4F4F3F0F0EFECECEBE8E8E7D9D9D9D2D2
        D21073AE085484FF00FFFF00FFFF00FF0A639B1178B3FEFEFDFEFEFDFEFEFDFA
        FAF9F6F6F5F2F2F1EEEEEDEAEAE9E0E0E01178B3085484FF00FFFF00FFFF00FF
        0A649C137CB7FEFEFDDEE1DF5A6962D0D4D2FAFAF9F6F6F586908BEEEEEDEAEA
        E9137CB7085484FF00FFFF00FFFF00FF0A649D137CB7FEFEFD5A6962FEFEFD5A
        6962C6CAC7FAFAF954645CF2F2F1EEEEED137CB7085484FF00FFFF00FFFF00FF
        0A659D1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CF6F6F5F2F2
        F11580BC085484FF00FFFF00FFFF00FF0B659E1580BCFEFEFDC6CAC75A6962C6
        CAC7FEFEFDFEFEFD54645CFAFAF9F6F6F51580BC085484FF00FFFF00FFFF00FF
        0B669E1580BCFEFEFDFEFEFDFEFEFD5A6962C6CAC7FEFEFD54645CFEFEFDFAFA
        F91580BC085484FF00FFFF00FFFF00FF0B679F1580BCFEFEFDC6CAC75A6962B3
        BAB6FEFEFD5A69625A6962FEFEFDFEFEFD1580BC085484FF00FFFF00FFFF00FF
        0B68A01580BCFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFEFDFEFE
        FD1580BC085484FF00FFFF00FFFF00FF0C69A11580BCFEFEFD8C94948C9494FE
        FEFDFEFEFDFEFEFD8C94948C9494FEFEFD1580BC085484FF00FFFF00FFFF00FF
        0C6AA21580BC6FBDEFAAAAAA4A5A526FBDEF6FBDEF6FBDEFAAAAAA4A5A526FBD
        EF1580BC085484FF00FFFF00FFFF00FF0D6BA41178B3147EBAF0F0F08C949414
        7EBA147EBA147EBAF0F0F08C9494147EBA1178B3085A8EFF00FFFF00FFFF00FF
        FF00FF0960970960970960970960970960970960970960970960970960970960
        97096097FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
    end
  end
  object fs1: TFormStorage
    Active = False
    Options = [fpState]
    UseRegistry = False
    StoredValues = <>
    Left = 476
    Top = 104
  end
  object taHist: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into HIST_DOC'
      '('
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      ')'
      'values'
      '('
      '   :DOCID,'
      '   :FIO,'
      '   current_timestamp,'
      '   :STATUS,'
      '   7'
      ')')
    SelectSQL.Strings = (
      'select'
      '   HISTID,'
      '   DOCID,'
      '   FIO,'
      '   HDATE,'
      '   STATUS,'
      '   TYPEDOC'
      'from HIST_DOC'
      'where DOCID=:INVID and TYPEDOC = 7'
      'order by HDATE')
    BeforeOpen = taHistBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 360
    Top = 100
    oRefreshAfterPost = False
    object taHistHISTID: TFIBIntegerField
      FieldName = 'HISTID'
    end
    object taHistDOCID: TFIBIntegerField
      FieldName = 'DOCID'
    end
    object taHistFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taHistHDATE: TFIBDateTimeField
      FieldName = 'HDATE'
    end
    object taHistSTATUS: TFIBStringField
      FieldName = 'STATUS'
      Size = 10
      EmptyStrToNull = True
    end
    object taHistTYPEDOC: TFIBSmallIntField
      FieldName = 'TYPEDOC'
    end
  end
  object dsrDlist_H: TDataSource
    DataSet = taHist
    Left = 320
    Top = 100
  end
  object taEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ALIAS, FIO, PSWD'
      'FROM D_EMP')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 432
    Top = 100
    object taEmpALIAS: TFIBStringField
      FieldName = 'ALIAS'
      EmptyStrToNull = True
    end
    object taEmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taEmpPSWD: TFIBStringField
      FieldName = 'PSWD'
      EmptyStrToNull = True
    end
  end
  object DataSource1: TDataSource
    DataSet = taEmp
    Left = 400
    Top = 100
  end
end
