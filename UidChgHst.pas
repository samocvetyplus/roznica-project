unit UidChgHst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, ExtCtrls, Mask,
  M207Ctrls, DBGridEhGrouping, rxPlacemnt, rxSpeedbar,
  rxToolEdit, GridsEh;

type
   TfmUChHist = class(TForm)
    DBGridEh1: TDBGridEh;
    tb1: TSpeedBar;
    laPeriod: TLabel;
    SpeedbarSection1: TSpeedbarSection;
    siOpen: TSpeedItem;
    siExit: TSpeedItem;
    spltWH: TSplitter;
    DBGridEh2: TDBGridEh;
    lbFind: TLabel;
    edArt: TComboEdit;
    Label1: TLabel;
    edUID: TComboEdit;
    siHelp: TSpeedItem;
    fr1: TM207FormStorage;
    procedure FormCreate(Sender: TObject);
    procedure siOpenClick(Sender: TObject);
    procedure DBGridEh1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure siExitClick(Sender: TObject);
    procedure DBGridEh2GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure edUIDButtonClick(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtButtonClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siHelpClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
     procedure ShowPeriod;
  public
    { Public declarations }
  end;

var
  fmUChHist: TfmUChHist;

implementation

uses data3, comData,Period,M207Proc, dbUtil, DB;
{$R *.dfm}

procedure TfmUChHist.FormCreate(Sender: TObject);
begin
  tb1.Wallpaper:=wp;
  ShowPeriod;
  ReOpenDataSets([dm3.quUIDChg, dmCom.DepSortWithDel]);
  edUID.Text := '';
  edArt.Text := '';
end;
procedure TfmUChHist.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm3.MBD) + ' �� ' +DateToStr(dm3.MED);
end;


procedure TfmUChHist.siOpenClick(Sender: TObject);
begin
  with dm3 do
    if GetPeriod(MBD, MED) then
      begin
        ShowPeriod;
        ReOpenDataSets([quUIDChg]);
      end;
end;

procedure TfmUChHist.DBGridEh1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'SNAME') and (dm3.quDetalChgDEPID.AsInteger>0) then
    Background := TColor(dm3.quDetalChgCOLOR.AsInteger)
  else Background := clInfoBk;

end;

procedure TfmUChHist.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmUChHist.DBGridEh2GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'UID') then
                              Background := dmCom.clMoneyGreen
  else if (Column.FieldName = 'OLDVALID') or (Column.FieldName = 'NEWVALID') then
                              Background := clLtGray
  else Background := clInfoBk;
end;

procedure TfmUChHist.edUIDButtonClick(Sender: TObject);
var
  Key : word;
begin
  Key := VK_RETURN;
  edUIDKeyDown(Sender, Key, []);
end;

procedure TfmUChHist.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_RETURN :
      try
        Screen.Cursor := crSQLWait;
        edUID.Glyph.Handle := LoadBitmap(hInstance, 'STOP');
        dm3.FilterUID := trim(edUID.Text);
        ReOpenDataSets([dm3.quUIDChg]);
      finally
        edUID.Glyph.Handle := LoadBitmap(hInstance, 'FIND');
        Screen.Cursor := crDefault;
      end;
  end;
end;
procedure TfmUChHist.edArtButtonClick(Sender: TObject);
var
  Key : word;
begin
  Key := VK_RETURN;
  edArtKeyDown(Sender, Key, []);
end;

procedure TfmUChHist.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    VK_RETURN :
      try
        Screen.Cursor := crSQLWait;
        edART.Glyph.Handle := LoadBitmap(hInstance, 'STOP');
        dm3.FilterArt := trim(edART.Text);
        ReOpenDataSets([dm3.quUIDChg]);
      finally
        edArt.Glyph.Handle := LoadBitmap(hInstance, 'FIND');
        Screen.Cursor := crDefault;
      end;
  end;

end;

procedure TfmUChHist.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100550)
end;

procedure TfmUChHist.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmUChHist.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
