object fmeClient: TfmeClient
  Left = 869
  Top = 238
  ActiveControl = edFIO
  BorderStyle = bsDialog
  Caption = #1053#1086#1074#1099#1081' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1100
  ClientHeight = 202
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 26
    Width = 88
    Height = 13
    Caption = #1060#1048#1054' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
  end
  object Label2: TLabel
    Left = 0
    Top = 2
    Width = 130
    Height = 13
    Caption = #1053#1086#1084#1077#1088' '#1076#1080#1089#1082#1086#1085#1090#1085#1086#1081' '#1082#1072#1088#1090#1099
  end
  object Label3: TLabel
    Left = 1
    Top = 58
    Width = 31
    Height = 13
    Caption = #1040#1076#1088#1077#1089
  end
  object Label4: TLabel
    Left = 1
    Top = 88
    Width = 41
    Height = 13
    Caption = #1044#1086#1084'/'#1050#1074
  end
  object Label5: TLabel
    Left = 0
    Top = 120
    Width = 79
    Height = 13
    Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
  end
  object edFIO: TDBEditEh
    Left = 136
    Top = 24
    Width = 137
    Height = 19
    DataField = 'NAME'
    DataSource = dm2.dsClient
    EditButtons = <>
    Flat = True
    TabOrder = 1
    Visible = True
    OnKeyDown = edFIOKeyDown
    OnKeyUp = edFIOKeyUp
    OnMouseUp = edFIOMouseUp
  end
  object btnOk: TButton
    Left = 64
    Top = 168
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    ModalResult = 1
    TabOrder = 6
    OnKeyDown = btnOkKeyDown
    OnKeyUp = btnOkKeyUp
  end
  object btnNo: TButton
    Left = 168
    Top = 168
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1080#1090#1100
    ModalResult = 2
    TabOrder = 7
    OnKeyDown = btnNoKeyDown
    OnKeyUp = btnNoKeyUp
  end
  object edAddress: TDBEditEh
    Left = 56
    Top = 56
    Width = 217
    Height = 19
    DataField = 'ADDRESS'
    DataSource = dm2.dsClient
    EditButtons = <>
    Flat = True
    TabOrder = 2
    Visible = True
    OnKeyDown = edAddressKeyDown
    OnKeyUp = edAddressKeyUp
    OnMouseUp = edAddressMouseUp
  end
  object edHome_Flat: TDBEditEh
    Left = 56
    Top = 90
    Width = 185
    Height = 21
    DataField = 'HOME_FLAT'
    DataSource = dm2.dsClient
    EditButtons = <>
    TabOrder = 4
    Visible = True
    OnKeyDown = edHome_FlatKeyDown
    OnKeyUp = edHome_FlatKeyUp
    OnMouseUp = edHome_FlatMouseUp
  end
  object edNoDCard: TDBEditEh
    Left = 136
    Top = 0
    Width = 137
    Height = 21
    DataField = 'NODCARD'
    DataSource = dm2.dsClient
    EditButtons = <>
    Enabled = False
    TabOrder = 0
    Visible = True
    OnKeyDown = edNoDCardKeyDown
    OnKeyUp = edNoDCardKeyUp
    OnMouseUp = edNoDCardMouseUp
  end
  object DBfotherDep: TDBCheckBoxEh
    Left = 40
    Top = 144
    Width = 185
    Height = 17
    Caption = #1044#1080#1089#1082'. '#1082#1072#1088#1090#1072' '#1101#1090#1086#1075#1086' '#1092#1080#1083#1080#1072#1083#1072
    DataField = 'FMAIN'
    DataSource = dm2.dsClient
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object dbBirthday: TDBDateTimeEditEh
    Left = 88
    Top = 120
    Width = 153
    Height = 21
    DataField = 'BIRTHDAY'
    DataSource = dm2.dsClient
    EditButtons = <>
    Kind = dtkDateEh
    TabOrder = 8
    Visible = True
    OnKeyUp = dbBirthdayKeyUp
  end
  object lbaddress: TListBox
    Left = 57
    Top = 72
    Width = 214
    Height = 89
    BorderStyle = bsNone
    ItemHeight = 13
    TabOrder = 3
    Visible = False
    OnDblClick = lbaddressDblClick
    OnKeyDown = lbaddressKeyDown
  end
end
