{****************************************}
{     ���������� �����������             }
{  Copyrigth(C) 2000-2004                }
{ ��������� ������������ �����������     }
{  ������  - ���������� ���              }
{  ������� - ��������� �������           }
{  ���������� - ���������� ��� ��������� }
{****************************************}

//************* ������� ��������� **************//

unit Comp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, ExtCtrls, Grids, DBGrids, RXDBCtrl, StdCtrls, DBCtrls,
  Mask, M207Grid, Menus, db, M207IBGrid,
  RxMenus, ActnList, DBGridEh, DBCtrlsEh, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar, rxToolEdit, dxSkinsCore, dxSkinsdxBarPainter,
  dxSkinsDefaultPainters, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxBar, FIBDataSet, pFIBDataSet, ImgList, cxCheckBox,
  cxDBLookupComboBox, cxContainer, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxDBEdit, FIBDatabase, pFIBDatabase, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin;

type
  TfmComp = class(TForm)
    StatusBar1: TStatusBar;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    cbPhis: TDBCheckBox;
    Label1: TLabel;
    edAddress: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edPhone: TDBEdit;
    edEMail: TDBEdit;
    edFax: TDBEdit;
    edWWW: TDBEdit;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    pc1: TPageControl;
    TabSheet1: TTabSheet;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label24: TLabel;
    edINN: TDBEdit;
    edBIK: TDBEdit;
    edOKPO: TDBEdit;
    edOKONH: TDBEdit;
    edBank: TDBEdit;
    edBill: TDBEdit;
    edKBank: TDBEdit;
    edKBill: TDBEdit;
    edAdrBill: TDBEdit;
    edBoss: TDBEdit;
    edBossPhone: TDBEdit;
    edBuh: TDBEdit;
    edBuhPhone: TDBEdit;
    edOFIO: TDBEdit;
    edOPhone: TDBEdit;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    edPaspSer: TDBEdit;
    edPaspNum: TDBEdit;
    deDistrDate: TDBDateEdit;
    edDistrPlace: TDBEdit;
    Label19: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    Label20: TLabel;
    DBEdit2: TDBEdit;
    edCity: TDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    edPostIndex: TDBEdit;
    pmCat: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siCategory: TSpeedItem;
    laCompCat: TLabel;
    TabSheet5: TTabSheet;
    DBImage1: TDBImage;
    od1: TOpenDialog;
    tsMol: TTabSheet;
    tb3: TSpeedBar;
    SpeedbarSection3: TSpeedbarSection;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    M207IBGrid1: TM207IBGrid;
    N4: TMenuItem;
    Label25: TLabel;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    cbSeller: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    cbBuyer: TDBCheckBox;
    Label30: TLabel;
    DBEdit5: TDBEdit;
    Label31: TLabel;
    DBDateEdit4: TDBDateEdit;
    spitPrint: TSpeedItem;
    ppPrint: TRxPopupMenu;
    mnitAll: TMenuItem;
    mnitCur: TMenuItem;
    fs1: TFormStorage;
    st1: TMenuItem;
    siSort: TSpeedItem;
    Label32: TLabel;
    lcPayType: TDBLookupComboBox;
    Label33: TLabel;
    lcNDS: TDBLookupComboBox;
    Label34: TLabel;
    edINN2: TDBEdit;
    Label35: TLabel;
    edPaddress: TDBEdit;
    lcCountry: TDBLookupComboBox;
    N6: TMenuItem;
    ActionList1: TActionList;
    acNewRecords: TAction;
    edCode: TEdit;
    Label36: TLabel;
    dg1: TDBGridEh;
    pmdg1: TPopupMenu;
    NUseComp: TMenuItem;
    acUseComp: TAction;
    edBossPost: TDBEdit;
    Label37: TLabel;
    edKPP: TDBEdit;
    Action1: TAction;
    dbcommission: TDBCheckBox;
    siHelp: TSpeedItem;
    chworkorg: TDBCheckBox;
    chretorg: TDBCheckBox;
    chAllWorkOrg: TCheckBox;
    cbWorkProd: TDBCheckBox;
    chWorkBuyer: TDBCheckBox;
    simeneger: TSpeedItem;
    pmMeneger: TPopupMenu;
    NAll: TMenuItem;
    LMeneger: TLabel;
    NNotMeneger: TMenuItem;
    Label38: TLabel;
    Edit1: TEdit;
    Label39: TLabel;
    N5: TMenuItem;
    DBCheckBox6: TDBCheckBox;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    BarDockControl: TdxBarDockControl;
    View: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    Contract: TpFIBDataSet;
    SourceContract: TDataSource;
    actionAddContract: TAction;
    actionDeleteContract: TAction;
    Images: TImageList;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    ContractID: TFIBIntegerField;
    ContractCONTRACTTYPE: TFIBIntegerField;
    ContractCONTRACTDATE: TFIBDateTimeField;
    ContractCONTRACTENDDATE: TFIBDateTimeField;
    ContractISUNLIMITED: TFIBSmallIntField;
    ContractTAG: TFIBStringField;
    ContractCOMMENT: TFIBStringField;
    ContractISDELETE: TFIBIntegerField;
    ViewCONTRACTTYPE: TcxGridDBColumn;
    ViewCONTRACTDATE: TcxGridDBColumn;
    ViewCONTRACTENDDATE: TcxGridDBColumn;
    ViewISUNLIMITED: TcxGridDBColumn;
    ViewTAG: TcxGridDBColumn;
    ViewCOMMENT: TcxGridDBColumn;
    ContractNUMBER: TFIBStringField;
    ViewNUMBER: TcxGridDBColumn;
    ContractCOMPANYID: TFIBIntegerField;
    ContractAGENTID: TFIBIntegerField;
    Label26: TLabel;
    REUDateEdit: TcxDBDateEdit;
    dxBarButton3: TdxBarButton;
    actionSaveChanges: TAction;
    ViewCOMPANYID: TcxGridDBColumn;
    SelfCompanys: TpFIBDataSet;
    SelfCompanysDataSource: TDataSource;
    DBRadioGroup1: TDBRadioGroup;
    Label27: TLabel;
    edADDRESSPOST: TDBEdit;
    function Stop: boolean;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N3Click(Sender: TObject);
    procedure SpeedItem4Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure siExitClick(Sender: TObject);
    procedure DBImage1DblClick(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure mnitAllClick(Sender: TObject);
    procedure mnitCurClick(Sender: TObject);
    procedure st1Click(Sender: TObject);
    procedure siSortClick(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure acNewRecordsExecute(Sender: TObject);
    procedure edCodeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edDogEdChange(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acUseCompUpdate(Sender: TObject);
    procedure acUseCompExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure dg1KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dg1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chAllWorkOrgClick(Sender: TObject);
    procedure NAllClick(Sender: TObject);
    procedure fmMainNManagerClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure dg1CellMouseClick(Grid: TCustomGridEh; Cell: TGridCoord;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ContractBeforeOpen(DataSet: TDataSet);
    procedure ContractNewRecord(DataSet: TDataSet);
    procedure actionAddContractExecute(Sender: TObject);
    procedure actionDeleteContractExecute(Sender: TObject);
    procedure ContractBeforeClose(DataSet: TDataSet);
    procedure ContractAfterPost(DataSet: TDataSet);
    procedure actionSaveChangesExecute(Sender: TObject);
    procedure actionSaveChangesUpdate(Sender: TObject);
    procedure actionDeleteContractUpdate(Sender: TObject);

  private
    Compid:integer;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;

var
  fmComp: TfmComp;
  StopFlag: Boolean;

  implementation

uses comdata, Data, Data2, ReportData, InsRepl, M207Proc, Sort, DBTree,
  FIBQuery, data3, UseComp, MsgDialog;

{$R *.DFM}

function TfmComp.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmComp.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmComp.FormCreate(Sender: TObject);
var  pm:TMenuItem;
begin
  Width:=Screen.Width;
  Height:=Screen.Height;
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  tb3.WallPaper:=wp;
  

  with dmCom do
    begin
      CompCat:=100;
    end;

  SelfCompanys.Active := true;  

  with dmCom, dm2 do
    begin
//      taEmp.Active:=false;
//      taEmp.SelectSQL[3]:=' and PERMISSIONS_USER in (0,1) and d_depid='+inttostr(CenterDepId);
      taComp.Active:=false;
      taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and (workorg=1 or workprod=1 or workbuyer=1)';
      taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-2]:='';      
      OpenDataSets([taPayType, taNDS, taCountry, qudep, taContractType, taComp]);
    end;

//  Contract.Active := true;
//
//  if not Contract.Transaction.Active then
//  begin
//    Contract.Transaction.StartTransaction;
//  end;

  SetCBDropDown(lcPayType);
  SetCBDropDown(lcNDS);
  SetCBDropDown(lcCountry);

  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select r.d_compid from d_rec r';
  dm.quTmp.ExecQuery;
  compid:=dm.quTmp.Fields[0].AsInteger;
  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;

  if dmcom.taCompSELLER.AsInteger=1 then
    chretorg.Enabled:=true
  else
    chretorg.Enabled:=false;

{  if CenterDep then dg1.FieldColumns['FIOUSER'].Visible:=true
  else dg1.FieldColumns['FIOUSER'].Visible:=false;}
//  dg1.FieldColumns['FIOUSER'].Visible:=true;

//  if not CenterDep then
//  begin
      simeneger.Visible:=false;
      LMeneger.Visible:=false
//  end else
//  begin
//   simeneger.Visible:=true;
//   LMeneger.Visible:=true;
//
//   dmcom.taEmp.First;
//   while not (dmcom.taEmp.Eof) do
//   begin
//    pm:=TMenuItem.Create(pmMeneger);
//    pm.Caption:=dmCom.taEmpFIO.Asstring;
//    pm.Tag:=dmCom.taEmpD_EMPID.AsInteger;
//    pm.OnClick:=NAllClick;
//
//    pmMeneger.Items.Add(pm);
//
//    dmcom.taEmp.Next;
//   end;
//   dmcom.taEmp.First;
//  end
end;

procedure TfmComp.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;  
end;



procedure TfmComp.FormClose(Sender: TObject; var Action: TCloseAction);
var i:string;
    j:integer;
begin
  if CenterDep then begin
   dm.quTmp.Close;
   dm.quTmp.sql.Text:='select count(*), Code from d_comp group by CODE having count(*)>=2';
   dm.quTmp.ExecQuery;
   i:='';j:=0;
   while not dm.quTmp.eof do
   begin
    if j=0 then i:='��� ������� ���� ��������'+#13#10+dm.quTmp.Fields[1].AsString
    else i:=i+#13#10+dm.quTmp.Fields[1].AsString;
    inc(j);
    dm.quTmp.Next;
   end;
   dm.quTmp.Close;
   if i<>'' then raise Exception.Create(i);
  end;

  if Contract.Active then
  begin
    Contract.Close;
  end;

  if SelfCompanys.Active then
  begin
    SelfCompanys.Close;
  end;

  with dmCom, dm2 do
    begin
      PostDataSets([taComp, taMol]);
      {quTmp.SQL.Text:='execute procedure upd_comp_prod';
      quTmp.ExecQuery;
      tr.CommitRetaining;}
      if dm.WORKMODE<>'SINV' then CloseDataSets([taMol, taComp, taPayType, taNDS, taCountry, qudep{, taEmp}])
      else CloseDataSets([taMol, taComp, taCountry, qudep, taContractType]);
      tr.CommitRetaining;
      dm.LoadArtSL(COMP_DICT);
//      taEmp.SelectSQL[3]:='';
    end;
  dm.R_Item:='';
  dmcom.taComp.Filtered := False;
end;

procedure TfmComp.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var wc: TWinControl;
begin
  if (Key=VK_RETURN) and (ActiveControl<>edCode)  then
    begin
      Key:=VK_TAB;
      wc:=FindNextControl(ActiveControl, True, True, False);
      if wc<>NIL then ActiveControl:=wc;
    end;
end;

procedure TfmComp.N3Click(Sender: TObject);
begin
  with dmCom, taComp do
    begin
      Active:=False;
      CompCat:=TComponent(Sender).Tag;
      case CompCat of
          1: SelectSQL[SelectSQL.Count-4]:='WHERE SELLER>=1 AND D_COMPID<>-1000';
          2: SelectSQL[SelectSQL.Count-4]:='WHERE BUYER=1 AND D_COMPID<>-1000';
          3: SelectSQL[SelectSQL.Count-4]:='WHERE PRODUCER=1 AND D_COMPID<>-1000';
          4: SelectSQL[SelectSQL.Count-4]:='WHERE IS$BANK=1 AND D_COMPID<>-1000';
          else SelectSQL[SelectSQL.Count-4]:='WHERE D_COMPID<>-1000';
        end;
      if not chAllWorkOrg.Checked then
      case CompCat of
          1: SelectSQL[SelectSQL.Count-3]:=' and workorg=1 ';
          2: SelectSQL[SelectSQL.Count-3]:=' and workbuyer=1 ';
          3: SelectSQL[SelectSQL.Count-3]:=' and workprod=1 ';
          else SelectSQL[SelectSQL.Count-3]:=' and (workorg=1 or workbuyer=1 or workprod=1) ';
        end
      else SelectSQL[SelectSQL.Count-3]:='';
      Open;
      (***********���������� check*****************)
      if dmcom.taCompSELLER.AsInteger>=1 then
      begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
      else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

     if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
     else cbWorkProd.Enabled:=false;

     if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
     else chWorkBuyer.Enabled:=false;
     (********************************************)
      case CompCat of
          1: laCompCat.Caption:='����������';
          2: laCompCat.Caption:='������� ����������';
          3: laCompCat.Caption:='������������';
          4: laCompCat.Caption:='�����';
          else laCompCat.Caption:='���';
        end;
    end;
end;

procedure TfmComp.SpeedItem4Click(Sender: TObject);
begin
 if dmCom.taComp.Locate ('CODE','<�� �����>',[]) then
  raise Exception.Create('��� ���������� ����� �������'+#13#10+
                        '�������� �������� <�� �����> � ���� ���');

 dmCom.taComp.Append;
end;

procedure TfmComp.SpeedItem5Click(Sender: TObject);
begin
 dm.qutmp.Close;
 dm.qutmp.sql.Clear;
 dm.qutmp.sql.text:= 'select * from BUY$INVOICE where agent$id='+ dmcom.taCompD_COMPID.AsString;
 dm.qutmp.ExecQuery;
 if dm.qutmp.FieldByName('id').AsInteger <> 0 then
   begin
      MessageDlg('�������� ����������! � ������������ ������� ������ �� ������������.',mtError, [mbOK], 0);
      exit;
   end; 
  dmCom.taComp.Delete;


 (*****************************************************************)
  if dmcom.taCompSELLER.AsInteger>=1 then
  begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
  else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

  if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
  else cbWorkProd.Enabled:=false;

  if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
  else chWorkBuyer.Enabled:=false;




 (******************************************************************)
end;

procedure TfmComp.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmComp.DBImage1DblClick(Sender: TObject);
begin
  if od1.Execute then
    with dmCom, taComp do
      begin
        if NOT (State in [dsInsert, dsEdit]) then Edit;
        taCompLogo.LoadFromFile(od1.FileName);
      end;
end;

procedure TfmComp.SpeedItem2Click(Sender: TObject);
begin
//  dmCom.taMol.Append;
end;

procedure TfmComp.SpeedItem3Click(Sender: TObject);
begin
//  dmCom.taMol.Delete;
end;

procedure TfmComp.FormActivate(Sender: TObject);
var i:string;
    j:integer;
begin
  pmCat.Items[4].Click;
//  dmCom.taMol.Active:=True;
  if CenterDep then begin
  if dmcom.taComp.Locate ('CODE','<�� �����>',[]) then
   Application.MessageBox('��� ���������� ����� �������'+#13#10+
                          '�������� �������� <�� �����> � ���� ���',
                          '��������',0);
  dm.quTmp.Close;
  dm.quTmp.sql.Text:='select count(*), Code from d_comp group by CODE having count(*)>=2';
  dm.quTmp.ExecQuery;
  i:='';j:=0;
  while not dm.quTmp.eof do
   begin
    if j=0 then i:='��� ������� ���� ��������'+#13#10+dm.quTmp.Fields[1].AsString
    else i:=i+#13#10+dm.quTmp.Fields[1].AsString;
    inc(j);
    dm.quTmp.Next;
   end;
  dm.quTmp.Close;
  if i<>'' then MessageDialog(i, mtInformation, [mbOk], 0);
  end;
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmComp.mnitAllClick(Sender: TObject);
begin
  PrintDict(dmCom.dsComp, 35); //'cpa'
end;

procedure TfmComp.mnitCurClick(Sender: TObject);
begin
  PrintDict(dmCom.dsComp, 36); //'cpo'
end;

procedure TfmComp.st1Click(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr:=gen_arr(dg1, dmcom.taCompD_COMPID);
  try
    PrintDocument(arr, cpa);
  finally
    Finalize(arr);
  end;
end;

procedure TfmComp.siSortClick(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_COMPID, SNAME '+
                       'FROM D_COMP '+
//                       'WHERE PRODUCER=1'+
                      ' ORDER BY SortInd';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_COMP set SortInd=?SortInd where D_CompId=?D_CompId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              ReOpenDataSets([dmCom.taComp]);
            end
        end;
    finally
      fmSort.Free;
      (*****************************************************************)
      if dmcom.taCompSELLER.AsInteger>=1 then
      begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
      else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

      if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
      else cbWorkProd.Enabled:=false;

      if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
      else chWorkBuyer.Enabled:=false;
      (******************************************************************)
    end;
end;

procedure TfmComp.N6Click(Sender: TObject);
begin
  PrintDict(dmCom.dsComprep, 135); //'cpa'
end;

procedure TfmComp.acNewRecordsExecute(Sender: TObject);
begin
 if dmcom.taComp.Locate ('CODE','<�� �����>',[]) then
  raise Exception.Create('��� ���������� ����� �������'+#13#10+
                        '�������� �������� <�� �����> � ���� ���');
 dmcom.taComp.Append;
end;

procedure TfmComp.edCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: begin
    if dmCom.taComp.Locate('CODE',edCode.Text,[]) then
    begin
      ActiveControl:=dg1;
      dg1.SelectedRows.CurrentRowSelected:=true;
      edCode.Text:='';

      (*****************************************************************)
      if dmcom.taCompSELLER.AsInteger>=1 then
      begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
      else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

      if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
      else cbWorkProd.Enabled:=false;

      if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
      else chWorkBuyer.Enabled:=false;
      (******************************************************************)
    end
    else
     Application.MessageBox('��� �� ������','��������!',0);
   end
 end;
end;

procedure TfmComp.edDogEdChange(Sender: TObject);
var
  fModify : boolean; 
begin
  // ���� ���� �����������, ����� ���� �� ����� ���� �� ������������
//  if(edDogEd.Date <> 0)then
//  begin
//    fModify := dmCom.taComp.State in [dsEdit, dsInsert];
//    if not fModify then dmCom.taComp.Edit;
//    dmCom.taCompDOGUNLIMITED.AsInteger := 0;
//    if not fModify then dmCom.taComp.Post;
//  end
end;

procedure TfmComp.Edit1Change(Sender: TObject);
begin
 with dmCom, taComp do
 if (Edit1.Text='')then taComp.Filtered := False
  else begin
    taComp.DisableScrollEvents;
    taComp.DisableControls;
    try
      taComp.Filtered := False;

      taComp.Filter := 'upper(SNAME) like upper('#39 + '%' + Edit1.Text + '%' + #39 + ')';
      taComp.Filtered := True;
    finally
      taComp.EnableScrollEvents;
      taComp.EnableControls;
    end;
  end;
    //if taComp.Active then
    // LocateF(dmCom.taComp, 'SNAME', Edit1.Text, [loBeginingPart], False, Stop);

end;

procedure TfmComp.dg1CellMouseClick(Grid: TCustomGridEh; Cell: TGridCoord;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  //Edit1.Text:=dmCom.taComp.FieldByName('SNAME').AsString;
end;

procedure TfmComp.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
var
  i : byte;
begin
  if Column.Field.FieldName='NAME' then
  with dmCom do
  begin
    i := 0;
    {
    if(taCompDOGUNLIMITED.AsInteger = 0)and(not taCompDOGBD.IsNull)then
    begin
       if(taCompDOGED.IsNull)or(taCompDOGED.AsDateTime< strtodatetime(datetostr(dmCom.GetServerTime))) then
       begin
         inc(i,1);
       end;
    end;
    }
    if (not taCompREUED.IsNull) and (taCompREUED.AsDateTime< strtodatetime(datetostr(dmCom.GetServerTime))) then
    begin
      inc(i,2);
    end;

    case i of
      0 : ;
      1 : Background:=dm.CU_RT1;
      2 : Background:=clYellow;
      3 : Background:=dm.CU_RT0;
    end
   end;
  if Column.Field.FieldName='SNAME' then
  begin
   case dmcom.taCompPRODUCER.AsInteger of
   1 : Background:=$0080FF80
   else Background:=clInfoBk;
   end;
  end;
end;

procedure TfmComp.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var i, j: integer;
begin
  with dmCom, taComp do
    if (Key in [9, VK_RETURN]){ and (State=dsInsert)} then
      begin
        j:=dg1.SelectedIndex;
        i:=taCompD_CompId.AsInteger;
        if taCompSNAME.AsString = '' then
        begin
          ActiveControl := dg1;
          dg1.SelectedField := taCompSNAME;
          Key := 0;
          SysUtils.Abort;
        end;

        if (State=dsInsert)or(State=dsEdit) then Post;

        if dg1.SelectedField.Name = 'taCompSNAME' then
        begin
         dm.quTmp.Close;
         dm.quTmp.SQL.Text:='select count(*) from d_comp where Sname ='''+taCompSNAME.AsString+'''';
         dm.quTmp.ExecQuery;
         If (dm.quTmp.Fields[0].AsInteger>1) then
          begin
           taComp.Close;
           taComp.SelectSQL[SelectSQL.Count-1]:='Order by SName ';
           taComp.Open;
           dm.quTmp.Close;
           dg1.SelectedIndex := dg1.SelectedIndex - 1;
           MessageDialog('������� ������������ ������� ����� ���������� ��������', mtWarning, [mbOk], 0);
           j:=j-1;
          end else dm.quTmp.Close;
        end
        else
        if dg1.SelectedField.Name = 'taCompCODE' then
        begin
         dm.quTmp.Close;
         dm.quTmp.SQL.Text:='select count(*) from d_comp where code ='''+taCompCODE.AsString+'''';
         dm.quTmp.ExecQuery;
         If (dm.quTmp.Fields[0].AsInteger>1) then
          begin
           taComp.Close;
           taComp.SelectSQL[SelectSQL.Count-1]:='Order by CODE ';
           taComp.Open;
           dm.quTmp.Close;
           MessageDialog('��� ������ ����� ���������� ��������', mtWarning, [mbOk], 0);
           j:=j-1;
          end else dm.quTmp.Close;
         end;
        Active:=False;
        taComp.Open;
        Locate('D_COMPID', i, []);
        ActiveControl:=dg1;
        dg1.SelectedIndex:=j;
      end;
end;

procedure TfmComp.acUseCompUpdate(Sender: TObject);
begin
 if dmcom.Adm and (not dmcom.taCompD_COMPID.IsNull)then acUseComp.Enabled:=true
 else acUseComp.Enabled:=false;
end;

procedure TfmComp.actionAddContractExecute(Sender: TObject);
begin
  Contract.Append;
end;

procedure TfmComp.actionDeleteContractExecute(Sender: TObject);
var
  Answer: TModalResult;
begin
  Answer := MessageDialog('������� �������?', mtConfirmation, [mbYes, mbNo], 0);

  if Answer = mrNo then
  begin
    Exit;
  end;

  Contract.Edit;

  ContractISDELETE.AsInteger := 1;

  Contract.Post;

  Contract.Delete;
end;

procedure TfmComp.actionDeleteContractUpdate(Sender: TObject);
begin
  actionDeleteContract.Enabled := not Contract.IsEmpty;
end;

procedure TfmComp.actionSaveChangesExecute(Sender: TObject);
var Answer: TModalResult;
begin

  if Contract.State in [dsEdit, dsInsert] then
  begin

    Answer := MessageDialog('��������� ���������?', mtConfirmation, [mbYes, mbNo], 0);

    if Answer = mrNo then
    begin

      dmCom.taComp.Cancel;

    end else
    begin

      Contract.Post;



    end;

  end;

end;

procedure TfmComp.actionSaveChangesUpdate(Sender: TObject);
begin
  actionSaveChanges.Enabled := Contract.State in [dsEdit, dsInsert];
end;

procedure TfmComp.acUseCompExecute(Sender: TObject);
begin
 dm3.D_Compid:=dmcom.taCompD_COMPID.AsInteger;
 ShowAndFreeForm(TfmUseComp, Self, TForm(fmUseComp), True, False);
end;

procedure TfmComp.Action1Execute(Sender: TObject);
begin
  if edKPP.Enabled then MessageDialog('�������� ����', mtInformation, [mbOk], 0)
  else MessageDialog('�� �������� ����', mtInformation, [mbOk], 0);

  if edKPP.ReadOnly then MessageDialog('readonly', mtInformation, [mbOk], 0)
  else MessageDialog('not readonly', mtInformation, [mbOk], 0);
end;

procedure TfmComp.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100102)
end;

procedure TfmComp.dg1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_DOWN, VK_UP: begin
    if dmcom.taCompSELLER.AsInteger>=1 then
    begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
    else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

    if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
    else cbWorkProd.Enabled:=false;

    if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
    else chWorkBuyer.Enabled:=false;
  end;
 end;
end;

procedure TfmComp.dg1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
 if dmcom.taCompSELLER.AsInteger>=1 then
 begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
 else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

 if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
 else cbWorkProd.Enabled:=false;

 if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
 else chWorkBuyer.Enabled:=false;
end;



procedure TfmComp.chAllWorkOrgClick(Sender: TObject);
begin
  dmCom.taComp.Active:=false;
  if chAllWorkOrg.Checked then
  begin
    case dmcom.CompCat of
    1: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workorg=1 ';
    2: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workbuyer=1 ';
    3: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workprod=1 ';
    else dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and (workorg=1 or workbuyer=1 or workprod=1) ';
    end
  end
  else
  begin
    case dmcom.CompCat of
    1: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workorg=0 ';
    2: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workbuyer=0 ';
    3: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workprod=0 ';
    else dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and (workorg=0 and workbuyer=0 and workprod=0) ';
    end
  end;
//end
// dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:='';
 dmCom.taComp.Active:=true;

 {dmCom.taComp.Active:=false;
 if not chAllWorkOrg.Checked then
 begin
  case dmcom.CompCat of
   1: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workorg=1 ';
   2: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workbuyer=1 ';
   3: dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and workprod=1 ';
   else dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:=' and (workorg=1 or workbuyer=1 or workprod=1) ';
  end
 end
 else dmCom.taComp.SelectSQL[dmCom.taComp.SelectSQL.Count-3]:='';
 dmCom.taComp.Active:=true;    }

 (*****************************************************************)
  if dmcom.taCompSELLER.AsInteger>=1 then
  begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
  else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

  if dmcom.taCompPRODUCER.AsInteger>=1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
  else cbWorkProd.Enabled:=false;

  if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
  else chWorkBuyer.Enabled:=false;
  (******************************************************************)
end;

procedure TfmComp.ContractAfterPost(DataSet: TDataSet);
begin
  Contract.Transaction.CommitRetaining;

  Contract.Refresh;
end;

procedure TfmComp.ContractBeforeClose(DataSet: TDataSet);
begin
  actionSaveChanges.Execute;
end;

procedure TfmComp.ContractBeforeOpen(DataSet: TDataSet);
begin
  { TODO : ������������� ��������� �� ����������� ������� ����������� }
  Contract.ParamByName('Company$ID').AsInteger := dmCom.taRecD_COMPID.AsInteger;
end;

procedure TfmComp.ContractNewRecord(DataSet: TDataSet);
begin
  Contract.FieldByName('Company$ID').AsInteger := 1;
  Contract.FieldByName('Agent$ID').AsInteger := dmCom.taCompD_CompID.AsInteger;
  Contract.FieldByName('Number').AsString := '';
  Contract.FieldByName('Comment').AsString := '';
  Contract.FieldByName('Tag').AsString := '';  
  Contract.FieldByName('Contract$Date').AsDateTime := Date;
  Contract.FieldByName('Contract$Type').AsInteger := 1;
  Contract.FieldByName('Is$Delete').AsInteger := 0;
  Contract.FieldByName('Is$Unlimited').AsInteger := 0;
end;

procedure TfmComp.NAllClick(Sender: TObject);
begin
 with dmcom do
   begin

   taComp.Active := false;

   if TMenuItem(Sender).Tag = -1000 then
   begin

     taComp.SelectSQL[taComp.SelectSQL.Count-2] := '';

   end else
   begin

     if TMenuItem(Sender).Tag = -999 then
     begin

       taComp.SelectSQL[taComp.SelectSQL.Count-2] := ' and (d_empid is null)';

     end else
     begin

       taComp.SelectSQL[taComp.SelectSQL.Count-2] := ' and d_empid=' + inttostr(TMenuItem(Sender).Tag) + ' ';

     end;

   end;

  taComp.Active:=true;

  LMeneger.Caption:=TMenuItem(Sender).Caption;
 end;
(*****************************************************************)
 if dmcom.taCompSELLER.AsInteger >= 1 then
 begin chretorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep); chworkorg.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep) end
 else begin chretorg.Enabled:=false; chworkorg.Enabled:=false end;

 if dmcom.taCompPRODUCER.AsInteger >= 1 then cbWorkProd.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
 else cbWorkProd.Enabled:=false;

 if dmcom.taCompBUYER.AsInteger=1 then chWorkBuyer.Enabled:=(GetBit(dmCom.EditRefBook, 4) and CenterDep)
 else chWorkBuyer.Enabled:=false;
 (******************************************************************)
end;






procedure TfmComp.fmMainNManagerClick(Sender: TObject);
begin
 PrintDict(dmCom.dsComp, 235);
end;

end.
