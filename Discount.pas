unit Discount;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, ComCtrls, Grids, DBGrids, RXDBCtrl, M207Grid,
  M207IBGrid, ActnList, PrnDbgeh, DBGridEh, DBGridEhGrouping, GridsEh,
  rxSpeedbar;

type
  TfmDiscount = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    spitPrint: TSpeedItem;
    dgEl: TDBGridEh;
    pdgel: TPrintDBGridEh;
    acList: TActionList;
    acPrintGrid: TAction;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmDiscount: TfmDiscount;

implementation

uses comdata, M207Proc, Data;

{$R *.DFM}

procedure TfmDiscount.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDiscount.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  with dmCom, dm do
    begin
      dgEl.ReadOnly:=not Centerdep;
      SpeedItem2.Enabled:=Centerdep;
      SpeedItem1.Enabled:=Centerdep;
      tr.Active:=True;
      OpenDataSets([taDiscount]);
    end;
end;

procedure TfmDiscount.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmCom do
    begin
      PostDataSets([taDiscount]);
      CloseDataSets([taDiscount]);
      tr.CommitRetaining;
    end;
end;

procedure TfmDiscount.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmDiscount.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmDiscount.SpeedItem2Click(Sender: TObject);
begin
  dmCom.taDiscount.Append;
end;

procedure TfmDiscount.SpeedItem1Click(Sender: TObject);
begin
  dmCom.taDiscount.Delete;
end;

procedure TfmDiscount.spitPrintClick(Sender: TObject);
begin
  PrintDict(dmCom.dsDiscount, 40); //'dsc'
end;

procedure TfmDiscount.acPrintGridExecute(Sender: TObject);
begin
 pdgel.Print;
end;

procedure TfmDiscount.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100111)
end;

procedure TfmDiscount.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
