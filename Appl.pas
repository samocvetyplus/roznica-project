unit Appl;
//
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  DBCtrls, db, StdCtrls, Mask, Menus, DBGridEh,
  ActnList, TB2Item, TB2Dock, TB2Toolbar, PrnDbgeh, Variants, jpeg,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxToolEdit, rxSpeedbar;

type
  TfmAppl = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siAdd: TSpeedItem;
    siPrint: TSpeedItem;
    fmstrAppl: TFormStorage;
    Splitter6: TSplitter;
    Panel1: TPanel;
    plFilter: TPanel;
    Splitter3: TSplitter;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    Splitter8: TSplitter;
    lbComp: TListBox;
    lbMat: TListBox;
    lbGood: TListBox;
    lbIns: TListBox;
    lbCountry: TListBox;
    Splitter1: TSplitter;
    plArt: TPanel;
    tb2: TSpeedBar;
    cbSearch: TCheckBox;
    ceArt: TComboEdit;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    dgArt: TM207IBGrid;
    lbAtt1: TListBox;
    Splitter11: TSplitter;
    lbAtt2: TListBox;
    Splitter10: TSplitter;
    gridAppl: TDBGridEh;
    acEvent: TActionList;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    cbGrMat: TComboBox;
    SpeedItem2: TSpeedItem;
    acPrintAppl: TAction;
    acExportToTxt: TAction;
    acAdd: TAction;
    acDel: TAction;
    acExit: TAction;
    prdg1: TPrintDBGridEh;
    acPrintgrid: TAction;
    Label2: TLabel;
    siHelp: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbGrMatClick(Sender: TObject);
    procedure lbCompClick(Sender: TObject);
    procedure lbCompKeyPress(Sender: TObject; var Key: Char);
    procedure ceArtEnter(Sender: TObject);
    procedure ceArtButtonClick(Sender: TObject);
    procedure ceArtChange(Sender: TObject);
    procedure ceArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ceArtKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cbSearchClick(Sender: TObject);
    procedure miInsFromSearchClick(Sender: TObject);
    procedure dgArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dgArtExit(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
    procedure acExportToTxtExecute(Sender: TObject);
    procedure acPrintApplUpdate(Sender: TObject);
    procedure acExportToTxtUpdate(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acPrintgridExecute(Sender: TObject);
    procedure ceArtClick(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
     SearchEnable: boolean;
     StopFlag: boolean;
     LogOperIdForm : string;
     function  Stop: boolean;
  public
     procedure miAEdit;
  end;

var
  fmAppl: TfmAppl;

implementation

uses ServData, WH, Comdata, ReportData, Data, M207Proc, DBTree, JewConst,
  Data3, AEdit, MsgDialog, dbUtil;

{$R *.DFM}

procedure TfmAppl.FormCreate(Sender: TObject);
var i:integer;
    q: boolean;
begin
  tb1.WallPaper:=wp;
  with dmServ, dmCom do
  begin
    OpenDataSets([taAppl]);
    Caption:='������ � '+taApplListNOAPPL.AsString+'. ������������� '+taApplListSNAME.AsString;
    GrMat:='';
    cbGrMat.Items.Assign(dm.slGrMAt);
    D_CompId:=dmServ.ApplSupid;
    D_MatId:='*';
    D_GoodId:='*';
    D_InsId:='*';
    D_CountryId:='*';
    D_Att1Id := ATT1_DICT_ROOT;
    D_Att2Id := ATT2_DICT_ROOT;
    SearchEnable:=False;
    ceArt.Text:='';
    SearchEnable:=True;
    dm.FillListBoxes(lbComp, lbMat, lbGood, lbIns, lbcountry, lbAtt1, lbAtt2);
    lbMat.ItemIndex:=0;
    lbGood.ItemIndex:=0;
    lbIns.ItemIndex:=0;
    lbCountry.ItemIndex:=0;
    lbAtt1.ItemIndex := 0;
    lbAtt2.ItemIndex := 0;
    q := false;
    with lbComp do
    for i:=0 to Items.Count-1 do
      if TNodeData(Items.Objects[i]).Code=D_CompId then
        begin
          ItemIndex:=i;
          lbCompClick(lbComp);
          q:=true;
          break;
        end;
    if not q then
    begin
      lbComp.ItemIndex:= 0;
      D_CompId:=-1;
    end
  end;
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID)
end;

function TfmAppl.Stop: boolean;
begin
  Result:=StopFlag;
end;

procedure TfmAppl.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmAppl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmServ.taAppl, dmCom.quComp]);
  dmServ.taAppl.Transaction.CommitRetaining;
  dmServ.ApplSupid:=-1;
  dmServ.taApplList.Refresh; 
  Action := caFree;
end;

procedure TfmAppl.cbGrMatClick(Sender: TObject);
begin
//
 dmServ.GrMat:=cbGrMat.Items[cbGrMat.ItemIndex];
 Reopendatasets([dmServ.taAppl]);
end;

procedure TfmAppl.lbCompClick(Sender: TObject);
begin
  dmCom.D_CompId:=TNodeData(lbComp.Items.Objects[lbComp.ItemIndex]).Code;
  dmCom.D_MatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
  dmCom.D_GoodId:=TNodeData(lbGood.Items.Objects[lbGood.ItemIndex]).Code;
  dmCom.D_InsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
  dmCom.D_CountryId:=TNodeData(lbCountry.Items.Objects[lbCountry.ItemIndex]).Code;
  dmCom.D_Att1Id:=TNodeData(lbAtt1.Items.Objects[lbAtt1.ItemIndex]).Code;
  dmCom.D_Att2Id:=TNodeData(lbAtt2.Items.Objects[lbAtt2.ItemIndex]).Code;
end;

procedure TfmAppl.lbCompKeyPress(Sender: TObject; var Key: Char);
begin
  dm.ListBoxKeyPress(Sender, Key);
end;

procedure TfmAppl.ceArtEnter(Sender: TObject);
begin
 with dmCom, dm do
    if (Old_D_CompId<>D_CompId) or
       (Old_D_MatId<>D_MatId) or
       (Old_D_GoodId<>D_GoodId) or
       (Old_D_CountryId<>D_CountryId) or
       (Old_D_InsId<>D_InsId) or
       (Old_D_Att1Id<>D_Att1Id) or
       (Old_D_Att2Id<>D_Att2Id)
    then begin
         Old_D_CompId:=D_CompId;
         Old_D_MatId:=D_MatId;
         Old_D_GoodId:=D_GoodId;
         Old_D_InsId:=D_InsId;
         Old_D_countryId:=D_countryId;
         Old_D_Att1Id:=D_Att1Id;
         Old_D_Att2Id:=D_Att2Id;
         if cbSearch.Checked then Art:=''
         else Art:=ceArt.Text;
         ReopenDataSets([quArt]);
   end;
end;

procedure TfmAppl.ceArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmAppl.ceArtChange(Sender: TObject);
begin
  cbSearch.Tag:=0;
  if SearchEnable then
    begin
      if cbSearch.Checked then
        begin
          StopFlag:=False;
          with dm, quArt do
            if quArt.Active then
             begin
              LocateF(dm.quArt, 'ART', ceArt.Text, [loBeginingPart], False, Stop);
             end;
        end;
    end;
end;

procedure TfmAppl.ceArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     VK_RETURN: with dm do
                   begin
                     if cbSearch.Checked then
                       begin
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                         if (quArt.FieldByName('ART').asString<>ceArt.Text) then miInsFromSearchClick(Sender);
                       end
                     else
                       begin
                         if cbSearch.Tag=0 then
                           begin
                             Art:=ceArt.Text;
                             ReopenDataSets([quArt]);
                             cbSearch.Tag:=1;
                             if (quArt.FieldByName('ART').asString<>ceArt.Text) then
                                miInsFromSearchClick(Sender);
                           end;
                         with ceArt do
                           if SelLength=0 then SelectAll
                           else SelStart:=Length(ceArt.Text);
                       end;
                    ActiveControl:=dgArt;
                   end;
     VK_INSERT: if Shift=[ssCtrl] then  miInsfromSearchClick(nil);
     VK_ESCAPE: StopFlag:=True;
    end;
end;

procedure TfmAppl.ceArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_DOWN then
  begin
    ActiveControl:=dgArt;
  end
end;

procedure TfmAppl.cbSearchClick(Sender: TObject);
begin
 if cbSearch.Checked then
    with dm do
      begin
        Art:='';
        ReopenDataSets([quArt]);
      end;
  cbSearch.Tag:=0;
end;

procedure TfmAppl.miInsFromSearchClick(Sender: TObject);
begin
  with dm, quArt do
    begin
      Append;
      quArtArt.AsString:=ceArt.Text;
    end
end;

procedure TfmAppl.dgArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  begin
  if (Key=VK_SPACE) and (dgArt.SelectedField.FieldName='UNITID') then
    with dm, quArt do
      begin
        if NOT (State in [dsInsert, dsEdit]) then Edit;
        with quArtUnitId do
          AsInteger:=Integer(NOT Boolean(AsInteger));
      end
  else if (Key=VK_RETURN) then
    begin
      acAdd.Execute;
      ReopenDatasets([dmServ.taAppl]); //  ��� ���� , ����� �������
{  ������� � ������ ����� � �������� ���������, �� ��������� ���������� ������ � ���� �� �������� ��� ���� ��� ������� ������ ������}
      ActiveControl:=gridAppl;
      gridAppl.FieldColumns['Q'].Field.FocusControl;
      Key:=0;
      if not dmServ.taAppl.Locate('Art;Q', VarArrayOf([dm.quArtART.AsVariant,0]) ,[]) then
        dmServ.taAppl.Locate('Art', dm.quArtART.AsVariant ,[]);
    end
  else
    with dm, quArt do
      if NOT (State in [dsInsert, dsEdit]) then Edit;
end;

procedure TfmAppl.dgArtExit(Sender: TObject);
begin
  PostDataSets([dm.quArt]);
end;

procedure TfmAppl.acPrintApplExecute(Sender: TObject);
var
  arr : TarrDoc;
  LogOperationID: string;
  count_ApplArt2: variant;
begin
  PostDataSets([dmServ.taAppl, dmServ.taApplList]);
  SetLength(arr, 1);
  arr[0] := dmServ.taApplListAPPLID.asInteger;
  LogOperationID := dm3.insert_operation(sLog_ApplPrint, LogOperIdForm);
  try
    count_ApplArt2 := ExecSelectSQL('select count(*) from aitem where art2 is not null and applid='+ dmserv.taApplListAPPLID.AsString, dmCom.quTmp);
    if (count_ApplArt2=0) then
       PrintDocument(arr, appl_sup1) else
       PrintDocument(arr, appl_sup);
  finally
    finalize(arr);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmAppl.acExportToTxtExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  PostDataSets([dmServ.taAppl, dmServ.taApplList]);
  LogOperationID := dm3.insert_operation(sLog_ApplExportTxt, LogOperIdForm);
  dmReport.AppleText(dmServ.taApplListAPPLID.AsInteger, dmServ.taApplListSNAME.AsString);
  dm3.update_operation(LogOperationID);
end;

procedure TfmAppl.acPrintApplUpdate(Sender: TObject);
begin
  acPrintAppl.Enabled := dmserv.taApplList.Active and dmServ.taAppl.Active and
    (not dmServ.taAppl.IsEmpty);
end;

procedure TfmAppl.acExportToTxtUpdate(Sender: TObject);
begin
  acExportToTxt.Enabled := dmServ.taApplList.Active and dmServ.taAppl.Active and
    (not dmServ.taAppl.IsEmpty);
end;

procedure TfmAppl.acExitExecute(Sender: TObject);
begin
  if dmserv.taAppl.State in [dsInsert, dsEdit] then dmserv.taAppl.Post;
  Close;
end;

procedure TfmAppl.acDelExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplDelRecord, LogOperIdForm);
  dmServ.taAppl.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmAppl.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dmServ.taAppl.Active;
end;

procedure TfmAppl.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dmServ.taAppl.Active and (not dmServ.taAppl.IsEmpty);
end;

procedure TfmAppl.acAddExecute(Sender: TObject);
var
  LogOperationID : string;
begin
  LogOperationID := dm3.insert_operation(sLog_ApplAddRecord, LogOperIdForm);
  if  (not dm.quArt.Active) or (dm.quArtART.AsString = '')  then
  begin
    MessageDialog('���������� ������ ������� � ���� "�������"', mtWarning, [mbOk], 0);
    exit;
  end;
  dmServ.taAppl.Append;
  dmServ.taApplQ.AsInteger :=0;
  dmServ.taAppl.Post;
  ActiveControl:=gridAppl;
  gridAppl.FieldColumns['Q'].Field.FocusControl;
  if not dmServ.taAppl.Locate('Art;Q', VarArrayOf([dm.quArtART.AsVariant,0]) ,[]) then
    dmServ.taAppl.Locate('Art', dm.quArtART.AsVariant ,[]);
  dm3.update_operation(LogOperationID);
end;

procedure TfmAppl.acPrintgridExecute(Sender: TObject);
begin
 prdg1.Print;
end;

procedure TfmAppl.ceArtClick(Sender: TObject);
begin
  if dmserv.taAppl.State in [dsInsert, dsEdit] then dmserv.taAppl.Post;
end;
procedure TfmAppl.miAEdit;
var LogOperationID: string;
begin
 {log}
 LogOperationID:=dm3.insert_operation('�������������� ������� ��������',LogOperIdForm);
 {***}


  PostDataSets([dm.taA2, dm.quArt]);
  if ShowAndFreeForm(TfmAEdit, Self, TForm(fmAEdit), True, False)=mrOK then
    with dm.taSEl do
      begin
        Active:=False;
        Open;
      end;
 if dm.WorkMode='SINV' then dm.taSList.Refresh;

 {log}
 dm3.update_operation(LogOperationID);
 {***}
end;


procedure TfmAppl.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100282)
end;

procedure TfmAppl.FormActivate(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
  siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

end.
