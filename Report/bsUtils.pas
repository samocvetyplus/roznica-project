unit bsUtils;

interface

uses
  Windows, Classes, SysUtils, Dialogs;

const
  cscDigit : set of char = ['0','1','2','3','4','5','6','7','8','9'];
  csbDigit : set of byte = [0,1,2,3,4,5,6,7,8,9];

type
  TPChar = array [0..255] of char;
  TGramme = double;
  TDate = type TDateTime;

  (* ����� = ������������, �����������, ��������� *)
  TCase = (subjective, genetive, dative);
  (* ��� = �������, ������� *)
  TGender = (Male, Female);
  (* ������ ����� *)
  TSection = (Hundred, Thousand, Million, Milliard);
  (* ������� ��������� *)
  TMeasureUnit = (muRouble, muGramme, muPiece);


                (* Report routines uses in report *)
function GetUnit(iUnit : integer) : string;(* ���������� ��. ���������*)
function GetAssay(Assay : PChar) : string;(* ���������� �����*)
function GetCapitalsMoney(dRouble : double) : string;(* ���������� ����� ������ �������� *)
function GetCapitalsQuantity(lwQ : LongWord) : string;(* ���������� ���������� �������� *)
function GetCapitalsWeight(dW : double) : string;(* ���������� ��� �������� *)



                (* Standart Word routines *)
(* ����� - ������������������ ��������, �� ���������� ������������.
   ������������ �������� ������, ��� ASCII-��� ������ 32. *)
function GetFirstWord(s : string) : string;(* ���������� ������ ����� *)
function GetExFirstWord(s : PChar) : PChar;overload;(* ���������� ������ ��� ������� �����. *)
function GetExFirstWord(s : string) : string;overload;(* ���������� ������ ��� ������� �����. *)
function GetWord(s : string; n : word) : string; (* ���������� n-�� �����. � ������ ������� '' *)
function GetPrPath(Path : PChar) : PChar;

                (* Convert number to capital string routines *)
{procedure GetGrammeByValue(lwValue : LongWord; pcResult : PChar);
procedure GetRoubleByValue(lwValue : LongWord; pcResult : PChar);
procedure GetNumByValue(lwValue : LongWord; pcResult : PChar);}
(* ���������� ����� ��������. �������� �� 0 �� 4 294 967 295 *)
function IntToCapitalsStr(lwValue : LongWord; Gender : TGender = male) : PChar;
(* ���������� ����� c ��������� ������ ��������. �������� ����������� *)
function FloatToCapitalsStr(dValue : double; Gender : TGender = male) : PChar;

function GetDateInGenetiveCase(Date : TDate): string;
function GetCommaWord(s : string; ind : integer) : string;

                (* Code Page Routines*)
procedure WinToDos(fn : string);
function DosToWin(fn : string) : boolean;

implementation

Uses RxStrUtils;

function PickOutSect(lwValue : LongWord; Sect : TSection) : word;
begin
  case Sect of
    Hundred : Result := lwValue mod 1000;
    Thousand : Result := (lwValue div 1000) mod 1000;
    Million : Result := (lwValue div 1000000) mod 1000;
    Milliard : Result := (lwValue div 1000000000);
  else Result := 0;
  end
end;

function GetUnit(iUnit : integer) : string;
begin
  if iUnit=0 then Result := '��.' else Result := '��.';
end;

function GetAssay(Assay : PChar) : string;
begin
  Result := StrPas(GetExFirstWord(Assay));
end;


function GetFirstWord(s : string) : string;
var
  i : integer;
  sTmp : string;
begin
  sTmp := s;
  i := 1;
  while i<=Length(sTmp) do // ������� ������ �����������
  begin
    if sTmp[i]<=#32 then Delete(sTmp, i, 1)
    else i := Length(sTmp)+1;
  end;
  i := 1;
  while i<=Length(sTmp) do   // �������� ������ �����
  begin
    if sTmp[i]=' ' then
    begin
      Delete(sTmp, i, Length(sTmp));
      Result := sTmp;
      eXit;
    end;
    inc(i);
  end;
  Result := sTmp;
end;

function GetExFirstWord(s : PChar) : PChar;
var
  i : integer;
  sFirstWord : string;
  sTmp : string;
begin
  sFirstWord := GetFirstWord(s); // ��������  ������ �����
  i := Pos(sFirstWord, s); // i - ������� ������ ������� �����
  sTmp := StrPas(s);
  Delete(sTmp, 1, i + Length(sFirstWord));
  Result := PChar(sTmp);
end;

function GetExFirstWord(s : string) : string;
begin
  Result := StrPas(GetExFirstWord(PChar(s)));
end;

function GetWord(s : string; n : word) : string;
var
  iWord : integer;
  f, nf  : string;
begin
  Result := '';
  if (n <= 0) then eXit;
  f := GetFirstWord(s);
  nf := f;
  iWord := 0;
  while (f<>'') and (iWord<n) do
  begin
    nf := f;
    s := GetExFirstWord(s);
    f := GetFirstWord(s);
    inc(iWord);
  end;
  if (iWord=n)and(nf<>'') then Result := nf;
end;

function GetPrPath(Path : PChar) : PChar;
var
  sTmp : string;
  i : word;
begin
  sTmp := StrPas(Path);
  i := Length(sTmp)-1;
  while (sTmp[i]<>'\')or(i<=1) do dec(i);
  Delete(sTmp, i, Length(sTmp)-1);
  Result := PChar(sTmp+'\');
end;

procedure GetWordByValueA(wValue : word; pcWord, pcResult : PChar; const Ending : array of PChar);
const
  // ��-�� ��� ���� ���� ��������, ������� ...
  sb1 : set of byte = [1];
  sb2 : set of byte = [2, 3, 4];
  sb3 : set of byte = [5, 6, 7, 8, 9, 0];
  sbSp : set of byte = [11, 12, 13, 14, 15, 16, 17, 18, 19];
var
  Buffer : PChar;
begin
  GetMem(Buffer, 100);
  StrCopy(Buffer, pcWord);
  if ((wValue mod 100) in sbSp) or ((wValue mod 10) in sb3) then StrCat(Buffer, Ending[2])
  else if (wValue mod 10) in sb2 then StrCat(Buffer, Ending[1])
       else if (wValue mod 10) in sb1 then StrCat(Buffer, Ending[0]);
  StrCat(Buffer, ' '#0);
  StrCopy(pcResult, Buffer);
  FreeMem(Buffer);
end;

procedure GetRoubleByValue(lwValue : LongWord; pcResult : PChar);
begin
  GetWordByValueA(lwValue, '����'#0, pcResult, ['�'#0, '�'#0, '��'#0]);
end;

procedure GetNumByValue(lwValue : LongWord; pcResult : PChar);
begin
  GetWordByValueA(lwValue, '����', pcResult , ['�'#0, '�'#0, ''#0]);
end;

procedure GetGrammeByValue(lwValue : LongWord; pcResult : PChar);
begin
  GetWordByValueA(lwValue, '�����'#0, pcResult, ['�'#0, '�'#0, '��'#0]);
end;

procedure GetMilliardByValue(wValue : word; pcResult : PChar);
begin
  GetWordByValueA(wValue, '��������'#0, pcResult, [''#0, '�'#0, '��'#0]);
end;

procedure GetMillionByValue(wValue : word; pcResult : PChar);
begin
  GetWordByValueA(wValue, '�������'#0, pcResult, [''#0, '�'#0, '��'#0]);
end;

procedure GetThousandByValue(wValue : word; pcResult : PChar);
begin
  GetWordByValueA(wValue, '�����'#0, pcResult, ['�'#0, '�'#0, ''#0]);
end;

procedure GetSafeByValue(wValue : word; pcResult : PChar);
begin
  GetWordByValueA(wValue, '���'#0, pcResult, ['��'#0, '��'#0, '��'#0]);
end;

procedure GetCentesimalByValue(wValue : word; pcResult : PChar);
begin
  GetWordByValueA(wValue, '���'#0, pcResult, ['��'#0, '��'#0, '��'#0]);
end;

procedure SectToStr(wValue : word; pcResult : PChar; Gender : TGender);
const
  arpcSectDigit_M : array[0..9] of PChar =
        (''#0, '����'#0, '���'#0, '���'#0, '������'#0, '����'#0, '�����'#0,
        '����'#0, '������'#0, '������'#0);
  arpcSectDigit_F : array[0..9] of PChar =
        (''#0, '����'#0, '���'#0, '���'#0, '������'#0, '����'#0, '�����'#0, '����'#0,
        '������'#0, '������'#0);
  arpcSectTen : array[0..9] of PChar =
        (''#0, ''#0, '��������'#0, '��������'#0, '�����'#0, '���������'#0,
         '����������'#0, '���������'#0, '�����������'#0, '���������'#0);
  arpcSectTen_ : array[1..9] of PChar =
        ('�����������'#0, '����������'#0, '����������'#0, '������������'#0,
         '����������'#0, '�����������'#0, '����������'#0, '������������'#0,
         '������������'#0);


  arpcSectHundred : array[0..9] of PChar =
        (''#0, '���'#0, '������'#0, '������'#0, '���������'#0, '�������'#0,
        '��������'#0, '�������'#0, '���������'#0, '���������'#0);
var
  Buffer : PChar;
begin
  GetMem(Buffer, 100);
  StrCopy(Buffer, arpcSectHundred[wValue div 100]);
  StrCat(Buffer, ' '#0);
  if (wValue div 10) mod 10 = 1
  then
    StrCat(Buffer, arpcSectTen_[wValue mod 10])
  else begin
    StrCat(Buffer, arpcSectTen[(wValue div 10) mod 10]);
    StrCat(Buffer, ' '#0);
    case Gender of
      Male : StrCat(Buffer, arpcSectDigit_M[wValue mod 10]);
      Female : StrCat(Buffer, arpcSectDigit_F[wValue mod 10]);
    end;
  end;
  StrCat(Buffer, ' '#0);
  StrCopy(pcResult, Buffer);
  FreeMem(Buffer);
end;

function IntToCapitalsStr(lwValue : LongWord; Gender : TGender = male) : PChar;

  function w(Sect : TSection; Gender : TGender) : PChar;
  var
    wTmp : word;
    chU, chN : TPChar;
  begin
    chN[0] := #0;
    chU[0] := #0;
    wTmp := PickOutSect(lwValue, Sect);
    if wTmp <> 0 then
    begin
      SectToStr(wTmp, chN, Gender);
      case Sect of
        Milliard : GetMilliardByValue(wTmp, chU);
        Million : GetMillionByValue(wTmp, chU);
        Thousand : GetThousandByValue(wTmp, chU);
      end;
      StrCat(chN, chU);
    end;
    Result := chN;
  end;

var
  ch, chU : TPChar;
begin
  ch := #0;
  chU := #0;
  StrCat(ch, w(Milliard, male));
  StrCat(ch, w(Million, male));
  StrCat(ch, w(Thousand, female));
  StrCat(ch, w(Hundred, Gender));
  Result := ch;
end;

function FloatToCapitalsStr(dValue : double; Gender : TGender = male) : PChar;
var
  chR, ch, chU : TPChar;
begin
  chU := #0;
  ch := #0;
  chR := #0;
  StrCat(ch, IntToCapitalsStr(Trunc(dValue), Female));
  GetSafeByValue(Trunc(dValue), chU);
  StrCat(ch, chU);
  StrCat(chR, ch);
  if Round(Frac(dValue)*100) <> 0 then
  begin
    ch := #0;
    chU := #0;
    StrCat(ch, IntToCapitalsStr(Round(Frac(dValue)*100), female));
    GetCentesimalByValue(Round(Frac(dValue)*100), chU);
    StrCat(ch, chU);
    StrCat(chR, ch);
  end;
  Result := chR;
end;

function GetCapitalsMoney(dRouble : double) : string;
var
  ch : TPChar;
begin
  if dRouble = 0 then
  begin
    Result := '0 ���.';
    eXit;
  end;
  ch := #0;
  GetRoubleByValue(PickOutSect(Trunc(dRouble), Hundred), ch);
  Result := StrPas(IntToCapitalsStr(Trunc(dRouble))) +
            StrPas(ch) +
            IntToStr(Round(Frac(dRouble)*100))+' ���.';
end;

function GetCapitalsQuantity(lwQ : LongWord) : string;
var
  ch : TPChar;
begin
  if (lwQ = 0) then
  begin
    Result := '0 ����';
    eXit;
  end;
  ch := #0;
  GetNumByValue(PickOutSect(lwQ, Hundred), ch);
  Result := StrPas(IntToCapitalsStr(lwQ, Female)) + StrPas(ch);
end;

function GetCapitalsWeight(dW : double) : string;
var
  ch : TPChar;
begin
  if(dW=0)then
  begin
    Result := '0 �����';
    eXit; 
  end;
  ch := #0;
  if Frac(dw)=0 then

  else

  GetGrammeByValue(Round(Frac(dW)*100), ch);
  Result := FloatToCapitalsStr(dW) + StrPas(ch);
end;


function GetDateInGenetiveCase(Date : TDate): string;
const
  cmonth : array[1..12]of string = ('������', '�������', '�����', '������',
                '���', '����', '����', '�������', '��������',
                '�������', '������', '�������');
var
  year, month, day : word;
begin
  DecodeDate(Date, year, month, day);
  if year=1899 then Result := '" "'
  else Result := '"'+IntToStr(day)+'" '+cmonth[month]+' '+IntToStr(year)+'�.';
end;

procedure WinToDos(fn : string);
var
  f : text;
  ms : TMemoryStream;
  s : string;
  ch : array[0..255] of char;
begin
  if not FileExists(fn) then eXit;
  ms := TMemoryStream.Create;
  try
    AssignFile(f, fn);
    try
      ms.SetSize(FileSize(f));
      while not Eof(f) do
      begin
        ReadLn(f, s);
        OemToChar(PChar(s), ch);
        ms.Write(ch, SizeOf(ch));
      end;
    finally
      CloseFile(f);
    end;
    ms.SaveToFile(fn);
  finally
    ms.Free;
  end;
end;

function DosToWin(fn : string) : boolean;

 function StrDosToWin (DosStr : string) : string;
 begin
   SetLength(Result, Length(DosStr));
   if Length(Result) > 0 then
     OemToCharBuff(PChar(DosStr), PChar(Result), Length(Result));
 end;

var
  f, r : text;
  ch1, ch2 : array[0..254] of char;
begin
  Result := False;
  if not FileExists(fn) then eXit;
  AssignFile(f, fn);
  AssignFile(r, fn+'.tmp');
  try
    Reset(f);
    Rewrite(r);
    while not Eof(f) do
    begin
      ReadLn(f, ch1);
      OemToAnsi(ch1, ch2);
      WriteLn(r, ch2);
    end;
  finally
    CloseFile(f);
    CloseFile(r);
  end;
  DeleteFile(Pchar(fn));
  RenameFile(fn+'.tmp', fn);
  Result := True;
end;

function GetCommaWord(s : string; ind : integer) : string;
begin
  Result:=ExtractWord(ind, s, [';']);
end;

initialization
 // ShowmEssage(GetCommaWord('aaa;v;333333;55',3));



end.



