unit ImportData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleServer, Excel_TLB, pFIBQuery, Regexpr, Db,
  Excel97, Excel2000, Math, IdGlobal, FIBDataSet, pFIBDataSet, FIBDatabase,
  pFIBDatabase, FIBQuery, xmldom, XMLIntf, msxmldom, XMLDoc;

const
  xlLCID = LOCALE_USER_DEFAULT;

type
  TInvKind = (sInv);
  TxlFormat = (xlGeneralFormat, xlTextFormat, xlMDYFormat, xlDMYFormat, xlYMDFormat,
               xlMYDFormat, xlDYMFormat, xlYDMFormat, xlEMDFormat, xlSkipColumn);

  TdmImp = class(TDataModule)
    xl: TExcelApplication;
    xlsh: TExcelWorksheet;
    xlbk: TExcelWorkbook;
    quSInvItem: TpFIBDataSet;
    taSInvEl: TpFIBDataSet;
    dsrSInvEl: TDataSource;
    quIns: TpFIBDataSet;
    quSumEl: TpFIBDataSet;
    quSInvItemSZ: TFIBStringField;
    quSInvItemW: TFloatField;
    quInsQUANTITY: TSmallintField;
    quInsWEIGHT: TFloatField;
    quInsCOLOR: TFIBStringField;
    quInsCHROMATICITY: TFIBStringField;
    quInsCLEANNES: TFIBStringField;
    quInsGR: TFIBStringField;
    quInsSHAPE: TFIBStringField;
    quInsET: TFIBStringField;
    quInsESH: TFIBStringField;
    taSInvElGOOD: TFIBStringField;
    taSInvElMAT: TFIBStringField;
    taSInvElART: TFIBStringField;
    taSInvElART2: TFIBStringField;
    taSInvElUNITID: TIntegerField;
    taSInvElPNDS: TFloatField;
    taSInvElAKCIZ: TFloatField;
    taSInvElPRICE: TFloatField;
    taSInvElITEMLINK: TIntegerField;
    taSInvElINSLINK: TIntegerField;
    quSumElTW: TFloatField;
    quSumElTC: TIntegerField;
    quInsD_INSID: TFIBStringField;
    sqlImp: TpFIBQuery;
    quExportDepSell: TpFIBDataSet;
    quExportDepSellUID: TFIBIntegerField;
    quExportDepSellW: TFIBFloatField;
    quExportDepSellSZ: TFIBStringField;
    quExportDepSellART2: TFIBStringField;
    quExportDepSellART: TFIBStringField;
    quExportDepSellD_COMPID: TFIBIntegerField;
    quExportDepSellD_GOODID: TFIBStringField;
    quExportDepSellD_INSID: TFIBStringField;
    quExportDepSellD_MATID: TFIBStringField;
    quExportDepSellUNITID: TFIBIntegerField;
    quExportDepSellD_COUNTRYID: TFIBStringField;
    quExportDepSellATT1: TFIBIntegerField;
    quExportDepSellATT2: TFIBIntegerField;
    quExportDepSellSUPID: TFIBIntegerField;
    quExportDepSellNDSID: TFIBIntegerField;
    quExportDepSellPRICE: TFIBFloatField;
    quExportDepSellD_GOODS_SAM1: TFIBIntegerField;
    quExportDepSellD_GOODS_SAM2: TFIBIntegerField;
    quExportDepSellINSID: TFIBStringField;
    quExportDepSellQUANTITY: TFIBSmallIntField;
    quExportDepSellWEIGTH: TFIBFloatField;
    quExportDepSellCOLOR: TFIBStringField;
    quExportDepSellCHROMATICITY: TFIBStringField;
    quExportDepSellCLEANNES: TFIBStringField;
    quExportDepSellGR: TFIBStringField;
    quExportDepSellMAIN: TFIBSmallIntField;
    quExportDepSellEDGTYPEID: TFIBStringField;
    quExportDepSellEDGSHAPEID: TFIBStringField;
    quExportDepSellPRICE2: TFIBFloatField;
    quExportDepSellSSF: TFIBStringField;
    quExportDepSellNDATE: TFIBDateTimeField;
    quExportDepSellSELLPRICE: TFIBFloatField;
    quExportDepSellSELLPRICE0: TFIBFloatField;
    quExportDepSellSELLDISCOUNT: TFIBFloatField;
    quExportDepSellSELLCLIENTID: TFIBIntegerField;
    quExportDepSellSELLUNITID: TFIBSmallIntField;
    quExportDepSellSELLQ0: TFIBFloatField;
    quExportDepSellSELLW: TFIBFloatField;
    xmlDoc: TXMLDocument;
    quExportDep: TpFIBDataSet;
    quExportDepUID: TFIBIntegerField;
    quExportDepW: TFIBFloatField;
    quExportDepSZ: TFIBStringField;
    quExportDepART2: TFIBStringField;
    quExportDepART: TFIBStringField;
    quExportDepD_COMPID: TFIBIntegerField;
    quExportDepD_GOODID: TFIBStringField;
    quExportDepD_INSID: TFIBStringField;
    quExportDepD_MATID: TFIBStringField;
    quExportDepUNITID: TFIBIntegerField;
    quExportDepD_COUNTRYID: TFIBStringField;
    quExportDepATT1: TFIBIntegerField;
    quExportDepATT2: TFIBIntegerField;
    quExportDepSUPID: TFIBIntegerField;
    quExportDepNDSID: TFIBIntegerField;
    quExportDepPRICE: TFIBFloatField;
    quExportDepD_GOODS_SAM1: TFIBIntegerField;
    quExportDepD_GOODS_SAM2: TFIBIntegerField;
    quExportDepINSID: TFIBStringField;
    quExportDepQUANTITY: TFIBSmallIntField;
    quExportDepWEIGTH: TFIBFloatField;
    quExportDepCOLOR: TFIBStringField;
    quExportDepCHROMATICITY: TFIBStringField;
    quExportDepCLEANNES: TFIBStringField;
    quExportDepGR: TFIBStringField;
    quExportDepMAIN: TFIBSmallIntField;
    quExportDepEDGTYPEID: TFIBStringField;
    quExportDepEDGSHAPEID: TFIBStringField;
    quExportDepPRICE2: TFIBFloatField;
    quExportDepSSF: TFIBStringField;
    quExportDepNDATE: TFIBDateTimeField;
    quImportSell: TpFIBQuery;
    quImportArtDep: TpFIBDataSet;
    quImportArtDepARTID: TFIBIntegerField;
    quImportArtDepART2ID: TFIBIntegerField;
    quImportArtDepINSERTA2: TFIBSmallIntField;
    procedure DataModuleCreate(Sender: TObject);
  private
    FDefaultSeparator : char;
    FIXl : Excel_TLB.TExcelApplication;
    FIWorkbook : Excel_TLB._Workbook;
    FISheet, FISheetGusset, FISheetItem : Excel_TLB._Worksheet;
    procedure ReleaseExcel;
  public
    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
    property IXl : Excel_TLB.TExcelApplication read FIXl;
    property IWorkbook : Excel_TLB._Workbook read FIWorkbook;
    property ISheet : Excel_TLB._Worksheet read FISheet;
    property ISheetItem : Excel_TLB._Worksheet read FISheetItem;
    property ISheetGusset : Excel_TLB._Worksheet read FISheetGusset;

    procedure ExportGem(NoInv, Sn : integer; InvDate : TDateTime; InvKind : TInvKind = sInv);
    procedure ImportArtCDMXls (Path: string);
  end;



var   ImpError : TStringList;
      dmimp: TdmImp;

implementation

uses comdata, data, ComObj, bsUtils, ActiveX, Variants, bsStrUtils,
    rxStrUtils, M207Proc, Splash, DbUtil;

{$R *.DFM}

function OLERun(nm : string) : boolean; forward;


procedure TdmImp.ExportGem(NoInv, Sn : integer; InvDate : TDateTime; InvKind : TInvKind = sInv);
var
  Marker, MIt, MIns : integer;

  procedure DoEl(No : integer);
  begin
    with dmImp do
    try
      ISheet.Range['B'+IntToStr(Marker), 'B'+IntToStr(Marker)].Value := taSInvElGOOD.Value;
      ISheet.Range['C'+IntToStr(Marker), 'C'+IntToStr(Marker)].Value := taSInvElMAT.Value;
      ISheet.Range['D'+IntToStr(Marker), 'D'+IntToStr(Marker)].Value := taSInvElART.Value;
      ISheet.Range['E'+IntToStr(Marker), 'E'+IntToStr(Marker)].Value := taSInvElART2.Value;
      ISheet.Range['F'+IntToStr(Marker), 'F'+IntToStr(Marker)].Value := quSumElTW.Value;
      ISheet.Range['G'+IntToStr(Marker), 'G'+IntToStr(Marker)].Value := quSumElTC.Value;
      ISheet.Range['H'+IntToStr(Marker), 'H'+IntToStr(Marker)].Value := taSInvElUNITID.Value;
      ISheet.Range['I'+IntToStr(Marker), 'I'+IntToStr(Marker)].Value := taSInvElPRICE.Value;
      ISheet.Range['J'+IntToStr(Marker), 'J'+IntToStr(Marker)].Value := taSInvElPNDS.Value;
      ISheet.Range['K'+IntToStr(Marker), 'K'+IntToStr(Marker)].Value := taSInvElAKCIZ.Value;
      quIns.Last;
      if quIns.RecordCount = 0
      then ISheet.Range['L'+IntToStr(Marker), 'L'+IntToStr(Marker)].Value := 1
      else ISheet.Range['L'+IntToStr(Marker), 'L'+IntToStr(Marker)].Value := 0;
      quIns.First;
      while not quSInvItem.Eof do
      begin
        ISheetItem.Range['A'+IntToStr(MIt), 'A'+IntToStr(MIt)].Value := No;
        ISheetItem.Range['B'+IntToStr(MIt), 'B'+IntToStr(MIt)].Value := quSInvItemSZ.Value;
        ISheetItem.Range['C'+IntToStr(MIt), 'C'+IntToStr(MIt)].Value := quSInvItemW.Value;
        inc(MIt);
        quSInvItem.Next;
      end;
      while not quIns.Eof do
      begin
        ISheetGusset.Range['A'+IntToStr(MIns), 'A'+IntToStr(MIns)].Value := No;
        ISheetGusset.Range['B'+IntToStr(MIns), 'B'+IntToStr(MIns)].Value := quInsQUANTITY.Value;
        ISheetGusset.Range['C'+IntToStr(MIns), 'C'+IntToStr(MIns)].Value := quInsWEIGHT.Value;
        ISheetGusset.Range['D'+IntToStr(MIns), 'D'+IntToStr(MIns)].Value := quInsCOLOR.Value;
        ISheetGusset.Range['E'+IntToStr(MIns), 'E'+IntToStr(MIns)].Value := quInsCLEANNES.Value;
        ISheetGusset.Range['F'+IntToStr(MIns), 'F'+IntToStr(MIns)].Value := quInsCHROMATICITY.Value;
        ISheetGusset.Range['G'+IntToStr(MIns), 'G'+IntToStr(MIns)].Value := quInsGR.Value;
        ISheetGusset.Range['H'+IntToStr(MIns), 'H'+IntToStr(MIns)].Value := quInsSHAPE.Value;
        ISheetGusset.Range['I'+IntToStr(MIns), 'I'+IntToStr(MIns)].Value := quInsET.Value;
        ISheetGusset.Range['J'+IntToStr(MIns), 'J'+IntToStr(MIns)].Value := quInsESH.Value;
        ISheetGusset.Range['K'+IntToStr(MIns), 'K'+IntToStr(MIns)].Value := quInsD_INSID.Value;
        inc(MIns);
        quIns.Next;
      end;
    finally
    end;
  end;

var
  No : integer;
begin
  ImpError.Clear;
  try
    IXl.Connect;
    FIWorkbook := IXl.Workbooks.Add(EmptyParam, xlLCID);
    FISheet := IWorkBook.Worksheets.Item['����1'] as Excel_TLB._WorkSheet;
    FISheetItem := IWorkBook.Worksheets.Item['����2'] as Excel_TLB._WorkSheet;
    FISheetGusset := IWorkBook.Worksheets.Item['����3'] as Excel_TLB._WorkSheet;
//    if trImp.Active then trImp.CommitRetaining else trImp.StartTransaction;
    CloseDataSets([taSInvEl, quSInvItem, quSumEl, quIns]);
    taSInvEl.ParamByName('SInvId').AsInteger := NoInv;
    OpenDataSets([taSInvEl, quSInvItem, quSumEl, quIns]);
    // ������������ ���������
    Marker := 2;
    with ISheet do
    begin
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '<HEAD>';
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := IntToStr(Sn)+';'+
                DateToStr(InvDate)+';'+'20%;����������;';
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '</HEAD>';
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '<DATAHEADER>';
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '� �� �������';
      Range['B'+IntToStr(Marker), 'B'+IntToStr(Marker)].Value := '�����';
      Range['C'+IntToStr(Marker), 'C'+IntToStr(Marker)].Value := '��������';
      Range['D'+IntToStr(Marker), 'D'+IntToStr(Marker)].Value := '�������';
      Range['E'+IntToStr(Marker), 'E'+IntToStr(Marker)].Value := '���2';
      Range['F'+IntToStr(Marker), 'F'+IntToStr(Marker)].Value := '���';
      Range['G'+IntToStr(Marker), 'G'+IntToStr(Marker)].Value := '���-��';
      Range['H'+IntToStr(Marker), 'H'+IntToStr(Marker)].Value := '��.���.';
      Range['I'+IntToStr(Marker), 'I'+IntToStr(Marker)].Value := '����';
      Range['J'+IntToStr(Marker), 'J'+IntToStr(Marker)].Value := '���';
      Range['K'+IntToStr(Marker), 'K'+IntToStr(Marker)].Value := '�����';
      Range['L'+IntToStr(Marker), 'L'+IntToStr(Marker)].Value := '�������';
      Range['A'+IntToStr(Marker), 'L'+IntToStr(Marker)].Interior.Color := clAqua;
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '</DATAHEADER>';
      Inc(Marker);
      Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '<DATA>';
      inc(Marker);
    end;
    No := Marker - 1;
    MIt := 1; MIns := 1;
    while not taSInvEl.Eof do
    begin
      ISheet.Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := Marker - No;
      DoEl(Marker - No);
      taSInvEl.Next;
      inc(Marker);
    end;
    ISheet.Range['A'+IntToStr(Marker), 'A'+IntToStr(Marker)].Value := '</DATA>';
    if taSInvEl.RecordCount <> 0 then
      IWorkbook.SaveAs(ExtractFilePath(ParamStr(0)) + 'Export\'+'Invoice'+IntToStr(Sn),
             EmptyParam, EmptyParam, EmptyParam, EmptyParam, EmptyParam, xlNoChange,
             EmptyParam, EmptyParam, EmptyParam, EmptyParam,  xlLCID);
    CloseDataSets([taSInvEl, quSInvItem, quSumEl, quIns]);
    ReleaseExcel;
  finally
    dmImp.xl.Disconnect;
    dmImp.Free;
  end;
end;




 // ��������� ���� �� instance ��������� �������
function OLERun(nm : string) : boolean;
var
  ClassId : TCLSID;
  Unknown : IUnknown;
begin
  Result := False;
  try
    ClassId := ProgIdToClassId(nm);
    if GetActiveObject(ClassId, nil, Unknown) = S_Ok then Result := True
    else Result := False;
  except
  end;
end;

{a b c d e f g h i j  k  l  m  n  o  p  q  r  s  t
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20}

{ TdmImp }

constructor TdmImp.Create(AOwner: TComponent);
begin
  inherited;
  FIXl := Excel_TLB.TExcelApplication.Create(Self);
  FIXl.ConnectKind := ckRunningorNew;
  FDefaultSeparator := DecimalSeparator;
  DecimalSeparator := '.';
end;

destructor TdmImp.Destroy;
begin
  DecimalSeparator := FDefaultSeparator;
  inherited;
end;

procedure TdmImp.ReleaseExcel;
begin
  if Assigned(IXl) then
  begin
    if (IXl.Workbooks.Count > 0) and (not IXl.Visible[0]) then
    begin
      IXl.WindowState[0] := TOLEEnum(Excel_TLB.xlMinimized);
      IXl.Visible[0] := true;
      Application.BringToFront;
    end;
  end;
  FreeAndNil(FIXl);
end;


procedure TdmImp.ImportArtCDMXls (Path: string);
var i:integer;
    IsEnd:boolean;
    tmp, tmp1:string;
begin
  ImpError.Clear;

  if not FileExists(Path) then
  begin
    ImpError.Add('���� '+Path+' �� ����������!');
    eXit;
  end;
  try
   try
      xl.ConnectKind := ckRunningOrNew;
      xl.Connect;
      xl.Workbooks.Add(Path, xlLCID);
    except
      ImpError.Add('�� �������������� ���������� Excel');
      eXit;
    end;
    xlbk.ConnectTo(xl.ActiveWorkbook);
    with  (xlbk.WorkSheets[1] as _Worksheet) do
    begin
     i:=3;
     IsEnd:=false;
     while not IsEnd do
     begin
      Tmp := Range['B'+IntToStr(i), 'B'+IntToStr(i)].Value;
      Tmp1 := Range['C'+IntToStr(i), 'C'+IntToStr(i)].Value;

      if tmp='' then IsEnd:=true
      else begin
       if tmp1<>'' then
        ExecSQL('update d_art set art_cdm ='#39+trim(tmp1)+#39' where d_compid=311 and art='#39+trim(tmp)+#39' ', sqlImp);
      end;
      inc(i);
     end
    end;
  finally
    Screen.Cursor := crDefault;
    xl.Disconnect;
    dmImp.Free;

  end;
end;

procedure TdmImp.DataModuleCreate(Sender: TObject);
begin

end;

initialization

  ImpError := TStringList.Create;

finalization
  ImpError.Free;
  ImpError:=nil;
end.



