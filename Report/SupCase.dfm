object fmSupCase: TfmSupCase
  Left = 127
  Top = 165
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1087#1086#1089#1090#1072#1074#1097#1080#1082#1072
  ClientHeight = 192
  ClientWidth = 531
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object paHeader: TPanel
    Left = 0
    Top = 0
    Width = 531
    Height = 155
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 8
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object redImp: TRichEdit
      Left = 592
      Top = 8
      Width = 185
      Height = 89
      Lines.Strings = (
        'redImp')
      TabOrder = 0
      Visible = False
    end
    object lbSup: TListBox
      Left = 76
      Top = 4
      Width = 445
      Height = 145
      Color = clInfoBk
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object plBn: TPanel
    Left = 0
    Top = 155
    Width = 531
    Height = 37
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object btnClose: TBitBtn
      Left = 348
      Top = 8
      Width = 73
      Height = 25
      TabOrder = 0
      OnClick = btnCloseClick
      Kind = bkOK
    end
    object BitBtn1: TBitBtn
      Left = 432
      Top = 7
      Width = 73
      Height = 25
      Caption = #1054#1090#1084#1077#1085#1072
      TabOrder = 1
      OnClick = btnCloseClick
      Kind = bkCancel
    end
  end
end
