unit ReportData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, pFIBDataSet, FR_DSet, FR_DBSet, FR_Class, dbgrids,
  data, pFIBQuery, RxDbCtrl, FR_Desgn, FR_E_TXT,
  FR_E_RTF, Variants, FR_E_CSV,DBGridEh, FIBQuery, FIBDatabase,
  pFIBDatabase, FIBDataSet, FR_BarC, dbUtil, FR_Rich, frXMLExl, RxStrHlder;

const
  MSG_User = $6000;

type
  Tarr = array of Variant;

  (* ������ ����������
     1 - ��������� ��������� �� 3-�� ������
     2 - ����-�������,
     3 - ���������
     4 - ����-������� ����������
     5 - ��������� ����������
     6 - ���������� ���������
     7 - ���������� ����-�������
     8 - ����-������� ������� �� ������� �����������
     9 - ��������� ������� �� ������� �����������
     10 - ����-������� ��� ������� �����������
     11 - ��������� ��� ������� �����������
     12 - ���������� ���������(�����������)
     13 - ���������� ���������(�� ��������)
     14 - ������, ������ �� ���������
     15 - ��������� �� ���������
     16 - ������ ��������
     17 - ��� ���������� ��-��������
     18 - ��� ���������� ��-����������
     19 - ��������� �� ���������� � �����
     20 - ��������� �����
     21 - �������� ��� ��� ������
     22 - �������� ���
     23 - ���� �� ����������
     24 - �� ���������� � ��������� �� ����� ����� �� ���������� ��� ���� �������
     25 - �� ���������� � ��������� �� ����� ����� �� ���������� ��� ������ ������
     26 - ������ ��� �� ��������� ���������
     27 - ������ ��� �� ������������
     28 - ���������� �� ������������
     29 - ������ ��� ����������
     41 - ���������� �� ����� ����� �� ���������� ��� ���� �������
     43 - ���������� �� ����� ����� �� ���������� ��� ������ ������
     44 - ��������� ��� ����������� ����������
     45 - ����-������� ��� ����������� ���������� ???
     68 - ������������������ �����-���� ������� , ����������
     69 - ������ �� ������� �� ��������
     71-��������� ��������� �� ���������
     72- ����� �� ������ �� ������ ���������
     73- ��� �������� �������
     74-  �������� ��������� �� ������� �����������. ������������������� �����
     75- ������ ������� � �������
    112 - ��������� �� �� ���������� �� ������
    113 - ��������� �� �� �������� �� ������
    122 - ��� ������ �� �������
    168 - ������ ��������������
    169 - ������ �� ������� �� ����������
    173 - ��� �������� �� �������
    268 - ��� ��������������
    622 - ��� ������ �� �������
    768 - ��� �� �������� � ��������������
    1003 -
  *)

  TKindDoc = (facture, invoice,
              facture_wh, invoice_wh,
              {facture_ret,} facture_retInPr, {invoice_ret, } invoice_art_ret,
              invoice_art_ret_2, invoice_ret_01, invoice_12_sup,
              facture_opt, invoice_opt, repair_uid,
              facture_opt_u, invoice_opt_u, invoice_opt_pr,
              facture_r_opt, invoice_r_opt,
              facture_u, invoice_u,invoice_u_cdm,
              invoice_in, invoice_uin, invoice_in_sprice,invoice_in_spriceSav{, invoice_art_ret},
              order, f_order,
              disv, appl, applresp,
              o_reduce, f_reduce,
              s_list, //r_list,
              act_uid, act_art,
              ticket,act_ticket,
              actret_ticket, // ��� ������ �� �������
              sell_ticket,sell_ticketOutlay,sell_ticketart,sell_ticketsinv,
              bill,
              pricelist,
              fullpricelist,
              cpa, // ���������� ����������� ����������
              appl_sup, // ������ ��� ����������� � ������ ��� 2
              appl_sup1, // ������ ��� ����������� ��� ����� ��� 2
              maindoc3, // ��������� ��������� �� 3-�� ������
              //layout_whall_uid // ���������� �� ����� ����� �� ���������� �� ���� �������
              maindoc, // ������ �� ��������� � ��������
              f_reduce2, //������ 2 ��������� �� �������
              maindocuid, //��.��������� ��-��������
              f_reduce3, //��������� ������� ����������
              dep_alaiz,  // ������ �� �������������
              uidstore_t_p, // �������� ����� (�����) � ����� ��������� :)
              uidstore_t_p_c,  //������� ����� (����� �����) �������, ��, � ���������, ��������
              uidstoredep_t_p_c, //�������� ����� �� ��������
              inventory_b, inventory_e, subscription_b, subscription_e,
              inventory_order,
              inventory_act,
              inventory_act_null, appl_dep, appl_dep_art,
              letter,
              shop_report, // �������� ����� �� ���������
              center_report, //����� ��� ������
              center_report_com, //�������� ����� ��� ������ (��������)
              center_registry_com, //������ ��� ������ (��������)
              center_report_tol, //�������� ����� ��� ������ (�������)

              uid_store_doc, // ��������� ���������
              facture_opt_pril, // ���������� � ������������� ����
              shop_report_baduid, // ������������ �������
              uidWH_Report, // ����� �� ������
              ActAllowance_Doc, //��� �������� ������
              PresentActAllowance_Doc, //��� �������� ������ �� �������
              writeoff_inventory_act,
              NewNodCard, //��� �������� � ��������������
              UidWh_INV_Art,  //��������� �� �� ���������� �� ������
              UidWh_Prord, //������ �� ������� ��������� �� ������
              Balance, //������ � �������
              act_retsinv, // ��� �������� �����������
              TermCard_check, //������ ������� ����������� ����� ��������
              report_sel_emp, // ������ ������ �� �������� ������� �������� ��� ������� � ���������� ������
              comissionAct //��� ��������              
              );


  TLayOutKindDoc = (art_all, // ������������ � ��������� �� ����� ����� �� ����������
                    uid_all, // ���������� �� ����� ����� �� ����������
                    art_dep,
                    uid_dep,
                    art_all2, //������ �������� �� �4 ����������� � ��� �����
                    uid_all2, //
                    art_dep2,
                    uid_dep2,
                    art_dep3,
                    art_all3,
                    art_all4,
                    art_all5);

  TdmReport = class(TDataModule)
    ibdsOrder: TpFIBDataSet;
    ibdsOrderItem: TpFIBDataSet;
    trReport: TpFIBTransaction;
    frdsOrder: TfrDBDataSet;
    frdsOrderItem: TfrDBDataSet;
    frdsSupplier: TfrDBDataSet;
    ibdsSupplier: TpFIBDataSet;
    ibdsSupplierNAME: TFIBStringField;
    ibdsSupplierADDRESS: TFIBStringField;
    ibdsSupplierPHONE: TFIBStringField;
    ibdsSupplierBILL: TFIBStringField;
    ibdsSupplierBIK: TFIBStringField;
    ibdsSupplierKBILL: TFIBStringField;
    ibdsSupplierBANK: TFIBStringField;
    ibdsSupplierINN: TFIBStringField;
    ibdsSupplierOKONH: TFIBStringField;
    ibdsSupplierOKPO: TFIBStringField;
    ibdsSupplierNDATE: TDateTimeField;
    ibdsCustomer: TpFIBDataSet;
    frdsCustomer: TfrDBDataSet;
    dsrBillItem: TDataSource;
    ibdsBillItem: TpFIBDataSet;
    frdsBillItem: TfrDBDataSet;
    ibdsSupplierV_SN: TIntegerField;
    ibdsTagWI: TpFIBDataSet;
    frdsTagWI: TfrDBDataSet;
    ibdsIns: TpFIBDataSet;
    frdsIns: TfrDBDataSet;
    ibdsOrderR_DEPNAME: TFIBStringField;
    ibdsOrderR_DEPCODE: TFIBStringField;
    ibdsOrderR_SNDATE: TDateTimeField;
    ibdsOrderR_SN: TIntegerField;
    ibdsOrderR_NOORDER: TIntegerField;
    ibdsOrderR_LINK: TIntegerField;
    ibdsOrderR_SETDATE: TDateTimeField;
    ibdsReduce: TpFIBDataSet;
    frdsReduce: TfrDBDataSet;
    ibdsReduceItem: TpFIBDataSet;
    frdsReduceItem: TfrDBDataSet;
    ibdsOrderItemR_GOOD: TFIBStringField;
    ibdsOrderItemR_ART: TFIBStringField;
    ibdsOrderItemR_ART2: TFIBStringField;
    ibdsOrderItemR_MAT: TFIBStringField;
    ibdsOrderItemR_UNITID: TIntegerField;
    ibdsOrderItemR_PRICE: TFloatField;
    ibdsInsR_UID: TIntegerField;
    ibdsInsR_INSCODE: TFIBStringField;
    ibdsInsR_ETYPE: TFIBStringField;
    ibdsInsR_ESHAPE: TFIBStringField;
    ibdsInsR_Q: TIntegerField;
    ibdsInsR_W: TFloatField;
    ibdsInsR_COLOR: TFIBStringField;
    ibdsInsR_CHROMATICITY: TFIBStringField;
    ibdsInsR_CLEANNES: TFIBStringField;
    ibdsInsR_GROUP: TFIBStringField;
    ibdsTagWOI: TpFIBDataSet;
    frdsTagWOI: TfrDBDataSet;
    frReport: TfrReport;
    ibdsGrIns: TpFIBDataSet;
    frdsGrIns: TfrDBDataSet;
    ibdsFrom: TpFIBDataSet;
    ibdsTo: TpFIBDataSet;
    ibdsBillUItem: TpFIBDataSet;
    frdsBillUItem: TfrDBDataSet;
    frdsUIDWH: TfrDBDataSet;
    ibdsUIDWH: TpFIBDataSet;
    frdsDep: TfrDBDataSet;
    ibdsUIDWHSITEMID: TIntegerField;
    ibdsUIDWHUID: TIntegerField;
    ibdsUIDWHPRODCODE: TFIBStringField;
    ibdsUIDWHMATID: TFIBStringField;
    ibdsUIDWHGOODID: TFIBStringField;
    ibdsUIDWHINSID: TFIBStringField;
    ibdsUIDWHART: TFIBStringField;
    ibdsUIDWHART2: TFIBStringField;
    ibdsUIDWHUNITID: TIntegerField;
    ibdsUIDWHNDSNAME: TFIBStringField;
    ibdsUIDWHNDSVALUE: TFloatField;
    ibdsUIDWHNDSID: TIntegerField;
    ibdsUIDWHPRICE: TFloatField;
    ibdsUIDWHW: TFloatField;
    ibdsUIDWHSZ: TFIBStringField;
    ibdsUIDWHDEP: TFIBStringField;
    ibdsUIDWHSUPCODE: TFIBStringField;
    ibdsUIDWHCOST: TFloatField;
    ibdsUIDWHART2ID: TIntegerField;
    ibdsUIDWHDEPID: TIntegerField;
    ibdsUIDWHCOLOR: TIntegerField;
    ibdsUIDWHSN0: TIntegerField;
    ibdsUIDWHSSF0: TFIBStringField;
    ibdsUIDWHSPRICE0: TFloatField;
    ibdsUIDWHSUPID0: TIntegerField;
    ibdsUIDWHSDATE0: TDateTimeField;
    ibdsUIDWHSN: TIntegerField;
    ibdsUIDWHSDATE: TDateTimeField;
    ibdsUIDWHSELLDATE: TDateTimeField;
    ibdsUIDWHT: TSmallintField;
    ibdsBillItemR_SELID: TIntegerField;
    ibdsBillItemR_GOODID: TFIBStringField;
    ibdsBillItemR_GOOD: TFIBStringField;
    ibdsBillItemR_ART: TFIBStringField;
    ibdsBillItemR_ART2ID: TIntegerField;
    ibdsBillItemR_ART2: TFIBStringField;
    ibdsBillItemR_COUNTRY: TFIBStringField;
    ibdsBillItemR_CUSTDECL: TFIBStringField;
    ibdsBillItemR_MATID: TFIBStringField;
    ibdsBillItemR_MAT: TFIBStringField;
    ibdsBillItemR_UNITID: TIntegerField;
    ibdsBillItemR_Q: TIntegerField;
    ibdsBillItemR_W: TFloatField;
    ibdsBillItemR_PRICEWONDS: TFloatField;
    ibdsBillItemR_COSTWONDS: TFloatField;
    ibdsBillItemR_EXCISE: TFIBStringField;
    ibdsBillItemR_PNDS: TFloatField;
    ibdsBillItemR_TOTALNDS: TFloatField;
    ibdsBillItemR_PRICE: TFloatField;
    ibdsBillItemR_COST: TFloatField;
    ibdsBillItemR_TR: TFloatField;
    ibdsBillItemR_PRICE2: TFloatField;
    ibdsBillItemR_COST2: TFloatField;
    frDesigner: TfrDesigner;
    ibdsReduceItemR_GOODID: TFIBStringField;
    ibdsReduceItemR_GOOD: TFIBStringField;
    ibdsReduceItemR_ART: TFIBStringField;
    ibdsReduceItemR_ART2: TFIBStringField;
    ibdsReduceItemR_MATID: TFIBStringField;
    ibdsReduceItemR_MAT: TFIBStringField;
    ibdsReduceItemR_UNITID: TIntegerField;
    ibdsReduceItemR_PRICE: TFloatField;
    ibdsReduceItemR_SIZE: TFIBStringField;
    ibdsReduceItemR_ASSAY: TFIBStringField;
    ibdsReduceItemR_W: TFloatField;
    ibdsReduceItemR_Q: TIntegerField;
    ibdsReduceItemR_COMPCODE: TFIBStringField;
    ibdsReduceItemR_COMPSNAME: TFIBStringField;
    ibdsReduceR_NO: TIntegerField;
    ibdsReduceR_DATE: TDateTimeField;
    ibdsBillUItemR_SELID: TIntegerField;
    ibdsBillUItemR_GOODID: TFIBStringField;
    ibdsBillUItemR_GOOD: TFIBStringField;
    ibdsBillUItemR_ART: TFIBStringField;
    ibdsBillUItemR_ART2ID: TIntegerField;
    ibdsBillUItemR_ART2: TFIBStringField;
    ibdsBillUItemR_COUNTRY: TFIBStringField;
    ibdsBillUItemR_CUSTDECL: TFIBStringField;
    ibdsBillUItemR_MATID: TFIBStringField;
    ibdsBillUItemR_MAT: TFIBStringField;
    ibdsBillUItemR_UNITID: TIntegerField;
    ibdsBillUItemR_Q: TIntegerField;
    ibdsBillUItemR_W: TFloatField;
    ibdsBillUItemR_PRICEWONDS: TFloatField;
    ibdsBillUItemR_COSTWONDS: TFloatField;
    ibdsBillUItemR_EXCISE: TFIBStringField;
    ibdsBillUItemR_PNDS: TFloatField;
    ibdsBillUItemR_TOTALNDS: TFloatField;
    ibdsBillUItemR_PRICE: TFloatField;
    ibdsBillUItemR_COST: TFloatField;
    ibdsBillUItemR_TR: TFloatField;
    ibdsBillUItemR_PRICE2: TFloatField;
    ibdsBillUItemR_COST2: TFloatField;
    ibdsBillUItemR_UID: TIntegerField;
    ibdsBillUItemR_SZ: TFIBStringField;
    ibdsBillUItemR_ITCOST: TFloatField;
    ibdsUIDWHMAT: TFIBStringField;
    dsrActOV: TDataSource;
    ibdsActOV: TpFIBDataSet;
    frdsActOV: TfrDBDataSet;
    ibdsActOVItem: TpFIBDataSet;
    frdsActOVitem: TfrDBDataSet;
    ibdsActOVR_DEPNAME: TFIBStringField;
    ibdsActOVR_DEPCODE: TFIBStringField;
    ibdsActOVR_NOORDER: TIntegerField;
    ibdsActOVR_LINK: TIntegerField;
    ibdsActOVR_SETDATE: TDateTimeField;
    ibdsBillItemR_UID: TIntegerField;
    ibdsBillItemR_SZ: TFIBStringField;
    ibdsBillItemR_ITCOST: TFloatField;
    ibdsBillItemR_ITW: TFloatField;
    ibdsBillItemR_NDATE: TDateTimeField;
    ibdsBillItemR_SSF: TFIBStringField;
    ibdsSupplierSDATE: TDateTimeField;
    ibdsBillItemR_PRICE2WONDS: TFloatField;
    ibdsBillItemR_COST2WONDS: TFloatField;
    ibdsBillItemR_TOTAL2NDS: TFloatField;
    dsrAppl: TDataSource;
    frdsAppl: TfrDBDataSet;
    ibdsDisv: TpFIBDataSet;
    dsrDisv: TDataSource;
    frdsDisv: TfrDBDataSet;
    ibdsAppl: TpFIBDataSet;
    ibdsApplUID: TIntegerField;
    ibdsApplSZ: TFIBStringField;
    ibdsApplW: TFloatField;
    ibdsApplGOOD: TFIBStringField;
    ibdsApplPRICE: TFloatField;
    ibdsApplART: TFIBStringField;
    ibdsApplART2: TFIBStringField;
    ibdsApplMAT: TFIBStringField;
    ibdsApplUNITID: TIntegerField;
    ibdsApplCOST: TFloatField;
    ibdsApplSELLDATE: TDateTimeField;
    ibdsApplCRDATE: TDateTimeField;
    ibdsApplSN: TIntegerField;
    ibdsApplWASRET: TFIBStringField;
    ibdsApplDESCR: TMemoField;
    ibdsDisvCOST: TFloatField;
    ibdsDisvCLIENTNAME: TFIBStringField;
    ibdsApplCLIENTNAME: TFIBStringField;
    ibdsApplCHECKNO: TIntegerField;
    dsrApplcause: TDataSource;
    ibdsApplCause: TpFIBDataSet;
    frdsApplcause: TfrDBDataSet;
    ibdsApplCauseCLIENTNAME: TFIBStringField;
    ibdsApplCauseUID: TIntegerField;
    ibdsApplCauseW: TFloatField;
    ibdsApplCauseSZ: TFIBStringField;
    ibdsApplCauseGOOD: TFIBStringField;
    ibdsApplCausePRICE: TFloatField;
    ibdsApplCauseART: TFIBStringField;
    ibdsApplCauseART2: TFIBStringField;
    ibdsApplCauseMAT: TFIBStringField;
    ibdsApplCauseUNITID: TIntegerField;
    ibdsApplCauseSELLDATE: TDateTimeField;
    ibdsApplCauseCOST: TFloatField;
    ibdsApplCauseSN: TIntegerField;
    ibdsApplCauseCRDATE: TDateTimeField;
    ibdsApplCauseWASRET: TFIBStringField;
    ibdsApplCauseDESCR: TBlobField;
    ibdsApplCauseCHECKNO: TIntegerField;
    ibdsTicketIt: TpFIBDataSet;
    frdsTicketIt: TfrDBDataSet;
    ibdsTicketItR_FULLART: TFIBStringField;
    ibdsTicketItR_ART2: TFIBStringField;
    ibdsTicketItR_SIZE: TFIBStringField;
    ibdsTicketItR_W: TFloatField;
    ibdsTicketItR_PRICE: TFloatField;
    ibdsTicketItR_UID: TIntegerField;
    ibdsTicketItR_DEPNAME: TFIBStringField;
    ibdsTicketItR_SELLDATE: TDateTimeField;
    ibdsTicketItR_CITY: TFIBStringField;
    ibdsTicketItR_ADDRESS: TFIBStringField;
    ibdsTicketItR_COST: TFloatField;
    ibdsTicketItR_PHONE: TFIBStringField;
    ibdsTicketItR_UNITID: TIntegerField;
    ibdsSBillItem: TpFIBDataSet;
    taSelf: TpFIBDataSet;
    ibdsSBillItemR_UID: TIntegerField;
    ibdsSBillItemR_SZ: TFIBStringField;
    ibdsSBillItemR_GOODID: TFIBStringField;
    ibdsSBillItemR_GOOD: TFIBStringField;
    ibdsSBillItemR_ART: TFIBStringField;
    ibdsSBillItemR_ART2: TFIBStringField;
    ibdsSBillItemR_COUNTRY: TFIBStringField;
    ibdsSBillItemR_CUSTDECL: TFIBStringField;
    ibdsSBillItemR_MATID: TFIBStringField;
    ibdsSBillItemR_MAT: TFIBStringField;
    ibdsSBillItemR_UNITID: TIntegerField;
    ibdsSBillItemR_Q: TIntegerField;
    ibdsSBillItemR_W: TFloatField;
    ibdsSBillItemR_PRICEWONDS: TFloatField;
    ibdsSBillItemR_COSTWONDS: TFloatField;
    ibdsSBillItemR_EXCISE: TFIBStringField;
    ibdsSBillItemR_PNDS: TFloatField;
    ibdsSBillItemR_TOTALNDS: TFloatField;
    ibdsSBillItemR_PRICE: TFloatField;
    ibdsSBillItemR_COST: TFloatField;
    ibdsSBillItemR_TR: TFloatField;
    ibdsSBillItemR_PRICE2: TFloatField;
    ibdsSBillItemR_COST2: TFloatField;
    ibdsSBillItemR_PRICE2WONDS: TFloatField;
    ibdsSBillItemR_COST2WONDS: TFloatField;
    ibdsSBillItemR_TOTAL2NDS: TFloatField;
    ibdsSBillItemR_SSF: TFIBStringField;
    ibdsSBillItemR_NDATE: TDateTimeField;
    ibdsSBillItemR_ART2ID: TIntegerField;
    frdsSBillItem: TfrDBDataSet;
    dsrDep: TDataSource;
    ibdsDep: TpFIBDataSet;
    ibdsDepR_SNAME: TFIBStringField;
    ibdsDepR_NAME: TFIBStringField;
    ibdsDepL_DEPID: TIntegerField;
    ibdsSupplierSSF: TFIBStringField;
    ibdsWhArt: TpFIBDataSet;
    dsrWhArt: TDataSource;
    ibdsWhArtART: TFIBStringField;
    ibdsWhArtLINK: TIntegerField;
    ibdsWhArtMAT: TFIBStringField;
    ibdsWhArtGOOD: TFIBStringField;
    ibdsWhArtUNITID: TSmallintField;
    ibdsWhArtCOMPCODE: TFIBStringField;
    frdsWhArt: TfrDBDataSet;
    ibdsWhArtQUANTITY: TFloatField;
    ibdsWhArtWEIGTH: TFloatField;
    ibdsWhArtTW: TFIBStringField;
    ibdsWhArtTQ: TFIBStringField;
    ibdsWhArtCOMP: TFIBStringField;
    ibdsWhSZ: TpFIBDataSet;
    frdsWhSZ: TfrDBDataSet;
    ibdsWhSZQ: TIntegerField;
    ibdsWhSZW: TFloatField;
    ibdsTicketItR_MAT: TFIBStringField;
    ibdsTicketItR_GOOD: TFIBStringField;
    ibdsTicketItR_ART: TFIBStringField;
    ibdsTicketItR_ID: TIntegerField;
    ibdsTicketItR_DISCPRICE: TFloatField;
    ibdsTicketItR_DISCOUNT: TFloatField;
    ibdsTicketItR_DISCCOST: TFloatField;
    dsrPrList: TDataSource;
    ibdsPrList: TpFIBDataSet;
    frdsPrList: TfrDBDataSet;
    ibdsPrListItem: TpFIBDataSet;
    frdsPrListItem: TfrDBDataSet;
    ibdsPrListR_SN: TIntegerField;
    ibdsPrListR_SDATE: TDateTimeField;
    ibdsPrListR_LINK: TIntegerField;
    ibdsPrListItemR_GOOD: TFIBStringField;
    ibdsPrListItemR_MAT: TFIBStringField;
    ibdsPrListItemR_ART: TFIBStringField;
    ibdsPrListItemR_ART2: TFIBStringField;
    ibdsPrListItemR_UNITID: TIntegerField;
    ibdsPrListItemR_PRICE: TFloatField;
    ibdsFullPrList: TpFIBDataSet;
    frdsFullPrList: TfrDBDataSet;
    taComp: TpFIBDataSet;
    taCompNAME: TFIBStringField;
    taCompOFIO: TFIBStringField;
    taCompPHONE: TFIBStringField;
    taCompFAX: TFIBStringField;
    taCompCODE: TFIBStringField;
    frdsComp: TfrDBDataSet;
    taApplSup: TpFIBDataSet;
    taApplSupART: TFIBStringField;
    taApplSupUNITID: TIntegerField;
    taApplSupD_INSID: TFIBStringField;
    taApplSupD_MATID: TFIBStringField;
    taApplSupD_GOODID: TFIBStringField;
    frdsApplSup: TfrDBDataSet;
    taJ3: TpFIBDataSet;
    frdsrJ3: TfrDBDataSet;
    taJ3ID: TIntegerField;
    taJ3ARTID: TIntegerField;
    taJ3ART: TFIBStringField;
    taJ3FULLART: TFIBStringField;
    taJ3D_COMPID: TIntegerField;
    taJ3D_MATID: TFIBStringField;
    taJ3D_GOODID: TFIBStringField;
    taJ3UNITID: TIntegerField;
    taJ3D_INSID: TFIBStringField;
    taJ3INS: TFloatField;
    taJ3INW: TFloatField;
    taJ3INQ: TIntegerField;
    taJ3OUTS: TFloatField;
    taJ3OUTW: TFloatField;
    taJ3OUTQ: TIntegerField;
    taJ3DEBTORS: TFloatField;
    taJ3DEBTORW: TFloatField;
    taJ3DEBTORQ: TIntegerField;
    taJ3SALES: TFloatField;
    taJ3SALEW: TFloatField;
    taJ3SALEQ: TIntegerField;
    taJ3RETS: TFloatField;
    taJ3RETW: TFloatField;
    taJ3RETQ: TIntegerField;
    taJ3RETOPTS: TFloatField;
    taJ3RETOPTW: TFloatField;
    taJ3RETOPTQ: TIntegerField;
    taJ3RETVENDORS: TFloatField;
    taJ3RETVENDORW: TFloatField;
    taJ3RETVENDORQ: TIntegerField;
    taJ3OPTS: TFloatField;
    taJ3OPTW: TFloatField;
    taJ3OPTQ: TIntegerField;
    taDoc: TpFIBDataSet;
    taDocID: TIntegerField;
    taDocDOC: TMemoField;
    dsrOrder: TDataSource;
    dsrReduce: TDataSource;
    dsrTagWI: TDataSource;
    frdsWHUID: TfrDBDataSet;
    ibdsWhSZSZ: TFIBStringField;
    ibdsWhArtART2: TFIBStringField;
    ibdsActOVR_NACT: TIntegerField;
    taProvider: TpFIBDataSet;
    taDocNAME: TFIBStringField;
    taSelfCOMPID: TIntegerField;
    taSelfNAME: TFIBStringField;
    taSelfBOSS: TFIBStringField;
    taSelfOFIO: TFIBStringField;
    taSelfADDRESS: TFIBStringField;
    taSelfPHONE: TFIBStringField;
    taSelfFAX: TFIBStringField;
    taSelfEMAIL: TFIBStringField;
    taSelfINN: TFIBStringField;
    taSelfBIK: TFIBStringField;
    taSelfOKPO: TFIBStringField;
    taSelfOKONH: TFIBStringField;
    taSelfBANK: TFIBStringField;
    taSelfBILL: TFIBStringField;
    taSelfKBILL: TFIBStringField;
    taSelfOFIOPHONE: TFIBStringField;
    ibdsBillUItemR_ITW: TFloatField;
    ibdsBillUItemR_PRICE2WONDS: TFloatField;
    ibdsBillUItemR_COST2WONDS: TFloatField;
    ibdsBillUItemR_TOTAL2NDS: TFloatField;
    ibdsBillUItemR_NDATE: TDateTimeField;
    ibdsBillUItemR_SSF: TFIBStringField;
    ibdsBillUItemR_ITCOST2: TFloatField;
    taProviderNAME: TFIBStringField;
    taProviderADDRESS: TFIBStringField;
    taProviderPHONE: TFIBStringField;
    taProviderBILL: TFIBStringField;
    taProviderKBILL: TFIBStringField;
    taProviderBIK: TFIBStringField;
    taProviderBANK: TFIBStringField;
    taProviderINN: TFIBStringField;
    taProviderOKONH: TFIBStringField;
    taProviderOKPO: TFIBStringField;
    taProviderSN: TIntegerField;
    taProviderNDATE: TDateTimeField;
    taProviderSDATE: TDateTimeField;
    taProviderSSF: TFIBStringField;
    ibdsToNAME: TFIBStringField;
    ibdsToADDRESS: TFIBStringField;
    ibdsToPHONE: TFIBStringField;
    ibdsToCITY: TFIBStringField;
    ibdsFromSN: TIntegerField;
    ibdsFromNDATE: TDateTimeField;
    ibdsFromNAME: TFIBStringField;
    ibdsFromCITY: TFIBStringField;
    ibdsFromADDRESS: TFIBStringField;
    ibdsFromPHONE: TFIBStringField;
    frSumReport: TfrReport;
    taUIDWH: TpFIBDataSet;
    taUIDWHA: TpFIBDataSet;
    frdsUIDWHA: TfrDBDataSet;
    dsrUIDWHA: TDataSource;
    taUIDWHAu: TpFIBDataSet;
    frdsUIDWHASz: TfrDBDataSet;
    ibdsBillItemR_ITCOST2: TFloatField;
    ibdsBillItemR_RATEURF: TFloatField;
    ibdsBillItemR_AKCIZ: TFloatField;
    ibdsBillUItemR_RATEURF: TFloatField;
    ibdsBillUItemR_AKCIZ: TFloatField;
    ibdsFullPrListR_GOOD: TFIBStringField;
    ibdsFullPrListR_MAT: TFIBStringField;
    ibdsFullPrListR_ART: TFIBStringField;
    ibdsFullPrListR_ART2: TFIBStringField;
    ibdsFullPrListR_UNITID: TIntegerField;
    ibdsFullPrListR_PRICE2: TFloatField;
    dsrSBillItem: TDataSource;
    ibdsBillItemR_UID_SUP: TIntegerField;
    taTSumDep: TpFIBDataSet;
    taTSumDepIDATE: TDateTimeField;
    taTSumDepIDATE2: TDateTimeField;
    taTSumDepDEP1: TIntegerField;
    taTSumDepDEP2: TIntegerField;
    taTSumDepINC: TFloatField;
    taTSumDepINW: TFloatField;
    taTSumDepINQ: TIntegerField;
    taTSumDepINVINC: TFloatField;
    taTSumDepINVW: TFloatField;
    taTSumDepINVQ: TIntegerField;
    taTSumDepINVOSTC: TFloatField;
    taTSumDepINVOSTW: TFloatField;
    taTSumDepINVOSTQ: TIntegerField;
    taTSumDepSLINC: TFloatField;
    taTSumDepSLOUTC: TFloatField;
    taTSumDepSLW: TFloatField;
    taTSumDepSLQ: TIntegerField;
    taTSumDepOPTINC: TFloatField;
    taTSumDepOPTOUTC: TFloatField;
    taTSumDepOPTW: TFloatField;
    taTSumDepOPTQ: TIntegerField;
    taTSumDepRETOPTINC: TFloatField;
    taTSumDepRETOPTRW: TFloatField;
    taTSumDepRETOPTRQ: TIntegerField;
    taTSumDepRETINC: TFloatField;
    taTSumDepRETW: TFloatField;
    taTSumDepRETQ: TIntegerField;
    taTSumDepRETSINC: TFloatField;
    taTSumDepRETSW: TFloatField;
    taTSumDepRETSQ: TIntegerField;
    taTSumDepOUTC: TFloatField;
    taTSumDepOUTW: TFloatField;
    taTSumDepOUTQ: TIntegerField;
    taTSumDepISTMP: TSmallintField;
    taTSumDepINGR: TFIBStringField;
    taTSumDepSINVGR: TFIBStringField;
    taTSumDepSELLIN: TFIBStringField;
    taTSumDepSELLOUT: TFIBStringField;
    taTSumDepOUTGR: TFIBStringField;
    taTSumDepSINVGR2: TFIBStringField;
    taTSumDepRETGR: TFIBStringField;
    taTSumDepDEPNAME: TFIBStringField;
    ibdsTicketItR_RET: TSmallintField;
    ibdsTagWOIR_COMP: TFIBStringField;
    ibdsTagWOIR_MAT: TFIBStringField;
    ibdsTagWOIR_GOOD: TFIBStringField;
    ibdsTagWOIR_ART: TFIBStringField;
    ibdsTagWOIR_ART2: TFIBStringField;
    ibdsTagWOIR_COUNTRY: TFIBStringField;
    ibdsTagWOIR_SIZE: TFIBStringField;
    ibdsTagWOIR_W: TFloatField;
    ibdsTagWOIR_PRICE: TFloatField;
    ibdsTagWOIR_UID: TIntegerField;
    ibdsTagWOIR_UNITID: TIntegerField;
    ibdsTagWOIR_LINK: TIntegerField;
    ibdsTagWOIR_INS: TFIBStringField;
    ibdsTagWOIR_DEPID: TIntegerField;
    ibdsTagWOIR_DEP: TFIBStringField;
    ibdsTagWOIR_DEPNAME: TFIBStringField;
    ibdsTagWOIR_CITY: TFIBStringField;
    ibdsTagWOIR_ADDRESS: TFIBStringField;
    ibdsTagWOIR_ISINS: TIntegerField;
    ibdsTagWOIR_SITEMID: TIntegerField;
    ibdsTagWOIR_ISNEWPRICE: TIntegerField;
    ibdsTagWOIR_DATE: TDateTimeField;
    ibdsTagWIR_COMP: TFIBStringField;
    ibdsTagWIR_MAT: TFIBStringField;
    ibdsTagWIR_GOOD: TFIBStringField;
    ibdsTagWIR_ART: TFIBStringField;
    ibdsTagWIR_ART2: TFIBStringField;
    ibdsTagWIR_COUNTRY: TFIBStringField;
    ibdsTagWIR_SIZE: TFIBStringField;
    ibdsTagWIR_W: TFloatField;
    ibdsTagWIR_PRICE: TFloatField;
    ibdsTagWIR_UID: TIntegerField;
    ibdsTagWIR_UNITID: TIntegerField;
    ibdsTagWIR_LINK: TIntegerField;
    ibdsTagWIR_INS: TFIBStringField;
    ibdsTagWIR_DEPID: TIntegerField;
    ibdsTagWIR_DEP: TFIBStringField;
    ibdsTagWIR_DEPNAME: TFIBStringField;
    ibdsTagWIR_CITY: TFIBStringField;
    ibdsTagWIR_ADDRESS: TFIBStringField;
    ibdsTagWIR_ISINS: TIntegerField;
    ibdsTagWIR_SITEMID: TIntegerField;
    ibdsTagWIR_ISNEWPRICE: TIntegerField;
    ibdsTagWIR_DATE: TDateTimeField;
    frTSumDep: TfrDBDataSet;
    quTmp: TpFIBQuery;
    taTSumDepOUTURFC: TFloatField;
    taTSumDepISINPRICE: TSmallintField;
    taApplSupSZ: TFIBStringField;
    taApplZ: TpFIBDataSet;
    frdstaApplZ: TfrDBDataSet;
    taApplSupQ: TIntegerField;
    dsrApplSup: TDataSource;
    taApplSupGOOD: TFIBStringField;
    taApplSupD_COMPID: TIntegerField;
    svdFile: TOpenDialog;
    frTextExp: TfrTextExport;
    frCSVExp: TfrCSVExport;
    frRTFExp: TfrRTFExport;
    taApplZNAME: TFIBStringField;
    taApplZADDRESS: TFIBStringField;
    taApplZPHONE: TFIBStringField;
    taApplZNOAPPL: TIntegerField;
    taApplZZDATE: TDateTimeField;
    taApplZSUPNAME: TFIBStringField;
    taUIDWHAuD_ARTID: TIntegerField;
    taUIDWHAuUNITID: TIntegerField;
    taUIDWHAuART: TFIBStringField;
    taUIDWHAuMATID: TFIBStringField;
    taUIDWHAuGOODID: TFIBStringField;
    taUIDWHAuINSID: TFIBStringField;
    taUIDWHAuCOMPCODE: TFIBStringField;
    taUIDWHAuSUPCODE: TFIBStringField;
    taUIDWHAuW: TFloatField;
    taUIDWHAuART2: TFIBStringField;
    taUIDWHAuPRICE: TFloatField;
    taUIDWHAuQ: TIntegerField;
    ibdsBillUItemR_UID_SUP: TIntegerField;
    taTSumDepID: TIntegerField;
    taTSumDepRETURFC: TFloatField;
    taTSumDepRETSELLERURFC: TFloatField;
    taTSumDepURFINC: TFloatField;
    taTSumDepURFOUTC: TFloatField;
    ibdsitemSlist: TpFIBDataSet;
    ibdsitemSlistR_UID: TIntegerField;
    ibdsitemSlistR_ART1: TFIBStringField;
    ibdsitemSlistR_COMP: TIntegerField;
    ibdsitemSlistR_MAT: TFIBStringField;
    ibdsitemSlistR_GOOD: TFIBStringField;
    frdsSitemSlist: TfrDBDataSet;
    ibdsitemSlistR_A2: TFIBStringField;
    ibdsitemSlistR_SZ: TFIBStringField;
    ibdsitemSlistR_W: TFloatField;
    ibdsitemSlistD_UNITID: TIntegerField;
    ibdsitemSlistR_PRISE: TFloatField;
    ibdsTicketItR_EMPNAME: TFIBStringField;
    taTicket: TpFIBDataSet;
    frTicket: TfrDBDataSet;
    ibdsTicketItR_CHECKNO: TIntegerField;
    taTicketCHECKNO: TIntegerField;
    dsrTicket: TDataSource;
    ibdsTicketItIN_PRICE: TFloatField;
    ibdsApplADDRESS: TFIBStringField;
    ibdsApplCauseADDRESS: TFIBStringField;
    ibdsTicketIt1: TpFIBDataSet;
    ibdsTicketIt1R_FULLART: TFIBStringField;
    ibdsTicketIt1R_ART2: TFIBStringField;
    ibdsTicketIt1R_SIZE: TFIBStringField;
    ibdsTicketIt1R_W: TFloatField;
    ibdsTicketIt1R_PRICE: TFloatField;
    ibdsTicketIt1R_UID: TIntegerField;
    ibdsTicketIt1R_DEPNAME: TFIBStringField;
    ibdsTicketIt1R_SELLDATE: TDateTimeField;
    ibdsTicketIt1R_CITY: TFIBStringField;
    ibdsTicketIt1R_ADDRESS: TFIBStringField;
    ibdsTicketIt1R_COST: TFloatField;
    ibdsTicketIt1R_PHONE: TFIBStringField;
    ibdsTicketIt1R_UNITID: TIntegerField;
    ibdsTicketIt1R_MAT: TFIBStringField;
    ibdsTicketIt1R_GOOD: TFIBStringField;
    ibdsTicketIt1R_ART: TFIBStringField;
    ibdsTicketIt1R_ID: TIntegerField;
    ibdsTicketIt1R_DISCPRICE: TFloatField;
    ibdsTicketIt1R_DISCOUNT: TFloatField;
    ibdsTicketIt1R_DISCCOST: TFloatField;
    ibdsTicketIt1R_RET: TSmallintField;
    ibdsTicketIt1R_EMPNAME: TFIBStringField;
    ibdsTicketIt1R_CHECKNO: TIntegerField;
    ibdsTicketIt1IN_PRICE: TFloatField;
    ibdsTicketIt1R_UNIT: TFIBStringField;
    ibdsDisvBOSS: TFIBStringField;
    ibdsDisvBUH: TFIBStringField;
    ibdsTicketIt1NODCARD: TFIBStringField;
    ibdsTicketItR_SSF: TFIBStringField;
    ibdsApplEMPNAME: TFIBStringField;
    ibdsApplCauseEMPNAME: TFIBStringField;
    ibdsTicketItR_UNIT: TFIBStringField;
    frBarCodeObject1: TfrBarCodeObject;
    frdsInventory: TfrDBDataSet;
    taInventoryBefore: TpFIBDataSet;
    taInventoryBeforecost: TFloatField;
    taInventoryBeforen: TIntegerField;
    taRec: TpFIBDataSet;
    taRecD_COMPID: TFIBIntegerField;
    taRecINVERDATE: TFIBDateField;
    taRecINVPRORDN: TFIBIntegerField;
    taRecINVPRORDDATE: TFIBDateField;
    taRecNAMECOMP: TFIBStringField;
    taRecBUH: TFIBStringField;
    taRecMOL: TFIBStringField;
    frdsmol: TfrDBDataSet;
    taMol: TpFIBDataSet;
    taMolD_COMPID: TFIBIntegerField;
    taMolST: TFIBStringField;
    taInventoryEnd: TpFIBDataSet;
    taInventoryEndRECN: TIntegerField;
    frdsInventoryE: TfrDBDataSet;
    taRecPRESEDENTFIO: TFIBStringField;
    taRecRESPONSIBLE_PERSON: TFIBStringField;
    taD_Emp: TpFIBDataSet;
    taMolFIO: TFIBStringField;
    taRecLastSentens: TStringField;
    taRecINVDATE: TFIBDateField;
    taD_EmpD_MOLID: TFIBIntegerField;
    taD_EmpFIO: TFIBStringField;
    taD_EmpPOST: TFIBStringField;
    taD_EmpPostFIO: TStringField;
    taInventoryAct_N: TpFIBDataSet;
    frD_Emp: TfrDBDataSet;
    taRecDEPNAME: TFIBStringField;
    taRecBOSS: TFIBStringField;
    taRecPRESEDENTPOST: TFIBStringField;
    taInventoryAct_E: TpFIBDataSet;
    frInventoryAct_N: TfrDBDataSet;
    frInventoryAct_E: TfrDBDataSet;
    taMol2: TpFIBDataSet;
    frD_Mol2: TfrDBDataSet;
    taMol2FIO: TFIBStringField;
    taInventoryBeforeINVENTORYID: TFIBIntegerField;
    taInventoryBeforeUID: TFIBIntegerField;
    taInventoryBeforeSZ: TFIBStringField;
    taInventoryBeforeW: TFIBFloatField;
    taInventoryBeforeISIN: TFIBSmallIntField;
    taInventoryBeforeUNITID: TFIBIntegerField;
    taInventoryBeforePRICE0: TFIBFloatField;
    taInventoryBeforeFULLART: TFIBStringField;
    taInventoryEndINVENTORYID: TFIBIntegerField;
    taInventoryEndUID: TFIBIntegerField;
    taInventoryEndSZ: TFIBStringField;
    taInventoryEndW: TFIBFloatField;
    taInventoryEndISIN: TFIBSmallIntField;
    taInventoryEndUNITID: TFIBIntegerField;
    taInventoryEndPRICE0: TFIBFloatField;
    taInventoryEndFULLART: TFIBStringField;
    ibdsBillItemR_TRNDS: TFIBFloatField;
    ibdsApplDep: TpFIBDataSet;
    dsApplDep: TDataSource;
    frdsApplDep: TfrDBDataSet;
    ibdsTicketIt1INSVAL: TFIBStringField;
    frdsClientAddress: TfrDBDataSet;
    quClientAddress: TpFIBDataSet;
    quClientAddressNAME: TFIBStringField;
    quClientAddressADDRESS: TFIBStringField;
    quClientAddressHOME_FLAT: TFIBStringField;
    quClientAddressCITY: TFIBStringField;
    quClientAddressCOMPANYNAME: TFIBStringField;
    quClientAddressCOMPANYADDRESS: TFIBStringField;
    taShopReport: TpFIBDataSet;
    frShopReport: TfrDBDataSet;
    taShopReportID: TFIBIntegerField;
    taShopReportSINVID: TFIBIntegerField;
    taShopReportT: TFIBSmallIntField;
    taShopReportTNAME: TFIBStringField;
    taShopReportDOCNO: TFIBStringField;
    taShopReportDOCDATE: TFIBDateTimeField;
    taShopReportQ: TFIBFloatField;
    taShopReportW: TFIBFloatField;
    taShopReportCOST: TFIBFloatField;
    taShopReportK: TFIBSmallIntField;
    taShopReportREPORTBD: TFIBDateTimeField;
    taShopReportREPORTED: TFIBDateTimeField;
    taShopReportREPORTNO: TFIBIntegerField;
    taShopReportSHOPNAME: TFIBStringField;
    taShopReportFNAME: TFIBStringField;
    taShopReportLNAME: TFIBStringField;
    taShopReportSNAME: TFIBStringField;
    frdsUid_Store: TfrDBDataSet;
    ibdsUID_Store: TpFIBDataSet;
    ibdsUID_StoreBD: TFIBDateTimeField;
    ibdsUID_StoreED: TFIBDateTimeField;
    ibdsUID_StoreGOODSNAME: TFIBStringField;
    ibdsUID_StoreART: TFIBStringField;
    ibdsUID_StoreUNIT: TFIBStringField;
    ibdsUID_StoreBDQ: TFIBIntegerField;
    ibdsUID_StoreBDS: TFIBFloatField;
    ibdsUID_StoreINQ: TFIBIntegerField;
    ibdsUID_StoreINS: TFIBFloatField;
    ibdsUID_StoreOUTQ: TFIBIntegerField;
    ibdsUID_StoreOUTS: TFIBFloatField;
    ibdsUID_StoreEDQ: TFIBIntegerField;
    ibdsUID_StoreEDS: TFIBFloatField;
    ibdsUID_StoreBUH: TFIBStringField;
    taInventoryAct_NUID: TFIBIntegerField;
    taInventoryAct_NSZ: TFIBStringField;
    taInventoryAct_NW: TFIBFloatField;
    taInventoryAct_NUNITID: TFIBSmallIntField;
    taInventoryAct_NPRICE0: TFIBFloatField;
    taInventoryAct_NCOST: TFIBFloatField;
    taInventoryAct_NFULLART: TFIBStringField;
    taInventoryAct_ND_MATID: TFIBStringField;
    taInventoryAct_ND_GOODID: TFIBStringField;
    taInventoryAct_NCOMPID: TFIBIntegerField;
    taInventoryAct_NSUPID: TFIBIntegerField;
    taInventoryAct_ND_GOODS_SAM1: TFIBIntegerField;
    taInventoryAct_ND_GOODS_SAM2: TFIBIntegerField;
    taInventoryAct_NATT1: TFIBIntegerField;
    taInventoryAct_NATT2: TFIBIntegerField;
    taInventoryAct_NART: TFIBStringField;
    taInventoryAct_EUID: TFIBIntegerField;
    taInventoryAct_ESZ: TFIBStringField;
    taInventoryAct_EW: TFIBFloatField;
    taInventoryAct_EUNITID: TFIBSmallIntField;
    taInventoryAct_EPRICE0: TFIBFloatField;
    taInventoryAct_ECOST: TFIBFloatField;
    taInventoryAct_EFULLART: TFIBStringField;
    taInventoryAct_ED_MATID: TFIBStringField;
    taInventoryAct_ED_GOODID: TFIBStringField;
    taInventoryAct_ECOMPID: TFIBIntegerField;
    taInventoryAct_ESUPID: TFIBIntegerField;
    taInventoryAct_ED_GOODS_SAM1: TFIBIntegerField;
    taInventoryAct_ED_GOODS_SAM2: TFIBIntegerField;
    taInventoryAct_EATT1: TFIBIntegerField;
    taInventoryAct_EATT2: TFIBIntegerField;
    taInventoryAct_EART: TFIBStringField;
    ibdsUIDWH_Report: TpFIBDataSet;
    frUIDWH_Report: TfrDBDataSet;
    ibdsUIDWH_ReportBD: TFIBDateTimeField;
    ibdsUIDWH_ReportCOMP: TFIBStringField;
    ibdsUIDWH_ReportSUP: TFIBStringField;
    ibdsUIDWH_ReportART: TFIBStringField;
    ibdsUIDWH_ReportUNIT: TFIBStringField;
    ibdsUIDWH_ReportPRICE: TFIBFloatField;
    ibdsUIDWH_ReportQ: TFIBIntegerField;
    ibdsUIDWH_ReportW: TFIBFloatField;
    ibdsUIDWH_ReportSUMM: TFIBFloatField;
    ibdsHatWrite_off: TpFIBDataSet;
    frHatwrite_off: TfrDBDataSet;
    ibdsWrite_off: TpFIBDataSet;
    frWrite_off: TfrDBDataSet;
    ibdsWrite_offD_MATID: TFIBStringField;
    ibdsWrite_offD_GOODID: TFIBStringField;
    ibdsWrite_offD_INSID: TFIBStringField;
    ibdsWrite_offART: TFIBStringField;
    ibdsWrite_offART2: TFIBStringField;
    ibdsWrite_offSPRICE: TFIBFloatField;
    ibdsWrite_offCOST: TFIBFloatField;
    ibdsWrite_offQ: TFIBIntegerField;
    ibdsWrite_offPRODCODE: TFIBStringField;
    ibdsApplDepAPPLDEPID: TFIBIntegerField;
    ibdsApplDepUID: TFIBIntegerField;
    ibdsApplDepW: TFIBFloatField;
    ibdsApplDepSZ: TFIBStringField;
    ibdsApplDepART2ID: TFIBIntegerField;
    ibdsApplDepART: TFIBStringField;
    ibdsApplDepPRODCODE: TFIBStringField;
    ibdsApplDepD_GOODID: TFIBStringField;
    ibdsApplDepD_INSID: TFIBStringField;
    ibdsApplDepD_MATID: TFIBStringField;
    ibdsApplDepART2: TFIBStringField;
    ibdsApplDepNAMEMAT: TFIBStringField;
    ibdsApplDepNAMECOMP: TFIBStringField;
    ibdsApplDepNAMEINS: TFIBStringField;
    ibdsApplDepNAMEGOOD: TFIBStringField;
    ibdsApplDepNAMEDEP: TFIBStringField;
    ibdsApplDepCOST: TFIBFloatField;
    ibdsApplDepCOSTP: TFIBFloatField;
    ibdsApplDepSDATE: TFIBDateTimeField;
    ibdsApplDepNAMEDEPFROM: TFIBStringField;
    ibdsDisvSELFNAME: TFIBStringField;
    ibdsDisvBOSSPOST: TFIBStringField;
    ibdsHatWrite_offSN: TFIBIntegerField;
    ibdsHatWrite_offSDATE: TFIBDateField;
    ibdsHatWrite_offCOMPRESNT: TFIBStringField;
    ibdsHatWrite_offCOMMEMBERS: TFIBStringField;
    ibdsHatWrite_offSPOILINGREASON: TFIBStringField;
    ibdsHatWrite_offDEFECTDISCRIPT: TFIBStringField;
    ibdsHatWrite_offWRITEOFFREASON: TFIBStringField;
    ibdsHatWrite_offSOLUTION: TFIBStringField;
    ibdsWrite_offW: TFIBFloatField;
    ibdsApplDepSN: TFIBIntegerField;
    ibdsApplDepCOMMENT: TFIBStringField;
    ibdsHatWrite_offACTNAME: TFIBStringField;
    ibdsHatWrite_offCOMPANY: TFIBStringField;
    ibdsHatWrite_offADDRESS: TFIBStringField;
    ibdsHatWrite_offRECIPIENTCOMPANY: TFIBStringField;
    ibdsHatWrite_offRECIPIENTADDRESS: TFIBStringField;
    frNodCard: TfrDBDataSet;
    quSelectNodCard: TpFIBDataSet;
    taInventoryAct_NISOUTPRICEPRN: TFIBIntegerField;
    taInventoryAct_EISOUTPRICEPRN: TFIBIntegerField;
    frCenterReport: TfrDBDataSet;
    taCenterReport: TpFIBDataSet;
    taCenterReportID: TFIBIntegerField;
    taCenterReportSINVID: TFIBIntegerField;
    taCenterReportT: TFIBSmallIntField;
    taCenterReportTNAME: TFIBStringField;
    taCenterReportDOCNO: TFIBStringField;
    taCenterReportDOCDATE: TFIBDateTimeField;
    taCenterReportQ: TFIBFloatField;
    taCenterReportW: TFIBFloatField;
    taCenterReportCOST: TFIBFloatField;
    taCenterReportK: TFIBSmallIntField;
    taCenterReportREPORTBD: TFIBDateTimeField;
    taCenterReportREPORTED: TFIBDateTimeField;
    taCenterReportREPORTNO: TFIBIntegerField;
    taCenterReportSHOPNAME: TFIBStringField;
    taCenterReportFNAME: TFIBStringField;
    taCenterReportLNAME: TFIBStringField;
    taCenterReportSNAME: TFIBStringField;
    taCenterReportCOST2: TFIBFloatField;
    taCenterReportCOST3: TFIBFloatField;
    taCenterReportBUHREP: TFIBStringField;
    quCenterIncoming: TpFIBDataSet;
    frCenterIncoming: TfrDBDataSet;
    quCenterIncomingID: TFIBIntegerField;
    quCenterIncomingSINVID: TFIBIntegerField;
    quCenterIncomingT: TFIBSmallIntField;
    quCenterIncomingTNAME: TFIBStringField;
    quCenterIncomingDOCNO: TFIBStringField;
    quCenterIncomingDOCDATE: TFIBDateTimeField;
    quCenterIncomingQ: TFIBFloatField;
    quCenterIncomingW: TFIBFloatField;
    quCenterIncomingCOST: TFIBFloatField;
    quCenterIncomingK: TFIBSmallIntField;
    quCenterIncomingCOST2: TFIBFloatField;
    quCenterIncomingCOST3: TFIBFloatField;
    quCenterIncomingBUHREP: TFIBStringField;
    frCenterItogIn: TfrDBDataSet;
    quCenterItogIn: TpFIBDataSet;
    quCenterItogInID: TFIBIntegerField;
    quCenterItogInSINVID: TFIBIntegerField;
    quCenterItogInT: TFIBSmallIntField;
    quCenterItogInTNAME: TFIBStringField;
    quCenterItogInDOCNO: TFIBStringField;
    quCenterItogInDOCDATE: TFIBDateTimeField;
    quCenterItogInQ: TFIBFloatField;
    quCenterItogInW: TFIBFloatField;
    quCenterItogInCOST: TFIBFloatField;
    quCenterItogInK: TFIBSmallIntField;
    quCenterItogInCOST2: TFIBFloatField;
    quCenterItogInCOST3: TFIBFloatField;
    quCenterItogInBUHREP: TFIBStringField;
    quCenterOut: TpFIBDataSet;
    frCenterOut: TfrDBDataSet;
    quCenterOutID: TFIBIntegerField;
    quCenterOutSINVID: TFIBIntegerField;
    quCenterOutT: TFIBSmallIntField;
    quCenterOutTNAME: TFIBStringField;
    quCenterOutDOCNO: TFIBStringField;
    quCenterOutDOCDATE: TFIBDateTimeField;
    quCenterOutQ: TFIBFloatField;
    quCenterOutW: TFIBFloatField;
    quCenterOutCOST: TFIBFloatField;
    quCenterOutK: TFIBSmallIntField;
    quCenterOutCOST2: TFIBFloatField;
    quCenterOutCOST3: TFIBFloatField;
    quCenterOutBUHREP: TFIBStringField;
    frCenterItogOut: TfrDBDataSet;
    quCenterItogOut: TpFIBDataSet;
    quCenterItog: TpFIBDataSet;
    frCenterItog: TfrDBDataSet;
    quCenterDefferent: TpFIBDataSet;
    frCenterDefferent: TfrDBDataSet;
    quCenterItogOutID: TFIBIntegerField;
    quCenterItogOutSINVID: TFIBIntegerField;
    quCenterItogOutT: TFIBSmallIntField;
    quCenterItogOutTNAME: TFIBStringField;
    quCenterItogOutDOCNO: TFIBStringField;
    quCenterItogOutDOCDATE: TFIBDateTimeField;
    quCenterItogOutQ: TFIBFloatField;
    quCenterItogOutW: TFIBFloatField;
    quCenterItogOutCOST: TFIBFloatField;
    quCenterItogOutK: TFIBSmallIntField;
    quCenterItogOutCOST2: TFIBFloatField;
    quCenterItogOutCOST3: TFIBFloatField;
    quCenterItogOutBUHREP: TFIBStringField;
    quCenterItogID: TFIBIntegerField;
    quCenterItogSINVID: TFIBIntegerField;
    quCenterItogT: TFIBSmallIntField;
    quCenterItogTNAME: TFIBStringField;
    quCenterItogDOCNO: TFIBStringField;
    quCenterItogDOCDATE: TFIBDateTimeField;
    quCenterItogQ: TFIBFloatField;
    quCenterItogW: TFIBFloatField;
    quCenterItogCOST: TFIBFloatField;
    quCenterItogK: TFIBSmallIntField;
    quCenterItogCOST2: TFIBFloatField;
    quCenterItogCOST3: TFIBFloatField;
    quCenterItogBUHREP: TFIBStringField;
    quCenterDefferentID: TFIBIntegerField;
    quCenterDefferentSINVID: TFIBIntegerField;
    quCenterDefferentT: TFIBSmallIntField;
    quCenterDefferentTNAME: TFIBStringField;
    quCenterDefferentDOCNO: TFIBStringField;
    quCenterDefferentDOCDATE: TFIBDateTimeField;
    quCenterDefferentQ: TFIBFloatField;
    quCenterDefferentW: TFIBFloatField;
    quCenterDefferentCOST: TFIBFloatField;
    quCenterDefferentK: TFIBSmallIntField;
    quCenterDefferentCOST2: TFIBFloatField;
    quCenterDefferentCOST3: TFIBFloatField;
    quCenterDefferentBUHREP: TFIBStringField;
    quUidWhArt: TpFIBDataSet;
    frUidWhArt: TfrDBDataSet;
    quUidWhArtART2: TFIBStringField;
    quUidWhArtART: TFIBStringField;
    quUidWhArtQ: TFIBIntegerField;
    quUidWhArtPRICE2: TFIBFloatField;
    quUidWhArtCOST2: TFIBFloatField;
    quUidWhArtW: TFIBFloatField;
    quUidWhArtUNITID: TFIBIntegerField;
    quUidWhArtMAT: TFIBStringField;
    quUidWhArtGOOD: TFIBStringField;
    quGrIns: TpFIBDataSet;
    quUidWhArtART2ID: TFIBIntegerField;
    dsUidWhArt: TDataSource;
    quGrInsR_UID: TFIBIntegerField;
    quGrInsR_INSCODE: TFIBStringField;
    quGrInsR_INSNAME: TFIBStringField;
    quGrInsR_ETYPE: TFIBStringField;
    quGrInsR_ESHAPE: TFIBStringField;
    quGrInsR_Q: TFIBIntegerField;
    quGrInsR_W: TFIBFloatField;
    quGrInsR_COLOR: TFIBStringField;
    quGrInsR_CHROMATICITY: TFIBStringField;
    quGrInsR_CLEANNES: TFIBStringField;
    quGrInsR_GROUP: TFIBStringField;
    frGrIns: TfrDBDataSet;
    quUidWhArtDEPNAME: TFIBStringField;
    quUidWhArtCENNAME: TFIBStringField;
    quUidWhArtADDRESSD: TFIBStringField;
    quUidWhArtADDRESSC: TFIBStringField;
    quUidWhArtPHONED: TFIBStringField;
    quUidWhArtPHONEC: TFIBStringField;
    ibdsTicketItR_ACTDISCOUNT: TFIBFloatField;
    quUidWhArtSETDATE: TFIBDateTimeField;
    quUidWhArtNPRORD: TFIBSmallIntField;
    ibdsOrderR_COMP: TFIBStringField;
    ibdsApplResp: TpFIBDataSet;
    frdsApplResp: TfrDBDataSet;
    ibdsApplRespCause: TpFIBDataSet;
    ibdsApplRespCauseCLIENTNAME: TFIBStringField;
    ibdsApplRespCauseUID: TFIBIntegerField;
    ibdsApplRespCauseW: TFIBFloatField;
    ibdsApplRespCauseSZ: TFIBStringField;
    ibdsApplRespCauseGOOD: TFIBStringField;
    ibdsApplRespCausePRICE: TFIBFloatField;
    ibdsApplRespCauseART: TFIBStringField;
    ibdsApplRespCauseART2: TFIBStringField;
    ibdsApplRespCauseMAT: TFIBStringField;
    ibdsApplRespCauseUNITID: TFIBIntegerField;
    ibdsApplRespCauseSELLDATE: TFIBDateTimeField;
    ibdsApplRespCauseCOST: TFIBFloatField;
    ibdsApplRespCauseCRDATE: TFIBDateTimeField;
    ibdsApplRespCauseWASRET: TFIBStringField;
    ibdsApplRespCauseADDRESS: TFIBStringField;
    ibdsApplRespCauseEMPNAME: TFIBStringField;
    ibdsApplRespCauseCHEKNO: TFIBIntegerField;
    frdsApplRespCause: TfrDBDataSet;
    ibdsApplRespCLIENTNAME: TFIBStringField;
    ibdsApplRespUID: TFIBIntegerField;
    ibdsApplRespW: TFIBFloatField;
    ibdsApplRespSZ: TFIBStringField;
    ibdsApplRespGOOD: TFIBStringField;
    ibdsApplRespPRICE: TFIBFloatField;
    ibdsApplRespART: TFIBStringField;
    ibdsApplRespART2: TFIBStringField;
    ibdsApplRespMAT: TFIBStringField;
    ibdsApplRespUNITID: TFIBIntegerField;
    ibdsApplRespSELLDATE: TFIBDateTimeField;
    ibdsApplRespCOST: TFIBFloatField;
    ibdsApplRespCRDATE: TFIBDateTimeField;
    ibdsApplRespWASRET: TFIBStringField;
    ibdsApplRespADDRESS: TFIBStringField;
    ibdsApplRespEMPNAME: TFIBStringField;
    ibdsApplRespCHEKNO: TFIBIntegerField;
    taInventoryEndCOST: TFloatField;
    frRichObject: TfrRichObject;
    ibdsCardCheck: TpFIBDataSet;
    frdsCardCheck: TfrDBDataSet;
    ibdsCardCheckCHECKNO: TFIBIntegerField;
    ibdsCardCheckRN: TFIBIntegerField;
    ibdsCardCheckSNAME: TFIBStringField;
    ibdsCardCheckCOSTCARD: TFIBFloatField;
    ibdsCardCheckCOST: TFIBFloatField;
    ibdsCardCheckCOST0: TFIBFloatField;
    ibdsCardCheckUID: TFIBIntegerField;
    ibdsCardCheckFULLART: TFIBStringField;
    ibdsCardCheckART2: TFIBStringField;
    ibdsCardCheckSZ: TFIBStringField;
    ibdsCardCheckW: TFIBFloatField;
    taCenterReport1: TpFIBDataSet;
    frCenterReport1: TfrDBDataSet;
    taCenterReport1ID: TFIBIntegerField;
    taCenterReport1SINVID: TFIBIntegerField;
    taCenterReport1T: TFIBSmallIntField;
    taCenterReport1TNAME: TFIBStringField;
    taCenterReport1DOCNO: TFIBStringField;
    taCenterReport1DOCDATE: TFIBDateTimeField;
    taCenterReport1Q: TFIBFloatField;
    taCenterReport1W: TFIBFloatField;
    taCenterReport1COST: TFIBFloatField;
    taCenterReport1K: TFIBSmallIntField;
    taCenterReport1ERRDOC: TFIBSmallIntField;
    taCenterReport1COST2: TFIBFloatField;
    taCenterReport1COST3: TFIBFloatField;
    taCenterReport1BUHREP: TFIBStringField;
    taCenterReport1DEPSEL: TFIBIntegerField;
    taCenterReport1COLOR: TFIBIntegerField;
    taCenterReport1SN: TFIBIntegerField;
    frXMLExcelExport1: TfrXMLExcelExport;
    frXMLExcelExport2: TfrXMLExcelExport;
    taUIDWHAD_ARTID: TFIBIntegerField;
    taUIDWHAUNITID: TFIBIntegerField;
    taUIDWHAART: TFIBStringField;
    taUIDWHAMATID: TFIBStringField;
    taUIDWHAGOODID: TFIBStringField;
    taUIDWHAINSID: TFIBStringField;
    taUIDWHACOMPCODE: TFIBStringField;
    taUIDWHAW: TFIBFloatField;
    taUIDWHAART2: TFIBStringField;
    taUIDWHAPRICE: TFIBFloatField;
    taUIDWHAQ: TFIBIntegerField;
    taUIDWHASPRICE: TFIBFloatField;
    taUIDWHB: TpFIBDataSet;
    taUIDWHBD_ARTID: TFIBIntegerField;
    taUIDWHBUNITID: TFIBIntegerField;
    taUIDWHBART: TFIBStringField;
    taUIDWHBMATID: TFIBStringField;
    taUIDWHBGOODID: TFIBStringField;
    taUIDWHBINSID: TFIBStringField;
    taUIDWHBCOMPCODE: TFIBStringField;
    taUIDWHBW: TFIBFloatField;
    taUIDWHBART2: TFIBStringField;
    taUIDWHBPRICE: TFIBFloatField;
    taUIDWHBQ: TFIBIntegerField;
    taUIDWHBSPRICE: TFIBFloatField;
    taUIDWHBHEADERCOMPANY: TFIBStringField;
    taUIDWHBBODYDEPARTMENT: TFIBStringField;
    taUIDWHBBODYSIZE: TFIBStringField;
    taUIDWHBBODYGOOD: TFIBStringField;
    frdsUIDWHB: TfrDBDataSet;
    taApplSupART2: TFIBStringField;
    frAnlzSelEmp: TpFIBDataSet;
    frAnlzSelEmpID: TFIBIntegerField;
    frAnlzSelEmpUSERID: TFIBIntegerField;
    frAnlzSelEmpSELLERNAME: TFIBStringField;
    frAnlzSelEmpCOUNTSELL: TFIBIntegerField;
    frAnlzSelEmpCOSTSELL: TFIBFloatField;
    dsAnlzSelEmp: TDataSource;
    frdsAnlzSelEmp: TfrDBDataSet;
    frdsFrom: TfrDBDataSet;
    dsrFrom: TDataSource;
    ibdsFromSINVID: TFIBIntegerField;
    ibdsBillItemVP: TpFIBDataSet;
    dsrBillItemVP: TDataSource;
    frdsBillItemVP: TfrDBDataSet;
    ibdsBillItemVPR_UID: TFIBIntegerField;
    ibdsBillItemVPR_SZ: TFIBStringField;
    ibdsBillItemVPR_ITCOST: TFIBFloatField;
    ibdsBillItemVPR_ITCOST2: TFIBFloatField;
    ibdsBillItemVPR_ITW: TFIBFloatField;
    ibdsBillItemVPR_SELID: TFIBIntegerField;
    ibdsBillItemVPR_GOODID: TFIBStringField;
    ibdsBillItemVPR_GOOD: TFIBStringField;
    ibdsBillItemVPR_ART: TFIBStringField;
    ibdsBillItemVPR_ART2ID: TFIBIntegerField;
    ibdsBillItemVPR_ART2: TFIBStringField;
    ibdsBillItemVPR_COUNTRY: TFIBStringField;
    ibdsBillItemVPR_CUSTDECL: TFIBStringField;
    ibdsBillItemVPR_MATID: TFIBStringField;
    ibdsBillItemVPR_MAT: TFIBStringField;
    ibdsBillItemVPR_UNITID: TFIBIntegerField;
    ibdsBillItemVPR_Q: TFIBIntegerField;
    ibdsBillItemVPR_W: TFIBFloatField;
    ibdsBillItemVPR_UID_SUP: TFIBIntegerField;
    ibdsBillItemVPR_PRICEWONDS: TFIBFloatField;
    ibdsBillItemVPR_COSTWONDS: TFIBFloatField;
    ibdsBillItemVPR_EXCISE: TFIBStringField;
    ibdsBillItemVPR_PNDS: TFIBFloatField;
    ibdsBillItemVPR_TOTALNDS: TFIBFloatField;
    ibdsBillItemVPR_PRICE: TFIBFloatField;
    ibdsBillItemVPR_COST: TFIBFloatField;
    ibdsBillItemVPR_TR: TFIBFloatField;
    ibdsBillItemVPR_PRICE2: TFIBFloatField;
    ibdsBillItemVPR_COST2: TFIBFloatField;
    ibdsBillItemVPR_PRICE2WONDS: TFIBFloatField;
    ibdsBillItemVPR_COST2WONDS: TFIBFloatField;
    ibdsBillItemVPR_TOTAL2NDS: TFIBFloatField;
    ibdsBillItemVPR_NDATE: TFIBDateTimeField;
    ibdsBillItemVPR_SSF: TFIBStringField;
    ibdsBillItemVPR_RATEURF: TFIBFloatField;
    ibdsBillItemVPR_AKCIZ: TFIBFloatField;
    ibdsBillItemVPR_TRNDS: TFIBFloatField;
    ibdsBillItemVPR_RETNAME: TFIBStringField;
    ibdsBillItemR_RETNAME: TFIBStringField;
    ibdsBillUItemVP: TpFIBDataSet;
    dsrBillUItemVP: TDataSource;
    frdsBillUItemVP: TfrDBDataSet;
    ibdsBillUItemVPR_UID: TFIBIntegerField;
    ibdsBillUItemVPR_SZ: TFIBStringField;
    ibdsBillUItemVPR_ITCOST: TFIBFloatField;
    ibdsBillUItemVPR_ITCOST2: TFIBFloatField;
    ibdsBillUItemVPR_ITW: TFIBFloatField;
    ibdsBillUItemVPR_SELID: TFIBIntegerField;
    ibdsBillUItemVPR_GOODID: TFIBStringField;
    ibdsBillUItemVPR_GOOD: TFIBStringField;
    ibdsBillUItemVPR_ART: TFIBStringField;
    ibdsBillUItemVPR_ART2ID: TFIBIntegerField;
    ibdsBillUItemVPR_ART2: TFIBStringField;
    ibdsBillUItemVPR_COUNTRY: TFIBStringField;
    ibdsBillUItemVPR_CUSTDECL: TFIBStringField;
    ibdsBillUItemVPR_MATID: TFIBStringField;
    ibdsBillUItemVPR_MAT: TFIBStringField;
    ibdsBillUItemVPR_UNITID: TFIBIntegerField;
    ibdsBillUItemVPR_Q: TFIBIntegerField;
    ibdsBillUItemVPR_W: TFIBFloatField;
    ibdsBillUItemVPR_UID_SUP: TFIBIntegerField;
    ibdsBillUItemVPR_PRICEWONDS: TFIBFloatField;
    ibdsBillUItemVPR_COSTWONDS: TFIBFloatField;
    ibdsBillUItemVPR_EXCISE: TFIBStringField;
    ibdsBillUItemVPR_PNDS: TFIBFloatField;
    ibdsBillUItemVPR_TOTALNDS: TFIBFloatField;
    ibdsBillUItemVPR_PRICE: TFIBFloatField;
    ibdsBillUItemVPR_COST: TFIBFloatField;
    ibdsBillUItemVPR_TR: TFIBFloatField;
    ibdsBillUItemVPR_PRICE2: TFIBFloatField;
    ibdsBillUItemVPR_COST2: TFIBFloatField;
    ibdsBillUItemVPR_PRICE2WONDS: TFIBFloatField;
    ibdsBillUItemVPR_COST2WONDS: TFIBFloatField;
    ibdsBillUItemVPR_TOTAL2NDS: TFIBFloatField;
    ibdsBillUItemVPR_NDATE: TFIBDateTimeField;
    ibdsBillUItemVPR_SSF: TFIBStringField;
    ibdsBillUItemVPR_RATEURF: TFIBFloatField;
    ibdsBillUItemVPR_AKCIZ: TFIBFloatField;
    ibdsBillUItemVPR_TRNDS: TFIBFloatField;
    ibdsBillUItemVPR_RETNAME: TFIBStringField;
    frdsGrInsVp: TfrDBDataSet;
    ibdsGrInsVP: TpFIBDataSet;
    ibdsDisvPASPSER: TFIBStringField;
    ibdsDisvPASPNUM: TFIBStringField;
    ibdsDisvPASPDISTRDATE: TFIBDateField;
    ibdsDisvPASPDISTRORG: TFIBStringField;
    frdsSales: TfrDBDataSet;
    ibdsSales: TpFIBDataSet;
    dsrSales: TDataSource;
    ibdsSalesFULLART: TFIBStringField;
    ibdsSalesPRICE: TFIBFloatField;
    ibdsSalesFCOUNT: TFIBBCDField;
    ibdsSalesSCOUNT: TFIBBCDField;
    ibdsSalesDCOUNT: TFIBIntegerField;
    ibdsSalesDISC: TFIBIntegerField;
    ibdsSalesRCOUNT: TFIBBCDField;
    ibdsSalesLCOUNT: TFIBBCDField;
    ibdsSalesSELLDATE: TFIBDateTimeField;
    ibdsTagWOIFLAG: TFIBIntegerField;
    ibdsTagWOIFLAGCHAR: TFIBStringField;
    ibdsTagWIFLAG: TFIBIntegerField;
    ibdsTagWIFLAGCHAR: TFIBStringField;
    ibdsCustomerNAME: TFIBStringField;
    ibdsCustomerADDRESS: TFIBStringField;
    ibdsCustomerPADDRESS: TFIBStringField;
    ibdsCustomerPHONE: TFIBStringField;
    ibdsCustomerBILL: TFIBStringField;
    ibdsCustomerKBILL: TFIBStringField;
    ibdsCustomerBIK: TFIBStringField;
    ibdsCustomerBANK: TFIBStringField;
    ibdsCustomerINN: TFIBStringField;
    ibdsCustomerOKONH: TFIBStringField;
    ibdsCustomerOKPO: TFIBStringField;
    ibdsCustomerSN: TFIBIntegerField;
    ibdsCustomerNDATE: TFIBDateTimeField;
    ibdsCustomerDOGBD: TFIBDateTimeField;
    ibdsCustomerNDOG: TFIBStringField;
    ibdsCustomerCONTRACTNAME: TFIBStringField;
    ibdsTagWIEUID: TFIBStringField;
    ibdsTagWOIEUID: TFIBStringField;
    ibdsTagWIART2: TFIBStringField;
    ibdsTagWOIART2: TFIBStringField;
    taUIDWHBEUID: TFIBStringField;
    taUIDWHBEART2: TFIBStringField;
    procedure ibdsWhArtBeforeOpen(DataSet: TDataSet);
    procedure ibdsWhArt2BeforeOpen(DataSet: TDataSet);
    procedure frReportUserFunction(const Name: String; p1, p2, p3: Variant;
      var Val: Variant);
    procedure taJ3BeforeOpen(DataSet: TDataSet);
    procedure frDesignerSaveReport(Report: TfrReport;
      var ReportName: String; SaveAs: Boolean; var Saved: Boolean);
    procedure taSelfBeforeOpen(DataSet: TDataSet);
    procedure ibdsFullPrListBeforeOpen(DataSet: TDataSet);
    procedure frReportBeginDoc;
    procedure taTSumDepBeforeOpen(DataSet: TDataSet);
    procedure taUIDWHAFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taInventoryBeforeCalcFields(DataSet: TDataSet);
    procedure taInventoryEndCalcFields(DataSet: TDataSet);
    procedure taD_EmpCalcFields(DataSet: TDataSet);
    procedure taRecCalcFields(DataSet: TDataSet);
    procedure UNITIDGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure taInventoryBeforeBeforeOpen(DataSet: TDataSet);
    procedure taInventoryEndBeforeOpen(DataSet: TDataSet);
    procedure taInventoryAct_NBeforeOpen(DataSet: TDataSet);
    procedure taInventoryAct_EBeforeOpen(DataSet: TDataSet);
    procedure ibdsUID_StoreBeforeOpen(DataSet: TDataSet);
    procedure ibdsUIDWH_ReportBeforeOpen(DataSet: TDataSet);
    procedure ibdsHatWrite_offBeforeOpen(DataSet: TDataSet);
    procedure quUidWhArtBeforeOpen(DataSet: TDataSet);
    procedure quGrInsBeforeOpen(DataSet: TDataSet);
    procedure taUIDWHABeforeOpen(DataSet: TDataSet);
    procedure ibdsCardCheckBeforeOpen(DataSet: TDataSet);
    procedure frAnlzSelEmpBeforeOpen(DataSet: TDataSet);
    procedure ibdsFromAfterOpen(DataSet: TDataSet);
    procedure ibdsBillItemAfterOpen(DataSet: TDataSet);
    procedure ibdsBillItemBeforeOpen(DataSet: TDataSet);
    procedure ibdsGrInsAfterOpen(DataSet: TDataSet);
    procedure ibdsSalesBeforeOpen(DataSet: TDataSet);
    procedure OnArt2Map(DataSet: TDataSet);
    procedure OnArt2Map2(DataSet: TDataSet);
   

  private
    comissionActSumm: Currency;
    IsFlagNew:boolean;
    WidthTagNew:integer;
    FNoTagWI : integer;
    SaveFNoTagWI : integer;
    OstTagWI:integer;
//    FNoTagWOI : integer;
    FColCount : integer;
    FInitDep : boolean;
    FActType: byte;
    FKindPrList : byte; // 1 - �� ��������� ���������; 2 - �� ��������� ����������� �����������
    procedure TagGetValue(const ParName: String; var ParValue: Variant);
    procedure FactureGetValue(const ParName: String; var ParValue: Variant);
    procedure OrderGetValue(const ParName: String; var ParValue: Variant);
    procedure ReduceGetValue(const ParName: String; var ParValue: Variant);
    procedure Act_OVGetValue(const ParName: String; var ParValue: Variant);
    procedure ApplGetValue(const ParName: String; var ParValue: Variant);
    procedure DisvGetValue(const ParName: String; var ParValue: Variant);
    procedure TicketGetValue(const ParName: String; var ParValue: Variant);
    procedure comissionActGetValue(const ParName: String; var ParValue: Variant);
    procedure BillGetValue(const ParName: String; var ParValue: Variant);
    procedure LayoutGetValue(const ParName: String; var ParValue: Variant);
    procedure LayoutGetValue2(const ParName: String; var ParValue: Variant);
    procedure PrListGetValue(const ParName: String; var ParValue: Variant);
    procedure FullPrListGetValue(const ParName: String; var ParValue: Variant);
    procedure MainDocGetValue(const ParName: String; var ParValue: Variant);
    procedure UIDStore_T_GetValue(const ParName : string; var ParValue: Variant);
    procedure UIDStoreDep_T_GetValue(const ParName : string; var ParValue: Variant);
    procedure Seller_Emp_GetValue(const ParName : string; var ParValue: Variant);
    procedure UIDWhInv_GetValue(const ParName : string; var ParValue: Variant);
   
    //
    procedure TicketItBeforeOpen(DataSet: TDataSet);
  public
    // ��� ��������� �� �������
    FilterText : string;
    Mat1, Mat2, Good1, Good2 : string;
    CompId1, CompId2, typeprint : integer;
    // ��� �����
    arr_sell : array of array of Variant;
    // ������� ���������� ����� � �����
    //OrdSort : string;
    // ��� �������� �����
    Discount : Tarr;
    CurrDisc : integer;
    FDocId : integer;     // ����. ����� ���������
    //��� ������ �� �������������� � ����/��������� �����
    InventOutPrice: integer; 
    procedure LoadDoc(DocId : integer);
    // ��� ���������� �� ��������� ��� �� �������
    property ActType : byte read FActType write FActType;
    // �� ��������� ��� ���������� ��������� ��������� ������ ���
    property KindPrList : byte read FKindPrList write FKindPrList;
    //urf=0 - �� ����������� ����; urf=1 - ����������� ��. ����; urf=2 - ���������� ��. ����
    // ��� ������������������ ���������� Id ������� ������ � arr
    procedure PrintDocumentA(arr : Variant; Kind : TKindDoc; URF:integer=0);
    // ��� ������ ���������
    procedure PrintDocumentB(Kind : TKindDoc);
    procedure PrintLayout(Kind : TLayoutKindDoc);
    procedure UIDWHSelectModif(DataSet: TDataSet; const Revis:boolean=false);
    procedure AppleText(const Appl_id:integer;const Sup:string);
  end;

  TKindTag = (f_tag, // �� ��������� ���������, ����������
              act_tag, //�� ���� ����������
              s_tag,  // ����� �� ������� �� ��������� ��������� SelId
              tr_tag,
              it_tag,   // ������������������ ������ SItemId
              new_tag,
              tr_new_tag,
              ret_tag  // �� ������������ �� ���������� �������
              );

  TdcDictKind = (dkMat, dkGood, dkIns, dkComp, dkNds);

  (* ����� ����������� �������������� ������� ��� FreeReport'a *)
  TfrAddLib = class(TfrFunctionLibrary)
  public
    constructor Create; override;
    procedure DoFunction(FNo: Integer; p1, p2, p3: Variant; var val: Variant); override;
  end;

(* arr - ������ Id; Kind - ��� Id: ���������, ������, ��.����� *)
procedure PrintDocument(const arr : TarrDoc; Kind : TKindDoc); overload;
(* arr - ������ Id; Kind - ��� Id: ���������, ������, ��.�����
   IsDep - ������ � ����������� �� ������� *)
//procedure ShowDoc;
procedure PrintTag(const arr : TarrDoc; Kind : TKindTag; IsOld : integer;
                   IsDep : boolean = false);
procedure gen_Arr(var arr : TarrDoc; Grid : TRxDbGrid; Field : TField); overload;
procedure gen_arr(var arr : Tarr; Grid : TRxDbGrid; Field : TField); overload;
procedure gen_arr(var arr : TArr; Grid : TDBGridEh; Field : TField);overload;
function gen_arr(Grid : TDbGrid; Field : TField) : Variant; overload;
function gen_arr(Grid :TDbGridEh; Field : TField) : Variant; overload;

//procedure PrintWHTag(const arr : TarrDoc; Kind : TKindTag);

var
  dmReport: TdmReport;
  FDepName : string;
  SAction : String;

implementation

uses comdata, bsUtils, IniFiles, ServData, WH, M207Proc,
  StdCtrls, ComCtrls, RxStrUtils, UIDStore_T, Data3, FR_Pars, UIDStore_T_C,
  UidStoreDepList, MsgDialog, ShopReport, uUtils, uDialog, Math;

{$R *.DFM}

const
  FRex = '.frf';
  NoTag = 8; //���-�� ����� � ������� �����
  NoTagNew = 7; //���-�� ����� � ������� ����� (�����)
  WidthTag = 88;
  HeigthTag = 100;//??
  WidthPages = 704; //������ ����� �����

procedure gen_arr(var arr : Tarr; Grid : TRxDbGrid; Field : TField);
var
  i : integer;
begin
  with Grid.DataSource.DataSet do
    if Grid.SelectedRows.Count = 0
    then begin
      SetLength(arr, 1);
      arr[0] := Field.AsInteger;
    end
    else begin
      SetLength(arr, Grid.SelectedRows.Count);
      for i:=0 to Pred(Grid.SelectedRows.Count) do
      begin
        GotoBookmark(Pointer(Grid.SelectedRows.Items[i]));
        arr[i] := Field.Value;
      end;
    end;
end;

procedure gen_arr(var arr : TArr; Grid : TDBGridEh; Field : TField);overload;
var
  i : integer;
begin
  with Grid.DataSource.DataSet do
    if Grid.SelectedRows.Count = 0
    then begin
      SetLength(arr, 1);
      arr[0] := Field.AsInteger;
    end
    else begin
      SetLength(arr, Grid.SelectedRows.Count);
      for i:=0 to Pred(Grid.SelectedRows.Count) do
      begin
        GotoBookmark(Pointer(Grid.SelectedRows.Items[i]));
        arr[i] := Field.Value;
      end;
    end;
end;

procedure gen_Arr(var arr : TarrDoc; Grid : TRxDbGrid; Field : TField);
var
  i : integer;
begin
  with Grid.DataSource.DataSet do
    if Grid.SelectedRows.Count = 0
    then begin
      SetLength(arr, 1);
      arr[0] := Field.AsInteger;
    end
    else begin
      SetLength(arr, Grid.SelectedRows.Count);
      for i:=0 to Pred(Grid.SelectedRows.Count) do
      begin
        GotoBookmark(Pointer(Grid.SelectedRows.Items[i]));
        arr[i] := Field.AsInteger;
      end;
    end;
end;

function gen_arr(Grid :TDbGrid; Field : TField) : Variant; overload;
var
  i : integer;
begin
  with Grid.DataSource.DataSet do
    if Grid.SelectedRows.Count = 0 then Result := VarArrayOf([Field.AsInteger])
    else begin
      Result := VarArrayCreate([0, Grid.SelectedRows.Count-1], varInteger);
      for i:=0 to Pred(Grid.SelectedRows.Count) do
      begin
        GotoBookmark(Pointer(Grid.SelectedRows.Items[i]));
        Result[i] := Field.AsInteger;
      end;
    end;
end;

function gen_arr(Grid :TDbGridEh; Field : TField) : Variant; overload;
var
  i : integer;
begin
  with Grid. DataSource.DataSet do
    if Grid.SelectedRows.Count = 0 then Result := VarArrayOf([Field.AsInteger])
    else begin
      Result := VarArrayCreate([0, Grid.SelectedRows.Count-1], varInteger);
      for i:=0 to Pred(Grid.SelectedRows.Count) do
      begin
        GotoBookmark(Pointer(Grid.SelectedRows.Items[i]));
        Result[i] := Field.AsInteger;
      end;
    end;
end;

function gen_Query(arr : TarrDoc; sBegin, sEnd : string; const Params : array of string)  : string;
var
  s, p : string;
  i,j : integer;
begin
{ TODO : gen_query }
  p := Params[Low(Params)]+IntToStr(Low(Params));
  for i := Low(Params)+1 to High(Params) do
    p := p + ',' + Params[i]+IntToStr(Low(Params))+' ';

  s := sBegin + p + sEnd;
  for i := Low(Params)+1 to High(arr) do
  begin
    p := Params[Low(Params)]+IntToStr(i);
    for j := Low(Params)+1 to High(Params) do
       p := p + ',' + Params[j]+IntToStr(i) +' ';
    s := s+' union '+sBegin + p + sEnd +#13;
  end;
  Result := s;
end;

procedure ShowDoc;
begin
  with dmReport.frReport do
    case DocPreview of
      pvPreview : ShowReport;
      pvDesign  : begin
        FileName := IntToStr(dmReport.FDocId)+'.frf';
        DesignReport;
      end;
      pvPrint : begin
        PrepareReport;
        PrintPreparedReport('', DefaultCopies, DefaultCollate, frAll);
      end;
    end;
end;

var
  iiiii: integer;

procedure PrintDocument(const arr : TarrDoc; Kind : TKindDoc);
var
  i : integer;
  DocPreview_1 : TdcPreview;
begin
  Screen.Cursor := crSQLWait;
  with dmReport do
  try
    if not trReport.Active then trReport.StartTransaction;
    case Kind of
      (* ��������� ��������� �� 3-�� ������ maindoc *)
      maindoc3 : begin
        LoadDoc(1); // maindoc
        frReport.OnGetValue := MainDocGetValue;
        ShowDoc;
      end;
      maindoc : begin
        LoadDoc(101);
        frReport.OnGetValue := MainDocGetValue;
        ShowDoc;
      end;
      (* ����-�������, ��������� *)
      facture, invoice, facture_wh, invoice_wh : begin
        case Kind of
          facture : LoadDoc(2); // facture
          invoice : LoadDoc(3); // invoice
          facture_wh : begin
            LoadDoc(4); // wh_facture
            ibdsSupplier.SelectSQL.Text :=
               'select c.Name, stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, c.Phone, c.Bill, c.KBill, c.BIK, c.Bank, '+
               'c.INN, c.OKONH, c.OKPO, s.Sn V_Sn, s.NDate, c.KPP , s.SINVID '+
               'from d_comp c, d_rec r, sinv s '+
               'where s.SInvId=:P_Sn and c.D_CompId=s.CompId';
          end;
          invoice_wh : begin
            LoadDoc(5); // wh_invoice
            ibdsSupplier.SelectSQL.Text :=
               'select c.Name, stretrim(c.PostIndex)||'#39+' '#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, c.Phone, c.Bill, c.KBill, c.BIK, c.Bank, '+
               'c.INN, c.OKONH, c.OKPO, s.Sn V_Sn, s.NDate, c.KPP, s.SINVID '+
               'from d_comp c, d_rec r, sinv s '+
               'where s.SInvId=:P_Sn and c.D_CompId=s.CompId';
          end;
        end;
        frReport.OnGetValue := FactureGetValue;
        for i := Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ibdsSupplier.Params[0].Value := arr[i];
          ibdsCustomer.Params[0].Value := arr[i];
          ibdsBillItem.Params[0].Value := arr[i];
          ibdsBillItem.Params[1].Value := 0;
          OpenDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ShowDoc;
        end;
      end;
      (* ���������� ��������� � ����-������� *)
     { facture_ret, invoice_ret,}
     invoice_art_ret, invoice_art_ret_2, invoice_ret_01, repair_uid, act_retsinv, invoice_12_sup: begin
        case Kind of
      //    facture_ret : LoadDoc(6); // facture_ret
     //     invoice_ret : LoadDoc(7);  //invoice_ret

          repair_uid : LoadDoc(307);
          invoice_art_ret :
            {$IFDEF RGOST}
            LoadDoc(74);
            {$ELSE}
            LoadDoc(107); //invoice_ret
            {$ENDIF}
          invoice_art_ret_2 : LoadDoc(74);
          invoice_ret_01 : LoadDoc(74); //invoice_ret
          act_retsinv : LoadDoc(76);
          invoice_12_sup: LoadDoc(374);
        end;
        ibdsSupplier.SelectSQL.Text := 'select c.Name,stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, c.Phone, c.Bill, '+
           'c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.SSF, s.Sn V_Sn, s.NDate, s.SDate, c.KPP, s.SINVID '+
           'from d_comp c, sinv s, d_rec r where s.SInvId=:P_Sn and c.D_CompId=r.D_CompId';
           //s.SupName
        if Kind =invoice_ret_01 then
        ibdsCustomer.SelectSQL.Text := 'select c.Name, stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, '+
        'stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.PAddress) PAddress, c.Phone, c.Bill, '+
           'c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn, s.NDate, s.SINVID, cnt.contract$date DOGBD, cnt.Number NDOG, ct.name ContractName '+
           'from d_comp c, sinv s, contract cnt, contract$types ct where s.CompId=476 and s.CompId = c.D_CompId and cnt.id = s.contract$id and ct.id = cnt.contract$type ' +
           'ct.id = cnt.contract$type'
        else
        ibdsCustomer.SelectSQL.Text := ' select Name,  Address,PAddress, Phone, Bill,KBill, BIK, Bank, INN, OKONH, OKPO, Sn, NDate, SINVID,DOGBD,  NDOG,ContractName from PRINT$SINV(:P_Sn)';

        frReport.OnGetValue := FactureGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ibdsSupplier.Params[0].Value := arr[i];
          if Kind <> invoice_ret_01 then ibdsCustomer.Params[0].Value := arr[i];
          ibdsBillItem.Params[0].Value := arr[i];
          if Kind=invoice_art_ret then ibdsBillItem.Params[1].Value := 33
          else if Kind=invoice_art_ret_2 then ibdsBillItem.Params[1].Value := 22
          else if Kind=invoice_ret_01 then ibdsBillItem.Params[1].Value := 44
          else if Kind=repair_uid then ibdsBillItem.Params[1].Value := 9
          else ibdsBillItem.Params[1].Value := 3;
          OpenDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ShowDoc;
        end;
      end;
      (* ����-�������, ��������� ������� �� ������� ����������� *)
      facture_r_opt, invoice_r_opt : begin
        LoadDoc(2);
        frReport.OnGetValue := FactureGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([taProvider, taSelf, ibdsBillItem]);
          taProvider.Params[0].Value := arr[i];
          taProvider.Params[1].Value := 5;
          taSelf.Params[0].Value := 0;
          taSelf.Params[1].Value := 1;
          ibdsBillItem.Params[0].Value := arr[i];
          ibdsBillItem.Params[1].Value := 5;
          OpenDataSets([taProvider, taSelf, ibdsBillItem]);
          ShowDoc;
        end;
      end;
      (* ����-�������, ��������� ��� ������� ����������� *)
      facture_opt, invoice_opt: begin
        case Kind of
          facture_opt : LoadDoc(10); // facture_opt
          invoice_opt : LoadDoc(11); // invoice_opt
        end;
        ibdsSupplier.SelectSQL.Text := 'select c.Name, stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, c.Phone, c.Bill, '+
           'c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn V_Sn, s.NDate, s.SDate, s.SSF, s.SINVID '+
           'c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn V_Sn, s.NDate, s.SDate, s.SSF, c.KPP '+
           'from d_comp c, d_rec r, sinv s where s.SInvId=:P_Sn and r.D_CompId = c.D_CompId';
        ibdsCustomer.SelectSQL.Text := 'select c.Name, stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.Address) Address, ' +
        'stretrim(c.PostIndex)||'#39+' '+#39'||stretrim(c.City)||'#39+' '+#39'||stretrim(c.PAddress) PAddress, c.Phone, c.Bill, '+
           'c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn, s.NDate, s.SINVID '+
           'from d_comp c, sinv s where s.SInvId=:P_Sn and s.CompId = c.D_CompId';
        frReport.OnGetValue := FactureGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ibdsSupplier.Params[0].Value := arr[i];
          ibdsCustomer.Params[0].Value := arr[i];
          ibdsBillItem.Params[0].Value := arr[i];
          ibdsBillItem.Params[1].Value := 4;
          OpenDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem]);
          ShowDoc;
        end;
      end;

       (* ��������� ��� ������� ����������� � ����.����� (�� ����� ����.��������� ����-12) *)
      invoice_opt_pr: begin
        LoadDoc(1303);
        frReport.OnGetValue := FactureGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem, taProvider, taSelf]);
          ibdsBillItem.Params[0].Value := arr[i];
          ibdsBillItem.Params[1].Value := 10;
          taProvider.Params[0].Value := arr[i];
          taProvider.Params[1].Value := 6;
          taSelf.Params[0].Value := arr[i];
          taSelf.Params[1].Value := 7;
          OpenDataSets([ibdsSupplier, ibdsCustomer, ibdsBillItem, taProvider, taSelf]);
          ShowDoc;
        end;
      end;
      invoice_in:begin
        LoadDoc(112);
        frReport.OnGetValue := FactureGetValue;
        CloseDataSets([ibdsFrom,ibdsTo,ibdsBillItemVP,ibdsGrIns]);
        ibdsFrom.SelectSQL.text:=gen_query(arr,'select SN, NDATE, NAME, CITY, ADDRESS, PHONE, SINVID from DOC_PROVIDER(',
        ')', [':ID',':KIND']);
        ibdsTo.SelectSQL.Text:=gen_query(arr,'select NAME,  ADDRESS, PHONE, CITY  from DOC_PROVIDER(',
        ')', [':ID',':KIND']);
        //ibdsBillItem.SelectSQL.text:= 'select * from doc_Bill(:SINVID,:KIND)';
        //ibdsGrIns.SelectSQL.text:='select * from InsByArt2(:R_Art2Id)';
        //taSelf.SelectSQL.text:='select COMPID, NAME, BOSS, OFIO, ADDRESS, PHONE, FAX,EMAIL, INN, BIK, OKPO, OKONH, BANK, BILL, KBILL,OFIOPHONE from DOC_PROVIDER(:ID, :KIND)' ;
        for i := Low(arr) to High(arr) do
        begin
         ibdsFrom.Params[2*i].AsInteger := arr[i];
         ibdsFrom.Params[2*i+1].AsInteger := 4;
         ibdsTo.Params[2*i].AsInteger := arr[i];
         ibdsTo.Params[2*i+1].AsInteger := 3;
        end;
        //ibdsBillItem.Params.ByName['SINVID'].AsInteger := arr[i];
        ibdsBillItemVP.Params.ByName['Kind'].AsInteger := 1;
        //ibdsGrIns.Params.ByName['R_Art2Id'].AsInteger := arr[i];
        taSelf.Params.ByName['ID'].AsInteger := 0;
        taSelf.Params.ByName['KIND'].AsInteger := 1;

        OpenDataSets([ibdsFrom,ibdsTo,ibdsBillItemVP,ibdsGrIns,taSelf]);

        ShowDoc;
        closeDataSets([ibdsFrom,ibdsTo,ibdsBillItemVP,ibdsGrIns,taSelf]);
        
      end;
      (* ���������� ���������(�����������), ���������� ���������(�� ��������)*)
      invoice_in_sprice, invoice_uin : begin
        case Kind of
           invoice_in_sprice : LoadDoc(12); // in_invoice
           invoice_uin : LoadDoc(13); // uin_invoice
        end;
        frReport.OnGetValue := FactureGetValue;
        CloseDataSets([ibdsFrom, ibdsTo]);
        if kind= invoice_in_sprice then CloseDataSets([ibdsBillItemVP, ibdsGrIns])
        else CloseDataSets([ibdsBillUItemVP]);
        ibdsFrom.SelectSQL.text:=gen_query(arr,'select SN, NDATE, NAME, CITY, ADDRESS, PHONE, SINVID from DOC_PROVIDER(',
      ')', [':ID',':KIND']);
        ibdsTo.SelectSQL.Text:=gen_query(arr,'select NAME,  ADDRESS, PHONE, CITY  from DOC_PROVIDER(',
      ')', [':ID',':KIND']);
        for i := Low(arr) to High(arr) do
         begin
          ibdsFrom.Params[2*i].AsInteger := arr[i];
          ibdsFrom.Params[2*i+1].AsInteger := 4;
          ibdsTo.Params[2*i].AsInteger := arr[i];
          ibdsTo.Params[2*i+1].AsInteger := 3;
         end;
          //ibdsBillItem.Params.ByName['SINVID'].AsInteger := arr[i];
       ibdsBillItemVP.Params.ByName['KIND'].AsInteger := 8;
          //ibdsBillUItem.Params.ByName['SINVID'].AsInteger := arr[i];
       ibdsBillUItemVP.Params.ByName['KIND'].AsInteger := 2;
       OpenDataSets([ibdsFrom, ibdsTo]);
       if Kind=invoice_in_sprice then OpenDataSets([ibdsBillItemVP, ibdsGrIns])
       else OpenDataSets([ibdsBillUItemVP]);
       ShowDoc;
       CloseDataSets([ibdsFrom, ibdsTo]);
       if Kind=invoice_in_sprice then CloseDataSets([ibdsBillItemVP, ibdsGrIns])
       else CloseDataSets([ibdsBillUItemVP]);
       end;

      (* ������, ������ �� ��������� *)
      order, f_order : begin
        LoadDoc(14); // order
        frReport.OnGetValue := OrderGetValue;
        CloseDataSets([ibdsOrder, ibdsOrderItem]);
        ibdsOrder.SelectSQL.Text := gen_Query(arr, 'select R_DepName, R_DepCode, R_SnDate,'+
              'R_Sn, R_NoOrder, R_Link, R_SetDate, R_SetEmpName, R_COMP from Doc_Order(',
              ') ', [':P_ID', ':P_Kind']);
        for i := Low(arr) to High(arr) do
        begin
          ibdsOrder.Params[2*i].Value := arr[i];
          case Kind of
            order  : ibdsOrder.Params[2*i+1].AsInteger := 0;
            f_order : ibdsOrder.Params[2*i+1].AsInteger := 1;
          end;
        end;
        OpenDataSets([ibdsOrder, ibdsOrderItem]);
        ShowDoc;
      end;

      (* ��������� �� ��������� *)
      o_reduce, f_reduce,f_reduce2, f_reduce3 : begin
        if Kind=f_reduce2 then LoadDoc(115)
           else LoadDoc(15); // reduce
 //       DocPreview:=pvDesign;
        if Kind=f_reduce3 then LoadDoc(215);
        frReport.OnGetValue := ReduceGetValue;
        CloseDataSets([ibdsReduce, ibdsReduceItem, ibdsitemSlist]);
        case Kind of
          f_reduce,f_reduce2 : ibdsReduce.SelectSQL.Text := gen_Query(arr, 'select s.SInvId R_Link, '+
              's.Sn R_No, s.SDate R_Date from SInv S where S.SInvId=','',[':P_SInvId']);
          f_reduce3 :ibdsReduce.SelectSQL.Text := gen_Query(arr, 'select s.SInvId R_Link, '+
              's.Sn R_No, s.SDate R_Date from SInv S where S.SInvId=','',[':P_SInvId']);
        end;
        for i := Low(arr) to High(arr) do
         begin
          ibdsReduce.Params[i].AsInteger := arr[i];
          ibdsitemSlist.Params[i].AsInteger:=arr[i];
         end;
        case Kind of
            f_reduce,f_reduce2,f_reduce3 : ibdsReduceItem.Params[1].AsInteger := 0;
        end;
        OpenDataSets([ibdsReduce, ibdsReduceItem, ibdsitemSlist]);
        ShowDoc;
      end;
      (* ������ �������� *)
      {r_list : begin
        LoadDoc(16); // r_list
        frReport.OnGetValue := R_ListGetValue;
        ibdsUIDWH.Params[0].AsInteger := -MAXINT;
        ibdsUIDWH.Params[1].AsInteger := MAXINT;
        OpenDataSets([ibdsUIDWH]);
        ShowDoc;
      end;}
      (* ������ ��������� ������� *)
      s_list : begin
      end;
      (* ��� ���������� ��-�������� *)
      act_uid : begin
        LoadDoc(17); // act_ov_i
        frReport.OnGetValue := Act_OVGetValue;
        CloseDataSets([ibdsActOV, ibdsActOVItem]);
        ibdsActOV.SelectSQL.Text := gen_query(arr, 'select R_DEPNAME, '+
          'R_DEPCODE, R_NOORDER, R_LINK, R_SETDATE, R_NACT from Doc_ActOV(', ') ',
          [':P_Id', ':P_Kind']);

        ibdsActOVItem.SelectSQL.Text := 'SELECT R_SUP, R_MAT, R_GOOD, R_ART, '+
          'R_ART2, R_SIZE, RW, RQ, PRICE2, OLDP2, R_UID, R_UNITID, ITYPE, NEWCOST, OLDCOST '+
          'FROM DOC_ACTOVITEM(:R_Link) ORDER BY R_ART, R_SIZE, R_ART2'; //R_UID';
        for i:= Low(arr) to High(arr) do
        begin
        ShowMessage(ibdsActOVItem.paramByName('R_Link').asString);

          ibdsActOV.Params[2*i].AsInteger := arr[i];
          ibdsActOV.Params[2*i+1].AsInteger := ActType;
        end;
        //OpenDataSets([ibdsActOV, ibdsActOVItem]);
        ShowDoc;
        CloseDataSets([ibdsActOV, ibdsActOVItem]);
      end;
      (* ��� ���������� ��-���������� *)
      act_art : begin
        LoadDoc(18); // act_ov
        frReport.OnGetValue := Act_OVGetValue;
        CloseDataSets([ibdsActOV, ibdsActOVItem]);
        ibdsActOV.SelectSQL.Text := gen_query(arr, 'select R_DEPNAME, '+
          'R_DEPCODE, R_NOORDER, R_LINK, R_SETDATE, R_NACT from Doc_ActOV(', ')',
          [':P_Id', ':P_Kind']);
        for i:= Low(arr) to High(arr) do
        begin
          ibdsActOV.Params[2*i].AsInteger := arr[i];
          ibdsActOV.Params[2*i+1].AsInteger := 0;
        end;
        //OpenDataSets([ibdsActOV, ibdsActOVItem]);
        ShowDoc;
        CloseDataSets([ibdsActOV, ibdsActOVItem]);
      end;
      (* ��������� �� ���������� � ����� *)
      appl : begin
        if dmcom.Printtype=0 then    LoadDoc(119) // appl
        else LoadDoc(19);
        frReport.OnGetValue := ApplGetValue;
        CloseDataSets([ibdsAppl, ibdsApplCause]);
        ibdsAppl.Params[0].AsInteger := arr[0];
        ibdsAppl.Params[1].AsInteger := 0;
        ibdsApplCause.Params[0].AsInteger := arr[0];
        ibdsApplCause.Params[1].AsInteger := 0;
        if not dmcom.IsPrintCheck then ShowDoc
        else
        begin
         DocPreview_1:=DocPreview;
         DocPreview:=pvPrint;
         ShowDoc;
         DocPreview:=DocPreview_1;
        end;
      end;
      applresp : begin
        if dmcom.Printtype=0 then    LoadDoc(319) // appl
        else LoadDoc(219);
        frReport.OnGetValue := ApplGetValue;
        CloseDataSets([ibdsAppl, ibdsApplCause]);
        ibdsApplResp.Params[1].AsInteger := arr[0];
        ibdsApplResp.Params[0].AsInteger := 0;
        ibdsApplRespCause.Params[1].AsInteger := arr[0];
        ibdsApplRespCause.Params[0].AsInteger := 0;
        if not dmcom.IsPrintCheck then ShowDoc
        else
        begin
         DocPreview_1:=DocPreview;
         DocPreview:=pvPrint;
         ShowDoc;
         DocPreview:=DocPreview_1;
        end;
      end;
      (* ��������� ����� *)
      disv : begin
       //if dmcom.Printtype=0 then LoadDoc(220)  // disv
        { else} LoadDoc(220);  // disv
        frReport.OnGetValue := DisvGetValue;
        CloseDataSets([ibdsDisv]);
        with ibdsDisv.Params do
        begin
          ByName['P_Id'].AsInteger := arr[0];
          ByName['P_Kind'].AsInteger := 1;
        end;
        if not dmcom.IsPrintCheck then
        ShowDoc
        else
        begin
         DocPreview_1:=DocPreview;
         DocPreview:=pvPrint;
         ShowDoc;
         DocPreview:=DocPreview_1;
        end;
      end;
      (* �������� ��� *)
      ticket : begin
        frReport.OnGetValue := TicketGetValue;
        case  dmcom.Printtype of
         1: LoadDoc(422);
         2: LoadDoc(22);
        end;
       ibdsTicketIt.close;
       taTicket.close;
        taTicket.SelectSQL.Text := gen_query(arr, 'select CHECKNO from SellItem '+
         ' where SellItemId=', '', [':ID']);
        for i:=Low(arr) to High(arr) do
          taTicket.Params[i].AsInteger := arr[i];
        taTicket.Open;
        //ibdsTicketIt.BeforeOpen := TicketItBeforeOpen;
        ibdsTicketIt.DataSource := dsrTicket;
        TicketItBeforeOpen(ibdsTicketIt);
        ibdsTicketIt.open;
        if not dmcom.IsPrintCheck then ShowDoc
        else
        begin
         DocPreview_1:=DocPreview;
         DocPreview:=pvPrint;
         ShowDoc;
         DocPreview:=DocPreview_1;
        end;
        ibdsTicketIt.BeforeOpen := nil;
      end;

      (*��� ������ �� �������*)
      act_ticket : begin
        frReport.OnGetValue := TicketGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          LoadDoc(122); //ticket_disc
          CloseDataSets([ibdsTicketIt]);
          ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                       '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                       '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                       '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                       ' from Doc_Ticket(:I_ID, :I_KIND) ';
          ibdsTicketIt.Prepare;
          ibdsTicketIt.Params[0].AsInteger := arr[i];
          ibdsTicketIt.Params[1].AsInteger := 1;
          OpenDataSets([ibdsTicketIt]);
          CurrDisc := i;
          ShowDoc;
        end;
      end;

      (*��� ������ �� �������*)
      actret_ticket : begin
        frReport.OnGetValue := TicketGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          LoadDoc(622); //ticket_disc
          CloseDataSets([ibdsTicketIt]);
          ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                       '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                       '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                       '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                       ' from Doc_Ticket(:I_ID, :I_KIND) ';
          ibdsTicketIt.Prepare;
          ibdsTicketIt.Params[0].AsInteger := arr[i];
          ibdsTicketIt.Params[1].AsInteger := 6;
          OpenDataSets([ibdsTicketIt]);
          CurrDisc := i;
          ShowDoc;
        end;
      end;

     (*��� ����� - ��������� ����*)
      sell_ticket : begin
//         DocPreview:=pvDesign ;
         frReport.OnGetValue := TicketGetValue;
         LoadDoc(222); //
         CloseDataSets([ibdsTicketIt]);
         ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                      '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                      '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                      '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                      ' from Doc_Ticket(:I_ID, :I_KIND) ';
         ibdsTicketIt.Prepare;                                      
         ibdsTicketIt.Params[0].AsInteger := arr[0];
         ibdsTicketIt.Params[1].AsInteger := 2;
         OpenDataSets([ibdsTicketIt]);
         ShowDoc;
        end;
     (*��� ����� - ��������� ����*)
      sell_ticketOutlay : begin
//         DocPreview:=pvDesign ;
         frReport.OnGetValue := TicketGetValue;
         LoadDoc(322); //
         CloseDataSets([ibdsTicketIt]);
         ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                      '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                      '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                      '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                      ' from Doc_Ticket(:I_ID, :I_KIND) ';
         ibdsTicketIt.Prepare;
         ibdsTicketIt.Params[0].AsInteger := arr[0];
         ibdsTicketIt.Params[1].AsInteger := 2;
         OpenDataSets([ibdsTicketIt]);
         ShowDoc;
        end;
      sell_ticketart : begin
//         DocPreview:=pvDesign ;
         frReport.OnGetValue := TicketGetValue;
         LoadDoc(222); //
         CloseDataSets([ibdsTicketIt]);
         ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                      '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                      '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                      '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                      ' from Doc_Ticket(:I_ID, :I_KIND) ';
         ibdsTicketIt.Prepare;
         ibdsTicketIt.Params[0].AsInteger := arr[0];
         ibdsTicketIt.Params[1].AsInteger := 4;
         OpenDataSets([ibdsTicketIt]);
         ShowDoc;
        end;
      sell_ticketsinv : begin
         frReport.OnGetValue := TicketGetValue;
         LoadDoc(522); //
         CloseDataSets([ibdsTicketIt]);
         ibdsTicketIt.SelectSQL.Text:='select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID, '+
                                      '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
                                      '  R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount, '+
                                      '  R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SSF, r_actdiscount '+
                                      ' from Doc_Ticket(:I_ID, :I_KIND) ';
         ibdsTicketIt.Prepare;
         ibdsTicketIt.Params[0].AsInteger := arr[0];
         ibdsTicketIt.Params[1].AsInteger := 5;
         OpenDataSets([ibdsTicketIt]);
         ShowDoc;
        end;

      (*��������� �� ������*)
      appl_dep : begin
        for i:=Low(arr) to High(arr) do
        begin
          LoadDoc(69);
          CloseDataSets([ibdsApplDep]);
          ibdsApplDep.ParamByName('SINVID').AsInteger := arr[i];
          ibdsApplDep.ParamByName('I_KIND').AsInteger := 0;
          OpenDataSets([ibdsApplDep]);
          ShowDoc;
        end;
      end;

      (*��������� �� ������*)
      appl_dep_art : begin
        for i:=Low(arr) to High(arr) do
        begin
          LoadDoc(69);
          CloseDataSets([ibdsApplDep]);
          ibdsApplDep.ParamByName('SINVID').AsInteger := arr[i];
          ibdsApplDep.ParamByName('I_KIND').AsInteger := 1;
          OpenDataSets([ibdsApplDep]);
          ShowDoc;
        end;
      end;

      (*���� �� ����������*)
      bill: begin
        ibdsGrIns.DataSource := dsrSBillItem;
        LoadDoc(23);  //
        frReport.OnGetValue := BillGetValue;
        CloseDataSets([taSelf, ibdsSBillItem, ibdsGrIns]);
        taSelf.Params[0].asInteger:=arr[0];
        taSelf.Params[1].asInteger:=1;
        ibdsSBillItem.Params[0].asInteger:=arr[0];
        ibdsSBillItem.Params[1].asInteger:=6;
        ibdsGrIns.Params[0].AsInteger := arr[0];
        OpenDataSets([taSelf, ibdsSBillItem, ibdsGrIns]);
        ShowDoc;
       end;
      (* ������ ��� �� ��������� ��������� *)
    {  pricelist : begin
        LoadDoc(26);  // prlist
        frReport.OnGetValue := PrListGetValue;
        for i:=Low(arr) to High(arr) do
        begin
          CloseDataSets([ibdsPrList, ibdsPrListItem]);
          ibdsPrList.Params[0].AsInteger := arr[i];
          OpenDataSets([ibdsPrList, ibdsPrListItem]);
          ShowDoc;
        end;
      end;  }
      (* ���������� �� ������������ *)
      cpa : begin
        LoadDoc(28); // cpa_s
        taComp.SelectSQL.Text := gen_Query(arr, 'SELECT NAME, OFIO, PHONE, FAX, CODE '+
                'FROM D_Comp where D_CompId=', ' ', [':D_CompId']);
        for i :=Low(arr) to High(arr) do taComp.Params[i].Value := arr[i];
         //taComp.Open;
        ShowDoc;
        CloseDataSets([taComp]);
      end;
      (* ������ ��� ���������� *)
      appl_sup : begin
        LoadDoc(29); // � ������ ��� 2
        taApplSup.Params[0].Value := arr[0];
        taApplZ.Params[0].Value := arr[0];
        OpenDataSets([taApplSup,taApplZ]);
        ShowDoc;
        CloseDataSets([taApplSup,taApplZ]);
        end;
      appl_sup1 : begin
        LoadDoc(1003); // ��� ����� ��� 2
        taApplSup.Params[0].Value := arr[0];
        taApplZ.Params[0].Value := arr[0];
        OpenDataSets([taApplSup,taApplZ]);
        ShowDoc;
        CloseDataSets([taApplSup,taApplZ]);
        end;
     shop_report : begin
        LoadDoc(116);
        taShopReport.ParamByName('SINVID').AsInteger := arr[0];
        OpenDataSet(taShopReport);
        ShowDoc;
        CloseDataSet(taShopReport);
     end;

     center_report : begin
        LoadDoc(216);

        taCenterReport.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, i.SDATE REPORTBD, '+
                                        'i.NDATE REPORTED, i.SN REPORTNO, d.NAME SHOPNAME, e.FNAME, '+
                                        'e.LNAME, e.SNAME, it.cost2, it.cost3, it.buhrep '+
                                        'from SInv i, ShopReport_S(:SINVID) it, D_Dep d, D_Emp e '+
                                        'where i.SINVID=it.SINVID and '+
                                              'i.DEPID=d.D_DEPID and '+
                                              'i.USERID = e.D_EMPID and '+
                                              'it.t in (114, 104, 102, 105, 107)';


        quCenterIncoming.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                          'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                          'from ShopReport_S(:SINVID) it where it.t=0';

        quCenterItog.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                      'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                      'from ShopReport_S(:SINVID) it where it.t in (999)';

        quCenterItogIn.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                        'from ShopReport_S(:SINVID) it where it.t in (-2, -3) order by it.t desc';

        quCenterItogOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                         'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                         'from ShopReport_S(:SINVID) it where it.t in (-5)';

        quCenterOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                     'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                     'from ShopReport_S(:SINVID) it where it.t in (106, 108, 109, 111, -6)';

        quCenterDefferent.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                           'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                           'from ShopReport_S(:SINVID) it where it.t in (-7,115, -9, 116)';

        taCenterReport1.SelectSQL.Text:= 'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDOC, '+
                                        'cost2, cost3, buhrep, depsel, color, sn from ShopReport_S(:SINVID)';

        taCenterReport1.ParamByName('SINVID').AsInteger := arr[0];
        taCenterReport.ParamByName('SINVID').AsInteger := arr[0];
        quCenterIncoming.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogIn.ParamByName('SINVID').AsInteger := arr[0];
        quCenterOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItog.ParamByName('SINVID').AsInteger := arr[0];
        quCenterDefferent.ParamByName('SINVID').AsInteger := arr[0];
        OpenDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                      quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);
        ShowDoc;
        CloseDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                       quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);
     end;

     center_report_com : begin
        LoadDoc(1216);   // ������ (�������� �� ����������)
        taCenterReport.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, i.SDATE REPORTBD, '+
                                        'i.NDATE REPORTED, i.SN REPORTNO, d.NAME SHOPNAME, e.FNAME, '+
                                        'e.LNAME, e.SNAME, it.cost2, it.cost3, it.buhrep '+
                                        'from SInv i, ShopReport_Com_S(:SINVID) it, D_Dep d, D_Emp e '+
                                        'where i.SINVID=it.SINVID and '+
                                              'i.DEPID=d.D_DEPID and '+
                                              'i.USERID = e.D_EMPID and '+
                                              'it.t in (114, 104, 102, 105, 107)';
        // �������� ����� �������
        quCenterIncoming.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                          'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                          'from ShopReport_Com_S(:SINVID) it where it.t=0';
        // ��������� ����� �������
        quCenterItog.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                      'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                      'from ShopReport_Com_S(:SINVID) it where it.t in (999)';
         // ����� ������ � ����� ������ � ���������
        quCenterItogIn.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                        'from ShopReport_Com_S(:SINVID) it where it.t in (-2, -3) order by it.t desc';
         // ����� ������
        quCenterItogOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                         'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                         'from ShopReport_Com_S(:SINVID) it where it.t in (-5)';
        // ����� �������: ��� ��������, �������, ������� �����������. + ������� ����-��� (�� ������� ����. ��������)
        quCenterOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                     'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                     'from ShopReport_Com_S(:SINVID) it where it.t in (106, 108, 109, 111, -6)';
        // ������� (������������� �� ��������)
        quCenterDefferent.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                           'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                           'from ShopReport_Com_S(:SINVID) it where it.t in (-7,115, -9, 116)';
        taCenterReport1.Fields.Clear;

        taCenterReport1.SelectSQL.Text:= 'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDOC, '+
                                        'cost2, cost3, buhrep, depsel, color, sn from ShopReport_Com_S(:SINVID)';

        taCenterReport1.ParamByName('SINVID').AsInteger := arr[0];
        taCenterReport.ParamByName('SINVID').AsInteger := arr[0];
        quCenterIncoming.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogIn.ParamByName('SINVID').AsInteger := arr[0];
        quCenterOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItog.ParamByName('SINVID').AsInteger := arr[0];
        quCenterDefferent.ParamByName('SINVID').AsInteger := arr[0];

        OpenDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                      quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);

        ShowDoc;

        CloseDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                       quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);
     end;

     center_registry_com : begin
        LoadDoc(1218);   // ������ (�������� �� ����������)

        taCenterReport.Fields.Clear;

        taCenterReport.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, i.SDATE REPORTBD, '+
                                        'i.NDATE REPORTED, i.SN REPORTNO, d.NAME SHOPNAME, e.FNAME, '+
                                        'e.LNAME, e.SNAME, it.cost2, it.cost3, it.buhrep '+
                                        'from SInv i, ShopReport_Com_S2(:SINVID) it, D_Dep d, D_Emp e '+
                                        'where i.SINVID=it.SINVID and '+
                                              'i.DEPID=d.D_DEPID and '+
                                              'i.USERID = e.D_EMPID and '+
                                              'it.t in (114, 104, 102, 105, 107)';
        // �������� ����� �������
        quCenterIncoming.Fields.Clear;
        quCenterIncoming.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                          'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                          'from ShopReport_Com_S2(:SINVID) it where it.t=0';
        // ��������� ����� �������
        quCenterItog.Fields.Clear;
        quCenterItog.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                      'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                      'from ShopReport_Com_S2(:SINVID) it where it.t in (999)';
         // ����� ������ � ����� ������ � ���������
        quCenterItogIn.Fields.Clear;
        quCenterItogIn.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                        'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                        'from ShopReport_Com_S2(:SINVID) it where it.t in (-2, -3) order by it.t desc';
         // ����� ������
        quCenterItogOut.Fields.Clear;
        quCenterItogOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                         'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                         'from ShopReport_Com_S2(:SINVID) it where it.t in (-5)';
        // ����� �������: ��� ��������, �������, ������� �����������. + ������� ����-��� (�� ������� ����. ��������)
        quCenterOut.Fields.Clear;
        quCenterOut.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                     'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                     'from ShopReport_Com_S2(:SINVID) it where it.t in (106, 108, 109, 111, -6)';
        // ������� (������������� �� ��������)
        quCenterDefferent.Fields.Clear;
        quCenterDefferent.SelectSQL.Text:= 'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '+
                                           'it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.buhrep '+
                                           'from ShopReport_Com_S2(:SINVID) it where it.t in (-7,115, -9, 116)';
        taCenterReport1.Fields.Clear;

        taCenterReport1.SelectSQL.Text:= 'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDOC, '+
                                        'cost2, cost3, buhrep, depsel, color, sn, dep$name from ShopReport_Com_S2(:SINVID) where t in (102, 111)';

        taCenterReport1.ParamByName('SINVID').AsInteger := arr[0];
        taCenterReport.ParamByName('SINVID').AsInteger := arr[0];
        quCenterIncoming.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogIn.ParamByName('SINVID').AsInteger := arr[0];
        quCenterOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItogOut.ParamByName('SINVID').AsInteger := arr[0];
        quCenterItog.ParamByName('SINVID').AsInteger := arr[0];
        quCenterDefferent.ParamByName('SINVID').AsInteger := arr[0];

        OpenDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                      quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);

        ShowDoc;

        CloseDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                       quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);
     end;


     center_report_tol:  begin

        LoadDoc(1217);

        taCenterReport.Fields.Clear;
        taCenterReport.SelectSQL.Text := 'select i.SDATE REPORTBD, i.NDATE REPORTED, i.SN REPORTNO from SInv i where sinvid = :invoice$id';

        // ��������
        quCenterIncoming.Fields.Clear;
        quCenterIncoming.SelectSQL.Text := 'select * from SHOP$REPORT$S2(:invoice$id) where t = 0';

        // ������
        taCenterReport1.fields.Clear;
        taCenterReport1.SelectSQL.Text   := 'select * from SHOP$REPORT$S2(:invoice$id) where t = -1 or k = 1';

        // ����� �� ������� + ����� � ��������
        quCenterItogIn.Fields.Clear;
        quCenterItogIn.SelectSQL.Text   := 'select * from SHOP$REPORT$S2(:invoice$id) where t in (-2, -3)';

        // ������
        quCenterOut.Fields.Clear;
        quCenterOut.SelectSQL.Text  := 'select * from SHOP$REPORT$S2(:invoice$id) where t = -4 or k = -1 or t = 301 or t = 202';

         // ����� �� �������
        quCenterItogOut.Fields.Clear;
        quCenterItogOut.SelectSQL.Text  := 'select * from SHOP$REPORT$S2(:invoice$id) where t = -5';

        // ���������
        quCenterItog.Fields.Clear;
        quCenterItog.SelectSQL.Text     := 'select * from SHOP$REPORT$S2(:invoice$id) where t = 999';


        taCenterReport.ParamByName('invoice$id').AsInteger := arr[0];

        taCenterReport1.ParamByName('invoice$id').AsInteger := arr[0];
        quCenterIncoming.ParamByName('invoice$id').AsInteger := arr[0];
        quCenterItogIn.ParamByName('invoice$id').AsInteger := arr[0];
        quCenterOut.ParamByName('invoice$id').AsInteger := arr[0];
        quCenterItogOut.ParamByName('invoice$id').AsInteger := arr[0];
        quCenterItog.ParamByName('invoice$id').AsInteger := arr[0];


        OpenDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                      quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);

        ShowDoc;

        CloseDataSets([taCenterReport1, taCenterReport, quCenterIncoming, quCenterItogIn, quCenterOut,
                       quCenterItogOut, quCenterItog, quCenterDefferent, ibdsUID_Store]);
     end;

     report_sel_emp: begin
        LoadDoc(1004);
        frReport.OnGetValue := Seller_Emp_GetValue;
        frAnlzSelEmp.ParamByName('userid').AsInteger := arr[0];
        OpenDataSets([frAnlzSelEmp]);
        ShowDoc;
        CloseDataSets([frAnlzSelEmp]);
     end;

   end;
  finally
    Screen.Cursor := crDefault;
    trReport.Commit;
  end;
end;

procedure PrintTag(const arr : TarrDoc; Kind : TKindTag; IsOld :  integer;
                   IsDep : boolean = false);
var user:integer;

 function RoundMy(r:real):integer;
  begin
   if frac(r)>1e-7 then result:=trunc(r)+1 else result:=trunc(r);
  end;

  procedure InitBounds;
  //�� ������ �� �������� ������ 8 ������ / 7 ����� ����� , �� ������ 8 ����� /4 �� ���������
  var
    i ,nt, t: integer;
  begin
    nt := 0;
    with dmReport do
    begin
      if ibdsTagWI.RecordCount = 0 then
        begin
         if (IsOld=2)or(IsOld=3)or(IsOld=4)or(IsOld=5)or(IsOld=6)  then FColCount := 4
         else FColCount := 8;
         FNoTagWI:=0;
         OstTagWI:=ibdsTagWOI.RecordCount;
         IsFlagNew:=true;
        end
      else begin
         case IsOld of
         0: nt:=NoTagNew;
         1: nt:=NoTag;
         2: begin nt:=NoTag; IsFlagNew:=true end;
         3: begin FColCount := 4; FNoTagWI:=0;
                  OstTagWI:=ibdsTagWOI.RecordCount+ibdsTagWI.RecordCount;
                  IsFlagNew:=true;
            end;
         4: begin FColCount := 4; FNoTagWI:=0;
                  OstTagWI:=ibdsTagWOI.RecordCount+ibdsTagWI.RecordCount;
                  IsFlagNew:=true;
            end;
         5: begin FColCount := 4; FNoTagWI:=0;
                  OstTagWI:=ibdsTagWOI.RecordCount+ibdsTagWI.RecordCount;
                  IsFlagNew:=true;
            end;
         6: begin FColCount := 4; FNoTagWI:=0;
                  OstTagWI:=ibdsTagWOI.RecordCount+ibdsTagWI.RecordCount;
                  IsFlagNew:=true;
            end
         end;
//        FNoTagWI := NoTag - ibdsTagWI.RecordCount mod NoTag + NoTag - 1;
{
             FNoTagWI:= NT - ibdsTagWI.RecordCount mod NT + NT - 1;//nt*4-ibdsTagWI.RecordCount;
             FColCount:= RoundMy(ibdsTagWI.RecordCount / NT)+7-(2*RoundMy(ibdsTagWI.RecordCount / NT));
             OstTagWI:=NT*(FColCount - RoundMy(ibdsTagWI.RecordCount / NT));
             }


      if (IsOLd<>3) and (IsOLd<>4) and (IsOLd<>5) and (IsOLd<>6) then
        begin
         if (IsOld=2)or(IsOld=3) then t:=ibdsTagWI.RecordCount mod (NT*2)
         else t:=ibdsTagWI.RecordCount mod (NT*4);
{
         t:=ibdsTagWI.RecordCount-NT*4;//���-�� ������ ������� �� ���������
         if t<0 then t:=ibdsTagWI.RecordCount//�� ��������� ������ 1-�� �����
         else begin nt:=6;
                    t:= t mod(nt*4);//������� ���-�� ����� �� ��������� ����� �� ���������
              end;
          }
         if (t mod NT =0) then FNoTagWI:=(t div NT)
         else
         FNoTagWI:= NT - t mod NT + NT-1;//nt*4-ibdsTagWI.RecordCount;
         if (IsOld=2)or(IsOld=3) or(IsOld=4)or(IsOld=5)or(IsOld=6) then FColCount:= 4-RoundMy(t / NT)
                                 else FColCount:= 8-RoundMy(t / NT);
          OstTagWI:=NT*(FColCount - RoundMy(t / NT));
      //  FNoTagWI := NT - ibdsTagWI.RecordCount mod NT + NT - 1;
//        SaveFNoTagWI:=FNoTagWI;
//        FColCount := 7; //- RoundMy((ibdsTagWI.RecordCount-4*NT*(ibdsTagWI.RecordCount div (4*NT)))/nt)*2//7;
        end
      end;
      SaveFNoTagWI:=FNoTagWI;
      ibdsTagWI.First;
      for i := 0 to Pred(frReport.Pages.Count) do
      begin
       if IsFlagNew then
         begin frReport.Pages[i].ColCount := 2; frReport.Pages[i].ColWidth :=264; end
       else frReport.Pages[i].ColCount := 4;
        frReport.Pages[i].ColGap := 0;
      end;
    end;
  end;

  procedure PrepareTag(Kind : integer);
  var
    i : integer;
  begin
    with dmReport do
    begin
      CloseDataSets([ibdsTagWI, ibdsTagWOI, ibdsIns]);
{      if IsDep then
      begin
        ibdsTagWI.SelectSQL.Text := gen_Query(arr, 'select R_COMP, R_MAT, R_GOOD, R_COUNTRY,'+
              'R_ART, R_ART2, R_SIZE, R_W, R_PRICE, R_UID, R_UNITID, R_LINK, '+
              'R_INS, R_DEPID, R_DEP, R_DEPNAME, R_CITY, R_ADDRESS, R_ISINS, '+
              'R_SITEMID, R_ISNEWPRICE, R_Date from Tag(', ') where R_IsIns=1 and R_DepId=:L_DepId ',
              [':P_Sn', ':P_Kind'])+'order by 5,7 ';//R_ART, R_SIZE'; //4';
        ibdsTagWOI.SelectSQL.Text := gen_Query(arr, 'select R_COMP, R_MAT, R_GOOD, R_COUNTRY, '+
              'R_ART, R_ART2, R_SIZE, R_W, R_PRICE, R_UID, R_UNITID, R_LINK, '+
              'R_INS, R_DEPID, R_DEP, R_DEPNAME, R_CITY, R_ADDRESS, R_ISINS, '+
              'R_SITEMID, R_ISNEWPRICE, R_Date from Tag(', ') where R_IsIns=-1 and R_DepId=:L_DepId ',
              [':P_Sn', ':P_Kind'])+'order by 5,7 ';//R_ART, R_SIZE'; //4';
      end
      else begin
        ibdsTagWI.SelectSQL.Text := gen_Query(arr, 'select R_COMP, R_MAT, R_GOOD, R_COUNTRY,'+
              'R_ART, R_ART2, R_SIZE, R_W, R_PRICE, R_UID, R_UNITID, R_LINK, '+
              'R_INS, R_DEPID, R_DEP, R_DEPNAME, R_CITY, R_ADDRESS, R_ISINS, '+
              'R_SITEMID, R_ISNEWPRICE, R_Date from Tag(', ') where R_IsIns=1', [':P_Sn', ':P_Kind'])+
              'order by 5,7 ';//R_ART, R_SIZE'; //4';
        ibdsTagWOI.SelectSQL.Text := gen_Query(arr, 'select R_COMP, R_MAT, R_GOOD, R_COUNTRY,'+
              'R_ART, R_ART2, R_SIZE, R_W, R_PRICE, R_UID, R_UNITID, R_LINK, '+
              'R_INS, R_DEPID, R_DEP, R_DEPNAME, R_CITY, R_ADDRESS, R_ISINS, '+
              'R_SITEMID, R_ISNEWPRICE, R_Date from Tag(', ') where R_IsIns=-1', [':P_Sn', ':P_Kind'])+
              'order by 5,7 ';//R_ART, R_SIZE'; //4';
      end;}
      for i:=Low(arr) to High(arr) do
      begin
        ExecSQL('execute procedure  TAG( '+inttostr(arr[i])+', '+inttostr(Kind)+
                ', '+inttostr(user)+')', quTmp);
       {ibdsTagWI.Params[2*i].AsInteger := arr[i];
        ibdsTagWI.Params[2*i+1].AsInteger := Kind;
        ibdsTagWOI.Params[2*i].AsInteger := arr[i];
        ibdsTagWOI.Params[2*i+1].AsInteger := Kind;}
      end;

      ibdsTagWI.SelectSQL.Text := 'select t.R_COMP, t.R_MAT, t.R_GOOD, t.R_COUNTRY,'+
             't.R_ART, t.R_ART2 EART2, t.R_SIZE, t.R_W, t.R_PRICE, t.R_UID, t.R_UNITID, t.R_LINK, '+
             't.R_INS, t.R_DEPID, t.R_DEP, t.R_DEPNAME, t.R_CITY, t.R_ADDRESS, t.R_ISINS, '+
             't.R_SITEMID, t.R_ISNEWPRICE, t.R_Date, u.flag, u.flag$char, u.euid from tmp_tag t left join uid_info u on t.r_uid = u.uid where t.R_IsIns=1 and t.userid ='+ inttostr(user)+
             ' order by t.R_ART, t.R_SIZE, t.R_UID';
      ibdsTagWOI.SelectSQL.Text :=  'select t.R_COMP, t.R_MAT, t.R_GOOD, t.R_COUNTRY,'+
             't.R_ART, t.R_ART2 EART2, t.R_SIZE, t.R_W, t.R_PRICE, t.R_UID, t.R_UNITID, t.R_LINK, '+
             't.R_INS, t.R_DEPID, t.R_DEP, t.R_DEPNAME, t.R_CITY, t.R_ADDRESS, t.R_ISINS, '+
             't.R_SITEMID, t.R_ISNEWPRICE, t.R_Date, u.flag, u.flag$char, u.euid from tmp_tag t left join uid_info u on t.r_uid = u.uid where t.R_IsIns=-1 and t.userid ='+ inttostr(user)+
             ' order by t.R_ART, t.R_SIZE, t.R_UID';
    end;
  end;

begin
  try
{ TODO : PrintTag }
  Screen.Cursor := crSQLWait;
  SAction:='';
  with dmReport do
  try
    if IsDep then
    begin
      if IsOld=1 then LoadDoc(30)
                 else LoadDoc(130); // tagdep
      ibdsTagWI.DataSource := dsrDep;
      ibdsTagWOI.DataSource := dsrDep;
      OpenDataSets([ibdsDep]);
    end
    else
    case IsOld of
    0: begin WidthTagNew:=WidthTag; LoadDoc(131) end;
    1: begin WidthTagNew:=WidthTag; LoadDoc(31) end;
    2: begin WidthTagNew:=0; LoadDoc(231) end;
    3: begin WidthTagNew:=3{WidthTag+2}; LoadDoc(332) end;
    4: begin if InputQuery('����� ���������','������� ��������� �� �����. ������ ��������� ����� ������� �� ���� ��������� ������',SAction)then
     begin
       WidthTagNew:=3{WidthTag+2}; LoadDoc(2000) end
     else begin
     trReport.Active:=true;
    exit;
     end;
    end;
    5: begin if InputQuery('������','������� ������ � %. ������ ������ ����� ������� �� ���� ��������� ������', SAction) then   begin
       WidthTagNew:=3{WidthTag+2}; LoadDoc(2000) end
     else begin
     trReport.Active:=true;
    exit;
     end;
    end;
    6: begin if InputQuery('���� �� ����','������� ���� �� ��. ������ ���� �� ��. ����� ������� �� ���� ��������� ������', SAction)then
      begin
       WidthTagNew:=3{WidthTag+2}; LoadDoc(2000) end
     else begin
     trReport.Active:=true;
    exit;
     end;
    end;
    end;
   typeprint:=IsOld;//�������� � ���������� ���������� ����� ������������ ��� ������ ����������

    IsFlagNew:=false;
    user := dmcom.GetID(44);

    frReport.OnGetValue := TagGetValue;
    if not trReport.Active then trReport.StartTransaction
    else trReport.CommitRetaining;
    case Kind of
      f_tag   : PrepareTag(0);
      act_tag : PrepareTag(2);
      s_tag   : PrepareTag(4);
      it_tag,
      ret_tag : PrepareTag(5);
    end;
    OpenDataSets([dmreport.ibdsTagWI, dmreport.ibdsTagWOI, dmreport.ibdsIns]);

    dmreport.ibdsTagWI.Last;
    if not IsDep then
    begin
      dmreport.ibdsTagWOI.Last;
      if (dmreport.ibdsTagWI.RecordCount <= 0)and(dmreport.ibdsTagWOI.RecordCount <= 0)
      then raise Exception.Create('��� ����� ��� ������!!!');
      dmreport.ibdsTagWOI.First;
    end;
    if isold<>3 then InitBounds;
    dmreport.ibdsTagWI.First;
    ShowDoc;
  finally
    Screen.Cursor := crDefault;
    trReport.Commit;
    CloseDataSets([ibdsTagWI, ibdsTagWOI, ibdsIns]);
    ExecSQL('delete from tmp_tag where userid='+inttostr(user), quTmp);
  end;

  except
     on E: Exception do
     begin
       TDialog.Error(E.Message);
     end;
  end;
end;


{procedure PrintWHTag(const arr : TarrDoc; Kind : TKindTag);overload;
begin
  with dm do
  begin
    quTmp.SQL.Text := 'execute procedure IsOpenWH('+IntToStr(arr[0])+');';
    quTmp.ExecQuery;
    if quTmp.Fields[0].AsInteger = 1 then PrintTag(arr, kind,1);
  end;
end;}

{ TdmReport }




procedure TdmReport.UidWhInv_GetValue(const ParName: String;
  var ParValue: Variant);
begin
  if (parName='�����') then ParValue := GetAssay(PChar(quUidWhArtMAT.AsString));
  if (parName='��. ���.') then
    if quUidWhArtUNITID.AsInteger=1 then ParValue := '��.'
    else ParValue := '��.';

end;

procedure TdmReport.TagGetValue(const ParName: String;
  var ParValue: Variant);
var
  i1, n : word;
begin
{ TODO : TagGetValue }
 n := 0;
if (parName = '���������1') then//'������1') then
    for i1 :=0 to Pred(frReport.Pages.Count) do
    begin
       if IsFlagNew then
        begin
         frReport.Pages[i1].ColCount := 2;
         frReport.Pages[i1].ColWidth :=264;
        end
       else frReport.Pages[i1].ColCount := 4;
      frReport.Pages[i1].ColGap := 0;
      n := i1;
    end;

  if (parName = '���������2') then //'������2') then
  begin
    if IsFlagNew then frReport.Pages[n].ColWidth :=171;
    dec(FNoTagWI);
    if (FNoTagWI < 0) then
    for n := 0 to Pred(frReport.Pages.Count) do
    begin
      if abs(FNoTagWI) < OstTagWI then frReport.Pages[n].ColCount := FColCount
      else if IsFlagNew then frReport.Pages[n].ColCount := 4
           else frReport.Pages[n].ColCount := 8;{ TODO : ���� 7 [����] }
      frReport.Pages[n].ColGap := -WidthTagNew + 3;
    end;
  end;


  if (parName = '������1') then
    if (ibdsTagWIR_SIZE.Value='-') then ParValue := ''
    else ParValue := '�.'+ibdsTagWIR_SIZE.Value;
  if (parName = '������2') then
    if (ibdsTagWOIR_SIZE.Value='') then ParValue := ''
    else ParValue := '�.'+ibdsTagWOIR_SIZE.Value;
  if (parName = '�����1') then ParValue := ibdsTagWIR_ADDRESS.Value;
  if (parName = '�����2') then ParValue := ibdsTagWOIR_ADDRESS.Value;
  if (parName = '�����1')then ParValue := trim(GetAssay(PChar(ibdsTagWIR_MAT.Value)))+chr(176);
//  if (parName = '�����2')then ParValue := trim(GetAssay(PChar(ibdsTagWOIR_MAT.Value)))+chr(176);
  if (parName = '�����1')then ParValue := GetAssay_a(ibdsTagWIR_MAT.Value)+chr(176);
  if (parName = '�����2')then ParValue := GetAssay_a(ibdsTagWOIR_MAT.Value)+chr(176);

  if (ParName = '���� �� ��.1')then
    if (ibdsTagWIR_UnitId.AsInteger = 0)
    then ParValue := FloatToStr(ibdsTagWIR_W.Value)+ ''//FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���'
    else ParValue := FloatToStr(ibdsTagWIR_W.Value)+ '*' + FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���';


  if (ParName = '���� �� ��.2')then
    if (ibdsTagWOIR_UnitId.AsInteger = 0)
    then ParValue := FloatToStr(ibdsTagWOIR_W.Value)+ ''//FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���'
    else ParValue := FloatToStr(ibdsTagWOIR_W.Value) + '*' + FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���';
  if (ParName = '���������1') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWIR_W.Value*ibdsTagWIR_Price.Value)+'���';
  if (ParName = '���������2') then
    if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWOIR_W.Value*ibdsTagWOIR_Price.Value)+'���';
  if (ParName = '���������1_') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWIR_W.Value*ibdsTagWIR_Price.Value)+'���';
  if (ParName = '���������2_') then
    if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWOIR_W.Value*ibdsTagWOIR_Price.Value)+'���';
  if (ParName = '���������3') then
    if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWOIR_W.Value*ibdsTagWOIR_Price.Value)+'���';
  if (ParName = '���������4') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���'
    else ParValue := FormatFloat('0.00',ibdsTagWIR_W.Value*ibdsTagWIR_Price.Value)+'���';


    if( typeprint=4 )then
    Begin
    if (ParName = '��������������') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',strtofloat(SAction))+'���'
    else ParValue := FormatFloat('0.00',strtofloat(SAction))+'���';

    if (ParName = '��������������2') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',strtofloat(SAction))+'���'
    else ParValue := FormatFloat('0.00',strtofloat(SAction))+'���';
    End;
    if( typeprint=5 )then
    Begin
       if (ParName = '��������������') then
         if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',((ibdsTagWOIR_Price.Value*(100-strtoInt(SAction))/100)))+'���'
          else ParValue := FormatFloat('0.00',(ibdsTagWOIR_W.Value*ibdsTagWOIR_Price.Value*(100-strtoInt(SAction))/100))+'���';

       if (ParName = '��������������2') then
         if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',((ibdsTagWIR_Price.Value*(100-strtoInt(SAction))/100)))+'���'
          else ParValue := FormatFloat('0.00',(ibdsTagWIR_W.Value*ibdsTagWIR_Price.Value*(100-strtoInt(SAction))/100))+'���';

    End;


    if( typeprint=6 )then
    Begin

    if (ParName = '���� �� ��.1')then
    if (ibdsTagWIR_UnitId.AsInteger = 0)
    then ParValue := FloatToStr(ibdsTagWIR_W.Value)+ ''//FormatFloat('0.00',ibdsTagWIR_Price.Value)+'���'
    else ParValue := FloatToStr(ibdsTagWIR_W.Value)+ '*' + FormatFloat('0.00',strtofloat(SAction))+'���';

    if (ParName = '���� �� ��.2')then
    if (ibdsTagWOIR_UnitId.AsInteger = 0)
    then ParValue := FloatToStr(ibdsTagWOIR_W.Value)+ ''//FormatFloat('0.00',ibdsTagWOIR_Price.Value)+'���'
    else ParValue := FloatToStr(ibdsTagWOIR_W.Value) + '*' + FormatFloat('0.00',strtofloat(SAction))+'���';

       if (ParName = '��������������') then
         if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',strtofloat(SAction))+'���'
          else ParValue := FormatFloat('0.00',(ibdsTagWOIR_W.Value*strtofloat(SAction)))+'���';

       if (ParName = '��������������2') then
         if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',strtofloat(SAction))+'���'
          else ParValue := FormatFloat('0.00',(ibdsTagWIR_W.Value*strtofloat(SAction)))+'���';

    End;

//
  if (ParName = '���������1_') then
    if ibdsTagWIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWIR_Price.Value)
    else ParValue := FormatFloat('0.00',ibdsTagWIR_W.Value*ibdsTagWIR_Price.Value);
  if (ParName = '���������2_') then
    if ibdsTagWOIR_UNITID.Value = 0 then ParValue := FormatFloat('0.00',ibdsTagWOIR_Price.Value)
    else ParValue := FormatFloat('0.00',ibdsTagWOIR_W.Value*ibdsTagWOIR_Price.Value);

  if (ParName = '�����')then ParValue := FDepName;
  if (ParName = '�������')then
  begin
    ParValue := '';
    ibdsIns.First;
    while not ibdsIns.Eof do
    begin   (*������� ���������� *)
      ParValue := ParValue + ibdsInsR_INSCODE.Value; //��. �������������� �������
      if ibdsInsR_Q.Value <> 0 then ParValue := ParValue+' '+IntToStr(ibdsInsR_Q.Value)
      else ParValue := ParValue + '  '; // ���-��

      if (ibdsInsR_ESHAPE.Value <> '')then
         ParValue := ParValue+' '+ibdsInsR_EShape.Value; //��� �������

      if (ibdsInsR_ETYPE.Value <> '')then
         ParValue := ParValue+' '+ibdsInsR_ETYPE.Value; //����� �������

      if ibdsInsR_W.Value <> 0 then ParValue := ParValue+' '+FloatToStr(ibdsInsR_W.Value)//FormatFloat('0.00', ibdsInsR_W.Value)
      else ParValue := ParValue + '    '; // ���
      if (ibdsInsR_CHROMATICITY.Value <> '')and(ibdsInsR_CLEANNES.Value <> '')then
         ParValue := ParValue+' '+ibdsInsR_CHROMATICITY.Value+'/'+ibdsInsR_CLEANNES.Value+' '+
                     ibdsInsR_GROUP.Value //���������/? ������
      else
        if (ibdsInsR_CHROMATICITY.Value <> '') then
          ParValue := ParValue+' '+ibdsInsR_CHROMATICITY.Value;
      ParValue := ParValue+ #13;
      ibdsIns.Next;
    end;
    ibdsIns.First;

  end;


end;

procedure TdmReport.FactureGetValue(const ParName: String; var ParValue: Variant);
begin
  if (ParName = '��. ���.')then ParValue := GetUnit(ibdsBillItemR_UNITID.Value);
  if (ParName = '��. ���.2')then ParValue := GetUnit(ibdsBillUItemR_UNITID.Value);
  if (ParName = '�����')then ParValue := GetAssay(PChar(ibdsBillItemR_Mat.Value));
  if (ParName = '�����2') then ParValue := GetAssay(PChar(ibdsBillUItemR_Mat.Value));
end;

procedure TdmReport.OrderGetValue(const ParName: String; var ParValue: Variant);
begin
  if (parName = '�����')then ParValue := GetAssay(PChar(ibdsOrderItemR_MAT.Value));
  if (ParName = '��. ���.')then ParValue := GetUnit(ibdsOrderItemR_UNITID.Value);
  if (ParName = '���� ��������� ���')then ParValue := GetDateInGenetiveCase(ibdsOrderR_SETDATE.AsDateTime);
  if (ParName = '�����1') then ParValue := GetAssay(PChar(ibdsReduceItemR_ASSAY.Value));
  if (ParName = '���� ��������� ���1') then ParValue := GetDateInGenetiveCase(ibdsReduceR_DATE.AsDateTime);
end;

procedure TdmReport.ReduceGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if(ParName = '�����') then ParValue := GetAssay(PChar(ibdsReduceItemR_ASSAY.Value));
  if(ParName = '���� ��������� ���')then ParValue := GetDateInGenetiveCase(ibdsReduceR_DATE.AsDateTime);
end;

procedure TdmReport.Act_OVGetValue(const ParName: String; var ParValue: Variant);
var d: double;
    FNullCost:boolean;
begin
  with ibdsActOVItem do
  begin
    if (ParName= 'ibdsActOVItem."PRICE2"'{' ����'}) then
      begin
        if FieldByName('PRICE2').IsNull then d:=FieldByName('OLDP2').AsFloat
        else  d:=FieldByName('PRICE2').AsFloat;
        ParValue:=d;
      end;

    if(ParName = ' ����� ��') then
    begin
      if not FieldByName('OLDCOST').IsNull then ParValue := FieldByName('OLDCOST').AsFloat
      else
      begin
       if FieldByName('ITYPE').AsInteger = 11 then
       begin
        if FieldByName('R_UNITID').AsInteger = 1
        then ParValue := FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat
        else ParValue := FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat
       end
       else
       begin
        if FieldByName('R_UNITID').AsInteger = 1
        then ParValue := FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat
        else ParValue := FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat;
       end
      end
    end;

    if (ParName = ' ����� �����') then
      begin
       if not FieldByName('NEWCOST').IsNull then ParValue := FieldByName('NEWCOST').AsFloat
       else begin
        if FieldByName('PRICE2').IsNull then d:=FieldByName('OLDP2').AsFloat
        else  d:=FieldByName('PRICE2').AsFloat;

        if FieldByName('R_UNITID').AsInteger = 1
        then d := d * FieldByName('RW').AsFloat
        else d := d * FieldByName('RQ').AsFloat;

        ParValue:=d;
       end
      end;
    if (ParName = '�������') then
      begin
        if (not FieldByName('NEWCOST').IsNull) and (not FieldByName('OLDCOST').IsNull) then
         ParValue := FieldByName('NEWCOST').AsFloat-FieldByName('OLDCOST').AsFloat
        else begin
         if FieldByName('PRICE2').IsNull then d:=FieldByName('OLDP2').AsFloat
         else  d:=FieldByName('PRICE2').AsFloat;

         if FieldByName('ITYPE').AsInteger = 11 then
         begin
          if FieldByName('R_UNITID').AsInteger = 1
          then ParValue := (d * FieldByName('RW').AsFloat)-
                           (FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat)
          else ParValue := (d * FieldByName('RQ').AsFloat) -
                           (FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat)
         end
         else
         begin
          if FieldByName('R_UNITID').AsInteger = 1
          then ParValue := (d * FieldByName('RW').AsFloat) -
                           (FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat)
          else ParValue := (d * FieldByName('RQ').AsFloat)-
                           (FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat);
         end
        end 
      end;
    if (ParName = '����� ����� ��')then
    begin
      FNullCost:=false;
      first;
      while not Eof do
      begin
       if FieldByName('OLDCOST').IsNull then FNullCost:=true;
       Next;
      end;

      First;
      ParValue := 0;
      if not FNullCost then
       while not Eof do
       begin
        ParValue := ParValue + FieldByName('OLDCOST').AsFloat;
        Next;
       end
      else begin
       if FieldByName('ITYPE').AsInteger = 11 then
        while not Eof do
        begin
          if FieldByName('R_UNITID').AsInteger = 1
          then ParValue := ParValue + FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat
          else ParValue := ParValue + FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat;
          Next;
        end
       else
        while not Eof do
        begin
          if FieldByName('R_UNITID').AsInteger = 1
          then ParValue := ParValue + FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat
          else ParValue := ParValue + FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat;
          Next;
        end;
      end;
    end;

    if (ParName = '����� ����� �����')then
    begin
      FNullCost:=false;
      first;
      while not Eof do
      begin
       if FieldByName('NEWCOST').IsNull then FNullCost:=true;
       Next;
      end;

      First;
      ParValue := 0;
      if not FNullCost then
       while not Eof do
       begin
        ParValue := ParValue + FieldByName('NEWCOST').AsFloat;
        Next;
       end
      else
       while not Eof do
       begin
        if FieldByName('PRICE2').IsNull then d:=FieldByName('OLDP2').AsFloat
        else  d:=FieldByName('PRICE2').AsFloat;

        if FieldByName('R_UNITID').AsInteger = 1
        then ParValue := ParValue + d * FieldByName('RW').AsFloat
        else ParValue := ParValue + d * FieldByName('RQ').AsFloat;
        Next;
       end;
    end;

    if (ParName = '����� �������')then
    begin
      FNullCost:=false;
      first;
      while not Eof do
      begin
       if FieldByName('NEWCOST').IsNull or FieldByName('OLDCOST').IsNull then FNullCost:=true;
       Next;
      end;

      First;
      ParValue := 0;
      if not FNullCost then
       while not Eof do
       begin
        ParValue := ParValue + (FieldByName('NEWCOST').AsFloat - FieldByName('OLDCOST').AsFloat);
        Next;
       end
      else
       while not Eof do
       begin
        if FieldByName('PRICE2').IsNull then d:=FieldByName('OLDP2').AsFloat
        else  d:=FieldByName('PRICE2').AsFloat;


        if FieldByName('ITYPE').AsInteger = 11 then
        begin
         if FieldByName('R_UNITID').AsInteger = 1
         then ParValue := (d * FieldByName('RW').AsFloat)-
                          (FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat)
         else ParValue := (d * FieldByName('RQ').AsFloat) -
                          (FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat)
        end
        else
        begin
         if FieldByName('R_UNITID').AsInteger = 1
         then ParValue := ParValue + (d * FieldByName('RW').AsFloat) -
                          (FieldByName('OLDP2').AsFloat * FieldByName('RW').AsFloat)
         else ParValue := ParValue + (d * FieldByName('RQ').AsFloat)-
                          (FieldByName('OLDP2').AsFloat * FieldByName('RQ').AsFloat);
        end;
        Next;
       end;
    end;
  end;
end;

(*  -----  *)
procedure Init;
var
  Ini : TIniFile;

begin
  Ini := TIniFile.Create(GetIniFileName);
  try
    // DocPreview := TdcPreview(Ini.ReadInteger('Report', 'Prv', 0));

    IsReCalc := Bool(Ini.ReadInteger('Sum', 'IsReCalc', 0));
  finally
    Ini.Free;
  end;
end;

procedure TdmReport.ApplGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if (ParName = '�������� ���')then
    if not ibdsApplCHECKNO.IsNull then ParValue := '�������� ����� �'+ibdsApplCHECKNO.AsString
    else ParValue := '';
  if(ParName = '�����')then
  begin
    ParValue := 0;
    ibdsAppl.First;
    while not ibdsAppl.Eof do
    begin
      ParValue := ParValue + ibdsApplCOST.AsFloat;
      ibdsAppl.Next;
    end;
  end;
  if(ParName = '�����1')then
  begin
    ParValue := 0;
    ibdsApplResp.First;
    while not ibdsApplResp.Eof do
    begin
      ParValue := ParValue + ibdsApplRespCOST.AsFloat;
      ibdsApplResp.Next;
    end;
  end;
end;



procedure TdmReport.DisvGetValue(const ParName: String; var ParValue: Variant);
begin
  if (ParName = '�����')then
    ParValue := GetCapitalsMoney(ibdsDisvCOST.Value);
end;

procedure TdmReport.TicketGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if (ParName = '��.���.') then
    if ibdsTicketItR_UNITID.AsInteger = 0 then ParValue := '��.'
    else ParValue := '��.';
  if(ParName = '�����')then ParValue := GetAssay(PChar(ibdsTicketItR_Mat.Value));
  if(ParName = '����')then
  if (ibdsTicketItR_Ret.asInteger=1)then ParValue :='�' else ParValue :='';
  if(ParName = '��������')then ParValue := GetFirstWord(ibdsTicketItR_Mat.AsString);
  if(ParName = '������')then ParValue := Discount[CurrDisc];
  if(ParName = '����� �� �������')then
    ParValue := ibdsTicketItR_COST.AsFloat - (ibdsTicketItR_COST.AsFloat/Discount[CurrDisc]);
  if(ParName = '������ ���')then
   if ibdsTicketItR_DISCOUNT.AsFloat<>0 then ParValue:= trim(ibdsTicketItR_DISCOUNT.AsString)+'%'
   else ParValue:=''
end;

procedure TdmReport.comissionActGetValue(const ParName: string; var ParValue: Variant);
begin

  if ParName = '�����' then ParValue := comissionActSumm;

  if ParName = '��������' then ParValue := GetCapitalsMoney(comissionActSumm);

end;

procedure TdmReport.BillGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if (ParName = '��. ���.')then ParValue := GetUnit(ibdsSBillItemR_UNITID.Value);
  if (ParName = '�����')then ParValue := GetAssay(PChar(ibdsSBillItemR_Mat.Value));
end;

procedure TdmReport.LayoutGetValue(const ParName: String;
  var ParValue: Variant);
var
  ind : integer;
begin
  FInitDep := true;
  if(ParName = '�����')then ParValue := dm.SDep;
  if(ParName = '������')then ParValue := FilterText;
  if(ParName = '�������') then
  begin
   if (dm.AllUIDWH) then ParValue := '���'
   else
    if (dm.lSellSelect) then ParValue := '�������'
        else ParValue := '';
  end;
  if (ParName = '����� ���')then
  if dm.SDepId = -1 then ParValue := ibdsWhArtWEIGTH.AsFloat
    else ParValue := '';//dm.getCommaBlob(ibdsWHArtTW.AsString, dm.getPosInArr(dm.arrWH, dm.SDep)+1);
  if (ParName = '����� ���-��')then
    if dm.SDepId = -1 then ParValue := ibdsWhArtQUANTITY.AsFloat
    else ParValue := '';//dm.getCommaBlob(ibdsWHArtTQ.AsString, dm.getPosInArr(dm.arrWH, dm.SDep)+1);
  if(ParName='��������� �������')then
    if taUIDWHAUNITID.AsInteger = 0 then parValue := taUIDWHAPRICE.Value*taUIDWHAQ.Value//parValue := taUIDWHAPRICE.Value
    else ParValue := taUIDWHAPRICE.Value*taUIDWHAW.Value;
  if(ParName='���������� �������')then
    if taUIDWHAUNITID.AsInteger = 0 then parValue := taUIDWHASPRICE.Value*taUIDWHAQ.Value//parValue := taUIDWHAPRICE.Value
    else ParValue := taUIDWHASPRICE.Value*taUIDWHAW.Value;

  //if(ParName = '���-��')then ParValue := ibdsWhArt2Q.AsFloat;
  //if(ParName = '���')then ParValue := ibdsWhArt2W.AsFloat;
  //if(ParName = '���� ������.')then ParValue := ibdsWHArt2R_Price2.AsString;
  //if(ParName = '0')then ParValue := dm.arrWH[0];
  //if(ParName = '1')then ParValue := dm.arrWH[1];
  //if(ParName = '2')then ParValue := dm.arrWH[2];
  ind := Pos('���', ParName);
  if ind <> 0 then
    case StrToInt(Copy(ParName, ind+3, Length(ParName))) of
      0 :; //ParValue := dm.getCommaBlob(ibdsWHArt2R_TW.AsString, dm.getPosInArr(dm.arrWH, dm.arrWH[0])+1);
      1 :; //ParValue := dm.getCommaBlob(ibdsWHArt2R_TW.AsString, dm.getPosInArr(dm.arrWH, dm.arrWH[1])+1);
      2 :; //ParValue := dm.getCommaBlob(ibdsWHArt2R_TW.AsString, dm.getPosInArr(dm.arrWH, dm.arrWH[2])+1);
    end;
  if(ParName = '�������')then
  begin
    quTmp.Close;
    quTmp.SQL.Text := 'select Name from D_Dep where D_DepId='+IntToStr(dmServ.WhDepId);
    quTmp.ExecQuery;
    ParValue := quTmp.Fields[0].AsString;
    quTmp.Close;
  end;
  if(ParName='�����������')then
  begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select dc.name from d_comp dc, d_rec dr where dc.d_compid=dc.d_compid';
    dm.quTmp.ExecQuery;
    parValue := trim(dm.quTmp.Fields[0].AsString);
    dm.quTmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
  end;
  if(ParName='����� �����������')then
  begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select dc.address, dc.city from d_comp dc, d_rec dr where dc.d_compid=dc.d_compid';
    dm.quTmp.ExecQuery;
    parValue := '�. '+trim(dm.quTmp.Fields[1].AsString)+'  '+trim(dm.quTmp.Fields[0].AsString);
    dm.quTmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
  end;
  if(ParName='������ ��������')then
    parValue := ' c '+DateTimeToStr(dm.UIDWHBD)+' �� ' + DateToStr(dm.UIDWHED);
end;

procedure TdmReport.LayoutGetValue2(const ParName: String;
  var ParValue: Variant);
var
  ind : integer;
begin
  FInitDep := true;
  if(ParName = '�����')then ParValue := dm.SDep;
  if(ParName = '������')then ParValue := FilterText;
  if(ParName = '�������') then
  begin
   if (dm.AllUIDWH) then ParValue := '���'
   else
    if (dm.lSellSelect) then ParValue := '�������'
        else ParValue := '';
  end;
  if (ParName = '����� ���')then
  if dm.SDepId = -1 then ParValue := ibdsWhArtWEIGTH.AsFloat
    else ParValue := '';//dm.getCommaBlob(ibdsWHArtTW.AsString, dm.getPosInArr(dm.arrWH, dm.SDep)+1);
  if (ParName = '����� ���-��')then
    if dm.SDepId = -1 then ParValue := ibdsWhArtQUANTITY.AsFloat
    else ParValue := '';//dm.getCommaBlob(ibdsWHArtTQ.AsString, dm.getPosInArr(dm.arrWH, dm.SDep)+1);
  if(ParName='��������� �������')then
    if taUIDWHBUNITID.AsInteger = 0 then parValue := taUIDWHBPRICE.Value*taUIDWHBQ.Value//parValue := taUIDWHBPRICE.Value
    else ParValue := taUIDWHBPRICE.Value*taUIDWHBW.Value;
  if(ParName='���������� �������')then
    if taUIDWHBUNITID.AsInteger = 0 then parValue := taUIDWHBSPRICE.Value*taUIDWHBQ.Value//parValue := taUIDWHBPRICE.Value
    else ParValue := taUIDWHBSPRICE.Value*taUIDWHBW.Value;
  if(ParName='������')then
    if dm.PayType = -1000 then parValue := '���'//parValue := taUIDWHBPRICE.Value
     else
      begin
    quTmp.Close;
    quTmp.SQL.Text := 'select Name from D_PAYTYPE where PAYTYPEID='+IntToStr(dm.PayType);
    quTmp.ExecQuery;
    ParValue := quTmp.Fields[0].AsString;
    quTmp.Close;
  end;


  ind := Pos('���', ParName);
  if ind <> 0 then
    case StrToInt(Copy(ParName, ind+3, Length(ParName))) of
      0 :;
      1 :;
      2 :;
    end;
  if(ParName = '�������')then
  begin
    quTmp.Close;
    quTmp.SQL.Text := 'select Name from D_Dep where D_DepId='+IntToStr(dmServ.WhDepId);
    quTmp.ExecQuery;
    ParValue := quTmp.Fields[0].AsString;
    quTmp.Close;
  end;
  if(ParName='�����������')then
  begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select dc.name from d_comp dc, d_rec dr where dc.d_compid=dc.d_compid';
    dm.quTmp.ExecQuery;
    parValue := trim(dm.quTmp.Fields[0].AsString);
    dm.quTmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
  end;
  if(ParName='����� �����������')then
  begin
    dm.quTmp.Close;
    dm.quTmp.SQL.Text:='select dc.address, dc.city from d_comp dc, d_rec dr where dc.d_compid=dc.d_compid';
    dm.quTmp.ExecQuery;
    parValue := '�. '+trim(dm.quTmp.Fields[1].AsString)+'  '+trim(dm.quTmp.Fields[0].AsString);
    dm.quTmp.Transaction.CommitRetaining;
    dm.quTmp.Close;
  end;
  if(ParName='������ ��������')then
    parValue := ' c '+DateTimeToStr(dm.UIDWHBD)+' �� ' + DateToStr(dm.UIDWHED);
end;


procedure TdmReport.ibdsWhArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['UserId'].AsInteger := dmcom.UserId;
    dmCom.SetArtFilter(Params);
    {if dmServ.WHDepId = -1 then
    begin
      ByName('DEPID1').AsInteger := -MAXINT;
      ByName('DEPID2').AsInteger := MAXINT;
    end
    else begin
      ByName('DEPID1').AsInteger := dmServ.WHDepId;
      ByName('DEPID2').AsInteger := dmServ.WHDepId;
    end;}
  end;
end;

procedure TdmReport.ibdsWhArt2BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ByName['USERID'].AsInteger := dmCom.UserId;
    if dmServ.WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MaxInt;
      ByName['DEPID2'].AsInteger := MaxInt;
    end
    else begin
      ByName['DEPID1'].AsInteger := dmServ.WHDepId;
      ByName['DEPID2'].AsInteger := dmServ.WHDepId;
    end;
  end;
end;

procedure TdmReport.PrListGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if(ParName = '���������')then
    case KindPrList of
      // �� ��������� ���������
      1 : ParValue := '�� ����. ����.�';
      // �� ��������� ����������� �����������
      2 : ParValue := '�� ����. �����.�����.�';
    end;
  if (ParName = '�����')then ParValue := GetAssay(PChar(ibdsPrListItemR_Mat.Value));
  if (ParName = '��. ���.')then ParValue := GetUnit(ibdsPrListItemR_UNITID.Value);
end;

procedure TdmReport.FullPrListGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if(ParName = '���������')then ParValue := dmcom.DefaultDep.Name;
  if(ParName='�����')then ParValue := GetAssay(PChar(ibdsFullPrListR_Mat.Value));
  if(ParName='��. ���.')then ParValue := GetUnit(ibdsFullPrListR_UNITID.Value);
end;

procedure TdmReport.MainDocGetValue(const ParName: String;
  var ParValue: Variant);
begin

end;

procedure TdmReport.LoadDoc(DocId : integer);
begin
  taDoc.Open;
  FDocId := DocId;
  frReport.LoadFromDB(taDoc, FDocId);
  frReport.FileName := taDocNAME.AsString;
  taDoc.Close;
end;

procedure TdmReport.PrintDocumentA(arr: Variant; Kind: TKindDoc; URF:integer=0);

  (*  ��������� �������������� �������� ���������� ��� ������� ������ ������ *)
  procedure PrintSeq(No : integer; DataSets: array of TDataSet; Params : array of Variant;
         GetValueEvent : TDetailEvent);
  var
    i,j,k : integer;
  begin
// DocPreview :=  pvDesign;
    LoadDoc(No); // ��������� ��������� ��������� �� ����
    frReport.OnGetValue := GetValueEvent; // ��������� ����������
    for i:=VarArrayLowBound(arr, 1) to VarArrayHighBound(arr, 1) do
    begin
      CloseDataSets(DataSets);
      begin
       for j:=Low(DataSets) to High(DataSets) do       // �� ������ �� ������� ������� �������
       begin
         if VarIsArray(Params[j]) then
           for k := VarArrayLowBound(Params[j],1) to VarArrayHighBound(Params[j], 1) do
             if Params[j][k] = Null then TpFIBDataSet(DataSets[j]).Params[k].AsVariant := arr[i]
             else TpFIBDataSet(DataSets[j]).Params[k].AsVariant := Params[j][k];
       end;
     {  ibdsFrom.SelectSQL.text:='select SN, NDATE, NAME, CITY, ADDRESS, PHONE from DOC_PROVIDER(600004093, 1) union select SN, NDATE, NAME, CITY, ADDRESS, PHONE from DOC_PROVIDER(600004094, 1)' ;
       ibdsTo.SelectSQL.text:='select NAME,  ADDRESS, PHONE, CITY from DOC_PROVIDER(600004093, 1) union select NAME,  ADDRESS, PHONE, CITY from DOC_PROVIDER(600004094, 1)';
       ibdsBillItem.SelectSQL.text:='select * from doc_Bill(600004093, 1) union select * from doc_Bill(600004094, 1)';
       ibdsGrIns.SelectSQL.text:='select * from InsByArt2(null) union select * from InsByArt2(null)';  }
     { ShowMessage(ibdsBillItem.paramByName('ID').asString);
       ShowMessage(ibdsBillItem.paramByName('KIND').asString);
       ShowMessage(ibdsGrIns.paramByName('R_Art2Id').asString);  }
       // ShowMessage(ibdsFrom.paramByName('ID').asString);
       // ShowMessage(ibdsFrom.paramByName('KIND').asString);
       { ibdsFrom.SelectSQL.text:='select SN, NDATE, NAME, CITY, ADDRESS, PHONE, SINVID from DOC_PROVIDER(600004093, 4) union select SN, NDATE, NAME, CITY, ADDRESS, PHONE, SINVID from DOC_PROVIDER(600004094, 4)' ;
        ibdsBillItem.SelectSQL.text:= 'select * from doc_Bill(?SINVID, 1)';
        ibdsFrom.CloseOpen(true);
        ibdsBillItem.CloseOpen(true);
        form6:= TForm6.Create(self);
        form6.showmodal;
        form6.free;  }
       ShowDoc;
      end;
    end;
    CloseDataSets(DataSets);
  end;

begin
  if trReport.Active then trReport.Commit;
  if not trReport.Active then trReport.StartTransaction;
  try
    case Kind of
      (* ����-������� ��� ����������� ���������� ??? *)
     subscription_b:begin
        frdsmol.DataSet:=taMol;
        taMol2.ParamByName('DEPID1').AsInteger := arr[0][0];
        taMol2.paramByName('DEPID2').AsInteger := arr[0][1];
        LoadDoc(268);
        ReOpenDataSets([taRec,taMol2]);
        ShowDoc;
      end;
     subscription_e:begin
        frdsmol.DataSet:=taMol;
        taMol2.ParamByName('DEPID1').AsInteger := arr[0][0];
        taMol2.paramByName('DEPID2').AsInteger := arr[0][1];
        LoadDoc(368);
        ReOpenDataSets([taRec,taMol2]);
         ShowMessage(ibdsBillItem.paramByName('ID').asString);
       ShowMessage(ibdsBillItem.paramByName('KIND').asString);
       ShowMessage(ibdsGrIns.paramByName('R_Art2Id').asString);
        ShowDoc;
      end;
      facture_u : ;
      (* �������� ��������� ��� ����������� (����������) *)
      invoice_u : PrintSeq(45, [taProvider, taSelf, ibdsBillUItem],
              [VarArrayOf([Null, 0]), VarArrayOf([0,1]), VarArrayOf([Null, 2])],
              FactureGetValue);
      invoice_u_cdm : PrintSeq(145, [taProvider, taSelf, ibdsBillUItem],
              [VarArrayOf([Null, 9]), VarArrayOf([Null,10]), VarArrayOf([Null, 22])],
              FactureGetValue);
      (* ����-������� ��� ����������� (�����������) *)
      facture : PrintSeq(
                 {$IFDEF RGOST}
                 302, // ����-12
                 {$ELSE}
                 2, // ������������ �����
                 {$ENDIF}
                 [taProvider, taSelf, ibdsBillItem],
              [VarArrayOf([Null, 0]), VarArrayOf([0,1]), VarArrayOf([Null, 0])],
              FactureGetValue);
      (* �������� ��������� ��� ����������� (������������) *)
      invoice : PrintSeq(
                 {$IFDEF RGOST}
                 303, // ����-12
                 {$ELSE}
                 3,  // ������������ �����
                 {$ENDIF}
                 [taProvider, taSelf, ibdsBillItem],
              [VarArrayOf([Null, 0]), VarArrayOf([0,1]), VarArrayOf([Null, 0])],
              FactureGetValue);
            (* �������� ��������� ��� ����������� (������������) *)
      invoice_opt_pr : PrintSeq(1303, [taProvider, taSelf, ibdsBillItem],
             [VarArrayOf([Null, 6]), VarArrayOf([Null,7]), VarArrayOf([Null, 10])],
              FactureGetValue);
     (* ���������� ��������� ����������� *)
     invoice_in :
       {$IFDEF RGOST}

         PrintSeq(112, [ibdsFrom, ibdsTo, ibdsBillItem, ibdsGrIns, taSelf],
             [VarArrayOf([Null,4]), VarArrayOf([Null,3]), VarArrayOf([Null, 1]), Null, VarArrayOf([0,1])],
             FactureGetValue);
       {$ELSE}
         PrintSeq(12, [ibdsFrom, ibdsTo, ibdsBillItem, ibdsGrIns],
             [VarArrayOf([Null,4]), VarArrayOf([Null,3]), VarArrayOf([Null, 1]), Null],
             FactureGetValue);
       {$ENDIF}
     (* ���������� ��������� ��-�������� *)
     invoice_uin : PrintSeq(13, [ibdsFrom, ibdsTo, ibdsBillUItem],
             [VarArrayOf([Null, 4]), VarArrayOf([Null,3]), VarArrayOf([Null, 2])],
             FactureGetValue);
     (* ���������� ��������� � ��������� ����� *)
     invoice_in_sprice : PrintSeq(8, [ibdsFrom, ibdsTo, ibdsBillItem, ibdsGrIns],
             [VarArrayOf([Null,4]), VarArrayOf([Null,3]), VarArrayOf([Null, 8]), Null],
             FactureGetValue);
     (* ���������� ��������� � ��������� �����  �� ��������*)
     invoice_in_spriceSav:
             PrintSeq(120, [taProvider, taSelf,ibdsFrom, ibdsTo, ibdsBillItem, ibdsGrIns],
             [VarArrayOf([Null, 0]), VarArrayOf([0,1]),VarArrayOf([Null,4]), VarArrayOf([Null,3]), VarArrayOf([Null, 8]), Null],
             FactureGetValue);

      (* ������ ��� �� ��������� *)
      pricelist : PrintSeq(26, [ibdsPrList, ibdsPrListItem],
                [VarArrayOf([Null]), Null],
                PrListGetValue);
      (* ��������� ��� ������� ����������� *)
      invoice_opt :
      begin
         PrintSeq(3, [taProvider, taSelf, ibdsBillItem],
         [VarArrayOf([Null, 6]), VarArrayOf([Null, 7]), VarArrayOf([Null, 4])],
         FactureGetValue);
      end;
      (* ����-������� ��� ������� ����������� *)
      facture_opt : PrintSeq(2, [taProvider, taSelf, ibdsBillItem],
          [VarArrayOf([Null, 6]), VarArrayOf([Null, 7]), VarArrayOf([Null, 4])],
          FactureGetValue);

      (* ��������� ��� ������� ����������� , ��������� ��-��������*)
      invoice_opt_u : PrintSeq(103, [taProvider, taSelf, ibdsBillItem],
         [VarArrayOf([Null, 6]), VarArrayOf([Null, 7]), VarArrayOf([Null, 104])],
         FactureGetValue);
      (* ����-������� ��� ������� �����������, ��������� ��-�������� *)
      facture_opt_u : PrintSeq(102, [taProvider, taSelf, ibdsBillItem],
          [VarArrayOf([Null, 6]), VarArrayOf([Null, 7]), VarArrayOf([Null, 104])],
          FactureGetValue);
      (* ���������� � ������������� ���� �� ���. ��������� *)
      facture_opt_pril : PrintSeq(146, [taProvider, taSelf, ibdsBillItem],
          [VarArrayOf([Null, 6]), VarArrayOf([Null, 7]), VarArrayOf([Null, 104])],
          FactureGetValue);
      (* ���� �� ���������� *)
      bill : begin
        ibdsGrIns.DataSource := dsrSBillItem;
        PrintSeq(23, [taSelf, ibdsSBillItem, ibdsGrIns],
          [VarArrayOf([Null, 1]), VarArrayOf([Null, 6]),  Null],
          BillGetValue);
      end;
      (* ������ �� �������������� *)
      // taRec
      inventory_order : PrintSeq(468, [taRec], arr, nil);
      (* ��� �� �������������� *)
      inventory_act: begin
        LoadDoc(568);
        frReport.OnGetValue := nil;
        taRec.ParamByName('DEPID').AsInteger := arr[0][0];
        taRec.ParamByName('SINVID').AsInteger := dmServ.CurInventory;
        taInventoryAct_N.ParamByName('DEPID1').AsInteger := arr[1][0];
        taInventoryAct_N.ParamByName('DEPID2').AsInteger := arr[1][1];
        taInventoryAct_E.ParamByName('DEPID1').AsInteger := arr[2][0];
        taInventoryAct_E.ParamByName('DEPID2').AsInteger := arr[2][1];
        taMol2.ParamByName('DEPID1').AsInteger := arr[1][0];
        taMol2.paramByName('DEPID2').AsInteger := arr[1][1];
        ShowDoc;
      end;
      (* ��� �� �������������� *)
      inventory_act_null : begin
        LoadDoc(668);                                                                            
        frReport.OnGetValue := nil;
        taRec.ParamByName('DEPID').AsInteger := arr[0][0];
        taRec.ParamByName('SINVID').AsInteger := dmserv.CurInventory;
        taMol2.ParamByName('DEPID1').AsInteger := arr[1][0];
        taMol2.paramByName('DEPID2').AsInteger := arr[1][1];
        ShowDoc;
      end;
      inventory_b:begin
        LoadDoc(68);
        taRec.ParamByName('DEPID').AsInteger := arr[0];
        taRec.ParamByName('SINVID').AsInteger := dmserv.CurInventory;
        ShowDoc;
      end;
      inventory_e:begin
        LoadDoc(168);
        taRec.ParamByName('DEPID').AsInteger := arr[0];
        taRec.ParamByName('SINVID').AsInteger := dmserv.CurInventory;
        ShowDoc;
      end;
      (*��� �� �������� � ��������������*)
      writeoff_inventory_act: begin
        LoadDoc(768);
        frReport.OnGetValue := nil;
        taRec.ParamByName('DEPID').AsInteger := arr[0][0];
        taRec.ParamByName('SINVID').AsInteger := dmserv.CurInventory;
        taInventoryAct_N.ParamByName('DEPID1').AsInteger := arr[1][0];
        taInventoryAct_N.ParamByName('DEPID2').AsInteger := arr[1][1];
        taInventoryAct_E.ParamByName('DEPID1').AsInteger := arr[2][0];
        taInventoryAct_E.ParamByName('DEPID2').AsInteger := arr[2][1];
        taMol2.ParamByName('DEPID1').AsInteger := arr[1][0];
        taMol2.paramByName('DEPID2').AsInteger := arr[1][1];
        ShowDoc;
      end;
      report_sel_emp : begin
        LoadDoc(1004);
        frReport.OnGetValue := Seller_Emp_GetValue;
        frAnlzSelEmp.ParamByName('userid').AsInteger := arr[0];
        ShowDoc;
      end;

    end;
  finally
    if trReport.Active then trReport.Commit;
  end;
end;


procedure TdmReport.PrintDocumentB(Kind: TKindDoc);

  procedure Prepare(DocId : integer; GetValue : TDetailEvent);
  begin
    LoadDoc(DocId);
    frReport.OnGetValue := GetValue;
    ShowDoc;
  end;

var
  //Memo : TfrMemoView;
  Band : TfrBandView;
  //i, j, k : integer;
begin
  { TODO : PrintDocumentB }
  if trReport.Active then trReport.Commit;
  if not trReport.Active then trReport.StartTransaction;
  try
    case Kind of
      (* ������ ��� *)
      fullpricelist : begin
        LoadDoc(27); // fprlist
        frReport.OnGetValue := FullPrListGetValue;
        ShowDoc;
      end;
      (* ��������� ��������� *)
      maindoc :begin
        LoadDoc(101);
        frReport.OnGetValue := MainDocGetValue;
        ShowDoc;
      end;
      {������� �� ������������ ��������}
      letter:begin
        ReOpenDataSet(quClientAddress);
        LoadDoc(70);
//        frReport.OnGetValue := MainDocGetValue;
        ShowDoc;
        CloseDataSet(quClientAddress);
      end;
      (* ��������� ��������� ��-��������*)
      maindocuid : Prepare(201, MainDocGetValue);
      (* �������� ����� *)
      uidstore_t_p : begin
        LoadDoc(423);
        frReport.OnGetValue := UIDStore_T_GetValue;
        // ��������� ����� ��� ������������� ������������ �������-�������
        Band := TfrBandView(frReport.FindObject('CrossData1'));
        if Assigned(Band)then
        begin
          Band.DataSet := 'MasterData2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterHeader2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterFooter2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterHeader5='+IntToStr(dmCom.DepCount)+';'+
                          'MasterData9='+IntToStr(dmCom.DepCount)+';'+
                          'MasterFooter3='+IntToStr(dmCom.DepCount)+';';
        end;
        ShowDoc;
      end;
      uidstore_t_p_c : begin
      if centerdep then LoadDoc(523) else LoadDoc(524);
        frReport.OnGetValue := UIDStore_T_GetValue;
        // ��������� ����� ��� ������������� ������������ �������-�������
        Band := TfrBandView(frReport.FindObject('CrossData1'));
        if Assigned(Band)then
        begin
          Band.DataSet := 'MasterData2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterHeader2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterFooter2='+IntToStr(dmCom.DepCount)+';'+
                          'MasterHeader5='+IntToStr(dmCom.DepCount)+';'+
                          'MasterData9='+IntToStr(dmCom.DepCount)+';'+
                          'MasterFooter3='+IntToStr(dmCom.DepCount)+';'+
                          'MasterFooter9='+IntToStr(dmCom.DepCount)+';'+
                          'MasterData14='+IntToStr(dmCom.DepCount)+';'+
                          'MasterData13='+IntToStr(dmCom.DepCount)+';' +
                          'MasterData15='+IntToStr(dmCom.DepCount)+';';
        end;
        ShowDoc;
      end;
      uidstoredep_t_p_c : begin
        LoadDoc(623);
        frReport.OnGetValue := UIDStoreDep_T_GetValue;
        ShowDoc;
      end;
      uid_store_Doc: begin
        ReOpenDataSet(ibdsUID_Store);
        LoadDoc(71);
        ShowDoc;
        CloseDataSet(ibdsUID_Store);
      end;
      shop_report_baduid : begin
        LoadDoc(117);
        ShowDoc;
      end;
      uidWH_Report: begin
        ReOpenDataSet(ibdsUIDWH_Report);
        LoadDoc(72);
        ShowDoc;
      end;
      ActAllowance_Doc: begin
        ReOpenDataSets([ibdsHatWrite_off, ibdsWrite_off]);
        LoadDoc(73);
        ShowDoc;
      end;
      PresentActAllowance_Doc: begin
        ReOpenDataSets([ibdsHatWrite_off, ibdsWrite_off, taSelf, ibdsFrom, ibdsTo]);
        LoadDoc(173);
        ShowDoc;
      end;
      newNodCard: begin
        ReOpenDataSet(quSelectNodCard);
        LoadDoc(74);
        ShowDoc;
      end;
     UidWh_INV_Art: begin
        frReport.OnGetValue:=UidWhInv_GetValue;
        CloseDataSets([quUidWhArt, quGrIns]);
        quUidWhArt.ParamByName('KIND').AsInteger:=0;
        OpenDataSets([quUidWhArt, quGrIns]);
        LoadDoc(112);
        ShowDoc;
      end;
     UidWh_Prord: begin
        frReport.OnGetValue:=UidWhInv_GetValue;
        CloseDataSets([quUidWhArt]);
        quUidWhArt.ParamByName('KIND').AsInteger:=1;
        OpenDataSets([quUidWhArt]);
        LoadDoc(114);
        ShowDoc;
      end;
     Balance: begin
//        ReOpenDataSet(ibdsBalance);
        LoadDoc(75);
        ShowDoc;
     end;
     TermCard_Check: begin
      ReOpenDataSet(ibdsCardCheck);
      LoadDoc(76);
      ShowDoc;
     end;

      comissionAct: begin

        comissionActSumm := 0;

        taUIDWHB.Open;

        taUIDWHB.First;

        while not taUIDWHB.Eof do
        begin

          if taUIDWHBUNITID.asInteger = 0 then
          begin
            comissionActSumm := comissionActSumm + (taUIDWHBPRICE.AsCurrency - taUIDWHBSPRICE.AsCurrency) * taUIDWHBQ.AsInteger;
          end else
          begin
            comissionActSumm := comissionActSumm + RoundTo((taUIDWHBPRICE.AsCurrency - taUIDWHBSPRICE.AsCurrency) * taUIDWHBW.AsFloat, -2);
          end;

          taUIDWHB.Next;

        end;

        frReport.OnGetValue := comissionActGetValue;

        loadDoc(9);        

        ShowDoc;

      end;


    end
  finally
    if trReport.Active then trReport.Commit;
  end;
end;

procedure TdmReport.PrintLayOut(Kind: TLayOutKindDoc);
begin
  if trReport.Active then trReport.Commit;
  if not trReport.Active then trReport.StartTransaction;
  try
    if (Kind = art_all4) or (Kind=art_all5) then frReport.OnGetValue := LayoutGetValue2
    else  frReport.OnGetValue := LayoutGetValue;
    case Kind of
      (* ��������� ��-�������� �� ������ *)
      uid_all : begin LoadDoc(41); end;
      uid_dep : begin LoadDoc(43); end;
      art_all : begin LoadDoc(48) end;
      art_dep : begin LoadDoc(25) end;
      uid_all2 : begin LoadDoc(141); end;
      uid_dep2 : begin LoadDoc(143); end;
      art_all2 : begin LoadDoc(148) end;
      art_dep2 : begin LoadDoc(125) end;
      art_dep3 : begin LoadDoc(225) end;
      art_all3 : begin LoadDoc(248) end;
      art_all4 : begin LoadDoc(348) end;
      art_all5 : begin LoadDoc(349) end;
    end;

    ShowDoc;
  finally
    if trReport.Active then trReport.Commit;
  end;
end;

procedure TdmReport.TicketItBeforeOpen(DataSet: TDataSet);
var
  p, i : integer;
begin
{ TODO : �������������� reopen ticket }
  p := -1;
  for i:=Low(arr_sell) to High(arr_sell) do
    if(arr_sell[i][0] = taTicketCHECKNO.AsVariant) then
    begin
      p := i;
      break;
    end;
  if(p = -1)then raise Exception.Create('');

  ibdsTicketIt.Close; //������������

  ibdsTicketIt.SelectSQL.Text := gen_query(TarrDoc(arr_sell[p]), 'select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, '+
          ' R_UID, R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE, '+
          ' R_UnitId, R_MAT, R_GOOD, R_ART, R_Id, R_DiscPrice, R_Discount, '+
          ' R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_Price, R_UNIT, R_SSF, r_actdiscount from Doc_Ticket(', ')', [':ID', ':KIND']);
  for i := Low(arr_sell[p])+1 to High(arr_sell[p]) do
  begin
    ibdsTicketIt.Params[2*i].AsInteger := arr_sell[p][i];
    ibdsTicketIt.Params[2*i+1].AsInteger := 0;
  end;
end;

procedure TdmReport.UIDStore_T_GetValue(const ParName: string; var ParValue: Variant);
begin
  if(ParName = '������ �������')then ParValue := dm3.taUIDStoreListSDATE.AsDateTime;
  if(ParName = '����� �������')then ParValue := dm3.taUIDStoreListNDATE.AsDateTime;
end;

procedure TdmReport.UIDStoreDep_T_GetValue(const ParName: string; var ParValue: Variant);
begin
  if(ParName = '������ �������')then ParValue := fmUidStoreDepList.taUIDStoreDepListSDATE.AsDateTime;
  if(ParName = '����� �������')then ParValue := fmUidStoreDepList.taUIDStoreDepListNDATE.AsDateTime;
  if(ParName = '������')then ParValue := fmUidStoreDepList.taUIDStoreDepListDEPNAME.AsString;
end;

procedure TdmReport.Seller_Emp_GetValue(const ParName: string; var ParValue: Variant);
begin
  if(ParName = '������ �������')then ParValue := DateToStr(dm.BeginDate);
  if(ParName = '����� �������')then ParValue := DateToStr(dm.EndDate);
  if(ParName = '������')then ParValue := dm.SDep;
end;

{ TfrAddLib }

constructor TfrAddLib.Create;
begin
  inherited Create;
  List.Add('CAPITALSMONEY');
  List.Add('CAPITALSWEIGHT');
  List.Add('CAPITALSQUANTITY');
  List.Add('U');
  List.Add('SELLDEPCOST_UIDSTORE');
  List.Add('SELL2DEPCOST_UIDSTORE');
  List.Add('SELLDEPNAME_UIDSTORE');
  List.Add('SELL2DEPNAME_UIDSTORE');
  List.Add('SELLDEPSUM_UIDSTORE');
  List.Add('SELL2DEPSUM_UIDSTORE');
  List.Add('EAN13_2');
  List.Add('EAN13_3');
  List.Add('SELLDEPCOST_UIDSTORE_C');
  List.Add('SELL2DEPCOST_UIDSTORE_C');
  List.Add('SELLDEPNAME_UIDSTORE_C');
  List.Add('SELL2DEPNAME_UIDSTORE_C');
  List.Add('SELLDEPSUM_UIDSTORE_C');
  List.Add('SELL2DEPSUM_UIDSTORE_C');
  List.Add('SELLCOMDEPCOST_UIDSTORE_C');
  List.Add('SELLALLDEPCOST_UIDSTORE_C');
  List.Add('SELLKEEPDEPCOST_UIDSTORE_C');
end;

procedure TfrAddLib.DoFunction(FNo: Integer; p1, p2, p3: Variant; var val: Variant);
var
  col, k, i : integer;
  DataSet : TDataSet;
  Bookmark : Pointer;
begin
  DataSet := nil;
  val := 0;
  case FNo of
    0 : val := GetCapitalsMoney(frParser.Calc(p1));
    1 : val := GetCapitalsWeight(frParser.Calc(p1));
    2 : val := GetCapitalsQuantity(frParser.Calc(p1));
    3 : if frParser.Calc(p1) = 1 then val := '��'
        else val := '��';
    4, 5 : begin // SELLDEPCOST_UIDSTORE, SELL2DEPCOST_UIDSTORE
          val := 0;
          if not Assigned(fmUIDStore_T)then eXit;
          col := frParser.Calc(p1);
          k := 0;
          case FNo of
            4 : DataSet := fmUIDStore_T.taSell;
            5 : DataSet := fmUIDStore_T.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := DataSet.Fields[i].Value;
                eXit;
              end
            end
        end;
    6, 7 : begin // SELLDEPNAME_UIDSTORE, SELL2DEPNAME_UIDSTORE
          val := '';
          if not Assigned(fmUIDStore_T) then eXit;
          col := frParser.Calc(p1);
          k:=0;
          case FNo of
            6 : DataSet := fmUIDStore_T.taSell;
            7 : DataSet := fmUIDStore_T.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := copy(DataSet.Fields[i].FieldName, 3, Length(DataSet.Fields[i].FieldName));
                val := dmCom.Dep[val].SName;
                eXit;
              end;
            end;
       end;
    8, 9 : begin
          val := 0;
          if not Assigned(fmUIDStore_T) then eXit;
          col := frParser.Calc(p1);
          k:=0;
          case FNo of
            8 : DataSet := fmUIDStore_T.taSell;
            9 : DataSet := fmUIDStore_T.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                Bookmark := DataSet.GetBookmark;
                DataSet.DisableControls;
                try
                  DataSet.First;
                  while not DataSet.Eof do
                  begin
                    val := val + DataSet.Fields[i].Value;
                    DataSet.Next;
                  end;
                finally
                  DataSet.GotoBookmark(Bookmark);
                  Dataset.EnableControls;
                end;
                eXit;
              end;
            end;
    end;
   10: begin //EAN13_2
        if not dmreport.ibdsTagWI.active then exit;
        val:=200000000000+frParser.Calc(p1);
       end;
   11: begin //EAN13_3
        if not dmreport.quSelectNodCard.active then exit;
        val:=300000000000+frParser.Calc(p1);
       end;
   12, 13 : begin // SELLDEPCOST_UIDSTORE_C, SELL2DEPCOST_UIDSTORE_C
          val := 0;
          if not Assigned(fmUIDStore_T_C)then eXit;
          col := frParser.Calc(p1);
          k := 0;
          case FNo of
            12 : DataSet := fmUIDStore_T_C.taSell;
            13 : DataSet := fmUIDStore_T_C.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := DataSet.Fields[i].Value;
                eXit;
              end
            end
        end;
    14, 15 : begin // SELLDEPNAME_UIDSTORE_C, SELL2DEPNAME_UIDSTORE_C
          val := '';
          if not Assigned(fmUIDStore_T_C) then eXit;
          col := frParser.Calc(p1);
          k:=0;
          case FNo of
            14 : DataSet := fmUIDStore_T_C.taSell;
            15 : DataSet := fmUIDStore_T_C.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := copy(DataSet.Fields[i].FieldName, 3, Length(DataSet.Fields[i].FieldName));
                val := dmCom.Dep[val].SName;
                eXit;
              end;
            end;
       end;
    16, 17 : begin
          val := 0;
          if not Assigned(fmUIDStore_T_C) then eXit;
          col := frParser.Calc(p1);
          k:=0;
          case FNo of
            16 : DataSet := fmUIDStore_T_C.taSell;
            17 : DataSet := fmUIDStore_T_C.taSell2;
          end;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                Bookmark := DataSet.GetBookmark;
                DataSet.DisableControls;
                try
                  DataSet.First;
                  while not DataSet.Eof do
                  begin
                    val := val + DataSet.Fields[i].Value;
                    DataSet.Next;
                  end;
                finally
                  DataSet.GotoBookmark(Bookmark);
                  Dataset.EnableControls;
                end;
                eXit;
              end;
            end;
    end;
   18 : begin // SELLCOMDEPCOST_UIDSTORE_C
          val := 0;
          if not Assigned(fmUIDStore_T_C)then eXit;
          col := frParser.Calc(p1);
          k := 0;
          DataSet := fmUIDStore_T_C.taSellCom;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := DataSet.Fields[i].Value;
                eXit;
              end
            end
        end;
   19 : begin // SELLALLDEPCOST_UIDSTORE_C
          val := 0;
          if not Assigned(fmUIDStore_T_C)then eXit;
          col := frParser.Calc(p1);
          k := 0;
          DataSet := fmUIDStore_T_C.taSellAll;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := DataSet.Fields[i].Value;
                eXit;
              end
            end
        end;
   20 : begin // SELLKEEPDEPCOST_UIDSTORE_C
          val := 0;
          if not Assigned(fmUIDStore_T_C)then eXit;
          col := frParser.Calc(p1);
          k := 0;
          DataSet := fmUIDStore_T_C.taSellKeep;
          for i:=0 to Pred(DataSet.FieldCount) do
            if Pos('F_', DataSet.Fields[i].FieldName)<>0 then
            begin
              inc(k);
              if(k=col)then
              begin
                val := DataSet.Fields[i].Value;
                eXit;
              end
            end
        end;


  end;
end;

procedure TdmReport.frReportUserFunction(const Name: String; p1, p2,
  p3: Variant; var Val: Variant);
begin
  if(Name='CAPITALSQUANTITY')then
     val := GetCapitalsQuantity(frParser.Calc(p1));
  if(Name='CAPITALSWEIGHT')then
       val := GetCapitalsWeight(frParser.Calc(p1));
  if(Name='CAPITALSMONEY')then val := GetCapitalsMoney(frParser.Calc(p1));
  if(Name='U')then
    if frParser.Calc(p1) = 1 then val := '��'
    else val := '��';
end;

procedure TdmReport.taJ3BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['USERID'].AsInteger := dmCom.UserId;
end;

procedure TdmReport.frDesignerSaveReport(Report: TfrReport;
  var ReportName: String; SaveAs: Boolean; var Saved: Boolean);
begin
  if dmcom.User.Adm then
  begin
    OpenDataSets([taDoc]);

    if not taDoc.Locate('Id', FDocId, []) then
    begin
      taDoc.Insert;
      taDocID.AsInteger := FDocId;
      taDoc.Post;
      taDoc.Transaction.CommitRetaining;
    end;

    Report.SaveToDB(taDoc, FDocId);

    taDoc.Transaction.CommitRetaining;

    CloseDataSets([taDoc]);
  end;
end;



procedure TdmReport.taSelfBeforeOpen(DataSet: TDataSet);
begin
{  with TpFIBDataSet(DataSet).Params do
    ByName('KIND').AsInteger := 1;}
end;

procedure TdmReport.UIDWHSelectModif(DataSet: TDataSet; const Revis:boolean=false);
var ch:char;
begin
  with TpFIBDataSet(DataSet) do
  begin
  Close;
  with dm do
    begin
      if lMultiSelect and (SD_MatID<>'')then SelectSQL[7]:=' a2.D_MatId in ('+SD_MatID+') and '
      else SelectSQL[7]:=' ';

      if lMultiSelect and (SD_GoodID<>'')then SelectSQL[9]:=' a2.D_GoodId in ('+SD_GoodID+') and '
      else SelectSQL[9]:=' ';

      if lMultiSelect and (SD_CompID<>'')then SelectSQL[11]:=' a2.D_CompId in ('+SD_CompID+') and '
      else SelectSQL[11]:=' ';

      if lMultiSelect and (SD_SupID<>'')then SelectSQL[13]:=' u.SupId0 in ('+SD_SupID+') and '
      else  SelectSQL[13]:=' ';

      if lMultiSelect and (SD_Note1<>'')then  SelectSQL[18]:=' and u.Goodsid1 in ('+SD_Note1+') '
      else  SelectSQL[18]:=' ';


      if lMultiSelect and (SD_Note2<>'')then SelectSQL[19]:=' and u.Goodsid2 in ('+SD_Note2+') '
      else SelectSQL[19]:=' ';

      if lMultiSelect and (SD_Att1<>'')then  SelectSQL[20]:=' and a2.Att1 in ('+SD_Att1+') '
      else SelectSQL[20]:=' ';

      if lMultiSelect and (SD_Att2<>'')then SelectSQL[21]:=' and a2.Att2 in ('+SD_Att2+') '
      else SelectSQL[21]:=' ';

      if dm.lMultiSelect and (dm.UidwhSZ<>'') then SelectSQL[31]:=' and u.sz in ('+dm.UidwhSZ+') '
      else SelectSQL[31]:=' ';

      ch:=DecimalSeparator;
      DecimalSeparator:='.';

      if dm.IFilterCost  then SelectSQL[32]:=' and FIF(a2.UnitId, u.Price*u.W, u.Price) between '+FloatToStr(dm.ICOst1)+' and '+FloatToStr(dm.ICost2)
      else SelectSQL[32]:=' ';

      if dm.IFilterW  then SelectSQL[33]:=' and u.W between '+FloatToStr(dm.Iw1)+' and '+FloatToStr(dm.Iw2)
      else SelectSQL[33]:=' ';

      DecimalSeparator:=ch;

      if Revis then
       begin
        SelectSQL[16]:=' and (u.T between -1 and 1 or u.T=5 or u.T=7 ) ';
        SelectSQL[17]:='  and not(u.Itype=2 and u.IsClosed=0) '
       end
      else
       begin
       SelectSQL[17]:=' ' ;
       if lSellSelect then  SelectSQL[16]:=' and u.T=2 '
       else if (not AllUIDWH) and (not lSRetSelect) and (not lOSinvSelect) then  SelectSQL[16]:=' and ((u.T between -1 and 1 ) or (u.T=5) or (u.T=7))'
        else if lSRetSelect then  SelectSQL[16]:=' and (u.T=4 or u.T=5) '
         else if lOSinvSelect then   SelectSQL[16]:=' and (u.T=-2) '
           else SelectSQL[16]:=' ';

       end;
    end;
    Prepare;
   end;
end;


procedure TdmReport.ibdsFromAfterOpen(DataSet: TDataSet);
var
 i: Integer;
begin
{  ShowMessage(ibdsFrom.SelectSQL.text);

  for i := 0 to ibdsFrom.Params.Count - 1 do
  begin
    ShowMessage (ibdsFrom.Params[i].Name + ' = ' + ibdsFrom.Params[i].AsString);
  end;

  Form6.DBGridEh1.DataSource := dsrFrom;
  Form6.ShowModal;}

end;

procedure TdmReport.ibdsFullPrListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    ByName['DEPID'].AsInteger := dmcom.DefaultDep.DepId;
end;

procedure TdmReport.ibdsGrInsAfterOpen(DataSet: TDataSet);
var
 i: Integer;
begin
{  ShowMessage(ibdsGrIns.SelectSQL.text);

  for i := 0 to ibdsGrIns.Params.Count - 1 do
  begin
    ShowMessage (ibdsGrIns.Params[i].Name + ' = ' + ibdsGrIns.Params[i].AsString);
  end;

  Form6.DBGridEh1.DataSource := dsrGrIns;
  Form6.ShowModal;}

end;

procedure TdmReport.frReportBeginDoc;
begin
  FNoTagWI:=SaveFNoTagWI;
end;

procedure TdmReport.taTSumDepBeforeOpen(DataSet: TDataSet);
begin
//
  with TpFIBDataSet(DataSet), Params, dm do
  begin
   ByName['I_BD'].AsDate:=trunc(BeginDate);
   ByName['I_ED'].AsDate:=trunc(EndDate);
   ByName['I_Depid1'].AsInteger:=SDepId;
   try
     ByName['I_Depid2'].AsInteger:=SDepId;
     //  0 - � ���������, 1 - � ���������;
     ByName['IIP'].AsInteger:=DAtaSet.Tag;
   except
   end;
  end;
end;

procedure TdmReport.taUIDWHAFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
// Accept:=dm.AcceptUIDWH(DataSet);
end;


procedure TdmReport.AppleText(const Appl_id:integer;const Sup:string);
var
  ftext:textfile;
  sDir, Path,sd:string;
begin
  sDir:=ExtractFilePath(ParamStr(0)) + 'Export';
  svdFile.DefaultExt:='txt';
  svdFile.InitialDir:=sDir;
  svdFile.FileName:=Sup+ReplaceStr(datetostr(dmCom.GetServerTime),'.','');
  try
 //�������� � ��������� ����
   if svdFile.Execute then
    begin
     Path:=svdFile.FileName;
     AssignFile(ftext, Path);
     try
       Rewrite(ftext);
       writeln(ftext,'��� "���������"');
       writeln(ftext,Sup);
       DateTimeToString(sd,'dd.mm.yyyy',dmCom.GetServerTime);
       writeln(ftext,sd);//datetostr(dmServ.L3BD));
       writeln(ftext,'  ������������  �������         �������   ������   ���-��');
       with dmReport.taApplSup do
         begin
          Params[0].Value :=Appl_id;
          Open;
          First;
          while not EOf do
            begin                           (* 1          2           3          4            5           6*)
              writeln(ftext,format('%13s %14s %9s %9s %10d',
              [FieldByName('Good').asString,extractword(1,FieldByName('ART').asString,[' ']),FieldByName('D_INSID').asString,
               FieldByName('SZ').asString,FieldByName('Q').asInteger]));
              next;
             end;
          Close;
        end;
      finally
         CloseFile(fText);
      end;
    end;
  finally
    MessageDialog('�������� � ��������� ���� ���������!', mtInformation, [mbOk], 0);
  end;
end;

procedure TdmReport.taInventoryBeforeCalcFields(DataSet: TDataSet);
begin
 taInventoryBeforen.AsInteger:=taInventoryBefore.RecNo;
 if taInventoryBeforeUNITID.AsInteger=1 then
  taInventoryBeforecost.AsFloat:=taInventoryBeforePRICE0.AsFloat*taInventoryBeforeW.AsFloat
 else taInventoryBeforecost.AsFloat:=taInventoryBeforePRICE0.AsFloat;
end;

procedure TdmReport.taInventoryEndCalcFields(DataSet: TDataSet);
begin
 taInventoryEndRECN.AsInteger:=taInventoryEnd.RecNo;
 if taInventoryEndUNITID.AsInteger=1 then
  taInventoryEndCOST.AsFloat:=taInventoryEndPRICE0.AsFloat*taInventoryEndW.AsFloat
 else taInventoryEndCOST.AsFloat:=taInventoryEndPRICE0.AsFloat;
end;

procedure TdmReport.taD_EmpCalcFields(DataSet: TDataSet);
begin
 if  taD_Emp.RecNo=1 then
  taD_EmpPostFIO.AsString:='�����:                '+trim(taD_EmpPOST.AsString)+'  '+trim(taD_EmpFIO.AsString)
 else
  taD_EmpPostFIO.AsString:='                            '+trim(taD_EmpPOST.AsString)+'  '+trim(taD_EmpFIO.AsString)
end;

procedure TdmReport.taRecCalcFields(DataSet: TDataSet);
begin
 taRecLastSentens.AsString:='������������ ������������������ �������� '+trim(taRecPRESEDENTFIO.AsString)+
                            ' ������� �������� �������� �� ������������� ������� �� '+
                            datetimetostr(taRecINVERDATE.AsDateTime)+'�. � ����������� �������� ��������� '+
                            ' �� ������������������ ������ �� �� �� �����, ����������� �� ��������������� '+
                            trim(taRecRESPONSIBLE_PERSON.AsString);   
end;

procedure TdmReport.UNITIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    0 : Text := '��';
    1 : Text := '��';
    else Text := '';
  end;
end;

procedure TdmReport.taInventoryBeforeBeforeOpen(DataSet: TDataSet);
begin
 with dm, taInventoryBefore do
 begin
  if lMultiSelect_I and (SD_MatId_I<>'')then
     SelectSQL[24]:=' and i.d_matid in ('+SD_MatID_I+') '
  else
     SelectSQL[24]:=' ';

  if lMultiSelect_I and (SD_GoodID_I<>'')then
     SelectSQL[17]:=' and i.d_goodid in ('+SD_GoodID_I+') '
  else
     SelectSQL[17]:=' ';

  if lMultiSelect_I and (SD_CompID_I<>'')then
     SelectSQL[18]:=' and i.COMPID in ('+SD_CompID_I+') '
  else
     SelectSQL[18]:=' ';

  if lMultiSelect_I and (SD_SupID_I<>'')then
     SelectSQL[19]:=' and i.SUPID in ('+SD_SupID_I+') '
  else
     SelectSQL[19]:=' ';

  if lMultiSelect_I and (SD_Note1_I<>'')then
     SelectSQL[20]:=' and i.d_goods_sam1 in ('+SD_Note1_I+') '
  else
     SelectSQL[20]:=' ';

  if lMultiSelect_I and (SD_Note2_I<>'')then
     SelectSQL[21]:=' and i.d_goods_sam2 in ('+SD_Note2_I+') '
  else
     SelectSQL[21]:=' ';

  if lMultiSelect_I and (SD_Att1_I<>'')then
     SelectSQL[22]:=' and i.att1 in ('+SD_Att1_I+') '
  else
     SelectSQL[22]:=' ';

  if lMultiSelect_I and (SD_Att2_I<>'')then
     SelectSQL[23]:=' and i.att2 in ('+SD_Att2_I+') '
  else
     SelectSQL[23]:=' ';
 end;
 taInventoryBefore.Prepare;
 taInventoryBefore.ParamByName('SINVID').AsInteger:=dmserv.CurInventory;
 dmCom.SetArtFilter(taInventoryBefore.Params);
 //taInventoryBefore.SelectSQL.SaveToFile('c:\before.sql');
end;

procedure TdmReport.taInventoryEndBeforeOpen(DataSet: TDataSet);
begin
 with dm, taInventoryEnd do
 begin
  if lMultiSelect_I and (SD_MatId_I<>'')then
     SelectSQL[24]:=' and i.d_matid in ('+SD_MatID_I+') '
  else
     SelectSQL[24]:=' ';

  if lMultiSelect_I and (SD_GoodID_I<>'')then
     SelectSQL[17]:=' and i.d_goodid in ('+SD_GoodID_I+') '
  else
     SelectSQL[17]:=' ';

  if lMultiSelect_I and (SD_CompID_I<>'')then
     SelectSQL[18]:=' and i.D_COMPID in ('+SD_CompID_I+') '
  else
     SelectSQL[18]:=' ';

  if lMultiSelect_I and (SD_SupID_I<>'')then
     SelectSQL[19]:=' and i.SUPID in ('+SD_SupID_I+') '
  else
     SelectSQL[19]:=' ';

  if lMultiSelect_I and (SD_Note1_I<>'')then
     SelectSQL[20]:=' and i.d_goods_sam1 in ('+SD_Note1_I+') '
  else
     SelectSQL[20]:=' ';

  if lMultiSelect_I and (SD_Note2_I<>'')then
     SelectSQL[21]:=' and i.d_goods_sam2 in ('+SD_Note2_I+') '
  else
     SelectSQL[21]:=' ';

  if lMultiSelect_I and (SD_Att1_I<>'')then
     SelectSQL[22]:=' and i.att1 in ('+SD_Att1_I+') '
  else
     SelectSQL[22]:=' ';

  if lMultiSelect_I and (SD_Att2_I<>'')then
     SelectSQL[23]:=' and i.att2 in ('+SD_Att2_I+') '
  else
     SelectSQL[23]:=' ';
 end;

 taInventoryEnd.Prepare;
 taInventoryEnd.ParamByName('SINVID').AsInteger:=dmserv.CurInventory;
 if dmcom.FilterRepDepID=0 then
 begin
  taInventoryEnd.ParamByName('DEPID1').AsInteger:=-MaxInt;
  taInventoryEnd.ParamByName('DEPID2').AsInteger:=MAXINT;
 end
 else
 begin
  taInventoryEnd.ParamByName('DEPID1').AsInteger:=dmcom.FilterRepDepID;
  taInventoryEnd.ParamByName('DEPID2').AsInteger:=dmcom.FilterRepDepID;
 end;

 dmCom.SetArtFilter(taInventoryEnd.Params);
 //taInventoryEnd.SelectSQL.SaveToFile('c:\after.sql');
end;

procedure TdmReport.taInventoryAct_NBeforeOpen(DataSet: TDataSet);
begin
 with dm, taInventoryAct_N do
 begin
  if lMultiSelect_I and (SD_MatId_I<>'')then
     SelectSQL[24]:=' and i.d_matid in ('+SD_MatID_I+') '
  else
     SelectSQL[24]:=' ';

  if lMultiSelect_I and (SD_GoodID_I<>'')then
     SelectSQL[17]:=' and i.d_goodid in ('+SD_GoodID_I+') '
  else
     SelectSQL[17]:=' ';

  if lMultiSelect_I and (SD_CompID_I<>'')then
     SelectSQL[18]:=' and i.D_COMPID in ('+SD_CompID_I+') '
  else
     SelectSQL[18]:=' ';

  if lMultiSelect_I and (SD_SupID_I<>'')then
     SelectSQL[19]:=' and i.SUPID in ('+SD_SupID_I+') '
  else
     SelectSQL[19]:=' ';

  if lMultiSelect_I and (SD_Note1_I<>'')then
     SelectSQL[20]:=' and i.d_goods_sam1 in ('+SD_Note1_I+') '
  else
     SelectSQL[20]:=' ';

  if lMultiSelect_I and (SD_Note2_I<>'')then
     SelectSQL[21]:=' and i.d_goods_sam2 in ('+SD_Note2_I+') '
  else
     SelectSQL[21]:=' ';

  if lMultiSelect_I and (SD_Att1_I<>'')then
     SelectSQL[22]:=' and i.att1 in ('+SD_Att1_I+') '
  else
     SelectSQL[22]:=' ';

  if lMultiSelect_I and (SD_Att2_I<>'')then
     SelectSQL[23]:=' and i.att2 in ('+SD_Att2_I+') '
  else
     SelectSQL[23]:=' ';
 end;
 taInventoryAct_N.Prepare;
 taInventoryAct_N.ParamByName('SINVID').AsInteger:=dmserv.CurInventory;
 taInventoryAct_N.ParamByName('ISOUTPRICE').AsInteger := InventOutPrice;
 dmCom.SetArtFilter(taInventoryAct_N.Params);

end;

procedure TdmReport.taInventoryAct_EBeforeOpen(DataSet: TDataSet);
begin
 with dm, taInventoryAct_E do
 begin
  if lMultiSelect_I and (SD_MatId_I<>'')then
     SelectSQL[24]:=' and i.d_matid in ('+SD_MatID_I+') '
  else
     SelectSQL[24]:=' ';

  if lMultiSelect_I and (SD_GoodID_I<>'')then
     SelectSQL[17]:=' and i.d_goodid in ('+SD_GoodID_I+') '
  else
     SelectSQL[17]:=' ';

  if lMultiSelect_I and (SD_CompID_I<>'')then
     SelectSQL[18]:=' and i.D_COMPID in ('+SD_CompID_I+') '
  else
     SelectSQL[18]:=' ';

  if lMultiSelect_I and (SD_SupID_I<>'')then
     SelectSQL[19]:=' and i.SUPID in ('+SD_SupID_I+') '
  else
     SelectSQL[19]:=' ';

  if lMultiSelect_I and (SD_Note1_I<>'')then
     SelectSQL[20]:=' and i.d_goods_sam1 in ('+SD_Note1_I+') '
  else
     SelectSQL[20]:=' ';

  if lMultiSelect_I and (SD_Note2_I<>'')then
     SelectSQL[21]:=' and i.d_goods_sam2 in ('+SD_Note2_I+') '
  else
     SelectSQL[21]:=' ';

  if lMultiSelect_I and (SD_Att1_I<>'')then
     SelectSQL[22]:=' and i.att1 in ('+SD_Att1_I+') '
  else
     SelectSQL[22]:=' ';

  if lMultiSelect_I and (SD_Att2_I<>'')then
     SelectSQL[23]:=' and i.att2 in ('+SD_Att2_I+') '
  else
     SelectSQL[23]:=' ';
 end;

 taInventoryAct_E.Prepare;
 taInventoryAct_E.ParamByName('SINVID').AsInteger:=dmserv.CurInventory;
 taInventoryAct_E.ParamByName('ISOUTPRICE').AsInteger := InventOutPrice;
 dmCom.SetArtFilter(taInventoryAct_E.Params);

end;

procedure TdmReport.ibdsUID_StoreBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
   try
    ByName['SInvID'].AsInteger :=  dm3.taUIDStoreListSINVID.AsInteger;
   except
   end;
end;

procedure TdmReport.ibdsUIDWH_ReportBeforeOpen(DataSet: TDataSet);
var i: integer;
    tmpstr: string;
begin
  with TpFIBDataSet(DataSet), Params do
    try
      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.UIDWHED);
      if dm.UIDWHType=3 then ByName['UserID'].AsInteger:=UserDefault
      else ByName['UserID'].AsInteger:= dmCom.UserId;
      if dm.SDepId = -1 then
      begin
        tmpstr := '';
        for i:=0 to Pred(dmCom.DepCount) do
           tmpstr :=  tmpstr + IntToStr(dmCom.DepInfo[i].DepId) + ',';
        System.delete(tmpstr, Length(tmpstr),1);
        ByName['DepID'].AsString := tmpstr;

      end
      else
        ByName['DepID'].AsString := IntToStr(dm.SDepId);

    except
    end;
end;

procedure TdmReport.ibdsHatWrite_offBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    ByName['SInvID'].AsInteger :=  dm3.quActAllowancesListSINVID.AsInteger;

end;

procedure TdmReport.ibdsSalesBeforeOpen(DataSet: TDataSet);
begin

 ibdsSales.ParamByName('BD').AsDateTime:=dm.UIDWHBD;
 ibdsSales.ParamByName('ED').AsDateTime:=dm.UIDWHED;
 ibdsSales.ParamByName('USERID').AsInteger:=dmcom.UserId;
 ibdsSales.ParamByName('COMPID').Asinteger:=dmcom.d_compid;
 ibdsSales.ParamByName('DEPID').Asinteger:=dm.SDepId;
 ibdsSales.ParamByName('SUPID').Asinteger:=dmCom.D_SupId;

end;


procedure TdmReport.quUidWhArtBeforeOpen(DataSet: TDataSet);
begin
 quUidWhArt.ParamByName('DEPID').AsInteger:=dm.SDepId;
 if dm.UIDWHType=3 then
  quUidWhArt.ParamByName('USERID').AsInteger:=UserDefault
 else
  quUidWhArt.ParamByName('USERID').AsInteger:=dmcom.UserId
end;

procedure TdmReport.quGrInsBeforeOpen(DataSet: TDataSet);
begin
 quGrIns.ParamByName('Art2Id').AsInteger:=quUidWhArtART2ID.AsInteger;
end;

procedure TdmReport.taUIDWHABeforeOpen(DataSet: TDataSet);
begin
   with TpFIBDataSet(DataSet), Params do
  begin
    if dm.UIDWHType=3 then ByName['USERID'].AsInteger:=UserDefault
      else  ByName['USERID'].AsInteger:=dmCom.UserId;
    if dmServ.WHDepId = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := dmServ.WHDepId;
      ByName['DEPID2'].AsInteger := dmServ.WHDepId;
    end;
    try
    Params.ByName['ART1'].AsString := dmCom.FilterArt;
    Params.ByName['ART2'].AsString := dmCom.FilterArt+'��';
    except
    end;

    try
     Params.ByName['ART21'].AsString := dm.FilterArt2;
     Params.ByName['ART22'].AsString := dm.FilterArt2+'��';
    except
    end;

    //====������ ���� �����
    if dm.PayType <> -1000 then
 begin
  case (DataSet.Tag) of
      {'taUIDWH'}1:
      begin
        SelectSQL[2]:='from  UIDWH_T u, Art2 a2, D_Dep d, sitem sit, sinfo inf, sinv s';
        SelectSQL[3] :=' where  u.UserId=:USERID and u.uid=sit.uid and sit.itype=1 and';
        SelectSQL[3] := SelectSQL[3] + ' sit.sinfoid=inf.sinfoid and inf.sinvid=s.sinvid and s.paytypeid='+IntToStr(dm.PayType) + ' and ';
        SelectSQL[36]:='PLAN SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),D INDEX (D_DEP_IDX1), SIT INDEX (SITEM_IDX3), INF INDEX (SINFO_IDX2), S INDEX (PK_SINV)))';
      end;
      {'taUIDWHA'}2:
      begin
       SelectSQL[2]:='from  UIDWH_T u, Art2 a2, d_comp c, sitem sit, sinfo inf, sinv s';
       SelectSQL[3] :=' where  u.UserId=:USERID and u.uid=sit.uid and sit.itype=1 and';
       SelectSQL[3] := SelectSQL[2] + ' sit.sinfoid=inf.sinfoid and inf.sinvid=s.sinvid and s.paytypeid='+IntToStr(dm.PayType) + ' and ';
       SelectSQL[37] :='PLAN SORT(SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),C INDEX (D_COMP_IDX1), SIT INDEX (SITEM_IDX3), INF INDEX (SINFO_IDX2), S INDEX (PK_SINV))))';
      end;
      {'taUIDWHB'}3:
      begin
       SelectSQL[2]:='c.name header$company, d.ssname body$department, u.sz body$size, g.name body$good  from  UIDWH_T u, Art2 a2, d_comp c, d_dep d, d_good g, sitem sit, sinfo inf, sinv s';
       SelectSQL[3] :=' where  u.UserId=:USERID and u.uid=sit.uid and sit.itype=1 and';
       SelectSQL[3] := SelectSQL[3] + ' sit.sinfoid=inf.sinfoid and inf.sinvid=s.sinvid and s.paytypeid='+IntToStr(dm.PayType) + ' and ';
      end;
      {'taUIDWHAu'}4:
      begin
       SelectSQL[2]:='from  UIDWH_T u, Art2 a2, d_comp c, D_comp c2, sitem sit, sinfo inf, sinv s';
       SelectSQL[3] :=' where  u.UserId=:USERID and u.uid=sit.uid and sit.itype=1 and';
       SelectSQL[3] := SelectSQL[3] + ' sit.sinfoid=inf.sinfoid and inf.sinvid=s.sinvid and s.paytypeid='+IntToStr(dm.PayType) + ' and ';
       SelectSQL[39]:='PLAN SORT(SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),C INDEX (D_COMP_IDX1),C2 INDEX (D_COMP_IDX1), SIT INDEX (SITEM_IDX3), INF INDEX (SINFO_IDX2), S INDEX (PK_SINV))))';
      end;
  end;
 end
 else
  begin
  case (DataSet.Tag) of
    {taUIDWH}1:
    begin
     SelectSQL[2]:='from  UIDWH_T u, Art2 a2, D_Dep d';
     SelectSQL[36]:='PLAN SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),D INDEX (D_DEP_IDX1)))';
    end;
    {taUIDWHA}2:
    begin
     SelectSQL[2]:='from  UIDWH_T u, Art2 a2, d_comp c';
     SelectSQL[3]:=' where  u.UserId=:USERID and ';
     SelectSQL[37] :='PLAN SORT(SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),C INDEX (D_COMP_IDX1))))';
    end;
    {taUIDWHB}3:
    begin
     SelectSQL[2]:='c.name header$company, d.ssname body$department, u.sz body$size, g.name body$good  from  UIDWH_T u, Art2 a2, d_comp c, d_dep d, d_good g';
     SelectSQL[3] :=' where  u.UserId=:USERID and ';
    end;
    {taUIDWHAu}4:
    begin
     SelectSQL[2]:='from  UIDWH_T u, Art2 a2, d_comp c, D_comp c2';
     SelectSQL[39]:='PLAN SORT(SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX2),C INDEX (D_COMP_IDX1),C2 INDEX (D_COMP_IDX1))))';
    end;
  end;
   if dm.Comission <> -1 then
   begin
     SelectSQL[3] :=' where  u.UserId=:USERID and';
     SelectSQL[3] := SelectSQL[3] + ' u.Comission = ' + IntToStr(dm.Comission) + ' and '
   end
   else
    SelectSQL[3] :=' where  u.UserId=:USERID and ';
  end;
    dmCom.SetArtFilter(Params);
  end;

end;

procedure TdmReport.ibdsBillItemAfterOpen(DataSet: TDataSet);
var
 i: Integer;
begin
{  ShowMessage(ibdsBillItem.SelectSQL.text);

  for i := 0 to ibdsBillItem.Params.Count - 1 do
  begin
    ShowMessage (ibdsBillItem.Params[i].Name + ' = ' + ibdsBillItem.Params[i].AsString);
  end;

  Form6.DBGridEh1.DataSource := dsrBillItem;
  Form6.ShowModal;}

end;

procedure TdmReport.ibdsBillItemBeforeOpen(DataSet: TDataSet);
begin
//ibdsBillItem.ParamByName('SINVID').asInteger:=ibdsFrom.FieldByName('SINVID').asInteger;
end;

procedure TdmReport.ibdsCardCheckBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
    begin
      if dm.SDepId=-1 then
        begin
          ByName['DEPID1'].AsInteger:=-MAXINT;
          ByName['DEPID2'].AsInteger:=MAXINT;
        end
      else
        begin
          ByName['DEPID1'].AsInteger:=dm.SDepId;
          ByName['DEPID2'].AsInteger:=dm.SDepId;
        end;

      ByName['BD'].AsTimeStamp:=DateTimeToTimeStamp(dm.BeginDate);
      ByName['ED'].AsTimeStamp:=DateTimeToTimeStamp(dm.EndDate);
    end;
end;


procedure TdmReport.frAnlzSelEmpBeforeOpen(DataSet: TDataSet);
begin
  frAnlzSelEmp.ParamByName('userid').AsInteger:=dmCom.UserID;
end;

procedure TdmReport.OnArt2Map(DataSet: TDataSet);
var
  art2: string;
  rart2: string;
  euid: string;
begin
  art2 := DataSet.FieldByName('EART2').AsString;

  euid := DataSet.FieldByName('EUID').AsString;

  rart2 := art2;

  if euid <> '' then
  begin
    rart2 := euid;
  end;

  DataSet.FieldByName('R_ART2').AsString := rart2;
end;

procedure TdmReport.OnArt2Map2(DataSet: TDataSet);
var
  art2: string;
  rart2: string;
  euid: string;
begin
  art2 := DataSet.FieldByName('EART2').AsString;

  euid := DataSet.FieldByName('EUID').AsString;

  rart2 := art2;

  if euid <> '' then
  begin
    rart2 := euid;
  end;

  DataSet.FieldByName('ART2').AsString := rart2;
end;

initialization
  Init;
  frRegisterFunctionLibrary(TfrAddLib);
end.


