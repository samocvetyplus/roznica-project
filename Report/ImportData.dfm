object dmImp: TdmImp
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 511
  Top = 229
  Height = 436
  Width = 405
  object quSInvItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.Sz Sz, it.W W'
      'from SItem it '
      'where it.SElId = :ItemLink '
      '          ')
    DataSource = dsrSInvEl
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 148
    Top = 68
    object quSInvItemSZ: TFIBStringField
      FieldName = 'SZ'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object quSInvItemW: TFloatField
      FieldName = 'W'
      Required = True
    end
  end
  object taSInvEl: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select g.Name Good, m.Name Mat, a2.Art Art, a2.Art2 Art2, a2.Uni' +
        'tId UnitId, e.PNDS PNDS, e.Akciz Akciz, e.Price Price, e.SelId I' +
        'temLink, a2.Art2Id InsLink'
      'from Sinv i, Sel e, D_Good g, D_Mat m, Art2 a2'
      'where i.SInvId = :SInvId and           '
      '          e.SInvId = i.SinvId and          '
      '          a2.Art2Id = e.Art2Id and'
      '          a2.D_GoodId = g.D_GoodId and'
      '          a2.D_MatId = m.D_MatId')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 20
    Top = 68
    object taSInvElGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object taSInvElMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object taSInvElART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taSInvElUNITID: TIntegerField
      FieldName = 'UNITID'
      Required = True
    end
    object taSInvElART2: TFIBStringField
      FieldName = 'ART2'
      Required = True
      EmptyStrToNull = True
    end
    object taSInvElPNDS: TFloatField
      FieldName = 'PNDS'
      Required = True
    end
    object taSInvElAKCIZ: TFloatField
      FieldName = 'AKCIZ'
    end
    object taSInvElPRICE: TFloatField
      FieldName = 'PRICE'
      Required = True
    end
    object taSInvElITEMLINK: TIntegerField
      FieldName = 'ITEMLINK'
      Required = True
    end
    object taSInvElINSLINK: TIntegerField
      FieldName = 'INSLINK'
      Required = True
    end
  end
  object dsrSInvEl: TDataSource
    DataSet = taSInvEl
    Left = 20
    Top = 120
  end
  object quIns: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select i.Quantity, i.Weight, i.Color, i.Chromaticity, i.Cleannes' +
        ', i.Gr, i.Shape, '
      '          e.Name ET, e1.Name ESh, i.D_InsId             '
      'from Ins I LEFT JOIN d_edgetion e on (e.edgetionId=I.edgtypeid)'
      
        '          left join d_edgetion e1 on (e1.edgetionId=i.edgshapeid' +
        ')'
      'where i.Art2Id = :InsLink and'
      '           i.D_InsId <> '#39'-'#39' '
      ''
      ''
      '    ')
    DataSource = dsrSInvEl
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 104
    Top = 68
    object quInsQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
    end
    object quInsWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object quInsCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object quInsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object quInsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object quInsGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object quInsSHAPE: TFIBStringField
      FieldName = 'SHAPE'
      EmptyStrToNull = True
    end
    object quInsET: TFIBStringField
      FieldName = 'ET'
      EmptyStrToNull = True
    end
    object quInsESH: TFIBStringField
      FieldName = 'ESH'
      EmptyStrToNull = True
    end
    object quInsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object quSumEl: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(it.W) TW, count(*) TC'
      'from SItem it'
      'where it.SelId = :ItemLink')
    DataSource = dsrSInvEl
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 208
    Top = 68
    object quSumElTW: TFloatField
      FieldName = 'TW'
    end
    object quSumElTC: TIntegerField
      FieldName = 'TC'
      Required = True
    end
  end
  object sqlImp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 312
    Top = 72
  end
  object quExportDepSell: TpFIBDataSet
    SelectSQL.Strings = (
      'select uid, w, sz, art2, art, d_compid, d_goodid, d_insid,'
      '       d_matid, unitid, d_countryid, att1, att2,'
      '       supid, ndsid, price, d_goods_sam1, d_goods_sam2,'
      '       insid, quantity, weigth, color, chromaticity,'
      '       cleannes, gr, main, edgtypeid, edgshapeid, price2,'
      '       ssf, Ndate, SELLPRICE, SELLPRICE0, SELLDISCOUNT,'
      '       SELLCLIENTID, SELLUNITID, SELLQ0, SELLW'
      'from ImportDepSell (:depid)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 316
    Top = 136
    poSQLINT64ToBCD = True
    object quExportDepSellUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quExportDepSellW: TFIBFloatField
      FieldName = 'W'
    end
    object quExportDepSellSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quExportDepSellART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quExportDepSellD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quExportDepSellD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quExportDepSellD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quExportDepSellATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quExportDepSellSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object quExportDepSellNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object quExportDepSellPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quExportDepSellD_GOODS_SAM1: TFIBIntegerField
      FieldName = 'D_GOODS_SAM1'
    end
    object quExportDepSellD_GOODS_SAM2: TFIBIntegerField
      FieldName = 'D_GOODS_SAM2'
    end
    object quExportDepSellINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellQUANTITY: TFIBSmallIntField
      FieldName = 'QUANTITY'
    end
    object quExportDepSellWEIGTH: TFIBFloatField
      FieldName = 'WEIGTH'
    end
    object quExportDepSellCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object quExportDepSellCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object quExportDepSellCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object quExportDepSellGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellMAIN: TFIBSmallIntField
      FieldName = 'MAIN'
    end
    object quExportDepSellEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepSellPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object quExportDepSellSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quExportDepSellNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object quExportDepSellSELLPRICE: TFIBFloatField
      FieldName = 'SELLPRICE'
    end
    object quExportDepSellSELLPRICE0: TFIBFloatField
      FieldName = 'SELLPRICE0'
    end
    object quExportDepSellSELLDISCOUNT: TFIBFloatField
      FieldName = 'SELLDISCOUNT'
    end
    object quExportDepSellSELLCLIENTID: TFIBIntegerField
      FieldName = 'SELLCLIENTID'
    end
    object quExportDepSellSELLUNITID: TFIBSmallIntField
      FieldName = 'SELLUNITID'
    end
    object quExportDepSellSELLQ0: TFIBFloatField
      FieldName = 'SELLQ0'
    end
    object quExportDepSellSELLW: TFIBFloatField
      FieldName = 'SELLW'
    end
  end
  object xmlDoc: TXMLDocument
    XML.Strings = (
      '<?xml version="1.0" encoding="UTF-8"?>'
      '<EXPORTDATA>'
      '</EXPORTDATA>'
      ''
      '')
    Left = 316
    Top = 188
    DOMVendorDesc = 'MSXML'
  end
  object quExportDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select uid, w, sz, art2, art, d_compid, d_goodid, d_insid,'
      '       d_matid, unitid, d_countryid, att1, att2,'
      '       supid, ndsid, price, d_goods_sam1, d_goods_sam2,'
      '       insid, quantity, weigth, color, chromaticity,'
      '       cleannes, gr, main, edgtypeid, edgshapeid, price2,'
      '       ssf, Ndate'
      'from ImportDepAll (:depid, :optsinvid)'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 248
    Top = 136
    poSQLINT64ToBCD = True
    object quExportDepUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object quExportDepW: TFIBFloatField
      FieldName = 'W'
    end
    object quExportDepSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quExportDepART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quExportDepD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object quExportDepD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quExportDepD_COUNTRYID: TFIBStringField
      FieldName = 'D_COUNTRYID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object quExportDepATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object quExportDepSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object quExportDepNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object quExportDepPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object quExportDepD_GOODS_SAM1: TFIBIntegerField
      FieldName = 'D_GOODS_SAM1'
    end
    object quExportDepD_GOODS_SAM2: TFIBIntegerField
      FieldName = 'D_GOODS_SAM2'
    end
    object quExportDepINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepQUANTITY: TFIBSmallIntField
      FieldName = 'QUANTITY'
    end
    object quExportDepWEIGTH: TFIBFloatField
      FieldName = 'WEIGTH'
    end
    object quExportDepCOLOR: TFIBStringField
      FieldName = 'COLOR'
      EmptyStrToNull = True
    end
    object quExportDepCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      EmptyStrToNull = True
    end
    object quExportDepCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      EmptyStrToNull = True
    end
    object quExportDepGR: TFIBStringField
      FieldName = 'GR'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepMAIN: TFIBSmallIntField
      FieldName = 'MAIN'
    end
    object quExportDepEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object quExportDepPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object quExportDepSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object quExportDepNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
  end
  object quImportSell: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    SQL.Strings = (
      'execute procedure CreateSell(:uid, :Bd, :userid,'
      ' :SELLPRICE, :SELLPRICE0, :SELLDISCOUNT,'
      ' :SELLCLIENTID, :SELLUNITID, :SELLQ0, :SELLW)')
    Left = 248
    Top = 200
  end
  object quImportArtDep: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ARTID, ART2ID, INSERTA2'
      'FROM IMPORT_REST (:UID, :COMPID, :MATID, :GOODID,'
      '     :INSID, :COUNTRYID, :ATT1ID, :ATT2ID, :UNITID,'
      '     :D_GOODS_SAM1, :D_GOODS_SAM2, :ART, :ART2,'
      '     :W, :SZ, :PRICE, :PRICE2, :SUPID, :NDSID,'
      '     :SSF, :Ndate, :DEPID, :USERID, :I_SDATE)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 168
    Top = 136
    poSQLINT64ToBCD = True
    object quImportArtDepARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object quImportArtDepART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quImportArtDepINSERTA2: TFIBSmallIntField
      FieldName = 'INSERTA2'
    end
  end
end
