object dmReport: TdmReport
  OldCreateOrder = False
  Height = 798
  Width = 1114
  object ibdsOrder: TpFIBDataSet
    SelectSQL.Strings = (
      'select D.Name R_DepName, D.SName R_DepCode, '
      
        '           P.SetDate R_SnDate, 0 R_Sn, P.Prord R_NoOrder, P.Pror' +
        'dId R_Link, P.SetDate R_SetDate, '#39#39' R_COMP'
      'from Prord P, D_Dep D'
      'where P.InvId=:I_PrordId and P.DepId=D.D_DepId;')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 24
    Top = 64
    object ibdsOrderR_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      EmptyStrToNull = True
    end
    object ibdsOrderR_DEPCODE: TFIBStringField
      FieldName = 'R_DEPCODE'
      EmptyStrToNull = True
    end
    object ibdsOrderR_SNDATE: TDateTimeField
      FieldName = 'R_SNDATE'
    end
    object ibdsOrderR_SN: TIntegerField
      DisplayWidth = 10
      FieldName = 'R_SN'
    end
    object ibdsOrderR_NOORDER: TIntegerField
      FieldName = 'R_NOORDER'
    end
    object ibdsOrderR_LINK: TIntegerField
      FieldName = 'R_LINK'
    end
    object ibdsOrderR_SETDATE: TDateTimeField
      FieldName = 'R_SETDATE'
    end
    object ibdsOrderR_COMP: TFIBStringField
      FieldName = 'R_COMP'
      Size = 0
      EmptyStrToNull = True
    end
  end
  object ibdsOrderItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from OrderItem(:R_Link)'
      'order by R_Art, R_art2')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrOrder
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 64
    object ibdsOrderItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsOrderItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsOrderItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsOrderItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsOrderItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsOrderItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
  end
  object trReport: TpFIBTransaction
    DefaultDatabase = dmCom.db
    TimeoutAction = TACommitRetaining
    TRParams.Strings = (
      'nowait'
      'rec_version'
      'read_committed')
    TPBMode = tpbDefault
    Left = 175
    Top = 9
  end
  object frdsOrder: TfrDBDataSet
    CloseDataSource = True
    DataSet = ibdsOrder
    Left = 24
    Top = 160
  end
  object frdsOrderItem: TfrDBDataSet
    DataSet = ibdsOrderItem
    Left = 88
    Top = 112
  end
  object frdsSupplier: TfrDBDataSet
    DataSet = ibdsSupplier
    Left = 561
    Top = 256
  end
  object ibdsSupplier: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.Name, stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39 +
        '||stretrim(c.Address) Address, c.Phone, c.Bill, c.KBill, c.BIK, ' +
        'c.Bank, '
      
        '          c.INN, c.OKONH, c.OKPO, s.Sn V_Sn, s.NDate, s.SDate, s' +
        '.SSF, c.kpp, s.SINVID'
      'from d_comp c, sinv s'
      'where s.SInvId=:P_Sn and c.D_CompId=s.SupId')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 561
    Top = 209
    object ibdsSupplierNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsSupplierADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 770
      EmptyStrToNull = True
    end
    object ibdsSupplierPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsSupplierBILL: TFIBStringField
      FieldName = 'BILL'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsSupplierBIK: TFIBStringField
      FieldName = 'BIK'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsSupplierKBILL: TFIBStringField
      FieldName = 'KBILL'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsSupplierINN: TFIBStringField
      FieldName = 'INN'
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsSupplierBANK: TFIBStringField
      FieldName = 'BANK'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsSupplierOKONH: TFIBStringField
      FieldName = 'OKONH'
      EmptyStrToNull = True
    end
    object ibdsSupplierOKPO: TFIBStringField
      FieldName = 'OKPO'
      EmptyStrToNull = True
    end
    object ibdsSupplierNDATE: TDateTimeField
      FieldName = 'NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object ibdsSupplierV_SN: TIntegerField
      FieldName = 'V_SN'
    end
    object ibdsSupplierSDATE: TDateTimeField
      FieldName = 'SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object ibdsSupplierSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
  end
  object ibdsCustomer: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.Name, stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39 +
        '||stretrim(c.Address) Address,'
      
        '       stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39'||stretr' +
        'im(c.PAddress) PAddress, c.Phone, c.Bill,'
      
        '       c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn, s.N' +
        'Date, t.contract$date DOGBD, t.number  NDOG, ct.name ContractNam' +
        'e'
      'from d_comp c, d_rec r, sinv s'
      'left join contract t on s.contract$id = t.id'
      'left join contract$types ct on (ct.id = t.contract$type)'
      
        'where s.SInvId=:P_SN and c.D_CompId=s.CompId and c.address$print' +
        ' = 1'
      ''
      'union all'
      ''
      
        'select c.Name, stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39 +
        '||stretrim(c.PAddress) Address,'
      
        '       stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39'||stretr' +
        'im(c.PAddress) PAddress, c.Phone, c.Bill,'
      
        '       c.KBill, c.BIK, c.Bank, c.INN, c.OKONH, c.OKPO, s.Sn, s.N' +
        'Date, t.contract$date DOGBD, t.number  NDOG, ct.name ContractNam' +
        'e'
      'from d_comp c, d_rec r, sinv s'
      'left join contract t on s.contract$id = t.id'
      'left join contract$types ct on (ct.id = t.contract$type)'
      
        'where s.SInvId=:P_SN and c.D_CompId=s.CompId and c.address$print' +
        ' = 2')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 638
    Top = 208
    object ibdsCustomerNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsCustomerADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 770
      EmptyStrToNull = True
    end
    object ibdsCustomerPADDRESS: TFIBStringField
      FieldName = 'PADDRESS'
      Size = 770
      EmptyStrToNull = True
    end
    object ibdsCustomerPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsCustomerBILL: TFIBStringField
      FieldName = 'BILL'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsCustomerKBILL: TFIBStringField
      FieldName = 'KBILL'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsCustomerBIK: TFIBStringField
      FieldName = 'BIK'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsCustomerBANK: TFIBStringField
      FieldName = 'BANK'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsCustomerINN: TFIBStringField
      FieldName = 'INN'
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsCustomerOKONH: TFIBStringField
      FieldName = 'OKONH'
      EmptyStrToNull = True
    end
    object ibdsCustomerOKPO: TFIBStringField
      FieldName = 'OKPO'
      EmptyStrToNull = True
    end
    object ibdsCustomerSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object ibdsCustomerNDATE: TFIBDateTimeField
      FieldName = 'NDATE'
    end
    object ibdsCustomerDOGBD: TFIBDateTimeField
      FieldName = 'DOGBD'
    end
    object ibdsCustomerNDOG: TFIBStringField
      FieldName = 'NDOG'
      Size = 32
      EmptyStrToNull = True
    end
    object ibdsCustomerCONTRACTNAME: TFIBStringField
      FieldName = 'CONTRACTNAME'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object frdsCustomer: TfrDBDataSet
    DataSet = ibdsCustomer
    Left = 639
    Top = 256
  end
  object dsrBillItem: TDataSource
    DataSet = ibdsBillItem
    Left = 24
    Top = 256
  end
  object ibdsBillItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from doc_Bill(:ID, :KIND)')
    AfterOpen = ibdsBillItemAfterOpen
    BeforeOpen = ibdsBillItemBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 24
    Top = 208
    object ibdsBillItemR_SELID: TIntegerField
      FieldName = 'R_SELID'
    end
    object ibdsBillItemR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_ART2ID: TIntegerField
      FieldName = 'R_ART2ID'
    end
    object ibdsBillItemR_CUSTDECL: TFIBStringField
      FieldName = 'R_CUSTDECL'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsBillItemR_Q: TIntegerField
      FieldName = 'R_Q'
    end
    object ibdsBillItemR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsBillItemR_PRICEWONDS: TFloatField
      FieldName = 'R_PRICEWONDS'
    end
    object ibdsBillItemR_COSTWONDS: TFloatField
      FieldName = 'R_COSTWONDS'
    end
    object ibdsBillItemR_EXCISE: TFIBStringField
      FieldName = 'R_EXCISE'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_PNDS: TFloatField
      FieldName = 'R_PNDS'
    end
    object ibdsBillItemR_TOTALNDS: TFloatField
      FieldName = 'R_TOTALNDS'
    end
    object ibdsBillItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsBillItemR_COST: TFloatField
      FieldName = 'R_COST'
    end
    object ibdsBillItemR_TR: TFloatField
      FieldName = 'R_TR'
    end
    object ibdsBillItemR_PRICE2: TFloatField
      FieldName = 'R_PRICE2'
    end
    object ibdsBillItemR_COST2: TFloatField
      FieldName = 'R_COST2'
    end
    object ibdsBillItemR_UID: TIntegerField
      FieldName = 'R_UID'
    end
    object ibdsBillItemR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemR_ITCOST: TFloatField
      FieldName = 'R_ITCOST'
    end
    object ibdsBillItemR_ITW: TFloatField
      FieldName = 'R_ITW'
    end
    object ibdsBillItemR_NDATE: TDateTimeField
      FieldName = 'R_NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object ibdsBillItemR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      EmptyStrToNull = True
    end
    object ibdsBillItemR_PRICE2WONDS: TFloatField
      FieldName = 'R_PRICE2WONDS'
    end
    object ibdsBillItemR_COST2WONDS: TFloatField
      FieldName = 'R_COST2WONDS'
    end
    object ibdsBillItemR_TOTAL2NDS: TFloatField
      FieldName = 'R_TOTAL2NDS'
    end
    object ibdsBillItemR_ITCOST2: TFloatField
      FieldName = 'R_ITCOST2'
      Origin = 'DOC_BILL.R_ITCOST2'
    end
    object ibdsBillItemR_RATEURF: TFloatField
      FieldName = 'R_RATEURF'
      Origin = 'DOC_BILL.R_RATEURF'
    end
    object ibdsBillItemR_AKCIZ: TFloatField
      FieldName = 'R_AKCIZ'
      Origin = 'DOC_BILL.R_AKCIZ'
    end
    object ibdsBillItemR_UID_SUP: TIntegerField
      FieldName = 'R_UID_SUP'
      Origin = 'DOC_BILL.R_UID_SUP'
    end
    object ibdsBillItemR_TRNDS: TFIBFloatField
      FieldName = 'R_TRNDS'
    end
    object ibdsBillItemR_RETNAME: TFIBStringField
      FieldName = 'R_RETNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frdsBillItem: TfrDBDataSet
    DataSource = dsrBillItem
    Left = 24
    Top = 304
  end
  object ibdsTagWI: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.*, u.flag, u.flag$char, u.euid, t.r_art2 eart2'
      'from tmp_tag t left join uid_info u on t.r_uid = u.uid'
      'where t.R_Ins=1 and t.userid=:userid'
      'order by t.r_art, t.r_art2, t.R_UID')
    OnCalcFields = OnArt2Map
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 23
    Top = 351
    object ibdsTagWIR_COMP: TFIBStringField
      FieldName = 'R_COMP'
      Origin = 'TAG.R_COMP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      Origin = 'TAG.R_MAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      Origin = 'TAG.R_GOOD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_ART: TFIBStringField
      FieldName = 'R_ART'
      Origin = 'TAG.R_ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_ART2: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'R_ART2'
      Origin = 'TAG.R_ART2'
      FixedChar = True
      EmptyStrToNull = True
      Calculated = True
    end
    object ibdsTagWIR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      Origin = 'TAG.R_COUNTRY'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_SIZE: TFIBStringField
      FieldName = 'R_SIZE'
      Origin = 'TAG.R_SIZE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTagWIR_W: TFloatField
      FieldName = 'R_W'
      Origin = 'TAG.R_W'
    end
    object ibdsTagWIR_PRICE: TFloatField
      FieldName = 'R_PRICE'
      Origin = 'TAG.R_PRICE'
    end
    object ibdsTagWIR_UID: TIntegerField
      FieldName = 'R_UID'
      Origin = 'TAG.R_UID'
    end
    object ibdsTagWIR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
      Origin = 'TAG.R_UNITID'
    end
    object ibdsTagWIR_LINK: TIntegerField
      FieldName = 'R_LINK'
      Origin = 'TAG.R_LINK'
    end
    object ibdsTagWIR_INS: TFIBStringField
      FieldName = 'R_INS'
      Origin = 'TAG.R_INS'
      Size = 80
      EmptyStrToNull = True
    end
    object ibdsTagWIR_DEPID: TIntegerField
      FieldName = 'R_DEPID'
      Origin = 'TAG.R_DEPID'
    end
    object ibdsTagWIR_DEP: TFIBStringField
      FieldName = 'R_DEP'
      Origin = 'TAG.R_DEP'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTagWIR_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      Origin = 'TAG.R_DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTagWIR_CITY: TFIBStringField
      FieldName = 'R_CITY'
      Origin = 'TAG.R_CITY'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWIR_ADDRESS: TFIBStringField
      FieldName = 'R_ADDRESS'
      Origin = 'TAG.R_ADDRESS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTagWIR_ISINS: TIntegerField
      FieldName = 'R_ISINS'
      Origin = 'TAG.R_ISINS'
    end
    object ibdsTagWIR_SITEMID: TIntegerField
      FieldName = 'R_SITEMID'
      Origin = 'TAG.R_SITEMID'
    end
    object ibdsTagWIR_ISNEWPRICE: TIntegerField
      FieldName = 'R_ISNEWPRICE'
      Origin = 'TAG.R_ISNEWPRICE'
    end
    object ibdsTagWIR_DATE: TDateTimeField
      FieldName = 'R_DATE'
      Origin = 'TAG.R_DATE'
    end
    object ibdsTagWIFLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object ibdsTagWIFLAGCHAR: TFIBStringField
      FieldName = 'FLAG$CHAR'
      Size = 1
      EmptyStrToNull = True
    end
    object ibdsTagWIEUID: TFIBStringField
      FieldName = 'EUID'
      Size = 32
      EmptyStrToNull = True
    end
    object ibdsTagWIART2: TFIBStringField
      FieldName = 'EART2'
      EmptyStrToNull = True
    end
  end
  object frdsTagWI: TfrDBDataSet
    DataSet = ibdsTagWI
    Left = 24
    Top = 456
  end
  object ibdsIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from TagIns(:R_Uid)')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrTagWI
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 256
    object ibdsInsR_UID: TIntegerField
      FieldName = 'R_UID'
    end
    object ibdsInsR_ETYPE: TFIBStringField
      FieldName = 'R_ETYPE'
      EmptyStrToNull = True
    end
    object ibdsInsR_INSCODE: TFIBStringField
      FieldName = 'R_INSCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsInsR_ESHAPE: TFIBStringField
      FieldName = 'R_ESHAPE'
      EmptyStrToNull = True
    end
    object ibdsInsR_Q: TIntegerField
      FieldName = 'R_Q'
    end
    object ibdsInsR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsInsR_COLOR: TFIBStringField
      FieldName = 'R_COLOR'
      EmptyStrToNull = True
    end
    object ibdsInsR_CHROMATICITY: TFIBStringField
      FieldName = 'R_CHROMATICITY'
      EmptyStrToNull = True
    end
    object ibdsInsR_CLEANNES: TFIBStringField
      FieldName = 'R_CLEANNES'
      EmptyStrToNull = True
    end
    object ibdsInsR_GROUP: TFIBStringField
      FieldName = 'R_GROUP'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object frdsIns: TfrDBDataSet
    DataSet = ibdsIns
    Left = 88
    Top = 304
  end
  object ibdsReduce: TpFIBDataSet
    SelectSQL.Strings = (
      'select P.PrordId R_Link,  P.Prord R_No, P.SetDate  R_Date'
      'from Prord P'
      'where P.PrordId=:I_PrordId')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 198
    Top = 256
    object ibdsReduceR_LINK: TIntegerField
      FieldName = 'R_LINK'
      Required = True
    end
    object ibdsReduceR_DATE: TDateTimeField
      FieldName = 'R_DATE'
    end
    object ibdsReduceR_NO: TIntegerField
      FieldName = 'R_NO'
    end
  end
  object frdsReduce: TfrDBDataSet
    DataSet = ibdsReduce
    Left = 177
    Top = 352
  end
  object ibdsReduceItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from ReduceItem(:R_Link, :R_Code)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsrReduce
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 199
    Top = 160
    object ibdsReduceItemR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsReduceItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsReduceItemR_SIZE: TFIBStringField
      FieldName = 'R_SIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_ASSAY: TFIBStringField
      FieldName = 'R_ASSAY'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsReduceItemR_Q: TIntegerField
      FieldName = 'R_Q'
    end
    object ibdsReduceItemR_COMPCODE: TFIBStringField
      FieldName = 'R_COMPCODE'
      EmptyStrToNull = True
    end
    object ibdsReduceItemR_COMPSNAME: TFIBStringField
      FieldName = 'R_COMPSNAME'
      EmptyStrToNull = True
    end
  end
  object frdsReduceItem: TfrDBDataSet
    DataSet = ibdsReduceItem
    Left = 199
    Top = 208
  end
  object ibdsTagWOI: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.*, u.flag, u.flag$char, u.euid, t.r_art2 eart2'
      'from tmp_Tag t left join uid_info u on t.r_uid = u.uid'
      'where t.R_IsIns=-1 and t.userid=:userid'
      'order by t.r_art, t.r_art2')
    OnCalcFields = OnArt2Map
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 352
    object ibdsTagWOIR_COMP: TFIBStringField
      FieldName = 'R_COMP'
      Origin = 'TAG.R_COMP'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      Origin = 'TAG.R_MAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      Origin = 'TAG.R_GOOD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_ART: TFIBStringField
      FieldName = 'R_ART'
      Origin = 'TAG.R_ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_ART2: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'R_ART2'
      Origin = 'TAG.R_ART2'
      FixedChar = True
      EmptyStrToNull = True
      Calculated = True
    end
    object ibdsTagWOIR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      Origin = 'TAG.R_COUNTRY'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_SIZE: TFIBStringField
      FieldName = 'R_SIZE'
      Origin = 'TAG.R_SIZE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_W: TFloatField
      FieldName = 'R_W'
      Origin = 'TAG.R_W'
    end
    object ibdsTagWOIR_PRICE: TFloatField
      FieldName = 'R_PRICE'
      Origin = 'TAG.R_PRICE'
    end
    object ibdsTagWOIR_UID: TIntegerField
      FieldName = 'R_UID'
      Origin = 'TAG.R_UID'
    end
    object ibdsTagWOIR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
      Origin = 'TAG.R_UNITID'
    end
    object ibdsTagWOIR_LINK: TIntegerField
      FieldName = 'R_LINK'
      Origin = 'TAG.R_LINK'
    end
    object ibdsTagWOIR_INS: TFIBStringField
      FieldName = 'R_INS'
      Origin = 'TAG.R_INS'
      Size = 80
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_DEPID: TIntegerField
      FieldName = 'R_DEPID'
      Origin = 'TAG.R_DEPID'
    end
    object ibdsTagWOIR_DEP: TFIBStringField
      FieldName = 'R_DEP'
      Origin = 'TAG.R_DEP'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      Origin = 'TAG.R_DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_CITY: TFIBStringField
      FieldName = 'R_CITY'
      Origin = 'TAG.R_CITY'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_ADDRESS: TFIBStringField
      FieldName = 'R_ADDRESS'
      Origin = 'TAG.R_ADDRESS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTagWOIR_ISINS: TIntegerField
      FieldName = 'R_ISINS'
      Origin = 'TAG.R_ISINS'
    end
    object ibdsTagWOIR_SITEMID: TIntegerField
      FieldName = 'R_SITEMID'
      Origin = 'TAG.R_SITEMID'
    end
    object ibdsTagWOIR_ISNEWPRICE: TIntegerField
      FieldName = 'R_ISNEWPRICE'
      Origin = 'TAG.R_ISNEWPRICE'
    end
    object ibdsTagWOIR_DATE: TDateTimeField
      FieldName = 'R_DATE'
      Origin = 'TAG.R_DATE'
    end
    object ibdsTagWOIFLAG: TFIBIntegerField
      FieldName = 'FLAG'
    end
    object ibdsTagWOIFLAGCHAR: TFIBStringField
      FieldName = 'FLAG$CHAR'
      Size = 1
      EmptyStrToNull = True
    end
    object ibdsTagWOIEUID: TFIBStringField
      FieldName = 'EUID'
      Size = 32
      EmptyStrToNull = True
    end
    object ibdsTagWOIART2: TFIBStringField
      FieldName = 'EART2'
      EmptyStrToNull = True
    end
  end
  object frdsTagWOI: TfrDBDataSet
    DataSet = ibdsTagWOI
    Left = 88
    Top = 400
  end
  object frReport: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    OnBeginDoc = frReportBeginDoc
    OnUserFunction = frReportUserFunction
    Left = 88
    Top = 9
    ReportForm = {19000000}
  end
  object ibdsGrIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from InsByArt2(:R_Art2Id)')
    AfterOpen = ibdsGrInsAfterOpen
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrBillItem
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 160
  end
  object frdsGrIns: TfrDBDataSet
    DataSet = ibdsGrIns
    Left = 88
    Top = 208
  end
  object ibdsFrom: TpFIBDataSet
    SelectSQL.Strings = (
      'select SN, NDATE, NAME, CITY, ADDRESS, PHONE, SINVID'
      'from DOC_PROVIDER(:ID, :KIND)')
    AfterOpen = ibdsFromAfterOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 391
    Top = 9
    object ibdsFromSN: TIntegerField
      FieldName = 'SN'
      Origin = 'DOC_PROVIDER.SN'
    end
    object ibdsFromNDATE: TDateTimeField
      FieldName = 'NDATE'
      Origin = 'DOC_PROVIDER.NDATE'
    end
    object ibdsFromNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROVIDER.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsFromCITY: TFIBStringField
      FieldName = 'CITY'
      Origin = 'DOC_PROVIDER.CITY'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsFromADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_PROVIDER.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsFromPHONE: TFIBStringField
      FieldName = 'PHONE'
      Origin = 'DOC_PROVIDER.PHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsFromSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
  end
  object ibdsTo: TpFIBDataSet
    SelectSQL.Strings = (
      'select NAME,  ADDRESS, PHONE, CITY '
      'from DOC_PROVIDER(:ID, :KIND)'
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 9
    object ibdsToNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROVIDER.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsToADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_PROVIDER.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsToPHONE: TFIBStringField
      FieldName = 'PHONE'
      Origin = 'DOC_PROVIDER.PHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object ibdsToCITY: TFIBStringField
      FieldName = 'CITY'
      Origin = 'DOC_PROVIDER.CITY'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
  end
  object ibdsBillUItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from doc_Bill(:ID, :KIND)'
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 177
    Top = 400
    object ibdsBillUItemR_SELID: TIntegerField
      FieldName = 'R_SELID'
    end
    object ibdsBillUItemR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_ART2ID: TIntegerField
      FieldName = 'R_ART2ID'
    end
    object ibdsBillUItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_CUSTDECL: TFIBStringField
      FieldName = 'R_CUSTDECL'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsBillUItemR_Q: TIntegerField
      FieldName = 'R_Q'
    end
    object ibdsBillUItemR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsBillUItemR_PRICEWONDS: TFloatField
      FieldName = 'R_PRICEWONDS'
    end
    object ibdsBillUItemR_COSTWONDS: TFloatField
      FieldName = 'R_COSTWONDS'
    end
    object ibdsBillUItemR_EXCISE: TFIBStringField
      FieldName = 'R_EXCISE'
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_PNDS: TFloatField
      FieldName = 'R_PNDS'
    end
    object ibdsBillUItemR_TOTALNDS: TFloatField
      FieldName = 'R_TOTALNDS'
    end
    object ibdsBillUItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsBillUItemR_COST: TFloatField
      FieldName = 'R_COST'
    end
    object ibdsBillUItemR_TR: TFloatField
      FieldName = 'R_TR'
    end
    object ibdsBillUItemR_PRICE2: TFloatField
      FieldName = 'R_PRICE2'
    end
    object ibdsBillUItemR_COST2: TFloatField
      FieldName = 'R_COST2'
    end
    object ibdsBillUItemR_UID: TIntegerField
      FieldName = 'R_UID'
    end
    object ibdsBillUItemR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_ITCOST: TFloatField
      FieldName = 'R_ITCOST'
    end
    object ibdsBillUItemR_ITW: TFloatField
      FieldName = 'R_ITW'
      Origin = 'DOC_BILL.R_ITW'
    end
    object ibdsBillUItemR_PRICE2WONDS: TFloatField
      FieldName = 'R_PRICE2WONDS'
      Origin = 'DOC_BILL.R_PRICE2WONDS'
    end
    object ibdsBillUItemR_COST2WONDS: TFloatField
      FieldName = 'R_COST2WONDS'
      Origin = 'DOC_BILL.R_COST2WONDS'
    end
    object ibdsBillUItemR_TOTAL2NDS: TFloatField
      FieldName = 'R_TOTAL2NDS'
      Origin = 'DOC_BILL.R_TOTAL2NDS'
    end
    object ibdsBillUItemR_NDATE: TDateTimeField
      FieldName = 'R_NDATE'
      Origin = 'DOC_BILL.R_NDATE'
    end
    object ibdsBillUItemR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      Origin = 'DOC_BILL.R_SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsBillUItemR_ITCOST2: TFloatField
      FieldName = 'R_ITCOST2'
      Origin = 'DOC_BILL.R_ITCOST2'
    end
    object ibdsBillUItemR_RATEURF: TFloatField
      FieldName = 'R_RATEURF'
      Origin = 'DOC_BILL.R_RATEURF'
    end
    object ibdsBillUItemR_AKCIZ: TFloatField
      FieldName = 'R_AKCIZ'
      Origin = 'DOC_BILL.R_AKCIZ'
    end
    object ibdsBillUItemR_UID_SUP: TIntegerField
      FieldName = 'R_UID_SUP'
      Origin = 'DOC_BILL.R_UID_SUP'
    end
  end
  object frdsBillUItem: TfrDBDataSet
    DataSet = ibdsBillUItem
    Left = 177
    Top = 448
  end
  object frdsUIDWH: TfrDBDataSet
    DataSet = ibdsUIDWH
    Left = 711
    Top = 204
  end
  object ibdsUIDWH: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT *'
      'FROM UIDWH_S(?DEPID1, ?DEPID2)'
      'order by art')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 711
    Top = 160
    object ibdsUIDWHSITEMID: TIntegerField
      FieldName = 'SITEMID'
    end
    object ibdsUIDWHUID: TIntegerField
      FieldName = 'UID'
    end
    object ibdsUIDWHPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsUIDWHART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsUIDWHUNITID: TIntegerField
      FieldName = 'UNITID'
    end
    object ibdsUIDWHNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object ibdsUIDWHNDSVALUE: TFloatField
      FieldName = 'NDSVALUE'
    end
    object ibdsUIDWHNDSID: TIntegerField
      FieldName = 'NDSID'
    end
    object ibdsUIDWHPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ibdsUIDWHW: TFloatField
      FieldName = 'W'
    end
    object ibdsUIDWHSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHDEP: TFIBStringField
      FieldName = 'DEP'
      EmptyStrToNull = True
    end
    object ibdsUIDWHSUPCODE: TFIBStringField
      FieldName = 'SUPCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWHCOST: TFloatField
      FieldName = 'COST'
    end
    object ibdsUIDWHART2ID: TIntegerField
      FieldName = 'ART2ID'
    end
    object ibdsUIDWHDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object ibdsUIDWHCOLOR: TIntegerField
      FieldName = 'COLOR'
    end
    object ibdsUIDWHSN0: TIntegerField
      FieldName = 'SN0'
    end
    object ibdsUIDWHSSF0: TFIBStringField
      FieldName = 'SSF0'
      EmptyStrToNull = True
    end
    object ibdsUIDWHSPRICE0: TFloatField
      FieldName = 'SPRICE0'
    end
    object ibdsUIDWHSUPID0: TIntegerField
      FieldName = 'SUPID0'
    end
    object ibdsUIDWHSDATE0: TDateTimeField
      FieldName = 'SDATE0'
    end
    object ibdsUIDWHSN: TIntegerField
      FieldName = 'SN'
    end
    object ibdsUIDWHSDATE: TDateTimeField
      FieldName = 'SDATE'
    end
    object ibdsUIDWHSELLDATE: TDateTimeField
      FieldName = 'SELLDATE'
    end
    object ibdsUIDWHT: TSmallintField
      FieldName = 'T'
    end
    object ibdsUIDWHMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
  end
  object frdsDep: TfrDBDataSet
    DataSource = dsrDep
    Left = 88
    Top = 552
  end
  object frDesigner: TfrDesigner
    OnSaveReport = frDesignerSaveReport
    Left = 24
    Top = 9
  end
  object dsrActOV: TDataSource
    DataSet = ibdsActOV
    Left = 487
    Top = 112
  end
  object ibdsActOV: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_DEPNAME, R_DEPCODE, R_NOORDER, R_LINK,'
      '          R_SETDATE, R_NACT'
      'from Doc_ActOV(:P_Id, :P_Kind) '
      ''
      ''
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 64
    object ibdsActOVR_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      EmptyStrToNull = True
    end
    object ibdsActOVR_DEPCODE: TFIBStringField
      FieldName = 'R_DEPCODE'
      EmptyStrToNull = True
    end
    object ibdsActOVR_NOORDER: TIntegerField
      FieldName = 'R_NOORDER'
    end
    object ibdsActOVR_LINK: TIntegerField
      FieldName = 'R_LINK'
      Required = True
    end
    object ibdsActOVR_SETDATE: TDateTimeField
      FieldName = 'R_SETDATE'
    end
    object ibdsActOVR_NACT: TIntegerField
      FieldName = 'R_NACT'
      Origin = 'DOC_ACTOV.R_NACT'
    end
  end
  object frdsActOV: TfrDBDataSet
    DataSource = dsrActOV
    Left = 487
    Top = 160
  end
  object ibdsActOVItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from PrordItem_S(:R_Link)'
      'where RQ<>0')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrActOV
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 559
    Top = 64
  end
  object frdsActOVitem: TfrDBDataSet
    DataSet = ibdsActOVItem
    Left = 559
    Top = 108
  end
  object dsrAppl: TDataSource
    DataSet = ibdsAppl
    Left = 335
    Top = 401
  end
  object frdsAppl: TfrDBDataSet
    DataSource = dsrAppl
    Left = 335
    Top = 449
  end
  object ibdsDisv: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select ClientName, Cost, BOSS, BUH, SELFNAME, BOSSPOST, paspser,' +
        ' paspnum, paspdistrdate, paspdistrorg '
      'from Doc_Ret(:P_Id, :P_Kind)'
      '          '
      '         '
      '        '
      '   '
      ''
      '         '
      '        '
      '   ')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 710
    Top = 347
    object ibdsDisvCOST: TFloatField
      FieldName = 'COST'
    end
    object ibdsDisvCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsDisvBOSS: TFIBStringField
      FieldName = 'BOSS'
      Origin = 'DOC_RET.BOSS'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsDisvBUH: TFIBStringField
      FieldName = 'BUH'
      Origin = 'DOC_RET.BUH'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsDisvSELFNAME: TFIBStringField
      FieldName = 'SELFNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsDisvBOSSPOST: TFIBStringField
      FieldName = 'BOSSPOST'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsDisvPASPSER: TFIBStringField
      FieldName = 'PASPSER'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsDisvPASPNUM: TFIBStringField
      FieldName = 'PASPNUM'
      EmptyStrToNull = True
    end
    object ibdsDisvPASPDISTRDATE: TFIBDateField
      FieldName = 'PASPDISTRDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object ibdsDisvPASPDISTRORG: TFIBStringField
      FieldName = 'PASPDISTRORG'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsrDisv: TDataSource
    DataSet = ibdsDisv
    Left = 710
    Top = 394
  end
  object frdsDisv: TfrDBDataSet
    DataSource = dsrDisv
    Left = 710
    Top = 440
  end
  object ibdsAppl: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select ClientName, UID, W, Sz, Good, Price, Art, Art2, Mat, Unit' +
        'Id, '
      
        '          SellDate, Cost, SN, CRDATE,  Wasret, Descr, CheckNo, A' +
        'ddress,'
      '          EMPNAME '
      'from Doc_Ret(:P_Id, :P_Kind)'
      '      '
      '   ')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 335
    Top = 352
    object ibdsApplUNITID: TIntegerField
      FieldName = 'UNITID'
      Required = True
    end
    object ibdsApplMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object ibdsApplART2: TFIBStringField
      FieldName = 'ART2'
      Required = True
      EmptyStrToNull = True
    end
    object ibdsApplART: TFIBStringField
      FieldName = 'ART'
      Required = True
      EmptyStrToNull = True
    end
    object ibdsApplPRICE: TFloatField
      Alignment = taLeftJustify
      FieldName = 'PRICE'
    end
    object ibdsApplGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object ibdsApplW: TFloatField
      Alignment = taLeftJustify
      FieldName = 'W'
      Required = True
    end
    object ibdsApplSZ: TFIBStringField
      FieldName = 'SZ'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplUID: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'UID'
    end
    object ibdsApplCOST: TFloatField
      FieldName = 'COST'
    end
    object ibdsApplSELLDATE: TDateTimeField
      FieldName = 'SELLDATE'
    end
    object ibdsApplCRDATE: TDateTimeField
      FieldName = 'CRDATE'
    end
    object ibdsApplSN: TIntegerField
      FieldName = 'SN'
    end
    object ibdsApplWASRET: TFIBStringField
      FieldName = 'WASRET'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsApplDESCR: TMemoField
      FieldName = 'DESCR'
      BlobType = ftMemo
      Size = 8
    end
    object ibdsApplCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
    end
    object ibdsApplADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_RET.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsApplEMPNAME: TFIBStringField
      FieldName = 'EMPNAME'
      Origin = 'DOC_RET.EMPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrApplcause: TDataSource
    DataSet = ibdsApplCause
    Left = 263
    Top = 545
  end
  object ibdsApplCause: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select ClientName, UID, W, Sz, Good, Price, Art, Art2, Mat, Unit' +
        'Id, '
      
        '          SellDate, Cost, SN, CRDATE,  Wasret, Descr, CheckNo, A' +
        'ddress,'
      '          EMPNAME'
      'from Doc_Ret(:P_Id, :P_Kind)')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 263
    Top = 496
    object ibdsApplCauseCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplCauseUID: TIntegerField
      FieldName = 'UID'
    end
    object ibdsApplCauseW: TFloatField
      FieldName = 'W'
    end
    object ibdsApplCauseSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = True
    end
    object ibdsApplCauseGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object ibdsApplCausePRICE: TFloatField
      FieldName = 'PRICE'
    end
    object ibdsApplCauseART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsApplCauseART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsApplCauseMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object ibdsApplCauseUNITID: TIntegerField
      FieldName = 'UNITID'
    end
    object ibdsApplCauseSELLDATE: TDateTimeField
      FieldName = 'SELLDATE'
    end
    object ibdsApplCauseCOST: TFloatField
      FieldName = 'COST'
    end
    object ibdsApplCauseSN: TIntegerField
      FieldName = 'SN'
    end
    object ibdsApplCauseCRDATE: TDateTimeField
      FieldName = 'CRDATE'
    end
    object ibdsApplCauseWASRET: TFIBStringField
      FieldName = 'WASRET'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsApplCauseDESCR: TBlobField
      FieldName = 'DESCR'
      Size = 8
    end
    object ibdsApplCauseCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
    end
    object ibdsApplCauseADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_RET.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsApplCauseEMPNAME: TFIBStringField
      FieldName = 'EMPNAME'
      Origin = 'DOC_RET.EMPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frdsApplcause: TfrDBDataSet
    DataSource = dsrApplcause
    Left = 263
    Top = 593
  end
  object ibdsTicketIt: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID,         ' +
        '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE,'
      'R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount,'
      
        'R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT,  R_SS' +
        'F, r_actdiscount'
      'from Doc_Ticket(:I_ID, :I_KIND)')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 348
    dcForceMasterRefresh = True
    dcForceOpen = True
    object ibdsTicketItR_FULLART: TFIBStringField
      FieldName = 'R_FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsTicketItR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_SIZE: TFIBStringField
      FieldName = 'R_SIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTicketItR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsTicketItR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsTicketItR_UID: TIntegerField
      FieldName = 'R_UID'
    end
    object ibdsTicketItR_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTicketItR_SELLDATE: TDateTimeField
      FieldName = 'R_SELLDATE'
    end
    object ibdsTicketItR_CITY: TFIBStringField
      FieldName = 'R_CITY'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_ADDRESS: TFIBStringField
      FieldName = 'R_ADDRESS'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTicketItR_COST: TFloatField
      FieldName = 'R_COST'
    end
    object ibdsTicketItR_PHONE: TFIBStringField
      FieldName = 'R_PHONE'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsTicketItR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsTicketItR_ID: TIntegerField
      FieldName = 'R_ID'
    end
    object ibdsTicketItR_DISCPRICE: TFloatField
      FieldName = 'R_DISCPRICE'
    end
    object ibdsTicketItR_DISCOUNT: TFloatField
      FieldName = 'R_DISCOUNT'
    end
    object ibdsTicketItR_DISCCOST: TFloatField
      FieldName = 'R_DISCCOST'
    end
    object ibdsTicketItR_RET: TSmallintField
      FieldName = 'R_RET'
      Origin = 'DOC_TICKET.R_RET'
    end
    object ibdsTicketItR_EMPNAME: TFIBStringField
      FieldName = 'R_EMPNAME'
      Origin = 'DOC_TICKET.R_EMPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsTicketItR_CHECKNO: TIntegerField
      FieldName = 'R_CHECKNO'
      Origin = 'DOC_TICKET.R_CHECKNO'
    end
    object ibdsTicketItIN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'DOC_TICKET.IN_PRICE'
    end
    object ibdsTicketItR_UNIT: TFIBStringField
      FieldName = 'R_UNIT'
      Origin = 'DOC_TICKET.R_UNIT'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTicketItR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      Origin = 'DOC_TICKET.R_SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketItR_ACTDISCOUNT: TFIBFloatField
      FieldName = 'R_ACTDISCOUNT'
    end
  end
  object frdsTicketIt: TfrDBDataSet
    DataSet = ibdsTicketIt
    Left = 487
    Top = 396
  end
  object ibdsSBillItem: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select R_UID, R_SZ, R_GOODID, R_GOOD, R_ART, R_ART2,  R_Art2Id, ' +
        '        R_COUNTRY, R_CUSTDECL, R_MATID, R_MAT, '
      
        '          R_UNITID, R_Q, R_W, R_PRICEWONDS, R_COSTWONDS,        ' +
        '   R_EXCISE, R_PNDS, '
      
        '          R_TOTALNDS, R_PRICE, R_COST, R_TR, R_PRICE2, R_COST2, ' +
        '                   R_PRICE2WONDS, R_COST2WONDS, R_TOTAL2NDS, R_S' +
        'SF,'
      '  R_NDATE'
      'from Doc_Bill(:P_ID, :P_Kind)')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 561
    Top = 303
    object ibdsSBillItemR_UID: TIntegerField
      FieldName = 'R_UID'
    end
    object ibdsSBillItemR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_CUSTDECL: TFIBStringField
      FieldName = 'R_CUSTDECL'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsSBillItemR_Q: TIntegerField
      FieldName = 'R_Q'
    end
    object ibdsSBillItemR_W: TFloatField
      FieldName = 'R_W'
    end
    object ibdsSBillItemR_PRICEWONDS: TFloatField
      FieldName = 'R_PRICEWONDS'
    end
    object ibdsSBillItemR_COSTWONDS: TFloatField
      FieldName = 'R_COSTWONDS'
    end
    object ibdsSBillItemR_EXCISE: TFIBStringField
      FieldName = 'R_EXCISE'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_PNDS: TFloatField
      FieldName = 'R_PNDS'
    end
    object ibdsSBillItemR_TOTALNDS: TFloatField
      FieldName = 'R_TOTALNDS'
    end
    object ibdsSBillItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsSBillItemR_COST: TFloatField
      FieldName = 'R_COST'
    end
    object ibdsSBillItemR_TR: TFloatField
      FieldName = 'R_TR'
    end
    object ibdsSBillItemR_PRICE2: TFloatField
      FieldName = 'R_PRICE2'
    end
    object ibdsSBillItemR_COST2: TFloatField
      FieldName = 'R_COST2'
    end
    object ibdsSBillItemR_PRICE2WONDS: TFloatField
      FieldName = 'R_PRICE2WONDS'
    end
    object ibdsSBillItemR_COST2WONDS: TFloatField
      FieldName = 'R_COST2WONDS'
    end
    object ibdsSBillItemR_TOTAL2NDS: TFloatField
      FieldName = 'R_TOTAL2NDS'
    end
    object ibdsSBillItemR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      EmptyStrToNull = True
    end
    object ibdsSBillItemR_NDATE: TDateTimeField
      FieldName = 'R_NDATE'
    end
    object ibdsSBillItemR_ART2ID: TIntegerField
      FieldName = 'R_ART2ID'
    end
  end
  object taSelf: TpFIBDataSet
    SelectSQL.Strings = (
      'select COMPID, NAME, BOSS, OFIO, ADDRESS, PHONE, FAX,'
      '         EMAIL, INN, BIK, OKPO, OKONH, BANK, BILL, KBILL,'
      '         OFIOPHONE'
      'from DOC_PROVIDER(:ID, :KIND)'
      ''
      ''
      '           ')
    BeforeOpen = taSelfBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 335
    Top = 9
    object taSelfCOMPID: TIntegerField
      FieldName = 'COMPID'
      Origin = 'DOC_PROVIDER.COMPID'
    end
    object taSelfNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROVIDER.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBOSS: TFIBStringField
      FieldName = 'BOSS'
      Origin = 'DOC_PROVIDER.BOSS'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfOFIO: TFIBStringField
      FieldName = 'OFIO'
      Origin = 'DOC_PROVIDER.OFIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_PROVIDER.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taSelfPHONE: TFIBStringField
      FieldName = 'PHONE'
      Origin = 'DOC_PROVIDER.PHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfFAX: TFIBStringField
      FieldName = 'FAX'
      Origin = 'DOC_PROVIDER.FAX'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Origin = 'DOC_PROVIDER.EMAIL'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfINN: TFIBStringField
      FieldName = 'INN'
      Origin = 'DOC_PROVIDER.INN'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfBIK: TFIBStringField
      FieldName = 'BIK'
      Origin = 'DOC_PROVIDER.BIK'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSelfOKPO: TFIBStringField
      FieldName = 'OKPO'
      Origin = 'DOC_PROVIDER.OKPO'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSelfOKONH: TFIBStringField
      FieldName = 'OKONH'
      Origin = 'DOC_PROVIDER.OKONH'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSelfBANK: TFIBStringField
      FieldName = 'BANK'
      Origin = 'DOC_PROVIDER.BANK'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBILL: TFIBStringField
      FieldName = 'BILL'
      Origin = 'DOC_PROVIDER.BILL'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfKBILL: TFIBStringField
      FieldName = 'KBILL'
      Origin = 'DOC_PROVIDER.KBILL'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfOFIOPHONE: TFIBStringField
      FieldName = 'OFIOPHONE'
      Origin = 'DOC_PROVIDER.OFIOPHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
  end
  object frdsSBillItem: TfrDBDataSet
    DataSet = ibdsSBillItem
    Left = 559
    Top = 396
  end
  object dsrDep: TDataSource
    DataSet = ibdsDep
    Left = 88
    Top = 504
  end
  object ibdsDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.D_Depid L_DepId, d.SNAME R_SNAME, d.Name R_Name'
      'from D_Dep d'
      'order by d.D_DepId')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 456
    object ibdsDepR_SNAME: TFIBStringField
      FieldName = 'R_SNAME'
      EmptyStrToNull = True
    end
    object ibdsDepR_NAME: TFIBStringField
      FieldName = 'R_NAME'
      EmptyStrToNull = True
    end
    object ibdsDepL_DEPID: TIntegerField
      FieldName = 'L_DEPID'
      Required = True
    end
  end
  object ibdsWhArt: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select distinct w.Art, w.D_ArtId Link, w.Mat, w.Good, W.UnitId, ' +
        'W.CompCode,'
      '          w.Quantity, w.Weigth, w.TW, w.TQ, w.Comp, a2.Art2'
      'from WhTmp w, UIDWH_T u, Art2 a2'
      'where w.UserId=:USERID and            '
      '           Mat between :Mat1 and :Mat2 and'
      '           Good between :Good1 and :Good2 and'
      '           CompId between :CompId1 and :CompId2  and'
      '           u.UserId=:USERID and '
      '           u.Art2Id=a2.Art2Id and'
      '           u.DepId between :DEPID1 and :DEPID2 and'
      '           w.D_ArtId=a2.D_ArtId'
      'order by w.Art')
    BeforeOpen = ibdsWhArtBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 263
    Top = 352
    object ibdsWhArtART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsWhArtLINK: TIntegerField
      FieldName = 'LINK'
    end
    object ibdsWhArtMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object ibdsWhArtGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object ibdsWhArtUNITID: TSmallintField
      FieldName = 'UNITID'
    end
    object ibdsWhArtCOMPCODE: TFIBStringField
      FieldName = 'COMPCODE'
      EmptyStrToNull = True
    end
    object ibdsWhArtQUANTITY: TFloatField
      FieldName = 'QUANTITY'
    end
    object ibdsWhArtWEIGTH: TFloatField
      FieldName = 'WEIGTH'
    end
    object ibdsWhArtTW: TFIBStringField
      FieldName = 'TW'
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsWhArtTQ: TFIBStringField
      FieldName = 'TQ'
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsWhArtCOMP: TFIBStringField
      FieldName = 'COMP'
      EmptyStrToNull = True
    end
    object ibdsWhArtART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object dsrWhArt: TDataSource
    DataSet = ibdsWhArt
    Left = 263
    Top = 399
  end
  object frdsWhArt: TfrDBDataSet
    DataSource = dsrWhArt
    Left = 263
    Top = 447
  end
  object ibdsWhSZ: TpFIBDataSet
    SelectSQL.Strings = (
      'select u.SZ, count(*) Q, sum(u.W) W'
      'from UIDWH_T u, Art2 a2'
      'where u.Art2Id=a2.Art2Id and '
      '           a2.D_ArtId=:LINK and'
      '           u.DepId between :DEPID1 and :DEPID2  and'
      '           u.UserId=:USERID and'
      '           u.T in (0,1)  '
      'group by u.Sz'
      'order by u.SZ')
    BeforeOpen = ibdsWhArt2BeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrWhArt
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 336
    Top = 544
    object ibdsWhSZQ: TIntegerField
      FieldName = 'Q'
    end
    object ibdsWhSZW: TFloatField
      FieldName = 'W'
    end
    object ibdsWhSZSZ: TFIBStringField
      FieldName = 'SZ'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object frdsWhSZ: TfrDBDataSet
    DataSet = ibdsWhSZ
    Left = 338
    Top = 591
  end
  object dsrPrList: TDataSource
    DataSet = ibdsPrList
    Left = 335
    Top = 112
  end
  object ibdsPrList: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.SN R_SN, '
      '          i.SDate R_Sdate, '
      '          i.SInvId R_Link         '
      'from SInv i'
      'where i.SInvId=:I_SinvId '
      '          '
      ' '
      ' ')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 335
    Top = 64
    object ibdsPrListR_SN: TIntegerField
      FieldName = 'R_SN'
    end
    object ibdsPrListR_SDATE: TDateTimeField
      FieldName = 'R_SDATE'
    end
    object ibdsPrListR_LINK: TIntegerField
      FieldName = 'R_LINK'
      Required = True
    end
  end
  object frdsPrList: TfrDBDataSet
    DataSource = dsrPrList
    Left = 335
    Top = 161
  end
  object ibdsPrListItem: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select g.Name R_Good, m.Name R_Mat, a2.Art R_Art, a2.Art2 R_Art2' +
        ',                     a2.UnitId R_UnitId, e.Price2 R_Price'
      'from Sel e, Art2 a2,  D_Good g, D_Mat m'
      'where e.SinvId = :R_Link and'
      '           a2.Art2Id = e.Art2Id and           '
      '           a2.D_GoodId=G.D_GoodId and'
      '          a2.D_MatId=m.D_MatId'
      'order by a2.Art')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrPrList
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 391
    Top = 56
    object ibdsPrListItemR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsPrListItemR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsPrListItemR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsPrListItemR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      Required = True
      EmptyStrToNull = True
    end
    object ibdsPrListItemR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
      Required = True
    end
    object ibdsPrListItemR_PRICE: TFloatField
      FieldName = 'R_PRICE'
      Required = True
    end
  end
  object frdsPrListItem: TfrDBDataSet
    DataSet = ibdsPrListItem
    Left = 391
    Top = 104
  end
  object ibdsFullPrList: TpFIBDataSet
    SelectSQL.Strings = (
      'select R_GOOD, R_MAT, R_ART, R_ART2, R_UNITID, R_PRICE2 '
      'from Doc_PriceList(:DEPID, 1)'
      'order by R_Art, R_Art2')
    BeforeOpen = ibdsFullPrListBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 24
    Top = 504
    object ibdsFullPrListR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      Origin = 'DOC_PRICELIST.R_GOOD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsFullPrListR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      Origin = 'DOC_PRICELIST.R_MAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsFullPrListR_ART: TFIBStringField
      FieldName = 'R_ART'
      Origin = 'DOC_PRICELIST.R_ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsFullPrListR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      Origin = 'DOC_PRICELIST.R_ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsFullPrListR_UNITID: TIntegerField
      FieldName = 'R_UNITID'
      Origin = 'DOC_PRICELIST.R_UNITID'
    end
    object ibdsFullPrListR_PRICE2: TFloatField
      FieldName = 'R_PRICE2'
      Origin = 'DOC_PRICELIST.R_PRICE2'
    end
  end
  object frdsFullPrList: TfrDBDataSet
    DataSet = ibdsFullPrList
    Left = 24
    Top = 552
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT NAME,  OFIO, PHONE, FAX, CODE '
      'FROM D_Comp'
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 88
    Top = 600
    object taCompNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taCompOFIO: TFIBStringField
      FieldName = 'OFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taCompPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taCompFAX: TFIBStringField
      FieldName = 'FAX'
      Size = 30
      EmptyStrToNull = True
    end
    object taCompCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object frdsComp: TfrDBDataSet
    DataSet = taComp
    Left = 88
    Top = 647
  end
  object taApplSup: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AItem'
      'set ARTID=?ARTID,'
      '     SZ=?SZ,'
      '    Q=?Q'
      'where AITEMID=?OLD_AITEMID')
    DeleteSQL.Strings = (
      'delete from aitem'
      'where AITEMID=?OLD_AITEMID')
    InsertSQL.Strings = (
      'insert into aitem(AITEMID,APPLID,ARTID,SZ,Q)'
      'values (?AITEMID,?APPLID,?ARTID,?SZ,?Q)')
    SelectSQL.Strings = (
      
        'select a.Art, ap.Art2, a.UnitId, a.D_InsId, a.D_matid, (select n' +
        'ame from d_good where d_goodid=a.d_goodid) Good, a.D_GoodId, a.D' +
        '_CompId, ap.Sz, ap.Q'
      'from AItem ap, D_Art a'
      'where ap.ApplId=:NAp and'
      '           ap.ArtId=a.D_ArtId '
      'order by a.Art, ap.Art2, ap.Sz '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 263
    Top = 207
    object taApplSupART: TFIBStringField
      FieldName = 'ART'
      Required = True
      EmptyStrToNull = True
    end
    object taApplSupUNITID: TIntegerField
      FieldName = 'UNITID'
      Required = True
    end
    object taApplSupD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplSupD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplSupD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplSupSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'AITEM.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplSupQ: TIntegerField
      FieldName = 'Q'
      Origin = 'AITEM.Q'
    end
    object taApplSupGOOD: TFIBStringField
      FieldName = 'GOOD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplSupD_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'D_ART.D_COMPID'
      Required = True
    end
    object taApplSupART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
  end
  object frdsApplSup: TfrDBDataSet
    DataSet = taApplSup
    Left = 262
    Top = 304
  end
  object taJ3: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, ARTID, ART, FULLART, D_COMPID, D_MATID, D_GOODID,'
      '    UNITID,  D_INSID,  INS, INW,  INQ, OUTS, OUTW,'
      '    OUTQ, DEBTORS, DEBTORW, DEBTORQ, SALES,'
      '    SALEW, SALEQ,  RETS, RETW, RETQ, RETOPTS,'
      '    RETOPTW,  RETOPTQ,  RETVENDORS, RETVENDORW,'
      '    RETVENDORQ,  OPTS,  OPTW, OPTQ'
      'from List3(:UserId)'
      'order by Art')
    BeforeOpen = taJ3BeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 710
    Top = 251
    object taJ3ID: TIntegerField
      FieldName = 'ID'
      Origin = 'LIST3.ID'
    end
    object taJ3ARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'LIST3.ARTID'
    end
    object taJ3ART: TFIBStringField
      FieldName = 'ART'
      Origin = 'LIST3.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taJ3FULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'LIST3.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taJ3D_COMPID: TIntegerField
      FieldName = 'D_COMPID'
      Origin = 'LIST3.D_COMPID'
    end
    object taJ3D_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Origin = 'LIST3.D_MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJ3D_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Origin = 'LIST3.D_GOODID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJ3UNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'LIST3.UNITID'
    end
    object taJ3D_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'LIST3.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taJ3INS: TFloatField
      FieldName = 'INS'
      Origin = 'LIST3.INS'
    end
    object taJ3INW: TFloatField
      FieldName = 'INW'
      Origin = 'LIST3.INW'
    end
    object taJ3INQ: TIntegerField
      FieldName = 'INQ'
      Origin = 'LIST3.INQ'
    end
    object taJ3OUTS: TFloatField
      FieldName = 'OUTS'
      Origin = 'LIST3.OUTS'
    end
    object taJ3OUTW: TFloatField
      FieldName = 'OUTW'
      Origin = 'LIST3.OUTW'
    end
    object taJ3OUTQ: TIntegerField
      FieldName = 'OUTQ'
      Origin = 'LIST3.OUTQ'
    end
    object taJ3DEBTORS: TFloatField
      FieldName = 'DEBTORS'
      Origin = 'LIST3.DEBTORS'
    end
    object taJ3DEBTORW: TFloatField
      FieldName = 'DEBTORW'
      Origin = 'LIST3.DEBTORW'
    end
    object taJ3DEBTORQ: TIntegerField
      FieldName = 'DEBTORQ'
      Origin = 'LIST3.DEBTORQ'
    end
    object taJ3SALES: TFloatField
      FieldName = 'SALES'
      Origin = 'LIST3.SALES'
    end
    object taJ3SALEW: TFloatField
      FieldName = 'SALEW'
      Origin = 'LIST3.SALEW'
    end
    object taJ3SALEQ: TIntegerField
      FieldName = 'SALEQ'
      Origin = 'LIST3.SALEQ'
    end
    object taJ3RETS: TFloatField
      FieldName = 'RETS'
      Origin = 'LIST3.RETS'
    end
    object taJ3RETW: TFloatField
      FieldName = 'RETW'
      Origin = 'LIST3.RETW'
    end
    object taJ3RETQ: TIntegerField
      FieldName = 'RETQ'
      Origin = 'LIST3.RETQ'
    end
    object taJ3RETOPTS: TFloatField
      FieldName = 'RETOPTS'
      Origin = 'LIST3.RETOPTS'
    end
    object taJ3RETOPTW: TFloatField
      FieldName = 'RETOPTW'
      Origin = 'LIST3.RETOPTW'
    end
    object taJ3RETOPTQ: TIntegerField
      FieldName = 'RETOPTQ'
      Origin = 'LIST3.RETOPTQ'
    end
    object taJ3RETVENDORS: TFloatField
      FieldName = 'RETVENDORS'
      Origin = 'LIST3.RETVENDORS'
    end
    object taJ3RETVENDORW: TFloatField
      FieldName = 'RETVENDORW'
      Origin = 'LIST3.RETVENDORW'
    end
    object taJ3RETVENDORQ: TIntegerField
      FieldName = 'RETVENDORQ'
      Origin = 'LIST3.RETVENDORQ'
    end
    object taJ3OPTS: TFloatField
      FieldName = 'OPTS'
      Origin = 'LIST3.OPTS'
    end
    object taJ3OPTW: TFloatField
      FieldName = 'OPTW'
      Origin = 'LIST3.OPTW'
    end
    object taJ3OPTQ: TIntegerField
      FieldName = 'OPTQ'
      Origin = 'LIST3.OPTQ'
    end
  end
  object frdsrJ3: TfrDBDataSet
    DataSet = taJ3
    Left = 710
    Top = 299
  end
  object taDoc: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Doc set'
      '  ID=:ID,'
      '  DOC=:DOC'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from Doc where id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into Doc(Id, Doc) values (:Id, :Doc)')
    SelectSQL.Strings = (
      'select Id, Doc, NAME'
      'from Doc')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 266
    Top = 9
    object taDocID: TIntegerField
      FieldName = 'ID'
      Origin = 'DOC.ID'
      Required = True
    end
    object taDocDOC: TMemoField
      FieldName = 'DOC'
      Origin = 'DOC.DOC'
      BlobType = ftMemo
      Size = 8
    end
    object taDocNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrOrder: TDataSource
    DataSet = ibdsOrder
    Left = 24
    Top = 112
  end
  object dsrReduce: TDataSource
    DataSet = ibdsReduce
    Left = 176
    Top = 304
  end
  object dsrTagWI: TDataSource
    DataSet = ibdsTagWI
    Left = 24
    Top = 400
  end
  object frdsWHUID: TfrDBDataSet
    DataSet = taUIDWH
    Left = 486
    Top = 683
  end
  object taProvider: TpFIBDataSet
    SelectSQL.Strings = (
      'select NAME, ADDRESS, PHONE, BILL,  KBILL, BIK, BANK,'
      '         INN, OKONH, OKPO, SN, NDATE, SDATE, SSF'
      'from DOC_PROVIDER(:ID, :KIND)')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 559
    Top = 9
    object taProviderNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROVIDER.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProviderADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Origin = 'DOC_PROVIDER.ADDRESS'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taProviderPHONE: TFIBStringField
      FieldName = 'PHONE'
      Origin = 'DOC_PROVIDER.PHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taProviderBILL: TFIBStringField
      FieldName = 'BILL'
      Origin = 'DOC_PROVIDER.BILL'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProviderKBILL: TFIBStringField
      FieldName = 'KBILL'
      Origin = 'DOC_PROVIDER.KBILL'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProviderBIK: TFIBStringField
      FieldName = 'BIK'
      Origin = 'DOC_PROVIDER.BIK'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProviderBANK: TFIBStringField
      FieldName = 'BANK'
      Origin = 'DOC_PROVIDER.BANK'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProviderINN: TFIBStringField
      FieldName = 'INN'
      Origin = 'DOC_PROVIDER.INN'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taProviderOKONH: TFIBStringField
      FieldName = 'OKONH'
      Origin = 'DOC_PROVIDER.OKONH'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taProviderOKPO: TFIBStringField
      FieldName = 'OKPO'
      Origin = 'DOC_PROVIDER.OKPO'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taProviderSN: TIntegerField
      FieldName = 'SN'
      Origin = 'DOC_PROVIDER.SN'
    end
    object taProviderNDATE: TDateTimeField
      FieldName = 'NDATE'
      Origin = 'DOC_PROVIDER.NDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taProviderSDATE: TDateTimeField
      FieldName = 'SDATE'
      Origin = 'DOC_PROVIDER.SDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taProviderSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'DOC_PROVIDER.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object frSumReport: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    RebuildPrinter = False
    Left = 875
    Top = 12
    ReportForm = {19000000}
  end
  object taUIDWH: TpFIBDataSet
    Tag = 1
    SelectSQL.Strings = (
      
        'select a2.D_ARTID, a2.UNITID, a2.ART, a2.D_MATId MATID, a2.D_GOO' +
        'DId GOODID, a2.D_INSId INSID,'
      
        '       u.UID, u.Sz, u.W, a2.Art2, u.Price, d.SName Depname, FIF(' +
        'a2.UnitId, u.Price*u.W, u.Price) COST, u.SPRICE0, FIF(a2.UnitId,' +
        ' u.SPrice0*u.W, u.SPrice0) COST0'
      'from  UIDWH_T u, Art2 a2, D_Dep d'
      'where         u.UserId=:USERID and'
      '       u.Art2Id=a2.Art2Id and'
      '       u.DepId between :DEPID1 and :DEPID2 and'
      '      a2.D_MatId between :MATID1 and :MATID2 and'
      '       '
      '       a2.D_GoodId between :GOODID1 and :GOODID2 and'
      '       '
      '       a2.D_CompId between :COMPID1 and :COMPID2 and'
      '       '
      '       u.SupId0 between :SUPID1 and :SUPID2 and'
      '       '
      '       a2.Art between :ART1 and :ART2 and'
      '       d.D_DepId=u.DepId '
      '       '
      ''
      ''
      ''
      ''
      ''
      '       and u.goodsid1 between :goodsid1_1 and :goodsid1_2'
      '       and u.goodsid2 between :goodsid2_1 and :goodsid2_2'
      '       and a2.ATT1 between :ATT1_1 and :ATT1_2'
      '       and a2.ATT2 between :ATT2_1 and :ATT2_2 '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '       and u.sz between :SZ1 and :SZ2'
      '       and a2.art2 between :ART21 and :ART22'
      ''
      'order by a2.D_MATId,a2.D_GoodId, a2.Art, u.Sz, a2.Art2')
    BeforeOpen = taUIDWHABeforeOpen
    Transaction = trReport
    Database = dmCom.db
    Filtered = True
    OnFilterRecord = taUIDWHAFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 486
    Top = 634
  end
  object taUIDWHA: TpFIBDataSet
    Tag = 2
    SelectSQL.Strings = (
      
        'select a2.D_ARTID, a2.UNITID, a2.ART, a2.D_MATId Matid, a2.D_GOO' +
        'DId Goodid, a2.D_INSId InsId, '
      
        '      c.SNAme  CompCode, sum(u.W) W, a2.Art2, u.Price,  count(*)' +
        ' Q, u.SPrice0 SPrice'
      'from  UIDWH_T u, Art2 a2, d_comp c'
      'where        u.UserId=:USERID and'
      '       u.DepId between :DEPID1 and :DEPID2 and'
      '        u.Art2Id=a2.Art2Id and'
      '        a2.D_MatId between :MATID1 and :MATID2 and'
      '        '
      '       a2.D_GoodId between :GOODID1 and :GOODID2 and'
      ''
      '       a2.D_CompId between :COMPID1 and :COMPID2 and'
      ''
      '       u.SupId0 between :SUPID1 and :SUPID2 and'
      ''
      '       a2.Art between :ART1 and :ART2 and'
      '       c.d_compid=a2.d_compid'
      ' '
      ''
      ''
      ''
      ''
      ''
      '       and u.goodsid1 between :goodsid1_1 and :goodsid1_2'
      '       and u.goodsid2 between :goodsid2_1 and :goodsid2_2'
      '       and a2.ATT1 between :ATT1_1 and :ATT1_2'
      '       and a2.ATT2 between :ATT2_1 and :ATT2_2 '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '       and u.sz between  :SZ1 and :SZ2'
      '       and a2.art2 between :ART21 and :ART22'
      
        'group by a2.D_ArtId, a2.UnitId, a2.Art, a2.D_MatId, a2.D_GoodId,' +
        ' a2.D_InsId, c.SName, a2.Art2, u.Price, u.SPrice0'
      
        'PLAN SORT(SORT (JOIN (U INDEX (UIDWH_T_IDX1), A2 INDEX (ART2_IDX' +
        '2),C INDEX (D_COMP_IDX1))))'
      'order by a2.Art, a2.Art2')
    BeforeOpen = taUIDWHABeforeOpen
    Transaction = trReport
    Database = dmCom.db
    OnFilterRecord = taUIDWHAFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 492
    object taUIDWHAD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object taUIDWHAUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object taUIDWHAART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taUIDWHAMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHACOMPCODE: TFIBStringField
      FieldName = 'COMPCODE'
      EmptyStrToNull = True
    end
    object taUIDWHAW: TFIBFloatField
      FieldName = 'W'
    end
    object taUIDWHAART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taUIDWHAPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taUIDWHAQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taUIDWHASPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
  end
  object frdsUIDWHA: TfrDBDataSet
    DataSet = taUIDWHA
    Left = 487
    Top = 588
  end
  object dsrUIDWHA: TDataSource
    DataSet = taUIDWHA
    Left = 487
    Top = 540
  end
  object taUIDWHAu: TpFIBDataSet
    Tag = 4
    SelectSQL.Strings = (
      
        'select a2.D_ARTID, a2.UNITID, a2.ART, a2.D_MATId Matid, a2.D_GOO' +
        'DId Goodid, a2.D_INSId InsId, '
      
        '      c.Code  CompCode,  c2.Code  SupCode,sum(u.W) W, a2.Art2, u' +
        '.Price,  count(*) Q'
      'from  UIDWH_T u, Art2 a2, d_comp c, D_comp c2'
      'where        u.UserId=:USERID and'
      '       u.DepId between :DEPID1 and :DEPID2 and'
      '        u.Art2Id=a2.Art2Id and'
      '        a2.D_MatId between :MATID1 and :MATID2 and'
      '        '
      '       a2.D_GoodId between :GOODID1 and :GOODID2 and'
      ''
      '       a2.D_CompId between :COMPID1 and :COMPID2 and'
      ''
      '       u.SupId0 between :SUPID1 and :SUPID2 and'
      ''
      '       a2.Art between :ART1 and :ART2 and'
      '       c.d_compid=a2.d_compid and     c2.d_compid=u.SupId0'
      ' '
      ''
      ''
      ''
      ''
      ''
      '       and u.goodsid1 between :goodsid1_1 and :goodsid1_2'
      '       and u.goodsid2 between :goodsid2_1 and :goodsid2_2'
      '       and a2.ATT1 between :ATT1_1 and :ATT1_2'
      '       and a2.ATT2 between :ATT2_1 and :ATT2_2 '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '       and u.sz between :SZ1 and :SZ2'
      '       and a2.art2 between :ART21 and :ART22'
      
        'group by a2.D_ArtId, a2.UnitId, a2.Art, a2.D_MatId, a2.D_GoodId,' +
        ' a2.D_InsId, c.Code, c2.Code, a2.Art2, u.Price'
      ''
      'order by a2.Art, a2.Art2')
    BeforeOpen = taUIDWHABeforeOpen
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 559
    Top = 444
    object taUIDWHAuD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
    end
    object taUIDWHAuUNITID: TIntegerField
      FieldName = 'UNITID'
      Required = True
    end
    object taUIDWHAuART: TFIBStringField
      FieldName = 'ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUIDWHAuMATID: TFIBStringField
      FieldName = 'MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAuGOODID: TFIBStringField
      FieldName = 'GOODID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAuINSID: TFIBStringField
      FieldName = 'INSID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAuCOMPCODE: TFIBStringField
      FieldName = 'COMPCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAuSUPCODE: TFIBStringField
      FieldName = 'SUPCODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHAuW: TFloatField
      FieldName = 'W'
    end
    object taUIDWHAuART2: TFIBStringField
      FieldName = 'ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taUIDWHAuPRICE: TFloatField
      FieldName = 'PRICE'
    end
    object taUIDWHAuQ: TIntegerField
      FieldName = 'Q'
      Required = True
    end
  end
  object frdsUIDWHASz: TfrDBDataSet
    CloseDataSource = True
    DataSet = taUIDWHAu
    Left = 559
    Top = 488
  end
  object dsrSBillItem: TDataSource
    DataSet = ibdsSBillItem
    Left = 559
    Top = 348
  end
  object taTSumDep: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select t2.*, (select sname from d_dep where d_depid=t2.dep1) Dep' +
        'Name'
      '    from Total t2'
      '    where  t2.ID=(select max(t.id) from total t'
      
        '                  where t.Dep1=:I_DEPID1 and t.Dep2=:I_DEPID2 an' +
        'd'
      
        '                         onlydate(t.IDate)=:I_BD and  onlydate(t' +
        '.IDate2)= :I_ED'
      '                         and isinprice=:IIP)')
    BeforeOpen = taTSumDepBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 209
    object taTSumDepDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taTSumDepIDATE: TDateTimeField
      FieldName = 'IDATE'
      Origin = 'TOTAL.IDATE'
      Required = True
    end
    object taTSumDepIDATE2: TDateTimeField
      FieldName = 'IDATE2'
      Origin = 'TOTAL.IDATE2'
      Required = True
    end
    object taTSumDepDEP1: TIntegerField
      FieldName = 'DEP1'
      Origin = 'TOTAL.DEP1'
    end
    object taTSumDepDEP2: TIntegerField
      FieldName = 'DEP2'
      Origin = 'TOTAL.DEP2'
    end
    object taTSumDepINC: TFloatField
      FieldName = 'INC'
      Origin = 'TOTAL.INC'
    end
    object taTSumDepINW: TFloatField
      FieldName = 'INW'
      Origin = 'TOTAL.INW'
    end
    object taTSumDepINQ: TIntegerField
      FieldName = 'INQ'
      Origin = 'TOTAL.INQ'
    end
    object taTSumDepINVINC: TFloatField
      FieldName = 'INVINC'
      Origin = 'TOTAL.INVINC'
    end
    object taTSumDepINVW: TFloatField
      FieldName = 'INVW'
      Origin = 'TOTAL.INVW'
    end
    object taTSumDepINVQ: TIntegerField
      FieldName = 'INVQ'
      Origin = 'TOTAL.INVQ'
    end
    object taTSumDepINVOSTC: TFloatField
      FieldName = 'INVOSTC'
      Origin = 'TOTAL.INVOSTC'
    end
    object taTSumDepINVOSTW: TFloatField
      FieldName = 'INVOSTW'
      Origin = 'TOTAL.INVOSTW'
    end
    object taTSumDepINVOSTQ: TIntegerField
      FieldName = 'INVOSTQ'
      Origin = 'TOTAL.INVOSTQ'
    end
    object taTSumDepSLINC: TFloatField
      FieldName = 'SLINC'
      Origin = 'TOTAL.SLINC'
    end
    object taTSumDepSLOUTC: TFloatField
      FieldName = 'SLOUTC'
      Origin = 'TOTAL.SLOUTC'
    end
    object taTSumDepSLW: TFloatField
      FieldName = 'SLW'
      Origin = 'TOTAL.SLW'
    end
    object taTSumDepSLQ: TIntegerField
      FieldName = 'SLQ'
      Origin = 'TOTAL.SLQ'
    end
    object taTSumDepOPTINC: TFloatField
      FieldName = 'OPTINC'
      Origin = 'TOTAL.OPTINC'
    end
    object taTSumDepOPTOUTC: TFloatField
      FieldName = 'OPTOUTC'
      Origin = 'TOTAL.OPTOUTC'
    end
    object taTSumDepOPTW: TFloatField
      FieldName = 'OPTW'
      Origin = 'TOTAL.OPTW'
    end
    object taTSumDepOPTQ: TIntegerField
      FieldName = 'OPTQ'
      Origin = 'TOTAL.OPTQ'
    end
    object taTSumDepRETOPTINC: TFloatField
      FieldName = 'RETOPTINC'
      Origin = 'TOTAL.RETOPTINC'
    end
    object taTSumDepRETOPTRW: TFloatField
      FieldName = 'RETOPTRW'
      Origin = 'TOTAL.RETOPTRW'
    end
    object taTSumDepRETOPTRQ: TIntegerField
      FieldName = 'RETOPTRQ'
      Origin = 'TOTAL.RETOPTRQ'
    end
    object taTSumDepRETINC: TFloatField
      FieldName = 'RETINC'
      Origin = 'TOTAL.RETINC'
    end
    object taTSumDepRETW: TFloatField
      FieldName = 'RETW'
      Origin = 'TOTAL.RETW'
    end
    object taTSumDepRETQ: TIntegerField
      FieldName = 'RETQ'
      Origin = 'TOTAL.RETQ'
    end
    object taTSumDepRETSINC: TFloatField
      FieldName = 'RETSINC'
      Origin = 'TOTAL.RETSINC'
    end
    object taTSumDepRETSW: TFloatField
      FieldName = 'RETSW'
      Origin = 'TOTAL.RETSW'
    end
    object taTSumDepRETSQ: TIntegerField
      FieldName = 'RETSQ'
      Origin = 'TOTAL.RETSQ'
    end
    object taTSumDepOUTC: TFloatField
      FieldName = 'OUTC'
      Origin = 'TOTAL.OUTC'
    end
    object taTSumDepOUTW: TFloatField
      FieldName = 'OUTW'
      Origin = 'TOTAL.OUTW'
    end
    object taTSumDepOUTQ: TIntegerField
      FieldName = 'OUTQ'
      Origin = 'TOTAL.OUTQ'
    end
    object taTSumDepISTMP: TSmallintField
      FieldName = 'ISTMP'
      Origin = 'TOTAL.ISTMP'
    end
    object taTSumDepINGR: TFIBStringField
      FieldName = 'INGR'
      Origin = 'TOTAL.INGR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taTSumDepSINVGR: TFIBStringField
      FieldName = 'SINVGR'
      Origin = 'TOTAL.SINVGR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taTSumDepSELLIN: TFIBStringField
      FieldName = 'SELLIN'
      Origin = 'TOTAL.SELLIN'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object taTSumDepSELLOUT: TFIBStringField
      FieldName = 'SELLOUT'
      Origin = 'TOTAL.SELLOUT'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object taTSumDepOUTGR: TFIBStringField
      FieldName = 'OUTGR'
      Origin = 'TOTAL.OUTGR'
      FixedChar = True
      Size = 400
      EmptyStrToNull = True
    end
    object taTSumDepSINVGR2: TFIBStringField
      FieldName = 'SINVGR2'
      Origin = 'TOTAL.SINVGR2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taTSumDepRETGR: TFIBStringField
      FieldName = 'RETGR'
      Origin = 'TOTAL.RETGR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taTSumDepOUTURFC: TFloatField
      FieldName = 'OUTURFC'
      Origin = 'TOTAL.OUTURFC'
    end
    object taTSumDepISINPRICE: TSmallintField
      FieldName = 'ISINPRICE'
      Origin = 'TOTAL.ISINPRICE'
    end
    object taTSumDepID: TIntegerField
      FieldName = 'ID'
      Origin = 'TOTAL.ID'
      Required = True
    end
    object taTSumDepRETURFC: TFloatField
      FieldName = 'RETURFC'
      Origin = 'TOTAL.RETURFC'
    end
    object taTSumDepRETSELLERURFC: TFloatField
      FieldName = 'RETSELLERURFC'
      Origin = 'TOTAL.RETSELLERURFC'
    end
    object taTSumDepURFINC: TFloatField
      FieldName = 'URFINC'
      Origin = 'TOTAL.URFINC'
    end
    object taTSumDepURFOUTC: TFloatField
      FieldName = 'URFOUTC'
      Origin = 'TOTAL.URFOUTC'
    end
  end
  object frTSumDep: TfrDBDataSet
    DataSet = taTSumDep
    OpenDataSource = False
    Left = 487
    Top = 256
  end
  object quTmp: TpFIBQuery
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 1015
    Top = 64
  end
  object taApplZ: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.name, stretrim(c.PostIndex)||'#39' '#39'||stretrim(c.City)||'#39','#39 +
        '||stretrim(c.Address) Address, c.Phone, ap.noappl,ap.zdate, c2.n' +
        'ame supname'
      'from APpl ap, D_comp c2, d_comp c, d_rec r'
      
        'where ap.ApplId=:Appl  and c2.d_compid=ap.supid and c.D_CompId=r' +
        '.D_CompId'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 336
    Top = 496
    object taApplZNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplZADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 770
      EmptyStrToNull = True
    end
    object taApplZPHONE: TFIBStringField
      FieldName = 'PHONE'
      Origin = 'D_COMP.PHONE'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taApplZNOAPPL: TIntegerField
      FieldName = 'NOAPPL'
      Origin = 'APPL.NOAPPL'
    end
    object taApplZZDATE: TDateTimeField
      FieldName = 'ZDATE'
      Origin = 'APPL.ZDATE'
    end
    object taApplZSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frdstaApplZ: TfrDBDataSet
    DataSet = taApplZ
    Left = 334
    Top = 306
  end
  object dsrApplSup: TDataSource
    DataSet = taApplSup
    Left = 263
    Top = 257
  end
  object svdFile: TOpenDialog
    DefaultExt = 'nlz'
    Left = 1016
    Top = 112
  end
  object frTextExp: TfrTextExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Left = 711
    Top = 8
  end
  object frCSVExp: TfrCSVExport
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    Delimiter = #9
    Left = 791
    Top = 8
  end
  object frRTFExp: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    ExportFrames = True
    ConvertToTable = True
    Left = 639
    Top = 8
  end
  object ibdsitemSlist: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select st.uid R_UID, a2.art R_ART1, a2.d_compid R_COMP, a2.d_mat' +
        'id R_MAT, a2.d_goodid R_GOOD, a2.art2 R_A2, st.sz R_SZ, st.w R_W' +
        ', da.unitid D_UNITID, sl.price2 R_PRISE'
      'from sinv sn, sel sl, sitem st,'
      '        art2 a2, d_art da'
      'where sn.sinvid = sl.sinvid and'
      '          sl.selid = st.selid and'
      '          sl.art2id = a2.art2id and'
      '          a2.d_artid = da.d_artid and'
      '          sn.sinvid = ?SInvId'
      'order by a2.art, st.uid')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 638
    Top = 300
    object ibdsitemSlistR_UID: TIntegerField
      FieldName = 'R_UID'
      Origin = 'SITEM.UID'
    end
    object ibdsitemSlistR_ART1: TFIBStringField
      FieldName = 'R_ART1'
      Origin = 'ART2.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsitemSlistR_COMP: TIntegerField
      FieldName = 'R_COMP'
      Origin = 'ART2.D_COMPID'
      Required = True
    end
    object ibdsitemSlistR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      Origin = 'ART2.D_MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsitemSlistR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      Origin = 'D_GOOD.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsitemSlistR_A2: TFIBStringField
      FieldName = 'R_A2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsitemSlistR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Origin = 'SITEM.SZ'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsitemSlistR_W: TFloatField
      FieldName = 'R_W'
      Origin = 'SITEM.W'
      Required = True
    end
    object ibdsitemSlistD_UNITID: TIntegerField
      FieldName = 'D_UNITID'
      Origin = 'D_ART.UNITID'
      Required = True
    end
    object ibdsitemSlistR_PRISE: TFloatField
      FieldName = 'R_PRISE'
      Origin = 'SEL.PRICE2'
      Required = True
    end
  end
  object frdsSitemSlist: TfrDBDataSet
    DataSet = ibdsitemSlist
    Left = 561
    Top = 160
  end
  object taTicket: TpFIBDataSet
    SelectSQL.Strings = (
      'select CHECKNO '
      'from SellItem '
      'where SellItemId=:SEllITEMID')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 709
    Top = 489
    dcForceMasterRefresh = True
    dcForceOpen = True
    object taTicketCHECKNO: TIntegerField
      FieldName = 'CHECKNO'
      Origin = 'SELLITEM.CHECKNO'
    end
  end
  object frTicket: TfrDBDataSet
    DataSet = taTicket
    Left = 709
    Top = 536
  end
  object dsrTicket: TDataSource
    DataSet = taTicket
    Left = 709
    Top = 584
  end
  object ibdsTicketIt1: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select R_FullArt, R_ART2, R_SIZE, R_W,  R_PRICE, R_UID,         ' +
        '  R_DEPNAME, R_SellDate, R_CITY, R_ADDRESS, R_COST, R_PHONE,'
      'R_UnitId, R_MAT, R_GOOD, R_ART, R_ID, R_DiscPrice, R_Discount,'
      'R_DiscCost, R_Ret, R_EMPNAME, R_CHECKNO, In_PRICE, R_UNIT, '
      'NodCard, InsVal'
      'from DOC_TICKET_U950(:I_ID,:I_KIND,:I_CHEKNO)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 304
    object ibdsTicketIt1R_FULLART: TFIBStringField
      FieldName = 'R_FULLART'
      Origin = 'DOC_TICKET_U950.R_FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_ART2: TFIBStringField
      FieldName = 'R_ART2'
      Origin = 'DOC_TICKET_U950.R_ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_SIZE: TFIBStringField
      FieldName = 'R_SIZE'
      Origin = 'DOC_TICKET_U950.R_SIZE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_W: TFloatField
      FieldName = 'R_W'
      Origin = 'DOC_TICKET_U950.R_W'
    end
    object ibdsTicketIt1R_PRICE: TFloatField
      FieldName = 'R_PRICE'
      Origin = 'DOC_TICKET_U950.R_PRICE'
    end
    object ibdsTicketIt1R_UID: TIntegerField
      FieldName = 'R_UID'
      Origin = 'DOC_TICKET_U950.R_UID'
    end
    object ibdsTicketIt1R_DEPNAME: TFIBStringField
      FieldName = 'R_DEPNAME'
      Origin = 'DOC_TICKET_U950.R_DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_SELLDATE: TDateTimeField
      FieldName = 'R_SELLDATE'
      Origin = 'DOC_TICKET_U950.R_SELLDATE'
    end
    object ibdsTicketIt1R_CITY: TFIBStringField
      FieldName = 'R_CITY'
      Origin = 'DOC_TICKET_U950.R_CITY'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_ADDRESS: TFIBStringField
      FieldName = 'R_ADDRESS'
      Origin = 'DOC_TICKET_U950.R_ADDRESS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_COST: TFloatField
      FieldName = 'R_COST'
      Origin = 'DOC_TICKET_U950.R_COST'
    end
    object ibdsTicketIt1R_PHONE: TFIBStringField
      FieldName = 'R_PHONE'
      Origin = 'DOC_TICKET_U950.R_PHONE'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_UNITID: TIntegerField
      FieldName = 'R_UNITID'
      Origin = 'DOC_TICKET_U950.R_UNITID'
    end
    object ibdsTicketIt1R_MAT: TFIBStringField
      FieldName = 'R_MAT'
      Origin = 'DOC_TICKET_U950.R_MAT'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      Origin = 'DOC_TICKET_U950.R_GOOD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_ART: TFIBStringField
      FieldName = 'R_ART'
      Origin = 'DOC_TICKET_U950.R_ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_ID: TIntegerField
      FieldName = 'R_ID'
      Origin = 'DOC_TICKET_U950.R_ID'
    end
    object ibdsTicketIt1R_DISCPRICE: TFloatField
      FieldName = 'R_DISCPRICE'
      Origin = 'DOC_TICKET_U950.R_DISCPRICE'
    end
    object ibdsTicketIt1R_DISCOUNT: TFloatField
      FieldName = 'R_DISCOUNT'
      Origin = 'DOC_TICKET_U950.R_DISCOUNT'
    end
    object ibdsTicketIt1R_DISCCOST: TFloatField
      FieldName = 'R_DISCCOST'
      Origin = 'DOC_TICKET_U950.R_DISCCOST'
    end
    object ibdsTicketIt1R_RET: TSmallintField
      FieldName = 'R_RET'
      Origin = 'DOC_TICKET_U950.R_RET'
    end
    object ibdsTicketIt1R_EMPNAME: TFIBStringField
      FieldName = 'R_EMPNAME'
      Origin = 'DOC_TICKET_U950.R_EMPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsTicketIt1R_CHECKNO: TIntegerField
      FieldName = 'R_CHECKNO'
      Origin = 'DOC_TICKET_U950.R_CHECKNO'
    end
    object ibdsTicketIt1IN_PRICE: TFloatField
      FieldName = 'IN_PRICE'
      Origin = 'DOC_TICKET_U950.IN_PRICE'
    end
    object ibdsTicketIt1R_UNIT: TFIBStringField
      FieldName = 'R_UNIT'
      Origin = 'DOC_TICKET_U950.R_UNIT'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsTicketIt1NODCARD: TFIBStringField
      FieldName = 'NODCARD'
      Origin = 'DOC_TICKET_U950.NODCARD'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsTicketIt1INSVAL: TFIBStringField
      FieldName = 'INSVAL'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object frBarCodeObject1: TfrBarCodeObject
    Left = 1017
    Top = 163
  end
  object frdsInventory: TfrDBDataSet
    DataSet = taInventoryBefore
    Left = 642
    Top = 580
  end
  object taInventoryBefore: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.*'
      'from DOC_INVENTORYBEFORE(:SINVID, :MATID1, :MATID2,'
      '     :GOODID1, :GOODID2, :SUPID1, :SUPID2,'
      '     :COMPID1, :COMPID2, :GOODSID1_1, :GOODSID1_2,              '
      
        '     :GOODSID2_1, :GOODSID2_2, :ATT1_1, :ATT1_2, :ATT2_1, :ATT2_' +
        '2) i'
      'where 1=1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '       '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'Order by i.d_matid, i.art, i.uid')
    BeforeOpen = taInventoryBeforeBeforeOpen
    OnCalcFields = taInventoryBeforeCalcFields
    Transaction = trReport
    Database = dmCom.db
    Left = 642
    Top = 536
    poSQLINT64ToBCD = True
    object taInventoryBeforeINVENTORYID: TFIBIntegerField
      FieldName = 'INVENTORYID'
    end
    object taInventoryBeforeUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taInventoryBeforeSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryBeforeW: TFIBFloatField
      FieldName = 'W'
    end
    object taInventoryBeforeISIN: TFIBSmallIntField
      FieldName = 'ISIN'
    end
    object taInventoryBeforeUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object taInventoryBeforePRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object taInventoryBeforeFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taInventoryBeforen: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'n'
      Calculated = True
    end
    object taInventoryBeforecost: TFloatField
      FieldKind = fkCalculated
      FieldName = 'cost'
      Calculated = True
    end
  end
  object taRec: TpFIBDataSet
    SelectSQL.Strings = (
      'select d_compid, inverdate, invprordN, invprorddate,'
      '       NameComp, BUH, PresedentPOST, MOL,'
      '       PresedentFIO, RESPONSIBLE_Person, InvDate, '
      '       DEPNAME, BOSS'
      'from Head_Inventory(:DEPID, :sinvId)')
    OnCalcFields = taRecCalcFields
    Transaction = trReport
    Database = dmCom.db
    Left = 875
    Top = 256
    poSQLINT64ToBCD = True
    object taRecD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taRecINVERDATE: TFIBDateField
      FieldName = 'INVERDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taRecINVPRORDN: TFIBIntegerField
      FieldName = 'INVPRORDN'
    end
    object taRecINVPRORDDATE: TFIBDateField
      FieldName = 'INVPRORDDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taRecNAMECOMP: TFIBStringField
      FieldName = 'NAMECOMP'
      Size = 30
      EmptyStrToNull = True
    end
    object taRecBUH: TFIBStringField
      FieldName = 'BUH'
      Size = 60
      EmptyStrToNull = True
    end
    object taRecMOL: TFIBStringField
      FieldName = 'MOL'
      Size = 2000
      EmptyStrToNull = True
    end
    object taRecPRESEDENTFIO: TFIBStringField
      FieldName = 'PRESEDENTFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRecRESPONSIBLE_PERSON: TFIBStringField
      FieldName = 'RESPONSIBLE_PERSON'
      Size = 60
      EmptyStrToNull = True
    end
    object taRecLastSentens: TStringField
      FieldKind = fkCalculated
      FieldName = 'LastSentens'
      Size = 500
      Calculated = True
    end
    object taRecINVDATE: TFIBDateField
      FieldName = 'INVDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taRecDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taRecBOSS: TFIBStringField
      FieldName = 'BOSS'
      Size = 60
      EmptyStrToNull = True
    end
    object taRecPRESEDENTPOST: TFIBStringField
      FieldName = 'PRESEDENTPOST'
      Size = 90
      EmptyStrToNull = True
    end
  end
  object frdsmol: TfrDBDataSet
    DataSet = taMol
    Left = 871
    Top = 352
  end
  object taMol: TpFIBDataSet
    SelectSQL.Strings = (
      'select r.d_compid, '#39#39' st, m.FIO'
      'from d_mol m, d_rec r'
      'where m.d_compid=r.d_compid')
    Transaction = trReport
    Database = dmCom.db
    Description = #1052#1072#1090#1077#1088#1080#1072#1083#1100#1085#1086' '#1086#1090#1074#1077#1090#1089#1074#1077#1085#1085#1099#1077' '#1083#1080#1094#1072
    Left = 873
    Top = 304
    poSQLINT64ToBCD = True
    object taMolD_COMPID: TFIBIntegerField
      FieldName = 'D_COMPID'
    end
    object taMolST: TFIBStringField
      FieldName = 'ST'
      Size = 0
      EmptyStrToNull = True
    end
    object taMolFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object taInventoryEnd: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.*'
      
        'from DOC_INVENTORYAFTER(:SINVID, :DEPID1, :DEPID2, :MATID1, :MAT' +
        'ID2, '
      '      :GOODID1, :GOODID2, :SUPID1, :SUPID2,       '
      
        '      :COMPID1, :COMPID2, :GOODSID1_1, :GOODSID1_2,             ' +
        ' '
      '      :GOODSID2_1, :GOODSID2_2, :ATT1_1, :ATT1_2,              '
      '      :ATT2_1, :ATT2_2) i'
      'where 1=1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'Order by i.d_matid, i.art, i.uid')
    BeforeOpen = taInventoryEndBeforeOpen
    OnCalcFields = taInventoryEndCalcFields
    Transaction = trReport
    Database = dmCom.db
    Left = 642
    Top = 440
    poSQLINT64ToBCD = True
    object taInventoryEndRECN: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RECN'
      Calculated = True
    end
    object taInventoryEndINVENTORYID: TFIBIntegerField
      FieldName = 'INVENTORYID'
    end
    object taInventoryEndUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taInventoryEndSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryEndW: TFIBFloatField
      FieldName = 'W'
    end
    object taInventoryEndISIN: TFIBSmallIntField
      FieldName = 'ISIN'
    end
    object taInventoryEndUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object taInventoryEndPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object taInventoryEndFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taInventoryEndCOST: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COST'
      Calculated = True
    end
  end
  object frdsInventoryE: TfrDBDataSet
    DataSet = taInventoryEnd
    Left = 642
    Top = 488
  end
  object taD_Emp: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.d_molid, cast(m.FIO as char(60)) FIO, '
      '      cast(m.Post as char(30)) Post'
      'from d_mol m'
      'where (m.member=3 or m.member=4 );')
    OnCalcFields = taD_EmpCalcFields
    Transaction = trReport
    Database = dmCom.db
    Description = #1063#1083#1077#1085#1099' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1086#1085#1085#1086#1081' '#1082#1086#1084#1084#1080#1089#1089#1080#1080
    Left = 871
    Top = 488
    poSQLINT64ToBCD = True
    object taD_EmpD_MOLID: TFIBIntegerField
      FieldName = 'D_MOLID'
    end
    object taD_EmpFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taD_EmpPOST: TFIBStringField
      FieldName = 'POST'
      Size = 30
      EmptyStrToNull = True
    end
    object taD_EmpPostFIO: TStringField
      FieldKind = fkCalculated
      FieldName = 'PostFIO'
      Size = 100
      Calculated = True
    end
  end
  object taInventoryAct_N: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.*'
      'from DOC_INVENTORYACT(:SINVID, :MATID1, :MATID2,'
      '     :GOODID1, :GOODID2, :SUPID1, :SUPID2, :COMPID1,'
      '     :COMPID2, :GOODSID1_1, :GOODSID1_2, :GOODSID2_1,'
      '     :GOODSID2_2, :ATT1_1, :ATT1_2, :ATT2_1, :ATT2_2,'
      '     :DEPID1, :DEPID2, 0, :ISOUTPRICE) i'
      'where 1=1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'Order by i.d_matid, i.art, i.uid'
      '     ')
    BeforeOpen = taInventoryAct_NBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    Left = 789
    Top = 400
    poSQLINT64ToBCD = True
    object taInventoryAct_NUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taInventoryAct_NSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_NW: TFIBFloatField
      FieldName = 'W'
    end
    object taInventoryAct_NUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object taInventoryAct_NPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object taInventoryAct_NCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taInventoryAct_NFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taInventoryAct_ND_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_ND_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_NCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taInventoryAct_NSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taInventoryAct_ND_GOODS_SAM1: TFIBIntegerField
      FieldName = 'D_GOODS_SAM1'
    end
    object taInventoryAct_ND_GOODS_SAM2: TFIBIntegerField
      FieldName = 'D_GOODS_SAM2'
    end
    object taInventoryAct_NATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object taInventoryAct_NATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object taInventoryAct_NART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taInventoryAct_NISOUTPRICEPRN: TFIBIntegerField
      FieldName = 'ISOUTPRICEPRN'
    end
  end
  object frD_Emp: TfrDBDataSet
    DataSet = taD_Emp
    Left = 871
    Top = 532
  end
  object taInventoryAct_E: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.*'
      'from DOC_INVENTORYACT(:SINVID, :MATID1, :MATID2,'
      '     :GOODID1, :GOODID2, :SUPID1, :SUPID2, :COMPID1,'
      '     :COMPID2, :GOODSID1_1, :GOODSID1_2, :GOODSID2_1,'
      '     :GOODSID2_2, :ATT1_1, :ATT1_2, :ATT2_1, :ATT2_2,'
      '     :DEPID1, :DEPID2, 2, :ISOUTPRICE) i'
      'where 1=1'
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      'Order by i.d_matid, i.art, i.uid'
      '     ')
    BeforeOpen = taInventoryAct_EBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    Left = 787
    Top = 488
    poSQLINT64ToBCD = True
    object taInventoryAct_EUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taInventoryAct_ESZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_EW: TFIBFloatField
      FieldName = 'W'
    end
    object taInventoryAct_EUNITID: TFIBSmallIntField
      FieldName = 'UNITID'
    end
    object taInventoryAct_EPRICE0: TFIBFloatField
      FieldName = 'PRICE0'
    end
    object taInventoryAct_ECOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taInventoryAct_EFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taInventoryAct_ED_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_ED_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taInventoryAct_ECOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taInventoryAct_ESUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taInventoryAct_ED_GOODS_SAM1: TFIBIntegerField
      FieldName = 'D_GOODS_SAM1'
    end
    object taInventoryAct_ED_GOODS_SAM2: TFIBIntegerField
      FieldName = 'D_GOODS_SAM2'
    end
    object taInventoryAct_EATT1: TFIBIntegerField
      FieldName = 'ATT1'
    end
    object taInventoryAct_EATT2: TFIBIntegerField
      FieldName = 'ATT2'
    end
    object taInventoryAct_EART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taInventoryAct_EISOUTPRICEPRN: TFIBIntegerField
      FieldName = 'ISOUTPRICEPRN'
    end
  end
  object frInventoryAct_N: TfrDBDataSet
    DataSet = taInventoryAct_N
    Left = 789
    Top = 444
  end
  object frInventoryAct_E: TfrDBDataSet
    DataSet = taInventoryAct_E
    Left = 787
    Top = 532
  end
  object taMol2: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.FIO'
      'from d_mol m, d_rec r'
      'where m.d_compid=r.d_compid and'
      '      not (m.Presedent=1 or m.Member=1) and'
      '      m.D_DepId between :DEPID1 and :DEPID2 ')
    Transaction = trReport
    Database = dmCom.db
    Left = 871
    Top = 400
    poSQLINT64ToBCD = True
    object taMol2FIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frD_Mol2: TfrDBDataSet
    DataSet = taMol2
    Left = 871
    Top = 444
  end
  object ibdsApplDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select appldepid, uid, w, sz, art2id, art,'
      '       prodcode, d_goodid, d_insid, d_matid, art2,'
      '       namemat, namecomp, nameins, namegood,'
      '       namedep, cost, costp, sdate, namedepfrom, sn, comment'
      'from DOC_APPLDEP (:SINVID, :I_KIND)')
    Transaction = dmCom.tr
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 791
    Top = 63
    poSQLINT64ToBCD = True
    object ibdsApplDepAPPLDEPID: TFIBIntegerField
      FieldName = 'APPLDEPID'
    end
    object ibdsApplDepUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object ibdsApplDepW: TFIBFloatField
      FieldName = 'W'
    end
    object ibdsApplDepSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplDepART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object ibdsApplDepART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsApplDepPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplDepD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplDepD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplDepD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsApplDepART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsApplDepNAMEMAT: TFIBStringField
      FieldName = 'NAMEMAT'
      EmptyStrToNull = True
    end
    object ibdsApplDepNAMECOMP: TFIBStringField
      FieldName = 'NAMECOMP'
      EmptyStrToNull = True
    end
    object ibdsApplDepNAMEINS: TFIBStringField
      FieldName = 'NAMEINS'
      EmptyStrToNull = True
    end
    object ibdsApplDepNAMEGOOD: TFIBStringField
      FieldName = 'NAMEGOOD'
      EmptyStrToNull = True
    end
    object ibdsApplDepNAMEDEP: TFIBStringField
      FieldName = 'NAMEDEP'
      EmptyStrToNull = True
    end
    object ibdsApplDepCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object ibdsApplDepCOSTP: TFIBFloatField
      FieldName = 'COSTP'
    end
    object ibdsApplDepSDATE: TFIBDateTimeField
      FieldName = 'SDATE'
    end
    object ibdsApplDepNAMEDEPFROM: TFIBStringField
      FieldName = 'NAMEDEPFROM'
      EmptyStrToNull = True
    end
    object ibdsApplDepSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object ibdsApplDepCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 200
      EmptyStrToNull = True
    end
  end
  object dsApplDep: TDataSource
    DataSet = ibdsApplDep
    Left = 791
    Top = 110
  end
  object frdsApplDep: TfrDBDataSet
    DataSet = ibdsApplDep
    Left = 790
    Top = 160
  end
  object frdsClientAddress: TfrDBDataSet
    DataSet = quClientAddress
    Left = 874
    Top = 208
  end
  object quClientAddress: TpFIBDataSet
    SelectSQL.Strings = (
      'select Name, Address, city, home_flat, companyname,'
      '       companyaddress '
      'from Print_LetterClient')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 874
    Top = 160
    poSQLINT64ToBCD = True
    object quClientAddressNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object quClientAddressADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 100
      EmptyStrToNull = True
    end
    object quClientAddressHOME_FLAT: TFIBStringField
      FieldName = 'HOME_FLAT'
      Size = 30
      EmptyStrToNull = True
    end
    object quClientAddressCITY: TFIBStringField
      FieldName = 'CITY'
      EmptyStrToNull = True
    end
    object quClientAddressCOMPANYNAME: TFIBStringField
      FieldName = 'COMPANYNAME'
      EmptyStrToNull = True
    end
    object quClientAddressCOMPANYADDRESS: TFIBStringField
      FieldName = 'COMPANYADDRESS'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object taShopReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO,'
      '       it.DOCDATE, it.Q, it.W, it.COST, it.K,'
      '       i.SDATE reportbd, i.NDATE reported, i.SN reportno,'
      '       d.NAME shopname, e.FNAME, e.LNAME, e.SNAME'
      'From SInv i, ShopReport_P(:SINVID) it, D_Dep d, D_Emp e'
      'where i.SINVID=it.SINVID and'
      '      i.DEPID=d.D_DEPID and'
      '      i.USERID = e.D_EMPID')
    Transaction = trReport
    Database = dmCom.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1058#1086#1074#1072#1088#1085#1099#1081' '#1086#1090#1095#1077#1090' '#1087#1086' '#1084#1072#1075#1072#1079#1080#1085#1072#1084
    Left = 713
    Top = 63
    poSQLINT64ToBCD = True
    object taShopReportID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taShopReportSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taShopReportT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taShopReportTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taShopReportDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object taShopReportDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taShopReportQ: TFIBFloatField
      FieldName = 'Q'
    end
    object taShopReportW: TFIBFloatField
      FieldName = 'W'
    end
    object taShopReportCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taShopReportK: TFIBSmallIntField
      FieldName = 'K'
    end
    object taShopReportREPORTBD: TFIBDateTimeField
      FieldName = 'REPORTBD'
    end
    object taShopReportREPORTED: TFIBDateTimeField
      FieldName = 'REPORTED'
    end
    object taShopReportREPORTNO: TFIBIntegerField
      FieldName = 'REPORTNO'
    end
    object taShopReportSHOPNAME: TFIBStringField
      FieldName = 'SHOPNAME'
      EmptyStrToNull = True
    end
    object taShopReportFNAME: TFIBStringField
      FieldName = 'FNAME'
      EmptyStrToNull = True
    end
    object taShopReportLNAME: TFIBStringField
      FieldName = 'LNAME'
      EmptyStrToNull = True
    end
    object taShopReportSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object frShopReport: TfrDBDataSet
    DataSet = taShopReport
    Left = 714
    Top = 108
  end
  object frdsUid_Store: TfrDBDataSet
    DataSet = ibdsUID_Store
    Left = 791
    Top = 255
  end
  object ibdsUID_Store: TpFIBDataSet
    SelectSQL.Strings = (
      'select BD,ED, GOODSNAME, ART, UNIT, BDQ, BDS,'
      '       INQ, INS, OUTQ, OUTS, EDQ, EDS, Buh '
      'from UID_StorePrn(:SInvID)')
    BeforeOpen = ibdsUID_StoreBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 791
    Top = 208
    poSQLINT64ToBCD = True
    object ibdsUID_StoreBD: TFIBDateTimeField
      FieldName = 'BD'
    end
    object ibdsUID_StoreED: TFIBDateTimeField
      FieldName = 'ED'
    end
    object ibdsUID_StoreGOODSNAME: TFIBStringField
      FieldName = 'GOODSNAME'
      EmptyStrToNull = True
    end
    object ibdsUID_StoreART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsUID_StoreUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUID_StoreBDQ: TFIBIntegerField
      FieldName = 'BDQ'
    end
    object ibdsUID_StoreBDS: TFIBFloatField
      FieldName = 'BDS'
    end
    object ibdsUID_StoreINQ: TFIBIntegerField
      FieldName = 'INQ'
    end
    object ibdsUID_StoreINS: TFIBFloatField
      FieldName = 'INS'
    end
    object ibdsUID_StoreOUTQ: TFIBIntegerField
      FieldName = 'OUTQ'
    end
    object ibdsUID_StoreOUTS: TFIBFloatField
      FieldName = 'OUTS'
    end
    object ibdsUID_StoreEDQ: TFIBIntegerField
      FieldName = 'EDQ'
    end
    object ibdsUID_StoreEDS: TFIBFloatField
      FieldName = 'EDS'
    end
    object ibdsUID_StoreBUH: TFIBStringField
      FieldName = 'BUH'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object ibdsUIDWH_Report: TpFIBDataSet
    SelectSQL.Strings = (
      'select BD, Comp, Sup, Art, Unit, Price, Q, W, Summ'
      'from UIDWH_Report(:UserID, :DepID, :BD)')
    BeforeOpen = ibdsUIDWH_ReportBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 791
    Top = 304
    poSQLINT64ToBCD = True
    object ibdsUIDWH_ReportBD: TFIBDateTimeField
      FieldName = 'BD'
    end
    object ibdsUIDWH_ReportCOMP: TFIBStringField
      FieldName = 'COMP'
      EmptyStrToNull = True
    end
    object ibdsUIDWH_ReportSUP: TFIBStringField
      FieldName = 'SUP'
      EmptyStrToNull = True
    end
    object ibdsUIDWH_ReportART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsUIDWH_ReportUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsUIDWH_ReportPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object ibdsUIDWH_ReportQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object ibdsUIDWH_ReportW: TFIBFloatField
      FieldName = 'W'
    end
    object ibdsUIDWH_ReportSUMM: TFIBFloatField
      FieldName = 'SUMM'
    end
  end
  object frUIDWH_Report: TfrDBDataSet
    DataSet = ibdsUIDWH_Report
    Left = 791
    Top = 352
  end
  object ibdsHatWrite_off: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  Sn,'
      '  SDate, '
      '  ComPresnt,'
      '  ComMembers, '
      '  stretrim(SPOILINGREASON) SPOILINGREASON,'
      '  stretrim(DefectDiscript) DefectDiscript,'
      '  stretrim(WriteOffReason) WriteOffReason, '
      '  stretrim(Solution) Solution,'
      '  stretrim(ActName) ActName,'
      '  Company, '
      '  Address,'
      '  RecipientCompany,'
      '  RecipientAddress    '
      'from WriteOff_Prn(:SInvID)')
    BeforeOpen = ibdsHatWrite_offBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    Left = 787
    Top = 584
    poSQLINT64ToBCD = True
    object ibdsHatWrite_offSN: TFIBIntegerField
      FieldName = 'SN'
    end
    object ibdsHatWrite_offSDATE: TFIBDateField
      FieldName = 'SDATE'
    end
    object ibdsHatWrite_offCOMPRESNT: TFIBStringField
      FieldName = 'COMPRESNT'
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offCOMMEMBERS: TFIBStringField
      FieldName = 'COMMEMBERS'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offSPOILINGREASON: TFIBStringField
      FieldName = 'SPOILINGREASON'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offDEFECTDISCRIPT: TFIBStringField
      FieldName = 'DEFECTDISCRIPT'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offWRITEOFFREASON: TFIBStringField
      FieldName = 'WRITEOFFREASON'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offSOLUTION: TFIBStringField
      FieldName = 'SOLUTION'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offACTNAME: TFIBStringField
      FieldName = 'ACTNAME'
      Size = 256
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offCOMPANY: TFIBStringField
      FieldName = 'COMPANY'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offRECIPIENTCOMPANY: TFIBStringField
      FieldName = 'RECIPIENTCOMPANY'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsHatWrite_offRECIPIENTADDRESS: TFIBStringField
      FieldName = 'RECIPIENTADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object frHatwrite_off: TfrDBDataSet
    DataSet = ibdsHatWrite_off
    Left = 787
    Top = 632
  end
  object ibdsWrite_off: TpFIBDataSet
    SelectSQL.Strings = (
      'select PRODCODE, D_MATID, D_GOODID, D_INSID, ART, ART2,'
      '      sum(COST) cost, sum(SCOST) SPrice, '
      '      count(SITEMID) q, sum(W) w'
      'from Actallowances_s(:SInvID)'
      'Group by PRODCODE, D_MATID, D_GOODID, D_INSID, ART, ART2')
    BeforeOpen = ibdsHatWrite_offBeforeOpen
    Transaction = trReport
    Database = dmCom.db
    Left = 263
    Top = 640
    poSQLINT64ToBCD = True
    object ibdsWrite_offD_MATID: TFIBStringField
      FieldName = 'D_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsWrite_offD_GOODID: TFIBStringField
      FieldName = 'D_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsWrite_offD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsWrite_offART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsWrite_offART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsWrite_offSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
    object ibdsWrite_offCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object ibdsWrite_offQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object ibdsWrite_offPRODCODE: TFIBStringField
      FieldName = 'PRODCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsWrite_offW: TFIBFloatField
      FieldName = 'W'
    end
  end
  object frWrite_off: TfrDBDataSet
    DataSet = ibdsWrite_off
    Left = 261
    Top = 688
  end
  object frNodCard: TfrDBDataSet
    DataSet = quSelectNodCard
    Left = 874
    Top = 111
  end
  object quSelectNodCard: TpFIBDataSet
    SelectSQL.Strings = (
      'select nodcard'
      'from Select_NodCard')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 874
    Top = 63
    poSQLINT64ToBCD = True
  end
  object frCenterReport: TfrDBDataSet
    DataSet = taCenterReport
    Left = 175
    Top = 496
  end
  object taCenterReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      '  it.DOCDATE, it.Q, it.W, it.COST, it.K, '
      '  i.SDATE REPORTBD, i.NDATE REPORTED, i.SN REPORTNO,'
      
        '  d.NAME SHOPNAME, e.FNAME, e.LNAME, e.SNAME, it.cost2, it.cost3' +
        ', it.buhrep'
      'from SInv i, ShopReport_S(:SINVID) it, D_Dep d, D_Emp e'
      'where i.SINVID=it.SINVID and'
      '      i.DEPID=d.D_DEPID and'
      '      i.USERID = e.D_EMPID and'
      '      it.t in (114, 104, 102, 105, 107) '
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 175
    Top = 544
    poSQLINT64ToBCD = True
    object taCenterReportID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCenterReportSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taCenterReportT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taCenterReportTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 110
      EmptyStrToNull = True
    end
    object taCenterReportDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object taCenterReportDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taCenterReportQ: TFIBFloatField
      FieldName = 'Q'
    end
    object taCenterReportW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taCenterReportCOST: TFIBFloatField
      FieldName = 'COST'
      currency = True
    end
    object taCenterReportK: TFIBSmallIntField
      FieldName = 'K'
    end
    object taCenterReportREPORTBD: TFIBDateTimeField
      FieldName = 'REPORTBD'
    end
    object taCenterReportREPORTED: TFIBDateTimeField
      FieldName = 'REPORTED'
    end
    object taCenterReportREPORTNO: TFIBIntegerField
      FieldName = 'REPORTNO'
    end
    object taCenterReportSHOPNAME: TFIBStringField
      FieldName = 'SHOPNAME'
      EmptyStrToNull = True
    end
    object taCenterReportFNAME: TFIBStringField
      FieldName = 'FNAME'
      EmptyStrToNull = True
    end
    object taCenterReportLNAME: TFIBStringField
      FieldName = 'LNAME'
      EmptyStrToNull = True
    end
    object taCenterReportSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taCenterReportCOST2: TFIBFloatField
      FieldName = 'COST2'
      currency = True
    end
    object taCenterReportCOST3: TFIBFloatField
      FieldName = 'COST3'
      currency = True
    end
    object taCenterReportBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object quCenterIncoming: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t=0')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 391
    Top = 160
    poSQLINT64ToBCD = True
    object quCenterIncomingID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterIncomingSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterIncomingT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterIncomingTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterIncomingDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterIncomingDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterIncomingQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterIncomingW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterIncomingCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterIncomingK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterIncomingCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterIncomingCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterIncomingBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frCenterIncoming: TfrDBDataSet
    DataSet = quCenterIncoming
    Left = 407
    Top = 208
  end
  object frCenterItogIn: TfrDBDataSet
    DataSet = quCenterItogIn
    Left = 407
    Top = 304
  end
  object quCenterItogIn: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t in (-2, -3)'
      'order by it.t desc')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 407
    Top = 256
    poSQLINT64ToBCD = True
    object quCenterItogInID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterItogInSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterItogInT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterItogInTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterItogInDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterItogInDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterItogInQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterItogInW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterItogInCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterItogInK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterItogInCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterItogInCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterItogInBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object quCenterOut: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t in (106, 108, 109, 111, -6)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 407
    Top = 352
    poSQLINT64ToBCD = True
    object quCenterOutID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterOutSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterOutT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterOutTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterOutDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterOutDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterOutQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterOutW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterOutCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterOutK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterOutCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterOutCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterOutBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frCenterOut: TfrDBDataSet
    DataSet = quCenterOut
    Left = 407
    Top = 400
  end
  object frCenterItogOut: TfrDBDataSet
    DataSet = quCenterItogOut
    Left = 407
    Top = 496
  end
  object quCenterItogOut: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t in (-5)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 407
    Top = 448
    poSQLINT64ToBCD = True
    object quCenterItogOutID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterItogOutSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterItogOutT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterItogOutTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterItogOutDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterItogOutDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterItogOutQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterItogOutW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterItogOutCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterItogOutK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterItogOutCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterItogOutCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterItogOutBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object quCenterItog: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t in (999)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 423
    Top = 544
    poSQLINT64ToBCD = True
    object quCenterItogID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterItogSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterItogT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterItogTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterItogDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterItogDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterItogQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterItogW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterItogCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterItogK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterItogCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterItogCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterItogBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frCenterItog: TfrDBDataSet
    DataSet = quCenterItog
    Left = 407
    Top = 592
  end
  object quCenterDefferent: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.SINVID, it.T, it.TNAME, it.DOCNO, '
      
        '  it.DOCDATE, it.Q, it.W, it.COST, it.K, it.cost2, it.cost3, it.' +
        'buhrep'
      'from ShopReport_S(:SINVID) it'
      'where it.t in (-7,115, -9, 116)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 407
    Top = 640
    poSQLINT64ToBCD = True
    object quCenterDefferentID: TFIBIntegerField
      FieldName = 'ID'
    end
    object quCenterDefferentSINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object quCenterDefferentT: TFIBSmallIntField
      FieldName = 'T'
    end
    object quCenterDefferentTNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quCenterDefferentDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object quCenterDefferentDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object quCenterDefferentQ: TFIBFloatField
      FieldName = 'Q'
    end
    object quCenterDefferentW: TFIBFloatField
      FieldName = 'W'
    end
    object quCenterDefferentCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object quCenterDefferentK: TFIBSmallIntField
      FieldName = 'K'
    end
    object quCenterDefferentCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quCenterDefferentCOST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object quCenterDefferentBUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frCenterDefferent: TfrDBDataSet
    DataSet = quCenterDefferent
    Left = 407
    Top = 688
  end
  object quUidWhArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select art2id, art2, art, q, price2, cost2,'
      '       w, unitid, mat, good, depname,'
      '       cenname, addressd, addressc,'
      '       phoned, phonec, setdate, nprord'
      'from DOC_INV_WH (:kind, :userid, :depid)')
    BeforeOpen = quUidWhArtBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 263
    Top = 112
    poSQLINT64ToBCD = True
    object quUidWhArtART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object quUidWhArtART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object quUidWhArtQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object quUidWhArtPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object quUidWhArtCOST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object quUidWhArtW: TFIBFloatField
      FieldName = 'W'
    end
    object quUidWhArtUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object quUidWhArtMAT: TFIBStringField
      FieldName = 'MAT'
      Size = 30
      EmptyStrToNull = True
    end
    object quUidWhArtGOOD: TFIBStringField
      FieldName = 'GOOD'
      Size = 30
      EmptyStrToNull = True
    end
    object quUidWhArtART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object quUidWhArtDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quUidWhArtCENNAME: TFIBStringField
      FieldName = 'CENNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object quUidWhArtADDRESSD: TFIBStringField
      FieldName = 'ADDRESSD'
      Size = 100
      EmptyStrToNull = True
    end
    object quUidWhArtADDRESSC: TFIBStringField
      FieldName = 'ADDRESSC'
      Size = 100
      EmptyStrToNull = True
    end
    object quUidWhArtPHONED: TFIBStringField
      FieldName = 'PHONED'
      EmptyStrToNull = True
    end
    object quUidWhArtPHONEC: TFIBStringField
      FieldName = 'PHONEC'
      EmptyStrToNull = True
    end
    object quUidWhArtSETDATE: TFIBDateTimeField
      FieldName = 'SETDATE'
    end
    object quUidWhArtNPRORD: TFIBSmallIntField
      FieldName = 'NPRORD'
    end
  end
  object frUidWhArt: TfrDBDataSet
    DataSet = quUidWhArt
    Left = 263
    Top = 160
  end
  object quGrIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from InsByArt2(:Art2Id)')
    BeforeOpen = quGrInsBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    DataSource = dsUidWhArt
    Left = 335
    Top = 208
    poSQLINT64ToBCD = True
    object quGrInsR_UID: TFIBIntegerField
      FieldName = 'R_UID'
    end
    object quGrInsR_INSCODE: TFIBStringField
      FieldName = 'R_INSCODE'
      Size = 10
      EmptyStrToNull = True
    end
    object quGrInsR_INSNAME: TFIBStringField
      FieldName = 'R_INSNAME'
      Size = 400
      EmptyStrToNull = True
    end
    object quGrInsR_ETYPE: TFIBStringField
      FieldName = 'R_ETYPE'
      EmptyStrToNull = True
    end
    object quGrInsR_ESHAPE: TFIBStringField
      FieldName = 'R_ESHAPE'
      EmptyStrToNull = True
    end
    object quGrInsR_Q: TFIBIntegerField
      FieldName = 'R_Q'
    end
    object quGrInsR_W: TFIBFloatField
      FieldName = 'R_W'
    end
    object quGrInsR_COLOR: TFIBStringField
      FieldName = 'R_COLOR'
      EmptyStrToNull = True
    end
    object quGrInsR_CHROMATICITY: TFIBStringField
      FieldName = 'R_CHROMATICITY'
      EmptyStrToNull = True
    end
    object quGrInsR_CLEANNES: TFIBStringField
      FieldName = 'R_CLEANNES'
      EmptyStrToNull = True
    end
    object quGrInsR_GROUP: TFIBStringField
      FieldName = 'R_GROUP'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsUidWhArt: TDataSource
    DataSet = quUidWhArt
    Left = 263
    Top = 64
  end
  object frGrIns: TfrDBDataSet
    DataSet = quGrIns
    Left = 336
    Top = 256
  end
  object ibdsApplResp: TpFIBDataSet
    SelectSQL.Strings = (
      'select CLIENTNAME, UID, W, SZ, GOOD, PRICE, ART, ART2,'
      '       MAT, UNITID, SELLDATE, COST, CRDATE, WASRET,'
      '       ADDRESS, EMPNAME, CHEKNO'
      'FROM DOC_RESP_SELL(:I_KIND, :I_RESPSTORINGID)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 24
    Top = 604
    poSQLINT64ToBCD = True
    object ibdsApplRespCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplRespUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object ibdsApplRespW: TFIBFloatField
      FieldName = 'W'
    end
    object ibdsApplRespSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = True
    end
    object ibdsApplRespGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object ibdsApplRespPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object ibdsApplRespART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsApplRespART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsApplRespMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object ibdsApplRespUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object ibdsApplRespSELLDATE: TFIBDateTimeField
      FieldName = 'SELLDATE'
    end
    object ibdsApplRespCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object ibdsApplRespCRDATE: TFIBDateTimeField
      FieldName = 'CRDATE'
    end
    object ibdsApplRespWASRET: TFIBStringField
      FieldName = 'WASRET'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsApplRespADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsApplRespEMPNAME: TFIBStringField
      FieldName = 'EMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplRespCHEKNO: TFIBIntegerField
      FieldName = 'CHEKNO'
    end
  end
  object frdsApplResp: TfrDBDataSet
    DataSet = ibdsApplResp
    Left = 24
    Top = 648
  end
  object ibdsApplRespCause: TpFIBDataSet
    SelectSQL.Strings = (
      'select CLIENTNAME, UID, W, SZ, GOOD, PRICE, ART,'
      '       ART2, MAT, UNITID, SELLDATE, COST, CRDATE,'
      '       WASRET, ADDRESS, EMPNAME, chekno'
      'from DOC_RESP_SELL(:I_KIND, :I_RESPSTORINGID)')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 176
    Top = 64
    poSQLINT64ToBCD = True
    object ibdsApplRespCauseCLIENTNAME: TFIBStringField
      FieldName = 'CLIENTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object ibdsApplRespCauseW: TFIBFloatField
      FieldName = 'W'
    end
    object ibdsApplRespCauseSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseGOOD: TFIBStringField
      FieldName = 'GOOD'
      EmptyStrToNull = True
    end
    object ibdsApplRespCausePRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object ibdsApplRespCauseART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseMAT: TFIBStringField
      FieldName = 'MAT'
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object ibdsApplRespCauseSELLDATE: TFIBDateTimeField
      FieldName = 'SELLDATE'
    end
    object ibdsApplRespCauseCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object ibdsApplRespCauseCRDATE: TFIBDateTimeField
      FieldName = 'CRDATE'
    end
    object ibdsApplRespCauseWASRET: TFIBStringField
      FieldName = 'WASRET'
      Size = 40
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 200
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseEMPNAME: TFIBStringField
      FieldName = 'EMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsApplRespCauseCHEKNO: TFIBIntegerField
      FieldName = 'CHEKNO'
    end
  end
  object frdsApplRespCause: TfrDBDataSet
    DataSet = ibdsApplRespCause
    Left = 176
    Top = 112
  end
  object frRichObject: TfrRichObject
    Left = 1011
    Top = 8
  end
  object ibdsCardCheck: TpFIBDataSet
    SelectSQL.Strings = (
      'select sl.checkno, s.rn , d.SNAME, sl.costcard,'
      '       smartround(sl.q0*sl.price, 1, 100) cost,'
      '       smartround(sl.q0*sl.price0, 1, 100) cost0,'
      '       sl.uid, a2.fullart, a2.art2, sl.sz, sl.w'
      'from SellItem sl, Sell s , D_DEP D, art2 a2'
      'where s.SellID=sl.SellID and'
      '      S.DEPID=D.D_DEPID and'
      '      D.D_DEPID between :depid1 and  :depid2 and'
      '      S.BD between :bd and :ed and'
      '      Card in (1, 2) and'
      '      sl.art2id = a2.art2id'
      'order by d.sname, s.rn, sl.checkno')
    BeforeOpen = ibdsCardCheckBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 642
    Top = 352
    object ibdsCardCheckCHECKNO: TFIBIntegerField
      FieldName = 'CHECKNO'
    end
    object ibdsCardCheckRN: TFIBIntegerField
      FieldName = 'RN'
    end
    object ibdsCardCheckSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object ibdsCardCheckCOSTCARD: TFIBFloatField
      FieldName = 'COSTCARD'
    end
    object ibdsCardCheckCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object ibdsCardCheckCOST0: TFIBFloatField
      FieldName = 'COST0'
    end
    object ibdsCardCheckUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object ibdsCardCheckFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsCardCheckART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object ibdsCardCheckSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsCardCheckW: TFIBFloatField
      FieldName = 'W'
    end
  end
  object frdsCardCheck: TfrDBDataSet
    DataSet = ibdsCardCheck
    Left = 643
    Top = 396
  end
  object taCenterReport1: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select ID, SINVID, T,TNAME, DOCNO, DOCDATE, Q, W, COST, K, ERRDO' +
        'C,'
      ' cost2, cost3, buhrep, depsel, color, sn'
      'from ShopReport_S(:SINVID)')
    Transaction = trReport
    Database = dmCom.db
    Left = 175
    Top = 640
    object taCenterReport1ID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCenterReport1SINVID: TFIBIntegerField
      FieldName = 'SINVID'
    end
    object taCenterReport1T: TFIBSmallIntField
      FieldName = 'T'
    end
    object taCenterReport1TNAME: TFIBStringField
      FieldName = 'TNAME'
      Size = 110
      EmptyStrToNull = True
    end
    object taCenterReport1DOCNO: TFIBStringField
      FieldName = 'DOCNO'
      EmptyStrToNull = True
    end
    object taCenterReport1DOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taCenterReport1Q: TFIBFloatField
      FieldName = 'Q'
    end
    object taCenterReport1W: TFIBFloatField
      FieldName = 'W'
    end
    object taCenterReport1COST: TFIBFloatField
      FieldName = 'COST'
    end
    object taCenterReport1K: TFIBSmallIntField
      FieldName = 'K'
    end
    object taCenterReport1ERRDOC: TFIBSmallIntField
      FieldName = 'ERRDOC'
    end
    object taCenterReport1COST2: TFIBFloatField
      FieldName = 'COST2'
    end
    object taCenterReport1COST3: TFIBFloatField
      FieldName = 'COST3'
    end
    object taCenterReport1BUHREP: TFIBStringField
      FieldName = 'BUHREP'
      Size = 60
      EmptyStrToNull = True
    end
    object taCenterReport1DEPSEL: TFIBIntegerField
      FieldName = 'DEPSEL'
    end
    object taCenterReport1COLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taCenterReport1SN: TFIBIntegerField
      FieldName = 'SN'
    end
  end
  object frCenterReport1: TfrDBDataSet
    DataSet = taCenterReport1
    Left = 175
    Top = 592
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 1018
    Top = 216
  end
  object frXMLExcelExport2: TfrXMLExcelExport
    Left = 1018
    Top = 264
  end
  object taUIDWHB: TpFIBDataSet
    Tag = 3
    SelectSQL.Strings = (
      
        'select a2.D_ARTID, a2.UNITID, a2.ART, a2.D_MATId Matid, a2.D_GOO' +
        'DId Goodid, a2.D_INSId InsId, '
      
        '       c.SNAme  CompCode, sum(u.W) W, a2.Art2, u.Price,  count(*' +
        ') Q, u.SPrice0 SPrice, u.euid, a2.art2 eart2,'
      
        'c.name header$company, d.ssname body$department, u.sz body$size,' +
        ' g.name body$good       from  UIDWH_T u, Art2 a2, d_comp c, d_de' +
        'p d, d_good g'
      'where  u.UserId=:USERID and'
      '       u.DepId between :DEPID1 and :DEPID2 and'
      '        u.Art2Id=a2.Art2Id and'
      '        a2.D_MatId between :MATID1 and :MATID2 and'
      '        '
      '       a2.D_GoodId between :GOODID1 and :GOODID2 and'
      ''
      '       a2.D_CompId between :COMPID1 and :COMPID2 and'
      ''
      '       u.SupId0 between :SUPID1 and :SUPID2 and'
      ''
      '       a2.Art between :ART1 and :ART2 and'
      '       c.d_compid=u.SupId0'
      ' '
      ''
      ''
      ''
      ''
      ''
      '       and u.goodsid1 between :goodsid1_1 and :goodsid1_2'
      '       and u.goodsid2 between :goodsid2_1 and :goodsid2_2'
      '       and a2.ATT1 between :ATT1_1 and :ATT1_2'
      '       and a2.ATT2 between :ATT2_1 and :ATT2_2 '
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '       and u.sz between  :SZ1 and :SZ2'
      '       and a2.art2 between :ART21 and :ART22'
      '       and d.d_depid = u.depid'
      '       and g.d_goodid = a2.D_GOODId'
      
        'group by a2.D_ArtId, a2.UnitId, a2.Art, a2.D_MatId, a2.D_GoodId,' +
        ' a2.D_InsId, c.SName, a2.Art2, u.Price, u.SPrice0,c.name, d.ssna' +
        'me, u.sz, g.name, u.euid'
      'order by a2.Art, a2.Art2')
    BeforeOpen = taUIDWHABeforeOpen
    OnCalcFields = OnArt2Map2
    Transaction = trReport
    Database = dmCom.db
    OnFilterRecord = taUIDWHAFilterRecord
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 487
    Top = 444
    object taUIDWHBD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object taUIDWHBUNITID: TFIBIntegerField
      FieldName = 'UNITID'
    end
    object taUIDWHBART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taUIDWHBMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHBGOODID: TFIBStringField
      FieldName = 'GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHBINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHBCOMPCODE: TFIBStringField
      FieldName = 'COMPCODE'
      EmptyStrToNull = True
    end
    object taUIDWHBW: TFIBFloatField
      FieldName = 'W'
    end
    object taUIDWHBART2: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'ART2'
      EmptyStrToNull = True
      Calculated = True
    end
    object taUIDWHBPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taUIDWHBQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taUIDWHBSPRICE: TFIBFloatField
      FieldName = 'SPRICE'
    end
    object taUIDWHBHEADERCOMPANY: TFIBStringField
      FieldName = 'HEADER$COMPANY'
      Size = 60
      EmptyStrToNull = True
    end
    object taUIDWHBBODYDEPARTMENT: TFIBStringField
      FieldName = 'BODY$DEPARTMENT'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHBBODYSIZE: TFIBStringField
      FieldName = 'BODY$SIZE'
      Size = 10
      EmptyStrToNull = True
    end
    object taUIDWHBBODYGOOD: TFIBStringField
      FieldName = 'BODY$GOOD'
      EmptyStrToNull = True
    end
    object taUIDWHBEUID: TFIBStringField
      FieldName = 'EUID'
      Size = 32
      EmptyStrToNull = True
    end
    object taUIDWHBEART2: TFIBStringField
      FieldName = 'EART2'
      EmptyStrToNull = True
    end
  end
  object frdsUIDWHB: TfrDBDataSet
    DataSet = taUIDWHB
    Left = 559
    Top = 536
  end
  object frAnlzSelEmp: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from seller where userid=:userid')
    BeforeOpen = frAnlzSelEmpBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 639
    Top = 110
    oFetchAll = True
    object frAnlzSelEmpID: TFIBIntegerField
      FieldName = 'ID'
    end
    object frAnlzSelEmpUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object frAnlzSelEmpSELLERNAME: TFIBStringField
      FieldName = 'SELLERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object frAnlzSelEmpCOUNTSELL: TFIBIntegerField
      FieldName = 'COUNTSELL'
    end
    object frAnlzSelEmpCOSTSELL: TFIBFloatField
      FieldName = 'COSTSELL'
    end
  end
  object dsAnlzSelEmp: TDataSource
    DataSet = frAnlzSelEmp
    Left = 639
    Top = 64
  end
  object frdsAnlzSelEmp: TfrDBDataSet
    DataSet = frAnlzSelEmp
    Left = 641
    Top = 160
  end
  object frdsFrom: TfrDBDataSet
    DataSet = ibdsFrom
    Left = 440
    Top = 8
  end
  object dsrFrom: TDataSource
    DataSet = ibdsFrom
    Left = 440
    Top = 56
  end
  object ibdsBillItemVP: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from doc_Bill(:SINVID, :KIND)')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrFrom
    Left = 544
    Top = 584
    object ibdsBillItemVPR_UID: TFIBIntegerField
      FieldName = 'R_UID'
    end
    object ibdsBillItemVPR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_ITCOST: TFIBFloatField
      FieldName = 'R_ITCOST'
    end
    object ibdsBillItemVPR_ITCOST2: TFIBFloatField
      FieldName = 'R_ITCOST2'
    end
    object ibdsBillItemVPR_ITW: TFIBFloatField
      FieldName = 'R_ITW'
    end
    object ibdsBillItemVPR_SELID: TFIBIntegerField
      FieldName = 'R_SELID'
    end
    object ibdsBillItemVPR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_ART2ID: TFIBIntegerField
      FieldName = 'R_ART2ID'
    end
    object ibdsBillItemVPR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_CUSTDECL: TFIBStringField
      FieldName = 'R_CUSTDECL'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_UNITID: TFIBIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsBillItemVPR_Q: TFIBIntegerField
      FieldName = 'R_Q'
    end
    object ibdsBillItemVPR_W: TFIBFloatField
      FieldName = 'R_W'
    end
    object ibdsBillItemVPR_UID_SUP: TFIBIntegerField
      FieldName = 'R_UID_SUP'
    end
    object ibdsBillItemVPR_PRICEWONDS: TFIBFloatField
      FieldName = 'R_PRICEWONDS'
    end
    object ibdsBillItemVPR_COSTWONDS: TFIBFloatField
      FieldName = 'R_COSTWONDS'
    end
    object ibdsBillItemVPR_EXCISE: TFIBStringField
      FieldName = 'R_EXCISE'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_PNDS: TFIBFloatField
      FieldName = 'R_PNDS'
    end
    object ibdsBillItemVPR_TOTALNDS: TFIBFloatField
      FieldName = 'R_TOTALNDS'
    end
    object ibdsBillItemVPR_PRICE: TFIBFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsBillItemVPR_COST: TFIBFloatField
      FieldName = 'R_COST'
    end
    object ibdsBillItemVPR_TR: TFIBFloatField
      FieldName = 'R_TR'
    end
    object ibdsBillItemVPR_PRICE2: TFIBFloatField
      FieldName = 'R_PRICE2'
    end
    object ibdsBillItemVPR_COST2: TFIBFloatField
      FieldName = 'R_COST2'
    end
    object ibdsBillItemVPR_PRICE2WONDS: TFIBFloatField
      FieldName = 'R_PRICE2WONDS'
    end
    object ibdsBillItemVPR_COST2WONDS: TFIBFloatField
      FieldName = 'R_COST2WONDS'
    end
    object ibdsBillItemVPR_TOTAL2NDS: TFIBFloatField
      FieldName = 'R_TOTAL2NDS'
    end
    object ibdsBillItemVPR_NDATE: TFIBDateTimeField
      FieldName = 'R_NDATE'
    end
    object ibdsBillItemVPR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      EmptyStrToNull = True
    end
    object ibdsBillItemVPR_RATEURF: TFIBFloatField
      FieldName = 'R_RATEURF'
    end
    object ibdsBillItemVPR_AKCIZ: TFIBFloatField
      FieldName = 'R_AKCIZ'
    end
    object ibdsBillItemVPR_TRNDS: TFIBFloatField
      FieldName = 'R_TRNDS'
    end
    object ibdsBillItemVPR_RETNAME: TFIBStringField
      FieldName = 'R_RETNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrBillItemVP: TDataSource
    DataSet = ibdsBillItemVP
    Left = 544
    Top = 632
  end
  object frdsBillItemVP: TfrDBDataSet
    DataSource = dsrBillItemVP
    Left = 608
    Top = 632
  end
  object ibdsBillUItemVP: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from doc_Bill(:SINVID, :KIND)')
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrFrom
    Left = 544
    Top = 712
    object ibdsBillUItemVPR_UID: TFIBIntegerField
      FieldName = 'R_UID'
    end
    object ibdsBillUItemVPR_SZ: TFIBStringField
      FieldName = 'R_SZ'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_ITCOST: TFIBFloatField
      FieldName = 'R_ITCOST'
    end
    object ibdsBillUItemVPR_ITCOST2: TFIBFloatField
      FieldName = 'R_ITCOST2'
    end
    object ibdsBillUItemVPR_ITW: TFIBFloatField
      FieldName = 'R_ITW'
    end
    object ibdsBillUItemVPR_SELID: TFIBIntegerField
      FieldName = 'R_SELID'
    end
    object ibdsBillUItemVPR_GOODID: TFIBStringField
      FieldName = 'R_GOODID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_GOOD: TFIBStringField
      FieldName = 'R_GOOD'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_ART: TFIBStringField
      FieldName = 'R_ART'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_ART2ID: TFIBIntegerField
      FieldName = 'R_ART2ID'
    end
    object ibdsBillUItemVPR_ART2: TFIBStringField
      FieldName = 'R_ART2'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_COUNTRY: TFIBStringField
      FieldName = 'R_COUNTRY'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_CUSTDECL: TFIBStringField
      FieldName = 'R_CUSTDECL'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_MATID: TFIBStringField
      FieldName = 'R_MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_MAT: TFIBStringField
      FieldName = 'R_MAT'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_UNITID: TFIBIntegerField
      FieldName = 'R_UNITID'
    end
    object ibdsBillUItemVPR_Q: TFIBIntegerField
      FieldName = 'R_Q'
    end
    object ibdsBillUItemVPR_W: TFIBFloatField
      FieldName = 'R_W'
    end
    object ibdsBillUItemVPR_UID_SUP: TFIBIntegerField
      FieldName = 'R_UID_SUP'
    end
    object ibdsBillUItemVPR_PRICEWONDS: TFIBFloatField
      FieldName = 'R_PRICEWONDS'
    end
    object ibdsBillUItemVPR_COSTWONDS: TFIBFloatField
      FieldName = 'R_COSTWONDS'
    end
    object ibdsBillUItemVPR_EXCISE: TFIBStringField
      FieldName = 'R_EXCISE'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_PNDS: TFIBFloatField
      FieldName = 'R_PNDS'
    end
    object ibdsBillUItemVPR_TOTALNDS: TFIBFloatField
      FieldName = 'R_TOTALNDS'
    end
    object ibdsBillUItemVPR_PRICE: TFIBFloatField
      FieldName = 'R_PRICE'
    end
    object ibdsBillUItemVPR_COST: TFIBFloatField
      FieldName = 'R_COST'
    end
    object ibdsBillUItemVPR_TR: TFIBFloatField
      FieldName = 'R_TR'
    end
    object ibdsBillUItemVPR_PRICE2: TFIBFloatField
      FieldName = 'R_PRICE2'
    end
    object ibdsBillUItemVPR_COST2: TFIBFloatField
      FieldName = 'R_COST2'
    end
    object ibdsBillUItemVPR_PRICE2WONDS: TFIBFloatField
      FieldName = 'R_PRICE2WONDS'
    end
    object ibdsBillUItemVPR_COST2WONDS: TFIBFloatField
      FieldName = 'R_COST2WONDS'
    end
    object ibdsBillUItemVPR_TOTAL2NDS: TFIBFloatField
      FieldName = 'R_TOTAL2NDS'
    end
    object ibdsBillUItemVPR_NDATE: TFIBDateTimeField
      FieldName = 'R_NDATE'
    end
    object ibdsBillUItemVPR_SSF: TFIBStringField
      FieldName = 'R_SSF'
      EmptyStrToNull = True
    end
    object ibdsBillUItemVPR_RATEURF: TFIBFloatField
      FieldName = 'R_RATEURF'
    end
    object ibdsBillUItemVPR_AKCIZ: TFIBFloatField
      FieldName = 'R_AKCIZ'
    end
    object ibdsBillUItemVPR_TRNDS: TFIBFloatField
      FieldName = 'R_TRNDS'
    end
    object ibdsBillUItemVPR_RETNAME: TFIBStringField
      FieldName = 'R_RETNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrBillUItemVP: TDataSource
    DataSet = ibdsBillUItemVP
    Left = 616
    Top = 712
  end
  object frdsBillUItemVP: TfrDBDataSet
    DataSet = ibdsBillUItemVP
    Left = 680
    Top = 712
  end
  object frdsGrInsVp: TfrDBDataSet
    DataSet = ibdsGrInsVP
    Left = 136
    Top = 232
  end
  object ibdsGrInsVP: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from InsByArt2(:R_Art2Id)')
    AfterOpen = ibdsGrInsAfterOpen
    Transaction = trReport
    Database = dmCom.db
    DataSource = dsrBillItemVP
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 136
    Top = 176
  end
  object frdsSales: TfrDBDataSet
    DataSet = ibdsSales
    Left = 168
    Top = 712
  end
  object ibdsSales: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select fullart fullart,price price,sum(fcount) fcount, sum(scoun' +
        't) scount, dcount dcount, disc disc, sum(rcount) rcount, sum(lco' +
        'unt) lcount, selldate from'
      'sales_analisys(:bd, :ed, :userid, :compid, :supid, :depid)'
      'group by fullart, price, DCOUNT, disc, selldate'
      'order by fullart')
    BeforeOpen = ibdsSalesBeforeOpen
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 32
    Top = 704
    object ibdsSalesFULLART: TFIBStringField
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object ibdsSalesPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object ibdsSalesFCOUNT: TFIBBCDField
      FieldName = 'FCOUNT'
      Size = 0
      RoundByScale = True
    end
    object ibdsSalesSCOUNT: TFIBBCDField
      FieldName = 'SCOUNT'
      Size = 0
      RoundByScale = True
    end
    object ibdsSalesDCOUNT: TFIBIntegerField
      FieldName = 'DCOUNT'
    end
    object ibdsSalesDISC: TFIBIntegerField
      FieldName = 'DISC'
    end
    object ibdsSalesRCOUNT: TFIBBCDField
      FieldName = 'RCOUNT'
      Size = 0
      RoundByScale = True
    end
    object ibdsSalesLCOUNT: TFIBBCDField
      FieldName = 'LCOUNT'
      Size = 0
      RoundByScale = True
    end
    object ibdsSalesSELLDATE: TFIBDateTimeField
      FieldName = 'SELLDATE'
    end
  end
  object dsrSales: TDataSource
    DataSet = ibdsSales
    Left = 96
    Top = 704
  end
end
