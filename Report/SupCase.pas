unit SupCase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, RXDBCtrl, StdCtrls, Mask, ExtCtrls, Buttons, Regexpr,
  ComCtrls;
const
  NM_PROGRESSBAR = $60f1;

type
  TfmSupCase = class(TForm)
    paHeader: TPanel;
    Label6: TLabel;
    plBn: TPanel;
    btnClose: TBitBtn;
    redImp: TRichEdit;
    BitBtn1: TBitBtn;
    lbSup: TListBox;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure ProgressBar(var Message:TMessage); message NM_PROGRESSBAR;
  end;

var
  fmSupCase: TfmSupCase;
  OrgCode : char;

implementation

uses comdata, Data, M207Proc,dbTree;

{$R *.DFM}

procedure TfmSupCase.btnCloseClick(Sender: TObject);
begin
  dm.SupCaseId:=TNodeData(lbSup.Items.Objects[lbSup.ItemIndex]).Code;
end;

procedure TfmSupCase.FormCreate(Sender: TObject);
begin
 lbSup.Items.Assign(dm.slSup);
end;

procedure TfmSupCase.ProgressBar(var Message: TMessage);
begin

end;


end.

