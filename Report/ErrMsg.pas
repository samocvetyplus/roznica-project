unit ErrMsg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TfmErrMsg = class(TForm)
    lstbxError: TListBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

procedure ShowErrorMsg(List : TStringList);

implementation

{$R *.DFM}

var
  fmErrMsg : TfmErrMsg;

procedure ShowErrorMsg(List : TStringList);
begin
  if not Assigned(fmErrMsg) then
    fmErrMsg := TfmErrMsg.Create(Application);
  fmErrMsg.lstbxError.Items.Assign(List);
  fmErrMsg.Show;
end;

procedure TfmErrMsg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
