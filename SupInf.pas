unit SupInf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, M207Grid, M207IBGrid,
  ComCtrls, DBGridEh, PrnDbgeh, ActnList, jpeg, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmSupInf = class(TForm)
    StatusBar1: TStatusBar;
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    fmstrSupInf: TFormStorage;
    ibgrSupInf: TDBGridEh;
    pibgrSupInf: TPrintDBGridEh;
    acList: TActionList;
    acPrintGrid: TAction;
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure siExitClick(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    { Public declarations }
  end;

var
  fmSupInf: TfmSupInf;

implementation

uses comdata, ServData, M207Proc;

{$R *.DFM}

{ TfmSupInf }

procedure TfmSupInf.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;

procedure TfmSupInf.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmSupInf.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmServ.qrSupInf]);
  ibgrSupInf.DataSource.DataSet.Close;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining;
  Action := caFree;
end;

procedure TfmSupInf.FormCreate(Sender: TObject);
begin
  tb1.WallPaper:=wp;
  if dmCom.tr.Active then dmCom.tr.CommitRetaining; 
  if not dmCom.tr.Active then  dmCom.tr.StartTransaction;
  OpenDataSets([dmServ.qrSupInf]);
end;

procedure TfmSupInf.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case key of
    #27 : Close;
  end;
end;

procedure TfmSupInf.siExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmSupInf.acPrintGridExecute(Sender: TObject);
begin
 pibgrSupInf.Print;
end;

end.
