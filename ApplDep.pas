unit ApplDep;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGridEh, M207Ctrls,
  StdCtrls, Menus, TB2Item, ActnList, jpeg, DB, DBGridEhGrouping,
  rxPlacemnt, GridsEh, DBCtrls, rxSpeedbar, FIBDataSet, pFIBDataSet;

type
  TfmApplDep = class(TForm)
    tb1: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    siExit: TSpeedItem;
    siDel: TSpeedItem;
    siEdit: TSpeedItem;
    spitPrint: TSpeedItem;
    dg1: TDBGridEh;
    fr: TM207FormStorage;
    tb2: TSpeedBar;
    laDepFrom: TLabel;
    laDepTo: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    siDep: TSpeedItem;
    siDepFrom: TSpeedItem;
    siPeriod: TSpeedItem;
    pmDepfrom: TTBPopupMenu;
    pmdep: TTBPopupMenu;
    siClosed: TSpeedItem;
    aclist: TActionList;
    acClosed: TAction;
    acDel: TAction;
    siadd: TSpeedItem;
    pmadd: TTBPopupMenu;
    ibAddItem: TTBItem;
    ibAddArt: TTBItem;
    acAddItem: TAction;
    acPrint: TAction;
    siAddAppl: TSpeedItem;
    acAddApplUser: TAction;
    acAddApplItem: TAction;
    pmAddAppl: TTBPopupMenu;
    biArt: TTBItem;
    biItem: TTBItem;
    acAddappl: TAction;
    siSend: TSpeedItem;
    acSend: TAction;
    pmDepEdit: TTBPopupMenu;
    chAll: TCheckBox;
    pmMerge: TTBPopupMenu;
    biUserMerge: TTBItem;
    biMerge: TTBItem;
    siRepeatUid: TSpeedItem;
    acRepeatUid: TAction;
    siHelp: TSpeedItem;
    pmCloseAppl: TTBPopupMenu;
    biCloseAppl: TTBItem;
    biCloseAllAppl: TTBItem;
    acCloseAppl: TAction;
    acCloseAllAppl: TAction;
    acOpen: TAction;
    acClear: TAction;
    acSendAppl: TAction;
    pmsend: TTBPopupMenu;
    biSend: TTBItem;
    biClear: TTBItem;
    siCreateInv: TSpeedItem;
    acCreateInv: TAction;
    pmPrint: TTBPopupMenu;
    acPrintGood: TAction;
    acPrintArt: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acClearOneAppl: TAction;
    TBItem3: TTBItem;
    siDelAll: TSpeedItem;
    SpeedItem1: TSpeedItem;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure siExitClick(Sender: TObject);
    procedure dg1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure siEditClick(Sender: TObject);
    procedure siPeriodClick(Sender: TObject);
    procedure acClosedUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acAddItemExecute(Sender: TObject);
    procedure acAddApplUserExecute(Sender: TObject);
    procedure acAddApplItemExecute(Sender: TObject);
    procedure acAddApplUserUpdate(Sender: TObject);
    procedure acSendExecute(Sender: TObject);
    procedure chAllClick(Sender: TObject);
    procedure acAddapplExecute(Sender: TObject);
    procedure acRepeatUidUpdate(Sender: TObject);
    procedure acRepeatUidExecute(Sender: TObject);
    procedure acSendUpdate(Sender: TObject);
    procedure siHelpClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acCloseApplExecute(Sender: TObject);
    procedure acCloseAllApplExecute(Sender: TObject);
    procedure acCloseApplUpdate(Sender: TObject);
    procedure acOpenUpdate(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure acSendApplExecute(Sender: TObject);
    procedure acCreateInvUpdate(Sender: TObject);
    procedure acCreateInvExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintGoodExecute(Sender: TObject);
    procedure acPrintArtExecute(Sender: TObject);
    procedure acClearOneApplExecute(Sender: TObject);
    procedure dg1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure siDelAllClick(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure biCloseApplClick(Sender: TObject);
    procedure siClosedClick(Sender: TObject);
  private
   LogOprIdForm:string;
    { Private declarations }
   procedure ShowPeriod;
   procedure pmDepClick (Sender: TObject);
   procedure pmDepFromClick (Sender: TObject);
   procedure pmDepEditClick (Sender: TObject);
   procedure dsApplDepListDataChange(Sender: TObject; Field: TField);   
   function create_fmDepSel : boolean;
  public
    { Public declarations }
  end;

var
  fmApplDep: TfmApplDep;

implementation

uses data3, M207Proc, comdata, ApplDepItem, ReportData, data, Period, dbUtil,
     JewConst, Data2, InsRepl, DepSel, pFIBQuery, FIBQuery, RepeatUidAppl,
     MsgDialog;

{$R *.dfm}

procedure TfmApplDep.pmDepClick (Sender: TObject);
begin
 if TTBItem(Sender).Tag =0 then
 begin
  dm3.AppldepDepID1:=-MaxInt;
  dm3.ApplDepDepID2:=MaxInt;
 end
 else
 begin
  dm3.AppldepDepID1:=TTBItem(Sender).Tag;
  dm3.AppldepDepID2:=TTBItem(Sender).Tag;
 end;
 laDepTo.Caption:=TTBItem(Sender).Caption;
 ReOpenDataSets([dm3.quApplDepList]);
end;

procedure TfmApplDep.pmDepFromClick (Sender: TObject);
begin
 if TTBItem(Sender).Tag =0 then
 begin
  dm3.ApplDepDepfromid1:=-MaxInt;
  dm3.ApplDepDepFromID2:=MaxInt;
 end
 else
 begin
  dm3.ApplDepDepfromid1:=TTBItem(Sender).Tag;
  dm3.ApplDepDepFromID2:=TTBItem(Sender).Tag;
 end;
 laDepFrom.Caption:=TTBItem(Sender).Caption;
 ReOpenDataSets([dm3.quApplDepList]);
end;

procedure TfmApplDep.pmDepEditClick (Sender: TObject);
begin
 if dm3.quApplDepListEDITTR.AsInteger=1 then
  raise Exception.Create('������ ���� �������� � �������������� �� ��������');
 if TTBItem(Sender).Tag = dm3.quApplDepListDEPFROMID.AsInteger then
  raise Exception.Create('������ ���� ������������� � ���������� �������');
 if TTBItem(Sender).Tag = dm3.quApplDepListDEPID.AsInteger then
  raise Exception.Create('�������, � �������� ����������� ������, � �� �������, �� ����� ���� �����������');
 dm3.quApplDepList.edit;
 dm3.quApplDepListDEPFROMID.AsInteger:=TTBItem(Sender).Tag;
 dm3.quApplDepList.Post;
 dm3.quApplDepList.Refresh;
end;

procedure TfmApplDep.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(dm3.AppldepBD) + ' �� ' +DateToStr(dm3.ApplDepEd);
end;

procedure TfmApplDep.FormCreate(Sender: TObject);
var  pm, pm1, pm2:TTBItem;
begin
 tb1.WallPaper:=wp;
 tb2.WallPaper:=wp;
 dm3.AppldepDepID1:=-MaxInt;
 dm3.ApplDepDepID2:=MaxInt;
 dm3.ApplDepDepfromid1:=-MaxInt;
 dm3.ApplDepDepFromID2:=MaxInt;
 dm3.AppldepBD:=dmcom.FirstMonthDate;
 dm3.ApplDepEd:=strtodatetime(datetostr(dmCom.GetServerTime))+0.9999;
 dm3.ApplDepAllShow:=false;
 ReOpenDataSets([dm3.quApplDepList]);
 ShowPeriod;
 laDepFrom.Caption:='��� ������';
 laDepTo.Caption:='��� ������';

 pm:=TTBItem.Create(pmdep);
 pm.Caption:='��� ������';
 pm.Tag:=0;
 pm.OnClick:=pmDepClick;

 pm1:=TTBItem.Create(pmDepfrom);
 pm1.Caption:='��� ������';
 pm1.Tag:=0;
 pm1.OnClick:=pmDepFromClick;

 pmdep.Items.Add(pm);
 pmDepfrom.Items.Add(pm1);

 {��������� �������}
 dm.quTmp.Close;
 dm.quTmp.SQL.Text:='select Sname, d_depid from d_dep where d_depid<>-1000 and ISDELETE <> 1';
 dm.quTmp.ExecQuery;

 while not (dm.qutmp.Eof) do
 begin
  pm:=TTBItem.Create(pmdep);
  pm.Caption:=dm.quTmp.Fields[0].Asstring;
  pm.Tag:=dm.quTmp.Fields[1].AsInteger;
  pm.OnClick:=pmDepClick;
  pmdep.Items.Add(pm);

  pm1:=TTBItem.Create(pmDepfrom);
  pm1.Caption:=dm.quTmp.Fields[0].Asstring;
  pm1.Tag:=dm.quTmp.Fields[1].AsInteger;
  pm1.OnClick:=pmDepFromClick;
  pmDepfrom.Items.Add(pm1);

  if CenterDep then
  begin
   pm2:=TTBItem.Create(pmDepEdit);
   pm2.Caption:='�������� �����, �� ������� ����� ���������� ������� �� '+trim(dm.quTmp.Fields[0].Asstring);
   pm2.Tag:=dm.quTmp.Fields[1].AsInteger;
   pm2.OnClick:=pmDepEditClick;
   pmDepEdit.Items.Add(pm2);
  end;

  dm.quTmp.Next;
 end;

 dm.quTmp.Transaction.CommitRetaining;
 dm.quTmp.Close;

 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 LogOprIdForm := dm3.defineOperationId(dm3.LogUserID);
 if dm3.quApplDepListISCLOSED.IsNull then siClosed.Enabled:=false else siClosed.Enabled:=true;
end;

procedure TfmApplDep.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 CloseDataSets([dm3.quApplDepList]);
end;

procedure TfmApplDep.siExitClick(Sender: TObject);
begin
 close;
end;

procedure TfmApplDep.dg1GetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
 if dm3.quApplDepListEDITTR.AsInteger=1 then Background:=clMedGray
 else if dm3.quApplDepListISCLOSED.AsInteger=1 then Background:=clBtnFace;
 if Column.FieldName='SNAME' then Background:=dm3.quApplDepListCOLOR.AsInteger;
 if Column.FieldName='SNAMEFROM' then Background:=dm3.quApplDepListCOLORDEPFROM.AsInteger;
 if Column.FieldName='COMMENT' then Background:=clYellow;
end;

procedure TfmApplDep.FormResize(Sender: TObject);
begin
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmApplDep.siEditClick(Sender: TObject);
var
  LogOperationID: string;
  BookMark : TBookMark;
begin
  BookMark:=dm3.quApplDepList.GetBookmark;
  if dm3.quApplDepListSINVID.IsNull then exit;
  try
    LogOperationID := dm3.insert_operation('������ �� ������� �� ��������', LogOprIdForm);
    ShowAndFreeForm(TfmApplDepItem, Self, TForm(fmApplDepItem), True, False);
    dm3.quApplDepList.Refresh;
    dg1.SumList.RecalcAll;
    dm3.quApplDepList.GotoBookmark(BookMark);
  finally
    dm3.quApplDepList.FreeBookmark(BookMark);
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmApplDep.siPeriodClick(Sender: TObject);
begin
 if GetPeriod(dm3.AppldepBD, dm3.Appldeped) then
 begin
  ReOpenDataSets([dm3.quApplDepList]);
  ShowPeriod;
 end;
end;

procedure TfmApplDep.acClosedUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quApplDepListSINVID.IsNull)
end;

procedure TfmApplDep.acDelExecute(Sender: TObject);
var
  LogOperationID: string;
begin
 LogOperationID := dm3.insert_operation('������� ������ �� �������', LogOprIdForm);
 if dm3.quApplDepListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!');
 dm3.quApplDepList.Delete;
 dm3.update_operation(LogOperationID);
end;

procedure TfmApplDep.acDelUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quApplDepListSINVID.IsNull)
  and (dm3.quApplDepListMODEID.AsInteger=SelfDepId) and
  (dm3.quApplDepListEDITTR.AsInteger=0)
end;

procedure TfmApplDep.acAddItemExecute(Sender: TObject);
begin
 if dm3.ApplDepDepID2 = MaxInt then MessageDialog('������� �����, �� ������� �������� ������', mtError, [mbOk], 0)
 else if dm3.ApplDepDepFromID2=MaxInt then MessageDialog('������� �����, � �������� �������� ������', mtError, [mbOk], 0)
 else begin
  if Centerdep then
  begin
   dm3.quApplDepList.Append;
   dm3.quApplDepListITYPE.AsInteger:=17;
   dm3.quApplDepList.Post;
   ShowAndFreeForm(TfmApplDepItem, Self, TForm(fmApplDepItem), True, False);
   dm3.quApplDepList.Refresh;
   dg1.SumList.RecalcAll;
  end
  else begin
   if dm3.ApplDepDepFromID2<>SelfDepId then MessageDialog('������ ����� ������������ ������ �� ������� ������', mtWarning, [mbOk], 0)
   else begin
    dm3.quApplDepList.Append;
    dm3.quApplDepListITYPE.AsInteger:=17;
    dm3.quApplDepList.Post;
    ShowAndFreeForm(TfmApplDepItem, Self, TForm(fmApplDepItem), True, False);
    dm3.quApplDepList.Refresh;
    dg1.SumList.RecalcAll;    
   end
  end
 end;
end;

procedure TfmApplDep.acAddApplUserExecute(Sender: TObject);
begin
 if MessageDialog('���������� ������ � ������ ������������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
 try
  Screen.Cursor := crSQLWait;
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select fres from Merger_Appldep ( 1, '+inttostr(dmcom.UserId)+
   ', '#39+DateTimeToStr(dm3.AppldepBD)+#39', '#39+DateTimeToStr(dm3.AppldepED)+#39', '+
   inttostr(dm3.ApplDepDepID2)+', '+inttostr(dm3.ApplDepDepFromID2)+')';
  dm.quTmp.ExecQuery;
  if dm.quTmp.Fields[0].AsInteger=1 then MessageDialog('��� ������ ��� �����������!', mtError, [mbOk], 0);
  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
  ReOpenDataSet(dm3.quApplDepList);
 finally
  Screen.Cursor := crDefault;
 end;
end;

procedure TfmApplDep.acAddApplItemExecute(Sender: TObject);
begin
 if MessageDialog('���������� ������ �� ��������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
 try
  Screen.Cursor := crSQLWait;
  dm.quTmp.Close;
  dm.quTmp.SQL.Text:='select fres from Merger_Appldep ( 0, '+inttostr(dmcom.UserId)+
   ', '#39+DateTimeToStr(dm3.AppldepBD)+#39', '#39+DateTimeToStr(dm3.AppldepED)+#39', '+
   inttostr(dm3.ApplDepDepID2)+', '+inttostr(dm3.ApplDepDepFromID2)+')';
  dm.quTmp.ExecQuery;
  if dm.quTmp.Fields[0].AsInteger=1 then MessageDialog('��� ������ ��� �����������!', mtError, [mbOk], 0);
  dm.quTmp.Transaction.CommitRetaining;
  dm.quTmp.Close;
  ReOpenDataSet(dm3.quApplDepList);
 finally
  Screen.Cursor := crDefault;
 end;
end;

procedure TfmApplDep.acAddApplUserUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled := (dm3.ApplDepDepID2<>MaxInt) and
   (dm3.ApplDepDepFromID2<>MaxInt) {and CenterDep}
end;

function TfmApplDep.create_fmDepSel : boolean;
begin
 if CenterDep then
 begin
  fmDepSel:=TfmDepSel.Create(NIL);
  try
   with fmDepSel do
    if ShowModal=mrOK  then result:=true
    else result:=false;
  finally
   fmDepSel.Free;
  end;
 end
  else result:=false;
end;

procedure TfmApplDep.acSendExecute(Sender: TObject);
var NeedSPR, NeedSPR1, j, PacketId:integer;
    CodeDep, CodedepC:string;
    flDescription:boolean;
    LogOperationID: string;
    s, s1:string;
    NeedSPR_mas:t_array_int;
    arraydep:array of string[10];
    centerdepid, i :integer;

procedure filldeparray;
var i, val:integer;
begin
 with dm, qutmp do
 begin
  close;
  sql.Text:='select d_depid, r_code from d_dep where d_depid<>-1000 and isdelete<>1 and centerdep<>1 and R_UseDep=1 order by d_depid';
  ExecQuery;
  s:=' in (';
  s1:=' in (';
  SetLength(NeedSPR_mas,0);
  SetLength(arraydep,0);
  while not eof do
  begin
   val:=Fields[0].AsInteger;
   i:=0;
   if length(dmcom.mas_dep)>0 then
    while (dmcom.mas_dep[i]<>val) and (i<length(dmcom.mas_dep)) do inc(i);
   if i>=length(dmcom.mas_dep) then
   begin
    if Fields[0].AsInteger<>centerdepid then
    begin
     s:=s+inttostr(val)+', ';
     qutmp1.Close;
     qutmp1.SQL.Text:='select r_packetid from r_packet where PacketState=0 and r_item='#39
                       +'DESCRIPTION'+#39' and sourceid='#39+CodeDep+#39' and DESTID='#39+Fields[1].AsString+#39;
     qutmp1.ExecQuery;
     NeedSPR1:=0;
     if qutmp1.Fields[0].IsNull then
     begin
      InsRPacket(dmCom.db, 'DESCRIPTION', Fields[1].AsString, 0, '', '������ ��������', 1,NeedSPR1, PacketId);
      SetLength(NeedSPR_mas,length(NeedSPR_mas)+1);
      NeedSPR_mas[length(NeedSPR_mas)-1]:=NeedSPR1;
      SetLength(arraydep,length(arraydep)+1);
      arraydep[length(arraydep)-1]:=Fields[1].AsString;
     end;
     qutmp1.Transaction.CommitRetaining;
     qutmp1.Close;
    end;
    s1:=s1+inttostr(val)+', ';
   end;
   next;
  end;
  s:=s+'-999) ';
  s1:=s1+'-999) ';
  close;
  Transaction.CommitRetaining;
  dm2.quRApplDep.SelectSQL[7]:=' and s.DepID '+s;
 end
end;

begin
 dmcom.flcall_delsel:=2;  //0;
 setlength(dmcom.mas_dep,0);

 if create_fmDepSel then
 begin
  NeedSPR1:=0;
  NeedSPR:=0;
  with dm do
  begin
   qutmp.SQL.Text:='select r_code, d_depid from d_dep where WH=1';
   qutmp.ExecQuery;
   CodeDepC:=qutmp.Fields[0].AsString;
   centerdepid:=qutmp.Fields[1].AsInteger;
   qutmp.Transaction.CommitRetaining;
   qutmp.Close;
  end;

  filldeparray;

 //�������
  LogOperationID := dm3.insert_operation(sLog_ApplDepExport, LogOprIdForm);


  ExecSQL('delete from DELRECORDS where rstate>=3', dm.quTmp);
  i:=0;
  with dm do
  begin
   qutmp.close;
   qutmp.sql.Text:='select d_depid, r_code from d_dep where d_depid<>-1000 and isdelete<>1 and centerdep<>1 order by d_depid';
   quTmp.ExecQuery;
   while not quTmp.Eof do
   begin
    if length(dmcom.mas_dep)>0 then
    while (dmcom.mas_dep[i]<>qutmp.Fields[0].AsInteger) and (i<length(dmcom.mas_dep)) do inc(i);
    if i>=length(dmcom.mas_dep) then
    InsRPacket(dmCom.db, 'DELRECORDS', trim(qutmp.Fields[1].AsString), 4,
     trim(qutmp.Fields[1].AsString)+';2;', '��������� ������ �� ������', 1,NeedSPR, PacketId);
    qutmp.Next;
   end;
   qutmp.Close;
   quTmp.Transaction.CommitRetaining;
  end;

 {������ �� ��������}
   with dm2, quRApplDep do
   begin
     Prepare;
     Active:=True;
     while not EOF do
     begin
      NeedSPR:=0;
      InsRPacket(dmCom.db, 'APPLDEP', slNDep.Values[quRApplDepDEPID.AsString], 2,
                 quRApplDepSINVID.AsString, '������ �� ������ �'+quRApplDepSN.AsString, 1,NeedSPR, PacketId);
      with quSetApplDEpRState, Params do
      begin
        ByName['SINVID'].AsInteger:=quRApplDepSINVID.AsInteger;
        ByName['RSTATE'].AsInteger:=1;
        ExecQuery;
      end;
      Next;
     end;
    Active:=False;
   end;
 {****************}
  for j:=0 to length(arraydep)-1 do
  begin
   NeedSPR1:=NeedSPR_mas[j];
   InsRPacket(dmCom.db, 'DESCRIPTION', arraydep[j], 0, '', '������ ��������', 1,NeedSPR1, PacketId);
  end;
  dm3.update_operation(LogOperationID);
  MessageDialog('������������ ������ ��� �������� ������ �� ������� ���������!', mtInformation, [mbOk], 0);
  ReOpenDataSet(dm3.quApplDepList);
 end;
 if not CenterDep then
 begin
  NeedSPR1:=0;
  NeedSPR:=0;
  with dm do
  begin
   qutmp.SQL.Text:='select r_code, d_depid from d_dep where WH=1';
   qutmp.ExecQuery;
   CodeDepC:=qutmp.Fields[0].AsString;
   CenterDepid:=qutmp.Fields[1].AsInteger;
   qutmp.Transaction.CommitRetaining;
   qutmp.Close;
   qutmp.Close;
   qutmp.SQL.Text:='select r_packetid from r_packet where PacketState=0 and r_item='#39
                      +'DESCRIPTION'+#39' and DESTID='#39+CodeDepC+#39;
   qutmp.ExecQuery;
   if qutmp.Fields[0].IsNull then flDescription:=false else flDescription:=true;
   qutmp.Transaction.CommitRetaining;
   qutmp.Close;

  end;

  if not flDescription then
    InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC , 0, '', '������ ��������', 1,NeedSPR1, PacketId);
  {***********************}
  {������ �� �������}
  {����������}
   with dm2, quRApplDep do
   begin
    Active:=True;
    while not EOF do
    begin
     InsRPacket(dmCom.db, 'APPLDEP', slNDep.Values[IntToStr(CenterDepId)], 2,
                quRApplDepSINVID.AsString, '������ �� ������ �� �������� �'+quRApplDepSN.AsString, 1,NeedSPR, PacketId);
      with quSetApplDEpRState, Params do
      begin
        ByName['SINVID'].AsInteger:=quRApplDepSINVID.AsInteger;
        ByName['RSTATE'].AsInteger:=1;
        ExecQuery;
      end;
      Next;
     end;
     Active:=False;
    end;
  if not flDescription then
    InsRPacket(dmCom.db, 'DESCRIPTION', CodeDepC , 0, '', '������ ��������', 1,NeedSPR1, PacketId);

  MessageDialog('������������ ������ ��� �������� ������ �� ������� ���������!', mtInformation, [mbOk], 0);
  ReOpenDataSet(dm3.quApplDepList);
 end;

end;

procedure TfmApplDep.chAllClick(Sender: TObject);
begin
  dm3.ApplDepAllShow:=chAll.Checked;
  ReOpenDataSet(dm3.quApplDepList);
end;

procedure TfmApplDep.acAddapplExecute(Sender: TObject);
begin
//
end;

procedure TfmApplDep.acRepeatUidUpdate(Sender: TObject);
begin
 if not CenterDep then acRepeatUid.Enabled:=false
 else with dm, qutmp do
 begin
  close;
  sql.Text:='select count(*) from appldep_item group by uid having count(*)>1 ';
  ExecQuery;
  if Fields[0].IsNull then acRepeatUid.Enabled:=false else acRepeatUid.Enabled:=true;
  close;
 end
end;

procedure TfmApplDep.acRepeatUidExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmRepeatUidAppl, Self, TForm(fmRepeatUidAppl), True, False);
 dg1.SumList.RecalcAll;
end;

procedure TfmApplDep.acSendUpdate(Sender: TObject);
begin
 acSend.Enabled:=(not acRepeatUid.Enabled) and (not dm3.quApplDepListSINVID.IsNull);
end;

procedure TfmApplDep.siHelpClick(Sender: TObject);
begin
 Application.HelpContext(100291)
end;

procedure TfmApplDep.FormActivate(Sender: TObject);
begin
dm3.dsApplDepList.OnDataChange:=dsApplDepListDataChange;
 siExit.Left:=tb1.Width-tb1.BtnWidth-10;
 siHelp.Left:=tb1.Width-2*tb1.BtnWidth-10;
end;

procedure TfmApplDep.acCloseApplExecute(Sender: TObject);
begin
 ExecSQL('update sinv set isclosed=1, userid='+inttostr(dmCom.UserId)+' where sinvid='+
         dm3.quApplDepListSINVID.AsString, dm.quTmp);
 dm3.quApplDepList.Refresh;
end;

procedure TfmApplDep.acCloseAllApplExecute(Sender: TObject);

begin
 dm3.quApplDepList.First;
 while not dm3.quApplDepList.Eof do
 begin
  ExecSQL('update sinv set isclosed=1, userid='+inttostr(dmCom.UserId)+' where sinvid='+
  dm3.quApplDepListSINVID.AsString, dm.quTmp);
  dm3.quApplDepList.Next;
 end;
 ReOpenDataSet(dm3.quApplDepList);
end;

procedure TfmApplDep.acCloseApplUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quApplDepListSINVID.IsNull) and
  (dm3.quApplDepListISCLOSED.AsInteger=0);
end;

procedure TfmApplDep.acOpenUpdate(Sender: TObject);
begin
 TAction(Sender).Enabled:= (not dm3.quApplDepListSINVID.IsNull) and
  (dm3.quApplDepListISCLOSED.AsInteger=1);
end;

procedure TfmApplDep.acOpenExecute(Sender: TObject);
begin
  ExecSQL('update sinv set isclosed=0, userid=null where sinvid='+
          dm3.quApplDepListSINVID.AsString, dm.quTmp);
  dm3.quApplDepList.Refresh;
end;

procedure TfmApplDep.acClearExecute(Sender: TObject);
var CodedepC:string;
    s1:string;
    NeedSPR_mas:t_array_int;
    arraydep:array of string[10];
procedure filldeparray;
var i, val:integer;
begin
 with dm, qutmp do
 begin
  close;
  sql.Text:='select d_depid, r_code from d_dep where d_depid<>-1000 and isdelete<>1 order by d_depid';
  ExecQuery;
  s1:=' in (';
  SetLength(NeedSPR_mas,0);
  SetLength(arraydep,0);
  while not eof do
  begin
   val:=Fields[0].AsInteger;
   i:=0;
   if length(dmcom.mas_dep)>0 then
    while (dmcom.mas_dep[i]<>val) and (i<length(dmcom.mas_dep)) do inc(i);
   if i<length(dmcom.mas_dep) then   s1:=s1+inttostr(val)+', ';
   next;
  end;
  s1:=s1+'-999) ';
  close;
  Transaction.CommitRetaining;
 end
end;
begin
 if MessageDialog('����� ������� ��� �������� ������ �������������� ������ �� ��������. �������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
 dmcom.flcall_delsel:=1;
 setlength(dmcom.mas_dep,0);

 if create_fmDepSel then
 begin
  with dm do
  begin
   qutmp.SQL.Text:='select r_code, d_depid from d_dep where WH=1';
   qutmp.ExecQuery;
   CodeDepC:=qutmp.Fields[0].AsString;
   qutmp.Transaction.CommitRetaining;
   qutmp.Close;
  end;

  filldeparray;

  Screen.Cursor := crSQLWait;
  ExecSQL('update sinv set edittr=1 where itype = 17 and (edittr<>1 or edittr is null) '+
          'and ((rstate>=3 and depid in (select d_depid from d_dep where centerdep<>1)) '+
          'or (isclosed=1 and depid in (select d_depid from d_dep where centerdep=1))) '+
          ' and depid '+s1, dm.quTmp);
  ExecSQL('delete from appldep_item where (appldepid in (select ap.appldepid '+
          'from appldep ap, sinv sn  where ap.sinvid=sn.sinvid and '+
          'sn.edittr = 1)) and (depid '+s1+' )', dm.quTmp);
  ReOpenDataSet(dm3.quApplDepList);
  Screen.Cursor := crDefault;
  MessageDialog('������� ������������ ������ ���������!', mtInformation, [mbOk], 0);  
 end;
 if not CenterDep then
 begin
  Screen.Cursor := crSQLWait;
  ExecSQL('update sinv set edittr=1 where itype = 17 and (edittr<>1 or edittr is null) '+
          'and rstate>=3 and depid<>'+inttostr(SelfDepId), dm.quTmp);
  ExecSQL('delete from appldep_item where (appldepid in (select ap.appldepid '+
          'from appldep ap, sinv sn  where ap.sinvid=sn.sinvid and '+
          'sn.edittr = 1)) ', dm.quTmp);
  ReOpenDataSet(dm3.quApplDepList);
  Screen.Cursor := crDefault;
  MessageDialog('������� ������������ ������ ���������!', mtInformation, [mbOk], 0);  
 end;
end;

procedure TfmApplDep.acSendApplExecute(Sender: TObject);
begin
//
end;

procedure TfmApplDep.acCreateInvUpdate(Sender: TObject);
begin
 Taction(Sender).Enabled:= (not dm3.quApplDepListSINVID.IsNull) and
  (dm3.quApplDepListDEPID.AsInteger= SelfDepId) and (dm3.quApplDepListISCLOSED.AsInteger=1) 
end;

procedure TfmApplDep.acCreateInvExecute(Sender: TObject);
begin
 with dm, qutmp do
 begin
  close;
  sql.Text:= 'select sn from CREATEDINV_FROM_APPLDEP ('+inttostr(dmcom.UserId)+', '+
  dm3.quApplDepListSINVID.AsString+')';
  ExecQuery;
  if (fields[0].AsInteger=-1) or fields[0].IsNull then MessageDialog('��������� �� ����� ���� �������', mtError, [mbOk], 0)
  else MessageDialog('������� ��������� �� �'+trim(fields[0].AsString), mtInformation, [mbOk], 0);
  close;
  Transaction.CommitRetaining;
 end;
end;

procedure TfmApplDep.acPrintExecute(Sender: TObject);
begin
//
end;

procedure TfmApplDep.acPrintGoodExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr := nil;
  if dm3.quApplDepListSINVID.IsNull then exit;
  arr := gen_arr(dg1, dm3.quApplDepListSINVID);
  PrintDocument(arr,appl_dep)
end;

procedure TfmApplDep.acPrintArtExecute(Sender: TObject);
var
  arr : TarrDoc;
begin
  arr := nil;
  if dm3.quApplDepListSINVID.IsNull then exit;
  arr := gen_arr(dg1, dm3.quApplDepListSINVID);
  PrintDocument(arr,appl_dep_art)
end;

procedure TfmApplDep.acClearOneApplExecute(Sender: TObject);
begin
 Screen.Cursor := crSQLWait;
 ExecSQL('update sinv set edittr=1, isclosed=1 where itype = 17 and sinvid='
  +dm3.quApplDepListSINVID.AsString, dm.quTmp);
 ExecSQL('delete from appldep_item where (appldepid in (select ap.appldepid '+
         'from appldep ap  where ap.sinvid='+dm3.quApplDepListSINVID.AsString+
         ')) ', dm.quTmp);
 ReOpenDataSet(dm3.quApplDepList);
 Screen.Cursor := crDefault;
 MessageDialog('������� ������ ���������!', mtInformation, [mbOk], 0); 
end;

procedure TfmApplDep.dg1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 case key of
  VK_RETURN: siEditClick(nil);
 end;
end;

procedure TfmApplDep.siDelAllClick(Sender: TObject);
var
  LogOperationID: string;
begin
 dm3.quApplDepList.BeforeDelete := nil;

 dm3.quApplDepList.First;
 while not dm3.quApplDepList.Eof do
 begin
   if dm3.quApplDepListISCLOSED.AsInteger=1 then acOpen.Execute;
   dm3.quApplDepList.Next;
 end;

 dm3.quApplDepList.First;
 while dm3.quApplDepList.RecordCount <> 0 do
 begin
   LogOperationID := dm3.insert_operation('������� ������ �� �������', LogOprIdForm);
   if dm3.quApplDepListISCLOSED.AsInteger=1 then raise Exception.Create('��������� �������!!');
   dm3.quApplDepList.Delete;
   dm3.update_operation(LogOperationID);
 end;

 dm3.quApplDepList.First;
end;

procedure TfmApplDep.SpeedItem1Click(Sender: TObject);
begin
with dm3, dmCom do
  begin
  PostDataSet(quApplDepList);
  tr.CommitRetaining;
  ReOpenDataSet(quApplDepList);
  quApplDepList.Refresh;
  end;
end;

procedure TfmApplDep.biCloseApplClick(Sender: TObject);
begin
 acCloseAppl.Execute;
end;

procedure TfmApplDep.dsApplDepListDataChange(Sender: TObject; Field: TField);
begin
  if dm3.quApplDepListISCLOSED.IsNull then siClosed.Enabled:=false else siClosed.Enabled:=true;
   if dm3.quApplDepListISCLOSED.AsInteger=0 then
  begin
   dm.SetCloseInvBtn(siClosed, 0);
   siClosed.DropDownMenu:=pmCloseAppl;
  end
 else
 begin
 dm.SetCloseInvBtn(siClosed, 1);
 siClosed.DropDownMenu:=nil;
 end;
end;

procedure TfmApplDep.siClosedClick(Sender: TObject);
begin
if dm3.quApplDepListISCLOSED.AsInteger=1 then acOpen.Execute;
end;

end.
