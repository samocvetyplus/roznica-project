object fmhSell: TfmhSell
  Left = 55
  Top = 114
  BorderStyle = bsNone
  Caption = #1053#1072#1078#1084#1080#1090#1077' '#1083#1102#1073#1091#1102' '#1082#1083#1072#1074#1080#1096#1091' '#1095#1090#1086#1073#1099' '#1079#1072#1082#1088#1099#1090#1100' '#1101#1090#1091' '#1092#1086#1088#1084#1091
  ClientHeight = 574
  ClientWidth = 880
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 298
    Height = 32
    Caption = #1044#1054#1041#1040#1042#1048#1058#1068' '#1048#1047#1044#1045#1051#1048#1045':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 80
    Width = 282
    Height = 32
    Caption = #1042#1045#1056#1053#1059#1058#1068' '#1048#1047#1044#1045#1051#1048#1045':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 320
    Top = 16
    Width = 71
    Height = 32
    Caption = 'Insert'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 320
    Top = 80
    Width = 79
    Height = 32
    Caption = 'Home'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 8
    Top = 136
    Width = 278
    Height = 32
    Caption = #1059#1044#1040#1051#1048#1058#1068' '#1048#1047#1044#1045#1051#1048#1045':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 320
    Top = 132
    Width = 158
    Height = 32
    Caption = 'Ctrl + Delete'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 8
    Top = 196
    Width = 144
    Height = 32
    Caption = #1055#1056#1054#1044#1040#1046#1040':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 320
    Top = 196
    Width = 107
    Height = 32
    Caption = #1055#1088#1086#1073#1077#1083
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label9: TLabel
    Left = 8
    Top = 264
    Width = 242
    Height = 32
    Caption = #1054#1058#1050#1056#1067#1058#1068' '#1057#1052#1045#1053#1059':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 4
    Top = 316
    Width = 240
    Height = 32
    Caption = #1047#1040#1050#1056#1067#1058#1068' '#1057#1052#1045#1053#1059':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 320
    Top = 288
    Width = 93
    Height = 32
    Caption = 'Ctrl + O'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 492
    Top = 436
    Width = 212
    Height = 64
    Caption = #1055#1077#1095#1072#1090#1090#1100#13#10#1090#1086#1074#1072#1088#1085#1086#1075#1086' '#1095#1077#1082#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 768
    Top = 448
    Width = 33
    Height = 32
    Caption = 'F5'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label14: TLabel
    Left = 496
    Top = 520
    Width = 248
    Height = 32
    Caption = #1055#1077#1095#1072#1090#1100' '#1079#1072#1103#1074#1083#1077#1085#1080#1103':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label15: TLabel
    Left = 768
    Top = 520
    Width = 33
    Height = 32
    Caption = 'F6'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label16: TLabel
    Left = 8
    Top = 368
    Width = 276
    Height = 64
    Caption = #1055#1054#1044#1058#1042#1045#1056#1044#1048#1058#1068' '#13#10#1042#1054#1047#1042#1056#1040#1058' '#1048#1047#1044#1045#1051#1048#1071':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label17: TLabel
    Left = 320
    Top = 376
    Width = 150
    Height = 32
    Caption = 'Ctrl + Home'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Shape1: TShape
    Left = 480
    Top = 0
    Width = 1
    Height = 579
  end
  object Label19: TLabel
    Left = 488
    Top = 8
    Width = 244
    Height = 64
    Caption = #1053#1086#1074#1099#1081' '#1095#1077#1082', '#1077#1089#1083#1080#13#10#1086#1090#1082#1088#1099#1090#1072' '#1074#1089#1103' '#1089#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label20: TLabel
    Left = 768
    Top = 24
    Width = 33
    Height = 32
    Caption = 'F9'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 488
    Top = 88
    Width = 132
    Height = 32
    Caption = #1042#1089#1103' '#1089#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label24: TLabel
    Left = 768
    Top = 88
    Width = 48
    Height = 32
    Caption = 'F10'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label18: TLabel
    Left = 488
    Top = 136
    Width = 244
    Height = 64
    Caption = #1058#1077#1082#1091#1097#1080#1081' '#1095#1077#1082', '#1077#1089#1083#1080#13#10#1086#1090#1082#1088#1099#1090#1072' '#1074#1089#1103' '#1089#1084#1077#1085#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label25: TLabel
    Left = 768
    Top = 152
    Width = 48
    Height = 32
    Caption = 'F11'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -29
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label26: TLabel
    Left = 488
    Top = 216
    Width = 176
    Height = 96
    Caption = #1054#1090#1082#1088#1099#1090#1100' '#13#10#1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#13#10#1055#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label27: TLabel
    Left = 768
    Top = 232
    Width = 77
    Height = 32
    Caption = 'Ctrl+C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label28: TLabel
    Left = 488
    Top = 328
    Width = 154
    Height = 96
    Caption = #1055#1077#1095#1072#1090#1100#13#10#1087#1086#1089#1083#1077#1076#1085#1077#1075#1086#13#10#1095#1077#1082#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label29: TLabel
    Left = 768
    Top = 344
    Width = 44
    Height = 32
    Caption = 'F12'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label21: TLabel
    Left = 8
    Top = 496
    Width = 258
    Height = 64
    Caption = #1055#1077#1095#1072#1090#1100' '#13#10#1088#1072#1089#1093#1086#1076#1085#1086#1075#1086' '#1086#1088#1076#1077#1088#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label22: TLabel
    Left = 320
    Top = 512
    Width = 30
    Height = 32
    Caption = 'F7'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label30: TLabel
    Left = 8
    Top = 448
    Width = 242
    Height = 32
    Caption = #1055#1088#1086#1089#1084#1086#1090' '#1080#1079#1076#1077#1083#1080#1103' :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label31: TLabel
    Left = 320
    Top = 448
    Width = 30
    Height = 32
    Caption = 'F8'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
end
