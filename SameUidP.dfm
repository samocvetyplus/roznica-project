object fmSameUIDP: TfmSameUIDP
  Left = 311
  Top = 191
  Width = 452
  Height = 364
  Caption = #1062#1077#1085#1072' '#1091' '#1080#1079#1076#1077#1083#1080#1081' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1099#1084' '#1086#1090#1083#1080#1095#1072#1077#1090#1089#1103' '#1086#1090' '#1094#1077#1085#1099' '#1072#1088#1090#1080#1082#1091#1083#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 360
    Top = 8
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 360
    Top = 40
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object dg1: TDBGridEh
    Left = 0
    Top = 0
    Width = 320
    Height = 337
    Align = alLeft
    AllowedOperations = []
    DataSource = dm2.dsSameUidP
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1048#1076'. '#1085#1086#1084#1077#1088#1072
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1072#1088#1090#1080#1082#1091#1083#1072
        Width = 99
      end
      item
        EditButtons = <>
        FieldName = 'PRICEINV'
        Footers = <>
        Title.Caption = #1062#1077#1085#1072' '#1080#1079#1076#1077#1083#1080#1103
        Width = 93
      end>
  end
end
