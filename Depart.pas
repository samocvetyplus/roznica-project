unit Depart;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, Db, ExtCtrls, Menus, StdCtrls,
  DBCtrls, Grids, DBGrids, RXDBCtrl, PFIBDataSet, M207Grid,
  M207IBGrid, RxDBComb, Mask, Buttons, RxCombos,
  ActnList, DBCtrlsEh, M207DBCtrls, DBGridEh, PrnDbgeh, jpeg,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar, rxToolEdit;
 
type
  TfmDepart = class(TForm)
    StatusBar1: TStatusBar;
    pa1: TPanel;
    tv1: TTreeView;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    pa2: TPanel;
    pmDepart: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    Splitter3: TSplitter;
    Panel3: TPanel;
    fs1: TFormStorage;
    N3: TMenuItem;
    pc1: TPageControl;
    tsDep: TTabSheet;
    tsHead: TTabSheet;
    cbS: TDBCheckBox;
    cbDL: TDBCheckBox;
    Label2: TLabel;
    lcRoot: TDBLookupComboBox;
    tb1: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    SpeedItem4: TSpeedItem;
    SpeedItem5: TSpeedItem;
    siExit: TSpeedItem;
    tb2: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    Label1: TLabel;
    RxDBComboBox1: TRxDBComboBox;
    Label3: TLabel;
    edMargin: TDBEdit;
    Label4: TLabel;
    edPostIndex: TDBEdit;
    Label5: TLabel;
    edCity: TDBEdit;
    Label6: TLabel;
    edAdress: TDBEdit;
    Label7: TLabel;
    edPhone: TDBEdit;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    M207DBColor1: TM207DBColor;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBEdit2: TDBEdit;
    Label9: TLabel;
    spitSort: TSpeedItem;
    rdgrPreview: TDBRadioGroup;
    lcDep: TDBLookupComboBox;
    Bevel1: TBevel;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acExit: TAction;
    acSort: TAction;
    acAddEmp: TAction;
    acDelEmp: TAction;
    acKeyEmp: TAction;
    cbAllWh: TDBCheckBoxEh;
    siUser_Discharge: TSpeedItem;
    acuser_discharge: TAction;
    siViewUserDischarge: TSpeedItem;
    acViewDischarge: TAction;
    edApplDepNum: TDBEdit;
    Label11: TLabel;
    chbxDEL: TDBCheckBox;
    dgEmp: TDBGridEh;
    prgr: TPrintDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tv1Change(Sender: TObject; Node: TTreeNode);
    procedure tv1Changing(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
    procedure tv1Edited(Sender: TObject; Node: TTreeNode; var S: String);
    procedure tv1Editing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure tv1Expanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure tv1Exit(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure tv1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tv1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tv1StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure tv1Deletion(Sender: TObject; Node: TTreeNode);
    procedure FormActivate(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure lcRootCloseUp(Sender: TObject);
    procedure rdgrPreviewClick(Sender: TObject);
    procedure cbAllWhClick(Sender: TObject);
    procedure lcDepCloseUp(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acSortExecute(Sender: TObject);
    procedure acAddEmpExecute(Sender: TObject);
    procedure acDelEmpExecute(Sender: TObject);
    procedure acKeyEmpExecute(Sender: TObject);
    procedure acViewDischargeExecute(Sender: TObject);
    procedure acuser_dischargeExecute(Sender: TObject);
    procedure acKeyEmpUpdate(Sender: TObject);
    procedure DBCheckBox3MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBCheckBox3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure chbxDELMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dgEmpKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    DraggingNode: TTreeNode;
    LogOperIdForm : string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure AddDepart;
    procedure DelDepart;
  end;

var
  fmDepart: TfmDepart;

implementation

uses comdata, DBTree, Access, InsRepl, Data, Variants, M207Proc, Sort, datafr,
  Data3, JewConst, {UserDisCharge,} dbUtil, MsgDialog;

{$R *.DFM}


procedure TfmDepart.WMSysCommand(var Message: TWMSysCommand);
begin
 if (Message.CmdType = SC_MINIMIZE) THEN MinimizeApp
  else inherited;
end;


procedure TfmDepart.FormCreate(Sender: TObject);
var i: integer;
begin
  Width:=Screen.Width;
  Height:=Screen.Height;
  tb1.WallPaper:=wp;
  tb2.WallPaper:=wp;
  with pc1 do
    for i:=0 to PageCount-1 do
      Pages[i].TabVisible:=False;
  with dmCom do
    begin
      if not CenterDep then
      begin
       taEmp.SelectSQL[3]:=' and (d_depid='+inttostr(SelfDepId)+' or allwh=1) ';
       qudept.SQL[4]:=' and d_depid='+inttostr(SelfDepId)+' ';
      end;
      taEmp.SelectSQL[4]:='';      
      quDep.SelectSQL[6]:='where d_depid>0';

{      if dmcom.Manager then taEmp.SelectSQL[3]:=taEmp.SelectSQL[3]+' and permissions_user>=1 '
      else if dmcom.Seller then taEmp.SelectSQL[3]:=taEmp.SelectSQL[3]+' and permissions_user>=2 '
           else taEmp.SelectSQL[3]:=taEmp.SelectSQL[3]+' and (permissions_user>=0 or permissions_user is null)';}

      tr.Active:=True;
      InitTree('������������ �����������', tv1, NULL);
      taRec.Active:=True;
      SelfName:=taRecComp.AsString;
      SelfId:=taRecD_CompId.AsInteger;
      OpenDataSets([quAllComp, taEmp, taNDS,quDep]);
//      taDepart.Active:=True;
//      taComp.Active:=True;
      SetCBDropDown(lcDEP);
      SetCBDropDown(lcRoot);
    end;
  dm.R_Item:='D_DEP';
//  if CenterDep then dgEmp.ColumnByName['NUMEMP'].Visible:=false;
  SpeedItem4.Enabled:=CenterDep and dmcom.Adm;
  SpeedItem5.Enabled:=CenterDep and dmcom.Adm;
  N1.Enabled:=CenterDep and dmcom.Adm;
  N2.Enabled:=CenterDep and dmcom.Adm;
  N3.Enabled:=CenterDep and dmcom.Adm;
//  tsDep.Enabled:=CenterDep;
 //**************tsDep.Enabled********************************//
   DBEdit1.Enabled:=CenterDep and dmcom.Adm;
   cbDL.Enabled:=CenterDep and dmcom.Adm;
   cbS.Enabled:=CenterDep and dmcom.Adm;
   DBCheckBox6.Enabled:=CenterDep and dmcom.Adm;
   RxDBComboBox1.Enabled:=CenterDep and dmcom.Adm;
   edMargin.Enabled:=CenterDep and dmcom.Adm;
   DBCheckBox2.Enabled:=CenterDep and dmcom.Adm;
   edPostIndex.Enabled:=CenterDep and dmcom.Adm;
   edCity.Enabled:=CenterDep and dmcom.Adm;
   edAdress.Enabled:=CenterDep and dmcom.Adm;
   edPhone.Enabled:=CenterDep and dmcom.Adm;
   M207DBColor1.Enabled:=CenterDep and dmcom.Adm;
   DBEdit2.Enabled:=CenterDep and dmcom.Adm;
   DBCheckBox4.Enabled:=CenterDep and dmcom.Adm;
   DBCheckBox3.Enabled:=CenterDep and dmcom.Adm;
   acuser_discharge.Enabled := dmcom.Adm and CenterDep;
   acViewDischarge.Enabled := dmcom.Adm and CenterDep;
   edApplDepNum.Enabled := dmcom.Adm and CenterDep;
   chbxDEL.Enabled := dmcom.Adm and CenterDep;
 //**********************************************************//

  tsHead.Enabled:=CenterDep  and dmcom.Adm;
  dgEmp.FieldColumns['FNAME'].ReadOnly:=(not CenterDep) or dmcom.Seller or dmcom.Manager;
  dgEmp.FieldColumns['LNAME'].ReadOnly:=(not CenterDep) or dmcom.Seller or dmcom.Manager;
  dgEmp.FieldColumns['SNAME'].ReadOnly:=(not CenterDep) or dmcom.Seller or dmcom.Manager;
  dgEmp.FieldColumns['NUMEMP'].ReadOnly:=dmcom.Seller;
  SpeedItem1.Enabled:=CenterDep and dmcom.Adm;
  SpeedItem2.Enabled:=CenterDep and dmcom.Adm;
  SpeedItem3.Enabled:=CenterDep and dmcom.Adm;
  lcdep.Enabled:=CenterDep and dmcom.Adm;
  cbAllWh.Enabled:=CenterDep and dmcom.Adm;
  rdgrPreview.Enabled:= not dmcom.Seller;
  LogOperIdForm := dm3.defineOperationId(dm3.LogUserID);
end;

procedure TfmDepart.FormResize(Sender: TObject);
begin
  siExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmDepart.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  tv1Exit(tv1);
  with dmCom do
    begin
//      tr.CommitRetaining;
      PostDataSets([taDep, taEmp,  taRec]);
      SelfName:=taRecComp.AsString;
      SelfId:=taRecD_CompId.AsInteger;
      CloseDataSets([taDep, taRec, taEmp, quAllComp,quDep]);
      if not CenterDep then begin taEmp.SelectSQL[3]:=' '; qudept.SQL[4]:=' ';  end;
      quDep.SelectSQL[6]:='where d_depid>0 and isdelete<>1 ';
      taEmp.SelectSQL[4]:='';
      tr.CommitRetaining;
    end;
  dm.R_Item:='';
end;

procedure TfmDepart.tv1Change(Sender: TObject; Node: TTreeNode);
var nd: TNodeData;
begin
  try
    nd:=TNodeData(Node.Data);
    if nd<>NIL then
      with  dmCom do
        begin
          if TNodeData(Node.Data).Code<>NULL then
            begin
              D_DepId:=TNodeData(Node.Data).Code;
              with taDep do
                begin
                  Active:=False;
                  Open;
                end;
              pc1.ActivePage:=tsDep;
            end
          else  pc1.ActivePage:=tsHead;

         with taEmp do
            begin
              Active:=False;
              if TNodeData(Node.Data).Code<>NULL then taEmp.SelectSQL[4]:='  and d_depid = '+taDepD_DEPID.asString
              else taEmp.SelectSQL[4]:='';
              Open;
            end;
       end;
  except
    on e: exception do MessageDialog(e.Message, mtError, [mbOK], 0);
  end;
end;

procedure TfmDepart.tv1Changing(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
  with dmCom do
    begin
      taEmp.Active:=False;
      taDep.Active:=False;
    end;
end;

procedure TfmDepart.tv1Edited(Sender: TObject; Node: TTreeNode;
  var S: String);
begin
  if Node.Data<>NIL then
     with dmCom, taDep do
       begin
         if not (State in [dsEdit, dsInsert]) then Edit;
         taDepName.AsString:=S;
       end;
end;

procedure TfmDepart.tv1Editing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  AllowEdit:=Node<>tv1.TopItem;
end;

procedure TfmDepart.tv1Expanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
begin
  ExpandTree(TTreeView(Node.TreeView), Node, NodeText1, dmCom.quDepT, [0,1,3]);
end;

procedure TfmDepart.tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
begin
  GetNodeImages(Node);
end;

procedure TfmDepart.tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
begin
  GetSelNodeImages(Node);
end;

procedure TfmDepart.tv1Exit(Sender: TObject);
begin
  if tv1.IsEditing then tv1.Selected.EndEdit(False);
end;


procedure TfmDepart.AddDepart;
var n: TTreeNode;
    nd: TNodeData;
begin
  if tv1.Selected=NIL then tv1.Selected:=tv1.TopItem;
  with tv1 do
    begin
      tv1.Selected.HasChildren:=True;
      n:=Items.AddChild(tv1.Selected, '');
      n.Data:=NIL;
      Selected:=n;
      n.EditText;
    end;

   nd:=TNodeData.Create;
   n.Data:=nd;
   with nd do
     begin
       Code:=dmCom.GetID(3);
       Loaded:=False;
     end;

   with dmCom, taDep do
     begin
       Params[0].AsInteger:=nd.Code;
       Active:=True;
       Insert;

       taDepD_DepId.AsInteger:=nd.Code;
       if  TNodeData(n.Parent.Data).Code<>NULL then taDepParent.AsInteger:=TNodeData(n.Parent.Data).Code
       else taDepParent.Clear;
     end;
end;

procedure TfmDepart.DelDepart;
var n: TTreeNode;
begin
  n:=tv1.Selected;
  if (n=tv1.TopItem) or (n=NIL) then exit;

  dmCom.taDep.Delete;

  n.Delete;
end;


procedure TfmDepart.N1Click(Sender: TObject);
begin
  AddDepart;
end;

procedure TfmDepart.N2Click(Sender: TObject);
begin
  DelDepart;
end;

procedure TfmDepart.tv1DragDrop(Sender, Source: TObject; X, Y: Integer);
var Node: TTreeNode;
begin
  Node:=tv1.GetNodeAt(X,Y);
  if Node=NIL then exit;
  if Node.Data=NIL then exit;

  DraggingNode.MoveTo(Node, naAddChild);

  with dmCom, taDep do
    begin
      if not (State in [dsInsert, dsEdit]) then Edit;
      if TNodeData(Node.Data).Code<>NULL then taDepParent.AsInteger:=TNodeData(Node.Data).Code
      else taDepParent.Clear;
      Post;
    end;
end;

procedure TfmDepart.tv1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var Node: TTreeNode;
begin
  Node:=tv1.GetNodeAt(X,Y);
  Accept:=(Node<>NIL) AND
          (Node<>DraggingNode) AND
          (Node<>DraggingNode.Parent) AND
          (DraggingNode<>tv1.TopItem);
end;

procedure TfmDepart.tv1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  if tv1.Selected=tv1.TopItem then SysUtils.Abort;
  DraggingNode:=tv1.Selected;
end;

procedure TfmDepart.tv1Deletion(Sender: TObject; Node: TTreeNode);
begin
  if (Node<>NIL) and Assigned(Node.Data) then TNodeData(Node.Data).Free;
end;

procedure TfmDepart.FormActivate(Sender: TObject);
begin
  tv1.TopItem.Text:=dmCom.SelfName;
end;

procedure TfmDepart.N3Click(Sender: TObject);
begin
  with tv1 do
    if Selected<>NIL then Selected.EditText;
end;

procedure TfmDepart.lcRootCloseUp(Sender: TObject);
begin
  tv1.TopItem.Text:=lcRoot.Text;
end;

procedure TfmDepart.rdgrPreviewClick(Sender: TObject);
begin
 //���� �������� �������� ������������, �� ������ ��� ���������
 if dmCom.UserId=dmCom.taEmpD_EMPID.AsInteger then
   DocPreview := TdcPreview(rdgrPreview.ItemIndex);
end;

procedure TfmDepart.cbAllWhClick(Sender: TObject);
begin
  lcDep.Enabled:= not cbAllWh.Checked;
  with dmCom do
    if UserId=taEmp.FieldByName('D_EMPID').AsInteger then
      USerAllWh:=cbAllWh.Checked;
end;

procedure TfmDepart.lcDepCloseUp(Sender: TObject);
begin
  with dmCom do
    if UserId=taEmp.FieldByName('D_EMPID').AsInteger then
      if UserAllWh or taEmp.FieldByName('D_DepId').IsNull then UserDepId:=-1
           else UserDepId:=taEmp.FieldByName('D_DepId').AsInteger;
end;

procedure TfmDepart.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDepart.acDelExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_DepartDel, LogOperIdForm);
  DelDepart;
  dm3.update_operation(LogOperationID);
end;

procedure TfmDepart.acAddExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_DepartAdd, LogOperationID);
  Screen.Cursor := crSQLWait;
  try
    AddDepart;
  finally
    Screen.Cursor := crDefault;
  end;
  dm3.update_operation(LogOperationID);
end;

procedure TfmDepart.acSortExecute(Sender: TObject);
var i: integer;
    nd: TNodeData;
    LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_DepartSort, LogOperationID);
  with dmCom do
    try
      fmSort:=TfmSort.Create(Application);
      with fmSort do
        begin
         lb1.Items.Clear;
         with quTmp do
           begin
             SQL.Text:='SELECT D_DEPID, NAME '+
                       'FROM D_DEP '+
                       'WHERE D_DEPID<>-1000 '+
                       'ORDER BY SORTIND';
             ExecQuery;
             while NOT EOF do
               begin
                 nd:=TNodeData.Create;
                 nd.Code:=Fields[0].AsString;
                 lb1.Items.AddObject(Fields[1].AsString, nd);
                 Next;
               end;
             Close;
           end;

          if ShowModal=mrOK then
            begin
              quTmp.SQL.Text:='update D_DEP set SortInd=?SortInd where D_DepId=?D_DepId';
              for i:=0 to lb1.Items.Count-1 do
                with quTmp  do
                  begin
                    Params[0].AsInteger:=i;
                    Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
                    ExecQuery;
                  end;
              dmCom.tr.CommitRetaining;
              //ReOpenDataSets([dmCom.taMat]);
            end
        end;
    finally
      fmSort.Free;
    end;
  // ���������� ����������� �������
  ExecSQL('execute procedure FillRestart', dmCom.quTmp);
  dm3.update_operation(LogOperationID);
end;

procedure TfmDepart.acAddEmpExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_EmpAdd, LogOperIdForm);
  dmCom.taEmp.Append;
  dm3.update_operation(LogOperationID);
end;

procedure TfmDepart.acDelEmpExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  LogOperationID := dm3.insert_operation(sLog_EmpDel, LogOperIdForm);
  dmCom.taEmp.Delete;
  dm3.update_operation(LogOperationID);
end;

procedure TfmDepart.acKeyEmpExecute(Sender: TObject);
var
  LogOperationID: string;
begin
  if dmCom.taEmpD_EmpId.IsNull then exit;
  LogOperationID := dm3.insert_operation(sLog_EmpKey, LogOperIdForm);
  if dmcom.taEmp.State in [dsEdit, dsInsert] then dmcom.taEmp.Post;  
  fmAccess:=TfmAccess.Create(Application);
  try
    with fmAccess do
      if ShowModal=mrOK then PostDataSet(dmCom.taEmp)
      else CancelDataSet(dmCom.taEmp);
    dmCom.taEmp.Refresh;
  finally
    fmAccess.Free;
  end;
  dm3.update_operation(LogOperIdForm);
end;

procedure TfmDepart.acViewDischargeExecute(Sender: TObject);
begin
 PostDataSets([dmcom.taEmp]);
 //ShowAndFreeForm(TfmUserDisCharge, Self, TForm(fmUserDisCharge), True, False);
end;

procedure TfmDepart.acuser_dischargeExecute(Sender: TObject);
begin
 PostDataSets([dmcom.taEmp]);
 dmcom.taEmp.Edit;
 dmcom.taEmpDISCHARGE.AsInteger:=1;
 dmcom.taEmp.Post;
 ReOpenDataSets([dmcom.taEmp]);
end;

procedure TfmDepart.acKeyEmpUpdate(Sender: TObject);
begin
  if dmCom.User.Adm then acKeyEmp.Enabled := True
  else
  begin
    if ((dmcom.taEmpD_EMPID.AsInteger<>dmcom.User.UserId) and (dmcom.taEmpPERMISSIONS_USER.AsInteger=0)) then acKeyEmp.Enabled:=AppDebug
    else acKeyEmp.Enabled:=true
  end;  
end;

procedure TfmDepart.DBCheckBox3MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
 PostDataSets([dmcom.taDep]);
 if DBCheckBox3.Checked then
 begin
  ExecSQL('update rdb$triggers set rdb$trigger_inactive=1 where rdb$trigger_name in ('#39+
           'D_ADDRESS_BU0'+#39', '#39+'CLIENT_BU0'+#39')', dm.quTmp);

  ExecSQL('update d_address set depid='+dmcom.taDepD_DEPID.AsString+' where d_address_id=1', dm.quTmp);

  ExecSQL('update client set depid='+dmcom.taDepD_DEPID.AsString+'where clientid = -1', dm.quTmp);

  ExecSQL('update rdb$triggers set rdb$trigger_inactive=0 where rdb$trigger_name in ('#39+
           'D_ADDRESS_BU0'+#39', '#39+'CLIENT_BU0'+#39')', dm.quTmp);
 end
end;

procedure TfmDepart.DBCheckBox3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 PostDataSets([dmcom.taDep]);
 if DBCheckBox3.Checked then
 begin
  ExecSQL('update rdb$triggers set rdb$trigger_inactive=1 where rdb$trigger_name in ('#39+
           'D_ADDRESS_BU0'+#39', '#39+'CLIENT_BU0'+#39')', dm.quTmp);

  ExecSQL('update d_address set depid='+dmcom.taDepD_DEPID.AsString+' where d_address_id=1', dm.quTmp);

  ExecSQL('update client set depid='+dmcom.taDepD_DEPID.AsString+'where clientid = -1', dm.quTmp);

  ExecSQL('update rdb$triggers set rdb$trigger_inactive=0 where rdb$trigger_name in ('#39+
           'D_ADDRESS_BU0'+#39', '#39+'CLIENT_BU0'+#39')', dm.quTmp);
 end
end;

procedure TfmDepart.chbxDELMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if dmCom.taDep.State = dsEdit then
  begin
    dmCom.taDep.Post;
    MessageDialog('���������� �������� ��������, ����� �������� ��������� ������������� ���������', mtInformation, [mbOk], 0);
    ExecSQL('execute procedure FILLRESTART', dm3.quTmp);
  end;
end;

procedure TfmDepart.dgEmpKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Shift = [ssCtrl])  and ((char(Key) = 'p') or (char(Key) = 'P')) then prgr.Print;
end;

end.
