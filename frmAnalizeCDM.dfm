object DialogAnalizeCDM: TDialogAnalizeCDM
  Left = 417
  Top = 348
  BorderStyle = bsDialog
  Caption = #1040#1085#1072#1083#1080#1079' '#1076#1083#1103' '#1062#1044#1052
  ClientHeight = 133
  ClientWidth = 253
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox1: TcxGroupBox
    Left = 8
    Top = 8
    Caption = #1055#1077#1088#1080#1086#1076
    TabOrder = 0
    Height = 113
    Width = 161
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 37
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086
    end
    object Label2: TLabel
      Left = 16
      Top = 56
      Width = 31
      Height = 13
      Caption = #1050#1086#1085#1077#1094
    end
    object EditorDateBegin: TcxDateEdit
      Left = 16
      Top = 32
      EditValue = 40179d
      Properties.ArrowsForYear = False
      TabOrder = 0
      Width = 121
    end
    object EditorDateEnd: TcxDateEdit
      Left = 16
      Top = 72
      EditValue = 40179d
      TabOrder = 1
      Width = 121
    end
  end
  object cxButton1: TcxButton
    Left = 176
    Top = 16
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton
    Left = 176
    Top = 48
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
  end
  object DBSell: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select s1.model, s1.model$size, s1.department$id, s1.sell$count,' +
        ' cast(0 as integer) storage$count'
      'from analize$cdm$sell(:begin$date, :end$date) s1')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 168
    Top = 80
    oFetchAll = True
  end
  object DBStorage: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select s2.model, s2.model$size, s2.department$id, cast(0 as inte' +
        'ger) sell$count, s2.storage$count'
      'from analize$cdm$storage(:begin$date, :end$date) s2'
      '')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 208
    Top = 80
    oFetchAll = True
  end
  object ProviderSell: TDataSetProvider
    DataSet = DBSell
    Left = 72
    Top = 56
  end
  object Sell: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'BEGIN$DATE'
        ParamType = ptInputOutput
      end
      item
        DataType = ftDate
        Name = 'END$DATE'
        ParamType = ptInputOutput
      end>
    ProviderName = 'ProviderSell'
    BeforeOpen = OnBeforeOpen
    Left = 72
    Top = 8
    object SellMODEL: TStringField
      FieldName = 'MODEL'
      Size = 32
    end
    object SellMODELSIZE: TStringField
      FieldName = 'MODEL$SIZE'
      Size = 8
    end
    object SellDEPARTMENTID: TIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object SellSELLCOUNT: TIntegerField
      FieldName = 'SELL$COUNT'
    end
    object SellSTORAGECOUNT: TIntegerField
      FieldName = 'STORAGE$COUNT'
    end
  end
  object ProviderStorage: TDataSetProvider
    DataSet = DBStorage
    Left = 120
    Top = 56
  end
  object Storage: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftDate
        Name = 'BEGIN$DATE'
        ParamType = ptInputOutput
      end
      item
        DataType = ftDate
        Name = 'END$DATE'
        ParamType = ptInputOutput
      end>
    ProviderName = 'ProviderStorage'
    BeforeOpen = OnBeforeOpen
    Left = 120
    Top = 8
    object StorageMODEL: TStringField
      FieldName = 'MODEL'
      Size = 32
    end
    object StorageMODELSIZE: TStringField
      FieldName = 'MODEL$SIZE'
      Size = 8
    end
    object StorageDEPARTMENTID: TIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object StorageSELLCOUNT: TIntegerField
      FieldName = 'SELL$COUNT'
    end
    object StorageSTORAGECOUNT: TIntegerField
      FieldName = 'STORAGE$COUNT'
    end
  end
  object Departments: TpFIBDataSet
    SelectSQL.Strings = (
      'select d_depid ID, stretrim(cast(name as varchar(32))) Name'
      'from d_dep'
      'where d_depid > 0 ')
    Transaction = dmCom.tr
    Database = dmCom.db
    Left = 16
    Top = 64
    oFetchAll = True
    object DepartmentsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DepartmentsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'jew'
    Filter = #1060#1072#1081#1083' '#1072#1085#1072#1083#1080#1079#1072' (*.jew)|*.jew'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Title = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    Left = 16
    Top = 8
  end
end
